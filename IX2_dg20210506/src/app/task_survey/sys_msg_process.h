
#ifndef _SYS_MSG_PROCESS_H
#define _SYS_MSG_PROCESS_H

//czn_20160418_s
#define SURVEY_MSG_CALLER_CLOSE			100
#define SURVEY_MSG_BECALLED_CLOSE		101
#define SURVEY_MSG_BECALLED_BEINVITE	102
#define SURVEY_MSG_CALLTEST_INVITE		103
#define SURVEY_MSG_CALLTEST_CANCEL		104
#define SURVEY_MSG_CALLTEST_ACK			105

#define SURVEY_MSG_KEY_RESPONSE			106

//czn_20161220_s
#define SURVEY_MSG_VIDEO_SERVICE_ON			107
#define SURVEY_MSG_VIDEO_SERVICE_OFF			108
#define SURVEY_MSG_VIDEO_CLIENT_ON			109
#define SURVEY_MSG_VIDEO_CLIENT_OFF			110
#define SURVEY_MSG_VIDEO_CLIENT_BEOFF		111
//czn_20161220_e
#define SURVEY_MSG_CHECK_NETWORK_CARD				112
#define SURVEY_MSG_CHECK_NETWORK_CARD_REBOOT		113

#define SURVEY_MSG_ThreadHeartbeatReq					114
#define SURVEY_MSG_CHECK_NETWORK_CARD_SOFTREBOOT		115
#define SURVEY_MSG_CHECK_NETWORK_WLAN_SOFTREBOOT		116

//czn_20160607_s
typedef struct
{
	VDP_MSG_HEAD 	head;
	Global_Addr_Stru 	addr;		//czn_20160607
} SURVEY_SUNDRY_MSG;
//czn_20160422_s
int API_Survey_SundryDeal(uint8 source_msg_id,uint8 msg_type,Global_Addr_Stru *addr);	
	#define API_Caller_Close_Notice()				API_Survey_SundryDeal(MSG_ID_CALLER,SURVEY_MSG_CALLER_CLOSE,NULL)
	#define API_BeCalled_Close_Notice()			API_Survey_SundryDeal(MSG_ID_BECALLED,SURVEY_MSG_BECALLED_CLOSE,NULL)
	#define API_BeCalled_BeInvite_Notice(s_addr)		API_Survey_SundryDeal(MSG_ID_BECALLED,SURVEY_MSG_BECALLED_BEINVITE,s_addr)
//czn_20160607_e
//czn_20160422_e
//czn_20160418_e
void Call_Survey_Unlink_Deal(UDP_MSG_TYPE *pUdpType);//czn_20160422

int	survey_sys_message_processing( char* pdata, int len );

// lzh_20160503_s
int Interface_monitor_local_video(void);
int Interface_monitor_remote_video(int ip,int dev_id);
int Interface_monitor_video_close(void);
int API_VIDEO_C_SERVICE_TURN_ON_MULTICAST(int ip,unsigned short dev_id,int t_time);

int UdpSurvey_idle_Prcessing(UDP_MSG_TYPE *pUdpType);
int UdpSurvey_Filter_Prcessing( UDP_MSG_TYPE *pUdpType );

// lzh_20160503_e

#endif


