
#include "task_survey.h"
#include "sys_msg_process.h"

#include "task_Hal.h"
#include "task_Beeper.h"

#include "ip_video_cs_control.h"

#include "obj_ip_mon_link.h"		//czn_20161219
#include "task_monitor.h"
#include "obj_DeviceManage.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "obj_UnitSR.h"
#include "task_IoServer.h"

#include "task_Ring.h"    



#include "obj_ip_mon_link.h"
#include "task_monitor.h"
#include "obj_SearchIpByFilter.h"
#include "obj_ThreadHeartBeatService.h"
#include "elog_forcall.h"
#include "define_Command.h"
#include "stack212.h"
#include "task_DXMonitor.h"

void DtCommand_Analyzer(DTSTACK_MSG_TYPE *Message_command);//czn_20170518

//czn_20160827_e
/****************************************************************************************************************************
 * @fn:		survey_sys_message_processing
 *
 * @brief:	����������Ϣ����
 *
 * @param:  pdata 			- ����ָ��
 * @param:  len 			- ���ݳ���
 *
 * @return: 0/ok, 1/full
****************************************************************************************************************************/
int	survey_sys_message_processing( char* pdata, int len )
{
	VDP_MSG_HEAD* 	pMsg 		= (VDP_MSG_HEAD*)pdata;			// �ڲ���Ϣͷ

	HAL_MSG_TYPE*	pHalType	= (HAL_MSG_TYPE*)pdata;
	unsigned int	tp_key;
	unsigned short	tp_x;
	unsigned short	tp_y;
	unsigned char	ts_key;

	UDP_MSG_TYPE*	pUdpType	= (UDP_MSG_TYPE*)pdata;
	// lzh_20160503_s
//cao_20170509 	VtkUnicastCmd_Stru* pSBUDat = (VtkUnicastCmd_Stru*)pUdpType->pbuf;	
	// lzh_20160503_e
	SURVEY_SUNDRY_MSG*  sundry_msg = (SURVEY_SUNDRY_MSG*)pdata;	//czn_20160418

	SYS_BRD_MSG_TYPE*	pSysBrdMsg = (SYS_BRD_MSG_TYPE*)pdata;
	DTSTACK_MSG_TYPE* dtStackMsg = (DTSTACK_MSG_TYPE*)pdata;;

	char* pChar;
	SearchIpRspData onLineDsListData;
	
	// ����hal���������
	switch( pMsg->msg_source_id )
	{
		case MSG_ID_udp_stack:

			printf("-----------MSG_ID_udp_stack--------\r\n");      //lyx_20171218
			
			// udp ���ݹ���
			UdpSurvey_Filter_Prcessing(pUdpType);
			API_VtkUnicastCmd_Analyzer(pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);
			// udp ҵ�����
			//bprintf("RECV_UDP_MSG:sourceip=%08x,cmd=%04x sn = %d Call_Survey_State = %d \n",pUdpType->target_ip,pUdpType->cmd,pUdpType->id,Call_Survey_State);
			#if 0
			if(Call_Survey_State == CALL_SURVEY_ASCALLER)
			{
				//API_DispatchOnePkt_ToCaller(CMD_SOURCE_UNICAST,pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);		//czn_20170328
			}
			else if( Call_Survey_State == CALL_SURVEY_ASBECALLED )
			{
				//API_DispatchOnePkt_ToBeCalled(CMD_SOURCE_UNICAST,pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);	//czn_20170328
			}
			else if( Call_Survey_State == CALL_SURVEY_MONITOR )
			{
				ip_mon_command_process( pUdpType );				
			}
			else
			{
				UdpSurvey_idle_Prcessing( pUdpType );
			}
			#endif
			break;
		case MSG_ID_StackAI:		//czn_20170518
			DtCommand_Analyzer((DTSTACK_MSG_TYPE*)pdata);
			if(dtStackMsg->command_mode == MODE_NEW_COM)
			{
				NewCommand_Process((DTSTACK_MSG_TYPE*)pdata);
			}
			else
			{
				DtCommand_Analyzer((DTSTACK_MSG_TYPE*)pdata);
			}
			break;
		//case MSG_ID_CALLER:		//czn_20170518
		//case MSG_ID_BECALLED:
			
		default:
			//bprintf("SURVEY_MSG %d\n",sundry_msg->head.msg_type);

			if(sundry_msg->head.msg_type == SURVEY_MSG_CALLER_CLOSE)
			{
				Call_Survey_State = CALL_SURVEY_IDLE;
				//czn_20160422
				Set_IPCallLink_Status(CLink_Idle);
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_BECALLED_CLOSE)
			{
				Call_Survey_State = CALL_SURVEY_IDLE;
				//czn_20160422
				Set_IPCallLink_Status(CLink_Idle);
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_BECALLED_BEINVITE)
			{
				Interface_BeCalled_Start(&(sundry_msg->addr));			//czn_20160607					
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_VIDEO_CLIENT_BEOFF)
			{
				monitor_video_client_beoff();					
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_CHECK_NETWORK_CARD_REBOOT)
			{
				dprintf("HardwareRestar\n");
				HardwareRestar();
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_CHECK_NETWORK_CARD)
			{
				//printf("11111111111111111111111111111111111111111111\n");
				API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 1, &onLineDsListData);
			}
			else if(sundry_msg->head.msg_type == SURVEY_MSG_ThreadHeartbeatReq)
			{
				RecvThreadHeartbeatReply(Survey_Heartbeat_Mask);
			}
			else if(sundry_msg->head.msg_type ==SURVEY_MSG_CHECK_NETWORK_CARD_SOFTREBOOT)
			{
				log_w("have reset lan");
				SoftwareResetLanNetWork();
			}
			else if(sundry_msg->head.msg_type ==SURVEY_MSG_CHECK_NETWORK_WLAN_SOFTREBOOT)
			{
				log_w("have reset wlan");
				SoftwareResetWlanNetWork();
			}
			break;
	}			
}

int API_Survey_SundryDeal(uint8 source_msg_id,uint8 msg_type,Global_Addr_Stru *addr)	//czn_20160607		//return 0:ok,-1:fail	//czn_20160422
{
	SURVEY_SUNDRY_MSG msg;
	msg.head.msg_source_id 	= source_msg_id;
	msg.head.msg_target_id  = MSG_ID_survey;
	msg.head.msg_type		= msg_type;
	if(addr != NULL)
	{
		msg.addr			= *addr;
	}

	if(push_vdp_common_queue(task_control.p_msg_buf, (char*)&msg, sizeof(SURVEY_SUNDRY_MSG) )== 0)
	{
		return 0;
	}

	return -1;
}


#if 0
int Interface_Just_Caller_State(void)
{
	return (Call_Survey_State == CALL_SURVEY_ASCALLER )?1:0;
}

int Interface_Just_BeCalled_State(void)
{
	return (Call_Survey_State == CALL_SURVEY_ASBECALLED)?1:0;	
}

int Interface_Caller_Start(int ip)
{
	//czn_20160422
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
//cao_20170509 		if(API_Caller_Invite(&task_control, ip) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
//cao_20170509 			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	
	return -1;
}

//czn_20160607
int Interface_Caller_Start_Use_GlobalAddr(Global_Addr_Stru *target_addr)
{
	//czn_20160422
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
//cao_20170509 		if(Get_Caller_State() != 0)
		{
//cao_20170509 			API_Caller_Unlink(&task_control);
		}
		
//cao_20170509 		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
//cao_20170509 			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_MONITOR)	//czn_20160614
	{
//cao_20170509		if(GetCurMenuCnt() == MENU_004_MONITOR)
		{
			api_video_c_service_turn_off(); 	
			popDisplayLastMenu();
		}

		Call_Survey_State = CALL_SURVEY_IDLE;

//cao_20170509 		if(Get_Caller_State() != 0)
		{
//cao_20170509 			API_Caller_Unlink(&task_control);
		}
		
//cao_20170509 		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
//cao_20170509 			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
		Call_Survey_State = CALL_SURVEY_IDLE;
		Set_IPCallLink_Status(CLink_Idle);

//cao_20170509 		if(Get_BeCalled_State() != 0)
		{
//cao_20170509 			API_BeCalled_Unlink(&task_control);
		}

//cao_20170509 		if(Get_Caller_State() != 0)
		{
//cao_20170509 			API_Caller_Unlink(&task_control);
		}
		
//cao_20170509 		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
//cao_20170509 			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
		Call_Survey_State = CALL_SURVEY_IDLE;
		Set_IPCallLink_Status(CLink_Idle);
		
//cao_20170509 		if(Get_Caller_State() != 0)
		{
//cao_20170509 			API_Caller_Unlink(&task_control);
		}
		
//cao_20170509 		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
//cao_20170509 			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	
	
	return -1;
}
//czn_20160607
int Interface_BeCalled_Start(Global_Addr_Stru *s_addr)	
{
	
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
		//czn_20161220_s
		/*if(is_monitor_client_busy() == 1)
		{
			monitor_client_montocall();
			usleep(500000);
		}*/
		//czn_20161220_e
 		if(Get_BeCalled_State() != 0)
		{
			API_BeCalled_Unlink(&task_control);
		}
		
		
		
		Register_BeCalled_Info(s_addr);
		
		//bprintf("Interface_BeCalled_Start ------------test\n");
		//return 0;
		
 		if(API_BeCalled_Invite(&task_control,s_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASBECALLED;
			
			Set_IPCallLink_Status(CLink_AsBeCalled);
 			Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

			return 0;
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_MONITOR)	//czn_20160614
	{
		if(GetCurMenuCnt() == MENU_004_MONITOR)
		{
			api_video_c_service_turn_off(); 	
			popDisplayLastMenu();
		}

		Call_Survey_State = CALL_SURVEY_IDLE;

		if(Get_BeCalled_State() != 0)
		{
 			API_BeCalled_Unlink(&task_control);
		}
		
		Register_BeCalled_Info(s_addr);
 		if(API_BeCalled_Invite(&task_control,s_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASBECALLED;
			
			Set_IPCallLink_Status(CLink_AsBeCalled);
			Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

			return 0;
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
		if(Get_IPCallLink_Status() == CLink_Idle)
		{
			Call_Survey_State = CALL_SURVEY_IDLE;
			
			if(Get_BeCalled_State() != 0)
			{
				API_BeCalled_Unlink(&task_control);
			}
			
			Register_BeCalled_Info(s_addr);
			if(API_BeCalled_Invite(&task_control,s_addr) == 0)
			{
				Call_Survey_State = CALL_SURVEY_ASBECALLED;
				
				Set_IPCallLink_Status(CLink_AsBeCalled);
				Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

				return 0;
			}
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
		if(Get_IPCallLink_Status() == CLink_Idle)
		{
			Call_Survey_State = CALL_SURVEY_IDLE;
			
//cao_20170509 			if(Get_Caller_State() != 0)
			{
//cao_20170509 				API_Caller_Unlink(&task_control);
			}

//cao_20170509 			if(Get_BeCalled_State() != 0)
			{
//cao_20170509 				API_BeCalled_Unlink(&task_control);
			}
			
			Register_BeCalled_Info(s_addr);
//cao_20170509 			if(API_BeCalled_Invite(&task_control,s_addr) == 0)
			{
				Call_Survey_State = CALL_SURVEY_ASBECALLED;
				
				Set_IPCallLink_Status(CLink_AsBeCalled);
//cao_20170509 				Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

				return 0;
			}
		}
	}
	return -1;
}

int Interface_BeCalled_Hook(void)
{
//cao_20170509 	API_BeCalled_200ok();
	return 0;
}

int Interface_Call_Cancel(void)
{
	if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
//cao_20170509 		API_BeCalled_Cancel();
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
//cao_20170509 		API_Caller_Cancel();
	}
	else
	{
		return 1;	//czn_20160503 //call_survey_idle
	}
	return 0;
}


#else
int Interface_Just_Caller_State(void)
{
	return (Call_Survey_State == CALL_SURVEY_ASCALLER )?1:0;
}

int Interface_Just_BeCalled_State(void)
{
	return (Call_Survey_State == CALL_SURVEY_ASBECALLED)?1:0;	
}

int Interface_Caller_Start(int ip)
{
#if 0
	//czn_20160422
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
		if(API_Caller_Invite(&task_control, ip) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
#endif	
	return -1;
}


//czn_20160607
int Interface_Caller_Start_Use_GlobalAddr(Global_Addr_Stru *target_addr)
{
#if 0
	//czn_20160422
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
		if(Get_Caller_State() != 0)
		{
			API_Caller_Unlink(&task_control);
		}
		
		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_MONITOR)	//czn_20160614
	{
		//if(GetCurMenuCnt() == MENU_004_MONITOR)    //lyx 20170614
		//{
			api_video_c_service_turn_off(); 	
		//	popDisplayLastMenu();
		//}

		Call_Survey_State = CALL_SURVEY_IDLE;

		if(Get_Caller_State() != 0)
		{
			API_Caller_Unlink(&task_control);
		}
		
		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
		Call_Survey_State = CALL_SURVEY_IDLE;
		Set_IPCallLink_Status(CLink_Idle);

		if(Get_BeCalled_State() != 0)
		{
			API_BeCalled_Unlink(&task_control);
		}

		if(Get_Caller_State() != 0)
		{
			API_Caller_Unlink(&task_control);
		}
		
		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
		Call_Survey_State = CALL_SURVEY_IDLE;
		Set_IPCallLink_Status(CLink_Idle);
		
		if(Get_Caller_State() != 0)
		{
			API_Caller_Unlink(&task_control);
		}
		
		if(API_Caller_Invite_Use_GlobalAddr(&task_control, target_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASCALLER;
			
			Set_IPCallLink_Status(CLink_AsCaller);
			Set_IPCallLink_CallerData(&Caller_Run.target_addr);
			return 0;
		}	
	}
	
#endif	
	return -1;
}
//czn_20160607


int Interface_BeCalled_Start(Global_Addr_Stru *s_addr)	
{
	#if 0
	if(Call_Survey_State == CALL_SURVEY_IDLE)
	{
		//czn_20161220_s
		/*if(is_monitor_client_busy() == 1)
		{
			monitor_client_montocall();
			usleep(500000);
		}*/
		//czn_20161220_e
		if(Get_BeCalled_State() != 0)
		{
			API_BeCalled_Unlink(&task_control);
		}
		
		
		
		Register_BeCalled_Info(s_addr);
		
		//bprintf("Interface_BeCalled_Start ------------test\n");
		//return 0;
		
		if(API_BeCalled_Invite(&task_control,s_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASBECALLED;
			
			Set_IPCallLink_Status(CLink_AsBeCalled);
			Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

			return 0;
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_MONITOR)	//czn_20160614
	{
		//if(GetCurMenuCnt() == MENU_004_MONITOR)      //lyx_20170614
		//{
			api_video_c_service_turn_off(); 	
		//	popDisplayLastMenu();
		//}

		Call_Survey_State = CALL_SURVEY_IDLE;

		if(Get_BeCalled_State() != 0)
		{
			API_BeCalled_Unlink(&task_control);
		}
		
		Register_BeCalled_Info(s_addr);
		if(API_BeCalled_Invite(&task_control,s_addr) == 0)
		{
			Call_Survey_State = CALL_SURVEY_ASBECALLED;
			
			Set_IPCallLink_Status(CLink_AsBeCalled);
			Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

			return 0;
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
		if(Get_IPCallLink_Status() == CLink_Idle)
		{
			Call_Survey_State = CALL_SURVEY_IDLE;
			
			if(Get_BeCalled_State() != 0)
			{
				API_BeCalled_Unlink(&task_control);
			}
			
			Register_BeCalled_Info(s_addr);
			if(API_BeCalled_Invite(&task_control,s_addr) == 0)
			{
				Call_Survey_State = CALL_SURVEY_ASBECALLED;
				
				Set_IPCallLink_Status(CLink_AsBeCalled);
				Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

				return 0;
			}
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
		if(Get_IPCallLink_Status() == CLink_Idle)
		{
			Call_Survey_State = CALL_SURVEY_IDLE;
			
			if(Get_Caller_State() != 0)
			{
				API_Caller_Unlink(&task_control);
			}

			if(Get_BeCalled_State() != 0)
			{
				API_BeCalled_Unlink(&task_control);
			}
			
			Register_BeCalled_Info(s_addr);
			if(API_BeCalled_Invite(&task_control,s_addr) == 0)
			{
				Call_Survey_State = CALL_SURVEY_ASBECALLED;
				
				Set_IPCallLink_Status(CLink_AsBeCalled);
				Set_IPCallLink_BeCalledData(&BeCalled_Run.source_addr);

				return 0;
			}
		}
	}
	#endif
	return -1;
}




int Interface_BeCalled_Hook(void)
{
	//API_BeCalled_200ok();
	return 0;
}

int Interface_Call_Cancel(void)
{
#if 0
	if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
		API_BeCalled_Cancel();
	}
	else if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
		API_Caller_Cancel();
	}
	else
	{
		return 1;	//czn_20160503 //call_survey_idle
	}
#endif
	return 0;
}


#endif





//czn_20160607
void Call_Survey_Unlink_Deal(UDP_MSG_TYPE *pUdpType)
{
	uint8 rspbuf[2];

	rspbuf[1] = 2;
	
	if(Call_Survey_State == CALL_SURVEY_ASCALLER)
	{
//cao_20170509 		if(API_Caller_Unlink(&task_control) == 0)
		{
			rspbuf[1] = 1;
			Call_Survey_State = CALL_SURVEY_IDLE;
			Set_IPCallLink_Status(CLink_Idle);
		}
	}
	else if(Call_Survey_State == CALL_SURVEY_ASBECALLED)
	{
//cao_20170509 		if(API_BeCalled_Unlink(&task_control) == 0)
		{
			rspbuf[1] = 1;
			Call_Survey_State = CALL_SURVEY_IDLE;
			Set_IPCallLink_Status(CLink_Idle);
		}
	}
	else
	{
		rspbuf[1] = 1;
		Set_IPCallLink_Status(CLink_Idle);
	}

	//rspbuf[0] = VTK_CMD_UNLINK_REP_1082 >> 8;
	//rspbuf[1] = VTK_CMD_UNLINK_REP_1082 & 0xff;
	//rspbuf[2] = pUdpType->pbuf[2];
	//rspbuf[3] = pUdpType->pbuf[3];

	//int id = (rspbuf[2]<<8) | rspbuf[3];
//cao_20170509 	api_udp_c5_ipc_send_rsp(pUdpType->target_ip,VTK_CMD_UNLINK_REP_1082,pUdpType->id,rspbuf,2);
	
}

// lzh_20160503_s

// return: 0/ok, -1/err

//czn_20161219_s
int Interface_monitor_local_video(void)
{
	#if 0
	if( open_monitor_local() == 0 )
	{
		Call_Survey_State = CALL_SURVEY_MONITOR;
		return 0;
	}
	else
		return -1;
	#else
	return -1;
	#endif
}

// return: 0/ok, -1/state err, 1/link err, 2/ng, 3/start remote video err
int Interface_monitor_remote_video(int ip , int dev_id )	
{	
	#if 0
	int link_result;	// 0/ok, -1/�豸�쳣��1/proxy ok, 2/�豸������
	int remote_ip;
	unsigned char remote_dev_id;
	
	if( Call_Survey_State == CALL_SURVEY_IDLE )
	{
		link_result = send_ip_mon_link_req( ip, dev_id, &remote_ip, &remote_dev_id );
		// ��������
		if( link_result == 0 )
		{
			if( open_monitor_remote(remote_ip) == 0 )
			{
				dprintf("open_monitor_remote: mon link 0x%08x:%d req ok!\n",remote_ip,remote_dev_id);			
				Call_Survey_State = CALL_SURVEY_MONITOR;
				return 0;
			}
			else
			{
				return 3;
			}
		}
		// �豸����
		else if( link_result == -1 )
		{
			dprintf("Interface_monitor_remote_video: mon link req er!\n");
			return 1;
		}
		// ����������
		else
		{
			dprintf("Interface_monitor_remote_video: mon link req ng!\n");
			return 2;			
		}
	}
	else
	{
		return -1;
	}
	#else
	return -1;
	#endif
}

// return: 0/ok, -1/state err, 1/stop remote video err
int Interface_monitor_video_close(void)
{
	#if 0
	if( Call_Survey_State != CALL_SURVEY_IDLE )
	{
		if( close_monitor() == 0 )
		{
			Call_Survey_State = CALL_SURVEY_IDLE;
			return 0;
		}
		else 
			return 1;
	}
	else
		return -1;
	#else
	return -1;
	#endif
}
//czn_20161205
int API_VIDEO_C_SERVICE_TURN_ON_MULTICAST(int ip,unsigned short dev_id,int t_time)
{
	#if 0
	int link_result;	// 0/ok, -1/�豸�쳣��1/proxy ok, 2/�豸������
	int remote_ip;
	unsigned char remote_dev_id;
	
	link_result = send_ip_mon_link_req( ip, dev_id, &remote_ip, &remote_dev_id );
	// ��������
	if( link_result == 0 )
	{
		if( api_video_c_service_turn_on( ip_video_multicast, remote_ip, remote_dev_id, t_time ) == 0 )
		{
			dprintf("mon link 0x%08x:%d req ok!\n",remote_ip,remote_dev_id);		
			return 0;
		}
		else
		{
			return 3;
		}
	}
	// �豸����
	else if( link_result == -1 )
	{
		dprintf("mon link req er!\n");
		return 1;
	}
	// ����������
	else
	{
		dprintf("mon link req ng!\n");
		return 2;			
	}
	#else
	return -1;
	#endif
}
//czn_20161219_e
//czn_20160516
int UdpSurvey_idle_Prcessing(UDP_MSG_TYPE *pUdpType)
{
	//VtkUnicastCmd_Stru* pSBUDat = (VtkUnicastCmd_Stru*)pUdpType->pbuf;	
	//short vtkcmd = (pSBUDat->cmd_type<<8) |pSBUDat->cmd_sub_type;	
	
	switch(pUdpType->cmd)
	{
		// ��������
//cao_20170509 		case VTK_CMD_DIAL_1003:
//cao_20170509 			API_DispatchOnePkt_ToBeCalled(CMD_SOURCE_UNICAST,pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);	//czn_20170328
			break;
		// ��������
//cao_20170509 		case VTK_CMD_MON_1004:
			break;
			
		default:
			//czn_20161220
//cao_20170509 			if(pUdpType->pbuf[0] == GlMonMr)
			{
				//ip_mon_command_process( pUdpType );
			}
			break;
	}
	return 0;
}

int STM8_CODE_VERSION = 0;
const char STR_FOR_CODE_VERSION[] = {"V1.0.0"};

#ifdef PID_IX482
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
		"IX482",
		"IX482-a1.2"
			};
#endif	

#ifdef PID_IXSE
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
		"IX482SE",
		"IX482SE-a1.0"
			};
#endif	

#ifdef PID_IX47
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
	"IX471S",
	"IX471S-a1.2"
	};
#endif	

#ifdef PID_IX481
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
	"IX481",
	"IX481-a1.2"
	};
#endif	

#ifdef PID_DX470
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
	"DX470",
	"DX470-a1.2"
	};
#endif	

#ifdef PID_DX482
	const char* STR_FOR_DEVICE_TYPE_EXT[] = {
	"DX482",
	"DX482-a1.2"
	};
#endif	

Updater_Run_Stru 	Updater_Run = 
{
	.configfile_cnt = 0,
	//.filename[0]={0}		
};

// lzh_20210918_s
void response_iperf_client_start(void *pdata);
// lzh_20210918_e

int UdpSurvey_Filter_Prcessing( UDP_MSG_TYPE *pUdpType )
{
 	VtkUnicastCmd_Stru* pSBUDat = (VtkUnicastCmd_Stru*)pUdpType->pbuf;		
	mon_link_request* pMonLink = (mon_link_request*)pUdpType->pbuf;
	UDP_Image_t*		pImageAdjust = (UDP_Image_t*)pUdpType->pbuf;

	// lzh_20160704_s
	fw_link_response			fw_link_rsp;	
	fw_download_start_request*	pdownload_start_req = (fw_download_start_request*)pUdpType->pbuf;
	fw_download_start_response	fw_download_start_rsp;

	fw_download_verify_request* pfw_download_verify_req = (fw_download_verify_request*)pUdpType->pbuf;
	fw_download_verify_response	fw_download_verify_rsp;
	
	fw_download_update_response	fw_download_update_rsp;
	int tftp_file_len,tftp_file_cks,update_return;	//czn_20160708
	// lzh_20160704_e

	switch( pUdpType->cmd )
	{

	
		//cao_20170304
		
		//�����豸���߼��
		case CMD_DEVIC_SINGLE_LINKING_REQ:
			DeviceManageSingleLinking(pUdpType);
			break;
		//����豸���߼�⿪ʼ
		case CMD_DEVIC_MULTIPLE_LINKING_START_REQ:
			DeviceManageMultipleLinkingStart(pUdpType);
			break;			
		//�豸����
		case CMD_DEVIC_REBOOT_REQ:
			DeviceManageRebootProcess(pUdpType);
			break;
			
		//�豸�ָ���������
		case CMD_DEVIC_RECOVER_REQ:
			DeviceManageRecover(pUdpType);
			break;
		//cao_20170304



		case CMD_NET_MANAGE_RSP:
			break;

		case CMD_CAM_REMOTE_ADJUST_REQ:
		#if 0
			printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CMD_CAM_REMOTE_ADJUST_REQ presource->type = %d ,dir = %d+++++++++++\n",pImageAdjust->type,pImageAdjust->dir);
			if( api_video_s_service_adjust(pImageAdjust) != -1 )
				pImageAdjust->state = 0;
			else
				pImageAdjust->state = 1;
			api_video_s_service_adjust_reply(pUdpType->target_ip,pUdpType->id,pImageAdjust);	//czn_20170803
		#endif			
			break;
		case CMD_CAM_REMOTE_GETPARA_REQ:
			api_video_s_service_getpara_reply(pUdpType->target_ip,pUdpType->id);
			break;
	
		case CMD_CALL_MON_LINK_REQ:		//czn_20170518
			bprintf("Get_Call_Link_Respones\n");
			// lzh_20160503_s
			//Get_Call_Link_Respones(pUdpType);
			if( pMonLink->mon_type== GlMonMr )
			{
				bprintf("Get_mon_Link_request\n");
				if( recv_ip_mon_link_req(pUdpType) == 0 )
				{
					bprintf("recv_ip_mon_link_req, send rsp ok!\n");
				}
			}
			else
			{					
				//Get_Call_Link_Respones(pUdpType);	
				Recv_CallLink_Req(pUdpType->target_ip,pUdpType->id);	//czn_20190527
			}
			// lzh_20160503_e
			break;
		#if 1
 		case VTK_CMD_UNLINK_1002:
			bprintf("Call_Survey_Unlink_Deal\n");
			// lzh_20160503_s
			//Call_Survey_Unlink_Deal(pUdpType);
			if( pSBUDat->call_type == GlMonMr )
			{
				//ip_mon_command_process( pUdpType ); 			
			}
 			else
			{
				Call_Survey_Unlink_Deal(pUdpType);					
			}
			// lzh_20160503_e
			break;
			//czn_20160614
 		case VTK_CMD_DIAL_1003:
			bprintf("VTK_CMD_DIAL_1003\n");
 			//API_DispatchOnePkt_ToBeCalled(CMD_SOURCE_UNICAST,pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);	//czn_20170328
			break;	
		#endif
		// lzh_20160704_s
		case CMD_DEVICE_TYPE_CODE_VER_REQ:
			bprintf("response CMD_DEVICE_TYPE_CODE_VER_REQ\n");
			fw_link_rsp.device_type_rsp		= 1;
			fw_link_rsp.fireware_ver_rsp	= 1;
			//API_Event_IoServer_InnerRead_All(ParaDeviceType, fw_link_rsp.device_type);
			strcpy(fw_link_rsp.device_type, GetSysVerInfo_type());
			memcpy( fw_link_rsp.fireware_ver, STR_FOR_CODE_VERSION,sizeof(STR_FOR_CODE_VERSION)+1 );
			api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_CODE_VER_RSP,(char*)&fw_link_rsp);
			//czn_20160708_s
			Updater_Run.configfile_cnt = 0;
			//Updater_Run.filename[0]	= 0;	//czn_20160827
			system("rm -r /mnt/nand1-2/tftp");
			usleep(50000);
			system("mkdir /mnt/nand1-2/tftp");
			//czn_20160708_e
			break;
		
		case CMD_DEVICE_TYPE_DOWNLOAD_START_REQ:
			bprintf("response CMD_DEVICE_TYPE_DOWNLOAD_START_REQ\n");
			bprintf("tftp: ip = 0x%08x, filename = %s\n",pdownload_start_req->server_ip_addr,pdownload_start_req->filename);
			//fw_download_start_rsp.result = 0; 
			//api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_DOWNLOAD_START_RSP,(char*)&fw_download_start_rsp);
			// ����tftp -g 192..... filename
			//czn_20160827_s
			if(Updater_Run.configfile_cnt == 0)
			{
				//API_Led_NormalLongIrregularFlash(LED_POWER);
			}
			if(start_tftp_download( pdownload_start_req->server_ip_addr, pdownload_start_req->filename ) != 0)
			{
				
				//API_Led_NormalOn(LED_POWER);
			}
			else
			{
				Updater_Run.resource_record[Updater_Run.configfile_cnt].rid = pdownload_start_req->file_type;
				strncpy(Updater_Run.resource_record[Updater_Run.configfile_cnt].filename,pdownload_start_req->filename,SERVER_FILE_NAME-1);
				Updater_Run.configfile_cnt ++;
			}
			//czn_20160827_e
			//czn_20160708_e
			break;
		case CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_REQ:
			bprintf("response CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_REQ\n");
			if( get_tftp_file_checksum( &tftp_file_len,&tftp_file_cks) == -1 )
			{
				bprintf("R-verifyE: len=0x%08x,cks=0x%08x\n",pfw_download_verify_req->file_len,pfw_download_verify_req->checksum);
				bprintf("L-verifyE: len=0x%08x,cks=0x%08x\n",tftp_file_len,tftp_file_cks);
				fw_download_verify_rsp.result = 1;
				api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_RSP, (char*)&fw_download_verify_rsp );
			}
			else
			{
				bprintf("R-verify: len=0x%08x,cks=0x%08x\n",pfw_download_verify_req->file_len,pfw_download_verify_req->checksum);
				bprintf("L-verify: len=0x%08x,cks=0x%08x\n",tftp_file_len,tftp_file_cks);
				if( (pfw_download_verify_req->checksum == tftp_file_cks) && (pfw_download_verify_req->file_len== tftp_file_len) )
				{
					fw_download_verify_rsp.result = 0;
					api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_RSP, (char*)&fw_download_verify_rsp );
				}
				else
				{
					fw_download_verify_rsp.result = 1;
					//API_Led_NormalOn(LED_POWER);		//czn_20160708
					api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_RSP, (char*)&fw_download_verify_rsp );
				}
			}
			break;
			//czn_20160827_s
		case CMD_DEVICE_TYPE_UPDATE_START_REQ:	//czn_20160708
			bprintf("response CMD_DEVICE_TYPE_UPDATE_START_REQ\n");
			//czn_20160705_s
			//czn_20160708_s
			if(UpdateResourceTable() != 0)
			{
				//API_Led_NormalOn(LED_POWER);
				fw_download_update_rsp.result = -1;
				api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_UPDATE_START_RSP, (char*)&fw_download_update_rsp );
				break;
			}
			
			Updater_Run.server_ip_addr = pUdpType->target_ip;
			update_return = UpdateFwAndResource();
			
			//API_Led_NormalOn(LED_POWER);
			if( update_return == -1 )
			{	
				fw_download_update_rsp.result = -1;
				api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_UPDATE_START_RSP, (char*)&fw_download_update_rsp );
			}
			else if(update_return == 1)	//reset_disable
			{	
				fw_download_update_rsp.result = 0;
				api_fireware_upgrade_cmd_send(pUdpType->target_ip,CMD_DEVICE_TYPE_UPDATE_START_RSP, (char*)&fw_download_update_rsp );
			}
			break;
/*		
		case DEVREG_CMD_REGISTER_APPLY:
		case DEVREG_CMD_REGISTER_REPLY:
		case DEVREG_CMD_REGISTER_COMFIRM:
		case DEVREG_CMD_REGISTER_DISCOVERY:
		case DEVREG_CMD_REGISTER_NOTIFY:
		case DEVREG_CMD_REGISTER_BROADCAST:
		case DEVREG_CMD_REGISTER_UPDATE:
		case LINKSTATE_CMD_BROADCAST:	
			DeviceRegister_Cmd_Process(pUdpType->target_ip,pUdpType->cmd,pUdpType->pbuf);
			break;
*/
		//czn_20170712_s
		case CMD_MON_OPENCMR_REQ:
		case CMD_MON_CLOSECMR_REQ:
		case CMD_MON_TALK_REQ:	
		case CMD_MON_UNLOCK_REQ:
		case CMD_MON_LINK_REQ:
		case CMD_MON_QSW_RESID:
		case CMD_MON_OPENNEXTCMR_REQ:
			bprintf("rev ip %08x rstrscmd %04x\n",pUdpType->target_ip,pUdpType->cmd);
			ip_mon_command_process( pUdpType ); 
			break;
			
		case CMD_DOORBELL_REQ:		//czn_20170809
			//IX2_TEST Api_DoorBell_RemoteTouch();
			break;
		//czn_20190221_s
		case CMD_DXFILETRS_CLEINT_REQ:
		case CMD_DXFILETRS_SERVER_REQ:
		case CMD_DXFILETRS_VERIFY_REQ:
			DxFileTrs_Cmd_Process(pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);
			break;	
		//czn_20190221_e	

		//czn_20190412_s
		case CMD_AUTOTEST_START_REQ:
		case CMD_AUTOTEST_START_RSP:
		case CMD_AUTOTEST_CALL_REQ:
		case CMD_AUTOTEST_CALL_RSP:
		case CMD_AUTOTEST_LOG_REQ:
		case CMD_AUTOTEST_LOG_RSP:	
		case CMD_AUTOTEST_STATE_REQ:
		case CMD_AUTOTEST_STATE_RSP:		
			AutoTest_Cmd_Process(pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);
			break;
		//czn_20190412_e

		// lzh_20210918_s
		case CMD_IPERF_START_REQ:
			response_iperf_client_start((void*)pUdpType);
			break;
		// lzh_20210918_e
		case CMD_OLDMULTALK_REQ:
		case CMD_LISTENTALK_REQ:
			ListenTalkCmd_Process(pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);
			break;
		
	}
}
// lzh_20160503_e

//czn_20170518_s


void API_Survey_MsgOldCommand(uint16 source_address , uint16 target_address , uint8 cmd, uint8 data_length, uint8* data)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= MSG_ID_StackAI;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_survey;
	rec_msg_from_stack.head.msg_type		= 0;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_OLD_COM;		//?????
	rec_msg_from_stack.command_type		= cmd;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
}
void API_Survey_MsgNewCommand(uint16 source_address , uint16 target_address , uint8 cmd, uint8 data_length, uint8* data)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= MSG_ID_StackAI;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_survey;
	rec_msg_from_stack.head.msg_type		= 0;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_NEW_COM;		//?????
	rec_msg_from_stack.command_type		= cmd;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
}
#if 0
Global_Addr_Stru DtAddr_Trs2_IpGlobalAddr(unsigned short dtaddr)
{
	Global_Addr_Stru gaddr = {0};
	
	if(dtaddr >= 0x80 && dtaddr <= 0xff)
	{
		gaddr.gatewayid = 0;
		gaddr.ip = GetLocalIp();
		gaddr.rt = dtaddr&0x03;
		gaddr.code = (dtaddr&0x7c)>>2;
		if(gaddr.code == 0)
		{
			gaddr.code = 32;
		}
	}
	else if(dtaddr >= DS1_ADDRESS && dtaddr <= DS4_ADDRESS)
	{
		gaddr.gatewayid = 0;
		gaddr.ip = GetLocalIp();
		gaddr.rt = 10;
		gaddr.code = dtaddr-DS1_ADDRESS;
	}
	else if(dtaddr == 0x3c)
	{
		gaddr.gatewayid = 0;
		gaddr.ip = GetLocalIp();
		gaddr.rt = 16;
		gaddr.code = 0;
	}

	return gaddr;
}

unsigned short IpGlobalAddr_Trs2_DtAddr(Global_Addr_Stru gaddr)
{
	unsigned short dtaddr = 0;

	if(gaddr.rt <= 4)
	{
		dtaddr = ((gaddr.code) << 2)|0x80;
		dtaddr |= gaddr.rt;
	}
	else if(gaddr.rt == 10)
	{
		dtaddr = DS1_ADDRESS + gaddr.code;
	}
	else if(gaddr.rt == 16)
	{
		dtaddr = 0x3c;
	}

	return dtaddr;
}




#endif


void DtCommand_Analyzer(DTSTACK_MSG_TYPE *Message_command)
{
	
	Global_Addr_Stru addr;
	uint8 tranferset;
	uint8 intercomen;
	char buff[5];
	
	switch(Message_command->command_type)
	{
		case MR_LINKING_ST:
			#if 1
			if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
			{
				API_Event_IoServer_InnerRead_All(CallScene_SET, buff);
				tranferset = atoi(buff);
			}
			if(tranferset== 1 || tranferset == 2)
			//if( !GetDoNotDisturbState() ) 	//���������Ź���
			{
				break;
			}
			#endif
			{
		 		API_Stack_APT_Without_ACK(Message_command->address ,SUB_MODEL);
			}
			break;
		case MAIN_CALL_ONCE_L:
		case MAIN_CALL_GROUP:
		case MAIN_CALL:
			//if(CallServer_Run.state == CallServer_Wait)
			{
				//addr.gatewayid = 0;
				//addr.ip = GetLocalIp();
				//addr.rt = 10;
				//addr.code = Message_command->address - DS1_ADDRESS;
				API_CallServer_DXInvite(DxCallScene1_Master,Message_command->address);
			}
			break;
			
		case S_CANCEL:
			if(CallServer_Run.state != CallServer_Wait)	//will_change	
			{
				//addr.gatewayid = 0;
				//addr.ip = GetLocalIp();
				//addr.rt = 10;
				//addr.code = Message_command->address - DS1_ADDRESS;
				API_CallServer_RemoteBye(DxCallScene1_Master,NULL);
			}		
			break;
		#if 1	
		case INTERCOM_CALL:
			//if(CallServer_Run.state == CallServer_Wait)
			
			API_Event_IoServer_InnerRead_All(IntercomEnable, buff);
			if(atoi(buff) == 1 || Message_command->address == 0x3c)
			{
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				API_CallServer_DXInvite(DxCallScene2_Master,Message_command->address);
			}
			break;
				
		case INTERCOM_CLOSE:
			if(CallServer_Run.state != CallServer_Wait)	//will_change	
			{
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				API_CallServer_RemoteBye(CallServer_Run.call_type,NULL);
			}			
			break;
			
		case INTERCOM_TALK:
			if( (CallServer_Run.state == CallServer_Ring) &&  ((CallServer_Run.call_type == DxCallScene3)||(CallServer_Run.call_type == DxCallScene4_Master)) )
			{
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				API_CallServer_RemoteAck(CallServer_Run.call_type,NULL);
			}
			break;
		
		#endif	
		case MR_CALL_ST_ON:		//���к��зֻ�
			
		case ST_MON_ON:			//�ֻ�����
				
		case DEVICE_CALL_GL_ON:
		case GL_CALL_DEVICE_ON:
		case ST_CALL_ST_ON:		//����ͨ��
		case INTERCOM_ON:		//����ͨ��
			//czn_20170328	if(IsCoverThisSr(Message_command) == 0)
			{
				//API_SR_SetState_Locate(API_Take_SrType_FromComd(Message_command->command_type), Message_command->address);		
			}
			API_SR_SetState_Locate(API_Take_SrType_FromComd(Message_command->command_type), Message_command->address);
			break;

		case MR_CALL_ST_OFF:		//���к��зֻ�
		case ST_MON_OFF:			//�ֻ�����
		case STATE_RESET:			//ǿ���ͷ�
		case ST_CALL_ST_OFF:		//����ͨ��
		case INTERCOM_OFF:			//����ͨ��
		case DEVICE_CALL_GL_OFF:	//read_change
		case GL_CALL_DEVICE_OFF: 	//read_change
		
			API_SR_ClearState_DeLocate();
			
			if(CallServer_Run.state != CallServer_Wait)
			{
				API_CallServer_DtSr_Disconnect();		//czn_20171030
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				//API_CallServer_RemoteBye(CallServer_Run.call_type,&addr);
			}
			#if 1
			else if(Mon_Run.state != MON_STATE_PHONE_NEXT && Mon_Run.state != MON_STATE_OFF)//czn_20161226
			{
				API_Event_DXMonitor_Cancel();
			}
			else if(Mon_Run.state != MON_STATE_PHONE_NEXT) 
				linphone_becall_cancel();
			#endif
			/*if (CallServer_Run.call_mode == IPGVTKCALL_CALLINGPLACE_WITHBJ)
			{
				API_Phone_VTKBjSrClear();
			}
			if(Business_Run.calling_place == IPGVTKCALL_CALLINGPLACE_WITHZJ)
			{
				API_Phone_VTKZjSrClear();
			}
		
			*/
			break;	
		case RECEIPT:	//˵��: IPC����RECEIPT�ظ�,�ʲ���ʹ�ô�ָ���ж���������(ϵͳ��/������)
			if( (CallServer_Run.state == CallServer_Invite) && ((CallServer_Run.call_type == DxCallScene3)||(CallServer_Run.call_type == DxCallScene4_Master))  )
			{
				API_CallServer_InviteOk(CallServer_Run.call_type);
			}
			#if 0		//czn_20170717
			else if( Mon_Run.state == MON_LINKING )
			{
				OS_SignalEvent(MON_LINK_OK, &task_localmon);
			}	
			#endif
			break;
		case RECEIPT_SINGLE:	
			#if 0
			 if( Mon_Run.state == MON_LINKING )
			{
				OS_SignalEvent(MON_LINK_OK, &task_localmon);
			}
			 #endif
			break;

		case MAIN_CANCEL_GROUP:	
		case ST_TALK:
		case INNER_TALK:
			#if 1
			if(CallServer_Run.state == CallServer_Ring || CallServer_Run.state == CallServer_Invite)
			{
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				API_CallServer_RemoteAck(CallServer_Run.call_type,NULL);	
			}
			#endif
			break;

		case INNER_CALL:
			#if 1
			//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
			API_CallServer_DXInvite(DxCallScene6_Master,Message_command->address);
			#endif
			break;

		case LET_DIDONG_ONCE:	//czn20171030
			API_CallServer_Redail(DxCallScene1_Master);
			break;		
			
	}
}
void NewCommand_Process(DTSTACK_MSG_TYPE *Message_command)
{
	char re_buff[10];
	int video_id;
	int da = Message_command->target_address;
	int sa = Message_command->address;
	char *data = Message_command->data_buf;
	int len = Message_command->data_len;

	switch(Message_command->command_type)
	{
		case APCI_GROUP_VALUE_READ:
		case APCI_GROUP_VALUE_RESPONSE:
		case APCI_GROUP_VALUE_WRITE:
			if(Message_command->target_address == GA41_FISH_EYE_REPORT)
			{
				API_FishEye_On(Message_command->address);	//czn_20181117
		//API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_INFORM_FISH_EYE, data, 2);
			}
			
			break;
			
		default:
			break;
	}
}



//czn_20170518_e	
