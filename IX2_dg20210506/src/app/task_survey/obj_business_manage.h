#ifndef _obj_business_manage_h
#define _obj_business_manage_h

typedef enum
{
	//Business_State_Idle = 0,
	Business_State_MainCall,	
	Business_State_BeMainCall,
	Business_State_IntercomCall,
	Business_State_BeIntercomCall,
	Business_State_InnerCall,
	Business_State_BeInnerCall,
	Business_State_SipCallTest,
	Business_State_BeSipCall,
	Business_State_MonitorDs,
	Business_State_MonitorIpc,
	Business_State_BeMonitor,
	Business_State_SipWaitInput,
	Business_State_MonitorQuad,
};

typedef enum
{
	Business_Idle = 0,
	Business_InUse,
	Business_Hold,
	Business_NoDisturb,
};

int API_Business_Request(int business_type);
void API_Business_Close(int business_type);
void API_Business_Reset(void);

#endif
