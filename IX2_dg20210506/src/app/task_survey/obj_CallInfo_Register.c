#include "task_survey.h"
#include "obj_IP_Call_Link.h"
#include "obj_CallInfo_Register.h"

//#include "../task_BeCalled/task_BeCalled.h"

//#include "../task_Sundry/task_Ring/task_Ring.h"		//czn_20170113

CALL_INFO_STRU Call_Info;

void Set_Call_Info_Type(unsigned char newtype)
{
	Call_Info.call_info_type = newtype;
}

unsigned char Get_Call_Info_Type(void)
{
	return Call_Info.call_info_type;
}


void Save_Call_Info(Global_Addr_Stru *global_addr,NLIST_ENTRY_STRU *info_list)
{
	if(global_addr != NULL)
	{
		Call_Info.global_addr = *global_addr;
	}
	
	if(info_list != NULL)
	{
		Call_Info.info_list = *info_list;
	}
}
//return 1: can be monitor 0: can not be monitor
int Judge_Monitor_Ability(void)
{
	/*if(!Info_Type_IsNameList())
	{
		return 0;
	}

	if(Call_Info.info_list.value_type == 0)
	{
	
		//will change
		return 0;
	}
	else
	{
		
		return 0;
	}
*/
#if 0
	if(BeCalled_Run.source_addr.rt == 10 || BeCalled_Run.source_addr.rt == 12)
	{
			return 1;
	}
#endif	
	
	return 0;
}

int Get_Call_Info_DevType(void)
{
	//return Call_Info.info_list.list_type;
	return 0;
}

int Get_Call_Info_Gwid(void)
{
	if(!Info_Type_IsNameList())
	{
		return Call_Info.global_addr.gatewayid;		//czn_20170329
	}
      	if(Call_Info.info_list.value_type == 0)
      	{
		return Call_Info.info_list.value.ip_table_value.ipnode_id;
      	}
	else if(Call_Info.info_list.value_type == 1)	
	{
		return Call_Info.info_list.value.dev_table_value.ipnode_id;
	}
	return -1;
}
//czn_20160615
int Get_Call_Info_Devid(void)
{
	int logic_addr;
	#if 0
	if(!Info_Type_IsNameList())
	{
		
		return Call_Info.global_addr.code;
	}
	/*if(Call_Info.info_list.list_type == GS_List || Call_Info.info_list.list_type == CDS_List)
	{
		return -1;
	}*/
	if(Call_Info.info_list.value_type == 0)
      	{
		return 0;
      	}
	else if(Call_Info.info_list.value_type == 1)	
	{
		return Call_Info.info_list.value.dev_table_value.logic_addr;
	}
	#endif
	logic_addr = Call_Info.global_addr.rt*32 + Call_Info.global_addr.code;

	return logic_addr;
}

int Get_Call_Info_Ip(void)
{
	if(!Info_Type_IsNameList())
	{
		return Call_Info.global_addr.ip;
	}
	else
	{
		
		
		if(Call_Info.info_list.value_type == 0)
		{
			return Call_Info.info_list.value.ip_table_value.ip_addr;
		}
		else if(Call_Info.info_list.value_type == 1)	
		{
			/*ip_table_t ip_table;
			if(Search_IpTab_By_DevTab(&Call_Info.info_list.value.dev_table_value,&ip_table) == 0)
			{
				return ip_table.ip_addr;
			}*/
			return GatewayId_Trs2_IpAddr(Call_Info.info_list.value.dev_table_value.ipnode_id);
		}
		else
		{
			return -1;
		}
	}
}

int Get_Call_Info_Name(char *name,int len)
{
	char name_temp[41];

	if(!Info_Type_IsNameList())
	{
		return -1;
	}

	/*if(Call_Info.info_list.list_type == GS_List || Call_Info.info_list.list_type == CDS_List)
	{
		strncpy(name,Call_Info.info_list.list1.name,len);
		
	}
	else
	{
		strcpy(name_temp,Call_Info.info_list.list1.name);
		strcat(name_temp,"-");
		strcat(name_temp,Call_Info.info_list.list2.name);

		strncpy(name,name_temp,len);
	}
*/
	
	if(Call_Info.info_list.value_type == 0)
      	{
		strncpy(name,Call_Info.info_list.value.ip_table_value.name,len);
      	}
	else if(Call_Info.info_list.value_type == 1)	
	{
		
		
		if(Call_Info.info_list.value.dev_table_value.logic_addr>=320 && Call_Info.info_list.value.dev_table_value.logic_addr <= 323)
		{
			int vrestb_index;
			//vrestb_index = Get_MonRes_MapEntryTb_Index(Call_Info.info_list.value.dev_table_value.ipnode_id,Call_Info.info_list.value.dev_table_value.logic_addr - 320 + 1);
			if(vrestb_index == -1)
			{
				strncpy(name,Call_Info.info_list.value.dev_table_value.name,len);
			}
			else
			{
				//Get_MonRes_MapEntryTb_Record(vrestb_index,NULL,NULL,NULL,NULL,NULL,name,NULL);
			}
		}
		else
		{
			strncpy(name,Call_Info.info_list.value.dev_table_value.name,len);
		}
	}
	else
	{
		return -1;
	}
	return 0;
}

int Get_Call_Info_Input(char *input,int len)
{
	char name_temp[41];

	if(!Info_Type_IsNameList())
	{
		return -1;
	}

	
	if(Call_Info.info_list.value_type == 0)
      	{
		strncpy(input,Call_Info.info_list.value.ip_table_value.ip_input,len);
      	}
	else if(Call_Info.info_list.value_type == 1)	
	{
		strncpy(input,Call_Info.info_list.value.dev_table_value.ip_input,len);
	}
	else
	{
		return -1;
	}
	return 0;
}

//czn_20160611
void Register_BeCalled_Info(Global_Addr_Stru *source_addr)
{
	NLIST_ENTRY_STRU list;
//cao_20170509 	if(Namelist_Search_ByAddr(source_addr, &list) == -1)
	{
		Set_Call_Info_Type(Call_Info_GlobalAddr);
		Save_Call_Info(source_addr,NULL);
	}
//cao_20170509 		else
	{
		Set_Call_Info_Type(Call_Info_NameList);
		Save_Call_Info(source_addr,&list);
	}
	
}
//czn_20170113_s
#if 0
Ring_Scene_Type Get_Ring_Scene(void)
{
//cao_20170509	if(BeCalled_Run.source_addr.rt == 10 || BeCalled_Run.source_addr.rt == 12)
	{
		return RING_SCENE_0;
	}
	
//cao_20170509	if(BeCalled_Run.source_addr.rt == 16)
	{
		return RING_SCENE_1;
	}
	
	
	return RING_SCENE_2;
}
#endif
//czn_20170113_e


