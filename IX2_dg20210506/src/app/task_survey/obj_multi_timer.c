#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

//IX2_TEST #include "../task_VideoMenu/task_VideoMenu.h"
#include "task_Hal.h"

//IX2_TEST #include "../task_VideoMenu/obj_menu_data.h"

//-------------------------------------------------------------------------------------------
OS_TIMER t_power_down;
static int power_Down_Timer_Cnt;
static int messageDisplay_Timer_Cnt;

void Power_Down_Timer_Process(void)
{
	OS_RetriggerTimer( &t_power_down );

	if(power_Down_Timer_Cnt > 0)
	{
		power_Down_Timer_Cnt--;
		if(power_Down_Timer_Cnt == 0)
		{
			#if defined(PID_IX850)
			ReturnMainMenu();
			#else
			// lzh_20230710_s		// test cvbs display, NOT CLOSE SCREEN!
			#if !defined(CVBS_DIRECT_INTO_T527)
			if(messageDisplay_Timer_Cnt)
			{
				MessageClose();
			}
			CloseOneMenu();
			#endif
			// lzh_20230710_e		// test
			#endif
		}
	}
	#if defined(PID_IX850)
	#else
	if(messageDisplay_Timer_Cnt)
	{
		messageDisplay_Timer_Cnt--;
		if(messageDisplay_Timer_Cnt <= 0)
		{
			MessageClose();
		}
	}
	#endif
	#if defined(PID_IX850)
	#else
	OnlineManageRebootTimerProcess();
	OnlineFwUpgradeTimerProcess();
	#endif
}

void Power_Down_Timer_Stop(void)
{
	power_Down_Timer_Cnt = 0;
	//OS_StopTimer(&t_power_down);
}


int Power_Down_Timer_Init(void)
{
	OS_CreateTimer( &t_power_down, Power_Down_Timer_Process, MUL_TIMER_SECOND);	
	OS_RetriggerTimer( &t_power_down );	
}

int Power_Down_Timer_Set( int sec )
{
	power_Down_Timer_Cnt = sec;
}

int MessageDisplay_Timer_Set( int sec )
{
	messageDisplay_Timer_Cnt = sec;
}


//-------------------------------------------------------------------------------------------
OS_TIMER t_rtc;
extern void Rtc_Timer_Process(void);

int Rtc_Timer_Init(void)
{
	//IX2_TEST OS_CreateTimer( &t_rtc, Rtc_Timer_Process, 100 );	
}

/********************************************************************************************/
//initial all timer
/********************************************************************************************/
void init_all_timer(void)
{
	init_mul_timer();
	Power_Down_Timer_Init();
	Rtc_Timer_Init();
	TimingCheckInit();
}


