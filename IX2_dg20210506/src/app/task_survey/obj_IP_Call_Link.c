#include "task_survey.h"
 //#include "../task_CommandInterface/obj_CommandInterface.h"
#include "../obj_IxSys_CallBusiness/obj_VtkUnicastCommandInterface/obj_VtkUnicastCommandInterface.h"
#include "obj_IP_Call_Link.h"

_IP_Call_Link_Run_Stru IP_Call_Link_Run = {.Status = CLink_Idle};


//czn_020160422_s
void Set_IPCallLink_Status(unsigned char newstatus)
{
	IP_Call_Link_Run.Status = newstatus ;
}

unsigned char Get_IPCallLink_Status(void)
{
	return IP_Call_Link_Run.Status;
}

void Set_IPCallLink_Data(unsigned char data_type,Global_Addr_Stru *data)
{
	if(data_type == 0)
	{
		IP_Call_Link_Run.Caller_Data.Call_Target= *data;
	}
	else if(data_type == 1)
	{
		IP_Call_Link_Run.BeCalled_Data.Call_Source = *data;
	}
	if(data_type == 2)
	{
		IP_Call_Link_Run.Transferred_Data.Transferred_Device = *data;
	}
}
//czn_020160422_e

//czn_20160516



int Get_Call_Link_Respones(UDP_MSG_TYPE *pUdpType)
{
	char *rspbuf = (char*)malloc(sizeof(_IP_Call_Link_Run_Stru) );
	if(rspbuf == NULL)
	{
		return -1;
	}
	//rspbuf[0]  = VTK_CMD_LINK_REP_1081 >> 8;
	//rspbuf[1]  = VTK_CMD_LINK_REP_1081 & 0xff; 
	//rspbuf[2]  = pUdpType->pbuf[2];
	//rspbuf[3]  = pUdpType->pbuf[3];
	
	memcpy(rspbuf,&IP_Call_Link_Run,sizeof(_IP_Call_Link_Run_Stru));

	//int id = (rspbuf[2]<<8) | rspbuf[3];
	
	if(api_udp_c5_ipc_send_rsp(pUdpType->target_ip,VTK_CMD_LINK_REP_1081,pUdpType->id,rspbuf,sizeof(_IP_Call_Link_Run_Stru)) == -1)
	{
		free(rspbuf);
		return -1;
	}

	free(rspbuf);
	return 0;
}


/*
int Get_Call_Link_Respones(UDP_MSG_TYPE *pUdpType)		//czn_20170518
{
	//char *rspbuf = (char*)malloc(sizeof(_IP_Call_Link_Run_Stru) );
	extern Global_Addr_Stru Register_DxIm_List[];
	extern int Register_DxIm_Num ;
	#include "../obj_DxSys_CallBusiness/task_CallServer/task_CallServer.h"
	
	int i;
	_IP_Call_Link_Run_Stru call_link_temp;
	
	
	memcpy(&call_link_temp,&IP_Call_Link_Run,sizeof(_IP_Call_Link_Run_Stru));

	if(CallServer_Run.state == CallServer_Ring && (CallServer_Run.call_type == DxCallScene1_Master ||CallServer_Run.call_type == DxCallScene2_Master))
	{
		for(i = 0;i < Register_DxIm_Num;i ++)
		{
			if(GatewayId_Trs2_IpAddr(Register_DxIm_List[i].gatewayid) == pUdpType->target_ip);
			{
				Register_DxIm_List[i].ip = pUdpType->target_ip;
				call_link_temp.Caller_Data.Call_Target = Register_DxIm_List[i];
				break;
			}
		}
	}

	
	
	if(api_udp_c5_ipc_send_rsp(pUdpType->target_ip,0x1081,pUdpType->id,(char*)&call_link_temp,sizeof(_IP_Call_Link_Run_Stru)) == -1)
	{
		return -1;
	}

	return 0;
}

*/


