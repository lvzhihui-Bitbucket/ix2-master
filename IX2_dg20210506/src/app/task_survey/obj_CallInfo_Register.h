#ifndef obj_CallInfo_Register_h
#define obj_CallInfo_Register_h

#include "obj_IPDeviceTable.h"

typedef struct
{
	unsigned char			value_type;
	union
	{
		ip_table_t		ip_table_value;
		dev_table_t		dev_table_value;
	}value;
}NLIST_ENTRY_STRU;

typedef enum
{
	Call_Info_GlobalAddr 		= 0,
	Call_Info_NameList,
	Max_Call_Info,
}CALL_INFO_ENUM;

typedef struct
{
	unsigned char call_info_type;
	Global_Addr_Stru	  global_addr;
	NLIST_ENTRY_STRU info_list;
}CALL_INFO_STRU;


void Set_Call_Info_Type(unsigned char newtype);
unsigned char Get_Call_Info_Type(void);
	#define Info_Type_IsNameList()			(Get_Call_Info_Type() == Call_Info_NameList)
void Save_Call_Info(Global_Addr_Stru *global_addr,NLIST_ENTRY_STRU *info_list);
int Judge_Monitor_Ability(void);
int Get_Call_Info_DevType(void);
int Get_Call_Info_Gwid(void);
int Get_Call_Info_Devid(void);
int Get_Call_Info_Ip(void);
int Get_Call_Info_Name(char *name,int len);
void Register_BeCalled_Info(Global_Addr_Stru *source_addr);
//Ring_Scene_Type Get_Ring_Scene(void);		//czn_20170113

#endif

