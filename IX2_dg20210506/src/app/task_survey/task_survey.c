

#include "task_survey.h"

#include "obj_multi_timer.h"
#include "sys_msg_process.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "task_debug_sbu.h"
#include "obj_Get_Ph_MFG_SN.h"
#include "obj_SYS_VER_INFO.h"		//czn_20170411
#include "obj_sip_account_default.h"
#include "task_Hal.h"
#include "define_file.h"

//czn_20160418_s
unsigned char Call_Survey_State = CALL_SURVEY_IDLE;
//czn_20160418_e

Loop_vdp_common_buffer	vdp_controller_mesg_queue;
Loop_vdp_common_buffer	vdp_controller_sync_queue;
vdp_task_t				task_control = {0};	//czn_20170807

void survey_mesg_data_process(char* msg_data, int len);
void* survey_mesg_task( void* arg );

void vtk_TaskInit_survey( unsigned char priority )
{
	init_vdp_common_queue(&vdp_controller_mesg_queue, 1000, (msg_process)survey_mesg_data_process, &task_control);
	init_vdp_common_queue(&vdp_controller_sync_queue, 100, NULL, 								  &task_control);
	init_vdp_common_task(&task_control, MSG_ID_survey, survey_mesg_task, &vdp_controller_mesg_queue, &vdp_controller_sync_queue);
	//pthread_join(task_control.task_pid, NULL );
	printf("======vtk_TaskInit_survey===========\n");	
}

void vtk_TaskExit_survey(void)
{
	exit_vdp_common_queue(&vdp_controller_mesg_queue);
	exit_vdp_common_queue(&vdp_controller_sync_queue);
	exit_vdp_common_task(&task_control);
}

















void* survey_mesg_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	//DevReg_Info dev_info;
	
	#if 1
	GetPhMFGSnInit();	
	
	FileTrsServer_Init();

	IsHaveUpdateNewRs();	//czn_20170216
	vtk_ObjectInit_SR();	//czn_20170525
	
	LoadRingPlayList(RING_DEFAULT_CFG_FILE);
	TableSurverInit();
	api_create_default_sip_info();
	SipConfig_Init();		//czn_20170725
	//IX2_TEST LoadAudioVolume();
	LoadIpCacheTable();
	LoadCallCtrlTable();
	LoadImNameListTable();
	ImCookieTableCreate();
	LoadMsListTable();
	LoadDeviceRegisterTable();
	LoadR8001Table();
	//IX2_TEST InitMovementTest();
	init_device_addr_prefix_string();
	#if defined(PID_IX850)
	#else
	ICP_MonitorInit();
	#endif

	DxFileTrs_Init();			//czn_20190221

	AutoTest_Init();		//czn_20190412
	
	ImNamelistTempTableCreate();
	MonRes_Temp_Table_Create();
	//MonCookieTableCreate();
	GetMonFavFromFile();
	#endif
	NDM_Init();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void survey_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pVdpUdp = (VDP_MSG_HEAD*)msg_data;

	// 判断是否为系统服务
	if( pVdpUdp->msg_target_id == MSG_ID_survey )
	{
		survey_sys_message_processing(msg_data,len);
	}
	else
	{
		vdp_task_t* pVdpTask = GetTaskAccordingMsgID(pVdpUdp->msg_target_id);
		if( pVdpTask != NULL )
		{
			push_vdp_common_queue(pVdpTask->p_msg_buf,msg_data,len);
		}
	}
}

// 功能: 等待业务同步应答
// 参数: pBusiness - 同步等待时的挂起的队列，business_type - 等待的业务类型， data - 得到的数据，plen - 数据有效长度
// 返回: 0 - 返回超时，1 -  返回正常
int WaitForBusinessACK( p_Loop_vdp_common_buffer pBusiness, unsigned char business_type,  char* data, int* plen, int timeout )
{
	VDP_MSG_HEAD* pbusiness_buf;
	p_vdp_common_buffer pdb = 0;
	int size;
	vdp_task_t*	powner;	
	
	int ret = 0;
	
	size = pop_vdp_common_queue( pBusiness,&pdb,timeout);
	if( size > 0  )
	{		
		pbusiness_buf = (VDP_MSG_HEAD*)pdb;

		powner = (vdp_task_t*)pBusiness->powner;
			
		if( pbusiness_buf->msg_type  == (business_type|COMMON_RESPONSE_BIT) )
		{				
			// 得到数据
			*plen = size;
			memcpy( data, pdb, size );
			//dprintf("task %d wait ack ok!\n",powner->task_id);
			ret = 1;
		}

		purge_vdp_common_queue( pBusiness );
	}
	return ret;
}

vdp_task_t* GetTaskAccordingMsgID(unsigned char msg_id)
{
	vdp_task_t* ptaskobj = NULL;
	
	switch( msg_id )
	{
		case MSG_ID_DEBUG_SBU:
			ptaskobj = &task_debug_sbu;
			break;
		case MSG_ID_Menu:
			ptaskobj = &task_VideoMenu;				
			break;
		case MSG_ID_SUNDRY:
			ptaskobj = &task_sundry;				
			break;
		case MSG_ID_IOServer:
			ptaskobj = &task_io_server;				
			break;
		case MSG_ID_survey:
			ptaskobj = &task_control; 			
			break;
		#if 0
		case MSG_ID_RSUPDATER:
			ptaskobj = &task_rsupdate;	
			break;
		#endif
		case MSG_ID_DHCP:
			ptaskobj = &task_DHCP;				
			break;
		case MSG_ID_WiFi:
			ptaskobj = &task_WiFi;				
			break;
		case MSG_ID_CallServer:
			ptaskobj = &task_callserver;				
			break;
		case MSG_ID_DtBeCalled:
			ptaskobj = &task_dtbecalled;				
			break;	
		case MSG_ID_DtCalller:
			ptaskobj = &task_dtcaller;				
			break;	
		case MSG_ID_IpBeCalled:
			ptaskobj = &task_ipbecalled;				
			break;	
		case MSG_ID_IpCalller:
			ptaskobj = &task_ipcaller;				
			break;
		case MSG_ID_LocalMonitor:
			ptaskobj = &task_dxmon;	
			break;
			//czn_20170518_e
		case MSG_ID_Autotest:		//czn_20190412
			ptaskobj = &task_autotest;				
			break;		

		case MSG_ID_ListUpdate:
			ptaskobj = &task_listUpdate;				
			break;
			
		case MSG_ID_IX_PROXY:
			ptaskobj = &task_ix_proxy;				
			break;

		case MSG_ID_IX_PROXY_CLIENT:
			ptaskobj = &task_ix_proxy_client;				
			break;
		case MSG_ID_VtkMediaTrans:
			ptaskobj = &task_VtkMediaTrans;
			break;

		case MSG_ID_IPC_BUSINESS:
			ptaskobj = &task_ipc_business;
			break;
		case MSG_ID_DS_MONITOR:
			ptaskobj = &task_ds_monitor;
			break;
		case MSG_ID_StackAI:
			ptaskobj = &task_StackAI;				
			break;	
		#if defined(PID_IX47)||defined(PID_IX482)
		case MSG_ID_AlarmingSurvey	:
			ptaskobj = &task_AlarmingSurvey;
			break;
		case MSG_ID_CallTransferSurvey	:
			ptaskobj = &task_CallTransferSurvey;
			break;	
		#endif
	}
	
	return ptaskobj;
}

const vdp_task_t* 	task_tab[] =
{
	&task_control,
	&task_debug_sbu,
	&task_VideoMenu,
	&task_sundry,
	&task_io_server,
	//&task_go_server,
	//&task_rsupdate,
	&task_StackAI,
	&task_DHCP,
//	&task_CacheTable,
	&task_ix_proxy,
	&task_ix_proxy_client,
	&task_callserver,		//czn_20170518
	&task_dtbecalled,
	&task_dtcaller,
	&task_ipbecalled,
	&task_ipcaller,
	&task_autotest,			//czn_20190412
	&task_listUpdate,
	&task_WiFi,
	&task_VtkMediaTrans,
	&task_ipc_business,
	&task_ds_monitor,
	&task_dxmon,
	#if defined(PID_IX47)||defined(PID_IX482)
	&task_AlarmingSurvey,
	&task_CallTransferSurvey,
	#endif
	
	NULL,
};

int GetMsgIDAccordingPid( pthread_t pid )
{
	int i;
	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )		//czn_20161008	
			return 0;
		if( task_tab[i]->task_pid == pid )
		{
			//dprintf("get pid=%lu msg id=%d\n",task_tab[i]->task_pid,task_tab[i]->task_id);
			return task_tab[i]->task_id;
		}
	}
	return 0;
}

const char* OS_GetTaskName(void)
{
	int i;
	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )
		{
			//log_e("Task table has no pid:%d", pthread_self());
			return NULL;
		}

		if( task_tab[i]->task_pid == pthread_self())
		{
			return task_tab[i]->task_name;
		}
	}
}

vdp_task_t* OS_GetTaskID(void)
{
	int i;
	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )
			return NULL;
		if( task_tab[i]->task_pid == pthread_self())
		{
			return task_tab[i];
		}
	}
}

OS_TASK_EVENT OS_WaitSingleEventTimed(OS_TASK_EVENT event, int time)
{
	p_vdp_common_buffer pdb = 0;
	OS_TASK_EVENT ret = 0x00;
	int size;
	
	size = pop_vdp_common_queue(OS_GetTaskID()->p_syc_buf,&pdb, time);
	if( size > 0)
	{		
		if(*pdb & event)
		{
			ret =  event;
		}
		purge_vdp_common_queue( OS_GetTaskID()->p_syc_buf);
	}
	
	return ret;
}

// 0 --OK/ 1 --ERR
int OS_SignalEvent(OS_TASK_EVENT event, vdp_task_t* taskTcb)
{
	return push_vdp_common_queue(taskTcb->p_syc_buf, (char*)&event, 1);
}

/****************************************************************************************************************************
 * @fn:		API_add_message_to_suvey_queue
 *
 * @brief:	加入消息到分发服务队列
 *
 * @param:  pdata 			- 数据指针
 * @param:  len 			- 数据长度
 *
 * @return: 0/ok, 1/full
****************************************************************************************************************************/
int	API_add_message_to_suvey_queue( char* pdata, unsigned int len )	//czn_20170807
{
	if(task_control.task_run_flag == 0)
	{
		return -1;
	}
	return push_vdp_common_queue(task_control.p_msg_buf, pdata, len);
}
//czn_20161220
int API_one_message_to_suvey_queue( int msg, int sub_msg )
{
	VDP_MSG_HEAD	head;
	head.msg_source_id	= MSG_ID_survey;
	head.msg_target_id	= MSG_ID_survey;
	head.msg_type		= msg;
	head.msg_sub_type	= sub_msg;	
	return push_vdp_common_queue(task_control.p_msg_buf, &head, sizeof(VDP_MSG_HEAD) );
}

int API_Survey_ThreadHeartbeatReq(void)		//czn_20190910
{
	return API_one_message_to_suvey_queue(SURVEY_MSG_ThreadHeartbeatReq,0);
}



