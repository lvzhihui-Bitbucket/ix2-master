/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <errno.h>

#include "vdp_uart.h"

#include "obj_SYS_VER_INFO.h"
#include "obj_Get_Ph_MFG_SN.h"
#include "obj_TableProcess.h"
#include "vdp_IoServer_State.h"
#include "obj_IoInterface.h"

#define SYS_VER_INFO_PATH		"/mnt/nand1-2/SysVerInfo.vdr"

SYS_VER_INFO_T sysVerInfo;

extern const char STR_FOR_CODE_VERSION[];
extern int STM8_CODE_VERSION;

int GetNetMode(void)
{
	char ixNet[21] = {0};
	API_Para_Read_String(IX_NT, ixNet);
	if(!strcmp(ixNet, NET_WLAN0))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

char* getDateFromMacro( const char* timeIn, char *timeOut)
{
    char s_month[5];
    int month, day, year;
    static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

    sscanf(timeIn, "%s %d %d", s_month, &day, &year);

    month = (strstr(month_names, s_month)-month_names)/3;

	snprintf(timeOut, 7, "%2.2d%2.2d%2.2d", year-2000, month+1, day);
	
    return timeOut;
}

char* getTimeFromMacro( const char* timeIn, char *timeOut)
{
	uint8 i;
	
	int hour, min, second;
	
	sscanf(timeIn, "%d:%d:%d", &hour, &min, &second);
	
	//snprintf(timeOut, 7, "%2.2d%2.2d%2.2d", hour, min, second);
	snprintf(timeOut, 7, "%2.2d%2.2d", hour, min);
	
    return timeOut;
}

uint8 updateIoDataFile( const char *filePath, uint16 property_id, const char *value)
{
	FILE* file = NULL;
	FILE* file_tem = NULL;
	char* ptr  = NULL;
	char line[100] = {0};
	char temp[20] = {0};
	int len = 100;
	uint8 updateResult = 0;

	if( value == NULL || value[0] == 0 || filePath == NULL)
		return updateResult;

	snprintf( line, len, "%s.temp", filePath);

	// read-only file   	// write file
	if( ((file=fopen(filePath,"r"))==NULL)  || ((file_tem=fopen(line,"w+"))==NULL) )
	{
		eprintf( "updateIoDataFile open error:%s\n",strerror(errno) );
		if( file != NULL)
			fclose( file );

		if( file_tem != NULL )
			fclose( file_tem );

		file 	 = NULL; 
		file_tem = NULL; 
		return updateResult;
	}

	snprintf( temp, 20, "value=,0x0080%04x,", 0x8001+property_id);
	
	//read line
	while( fgets(line,len,file) != NULL )
	{	
		if( strstr(line,temp) != NULL )
		{
			char oldValue[40];
			char *pchar;
			strcpy(oldValue, line+strlen(temp));

			if((pchar = strchr(oldValue,'\r')) != NULL)
			{
				*pchar = 0;
			}
			
			if(strcmp(value, oldValue))
			{
				snprintf( line, len, "%s%s\r\n", temp, value);
				updateResult = 1;
			}
		}
		
		// copy file
		fprintf( file_tem, "%s", line );
	}
	
	fclose( file );		
	file = NULL;
	fclose( file_tem );
	file_tem = NULL;
	
	//snprintf( line, 100,  "cp %s %s.bak", filePath, filePath);
	//system(line);
		
	snprintf( line, 100,  "mv %s.temp %s", filePath, filePath);
	system(line);
	
	sync();

	return updateResult;

}

void UpdateSwVer(void)
{
	char temp1[10];
	char temp2[10];
	
	snprintf(sysVerInfo.swVer, 60, "%s.%s%s-%02d", STR_FOR_CODE_VERSION, getDateFromMacro(__DATE__, temp1), getTimeFromMacro(__TIME__, temp2), STM8_CODE_VERSION);
}

pthread_mutex_t 	receiveStm8lSwVerLock = PTHREAD_MUTEX_INITIALIZER;
int GetStm8lSwVerFlag;

int GetStm8lSwVerFromStm8l(void)
{
	int i;
	int ret;
	
	GetStm8lSwVerFlag = -1;
	api_uart_send_pack(UART_TYPE_STM8_VERSION_GET, NULL, 0);
	
	for(i = 10; i > 0; i--)
	{
		usleep(100*1000);
		pthread_mutex_lock(&receiveStm8lSwVerLock);
		ret = GetStm8lSwVerFlag;
		pthread_mutex_unlock(&receiveStm8lSwVerLock);
		
		if(ret >= 0)
		{
			break;
		}
	}
	
	printf("GetStm8lSwVerFlag = %d, i = %d\n", GetStm8lSwVerFlag, i);
	return GetStm8lSwVerFlag;	
}

void ReceiveStm8lSwVerFromStm8l(unsigned char* buff)
{
	pthread_mutex_lock(&receiveStm8lSwVerLock);
	STM8_CODE_VERSION = buff[1]; 
	GetStm8lSwVerFlag = 0; 
	pthread_mutex_unlock(&receiveStm8lSwVerLock);
	UpdateSwVer();
	printf("STM8_CODE_VERSION = %d, GetStm8lSwVerFlag = %d\n", STM8_CODE_VERSION, GetStm8lSwVerFlag);
}


void SysVerInfoInit(uint8 updateIoEnable)
{
	char temp1[10];
	char temp2[10];
	int ip;
	extern char* STR_FOR_DEVICE_TYPE_EXT[];

	if(updateIoEnable)
	{
		pthread_mutex_init( &sysVerInfo.lock, 0);
	}
	
	pthread_mutex_lock(&sysVerInfo.lock);

	GetStm8lSwVerFromStm8l();
	SetTimeUpdateCnt(3);

	sysVerInfo.myDeviceType = TYPE_IM;
	
	//API_Event_IoServer_InnerRead_All(ParaDeviceType, sysVerInfo.type);
	strcpy(sysVerInfo.type, STR_FOR_DEVICE_TYPE_EXT[0]);
	API_Event_IoServer_InnerRead_All(DeviceModel, sysVerInfo.deviceModel);

	snprintf(sysVerInfo.swVer, 60, "%s.%s%s-%02d", STR_FOR_CODE_VERSION, getDateFromMacro(__DATE__, temp1), getTimeFromMacro(__TIME__, temp2), STM8_CODE_VERSION);
	
	sysVerInfo.id = API_GetLocalIpNodeId();
	API_GetLocalSn(temp1);
	snprintf(sysVerInfo.sn, 13, "%02x%02x%02x%02x%02x%02x", temp1[0], temp1[1], temp1[2], temp1[3], temp1[4], temp1[5]);
	sysVerInfo.net_mode=api_nm_if_get_net_mode();
	sysVerInfo.lan_link=get_lan_link();
	sysVerInfo.lan_act_type=GetIpActionType();
	my_inet_ntoa(GetLocalIpByDevice(NET_ETH0), sysVerInfo.ip);

	GetLocalMacByDevice(NET_ETH0, temp1);
	snprintf(sysVerInfo.mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp1[0], temp1[1], temp1[2], temp1[3], temp1[4], temp1[5]);

	my_inet_ntoa(GetLocalGatewayByDevice(NET_ETH0), sysVerInfo.gw);
	
	my_inet_ntoa(GetLocalMaskAddrByDevice(NET_ETH0), sysVerInfo.mask);

	sysVerInfo.wlan_en=get_wifi_switch();
	sysVerInfo.wlan_state=get_wifi_connect_state();
	sysVerInfo.wifi_signal_level=get_wifi_signal_level();
	strcpy(sysVerInfo.wlan_cur_ssid,get_wifi_cur_ssid());
	my_inet_ntoa(GetLocalIpByDevice(NET_WLAN0), sysVerInfo.wlan_ip);

	GetLocalMacByDevice(NET_WLAN0, temp1);
	snprintf(sysVerInfo.wlan_mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp1[0], temp1[1], temp1[2], temp1[3], temp1[4], temp1[5]);

	my_inet_ntoa(GetLocalGatewayByDevice(NET_WLAN0), sysVerInfo.wlan_gw);
	
	my_inet_ntoa(GetLocalMaskAddrByDevice(NET_WLAN0), sysVerInfo.wlan_mask);
	
	pthread_mutex_unlock(&sysVerInfo.lock);

	if(updateIoEnable)
	{

		int ver1, ver2, ver3;
		int swRevision = 0;
		
		sscanf(sysVerInfo.swVer, "V%d.%d.%d", &ver1, &ver2, &ver3);
		
		/*
		API_Event_IoServer_InnerRead_All(SWRevision, temp1);
		swRevision = atoi(temp1);
		if(swRevision != ((ver1<<11)+(ver2<<6)+ver3))
		{
			swRevision = (ver1<<11)+(ver2<<6)+ver3;
			sprintf(temp1, "%d", swRevision);
			API_Event_IoServer_InnerWrite_All(SWRevision, temp1);
		}
		*/
		
		//strcpy(sysVerInfo.bd_rm_ms, "0099990100");
		//strcpy(sysVerInfo.bd_rm_ms, "0099002001");
		API_Event_IoServer_InnerRead_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
		memcpy(sysVerInfo.bd, sysVerInfo.bd_rm_ms, 4);
		memcpy(sysVerInfo.rm, sysVerInfo.bd_rm_ms+4, 4);
		memcpy(sysVerInfo.ms, sysVerInfo.bd_rm_ms+8, 2);
		sysVerInfo.bd[4] = 0;
		sysVerInfo.rm[4] = 0;
		sysVerInfo.ms[2] = 0;

		API_Event_IoServer_InnerRead_All(MY_NAME, (uint8*)sysVerInfo.myName);
		API_Event_IoServer_InnerRead_All(SYSTEM_TYPE, temp1);
		sysVerInfo.systemType = atoi(temp1);
		API_Event_IoServer_InnerRead_All(MY_GLOBAL_NUM, (uint8*)sysVerInfo.GLOBAL_NUM);
		API_Event_IoServer_InnerRead_All(MY_LOCAL_NUM, (uint8*)sysVerInfo.LOCAL_NUM);
	}
	
	SetMyProxyData(NULL, NULL, NULL, NULL);
	printfSysVerInfo();
}

SYS_VER_INFO_T GetSysVerInfo(void)
{
	return sysVerInfo;
}

void SetSysVerInfo(SYS_VER_INFO_T data)
{
	char temp[6];
	uint8 i, result = 0;
	char tempHex[5] = "0x00";
	unsigned tempValue;
	uint8 updateFlag = 0;

	pthread_mutex_lock(&sysVerInfo.lock);
	
	/*
	if(sysVerInfo.id != data.id)
	{
		sysVerInfo.id = data.id;
		API_SetLocalIpNodeId(data.id);
		sysVerInfo.id = API_GetLocalIpNodeId();
		updateFlag = 1;
	}
	
	if(memcmp(sysVerInfo.sn, data.sn, 12))
	{
		memcpy(sysVerInfo.sn, data.sn, 12);
		for(i = 0; i < 6; i++)
		{
			memcpy(tempHex+2, sysVerInfo.sn+i*2, 2);
			sscanf(tempHex, "%x", &tempValue);
			temp[i] = tempValue&0xFF;
		}
		API_SetLocalSn(temp);

		
		API_GetLocalSn(temp);
		snprintf(sysVerInfo.sn, 13, "%02x%02x%02x%02x%02x%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
		
		updateFlag = 1;
	}
	*/

	if(memcmp(sysVerInfo.ip, data.ip, 15))
	{
		memcpy(sysVerInfo.ip, data.ip, 15);
		SetNetWork(NULL, data.ip, NULL, NULL);
		result = 1;
		updateFlag = 1;
	}
	
	if(memcmp(sysVerInfo.mac, data.mac, 17))
	{
		memcpy(sysVerInfo.mac, data.mac, 17);
		SetNetWork(data.mac, NULL, NULL, NULL);
		result = 1;
		updateFlag = 1;
	}
	
	if(memcmp(sysVerInfo.gw, data.gw, 15))
	{
		memcpy(sysVerInfo.gw, data.gw, 15);
		SetNetWork(NULL, NULL, NULL, data.gw);
		result = 1;
		updateFlag = 1;
	}
	
	if(memcmp(sysVerInfo.mask, data.mask, 15))
	{
		memcpy(sysVerInfo.mask, data.mask, 15);
		SetNetWork(NULL, NULL, data.mask, NULL);
		result = 1;
		updateFlag = 1;
	}
	
	if(result)
	{
		int ip;
		int gateway;
		
		ResetNetWork();

		
		my_inet_ntoa(GetLocalIpByDevice(NET_ETH0), sysVerInfo.ip);
		
		GetLocalMacByDevice(NET_ETH0, temp);
		snprintf(sysVerInfo.mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);

		gateway = GetLocalGatewayByDevice(NET_ETH0);
		if(gateway != -1)
		{
			my_inet_ntoa(gateway, sysVerInfo.gw);
		}
		
		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_ETH0), sysVerInfo.mask);

		
		
		my_inet_ntoa(GetLocalIpByDevice(NET_WLAN0), sysVerInfo.wlan_ip);
		
		GetLocalMacByDevice(NET_WLAN0, temp);
		snprintf(sysVerInfo.wlan_mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
		
		gateway = GetLocalGatewayByDevice(NET_WLAN0);
		if(gateway != -1)
		{
			my_inet_ntoa(gateway, sysVerInfo.wlan_gw);
		}
		
		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_WLAN0), sysVerInfo.wlan_mask);
		
	}
	SetMyProxyData(NULL, NULL, NULL, NULL);

	pthread_mutex_unlock(&sysVerInfo.lock);
	
	if(updateFlag)
	{
		//OutputSysVerInfo();
	}

}

char* GetNetDeviceNameByTargetIp(int targetIp)
{  
	if((targetIp & inet_addr(sysVerInfo.mask)) == (inet_addr(sysVerInfo.ip) & inet_addr(sysVerInfo.mask)))
	{
		//printf("%s:%s\n",__func__,"eth");
		return  NET_ETH0;
	}
	if(GetSysVerInfo_WlanEn()==1&&GetSysVerInfo_WlanConnect()==2&&(targetIp & inet_addr(sysVerInfo.wlan_mask)) == (inet_addr(sysVerInfo.wlan_ip) & inet_addr(sysVerInfo.wlan_mask)))
	{
		//printf("%s:%s\n",__func__,"wlan");
		return  NET_WLAN0;
	}
	//printf("%s:%d:%d\n",__func__,GetSysVerInfo_WlanConnect(),GetSysVerInfo_WlanEn());
	//else 
	
	return NET_ETH0; 
}  

int IfOnLan(int targetIp)
{  
	if((targetIp & inet_addr(sysVerInfo.wlan_mask)) == (inet_addr(sysVerInfo.wlan_ip) & inet_addr(sysVerInfo.wlan_mask)))
	{
		return	2;
	}
	else if((targetIp & inet_addr(sysVerInfo.mask)) == (inet_addr(sysVerInfo.ip) & inet_addr(sysVerInfo.mask)))
	{
		return	1;
	}
	
	return 0; 
}  

void OutputSysVerInfo(void)
{
	FILE* file = NULL;

	file = fopen(SYS_VER_INFO_PATH, "w+");
	if( file == NULL )
	{
		printf( "open %s error.\n",SYS_VER_INFO_PATH );
		return;
	}
	
	fprintf(file, "[Device Type]\r\n");
	fprintf(file, "type = %s\r\n\r\n", sysVerInfo.type);

	fprintf(file, "[Software Version]\r\n");
	fprintf(file, "version = %s\r\n\r\n", sysVerInfo.swVer);

	fprintf(file, "[Serial Number]\r\n");
	fprintf(file, "sn = %s\r\n\r\n", sysVerInfo.sn);
	
	fprintf(file, "[Node Id]\r\n");
	fprintf(file, "id = %d\r\n\r\n", sysVerInfo.id);
	
	fprintf(file, "[Network Parameters]\r\n");
	fprintf(file, "ip      = %s\r\n", sysVerInfo.ip);
	fprintf(file, "mac     = %s\r\n", sysVerInfo.mac);
	fprintf(file, "mask    = %s\r\n", sysVerInfo.mask);
	fprintf(file, "gateway = %s\r\n\r\n", sysVerInfo.gw);

	
	fclose(file);

	sync();
}

void printfSysVerInfo(void)
{
	
	printf("[Device Type]\n");
	printf("type = %s\n\n", sysVerInfo.type);

	printf("[Software Version]\n");
	printf("version = %s\n\n", sysVerInfo.swVer);

	printf("[Serial Number]\n");
	printf("sn = %s\n\n", sysVerInfo.sn);
	
	printf("[Node Id]\n");
	printf("id = %d\n\n", sysVerInfo.id);
	
	printf("[eth0 Parameters]\n");
	printf("ip      = %s\n", sysVerInfo.ip);
	printf("mac     = %s\n", sysVerInfo.mac);
	printf("mask    = %s\n", sysVerInfo.mask);
	printf("gateway = %s\n\n", sysVerInfo.gw);

	printf("[wlan0 Parameters]\n");
	printf("ip      = %s\n", sysVerInfo.wlan_ip);
	printf("mac     = %s\n", sysVerInfo.wlan_mac);
	printf("mask    = %s\n", sysVerInfo.wlan_mask);
	printf("gateway = %s\n\n", sysVerInfo.wlan_gw);
	
	printf("[device Parameters]\n");
	printf("bd_rm_ms    = %s\n", sysVerInfo.bd_rm_ms);
	printf("myName      = %s\n", sysVerInfo.myName);
	printf("GLOBAL_NUM  = %s\n", sysVerInfo.GLOBAL_NUM);
	printf("LOCAL_NUM   = %s\n", sysVerInfo.LOCAL_NUM);
	printf("myDeviceType= %d\n", sysVerInfo.myDeviceType);
	printf("deviceModel = %s\n", sysVerInfo.deviceModel);
	
}


// lzh_20170420_s
uint16 GetSysVerInfo_IP_NODE_ID(void)
{
	
	return sysVerInfo.id;	
}
// lzh_20170420_e

const char* GetSysVerInfo_IP(void)
{
	return sysVerInfo.ip;
}

void SetSysVerInfo_IP(const char* ip)
{
	strcpy(sysVerInfo.ip, ip);
}


const char* GetSysVerInfo_gateway(void)
{
	return sysVerInfo.gw;
}

void SetSysVerInfo_gateway(const char* ip)
{
	strcpy(sysVerInfo.gw, ip);
}

const char* GetSysVerInfo_mask(void)
{
	return sysVerInfo.mask;
}

void SetSysVerInfo_mask(const char* ip)
{
	strcpy(sysVerInfo.mask, ip);
}
const char* GetSysVerInfo_IP_by_device(char* net_device_name)
{
	char * pReturn;
	
	if(net_device_name == NULL)
	{
		pReturn = sysVerInfo.ip;
	}
	else
	{
		pReturn = (!strcmp(net_device_name, NET_WLAN0) ? sysVerInfo.wlan_ip : sysVerInfo.ip);
	}
	
	return pReturn;
}

const char* GetSysVerInfo_mask_by_device(char* net_device_name)
{
	char * pReturn;
	
	if(net_device_name == NULL)
	{
		pReturn = sysVerInfo.mask;
	}
	else
	{
		pReturn = (!strcmp(net_device_name, NET_WLAN0) ? sysVerInfo.wlan_mask : sysVerInfo.mask);
	}
	
	return pReturn;
}
const char* GetSysVerInfo_gateway_by_device(char* net_device_name)
{
	char * pReturn;
	
	if(net_device_name == NULL)
	{
		pReturn = sysVerInfo.gw;
	}
	else
	{
		pReturn = (!strcmp(net_device_name, NET_WLAN0) ? sysVerInfo.wlan_gw : sysVerInfo.gw);
	}
	
	return pReturn;
}

const char* GetSysVerInfo_mac_by_device(char* net_device_name)
{
	char * pReturn;
	
	if(net_device_name == NULL)
	{
		pReturn = sysVerInfo.mac;
	}
	else
	{
		pReturn = (!strcmp(net_device_name, NET_WLAN0) ? sysVerInfo.wlan_mac : sysVerInfo.mac);
	}
	
	return pReturn;
}

const char* GetSysVerInfo_bd(void)
{
	return sysVerInfo.bd;
}

void SetSysVerInfo_bd(const char* bd)
{
	char temp[20];
	
	strcpy(sysVerInfo.bd, bd);
	memcpy(sysVerInfo.bd_rm_ms, sysVerInfo.bd, 4);
	//dh_20190916_s
	#if 0
	if(!memcmp(sysVerInfo.bd_rm_ms, "0099", 4))
	{
		sysVerInfo.systemType = 0;
	}
	else
	{
		sysVerInfo.systemType = 1;
	}
	sprintf(temp, "%d", sysVerInfo.systemType);
	API_Event_IoServer_InnerWrite_All(SYSTEM_TYPE, temp);
	#endif
	//dh_20190916_e
	API_Event_IoServer_InnerWrite_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}

const char* GetSysVerInfo_rm(void)
{
	return sysVerInfo.rm;
}

void SetSysVerInfo_rm(const char* rm)
{
	strcpy(sysVerInfo.rm, rm);
	memcpy(sysVerInfo.bd_rm_ms+4, sysVerInfo.rm, 4);
	//dh_20190916_s
	#if 0
	if(!memcmp(sysVerInfo.bd_rm_ms, "0099", 4))
	{
		sysVerInfo.systemType = 0;
	}
	else
	{
		sysVerInfo.systemType = 1;
	}
	
	char temp[20];
	sprintf(temp, "%d", sysVerInfo.systemType);

	API_Event_IoServer_InnerWrite_All(SYSTEM_TYPE, temp);
	#endif
	//dh_20190916_e
	API_Event_IoServer_InnerWrite_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}


const char* GetSysVerInfo_ms(void)
{
	return sysVerInfo.ms;
}

void SetSysVerInfo_ms(const char* ms)
{
	strcpy(sysVerInfo.ms, ms);
	memcpy(sysVerInfo.bd_rm_ms+8, sysVerInfo.ms, 2);
	API_Event_IoServer_InnerWrite_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}

const char* GetSysVerInfo_BdRmMs(void)
{
	return sysVerInfo.bd_rm_ms;
}

void SetSysVerInfo_BdRmMs(const char* bd_rm_ms)
{
	int len, i;

	len = strlen(bd_rm_ms);
	if(len != 10)
	{
		//房号长度不对
		dprintf("Error!!! bd_rm_ms len = %d\n", len);
		return;
	}

	for(i = 0; i < len; i++)
	{
		if(bd_rm_ms[i] < '0' || bd_rm_ms[i] > '9')
		{
			//房号非数字不对
			dprintf("Error!!! bd_rm_ms is not number\n");
			return;
		}
	}
	
	strcpy(sysVerInfo.bd_rm_ms, bd_rm_ms);
	memcpy(sysVerInfo.bd, sysVerInfo.bd_rm_ms, 4);
	memcpy(sysVerInfo.rm, sysVerInfo.bd_rm_ms+4, 4);
	memcpy(sysVerInfo.ms, sysVerInfo.bd_rm_ms+8, 2);

	API_Event_IoServer_InnerWrite_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}

const char* GetSysVerInfo_GlobalNum(void)
{
	return sysVerInfo.GLOBAL_NUM;
}

void SetSysVerInfo_GlobalNum(const char* number)
{
	strcpy(sysVerInfo.GLOBAL_NUM, number);
	API_Event_IoServer_InnerWrite_All(MY_GLOBAL_NUM, (uint8*)sysVerInfo.GLOBAL_NUM);
}


const char* GetSysVerInfo_LocalNum(void)
{
	return sysVerInfo.LOCAL_NUM;
}

void SetSysVerInfo_LocalNum(const char* number)
{
	strcpy(sysVerInfo.LOCAL_NUM, number);
	API_Event_IoServer_InnerWrite_All(MY_LOCAL_NUM, (uint8*)sysVerInfo.LOCAL_NUM);
}

const char* GetSysVerInfo_name(void)
{
	return sysVerInfo.myName;
}

void SetSysVerInfo_name(const char* name)
{
	strcpy(sysVerInfo.myName, name);
	API_Event_IoServer_InnerWrite_All(MY_NAME, (uint8*)sysVerInfo.myName);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}

//dh_20190916
void SetSysVerInfo_SystemType(int type)
{
	char temp[20];
	sysVerInfo.systemType = type;
	sprintf(temp, "%d", sysVerInfo.systemType);

	API_Event_IoServer_InnerWrite_All(SYSTEM_TYPE, temp);
}

char GetSysVerInfo_SystemType(void)
{
	return sysVerInfo.systemType;
}

const char* GetSysVerInfo_Sn(void)
{
	return sysVerInfo.sn;
}

const char* GetSysVerInfo_swVer(void)
{
	return sysVerInfo.swVer;
}

const char* GetSysVerInfo_type(void)
{
	return sysVerInfo.type;
}

const char* GetSysVerInfo_MAC(void)
{
	return sysVerInfo.mac;
}

Type_e GetSysVerInfo_MyDeviceType(void)
{
	return sysVerInfo.myDeviceType;
}

const char* DeviceTypeToString(Type_e type)
{
	if(type == TYPE_IM)
	{
		return "IM";
	}
	else if(type == TYPE_DS)
	{
		return "DS";
	}
	else if(type == TYPE_OS)
	{
		return "OS";
	}
	else if(type == TYPE_GL)
	{
		return "GL";
	}
	else if(type == TYPE_VM)
	{
		return "VM";
	}

	return NULL;
}

//dh_20190919_s
void SetSysDefault_BdRmMs(void)
{
	API_Event_IoServer_Inner_factoryDefaultById(BD_RM_MS_NUM);
	API_Event_IoServer_InnerRead_All(BD_RM_MS_NUM, (uint8*)sysVerInfo.bd_rm_ms);
	memcpy(sysVerInfo.bd, sysVerInfo.bd_rm_ms, 4);
	memcpy(sysVerInfo.rm, sysVerInfo.bd_rm_ms+4, 4);
	memcpy(sysVerInfo.ms, sysVerInfo.bd_rm_ms+8, 2);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}

void SetSysDefault_GlobalNum(void)
{
	API_Event_IoServer_Inner_factoryDefaultById(MY_GLOBAL_NUM);
	API_Event_IoServer_InnerRead_All(MY_GLOBAL_NUM, (uint8*)sysVerInfo.GLOBAL_NUM);
}

void SetSysDefault_LocalNum(void)
{
	API_Event_IoServer_Inner_factoryDefaultById(MY_LOCAL_NUM);
	API_Event_IoServer_InnerRead_All(MY_LOCAL_NUM, (uint8*)sysVerInfo.LOCAL_NUM);
}

void SetSysDefault_name(void)
{
	API_Event_IoServer_Inner_factoryDefaultById(MY_NAME);
	API_Event_IoServer_InnerRead_All(MY_NAME, (uint8*)sysVerInfo.myName);
	SetMyProxyData(NULL, NULL, NULL, NULL);
}
//dh_20190919_e

void SysVerInfoUpdateIp(int networkCardSelect)		 //networkCardSelect 0 NET_ETH0, 1 NET_WLAN0
{
	char temp[10] = {0};
	char *networkCard;
	int gwSelect = 0;
	int gateway;
	
	SetTimeUpdateCnt(3);
	API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
	gwSelect = atoi(temp);
	
	printf("---------------------- gwSelect = %d\n", gwSelect);

	pthread_mutex_lock(&sysVerInfo.lock);

	if(networkCardSelect)
	{
		my_inet_ntoa(GetLocalIpByDevice(NET_WLAN0), sysVerInfo.wlan_ip);
		
		GetLocalMacByDevice(NET_WLAN0, temp);
		snprintf(sysVerInfo.wlan_mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);

		gateway = GetLocalGatewayByDevice(NET_WLAN0);
		if(gateway != -1)
		{
			my_inet_ntoa(gateway, sysVerInfo.wlan_gw);
		}
		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_WLAN0), sysVerInfo.wlan_mask);

		//使用有线网关，删掉无线网关
		if(!gwSelect)
		{
			DelDefaultGatewayByDevice(NET_WLAN0);
		}

		sysVerInfo.wlan_en=get_wifi_switch();
		sysVerInfo.wlan_state=get_wifi_connect_state();
		sysVerInfo.wifi_signal_level=get_wifi_signal_level();
		strcpy(sysVerInfo.wlan_cur_ssid,get_wifi_cur_ssid());
	}
	else
	{
		my_inet_ntoa(GetLocalIpByDevice(NET_ETH0), sysVerInfo.ip);
		
		GetLocalMacByDevice(NET_ETH0, temp);
		snprintf(sysVerInfo.mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
		
		gateway = GetLocalGatewayByDevice(NET_ETH0);
		if(gateway != -1)
		{
			my_inet_ntoa(gateway, sysVerInfo.gw);
		}

		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_ETH0), sysVerInfo.mask);

		//使用无线网关，删掉有线网关
		if(gwSelect)
		{
			DelDefaultGatewayByDevice(NET_ETH0);
		}
		sysVerInfo.lan_link=get_lan_link();
		sysVerInfo.lan_act_type=GetIpActionType();
	}

	pthread_mutex_unlock(&sysVerInfo.lock);
	
	SetMyProxyData(NULL, NULL, NULL, NULL);

	printfSysVerInfo();
}
int GetSysVerInfo_NetMode(void)
{
	return sysVerInfo.net_mode;
}
void SysVerInfo_NetMode_Update(int new_mode)
{
	sysVerInfo.net_mode=new_mode;
}
int GetSysVerInfo_LanLink(void)
{
	return sysVerInfo.lan_link;
}
int GetSysVerInfo_LanActType(void)
{
	return sysVerInfo.lan_act_type;
}
int GetSysVerInfo_WlanEn(void)
{
	return sysVerInfo.wlan_en;
}
int GetSysVerInfo_WlanConnect(void)
{
	return sysVerInfo.wlan_state;
}
int GetSysVerInfo_WifiSignalLev(void)
{
	return sysVerInfo.wifi_signal_level;
}
void SysVerInfo_WifiSignalLev_Update(int level)
{
	sysVerInfo.wifi_signal_level=level;
}

char *GetSysVerInfo_WlanCurSsid(void)
{
	return sysVerInfo.wlan_cur_ssid;
}

const char *get_time_string(void) {
    static char cur_system_time[24] = { 0 };
    time_t cur_t;
    struct tm cur_tm;

    time(&cur_t);
    localtime_r(&cur_t, &cur_tm);

    strftime(cur_system_time, sizeof(cur_system_time), "%Y-%m-%d %T", &cur_tm);

    return cur_system_time;
}

int SetIP_ADDR_ByDS1(void)
{
#include "obj_GetIpByNumber.h"
#include "obj_RemoteTB_Process.h"
#include "obj_TableSurver.h"
#include "obj_PublicInformation.h"
	int flag = 0;
	char targetIX_ADDR[11];
	snprintf(targetIX_ADDR, 11, "%s000001", GetSysVerInfo_bd());

	GetIpRspData data = {0};
	if(!API_GetIpNumberFromNet(GetSysVerInfo_BdRmMs(), NULL, NULL, 2, 1, &data) && data.cnt > 0)
	{
		//系统有房号重复
		flag = 1;
		CmdRingReq("IX_ADDR is duplicate");
		return flag;
	}

	if(!API_GetIpNumberFromNet(targetIX_ADDR, NULL, NULL, 2, 1, &data) && data.cnt > 0)
	{
		cJSON* View = cJSON_CreateArray();
		cJSON* Where = cJSON_CreateObject();
		cJSON_AddStringToObject(Where, "IX_ADDR", GetSysVerInfo_BdRmMs());
		if(API_RemoteTableSelect(data.Ip[0], TB_NAME_IP_CONFIG, View, Where, 0))
		{
			cJSON* record = cJSON_GetArrayItem(View, 0);
			int ipStatic = GetEventItemInt(record,  "IP_STATIC");
			char* ip = GetEventItemString(record,  "IP_ADDR");
			char* mask = GetEventItemString(record,  "IP_MASK");
			char* gw = GetEventItemString(record,  "IP_GATEWAY");
			if(CheckNetWorkParam(GetSysVerInfo_mac_by_device(NULL), ip, mask, gw))
			{
				char* pbCertState = API_PublicInfo_Read_String(PB_CERT_STATE);
				if(pbCertState && !strcmp(pbCertState, "Delivered"))
				{
					API_Delete_CERT();
				}

				if(ipStatic)
				{
					api_nm_if_set_lan_mode(0, ip, gw, mask);
					if(API_Create_DCFG_SELF("CERT_ETH0", NULL))
					{
						if(API_Create_CERT(CERT_DCFG_SELF_NAME))
						{
							if(API_Go_Live() == 1)
							{
								CmdRingReq("Config static ip by table and CERT success");
							}
							else
							{
								flag = 7;
							}
						}
						else
						{
							flag = 6;
						}
					}
					else
					{
						flag = 5;
					}

					if(flag)
					{
						CmdRingReq("Config ip by table success but CERT error");
					}
				}
				else
				{
					api_nm_if_set_lan_mode(1, ip, gw, mask);
					CmdRingReq("Config auto ip success");
				}
			}
			else
			{
				//表记录错误
				flag = 4;
				CmdRingReq("Config table's record error");
			}
		}
		else
		{
			//配置表不存在
			flag = 3;
			CmdRingReq("Config table not exist");
		}
		cJSON_Delete(Where);
		cJSON_Delete(View);
	}
	else
	{
		//主机不存在
		flag = 2;
		CmdRingReq("DS1 not online");
	}

    return flag;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

