
// lzh_20181025_s

#ifndef _AUTO_REBOOT_H_
#define _AUTO_REBOOT_H_

#include "utility.h"

typedef  void (*auto_reboot_callback)(void);

typedef struct
{
	int						second_counter;
	int						every_day_enable;	// 0:disable, 1:enable
	pthread_mutex_t 		lock;	
	auto_reboot_callback	pcallback;
} AUTO_REBOOT_T;


void auto_reboot_init( auto_reboot_callback* pcall);
void auto_reboot_deinit(void);

// 使能每天自动重启
// enable:  0/disable, 1/enable
// return：0/ok，-1/err
int auto_reboot_everyday_enable( int enable);

// 定时polling函数
// return：0/reboot ok，-1/Err, other/have no reboot
int auto_reboot_polling();

// 特定场合: eg:测试时设置时间后定时器归零，等待15分钟后会复位
void auto_reboot_polling_reset( void );


#endif

// lzh_20181025_e


