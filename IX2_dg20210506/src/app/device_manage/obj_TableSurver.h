﻿#ifndef _obj_TableSurver_H
#define _obj_TableSurver_H

#include "cJSON.h"
#include <pthread.h>

typedef enum
{
    ERROR_TB,
    SIMPLE_TB,
    STANDARD_TB,
}TableType;

#define TB_NAME         "TB_NAME"
#define TB_DESC         "TB_DESC"
#define TB_TPL          "TB_TPL"
#define TB_KEY          "TB_KEY"
#define TB_RULE         "TB_RULE"
#define TB_MAX_CNT      "TB_MAX_CNT"
#define TB_DAT          "TB_DAT"

int API_TB_Count(const cJSON* table, cJSON* Where);
int API_TB_Add(const cJSON* table, cJSON* data);
int API_TB_Delete(const cJSON* table, cJSON* Where);
//删除index前面的表记录
int API_TB_DeleteBeforeIndex(const cJSON* table, int index);

//截取index前面的部分表
cJSON* API_TB_CopyBeforeIndex(const cJSON* table, int index);

int API_TB_Update(const cJSON* table, cJSON* Where, cJSON* newitem);
int API_TB_SelectBySort(const cJSON* table, cJSON* View, cJSON* Where, int Sort);

// 创建表，从内存表对象复制, 判断来源表是否有模板行：如无，自动拷贝记录1，增加到模板行
cJSON* API_TB_Create(const cJSON* table, const cJSON* tempData);
cJSON* API_TB_CreateNew(const cJSON* table, const char* name, const char* desc, const cJSON* tpl, const char* key, const cJSON* rule);

// 创建表，从表文件或者字符串加载
cJSON* API_TB_LoadFromFile(const char *filename, const char* name, const char* desc, const cJSON* tpl, const char* key, const cJSON* rule);
// 删除表模板
int API_TB_DropTemp(cJSON* table);


#define TB_NAME_IM_DIVERT_TB          	"IM_DIVERT_TB"
#define TB_NAME_IPC_DEV_TB          	"IPC_DEV_TB"
#define TB_NAME_DEV_TB          		"DEV_TB"
#define TB_NAME_IMCallRecord          	"IMCallRecord"
#define TB_NAME_PRIVATE_PWD          	"PRIVATE_PWD"
#define TB_NAME_UNLOCK_RECORD          	"UNLOCK_RECORD"
#define TB_NAME_DSCallRecord          	"DSCallRecord"
#define TB_NAME_DivertAcc_TB          	"DivertAcc_TB"
#define TB_NAME_TB_Sub_SIP_ACC          "TB_Sub_SIP_ACC"
#define TB_NAME_TB_Divert_On          	"TB_Divert_On"
#define TB_NAME_LOG_TB          		"LOG_TB"
#define TB_NAME_IXG_LIST          		"IXG_LIST"
#define TB_NAME_DT_IM          			"DT_IM"
#define TB_NAME_R8001          			"R8001"
#define TB_NAME_CardTable      			"CardTable"
#define TB_NAME_EventRecord          	"EventRecord"
#define TB_NAME_CertLog          		"CertLog"

#define TB_NAME_IXGDFRMKeyFunc          		"IXGDFKeyFunc"
#define TB_NAME_IXGMonRes          		"IXGMonRes"

#define TB_NAME_NDM_Register          	"NDM_Register"
#define TB_NAME_NDM_Log          		"NDM_Log"
#define TB_NAME_NDM_S_Log          		"NDM_S_Log"
#define TB_NAME_Elog          			"Elog"
#define TB_NAME_PRIVATE_PWD_BY_L_NBR          	"PRIVATE_PWD_BY_L_NBR"
#define TB_NAME_IP_CONFIG          		"IP_CONFIG"


typedef struct
{
	char*         name;
	char*         path;
	char*         desc;
	char*         tpl;
	char*         key;
	char*         rule;
	int			  maxRecordCnt;
} TB_Define_S;

typedef struct
{
	cJSON*              tb;
	char*               name;
	char*               path;
    pthread_mutex_t 	lock;
	int                 saveFlag;
	int			  		maxRecordCnt;
} TB_Property_S;

//停止定时保存表
void StopTimingSaveTable(void);

//开启定时保存表
void StartTimingSaveTable(void);

//重新导入表，如果filePath为NULL则从默认路径取文件
int API_TB_ReloadByName(const char* name, const char* filePath);

//读取表数量
int API_TB_CountByName(const char* name, cJSON* Where);

//增加表记录
int API_TB_AddByName(const char* name, cJSON* record);

//删除表记录
int API_TB_DeleteByName(const char* name, cJSON* Where);
int API_TB_DeleteByNameByIndex(const char* name, int index);

//更新表记录
int API_TB_UpdateByName(const char* name, cJSON* Where, cJSON* newitem);

//读表记录
int API_TB_SelectBySortByName(const char* name, cJSON* View, cJSON* Where, int Sort);

const cJSON* API_TB_GetKeyByName(const char* name);

cJSON* API_TB_CreakNewRecordByName(const char* name);

cJSON* API_TB_CopyNewTableByName(const char* name);

cJSON* API_TB_SaveByName(const char* name);

#endif 
