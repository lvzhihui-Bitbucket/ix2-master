/**
  ******************************************************************************
  * @file    obj_GetDviceAddrDispStr.h
  * @author  zeng
  * @version V00.01.00
  * @date    2019.5.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_GetDviceAddrDispStr_h
#define _obj_GetDviceAddrDispStr_h

typedef struct
{
  char MS[5];
  char COMMON[5];
  char IM[5];
  char DS[5];
  char OS[5];
  char GL[5];
  char AC[5];
  int dispMode;
  int nameOnly;
  int globalLocalEnable;
}DeviceAddrDispStr_t;


void get_device_addr_and_name_disp_str(int force_full_disp, const char* bd_rm_ms, const char* global_num, const char* local_num, const char* name, char* p_str_to_put);



#endif


