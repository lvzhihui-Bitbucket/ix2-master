
#ifndef _FTP_DOWNLOAD_H_
#define _FTP_DOWNLOAD_H_


#define DOWNLOAD_INFO_PROCESS			0
#define DOWNLOAD_INFO_OK                1
#define DOWNLOAD_INFO_PARA_ERR1         2
#define DOWNLOAD_INFO_PARA_ERR2         3
#define DOWNLOAD_INFO_PERFORM_ERR       4
#define DOWNLOAD_INFO_INIT_ERR          5
#define	DOWNLOAD_INFO_MAX				6

#define DEFAULT_USERNAME				"userota"
#define DEFAULT_PASSWORD				"userota"

// ftpclient shell command usage: "ftpclient server user password filename [store_dir] [download_max_timeout] [neterr_max_timeout]"
int ftp_download_start( char* server, char* user, char* password, char* filename, char* storedir, int dm_tout, int nm_tout, void (*cb)(int, char*) );

int ftp_download_start_default( char* server, char* filename, char* storedir, void (*cb)(int, char*) );

// cancel the ftp process
int ftp_download_cancel(void);

// report the ftp process state��once per one second
int ftp_download_process(void);

#endif



