/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_sip_account_default_h
#define _obj_sip_account_default_h

#include "task_survey.h"

int api_create_default_sip_info(void);
int api_get_default_sip_local_username( char* username);
int api_get_default_sip_local_password( char* password);
int api_get_default_sip_divert_username( char* username);
int api_get_default_sip_divert_password( char* password);

#endif


