
// lzh_20181025_s
/**
  ******************************************************************************
  * @file    obj_auto_reboot.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>

#include "obj_auto_reboot.h"
#include "task_IoServer.h"

static time_t GetSystemBootTime(void)
{	
	struct sysinfo info;		
	if (sysinfo(&info)) 	
	{		
		fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n", errno, strerror(errno));
		return -1;	
	}	
	return info.uptime;
}

#define	AUTO_REBOOT_POLLING_PERIOD		15			// 定时扫描查询时间	(mininute)

#define AUTO_REBOOT_EVERYDAY_HOUR		3			// 每天定时复位时间点 - 小时
#define	AUTO_REBOOT_EVERYDAY_MINUTE		20			// 每天定时复位时间段 - 分钟

#define AUTO_REBOOT_SYSTICK_MAX			(3*24*3600)	// 设别上电后运行最大时间 (second)


AUTO_REBOOT_T	my_auto_reboot;

// 上电检查执行标志，仅仅执行一次
static int		poweron_auto_reboot_invaliable = 0;

void auto_reboot_init( auto_reboot_callback* pcall)
{
	char temp[20];
	// 读取io参数
	API_Event_IoServer_InnerRead_All(AUTO_REBOOT, temp);
	my_auto_reboot.every_day_enable	= atoi(temp);

	dprintf("my_auto_reboot.every_day_enable = %d\n", my_auto_reboot.every_day_enable);

	my_auto_reboot.second_counter	= 0;
	pthread_mutex_init( &my_auto_reboot.lock, 0);
	
	my_auto_reboot.pcallback = pcall;
	
	// 防止时间总是3点钟10分以内左右，则上电后时间点若为2:50-3:10期间不复位
	if( my_auto_reboot.every_day_enable )
	{
		time_t cur_time;	
		struct tm *ltime;
		time(&cur_time);
		ltime = localtime(&cur_time);		
		if( ( ltime->tm_hour == (AUTO_REBOOT_EVERYDAY_HOUR-1) && ltime->tm_min >=50 ) || ( ltime->tm_hour == AUTO_REBOOT_EVERYDAY_HOUR && ltime->tm_min <= 10 ) )
		{		
			poweron_auto_reboot_invaliable = 1;
		}
		else
		{
			poweron_auto_reboot_invaliable = 0;
		}
	}
}

void auto_reboot_deinit(void)
{
	my_auto_reboot.pcallback = NULL;	
}

// 使能每天自动重启
// enable:  0/disable, 1/enable
// return：0/ok，-1/err
int auto_reboot_everyday_enable( int enable)
{
	char temp[20];
	pthread_mutex_lock(&my_auto_reboot.lock);
	my_auto_reboot.every_day_enable	= enable;
	pthread_mutex_unlock(&my_auto_reboot.lock);	
	return 0;
}

int auto_reboot_everyday_is_enable( void )
{
	int every_day_enable;
	pthread_mutex_lock(&my_auto_reboot.lock);
	every_day_enable = my_auto_reboot.every_day_enable;
	pthread_mutex_unlock(&my_auto_reboot.lock);	
	return every_day_enable;
}


// 特定场合: eg:测试时设置时间后定时器归零，等待15分钟后会复位
void auto_reboot_polling_reset( void )
{
	pthread_mutex_lock(&my_auto_reboot.lock);
	my_auto_reboot.second_counter = 0;
	pthread_mutex_unlock(&my_auto_reboot.lock);	
}

// 定时60s polling一次
// return：1/reboot ok，-1/Err, 0/have no reboot
int auto_reboot_polling()
{
	int every_day_enable;
	// 每15分钟一次
	pthread_mutex_lock(&my_auto_reboot.lock);
	if( ++my_auto_reboot.second_counter < AUTO_REBOOT_POLLING_PERIOD )
	{
		pthread_mutex_unlock(&my_auto_reboot.lock);
		return 0;
	}
	my_auto_reboot.second_counter = 0;
	every_day_enable = my_auto_reboot.every_day_enable;	
	pthread_mutex_unlock(&my_auto_reboot.lock);

	time_t cur_time;
	// 1、判断system tick
	cur_time = GetSystemBootTime();

	printf("auto_reboot_polling: system tick is %d\n", cur_time);
	
	if( cur_time >= AUTO_REBOOT_SYSTICK_MAX || every_day_enable)
	{
		struct tm *ltime;
		time(&cur_time);
		ltime = localtime(&cur_time);
		
		if( ltime->tm_hour == AUTO_REBOOT_EVERYDAY_HOUR && ltime->tm_min <= AUTO_REBOOT_EVERYDAY_MINUTE )
		{		
			if( poweron_auto_reboot_invaliable )
				poweron_auto_reboot_invaliable = 0;
			else
				goto prepare_reboot;
		}
		else
		{
			if( poweron_auto_reboot_invaliable&&ltime->tm_hour > AUTO_REBOOT_EVERYDAY_HOUR)
				poweron_auto_reboot_invaliable = 0;
		}
	}	
	return 0;
	
	prepare_reboot:
	// 关屏幕状态下再进行复位处理
	if(GetMenuOnState() == 0)
	{		
		(*my_auto_reboot.pcallback)();
		usleep(2000*1000);
	}

	return 1;
}

// lzh_20181025_e


