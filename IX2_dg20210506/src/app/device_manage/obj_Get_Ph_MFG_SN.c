/**
  ******************************************************************************
  * @file    obj_RequestSn.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_Get_Ph_MFG_SN.h"
#include "task_IoServer.h"
#include "obj_SYS_VER_INFO.h"
#include <sys/wait.h>
#include "define_file.h"
#include "vdp_uart.h"
#include "task_Beeper.h"

typedef struct
{
    char *name;
    char addrIndex;
    char len;
}IO_MCU_ADDR_S;
IO_MCU_ADDR_S MCU_ADDR_TABLE[] = 
{
    {"MFG_SN", 0x20, 0x14},
    {"MFG_SN_CHECK", 0x34, 0x10},
};

const int  MCU_ADDR_TABLE_CNT = sizeof(MCU_ADDR_TABLE)/sizeof(MCU_ADDR_TABLE[0]);

#ifdef PID_IXSE

static char* mcuSimulateFlash = NULL;
static int mcuSimulateFlashLen = 0;


int MCU_SimulateFlashReadWrite(int readOrWrite, int index, int len, char* value)
{
	FILE* file = NULL;
    int myIndex;

    if(MCU_ADDR_TABLE_CNT == 0)
    {
        dprintf("MCU_SimulateFlashReadWrite error MCU_ADDR_TABLE_CNT == 0\n");
        return 0;
    }
    myIndex = MCU_ADDR_TABLE[0].addrIndex;

    if(index < myIndex || index + len > myIndex + mcuSimulateFlashLen || len <= 0)
    {
        dprintf("MCU_SimulateFlashReadWrite error len = %d\n", len);
        return 0;
    }

    //写
    if(readOrWrite)
    {
        if(memcmp(value, mcuSimulateFlash + index - myIndex, len))
        {
            memcpy(mcuSimulateFlash + index-myIndex, value, len);
            if(file=fopen(MCU_SIMULATE_FLASH_PATH,"w"))
            {
                fwrite(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
                fclose(file);
            }
        }
    }
    //读
    else
    {
        memcpy(value, mcuSimulateFlash + index - myIndex, len);
    }

    return len;
}

int MCU_SimulateFlashInit(void)
{
	FILE* file = NULL;
    int i;
    int ret = 0;

    if(MCU_ADDR_TABLE_CNT)
    {
        for(i = 0, mcuSimulateFlashLen = 0; i < MCU_ADDR_TABLE_CNT; i++)
        {
            mcuSimulateFlashLen += MCU_ADDR_TABLE[i].len;
        }

        if(mcuSimulateFlash)
        {
            free(mcuSimulateFlash);
        }
        mcuSimulateFlash = malloc(mcuSimulateFlashLen);

        if(file=fopen(MCU_SIMULATE_FLASH_PATH,"r"))
        {
            ret = fread(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
        }
        else if(file=fopen(MCU_SIMULATE_FLASH_PATH,"w"))
        {
            ret = fwrite(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
        }
        fclose(file);
    }
    return ret;
}

int API_IO_ReadMcu(const char* key, char* pvalue)
{
    int i;
    int ret = 0;

    for(i = 0; i < MCU_ADDR_TABLE_CNT; i++)
    {
        if(!strcmp(key, MCU_ADDR_TABLE[i].name))
        {
            break;
        }
    }

    if(i < MCU_ADDR_TABLE_CNT)
    {
        ret = MCU_SimulateFlashReadWrite(0, MCU_ADDR_TABLE[i].addrIndex, MCU_ADDR_TABLE[i].len, pvalue);
    }
    dprintf("API_IO_ReadMcu key =%s, ret = %d\n", key, ret);

    return ret;
}
int API_IO_WriteMcu(const char* key, char* pvalue)
{
    int i;
    int ret = 0;

    for(i = 0; i < MCU_ADDR_TABLE_CNT; i++)
    {
        if(!strcmp(key, MCU_ADDR_TABLE[i].name))
        {
            break;
        }
    }

    if(i < MCU_ADDR_TABLE_CNT)
    {
        ret = MCU_SimulateFlashReadWrite(1, MCU_ADDR_TABLE[i].addrIndex, MCU_ADDR_TABLE[i].len, pvalue);
    }

    dprintf("API_IO_WriteMcu key =%s, ret = %d\n", key, ret);

    return ret;
}

static char* SixByteSnToString(char* sixByteSn, char* snString,int order)
{
	int i;
	char sn[6];

	for(i = 0; i < 6; i++)
	{

		if(order==1)
		{
			if(i == 0)
			{
				sn[i] = (sixByteSn[5-i]<<4)+4;;
			}
			else
			{
				sn[i] = sixByteSn[5-i];
			}
		}
		else
		{
			if(i == 0)
			{
				sn[i] = (sixByteSn[i]<<4)+4;;
			}
			else
			{
				sn[i] = sixByteSn[i];
			}
		}
	
	}
	snprintf(snString, 20, "%02x%02x%02x%02x%02x%02x", sn[0], sn[1], sn[2], sn[3], sn[4], sn[5]);

	return snString;
}
#endif

#define MAC_PARAM				0
#define IP_PARAM				1
#define GW_PARAM				2
#define MASK_PARAM				3

static MY_SN mySn = {.type = SN_Type_NONE, .lock = PTHREAD_MUTEX_INITIALIZER};

static int ReadDs2411Sn(void)
{
	int i;
	DS2411_SN ds2411_sn;
	extern FILE *hal_fd;
	int ret = 0;

	for(i = 0; i < 5; i++)
	{
		memset(&ds2411_sn, 0, sizeof(DS2411_SN));
		ret = ioctl(hal_fd, _IOR('G', 28,  DS2411_SN), &ds2411_sn);
		if(ret == 0)
		{
			break;
		}
		dprintf("ReadDs2411 Error %d times\n", i+1);
		usleep(20*1000);
	}

	if(ret == 0)
	{
		pthread_mutex_lock(&mySn.lock);
		for(i = 0, mySn.checkCode = 0; i < 6; i++)
		{
			if(i == 0)
			{
				mySn.serial_number[i] = (ds2411_sn.serial_number[5-i]<<4)+4;;
			}
			else
			{
				mySn.serial_number[i] = ds2411_sn.serial_number[5-i];
			}
			mySn.checkCode += mySn.serial_number[i];
		}
		mySn.type = SN_Type_DS2411;
	    pthread_mutex_unlock(&mySn.lock);

		dprintf("ReadDs2411Sn [%02x.%02x.%02x.%02x.%02x.%02x] chech = 0x%04x!\n",mySn.serial_number[0],mySn.serial_number[1],mySn.serial_number[2],mySn.serial_number[3],mySn.serial_number[4],mySn.serial_number[5], mySn.checkCode);
	}

	return ret;
}

static int ReadRandSn(void)
{
	char mac[6];
	int i;

	GetLocalMacByDevice(NET_ETH0, mac);	

	pthread_mutex_lock(&mySn.lock);

	for(mySn.checkCode = 0, i = 0; i < 6; i++)
	{
		srand(mac[i]+time(NULL));
		mySn.serial_number[i] = rand()%256;

		if(i == 0)
		{
			mySn.serial_number[i] = (mySn.serial_number[i]<<4)+4;
		}

		mySn.checkCode += mySn.serial_number[i];
	}
	
	mySn.type = SN_Type_Rand;

    pthread_mutex_unlock(&mySn.lock);

	dprintf("ReadRandSn [%02x.%02x.%02x.%02x.%02x.%02x] chech = 0x%04x!\n",mySn.serial_number[0],mySn.serial_number[1],mySn.serial_number[2],mySn.serial_number[3],mySn.serial_number[4],mySn.serial_number[5], mySn.checkCode);

	return 0;
}

int UartRecvGetSnRsp(char* precvbuf, int len)
{
	int i;
	short checkCode;

	pthread_mutex_lock( &mySn.lock );
	if(mySn.type == SN_Type_NONE)
	{
		if(len != 8)
		{
			mySn.type = SN_Type_NONE;
			dprintf("len=%d\n", len);
		}
		else
		{
			for(i = 0; i < len; i++)
			{
				if(precvbuf[i] != 0)
				{
					break;
				}
			}

			//不全为0
			if(i < 8)
			{
				mySn.checkCode = 0;
				checkCode = 0;
				for(i = 0; i < 6; i++)
				{
					mySn.serial_number[i] = precvbuf[i];
					checkCode += precvbuf[i];
					if(i == 0)
					{
						mySn.serial_number[i] = (mySn.serial_number[i]<<4)+4;
					}
					mySn.checkCode += mySn.serial_number[i];
				}

				if(checkCode == (precvbuf[6] << 8) + precvbuf[7])
				{
					mySn.type = SN_Type_Tiny1616;
				}
				//校验不对，数据有错
				else
				{
					mySn.type = SN_Type_NONE;
					dprintf("checkCode = 0x%04x, (precvbuf[6] << 8) + precvbuf[7] = 0x%04x\n",checkCode, (precvbuf[6] << 8) + precvbuf[7]);
				}
			}
			//全部为0，数据有错
			else
			{
				mySn.type = SN_Type_NONE;
				dprintf("precvbuf=0 0 0 0 0 0 0 0\n");
			}
		}
	}
    pthread_mutex_unlock( &mySn.lock );
    return 0;
}

static int ReadTiny1616Sn(void)
{
#ifdef PID_IXSE
	int ret = -1;
	int i;
	int sn[6];
	char tempSnString[20];
	char tempMd5[16];
	char md5[16];

	if(API_IO_ReadMcu("MFG_SN", tempSnString))
	{
		StringMd5_Calculate(tempSnString, md5);
		if(API_IO_ReadMcu("MFG_SN_CHECK", tempMd5) && !memcmp(md5, tempMd5, 16))
		{
			dprintf("ReadTiny1616Sn :%s\n", tempSnString);
			ret = 0;
			pthread_mutex_lock(&mySn.lock);
			mySn.type = SN_Type_Tiny1616;
			sscanf(tempSnString, "%02x%02x%02x%02x%02x%02x", &sn[0], &sn[1], &sn[2], &sn[3], &sn[4], &sn[5]);
			for(mySn.checkCode = 0, i = 0; i < 6; i++)
			{
				mySn.serial_number[i]=sn[i];
				mySn.checkCode += mySn.serial_number[i];
			}
			pthread_mutex_unlock(&mySn.lock);
			dprintf("ReadTiny1616Sn [%02x.%02x.%02x.%02x.%02x.%02x] chech = 0x%04x!\n",mySn.serial_number[0],mySn.serial_number[1],mySn.serial_number[2],mySn.serial_number[3],mySn.serial_number[4],mySn.serial_number[5], mySn.checkCode);
		}
	}

	return ret;
#else
	int i;
	int ret = -1;
	SN_Type_e type;

	pthread_mutex_lock(&mySn.lock);
	mySn.type = SN_Type_NONE;
	type = SN_Type_NONE;
    pthread_mutex_unlock(&mySn.lock);

	for(i = 0; i < 3 && type == SN_Type_NONE; i++)
	{
		api_uart_send_pack(UART_TYPE_GetSn, NULL, 0);
		usleep(100*1000);
		pthread_mutex_lock(&mySn.lock);
		type = mySn.type;
		pthread_mutex_unlock(&mySn.lock);
	}

	if(type == SN_Type_Tiny1616)
	{
		ret = 0;
	}
	else if(type == SN_Type_NONE)
	{
		char mac[18] = {0};
		int sn[6];

		dprintf("ReadTiny1616Sn error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Tiny1616 not respone\n");

		if((API_Event_IoServer_InnerRead_All(MacAddress, mac) == 0) && strcmp(mac, "-") && mac[0])
		{
			dprintf("ReadTiny1616Sn last mac:%s\n", mac);

			sscanf(mac, "%x:%x:%x:%x:%x:%x", &sn[0], &sn[1], &sn[2], &sn[3], &sn[4], &sn[5]);
			for(mySn.checkCode = 0, i = 0; i < 6; i++)
			{
				mySn.serial_number[i] = sn[i];
				mySn.checkCode += mySn.serial_number[i];
			}
			pthread_mutex_lock(&mySn.lock);
			mySn.type = SN_Type_Tiny1616;
			pthread_mutex_unlock(&mySn.lock);
			ret = 0;
		}
	}

	if(ret == 0)
	{
		dprintf("ReadTiny1616Sn [%02x.%02x.%02x.%02x.%02x.%02x] chech = 0x%04x!\n",mySn.serial_number[0],mySn.serial_number[1],mySn.serial_number[2],mySn.serial_number[3],mySn.serial_number[4],mySn.serial_number[5], mySn.checkCode);
	}

	return ret;
#endif	
}
int SetTiny1616Sn(char* sn)
{
#ifdef PID_IXSE
	int ret;
	char md5[16];
	char snString[20];
	SixByteSnToString(sn, snString,0);
	dprintf("SetTiny1616Sn :%s\n", snString);
	ret = API_IO_WriteMcu("MFG_SN", snString);
	if(ret)
	{
		StringMd5_Calculate(snString, md5);
		ret = API_IO_WriteMcu("MFG_SN_CHECK", md5);
		if(ret)
		{
			dprintf("SetTiny1616Sn :%s ok!!!\n", snString);
			//sleep(1);
			//GetPhMFGSnInit();
			HardwareRestar();
		}
	}
	
	return ret;
#else
	int i;
	int ret = -1;
	SN_Type_e type;
	char sendData[8];
	short checkCode;

	for(i = 0, checkCode = 0; i < 8; i++)
	{
		if(i < 6)
		{
			sendData[i] = sn[i];
			checkCode += sn[i];
		}
		else if(i == 6)
		{
			sendData[i] = (checkCode >> 8);
		}
		else
		{
			sendData[i] = (checkCode & 0xFF);
		}
	}

	pthread_mutex_lock(&mySn.lock);
	mySn.type = SN_Type_NONE;
	type = SN_Type_NONE;
    pthread_mutex_unlock(&mySn.lock);

	for(i = 0; i < 3 && type == SN_Type_NONE; i++)
	{
		api_uart_send_pack(UART_TYPE_SetSn, sendData, 8);
		usleep(100*1000);
		pthread_mutex_lock(&mySn.lock);
		type = mySn.type;
		pthread_mutex_unlock(&mySn.lock);
	}

	if(type == SN_Type_Tiny1616)
	{
		ret = 0;
		dprintf("SetTiny1616Sn [%02x.%02x.%02x.%02x.%02x.%02x] chech = 0x%04x!\n",mySn.serial_number[0],mySn.serial_number[1],mySn.serial_number[2],mySn.serial_number[3],mySn.serial_number[4],mySn.serial_number[5], mySn.checkCode);
		//HardwareRestar();
		sleep(1);
		
		
		GetPhMFGSnInit();
		if(IfHasSn())
		{
			BEEP_CONFIRM();
			api_create_default_sip_info();
		}
	}

	return ret;
#endif	
}

int IfHasSn(void)
{
	int i;
	int ret = -1;
	SN_Type_e type;

	pthread_mutex_lock(&mySn.lock);
	type = mySn.type;
	pthread_mutex_unlock(&mySn.lock);

	if(type == SN_Type_Rand)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}


void GetPhMFGSnInit(void)
{
	SYS_VER_INFO_T sysInfo;
	int i;
	char mac[18];

	if(ReadDs2411Sn() != 0)
	{
		if(ReadTiny1616Sn() != 0)
		{
			ReadRandSn();
		}
		else
		{
			if(mySn.serial_number[0]!=0x64||(mySn.serial_number[1]&0xf0)!=0)
			{
				ReadRandSn();
			}
		}
	}

	SysVerInfoInit(1);

	sysInfo = GetSysVerInfo();
	//修改MAC和sn一样
	for(i = 0; i<6; i++)
	{
		memcpy(sysInfo.mac+3*i, sysInfo.sn+2*i, 2);
		if(i == 5)
		{
			sysInfo.mac[3*i+2] = 0;
		}
		else
		{
			sysInfo.mac[3*i+2] = ':';
		}
	}
	
	SetSysVerInfo(sysInfo);
	if(mySn.type != SN_Type_Rand)
	{
		API_Event_IoServer_InnerRead_All(MacAddress, mac);
		if(strcmp(sysInfo.mac,mac)!=0)
			API_Event_IoServer_InnerWrite_All(MacAddress, sysInfo.mac);
	}
}
/*
int RequestChipIdFromN329ToStm32(char * pChipId)
{
	int len;
	int ret = 0;
	int sendcnt;

	GET_CHIP_ID_T	data;
	
	data.head.msg_target_id	= MSG_ID_NetManage;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= VDP_NET_MANANGE_MSG_IPG_GET_SN_REQ;
	data.head.msg_sub_type 	= 0;	
	memset(data.chipId, 0, 6);
	
	vdp_task_t* pTask = GetTaskAccordingMsgID(GetMsgIDAccordingPid(pthread_self()));


	if( pTask != NULL )
	{
		for( sendcnt = 2; sendcnt >0; sendcnt-- )
		{	
			WaitForBusinessACK( pTask->p_syc_buf, VDP_NET_MANANGE_MSG_IPG_GET_SN_REQ, (char*)&data, &len, 1 );
			WaitForBusinessACK( pTask->p_syc_buf, VDP_NET_MANANGE_MSG_IPG_GET_SN_REQ, (char*)&data, &len, 1 );
			vdp_uart_send_data((char*)&data, sizeof(VDP_MSG_HEAD));
			// 等待业务应答
			ret = WaitForBusinessACK( pTask->p_syc_buf, VDP_NET_MANANGE_MSG_IPG_GET_SN_REQ, (char*)&data, &len, 5000 );
			if( ret > 0 )
			{		
				memcpy(pChipId, data.chipId, 6);
				break;
			}
		}
	}
	return ret;	
}
*/
int API_GetLocalSn(char* pSn)
{
	short checkCode;
	int i;
	int result;

	if(mySn.type != SN_Type_NONE)
	{
		for(i = 0, checkCode = 0; i < 6; i++)
		{
			checkCode += mySn.serial_number[i];
		}

		if(checkCode == mySn.checkCode)
		{
			if(pSn != NULL)
			{
				memcpy(pSn, mySn.serial_number, 6);
			}
			result = 0;
		}
		else
		{
			dprintf("Sn check error !!! checkCode=0x%04x, mySn.checkCode=0x%04x\n", checkCode, mySn.checkCode);
			result = -1;
		}
	}
	else
	{
		dprintf("Have no sn !!!\n");
		result = -1;
	}

	return result;
}

/*

uint8 API_SetLocalSn(char* pSn)
{
	FILE* file = NULL;
	FILE* file_tem = NULL;
	char* ptr  = NULL;
	char line[100] = {'0'};
	unsigned char ret = 1;
	unsigned char len = sizeof(line);

	// read-only file   	// write file
	if( ((file=fopen(MFG_SN_PATH,"r"))==NULL)  || ((file_tem=fopen(MFG_SN_TEMP_PATH,"w+"))==NULL) )
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		goto err;
	}

	//read line
	while( fgets(line,len,file) != NULL)
	{	
		// Invalid item ->delete
		if((ptr = strchr(line,'#')) == NULL)
		{
			continue;
		}
		
		// find ip_node_id item
		if( memcmp(ptr,"#mfg_sn",strlen("#mfg_sn")) == 0 )
		{		
			snprintf( line, len, "#mfg_sn %02x%02x%02x%02x%02x%02x\r\n", pSn[0], pSn[1], pSn[2], pSn[3], pSn[4], pSn[5]);
		}
		// copy file
		fprintf( file_tem, "%s", line );
	}

	fclose( file );		
	fclose( file_tem );
	file_tem = NULL;
	file = NULL;
	
	snprintf( line, 100,  "cp %s %s", MFG_SN_PATH, MFG_SN_BAK_PATH);
	system(line);
		
	snprintf( line, 100,  "cp %s %s", MFG_SN_TEMP_PATH, MFG_SN_PATH);
	system(line);

	ret = 0;

	err:
	{
		if( file != NULL)
			fclose( file );

		if( file_tem != NULL )
			fclose( file_tem );
	}
	
	sync();

	return ret;
}
*/

uint16 API_GetLocalIpNodeId(void)
{
	return (uint16)(GetLocalIpByDevice(NET_ETH0)>>24)&0xFF;
	/*
	FILE *file;
	char line[100] = {0};
	char* ptr  = NULL;
	uint16 ipNodeId = 0;

	if( ((file=fopen(MFG_SN_PATH,"r"))==NULL) )
	{
		eprintf( "GetLocalIpNodeId error:%s\n",strerror(errno) );
		return 0;
	}

	//read line
	while( fgets(line,100,file) != NULL )
	{	
		// Invalid item ->delete
		if( (ptr = strstr(line,"#ip_node_id")) == NULL )
		{
			memset(line, 0, 100);
			continue;
		}

		ptr += sizeof("#ip_node_id");

		ipNodeId = atoi(ptr);
	}
	fclose( file );	
	
	return ipNodeId;
	*/
}

/*
uint8 API_SetLocalIpNodeId(uint16 nodeId)
{
	return 0;

	

	FILE* file = NULL;
	FILE* file_tem = NULL;
	char* ptr  = NULL;
	char line[100] = {'0'};
	unsigned char ret = 1;
	unsigned char len = sizeof(line);

	// read-only file   	// write file
	if( ((file=fopen(MFG_SN_PATH,"r"))==NULL)  || ((file_tem=fopen(MFG_SN_TEMP_PATH,"w+"))==NULL) )
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		goto err;
	}

	//read line
	while( fgets(line,len,file) != NULL )
	{	
		// Invalid item ->delete
		if( (ptr = strchr(line,'#')) == NULL )
			continue;					
		
		// find ip_node_id item
		if( memcmp(ptr, "#ip_node_id", strlen("#ip_node_id")) == 0 )
		{		
			snprintf( line, len, "#ip_node_id %d\n", nodeId);
		}
		
		// copy file
		fprintf( file_tem, "%s", line );
	}
	fclose( file );		
	fclose( file_tem );
	file_tem = NULL;
	file = NULL;
	
	snprintf( line, 100,  "cp %s %s", MFG_SN_PATH, MFG_SN_BAK_PATH);
	system(line);
	
	snprintf( line, 100,  "cp %s %s", MFG_SN_TEMP_PATH, MFG_SN_PATH);
	system(line);

	ret = 0;

	err:
	{
		if( file != NULL)
			fclose( file );

		if( file_tem != NULL )
			fclose( file_tem );
	}
	sync();
	
	return ret;
}

uint16 API_GetDipSetIpNodeId(void)
{
	
	FILE *file;
	char line[100] = {0};
	char* ptr  = NULL;
	uint16 ipNodeId = 0;

	if( ((file=fopen(MFG_SN_PATH,"r"))==NULL) )
	{
		eprintf( "GetLocalIpNodeId error:%s\n",strerror(errno) );
		return 0;
	}

	//read line
	while( fgets(line,100,file) != NULL )
	{	
		// Invalid item ->delete
		if( (ptr = strstr(line,"#dip_set_node_id")) == NULL )
		{
			memset(line, 0, 100);
			continue;
		}

		ptr += sizeof("#dip_set_node_id");

		ipNodeId = atoi(ptr);
	}
	fclose( file );	

	return ipNodeId;
}

uint8 API_SetDipSetIpNodeId(uint16 nodeId)
{
	FILE* file = NULL;
	FILE* file_tem = NULL;
	char* ptr  = NULL;
	char line[100] = {'0'};
	unsigned char ret = 1;
	unsigned char len = sizeof(line);

	// read-only file   	// write file
	if( ((file=fopen(MFG_SN_PATH,"r"))==NULL)  || ((file_tem=fopen(MFG_SN_TEMP_PATH,"w+"))==NULL) )
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		goto err;
	}

	//read line
	while( fgets(line,len,file) != NULL )
	{	
		// Invalid item ->delete
		if( (ptr = strchr(line,'#')) == NULL )
			continue;					
		
		// find dip_set_node_id item
		if( memcmp(ptr,"#dip_set_node_id",strlen("#dip_set_node_id")) == 0 )
		{		
			snprintf( line, len, "#dip_set_node_id %d\n", nodeId);
		}
		
		// copy file
		fprintf( file_tem, "%s", line );
	}
	fclose( file );		
	fclose( file_tem );
	file_tem = NULL;
	file = NULL;
	
	snprintf( line, 100,  "cp %s %s", MFG_SN_PATH, MFG_SN_BAK_PATH);
	system(line);
	
	snprintf( line, 100,  "cp %s %s", MFG_SN_TEMP_PATH, MFG_SN_PATH);
	system(line);

	ret = 0;
	err:
	{
		if( file != NULL)
			fclose( file );

		if( file_tem != NULL )
			fclose( file_tem );
	}
	sync();
	
	return ret;
}
*/

int GetLocalMac( char* pMac ) 
{  
	int sock_get_mac;        
	struct ifreq ifr_mac;
	if( (sock_get_mac=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		return -1;  
	}      
	memset(&ifr_mac,0,sizeof(ifr_mac));     
	strncpy( ifr_mac.ifr_name, "eth0", sizeof(ifr_mac.ifr_name)-1);   
	if( ioctl( sock_get_mac, SIOCGIFHWADDR, &ifr_mac) < 0) 
	{  
		close(sock_get_mac);
	    return -1;
	}  	 
	pMac[0] = (char)ifr_mac.ifr_hwaddr.sa_data[0];  
	pMac[1] = (char)ifr_mac.ifr_hwaddr.sa_data[1];  
	pMac[2] = (char)ifr_mac.ifr_hwaddr.sa_data[2];  
	pMac[3] = (char)ifr_mac.ifr_hwaddr.sa_data[3];  
	pMac[4] = (char)ifr_mac.ifr_hwaddr.sa_data[4];  
	pMac[5] = (char)ifr_mac.ifr_hwaddr.sa_data[5];  

	close(sock_get_mac);
	return 0;
}  

//czn_20160812_s
int GetLocalMacByDevice(char* net_device_name, char* pMac ) 
{  
	int sock_get_mac;        
	struct ifreq ifr_mac;
	if( (sock_get_mac=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		return -1;  
	}      
	memset(&ifr_mac,0,sizeof(ifr_mac));     
	strncpy( ifr_mac.ifr_name, net_device_name, sizeof(ifr_mac.ifr_name)-1);   
	if( ioctl( sock_get_mac, SIOCGIFHWADDR, &ifr_mac) < 0) 
	{  
		close(sock_get_mac);
	    return -1;
	}  	 
	pMac[0] = (char)ifr_mac.ifr_hwaddr.sa_data[0];  
	pMac[1] = (char)ifr_mac.ifr_hwaddr.sa_data[1];  
	pMac[2] = (char)ifr_mac.ifr_hwaddr.sa_data[2];  
	pMac[3] = (char)ifr_mac.ifr_hwaddr.sa_data[3];  
	pMac[4] = (char)ifr_mac.ifr_hwaddr.sa_data[4];  
	pMac[5] = (char)ifr_mac.ifr_hwaddr.sa_data[5];  

	close(sock_get_mac);
	return 0;
}  



int mypopen(const char* type, FILE* fp[2], char * execPro, ... )
{
	#define OPT_MAX 	5
	//mypopen 函数返回值大于0表明shell命令的进程ID,返回负值表明出现错误
	int i, j;
	int pfd[2];
	int pfderr[2];
	pid_t pid;
	int Ret;

	va_list IpArgs;
	char* arg[OPT_MAX+1] = {0};

	//标准输入输出管道文件生成
	if(pipe(pfd)<0) return -1;

	if(pipe(pfderr)<0)
	{
		close(pfd[0]);
		close(pfd[1]);
		return -1;
	}

	if((pid = fork()) < 0)
	{
		return -1;
	}
	else if(pid == 0)
	{
		if(*type == 'r')
		{
			if(pfd[1] != STDOUT_FILENO)
			{
				Ret = dup2(pfd[1], STDOUT_FILENO);
			}

			if(pfderr[1] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[1], STDERR_FILENO);
			}
			
		}
		else
		{
			if(pfd[0] != STDIN_FILENO)
			{
				Ret = dup2(pfd[0], STDIN_FILENO);
			}

			if(pfderr[0] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[0], STDERR_FILENO);
			}
		}

		va_start(IpArgs, execPro);
		for(i = 0; i < OPT_MAX; i++)
		{
			if(i == 0)
			{
				arg[i] = execPro;
			}
			else
			{
				arg[i] = va_arg(IpArgs, char*);
			}
			
			if(arg[i] == NULL)
			{

				break;
			}
		}
		va_end(IpArgs);
		
		execvp(execPro, arg);
		
		_exit(0);
	}
	else
	{
		if(*type == 'r')
		{
			close(pfd[1]);
			close(pfderr[1]);
			if((fp[0] = fdopen(pfd[0], type)) == NULL)
			{
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[0], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}
		}
		else
		{
			close(pfd[0]);
			close(pfderr[0]);
			if((fp[0] = fdopen(pfd[1], type)) == NULL)
			{
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[1], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}
		}
	}

	return pid;

}


int myfread(FILE* fp[2], void* RetVal, int RetSize, int TimeOut)
{
	int size, Ret, RecvSize, MaxFD, RecvSize1;
	int Flag[2] ={0, 0};
	struct timeval tv;
	fd_set fdR;

	MaxFD = (fileno(fp[0]) > fileno(fp[1]) ? fileno(fp[0]) : fileno(fp[1]));
	int i;
	size = 0;
	while(size < RetSize)
	{
		memset(&tv, 0, sizeof(tv));
		tv.tv_sec = TimeOut;
		tv.tv_usec = 0;
		FD_ZERO(&fdR);
		FD_SET(fileno(fp[0]), &fdR);
		FD_SET(fileno(fp[1]), &fdR);
		
		Ret = select(MaxFD + 1, &fdR, NULL, NULL, &tv);
		//printf("111111111 Ret = %d\n", Ret);
		if(Ret <= 0)
		{
			if(size)
			{
				break;
			}
			else
			{
				return -1;
			}
		}
		else if(Ret > 0)
		{
			if(FD_ISSET(fileno(fp[0]), &fdR))
			{
				RecvSize = read(fileno(fp[0]), RetVal + size, RetSize - size);
				
				//printf("22222222 RecvSize = %d\n", RecvSize);
				if(RecvSize > 0)
				{
					size += RecvSize;
				}
				else if(RecvSize < 0)
				{
					return -1;
				}
			}

			if(FD_ISSET(fileno(fp[1]), &fdR))
			{
				RecvSize1 = read(fileno(fp[1]), RetVal + size, RetSize - size);
				//printf("3333333 RecvSize = %d\n", RecvSize);
				if(RecvSize1 > 0)
				{
					size += RecvSize1;
				}
				else if(RecvSize1 < 0)
				{
					return -1;
				}
			}
			
			if(!RecvSize && !RecvSize1)
			{
				break;
			}
		}
	}

	return size;
}


int mypclose(FILE* fp[2], int* processPid)
{
	int states;
	pid_t pid;
	pid_t pid_tmp;
	int ret;

	if(fclose(fp[0]) == EOF) return -1;
	if(fclose(fp[1]) == EOF) return -1;
	fp[0] = NULL;
	fp[1] = NULL;

	if((pid = *processPid) == 0) return -1;
	ret = waitpid(pid, &states, WNOHANG);
	if(ret == 0)
	{
		kill(pid, SIGKILL);
		do
		{
			pid_tmp = waitpid(pid, &states, 0);
		}while(pid_tmp == -1 && errno == EINTR);
	}

	*processPid = 0;

	return states;
}


// lzh_20180110_s
int GetLocalGatewayAddr(void)
{

	FILE* stream[2];
	int gw = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;
	
	pid = mypopen("r", stream, "route", "-n", NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			//printf("myfread error!\n");
		}
		else
		{
			strtok(readBuffer,"\n");
			while(pos1 = strtok(NULL,"\n"))
			{
				if(strstr(pos1, "UG"))
				{
					sscanf(pos1, "%*s%s", temp2);
					sscanf(temp2, "%[^ ]", gwChar);
					gw = inet_addr(gwChar);
					break;
				}
			}
		}
	}
	mypclose(stream, &pid);

	return gw;
}
// lzh_20180110_e

int GetLocalGateway(void)
{
	FILE* stream[2];
	int gw = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;
	
	pid = mypopen("r", stream, "route", "-n", NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			printf("myfread error!\n");
		}
		else
		{
			strtok(readBuffer,"\n");
			while(pos1 = strtok(NULL,"\n"))
			{
				if(strstr(pos1, "UG"))
				{
					sscanf(pos1, "%*s%s", temp2);
					sscanf(temp2, "%[^ ]", gwChar);
					gw = inet_addr(gwChar);
					break;
				}
			}
		}
	}
	mypclose(stream, &pid);

	return gw;
}

int GetLocalGatewayByDevice(char* net_device_name)
{
	FILE* stream[2];
	int gw = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;
	pid = mypopen("r", stream, "route", "-n", NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			printf("myfread error!\n");
		}
		else
		{
			strtok(readBuffer,"\n");
			while(pos1 = strtok(NULL,"\n"))
			{
				if(strstr(pos1, "UG") && strstr(pos1, net_device_name))
				{
					sscanf(pos1, "%*s%s", temp2);
					sscanf(temp2, "%[^ ]", gwChar);
					gw = inet_addr(gwChar);
					break;
				}
			}
		}
	}
	mypclose(stream, &pid);

	return gw;
}

int DelDefaultGatewayByDevice(char* net_device_name)
{
	FILE* stream[2];
	char readBuffer[1024] = {0};
	int pid;
	int ret = -1;
	char gw[20] = {0};
	int intGw;

	if((intGw = GetLocalGatewayByDevice(net_device_name)) != -1)
	{
		my_inet_ntoa(intGw, gw);
		
		pid = mypopen("r", stream, "route", "del", "default", "gw", gw, NULL);
		mypclose(stream, &pid);

		pid = mypopen("r", stream, "route", "del", "default", "gw", gw, NULL);
		if(pid > 0)
		{
			if(myfread(stream, readBuffer, 1023, 1) == -1)
			{
				printf("myfread error!\n");
			}
			else
			{
				if(strstr(readBuffer, "No such process"))
				{
					ret = 0;
				}
			}
		}
		mypclose(stream, &pid);
	}

	return ret;
}

int SetDefaultGatewayByDevice(char* net_device_name, char* gw)
{
	FILE* stream[2];
	char readBuffer[1024] = {0};
	int pid;
	int ret = -1;
	
	pid = mypopen("r", stream, "route", "add", "default", "gw", gw, net_device_name, NULL);
	
	mypclose(stream, &pid);

	pid = mypopen("r", stream, "route", "add", "default", "gw", gw, net_device_name, NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			printf("myfread error!\n");
		}
		else
		{
			if(strstr(readBuffer, "File exists"))
			{
				ret = 0;
			}
		}
	}
	mypclose(stream, &pid);

	return ret;
}

int ChangeDefaultGateway(char* net_device_name, char* gw)
{
	char temp[10] = {0};
	char *addNetworkCard;
	char *delNetworkCard;
	char *gateway;
	
	if(net_device_name == NULL)
	{
		API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
		addNetworkCard = (atoi(temp) ? NET_WLAN0 : NET_ETH0);
		delNetworkCard = (atoi(temp) ? NET_ETH0 : NET_WLAN0);
	}
	else if(!strcmp(net_device_name, NET_ETH0))
	{
		addNetworkCard = NET_ETH0;
		delNetworkCard = NET_WLAN0;
	}
	else if(!strcmp(net_device_name, NET_WLAN0))
	{
		addNetworkCard = NET_WLAN0;
		delNetworkCard = NET_ETH0;
	}
	else
	{
		API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
		addNetworkCard = (atoi(temp) ? NET_WLAN0 : NET_ETH0);
		delNetworkCard = (atoi(temp) ? NET_ETH0 : NET_WLAN0);
	}

	if(gw == NULL)
	{
		gateway = GetSysVerInfo_gateway_by_device(addNetworkCard);
	}
	else
	{
		gateway = gw;
	}
	
	DelDefaultGatewayByDevice(delNetworkCard);
	SetDefaultGatewayByDevice(addNetworkCard, gateway);
	
}

int PingNetwork(char* ipString)
{
	FILE* stream[2];
	int ret = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;

	//printf("PingNetwork %s start!\n", ipString);
	
	pid = mypopen("r", stream, "ping", ipString, NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 2) == -1)
		{
			//printf("myfread error!\n");
		}
		else
		{
			if(strstr(readBuffer, "time="))
			{
				ret = 0;
			}
		}
	}
	mypclose(stream, &pid);
	
	//printf("ret = %d, readBuffer = %s\n", ret, readBuffer);

	return ret;
}

int PingNetwork2(char* ipString,float *pingtime)	//20181017
{
	FILE* stream[2];
	int ret = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;
	
	//printf("PingNetwork %s start!\n", ipString);
	
	pid = mypopen("r", stream, "ping", ipString, NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 2) == -1)
		{
			//printf("myfread error!\n");
		}
		else
		{
			char *pch1,*pch2;
			if(pch1 = strstr(readBuffer, "time="))
			{
				pch1  += strlen("time=");
				if(pch2 = strstr(pch1, " ms"))
				{
					pch2[0] = 0;
					*pingtime = atof(pch1);
					ret = 0;
				}
				
			}
		}
	}
	mypclose(stream, &pid);
	
	//printf("ret = %d, readBuffer = %s\n", ret, readBuffer);

	return ret;
}

int GetLocalMaskAddr(void)
{
	char* pIP;
	int sock_get_ip;  
	int ip;
	struct   ifreq ifr_ip;     	
	if ((sock_get_ip=socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{  
		eprintf("socket create failse...GetLocalIp!\n");  
		return -1;  
	}  
	memset(&ifr_ip, 0, sizeof(ifr_ip));
	strncpy(ifr_ip.ifr_name, "eth0", sizeof(ifr_ip.ifr_name) - 1);     
	if( ioctl( sock_get_ip, SIOCGIFNETMASK, &ifr_ip) < 0 )     
	{     
		close( sock_get_ip ); 
		return -1;
	}       

	// 得到ip地址的字符串
	pIP = inet_ntoa(((struct sockaddr_in *)&ifr_ip.ifr_addr)->sin_addr);

	// 得到网络字节序IP 地址
	ip = htonl(inet_network(pIP) );

	close( sock_get_ip ); 
	
	return ip; 
}

int GetLocalMaskAddrByDevice(char* net_device_name)
{
	char* pIP;
	int sock_get_ip;  
	int ip = -1;
	struct   ifreq ifr_ip;     	
	if ((sock_get_ip=socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{  
		eprintf("socket create failse...GetLocalIp!\n");  
		return -1;  
	}  
	memset(&ifr_ip, 0, sizeof(ifr_ip));
	strncpy(ifr_ip.ifr_name, net_device_name, sizeof(ifr_ip.ifr_name) - 1);     
	if( ioctl( sock_get_ip, SIOCGIFNETMASK, &ifr_ip) < 0 )     
	{     
		close( sock_get_ip ); 
		return -1;
	}       

	// 得到ip地址的字符串
	pIP = inet_ntoa(((struct sockaddr_in *)&ifr_ip.ifr_addr)->sin_addr);

	// 得到网络字节序IP 地址
	ip = htonl(inet_network(pIP) );

	close( sock_get_ip ); 
	
	return ip; 
}

/*****************************************************************************************************
 * @fn      AssertParam
 *
 * @brief   judging the input address is correct
 *
 * @param   type-> address type 		param-> address data
 *			
 * @return  1--ok 0--fail
 *******************************************************************************************************/
unsigned char AssertParam( char type, char* param )
{
	int n[6];
	unsigned char c[6];
	unsigned char i;

	if( param == NULL )
		return 0;
	
	switch( type )
	{
		case MAC_PARAM:
			if( 11== sscanf(param,"%02x%c%02x%c%02x%c%02x%c%02x%c%02x",&n[0],&c[0],&n[1],&c[1],&n[2],&c[2],&n[3],&c[3],&n[4],&c[4],&n[5]) )//czn_20160812
			{
				c[5] = ':';

				// comparison address data
				for( i = 0; i < 6; i++ )
				{
					if( (c[i] != ':' ) || (n[i] > 255 || n[i] < 0) )
						return 0;
				}
				return 1;
			} 
			return 0;

		case IP_PARAM:
		case GW_PARAM:
		case MASK_PARAM:
			// format
			if( 7 == sscanf(param,"%d%c%d%c%d%c%d",&n[0],&c[0],&n[1],&c[1],&n[2],&c[2],&n[3]) )
			{
				c[3] = '.';

				// comparison address data
				for( i = 0; i < 4; i++ )
				{
					if( (c[i] != '.' ) || (n[i] > 255 || n[i] < 0) )
						return 0;
				}
				return 1;
			} 
			return 0;
	}

	return 0;
}

/*****************************************************************************************************
 * @fn      CheckNetWorkParam
 *
 * @brief   configuration network address
 *
 * @param   mac-> mac address  ip-> ip address  mask-> mask address  gw-> gw address
 *			
 * @return  1--ok 0--fail
 *******************************************************************************************************/
int CheckNetWorkParam( char* mac, char* ip, char* mask, char* gw )
{
	int ret = 1;

	if(!AssertParam(MAC_PARAM,mac))
	{
		ret = 0;
	}
	else if(!AssertParam(IP_PARAM,ip))
	{	
		ret = 0;
	}
	else if(!AssertParam(MASK_PARAM,mask))
	{	
		ret = 0;
	}
	else if(!AssertParam(GW_PARAM,gw))
	{	
		ret = 0;
	}
	
	return ret;
}

/*****************************************************************************************************
 * @fn      SetNetWork
 *
 * @brief   configuration network address
 *
 * @param   mac-> mac address  ip-> ip address  mask-> mask address  gw-> gw address
 *			
 * @return  1--ok 0--fail
 *******************************************************************************************************/
unsigned char SetNetWork( char* mac, char* ip, char* mask, char* gw )	//czn_20190604
{
	FILE* file = NULL;
	//FILE* file_tem = NULL;
	char* ptr  = NULL;
	char line[100] = {'0'};
	unsigned char ret = 0;
	unsigned char len = sizeof( line );
	
	

	//if( mac == NULL && ip == NULL && mask == NULL && gw == NULL )
	//	goto err;

	// read-only file   	// write file
	#if 0
	if( ((file=fopen(ROUTE_PATH,"r"))==NULL)  || ((file_tem=fopen(ROUTE_TEMP_PATH,"w+"))==NULL) )
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		goto err;
	}
	#endif
	
		
		
	if( AssertParam(MAC_PARAM,mac) )
	{	
		API_Event_IoServer_InnerWrite_All(MacAddress, mac);
		//format
		//fprintf( file, "/sbin/ifconfig eth0 hw ether %s #mac\n", mac);
	}
	
// find ip item

	// check parameter
	if( AssertParam(IP_PARAM,ip) )
	{	
		API_Event_IoServer_InnerWrite_All(IP_Address, ip);
		//fprintf( file, "/sbin/ifconfig eth0 %s #ip\n", ip);
		//dprintf("SetNetWork write ip_address string = %s ------------\n",ip);
	}
	

	// check parameter
	if( AssertParam(MASK_PARAM,mask) )
	{			
		API_Event_IoServer_InnerWrite_All(SubnetMask, mask);
		//fprintf( file, "/sbin/ifconfig eth0 netmask %s #netmask\n", mask);
		//dprintf("SetNetWork write ip_address string = %s ------------\n",ip);
		
	}
	
			// check parameter
			
		
	if( AssertParam(GW_PARAM,gw) )
	{	
		API_Event_IoServer_InnerWrite_All(DefaultRoute, gw);
		//fprintf( file, "/sbin/route add default gw %s dev eth0 #gateway\n", gw);		
	}
	
	

	
	bprintf("------SetNetWork is ok ---%s %s\n",__DATE__,__TIME__);
	
	return ret;
}

/*****************************************************************************************************
 * @fn      ResetNetWork
 *
 * @brief   none
 *
 * @param   none
 *			
 * @return  1--ok 0--fail
 *******************************************************************************************************/
unsigned char ResetNetWork( void )
{
	FILE* file   = NULL;
	char line[100];
	// run route.sh 
	//bprintf("---------ResetNetWork ---\n");
	if( (file=fopen(ROUTE_TEMP,"w"))==NULL)
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}
	
	//fchmod(file,0777);
	fprintf( file, "#!/bin/sh\n");
	fprintf( file, "/sbin/ifconfig eth0 down\n");
		
		
	
	API_Event_IoServer_InnerRead_All(MacAddress, line);
	fprintf( file, "/sbin/ifconfig eth0 hw ether %s #mac\n", line);
	
// find ip item

	// check parameter
	
	API_Event_IoServer_InnerRead_All(IP_Address, line);
	fprintf( file, "/sbin/ifconfig eth0 %s #ip\n", line);
	

	// check parameter

	API_Event_IoServer_InnerRead_All(SubnetMask, line);
	fprintf( file, "/sbin/ifconfig eth0 netmask %s #netmask\n", line);
	
			// check parameter
			
	fprintf( file, "/sbin/ifconfig eth0 up\n");	
	
	API_Event_IoServer_InnerRead_All(DefaultRoute, line);
	fprintf( file,  "/sbin/route add default gw %s dev eth0 #gateway\n", line);
	
	fclose( file );
	chmod(ROUTE_TEMP, S_IRUSR|S_IWUSR|S_IXUSR);
	if( (file = popen(ROUTE_TEMP, "r")) == NULL )
	{
		eprintf( "ReSetNetWork error:%s\n",strerror(errno) );
		return 0;
	}

	pclose( file );
	//remove(ROUTE_TEMP);
	file= NULL;
	return 1;
}

#define ROUTE_PATH1			"/mnt/nand1-1/route.sh"
//#define ROUTE_BAK_PATH		"/mnt/nand1-1/route.bak"
#define ROUTE_TEMP_PATH1		"/mnt/nand1-1/route.temp"
unsigned char SetNetWork_new( char* mac, char* ip, char* mask, char* gw )
{
	FILE* file   = NULL;
	char cmd[100];
	int ret=0;
	
	if( (file=fopen(ROUTE_TEMP_PATH1,"w"))==NULL)
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}
	
	//fchmod(file,0777);
	fprintf( file, "#!/bin/sh\n");
	fprintf( file, "/sbin/ifconfig eth0 down\n");
		
		
	
	//API_Event_IoServer_InnerRead_All(StaticIPMac, line);
	if( AssertParam(MAC_PARAM,mac))
		fprintf( file, "/sbin/ifconfig eth0 hw ether %s #mac\n", mac);
	else
		ret=-1;

	if( AssertParam(IP_PARAM,ip))
		fprintf( file, "/sbin/ifconfig eth0 %s #ip\n", ip);
	else
		ret =-1;
	

	// check parameter
	if( AssertParam(MASK_PARAM,mask))
		fprintf( file, "/sbin/ifconfig eth0 netmask %s #netmask\n", mask);
	else
		ret =-1;

			
	fprintf( file, "/sbin/ifconfig eth0 up\n");	

	if( AssertParam(GW_PARAM,gw))
		fprintf( file,  "/sbin/route add default gw %s dev eth0 #gateway\n", gw);
	else
		ret =-1;
	
	fclose( file );
	
	if(ret==0)
	{
		sprintf(cmd,"mv %s %s",ROUTE_TEMP_PATH1,ROUTE_PATH1);
		system(cmd);
		chmod(ROUTE_PATH1, S_IRUSR|S_IWUSR|S_IXUSR);
		sync();

		
	}
	FILE* fd   = NULL;
	if( (fd = popen(ROUTE_PATH1, "r")) == NULL )
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}

	pclose( fd );
	fd = NULL;
	
	return ret;
	
}
int NodeIdToIp(unsigned char nodeId)
{
	return (GetLocalIpByDevice(NET_ETH0)&0x00ffffff) + (nodeId<<24);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

