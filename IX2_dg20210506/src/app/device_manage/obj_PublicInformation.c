/**
  ******************************************************************************
  * @file    obj_PublicInformation.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include "obj_PublicInformation.h"
#include "define_file.h"
#include "utility.h"

static PUB_INFO_RUN_S publicInfo = {.info = NULL, .lock = PTHREAD_MUTEX_INITIALIZER};

char* TimeToString(int time);

static void PublicInfo_Lock(void)
{
    pthread_mutex_lock(&publicInfo.lock);
}

static void PublicInfo_Unlock(void)
{
    pthread_mutex_unlock(&publicInfo.lock);
}

long GetSystemBootTime(void)
{
	struct sysinfo info;
	
	if (sysinfo(&info)) 
	{
		fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n", errno, strerror(errno));
		return -1;
	}

	return info.uptime;
}

int API_LoadPublicInfoFile(const char* filePath)
{
    PublicInfo_Lock();

    publicInfo.info = GetJsonFromFile(filePath);
    if(publicInfo.info == NULL)
    {
        dprintf("%s error!!!!\n", filePath);
    }

    if(publicInfo.info == NULL)
    {
        publicInfo.info = cJSON_CreateObject();
    }
    PublicInfo_Unlock();
	API_PublicInfo_Write_String(PB_SYS_UP_TIME, TimeToString(GetSystemBootTime()));
    return 1;
}

char* TimeToString(int time)
{
	static char display[100];
	snprintf(display, 100, "%d:%02d:%02d", time/3600, (time%3600)/60, time%60);
	return display;
}

int UpdatePB_Process(const cJSON* pbValue, const char* key);

cJSON * API_PublicInfo_Read(const char* key)
{
	cJSON * ret = NULL;
	char* systemStateString;
	cJSON * parent = NULL;

    PublicInfo_Lock();
	if(key && key[0])
	{
		ret = MyCjson_GetObjectItem(publicInfo.info, key);
	}
	else
	{
		ret = publicInfo.info;
	}

	if(ret)
	{
		//如果PB在更新范围内，则更新
		if(UpdatePB_Process(ret, key))
		{
			ret = NULL;
		}

		//如果读取的内容中包含PB_System_State，则更新内容
		if(systemStateString = MyCjson_GetObjectString(ret, PB_System_State))
		{
			//不在启动中
			if(strcmp(systemStateString, "Starting"))
			{
				char* callServerStateString = MyCjson_GetObjectString(publicInfo.info, PB_CALL_SER_STATE);
				if(callServerStateString != NULL)
				{
					parent = MyCjson_GetObjectParent(publicInfo.info, PB_System_State);
					//在待机状态下
					if(!strcmp(callServerStateString, "Wait"))
					{
						cJSON_ReplaceItemInObjectCaseSensitive(parent, PB_System_State, cJSON_CreateString("Standby"));
					}
					else
					{
						cJSON_ReplaceItemInObjectCaseSensitive(parent, PB_System_State, cJSON_CreateString("Calling"));
					}
					if(key != NULL && !strcmp(key, PB_System_State))
					{
						ret = NULL;
					}
				}
			}
		}

		if(ret == NULL)
		{
			ret = MyCjson_GetObjectItem(publicInfo.info, key);
		}
	}


    PublicInfo_Unlock();
    return ret;
}

int API_PublicInfo_Read_Int(const char* key)
{
	int value = 0;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsNumber(ret))
	{
		value = ret->valuedouble;
	}

    return value;
}

const char* API_PublicInfo_Read_String(const char* key)
{
	char* value = NULL;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsString(ret))
	{
		value = cJSON_GetStringValue(ret);
	}

    return value;
}

PublicInfo_bool API_PublicInfo_Read_Bool(const char* key)
{
	PublicInfo_bool value = 0;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsTrue(ret))
	{
		value = 1;
	}

    return value;
}

int API_PublicInfo_Write(const char* key, const cJSON* value)
{
	cJSON * parent = NULL;

	if(value == NULL || key == NULL)
	{
		return 0;
	}

    PublicInfo_Lock();

	parent = MyCjson_GetObjectParent(publicInfo.info, key);

	if(parent == NULL)
	{
		cJSON_AddItemToObject(publicInfo.info, key, cJSON_Duplicate(value, 1));
	}
	else
	{
        cJSON_ReplaceItemInObjectCaseSensitive(parent, key, cJSON_Duplicate(value, 1));
	}
    PublicInfo_Unlock();

    return 1;
}

int API_PublicInfo_Write_Int(const char* key, int value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateNumber(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}
int API_PublicInfo_Write_String(const char* key, const char* value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateString(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}
int API_PublicInfo_Write_Bool(const char* key, PublicInfo_bool value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateBool(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}


typedef cJSON* (*CREATE_PB)(void);
typedef struct
{	
	char*		pbName;		//PB名字
	CREATE_PB	createPb;	//即时生成PB值
}UPDATE_PB_S;

static cJSON* CreatePB_UP_TIME(void)
{
    return cJSON_CreateString(TimeToString(GetSystemBootTime()));
}

static cJSON* CreatePB_SDcardCapacity(void)
{
   	char buffer[100];
	snprintf(buffer, 100, "%.2f MB", (GetDirFreeSizeInMByte(DISK_SDCARD)));
    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_NandCapacity(void)
{
   	char buffer[100];
	snprintf(buffer, 100, "%.2f MB", (GetDirFreeSizeInMByte("/mnt/")));
    return cJSON_CreateString(buffer);
}
static cJSON* CreatePB_DATE(void)
{
    time_t rawtime;
    struct tm info;
   	char buffer[100];

    time(&rawtime);
    localtime_r(&rawtime,&info);
	strftime(buffer, 20, "%Y%m%d", &info);
    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_TIME(void)
{
    time_t rawtime;
    struct tm info;
   	char buffer[100];

    time(&rawtime);
    localtime_r(&rawtime,&info);
	strftime(buffer, 20, "%H%M", &info);


    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_SYS_MemAvailable(void)
{
	char buff[500];
	char memAvailable[100] = {0};
	char *pos1;
	FILE *pf=fopen("/proc/meminfo","r");
	while(fgets(buff,500,pf)!=NULL)
	{
		if(strstr(buff,"MemAvailable:"))
		{
	        sscanf(buff, "%*s %[0-9] kB", memAvailable);
			strcat(memAvailable, " kB");
			break;
		}
	}
	fclose(pf);

	return cJSON_CreateString(memAvailable);
}

UPDATE_PB_S UPDATE_PB_TAB[] = {
	{PB_UP_TIME, CreatePB_UP_TIME},
	{PB_SDcardCapacity, CreatePB_SDcardCapacity},
	{PB_NandCapacity, CreatePB_NandCapacity},
	{PB_DATE, CreatePB_DATE},
	{PB_TIME, CreatePB_TIME},
	{PB_SYS_MemAvailable, CreatePB_SYS_MemAvailable},
};
const int UPDATE_PB_TAB_CNT = sizeof(UPDATE_PB_TAB)/sizeof(UPDATE_PB_TAB[0]);

//返回：1 -- 有更新， 0 -- 无更新
int UpdatePB_Process(const cJSON* pbValue, const char* key)
{
	int index;
	int ret = 0;
	cJSON * parent = NULL;

	for(index = 0; index < UPDATE_PB_TAB_CNT; index++)
	{
		//如果读取的内容中包含pbName，则更新内容
		if(MyCjson_GetObjectItem(pbValue, UPDATE_PB_TAB[index].pbName))
		{
			parent = MyCjson_GetObjectParent(publicInfo.info, UPDATE_PB_TAB[index].pbName);
			cJSON_ReplaceItemInObjectCaseSensitive(parent, UPDATE_PB_TAB[index].pbName, UPDATE_PB_TAB[index].createPb());
			if(key != NULL && !strcmp(key, UPDATE_PB_TAB[index].pbName))
			{
				ret = 1;
				break;
			}
		}
	}

	return ret;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

