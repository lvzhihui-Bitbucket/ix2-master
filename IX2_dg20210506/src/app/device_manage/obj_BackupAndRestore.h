/**
  ******************************************************************************
  * @file    obj_BackupAndRestore.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_BackupAndRestore_H
#define _obj_BackupAndRestore_H
#include "obj_TableProcess.h"


// Define Object Property-------------------------------------------------------
#define PARA_ID							"paraId"
#define VALUE							"value"
#define MAX_IO_DATA_VALUE_LEN			60

#define BACKUP_MAX_CNT					6

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	paraId[4+1];						//参数ID
	char	value[MAX_IO_DATA_VALUE_LEN+1];		//参数值
} IoDataBackup_T;

typedef struct
{
	int		cnt;	
	char	name[BACKUP_MAX_CNT][60];		//参数值
} Backup_FILE_NAME_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------


// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetIoDataBackupRecordItems(one_vtk_table* table, int index,  IoDataBackup_T* record);


//取记录数量
int GetIoDataBackupRecordCnt(one_vtk_table* table);


//修改的一条记录值
void IoDataBackupModifyRecordCValue(one_vtk_table* table, int index, const char *pValue);


//保存列表到文件
int SaveIoDataBackupTableToFile(one_vtk_table* table, const char* fileName);


int BackupIoDataBackupTable(one_vtk_table* table, const char* fileName);


int RestoreIoDataBackupTable(const char* fileName);


// Define Object Function - Private---------------------------------------------




#endif


