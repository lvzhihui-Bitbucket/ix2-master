/**
  ******************************************************************************
  * @file    obj_Stm8HeartBeatService.h
  * @author  lyx
  * @version V00.01.00
  * @date    2017.7.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Stm8HeartBeatService_H
#define _obj_Stm8HeartBeatService_H

#include "utility.h"

// Define Object Property-------------------------------------------------------

#define 	SEND_HEART_BEAT_TIME         60	//每15秒发送一次心跳

typedef struct
{
	OS_TIMER		HBtimer;
	uint16 			sendTimeCnt;		//发送计时
	pthread_mutex_t	lock;			//并发处理数据保护锁
	uint16			ThreadHeartbeat_Age;		
}Stm8HeartBeat_t;

// Define Object Function - Public----------------------------------------------
//心跳服务对象初始化
void Stm8HeartBeatServiceInit(void);

// Define Object Function - Private---------------------------------------------
//心跳服务定时器处理函数
void Stm8HeartBeatServiceTimerCallback(void);

//发送心跳服务包
void SendHeartBeatToStm8(void);

#endif


