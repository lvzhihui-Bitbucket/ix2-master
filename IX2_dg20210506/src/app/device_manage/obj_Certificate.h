/**
  ******************************************************************************
  * @file    obj_Certificate.h
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Certificate_H_
#define _obj_Certificate_H_

#include "utility.h"

//生成dcfg文件，name目前只支持"CERT_ETH0"
int API_Create_DCFG_SELF(const char* name, ...);

//dcfg文件转换为Cert文件
int API_Create_CERT(const char* dcfgName);

//返回-1，Go_Live失败； 0，无文件或者文件错误； 1，Go_Live成功
int API_Go_Live(void);

//删除Cert文件，写PB状态，清除定时检测
int API_Delete_CERT(void);

//返回Go live的结果；启动定时检测环境是否变化
int API_Check_CERT(void);

#endif



