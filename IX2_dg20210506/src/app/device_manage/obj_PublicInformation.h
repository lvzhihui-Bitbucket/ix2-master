/**
  ******************************************************************************
  * @file    obj_PublicInformation.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_PublicInformation_H
#define _obj_PublicInformation_H

#include "cJSON.h"
#include <pthread.h>

// Define Object Property-------------------------------------------------------

#define PB_Identifier                      "PB"

#if defined(PID_821DX)
    #define MY_DEV_NAME                      "DX821"
    #define MY_DEV_MODEL                     "DX-DS"
#elif defined(PID_IX611)
    #define MY_DEV_NAME                      "IX611"
    #define MY_DEV_MODEL                     "IX-DS"
#elif defined(PID_IX622)
    #define MY_DEV_NAME                      "IX622"
    #define MY_DEV_MODEL                     "IX-DS"
#elif defined(PID_IXG)
  #define MY_DEV_NAME                      "IXG"
  #define MY_DEV_MODEL                     "IXG"
#elif defined(PID_IX482)
  #define MY_DEV_NAME                      "IX482SE"
  #define MY_DEV_MODEL                     "IX-IM"
#elif defined(PID_IX850)
  #define MY_DEV_NAME                      "IX850S"
  #define MY_DEV_MODEL                     "IX-IM"
#else
    #define MY_DEV_NAME                      "IX471S"
    #define MY_DEV_MODEL                     "IX-IM"
#endif


#define PB_MFG_SN_INFO                  "MFG_SN-Info"
    #define PB_MFG_SN                       "MFG_SN"
    #define PB_MFG_SN_SOURCE                "MFG_SN.Source"
    #define PB_MFG_SN_LICENSE               "MFG_SN.License"

#define PB_FM_VER_INFO                  "FM_VER_INFO"
    #define PB_HD_VER                       "HD_VER"
    #define PB_FW_VER                       "FW_VER"
    #define PB_MCU_VER                      "MCU_VER"

#define PB_CALL_Nbr                     "CALL_Nbr"
    #define PB_IX_ADDR                       "IX_ADDR"
    #define PB_IX_NAME                       "IX_NAME"
    #define PB_G_NBR                         "G_NBR"
    #define PB_L_NBR                         "L_NBR"

#define PB_DEV_INFO                     "DEV_INFO"
    #define PB_DEV_NAME                      "DEV_NAME"
    #define PB_DEV_MODEL                     "DEV_MODEL"
    #define PB_CUS_DEV_NAME                  "CUS_DEV_NAME"
    #define PB_CUS_DEV_MODEL                 "CUS_DEV_MODEL"

#define PB_DATE_TIME_INFO               "DATE_TIME_INFO"
    #define PB_DATE                         "DATE"
    #define PB_TIME                         "TIME"
    #define PB_UP_TIME                      "UP_TIME"
    #define PB_SYS_UP_TIME                  "SYS_UP_TIME"
    #define PB_APP_UP_TIME                  "APP_UP_TIME"

#define PB_LAN_STATE                    "LAN_STATE"
    #define PB_SUB_LAN_STATE                  "LAN-STATE"
    #define PB_LAN_POLICY                     "LAN-IP-Policy"
    #define PB_LAN_IP                         "LAN-IP_Address"
    #define PB_LAN_MASK                       "LAN-SubnetMask"
    #define PB_LAN_GATEWAY                    "LAN-DefaultRoute"
    #define PB_LAN_MAC                        "LAN-MACAddr"

#define PB_WLAN_STATE                   "WLAN_STATE"
    #define PB_SUB_WLAN_STATE                 "WLAN-STATE"
    #define PB_WLAN_SW                        "WLAN-SW"
    #define PB_WIFI_STATE                     "WIFI_STATE"
    #define PB_WIFI_SSID                      "WIFI_SSID"
    #define PB_WLAN_IP                        "WLAN-IP_Address"
    #define PB_WLAN_MASK                      "WLAN-SubnetMask"
    #define PB_WLAN_GATEWAY                   "WLAN-DefaultRoute"
    #define PB_WLAN_MAC                       "WLAN-MACAddr"

#define PB_MY_STATE                      "MY_STATE"
    #define PB_LOCK1_STATE                      "LOCK1_STATE"
    #define PB_LOCK2_STATE                      "LOCK2_STATE"
    #define PB_DEBUG_STATE                      "DEBUG_STATE"
    #define PB_DT_LINK_STATE                    "DT_LINK_STATE"
	#define PB_DT_LINK_TYPE                    "DT_LINK_TYPE"
    #define PB_XD_STATE                         "XD_STATE"
    #define PB_CHECK_SIP_STATE                  "CHECK_SIP_STATE" 
    #define PB_CHECK_XD_STATE                   "CHECK_XD_STATE" 
    #define PB_CONNECT_PHONE_STATE              "CONNECT_PHONE_STATE" 
    #define PB_SIP_CONNECT_NETWORK              "SIP_CONNECT_NETWORK" 
    #define PB_IPC_AUTO_CONFIG                  "IPC_AUTO_CONFIG" 
    #define PB_System_State                     "System_State" 
    #define PB_StartUp5MinutesLater             "StartUp5MinutesLater" 
	#define PB_CmrDevState             		    "CmrDevState" 
    #define PB_XD2_STATE	                    "XD2_State"
    #define PB_CERT_STATE                       "CERT_State"
    #define PB_CERT_ERROR_STATE                 "CERT_ErrorState"
    #define PB_NDM_C_STATE                      "NDM_C_STATE"
    #define PB_NDM_C_RESULT                     "NDM_C_RESULT"    
    #define PB_NDM_S_STATE                      "NDM_S_STATE"
    #define PB_SDcardCapacity                   "SDcardCapacity"
    #define PB_NandCapacity                     "NandCapacity"
    #define PB_SYS_MemAvailable                 "SYS_MemAvailable"
    #define PB_TlogIX_ADDR                      "TlogIX_ADDR"
    #define PB_TlogServerState                  "TlogServerState"

#define PB_SEARCH_SERVER_STATE            "SEARCH_SERVER_STATE"
    #define PB_SEARCH_SERVER_TYPE             "SEARCH_SERVER_TYPE"
    #define PB_SEARCH_SERVER_IP               "SEARCH_SERVER_IP"
    #define PB_SEARCH_LIST_MD5                "SEARCH_LIST_MD5"
    #define PB_SEARCH_LIST_SIZE               "SEARCH_LIST_SIZE"
    #define PB_SEARCH_LIST_TIME               "SEARCH_LIST_TIME"

#define PB_CARD_INFO                      "CARD_INFO"
    #define PB_MasterAdd                  "MasterAdd" 
    #define PB_MasterDel                  "MasterDel"
    #define PB_UserCards                  "UserCards" 
    #define PB_CardSetup_State            "CardSetup_State" 

#define PB_MDS_MODULS                   "MDS_MODULS"
#define PB_BC-CALL                      "BC-CALL"
#define PB_BC_MON                       "BC_MON"
#define PB_OnlineMonitors               "OnlineMonitors"

#define PB_CALL_INFO			"Call_Info"
	#define PB_CALL_ID			"Call_ID"
	#define PB_CALL_SER_STATE		"CallServerState"
	#define PB_CALL_PART			"Call_Part"
	#define PB_CALL_SIP			"Call_SIP"
	#define PB_CALL_SOURCE			"Call_Source"
			#define PB_CALL_SRC_IxDevNum			"DeviceNbr"	//"IxDevNum"
	#define PB_CALL_TARGET			"Call_Target"
		#define PB_CALL_TAR_NAME			"Call_Tar_Name"
		#define PB_CALL_TAR_DTADDR		"Call_Tar_DtAddr"
		#define PB_CALL_TAR_DT_LOGIC_ADDR		"Call_Tar_DtLogicAddr"
		#define PB_CALL_TAR_SIPACC			"Call_Tar_SipAcc"
		#define PB_CALL_TAR_INPUT			"Call_Tar_Input"
		#define PB_CALL_TAR_IXADDR			"Call_Tar_IXAddr"
		#define PB_CALL_TAR_IxDev			"Call_Tar_IxDev"
			#define PB_CALL_TAR_IxDevNum			IX2V_IX_ADDR//"DeviceNbr"	//"IxDevNum"
			#define PB_CALL_TAR_IxDevIP			IX2V_IP_ADDR//"IP_Addr"		//"IxDevIP"
		#define PB_CALL_TAR_IxHook			"Call_Tar_IxHook"
	#define PB_CALL_DETAIL			"Call_Detail"
	#define PB_CALL_DTCALLER			"Call_DtCaller"
		#define PB_CALL_DTCALLER_STATE			"Call_DtCallerState"
		#define PB_CALL_DTCALLER_TYPE				"Call_DtCallerType"
	#define PB_CALL_SIPCALLER			"Call_SipCaller"
		#define PB_CALL_SIPCALLER_STATE			"Call_SipCallerState"
		#define PB_CALL_SIPCALLER_TYPE				"Call_SipCallerType"
	#define PB_CALL_IPCALLER			"Call_IPCaller"
		#define PB_CALL_IPCALLER_STATE			"Call_IPCallerState"
		#define PB_CALL_IPCALLER_TYPE				"Call_IPCallerType"
	#define PB_CALL_IPBECALLED						"Call_IPBecalled"
		#define PB_CALL_IPBECALLED_STATE			"Call_IPBecalledState"
		#define PB_CALL_IPBECALLED_TYPE			"Call_IPBecalledType"
		
#define PB_SystemStat	                        "SystemStat"

#define PB_DT_IM_Search	                        "DT_IM_Search"
    #define PB_InSearching	                        "InSearching"
    #define PB_LastSearchDate	                    "LastSearchDate"
    #define PB_LastSearchResult	                    "LastSearchResult"

// lzh_20230306
#define PB_IXG_ADDR_INT_DATA			"IXG_ADDR_INT_DATA"
// lzh_20230306

#define PB_MISS_CALL_STATE			"MissCallState"
#define PB_MISS_CALL_SOURCE			"MissCallSource"
#define PB_MISS_CALL_TIME			"MissCallTime"
#define PB_NODISTURB_STATE			"NoDisturbState"
#define PB_RADAR_MODULE					"RADAR_MODULE"
#define PB_VIDEO_CLIENT					"VIDEO_CLIENT"
#define PB_MENU_THEME_MODE			"MenuThemeMode"
#define PB_MEM_LIMIT_SIZE			"MemLimitSize"

#define PB_IXGRM_SCREEN_SIZE			"IXGRM_SCREEN_SIZE"

typedef int PublicInfo_bool;

typedef struct
{
    cJSON *info;
	  pthread_mutex_t	lock;
}PUB_INFO_RUN_S;

int API_PublicInfo_Write(const char* key, const cJSON* value);
int API_PublicInfo_Write_Int(const char* key, int value);
int API_PublicInfo_Write_String(const char* key, const char* value);
int API_PublicInfo_Write_Bool(const char* key, PublicInfo_bool value);

cJSON * API_PublicInfo_Read(const char* key);
int API_PublicInfo_Read_Int(const char* key);
const char* API_PublicInfo_Read_String(const char* key);
PublicInfo_bool API_PublicInfo_Read_Bool(const char* key);

#endif


