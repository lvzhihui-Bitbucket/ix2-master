/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_SYS_VER_INFO_H
#define _obj_SYS_VER_INFO_H

#include "task_survey.h"
#include "obj_GetDeviceTypeAndArea.h"
#include "obj_WiFi_State.h"

// Define Object Property-------------------------------------------------------
#pragma pack(1)

typedef struct 
{
	char 	type[10];		//设备类型
	
	char 	swVer[60];		//软件版本
	uint16 	id;				//NodeId
	char 	sn[13];			//序列号
	
	char 	ip[16];			//IP
	char 	mac[18];		//MAC
	char 	mask[16];		//MASK
	char 	gw[16];			//GateWay
	
	char 	bd[5];			//bd
	char 	rm[5];			//rm
	char 	ms[3];			//ms
	char	bd_rm_ms[11];	//bd_rm_ms
	char	myName[41];		//my name
	char	GLOBAL_NUM[11]; //GLOBAL_NUM
	char	LOCAL_NUM[7];	//LOCAL_NUM
	int		systemType;		// 1系统型，0单户型
	Type_e	myDeviceType;	//设备类型
	char 	deviceModel[20];//设备模式

	char 	wlan_ip[16];		//IP
	char 	wlan_mac[18];		//MAC
	char 	wlan_mask[16];		//MASK
	char 	wlan_gw[16];		//GateWay

	int		net_mode;
	int 		lan_link;
	int 		lan_act_type;
	int		wlan_en;
	int		wlan_state;
	int 		wifi_signal_level;
	int		wlan_cur_ssid[WIFI_SSID_LENGTH+1];
	pthread_mutex_t	lock;	// 并发处理数据保护锁
	
} SYS_VER_INFO_T;
#pragma pack()

enum
{
	LAN_ACT_UNKOWN=0,
	LAN_ACT_DHCP,
	LAN_ACT_AUTOIP,
	LAN_ACT_STATIC,
};

enum
{
	WLAN_ACT_DISCONNECT=0,
	WLAN_ACT_CONNECTING,
	WLAN_ACT_CONNECTED,
};
// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------
void SysVerInfoInit(uint8 updateIoEnable);
SYS_VER_INFO_T GetSysVerInfo(void);
void SetSysVerInfo(SYS_VER_INFO_T data);
Type_e GetSysVerInfo_MyDeviceType(void);

void OutputSysVerInfo(void);
// lzh_20170420_s
uint16 GetSysVerInfo_IP_NODE_ID(void);
// lzh_20170420_e


#endif


