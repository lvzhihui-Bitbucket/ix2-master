/**
  ******************************************************************************
  * @file    obj_InputDecode.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_InputDecode.h"


//得到一个指定字符串的房号：
// paras：
// input_str: 输入字符串
// bd_rm_ms: 输出房号
// -1/不能拆解, 0/拆解
int GetBdRmMsByInput( const char* input_str,  char* bd_rm_ms)
{
	int inputLen;
	int i;
	int ret;

	inputLen = strlen(input_str);

	//输入中有字母则不能拆解
	for(i = 0; i<inputLen; i++)
	{
		if(input_str[i] < '0' || input_str[i] > '9')
		{
			return -1;
		}
	}
	
	strcpy(bd_rm_ms, "0000000000");

	//10位数字，按4-4-2拆分10位房号
	if(inputLen == 10)
	{
		strcpy(bd_rm_ms, input_str);
	}
	//9位数字，前补0为10位数字，按4-4-2拆分10位房号
	else if(inputLen == 9)
	{
		strcpy(bd_rm_ms+1, input_str);
	}
	//8位数字，则为8位的BD&RM_Nbr
	else if(inputLen == 8)
	{
		memcpy(bd_rm_ms, input_str, 8);
	}
	//5~7位数字，前补0，得到8位的BD&RM_Nbr
	else if(inputLen >= 5 && inputLen <= 7)
	{
		memcpy(bd_rm_ms+(8-inputLen), input_str, inputLen);
	}
	//1~4位的数字，认为是本单元呼叫，自行添加BD_Nbr.
	else if(inputLen >= 1 && inputLen <= 4)
	{
		memcpy(bd_rm_ms, GetSysVerInfo_bd(), 4);
		memcpy(bd_rm_ms+(8-inputLen), input_str, inputLen);
	}
	else
	{
		return -1;
	}

	return 0;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

