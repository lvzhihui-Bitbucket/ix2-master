/**
  ******************************************************************************
  * @file    obj_IPHeartBeatService.c
  * @author  czb
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "obj_ThreadHeartBeatService.h"
#include "task_VideoMenu.h"
#include "obj_Stm8HeartBeatService.h"

//#define Linphone_Heartbeat_Mask		0x01
//#define AllThread_Heartbeat_Mask	Linphone_Heartbeat_Mask
void HardwareRestar(void);


pthread_mutex_t ThreadHeartbeat_Lock = PTHREAD_MUTEX_INITIALIZER;

static int heart_err_cnt=0;
static int sendTimeCnt=0;
static OS_TIMER ThreadHeartbeat_timer;


ThreadHeartbeat_Callback_Def  ThreadHeartbeat_Callback = NULL;
int ThreadHeartbeat_Mask = 0;

void SendAllThreadHeartbeatApply(ThreadHeartbeat_Callback_Def Finish_Callback)
{
	printf("SendAllThreadHeartbeatApply !!!!!!!!!!!!!\n");
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	
	API_linphonec_Apply_Heartbeat();
	#if defined(PID_IX850)

	#else
	API_MenuThreadHeartbeatReq();
	#endif
	API_CallserverThreadHeartbeatReq();
	API_Survey_ThreadHeartbeatReq();
	
	ThreadHeartbeat_Mask = AllThread_Heartbeat_Mask;
	ThreadHeartbeat_Callback = Finish_Callback;
	#if defined(PID_IX850)
	
	#else
	if(GetCurMenuCnt()!=MENU_001_MAIN&&GetCurMenuCnt()!=MENU_027_CALLING2&&GetCurMenuCnt()!=MENU_013_CALLING&&
		GetCurMenuCnt()!=MENU_028_MONITOR2&&GetCurMenuCnt()!=MENU_056_IPC_MONITOR
		&&GetCurMenuCnt()!=MENU_026_PLAYBACK)
		ThreadHeartbeat_Mask &=(~Menu_Heartbeat_Mask);
	#endif
	
	pthread_mutex_unlock(&ThreadHeartbeat_Lock);
}

void RecvThreadHeartbeatReply(int Mask)
{
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	
	ThreadHeartbeat_Mask &= (~Mask);
	if(ThreadHeartbeat_Mask == 0)
	{
		#if !defined(PID_IXSE)
		if(ThreadHeartbeat_Callback != NULL)
		{
			(*ThreadHeartbeat_Callback)();
			printf("RecvThreadHeartbeatReply Finish!!!!!!!!!!!!!\n");
			ThreadHeartbeat_Callback = NULL;
		}
		#else
		heart_err_cnt=0;
		ak_drv_wdt_feed();
		#endif
	}
	pthread_mutex_unlock(&ThreadHeartbeat_Lock);	
}

void ThreadHeartbeat_Callback2(void)
{
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	sendTimeCnt++;
	if(sendTimeCnt >= SEND_HEART_BEAT_TIME ) 
	{
		sendTimeCnt = 0;
		ThreadHeartbeat_Mask = AllThread_Heartbeat_Mask;
		API_MenuThreadHeartbeatReq();
		API_CallserverThreadHeartbeatReq();
		API_Survey_ThreadHeartbeatReq();
		if(GetCurMenuCnt()!=MENU_001_MAIN&&GetCurMenuCnt()!=MENU_027_CALLING2&&GetCurMenuCnt()!=MENU_013_CALLING&&
			GetCurMenuCnt()!=MENU_028_MONITOR2&&GetCurMenuCnt()!=MENU_056_IPC_MONITOR
			&&GetCurMenuCnt()!=MENU_026_PLAYBACK)
			ThreadHeartbeat_Mask &=(~Menu_Heartbeat_Mask);

		if(++heart_err_cnt>=3)
		{
			dprintf("HardwareRestar\n");
			HardwareRestar();
			pthread_mutex_unlock(&ThreadHeartbeat_Lock);
			return;
		}

		auto_reboot_polling();
		InternetTimeAutoUpdate();
		timer_EixtInstaller_callback();
	} 

	NetworkCardCheckTimerCallback();

	video_client_frame_rate_update();

	OS_RetriggerTimer( &ThreadHeartbeat_timer );	

	pthread_mutex_unlock(&ThreadHeartbeat_Lock);
}
void ThreadHeartbeat_Init(void)
{
	heart_err_cnt=0;
	sendTimeCnt = 0;
	ThreadHeartbeat_Mask=0;
	OS_CreateTimer( &ThreadHeartbeat_timer, ThreadHeartbeat_Callback2, 1000/25);
	ak_drv_wdt_open(180);	// range[1s-357s]
	OS_RetriggerTimer( &ThreadHeartbeat_timer );	

	NetworkCardCheckInit();
	auto_reboot_init(HardwareRestar);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

