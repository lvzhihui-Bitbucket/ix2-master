/**
  ******************************************************************************
  * @file    obj_IPHeartBeatService.h
  * @author  cao
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_IPHeartBeatService_H
#define _obj_IPHeartBeatService_H

#include "utility.h"

// Define Object Property-------------------------------------------------------
typedef enum
{
	STATE_SEND_HEART_BEAT,
	STATE_WAIT_HEART_BEAT,
}HB_STATE_TYPE_E;

typedef struct
{
	OS_TIMER		HBtimer;
	HB_STATE_TYPE_E	state;
	int				rebootCnt;
	uint16 			sendCnt;		//发送计时
	uint16 			receiveCnt;		//接收计时
	pthread_mutex_t	lock;			//并发处理数据保护锁
}IPHeartBeat_t;

// Define Object Function - Public----------------------------------------------
//心跳服务对象初始化
void IPHeartBeatServiceInit(void);
//接收到心跳服务包
void ReceiveHeartBeat(void);

// Define Object Function - Private---------------------------------------------
//心跳服务定时器处理函数
void IPHeartBeatServiceTimerCallback(void);
//读取重启次数
int ReadHeartBeatRebootTimes(void);
//写重启次数到文件
void WriteHeartBeatRebootTimes(int times);
//发送心跳服务包
void SendHeartBeat(void);
//设备重启
void HeartBeatServiceDeviceReboot(void);


#endif


