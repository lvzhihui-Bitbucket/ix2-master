/**
  ******************************************************************************
  * @file    obj_GetDviceAddrDispStr.c
  * @author  zeng
  * @version V00.01.00
  * @date    2019.5.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_DeviceManage.h"
#include "obj_SYS_VER_INFO.h"
#include "task_IoServer.h"
#include "obj_GetIpByNumber.h"
#include "obj_GetDeviceTypeAndArea.h"
#include "obj_GetDviceAddrDispStr.h"

static char string1[5] = {0};
static char string2[5] = {0};
static DeviceAddrDispStr_t deviceAddrDispStr;

char* delete_leading_zero(char* str)
{
	int temp;
	temp = atoi(str);
	sprintf(string1,"%d",temp);
	return string1;
}

char* simplified_os_gl_ms(char* str)
{
	int temp;
	temp = atoi(str);
	temp -= 50;
	sprintf(string2,"%d",temp);
	return string2;
}

void init_device_addr_prefix_string(void)
{
	char ioData[5] = {0};

	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_MS, deviceAddrDispStr.MS); // MY
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_COMMON, deviceAddrDispStr.COMMON); // C
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_IM, deviceAddrDispStr.IM); // IM
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_DS, deviceAddrDispStr.DS); // DS
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_OS, deviceAddrDispStr.OS); // OS
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_GL, deviceAddrDispStr.GL); // GL
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_AC, deviceAddrDispStr.AC); // AC

	API_Event_IoServer_InnerRead_All(DEVICE_DISP_SIMPLIFIED, ioData);
	deviceAddrDispStr.dispMode = atoi(ioData);
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_NAME_ONLY, ioData);
	deviceAddrDispStr.nameOnly = atoi(ioData);
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_USE_G_L_NUM, ioData);
	deviceAddrDispStr.globalLocalEnable = atoi(ioData);
}


static void get_device_addr_prefix_string(char* my_bd_rm_ms, char* bd_rm_ms, DeviceTypeAndArea_T type_area, char* p_str_to_put)
{
	if( type_area.area == SameRoom ) // ==bd_rm
	{
		//printf("Same room1\n");
		strcpy(p_str_to_put, deviceAddrDispStr.MS);
		strcat(p_str_to_put," ");
	}
	
	if( type_area.area == CommonArea )
	{
		strcat(p_str_to_put,deviceAddrDispStr.COMMON);
	}

	switch(type_area.type)
	{
		case TYPE_IM:
			strcat(p_str_to_put,deviceAddrDispStr.IM);
			break;
		case TYPE_DS:
			strcat(p_str_to_put,deviceAddrDispStr.DS);
			break;
		case TYPE_OS:
			strcat(p_str_to_put,deviceAddrDispStr.OS);
			break;
		case TYPE_GL:
			strcat(p_str_to_put,deviceAddrDispStr.GL);
			break;
		default:
			strcat(p_str_to_put,deviceAddrDispStr.AC);
			break;
	}
}

static void get_full_addr_disp_string(const char* bd_rm_ms,DeviceTypeAndArea_T type_area, char* p_str_to_put)
{
	if( (type_area.type == TYPE_IM || type_area.type == TYPE_OS || type_area.type == TYPE_DS || type_area.type == TYPE_GL) && (type_area.area == SameBuilding || type_area.area == SameRoom) )
	{
		memcpy(p_str_to_put,bd_rm_ms+4,4); // rm_num
		strcat(p_str_to_put,"(");
		strcat(p_str_to_put,bd_rm_ms+8); // ms_num
		strcat(p_str_to_put,")");
	}
	else
	{
		memcpy(p_str_to_put,bd_rm_ms,8); // bd_rm num
		strcat(p_str_to_put,"(");
		strcat(p_str_to_put,bd_rm_ms+8); // ms_num
		strcat(p_str_to_put,")");
	}
}

static void get_simplifed_addr_disp_string(const char* bd_rm_ms,DeviceTypeAndArea_T type_area, char* p_str_to_put)
{
	char bd[5] = {0};
	char rm[5] = {0};
	char ms[3] = {0};
	memcpy(bd,bd_rm_ms,4);
	memcpy(rm,bd_rm_ms+4,4);
	memcpy(ms,bd_rm_ms+8,2);
	if( type_area.type == TYPE_IM || type_area.type == TYPE_OS )
	{
		switch( type_area.area )
		{
			case SameRoom:
				//printf("Same room2\n");
				strcpy(p_str_to_put,"(");
				if( type_area.type == TYPE_OS )
				{
					strcat(p_str_to_put,delete_leading_zero(simplified_os_gl_ms(ms))); // -50, del 0
				}
				else
				{
					strcat(p_str_to_put,delete_leading_zero(ms)); // del 0
				}
				strcat(p_str_to_put,")");
				break;
			case SameBuilding:
				//printf("Same building\n");
				strcpy(p_str_to_put,delete_leading_zero(rm));
				if( !(type_area.type == TYPE_IM && strcmp(ms,"01") == 0) ) // != master im
				{
					strcat(p_str_to_put,"(");
					if( type_area.type == TYPE_OS )
					{
						strcat(p_str_to_put,delete_leading_zero(simplified_os_gl_ms(ms))); // -50, del 0
					}
					else
					{
						strcat(p_str_to_put,delete_leading_zero(ms)); //  del 0
					}
					strcat(p_str_to_put,")");
				}
				break;
			case OtherBuilding:
				strcat(p_str_to_put,bd);
				strcat(p_str_to_put,rm);
				if( !(type_area.type == TYPE_IM && strcmp(ms,"01") == 0) ) // != master im
				{
					strcat(p_str_to_put,"(");
					if( type_area.type == TYPE_OS )
					{
						strcat(p_str_to_put,delete_leading_zero(simplified_os_gl_ms(ms))); // -50, del 0
					}
					else
					{
						strcat(p_str_to_put,delete_leading_zero(ms)); //  del 0
					}
					strcat(p_str_to_put,")");
				}
				break;
			case CommonArea:
				p_str_to_put[0] = 0;
				break;
		}
	}
	else if( type_area.type == TYPE_DS || type_area.type == TYPE_GL )
	{
		switch( type_area.area )
		{
			case SameRoom:
				p_str_to_put[0] = 0;
				break;
			case SameBuilding:
			case CommonArea:
				strcat(p_str_to_put,"(");
				if( type_area.type == TYPE_GL )
				{
					strcat(p_str_to_put,delete_leading_zero(simplified_os_gl_ms(ms))); // -50, del 0
				}
				else
				{
					strcat(p_str_to_put,delete_leading_zero(ms)); //  del 0
				}
				strcat(p_str_to_put,")");
				break;
			case OtherBuilding:
				strcat(p_str_to_put,bd);
				strcat(p_str_to_put,"(");
				if( type_area.type == TYPE_GL )
				{
					strcat(p_str_to_put,delete_leading_zero(simplified_os_gl_ms(ms))); // -50, del 0
				}
				else
				{
					strcat(p_str_to_put,delete_leading_zero(ms)); //  del 0
				}
				strcat(p_str_to_put,")");
				break;
		}
	}
}

static void get_addr_disp_string(int force_full_disp,const char* bd_rm_ms,DeviceTypeAndArea_T type_area, char* p_str_to_put)
{
	if( force_full_disp || !deviceAddrDispStr.dispMode )
	{
		//printf("full display mode\n");
		get_full_addr_disp_string(bd_rm_ms,type_area,p_str_to_put);
	}
	else
	{
		//printf("simplified display mode\n");
		get_simplifed_addr_disp_string(bd_rm_ms,type_area,p_str_to_put);
	}
}

void get_device_addr_and_name_disp_str(int force_full_disp, const char* bd_rm_ms, const char* global_num, const char* local_num, const char* name, char* p_str_to_put)
{
	DeviceTypeAndArea_T type_area;
	char str1[30] = {0};
	char str2[30] = {0};
	int leadZeroDisp;
	SYS_VER_INFO_T sysInfo;

	p_str_to_put[0] = 0;
	if( deviceAddrDispStr.nameOnly == 1 && !force_full_disp )
	{
		strcpy( p_str_to_put, name );
		return;
	}

	type_area = GetDeviceTypeAndAreaByNumber(bd_rm_ms);

	// zfz_20190605 start
	if( deviceAddrDispStr.globalLocalEnable && !force_full_disp )
	{
		if( type_area.area == SameBuilding || type_area.area == SameRoom )
		{
			if( local_num != NULL )
			{
				strcpy(p_str_to_put,"(");
				strcat(p_str_to_put,local_num);
				strcat(p_str_to_put,")");
				return;
			}
		}
		else
		{
			if( local_num != NULL )
			{
				strcpy(p_str_to_put,"[");
				strcat(p_str_to_put,global_num);
				strcat(p_str_to_put,"]");
				return;
			}
		}		
	}
	// end

	get_device_addr_prefix_string(sysInfo.bd_rm_ms,bd_rm_ms,type_area,str1);		
	get_addr_disp_string(force_full_disp,bd_rm_ms,type_area,str2);
	//printf("prefix string = %s\n",str1);
	//printf("address string = %s\n",str2);
	strcpy(p_str_to_put,str1);
	strcat(p_str_to_put," ");
	strcat(p_str_to_put,str2);
	strcat(p_str_to_put," ");
	//strcat(p_str_to_put,name);
	if(name!=NULL)
	{
		char buff[41];
		//strcpy(buff,name);
		snprintf(buff,40,"%s%s",p_str_to_put,name);
		strcpy(p_str_to_put,buff);
	}
	//printf("prefix+address string = %s\n",p_str_to_put);
	
}
