/**
  ******************************************************************************
  * @file    obj_DeviceManage.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#include "obj_DeviceManage.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "task_IoServer.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_GetInfoByIp.h"
#include "obj_IX_Report.h"
#include "obj_SearchIxProxy.h"
#include "cJSON.h"
#include "define_file.h"

static cJSON *rebootFlagJson = NULL;

int deviceLinkingReportIp;

int deviceRemoteManageFlag = 0;

int GetDeviceRemoteManageFlag(void)
{
	return deviceRemoteManageFlag;
}

int SetDeviceRemoteManageFlag(int flag)
{
	deviceRemoteManageFlag = flag;
}

int DeviceRecoverToDefault(int recover_scene)
{
	struct in_addr temp;
	int ip;
	char ip_str[20] = {0};
	
	if(recover_scene == 0 ||recover_scene == 1 ||recover_scene == 2)
	{
		API_Event_IoServer_Factory_Default();
	}
	
	if(recover_scene == 0 ||recover_scene == 1 ||recover_scene == 2)
	{
		call_record_delete_all();
	}

	if(recover_scene == 1 ||recover_scene == 2)
	{
		API_ResourceTb_ClearWithFail();
	}
	
	if(recover_scene == 2)
	{
		SYS_VER_INFO_T sysInfo;
			
		sysInfo = GetSysVerInfo();
		sysInfo.id = 201;
		
		ip = (inet_addr(sysInfo.ip)&0x00ffffff)|(201<<24);
		snprintf(ip_str, 20, "%03d.%03d.%03d.%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
		
		strcpy(sysInfo.ip, ip_str);
			


		SetSysVerInfo(sysInfo);
	}
	
}


void DeviceManageSingleLinking(UDP_MSG_TYPE *pdata)
{
	DEVICE_SINGLE_LINKING_REQ_T *pRequest = (DEVICE_SINGLE_LINKING_REQ_T*)pdata->pbuf;
	DEVICE_SINGLE_LINKING_RSP_T response;
	
	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		memcpy(&response, pRequest, sizeof(DEVICE_SINGLE_LINKING_REQ_T));
		//本机在线
		if(ntohs(pRequest->subAddr) == 0)
		{
			response.result = 0;
		}
		//检测其它设备
		else
		{
			response.result = 1;
		}
		api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_SINGLE_LINKING_RSP_T));
	}
	
}

void DeviceManageMultipleLinkingStart(UDP_MSG_TYPE *pdata)
{
	DEVICE_MULTIPLE_LINKING_START_REQ_T *pRequest = (DEVICE_MULTIPLE_LINKING_START_REQ_T*)pdata->pbuf;
	DEVICE_MULTIPLE_LINKING_START_RSP_T response;
	uint8 i;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		deviceLinkingReportIp = pdata->target_ip;
		
		//不能接受多设备检测
		response.result = 1;
			
		api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_MULTIPLE_LINKING_START_RSP_T));
	}
}

void DeviceManageMultipleLinkingReport(uint8 deviceNum, DEVICE_LINKING_RESULT_T* pDevice)
{
	DEVICE_MULTIPLE_LINKING_REPORT_REQ_T request;
	DEVICE_MULTIPLE_LINKING_REPORT_RSP_T response;
	uint8 i;
	
	request.IpAddr = GetLocalIpByDevice(GetNetDeviceNameByTargetIp(deviceLinkingReportIp));;
	request.subAddrNum = deviceNum;
	memcpy(&request.subDevice, pDevice, deviceNum);
	for(i = 0; i < deviceNum; i++)
	{
		request.subDevice[i].addr = htons(request.subDevice[i].addr);
	}
	api_udp_c5_ipc_send_req(deviceLinkingReportIp, CMD_DEVIC_MULTIPLE_LINKING_REPORT_REQ, &request, sizeof(DEVICE_MULTIPLE_LINKING_REPORT_REQ_T) , &response, sizeof(DEVICE_MULTIPLE_LINKING_REPORT_RSP_T));
}

int DeviceManageRebootRequest(int ip, DEVICE_REBOOT_REQ_T data, DEVICE_REBOOT_RSP_T* result)
{
	int resultLen = sizeof(DEVICE_REBOOT_RSP_T);
	
	return api_udp_c5_ipc_send_req(ip, CMD_DEVIC_REBOOT_REQ, &data, sizeof(DEVICE_REBOOT_REQ_T) , result, &resultLen);
}


void WriteRebootFlag(int* ip, int cnt)
{
	SearchIxProxyRspData ixProxyData;
	char *string = NULL;
	cJSON *wlanJson = NULL;
	cJSON *lanJson = NULL;
	cJSON *reportJson = NULL;
	int i;
		
	if(rebootFlagJson == NULL)
	{
		rebootFlagJson = GetJsonFromFile(REBOOT_FLAG_PATH);
		if(rebootFlagJson == NULL)
		{
			rebootFlagJson = cJSON_CreateObject();
			cJSON_AddItemToObject(rebootFlagJson, NET_WLAN0, wlanJson = cJSON_CreateArray());
			cJSON_AddItemToObject(rebootFlagJson, NET_ETH0, lanJson = cJSON_CreateArray());
		}
	}
	else
	{
		wlanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_WLAN0);
		lanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_ETH0);
	}

	if(cnt == 0 || ip == NULL)
	{
		API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
		if(ixProxyData.deviceCnt == 0)
		{
			dprintf("ixProxyData.deviceCnt=%d\n", ixProxyData.deviceCnt);
			return;
		}
		else
		{
			cnt = ixProxyData.deviceCnt;
			dprintf("ixProxyData.deviceCnt=%d\n", ixProxyData.deviceCnt);
			for(i = 0; i < cnt; i++)
			{
				reportJson = (strcmp(GetNetDeviceNameByTargetIp(inet_addr(ixProxyData.data[i].ip_addr)), NET_ETH0) ? wlanJson : lanJson);
				cJSON_AddItemToArray(reportJson, cJSON_CreateString(ixProxyData.data[i].ip_addr));
				dprintf("ixProxyData.data[%d].ip_addr=%s\n", i, ixProxyData.data[i].ip_addr);
			}
		}
	}
	else
	{
		cnt = (cnt > 10 ? 10 : cnt);
		
		for(i = 0; i < cnt; i++)
		{
			reportJson = (strcmp(GetNetDeviceNameByTargetIp(ip[i]), NET_ETH0) ? wlanJson : lanJson);
			cJSON_AddItemToArray(reportJson, cJSON_CreateString(my_inet_ntoa2(ip[i])));
		}
	}

	SetJsonToFile(REBOOT_FLAG_PATH, rebootFlagJson);
}

void RebootReport(char* netDeviceName)
{
	cJSON *wlanJson = NULL;
	cJSON *lanJson = NULL;
	cJSON *reportJson = NULL;
	int cnt, saveFileFlag;

	rebootFlagJson = GetJsonFromFile(REBOOT_FLAG_PATH);
	if(rebootFlagJson == NULL)
	{
		rebootFlagJson = cJSON_CreateObject();
		cJSON_AddItemToObject(rebootFlagJson, NET_WLAN0, wlanJson = cJSON_CreateArray());
		cJSON_AddItemToObject(rebootFlagJson, NET_ETH0, lanJson = cJSON_CreateArray());
	}
	else
	{
		wlanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_WLAN0);
		lanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_ETH0);
	}

	if(!strcmp(netDeviceName, NET_ETH0))
	{
		reportJson = lanJson;
	}
	else
	{
		reportJson = wlanJson;
	}

	saveFileFlag = cJSON_GetArraySize(reportJson);

	while(cnt = cJSON_GetArraySize(reportJson))
	{
		IxReport(inet_addr(cJSON_GetStringValue(cJSON_GetArrayItem(reportJson, cnt-1))), REPORT_REBOOT_REQ);
		dprintf("------------------------------------ IxReport reboot ip=%s\n", cJSON_GetStringValue(cJSON_GetArrayItem(reportJson, cnt-1)));
		cJSON_DeleteItemFromArray(reportJson, cnt-1);
	}
	
	if(saveFileFlag)
	{
		SetJsonToFile(REBOOT_FLAG_PATH, rebootFlagJson);
	}
}



void DeviceManageRebootProcess(UDP_MSG_TYPE *pdata)
{
	
	DEVICE_REBOOT_REQ_T *pRequest = (DEVICE_REBOOT_REQ_T*)pdata->pbuf;
	DEVICE_REBOOT_RSP_T response;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		//重启本机
		if(ntohs(pRequest->subAddr) == 0)
		{
			if(GetDeviceRemoteManageFlag())
			{
				return;
			}
			SetDeviceRemoteManageFlag(1);
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 0;
			response.time = htons(40);		//40s
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_REBOOT_RSP_T));
			WriteRebootFlag(&(pdata->target_ip), 1);
			SendRebootLogToStm8(11, GetCurMenuCnt());//dh_20200303
			HardwareRestar();
			SetDeviceRemoteManageFlag(0);
		}
		//重启其它设备
		else
		{
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 2;
			response.time = 0xFFFF;
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_REBOOT_RSP_T));
		}
	}
}

void DeviceManageRecover(UDP_MSG_TYPE *pdata)
{
	
	DEVICE_RECOVER_REQ_T *pRequest = (DEVICE_RECOVER_REQ_T*)pdata->pbuf;
	DEVICE_RECOVER_RSP_T response;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		//恢复本机
		if(ntohs(pRequest->subAddr) == 0)
		{
			memcpy(&response, pRequest, sizeof(DEVICE_RECOVER_REQ_T));
			
			DeviceRecoverToDefault(2);

			response.result = 0;	//成功
			
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_RECOVER_RSP_T));
			
			SoftRestar();
		}
		//恢复其它设备
		else
		{
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 1;
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_RECOVER_RSP_T));
		}
	}
}
//czn_20170329_s
uint8 DeviceManageSingleLinkingReq(int target_ip, uint16 subAddr)
{
	DEVICE_SINGLE_LINKING_REQ_T request;
	DEVICE_SINGLE_LINKING_RSP_T response = {0};
	unsigned int rlen = sizeof(DEVICE_SINGLE_LINKING_RSP_T);
	request.IpAddr = target_ip;
	request.subAddr = htons(subAddr);
	request.checkType = 0;
	if(api_udp_c5_ipc_send_req(target_ip, CMD_DEVIC_SINGLE_LINKING_REQ, &request, sizeof(DEVICE_SINGLE_LINKING_REQ_T), &response, &rlen) == 0)
	{
		printf("!*!*!*!&!!!!!!ip = %08x saddr = %04x result = %d\n",response.IpAddr,response.subAddr,response.result);
		if(request.IpAddr ==  response.IpAddr && request.subAddr == response.subAddr && response.result == 0)
		{
			//设备在线
			return 0;
		}
		else
		{
			//不在线
			return 1;
		}
	}
	else
	{
		return 2;//ipdevice offline 
	}
}
//czn_20170329_e

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

