/**
  ******************************************************************************
  * @file    obj_InputDecode.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_InputDecode_H
#define _obj_InputDecode_H

// Define Object Property-------------------------------------------------------




// Define Object Function - Public----------------------------------------------

//得到一个指定字符串的房号：
// paras：
// input_str: 输入字符串
// bd_rm_ms: 输出单元号
// 0/ok，-1/err
int GetBdRmMsByInput( const char* input_str,  char* bd_rm_ms);


// Define Object Function - Private---------------------------------------------


#endif


