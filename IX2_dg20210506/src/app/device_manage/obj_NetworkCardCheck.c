/**
  ******************************************************************************
  * @file    obj_NetworkCardCheck.c
  * @author  czb
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "obj_NetworkCardCheck.h"
#include "obj_SYS_VER_INFO.h"
#include "task_survey.h"
#include "sys_msg_process.h"
#include "task_IoServer.h"
#include "obj_NetManagerInterface.h"



static int networkCardCheckCfg;
static int networkCardCheckTimeout;
static int networkCardCheckTimeCnt;
static int networkCardCheckTimeRand;
static int networkCardCheckSwreset;

static int networkWLANCheckCfg;
static int networkWLANCheckTimeout;
static int networkWLANCheckTimeCnt;
static int networkWLANCheckTimeRand;
static int networkWLANCheckSwreset;



//网卡检测对象初始化
void NetworkCardCheckInit(void)
{
	char temp[10] = {0};
	
	API_Event_IoServer_InnerRead_All(RebootIfNetErrorTimeout, temp);
	networkCardCheckCfg = atoi(temp);
	networkCardCheckTimeout = networkCardCheckCfg * 60;
	if(networkCardCheckTimeout>0)
		networkCardCheckTimeout = ((networkCardCheckTimeout<60)?60:networkCardCheckTimeout);
	networkCardCheckTimeCnt = 0;
	networkCardCheckTimeRand = 0;
	networkCardCheckSwreset = 0;
	//printf("NetworkCardCheckInit networkCardCheckTimeout=%d\n", networkCardCheckTimeout);
}

void NetworkWLANCheckInit(void)
{
	char temp[10] = {0};
	
	API_Event_IoServer_InnerRead_All(RebootIfNetErrorTimeout, temp);
	networkWLANCheckCfg = atoi(temp);
	networkWLANCheckTimeout = networkWLANCheckCfg * 60;
	if(networkWLANCheckTimeout>0)
		networkWLANCheckTimeout = ((networkWLANCheckTimeout<60)?60:networkWLANCheckTimeout);
	networkWLANCheckTimeCnt = 0;
	networkWLANCheckTimeRand = 0;
	networkWLANCheckSwreset = 0;
	//printf("NetworkCardCheckInit networkCardCheckTimeout=%d\n", networkCardCheckTimeout);
}
//网卡检测对象初始化
void ResetNetworkCardCheck(void)
{
	networkCardCheckTimeout = networkCardCheckCfg * 60;
	if(networkCardCheckTimeout>0)
		networkCardCheckTimeout = ((networkCardCheckTimeout<60)?60:networkCardCheckTimeout);
	networkCardCheckTimeCnt = 0;
	networkCardCheckTimeRand = 0;
	networkCardCheckSwreset = 0;

	//printf("ResetNetworkCardCheck networkCardCheckTimeout=%d\n", networkCardCheckTimeout);
}

void ResetNetworkWLANCheck(void)
{
	networkWLANCheckTimeout = networkWLANCheckCfg * 60;
	if(networkWLANCheckTimeout>0)
		networkWLANCheckTimeout = ((networkWLANCheckTimeout<60)?60:networkWLANCheckTimeout);
	networkWLANCheckTimeCnt = 0;
	networkWLANCheckTimeRand = 0;
	networkWLANCheckSwreset = 0;

	//printf("ResetNetworkCardCheck networkCardCheckTimeout=%d\n", networkCardCheckTimeout);
}
//网卡检测定时回调函数
void NetworkCardCheckTimerCallback(void)
{
	//printf("NetworkCardCheckTimerCallback networkCardCheckTimeCnt = %d, networkCardCheckTimeout=%d\n", networkCardCheckTimeCnt, networkCardCheckTimeout);

	if(api_nm_if_get_net_mode()==NM_MODE_LAN&&networkCardCheckTimeout>0)
	{
		if(++networkCardCheckTimeCnt >= networkCardCheckTimeout)
		{
			if(networkCardCheckTimeCnt == networkCardCheckTimeout)
			{
				networkCardCheckTimeRand = ((unsigned int)MyRand())%10+1;
			}
			else if(networkCardCheckTimeCnt == networkCardCheckTimeout + networkCardCheckTimeRand)
			{
				CHECK_NETWORK_CARD();
			}
			else if(networkCardCheckTimeCnt == networkCardCheckTimeout + networkCardCheckTimeRand + 2)
			{
				if(networkCardCheckSwreset<2)
				{
					networkCardCheckTimeCnt = 0;
					networkCardCheckTimeRand = 0;
					networkCardCheckSwreset++;
					CHECK_NETWORK_CARD_SOFTREBOOT();
				}
				else
				{
					ResetNetworkCardCheck();
					CHECK_NETWORK_CARD_REBOOT();
				}
			}
		}
	}
	if(api_nm_if_get_net_mode()==NM_MODE_WLAN&&JudgeIfWifiConnected()&&networkWLANCheckTimeout>0)
	{
		if(++networkWLANCheckTimeCnt >= networkWLANCheckTimeout)
		{
			if(networkWLANCheckTimeCnt == networkWLANCheckTimeout)
			{
				networkWLANCheckTimeRand = ((unsigned int)MyRand())%10+10;
			}
			else if(networkWLANCheckTimeCnt == networkWLANCheckTimeout + networkWLANCheckTimeRand)
			{
				CHECK_NETWORK_CARD();
			}
			else if(networkWLANCheckTimeCnt == networkWLANCheckTimeout + networkWLANCheckTimeRand + 2)
			{
				if(networkWLANCheckSwreset<2)
				{
					networkWLANCheckTimeCnt = 0;
					networkWLANCheckTimeRand = 0;
					networkWLANCheckSwreset++;
					CHECK_NETWORK_WLAN_SOFTREBOOT();
				}
				else
				{
					ResetNetworkWLANCheck();
					CHECK_NETWORK_CARD_REBOOT();
				}
			}
		}
	}
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

