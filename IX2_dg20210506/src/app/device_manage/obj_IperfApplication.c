
// lzh_20181025_s
/**
  ******************************************************************************
  * @file    obj_IperfApplication.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "obj_IperfApplication.h"
#include "cJSON.h"
#include "task_Event.h"
#include "obj_IoInterface.h"

static int iperfTestState = 0;
static int iperfReportIp = 0;
static int iperfTargetIp = 0;
static int iperfSourceIp = 0;

//souceIp:作为Iperf服务端，接收客户端发送的UDP包，并汇报结果
//targetIp:作为Iperf客户端，向服务端发送UDP包。
int API_IperfTestStart(int souceIp, int targetIp)
{
	int ret = 0;
	if(souceIp == targetIp)
	{
		return ret;
	}
	int testTime = API_Para_Read_Int(Iperf_Test_Time);
	testTime = (testTime < 5 ? 5 : testTime);

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, Event_IPERF_TEST_START);
	cJSON_AddStringToObject(event, "SOURCE_IP", my_inet_ntoa2(souceIp));
	cJSON_AddStringToObject(event, "TARGET_IP", my_inet_ntoa2(targetIp));
	cJSON_AddStringToObject(event, "REPORT_IP", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(souceIp)));
	cJSON_AddNumberToObject(event, "PORT", 6789);
	cJSON_AddNumberToObject(event, "TIME", testTime);
	cJSON_AddNumberToObject(event, "INTERVAL", 1);
	ret = API_XD_EventJson(souceIp, event);
	cJSON_Delete(event);

	return ret;
}

static int IperfTestReport(char* message)
{
	int ret = 0;
	if(message && message[0])
	{
		cJSON* event = cJSON_CreateObject();
		cJSON_AddStringToObject(event, EVENT_KEY_EventName, Event_IPERF_TEST_REPORT);
		cJSON_AddStringToObject(event, "SOURCE_IP", my_inet_ntoa2(iperfSourceIp));
		cJSON_AddStringToObject(event, "TARGET_IP", my_inet_ntoa2(iperfTargetIp));
		cJSON_AddStringToObject(event, "REPORT_IP", my_inet_ntoa2(iperfReportIp));
		cJSON_AddStringToObject(event, "REPORT_MSG", message);
		ret = API_XD_EventJson(iperfReportIp, event);
		cJSON_Delete(event);
	}
	return ret;
}

static int IperfTestReportEnd(int timing)
{
	IperfTestReport("End");
	return 2;
}

static void IperfTestCallback(int type, void* pbuf,int len )
{
	int i, j;
	char temp[200+1];
	float time1 = 0;
	float time2 = 0;
	char bandwidth[50] = {0};
	char packetLossRate[50] = {0};
	char* inputString = (char*)pbuf;
	for(i = 0, j = 0; i < len; i++)
	{
		if(inputString[i] != ' ' && inputString[i] != '\t')
		{
			temp[j++] = inputString[i];
			if(j >= 200)
			{
				temp[j] = 0;
				break;
			}
		}
	}

	sscanf(temp, "%f-%fs%[^(](%[^)]", &time1, &time2, bandwidth, packetLossRate);

	snprintf(temp, 200, "%.1f-%.1f s %s %s", time1, time2, bandwidth, packetLossRate);
	if(time2 != time1)
	{
		IperfTestReport(temp);

		if(time2 - time1 > 1)
		{
			API_Del_TimingCheck(IperfTestReportEnd);
			IperfTestReport("End");
		}
	}
}

static int IperfEventGetIp(cJSON *cmd, char* ipName)
{
	int ipRet = 0; 
	char* ipString = GetEventItemString(cmd, ipName);

	if(strcmp(ipString, ""))
	{
		ipRet = inet_addr(ipString);
	}
	return ipRet;
}

int EventIperfTestStartCallback(cJSON *cmd)
{
	iperfTargetIp = IperfEventGetIp(cmd,  "TARGET_IP");
	iperfReportIp = IperfEventGetIp(cmd,  "REPORT_IP");
	iperfSourceIp = IperfEventGetIp(cmd,  "SOURCE_IP");
	int time = GetEventItemInt(cmd, "TIME");

	if(iperfTargetIp)
	{
		if(iperfTestState)
		{
			api_iperf_udp_server_stop();
		}

		if(api_iperf_udp_server_start(iperfTargetIp, GetEventItemInt(cmd, "PORT"), time, GetEventItemInt(cmd, "INTERVAL"), 0, IperfTestCallback) != 0 )
		{
			IperfTestReport("Error");
			iperfTestState = 0;
		}
		else
		{
			IperfTestReport("Start");
			iperfTestState = 1;
		}
	}
	else
	{
		IperfTestReport("Target ip error");
	}

	if(iperfTestState)
	{
		API_Add_TimingCheck(IperfTestReportEnd, time + 10);
	}
	else
	{
		API_Del_TimingCheck(IperfTestReportEnd);
		IperfTestReport("End");
	}

	return 1;
}
