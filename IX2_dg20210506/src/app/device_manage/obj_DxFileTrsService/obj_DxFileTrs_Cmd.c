#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <dirent.h>
#include "obj_TableProcess.h"
#include "obj_SYS_VER_INFO.h"
#include "sys_msg_process.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_DxFileTrs_Cmd.h"

#pragma pack(1)
typedef struct
{
	char src_dir[FileTrsDirLength];
	char src_file[FileTrsFileLength];
	char tar_dir[FileTrsDirLength];
	char tar_file[FileTrsFileLength];
}DxFileTrs_Cmd_Req;

typedef struct
{
	char result;
}DxFileTrs_Cmd_Rsp;

typedef struct
{
	char md5[16];
	char tar_dir[FileTrsDirLength];
	char tar_file[FileTrsFileLength];
}DxFileTrs_Cmd_Verify;
#pragma pack()

int Recv_DxFileTrs_Verify_Req(int req_ip,int rsp_id,char *md5,char *verify_tar_dir,char *verify_tar_file);

void DxFileTrs_Cmd_Process(int source_ip,int cmd,int sn,uint8 *pkt , uint16 len)
{
	DxFileTrs_Cmd_Req *ptrs_req;
	DxFileTrs_Cmd_Verify *pverify_req;
	switch(cmd)
	{
		case CMD_DXFILETRS_SERVER_REQ:
			ptrs_req = (DxFileTrs_Cmd_Req *)pkt;
			Recv_DxFileTrs_Server_Req(source_ip, sn, ptrs_req->src_dir, ptrs_req->src_file, ptrs_req->tar_dir, ptrs_req->tar_file);
			break;

		case CMD_DXFILETRS_VERIFY_REQ:
			pverify_req = (DxFileTrs_Cmd_Verify *)pkt;
			Recv_DxFileTrs_Verify_Req(source_ip,sn,pverify_req->md5,pverify_req->tar_dir,pverify_req->tar_file);
			break;
	}
}

int Send_DxFileTrs_Server_Rsp(int server_ip,int rsp_id,int result)
{
	DxFileTrs_Cmd_Rsp send_cmd;

	send_cmd.result = result;

	api_udp_c5_ipc_send_rsp(server_ip,CMD_DXFILETRS_SERVER_RSP,rsp_id,(char*)&send_cmd,sizeof(DxFileTrs_Cmd_Rsp));

	return 0;
}

int Send_DxFileTrs_Server_Req(int cleint_ip,char* srcDir, char* srcFile,char* tarDir,char* tarFile)
{
	DxFileTrs_Cmd_Req send_cmd;
	DxFileTrs_Cmd_Rsp rsp_cmd;
	int rsp_len = sizeof(DxFileTrs_Cmd_Rsp);

	snprintf(send_cmd.src_dir, FileTrsDirLength, "%s", srcDir);
	snprintf(send_cmd.src_file, FileTrsFileLength, "%s", srcFile);
	snprintf(send_cmd.tar_dir, FileTrsDirLength, "%s", tarDir);
	snprintf(send_cmd.tar_file, FileTrsFileLength, "%s", tarFile);

	if(api_udp_c5_ipc_send_req(cleint_ip,CMD_DXFILETRS_SERVER_REQ,(char*)&send_cmd,sizeof(DxFileTrs_Cmd_Req),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.result != 0)
	{
		return -1;
	}

	return 0;
}

int Send_DxFileTrs_Verify(int tar_ip,char *md5,char *verify_tar_dir,char *verify_tar_file)
{
	DxFileTrs_Cmd_Verify send_cmd;
	DxFileTrs_Cmd_Rsp rsp_cmd;
	int rsp_len = sizeof(DxFileTrs_Cmd_Rsp);
	strncpy(send_cmd.tar_dir,verify_tar_dir,FileTrsDirLength);
	strncpy(send_cmd.tar_file,verify_tar_file,FileTrsFileLength);
	memcpy(send_cmd.md5,md5,16);

	dprintf("send_cmd.tar_dir=%s send_cmd.tar_file=%s\n", send_cmd.tar_dir, send_cmd.tar_file);

	if(api_udp_c5_ipc_send_req(tar_ip,CMD_DXFILETRS_VERIFY_REQ,(char*)&send_cmd,sizeof(DxFileTrs_Cmd_Verify),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		dprintf("Send_DxFileTrs_Verify error1++++++++++++++\n");
		return -1;
	}

	if(rsp_cmd.result != 0)
	{
		dprintf("Send_DxFileTrs_Verify error2++++++++++++++\n");
		return -1;
	}
	return 0;	
}

int Send_DxFileTrs_Verify_Rsp(int req_ip,int rsp_id,int result)
{
	DxFileTrs_Cmd_Rsp send_cmd;

	send_cmd.result = result;

	api_udp_c5_ipc_send_rsp(req_ip,CMD_DXFILETRS_VERIFY_RSP,rsp_id,(char*)&send_cmd,sizeof(DxFileTrs_Cmd_Rsp));

	return 0;
}

int Recv_DxFileTrs_Verify_Req(int req_ip,int rsp_id,char *md5,char *verify_tar_dir,char *verify_tar_file)
{
	char tar_path[150];
	char tar_md5[16];
	int result = 0;

	//snprintf(tar_path,150,"%s/%s",verify_tar_dir,verify_tar_file);
	snprintf(tar_path,150,"%s/%s",ResFileDir,verify_tar_file);
	
	if(FileMd5_Calculate(tar_path,(unsigned char*)tar_md5) != 0)
	{
		result = 1;
	}
	else
	{
		if(memcmp(md5,tar_md5,16) == 0)
		{
			result = 0;
		}
		else
		{
			/*
			int i;
			dprintf("md5:");
			for(i = 0; i < 16; i++)
			{
				printf(" %02x", md5[i]);
			}
			printf("\n");
			*/
		
			result = 1;
		}
	}

	Send_DxFileTrs_Verify_Rsp(req_ip,rsp_id,result);

	return 0;
}
