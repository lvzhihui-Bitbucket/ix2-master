#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <dirent.h>
#include "obj_TableProcess.h"
#include "obj_SYS_VER_INFO.h"
#include "sys_msg_process.h"
#include "obj_DxFileTrs_Cmd.h"


#define TFTP_SERVER_DIR_DEFAULT	"/mnt/nand1-1"
#define TFTP_SERVER_TRS_START	"start "
#define TFTP_SERVER_TRS_END		"end "

typedef struct
{
	int 					state;			// 0 = idle,1 = busy
	int					req_type;		// 0 = server req,1 = cleint req
	int 					retrs_cnt;
	int					trs_result;
	pthread_mutex_t		lock;
	pthread_t 			thread;
	pthread_attr_t 		attr_thread;
	char					src_dir[FileTrsDirLength];
	char					src_file[FileTrsFileLength];
	char					tar_dir[FileTrsDirLength];
	char					tar_file[FileTrsFileLength];
}DxFileTrs_S_Run_Stru;

DxFileTrs_S_Run_Stru DxFileTrs_S_Run;

int Start_Tftp_Server(int req_type);
int Wait_Tftp_Server_Exit(void);
void Thread_Tftp_Server(void);

void DxFileTrs_Server_Init(void)
{
	DxFileTrs_S_Run.state = 0;
	pthread_mutex_init( &DxFileTrs_S_Run.lock, 0);
}

void DxFileTrs_Init(void)
{
	DxFileTrs_Server_Init();
	DxFileTrs_Cleint_Init();
}

int api_server_tftp_trans( int client_ip, const char* src_file_dir, const char* src_file_name, const char* tar_file_dir, const char* tar_file_name)
{
	pthread_mutex_lock( &DxFileTrs_S_Run.lock);
	
	if(DxFileTrs_S_Run.state != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
		return -1;
	}

	DxFileTrs_S_Run.req_type = 0;
	DxFileTrs_S_Run.retrs_cnt = 0;
	DxFileTrs_S_Run.trs_result = -1;
	if(src_file_dir != NULL)
	{
		strncpy(DxFileTrs_S_Run.src_dir,src_file_dir,FileTrsDirLength);
	}
	else
	{
		strncpy(DxFileTrs_S_Run.src_dir,TFTP_SERVER_DIR_DEFAULT,FileTrsDirLength);
		if(DxFileTrs_S_Run.src_dir[sizeof(DxFileTrs_S_Run.src_dir) - 1] == '/')
		{
			DxFileTrs_S_Run.src_dir[sizeof(DxFileTrs_S_Run.src_dir) - 1] = 0;
		}
	}
	strncpy(DxFileTrs_S_Run.src_file,src_file_name,FileTrsFileLength);

	if(tar_file_dir != NULL)
	{
		strncpy(DxFileTrs_S_Run.tar_dir,tar_file_dir,FileTrsDirLength);
	}
	else
	{
		strncpy(DxFileTrs_S_Run.tar_dir,TFTP_SERVER_DIR_DEFAULT,FileTrsDirLength);
		if(DxFileTrs_S_Run.tar_dir[sizeof(DxFileTrs_S_Run.tar_dir) - 1] == '/')
		{
			DxFileTrs_S_Run.tar_dir[sizeof(DxFileTrs_S_Run.tar_dir) - 1] = 0;
		}
	}
	strncpy(DxFileTrs_S_Run.tar_file,tar_file_name,FileTrsFileLength);

	if(Start_Tftp_Server(DxFileTrs_S_Run.req_type) != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
		return -1;
	}
	
	if(Send_DxFileTrs_Server_Req(client_ip,DxFileTrs_S_Run.src_dir,DxFileTrs_S_Run.src_file,DxFileTrs_S_Run.tar_dir,DxFileTrs_S_Run.tar_file) != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
		Wait_Tftp_Server_Exit();
		return -1;
	}

	DxFileTrs_S_Run.state = 1;
	pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
	
	Wait_Tftp_Server_Exit();

	if(DxFileTrs_S_Run.trs_result != 0)
	{
		DxFileTrs_S_Run.state = 0;
		return -1;
	}
	
	char file_path[80];
	char md5[16];
	snprintf(file_path,80,"%s/%s",DxFileTrs_S_Run.src_dir,DxFileTrs_S_Run.src_file);

	if(FileMd5_Calculate(file_path,(unsigned char*)md5) != 0)
	{
		DxFileTrs_S_Run.state = 0;
		return -1;
	}
	
	if(Send_DxFileTrs_Verify(client_ip,md5,DxFileTrs_S_Run.tar_dir,DxFileTrs_S_Run.tar_file) != 0)
	{
		DxFileTrs_S_Run.state = 0;
		return -1;
	}
	
	DxFileTrs_S_Run.state = 0;
	return 0;
}


int Start_Tftp_Server(int req_type)
{
	pthread_attr_init(&DxFileTrs_S_Run.attr_thread );
	
	if(req_type == 1)
	{
		pthread_attr_setdetachstate( &DxFileTrs_S_Run.attr_thread, PTHREAD_CREATE_DETACHED );
	}
	
	if( pthread_create(&DxFileTrs_S_Run.thread,&DxFileTrs_S_Run.attr_thread,(void*)Thread_Tftp_Server, NULL) != 0 )
	{
		return -1;
	}
	
	return 0;
}

int Wait_Tftp_Server_Exit(void)
{
	if(DxFileTrs_S_Run.req_type == 0)
	{
		pthread_join(DxFileTrs_S_Run.thread,NULL);
	}

	return 0;
}

void Thread_Tftp_Server(void)
{
	FILE* pf   = NULL;
	char cmd_str[200]={'\0'};
	int pid;
	fd_set fds;
	struct timeval tv={1,0};
	int ret,fd;

	pthread_mutex_lock( &DxFileTrs_S_Run.lock);
	if(DxFileTrs_S_Run.state == 0 )
	{
		DxFileTrs_S_Run.trs_result = -1;
		pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
		return;
	}
	pthread_mutex_unlock( &DxFileTrs_S_Run.lock);

	snprintf(cmd_str,200,"/usr/bin/udpsvd -vE 0.0.0.0 69 tftpd -c %s 2>&1 &", DxFileTrs_S_Run.src_dir);//
	if( (pf = popen( cmd_str, "r" )) == NULL )
	{
		printf( "start_tftpd_download error:%s\n",strerror(errno) );
		DxFileTrs_S_Run.trs_result = -1;
		goto Tftp_Server_Thread_Exit;
		return;
	}

	fd = fileno(pf);
	while(1)
	{
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 1;
		tv.tv_usec = 0;	
		ret = select( fd + 1, &fds, NULL, NULL, &tv );
		if(ret == 0 ||ret == -1)
		{
			DxFileTrs_S_Run.trs_result = 0;
			dprintf("Thread_Tftp_Server start error ret=%d\n",ret);
			goto Tftp_Server_Thread_Exit;
		}
		fgets(cmd_str,200,pf);
		
		dprintf("pipe print:%s\n",cmd_str);
			
		if(strstr(cmd_str,TFTP_SERVER_TRS_START) != NULL)
		{
			break;
		}
	}

	while(1)
	{
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 180;
		tv.tv_usec = 0;	

		ret = select( fd + 1, &fds, NULL, NULL, &tv );
		if(ret == 0 ||ret == -1)
		{
			dprintf("Thread_Tftp_Server download error ret= %d\n",ret);
			DxFileTrs_S_Run.trs_result = -1;
			goto Tftp_Server_Thread_Exit;
		}

		fgets(cmd_str,200,pf);
		
		dprintf("pipe print:%s\n",cmd_str);
			
		if(strstr(cmd_str,TFTP_SERVER_TRS_END) != NULL)
		{
			DxFileTrs_S_Run.trs_result = 0;
			break;
		}
	}

Tftp_Server_Thread_Exit:
	pid = getpid_bycmdline("/usr/bin/udpsvd -vE 0.0.0.0 69");//
	if(pid > 0)
	{
		printf("kill the process pid =%d\n",pid);
		kill(pid,SIGKILL);
		pclose(pf);
	}
	dprintf("start_tftpd_download time:%d-- over\n",time(NULL));
	

	pthread_mutex_lock( &DxFileTrs_S_Run.lock);
	if(DxFileTrs_S_Run.req_type == 1)
	{
		DxFileTrs_S_Run.state = 0;
	}
	pthread_mutex_unlock( &DxFileTrs_S_Run.lock);
}


