#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <dirent.h>
#include "obj_TableProcess.h"
#include "obj_SYS_VER_INFO.h"
#include "sys_msg_process.h"
#include "obj_DxFileTrs_Cmd.h"
#include "define_file.h"


typedef struct
{
	int 					state;			// 0 = idle,1 = busy
	int					req_type;		// 0 = server req,1 = cleint req
	int 					retrs_cnt;
	int					trs_result;
	int					server_ip;
	pthread_mutex_t		lock;
	pthread_t 			thread;
	pthread_attr_t 		attr_thread;
	char					src_dir[FileTrsDirLength];
	char					src_file[FileTrsFileLength];
	char					tar_dir[FileTrsDirLength];
	char					tar_file[FileTrsFileLength];
}DxFileTrs_C_Run_Stru;

DxFileTrs_C_Run_Stru DxFileTrs_C_Run;

int Start_Tftp_Cleint(int req_type);
int Wait_Tftp_Cleint_Exit(void);
void Thread_Tftp_Cleint(void);

void DxFileTrs_Cleint_Init(void)
{
	DxFileTrs_C_Run.state = 0;
	pthread_mutex_init( &DxFileTrs_C_Run.lock, 0);
}

int Recv_DxFileTrs_Server_Req(int server_ip, int rsp_id,char *srcDir, char *srcFile, char *tarDir,char *tarFile)
{
	pthread_mutex_lock( &DxFileTrs_C_Run.lock);
	
	if(DxFileTrs_C_Run.state != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
		return -1;
	}

	DxFileTrs_C_Run.req_type = 0;
	DxFileTrs_C_Run.retrs_cnt = 0;
	DxFileTrs_C_Run.trs_result = -1;
	DxFileTrs_C_Run.server_ip = server_ip;
	
	snprintf(DxFileTrs_C_Run.src_file, FileTrsFileLength, "%s", srcFile);
	snprintf(DxFileTrs_C_Run.tar_file, FileTrsFileLength, "%s", tarFile);
	snprintf(DxFileTrs_C_Run.src_dir, FileTrsDirLength, "%s", srcDir);
	snprintf(DxFileTrs_C_Run.tar_dir, FileTrsDirLength, "%s", ResFileDir);
	
	if(Start_Tftp_Cleint(DxFileTrs_C_Run.req_type) != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
		return -1;
	}
	
	if(Send_DxFileTrs_Server_Rsp(server_ip,rsp_id,0) != 0)
	{
		pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
		//Wait_Tftp_Server_Exit();
		return -1;
	}
	DxFileTrs_C_Run.state = 1;

	pthread_mutex_unlock( &DxFileTrs_C_Run.lock);

	return 0;
}

int Start_Tftp_Cleint(int req_type)
{
	pthread_attr_init(&DxFileTrs_C_Run.attr_thread );
	
	//if(req_type == 1)	//czn_20190221
	{
		pthread_attr_setdetachstate( &DxFileTrs_C_Run.attr_thread, PTHREAD_CREATE_DETACHED );
	}
	
	if( pthread_create(&DxFileTrs_C_Run.thread,&DxFileTrs_C_Run.attr_thread,(void*)Thread_Tftp_Cleint, NULL) != 0 )
	{
		return -1;
	}
	
	return 0;
}

int Wait_Tftp_Cleint_Exit(void)
{
	if(DxFileTrs_C_Run.req_type == 0)
	{
		pthread_join(DxFileTrs_C_Run.thread,NULL);
	}

	return 0;
}

void Thread_Tftp_Cleint(void)
{
	pthread_mutex_lock( &DxFileTrs_C_Run.lock);
	if(DxFileTrs_C_Run.state == 0 )
	{
		DxFileTrs_C_Run.trs_result = -1;
		pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
		return;
	}
	pthread_mutex_unlock( &DxFileTrs_C_Run.lock);

	FILE* pf   = NULL;
	char cmd_str[200]={'\0'};
	char local_file[80];
	char ip_str[20];

	memset( local_file, 0, 80);
	memset( ip_str, 0, 20);
	
	ConvertIpInt2IpStr( DxFileTrs_C_Run.server_ip,ip_str );
		
	snprintf(local_file,100,"%s/%s",DxFileTrs_C_Run.tar_dir,DxFileTrs_C_Run.tar_file);
	
	snprintf(cmd_str,200,"tftp -g -r %s -l %s -b 512 %s",DxFileTrs_C_Run.src_file,local_file,ip_str);
	dprintf("start_tftp_download -- %s\n",cmd_str);

	if( (pf = popen( cmd_str, "r" )) == NULL )
	{
		dprintf( "start_tftp_download error:%s\n",strerror(errno) );
		pthread_mutex_lock( &DxFileTrs_C_Run.lock);
		DxFileTrs_C_Run.trs_result = -1;
		if(DxFileTrs_C_Run.req_type == 0)
		{
			DxFileTrs_C_Run.state = 0;
		}
		pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
		return;
	}
	pclose( pf );
	dprintf("start_tftp_download -- over\n");

	pthread_mutex_lock( &DxFileTrs_C_Run.lock);
	DxFileTrs_C_Run.trs_result = 0;
	if(DxFileTrs_C_Run.req_type == 0)
	{
		DxFileTrs_C_Run.state = 0;
	}
	pthread_mutex_unlock( &DxFileTrs_C_Run.lock);
}
