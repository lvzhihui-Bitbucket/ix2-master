
#include "task_survey.h"
#include "ftp_download.h"
#include "define_file.h"

const char* download_info_tab[]=
{
	"***",
	"download_info_ok",
	"download_info_para_err1",
	"download_info_para_err2",
	"download_info_perform_err",
	"download_info_init_err",
};

pthread_mutex_t ftp_pf_lock = PTHREAD_MUTEX_INITIALIZER;
static FILE *ftp_pf = NULL;
void (*ftp_cb)(int, char*);

// ftpclient shell command usage: "ftpclient server user password filename [store_dir] [download_max_timeout] [neterr_max_timeout]"
int ftp_download_start( char* server, char* user, char* password, char* filename, char* storedir, int dm_tout, int nm_tout, void (*cb)(int, char*) )
{
	char command[300];

	pthread_mutex_lock(&ftp_pf_lock);
	
	if( ftp_pf )
	{
		printf( "ftp_download_start, close last\n");	
		pclose( ftp_pf );
	}

	if( storedir == NULL )
		sprintf(command,"%s %s %s %s %s", FTP_CLIENT_PATH, server,user,password,filename);
	else
		sprintf(command,"%s %s %s %s %s %s %d %d", FTP_CLIENT_PATH, server,user,password,filename,storedir,dm_tout,nm_tout);
	
	printf( "process command[%s]\n", command );
	
	if( (ftp_pf = popen( command, "r" )) == NULL )
	{
		printf( "open command[%s] error\n", command );
		pthread_mutex_unlock(&ftp_pf_lock);
		return -1;
	}
	
	ftp_cb = cb;
	// set ftp_ps as non-block
	int flags;
	int ftp_fd;	
	ftp_fd = fileno(ftp_pf);
	flags = fcntl(ftp_fd, F_GETFL, 0); 
	flags |= O_NONBLOCK; 
	fcntl(ftp_fd, F_SETFL, flags);	

	pthread_mutex_unlock(&ftp_pf_lock);
	return 0;
}

int ftp_download_start_default( char* server, char* filename, char* storedir, void (*cb)(int, char*) )
{
	return ftp_download_start(server,DEFAULT_USERNAME,DEFAULT_PASSWORD,filename,storedir,7200,10,cb);
}


int ftp_download_cancel(void)
{
	pthread_mutex_lock(&ftp_pf_lock);

	if( ftp_pf != NULL )
	{
		printf( "ftp_download_cancel: start close ftp...\n");
		if( pclose( ftp_pf ) == 0 )
		{
			printf("ftp close successful!!!\n");
		}
		else
		{
			printf("ftp close failed!!!err[%u]\n",strerror(errno));
		}
		ftp_pf = NULL;
		pthread_mutex_unlock(&ftp_pf_lock);
		return 0;
	}
	else
	{
		printf( "ftp_download_cancel: have no ftp\n");	
		pthread_mutex_unlock(&ftp_pf_lock);
		return -1;
	}
}

int ftp_download_process(  void)
{
	pthread_mutex_lock(&ftp_pf_lock);

	if( ftp_pf != NULL )
	{
		int i;
		char linestr[200];
		memset(linestr,0,200);
		while( fgets( linestr, 200, ftp_pf ) != NULL )
		{
			linestr[strlen(linestr)-1] = 0;	// del the '\n'
			for( i = 0; i < DOWNLOAD_INFO_MAX; i++ )
			{
				if( strstr( linestr, download_info_tab[i] ) != NULL )
				{
					if( i == DOWNLOAD_INFO_PROCESS )
						(*ftp_cb)(i,linestr+sizeof(download_info_tab[i])-1);
					else
						(*ftp_cb)(i,download_info_tab[i]);	
					break;
				}		
			}
			memset(linestr,0,200);
		}
		pthread_mutex_unlock(&ftp_pf_lock);
		return 0;
	}
	else
	{
		pthread_mutex_unlock(&ftp_pf_lock);
		return -1;
	}
}

