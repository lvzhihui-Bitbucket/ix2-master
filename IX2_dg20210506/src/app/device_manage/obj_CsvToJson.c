
/**
  ******************************************************************************
  * @file    obj_CsvToJson.c
  * @author  cao
  * @version V00.01.00
  * @date    2023.11.29
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "libcsv.h"
#include "cJSON.h"

typedef struct                         
{
    cJSON* jsonTable;
    int row;
    int col;
    int state;
}CSV_TO_JSON_RUN;

static int CheckString(char* checkStr, char* s, int sLen)
{
    int ret = 0;
    int keyStrLen = 0;

    if(checkStr)
    {
        keyStrLen = strlen(checkStr);
    }

    if(keyStrLen == sLen)
    {
        if(s && checkStr && !strncmp(s, checkStr, keyStrLen))
        {
            ret = 1;
        }
    }

    return ret;
}

static cJSON* CreateJsonString(char* s, int sLen)
{
    cJSON* ret;
    char* tempString = malloc(sLen+1);
    if(s)
    {
        memcpy(tempString, s, sLen);
    }
    tempString[sLen] = 0;
    ret = cJSON_CreateString(tempString);
    free(tempString);

    return ret;
}

static void FieldCB (void *s, size_t i, void *para) 
{
    CSV_TO_JSON_RUN* pRun = para;
    if(pRun->state == 0)
    {
        if(pRun->col == 0 && CheckString("[TABLE_KEY_NAME]", s, i))
        {
            if(!(pRun->jsonTable))
            {
                pRun->jsonTable = cJSON_CreateObject();
            }
            cJSON_AddArrayToObject(pRun->jsonTable, "KEY_NAME");
            pRun->state = 1;
        }
    }
    else if (pRun->state == 1)
    {
        if(pRun->col == 0 && CheckString("key=", s, i))
        {
            pRun->state = 2;
        }
    }
    else if (pRun->state == 2)
    {
        if(pRun->col)
        {
            cJSON* keyName = cJSON_GetObjectItemCaseSensitive(pRun->jsonTable, "KEY_NAME");
            cJSON_AddItemToArray(keyName, CreateJsonString(s, i));
        }
        else if(CheckString("max=", s, i))
        {
            cJSON_AddArrayToObject(pRun->jsonTable, "VALUE_MAX");
            pRun->state = 3;
        }
    }
    else if (pRun->state == 3)
    {
        if(pRun->col)
        {
            cJSON* valueMax = cJSON_GetObjectItemCaseSensitive(pRun->jsonTable, "VALUE_MAX");
            cJSON_AddItemToArray(valueMax, CreateJsonString(s, i));
        }
        else if(CheckString("[TABLE_KEY_VALUE]", s, i))
        {
            cJSON_AddArrayToObject(pRun->jsonTable, "TB_DAT");
            pRun->state = 4;
            pRun->row = 0;
        }
    }
    else if (pRun->state == 4)
    {
        if(pRun->col == 0 && CheckString("value=", s, i))
        {
            cJSON* tbDat = cJSON_GetObjectItemCaseSensitive(pRun->jsonTable, "TB_DAT");
            cJSON_AddItemToArray(tbDat, cJSON_CreateObject());
            pRun->state = 5;
        }
    }
    else if (pRun->state == 5)
    {
        cJSON* keyName = cJSON_GetObjectItemCaseSensitive(pRun->jsonTable, "KEY_NAME");
        if((pRun->col >= 1) && (pRun->col < cJSON_GetArraySize(keyName)+1))
        {
            cJSON* tbDat = cJSON_GetObjectItemCaseSensitive(pRun->jsonTable, "TB_DAT");
            cJSON* record = cJSON_GetArrayItem(tbDat, pRun->row);
            cJSON* name = cJSON_GetArrayItem(keyName, pRun->col-1);
            if(cJSON_IsString(name) && cJSON_IsObject(record))
            {
                cJSON_AddItemToObject(record, cJSON_GetStringValue(name), CreateJsonString(s, i));
            }
        }
    }
    pRun->col++;
}

static void RowCB(int c, void *para)
{
    CSV_TO_JSON_RUN* pRun = para;
    if(pRun->state == 5)
    {
        pRun->row++;
        pRun->state = 4;
    }
    pRun->col = 0;
}

static CSV_TO_JSON_RUN* CreateCsvToJsonRun(void)
{
    CSV_TO_JSON_RUN * ret;

    ret = malloc(sizeof(CSV_TO_JSON_RUN));
    ret->jsonTable = NULL;
    ret->col = 0;
    ret->row = 0;
    ret->state = 0;
    return ret;
}

cJSON* CsvToJson(char *filePath)
{
    FILE *fp;
    struct csv_parser p;
    char buf[1024];
    size_t bytes_read;
    unsigned char options = 0;
    cJSON* ret;
    size_t pos = 0;
    size_t retval;

    CSV_TO_JSON_RUN* pRun = CreateCsvToJsonRun();

    if (csv_init(&p, CSV_STRICT) != 0)
    {
        fprintf(stderr, "Failed to initialize csv parser\n");
    }

    fp = fopen(filePath, "rb");
    if(fp)
    {
        while ((bytes_read = fread(buf, 1, 1024, fp)) > 0)
        {
            if((retval = csv_parse(&p, buf, bytes_read, FieldCB, RowCB, pRun)) != bytes_read) 
            {
                if (csv_error(&p) == CSV_EPARSE) {
                    printf("%s: malformed at byte %lu\n", filePath, (unsigned long)pos + retval + 1);
                }
                else
                {
                    printf("Error while processing %s: %s\n", filePath, csv_strerror(csv_error(&p)));
                }
                break;
            }
            pos += bytes_read;
        }
        printf("CsvToJson retval = %d, bytes_read = %d\n", retval, bytes_read);
        fclose(fp);
    }
    
    csv_fini(&p, FieldCB, RowCB, pRun);
    csv_free(&p);
    ret = pRun->jsonTable;
    free(pRun);

    return ret;
}