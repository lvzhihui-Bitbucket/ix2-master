/**
 ******************************************************************************
 * @file    obj_IXS_Rules.c
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */
#include "OSTIME.h"
#include "obj_IoInterface.h"
#include <pthread.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_PublicUnicastCmd.h"
#include "obj_PublicInformation.h"
#include "vtk_udp_stack_device_update.h"
#include "task_Event.h"
#include "define_string.h"
#include "define_file.h"
#include "obj_IXS_Rules.h"
#include "obj_GetInfoByIp.h"
#include "obj_SearchIpByFilter.h"

int IXS_Search(const char* net, cJSON* searchCtrl, const char* BD_Nbr, const char* deviceType, const cJSON* resultTable)
{
	int ret = -1;
	if(!IXS_SearchIsBusy())
	{
		ret = IXS_SearchJsonDevTb(net, searchCtrl, BD_Nbr, deviceType, resultTable);
	}

	return ret;
}

int IXS_Info(const cJSON* ipTable, const cJSON* resultTable)
{
	int ret = 0;
	int ip = 0;
	cJSON* element;
	cJSON* record;
	cJSON* newRecord;
	char* platformString;
	DeviceInfo info;
	char infoString[2000];

	if(!cJSON_IsArray(resultTable))
	{
		return ret;
	}

	cJSON_ArrayForEach(element, ipTable)
	{
		ip = inet_addr(GetShellCmdJsonString(element, IX2V_IP_ADDR));
/*
		platformString = GetShellCmdJsonString(element, IX2V_Platform);
		//IX2V设备
		if(platformString && !strcmp(platformString, IX2V))
		{
			cJSON* view;
			cJSON* record;

			view = cJSON_CreateObject();
			cJSON_AddStringToObject(view, PB_MFG_SN, PB_Identifier);
			cJSON_AddStringToObject(view, PB_IX_ADDR, PB_Identifier);
			cJSON_AddStringToObject(view, PB_IX_NAME, PB_Identifier);
			cJSON_AddStringToObject(view, PB_G_NBR, PB_Identifier);
			cJSON_AddStringToObject(view, PB_L_NBR, PB_Identifier);
			cJSON_AddStringToObject(view, PB_UP_TIME, PB_Identifier);
			cJSON_AddStringToObject(view, IX_NT, IO_Identifier);

			cJSON_AddStringToObject(view, PB_LAN_IP, PB_Identifier);
			cJSON_AddStringToObject(view, PB_LAN_MASK, PB_Identifier);
			cJSON_AddStringToObject(view, PB_LAN_GATEWAY, PB_Identifier);
			cJSON_AddStringToObject(view, PB_WLAN_IP, PB_Identifier);
			cJSON_AddStringToObject(view, PB_WLAN_MASK, PB_Identifier);
			cJSON_AddStringToObject(view, PB_WLAN_GATEWAY, PB_Identifier);

			//使用API_GetPbIo指令获取信息
			if(record = API_GetPbIo(ip, NULL, view))
			{

			}
		}
		//IX1/2设备
		else
*/		
		{
			if(API_GetMergeInfoByIp(ip, BusinessWaitUdpTime, infoString) == 0)
			{
				char* name;
				cJSON* platform;
				ret++;
				record = cJSON_Parse(infoString);
				newRecord = cJSON_CreateObject();

				platform = cJSON_GetObjectItemCaseSensitive(record, IX2V_Platform);
				if(platform)
				{
					cJSON_AddItemToObject(newRecord, IX2V_Platform, cJSON_Duplicate(platform, 1));
				}
				else
				{
					cJSON_AddStringToObject(newRecord, IX2V_Platform, "IX1/2");
				}

				cJSON_AddItemToObject(newRecord, IX2V_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_MFG_SN), 1));
				cJSON_AddItemToObject(newRecord, IX2V_IP_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "IP_ADDR"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_IX_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "BD_RM_MS"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_G_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_Global), 1));
				cJSON_AddItemToObject(newRecord, IX2V_L_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_Local), 1));

				name = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_name2_utf8));
				name = ((name == NULL || name[0] == 0) ? cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_name1)) : name);
				cJSON_AddStringToObject(newRecord, IX2V_IX_NAME, (name ? name : IX2V_Unknow));					

				cJSON_AddStringToObject(newRecord, IX2V_IX_TYPE, DeviceTypeToString(cJSON_GetObjectItemCaseSensitive(record, DM_KEY_deviceType)->valueint));
				cJSON_AddItemToObject(newRecord, IX2V_IX_Model, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "Ver_Devicetype"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_DevModel, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "DeviceModel"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_FW_VER, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "SW_Ver"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_HW_VER, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "HW_Ver"), 1));
				cJSON_AddItemToObject(newRecord, IX2V_UpTime, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "UpTime"), 1));
				cJSON_Delete(record);
			}
			else if(API_GetInfoByIp(ip, BusinessWaitUdpTime, &info) == 0)
			{
				ret++;
				newRecord = cJSON_CreateObject();
				cJSON_AddStringToObject(newRecord, IX2V_Platform, "IX1/2");
				cJSON_AddStringToObject(newRecord, IX2V_MFG_SN, info.MFG_SN);
				cJSON_AddStringToObject(newRecord, IX2V_IP_ADDR, info.IP_ADDR);
				cJSON_AddStringToObject(newRecord, IX2V_IX_ADDR, info.BD_RM_MS);
				cJSON_AddStringToObject(newRecord, IX2V_G_NBR, info.Global);
				cJSON_AddStringToObject(newRecord, IX2V_L_NBR, info.Local);
				cJSON_AddStringToObject(newRecord, IX2V_IX_NAME, (info.name2_utf8[0] ? info.name2_utf8 : info.name1));					
				cJSON_AddStringToObject(newRecord, IX2V_IX_TYPE, DeviceTypeToString(info.deviceType));
				cJSON_AddStringToObject(newRecord, IX2V_IX_Model, IX2V_Unknow);
				cJSON_AddStringToObject(newRecord, IX2V_DevModel, IX2V_Unknow);
				cJSON_AddStringToObject(newRecord, IX2V_FW_VER, IX2V_Unknow);
				cJSON_AddStringToObject(newRecord, IX2V_HW_VER, IX2V_Unknow);
				cJSON_AddStringToObject(newRecord, IX2V_UpTime, IX2V_Unknow);
			}
			else
			{
				newRecord = cJSON_Duplicate(element, 1);
			}
		}

		cJSON_AddItemToArray(resultTable, newRecord);
	}

	return ret;
}

/**********************************************************************
	发送By number搜索指令
	paras:
		net:网卡名字
		bdRmMs: bdRmMs
		input : input
		resultTable : 查找结果
	return: -1:系统忙, -2:传递进来参数错误, 0:没有查找到, >0:查找完成
**********************************************************************/
int IXS_GetByNBR(const char* net, cJSON* searchCtrl, const char* bdRmMs, const char* input, const cJSON* resultTable)
{
	return API_JsonGetIpByNumber(net, searchCtrl, bdRmMs, input, resultTable);
}

int IXS_GetByMFG_SN(const char* MFG_SN, const cJSON* resultTable)
{
	int ip;
	int upTime;
	char temp[100];

	if(API_GetIpByMFG_SN(MFG_SN, 1, &ip, &upTime) == 0)
	{
		if(cJSON_IsArray(resultTable))
		{
			while (cJSON_GetArraySize(resultTable))
			{
				cJSON_DeleteItemFromArray(resultTable, 0);
			}
			
			cJSON* record = cJSON_CreateObject();
			cJSON_AddItemToArray(resultTable, record);
			cJSON_AddStringToObject(record, IX2V_MFG_SN, MFG_SN);
			cJSON_AddStringToObject(record, IX2V_IP_ADDR, my_inet_ntoa2(ip));
			snprintf(temp, 100, "%d:%02d:%02d", upTime/3600, (upTime%3600)/60, upTime%60);
			cJSON_AddStringToObject(record, IX2V_UpTime, temp);
		}
		return 1;
	}

	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/