#include <stdio.h>
#include <stdlib.h>
#include <sys/statfs.h>
#include "utility.h"
#include "define_file.h"
#include "obj_FileManageUtility.h"
#include "cJSON.h"

void SetJsonToFile(const char* filePath, const cJSON *json)
{
	FILE	*file = NULL;
	char* string = NULL;

	string = cJSON_Print(json);
	if(string != NULL)
	{
		if((file=fopen(filePath,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
			sync();
		}

		//dprintf("filePath=%s\n%s", filePath, string);
		free(string);
	}
}

CJSON_PUBLIC(cJSON *) GetJsonFromFile(const char* filePath)
{
	FILE* file = NULL;
	char* json = NULL;
	cJSON* retJson = NULL;
	
	if((file=fopen(filePath,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if((json = malloc(size+1)) == NULL )
		{
			fclose(file);
			return retJson;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		json[size] = 0;
		
		fclose(file);

		retJson = cJSON_Parse(json);
		free(json);
	}
	else
	{
		dprintf("%s does not exist.\n", filePath);
	}

	return retJson;
}

int DeleteFileProcess(char* dir, char* fileName)
{
	char cmd_line[250]={0};
	
	if((dir != NULL && dir[0] == 0) || (fileName != NULL && fileName[0] == 0))
	{
		return -1;
	}

	snprintf(cmd_line, 250, "rm -rf %s/%s", (dir == NULL) ? "" : dir, (fileName == NULL) ? "" : fileName);

	if(system(cmd_line) != 0)
	{
		return -1;
	}

	sync();

	dprintf("%s\n", cmd_line);

	return 0;
}

unsigned long GetDirFreeSizeInMByte(char *path)
{
	unsigned long freeSize = 0;
	struct statfs myStatfs;

	if(!path)
	{
		return freeSize;
	}

	if(strlen(path) >= strlen(DISK_SDCARD) && !memcmp(path, DISK_SDCARD, strlen(DISK_SDCARD)))
	{
		if(!Judge_SdCardLink())
		{
			return freeSize;
		}		
	}

	if(statfs(path, &myStatfs) != -1 )
	{
		freeSize = (long long)myStatfs.f_bsize * (long long)myStatfs.f_bfree/1024/1024;
	}
	//printf("f_type=%d, f_bsize=%d, f_blocks=%d, f_bfree=%d, f_bavail=%d, f_files=%d, f_ffree=%d, f_fsid=%d, f_namelen=%d\n", myStatfs.f_type, myStatfs.f_bsize, myStatfs.f_blocks, myStatfs.f_bfree, myStatfs.f_bavail, myStatfs.f_files, myStatfs.f_ffree, myStatfs.f_fsid, myStatfs.f_namelen);

	return freeSize;
}

int MakeDir(char* path)
{
	char  cmd_buff[200];
	
	if(!path)
	{
		return -1;
	}
	
	if(access( path, F_OK ) != 0 )
	{
		if(strlen(path) >= strlen(DISK_SDCARD) && !memcmp(path, DISK_SDCARD, strlen(DISK_SDCARD)))
		{
			if(!Judge_SdCardLink())
			{
				return -1;
			}
		}
		
		snprintf(cmd_buff, 200, "mkdir -p %s", path);
		system(cmd_buff);

		if(access( path, F_OK ) != 0 )
		{
			return -1;
		}
		sync();
	}

	return 0;
}

int IsFileExist(char* path)
{
	if(access( path, F_OK ) == 0 )
	{
		return 1;
	}
	
	return 0;
}

int FileMd5_CalculateString(char *file_path, char *file_md5_string)
{
	int ret;
	char md5[16];
	int i;

	ret = FileMd5_Calculate(file_path, md5);
	if(ret == 0 && file_md5_string != NULL)
	{
		for(i = 0; i < 16; i++)
		{
			sprintf(file_md5_string + i*2, "%02X", md5[i]);
		}
		dprintf("%s calmd5:%s\n", file_path, file_md5_string);
	}

	return 0;
}

//output长度必须>=33，返回 0 失败，1 成功
int CalculateJsonMd5(cJSON *input, char *output)
{
	int ret;
	char md5[16];
	char* string = cJSON_PrintUnformatted(input);

	ret = (StringMd5_Calculate(string, md5) ? 0 : 1);
	if(ret && output)
	{
		for(int i = 0; i < 16; i++)
		{
			sprintf(output + i*2, "%02X", md5[i]);
		}
	}

	if(string)
	{
		free(string);
	}

	return ret;
}


int MoveFile(const char* srcFile, const char* tagFile)
{
	char cmd_line[250];

	snprintf(cmd_line, 250,"mv %s %s", srcFile, tagFile);

	if(system(cmd_line) != 0)
	{
		return -1;
	}

	return 0;
}

int CopyFile(const char* srcFile, const char* tagFile)
{
	char cmd_line[250];

	snprintf(cmd_line, 250,"cp -r %s %s", srcFile, tagFile);

	if(system(cmd_line) != 0)
	{
		return -1;
	}

	return 0;
}

int FileChmod(const char* srcFile, const char* mode)
{
	char cmd_line[250];

	snprintf(cmd_line, 250,"chmod -R %s %s", mode, srcFile);

	if(system(cmd_line) != 0)
	{
		return -1;
	}

	return 0;
}

//selectFileOrDir：0/文件，1/目录，2/文件和目录
int GetFileAndDirList(const char* dir, cJSON *fileList, int selectFileOrDir)
{
	char saveDir[200];
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
	cJSON *file = NULL;
	char filePath[100];
	char fileTime[100];
	char permission[11];
	char* path;
	int ifDir;
	
    if((dp = opendir(dir)) == NULL)
	{
        dprintf("cannot open directory: %s\n", dir);
        return -1;
    }
	
	getcwd(saveDir, 200);

	chdir(dir);
	
    while((entry = readdir(dp)) != NULL) 
	{
        lstat(entry->d_name,&statbuf);

        ifDir = S_ISDIR(statbuf.st_mode) ? 1 : 0;
		if(ifDir)
		{
			if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0)
			{
				continue;
			}
		}

		if(selectFileOrDir == 0 && ifDir)
		{
			continue;
		}
		else if((selectFileOrDir == 1) && (ifDir == 0))
		{
			continue;
		}

		snprintf(permission, 11, "%s%s%s%s%s%s%s%s%s%s",
		ifDir ? "d" : "-", 
		IF_TRUE(statbuf.st_mode, S_IRUSR) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWUSR) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXUSR) ? "x" : "-", 
		IF_TRUE(statbuf.st_mode, S_IRGRP) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWGRP) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXGRP) ? "x" : "-", 
		IF_TRUE(statbuf.st_mode, S_IROTH) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWOTH) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXOTH) ? "x" : "-");

		if(fileList)
		{
			file = cJSON_CreateArray();
			cJSON_AddItemToArray(fileList, file);
			
			//添加文件属性
			cJSON_InsertItemInArray(file, 0, cJSON_CreateString(permission));

			//添加文件大小
			cJSON_InsertItemInArray(file, 1, cJSON_CreateNumber(statbuf.st_size));

			//添加文件修改时间
			struct tm* pModifytime=localtime(&(statbuf.st_mtime));
			strftime(fileTime, 100, "%Y-%m-%d %H:%M:%S", pModifytime);
			cJSON_InsertItemInArray(file, 2, cJSON_CreateString(fileTime));

			//添加文件路径
			cJSON_InsertItemInArray(file, 3, cJSON_CreateString(entry->d_name));
		}
    }
	
    closedir(dp);
    chdir(saveDir);

	return 0;
}

int GetFileAndDirInfo(const char* dir, cJSON *fileInfo)
{
    struct stat statbuf;
	char filePath[100];
	char fileTime[100];
	char permission[11];
	int ifDir;
	int ret;
	
	if(lstat(dir,&statbuf) < 0)
	{
		ret = -1;
	}
	else
	{
		ifDir = S_ISDIR(statbuf.st_mode) ? 1 : 0;

		snprintf(permission, 11, "%s%s%s%s%s%s%s%s%s%s",
		ifDir ? "d" : "-", 
		IF_TRUE(statbuf.st_mode, S_IRUSR) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWUSR) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXUSR) ? "x" : "-", 
		IF_TRUE(statbuf.st_mode, S_IRGRP) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWGRP) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXGRP) ? "x" : "-", 
		IF_TRUE(statbuf.st_mode, S_IROTH) ? "r" : "-",
		IF_TRUE(statbuf.st_mode, S_IWOTH) ? "w" : "-", 
		IF_TRUE(statbuf.st_mode, S_IXOTH) ? "x" : "-");

		if(fileInfo)
		{
			//添加文件属性
			cJSON_InsertItemInArray(fileInfo, 0, cJSON_CreateString(permission));

			//添加文件大小
			cJSON_InsertItemInArray(fileInfo, 1, cJSON_CreateNumber(statbuf.st_size));

			//添加文件修改时间
			struct tm* pModifytime=localtime(&(statbuf.st_mtime));
			strftime(fileTime, 100, "%Y-%m-%d %H:%M:%S", pModifytime);
			cJSON_InsertItemInArray(fileInfo, 2, cJSON_CreateString(fileTime));
			//添加文件名
			cJSON_InsertItemInArray(fileInfo, 3, cJSON_CreateString(basename(dir)));
		}
		ret = 0;
	}

	return ret;
}

