#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
#include "obj_reboot_log.h"
#include "../task_debug_sbu/task_debug_sbu.h"



int RebootLog_start_line_save = 0;
extern char logfile_name[];
extern int	  logfile_line;

void RebootLog_Stat_Init(void)	
{
	FILE *pf = fopen(logfile_name,"r");
	char line_buff[LOG_DESC_LEN*2];
	int line_cnt = 0;
	
	
	RebootLog_start_line_save = 0;
	
	
	if(pf!=NULL)
	{
		line_buff[0] = 0;
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			line_cnt ++;
			if(( strstr(line_buff,Log_Reboot_Title)) != NULL)
			{
				
				//if(tindex == 0||tindex == 1)
				{
					//if(divert_total >1)
					{
						
						//siplog_start_line_save = line_cnt;
						if(strstr(line_buff,"StatClear") != NULL)
						{
							RebootLog_start_line_save = line_cnt;
						}
						
					}
				}
			}
			
			line_buff[0] = 0;	
		}
		fclose(pf);
	}
}

#if 0
void RebootLog_Stat_Clear(void)
{
	RebootLog_start_line_save = logfile_line+1;
	
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T0000^StatClear");	
	API_add_log_item(LOG_Reboot_Level,Log_Reboot_Title,detail,NULL);
}
#endif
void SetN329Reboot_Log(int type, int menu)
{
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T0000^reboot_id=RB4%02d:menu_id=%d",type, menu);
	//printf("Set Reboot Log : %s \n", detail);
	API_add_log_item(LOG_Reboot_Level,Log_Reboot_Title,detail,NULL);
}


int RebootLog_Num;
int *RebootLog_Index_Tb = NULL;

void Create_RebootLog_Index_Tb(void)
{
	int Index_max;
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	
	if(RebootLog_Index_Tb  != NULL)
	{
		free(RebootLog_Index_Tb);
		RebootLog_Index_Tb = NULL;
	}
	RebootLog_Num = 0;
	
	FILE *pf = fopen(logfile_name,"r");

	
	if(pf != NULL)
	{
		Index_max = logfile_line - RebootLog_start_line_save;
		printf("!!!!!!!!!!!!!logfile_line =%d,reboot_start_line_save =%d\n",logfile_line,RebootLog_start_line_save);
		if(Index_max > 0)
		{
			RebootLog_Index_Tb = malloc(sizeof(int)*Index_max);

			if(RebootLog_Index_Tb != NULL)
			{
				while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
				{
					if(++line_cnt >= RebootLog_start_line_save)
					{
						if(strstr(line_buff,Log_Reboot_Title)!= NULL)
						{
							if(strstr(line_buff,"StatClear")== NULL)
							{
								
								RebootLog_Index_Tb[RebootLog_Num++] = line_cnt;

								if(RebootLog_Num >= Index_max)
									break;
							}
						}
					}
					//printf("!!!!!!!!!!!!!rebootLog_Num=%d,line_cnt = %d,%s\n",RebootLog_Num,line_cnt,line_buff);
				}
			}
		}
		fclose(pf);
	}

	printf("!!!!!!!!!!!!!rebootLog_Num=%d,line_cnt = %d\n",RebootLog_Num,line_cnt);
}

void free_RebootLog_Index_Tb(void)
{
	if(RebootLog_Index_Tb  != NULL)
	{
		free(RebootLog_Index_Tb);
		RebootLog_Index_Tb = NULL;
	}
	RebootLog_Num = 0;
}

int Get_RebootLog_Num(void)
{
	return RebootLog_Num;
}

int Get_One_RebootLog(int index,char *log_str)
{
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	char strbuff[LOG_DESC_LEN];
	//int strindex = 0;
	char *pch1,*pch2,*str;
	int result = -1;
	
	if(RebootLog_Index_Tb == NULL || index >= RebootLog_Num)
	{
		return -1;
	}
	str = strbuff;
	
	FILE *pf = fopen(logfile_name,"r");
	index = RebootLog_Num-1-index; 
	if(pf != NULL)
	{
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			if(++line_cnt == RebootLog_Index_Tb[index])
			{
				if(strstr(line_buff,Log_Reboot_Title)!= NULL)
				{
					pch1 = strstr(line_buff," ");
					
					if(pch1 != NULL)
					{
						pch1 += 3;
						memcpy(str,pch1,2);
						str += 2;
						pch1+=2;
						*str++ = '/';
						pch2 = strstr(pch1," ");
						memcpy(str,pch1,pch2-pch1+1);
						str += (pch2-pch1+1);
						pch2++;
						pch1 = strstr(pch2," ");//T0000^
						pch1 += 7;
						pch2 = strstr(pch1," ");

						memcpy(str,pch1,pch2-pch1);
						str += (pch2-pch1);
						*str = 0;
						str = strbuff;
						while(*str)
						{
							if(*str == '_')
							{
								*str = ' ';
							}
							str++;
						}
						strcpy(log_str,strbuff);
					}
					result = 0;
				}
				break;
			}
		}
		fclose(pf);
	}

	return result;
}

