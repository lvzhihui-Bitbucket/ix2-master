/**
  ******************************************************************************
  * @file    obj_BackupAndRestore.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <unistd.h>
#include "obj_BackupAndRestore.h"
#include "obj_TableProcess.h"
#include "obj_ResSync.h"


#define BACKUP_RESTORE_NAND_DIR				"/mnt/nand1-2/Backup/"
#define BACKUP_RESTORE_SD_CARD_DIR			"/mnt/sdcard/Backup/"
#define BACKUP_RESTORE_DIR_NAME				"BackupAndRestore"

#define BACKUP_ADMIN_IO_FILE_NAME					"io_data_admin_backup_table.csv"
#define BACKUP_USER_IO_FILE_NAME					"io_data_user_backup_table.csv"
#define BACKUP_CALL_RECORD_FILE_NAME				"call_record_table.csv"
#define BACKUP_PHOTO_FILE_NAME						"photo"


extern one_vtk_table* ioDataAdminBackupTable;
extern one_vtk_table* ioDataUserBackupTable;



int API_Backup(int ver, int level)
{
	int flag;
	char *backupRestoreDir;
	char cmd[200];
	char fileName[200];
	int i;
	Backup_FILE_NAME_T backupFile;
	

	//自动选择备份目录
	//无SD卡
	if(Judge_SdCardLink() == 0)
	{
		backupRestoreDir = BACKUP_RESTORE_NAND_DIR;
	}
	//有SD卡
	else
	{
		backupRestoreDir = BACKUP_RESTORE_SD_CARD_DIR;
	}

	snprintf(fileName, 200, "%s%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME);	
	//备份文件夹已经存在，则删除文件夹
	DeleteFileProcess(fileName, NULL);

	//创建备份文件夹
	if(MakeDir(fileName) != 0)
	{
		return -1;
	}

	//选择备份内容
	if(level == 0)
	{
		flag = 1;
	}
	else if(level == 1)
	{
		flag = 2;
	}
	else if(level == 2)
	{
		flag = 4;
	}
	else if(level == 3)
	{
		flag = 7;
	}
	else
	{
		return -2;
	}

	//备份工程设置
	if(flag & 0x01)
	{
		snprintf(fileName, 200, "%s%s/%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_ADMIN_IO_FILE_NAME);
		BackupIoDataBackupTable(ioDataAdminBackupTable, fileName);

		//备份RES资源表
		extern const ResReloadTable_T ResReloadTable[];
		extern const int ResReloadTable_num;
		int i;
		for(i = 0; i < ResReloadTable_num; i++)
		{
			if(ResReloadTable[i].act_file_path == NULL)
			{
				continue;
			}
			
			snprintf(cmd, 200, "cp %s %s%s/%s", ResReloadTable[i].act_file_path(), backupRestoreDir, BACKUP_RESTORE_DIR_NAME, ResReloadTable[i].act_file_path+strlen("/mnt/nand1-2/"));
			system(cmd);
		}
		
	}

	//备份用户设置
	if(flag & 0x02)
	{
		snprintf(fileName, 200, "%s%s/%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_USER_IO_FILE_NAME);
		BackupIoDataBackupTable(ioDataUserBackupTable, fileName);
	}

	//备份用户数据
	if(flag & 0x04)
	{
		snprintf(cmd, 200, "cp -r /mnt/nand1-2/%s %s%s", BACKUP_PHOTO_FILE_NAME, backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
		system(cmd);
		snprintf(cmd, 200, "cp /mnt/nand1-2/%s %s%s/%s", BACKUP_CALL_RECORD_FILE_NAME, backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_CALL_RECORD_FILE_NAME);
		system(cmd);
	}

	snprintf(cmd, 200, "cp %s %s%s/%s", IM_COOKIE_TABLE_NAME, backupRestoreDir, BACKUP_RESTORE_DIR_NAME, IM_COOKIE_TABLE_NAME+strlen("/mnt/nand1-2/"));
	system(cmd);
	
	snprintf(cmd, 200, "cp %s %s%s/%s", MON_COOKIE_TABLE_NAME, backupRestoreDir, BACKUP_RESTORE_DIR_NAME, MON_COOKIE_TABLE_NAME+strlen("/mnt/nand1-2/"));
	system(cmd);


	if(1)
	{
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);
		
		snprintf(cmd, 200, "tar -cvf %s/BAK.%s.20%02d%02d%02d%02d%02d.tar -C %s %s/", backupRestoreDir, GetSysVerInfo_BdRmMs(), tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min, backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	}

	/*
	//压缩备份文件夹
	if(ver == 0)
	{
		snprintf(cmd, 200, "tar -zcvf %s/%s_Admin_%s.tar.gz -C %s %s/", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs(), backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	}
	else
	{
		snprintf(cmd, 200, "tar -zcvf %s/%s_User_%s.tar.gz -C %s %s/", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs(), backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	}
	*/

	
	system(cmd);

	//删除备份文件价
	snprintf(cmd, 200, "rm -r %s%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	system(cmd);
	
	sync();
	return 0;
}

int API_RestoreFromBackup(int ver, int level, char* backFile)
{
	int flag;
	char *backupRestoreDir;
	char cmd[200];
	char fileName[200];
	extern const ResReloadTable_T ResReloadTable[];
	extern const int ResReloadTable_num;
	int i;
	
	//房号不对
	if(strstr(backFile, GetSysVerInfo_BdRmMs()) == NULL)
	{
		return -1;
	}

	//选择备份目录
	//无SD卡
	if(Judge_SdCardLink() == 0)
	{
		backupRestoreDir = BACKUP_RESTORE_NAND_DIR;
	}
	//有SD卡
	else
	{
		backupRestoreDir = BACKUP_RESTORE_SD_CARD_DIR;
	}

	snprintf(fileName, 200, "%s%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	
	//有备份目录，则删除
	if(access( fileName, F_OK ) == 0)
	{
		snprintf(cmd, 200, "rm -r %s", fileName);
		system(cmd);
	}

	snprintf(fileName, 200, "%s/%s", backupRestoreDir, backFile);
	if(strstr(backFile, ".tar") != NULL)
	{
		snprintf(cmd, 200, "tar -xvf %s -C %s %s/", fileName, backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	}
	else if(strstr(backFile, ".zip") != NULL)
	{
		snprintf(cmd, 200, "unzip -o %s -d %s", fileName, backupRestoreDir);
	}


/*
	//解压.tar.gz包或者.zip包到 BackupAndRestore目录下
	if(ver == 0)
	{
		snprintf(fileName, 200, "%s/%s_Admin_%s.zip", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs());
		//无zip文件
		if( access( fileName, F_OK ) < 0 )
		{
			snprintf(fileName, 200, "%s/%s_Admin_%s.tar.gz", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs());
			//无tar.gz文件
			if( access( fileName, F_OK ) < 0 )
			{
				return -1;				
			}
			//有tar.gz文件
			else
			{
				snprintf(cmd, 200, "tar -zxvf %s -C %s %s/", fileName, backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
			}
		}
		//有zip文件
		else
		{
			snprintf(cmd, 200, "unzip -o %s -d %s", fileName, backupRestoreDir);
		}
	}
	else
	{
		snprintf(fileName, 200, "%s/%s_User_%s.zip", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs());
		//无zip文件
		if( access( fileName, F_OK ) < 0 )
		{
			snprintf(fileName, 200, "%s/%s_User_%s.tar.gz", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, GetSysVerInfo_BdRmMs());
			//无tar.gz文件
			if( access( fileName, F_OK ) < 0 )
			{
				return -1;				
			}
			//有tar.gz文件
			else
			{
				snprintf(cmd, 200, "tar -zxvf %s -C %s %s/", fileName, backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
			}
		}
		//有zip文件
		else
		{
			snprintf(cmd, 200, "unzip -o %s -d %s", fileName, backupRestoreDir);
		}
	}
*/


	system(cmd);
	
	//恢复工程设置
	snprintf(fileName, 200, "%s%s/%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_ADMIN_IO_FILE_NAME);
	RestoreIoDataBackupTable(fileName);
	//恢复用户设置
	snprintf(fileName, 200, "%s%s/%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_USER_IO_FILE_NAME);
	RestoreIoDataBackupTable(fileName);

	//删除photo
	snprintf(cmd, 200, "rm -r /mnt/nand1-2/%s", BACKUP_PHOTO_FILE_NAME);
	system(cmd);
	
	//删除call record
	snprintf(cmd, 200, "rm /mnt/nand1-2/%s", BACKUP_CALL_RECORD_FILE_NAME);
	system(cmd);

	//恢复photo
	snprintf(cmd, 200, "cp -r %s%s/%s /mnt/nand1-2/", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_PHOTO_FILE_NAME);
	system(cmd);
	
	//恢复call record
	snprintf(cmd, 200, "cp %s%s/%s /mnt/nand1-2/%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, BACKUP_CALL_RECORD_FILE_NAME, BACKUP_CALL_RECORD_FILE_NAME);
	system(cmd);
	
	//api_reload_call_record();

	//恢复RES资源表
	for(i = 0; i < ResReloadTable_num; i++)
	{
		if(ResReloadTable[i].act_file_path == NULL)
		{
			continue;
		}

		snprintf(cmd, 200, "cp %s%s/%s %s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, ResReloadTable[i].act_file_path() + strlen("/mnt/nand1-2/"), ResReloadTable[i].act_file_path());
		system(cmd);
	}

	snprintf(cmd, 200, "cp %s%s/%s %s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, IM_COOKIE_TABLE_NAME + strlen("/mnt/nand1-2/"), IM_COOKIE_TABLE_NAME);
	system(cmd);
	
	snprintf(cmd, 200, "cp %s%s/%s %s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME, MON_COOKIE_TABLE_NAME + strlen("/mnt/nand1-2/"), MON_COOKIE_TABLE_NAME);
	system(cmd);

	snprintf(cmd, 200, "rm -r %s%s", backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
	system(cmd);
	
	sync();

	HardwareRestar();
	return 0;
	
}


int GetBackupFileName(Backup_FILE_NAME_T *backupFile)
{
	char linestr[100];
	int ret = 0;
	char *backupDir;
	char *p;
	int i;

	backupFile->cnt = 0;
	
	//选择备份目录
	//无SD卡
	if(Judge_SdCardLink() == 0)
	{
		backupDir = BACKUP_RESTORE_NAND_DIR;
	}
	//有SD卡
	else
	{
		backupDir = BACKUP_RESTORE_SD_CARD_DIR;
	}
	
	snprintf(linestr, 100, "ls %s\n", backupDir);
	
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		//文件名太长
		if(strlen(linestr) >= 60)
		{
			continue;
		}

		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}

		if(strstr(linestr,".tar") != NULL)
		{
			strcpy(backupFile->name[backupFile->cnt++], linestr);
		}
		else if(strstr(linestr,".zip") != NULL)
		{
			strcpy(backupFile->name[backupFile->cnt++], linestr);
		}
		
		if(backupFile->cnt > 0)
		{
			//printf("name[%d] = %s\n", backupFile->cnt-1, backupFile->name[backupFile->cnt-1]);
		}

		if(backupFile->cnt >= BACKUP_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}


int DeleteBackupFileName(int index)
{
	char *backupRestoreDir;
	char cmd[200];
	char fileName[200];
	int i;
	Backup_FILE_NAME_T backupFile;

	//选择备份目录
	//无SD卡
	if(Judge_SdCardLink() == 0)
	{
		backupRestoreDir = BACKUP_RESTORE_NAND_DIR;
	}
	//有SD卡
	else
	{
		backupRestoreDir = BACKUP_RESTORE_SD_CARD_DIR;
	}

	GetBackupFileName(&backupFile);

	if(index < backupFile.cnt)
	{
		snprintf(cmd, 200, "rm %s%s", backupRestoreDir, backupFile.name[index]);
		system(cmd);
		for(i = index+1; i < backupFile.cnt; i++)
		{
			strcpy(backupFile.name[i-1], backupFile.name[i]);
		}
		backupFile.cnt--;
	}

	return 0;
}

int DeleteAllBackupFile(void)
{
	char *backupRestoreDir;
	char cmd[2000];
	char fileName[200];
	int i;
	Backup_FILE_NAME_T backupFile;

	//选择备份目录
	//无SD卡
	if(Judge_SdCardLink() == 0)
	{
		backupRestoreDir = BACKUP_RESTORE_NAND_DIR;
	}
	//有SD卡
	else
	{
		backupRestoreDir = BACKUP_RESTORE_SD_CARD_DIR;
	}

	GetBackupFileName(&backupFile);

	strcpy(cmd, "rm ");
		
	for(i = 0; i < backupFile.cnt; i++)
	{
		strcat(cmd, backupRestoreDir);
		strcat(cmd, backupFile.name[i]);
		strcat(cmd, " ");
	}

	system(cmd);

	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

