/**
  ******************************************************************************
  * @file    obj_Stm8HeartBeatService.c
  * @author  lyx
  * @version V00.01.00
  * @date    2017.7.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "obj_Stm8HeartBeatService.h"
#include "vdp_uart.h"
#include "obj_ThreadHeartBeatService.h"	//czn_20180109

// lzh_20181025_s
#include "obj_auto_reboot.h"
void HardwareRestar(void);
// lzh_20181025_e

Stm8HeartBeat_t 	stm8HeartBeatRun;

#if 1
// lzh_20181031_s
 #include "task_VideoMenu.h"
static int manual_register_timeout = 0;
void manual_register_init(void)
{
	manual_register_timeout = 0;
}
void manual_register_decrease(void)
{	
	if( manual_register_timeout ) 
	{
		manual_register_timeout--;	
		if( manual_register_timeout == 0 )
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MANUAL_REGISTER_TIMEOUT);
		}
	}
}

void manual_register_enable(void)
{
	pthread_mutex_lock(&stm8HeartBeatRun.lock);
	manual_register_timeout = 25;	
	pthread_mutex_unlock(&stm8HeartBeatRun.lock);
}

void manual_register_disable(void)
{
	pthread_mutex_lock(&stm8HeartBeatRun.lock);
	manual_register_timeout = 0;	
	pthread_mutex_unlock(&stm8HeartBeatRun.lock);
}

int get_manual_register_count(void)
{	
	int counter;
	pthread_mutex_lock(&stm8HeartBeatRun.lock);
	counter = manual_register_timeout;
	pthread_mutex_unlock(&stm8HeartBeatRun.lock);
	return counter;
}

// lzh_20181031_e
#endif

//������������ʼ��
void Stm8HeartBeatServiceInit(void)
{

	pthread_mutex_init( &stm8HeartBeatRun.lock, 0);
	
	pthread_mutex_lock(&stm8HeartBeatRun.lock);
	
	stm8HeartBeatRun.sendTimeCnt = 0;
	stm8HeartBeatRun.ThreadHeartbeat_Age = 0;

	OS_CreateTimer(&stm8HeartBeatRun.HBtimer, Stm8HeartBeatServiceTimerCallback, 1000/25);
	OS_StartTimer(&stm8HeartBeatRun.HBtimer);

	api_uart_send_pack(UART_TYPE_N2S_LINKING, NULL, NULL);       //��ʼ�ȷ�һ������
	
	api_uart_send_pack(UART_TYPE_STM8_VERSION_GET, NULL, 0);      

	pthread_mutex_unlock(&stm8HeartBeatRun.lock);

	// lzh_20181025_s
	auto_reboot_init(HardwareRestar);
	// lzh_20181025_e

	// lzh_20181031_s
	manual_register_init();
	// lzh_20181031_e
	NetworkCardCheckInit();
	NetworkWLANCheckInit();

}

//��������ʱ����������
void Stm8HeartBeatServiceTimerCallback(void)
{
	pthread_mutex_lock(&stm8HeartBeatRun.lock);

	stm8HeartBeatRun.sendTimeCnt++;

	if( stm8HeartBeatRun.sendTimeCnt >= SEND_HEART_BEAT_TIME )    		//ÿ15�뷢��һ������
	{
		OneHourSaveDateAndTime();	//cao_20181025
		stm8HeartBeatRun.sendTimeCnt = 0;
		#if 0		//czn_20180109
		SendHeartBeatToStm8();
		#else
		SendAllThreadHeartbeatApply(SendHeartBeatToStm8);
		#endif
		if(stm8HeartBeatRun.ThreadHeartbeat_Age++ >= 3)
		{
			dprintf("HardwareRestar\n");
			HardwareRestar();
		}

		// lzh_20181025_s
		auto_reboot_polling();
		// lzh_20181025_e

		InternetTimeAutoUpdate();

		timer_EixtInstaller_callback();
		CPU_Ratio_Measure();
		GetMemAvailable();

	}

	// lzh_20181031_s
	manual_register_decrease();
	// lzh_20181031_e

	NetworkCardCheckTimerCallback();

	video_client_frame_rate_update();
	
	OS_RetriggerTimer(&stm8HeartBeatRun.HBtimer);


	pthread_mutex_unlock(&stm8HeartBeatRun.lock);
}


//�������������
void SendHeartBeatToStm8(void)
{
	api_uart_send_pack(UART_TYPE_STM8_VERSION_GET, NULL, 0);      
	api_uart_send_pack(UART_TYPE_N2S_LINKING, NULL, NULL);  
	stm8HeartBeatRun.ThreadHeartbeat_Age = 0;
}

int SoftRestar(void)
{
	if(IsMyProxyLinked())
	{
		SetIxProxyRebootFlag(1);
		return -1;
	}
	else
	{
		dprintf("----------------------------I will restart in 2 seconds!!!!!!!\n");
		API_Event_NameAndMsg("EventWillReboot", "SoftRestar");
		sync();
		usleep(2000*1000);
		WriteRebootFlag(NULL, 0);
		api_notify_global_reset();
		usleep(100*1000);
		api_notify_global_reset();
		usleep(100*1000);
		SetIxProxyRebootFlag(0);
		return system("reboot");
	}
}

void HardwareRestar()
{
	if(IsMyProxyLinked())
	{
		SetIxProxyRebootFlag(1);
	}
	else
	{
		API_Event_NameAndMsg("EventWillReboot", "HardwareRestar");
		WriteRebootFlag(NULL, 0);
		api_notify_global_reset();
		usleep(100*1000);
		api_notify_global_reset();
		SetIxProxyRebootFlag(0);
		usleep(100*1000);
		return system("reboot");
	}
}

void ReceiveRestarNotify()
{
	UpdateLocalTimeToStm8l();
	SaveDateAndTimeToFile();
}

//dh_20200303_s
void SendRebootLogToStm8(int type, int menu)
{
	unsigned char buff[2];
	buff[0] = type;
	buff[1] = menu;
	api_uart_send_pack(UART_TYPE_SEND_REBOOT_LOG, buff, 2);      
}

//dh_20200303_e

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

