/**
  ******************************************************************************
  * @file    obj_IPHeartBeatService.h
  * @author  cao
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_ThreadHeartBeatService_H
#define _obj_ThreadHeartBeatService_H

#include "utility.h"

#define Linphone_Heartbeat_Mask		0x01
#define Menu_Heartbeat_Mask			0x02
#define Survey_Heartbeat_Mask			0x04
#define CallServer_Heartbeat_Mask		0x08

#if !defined(PID_IXSE)
#define AllThread_Heartbeat_Mask	(Menu_Heartbeat_Mask|Survey_Heartbeat_Mask|CallServer_Heartbeat_Mask|Linphone_Heartbeat_Mask)
#else
#define AllThread_Heartbeat_Mask	(Menu_Heartbeat_Mask|Survey_Heartbeat_Mask|CallServer_Heartbeat_Mask)
#endif
typedef  void (*ThreadHeartbeat_Callback_Def)(void); 

void SendAllThreadHeartbeatApply(ThreadHeartbeat_Callback_Def Finish_Callback);
void RecvThreadHeartbeatReply(int Mask);

#endif


