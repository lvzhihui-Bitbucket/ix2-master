/**
  ******************************************************************************
  * @file    obj_IPHeartBeatService.c
  * @author  czb
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "obj_IPHeartBeatService.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_SYS_VER_INFO.h"


#define HEART_BEAT_DATA_PATH			"/mnt/nand1-2/HeartBeatServiceData"
#define REBOOT_MAX_TIMES				3
#define REBOOT_MAX_TIMEOUT				240


IPHeartBeat_t ipHeartBeatRun;

//心跳服务对象初始化
void IPHeartBeatServiceInit(void)
{
	pthread_mutex_init( &ipHeartBeatRun.lock, 0);
	
	pthread_mutex_lock(&ipHeartBeatRun.lock);
	
	ipHeartBeatRun.state = STATE_SEND_HEART_BEAT;
	ipHeartBeatRun.sendCnt = 0;
	ipHeartBeatRun.receiveCnt = 0;
	ipHeartBeatRun.rebootCnt = ReadHeartBeatRebootTimes();

	OS_CreateTimer(&ipHeartBeatRun.HBtimer, IPHeartBeatServiceTimerCallback, 1000/25);
	OS_StartTimer(&ipHeartBeatRun.HBtimer);

	pthread_mutex_unlock(&ipHeartBeatRun.lock);
}

//心跳服务定时器处理函数
void IPHeartBeatServiceTimerCallback(void)
{
	pthread_mutex_lock(&ipHeartBeatRun.lock);
	
	OS_RetriggerTimer(&ipHeartBeatRun.HBtimer);

	switch(ipHeartBeatRun.state)
	{
		case STATE_SEND_HEART_BEAT:
			if(++ipHeartBeatRun.sendCnt >= GetSysVerInfo().id)
			{
				SendHeartBeat();
				ipHeartBeatRun.receiveCnt = 0;
				ipHeartBeatRun.state = STATE_WAIT_HEART_BEAT;
			}
			break;
			
		case STATE_WAIT_HEART_BEAT:
			if(++ipHeartBeatRun.receiveCnt >= REBOOT_MAX_TIMEOUT)
			{
				if((ipHeartBeatRun.rebootCnt < REBOOT_MAX_TIMES) && (GetSysVerInfo().id != 1))
				{
					WriteHeartBeatRebootTimes(ipHeartBeatRun.rebootCnt + 1);
					HeartBeatServiceDeviceReboot();
				}
				else
				{
					ipHeartBeatRun.sendCnt = 0;
					ipHeartBeatRun.state = STATE_SEND_HEART_BEAT;
				}
			}
			break;
	}
	
	pthread_mutex_unlock(&ipHeartBeatRun.lock);
}

//读取重启次数
int ReadHeartBeatRebootTimes(void)
{
	int times = 0;

	FILE *file;
	char line[100] = {0};
	char* ptr  = NULL;
	
	if( ((file=fopen(HEART_BEAT_DATA_PATH,"r"))==NULL) )
	{
		return times;
	}

	//read line
	while( fgets(line,100,file) != NULL)
	{	
		if( (ptr = strstr(line,"#HeartBeatRebootTimes = ")) == NULL )
		{
			memset(line, 0, 100);
		}
		else
		{
			ptr += strlen("#HeartBeatRebootTimes = ");
			times = atoi(ptr);
			break;
		}
	}
	
	fclose( file );	
	
	return times;
}

//写重启次数到文件
void WriteHeartBeatRebootTimes(int times)
{
	FILE* file = NULL;

	file = fopen(HEART_BEAT_DATA_PATH, "w+");
	if( file == NULL )
	{
		printf( "open %s error.\n", HEART_BEAT_DATA_PATH);
		return;
	}
	
	fprintf(file, "#HeartBeatRebootTimes = %d\r\n", times);

	fclose(file);

	sync();
}

//发送心跳服务包
void SendHeartBeat(void)
{
	uint16 nodeId;
	
	nodeId = htons(GetSysVerInfo().id);
	
	api_udp_device_update_send_data(inet_addr("236.6.6.1"), htons(DEVICE_IP_HEART_BEAT_SERVICE), (char*)&nodeId, 2);
}

//接收到心跳服务包
void ReceiveHeartBeat(void)
{
	pthread_mutex_lock(&ipHeartBeatRun.lock);

	ipHeartBeatRun.sendCnt = 0;
	ipHeartBeatRun.state = STATE_SEND_HEART_BEAT;
	
	if(ipHeartBeatRun.rebootCnt)
	{
		ipHeartBeatRun.rebootCnt = 0;
		WriteHeartBeatRebootTimes(ipHeartBeatRun.rebootCnt);
	}
	
	pthread_mutex_unlock(&ipHeartBeatRun.lock);
}

//设备重启
void HeartBeatServiceDeviceReboot(void)
{	
	SoftRestar();
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

