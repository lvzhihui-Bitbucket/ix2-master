
/**
  ******************************************************************************
  * @file    obj_FileManage.c
  * @author  cao
  * @version V00.01.00
  * @date    2023.11.29
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "define_file.h"
#include "obj_FileManageUtility.h"


//删除临时文件
void API_DeleteTempFile(void)
{
    if(IsFileExist(REBOOT_FLAG_PATH_OLD))
    {
        MoveFile(REBOOT_FLAG_PATH_OLD, REBOOT_FLAG_PATH);
    }

    //如果没插卡，又存在SDcard目录，则删除SDcard目录
    Judge_SdCardLink();
    
    DeleteFileProcess(TEMP_Folder, "*");
    DeleteFileProcess(NAND_DOWNLOAD_PATH, "*");
}


//删除录像文件
void API_DeletePhoto(void)
{
	DeleteFileProcess(JPEG_STORE_DIR, "*");
}
