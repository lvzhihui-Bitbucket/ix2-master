
/**
  ******************************************************************************
  * @file    obj_NDM.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include <sys/sysinfo.h>
#include "obj_Certificate.h"
#include "define_file.h"
#include "cJSON.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "task_Event.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"
#include "obj_NDM.h"

static pthread_mutex_t	NDM_S_Lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t	NDM_C_Lock = PTHREAD_MUTEX_INITIALIZER;


//sOrc : 1--server, 0--client
static int API_Event_NDM_Ctrl(int sOrc, const char* cmd)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, sOrc ? EventNDM_S_Ctrl : EventNDM_C_Ctrl);
	cJSON_AddStringToObject(event, "CMD", cmd);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

static int API_Event_NDM_Remote_C_Ctrl(int ip, char* cmd)
{
	int ret;
	char* ipString;
	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventNDM_C_Ctrl);
	cJSON_AddStringToObject(event, "CMD", cmd);
	if(cmd && !strcmp(cmd, NDM_C_CTRL_START))
	{
		ipString = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip));
		cJSON_AddStringToObject(event, NDM_S_MFG_SN, GetSysVerInfo_Sn());
		cJSON_AddStringToObject(event, NDM_S_IP_ADDR, ipString);
		cJSON_AddNumberToObject(event, NDM_C_TIME, API_Para_Read_Int(NDM_C_TIME));
		cJSON_AddItemToObject(event, NDM_C_CHECK, cJSON_Duplicate(API_Para_Read_Public(NDM_C_CHECK), 1));
		cJSON_AddStringToObject(event, "SOURCE_IP", ipString);
	}
	ret = API_XD_EventJson(ip, event);
	cJSON_Delete(event);

	return ret;
}


static int NDM_Log(int ip, const cJSON* result, char* info)
{
	int ret;
	char temp[100];
	cJSON* log = cJSON_CreateObject();
	cJSON_AddStringToObject(log, "DATE", GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
	cJSON_AddStringToObject(log, "IP_ADDR", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	cJSON_AddStringToObject(log, "MFG_SN", GetSysVerInfo_Sn());
	cJSON_AddStringToObject(log, "IX_ADDR", GetSysVerInfo_BdRmMs());
	cJSON_AddItemToObject(log, "NDM_RESULT", result ? cJSON_Duplicate(result, 1) : cJSON_CreateNull());
	cJSON_AddStringToObject(log, "INFO", info ? info : "");

	ret = API_TB_AddByName(TB_NAME_NDM_Log, log);
	cJSON_Delete(log);

	return ret;
}

cJSON* API_GetRemotePbByArray(int ip, const cJSON* pbNameArray)
{
	cJSON* retJson = NULL;
	cJSON* element;
	if(cJSON_IsArray(pbNameArray))
	{
		cJSON *view = cJSON_CreateObject();

		cJSON_ArrayForEach(element, pbNameArray)
		{
			if(cJSON_IsString(element) && element->valuestring)
			{
				cJSON_AddStringToObject(view, element->valuestring, "PB");
			}
		}
		retJson = API_GetPbIo(ip, NULL, view);
		cJSON_Delete(view);
	}
	return retJson;
}

static int NDM_C_Response(int ip, char* cmd, char* responseString)
{
	int ret;
	char temp[100];
	cJSON* responseEvent = cJSON_CreateObject();
	cJSON_AddStringToObject(responseEvent, EVENT_KEY_EventName, EventNDM_C_CtrlResponse);
	cJSON_AddStringToObject(responseEvent, "IP_ADDR", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	cJSON_AddStringToObject(responseEvent, "MFG_SN", GetSysVerInfo_Sn());
	cJSON_AddStringToObject(responseEvent, "IX_ADDR", GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(responseEvent, PB_NDM_C_STATE, API_PublicInfo_Read_String(PB_NDM_C_STATE));
	
	snprintf(temp, 100, "%s %s", cmd, responseString);
	cJSON_AddStringToObject(responseEvent, "NDM_C_CTRL_RESULT", temp);

	ret = API_XD_EventJson(ip, responseEvent);
	cJSON_Delete(responseEvent);

	return ret;
}

static int NDM_S_Log(const char* date, const char* ipString, const char* sn, const char* connect, cJSON* pb)
{
	int ret;
	char temp[100];
	cJSON* log = cJSON_CreateObject();
	cJSON_AddStringToObject(log, "DATE", date);
	cJSON_AddStringToObject(log, "IP_ADDR", ipString);
	cJSON_AddStringToObject(log, "MFG_SN", sn);
	cJSON_AddStringToObject(log, "NDM_CONN", connect);
	cJSON_AddItemToObject(log, "NDM_ITEM", pb ? pb : cJSON_CreateNull());

	ret = API_TB_AddByName(TB_NAME_NDM_S_Log, log);
	cJSON_Delete(log);
	return ret;
}


static int NDM_S_CheckCallback(int time)
{
	API_Event_NDM_Ctrl(1, NDM_S_CTRL_CHECK);
	return 1;
}

static int NDM_C_CheckCallback(int time)
{
	API_Event_NDM_Ctrl(0, NDM_C_CTRL_CHECK);
	return 1;
}

int NDM_Init(void)
{
	if(API_Para_Read_Int(NDM_S_BOOT))
	{
		API_Event_NDM_Ctrl(1, NDM_S_CTRL_START);
	}
	else
	{
		API_Event_NDM_Ctrl(1, NDM_S_CTRL_STOP);
	}

	if(API_Para_Read_Int(NDM_C_BOOT))
	{
		API_Event_NDM_Ctrl(0, NDM_C_CTRL_BOOT);
	}
}


static int NDM_S_StartList(void)
{
	cJSON* element;
	cJSON* NDM_State = NULL;
	char* ipString;
	char* snString;
	char* stateString;
	char* tempState;
	DeviceInfo info;

	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		cJSON_AddStringToObject(Where, NDM_RES_MFG_SN, "");
		cJSON_ArrayForEach(element, View)
		{
			//获坖所有客户端的状思
			ipString = GetEventItemString(element, NDM_RES_IP_ADDR);
			stateString = GetEventItemString(element, NDM_RES_NDM_State);
			snString = GetEventItemString(element, NDM_RES_MFG_SN);
			if(!API_GetInfoByIp(inet_addr(ipString), 1, &info))
			{
				if(!strcmp(snString, info.MFG_SN))
				{
					NDM_State = API_GetRemotePb(inet_addr(ipString), PB_NDM_C_STATE);
					if(cJSON_IsString(NDM_State))
					{
						tempState = NDM_State->valuestring;
					}
					else
					{
						tempState = NDM_C_STATE_NotSupported;
					}
				}
				else
				{
					tempState = NDM_C_STATE_IP_CHANGE;
				}
			}
			else
			{
				tempState = NDM_C_STATE_OFFLINE;
			}

			if(strcmp(stateString, tempState))
			{
				cJSON_ReplaceItemInObjectCaseSensitive(element, NDM_RES_NDM_State, cJSON_CreateString(tempState));
				cJSON_ReplaceItemInObjectCaseSensitive(Where, NDM_RES_MFG_SN, cJSON_CreateString(snString));
				API_TB_UpdateByName(TB_NAME_NDM_Register, Where, element);
			}

			//通知开启客户端，同步参数过去
			if(strcmp(tempState, NDM_C_STATE_OFFLINE) && strcmp(tempState, NDM_C_STATE_NotSupported))
			{
				API_Event_NDM_Remote_C_Ctrl(inet_addr(ipString), NDM_C_CTRL_START);
			}

			if(NDM_State)
			{
				cJSON_Delete(NDM_State);
				NDM_State = NULL;
			}
			usleep(100*1000);
		}

		sleep(1);

		cJSON* pbResult;
		char curTime[100];

		GetCurrentTime(curTime, 100, "%Y-%m-%d %H:%M:%S");
		cJSON_ArrayForEach(element, View)
		{
			//获坖所有客户端的状思
			stateString = GetEventItemString(element, NDM_RES_NDM_State);
			ipString = GetEventItemString(element, NDM_RES_IP_ADDR);
			snString = GetEventItemString(element, NDM_RES_MFG_SN);
			if(!strcmp(stateString, NDM_C_STATE_OFFLINE))
			{
				NDM_S_Log(curTime, ipString, snString, NDM_C_STATE_OFFLINE, cJSON_CreateNull());
			}
			else if(!strcmp(stateString, NDM_C_STATE_NotSupported))
			{
				NDM_S_Log(curTime, ipString, snString, NDM_C_STATE_NotSupported, cJSON_CreateNull());
			}
			else
			{
				NDM_State = NULL;
				//如果状态是关的，刷新状态。
				if(!strcmp(stateString, NDM_C_STATE_OFF))
				{
					NDM_State = API_GetRemotePb(inet_addr(ipString), PB_NDM_C_STATE);
					if(cJSON_IsString(NDM_State))
					{
						tempState = NDM_State->valuestring;
						cJSON_ReplaceItemInObjectCaseSensitive(element, NDM_RES_NDM_State, cJSON_CreateString(tempState));
						cJSON_ReplaceItemInObjectCaseSensitive(Where, NDM_RES_MFG_SN, cJSON_CreateString(snString));
						API_TB_UpdateByName(TB_NAME_NDM_Register, Where, element);
						stateString = GetEventItemString(element, NDM_RES_NDM_State);
					}
				}

				pbResult = API_GetRemotePbByArray(inet_addr(ipString), API_Para_Read_Public(NDM_S_GET_PB));
				NDM_S_Log(curTime, ipString, snString, stateString, pbResult);

				if(NDM_State)
				{
					cJSON_Delete(NDM_State);
					NDM_State = NULL;
				}
			}
		}
	}

	cJSON_Delete(Where);
	cJSON_Delete(View);

	return 1;
}

static int NDM_S_StopList(void)
{
	cJSON* element;
	char* ipString;
	char* snString;
	char* stateString;

	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(Where, NDM_RES_ENABLE);
		cJSON_AddStringToObject(Where, NDM_RES_MFG_SN, "");
		cJSON_ArrayForEach(element, View)
		{
			ipString = GetEventItemString(element, NDM_RES_IP_ADDR);
			stateString = GetEventItemString(element, NDM_RES_NDM_State);
			snString = GetEventItemString(element, NDM_RES_MFG_SN);

			//状思还开坯的，坑指令关闭
			if(!strcmp(stateString, NDM_C_STATE_ON))
			{
				API_Event_NDM_Remote_C_Ctrl(inet_addr(ipString), NDM_C_CTRL_STOP);
			}

			//状思还没关的设备，关闭
			if(strcmp(stateString, NDM_C_STATE_OFF))
			{
				cJSON_ReplaceItemInObjectCaseSensitive(element, NDM_RES_NDM_State, cJSON_CreateString(NDM_C_STATE_OFF));
				cJSON_ReplaceItemInObjectCaseSensitive(Where, NDM_RES_MFG_SN, cJSON_CreateString(snString));
				API_TB_UpdateByName(TB_NAME_NDM_Register, Where, element);
			}
		}
	}
	cJSON_Delete(Where);
	cJSON_Delete(View);

	return 1;
}


static cJSON* NDM_C_Check(const cJSON* option)
{
	char info[200];
	cJSON* element;
	char* ipString;
	char* snString;
	cJSON* result = cJSON_CreateObject();
	int failFlag = 0;
	int ip, ret;

	snString = API_Para_Read_String2(NDM_S_MFG_SN);
	ipString = API_Para_Read_String2(NDM_S_IP_ADDR);
	ip = inet_addr(ipString);

	cJSON_ArrayForEach(element, option)
	{
		if(cJSON_IsString(element))
		{
			if(!strcmp(element->valuestring, NDM_C_CHECK_UDP))
			{
				if(!API_GetIpByMFG_SN(snString,  1, NULL, NULL))
				{
					cJSON_AddItemToObject(result, NDM_C_CHECK_UDP, cJSON_CreateTrue());
				}
				else
				{
					cJSON_AddItemToObject(result, NDM_C_CHECK_UDP, cJSON_CreateFalse());
					failFlag = 1;
				}
			}
			else if(!strcmp(element->valuestring, NDM_C_CHECK_PING))
			{
				float pingTime = -1;
				if(PingNetwork2(ipString, &pingTime) == -1)
				{
					failFlag = 2;
				}
				cJSON_AddNumberToObject(result, NDM_C_CHECK_PING, pingTime);
			}
		}
	}

	if(failFlag)
	{
		NDM_Log(ip, result, "Check error");
	}

	ret = API_GetRemotePbString(ip, PB_NDM_S_STATE, info);
	//朝务器丝在线
	if(ret == 0)
	{
		NDM_Log(ip, result, "Server offline");
	}
	//朝务器丝支挝
	else if(ret == -1)
	{
		API_Event_NDM_Ctrl(0, NDM_C_CTRL_STOP);
		NDM_Log(ip, result, "Server not supported");
	}
	//朝务器在线
	else if(ret == 1)
	{
		//朝务器开坯
		if(!strcmp(info, NDM_S_STATE_ON))
		{
			cJSON* View = cJSON_CreateArray();
			cJSON* Where = cJSON_CreateObject();
			cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());
			cJSON_AddStringToObject(Where, NDM_RES_MFG_SN, GetSysVerInfo_Sn());
			//列表里面有
			if(API_RemoteTableSelect(ip, TB_NAME_NDM_Register, View, Where, 0))
			{
				//不写服务器端的表了
			}
			//列表里面没有
			else
			{
				NDM_Log(ip, result, "Server list clear");
				API_Event_NDM_Ctrl(0, NDM_C_CTRL_STOP);
			}
		}
		//朝务器关闭
		else
		{
			NDM_Log(ip, result, "Server stop");
			API_Event_NDM_Ctrl(0, NDM_C_CTRL_STOP);
		}
	}

	return result;
}

//朝务器控制回调函数
int NDM_S_CtrlCallback(cJSON *cmd)
{
	char* cmdString = GetEventItemString(cmd, "CMD");
	char* myServerState;

	pthread_mutex_lock( &NDM_S_Lock);

	if(!strcmp(cmdString, NDM_S_CTRL_START))
	{
		int time = GetEventItemInt(cmd, NDM_S_TIME);
		API_PublicInfo_Write_String(PB_NDM_S_STATE, NDM_S_STATE_ON);

		API_Add_TimingCheck(NDM_S_CheckCallback, time ? time : API_Para_Read_Int(NDM_S_TIME));
		NDM_S_StartList();
		API_Event_By_Name(EventNDM_S_CheckEnd);
	}
	else if(!strcmp(cmdString, NDM_S_CTRL_STOP))
	{
		API_PublicInfo_Write_String(PB_NDM_S_STATE, NDM_S_STATE_OFF);
		API_Del_TimingCheck(NDM_S_CheckCallback);
		NDM_S_StopList();
	}
	else if(!strcmp(cmdString, NDM_S_CTRL_CHECK))
	{
		myServerState = API_PublicInfo_Read_String(PB_NDM_S_STATE);
		if(myServerState && (!strcmp(myServerState, NDM_S_STATE_ON)))
		{
			NDM_S_StartList();
			API_Event_By_Name(EventNDM_S_CheckEnd);
		}
	}

	pthread_mutex_unlock( &NDM_S_Lock);

	return 1;
}

//朝务器控制cmd: "NDM_START"/"NDM_STOP"/"NDM_CHECK"；time:定时时间坕佝秒
int API_NDM_S_Ctrl(char* cmd, int time)
{
	if(cmd == NULL)
	{
		return 0;
	}
	int ret = 1;

	pthread_mutex_lock( &NDM_S_Lock);

	if(!strcmp(cmd, NDM_S_CTRL_START))
	{
		API_PublicInfo_Write_String(PB_NDM_S_STATE, NDM_S_STATE_ON);
		API_Add_TimingCheck(NDM_S_CheckCallback, time ? time : API_Para_Read_Int(NDM_S_TIME));
		NDM_S_StartList();
		API_Event_By_Name(EventNDM_S_CheckEnd);
	}
	else if(!strcmp(cmd, NDM_S_CTRL_STOP))
	{
		API_PublicInfo_Write_String(PB_NDM_S_STATE, NDM_S_STATE_OFF);
		API_Del_TimingCheck(NDM_S_CheckCallback);
		NDM_S_StopList();
		API_Event_By_Name(EventNDM_S_CheckEnd);
	}
	else if(!strcmp(cmd, NDM_S_CTRL_CHECK))
	{
		char* myServerState = API_PublicInfo_Read_String(PB_NDM_S_STATE);
		if(myServerState && (!strcmp(myServerState, NDM_S_STATE_ON)))
		{
			NDM_S_StartList();
			API_Event_By_Name(EventNDM_S_CheckEnd);
		}
	}
	else
	{
		ret = 0;
	}

	pthread_mutex_unlock( &NDM_S_Lock);

	return ret;
}

//客户端控制回调函数
int NDM_C_CtrlCallback(cJSON *cmd)
{
	char* cmdString;
	cJSON* checkOption = NULL;
	cJSON* sourceIp = NULL;
	char* responseString = "success";

	cmdString = GetEventItemString(cmd, "CMD");
	pthread_mutex_lock( &NDM_C_Lock);

	if(!strcmp(cmdString, NDM_C_CTRL_START))
	{
		if(cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_MFG_SN) && cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_IP_ADDR))
		{
			API_PublicInfo_Write_String(PB_NDM_C_STATE, NDM_C_STATE_ON);
			API_Para_Write_Int(NDM_C_BOOT, 1);
			cJSON* ioValue = cJSON_CreateObject();
			cJSON_AddItemToObject(ioValue, NDM_S_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_MFG_SN), 1));
			cJSON_AddItemToObject(ioValue, NDM_S_IP_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_IP_ADDR), 1));
			cJSON_AddItemToObject(ioValue, NDM_C_CHECK, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_C_CHECK), 1));
			cJSON_AddItemToObject(ioValue, NDM_C_TIME, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_C_TIME), 1));
			API_Para_Write_Public(ioValue);
			cJSON_Delete(ioValue);
			API_Add_TimingCheck(NDM_C_CheckCallback, API_Para_Read_Int(NDM_C_TIME));
			checkOption = API_Para_Read_Public(NDM_C_CHECK);
		}
		else
		{
			responseString = "data error";
		}
	}
	else if(!strcmp(cmdString, NDM_C_CTRL_STOP))
	{
		API_PublicInfo_Write_String(PB_NDM_C_STATE, NDM_C_STATE_OFF);
		API_Para_Write_Int(NDM_C_BOOT, 0);
		API_Del_TimingCheck(NDM_C_CheckCallback);
	}
	else if(!strcmp(cmdString, NDM_C_CTRL_BOOT))
	{
		API_PublicInfo_Write_String(PB_NDM_C_STATE, NDM_C_STATE_ON);
		API_Add_TimingCheck(NDM_C_CheckCallback, API_Para_Read_Int(NDM_C_TIME));
		checkOption = API_Para_Read_Public(NDM_C_CHECK);
	}
	else if(!strcmp(cmdString, NDM_C_CTRL_CHECK))
	{
		cJSON* ioValue = cJSON_CreateObject();
		cJSON_AddItemToObject(ioValue, NDM_S_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_MFG_SN), 1));
		cJSON_AddItemToObject(ioValue, NDM_S_IP_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_S_IP_ADDR), 1));
		cJSON_AddItemToObject(ioValue, NDM_C_CHECK, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, NDM_C_CHECK), 1));
		API_Para_Write_Public(ioValue);
		cJSON_Delete(ioValue);
		checkOption = API_Para_Read_Public(NDM_C_CHECK);
	}
	else
	{
		responseString = "error";
	}
	
	if(checkOption)
	{
		char temp[100];
		cJSON* checkResult = NDM_C_Check(checkOption);
		cJSON_AddStringToObject(checkResult, NDM_C_CHECK_DATE, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
		API_PublicInfo_Write(PB_NDM_C_RESULT, checkResult);
		cJSON_Delete(checkResult);
	}

	sourceIp = cJSON_GetObjectItemCaseSensitive(cmd, "SOURCE_IP");
	if(cJSON_IsString(sourceIp))
	{
		NDM_C_Response(inet_addr(sourceIp->valuestring), cmdString, responseString);
	}
	
	pthread_mutex_unlock(&NDM_C_Lock);

	return 1;
}

//客户端应答朝务器的回调函数
int NDM_C_ResponeCallback(cJSON *cmd)
{
	cJSON* element;
	char* snString = GetEventItemString(cmd, "MFG_SN");
	char* ndmCState = GetEventItemString(cmd, PB_NDM_C_STATE);
	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddStringToObject(Where, NDM_RES_MFG_SN, snString);
	cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());

	pthread_mutex_lock( &NDM_S_Lock);
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		cJSON_ArrayForEach(element, View)
		{
			cJSON_ReplaceItemInObjectCaseSensitive(element, NDM_RES_NDM_State, cJSON_CreateString(ndmCState));
			API_TB_UpdateByName(TB_NAME_NDM_Register, Where, element);
		}
	}
	pthread_mutex_unlock(&NDM_S_Lock);

	cJSON_Delete(View);
	cJSON_Delete(Where);
	return 1;
}