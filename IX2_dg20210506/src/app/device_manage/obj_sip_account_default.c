/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_SYS_VER_INFO.h"
#include "obj_Get_Ph_MFG_SN.h"
#include "obj_TableProcess.h"
#include "vdp_IoServer_State.h"

#define	SIP_ACCOUNT_LENGTH		12
#define	SIP_PASSWORD_LENGTH		5

typedef struct
{
	int update_flag;
	char local_acc[SIP_ACCOUNT_LENGTH+1];
	char local_pw[SIP_PASSWORD_LENGTH+1];
	char divert_acc[SIP_ACCOUNT_LENGTH+1];
	char divert_pw[SIP_PASSWORD_LENGTH+1];
}Sip_Acc_Default_Stru;

Sip_Acc_Default_Stru Sip_Acc_Default = {.update_flag = 0};

int api_create_default_sip_info(void)
{
	unsigned char md5[16];
	unsigned short password;
	
	SYS_VER_INFO_T sysInfo;

	memset(&Sip_Acc_Default,0,sizeof(Sip_Acc_Default_Stru));
	
	sysInfo = GetSysVerInfo();
	
	strncpy(Sip_Acc_Default.local_acc,sysInfo.sn,SIP_ACCOUNT_LENGTH+1);

	StringMd5_Calculate(Sip_Acc_Default.local_acc,md5);
	password = (md5[14]<<8) | md5[15];

	snprintf(Sip_Acc_Default.local_pw,SIP_PASSWORD_LENGTH+1,"%d",password);

	strncpy(Sip_Acc_Default.divert_acc,sysInfo.sn,SIP_ACCOUNT_LENGTH+1);
	Sip_Acc_Default.divert_acc[1] = 'e';
	
	StringMd5_Calculate(Sip_Acc_Default.divert_acc,md5);
	password = (md5[14]<<8) | md5[15];

	snprintf(Sip_Acc_Default.divert_pw,SIP_PASSWORD_LENGTH+1,"%d",password);
	
	Sip_Acc_Default.update_flag = 1;
	return 0;
}

int api_get_default_sip_local_username( char* username)
{
	if(Sip_Acc_Default.update_flag == 0)
	{
		*username = 0;
		return -1;
	}
	
	strcpy(username,Sip_Acc_Default.local_acc);
	
	return 0;
}

int api_get_default_sip_local_password( char* password)
{
	if(Sip_Acc_Default.update_flag == 0)
	{
		*password = 0;
		return -1;
	}
	
	strcpy(password,Sip_Acc_Default.local_pw);
	
	return 0;
}

int api_get_default_sip_divert_username( char* username)
{
	if(Sip_Acc_Default.update_flag == 0)
	{
		*username = 0;
		return -1;
	}
	
	strcpy(username,Sip_Acc_Default.divert_acc);
	
	return 0;
}

int api_get_default_sip_divert_password( char* password)
{
	if(Sip_Acc_Default.update_flag == 0)
	{
		*password= 0;
		return -1;
	}
	
	strcpy(password,Sip_Acc_Default.divert_pw);
	
	return 0;
}

// lzh_20170420_e
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

