/**
  ******************************************************************************
  * @file    obj_RequestSn.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Get_Ph_MFG_SN_H
#define _obj_Get_Ph_MFG_SN_H

#include "task_survey.h"

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐
typedef struct 
{
	VDP_MSG_HEAD	head;
	unsigned char 	chipId[6];
} GET_CHIP_ID_T;

#pragma pack()

typedef enum
{
	SN_Type_NONE = 0,
	SN_Type_DS2411,
	SN_Type_Tiny1616,
	SN_Type_Rand,
} SN_Type_e;

typedef struct                         
{
   unsigned char family_code;			//8bit
   unsigned char serial_number[6];		//48bit
   unsigned char crc_code;				//8bit
}DS2411_SN;

typedef struct                         
{
  char            serial_number[6];		//48bit
  short           checkCode;				  //16bit
  SN_Type_e       type;               //获取序列号类型
	pthread_mutex_t	lock;	              //并发处理数据保护锁
}MY_SN;

// Define Object Function - Public----------------------------------------------
void GetPhMFGSnInit(void);
int API_GetLocalSn(char* pSn);
uint8 API_SetLocalSn(char* pSn);

uint16 API_GetLocalIpNodeId(void);
uint8 API_SetLocalIpNodeId(uint16 nodeId);

uint16 API_GetDipSetIpNodeId(void);
uint8 API_SetDipSetIpNodeId(uint16 nodeId);



int GetLocalMac( char* pMac );
int GetLocalGateway(void);
int GetLocalMaskAddr(void);

unsigned char SetNetWork( char* mac, char* ip, char* mask, char* gw );
unsigned char ResetNetWork( void );
int NodeIdToIp(unsigned char nodeId);


// Define Object Function - Private---------------------------------------------
int RequestChipIdFromN329ToStm32(char * pChipId);
static int ReadDs2411Sn(void);


#endif


