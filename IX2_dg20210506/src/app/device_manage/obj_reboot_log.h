


#ifndef _OBJ_REBOOT_LOG_H_
#define _OBJ_REBOOT_LOG_H_


void RebootLog_Stat_Init(void);	
void Create_RebootLog_Index_Tb(void);
void free_RebootLog_Index_Tb(void);
int Get_RebootLog_Num(void);
int Get_One_RebootLog(int index,char *log_str);


#endif


