/**
  ******************************************************************************
  * @file    obj_NDM.h
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#define	NDM_S_CTRL_START					"NDM_START"
#define	NDM_S_CTRL_STOP						"NDM_STOP"
#define	NDM_S_CTRL_CHECK					"NDM_CHECK"

#define	NDM_S_STATE_ON									"ON"
#define	NDM_S_STATE_OFF									"OFF"

#define	NDM_C_CTRL_START					"NDM_START"
#define	NDM_C_CTRL_STOP						"NDM_STOP"
#define	NDM_C_CTRL_BOOT						"NDM_BOOT"
#define	NDM_C_CTRL_CHECK					"NDM_CHECK"

#define	NDM_C_STATE_ON									"ON"
#define	NDM_C_STATE_OFF									"OFF"
#define	NDM_C_STATE_NotSupported						"NotSupported"
#define	NDM_C_STATE_IP_CHANGE							"IP_CHANGE"
#define	NDM_C_STATE_OFFLINE								"OFFLINE"

#define	NDM_RES_IP_ADDR									"IP_ADDR"
#define	NDM_RES_MFG_SN									"MFG_SN"
#define	NDM_RES_IX_ADDR									"IX_ADDR"
#define	NDM_RES_ENABLE									"ENABLE"
#define	NDM_RES_NDM_State								"NDM_State"

#define	NDM_C_CHECK_UDP									"UDP_CHECK"
#define	NDM_C_CHECK_PING								"PING_CHECK"
#define	NDM_C_CHECK_DATE								"CHECK_DATE"

int NDM_Init(void);
int API_NDM_S_Ctrl(char* cmd, int time);

int NDM_S_CtrlCallback(cJSON *cmd);
int NDM_C_CtrlCallback(cJSON *cmd);
int NDM_C_ResponeCallback(cJSON *cmd);