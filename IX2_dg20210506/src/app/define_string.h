/**
  ******************************************************************************
  * @file    define_string.h
  * @author  cao
  * @version V1.0.0
  * @date    2022.03.10
  * @brief   This file contains the define of GPIO use.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _define_string_H_
#define _define_string_H_

#define IxDeviceFlag					"IX_Device"
#define RESULT_SUCC						"Succ"			//成功
#define RESULT_ERR01					"Err01"			//Device_Addr和IP_Addr不匹配
#define RESULT_ERR02					"Err02"			//No Target device (Device_Addr)
#define RESULT_ERR03					"Err03"			//Multi Target devices found (同地址多个IP) 例如在REQ_DM_INFO指令执行中 存在重复的Device_Addr的多个设备 则RSP指令会返回全部设备情况
#define RESULT_ERR04					"Err04"			//不支持
#define RESULT_ERR05					"Err05"			//数据有错
#define RESULT_ERR06					"Err06"			//操作失败
#define RESULT_ERR07					"Err07"	

#define RESULT_ERR10					"Err10"			//参数读写错误	

#define RES_DOWNLOAD_NO_SDCARD				"NO_SDCARD"
#define RES_DOWNLOAD_CHECK_OK				"CHECK_OK"
#define RES_DOWNLOAD_INSTALLING				"INSTALLING"
#define RES_DOWNLOAD_INSTALL_OK				"INSTALL_OK"
#define RES_DOWNLOAD_INSTALL_FAIL			"INSTALL_FAIL"
#define RES_DOWNLOAD_DOWNLOADING			"DOWNLOADING"
#define RES_DOWNLOAD_CONNECTING				"CONNECTING"
#define RES_DOWNLOAD_REBOOT					"REBOOT"
#define RES_DOWNLOAD_WAITING_REBOOT			"WAITING_REBOOT"
#define RES_DOWNLOAD_CANCEL					"CANCEL"

#define RES_DOWNLOAD_ERR_TYPE				"ERR_TYPE"
#define RES_DOWNLOAD_ERR_NO_FILE			"ERR_NO_FILE"
#define RES_DOWNLOAD_ERR_CODE				"ERR_CODE"
#define RES_DOWNLOAD_ERR_NO_SPASE			"ERR_NO_SPASE"
#define RES_DOWNLOAD_ERR_CONNECT			"ERR_CONNECT"
#define RES_DOWNLOAD_ERR_DOWNLOAD			"ERR_DOWNLOAD"

#define DM_KEY_DeviceNbr				"DeviceNbr"
#define DM_KEY_IP_Addr					"IP_Addr"
#define DM_KEY_MFG_SN					"MFG_SN"
#define DM_KEY_DeviceType			"DeviceType"
#define DM_KEY_BAK_SELECT				"bakSelect"
#define DM_KEY_Result					"Result"
#define DM_KEY_operate					"operate"
#define DM_KEY_OPERATE					"OPERATE"
#define DM_KEY_server					"server"
#define DM_KEY_SERVER					"SERVER"
#define DM_KEY_code						"code"
#define DM_KEY_CODE						"CODE"
#define DM_KEY_Operation				"Operation"
#define DM_KEY_ReportIp					"ReportIp"
#define DM_KEY_CANCEL					"CANCEL"

#define DM_KEY_GetFileList				"GetFList"
#define DM_KEY_DeleteFile				"DelFile"
#define DM_KEY_FileMove					"FileMv"
#define DM_KEY_FileCopy					"FileCp"
#define DM_KEY_MakeDir					"MakeDir"
#define DM_KEY_GetFileInfo				"GetFInfo"
#define DM_KEY_FileChmod				"FileChmod"

#define DM_KEY_Path						"Path"
#define DM_KEY_NewPath					"NewPath"
#define DM_KEY_FileList					"FileList"
#define DM_KEY_FileInfo					"FileInfo"
#define DM_KEY_FileMode					"Mode"

#define DM_KEY_TYPE						"TYPE"
#define DM_KEY_STATE					"STATE"
#define DM_KEY_RATES					"RATES"
#define DM_KEY_RES_NBR					"RES_NBR"
#define DM_KEY_IP						"IP"
#define DM_KEY_Local					"Local"
#define DM_KEY_Global					"Global"
#define DM_KEY_IP_STATIC				"IP_STATIC"
#define DM_KEY_IP_MASK					"IP_MASK"
#define DM_KEY_IP_GATEWAY				"IP_GATEWAY"
#define DM_KEY_deviceType				"deviceType"
#define DM_KEY_name1					"name1"
#define DM_KEY_name2_utf8				"name2_utf8"
#define DM_KEY_bootTime					"bootTime"
#define DM_KEY_UpTime					  "UpTime"

#define DM_KEY_SRC_DIR					"SRC_DIR"
#define DM_KEY_SRC_FILE					"SRC_FILE"
#define DM_KEY_TAR_DIR					"TAR_DIR"
#define DM_KEY_TAR_FILE					"TAR_FILE"
#define DM_KEY_SHELL_CMD				"SHELL_CMD"
#define DM_KEY_SHELL_REPORT				"SHELL_REPORT"
#define DM_KEY_Event				      "Event"
#define DM_KEY_EventID					  "EventID"

#define IX2V_TABLE                            "IX2V_TB"
#define IX2V                                  "IX2V"

#define DM_KEY_Idle					          "Idle"
#define DM_KEY_Remote					        "Remote"
#define DM_KEY_ProxyState					    "ProxyState"
#define DM_KEY_ProxyNetwork					  "ProxyNetwork"
#define DM_KEY_ProxyInfo					    "ProxyInfo"
#define DM_KEY_BY_NBR					        "BY_NBR"
#define DM_KEY_BY_INPUT					      "BY_INPUT"
#define DM_KEY_IO					            "IO"
#define DM_KEY_READ					          "READ"
#define DM_KEY_WRITE					        "WRITE"


#define IX2V_Platform                 "Platform"
#define IX2V_MFG_SN                   "MFG_SN"
#define IX2V_IP_ADDR                  "IP_ADDR" 
#define IX2V_IX_ADDR                  "IX_ADDR" 
#define IX2V_DT_ADDR                  "DT_ADDR" 
#define IX2V_BD_NBR                   "BD_NBR" 
#define IX2V_G_NBR                    "G_NBR"
#define IX2V_L_NBR                    "L_NBR"
#define IX2V_IX_NAME                  "IX_NAME" 
#define IX2V_IX_TYPE                  "IX_TYPE" 
#define IX2V_IX_Model                 "IX_Model"  
#define IX2V_DevModel                 "DevModel"  
#define IX2V_FW_VER                   "FW_VER"
#define IX2V_HW_VER                   "HW_VER"
#define IX2V_UpTime                   "UpTime"
#define IX2V_Unknow                   "Unknow"
#define IX2V_NET                      "NET" 
#define IX2V_DT_IM                    "DT_IM" 
#define IX2V_IXG_ID                   "IXG_ID" 
#define IX2V_ONLINE                   "ONLINE" 
#define IX2V_OFFLINE                  "OFFLINE" 

#define IX2V_Result					          "Result"
#define IX2V_SOURCE                   "SOURCE" 
#define IX2V_TARGET					          "TARGET"
#define IX2V_LOCK_NBR                 "LOCK_NBR" 
#define IX2V_UNLOCK_TIME              "UNLOCK_TIME" 
#define IX2V_EventName                "EventName" 
#define IX2V_EventData                "EventData" 
#define IX2V_STATE					          "STATE"
#define IX2V_MSG					            "MSG"
#define IX2V_TIME					            "TIME"
#define IX2V_Event				            "Event"
#define IX2V_ERROR					          "ERROR"
#define IX2V_SUCCESS					        "SUCCESS"
#define IX2V_START					          "START"
#define IX2V_TYPE					            "TYPE"
#define IX2V_Phone_Acc                "Phone_Acc" 
#define IX2V_Call_Type                "Call_Type" 
#define IX2V_UNLOCK_RECORD_TB         "UNLOCK_RECORD" 
#define IX2V_Data                     "Data" 
#define IX2V_CallBack                 "CallBack" 
#endif

