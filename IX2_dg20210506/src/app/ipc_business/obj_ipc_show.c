
#include "MENU_public.h"
#include "obj_ipc_show.h"
#include "obj_IPCTableSetting.h"
#include "obj_ipc_manager.h"

int Get_WinId(int x, int y)
{
	int posx,posy,id;
	int vdshowH;
	#ifdef PID_DX482
	posx = y;
	posy = bkgd_h - x;
	#else
	if( get_pane_type()	== 1 )
	{
		posx = bkgd_w - y;
		posy = x;
	}
	else if( get_pane_type() == 0 )
	{
		posx = (bkgd_w-x);
		posy = (bkgd_h-y); 	
	}
	else if( get_pane_type() == 2 || get_pane_type() == 8 )
	{
		
		posx = y;
		posy = bkgd_h - x;
	}
	else if( get_pane_type() == 4 || get_pane_type() == 6)
	{
		#if defined(PID_DX470_V25)
		posx = x;
		posy = y;
		#else
		posx = y;
		posy = x;
		#endif
	}
	else if( get_pane_type() == 5 || get_pane_type() == 7)
	{
		#if defined(PID_DX470_V25)
		posx = (bkgd_w-y);
		posy = x;
		#else
		posx = (bkgd_w-x);
		posy = y;
		#endif
	}
	#endif
	if( get_pane_type() == 7 || get_pane_type() == 5 )
	{
		vdshowH = bkgd_h - 64;
		if(posy<vdshowH/2)
		{
			id = 1;
		}
		else if(posx<bkgd_w/2 && posy<vdshowH*3/4)
		{
			id = 2;
		}
		else if(posx>=bkgd_w/2 && posy<vdshowH*3/4)
		{
			id = 3;
		}		
		else if(posx<bkgd_w/2 && posy>=vdshowH*3/4)
		{
			id = 4;
		}
		else
		{
			id = 5;
		}
	}
	else
	{
		if(posx<bkgd_w/2 && posy<bkgd_h/2)
		{
			id = 1;
		}
		else if(posx>=bkgd_w/2 && posy<bkgd_h/2)
		{
			id = 2;
		}
		else if(posx<bkgd_w/2 && posy>=bkgd_h/2)
		{
			id = 3;
		}
		else 
		{
			id = 4;
		}
	}
		
	return id;
}


void Get_OneIpc_ShowPos(int win_id, int* x, int* y, int* width, int* height)
{
	int vdshowH;
	if( get_pane_type() == 7 || get_pane_type() == 5 )
	{
		vdshowH = bkgd_h - 64;
		switch(win_id)
		{
			case 0:
				*x = 0;
				*y = 0;
				*width = bkgd_w;
				*height = vdshowH/2;
				break;
			case 1:
				*x = 0;
				*y = 0;
				*width = bkgd_w;
				*height = vdshowH/2;
				break;
			case 2:
				*x = 0;
				*y = vdshowH/2;
				*width = bkgd_w/2;
				*height = vdshowH/4;
				break;
			case 3:
				*x = bkgd_w/2;
				*y = vdshowH/2;
				*width = bkgd_w/2;
				*height = vdshowH/4;
				break;
			case 4:
				*x = 0;
				*y = vdshowH*3/4;
				*width = bkgd_w/2;
				*height = vdshowH/4;
				break;
		}
	}
	else
	{
		switch(win_id)
		{
			case 0:
				*x = 0;
				*y = 0;
				*width = bkgd_w;
				*height = bkgd_h;
				break;
			case 1:
				*x = 0;
				*y = 0;
				*width = bkgd_w/2;
				*height = bkgd_h/2;
				break;
			case 2:
				*x = bkgd_w/2;
				*y = 0;
				*width = bkgd_w/2;
				*height = bkgd_h/2;
				break;
			case 3:
				*x = 0;
				*y = bkgd_h/2;
				*width = bkgd_w/2;
				*height = bkgd_h/2;
				break;
			case 4:
				*x = bkgd_w/2;
				*y = bkgd_h/2;
				*width = bkgd_w/2;
				*height = bkgd_h/2;
				break;
		}
	}
	
}

int Set_ipc_show_pos(int ins_num, int x, int y, int width, int height)
{
	#ifdef PID_IXSE
	return SetLayerPos(ins_num + AK_VO_LAYER_VIDEO_5, x, y, width, height);
	#else
	return SetLayerPos(ins_num + AK_VO_LAYER_VIDEO_5, x, y, width, height);
	#endif

}

int Clear_ipc_show_layer(int ins_num)
{
	#ifdef PID_IXSE
	return close_one_layer(ins_num + AK_VO_LAYER_VIDEO_5);
	#else
	return close_one_layer(ins_num + AK_VO_LAYER_VIDEO_5);
	#endif
}

int api_OneIpc_Rtsp_Show(int ins_num, unsigned char* rtsp, int width ,int height, int vdtype, int x, int y, int w, int h)
{

	//SetWinDeviceName(ins_num, "ipcProxy");
	SetWinVdType(ins_num, vdtype);
	Set_ipc_show_pos(ins_num, x, y, w, h);
	return api_ipc_show_one_stream(ins_num, rtsp, width, height, vdtype);
}

int api_OneIpc_Show_start(int ins_num, char* device_ip, char* device_name, int ipc_ch, int win_id)
{
	int x,y,w,h;
	int chApply;	
	IPC_ONE_DEVICE dat;
	char name_temp[41]="-";
	int ret = -1;
	//if(ipc_check_online(device_ip) != 0)
	//{
	//	return -1;
	//}
	LoadSpriteDisplay(1, win_id);
	//printf("api_OneIpc_Show_start device_ip=%s device_name=%s\n", device_ip, device_name);
	if(GetIpcDeviceInfo(device_ip, device_name, &dat) == 0)
	{
		GetMonFavByAddr(NULL, dat.NAME, name_temp);
		SetWinDeviceName(ins_num, strcmp(name_temp, "-") ? name_temp : dat.NAME);
		chApply = (ipc_ch == 0)? dat.CH_FULL : dat.CH_QUAD;
		SetWinVdType(ins_num, dat.CH_DAT[chApply].vd_type);
		Get_OneIpc_ShowPos(win_id,&x,&y,&w,&h);
		Set_ipc_show_pos(ins_num, x, y, w, h);
		ret = api_ipc_show_one_stream(ins_num, dat.CH_DAT[chApply].rtsp_url, dat.CH_DAT[chApply].width, dat.CH_DAT[chApply].height, dat.CH_DAT[chApply].vd_type);		
	}
	LoadSpriteDisplay(0, win_id);
	return ret;
}
int api_IpcIndex_Show(int ins_num, int table_type,int ipc_index, int ipc_ch, int win_id)
{
	int x,y,w,h;	
	int chApply;	
	IPC_ONE_DEVICE dat;
	char name_temp[41]="-";
	int ret = -1;
	LoadSpriteDisplay(1, win_id);
	if(table_type==0)
	{
		if(GetIpcCacheRecord(ipc_index, &dat) != 0)
		{
			LoadSpriteDisplay(2, win_id);
			return -1;
		}
	}
	else if(table_type==1)
	{
		if(GetWlanIpcRecord(ipc_index, &dat) != 0)
		{
			LoadSpriteDisplay(2, win_id);
			return -1;
		}
	}
	//if(ipc_check_online(dat.IP) != 0)
	//{
	//	return -1;
	//}
	GetMonFavByAddr(NULL, dat.NAME, name_temp);
	SetWinDeviceName(ins_num, strcmp(name_temp, "-") ? name_temp : dat.NAME);
	chApply = (ipc_ch == 0)? dat.CH_FULL : dat.CH_QUAD;
	SetWinVdType(ins_num, dat.CH_DAT[chApply].vd_type);
	Get_OneIpc_ShowPos(win_id,&x,&y,&w,&h);
	Set_ipc_show_pos(ins_num, x, y, w, h);
	ret = api_ipc_show_one_stream(ins_num, dat.CH_DAT[chApply].rtsp_url, dat.CH_DAT[chApply].width, dat.CH_DAT[chApply].height, dat.CH_DAT[chApply].vd_type);
	LoadSpriteDisplay(0, win_id);
	return ret;
}

int api_OneIpc_Show_xy(int ins_num, int table_type,int ipc_index, int ipc_ch, int x, int y, int w, int h)
{
	IPC_ONE_DEVICE dat;
	int chApply;	
	char name_temp[41]="-";
	if(table_type==0)
	{
		if(GetIpcCacheRecord(ipc_index, &dat) != 0)
		{
			return -1;
		}
	}
	else if(table_type==1)
	{
		if(GetWlanIpcRecord(ipc_index, &dat) != 0)
		{
			return -1;
		}
	}
	//if(ipc_check_online(dat.IP) != 0)
	//{
	//	return -1;
	//}
	
	GetMonFavByAddr(NULL, dat.NAME, name_temp);
	SetWinDeviceName(ins_num, strcmp(name_temp, "-") ? name_temp : dat.NAME);
	chApply = (ipc_ch == 0)? dat.CH_FULL : dat.CH_QUAD;
	SetWinVdType(ins_num, dat.CH_DAT[chApply].vd_type);
	Set_ipc_show_pos(ins_num, x, y, w, h);
	return api_ipc_show_one_stream(ins_num, dat.CH_DAT[chApply].rtsp_url, dat.CH_DAT[chApply].width, dat.CH_DAT[chApply].height, dat.CH_DAT[chApply].vd_type);
}

int api_IpcDevice_Show_xy(int ins_num, char* device_ip, char* device_name, char* user, char* pwd, int x, int y, int w, int h)
{
	IPC_ONE_DEVICE dat;
	onvif_login_info_t Login;
	char name_temp[41]="-";
	//if(ipc_check_online(device_ip) != 0)
	//{
	//	return -1;
	//}

	//printf("api_OneIpc_Show_start device_ip=%s device_name=%s\n", device_ip, device_name);
	if(GetIpcDeviceInfo(device_ip, device_name, &dat) == 0)
	{
		GetMonFavByAddr(NULL, dat.NAME, name_temp);
		SetWinDeviceName(ins_num, strcmp(name_temp, "-") ? name_temp : dat.NAME);
		SetWinVdType(ins_num, dat.CH_DAT[1].vd_type);
		Set_ipc_show_pos(ins_num, x, y, w, h);
		return api_ipc_show_one_stream(ins_num, dat.CH_DAT[1].rtsp_url, dat.CH_DAT[1].width, dat.CH_DAT[1].height, dat.CH_DAT[1].vd_type);
	}
	else
	{
		if(ipc_check_online(device_ip) != 0)
		{
			return -1;
		}
		memset(&Login, 0, sizeof(onvif_login_info_t));
		if(one_ipc_Login(device_ip, user, pwd, &Login) == 0)
		{
			SetWinDeviceName(ins_num, device_name);
			SetWinVdType(ins_num, 0);
			Set_ipc_show_pos(ins_num, x, y, w, h);
			return api_ipc_show_one_stream(ins_num, Login.dat[1].rtsp_url, Login.dat[1].width, Login.dat[1].height, 0);
		}
		else
		{
			return -1;
		}
	}
	return -1;
}

int api_OneIpc_Show_stop( int ins_num )
{
	return api_ipc_stop_mointor_one_stream(ins_num);
}




