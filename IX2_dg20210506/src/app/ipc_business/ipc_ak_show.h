#ifndef _IPC_AK_SHOW_H_
#define _IPC_AK_SHOW_H_

#include "obj_ak_show.h"

#define 	MAX_IPC_DEC_INS		4

int api_h264_show_initial(void);
int api_h264_show_start(int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype );
int api_h264_show_update(int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype );
int api_h264_show_just_running(int index);
int api_h264_show_stop( int index );
int api_h264_show_dump_buf( int index, char* pdatbuf, int datlen );

#endif
