#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "OSTIME.h"
#include "obj_ipc_rec.h"
#include "obj_IPCTableSetting.h"
#include "MENU_public.h"

static REC_SCHEDULE_Table RecScheduleTable = {0};
VD_REC_T	multi_ipc_record[4];
extern OS_TIMER 	timer_rec_schedule;

int Get_OneIpc_Rec_State(int ch)
{
	return multi_ipc_record[ch].state;
}

int API_OneIpc_Rec_stop( int ch )
{
	if(multi_ipc_record[ch].state == 1)
	{
		multi_ipc_record[ch].state = 0;
		api_stop_ffmpeg_rec(ch);
	}
}

int Check_OneRec_Space(int bitrate, int min)
{
	#if defined(PID_IX850)
	#else
	int sizeNeed = (min>=10? 1024 : min*60*((bitrate==0? 3072 : bitrate)/8)/1024); // 1024MB
	int freesize, totalsize;
	printf("Check_OneRec_Space sizeNeed=%dMB \n", sizeNeed);
	API_GetSDCardSize(&freesize, &totalsize);
	if(totalsize <= sizeNeed)
	{
		return -1;
	}
	if( freesize >  sizeNeed)
	{
		return 0;
	}
	while(getSdcardSpace() <=  sizeNeed)
	{
		if(!GetIpcRecTableNum())
		{
			GetIpcRecListByKey(".mp4");
			if(!GetIpcRecTableNum())
				return -1;
		}			
		DeleteOneRecRecord(0);
	}
	#endif
	return 0;
}

void stop_ffmpeg_rec(int index)
{
	if( multi_ipc_record[index].state )
	{
		multi_ipc_record[index].state = 0;
 		api_stop_ffmpeg_rec(index);
	}
}

int API_OneIpc_Rec_start(int ch, char* device_ip, char* device_name, int Rectime, int filesize)
{
	IPC_ONE_DEVICE dat;
	char tempData[50+1];
	char recfile[100+1];
	time_t t;
	struct tm *tblock; 	
	int ret;
 	if(!Judge_SdCardLink()) 
	{
		return -1;
	}

	if(MakeDir(VIDEO_STORE_DIR) != 0)
	{
		return -1;
	}
	
	if(multi_ipc_record[ch].state == 1)
	{
		API_OneIpc_Rec_stop(ch);
		return -1;
	}
	if(ipc_check_online(device_ip) != 0)
	{
		return -1;
	}
	if(GetIpcDeviceInfo(device_ip, device_name, &dat) != 0)
	{
		return -1;
	}
	if(Check_OneRec_Space(dat.CH_DAT[dat.CH_REC].bitrate, Rectime) != 0)
	{
		return -1;
	}
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( tempData,"20%02d%02d%02d_%02d%02d%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	snprintf(recfile,100,"%s/%s-CH%d.mp4",VIDEO_STORE_DIR, tempData, ch+1);
	int fps = dat.CH_DAT[dat.CH_REC].framerate;
	ret = api_start_ffmpeg_rec(ch, dat.CH_DAT[dat.CH_REC].rtsp_url, recfile, Rectime, (fps==0? 15: fps), stop_ffmpeg_rec);
	if(ret == 0)
	{
		multi_ipc_record[ch].state = 1;
	}
	return ret;
}

int API_IpcMon_Rec_Start(int ch)
{
 	if(!Judge_SdCardLink()) 
	{
		return -1;
	}

	if(MakeDir(VIDEO_STORE_DIR) != 0)
	{
		return -1;
	}

	if(Check_OneRec_Space(0, 10) != 0)
	{
		return -1;
	}
	return api_start_ipc_rec(ch);
}

int API_IpcMon_Rec_Stop(int ch)
{
	return api_stop_ipc_rec(ch);
}


char* CreateRecScheduleObject(void)
{
    cJSON *root = NULL;
	cJSON *channels = NULL;
	cJSON *channel = NULL;
	char *string = NULL;
	int i;
    root = cJSON_CreateObject();
	cJSON_AddItemToObject(root, "rec_setup", channels = cJSON_CreateArray());	
    for (i = 0; i < RecScheduleTable.num; i++)
    {
		channel = cJSON_CreateObject();
		cJSON_AddNumberToObject(channel, "CH", RecScheduleTable.channel[i].ch);
		cJSON_AddStringToObject(channel, "timeS", RecScheduleTable.channel[i].timeS);
		cJSON_AddStringToObject(channel, "timeE", RecScheduleTable.channel[i].timeE);
		cJSON_AddItemToObject(channel, "week", cJSON_CreateIntArray(RecScheduleTable.channel[i].week, RecScheduleTable.channel[i].weekNum));

		cJSON_AddItemToArray(channels, channel);
	}
	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}
int ParseRecScheduleObject(const char* json)
{
	int status = 0;
	int i,j;
    cJSON *channels = NULL;
    cJSON *channel = NULL;
    cJSON *week = NULL;
    cJSON *pSub = NULL;
	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	channels = cJSON_GetObjectItem( root, "rec_setup");
	if(cJSON_IsArray(channels))
	{
		RecScheduleTable.num = cJSON_GetArraySize ( channels );
		printf("ParseRecScheduleObject cnt=%d\n", RecScheduleTable.num);
		for(i=0; i<RecScheduleTable.num; i++)
		{
			channel = cJSON_GetArrayItem(channels, i);
			if(NULL == channel ){ continue ; }
			ParseJsonNumber(channel, "CH", &(RecScheduleTable.channel[i].ch));
			ParseJsonString(channel, "timeS", RecScheduleTable.channel[i].timeS, 10);
			ParseJsonString(channel, "timeE", RecScheduleTable.channel[i].timeE, 10);
			week = cJSON_GetObjectItem( channel, "week");
			if( week != NULL )
			{
				RecScheduleTable.channel[i].weekNum = cJSON_GetArraySize ( week );
				for( j = 0 ; j < RecScheduleTable.channel[i].weekNum; j ++ )
				{
					pSub = cJSON_GetArrayItem(week, j);
					if(NULL == pSub ){ continue ; }
					RecScheduleTable.channel[i].week[j] = pSub->valueint ;
				}
			}
			
		}
	}
	status = 1;
end:
	cJSON_Delete(root);
	return status;
}

int GetRecScheduleRecord(int index, REC_SCHEDULE_DAT_T* record)
{
	if(RecScheduleTable.num <= index)
	{
		return -1;
	}
	*record = RecScheduleTable.channel[index];
	return 0;
}

int GetOneRecSchedule(int ch, REC_SCHEDULE_DAT_T* record)
{
	int i;
	for(i = 0; i < RecScheduleTable.num; i++)
	{
		if(ch == RecScheduleTable.channel[i].ch)
		{
			//printf("GetOneDvrSetup CH=%d ok\n", ch);
			*record = RecScheduleTable.channel[i];
			return 0;
		}
	}
	return -1;

}

int SetOneRecSchedule(REC_SCHEDULE_DAT_T* record)
{
	int i;
	
	for(i = 0; i < RecScheduleTable.num; i++)
	{
		if(record->ch == RecScheduleTable.channel[i].ch)
		{
			RecScheduleTable.channel[i] = *record;
			return 1;	
		}
	}

	RecScheduleTable.channel[RecScheduleTable.num++] = *record;
	return 0;	
}

int DeleteOneRecSchedule(int ch)
{
	int i;
	for(i = 0; i < RecScheduleTable.num; i++)
	{
		if(ch == RecScheduleTable.channel[i].ch)
		{
			break;
		}
	}
	if(i == RecScheduleTable.num)
		return -1;
	
	for(; i+1 < RecScheduleTable.num; i++)
	{
		RecScheduleTable.channel[i] = RecScheduleTable.channel[i+1];
	}
	RecScheduleTable.num--;
	return 0;
}

void SaveRecScheduleToFile(void)
{
	char* jsonString;
	jsonString = CreateRecScheduleObject();
	if(jsonString != NULL)
	{
		SetJsonStringToFile(REC_SCHEDULE_FILE_NAME, jsonString);
		free(jsonString);
	}
}

void GetRecScheduleFromFile(void)
{
	ParseRecScheduleObject(GetJsonStringFromFile(REC_SCHEDULE_FILE_NAME));
	if(RecScheduleTable.num)
		OS_RetriggerTimer(&timer_rec_schedule);
}


int CheckWeekday(int week, int cnt, int* input)
{
	int i;
	printf("CheckWeekday cur week=%d\n", week);
	for(i=0; i<cnt; i++)
	{
		if(input[i] == week)
		{
			return 1;
		}
	}
	return 0;
}

void RecScheduleOperation(void)
{
	int i;
	REC_SCHEDULE_DAT_T dat;
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	unsigned char hour[3] = {0};
	unsigned char min[3] = {0};
	unsigned char hour2[3] = {0};
	unsigned char min2[3] = {0};
	int rectime;
	
	for(i = 0; i < RecScheduleTable.num; i++)
	{
		if( GetRecScheduleRecord(i, &dat) == 0 )
		{
			if(!CheckWeekday(tblock->tm_wday, dat.weekNum, dat.week))
			   continue;
			memcpy(hour, dat.timeS, 2);
			memcpy(min, dat.timeS+2, 2);
			memcpy(hour2, dat.timeE, 2);
			memcpy(min2, dat.timeE+2, 2);
			//printf("DvrInformOperation h_s=%s m_s=%s h_e=%s m_e=%s\n", hour, min, hour2, min2);
			if((tblock->tm_hour == atol(hour)) && (tblock->tm_min == atol(min)))
			{
				 if(atol(hour2) > atol(hour))
				 {
					rectime = 60*(atol(hour2)-atol(hour)-1) + 60+atol(min2)-atol(min);
				 }
				 else
				 {
					rectime = atol(min2)-atol(min);
				 }
				printf("DvrOperation start ch=%d min=%d\n", dat.ch, rectime);
				API_OneIpc_Rec_start(dat.ch, dat.ipc_ip, dat.ipc_name, rectime, 0);
			}
			else if((tblock->tm_hour == atol(hour2)) && (tblock->tm_min == atol(min2)))
			{
				printf("DvrOperation stop ch=%d \n", dat.ch);
				API_OneIpc_Rec_stop(dat.ch);
			}
			else
			{
				if((tblock->tm_hour == atol(hour)) && (tblock->tm_min > atol(min)) ||
					(tblock->tm_hour > atol(hour)) && tblock->tm_hour < atol(hour2) ||
					(tblock->tm_hour == atol(hour2)) && (tblock->tm_min < atol(min2)))
				{
					if(multi_ipc_record[dat.ch].state == 0)
					{
						if(atol(hour2) > tblock->tm_hour)
						 {
							rectime = 60*(atol(hour2)-tblock->tm_hour-1) + 60+atol(min2)-tblock->tm_min;
						 }
						 else
						 {
							rectime = atol(min2)-tblock->tm_min;
						 }
						printf("DvrOperation start ch=%d min=%d\n", dat.ch, rectime);
						API_OneIpc_Rec_start(dat.ch, dat.ipc_ip, dat.ipc_name, rectime, 0);
					}
				}
			}
		}
	}
	
}


