

#include "ffmpeg_show.h"
#include "ipc_ak_show.h"
#include "task_VideoMenu.h"

int av_register_init  =0;
// 线程启动前的初始化
void ffmpeg_init(void)
{
	/* initialize libavcodec, and register all codecs and formats */
	printf("\n=================begin to av_register_all...========\n");
	av_register_init = 1;
	av_register_all();
	avcodec_register_all();
	avformat_network_init();
	//avfilter_register_all();
}

ffmpeg4ipc_dat_t	ffmpeg4ipc[8] =
{
{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
};

void set_ffmpeg_show_state(ffmpeg4ipc_dat_t* pusr_dat, int state)
{
	pthread_mutex_lock(&pusr_dat->ffmpegStateLock);
	pusr_dat->state = state;
	pthread_mutex_unlock(&pusr_dat->ffmpegStateLock);
	printf( "set_ffmpeg_show_state = %d\n", state) ;		
}

int get_ffmpeg_show_state(ffmpeg4ipc_dat_t* pusr_dat)
{
	int state;
	pthread_mutex_lock(&pusr_dat->ffmpegStateLock);
	state = pusr_dat->state;
	pthread_mutex_unlock(&pusr_dat->ffmpegStateLock);
	return state;
}


int task_ffmpeg4ipc_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg4ipc_dat_t *pusr_dat = (ffmpeg4ipc_dat_t*)ptiny_task->pusr_dat;	

#ifndef FFMPEG_3_4_2_VERSION
	int ret;
	
    AVFormatParameters params, *ap= &pusr_dat->params;
	
	printf("\n=================begin to av_register_all...========\n");

    /* initialize libavcodec, and register all codecs and formats */
	if( !av_register_init )
	{
		av_register_init = 1;
	    av_register_all();
		avcodec_register_all();
	}
	printf("\n=================begin to av_open_input_file: [%s]...========\n", pusr_dat->filename );

    ret = av_open_input_file(&pusr_dat->ic, pusr_dat->filename, NULL, 0, ap);
    if (ret < 0) 
	{
        printf("cannot open %s\n", pusr_dat->filename);
		return -1;
    }

	////////////////////////////////////////////
	pusr_dat->ic->probesize = 50 *1024;
	//pusr_dat->ic->probesize = 0 *4096;
    pusr_dat->ic->max_analyze_duration = 10 * AV_TIME_BASE;	
	////////////////////////////////////////////
	printf("\n=================begin to av_find_stream_info...========\n");

    ret = av_find_stream_info(pusr_dat->ic);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
    if (ret < 0) 
	{
        printf( "%s: could not find codec parameters\n", pusr_dat->filename);
		return -1;
    }

	///////////////////////////////////////////////////////
	// 查看有几路流媒体
	pusr_dat->videostream = -1;
	if( pusr_dat->ic->nb_streams <= 0 )
	{
		printf("not found stream\n");
		return -1;
	}

	// 查看有几路流媒体
	printf(" total stream numbers=%d \n",pusr_dat->ic->nb_streams);
	int i;
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			break;
		}
	}
	printf(" video stream number=%d \n",pusr_dat->videostream);
	switch( pusr_dat->ic->video_codec_id )
	{
	  case CODEC_ID_H264:
		  printf("It is H264 Video\n");
		  pusr_dat->videotype = 0;
		  pusr_dat->width = pusr_dat->ic->streams[pusr_dat->videostream]->codec->width;		 
		  pusr_dat->height = pusr_dat->ic->streams[pusr_dat->videostream]->codec->height;		 	  	  
		  break;
	  case CODEC_ID_MPEG4:
		  printf("It is MPEG4 Video\n");
		  pusr_dat->videotype = 2;
		  break;
	  default:
		  printf("It is unknown Video\n");
		  pusr_dat->videotype = 0; // default as H264		  
		  break;
	}
	return 0;
#else
	int ret;
	long long cur_time;
	cur_time = time_since_last_call(-1);	

	printf("\n=================begin to avformat_open_input: [%s]...========\n", pusr_dat->filename );

	//pusr_dat->ic = avformat_alloc_context();
	pusr_dat->ic=NULL;
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"204800", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以udp方式打开，如果以tcp方式打开将udp替换为tcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
	if (ret < 0) 
	{
		pusr_dat->ic=NULL;
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
		if (ret < 0) 
		{
			char data[1];
			data[0] = pusr_dat->videoCH;			
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_IpcOpenRtspErr, data, 1);
			printf("cannot open %s ret=%d\n", pusr_dat->filename, ret);
			return -1;
		}
	}
	printf("==task_ffmpeg4ipc_init ins[%d] time[%d] \n",pusr_dat->videoCH, time_since_last_call(cur_time));	

	#if 0

	pusr_dat->ic->probesize = 1 *1024;
	pusr_dat->ic->max_analyze_duration = 1 * AV_TIME_BASE;	

	printf("\n=================begin to av_find_stream_info...========\n");

	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
	if (ret < 0) 
	{
		printf( "%s: could not find codec parameters\n", pusr_dat->filename);
		return -1;
	}

	///////////////////////////////////////////////////////
	// 查看有几路流媒体
	pusr_dat->videostream = -1;
	// 查看有几路流媒体
	printf(" total stream numbers=%d \n",pusr_dat->ic->nb_streams);
	int i;
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			break;
		}
	}
	printf(" video stream number=%d \n",pusr_dat->videostream);

	switch( pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->codec_id )
	{
	  case AV_CODEC_ID_H264:
		  pusr_dat->videotype = 0;		 
		  pusr_dat->width = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->width;		 
		  pusr_dat->height = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->height;		 	  
		  printf("It is H264 Video width=%d, height=%d\n", pusr_dat->width, pusr_dat->height);
		  break;
	  case AV_CODEC_ID_H265:
		  pusr_dat->videotype = 1;		  
		  pusr_dat->width = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->width;		 
		  pusr_dat->height = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->height;		 	  
		  printf("It is H265 Video width=%d, height=%d\n", pusr_dat->width, pusr_dat->height);
		  break;
	  default:
		  printf("It is unknown Video\n");
		  pusr_dat->videotype = 0; // default as H264		  
		  break;
	}
	#endif
	return 0;
#endif	
}

// 线程循环处理
//#define FRAME_BUFFER_LEN	(300*1024)
int task_ffmpeg4ipc_process( void* arg )
{
    int i,k, ret;
    unsigned char *byte_buffer=NULL;
    int byte_buffer_size = 0;
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg4ipc_dat_t *pusr_dat = (ffmpeg4ipc_dat_t*)ptiny_task->pusr_dat;	


	ret = 0;

    AVPacket pkt;
	int64_t pts_last;
	struct timeval start, end;
	int time_use;

	set_ffmpeg_show_state(pusr_dat, 2); // run flag;
	printf("\n=================task_ffmpeg4ipc_process...========\n");
	printf("\n=================task_ffmpegshow_process...========\n");
	if(pusr_dat->data_buff==NULL)
		pusr_dat->data_buff=malloc(pusr_dat->data_buff_len);
	av_init_packet(&pkt);
    while( one_tiny_task_is_running(ptiny_task) )
	{		
		if( get_ffmpeg_show_state(pusr_dat) == 2)
		{
			memset(&pkt, 0, sizeof(pkt));
			gettimeofday( &start, NULL );
			ret= av_read_frame(pusr_dat->ic, &pkt);
			gettimeofday( &end, NULL );
			time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
			if(ret<0 || time_use > 2000000)// 
			{
				printf("task_ffmpeg4ipc_process av_read_frame ret=%d time_use=%d\n", ret, time_use);
				if( get_ffmpeg_show_state(pusr_dat) == 2)
					set_ffmpeg_show_state(pusr_dat, 3); // restart flag;
			}
			else
			{
				//if( pkt.stream_index == pusr_dat->videostream )
				if( pkt.stream_index == 0)

	{
					//byte_buffer = realloc(byte_buffer, pkt.size + 1);
					if(pusr_dat->data_buff_len<(pkt.size+1))
					{
						free(pusr_dat->data_buff);
						pusr_dat->data_buff=NULL;
						while(pusr_dat->data_buff_len<(pkt.size+1))
						{
							pusr_dat->data_buff_len+=512;
						}
						pusr_dat->data_buff=malloc(pusr_dat->data_buff_len);
					}
				byte_buffer=pusr_dat->data_buff;	
					// 若数据包开始为00 00 01则补充为00 00 00 01
					if( pkt.data[0]==0x00 && pkt.data[1]==0x00 && pkt.data[2]==0x01 )
					{
						byte_buffer[byte_buffer_size] = 0x00;
						byte_buffer_size++;
						//printf("pkt.data[0] filled 0...\n");						
					}
					memcpy( byte_buffer+byte_buffer_size, pkt.data, pkt.size );
					byte_buffer_size += pkt.size;
	
					//linphone 监视 IPC cao_20200907
					if(pusr_dat->rtpMonitor)
					{
						//rtp_sender_send_with_ts(pkt.data, pkt.size);
						rtp_sender_send_with_ts_wan( byte_buffer,byte_buffer_size,0,pusr_dat->videotype);
						rtp_sender_send_with_ts_uni( byte_buffer,byte_buffer_size,0,pusr_dat->videotype);
						byte_buffer_size = 0;
						//printf("IPC rtp_sender_send_with_ts_wan\n");
					}
					else
					{
							#if 0
						if(pusr_dat->videotype==0)
						{
							if(h264_data_check(byte_buffer,byte_buffer_size)!=0)
							{
								if(++unkown_frame<3)
									log_w("have h264 unkown_frame,len=%d",byte_buffer_size);
								continue;
								
							}
						}
						else
						{
							if(h265_data_check(byte_buffer,byte_buffer_size)!=0)
							{
								if(++unkown_frame<3)
									log_w("have h265 unkown_frame,len=%d",byte_buffer_size);
								continue;
								
							}
						}
						#endif
						//long long cur_time;
						//cur_time = time_since_last_call(-1);	
						(*pusr_dat->dat_process)(byte_buffer,byte_buffer_size);
						//printf("==IPC index[%d] decode time:[%d]\n",pusr_dat->videoCH, time_since_last_call(cur_time));	
						API_Recording_mux( byte_buffer,byte_buffer_size, pusr_dat->videoCH, pusr_dat->videotype );
						byte_buffer_size = 0;
					}
	
					
					if(pusr_dat->recFlag)
					{
						pkt.pts = 90000/pusr_dat->fps + pts_last;
						pkt.dts = pkt.pts;
						pts_last = pkt.pts;
						ret = av_interleaved_write_frame(pusr_dat->out_ic, &pkt);
						//printf("av_interleaved_write_frame ret=%d\n", ret);
						if (ret < 0) 
						{
							printf("write_frame ERR=%d\n", ret);
	
						}
					}
				}
			} 
			av_packet_unref(&pkt);
		}
		else if( get_ffmpeg_show_state(pusr_dat) == 3)
		{
			api_restart_ipc_show(pusr_dat->videoCH);
		}
        
		usleep(1*1000);
    }
	#if 0
	if(byte_buffer != NULL)
	{
		free(byte_buffer);
		byte_buffer = NULL;
	}
	#endif
    return 0;
}

int api_start_ipc_rec(int ffmpe_num)
{
	ffmpeg4ipc_dat_t* pusr_dat = &ffmpeg4ipc[ffmpe_num];

	char recfile[100];
	char temp[50];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( temp,"20%02d%02d%02d_%02d%02d%02d-CH%d.mp4",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec,ffmpe_num+1);
	snprintf(recfile,100,"/mnt/sdcard/video/%s",temp);
	
	int ret;
	ret = avformat_alloc_output_context2(&pusr_dat->out_ic, NULL, NULL, recfile);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		return -1;
    }
	printf("videoch=%d Videotype=%d width=%d, height=%d\n", ffmpe_num, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
	AVStream *in_stream = pusr_dat->ic->streams[pusr_dat->videostream];
	AVStream *out_stream = avformat_new_stream(pusr_dat->out_ic, NULL);
	avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
	ret = avio_open(&pusr_dat->out_ic->pb, recfile, AVIO_FLAG_WRITE);
	if (ret < 0) 
	{
		printf("Could not open output file %s\n", recfile);
		return -1;
	}
	ret = avformat_write_header(pusr_dat->out_ic, NULL);
	if (ret < 0) 
	{
        printf("Error when avformat_write_header\n");
		return -1;
	}
	pusr_dat->recFlag = 1;
	pusr_dat->fps = 15;
	printf( "api_start_ipc_rec ch[%d] success!\n", ffmpe_num);
	return 0;
}

int api_stop_ipc_rec(int ffmpe_num)
{
	ffmpeg4ipc_dat_t* pusr_dat = &ffmpeg4ipc[ffmpe_num];
	if(pusr_dat->recFlag)
	{
		av_write_trailer(pusr_dat->out_ic);
		avio_closep(&pusr_dat->out_ic->pb);
		avformat_free_context(pusr_dat->out_ic);
		pusr_dat->recFlag = 0;
		printf( "api_stop_ipc_rec ch[%d] !\n", ffmpe_num);
	}
	return 0;
}

int api_restart_ipc_show(int ffmpe_num)
{
	int ret;
	ffmpeg4ipc_dat_t* pusr_dat = &ffmpeg4ipc[ffmpe_num];

	printf("==api_restart_ipc_show ins[%d] \n",pusr_dat->videoCH);	
	
    avformat_close_input(&pusr_dat->ic);
	//pusr_dat->ic = avformat_alloc_context();
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"204800", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以udp方式打开，如果以tcp方式打开将udp替换为tcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	pusr_dat->ic=NULL;
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
	if (ret < 0) 
	{
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
		if (ret < 0) 
		{
			printf("cannot open %s ret=%d\n", pusr_dat->filename, ret);
			set_ffmpeg_show_state(pusr_dat, 1);
			return -1;
		}
	}
	set_ffmpeg_show_state(pusr_dat, 2);
	return 0;

}

// 退出该线程的处理: 内存和资源的回收释放
int task_ffmpeg4ipc_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg4ipc_dat_t *pusr_dat = (ffmpeg4ipc_dat_t*)ptiny_task->pusr_dat;	
#ifndef FFMPEG_3_4_2_VERSION				
    av_close_input_file(pusr_dat->ic);
#else
    avformat_close_input(&pusr_dat->ic);
#endif
	//avformat_free_context(pusr_dat->ic);
	if(pusr_dat->recFlag)
	{
		av_write_trailer(pusr_dat->out_ic);
		avio_closep(&pusr_dat->out_ic->pb);
		avformat_free_context(pusr_dat->out_ic);
		pusr_dat->recFlag = 0;
	}
	printf( "--task_ffmpeg4ipc_exit ok!!!\n"); 
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
int api_start_ffmpeg4ipc(int ffmpe_num, unsigned char* rtsp_url, int ch_main, int width, int height, int vdtype, cb_ffmpeg_proc proc )
{
	ffmpeg4ipc_dat_t* pins = &ffmpeg4ipc[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	if( state != 0)
	{
		printf( "------------api_start_ffmpeg4ipc fail--state=%d--------------\n", state);	
		return -1;
	}
	pins->videoCH = ffmpe_num;
	pins->videotype = vdtype;
	pins->recFlag = 0;
	pins->rtpMonitor = 0; //cao_20200907
	
	strcpy( pins->filename, rtsp_url );

#ifndef FFMPEG_3_4_2_VERSION	
    pins->params.channels=ch_main;
    pins->params.sample_rate= rate;
	pins->params.width = width;
	pins->params.height = height;	
	pins->params.initial_pause = 0;
	pins->params.video_codec_id = CODEC_ID_H264;
#else
#endif

	pins->dat_process 	= proc;
	pins->start_time	=time(NULL);
	set_ffmpeg_show_state(pins, 1);

	if( one_tiny_task_start2( &pins->task_process, task_ffmpeg4ipc_init, task_ffmpeg4ipc_process, task_ffmpeg4ipc_exit, (void*)pins ) == 0 )
	{
		return 0;
 	}
	else
	{
		return -1;
	}
}
int ffmpeg4ipc_check_state_error(int ffmpe_num)
{
	ffmpeg4ipc_dat_t* pins = &ffmpeg4ipc[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	if( state != 0&&abs(time(NULL)-pins->start_time)>30)
		return 1;
	return 0;
}
int api_stop_ffmpeg4ipc(int ffmpe_num)
{
	ffmpeg4ipc_dat_t* pins = &ffmpeg4ipc[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	
	printf("api_stop_ffmpeg4ipc ins=[%d] state=%d !!\n", ffmpe_num, state);
	if( state != 0)
	{
		one_tiny_task_stop2( &pins->task_process );

		set_ffmpeg_show_state(pins, 0);
	}
	return 0;	
}

int dvr_show_to_quad(int index, int dispx, int dispy, int dispw, int disph)
{
	int ret;
	printf("dvr_show_to_quad !!!\n");
	ret = api_h264_show_start(index, ffmpeg4ipc[index].width, ffmpeg4ipc[index].height, 15, 512, dispx, dispy, dispw, disph, ffmpeg4ipc[index].videotype);
	printf("dvr_show_to_quad CH=%d ret=%d\n", index,ret);
	return ret;
}

//cao_20200907_start
ffmpeg4ipc_dat_t		ffmpegForRtp;
int api_start_ffmpeg_for_rtp(unsigned char* rtsp_url,int videotype)
{
	ffmpeg4ipc_dat_t* pins = &ffmpegForRtp;

	if( pins->state )
		return -1;
	
	//SetRtpVedioEncodeH265(pins->videotype);
	
	pins->recFlag = 0;
	pins->rtpMonitor = 1;
	pins->videotype=videotype;
	strcpy( pins->filename, rtsp_url );
	pins->state			= 1;
	if( one_tiny_task_start( &pins->task_process, task_ffmpeg4ipc_init, task_ffmpeg4ipc_process, task_ffmpeg4ipc_exit, (void*)pins ) == 0 )
	{
		int wait_to = 0;
		while(1)
		{
			if( pins->state == 2 )
				return pins->videotype;
			else
			{
				usleep(100*1000);
				if( ++wait_to >= 50 )	// 3s
					return -2;
 			}
 		}		
 	}
	else
		return -1;
}

int api_stop_ffmpeg_for_rtp(void)
{
	ffmpeg4ipc_dat_t* pins = &ffmpegForRtp;

	if(pins->state)
	{
		one_tiny_task_stop( &pins->task_process );
		pins->state = 0;
	}
	
	return 0;	
}
// 线程启动前的初始化
int Get_ffmpeg4ipc_videoType( char* filename, int* w, int* h)
{
	ffmpeg4ipc_dat_t ffmpeg_dat;
	ffmpeg4ipc_dat_t *pusr_dat = (ffmpeg4ipc_dat_t*)&ffmpeg_dat;	
	int ret;

	strcpy(ffmpeg_dat.filename, filename);
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"102400", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以udp方式打开，如果以tcp方式打开将udp替换为tcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	//pusr_dat->ic = avformat_alloc_context();
	pusr_dat->ic=NULL;
	printf("\n=================begin to avformat_open_input: [%s]========\n", pusr_dat->filename );
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
	if (ret < 0) 
	{
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, &options);
		if (ret < 0) 
		{
			printf("cannot open %s\n", pusr_dat->filename);
			return -1;
		}
	}
	
	printf("\n=================begin to av_find_stream_info...========\n");
#if defined(PID_IXSE)
	pusr_dat->ic->probesize = 3 *1024;
#else
	pusr_dat->ic->probesize = 5 *1024;
#endif
	pusr_dat->ic->max_analyze_duration = 1 * AV_TIME_BASE;	
	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
	if (ret < 0) 
	{
		printf( "%s: could not find codec parameters\n", pusr_dat->filename);
		return -1;
	}

	printf("\n=================begin to av_find_stream_info cnt=[%d]========\n", pusr_dat->ic->nb_streams);
	///////////////////////////////////////////////////////

	int i;
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			*w = pusr_dat->ic->streams[i]->codecpar->width; 	   
			*h = pusr_dat->ic->streams[i]->codecpar->height;			
			pusr_dat->videotype = pusr_dat->ic->streams[i]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			break;
		}
	}
	printf("Videotype=%d width=%d, height=%d\n", pusr_dat->videotype, *w, *h);
    avformat_close_input(&pusr_dat->ic);
	return pusr_dat->videotype;
}

int GetRegisterState(void)
{
	return av_register_init;
}
int SetRegisterState(int state)
{
	av_register_init = state;
}



int cb_show_buf1( char* pdatbuf, int datlen )
{
	return api_h264_show_dump_buf( 0, pdatbuf, datlen );
}
int cb_show_buf2( char* pdatbuf, int datlen )
{
	return api_h264_show_dump_buf( 1, pdatbuf, datlen );
}
int cb_show_buf3( char* pdatbuf, int datlen )
{
	return api_h264_show_dump_buf( 2, pdatbuf, datlen );
}
int cb_show_buf4( char* pdatbuf, int datlen )
{
	return api_h264_show_dump_buf( 3, pdatbuf, datlen );
}

IPC_SHOW_DAT_T ipc_array[4] = 
{
	{
		.ipcMonitorState 	= 0,
		.src_width 			= 640,
		.src_height 		= 480,
		.vd_posx			= 0,
		.vd_posy			= 0,
		.vd_zoomin_width	= 640,
		.vd_zoomin_height	= 400,
		.vd_zoomout_width	= 640,
		.vd_zoomout_height	= 480,
		.vd_zoomin_flag		= 0,
		.vd_type			= 0,
		.vd_layer			= 0,
		.vd_fps				= 15,
		.vd_rate			= 512,
	},
	{
		.ipcMonitorState	= 0,
		.src_width			= 640,
		.src_height 		= 480,
		.vd_posx			= 640,
		.vd_posy			= 0,
		.vd_zoomin_width	= 640,
		.vd_zoomin_height	= 400,
		.vd_zoomout_width	= 640,
		.vd_zoomout_height	= 480,
		.vd_zoomin_flag 	= 0,
		.vd_type			= 0,
		.vd_layer			= 0,
		.vd_fps 			= 15,
		.vd_rate			= 512,
	},
	{
		.ipcMonitorState	= 0,
		.src_width			= 640,
		.src_height 		= 480,
		.vd_posx			= 0,
		.vd_posy			= 400,
		.vd_zoomin_width	= 640,
		.vd_zoomin_height	= 400,
		.vd_zoomout_width	= 640,
		.vd_zoomout_height	= 480,
		.vd_zoomin_flag 	= 0,
		.vd_type			= 0,
		.vd_layer			= 0,
		.vd_fps 			= 15,
		.vd_rate			= 512,
	},
	{
		.ipcMonitorState	= 0,
		.src_width			= 640,
		.src_height 		= 480,
		.vd_posx			= 640,
		.vd_posy			= 400,
		.vd_zoomin_width	= 640,
		.vd_zoomin_height	= 400,
		.vd_zoomout_width	= 640,
		.vd_zoomout_height	= 480,
		.vd_zoomin_flag 	= 0,
		.vd_type			= 0,
		.vd_layer			= 0,
		.vd_fps 			= 15,
		.vd_rate			= 512,
	},	
};

cb_ffmpeg_proc ipc_cb[4] =
{
	cb_show_buf1,
	cb_show_buf2,
	cb_show_buf3,		
	cb_show_buf4,
};

int api_ipc_show_one_stream( int ins_num, unsigned char* rtsp, int width ,int height, int vdtype)
{
	IPC_SHOW_DAT_T* ins = &ipc_array[ins_num];
	int ret;
	if(ffmpeg4ipc_check_state_error(ins_num))
	{
		api_ipc_stop_mointor_one_stream(ins_num);
	}
	long long cur_time;
	ins->src_width	= width;
	ins->src_height = height;
	ins->vd_type 	= vdtype;
	if( !ins->src_width || !ins->src_height )
	{
		ins->src_width		= 704; //1280;
		ins->src_height 	= 520; //720;
	}
	ins->src_height 	= ins->src_height==1080? 1088 : ins->src_height;
	//printf("api_ipc_start_mointor_one_stream width[%d]height[%d]-----\n", ins->src_width, ins->src_height);
	if( ins->vd_type == 0 || ins->vd_type == 1 )
	{
		#if 1
		cur_time = time_since_last_call(-1);	
		
		ret=api_h264_show_start(ins_num,
							ins->src_width,
							ins->src_height,
							ins->vd_fps,
							ins->vd_rate,
							ins->vd_posx,
							ins->vd_posy,
							ins->vd_zoomin_width,
							ins->vd_zoomin_height,
							ins->vd_type);
		if(ret!=0)
			return -1;
		printf("==api_h264_show_start ins[%d] time[%d]\n",ins_num, time_since_last_call(cur_time));	
		#endif
	}			
	ret=api_start_ffmpeg4ipc(ins_num, rtsp, 0, width, height, vdtype, ipc_cb[ins_num] );

	
	return ret;			
}


int api_ipc_start_mointor_one_stream( int ins_num, unsigned char* pstream, int ch_main, int width ,int height, int rate )
{
	IPC_SHOW_DAT_T* ins = &ipc_array[ins_num];
		
	printf("api_ipc_start_mointor_one_stream width[%d]height[%d]-----\n", width, height);
	//if(!ins->ipcMonitorState)
	{
		//ins->ipcMonitorState = 1;
		if( get_pane_type() == 1 )
		{
			ins->vd_zoomin_width = 800;
			ins->vd_zoomin_height = 640;
		}
		else if( get_pane_type() == 0 )
		{
			ins->vd_zoomin_width = 1280;
			ins->vd_zoomin_height = 800;
		}
		else
		{
			ins->vd_zoomin_width = 1024;
			ins->vd_zoomin_height = 600;
		}
		
		ins->vd_type = api_start_ffmpeg4ipc(ins_num, pstream, ch_main, width, height, rate, ipc_cb[ins_num] );
		printf("-api_start_ffmpeg4ipc-ins num[%d]vd_type[%d]width[%d]height[%d]-----\n",ins_num,ins->vd_type,ffmpeg4ipc[ins_num].width,ffmpeg4ipc[ins_num].height);
		#if 1
		if( !width || !height )
		{
			ins->src_width		= ffmpeg4ipc[ins_num].width;
			ins->src_height 	= ffmpeg4ipc[ins_num].height;
		}
		else
		{
			ins->src_width 		= width;
			ins->src_height 	= height;
		}
		if( !ins->src_width || !ins->src_height )
		{
			ins->src_width		= 640;
			ins->src_height 	= 480;
		}
		#endif
		ins->src_height 	= ins->src_height==1080? 1088 : ins->src_height;
		//printf("api_ipc_start_mointor_one_stream width[%d]height[%d]-----\n", ins->src_width, ins->src_height);
		if( ins->vd_type == 0 || ins->vd_type == 1 )
		{
			api_h264_show_start(ins_num,
								ins->src_width,
								ins->src_height,
								ins->vd_fps,
								ins->vd_rate,
								ins->vd_posx,
								ins->vd_posy,
								ins->vd_zoomin_width,
								ins->vd_zoomin_height,
								ins->vd_type);
			return ins->vd_type;			
		}			
		else 
		{
			return -1;
		}
	}
	//else
	//	return -1;
}

int api_ipc_start_mointor_size( int ipc_num, unsigned char* pstream, int ch_main, int width ,int height, int rate, int x, int y, int sizeX, int sizeY )
{
	IPC_SHOW_DAT_T* ins = &ipc_array[ipc_num];
		
	printf("api_ipc_start_mointor_one_stream width[%d]height[%d]-----\n", width, height);
	//if(!ins->ipcMonitorState)
	{		
		//ins->src_width 		= width;
		//ins->src_height 	= height;
		//ins->ipcMonitorState = 1;
				
		ins->vd_type = api_start_ffmpeg4ipc(ipc_num, pstream, ch_main, width, height, rate, ipc_cb[ipc_num] );
		printf("-api_start_ffmpeg4ipc-ins num[%d]vd_type[%d]width[%d]height[%d]-----\n",ipc_num,ins->vd_type,ffmpeg4ipc[ipc_num].width,ffmpeg4ipc[ipc_num].height);
		if( !width || !height )
		{
			ins->src_width		= ffmpeg4ipc[ipc_num].width;
			ins->src_height 	= ffmpeg4ipc[ipc_num].height;
		}
		else
		{
			ins->src_width 		= width;
			ins->src_height 	= height;
		}
		if( !ins->src_width || !ins->src_height )
		{
			ins->src_width		= 640;
			ins->src_height 	= 480;
		}
		ins->src_height 	= ins->src_height==1080? 1088 : ins->src_height;
		if( ins->vd_type == 0 || ins->vd_type == 1 )
		{
			api_h264_show_start(ipc_num,
								ins->src_width,
								ins->src_height,
								ins->vd_fps,
								ins->vd_rate,
								x,
								y,
								sizeX,
								sizeY,
								ins->vd_type);
			return ins->vd_type;			
		}			
		else 
		{
			return -1;
		}
	}
	//else
	//	return -1;
}



// 3.2 应用接口：停止流媒体显示接口
int api_ipc_stop_mointor_one_stream(int ins_num)
{
	IPC_SHOW_DAT_T* ins = &ipc_array[ins_num];

	long long cur_time;
	cur_time = time_since_last_call(-1);	
	//if(ins->ipcMonitorState)
	{
		//printf("[%d]api_ipc_stop_mointor_one_stream!!\n", ins_num);
		api_stop_ffmpeg4ipc(ins_num);
		//usleep(200*1000);
		api_h264_show_stop(ins_num);
		//ins->ipcMonitorState = 0;
		printf("==api_ipc_stop_mointor_one_stream ins[%d] time[%d] \n",ins_num, time_since_last_call(cur_time));	
	}
	return 0;
}


