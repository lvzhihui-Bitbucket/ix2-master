#ifndef _OBJ_IPC_SHOW_H_
#define _OBJ_IPC_SHOW_H_


int Get_WinId(int x, int y);
void Get_OneIpc_ShowPos(int win_id, int* x, int* y, int* width, int* height);
int Set_ipc_show_pos(int ins_num, int x, int y, int width, int height);
int Clear_ipc_show_layer(int ins_num);
	
int api_OneIpc_Show_start(int ins_num, char* device_ip, char* device_name, int ipc_ch, int win_id);
int api_IpcIndex_Show(int ins_num, int table_type,int ipc_index, int ipc_ch, int win_id);
int api_OneIpc_Show_stop( int ins_num );


#endif
