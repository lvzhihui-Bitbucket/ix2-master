

#ifndef _task_ipc_business_H
#define _task_ipc_business_H

#include "task_survey.h"

#define MSG_TYPE_IpcBusiness_Show_Start  		0
#define MSG_TYPE_IpcBusiness_Show_Stop  		1
#define MSG_TYPE_IpcBusiness_Rec_Start  		2
#define MSG_TYPE_IpcBusiness_Rec_Stop  			3



typedef struct
{
	VDP_MSG_HEAD head;
	int  ins_num;
	char device_ip[21];
	char device_name[41];
	int  table_type;
	int  device_index;
	int  ipc_ch;
	int  win_id;
}IpcBusiness_Show_Msg_t;

typedef struct
{
	VDP_MSG_HEAD head;
	int  ins_num;
}IpcBusiness_Stop_Msg_t;

typedef struct
{
	VDP_MSG_HEAD head;
	int  	ins_num;
	char 	rtsp_url[200];
	int	 	width;
	int	 	height;
	int 	vd_type;	
	int	 	x;
	int 	y;
	int  	w;
	int  	h;
}IpcBusiness_Rtsp_Msg_t;

typedef struct
{
	VDP_MSG_HEAD head;
	int  ins_num;
	char device_ip[21];
	char device_name[41];
	int  table_type;
	int  device_index;
	int  ipc_ch;
	char user[21];
	char pwd[21];
	int	 x;
	int  y;
	int  w;
	int  h;
}IpcBusiness_Show_XY;

typedef struct
{
	VDP_MSG_HEAD head;
	char device_ip[21];
	char device_name[41];
	int  device_index;
	int  ins_num;
	int  Rectime;
	int  filesize;
}IpcBusiness_Rec_Msg_t;



int API_IpcIndex_Show(int ins_num, int table_type,int ipc_index, int ipc_ch, int win_id);
int API_OneIpc_Show_XY(int ins_num, int table_type,int ipc_index, int ipc_ch, int x, int y, int w, int h);
int API_OneIpc_Show_start(int ins_num, char* device_ip, char* device_name, int ipc_ch, int win_id);
int API_IpcDevice_Show_XY(int ins_num, char* device_ip, char* device_name, char* user, char* pwd, int x, int y, int w, int h);
int API_OneIpc_Rtsp_Show(int ins_num, unsigned char* rtsp, int width ,int height, int vdtype,int x, int y, int w, int h);
int API_OneIpc_Show_stop( int ins_num );

#endif

