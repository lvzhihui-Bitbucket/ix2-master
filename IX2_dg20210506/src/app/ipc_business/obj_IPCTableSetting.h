#ifndef obj_IPCTableSetting_H
#define obj_IPCTableSetting_H


//#define IPC_SETUP_FILE_NAME			"/mnt/nand1-2/IPC_Setup.cfg"
//#define IPC_WLAN_SETUP_FILE_NAME	"/mnt/nand1-2/IPC_WLAN_Setup.cfg"

/*
{
    "IPC":{
    	"ID": "CAM1", 
    	"NAME":"CAM1", 
    	"IP": "192.168.243.6", 
    	"CH_ALL": 3, 
    	"User": "admin",
    	"PWD": "admin",
    	"CH0":{
    		"RTSP_URL": "rtsp://admin:admin@192.168.243.6:80/0",
    		"W": 1920,
			"H": 1080,
    		},
    	"CH1":{
    		"RTSP_URL": "rtsp://admin:admin@192.168.243.6:80/1",
    		"W": 640,
			"H": 480,
    		},
    	"CH2":{
    		"RTSP_URL": "rtsp://admin:admin@192.168.243.6:80/2",
    		"W": 320,
			"H": 240,
    		},
    	},

};
*/
#define	MAX_IPC_DEVICE	100

// ipc设备媒体属性表
typedef struct
{
	char 	rtsp_url[200];
	int	 	width;
	int	 	height;
	int  	bitrate;	//码率
	int		framerate;	//帧率
	int 	vd_type;	//编码格式
}login_dat_t;

typedef struct 
{
	char	ID[40];
	char	NAME[40];
	char	IP[40];
	char	USER[40];
	char	PWD[40];
	int		CH_ALL;
	int		CH_REC;
	int		CH_FULL;
	int		CH_QUAD;
	int		CH_APP;
	login_dat_t	CH_DAT[4];	
} IPC_ONE_DEVICE;

typedef struct
{
	int					deviceCnt;				//设备数量
	IPC_ONE_DEVICE 		device[MAX_IPC_DEVICE];
} IPC_DeviceTable;


char* CreateIpcSetupObject(void);
int ParseIpcSetupObject(const char* json);
int GetIpcCacheRecord(int index, IPC_ONE_DEVICE* ipcRecord);
int DeleteOneIpcRecord(int index);


int GetIpcNum(void);
int GetOneIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord);
int AddOrModifyOneIpc(IPC_ONE_DEVICE* record);
int DeleteOneIpcByIpOrName(char* ip, char* name);
void SaveIpcToFile(void);
void GetIpcFromFile(void);

int GetWlanIpcNum(void);
int GetWlanIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord);
int AddOrModifyWlanIpc(IPC_ONE_DEVICE* record);
int DeleteOneWlanIpcRecord(int index);
int DeleteWlanIpcByIpOrName(char* ip, char* name);
void SaveWlanIpcToFile(void);
void GetWlanIpcFromFile(void);

#endif

