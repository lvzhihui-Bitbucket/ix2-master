
#include "MENU_public.h"
#include "obj_ipc_manager.h"
#include "obj_IPCTableSetting.h"

/////////////////////////////////////////////////////////////////////////////////////////////////

int IpcDeviceLogin(cJSON* jsonIn, cJSON* result)
{
	onvif_login_info_t info;
	char deviceUrl[100] = {0};
	char username[50] = {0};
	char password[50] = {0};
	cJSON *ch_dat = NULL;
	int i;

	GetJsonDataPro(jsonIn, "IP_ADDR", deviceUrl);
	GetJsonDataPro(jsonIn, "USR_NAME", username);
	GetJsonDataPro(jsonIn, "USR_PWD", password);

	memset(&info, 0, sizeof(onvif_login_info_t));
	int ret = one_ipc_Login(deviceUrl, username, password, &info);
	for (i = 0; i < info.ch_cnt; i++)
	{
		//info.dat[i].vd_type = Get_ffmpeg4ipc_videoType(info.dat[i].rtsp_url, &info.dat[i].width, &info.dat[i].height);
		ch_dat = cJSON_CreateObject();
		if(info.dat[i].vd_type == -1)
			info.dat[i].vd_type = 0;
		cJSON_AddStringToObject(ch_dat, "RTSP_URL", info.dat[i].rtsp_url);
		cJSON_AddNumberToObject(ch_dat, "RES_W", info.dat[i].width);
		cJSON_AddNumberToObject(ch_dat, "RES_H", info.dat[i].height);
		cJSON_AddNumberToObject(ch_dat, "BPS", info.dat[i].bitrate);
		cJSON_AddNumberToObject(ch_dat, "FPS", info.dat[i].framerate);
		cJSON_AddNumberToObject(ch_dat, "CODEC", info.dat[i].vd_type);
		cJSON_AddItemToArray(result, ch_dat);
	}
	return ret;
}

int ShellCmd_IpcManage(cJSON *cmd)
{
	char* jsonstr;
	cJSON *login;
	char* ctrlStr = GetShellCmdInputString(cmd, 1);
	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Input error!\n");
	}
	else if(!strcmp(ctrlStr, "login"))
	{
		ShellCmdPrintf("Ipc login start!\n");
		jsonstr = GetShellCmdInputString(cmd, 2);
		if( jsonstr != NULL )
		{
			cJSON *cmd_json = cJSON_Parse(jsonstr);
			if( cmd_json != NULL )
			{
				login = cJSON_CreateArray();
				IpcDeviceLogin(cmd_json, login);
				char* viewStr = cJSON_Print(login);
				ShellCmdPrintf("Ipc login result:%s\n", viewStr);
				if(viewStr)
				{
					free(viewStr);
				}
				cJSON_Delete(login);
				cJSON_Delete(cmd_json);
			}
			else
			{
				ShellCmdPrintf("Input error!\n");
			}
		}
		else
		{
			ShellCmdPrintf("Input error!\n");
		}
	}
}

//LAN/WLAN��ͬ��������
int ipc_device_discovery(int wlan)
{
#if 0
	if(mode == 1)
		ChangeDefaultGateway(NET_WLAN0, NULL);
	
	onvif_device_discovery();
	
	usleep(100000);

	if(mode == 1)
		ChangeDefaultGateway(NULL, NULL);
#endif	
	
}

int AddIPCBySearching(int wlan)
{
	int dis_num,add_num = 0;
	int i,j;
	
	IPC_ONE_DEVICE ipcRecord;
	onvif_login_info_t ipcLogin;
	
	if(wlan == 1)
		ChangeDefaultGateway(NET_WLAN0, NULL);
	
	onvif_device_discovery();
	
	usleep(100000);

	if(wlan == 1)
		ChangeDefaultGateway(NULL, NULL);
	
	dis_num = get_ipc_device_total();
	
	for(i = 0;i < dis_num;i ++)
	{
		ipc_discovery_device_get_addr(i, ipcRecord.IP, ipcRecord.NAME);

		strcpy(ipcRecord.USER, "admin");
		strcpy(ipcRecord.PWD, "admin");
		strcpy(ipcRecord.ID, ipcRecord.NAME);

		if(wlan == 1)
		{
			if(strcmp(GetNetDeviceNameByTargetIp(inet_addr(ipcRecord.IP)), NET_WLAN0))
			{
				continue;
			}
		}

		if(one_ipc_Login(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, &ipcLogin) == 0)
		{
			sprintf(ipcRecord.NAME,"IP-CAM%d",++add_num);
			ipcRecord.CH_ALL = ipcLogin.ch_cnt;
			if(ipcRecord.CH_ALL < 3)
			{
				ipcRecord.CH_REC = 0;
				ipcRecord.CH_FULL = 0;
				ipcRecord.CH_QUAD = 1;
				ipcRecord.CH_APP = 1;
			}
			else
			{
				ipcRecord.CH_REC = 0;
				ipcRecord.CH_FULL = 0;
				ipcRecord.CH_QUAD = 1;
				ipcRecord.CH_APP = 2;
			}
			for(j=0; j<ipcRecord.CH_ALL; j++ )
			{
				ipcRecord.CH_DAT[j] = ipcLogin.dat[j];
			}
			SaveIpcDevice(wlan, &ipcRecord);
			printf("!!!!!!!!!!add tb ok %d,%s\n",i,ipcRecord.NAME);
		}
		else
		{
			printf("!!!!!!!!!!fail onvif_Login %d\n",i);
		}
	}

	return add_num;
}

int AddOneIpc(const char* device_url, const char* username,const char* password)
{
	int i;
	onvif_login_info_t ipcLogin;
	IPC_ONE_DEVICE ipcRecord;
	if(GetNameByIp(device_url, ipcRecord.NAME) == 0)
	{
		strcpy(ipcRecord.USER, device_url);
		strcpy(ipcRecord.PWD, username);
		strcpy(ipcRecord.ID, ipcRecord.NAME);
		memset(&ipcLogin, 0, sizeof(onvif_login_info_t));
		if(one_ipc_Login(device_url, username, password, &ipcLogin) == 0)
		{
			ipcRecord.CH_ALL = ipcLogin.ch_cnt;
			if(ipcRecord.CH_ALL < 3)
			{
				ipcRecord.CH_REC = 0;
				ipcRecord.CH_FULL = 0;
				ipcRecord.CH_QUAD = 1;
				ipcRecord.CH_APP = 1;
			}
			else
			{
				ipcRecord.CH_REC = 0;
				ipcRecord.CH_FULL = 0;
				ipcRecord.CH_QUAD = 1;
				ipcRecord.CH_APP = 2;
			}
			for(i=0; i<ipcRecord.CH_ALL; i++ )
			{
				ipcRecord.CH_DAT[i] = ipcLogin.dat[i];
			}
			if(strcmp(GetNetDeviceNameByTargetIp(inet_addr(ipcRecord.IP)), NET_WLAN0) == 0)
				SaveIpcDevice(1, &ipcRecord);
			else
				SaveIpcDevice(0, &ipcRecord);
		}
		else
		{
			printf("!!!!!!!!!!fail onvif_Login %d\n",i);
		}
	}
		
}


int ipc_check_online(const char* pIP)
{
	char name[100] = {0};
	return GetNameByIp(pIP, name);

}

int SaveIpcDevice(int wlan, IPC_ONE_DEVICE* ipcRecord )
{
	if(wlan == 0)
		AddOrModifyOneIpc(ipcRecord);
	else
		AddOrModifyWlanIpc(ipcRecord);
}

int ipc_check_exist(char* device_ip, char* device_name)
{
	int ret = -1;
	ret = CheckIpcExist(device_ip, NULL);
	if(ret == -1)
	{
		ret = CheckWlanIpcExist(device_ip, NULL);
	}
	return ret;
}
int GetIpcChannelInfo(char* device_ip, char* device_name, int* ch_rec, int* ch_full, int* ch_quad, int* ch_app)
{
	int ret = -1;
	ret = getIpcChannel(device_ip, NULL, ch_rec, ch_full, ch_quad, ch_app);
	if(ret == -1)
	{
		ret = getWlanIpcChannel(device_ip, NULL, ch_rec, ch_full, ch_quad, ch_app);
	}
	return ret;
}
int GetIpcDeviceInfo(char* device_ip, char* device_name,IPC_ONE_DEVICE* ipcRecord)
{
	int ret = -1;
	ret = GetOneIpcByIpOrName(device_ip, NULL, ipcRecord);
	if(ret == -1)
	{
		ret = GetWlanIpcByIpOrName(device_ip, NULL, ipcRecord);
	}
	return ret;
}
int DeleteOneIpc(char* device_ip, char* device_name)
{
	int ret = -1;
		ret = DeleteOneIpcByIpOrName(device_ip, NULL);
	if(ret == -1)
	{
		DeleteWlanIpcByIpOrName(device_ip, NULL);
	}
	return ret;
}

int SetIpcDeviceInfo(char* device_ip, char* name)
{
	IPC_ONE_DEVICE ipcRecord;
	if(strcmp(GetNetDeviceNameByTargetIp(inet_addr(device_ip)), NET_WLAN0))
	{
		if(GetOneIpcByIpOrName(device_ip, NULL, &ipcRecord) == 0)
		{
			strcpy(ipcRecord.NAME, name);
			AddOrModifyOneIpc(&ipcRecord);
		}
	}
	else
	{
		if(GetWlanIpcByIpOrName(device_ip, NULL, &ipcRecord) ==0)
		{
			strcpy(ipcRecord.NAME, name);
			AddOrModifyWlanIpc(&ipcRecord);
		}
	}
}

int ClearIpcDevice(int wlan)
{
	if(wlan == 0)
	{
		ClearIpcTable();
		SaveIpcToFile();
	}
	else
	{
		ClearWlanIpc();
		SaveWlanIpcToFile();
	}

}

int ReloadIpcDevice(int wlan)
{
	if(wlan == 0)
	{
		ClearIpcTable();
		GetIpcFromFile();
	}
	else
	{
		ClearWlanIpc();
		GetWlanIpcFromFile();
	}
}

void Ipc_Reload(void)
{
	ClearIpcTable();
	GetIpcFromFile();
	DeleteFileProcess(NULL, GetR8023FileName());
	GetAppIpcListFromFile();
}
void WlanIpc_Reload(void)
{
	ClearWlanIpc();
	GetWlanIpcFromFile();
	DeleteFileProcess(NULL, GetR8023FileName());
	GetAppIpcListFromFile();
}

static ipc_device_list_t	ipc_discovery_device;
int ipc_discovery_device_init(void)
{
	ipc_discovery_device.all = 0;
	return 0;
}

int ipc_discovery_add_addr(char* device_ip, char* device_name )
{
	int i;
	
	if( ipc_discovery_device.all >= TOTAL_IPC_DEVICES )
		return -1;
	
	for(i=0; i < ipc_discovery_device.all; i++)
	{
		if(!strcmp(device_ip, ipc_discovery_device.dat[i].url))
		{
			return 0;
		}
	}
	if(ipc_check_exist(device_ip, NULL) == 0)
	{
		return 0;
	}
	strcpy( ipc_discovery_device.dat[ipc_discovery_device.all].url, device_ip);
	strcpy( ipc_discovery_device.dat[ipc_discovery_device.all].name, device_name);
	
	ipc_discovery_device.all++;
	printf("ipc_discovery_device:cnt=%d,device_url[%s],device_name[%s]\n",ipc_discovery_device.all,device_ip,device_name);
	return 0;
}

int get_ipc_device_total( void )
{
	return ipc_discovery_device.all;
}

int	ipc_discovery_device_get_addr( int index, char* device_url, char* name)
{
	if( index >= ipc_discovery_device.all )
		return -1;
	 strcpy( device_url, ipc_discovery_device.dat[index].url );
	 strcpy( name, ipc_discovery_device.dat[index].name );
	 printf("index=%d,device url[%s],device name[%s]\n",index,device_url,name);
	 return 0;
}

int ipc_discovery_device_list_get_url( int index, char* device_url )
{
	if(index >= ipc_discovery_device.all)
	{
		return -1;
	}
	strcpy(device_url, ipc_discovery_device.dat[index].url);
	return 0;
}

int ipc_discovery_device_list_set_name( int index, char* name )
{
	if(index >= ipc_discovery_device.all)
	{
		return -1;
	}
	
	strcpy(ipc_discovery_device.dat[index].name, name);
	return 0;
}

int ipc_discovery_device_list_get_name( int index, char* name )
{
	if(index >= ipc_discovery_device.all)
	{
		return -1;
	}
	strcpy(name, ipc_discovery_device.dat[index].name);

	return 0;
}



int ipc_discovery_device_list_delete( const char* url)
{
	int i;
	
	for(i = 0; i< ipc_discovery_device.all; i++)
	{
		if(!strcmp(url, ipc_discovery_device.dat[i].url))
		{
			for( ; i+1 < ipc_discovery_device.all; i++)
			{
				ipc_discovery_device.dat[i] = ipc_discovery_device.dat[i+1];
			}
			ipc_discovery_device.all--;
			return 0;
		}
	}

	return -1;
}


