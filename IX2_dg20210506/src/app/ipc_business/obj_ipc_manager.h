

#ifndef _OBJ_IPC_MANAGER_H_
#define _OBJ_IPC_MANAGER_H_

#include "obj_IPCTableSetting.h"

#define	TOTAL_IPC_DEVICES		100

typedef struct
{
	// 搜索得到的
	char	url[100];	
	char	name[100];
} ipc_device_node;

// 所有设备地址属性列表
typedef struct
{
	int		all;
	ipc_device_node	dat[TOTAL_IPC_DEVICES];
} ipc_device_list_t;


typedef struct
{
	int		ch_cnt;	//通道数
	login_dat_t	dat[4];	
} onvif_login_info_t;


int ipc_discovery_device_init(void);
int ipc_discovery_add_addr(char* device_ip, char* device_name );
int get_ipc_device_total( void );
int ipc_discovery_device_list_get_url( int index, char* device_url );
int ipc_discovery_device_list_get_name( int index, char* name );
int ipc_discovery_device_list_set_name( int index, char* name );
int ipc_discovery_device_list_delete( const char* url);


int AddIPCBySearching(int wlan);
int AddOneIpc(const char* device_url, const char* username,const char* password);
int ipc_check_online(const char* pIP);
int SaveIpcDevice(int wlan, IPC_ONE_DEVICE* ipcRecord );
int GetIpcDeviceInfo(char* device_ip, char* device_name,IPC_ONE_DEVICE* ipcRecord);
int SetIpcDeviceInfo(char* device_ip, char* name);
int DeleteOneIpc(char* device_ip, char* device_name);
int ClearIpcDevice(int wlan);

#endif
