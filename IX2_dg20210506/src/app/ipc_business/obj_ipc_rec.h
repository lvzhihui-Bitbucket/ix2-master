#ifndef _OBJ_IPC_REC_H_
#define _OBJ_IPC_REC_H_

#define REC_SCHEDULE_FILE_NAME			"/mnt/nand1-2/Rec_Schedule.cfg"

typedef struct
{
	int			state;
	int			recTime;
} VD_REC_T;


typedef struct 
{
	int 	ch;
	char	ipc_name[40];
	char	ipc_ip[40];
	char	timeS[10];
	char	timeE[10];
	int		week[8];	
	int 	weekNum;
}REC_SCHEDULE_DAT_T;
typedef struct
{
	int						num;				//ͨ����
	REC_SCHEDULE_DAT_T		channel[4];
} REC_SCHEDULE_Table;

	
int API_OneIpc_Rec_start(int ch, char* device_ip, char* device_name, int Rectime, int filesize);
int API_IpcIndex_Rec(int ch, int ipc_index, int Rectime, int filesize);
int API_OneIpc_Rec_stop( int ch );
int Get_OneIpc_Rec_State(int ch);


int GetRecScheduleRecord(int index, REC_SCHEDULE_DAT_T* record);
int GetOneRecSchedule(int ch, REC_SCHEDULE_DAT_T* record);
int SetOneRecSchedule(REC_SCHEDULE_DAT_T* record);
int DeleteOneRecSchedule(int ch);
void SaveRecScheduleToFile(void);
void GetRecScheduleFromFile(void);

void RecScheduleOperation(void);


#endif
