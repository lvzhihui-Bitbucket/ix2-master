
#ifndef _FFMPEG_PLAY_H_
#define _FFMPEG_PLAY_H_

#include "onvif_tiny_task.h"
#include "video_utility.h"
#include "task_VideoMenu.h"

#include "libavutil/common.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"

typedef enum
{
	 FFMPEG_PLAYBACK_IDLE,
	 FFMPEG_PLAYBACK_START,
	 FFMPEG_PLAYBACK_RUN,
	 FFMPEG_PLAYBACK_STOP,
	 FFMPEG_PLAYBACK_PAUSE,
	 FFMPEG_PLAYBACK_END,
	 FFMPEG_PLAYBACK_REPLAY,
	 FFMPEG_PLAYBACK_CH_FILE_START,
	 FFMPEG_PLAYBACK_CH_FILE_DOING,
	 FFMPEG_PLAYBACK_CH_FILE_OK,
}FFMPEG_PLAYBACK_STATE_t;

typedef  void (*PlaybackCallBack)(int sec, int max);

typedef struct
{
	FFMPEG_PLAYBACK_STATE_t	state;		// 
	char				filename[240];
	// attribute
	int					videotype;		// 0:h264, 1:h265, 2:mpeg4
	int					width;
	int					height;
	int					fps;
	int 				videostream;	
	int 				TotalSec;
	int					TotalFrames;
	int					play_time;	//当前走时
	int					play_frame; //当前帧数
	int					speedFF;	//前进、后退，跳转标识
	int					ffTime;		//跳到第几秒
	int					play_rate; //倍速
	PlaybackCallBack 	CallBack;		
	pthread_mutex_t 	aviPlaybackStateLock;

	AVFormatContext 	*ic;
	one_tiny_task_t 	task_process;	
} ffmpegPlay_dat_t;

int api_start_ffmpegPlay(char* file, PlaybackCallBack fun );
int api_stop_ffmpegPlay(void);
int api_ffmpegPlayFf(int sec, int ff );
int api_ffmpegPlaySpeed(int           speed );
int api_ffmpegPlayPause( void );
int api_ffmpegPlayContinue( void );
int api_ffmpegPlayReplay( void );
FFMPEG_PLAYBACK_STATE_t api_getPlayState( void );
void api_setPlayState( FFMPEG_PLAYBACK_STATE_t state );

#endif



