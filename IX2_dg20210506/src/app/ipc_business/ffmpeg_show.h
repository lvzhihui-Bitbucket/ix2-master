
#ifndef _FFMPEG_SHOW_H_
#define _FFMPEG_SHOW_H_

#include "onvif_tiny_task.h"

#include "libavutil/common.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"

#define	FFMPEG_3_4_2_VERSION

typedef  int (*cb_ffmpeg_proc)(unsigned char* buf, int len);

typedef struct
{
	int					state;		// 0:idle, 1:running
		
	char				filename[240];
	// attribute
	int					width;
	int					height;
	int					fps;
	int					baudrate;

	AVFormatContext 	*ic;
	AVFormatContext 	*out_ic;

#ifdef FFMPEG_3_4_2_VERSION
	AVInputFormat 		params;
#else
	AVFormatParameters 	params;
#endif
	int 				videostream;	
	int					videotype;		// 0:h264, 1:h265, 2:mpeg4
	int					videoCH;		// ͨ����
	int					recFlag;		// ¼���־
	int					rtpMonitor;		//cao_20200907
	cb_ffmpeg_proc		dat_process;
	one_tiny_task_t 	task_process;	
	pthread_mutex_t 	ffmpegStateLock;
	char *			data_buff;
	int				data_buff_len;
	time_t			start_time;
} ffmpeg4ipc_dat_t;


typedef struct 
{
	int ipcMonitorState;
	int src_width;
	int src_height;
	int vd_posx;
	int vd_posy;
	int vd_zoomin_width;
	int vd_zoomin_height;
	int vd_zoomout_width;
	int vd_zoomout_height;
	int vd_zoomin_flag;
	int vd_type;
	int vd_layer;
	int vd_fps;
	int vd_rate;
} IPC_SHOW_DAT_T;

int api_ipc_show_one_stream( int ins_num, unsigned char* rtsp, int width ,int height, int vdtype);

int api_ipc_start_mointor_one_stream( int ins_num, unsigned char* pstream, int ch_main, int width ,int height, int rate );

int api_ipc_stop_mointor_one_stream(int ins_num);

int api_ipc_start_mointor_size( int ipc_num, unsigned char* pstream, int ch_main, int width ,int height, int rate, int x, int y, int sizeX, int sizeY );


int api_start_ffmpeg4ipc(int ffmpe_num, unsigned char* rtsp_url, int ch_main, int width, int height, int rate, cb_ffmpeg_proc proc );
int api_stop_ffmpeg4ipc(int ffmpe_num);

#endif



