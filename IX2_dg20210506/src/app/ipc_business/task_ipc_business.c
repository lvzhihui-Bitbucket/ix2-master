#include "task_ipc_business.h"
#include "task_VideoMenu.h"


Loop_vdp_common_buffer	vdp_ipc_business_inner_queue;
Loop_vdp_common_buffer	vdp_ipc_business_sync_queue;
vdp_task_t				task_ipc_business;

OS_TIMER 	timer_rec_schedule;

void vdp_ipc_business_inner_data_process(char* msg_data, int len);
void* vdp_ipc_business_task( void* arg );

void timer_rec_schedule_callback(void)
{
	//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DVR_SCHEDULE);
	OS_RetriggerTimer(&timer_rec_schedule);
}


void Ipc_Business_init(void)
{
	ffmpeg_init();
	api_monitor_show_initial();
	api_h264_show_initial();
	OS_CreateTimer(&timer_rec_schedule, timer_rec_schedule_callback, 60000/25);
	GetIpcFromFile();
	GetWlanIpcFromFile();
	GetAppIpcListFromFile();
	GetRecScheduleFromFile();
}

	
void vtk_TaskInit_ipc_business(int priority)
{	
	init_vdp_common_queue(&vdp_ipc_business_inner_queue, 1000, (msg_process)vdp_ipc_business_inner_data_process, &task_ipc_business);
	init_vdp_common_queue(&vdp_ipc_business_sync_queue, 100, NULL,								  			&task_ipc_business);
	init_vdp_common_task(&task_ipc_business, MSG_ID_IPC_BUSINESS, vdp_ipc_business_task, &vdp_ipc_business_inner_queue, &vdp_ipc_business_sync_queue);

	dprintf("vdp_ipc_business_task starting............\n");
}

void exit_vdp_ipc_business_task(void)
{
	exit_vdp_common_queue(&vdp_ipc_business_inner_queue);
	exit_vdp_common_queue(&vdp_ipc_business_sync_queue);
	exit_vdp_common_task(&task_ipc_business);
}

void* vdp_ipc_business_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	Ipc_Business_init();
	
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_ipc_business_inner_data_process(char* msg_data,int len)
{
	IpcBusiness_Show_Msg_t* pMsgIpcShow = (IpcBusiness_Show_Msg_t*)msg_data;
	IpcBusiness_Stop_Msg_t* pMsgIpcStop = (IpcBusiness_Stop_Msg_t*)msg_data;
	IpcBusiness_Rtsp_Msg_t* pMsgIpcRtsp = (IpcBusiness_Rtsp_Msg_t*)msg_data;
	IpcBusiness_Show_XY* pMsgIpcShowXY = (IpcBusiness_Show_XY*)msg_data;
	int ret;
	switch (pMsgIpcShow->head.msg_type)
	{
		case MSG_TYPE_IpcBusiness_Show_Start:
			#if defined(PID_IX850)
			#else
			#ifdef PID_IXSE
			Api_Ds_Show_BeClose(0);
			#endif
			if(pMsgIpcShow->head.msg_sub_type == 0)
				ret = api_IpcIndex_Show(pMsgIpcShow->ins_num,pMsgIpcShow->table_type,pMsgIpcShow->device_index, pMsgIpcShow->ipc_ch, pMsgIpcShow->win_id);
			else if(pMsgIpcShow->head.msg_sub_type == 1)
				ret = api_OneIpc_Show_start(pMsgIpcShow->ins_num,pMsgIpcShow->device_ip,pMsgIpcShow->device_name,pMsgIpcShow->ipc_ch, pMsgIpcShow->win_id);
			else if(pMsgIpcShow->head.msg_sub_type == 2)
				ret = api_OneIpc_Rtsp_Show(pMsgIpcRtsp->ins_num,pMsgIpcRtsp->rtsp_url,pMsgIpcRtsp->width,pMsgIpcRtsp->height, pMsgIpcRtsp->vd_type,pMsgIpcRtsp->x,pMsgIpcRtsp->y,pMsgIpcRtsp->w,pMsgIpcRtsp->h);
			else if(pMsgIpcShow->head.msg_sub_type == 3)
				ret = api_OneIpc_Show_xy(pMsgIpcShowXY->ins_num,pMsgIpcShowXY->table_type,pMsgIpcShowXY->device_index, pMsgIpcShowXY->ipc_ch, pMsgIpcShowXY->x, pMsgIpcShowXY->y, pMsgIpcShowXY->w, pMsgIpcShowXY->h);
			else
				ret = api_IpcDevice_Show_xy(pMsgIpcShowXY->ins_num,pMsgIpcShowXY->device_ip,pMsgIpcShowXY->device_name, pMsgIpcShowXY->user,pMsgIpcShowXY->pwd, pMsgIpcShowXY->x, pMsgIpcShowXY->y, pMsgIpcShowXY->w, pMsgIpcShowXY->h);
			if(ret != -1)
			{
				SetWinMonState(pMsgIpcShow->ins_num, 1);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStart);
			}
			else
			{
				API_Beep(BEEP_TYPE_DI3);
			}
			#endif
		break;
		case MSG_TYPE_IpcBusiness_Show_Stop:
			api_OneIpc_Show_stop(pMsgIpcStop->ins_num);
		break;
	}
	
}


int API_IpcIndex_Show(int ins_num,int table_type, int ipc_index, int ipc_ch, int win_id)
{
	IpcBusiness_Show_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Start;
	msg.head.msg_sub_type 	= 0;
	msg.ins_num = ins_num;
	msg.table_type=table_type;
	msg.device_index = ipc_index;
	msg.ipc_ch = ipc_ch;
	msg.win_id = win_id;
	// 压入本地队列
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));

}

int API_OneIpc_Show_start(int ins_num, char* device_ip, char* device_name, int ipc_ch, int win_id)
{
	IpcBusiness_Show_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Start;
	msg.head.msg_sub_type 	= 1;
	msg.ins_num = ins_num;
	msg.ipc_ch = ipc_ch;
	msg.win_id = win_id;
	memcpy(msg.device_ip,device_ip,20);
	memcpy(msg.device_name,device_name,40);
	
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));

}
int API_OneIpc_Rtsp_Show(int ins_num, unsigned char* rtsp, int width ,int height, int vdtype,int x, int y, int w, int h)
{
	IpcBusiness_Rtsp_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Start;
	msg.head.msg_sub_type 	= 2;
	msg.ins_num = ins_num;
	msg.width = width;
	msg.height = height;
	msg.vd_type = vdtype;
	memcpy(msg.rtsp_url,rtsp,200);
	msg.x = x;
	msg.y = y;
	msg.w = w;
	msg.h = h;
	
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));

}

int API_OneIpc_Show_XY(int ins_num, int table_type,int ipc_index, int ipc_ch, int x, int y, int w, int h)
{
	IpcBusiness_Show_XY msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Start;
	msg.head.msg_sub_type 	= 3;
	msg.ins_num = ins_num;
	msg.table_type=table_type;
	msg.device_index = ipc_index;
	msg.ipc_ch = ipc_ch;
	msg.x = x;
	msg.y = y;
	msg.w = w;
	msg.h = h;
	
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));
}

int API_IpcDevice_Show_XY(int ins_num, char* device_ip, char* device_name, char* user, char* pwd, int x, int y, int w, int h)
{
	IpcBusiness_Show_XY msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Start;
	msg.head.msg_sub_type 	= 4;
	msg.ins_num = ins_num;
	memcpy(msg.device_ip,device_ip,20);
	memcpy(msg.device_name,device_name,40);
	memcpy(msg.user,user,20);
	memcpy(msg.pwd,pwd,20);
	msg.x = x;
	msg.y = y;
	msg.w = w;
	msg.h = h;
	
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));
}

int API_OneIpc_Show_stop( int ins_num )
{
	IpcBusiness_Stop_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_IPC_BUSINESS;
	msg.head.msg_type 		= MSG_TYPE_IpcBusiness_Show_Stop;
	msg.head.msg_sub_type 	= 0;
	msg.ins_num = ins_num;
	return push_vdp_common_queue(&vdp_ipc_business_inner_queue, (char*)&msg, sizeof(msg));
}


