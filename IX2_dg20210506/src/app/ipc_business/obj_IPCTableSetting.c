#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "obj_IPCTableSetting.h"
#include "obj_APP_IPC_List.h"
#include "define_file.h"

/*
{
    "IPC":{
    	"ID": "CAM1", 
    	"NAME":"CAM1", 
    	"IP": "192.168.243.6", 
    	"CH_ALL": 3, 
    	"User": "admin",
    	"PWD": "admin",
    	"CH0":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/0",
    		"width": 1920,
			"height": 1080,
    		},
    	"CH1":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/1",
    		"width": 640,
			"height": 480,
    		},
    	"CH2":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/2",
    		"width": 320,
			"height": 240,
    		},
    	},

};
*/

static IPC_DeviceTable ipcCacheTable = {0};

int GetIpcNum(void)
{
	return ipcCacheTable.deviceCnt;
}

int CheckIpcExist(char* ip, char* name)
{
	int i;
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(ip!= NULL)
		{
			if(!strcmp(ip, ipcCacheTable.device[i].IP))
			{
				return 0;
			}
		}
		else if(name != NULL)
		{
			if(!strcmp(name, ipcCacheTable.device[i].NAME))
			{
				return 0;
			}
		}
	}

	return -1;
}
int getIpcChannel(char* ip, char* name, int* ch_rec, int* ch_full, int* ch_quad, int* ch_app)
{
	int i;
	int ret = -1;
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(ip!= NULL)
		{
			if(!strcmp(ip, ipcCacheTable.device[i].IP))
			{
				ret = 0;
				break;
			}
		}
		else if(name != NULL)
		{
			if(!strcmp(name, ipcCacheTable.device[i].NAME))
			{
				ret = 0;
				break;
			}
		}
	}
	if(i < ipcCacheTable.deviceCnt)
	{
		*ch_rec = ipcCacheTable.device[i].CH_REC;
		*ch_full = ipcCacheTable.device[i].CH_FULL;
		*ch_quad = ipcCacheTable.device[i].CH_QUAD;
		*ch_app = ipcCacheTable.device[i].CH_APP;
	}
	
	return ret;
}

int GetIpcCacheRecord(int index, IPC_ONE_DEVICE* ipcRecord)
{
	if(ipcCacheTable.deviceCnt <= index)
	{
		return -1;
	}
	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));

	*ipcRecord = ipcCacheTable.device[index];
	return 0;
}

int GetOneIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord)
{
	int i;

	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));

	
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(strcmp(name, ipcCacheTable.device[i].NAME))
			{
				continue;
			}
		}
		else if(ip!= NULL)
		{
			if(strcmp(ip, ipcCacheTable.device[i].IP))
			{
				continue;
			}
		}

		*ipcRecord = ipcCacheTable.device[i];
		return 0;
	}

	return -1;
}


int GetIpcRecordByNameOrId(char* name, char* id, IPC_ONE_DEVICE* ipcRecord)
{
	int i;

	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));

	
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(strcmp(name, ipcCacheTable.device[i].NAME))
			{
				continue;
			}
		}
		if(id != NULL)
		{
			if(strcmp(id, ipcCacheTable.device[i].ID))
			{
				continue;
			}
		}

		*ipcRecord = ipcCacheTable.device[i];
		return 0;
	}

	return -1;
}

int DeleteOneIpcRecord(int index)
{
	int i;
	if(ipcCacheTable.deviceCnt <= index)
	{
		return -1;
	}
	DeleteOneAppIpcListRecordByIdAndName(ipcCacheTable.device[index].ID, ipcCacheTable.device[index].NAME);

	for(i=index; i+1 < ipcCacheTable.deviceCnt; i++)
	{
		ipcCacheTable.device[i] = ipcCacheTable.device[i+1];
	}
	ipcCacheTable.deviceCnt--;
	SaveIpcToFile();	
	return 0;	
}

int DeleteOneIpcByIpOrName(char* ip, char* name)
{
	int i,j;
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(!strcmp(name, ipcCacheTable.device[i].NAME))
			{
				break;
			}
		}
		else if(ip!= NULL)
		{
			if(!strcmp(ip, ipcCacheTable.device[i].IP))
			{
				break;
			}
		}
	}
	if(i == ipcCacheTable.deviceCnt)
		return -1;

	return DeleteOneIpcRecord(i);
}


int AddOrModifyOneIpc(IPC_ONE_DEVICE* record)
{
	int i;

	if( ipcCacheTable.deviceCnt >= MAX_IPC_DEVICE )
		return -1;
	//AddOneAppIpcListFromIpcCacheTable(*record, 1);
	
	for(i = 0; i < ipcCacheTable.deviceCnt; i++)
	{
		if(!strcmp(record->IP, ipcCacheTable.device[i].IP))
		{

			DeleteOneAppIpcListRecordByIdAndName(ipcCacheTable.device[i].ID,ipcCacheTable.device[i].NAME);
			AddOneAppIpcListFromIpcCacheTable(*record, 1);
			ipcCacheTable.device[i] = *record;
			SaveIpcToFile();
			return 1;	//已经存在，修改完成
		}
		#if 0
		else if(!strcmp(record->ID, ipcCacheTable.device[i].ID))
		{
			ipcCacheTable.device[i] = *record;
			SaveIpcToFile();
			return 1;	//已经存在，修改完成
		}
		#endif
	}
	AddOneAppIpcListFromIpcCacheTable(*record, 1);
	printf("AddOneIpcRecord record=%s\n", record->NAME);
	ipcCacheTable.device[ipcCacheTable.deviceCnt++] = *record;
	SaveIpcToFile();
	return 0;	//添加成功
}


char* CreateIpcSetupObject(void)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	cJSON *ch = NULL;
	cJSON *ch_dat = NULL;
	char *string = NULL;
	int i,j;
	char name[20];

    root = cJSON_CreateObject();
	cJSON_AddItemToObject(root, "ipcs", devices = cJSON_CreateArray());	
    for (i = 0; i < ipcCacheTable.deviceCnt; i++)
    {
		device = cJSON_CreateObject();
		
		cJSON_AddStringToObject(device, "ID", ipcCacheTable.device[i].ID);
		cJSON_AddStringToObject(device, "NAME", ipcCacheTable.device[i].NAME);
		cJSON_AddStringToObject(device, "IP", ipcCacheTable.device[i].IP);
		cJSON_AddStringToObject(device, "USER", ipcCacheTable.device[i].USER);
		cJSON_AddStringToObject(device, "PWD", ipcCacheTable.device[i].PWD);
		cJSON_AddNumberToObject(device, "CH_ALL", ipcCacheTable.device[i].CH_ALL);
		cJSON_AddNumberToObject(device, "CH_REC", ipcCacheTable.device[i].CH_REC);
		cJSON_AddNumberToObject(device, "CH_FULL", ipcCacheTable.device[i].CH_FULL);
		cJSON_AddNumberToObject(device, "CH_QUAD", ipcCacheTable.device[i].CH_QUAD);
		cJSON_AddNumberToObject(device, "CH_APP", ipcCacheTable.device[i].CH_APP);
		
		cJSON_AddItemToObject(device, "CH_DAT", ch = cJSON_CreateArray()); 
		for (j = 0; j < ipcCacheTable.device[i].CH_ALL; j++)
		{
			ch_dat = cJSON_CreateObject();
			sprintf(name, "RTSP_URL%d", j);
			cJSON_AddStringToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].rtsp_url);
			sprintf(name, "W%d", j);
			cJSON_AddNumberToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].width);
			sprintf(name, "H%d", j);
			cJSON_AddNumberToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].height);
			sprintf(name, "BPS%d", j);
			cJSON_AddNumberToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].bitrate);
			sprintf(name, "FPS%d", j);
			cJSON_AddNumberToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].framerate);
			sprintf(name, "CODE%d", j);
			cJSON_AddNumberToObject(ch_dat, name, ipcCacheTable.device[i].CH_DAT[j].vd_type);
			cJSON_AddItemToArray(ch, ch_dat);
		}
		
		cJSON_AddItemToArray(devices, device);
	}
	
	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


int ParseIpcSetupObject(const char* json)
{
	int status = 0;
	int i,j;
    cJSON *devices = NULL;
    cJSON *pSub = NULL;
	cJSON *ch_dat = NULL;
    cJSON *pSub2 = NULL;
	char name[20];
	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}

	devices = cJSON_GetObjectItem( root, "ipcs");
	if(cJSON_IsArray(devices))
	{
		ipcCacheTable.deviceCnt = cJSON_GetArraySize ( devices );
		printf("ParseIpcSetupObject cnt=%d\n", ipcCacheTable.deviceCnt);
		for(i=0; i<ipcCacheTable.deviceCnt; i++)
	    {
			pSub = cJSON_GetArrayItem(devices, i);
			if(NULL == pSub ){ continue ; }
			ch_dat = cJSON_GetObjectItem( pSub, "CH_DAT");
			
			ParseJsonString(pSub, "ID", ipcCacheTable.device[i].ID, 40);
			ParseJsonString(pSub, "NAME", ipcCacheTable.device[i].NAME, 40);
			ParseJsonString(pSub, "IP", ipcCacheTable.device[i].IP, 100);
			ParseJsonString(pSub, "USER", ipcCacheTable.device[i].USER, 40);
			ParseJsonString(pSub, "PWD", ipcCacheTable.device[i].PWD, 40);
			ParseJsonNumber(pSub, "CH_ALL", &(ipcCacheTable.device[i].CH_ALL));
			ParseJsonNumber(pSub, "CH_REC", &(ipcCacheTable.device[i].CH_REC));
			ParseJsonNumber(pSub, "CH_FULL", &(ipcCacheTable.device[i].CH_FULL));
			ParseJsonNumber(pSub, "CH_QUAD", &(ipcCacheTable.device[i].CH_QUAD));
			ParseJsonNumber(pSub, "CH_APP", &(ipcCacheTable.device[i].CH_APP));
			for(j=0; j<ipcCacheTable.device[i].CH_ALL; j++)
			{
				pSub2 = cJSON_GetArrayItem(ch_dat, j);
				if(NULL == pSub2 ){ continue ; }
				sprintf(name, "RTSP_URL%d", j);
				ParseJsonString(pSub2, name, ipcCacheTable.device[i].CH_DAT[j].rtsp_url, 200);
				sprintf(name, "W%d", j);
				ParseJsonNumber(pSub2, name, &(ipcCacheTable.device[i].CH_DAT[j].width));
				sprintf(name, "H%d", j);
				ParseJsonNumber(pSub2, name, &(ipcCacheTable.device[i].CH_DAT[j].height));
				sprintf(name, "BPS%d", j);
				ParseJsonNumber(pSub2, name, &(ipcCacheTable.device[i].CH_DAT[j].bitrate));
				sprintf(name, "FPS%d", j);
				ParseJsonNumber(pSub2, name, &(ipcCacheTable.device[i].CH_DAT[j].framerate));
				sprintf(name, "CODE%d", j);
				ParseJsonNumber(pSub2, name, &(ipcCacheTable.device[i].CH_DAT[j].vd_type));
			}			
		}	
	}
    
	status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void SaveIpcToFile(void)
{
	char* jsonString;
	jsonString = CreateIpcSetupObject();
	if(jsonString != NULL)
	{
		SetJsonStringToFile(IPC_SETUP_FILE_NAME, jsonString);
		free(jsonString);
	}

}
void GetIpcFromFile(void)
{
	ParseIpcSetupObject(GetJsonStringFromFile(IPC_SETUP_FILE_NAME));
}

void ClearIpcTable(void)
{

	ipcCacheTable.deviceCnt = 0;
}

#if 1

static IPC_DeviceTable wlanipcCacheTable = {0};


int GetWlanIpcNum(void)
{
	return wlanipcCacheTable.deviceCnt;
}

int CheckWlanIpcExist(char* ip, char* name)
{
	int i;
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(ip!= NULL)
		{
			if(!strcmp(ip, wlanipcCacheTable.device[i].IP))
			{
				return 0;
			}
		}
		else if(name != NULL)
		{
			if(!strcmp(name, wlanipcCacheTable.device[i].NAME))
			{
				return 0;
			}
		}
	}

	return -1;
}

int GetWlanIpcRecord(int index, IPC_ONE_DEVICE* ipcRecord)
{
	if(wlanipcCacheTable.deviceCnt <= index)
	{
		return -1;
	}
	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));
	*ipcRecord = wlanipcCacheTable.device[index];
	
	return 0;
}

int GetWlanIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord)
{
	int i;

	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));

	
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(strcmp(name, wlanipcCacheTable.device[i].NAME))
			{
				continue;
			}
		}
		else if(ip!= NULL)
		{
			if(strcmp(ip, wlanipcCacheTable.device[i].IP))
			{
				continue;
			}
		}

		
		*ipcRecord = wlanipcCacheTable.device[i];
		return 0;
	}

	return -1;
}
int GetWlanIpcRecordByNameOrId(char* name, char* id, IPC_ONE_DEVICE* ipcRecord)
{
	int i;

	memset(ipcRecord, 0, sizeof(IPC_ONE_DEVICE));

	
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(strcmp(name, wlanipcCacheTable.device[i].NAME))
			{
				continue;
			}
		}
		if(id != NULL)
		{
			if(strcmp(id, wlanipcCacheTable.device[i].ID))
			{
				continue;
			}
		}

		*ipcRecord = wlanipcCacheTable.device[i];
		return 0;
	}

	return -1;
}

int DeleteOneWlanIpcRecord(int index)
{
	int i;
	if(wlanipcCacheTable.deviceCnt <= index )
	{
		return -1;
	}

	DeleteOneAppIpcListRecordByIdAndName(wlanipcCacheTable.device[index].ID, wlanipcCacheTable.device[index].NAME);

	for(i = index; i+1 < wlanipcCacheTable.deviceCnt; i++)
	{
		wlanipcCacheTable.device[i] = wlanipcCacheTable.device[i+1];
	}
	
	wlanipcCacheTable.deviceCnt--;
	SaveWlanIpcToFile();	
	return 0;	
	
}

int DeleteWlanIpcByIpOrName(char* ip, char* name)
{
	int i,j;
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(name != NULL)
		{
			if(!strcmp(name, wlanipcCacheTable.device[i].NAME))
			{
				break;
			}
		}
		else if(ip!= NULL)
		{
			if(!strcmp(ip, wlanipcCacheTable.device[i].IP))
			{
				break;
			}
		}
	}
	if(i == wlanipcCacheTable.deviceCnt)
		return -1;

	return DeleteOneWlanIpcRecord(i);
}

int AddOrModifyWlanIpc(IPC_ONE_DEVICE* record)
{
	int i;
	if( wlanipcCacheTable.deviceCnt >= MAX_IPC_DEVICE )
		return -1;
	//AddOneAppIpcListFromIpcCacheTable(*record, 1);
	
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(!strcmp(record->IP, wlanipcCacheTable.device[i].IP))
		{
			DeleteOneAppIpcListRecordByIdAndName(wlanipcCacheTable.device[i].ID,wlanipcCacheTable.device[i].NAME);
			AddOneAppIpcListFromIpcCacheTable(*record, 1);
			wlanipcCacheTable.device[i] = *record;
			SaveWlanIpcToFile();
			return 1;	//已经存在，修改完成
		}
		#if 0
		else if(!strcmp(record->ID, wlanipcCacheTable.device[i].ID))
		{
			wlanipcCacheTable.device[i] = *record;
			SaveWlanIpcToFile();
			return 1;	//已经存在，修改完成
		}
		#endif
	}
	AddOneAppIpcListFromIpcCacheTable(*record, 1);
	printf("AddOneIpcRecord record=%s\n", record->NAME);
	wlanipcCacheTable.device[wlanipcCacheTable.deviceCnt++] = *record;
	SaveWlanIpcToFile();
	return 0;	//添加成功
}


char* CreateWlanIpcSetupObject(void)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	cJSON *ch = NULL;
	cJSON *ch_dat = NULL;
	char *string = NULL;
	int i,j;
	char name[20];

    root = cJSON_CreateObject();
	cJSON_AddItemToObject(root, "ipcs", devices = cJSON_CreateArray());	
    for (i = 0; i < wlanipcCacheTable.deviceCnt; i++)
    {
		device = cJSON_CreateObject();
		
		cJSON_AddStringToObject(device, "ID", wlanipcCacheTable.device[i].ID);
		cJSON_AddStringToObject(device, "NAME", wlanipcCacheTable.device[i].NAME);
		cJSON_AddStringToObject(device, "IP", wlanipcCacheTable.device[i].IP);
		cJSON_AddStringToObject(device, "USER", wlanipcCacheTable.device[i].USER);
		cJSON_AddStringToObject(device, "PWD", wlanipcCacheTable.device[i].PWD);
		cJSON_AddNumberToObject(device, "CH_ALL", wlanipcCacheTable.device[i].CH_ALL);
		cJSON_AddNumberToObject(device, "CH_REC", wlanipcCacheTable.device[i].CH_REC);
		cJSON_AddNumberToObject(device, "CH_FULL", wlanipcCacheTable.device[i].CH_FULL);
		cJSON_AddNumberToObject(device, "CH_QUAD", wlanipcCacheTable.device[i].CH_QUAD);
		cJSON_AddNumberToObject(device, "CH_APP", wlanipcCacheTable.device[i].CH_APP);
		
		cJSON_AddItemToObject(device, "CH_DAT", ch = cJSON_CreateArray()); 
		for (j = 0; j < wlanipcCacheTable.device[i].CH_ALL; j++)
		{
			ch_dat = cJSON_CreateObject();
			sprintf(name, "RTSP_URL%d", j);
			cJSON_AddStringToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].rtsp_url);
			sprintf(name, "W%d", j);
			cJSON_AddNumberToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].width);
			sprintf(name, "H%d", j);
			cJSON_AddNumberToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].height);
			sprintf(name, "BPS%d", j);
			cJSON_AddNumberToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].bitrate);
			sprintf(name, "FPS%d", j);
			cJSON_AddNumberToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].framerate);
			sprintf(name, "CODE%d", j);
			cJSON_AddNumberToObject(ch_dat, name, wlanipcCacheTable.device[i].CH_DAT[j].vd_type);
			cJSON_AddItemToArray(ch, ch_dat);
		}
		
		cJSON_AddItemToArray(devices, device);
	}
	
	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


int ParseWlanIpcSetupObject(const char* json)
{
	int status = 0;
	int i,j;
    cJSON *devices = NULL;
    cJSON *pSub = NULL;
	cJSON *ch_dat = NULL;
    cJSON *pSub2 = NULL;
	char name[20];
	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}

	devices = cJSON_GetObjectItem( root, "ipcs");
	if(cJSON_IsArray(devices))
	{
		wlanipcCacheTable.deviceCnt = cJSON_GetArraySize ( devices );
		printf("ParseWlanIpcSetupObject cnt=%d\n", wlanipcCacheTable.deviceCnt);
		for(i=0; i<wlanipcCacheTable.deviceCnt; i++)
	    {
			pSub = cJSON_GetArrayItem(devices, i);
			if(NULL == pSub ){ continue ; }
			ch_dat = cJSON_GetObjectItem( pSub, "CH_DAT");
			
			
			ParseJsonString(pSub, "ID", wlanipcCacheTable.device[i].ID, 40);
			ParseJsonString(pSub, "NAME", wlanipcCacheTable.device[i].NAME, 40);
			ParseJsonString(pSub, "IP", wlanipcCacheTable.device[i].IP, 100);
			ParseJsonString(pSub, "USER", wlanipcCacheTable.device[i].USER, 40);
			ParseJsonString(pSub, "PWD", wlanipcCacheTable.device[i].PWD, 40);
			ParseJsonNumber(pSub, "CH_ALL", &(wlanipcCacheTable.device[i].CH_ALL));
			ParseJsonNumber(pSub, "CH_REC", &(wlanipcCacheTable.device[i].CH_REC));
			ParseJsonNumber(pSub, "CH_FULL", &(wlanipcCacheTable.device[i].CH_FULL));
			ParseJsonNumber(pSub, "CH_QUAD", &(wlanipcCacheTable.device[i].CH_QUAD));
			ParseJsonNumber(pSub, "CH_APP", &(wlanipcCacheTable.device[i].CH_APP));
			for(j=0; j<wlanipcCacheTable.device[i].CH_ALL; j++)
			{
				pSub2 = cJSON_GetArrayItem(ch_dat, j);
				if(NULL == pSub2 ){ continue ; }
				sprintf(name, "RTSP_URL%d", j);
				ParseJsonString(pSub2, name, wlanipcCacheTable.device[i].CH_DAT[j].rtsp_url, 200);
				sprintf(name, "W%d", j);
				ParseJsonNumber(pSub2, name, &(wlanipcCacheTable.device[i].CH_DAT[j].width));
				sprintf(name, "H%d", j);
				ParseJsonNumber(pSub2, name, &(wlanipcCacheTable.device[i].CH_DAT[j].height));
				sprintf(name, "BPS%d", j);
				ParseJsonNumber(pSub2, name, &(wlanipcCacheTable.device[i].CH_DAT[j].bitrate));
				sprintf(name, "FPS%d", j);
				ParseJsonNumber(pSub2, name, &(wlanipcCacheTable.device[i].CH_DAT[j].framerate));
				sprintf(name, "CODE%d", j);
				ParseJsonNumber(pSub2, name, &(wlanipcCacheTable.device[i].CH_DAT[j].vd_type));
			}			
		}	
	}
    
	status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void SaveWlanIpcToFile(void)
{
	char* jsonString;
	jsonString = CreateWlanIpcSetupObject();
	if(jsonString != NULL)
	{
		SetJsonStringToFile(IPC_WLAN_SETUP_FILE_NAME, jsonString);
		free(jsonString);
	}

}
void GetWlanIpcFromFile(void)
{
	ParseWlanIpcSetupObject(GetJsonStringFromFile(IPC_WLAN_SETUP_FILE_NAME));
}

void ClearWlanIpc(void)
{

	wlanipcCacheTable.deviceCnt = 0;
}

void AddAppIpcFromLanIpcOrWlanIpc(void)
{
	int i;
	
	for(i=0; i < ipcCacheTable.deviceCnt; i++)
	{
		AddOneAppIpcListFromIpcCacheTable(ipcCacheTable.device[i], 0);
	}

	for(i=0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		AddOneAppIpcListFromIpcCacheTable(wlanipcCacheTable.device[i], 0);
	}

	if(ipcCacheTable.deviceCnt || wlanipcCacheTable.deviceCnt)
	{
		SaveAppIpcListToFile();
	}	
}

int getWlanIpcChannel(char* ip, char* name, int* ch_rec, int* ch_full, int* ch_quad, int* ch_app)
{
	int i;
	int ret = -1;
	for(i = 0; i < wlanipcCacheTable.deviceCnt; i++)
	{
		if(ip!= NULL)
		{
			if(!strcmp(ip, wlanipcCacheTable.device[i].IP))
			{
				ret = 0;
				break;
			}
		}
		else if(name != NULL)
		{
			if(!strcmp(name, wlanipcCacheTable.device[i].NAME))
			{
				ret = 0;
				break;
			}
		}
	}
	if(i < wlanipcCacheTable.deviceCnt)
	{
		*ch_rec = wlanipcCacheTable.device[i].CH_REC;
		*ch_full = wlanipcCacheTable.device[i].CH_FULL;
		*ch_quad = wlanipcCacheTable.device[i].CH_QUAD;
		*ch_app = wlanipcCacheTable.device[i].CH_APP;
	}
	
	return ret;
}
#endif
#if 0
char *GetIpcJsonStringFromFile(char *filename,int index)
{
	char *json_string=GetJsonStringFromFile(filename);
	if(json_string==NULL)
		return NULL;
	cJSON *root=cJSON_Parse(json_string);
	free(json_string);
	if(root==NULL)
		return NULL;
	cJSON *devices = cJSON_GetObjectItem( root, "ipcs");
	if(devices==NULL)
	{
		cJSON_Delete(root);
		return NULL;
	}
	cJSON *device=cJSON_GetArrayItem(devices,index);
	if(device==NULL)
	{
		cJSON_Delete(root);
		return NULL;
	}
	json_string=cJSON_Print(device);
	cJSON_Delete(root);
	return json_string;
	
}

int ParseOneIPCDeviceFromJsonString(char *string,IPC_ONE_DEVICE *ipc_dev)
{
	int j;
	cJSON *pSub=cJSON_Parse(string);
			if(NULL == pSub )
				return -1;
	cJSON *ch_dat = cJSON_GetObjectItem( pSub, "CH_DAT");
	
	
	ParseJsonString(pSub, "ID", ipc_dev->ID, 40);
	ParseJsonString(pSub, "NAME", ipc_dev->NAME, 40);
	ParseJsonString(pSub, "IP", ipc_dev->IP, 100);
	ParseJsonString(pSub, "USER", ipc_dev->USER, 40);
	ParseJsonString(pSub, "PWD", ipc_dev->PWD, 40);
	ParseJsonNumber(pSub, "CH_ALL", &(ipc_dev->CH_ALL));
	ParseJsonNumber(pSub, "CH_REC", &(ipc_dev->CH_REC));
	ParseJsonNumber(pSub, "CH_FULL", &(ipc_dev->CH_FULL));
	ParseJsonNumber(pSub, "CH_QUAD", &(ipc_dev->CH_QUAD));
	ParseJsonNumber(pSub, "CH_APP", &(ipc_dev->CH_APP));
	for(j=0; j<ipc_dev->CH_ALL; j++)
	{
		cJSON *pSub2 = cJSON_GetArrayItem(ch_dat, j);
		if(NULL == pSub2 ){ continue ; }
		sprintf(name, "RTSP_URL%d", j);
		ParseJsonString(pSub2, name, ipc_dev->CH_DAT[j].rtsp_url, 200);
		sprintf(name, "W%d", j);
		ParseJsonNumber(pSub2, name, &(ipc_dev->CH_DAT[j].width));
		sprintf(name, "H%d", j);
		ParseJsonNumber(pSub2, name, &(ipc_dev->CH_DAT[j].height));
		sprintf(name, "BPS%d", j);
		ParseJsonNumber(pSub2, name, &(ipc_dev->CH_DAT[j].bitrate));
		sprintf(name, "FPS%d", j);
		ParseJsonNumber(pSub2, name, &(ipc_dev->CH_DAT[j].framerate));
		sprintf(name, "CODE%d", j);
		ParseJsonNumber(pSub2, name, &(ipc_dev->CH_DAT[j].vd_type));
	}
	return 0;
}
#endif
