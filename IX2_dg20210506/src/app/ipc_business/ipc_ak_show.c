
#include "ipc_ak_show.h"

H264_SHOW_INSTANCE_t	h264_show_ins_array[4];

int api_h264_show_initial(void)
{
	int i,layer;
	#ifdef PID_IXSE
	layer = AK_VO_LAYER_VIDEO_5;
	#else
	layer = AK_VO_LAYER_VIDEO_5;
	#endif
	for( i = 0; i < 4; i++ )
	{
		h264_show_initial( &h264_show_ins_array[i],layer++);
	}
}

int api_h264_show_start( int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	return h264_show_start( &h264_show_ins_array[index], width, height, fps, baudrate , dispx, dispy, dispw, disph, videotype );	
}

int api_h264_show_update( int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	return h264_show_update( &h264_show_ins_array[index], width, height, fps, baudrate , dispx, dispy, dispw, disph, videotype  );	
}

int api_h264_show_just_running(int index)
{
	H264_SHOW_STATE_t cur;
	pthread_mutex_lock( &h264_show_ins_array[index].h264_show_state_lock );
	cur = get_h264_show_state(&h264_show_ins_array[index]);
	pthread_mutex_unlock( &h264_show_ins_array[index].h264_show_state_lock );
	if( cur == H264_SHOW_RUN )
		return 1;
	else
		return 0;
}

int api_h264_show_stop( int index )
{
	return h264_show_stop( &h264_show_ins_array[index] );
}

int api_h264_show_dump_buf( int index, char* pdatbuf, int datlen )
{
	return h264_show_dump_buf( &h264_show_ins_array[index], pdatbuf, datlen,0);
}



