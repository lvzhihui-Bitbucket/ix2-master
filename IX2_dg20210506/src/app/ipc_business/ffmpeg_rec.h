
#ifndef _FFMPEG_REC_H_
#define _FFMPEG_REC_H_

#include "onvif_tiny_task.h"
#include "task_VideoMenu.h"

#include "libavutil/common.h"
#include "libavformat/avformat.h"
#include <libavutil/timestamp.h>

typedef  void (*ffmpegRecCallBack)(int index);

typedef struct
{
	int					state;		// 0:idle, 1:running
	char				rtsp_url[240];
	char				out_file[100];
	// attribute
	int					width;
	int					height;
	int					videotype;		// 0:h264, 1:h265
	int					fps;
	int 				videostream;	
	int					videoCH;		// 通道号
	int					recOnly;		// 仅录像标志
	int 				TotalSec;
	int 				CurSec;
	int 				fileSplitCnt;
	int 				fileSplit;

	AVFormatContext 	*ifmt_ctx;
	AVFormatContext 	*ofmt_ctx;
	one_tiny_task_t 	task_process;	
	ffmpegRecCallBack 	CallBack;	
} ffmpeg_rec_dat_t;


#endif



