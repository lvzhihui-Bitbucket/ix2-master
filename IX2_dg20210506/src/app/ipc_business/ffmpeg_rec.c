

#include "ffmpeg_rec.h"


int task_ffmpeg_rec_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
	AVOutputFormat *ofmt = NULL;
	int ret = 0;
	int i;
	/* initialize libavcodec, and register all codecs and formats */
	if( !GetRegisterState() )
	{
		SetRegisterState(1);
		printf("\n=================begin to av_register_all...========\n");
		av_register_all();
		avcodec_register_all();
		avformat_network_init();
	}
	
	printf("\n=================begin to avformat_open_input: [%s]...========\n", pusr_dat->rtsp_url );
	pusr_dat->ifmt_ctx = avformat_alloc_context();
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"102400", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以udp方式打开，如果以tcp方式打开将udp替换为tcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	ret = avformat_open_input(&pusr_dat->ifmt_ctx, pusr_dat->rtsp_url, NULL, &options);
	if (ret < 0) 
	{
		printf("cannot open %s ret=%d\n", pusr_dat->rtsp_url, ret);
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
	}

	printf("\n=================begin to av_find_stream_info...========\n");
	pusr_dat->ifmt_ctx->probesize = 10 *1024;
	pusr_dat->ifmt_ctx->max_analyze_duration = 1 * AV_TIME_BASE;	
	ret = avformat_find_stream_info(pusr_dat->ifmt_ctx,NULL ); //&options);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
	if (ret < 0) 
	{
		printf( "avformat_find_stream_info error!\n");
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
	}

	for( i = 0; i < pusr_dat->ifmt_ctx->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			pusr_dat->width = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->width;	   
			pusr_dat->height = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->height;				
			pusr_dat->videotype = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			printf("videostream=%d Videotype=%d width=%d, height=%d\n", pusr_dat->videostream, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
			if( !pusr_dat->width || !pusr_dat->height )
			{
				pusr_dat->width = 1920;
				pusr_dat->height = 1088;
				pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->width = 1920;
				pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->height = 1080;
			}
			pusr_dat->height 	= pusr_dat->height==1080? 1088 : pusr_dat->height;
			break;
		}
	}

	// 创建上下文
	ret = avformat_alloc_output_context2(&pusr_dat->ofmt_ctx, NULL, NULL, pusr_dat->out_file);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
    }
	pusr_dat->state			= 1;
	ofmt = pusr_dat->ofmt_ctx->oformat;
	// 添加流
	AVStream *in_stream = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream];
	#if 0
	AVCodec *codec = avcodec_find_decoder(in_stream->codecpar->codec_id);
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, codec);
	if (!out_stream) 
	{
		printf("Failed allocating output stream\n");
		goto end;
	 
	}
	AVCodecContext *pCodecCtx = avcodec_alloc_context3(codec);
	ret = avcodec_parameters_to_context(pCodecCtx, in_stream->codecpar);
	if (ret < 0) 
	{
		printf("Failed to copy context input to output stream codec context\n");
		goto end;
	}
	#if 0
	ret = avcodec_open2(pCodecCtx, codec, NULL);
    if (ret < 0) 
	{
        printf("Failed to open decoder for stream 0\n");
		goto end;
    }
	#endif
	if (ofmt->flags & AVFMT_GLOBALHEADER) 
	{
		pCodecCtx->flags |= CODEC_FLAG_GLOBAL_HEADER;
	}
	pCodecCtx->codec_tag = 0;
	ret = avcodec_parameters_from_context(out_stream->codecpar, pCodecCtx);
	if (ret < 0) 
	{
		printf("Failed to copy context input to output stream codec context\n");
		goto end;
	}
	#endif
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, NULL);
	AVCodecParameters *in_codecpar = in_stream->codecpar;
	avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
	out_stream->codecpar->codec_tag = 0;
	
	// 打开AVIOContext缓冲区
	if (!(ofmt->flags & AVFMT_NOFILE)) 
	{
		ret = avio_open(&pusr_dat->ofmt_ctx->pb, pusr_dat->out_file, AVIO_FLAG_WRITE);
		if (ret < 0) 
		{
			printf("Could not open output file %s\n", pusr_dat->out_file);
			return -1;
		}
    }
	// 写入头部信息
    ret = avformat_write_header(pusr_dat->ofmt_ctx, NULL);
	if (ret < 0) 
	{
        printf("Error occurred when opening output file\n");
		return -1;
	}
	
	#if 0
end:
	if (ret < 0) 
	{
		avformat_close_input(&pusr_dat->ifmt_ctx);
		//avcodec_free_context(&pCodecCtx);
		/* close output */
		if (pusr_dat->ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&pusr_dat->ofmt_ctx->pb);
		avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_free_context(pusr_dat->ofmt_ctx);
		return -1;
	}
	#endif
	return 0;
}
#if 0
int calfps(int deltaTime)
{
    static int fps = 0;
    static int timeLeft = 1000000; // 取固定时间间隔为1秒
    static int frameCount = 0;

    ++frameCount;
    timeLeft -= deltaTime;
    if (timeLeft < 0)
    {
        fps = frameCount;
        frameCount = 0;
        timeLeft = 1000000;
    }
	printf("av_read_frame fps=%d\n", fps);
    return fps;
}
#endif
// 线程循环处理
int task_ffmpeg_rec_process( void* arg )
{
    int ret;
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
    AVPacket pkt;
	AVStream *in_stream, *out_stream;
	struct timeval start, end;
	int time_use;
	int m_frame_index = 0;
	int IDR = 0;
	int64_t pts_last;
	pusr_dat->state = 2;	// run flag;

	#if 0
	#include <sched.h>
	struct sched_param sh_param;
	sh_param.sched_priority = sched_get_priority_min(SCHED_FIFO);
	sched_setscheduler(0,SCHED_FIFO,&sh_param);
	#endif
    while( one_tiny_task_is_running(ptiny_task) )
	{		
        memset(&pkt, 0, sizeof(pkt));
        gettimeofday( &start, NULL );
        ret= av_read_frame(pusr_dat->ifmt_ctx, &pkt);
		gettimeofday( &end, NULL );
		time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
		if(ret<0 || time_use > 2000000)
		{
			printf("task_ffmpeg_rec_process av_read_frame ret=%d time_use=%d\n", ret, time_use);
			av_packet_unref(&pkt);
			(pusr_dat->CallBack)(pusr_dat->videoCH);
		}
        else
		{
			if( pkt.stream_index == pusr_dat->videostream )
			{
				#if 0
				if( IDR == 0 && pkt.data[0] == 0 && pkt.data[1] == 0 && pkt.data[2] == 0 && pkt.data[3] == 1 )
				{
					if( ( (pkt.data[4]) & (0x1f) ) == 7)
					{
						IDR = 1;
						printf("API_Recording_mux H264 IDR OK\n");
					}
					else if( ( (pkt.data[4]) & (0x7e) ) == 0x40)
					{
						IDR = 1;
						printf("API_Recording_mux H265 IDR OK\n");
					}
				}
				#endif
				in_stream  = pusr_dat->ifmt_ctx->streams[pkt.stream_index];
				out_stream = pusr_dat->ofmt_ctx->streams[pkt.stream_index];
				if(!pusr_dat->recOnly)
					dvr_quad_full_buf(pkt.data, pkt.size);
				//if(IDR)
				{
					/* copy packet */
					//pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
					//pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
					//pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
					//av_packet_rescale_ts(&pkt, in_stream->time_base, out_stream->time_base);
					//文件分包后pts处理
					if(pusr_dat->fileSplit)
					{
						pusr_dat->fileSplit = 0;
						pts_last = 0;
					}
					pkt.pts = 90000/pusr_dat->fps + pts_last;
					pkt.dts = pkt.pts;
					pts_last = pkt.pts;
					ret = av_interleaved_write_frame(pusr_dat->ofmt_ctx, &pkt);
					if (ret < 0) 
					{
						printf("write_frame ERR=%d\n", ret);
						(pusr_dat->CallBack)(pusr_dat->videoCH);
						break;

					}
					if(m_frame_index < pusr_dat->fps - 1)
					{
						m_frame_index++;
					}
					else
					{
						printf("m_frame_index=%d rec_time=%d\n",m_frame_index, pusr_dat->CurSec);
						m_frame_index = 0;
						pusr_dat->CurSec++;
					}
					if(pusr_dat->CurSec >= pusr_dat->TotalSec)
					{
						printf("task_ffmpeg_rec_process CurSec=%d\n", pusr_dat->CurSec);
						(pusr_dat->CallBack)(pusr_dat->videoCH);
						break;
					}
					if((pusr_dat->CurSec%600 == 0) && (m_frame_index==0)) 
					{
						pusr_dat->fileSplit = 1;
						//pusr_dat->fileSplitCnt++;
						ret = RecFileSplit(pusr_dat->videoCH);
						if (ret < 0) 
						{
							printf("RecFileSplit ERR!!!\n");
							(pusr_dat->CallBack)(pusr_dat->videoCH);
							break;
						}
					}
				}
			}
			av_packet_unref(&pkt);
        } 
		usleep(1*1000);
    }
	av_write_trailer(pusr_dat->ofmt_ctx);
	printf("-----end task_ffmpeg_rec_process !!!\n");
    return 0;
}

// 退出该线程的处理: 内存和资源的回收释放
int task_ffmpeg_rec_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
	printf( "--task_ffmpeg_rec_exit state=%d--\n", pusr_dat->state); 
    AVOutputFormat *ofmt = NULL;
	if(pusr_dat->state)
	{
		pusr_dat->state = 0;
		avformat_close_input(&pusr_dat->ifmt_ctx);
		ofmt = pusr_dat->ofmt_ctx->oformat;
		if (pusr_dat->ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&pusr_dat->ofmt_ctx->pb);
		avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_free_context(pusr_dat->ofmt_ctx);
		printf("-----task_ffmpeg_rec_exit !!!\n");
	}
	return 0;
}

#if 0
void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt, const char *tag)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;

    printf("%s: pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
           tag,
           av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
           av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
           av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
           pkt->stream_index);
}
#endif


ffmpeg_rec_dat_t		ffmpegRec[4];
int dvr_quad_to_full(int ffmpe_num)
{
	int ret;
	ffmpegRec[ffmpe_num].recOnly = 0;
	ret = api_h264_show_start(4, ffmpegRec[ffmpe_num].width, ffmpegRec[ffmpe_num].height, 15, 512, 0, 0, bkgd_w, bkgd_h, ffmpegRec[ffmpe_num].videotype);
	printf("dvr_swich_to_full ret = %d\n", ret);
	return ret;
}
int dvr_full_to_quad(int ffmpe_num)
{
	int ret;
	ffmpegRec[ffmpe_num].recOnly = 1;
	printf("close dvr_full_show !!!\n");
	ret = api_h264_show_stop(4);
	printf("close dvr_full_show ret = %d\n", ret);
	return ret;
}
	
int dvr_quad_full_buf( char* pdatbuf, int datlen )
{
	return api_h264_show_dump_buf( 4, pdatbuf, datlen );
}

int RecFileSplit(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pusr_dat = &ffmpegRec[ffmpe_num];
	av_write_trailer(pusr_dat->ofmt_ctx);
	avio_closep(&pusr_dat->ofmt_ctx->pb);
	avformat_free_context(pusr_dat->ofmt_ctx);

	char recfile[50];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"20%02d%02d%02d_%02d%02d%02d-CH%d.mp4",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec,ffmpe_num+1);
	snprintf(pusr_dat->out_file,100,"/mnt/sdcard/video/%s",recfile);
	
	printf( "--RecFileSplit CurSec=%d-\n", pusr_dat->CurSec); 
	int ret;
	ret = avformat_alloc_output_context2(&pusr_dat->ofmt_ctx, NULL, NULL, pusr_dat->out_file);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		return -1;
    }
	AVStream *in_stream = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream];
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, NULL);
	avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
	ret = avio_open(&pusr_dat->ofmt_ctx->pb, pusr_dat->out_file, AVIO_FLAG_WRITE);
	if (ret < 0) 
	{
		printf("Could not open output file %s\n", pusr_dat->out_file);
		return -1;
	}
	ret = avformat_write_header(pusr_dat->ofmt_ctx, NULL);
	if (ret < 0) 
	{
        printf("Error when avformat_write_header\n");
		return -1;
	}
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DVR_SPACE_CHECK);
	return 0;
}

int RecFileFlush(int ffmpe_num)
{
	printf("RecFileFlush ch=%d\n", ffmpe_num);
	ffmpeg_rec_dat_t* pusr_dat = &ffmpegRec[ffmpe_num];
	if (pusr_dat->ofmt_ctx->pb)
       avio_flush(pusr_dat->ofmt_ctx->pb);
	//av_freep(&pusr_dat->ofmt_ctx->priv_data);
	char cmd[100];
	strcpy(cmd,"echo 3 > /proc/sys/vm/drop_caches");
	system(cmd);
	sync();
}
int GetRecFileSize(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	//avio_seek(pins->ofmt_ctx->pb, 0, SEEK_END);
	int64_t filesize = avio_tell(pins->ofmt_ctx->pb);
	avio_seek(pins->ofmt_ctx->pb, filesize, SEEK_SET);
	filesize /= (1024*1024);
	printf("GetRecFileSize %d\n", filesize);
	return filesize;
}

int GetRecCurSec(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	return pins->CurSec;
}

int api_start_ffmpeg_rec(int ffmpe_num, unsigned char* rtsp_url, unsigned char*file, int min, int fps, ffmpegRecCallBack fun )
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];

	if( pins->state )
		return -1;
	pins->videoCH = ffmpe_num;
	pins->recOnly = 1;
	pins->fps = fps;
	pins->TotalSec = min*60;
	pins->CurSec = 0;
	pins->fileSplit = 0;
	pins->CallBack = fun;
	strcpy( pins->rtsp_url, rtsp_url );
	strcpy( pins->out_file, file );
	printf( "--api_start_ffmpeg_rec fps=%d---time=%d-\n", pins->fps, pins->TotalSec); 
	if( one_tiny_task_start( &pins->task_process, task_ffmpeg_rec_init, task_ffmpeg_rec_process, task_ffmpeg_rec_exit, (void*)pins ) == 0 )
	{
		int wait_to = 0;
		while(1)
		{
			if( pins->state == 2 )
				return 0;
			else
			{
				usleep(100*1000);
				if( ++wait_to >= 50 )	// 3s
					return -2;
 			}
 		}		
 	}
	else
		return -1;
}

int api_stop_ffmpeg_rec(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	one_tiny_task_stop( &pins->task_process );
	
	return 0;	
}



