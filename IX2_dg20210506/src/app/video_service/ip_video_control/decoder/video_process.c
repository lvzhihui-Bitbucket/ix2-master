
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
//IX2_VD #include <linux/videodev.h>
#include <errno.h>
#include <linux/fb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include <dirent.h>
#include <pthread.h>
#include <net/if.h>

#include "video_displaye_controller.h"
#include "video_process.h" 

#include "udp_fragment_opt.h"
#include "utility.h"

#include "obj_ak_decoder.h"
#include "obj_ak_show.h"
#include "task_Event.h"

#define DEC_SIZE	(150*1024)


#if 0
struct ak_one_decoder *ptr_vdec_array[4]={0};


///////////////////////////////////////////////////////////////////////////////////////////////
// h.264解码设备开关
///////////////////////////////////////////////////////////////////////////////////////////////
int open_h264_dec_device(void)
{
	//ptr_vdec_array[0] = ak_new_one_decode( AK_CODEC_H264,1280,720,AK_TILE_YUV,0,0,1280,720,AK_VO_LAYER_VIDEO_1);
	//QUART_POS_T pos;
	//GetVDPos(&pos);
	//SetLayerPos(AK_VO_LAYER_VIDEO_1,pos.vd_posx,pos.vd_posy,pos.vd_width,pos.vd_height);
	ptr_vdec_array[0] = ak_new_one_decode2( 0,640,480,AK_TILE_YUV,AK_VO_LAYER_VIDEO_1);//AK_CODEC_H264
#if 0	
	if( get_pane_type()	== 0 )
		ptr_vdec_array[0] = ak_new_one_decode( AK_CODEC_H264,640,480,AK_YUV420SP,0,0,1013,800,AK_VO_LAYER_VIDEO_1);
	else
		ptr_vdec_array[0] = ak_new_one_decode( AK_CODEC_H264,640,480,AK_TILE_YUV,0,0,800,1014,AK_VO_LAYER_VIDEO_1);
#endif
	return ptr_vdec_array[0];	
}

int close_h264_dec_device(void)
{
	return ak_del_one_decode(ptr_vdec_array[0]);
}

VIDEO_DISPLAY_STATE_t  	VideoDisplayState 		= VIDEO_DISPLAY_IDLE;
pthread_mutex_t 		VideoDisplayState_lock 	= PTHREAD_MUTEX_INITIALIZER;

VIDEO_DISPLAY_STATE_t video_process_state_get( void )
{
	VIDEO_DISPLAY_STATE_t VideoDisplayState_read;	
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState_read = VideoDisplayState;
	pthread_mutex_unlock( &VideoDisplayState_lock );
	return VideoDisplayState_read;
}

int video_process_state_set_idle( void )
{
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState = VIDEO_DISPLAY_IDLE;		
	pthread_mutex_unlock( &VideoDisplayState_lock );		
	return 0;
}

int video_process_state_set_start( void )
{
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState = VIDEO_DISPLAY_START;		
	pthread_mutex_unlock( &VideoDisplayState_lock );		
	return 0;
}

int video_process_state_set_wait( void )
{
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState = VIDEO_DISPLAY_WAIT;		
	pthread_mutex_unlock( &VideoDisplayState_lock );		
	return 0;
}

int video_process_state_set_run( void )
{
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState = VIDEO_DISPLAY_RUN;		
	pthread_mutex_unlock( &VideoDisplayState_lock );		
	return 0;
}

int video_process_state_set_stop( void )
{
	pthread_mutex_lock( &VideoDisplayState_lock );		
	VideoDisplayState = VIDEO_DISPLAY_STOP;		
	pthread_mutex_unlock( &VideoDisplayState_lock );		
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// /h.264接收及解码显示任务处理
///////////////////////////////////////////////////////////////////////////////////////////////
static H264_RECEIVE_t 	Receive;
pthread_t 				task_for_h264_rcv;
pthread_attr_t 			task_attr_for_h264_rcv;

pthread_t 				task_for_h264_dec;
pthread_attr_t 			task_attr_H264_dec;

extern unsigned long GetTickCount(void);

void thread_for_h264_rcv_clear_up( void *arg )
{	
	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;	
	if( Receive.type ) leave_multicast_group2(Receive.net_device_name, Receive.Sd, Receive.addr );	
	close( Receive.Sd );
}

void  thread_for_h264_rcv( void )
{
	int size = 0;

	if( ( Receive.Sd = socket( PF_INET, SOCK_DGRAM, 0 )  ) == -1 )
	{
		// 关闭解码线程
		pthread_cancel( task_for_h264_dec ) ;
		pthread_join( task_for_h264_dec, NULL );	
		// 进入空闲状态
		video_process_state_set_idle(); 	
		printf("creating socket failed!");
		return;
	}

	// 设置超时
	struct timeval timeout;
	timeout.tv_sec = 	1;
	timeout.tv_usec = 	0;
	if( setsockopt(Receive.Sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1 ) 
	{
		printf("set socket timeout failed!\n");
	}	
	
	bzero( &Receive.sock_addr, sizeof(Receive.sock_addr) );
	Receive.sock_addr.sin_family		= AF_INET;
	Receive.sock_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	Receive.sock_addr.sin_port			= htons( Receive.Port);
	
	if( bind(Receive.Sd, (struct sockaddr*)&Receive.sock_addr, sizeof(Receive.sock_addr)) != 0 )
	{	
		close( Receive.Sd );		
		// 关闭解码线程
		pthread_cancel( task_for_h264_dec ) ;
		pthread_join( task_for_h264_dec, NULL );			
		// 进入空闲状态
		video_process_state_set_idle();
		perror( "bind" );
		return;
	}
	
	if( Receive.type )	join_multicast_group2(Receive.net_device_name, Receive.Sd, Receive.addr );

	u_int32_t addr_len = sizeof( Receive.sock_addr );

	pthread_cleanup_push( thread_for_h264_rcv_clear_up, NULL  );

	udp_fragment_t	recv_fragment;

	// 进入运行状态
	video_process_state_set_run();

	printf("======video_process_state_set_run!!!!!=======\n");
	#include <sched.h>
	struct sched_param sh_param;
	sh_param.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0,SCHED_FIFO,&sh_param);
	
	while( 1 )
	{	
		size = recvfrom( Receive.Sd, (unsigned char*)&recv_fragment, sizeof(udp_fragment_t), 0, (struct sockaddr*)&Receive.addr, &addr_len );
		
		if(video_process_state_get() != VIDEO_DISPLAY_RUN)
			continue;

		if( size == 0 )
		{
			printf("======recvfrom broken!=======\n");			
		}
		else if( size < 0 )
		{
			printf("======recvfrom error!=======\n");			
		}
		else
		{
			//printf("======recvfrom size=%d=======\n",size);
			//PrintCurrentTime(1);
			CVideoPackProcessor_ProcPack((unsigned char*)&recv_fragment,size);
		}
		//usleep(5);
	}	

	pthread_cleanup_pop( 1 );
	
}

void thread_for_h264_dec_clean_up( void *arg )
{
	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
	close_h264_dec_device();
	CVideoPackProcessor_ClearNode();	
}

#define DEC_SIZE	(100*1024)

void  thread_for_h264_dec( void )
{
	int result = 0;
	int size = 0;
	unsigned char msgMulticast_Data[DEC_SIZE];
	int IDR = 0;	

	if( open_h264_dec_device() < 0 )
	{
		// 进入空闲状态
		video_process_state_set_idle();
		return;
	}
	pthread_cleanup_push( thread_for_h264_dec_clean_up, NULL  );

	CVideoPackProcessor_InitNode();

	// 进入等待状态
	video_process_state_set_wait();

	printf("======video_process_state_set_wait!!!!!=======\n");
	
	while( 1 )
	{	
		ReleaseSemaphore();

		if(video_process_state_get() != VIDEO_DISPLAY_RUN)
			continue;
				
		// 解码是否停止
		// 解码并显示
		AVPackNode *Node;		

		CVideoPackProcessor_LockNode();
		
		Node = CVideoPackProcessor_PickPack();
		if( Node != NULL )
		{
			memcpy(msgMulticast_Data,Node->Content.Buffer,Node->Content.Len);
			
			//printf("r:frame_sn=%d,len=%d,t=%d,%d\n",Node->Content.FrameNo,Node->Content.Len,Node->Content.Timestamp,(unsigned int)GetTickCount());
			
			size = Node->Content.Len;		
			CVideoPackProcessor_ReturnNode(Node);
			// 若无解码和显示则数据包接收正常，开启解码和显示，则解码的时间超过了40ms(25帧的情况下)，导致缓冲数据满而丢包
			// decode 处理
			if( IDR == 0 && msgMulticast_Data[0] == 0 && msgMulticast_Data[1] == 0 && msgMulticast_Data[2] == 0 && msgMulticast_Data[3] == 1 )
			{
				if( ( (msgMulticast_Data[4]) & (0x1f) ) == 7)   // 等待关键帧 SPS + PPS + IDR 
				{
					IDR = 1;				
					printf("-------IDR OK!!!!!!!!!!!!!!!-------\n");
				}
			}
			if( IDR )
			{	
				//printf("ak_push_frame len[%d] handle_id[%d]\n",size, ptr_vdec_array[0]->dec_handle);
				ak_push_frame( ptr_vdec_array[0],(unsigned char*)msgMulticast_Data, size);
				
				//rtp_sender_send_with_ts( msgMulticast_Data, size);
				
				API_Recording( msgMulticast_Data, size );
			}
		}
		
		CVideoPackProcessor_UnLockNode();
	}	
	pthread_cleanup_pop( 0 );
}

///////////////////////////////////////////////////////////////////////////////////////////////
// h.264解码显示接口
///////////////////////////////////////////////////////////////////////////////////////////////
pthread_mutex_t interface_reenter_lock 	= PTHREAD_MUTEX_INITIALIZER;
static int		interface_reenter_flag	= 0;	

#define	INTERFACE_REENTER_FLAG_RESET	pthread_mutex_lock( &interface_reenter_lock ); \
										interface_reenter_flag = 0;	\
										pthread_mutex_unlock( &interface_reenter_lock );

int start_h264_dec_process(int  	     server_ip, int type, short port, int mcg_lowbyte )
{	
	pthread_mutex_lock( &interface_reenter_lock );
	if( interface_reenter_flag ) 
	{
		pthread_mutex_unlock( &interface_reenter_lock );
		return 0;
	}	
	interface_reenter_flag = 1;
	pthread_mutex_unlock( &interface_reenter_lock );
	
	if( video_process_state_get() == VIDEO_DISPLAY_IDLE )
	{
		// 进入启动状态
		video_process_state_set_start();
	
		// 登记h.264接收类型、端口号、地址
		Receive.addr	= mcg_lowbyte;
		Receive.Port	= port;
		Receive.type	= type;
		Receive.net_device_name = GetNetDeviceNameByTargetIp(server_ip);
		
		// 开启解码线程
		pthread_attr_init( &task_attr_H264_dec );
		if( pthread_create(&task_for_h264_dec,&task_attr_H264_dec,(void*)thread_for_h264_dec, NULL ) != 0 )
		{
			// 进入空闲状态
			video_process_state_set_idle();				
			printf( "Create thread_for_h264_dec pthread error! \n" );
			INTERFACE_REENTER_FLAG_RESET
			return -1;
		}
		// 等待进入解码状态
		int wait_to = 0;
		while(1)
		{
			if( video_process_state_get() == VIDEO_DISPLAY_WAIT )
				break;
			usleep(100*1000);
			// wait for 5s, perhaps state ERROR!!!
			if( ++wait_to > 50 )
			{
				printf("start_h264_dec_process is WAITING TIMEOUT!!!!!!!!!\n"); 
				// 进入空闲状态
				video_process_state_set_idle();
				INTERFACE_REENTER_FLAG_RESET
				return -1;
			}
		}
		
		// 开启udp接收线程
		pthread_attr_init( &task_attr_for_h264_rcv );
		if( pthread_create(&task_for_h264_rcv,&task_attr_for_h264_rcv,(void*)thread_for_h264_rcv, NULL ) != 0 )
		{
			// 关闭解码线程
			pthread_cancel( task_for_h264_dec ) ;
			pthread_join( task_for_h264_dec, NULL );		
			// 进入空闲状态
			video_process_state_set_idle();		
			printf( "Create task_h264_udp_rcv pthread error! \n" );
			INTERFACE_REENTER_FLAG_RESET			
			return -1;
		}	
		else
		{
			printf("start_h264_dec_process is OK !!!!!!!!!\n");
			INTERFACE_REENTER_FLAG_RESET
			return 0;
		}
	}
	else
	{
		printf("start_h264_dec_process is busy!!!!!!!!!\n");
		INTERFACE_REENTER_FLAG_RESET
		return -1;
	}
}

int close_h264_dec_process( void )
{
	pthread_mutex_lock( &interface_reenter_lock );
	if( interface_reenter_flag ) 
	{
		pthread_mutex_unlock( &interface_reenter_lock );
		return 0;
	}	
	interface_reenter_flag = 1;
	pthread_mutex_unlock( &interface_reenter_lock );

	if( video_process_state_get() == VIDEO_DISPLAY_IDLE )
	{
		printf("close_h264_dec_process is idle!!!!!!!!!\n");
		INTERFACE_REENTER_FLAG_RESET
		return -1;
	}
	else
	{
		if(  video_process_state_get() != VIDEO_DISPLAY_RUN )
		{
			printf("close_h264_dec_process is NOT run!!!!!!!!!\n");
			int wait_run_to = 0;
			while(1)
			{
				if( video_process_state_get() == VIDEO_DISPLAY_RUN )
					break;
				usleep(100*1000);
				// wait for 2s, perhaps state ERROR!!!
				if( ++wait_run_to > 20 )
				{
					printf("close_h264_dec_process is WAITING TIMEOUT!!!!!!!!!\n");	
					// 进入空闲状态
					video_process_state_set_idle();
					INTERFACE_REENTER_FLAG_RESET
					return -1;
				}
			}
		}		
		video_process_state_set_stop();

		API_RecordEnd();
		
		printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
		pthread_cancel( task_for_h264_rcv ) ;
		pthread_join( task_for_h264_rcv, NULL );		
			
		printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
			
		pthread_cancel( task_for_h264_dec ) ;
		pthread_join( task_for_h264_dec, NULL );
						
		printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;

		// 进入空闲状态
		video_process_state_set_idle();
		INTERFACE_REENTER_FLAG_RESET
		return 0;
	}
}
#endif

H264_SHOW_INSTANCE_t	monitor_ins_array[4];

int api_monitor_show_initial(void)
{
	int i,layer;
	layer = AK_VO_LAYER_VIDEO_1;
	for( i = 0; i < 4; i++ )
	{
		h264_show_initial( &monitor_ins_array[i],layer++);
	}
}

int api_monitor_show_start( int index )
{
	h264_show_start( &monitor_ins_array[index], 1280, 720, 0, 0 , 0, 0, 0, 0, 0 );	
}

int api_monitor_show_stop( int index )
{
	return h264_show_stop( &monitor_ins_array[index] );
}

int api_monitor_show_buf( int index, char* pdatbuf, int datlen,int tick)
{
	return h264_show_dump_buf( &monitor_ins_array[index], pdatbuf, datlen,tick);
}


int task_monitor4ds_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;	

	long long cur_time;
	cur_time = time_since_last_call(-1);	
	
	CVideoPackProcessor_InitNode(pusr_dat->ds_ch);
	api_monitor_show_start(pusr_dat->ds_ch);
	if( ( pusr_dat->Receive.Sd = socket( PF_INET, SOCK_DGRAM, 0 )  ) == -1 )
	{
		api_monitor_show_stop(pusr_dat->ds_ch);
		printf("creating socket failed!");
		return -1;
	}
	// 设置超时
	struct timeval timeout;
	timeout.tv_sec =	1;
	timeout.tv_usec =	0;
	if( setsockopt(pusr_dat->Receive.Sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1 ) 
	{
		printf("set socket timeout failed!\n");
	}	
	
	bzero( &pusr_dat->Receive.sock_addr, sizeof(pusr_dat->Receive.sock_addr) );
	pusr_dat->Receive.sock_addr.sin_family		= AF_INET;
	pusr_dat->Receive.sock_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	pusr_dat->Receive.sock_addr.sin_port		= htons( pusr_dat->Receive.Port);
	
	if( bind(pusr_dat->Receive.Sd, (struct sockaddr*)&pusr_dat->Receive.sock_addr, sizeof(pusr_dat->Receive.sock_addr)) != 0 )
	{	
		close( pusr_dat->Receive.Sd );		
		perror( "bind" );
		return -1;
	}
	int defRcvBufSize = -1;
    	socklen_t optlen = sizeof(defRcvBufSize);
	if(getsockopt(pusr_dat->Receive.Sd, SOL_SOCKET, SO_RCVBUF, &defRcvBufSize, &optlen) >= 0)
	{
		printf("1111111111:%d\n",defRcvBufSize);
		defRcvBufSize=defRcvBufSize*4;
		optlen = sizeof(defRcvBufSize);
		setsockopt(pusr_dat->Receive.Sd, SOL_SOCKET, SO_RCVBUF, &defRcvBufSize, optlen);
	}
	printf("33333333333:%d\n",defRcvBufSize);
	if( pusr_dat->Receive.type )	
		join_multicast_group2(pusr_dat->Receive.net_device_name, pusr_dat->Receive.Sd, pusr_dat->Receive.addr );
	printf("task_monitor4ds_init ins[%d] time[%d]\n",pusr_dat->ds_ch, time_since_last_call(cur_time));	
	return 0;
}

int task_monitor4ds_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;	
	int size = 0;
	u_int32_t addr_len = sizeof( pusr_dat->Receive.sock_addr );
	udp_fragment_t	recv_fragment;
	AVPackNode *Node;		
	unsigned char msgMulticast_Data[DEC_SIZE];
	int IDR = 0;
	int errorCnt=0;
	int not_idr=0;
	
	fd_set fds;
	int ret;
	struct timeval tv={1,0};
	int save_frameid=0;
	int save_frameid2=0;
	pusr_dat->state			= 1;
	printf("======task_monitor4ds_process run!!!!!=======\n");
	//int try_cnt=0
	//sleep(3);
	//usleep(100*1000);
	PrintCurrentTime(15000+__LINE__);
	while( one_tiny_task_is_running(ptiny_task) )
	{	
		FD_ZERO(&fds);
		FD_SET(pusr_dat->Receive.Sd,&fds);
		
		tv.tv_sec = 0;
		tv.tv_usec = 200*1000;	// 100ms
		ret = select( pusr_dat->Receive.Sd + 1, &fds, NULL, NULL, &tv );
		//usleep(10);
		//printf("00000000000task_monitor4ds_process\n");
		if(ret>0)
		{
			//printf("111111111111111task_monitor4ds_process\n");
			while(size = recvfrom( pusr_dat->Receive.Sd, (unsigned char*)&recv_fragment, sizeof(udp_fragment_t), O_NONBLOCK, (struct sockaddr*)&pusr_dat->Receive.addr, &addr_len ))
				{
			//printf("222222222222222task_monitor4ds_process\n");
			if( size == 0 )
			{
				break;
				printf("======recvfrom broken!=======\n");			
			}
			else if( size < 0 )
			{
				break;
				printf("======recvfrom error!=======\n");			
				errorCnt++;
				if(errorCnt >= 10)
					stop_monitor_dec_process(pusr_dat->ds_ch);
			}
			else
			{
				errorCnt = 0;
				if(CVideoPackProcessor_ProcPack(pusr_dat->ds_ch, (unsigned char*)&recv_fragment,size) == 0)
				{
					Node = CVideoPackProcessor_PickPack(pusr_dat->ds_ch);
					if( Node != NULL )
					{
						size = Node->Content.Len;		
						memcpy(msgMulticast_Data,Node->Content.Buffer,size);
						
						//printf("r:frame_sn=%d,len=%d,t=%d\n",Node->Content.FrameNo,size,Node->Content.Timestamp);
						//printf("333333333333333task_monitor4ds_process %d\n",Node->Content.FrameNo);
						//CVideoPackProcessor_ReturnNode(pusr_dat->ds_ch,Node);
						//if( IDR == 0 && msgMulticast_Data[0] == 0 && msgMulticast_Data[1] == 0 && msgMulticast_Data[2] == 0 && msgMulticast_Data[3] == 1 )
						//printf(":::%d:%d\n",Node->Content.FrameNo,save_frameid);
						if(save_frameid==0)
						{
							save_frameid=Node->Content.FrameNo;
							save_frameid2=save_frameid;
						}
						else
						{
							if((save_frameid+1)!=(Node->Content.FrameNo))
							{
								printf("-------%d:%d\n",save_frameid,Node->Content.FrameNo);
								video_client_recvno_err_add(pusr_dat->ds_ch,Node->Content.FrameNo-save_frameid2);
							}
							else
								video_client_recvno_frame_add(pusr_dat->ds_ch);
							save_frameid=Node->Content.FrameNo;
						}
						
						if(IDR == 0 && msgMulticast_Data[0] == 0 && msgMulticast_Data[1] == 0 && msgMulticast_Data[2] == 0 && msgMulticast_Data[3] == 1 )
						{
							if( ( (msgMulticast_Data[4]) & (0x1f) ) == 7)   // 等待关键帧 SPS + PPS + IDR 
							{
								IDR = 1;		
								PrintCurrentTime(15000+__LINE__);
								printf("-------IDR OK!!!!!!!!!!!!!!!-------\n");
								//save_frameid=Node->Content.FrameNo-1;
								PrintCurrentTime(98765);
							}
						}
						//if( IDR&&save_frameid==(Node->Content.FrameNo-1))
						//if( IDR&&size>2000)	
						//video_client_recv_add(pusr_dat->ds_ch,size);
						if( IDR)
						{	
							//PrintCurrentTime(0);
							//save_frameid=Node->Content.FrameNo;
							
							if(get_dsmonitor_client_rtp(pusr_dat->ds_ch))
							{
								//video_client_recv_add(pusr_dat->ds_ch,size);
								rtp_sender_send_with_ts_wan(msgMulticast_Data, size,Node->Content.Timestamp,0);
								rtp_sender_send_with_ts_uni(msgMulticast_Data, size,Node->Content.Timestamp,0);
							}
							//else
							#ifdef PID_IXSE
							if(pusr_dat->ds_ch==0||!get_dsmonitor_client_rtp(pusr_dat->ds_ch))
							#endif
							{
								
								
								if(api_monitor_show_buf(pusr_dat->ds_ch, msgMulticast_Data, size,Node->Content.Timestamp)==0)
								{
									video_client_recv_add(pusr_dat->ds_ch,size);
								}
								else
								{
									break;
								}
								API_Recording_mux( msgMulticast_Data, size, pusr_dat->ds_ch, 0 );
								
							}
							//ak_push_frame( ptr_vdec_array[0],(unsigned char*)msgMulticast_Data, size);
							//PrintCurrentTime(1);
							//IDR=0;
							
						}
						else
						{
							video_client_recv_add(pusr_dat->ds_ch,size);
							printf("-------IDR not OK!!!!!!!!!!!!!!!-------\n");
							if(pusr_dat->ds_ch==0&&not_idr++%5==0)
								API_Remote_Event_NameAndMsg(pusr_dat->ser_ip, Event_VideoSerError, "REQ_IDR");
						}
						CVideoPackProcessor_ReturnNode(pusr_dat->ds_ch,Node);
					}
				}
				if(!one_tiny_task_is_running(ptiny_task))
					break;
			}
				}
			//printf("555555555555task_monitor4ds_process\n");
		}
	}	
	printf("======task_monitor4ds_process exit!!!!!====1===\n");
	//usleep(10);
	//printf("======task_monitor4ds_process exit!!!!!====2===\n");
	//usleep(10);
	//printf("======task_monitor4ds_process exit!!!!!====3===\n");
	return 0;
}

int task_monitor4ds_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;
	pusr_dat->ser_ip=0;
	printf("======task_monitor4ds_exit ch=%d !!!!!=======\n", pusr_dat->ds_ch);
	if( pusr_dat->Receive.type ) leave_multicast_group2(pusr_dat->Receive.net_device_name, pusr_dat->Receive.Sd, pusr_dat->Receive.addr );	
	close( pusr_dat->Receive.Sd );
	CVideoPackProcessor_ClearNode(pusr_dat->ds_ch);	
}

monitor4ds_dat_t		monitor4ds[4]={0};
int start_monitor_dec_process(int index, int     	     server_ip, short port, int mcg_lowbyte )
{	
	monitor4ds_dat_t* pins = &monitor4ds[index];
	printf("======start_monitor_dec_process index=%d state=%d!!!!!=======\n", index, pins->state);
	if( pins->state )
		return -1;
	pins->ds_ch = index;
	// 登记h.264接收类型、端口号、地址
	pins->Receive.addr	= mcg_lowbyte;
	pins->Receive.Port	= port;
	pins->Receive.type	= 1;
	pins->Receive.net_device_name = GetNetDeviceNameByTargetIp(server_ip);
	pins->ser_ip=server_ip;
	if( one_tiny_task_start( &pins->task_process, task_monitor4ds_init, task_monitor4ds_process, task_monitor4ds_exit, (void*)pins ) == 0 )
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

int stop_monitor_dec_process(int index)
{
	long long cur_time;
	cur_time = time_since_last_call(-1);	
	monitor4ds_dat_t* pins = &monitor4ds[index];
	printf("======stop_monitor_dec_process index=%d state=%d!!!!!=======\n", index, pins->state);
	one_tiny_task_stop( &pins->task_process );
	//close_h264_dec_device();
	//api_monitor_show_stop(pins->ds_ch);
	//usleep(500*1000);
	api_monitor_show_stop(pins->ds_ch);
	pins->state = 0;
	printf("==stop_monitor_dec_process ins[%d] time[%d] \n",index, time_since_last_call(cur_time));	
	return 0;
}
static int video_client_rate[4]={0};
static int video_client_recv_size[4]={0};
static int video_client_recv_size_save[4]={0};
static int video_client_recv_size_save_buff[4][3]={0};
static int video_client_rate_keep[4]={0};
static int video_client_ser_ip[4]={0};
static int video_client_recvno_err[4]={0};
static int video_client_recvno_err_save[4]={-1};
static int video_client_recvno_size[4]={0};
//static int video_client_rate_write_index=0;
//static int video_client_rate_read_index=0;
static int video_client_rate_index=0;
void video_client_recv_add(int ch,int add_size)
{
	if(ch>=0&&ch<4)
		video_client_recv_size[ch]+=add_size;
}
void video_client_recvno_err_add(int ch,int size)
{
	if(ch>=0&&ch<4)
	{
		video_client_recvno_err[ch]++;
		video_client_recvno_size[ch]=size;
	}
	
}
void video_client_recvno_frame_add(int ch)
{
	if(ch>=0&&ch<4)
	{
		//video_client_recvno_err[ch]++;
		video_client_recvno_size[ch]++;
	}
	
}
void video_client_frame_rate_update(void)
{
	int i,j;
	static int time=0;
	//if(video_client_rate_time++>=6)
	//	video_client_rate_time=0;
	int rate;
	char key_buff[30];
	char ip_str[30];
	float r_err;
	for(i=0;i<4;i++)
	{
		if(video_client_recv_size_save[i]!=video_client_recv_size[i])
		{
			video_client_recv_size_save[i]=video_client_recv_size[i];
			video_client_rate_keep[i]=0;
		}
		else if(video_client_recv_size_save[i]!=0&&++video_client_rate_keep[i]>3)
		{
			video_client_recv_size_save[i]=0;
			video_client_recv_size[i]=0;
			video_client_rate_keep[i]=0;
			video_client_recvno_err[i]=0;
			video_client_recvno_size[i]=0;
			for(j=0;j<3;j++)
			{
				video_client_recv_size_save_buff[i][j]=0;
			}
		}
		if(time>=3)
		{
			rate= (video_client_recv_size_save[i]-video_client_recv_size_save_buff[i][video_client_rate_index])/3;
			if(rate!=video_client_rate[i])
			{
				video_client_rate[i]=rate;
				sprintf(key_buff,"VC_CH%d_Bandwind",i);
				API_PublicInfo_Write_Int(key_buff,(rate>>7));
				
			}
			if(monitor4ds[i].ser_ip!=video_client_ser_ip[i])
			{
				video_client_ser_ip[i]=monitor4ds[i].ser_ip;
				my_inet_ntoa(monitor4ds[i].ser_ip, ip_str);
				sprintf(key_buff,"VC_CH%d_SerIP",i);
				API_PublicInfo_Write_String(key_buff,ip_str);
				sprintf(key_buff,"VC_CH%d_Bandwind",i);
				if(API_PublicInfo_Read(key_buff)==NULL)
					API_PublicInfo_Write_Int(key_buff,(video_client_rate[i]>>7));
			}
			if(video_client_recvno_err_save[i]!=video_client_recvno_err[i])
			{
				video_client_recvno_err_save[i]=video_client_recvno_err[i];
				sprintf(key_buff,"VC_CH%d_RecvNOErr",i);
				API_PublicInfo_Write_Int(key_buff,video_client_recvno_err[i]);
				
				if(video_client_recvno_size[i]>0)
					r_err=(float)video_client_recvno_err[i]/(float)video_client_recvno_size[i];
				else
					r_err=0;
				sprintf(key_buff,"VC_CH%d_RecvErrRate",i);
				snprintf(ip_str,30,"%f",r_err);
				API_PublicInfo_Write_String(key_buff,ip_str);
			}
			else if(video_client_recvno_size[i]>0&&time%3==0)
			{
				r_err=(float)video_client_recvno_err[i]/(float)video_client_recvno_size[i];
				sprintf(key_buff,"VC_CH%d_RecvErrRate",i);
				snprintf(ip_str,30,"%f",r_err);
				API_PublicInfo_Write_String(key_buff,ip_str);
			}
		}
		video_client_recv_size_save_buff[i][video_client_rate_index]=video_client_recv_size_save[i];
			
	}
	
	
	if(++video_client_rate_index>=3)
		video_client_rate_index=0;
	time++;
	
}
int get_video_client_rate(int ch)
{
	if(ch>=0&&ch<4)
	{
		return (video_client_rate[ch]>>7);
	}
	return 0;
}