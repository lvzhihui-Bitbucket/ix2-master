
#ifndef _VIDEO_DISPLAYE_CONTROLLER_H_
#define _VIDEO_DISPLAYE_CONTROLLER_H_

#include "video_process.h"
#include "video_playback.h"
#include "video_record.h"

void API_FromMulticastJoin(int ins, int server_ip, int16_t port, int32_t mcg_lowbyte );
void API_FromMulticastLeave(  int ins, int server_ip,      int16_t port, int32_t mcg_lowbyte );	


#endif

