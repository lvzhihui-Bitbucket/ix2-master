
#ifndef _VIDEO_RECORD_H_
#define _VIDEO_RECORD_H_

#include "video_utility.h"

typedef enum
{
	 RECORD_IDLE,
	 RECORD_START,
	 RECORD_RUN,
}
RECORD_STATE_t;

typedef  void (*CallBackFun)(int ins);

typedef struct
{
	FILE*				File;
	pthread_mutex_t		lock;
	float			 	Fps;
	char				Fcc[4];
	unsigned short   	Width;
	unsigned short   	Height;
	long long 			Movi;
	long long 			MoviEnd;
	long long  			Riff;
	int 				TotalFrame;
	int 				AudioFrame;
	int 				VideoFrame;
	
	AVIIndexEntry	*pVideoIndexMap;
	AVIIndexEntry	*pAudioIndexMap;
	
	int				IDR;
	char			fileName[50];
	int				AudioFlag;
	int				TotalSec;
	int				CurrSec;	
	RECORD_STATE_t  State;
	CallBackFun 	CallBack;	
	int 			TotalSize;
} avi_t;

typedef struct
{
	// thread
	pthread_mutex_t 	rec_mux_state_lock;
	avi_t				rec_mux_ins;
	uint8			lock_init_flag;

}REC_INSTANCE_t;


int API_WriteAudio( char* p_data, int size );
void API_Recording( char* p_data, int size );

int API_RecordStart( char* file_name, int sec, CallBackFun fun );
int API_RecordEnd(void);

int api_rec_mux_initial(void);
int API_RecordStart_mux( int index, int vdtype, int fps, char* file_name, int sec, int fileSize, CallBackFun fun );
void API_Recording_mux( char* p_data, int size, int num, int vdtype );
int API_RecordEnd_mux(int num);

#endif



