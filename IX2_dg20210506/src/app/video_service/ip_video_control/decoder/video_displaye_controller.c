
//#include "multicast_data_process.h"
#include "video_displaye_controller.h"

void API_FromMulticastJoin(int ins, int server_ip,        int16_t port, int32_t mcg_lowbyte )
{
	//start_h264_dec_process(server_ip, 1, port, mcg_lowbyte  );
	start_monitor_dec_process(ins, server_ip, port, mcg_lowbyte);
}

void API_FromMulticastLeave(int ins, int server_ip,        int16_t port, int32_t mcg_lowbyte )
{
	//close_h264_dec_process();
	stop_monitor_dec_process(ins);
}

