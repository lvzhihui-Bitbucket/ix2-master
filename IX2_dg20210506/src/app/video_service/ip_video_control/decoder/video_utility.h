
#ifndef _VIDEO_UTILITY_H_
#define _VIDEO_UTILITY_H_

#include <fcntl.h>
#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>
#include <signal.h>
#include <dirent.h>
#include <pthread.h>

#include <asm/types.h>
#include <arpa/inet.h>

#include <sys/vfs.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/utsname.h> 
#include <sys/statfs.h>

#include <netdb.h>  
#include <net/if.h>
#include <netinet/in.h>
#include <net/route.h>
#include <net/if_arp.h>

#include <linux/fs.h>
#include <linux/sockios.h>   
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <netinet/in.h>
#include <netinet/ip.h>   
#include <netinet/ether.h>
#include <netinet/ip_icmp.h>  

#include "obj_AviFileParser.h"
#include "onvif_tiny_task.h"
#include "define_file.h"

#define FRAMES_PER_SEC	15

int leave_multicast_group2(char* net_device_name, int socket_fd, int mcg_addr );
int join_multicast_group2(char* net_device_name, int socket_fd, int mcg_addr );

int getSdcardSpace( void );
int GetPhotoNum(const char *path);
int Judge_SdCardLink(void);
int API_GetSDCardSize(int *freesize,int *totalsize);

int Delete_VideoFile(char *filename);


#endif



