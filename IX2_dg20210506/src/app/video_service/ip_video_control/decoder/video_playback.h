
#ifndef _VIDEO_PLAYBACK_H_
#define _VIDEO_PLAYBACK_H_

#include "video_utility.h"

typedef  void (*PlaybackCallBackFun)(int sec);

typedef struct
{
	// file name
	char		filename[200];
	
	// file data
	AVIFileParset aviParset;

	// audio data
	int			au_channel;
	int			au_rate;

	// video data
	int 		index;
	int 		width;
	int 		height;
	int 		fps;
	int 		baudrate;
	int 		dispx;
	int 		dispy;
	int 		dispw;
	int 		disph;
	int			videotype;
	
	int			vd_play_time;
	int			vd_play_frame;
	int			vd_play_rate;
	// play state
	RECORD_PLAYBACK_STATE_t State;	
	pthread_mutex_t 		aviPlaybackStateLock;
	PlaybackCallBackFun 	CallBack;		
	one_tiny_task_t 		tiny_task;
}AV_PLAYBACK_DAT_T;


int API_PlaybackStart( char* file_name, int* sec, PlaybackCallBackFun fun );
int API_PlaybackPause( void );
int API_PlaybackContinue( void );
int API_PlaybackClose( void );
RECORD_PLAYBACK_STATE_t API_GetPlaybackState(void);

#endif


