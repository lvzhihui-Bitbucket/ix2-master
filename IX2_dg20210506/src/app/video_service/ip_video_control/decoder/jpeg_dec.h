
#ifndef _JPEG_DEC_H_
#define _JPEG_DEC_H_

#include "video_utility.h"

typedef enum
{	
	JPEG_STATE_IDLE,
	JPEG_INIT,
	CAPTURE_RUN,
	CAPTURE_STOP,
	PLAYBACK_RUN,
	//PLAYBACK_STOP,
} JPEG_RUN_STATE_t;


typedef  void (*CallBackFun)(void);

typedef struct
{
	// play state
	JPEG_RUN_STATE_t state;	
	char		FileName[50];
	CallBackFun 		CallBack;	
	pthread_mutex_t 		jpegState_lock;
}JPEG_PLAYBACK_DAT_T;

int jpegState_init( void );
int jpegState_get( void );
int jpegState_set( int state );
int API_JpegPlaybackStart( char* file_name);
int API_JpegPlaybackClose( void );

#endif


