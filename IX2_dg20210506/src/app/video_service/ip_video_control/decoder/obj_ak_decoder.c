
#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>


#include "obj_ak_decoder.h"
#include "elog_forcall.h"

struct ak_layer_pos  layer_pos[8];              //store the pos
int layerShow[8]={0};
pthread_mutex_t decoder_com_lock = PTHREAD_MUTEX_INITIALIZER;

#if 0
struct ak_tde_cmd tde_cmd_param;
struct ak_tde_layer tde_layer_src = { 640 , 480 , 0 , 0 , 640 , 480 , 0 , GP_FORMAT_TILED32X4 };
struct ak_tde_layer tde_layer_tgt = { 640 , 480 , 0 , 0 , 640 , 480 , 0 , GP_FORMAT_YUV420SP } ;


char SrcDat[1382400+100];
char TarDat[1382400+100];

int Convert_Format(char* psrc, int len )
{
	memset( &tde_cmd_param , 0 , sizeof( struct ak_tde_cmd ) );	
	tde_cmd_param.opt = GP_OPT_SCALE;

    ak_mem_dma_vaddr2paddr( SrcDat , ( unsigned long * )&tde_layer_src.phyaddr );
	memcpy(SrcDat,psrc,len);	
    ak_mem_dma_vaddr2paddr( TarDat , ( unsigned long * )&tde_layer_tgt.phyaddr );	
	
    tde_cmd_param.tde_layer_src = tde_layer_src;
    tde_cmd_param.tde_layer_dst = tde_layer_tgt;	
	
    if( ak_tde_opt( &tde_cmd_param ) == 0 )
	{
		printf("===========================ok===============\n");
	}
	else
	{
		printf("===========================er===============\n");		
	}
}

#endif



static int set_decoder_status(struct ak_one_decoder *pdecoder, int status )
{
	//ak_thread_mutex_lock( &pdecoder->dec_mutex );
	pdecoder->dec_status = status;
	printf( "set_decoder_status = %d\n", status) ;		
	//ak_thread_mutex_unlock( &pdecoder->dec_mutex );
	return 0;
}

static int get_decoder_status(struct ak_one_decoder *pdecoder )
{
	int status;
	//ak_thread_mutex_lock( &pdecoder->dec_mutex );
	status = pdecoder->dec_status;
	//ak_thread_mutex_unlock( &pdecoder->dec_mutex );
	printf( "get_decoder_status = %d\n", status) ;		
	return status;
}

static int show_one_frame(struct ak_vo_obj *obj, struct ak_vdec_frame *frame, int top, int left, int w, int h, int layer)
{
    /* set the pos in the screen */
    int pos_top 	= top;
    int pos_left 	= left;
    int width		= w;
    int height		= h;

    /* obj to add to layer */
    memset(obj, 0x00, sizeof(struct ak_vo_obj));
    if (frame->data_type == AK_TILE_YUV)
    {
        /* if the data type is tileyuv */
        obj->format 		 	= GP_FORMAT_TILED32X4;
        obj->vo_tileyuv.data 	= frame->tileyuv_data;
    }
    else
    {
        /* if the data type is yuv420sp */
        obj->format 			= GP_FORMAT_YUV420SP;
        obj->cmd 				= GP_OPT_SCALE|GP_OPT_COLORKEY;     /* scale to screen 	*/
		//obj->cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY; //|GP_OPT_TRANSPARENT;
		//gui_obj.alpha 			= 15;
		obj->colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		obj->colorkey.color_min		= 0; //0xfffff8;			/* min value */
		obj->colorkey.color_max		= 0; //0xfffff8;			/* max value */
		
        obj->vo_layer.width 	= frame->yuv_data.pitch_width;  /* the real width 	*/
        obj->vo_layer.height 	= frame->yuv_data.pitch_height; /* the real height 	*/

        /* pos and range from the src */
        obj->vo_layer.clip_pos.top 		= 0;
        obj->vo_layer.clip_pos.left 	= 0;
        obj->vo_layer.clip_pos.width 	= frame->width;
        obj->vo_layer.clip_pos.height 	= frame->height;
        ak_mem_dma_vaddr2paddr(frame->yuv_data.data, &(obj->vo_layer.dma_addr));
    }

    /* pos and range for the dst layer to contain the src */
    obj->dst_layer.top 		= 0;
    obj->dst_layer.left 	= 0;
    obj->dst_layer.width 	= width;
    obj->dst_layer.height 	= height;
    ak_vo_add_obj(obj, layer);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<layer));
    return 0;
}

static int frame_cnt = 0;
static void *vdec_recv_frame(void *arg)
{
	struct ak_one_decoder *pdecoder = (struct ak_one_decoder*)arg;
    int ret = -1;
    int status = 0;

	set_decoder_status( pdecoder, DECODER_STATUS_RUN );
	ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_RUN!!!\n",pdecoder->dec_handle);			

	rtp_process_set_high_prio();
	PrintCurrentTime(25000+__LINE__);
    /* a loop for getting the frame and display */
    do{
        /* get frame */
	//ak_thread_mutex_lock( &pdecoder->frame_mutex );	
	//struct ak_vdec_frame 	frame;	
	#if 1
//	pthread_mutex_lock( &pdecoder->frame_lock);
        ret = ak_vdec_get_frame(pdecoder->dec_handle, &pdecoder->frame);
	//ret = ak_vdec_get_frame(pdecoder->dec_handle, &frame);	
	
        if(ret == 0)
        {	
        	if(pdecoder->frame_cnt==0)
			PrintCurrentTime(56789);
        	pdecoder->frame_cnt++;
			//frame_cnt--;
			//printf("vdec_recv_frame cnt=%d\n", frame_cnt);
        	//ak_print_normal(MODULE_ID_VDEC,"ak_vdec_get_frame[dst_layer--%d]!!!\n",pdecoder->dst_layer);
        	if( (pdecoder->frame_cnt&1) == 1 )
			//if( pdecoder->frame_cnt <=3 )
            	//show_one_frame(&pdecoder->obj,&pdecoder->frame,pdecoder->dst_top,pdecoder->dst_left,pdecoder->dst_width,pdecoder->dst_height,pdecoder->dst_layer);
				show_one_frame2(&pdecoder->obj,&pdecoder->frame,pdecoder->dst_layer);
			//API_JpegCaptureing(pdecoder->frame.yuv_data.data, 1382400);//frame.tileyuv_data
			ak_vdec_release_frame(pdecoder->dec_handle, &pdecoder->frame);
			//ak_print_normal(MODULE_ID_VDEC,"ak_vdec_get_frame[0x%08x]!!!\n",ret); 		
			//ak_sleep_ms(1);
			// jpeg_capture_process
		//	pthread_mutex_unlock( &pdecoder->frame_lock);
        }
        else
        {
            /* get frame failed , sleep 10ms before next cycle*/
		//printf("9999999999999999999\n");
		//pthread_mutex_unlock( &pdecoder->frame_lock);
            //ak_sleep_ms(1);
		usleep(1000);
			
        }
		#endif
		//usleep(500*1000);
	//ak_thread_mutex_unlock( &pdecoder->frame_mutex );	
        /* check the status finished */
        ak_vdec_get_decode_finish(pdecoder->dec_handle, &status);
        /* true means finished */
        if (status)
        {
			printf("decoder[%d] status is DECODER_STATUS_STOPing !!!\n",pdecoder->dec_handle);
			//ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_STOPing !!!\n",pdecoder->dec_handle);			
			set_decoder_status( pdecoder, DECODER_STATUS_STOP );
			break;
        }
    }while(1);
}

void decode_stream(int handle_id, unsigned char *data, int read_len)
{
    int send_len = 0;
    int dec_len = 0;
    int ret = 0;

	//frame_cnt++;
	//printf("decode_stream cnt=%d\n", frame_cnt);
	
    /* cycle to send the data to decode */
    while (read_len > 0)
    {
        /* send stream to decode */
		//printf("11111111111decode_stream len[%d] handle_id[%d]\n",read_len, handle_id);
       	ret = ak_vdec_send_stream(handle_id, &data[send_len], read_len, 1, &dec_len);// ���׿������⣿
		//printf("2222222222222222222decode_stream ret[%d]\n",ret);
        if (ret !=  0)
        {
           ak_print_error_ex(MODULE_ID_VDEC, "write video data failed!\n");
            break;
        }
        /* record the length to send to decoder */
        read_len -= dec_len;
        send_len += dec_len;
    }
}

struct ak_one_decoder* ak_new_one_decode( int in_type, int src_w, int src_h, int out_type, int dst_l, int dst_t, int dst_w, int dst_h,int dst_layer )
{
	struct ak_one_decoder *pOneDecoder;
	
	ak_print_normal(MODULE_ID_VDEC,"ak_new_one_decode start!!!\n");

	pOneDecoder = (struct ak_one_decoder *)malloc(sizeof(struct ak_one_decoder));

	pOneDecoder->in_type	= in_type;
	pOneDecoder->out_type	= out_type;
	pOneDecoder->src_width	= src_w;
	pOneDecoder->src_height	= src_h;
	pOneDecoder->dst_left	= dst_l;
	pOneDecoder->dst_top	= dst_t;
	pOneDecoder->dst_width	= dst_w;
	pOneDecoder->dst_height	= dst_h;
	pOneDecoder->dst_layer	= dst_layer;

	//ak_thread_mutex_init(&pOneDecoder->dec_mutex,0);	// must initial 0
	
#if 1
pthread_mutex_lock(&decoder_com_lock);
    /* create the video layer */
    struct ak_vo_layer_in video_layer;
    video_layer.create_layer.height = dst_h; 		//layer size
    video_layer.create_layer.width  = dst_w; 		//layer size
    video_layer.create_layer.left  	= dst_l; 		//layer pos 
    video_layer.create_layer.top   	= dst_t; 		//layer pos
    video_layer.layer_opt          	= 0;			//opt
    if( ak_vo_create_video_layer(&video_layer, pOneDecoder->dst_layer) )
    {
        ak_print_error_ex(MODULE_ID_VDEC, "ak_vo_create_video_layer failed!\n");
		if( pOneDecoder )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}
pthread_mutex_unlock(&decoder_com_lock);
		return NULL;
    }

	struct ak_vdec_param param = {0};
	param.vdec_type 		= pOneDecoder->in_type;
	param.sc_height 		= pOneDecoder->src_height;
	param.sc_width 			= pOneDecoder->src_width;
	param.output_type 		= pOneDecoder->out_type;	
	param.frame_buf_num =3;
	/* open the vdec */
	if( ak_vdec_open(&param, &pOneDecoder->dec_handle) != 0 )
	{
		ak_print_error_ex(MODULE_ID_VDEC, "ak_vdec_open failed!\n");
		ak_vo_destroy_layer(pOneDecoder->dst_layer);
		if( pOneDecoder )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}
pthread_mutex_unlock(&decoder_com_lock);	
		return NULL;
	}
#endif

	//ak_mutexattr_t attr;
	//ak_thread_mutex_init(&pOneDecoder->frame_mutex,&attr);
	//pthread_mutex_init( &pOneDecoder->frame_lock, 0);
	set_decoder_status( pOneDecoder, DECODER_STATUS_START ); 
	ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_START!!!\n",pOneDecoder->dec_handle);			

	/* get frame thread */
	ak_thread_create(&pOneDecoder->recv_frame_id, vdec_recv_frame, (void*)pOneDecoder, ANYKA_THREAD_MIN_STACK_SIZE, -1);

	ak_print_normal(MODULE_ID_VDEC,"ak_new_one_decode[%d] ok!!!\n",pOneDecoder->dec_handle);
	pthread_mutex_unlock(&decoder_com_lock);
	return pOneDecoder;
}

int ak_del_one_decode(struct ak_one_decoder *pOneDecoder)
{
	frame_cnt = 0;
	if( pOneDecoder != NULL )
	{
		if( get_decoder_status(pOneDecoder) != DECODER_STATUS_RUN )
		{
			ak_print_normal(MODULE_ID_VDEC,"get_decoder_status != DECODER_STATUS_RUN!!!\n");			
			//ak_print_normal(MODULE_ID_VDEC,"ak_decoder[%d] is starting, please waiti...\n",pOneDecoder->dec_handle);
			return 1;
		}
		pthread_mutex_lock(&decoder_com_lock);
		int dec_id = pOneDecoder->dec_handle;
		ak_print_normal(MODULE_ID_VDEC,"ak_del_one_decode[%d] start!!!\n",dec_id);
		//pthread_mutex_lock( &pOneDecoder->frame_lock);
		ak_vdec_end_stream(pOneDecoder->dec_handle);	
	//	pthread_mutex_unlock( &pOneDecoder->frame_lock);
	int cnt = 0;
		while(cnt++<50)
		{
			if( get_decoder_status(pOneDecoder) == DECODER_STATUS_STOP )
			{
				ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_STOPed !!!\n",pOneDecoder->dec_handle);			
				break;
			}
			usleep(100*1000);
		}
			if(cnt>=30)
		{
			pthread_detach(pOneDecoder->recv_frame_id);
			pthread_cancel(pOneDecoder->recv_frame_id);
		}
		else
			pthread_join( pOneDecoder->recv_frame_id,NULL);
		ak_thread_join( pOneDecoder->recv_frame_id);
		set_decoder_status( pOneDecoder, DECODER_STATUS_IDLE );
		ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_IDLE ,frame cnt=%d!!!\n",pOneDecoder->dec_handle,pOneDecoder->frame_cnt);			
		//hong long time?
		ak_vdec_close(pOneDecoder->dec_handle);
		ak_print_normal(MODULE_ID_VDEC,"ak_vdec_close ok!!!\n");			
		ak_vo_destroy_layer(pOneDecoder->dst_layer);
		layerShow[pOneDecoder->dst_layer - AK_VO_LAYER_VIDEO_1] = 0;
		ak_print_normal(MODULE_ID_VDEC,"ak_vo_destroy_layer ok!!!\n");			
		//ak_thread_mutex_destroy(&pOneDecoder->frame_mutex);
		pthread_mutex_unlock(&decoder_com_lock);

		if( pOneDecoder != NULL )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}

		ak_print_normal(MODULE_ID_VDEC,"ak_del_one_decode[%d] over!!!\n",dec_id);

		return 0;
	}
	else
		return -1;
}

//#define STORE_IN_FILE
#ifdef STORE_IN_FILE
#include <fcntl.h>
#define STORE_FILE_NAME		"h265rawdata.dat"
static int store_file_packegs = 0;
static int storefp;
int store_in_file(unsigned char *pdat, int len)
{
	if( store_file_packegs == -1 )
		return -1;
	
	if( store_file_packegs == 0 )
	{
		storefp = open(STORE_FILE_NAME,O_CREAT|O_WRONLY|O_TRUNC);		
		printf("store_in_file[%s] starting!!!!\n",STORE_FILE_NAME);
	}
	if( storefp )
	{
		write(storefp,pdat,len);
	}
	store_file_packegs++;
	if( store_file_packegs >= 300 )
	{		
		if( storefp != NULL )
		{
			close(storefp);
			printf("store_in_file[%s] completed!!!!\n",STORE_FILE_NAME);
			store_file_packegs = -1;
		}
	}
	return 0;
}
#endif
int ak_push_frame(struct ak_one_decoder *pOneDecoder, unsigned char *pdat, int len )
{
	if( pOneDecoder == NULL )
		return -1;
	
	//ak_thread_mutex_lock( &pOneDecoder->frame_mutex );
	//pthread_mutex_lock( &pOneDecoder->frame_lock);
#ifndef 	STORE_IN_FILE
	//printf("333333333333333ak_push_frame,%d\n",pOneDecoder->frame_cnt);
	//if(pOneDecoder->frame_cnt<=5)
	{
		//pOneDecoder->frame_cnt++;
		decode_stream( pOneDecoder->dec_handle, pdat, len );	
	}

#else
	if( store_in_file(pdat,len) == -1 )
	{
		decode_stream( pOneDecoder->dec_handle, pdat, len );		
	}
#endif
	//ak_thread_mutex_unlock( &pOneDecoder->frame_mutex );
//	pthread_mutex_unlock( &pOneDecoder->frame_lock);
	return 0;
}


int Set_ds_show_pos(int ins_num, int x, int y, int width, int height)
{
	return SetLayerPos(ins_num + AK_VO_LAYER_VIDEO_1, x, y, width, height);
}
int Clear_ds_show_layer(int ins_num)
{
	return close_one_layer(ins_num + AK_VO_LAYER_VIDEO_1);
}

int close_one_layer(int layer)
{
	int id = layer-AK_VO_LAYER_VIDEO_1;
	int ret;
	layerShow[id] = 0;
 	ret = ak_vo_destroy_layer(layer);
	printf("close_one_layer layer=%d id=%d, ret=%d \n",layer, id,ret);
	return 0;
}

int SetLayerPos(int dst_layer, int dst_x, int dst_y, int dst_w, int dst_h)
{
	int num = dst_layer - AK_VO_LAYER_VIDEO_1;
	printf("---SetLayerPos layer num=%d posx=%d posy=%d width=%d height=%d---\n", num,dst_x,dst_y,dst_w,dst_h);
    /* create the video layer */
    struct ak_vo_layer_in video_layer;
    video_layer.create_layer.left  	= dst_x; 		//layer pos 
    video_layer.create_layer.top   	= dst_y; 		//layer pos
    video_layer.create_layer.width  = dst_w; 		//layer size
    video_layer.create_layer.height = dst_h; 		//layer size
    video_layer.layer_opt          	= 0;			//opt
    if( ak_vo_create_video_layer(&video_layer, dst_layer) )
    {
    	ak_vo_destroy_layer(dst_layer);
		if( ak_vo_create_video_layer(&video_layer, dst_layer) )
		{
			ak_print_error_ex(MODULE_ID_VDEC, "ak_vo_create_video_layer failed!\n");
			return -1;
		}
    }

    layer_pos[num].left = dst_x;
	layer_pos[num].top = dst_y;
    layer_pos[num].width = dst_w;
    layer_pos[num].height = dst_h;
	layerShow[num] = dst_layer;
	return 0;
}

int get_ds_show_layer(int index)
{
	return layerShow[index];
}
void set_ds_show_layer(int index, int layer)
{
	layerShow[index] = layer;
}
void set_default_show_layer(int index)
{
	layerShow[index] = index + AK_VO_LAYER_VIDEO_1;
}
void set_ds_layer_hidden(int index)
{
	layerShow[index] = 0;
}
void get_layer_pos(int index,int *width,int *height)
{
	*width=layer_pos[index].width;
	*height=layer_pos[index].height;
}
struct ak_one_decoder* ak_new_one_decode2( int in_type, int src_w, int src_h, int out_type, int dst_layer )
{
	struct ak_one_decoder *pOneDecoder;
	
	ak_print_normal(MODULE_ID_VDEC,"ak_new_one_decode start!!!\n");

	pOneDecoder = (struct ak_one_decoder *)malloc(sizeof(struct ak_one_decoder));

	pOneDecoder->in_type	= in_type;
	pOneDecoder->out_type	= out_type;
	pOneDecoder->src_width	= src_w;
	pOneDecoder->src_height	= src_h;
	pOneDecoder->dst_layer	= dst_layer;
	pOneDecoder->frame_cnt =0;
	//ak_thread_mutex_init(&pOneDecoder->dec_mutex,0);	// must initial 0
	

	struct ak_vdec_param param = {0};
	param.vdec_type 		= pOneDecoder->in_type;
	param.sc_height 		= pOneDecoder->src_height;
	param.sc_width 			= pOneDecoder->src_width;
	param.output_type 		= pOneDecoder->out_type;	
	param.frame_buf_num =3;
	/* open the vdec */
	printf("---ak_vdec_open vd_height[%d] vd_width[%d] vd_type[%d]---\n",param.vdec_type, param.sc_height, param.sc_width);
	if( ak_vdec_open(&param, &pOneDecoder->dec_handle) != 0 )
	{
		ak_print_error_ex(MODULE_ID_VDEC, "ak_vdec_open failed!\n");
		ak_vo_destroy_layer(pOneDecoder->dst_layer);
		if( pOneDecoder )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}
		return NULL;
	}

	//ak_mutexattr_t attr;
	//ak_thread_mutex_init(&pOneDecoder->frame_mutex,&attr);
	//pthread_mutex_init( &pOneDecoder->frame_lock, 0);
	set_decoder_status( pOneDecoder, DECODER_STATUS_START ); 
	ak_print_normal(MODULE_ID_VDEC,"decoder[%d] status is DECODER_STATUS_START!!!\n",pOneDecoder->dec_handle);			

	/* get frame thread */
//	ak_thread_create(&pOneDecoder->recv_frame_id, vdec_recv_frame, (void*)pOneDecoder, ANYKA_THREAD_MIN_STACK_SIZE, -1);
pthread_attr_init( &pOneDecoder->pattr);
	pthread_create(&pOneDecoder->recv_frame_id,&pOneDecoder->pattr,(void*)vdec_recv_frame, (void*)pOneDecoder );
	ak_print_normal(MODULE_ID_VDEC,"ak_new_one_decode[%d] ok!!!\n",pOneDecoder->dec_handle);
	
	return pOneDecoder;
}

void rgb888_to_rgb565(unsigned char * dest, unsigned char * src, int width, int height);
int image_scale(void* src, int fmt_src, int w_src, int h_src, void* dst, int fmt_dst, int w_dst, int h_dst);

int show_one_frame2(struct ak_vo_obj *obj, struct ak_vdec_frame *frame, int layer)
{
    /* set the pos in the screen */
	int num = layer - AK_VO_LAYER_VIDEO_1;
	//printf("---show_one_frame2 layer=%d layerShow[num]=%d\n", layer, layerShow[num]);
	if(layerShow[num]  == 0)
		return;
	int dst_layer   = layerShow[num];
	num = dst_layer - AK_VO_LAYER_VIDEO_1;
    int width		= layer_pos[num].width;
    int height		= layer_pos[num].height;
	//printf("---show_one_frame2 layer=%d x=%d y=%d width=%d height=%d\n", layer, pos_left, pos_top, width, height);
	/* obj to add to layer */
    memset(obj, 0x00, sizeof(struct ak_vo_obj));

#if 0 //defined(ENABLE_RGB888_TO_RGB565)
	if( frame->data_type == AK_TILE_YUV ) goto process_AK_TILE_YUV;

	if( (rgb565_buffer != NULL) &&  (rgb888_buffer != NULL) )
	{
		if( image_scale((void*)frame->yuv_data.data, GP_FORMAT_YUV420SP,  
									frame->yuv_data.pitch_width, frame->yuv_data.pitch_height,
								 	(void*)rgb888_buffer, GP_FORMAT_RGB888, width, height) == 0 )
		{
			rgb888_to_rgb565(rgb565_buffer,rgb888_buffer,width,height);

			obj->format 					= GP_FORMAT_RGB565;
			obj->cmd						= GP_OPT_SCALE|GP_OPT_COLORKEY; 	// GP_OPT_BLIT /* scale to screen	*/
			obj->colorkey.coloract 			= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
			obj->colorkey.color_min			= 0; //0xfffff8;			/* min value */
			obj->colorkey.color_max			= 0; //0xfffff8;			/* max value */

			obj->vo_layer.width 			= width;
			obj->vo_layer.height			= height;
			/* pos and range from the src */
			obj->vo_layer.clip_pos.top		= 0;
			obj->vo_layer.clip_pos.left 	= 0;
			obj->vo_layer.clip_pos.width	= width;
			obj->vo_layer.clip_pos.height	= height;
			ak_mem_dma_vaddr2paddr(rgb565_buffer, &obj->vo_layer.dma_addr);
			
			/* pos and range for the dst layer to contain the src */
			obj->dst_layer.top				= 0;
			obj->dst_layer.left 			= 0;
			obj->dst_layer.width			= width;
			obj->dst_layer.height			= height;

			ak_vo_add_obj(obj, dst_layer);
			ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<dst_layer));
		}
	}
	else
	{
#endif
		process_AK_TILE_YUV:
		if (frame->data_type == AK_TILE_YUV)
		{
			/* if the data type is tileyuv */
			obj->format 		 	= GP_FORMAT_TILED32X4;
			obj->vo_tileyuv.data 	= frame->tileyuv_data;
		}
		else
		{
			/* if the data type is yuv420sp */
			obj->format 			= GP_FORMAT_YUV420SP;
			obj->cmd 				= GP_OPT_SCALE|GP_OPT_COLORKEY;     /* scale to screen 	*/
			//obj->cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
			//obj->alpha 			= 10;
			obj->colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
			obj->colorkey.color_min		= 0; //0xfffff8;			/* min value */
			obj->colorkey.color_max		= 0; //0xfffff8;			/* max value */
			
			obj->vo_layer.width 	= frame->yuv_data.pitch_width;  /* the real width 	*/
			obj->vo_layer.height 	= frame->yuv_data.pitch_height; /* the real height 	*/

			/* pos and range from the src */
			obj->vo_layer.clip_pos.top 		= 0;
			obj->vo_layer.clip_pos.left 	= 0;
			obj->vo_layer.clip_pos.width 	= frame->width;
			obj->vo_layer.clip_pos.height 	= frame->height;
			ak_mem_dma_vaddr2paddr(frame->yuv_data.data, &(obj->vo_layer.dma_addr));
			//printf("11111111show_one_frame2 %d*%d %d*%d\n",frame->yuv_data.pitch_width,frame->yuv_data.pitch_height,frame->width,frame->height);
		}
		/* pos and range for the dst layer to contain the src */
		obj->dst_layer.top 		= 0;
		obj->dst_layer.left 	= 0;
		obj->dst_layer.width 	= width;
		obj->dst_layer.height 	= height;
		ak_vo_add_obj(obj, dst_layer);
		ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<dst_layer));
#if 0 //defined(ENABLE_RGB888_TO_RGB565)
	}
#endif	
    return 0;
}

int clear_one_layer(int layer, int width, int height)
{
	struct ak_vo_obj obj;
	int id = layer+AK_VO_LAYER_VIDEO_1;
	unsigned char *data;
	
	layerShow[layer] = 0;
	printf("clear_one_layer id=%d\n", id);
	data = malloc(width*height*3/2);
    memset(data, 0, width*height*3/2);
	
	obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
	obj.colorkey.color_max		= 1; //0xfffff8;			/* max value */ 		
    obj.format 					= GP_FORMAT_YUV420SP;
    obj.cmd 					= GP_OPT_SCALE|GP_OPT_COLORKEY;     /* scale to screen 	*/
	//obj.format 					= GP_FORMAT_RGB565;   //GP_FORMAT_YUV420SP;
	//obj.cmd						= GP_OPT_BLIT|GP_OPT_COLORKEY; 	/* scale to screen	*/
	obj.colorkey.coloract		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
	
	obj.vo_layer.width 	= width;	/* the real width	*/
	obj.vo_layer.height	= height; /* the real height	*/
	
	/* pos and range from the src */
	obj.vo_layer.clip_pos.top	= 0;
	obj.vo_layer.clip_pos.left 	= 0;
	obj.vo_layer.clip_pos.width	= width;
	obj.vo_layer.clip_pos.height	= height;
	ak_mem_dma_vaddr2paddr(data, &(obj.vo_layer.dma_addr));

	printf("clear_one_layer width=%d height=%d\n", width, height);
    obj.dst_layer.top 		= 0;
    obj.dst_layer.left 	= 0;
    obj.dst_layer.width 	= width;
    obj.dst_layer.height 	= height;
    ak_vo_add_obj(&obj, id);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<id));
	free(data);
    return 0;
}
#if 0
static void *join_test_t(void *arg)
{
	int t=(int)arg;
	printf("11111111join_test_t:%d\n",t);
}
void join_test(void)
{
	//int arg;
	int i=0;
	ak_pthread_t	recv_frame_id[1000];
	for(i=0;i<1000;i++)
	{
		ak_thread_create(&recv_frame_id[i], join_test_t, (void*)i, ANYKA_THREAD_MIN_STACK_SIZE, -1);
		ak_thread_join(recv_frame_id[i]);
		usleep(100*1000);
	}
}
#endif
