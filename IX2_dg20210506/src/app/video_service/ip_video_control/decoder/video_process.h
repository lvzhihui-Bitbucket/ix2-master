
#ifndef _MULTICAST_VIDEO_PROCESS_H_
#define _MULTICAST_VIDEO_PROCESS_H_

#include "video_utility.h"

#define SUPPORT_PACK_FRAGMENT	1

typedef enum
{
	 VIDEO_DISPLAY_JOIN,
	 VIDEO_DISPLAY_LEAVE,
}
VIDEO_DISPLAY_CMD_t;

typedef enum
{
	 VIDEO_DISPLAY_IDLE,
	 VIDEO_DISPLAY_START,
	 VIDEO_DISPLAY_WAIT,
	 VIDEO_DISPLAY_RUN,
	 VIDEO_DISPLAY_STOP,
}
VIDEO_DISPLAY_STATE_t;

typedef struct
{
	int 	Sd;
	int 	addr;
	int 	Port;
	struct sockaddr_in 	sock_addr;	
	int 	type;
	char* 	net_device_name;
} H264_RECEIVE_t;

typedef struct
{
	int					state;		// 0:idle, 1:running
	int					ds_ch;		// 通道号
	H264_RECEIVE_t 		Receive;
	int					recFlag;	// 录像标志
	int					ser_ip;
	one_tiny_task_t 	task_process;	
	//int	rtpMonitor;
} monitor4ds_dat_t;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// public api
//////////////////////////////////////////////////////////////////////////////////////////////////////////

// 开启接收解码
int start_h264_dec_process(int  	     server_ip, int type, short port, int mcg_lowbyte );

// 关闭接收解码
int close_h264_dec_process( void );


#endif



