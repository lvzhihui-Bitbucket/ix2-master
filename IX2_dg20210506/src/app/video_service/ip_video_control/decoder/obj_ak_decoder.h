
#ifndef _AK_DECODER_H_
#define	_AK_DECODER_H_

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"

#define		DECODER_STATUS_IDLE		0
#define		DECODER_STATUS_START	1
#define		DECODER_STATUS_RUN		2
#define		DECODER_STATUS_STOP		3

struct ak_one_decoder
{
    int 			in_type;
    int 			out_type;	
	unsigned int 	src_width;
	unsigned int 	src_height;
	unsigned int 	dst_top;
	unsigned int 	dst_left;
	unsigned int 	dst_width;
	unsigned int 	dst_height;
	unsigned int 	dst_layer;

	int				dec_status;			// 0:idle, 1:start, 2:run, 3:stop
	//ak_mutex_t		dec_mutex;

	int				dec_handle;
	//ak_pthread_t	recv_frame_id;
	//ak_mutex_t		frame_mutex;
	//pthread_mutex_t 	frame_lock;
	pthread_t 			recv_frame_id;			// ����id
	pthread_attr_t 		pattr;
	struct ak_vdec_frame 	frame;	
	struct ak_vo_obj 		obj;
	int frame_cnt;
};


struct ak_one_decoder* ak_new_one_decode( int in_type, int src_w, int src_h, int out_type, int dst_l, int dst_t, int dst_w, int dst_h,int dst_layer );
int ak_del_one_decode(struct ak_one_decoder *pOneDecoder);
int ak_push_frame(struct ak_one_decoder *pOneDecoder, unsigned char *pdat, int len );
void decode_stream(int handle_id, unsigned char *data, int read_len);
int SetLayerPos(int dst_layer, int dst_x, int dst_y, int dst_w, int dst_h);
struct ak_one_decoder* ak_new_one_decode2( int in_type, int src_w, int src_h, int out_type, int dst_layer );
int show_one_frame2(struct ak_vo_obj *obj, struct ak_vdec_frame *frame, int layer);
int set_h264_show_pos(int layer, int x, int y, int width, int height);
int close_one_layer(int layer);


#endif



