#ifndef _OBJ_AVI_FILE_PARSER_H_
#define _OBJ_AVI_FILE_PARSER_H_

#include "utility.h"

//////////////////////////////////////////////////////////////////////////  
#define AVIIF_KEYFRAME  				0x00000010L		// 索引里面的关键帧标志
#define AVIIF_LIST  					0x00000001L  
  
// Flags for dwFlags|AVI婢剁繝鑵戦惃鍕垼韫囨ぞ缍?
#define AVIFILEINFO_HASINDEX        	0x00000010  // 是否有索引 
#define AVIFILEINFO_MUSTUSEINDEX    	0x00000020  
#define AVIFILEINFO_ISINTERLEAVED   	0x00000100  
#define AVIFILEINFO_WASCAPTUREFILE  	0x00010000  
#define AVIFILEINFO_COPYRIGHTED     	0x00020000  
  
// 最大允许的AVI头大小 
#define MAX_ALLOWED_AVI_HEADER_SIZE 131072  
  
// 打不开文件 
#define ERROR_OPEN_AVI              		0  
// 不是有效AVI  
#define ERROR_INVALID_AVI           		1  
// 有效AVI  
#define SUCCESS_VALID_AVI           		2  


typedef uint32 			DWORD;		// 双字
typedef uint16 			WORD;		// 单字 
typedef DWORD 			LONG;		// 定义长整型 
typedef uint8 			BYTE;		// 字节
typedef DWORD 			FourCC;		// 定义four cc;
typedef int				bool;


#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

// 定义fourcc对应的整数值，avi文件中保存的是小端
#define FOURCC_RIFF		0x46464952
#define FOURCC_AVI		0x20495641
#define FOURCC_LIST		0x5453494C
#define FOURCC_hdrl  	0x6C726468
#define FOURCC_avih		0x68697661
#define FOURCC_strl		0x6C727473
#define FOURCC_strh		0x68727473
#define FOURCC_strf		0x66727473
#define FOURCC_STRD		0x64727473
#define FOURCC_vids		0x73646976
#define FOURCC_auds		0x73647561
#define FOURCC_INFO		0x4F464E49
#define FOURCC_ISFT		0x54465349
#define FOURCC_idx1		0x31786469
#define FOURCC_movi		0x69766F6D
#define FOURCC_JUNK		0x4B4E554A
#define FOURCC_vprp		0x70727076
#define FOURCC_PAD		0x20444150
#define FOURCC_DIV3		861292868
#define FOURCC_DIVX		1482049860
#define FOURCC_XVID		1145656920
#define FOURCC_DX50		808802372
  
#define FOURCC_fmt		0x20746D66    // for WAVE files
#define FOURCC_data		0x61746164   // for WAVE files
#define FOURCC_WAVE		0x45564157   // for WAVE files

#define FOURCC_00dc		0x63643030   // for WAVE files
#define FOURCC_01wb		0x62773130   // for WAVE files

#define FOURCC_H264		0x34363268
#define FOURCC_H265		0x31766568

// 调色板
typedef struct  
{  
	BYTE rgbBlue;                       	// 蓝  
	BYTE rgbGreen;                       	// 绿
	BYTE rgbRed;                        	// 红
	BYTE rgbReserved;                   	// 保留 
} RGBQUAD;  
  
// AVI主头部 
typedef struct  
{  
	FourCC fcc;                         // 必须为 avih  
	DWORD cb;                           // 本数据结构的大小，不包括最初的8个字节（fcc和cb两个域）
	DWORD dwMicroSecPerFrame;           // 视频帧间隔时间（以毫秒为单位）
	DWORD dwMaxBytesPerSec;             // 这个AVI文件的最大数据率
	DWORD dwPaddingGranularity;         // 数据填充的粒度
	DWORD dwFlags;                      // AVI文件的全局标记，比如是否含有索引块等
	DWORD dwTotalFrames;                // 总帧数
	DWORD dwInitialFrames;              // 为交互格式指定初始帧数（非交互格式应该指定为0）
	DWORD dwStreams;                    // 本文件包含的流的个数
	DWORD dwSuggestedBufferSize;        // 建议读取本文件的缓存大小（应能容纳最大的块）
	DWORD dwWidth;                      // 视频图像的宽（以像素为单位）
	DWORD dwHeight;                     // 视频图像的高（以像素为单位）
	DWORD dwReserved[4];                // 保留
} AVIMainHeader;  //4*16 = 64
  
  // 定义矩形区域 
typedef struct  
{  
	uint16 left;                     // 左边距
	uint16 top;                      // 顶边距
	uint16 right;                    // 右边距
	uint16 bottom;                   // 底边距
}RECT;  
  
  // AVI流头部
typedef struct  
{  
	FourCC fcc;                         // 必须为 strh
	DWORD cb;                           // 本数据结构的大小,不包括最初的8个字节(fcc和cb两个域)
	FourCC fccType;                     // 流的类型: auds(音频流) vids(视频流) mids(MIDI流) txts(文字流)  
	FourCC fccHandler;                  // 指定流的处理者，对于音视频来说就是解码器
	DWORD dwFlags;                      // 标记：是否允许这个流输出？调色板是否变化？ 
	WORD wPriority;                     // 流的优先级（当有多个相同类型的流时优先级最高的为默认流）  
	WORD wLanguage;                     // 语言  
	DWORD dwInitialFrames;              // 为交互格式指定初始帧数 
	DWORD dwScale;                      // 每帧视频大小或者音频采样大小
	DWORD dwRate;                       // dwScale/dwRate，每秒采样率
	DWORD dwStart;                      // 流的开始时间
	DWORD dwLength;                     // 流的长度（单位与dwScale和dwRate的定义有关）
	DWORD dwSuggestedBufferSize;        // 读取这个流数据建议使用的缓存大小
	DWORD dwQuality;                    // 流数据的质量指标（0 ~ 10,000）
	DWORD dwSampleSize;                 // Sample的大小 
	RECT rcFrame;                       // 指定这个流（视频流或文字流）在视频主窗口中的显示位置，视频主窗口由AVIMAINHEADER结构中的dwWidth和dwHeight决定
} AVIStreamHeader;  //4*16 = 64
  
  // 位图头 
typedef struct  
{  
	DWORD  biSize;  
	LONG   biWidth;  
	LONG   biHeight;  
	WORD   biPlanes;  
	WORD   biBitCount;  
	DWORD  biCompression;  
	DWORD  biSizeImage;  
	LONG   biXPelsPerMeter;  
	LONG   biYPelsPerMeter;  
	DWORD  biClrUsed;  
	DWORD  biClrImportant;  
} BitmapInfoHeader;	//4*10 = 40
  
  // 位图信息
typedef struct  
{  
	BitmapInfoHeader bmiHeader;         // 位图头
	RGBQUAD bmiColors[1];               // 调色板
} BitmapInfo;  
  
  // 音频波形信息
typedef struct  
{  
	WORD wFormatTag;  
	WORD nChannels;                     // 声道数
	DWORD nSamplesPerSec;               // 采样率
	DWORD nAvgBytesPerSec;              // 每秒的数据量
	WORD nBlockAlign;                   // 数据块对齐标志
	WORD wBitsPerSample;                // 每次采样的数据量
	WORD cbSize;                        // 大小
} WaveFormatEx;  //18
  
  // 索引节点信息
typedef struct  
{  
	DWORD dwChunkId;                    // 本数据块的四字符码(00dc 01wb)
	DWORD dwFlags;                      // 说明本数据块是不是关键帧、是不是‘rec ’列表等信息
	DWORD dwOffset;                     // 本数据块在文件中的偏移量
	DWORD dwSize;                       // 本数据块的大小
} AVIIndexEntry;  

// 索引信息
typedef struct
{
	FourCC fcc;                         // 必须为‘idx1’
	DWORD cb;                           // 本数据结构的大小，不包括最初的8个字节（fcc和cb两个域）
	uint32 position;                  // 数据起始位置偏移
	AVIIndexEntry*  videoIndexMap;      // 视频索引表,00dc等转换成整形表示
	AVIIndexEntry*  audioIndexMap;		// 音频索引表,00wb等转换成整形表示
} AVIIndex;

typedef enum
{
	 RECORD_PLAYBACK_IDLE,
	 RECORD_PLAYBACK_START,
	 RECORD_PLAYBACK_RUN,
	 RECORD_PLAYBACK_STOP,
	 RECORD_PLAYBACK_PAUSE,
	 RECORD_PLAYBACK_END,
	 RECORD_PLAYBACK_REPLAY,
}RECORD_PLAYBACK_STATE_t;


typedef struct
{
    //打开AVI文件 
	FILE* aviFd;  
	
	pthread_mutex_t 	lock;

	//是否是有效avi
	bool isValid;

	//是否有视频
	bool hasVideo;  

	//是否有音频
	bool hasAudio;
	
	// 文件长度
	uint32 fLen; 
	
	//avi头
	AVIMainHeader aviMainHeader;  
	
	//avi视频流头
	AVIStreamHeader aviVideoStreamHeader;  
	
	//avi音频流头
	AVIStreamHeader aviAudioStreamHeader;
	
	//位图信息
	BitmapInfo bitmapInfo;
	
	//音频信息
	WaveFormatEx waveInfo;
	
	//// 索引信息
	AVIIndex aviIndex; 	
	
	// movi的开始位置偏移
	uint32 moviOff;  

	//最大视频帧大小
	uint32 maxFrameSize;  
	
	// 当前视频位置索引(数组中的位置,下标)
	//uint32 currVideoIndex;
} AVIFileParset;

uint8 	parseAviFile(AVIFileParset* aviParset, const char * fileName);
int 	getVideoFrame(AVIFileParset* aviParset, char* buf, int index);
int 	getAudioFrame(AVIFileParset* aviParset, char* buf, int index);
int 	getTotalVideoFrames(AVIFileParset* aviParset);
int 	getTotalAudioFrames(AVIFileParset* aviParset);
void 	CloseAviFile(AVIFileParset* aviParset);
void getVideoSize(AVIFileParset* aviParset, int* width, int* height, int* vdtype, int* fps);


#endif


