
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "obj_ak_vi_camera.h"
#include "task_IoServer.h"
#include "elog.h"

int sensor_adjust_initial(void);

#if defined PID_DX470_V25
static int sensor_para_type     = SENSOR_TYPE_TP9950;
#else
static int sensor_para_type     = SENSOR_TYPE_T527;
#endif

static int sensor_para_fd       = -1;
static int sensor_crop_x     =  0;
static int sensor_crop_y     =  0;

int sensor_type_is_t527(void)
{
        return (sensor_para_type == SENSOR_TYPE_T527)?1:0;
}

int sensor_type_is_tw9912(void)
{
        return (sensor_para_type == SENSOR_TYPE_TW9912)?1:0;
}

int sensor_type_is_pr2000(void)
{
        return (sensor_para_type == SENSOR_TYPE_PR2000)?1:0;
}

int sensor_type_is_tp9950(void)
{
        return (sensor_para_type == SENSOR_TYPE_TP9950)?1:0;
}

int get_sensor_crop_x(void)
{
        return sensor_crop_x;
}

int get_sensor_crop_y(void)
{
        return sensor_crop_y;
}


/**
 * @fn:		sensor_para_open
 *
 * @brief:	开启sensor参数调节设备，开启摄像头设备调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_open(void)
{
        sensor_para_fd = open("/dev/sensor_para",O_RDWR);
        if( sensor_para_fd == -1 )
        {
                printf("error open\n");
                return -1;
        }
        else
        {
                // initial sensor type
                SENSOR_CHECK_TYPE para_get;
                if( ioctl( sensor_para_fd, SENSOR_TYPE_GET, (unsigned long)&para_get ) == 0 )
                {
                        sensor_para_type = para_get.type;
                        sensor_crop_x = (para_get.rev>>16)&0xff;
                        sensor_crop_y = para_get.rev&0xff;
			printf("SENSOR_TYPE_GET ok[%d],crpy[x=%d,y=%d]\n", sensor_para_type,sensor_crop_x,sensor_crop_y); 
                        // init power on
                       // sensor_para_power_down(0);
		} 
		else 
		{  
			printf("SENSOR_TYPE_GET failed!: %s\n", strerror(errno));  
			return -1;  
		}  
        }
        return 0;
}

/**
 * @fn:		sensor_para_close
 *
 * @brief:	关闭sensor参数调节设备，关闭摄像头设备时调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_close(void)
{
        if( sensor_para_fd )
        {
        	 sensor_para_power_down(1);	
                close(sensor_para_fd);
                sensor_para_fd = -1;
                // init power off
        }
        return 0;
}

/**
 * @fn:		sensor_para_adjust
 *
 * @brief:	sensor参数调节
 *
 * @param:	page    - 页地址
 * @param:	reg     - 寄存器地址
 * @param:	value   - 数据
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_write(int page, int reg, int value)
{
        SENSOR_IIC_PARA para_set;

        if( sensor_para_fd == -1 )
        {
                printf("sensor para device have no openned\n");
                return -1;
        }

        para_set.page  = page;
        para_set.reg   = reg;
        para_set.value = value;

        printf("------ set sensor para, page[0x%02x],reg[0x%02x],value[0x%02x] ------\n",para_set.page,para_set.reg,para_set.value);

        ioctl( sensor_para_fd, SENSOR_PARA_SET, (unsigned long)&para_set );

        return 0;
}

int sensor_para_read(int page, int reg)
{
        SENSOR_IIC_PARA para_get;

        if( sensor_para_fd == -1 )
        {
                printf("sensor para device have no openned\n");
                return -1;
        }

        para_get.page  = page;
        para_get.reg   = reg;
        para_get.value = 0;

        printf("------ get sensor para, page[0x%02x],reg[0x%02x],value[0x%02x] ------\n",para_get.page,para_get.reg,para_get.value);

        if( ioctl( sensor_para_fd, SENSOR_PARA_GET, (unsigned long)&para_get ) == 0 )
        {
                printf("get sensor para[%d]\n", para_get.value);
                return para_get.value;
        } 
        else 
        {  
                printf("get sensor para failed: %s\n", strerror(errno));  
                return -1;  
        }  
}

// TP9950 para
/*
{0x10,0x00},// brightness       (-63,63)
{0x11,0x40},// contrast         (0-6db, 0x40=0db)
{0x12,0x00},// saturation       (0-6db, 0x40=0db)
*/
#define TP9950_SENSOR_BRIGHT_REG          0x10
#define TP9950_SENSOR_CONTRAST_REG        0x11
#define TP9950_SENSOR_COLOR_REG           0x12

// [0x40:darkest <--> 0x3F:brightest]
const int TP9950_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        0x50, 0x58, 0x60, 0x68, 0x70, 0x00, 0x08, 0x10, 0x18, 0x20
};

// [0x2C:darkest <--> 0x50:brightest]
const int TP9950_SENSOR_CONTRAST_STEP_TAB[10] =
{        
        //0x2C, 0x30, 0x34, 0x38, 0x3C, 0x40, 0x44, 0x48, 0x4C, 0x50
        0x36, 0x3A, 0x3E, 0x42, 0x46, 0x4A, 0x4E, 0x52, 0x56, 0x5A
};

// [0x2C:colorless <--> 0x50:colorful]
const int TP9950_SENSOR_COLOR_STEP_TAB[10] =
{        
        0x2C, 0x30, 0x34, 0x38, 0x3C, 0x40, 0x44, 0x48, 0x4C, 0x50
        //0x30, 0x38, 0x40, 0x48, 0x50, 0x55, 0x59, 0x5D, 0x61, 0x65
};

// T527 para
/*
Page0:
{0x68,0x88},// contrast
{0x69,0x80},// brightness
{0x6A,0x00},// hue sin
{0x6B,0x7F},// hue cos RONLENK    //lyx_20171219 0x60->0x7F
{0x6C,0x80},// chrome saturation
*/
#define T527_SENSOR_BRIGHT_REG          0x69
#define T527_SENSOR_COLOR_REG           0x6C
#define T527_SENSOR_CONTRAST_REG        0x68

const int T527_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        110, 114, 118, 122, 126, 130, 135, 140, 150, 160
};

const int T527_SENSOR_COLOR_STEP_TAB[10] =
{        
        50, 60, 70, 80, 90, 100, 115, 130, 145, 170
};

const int T527_SENSOR_CONTRAST_STEP_TAB[10] =
{        
        80, 88, 96, 104, 112, 120, 130, 140, 150, 160
};

// TW9912 para
/*
Page0:
{0x68,0x88},// contrast
{0x69,0x80},// brightness
{0x6A,0x00},// hue sin
{0x6B,0x7F},// hue cos RONLENK    //lyx_20171219 0x60->0x7F
{0x6C,0x80},// chrome saturation
*/
#define TW9912_SENSOR_BRIGHT_REG          0x10
#define TW9912_SENSOR_COLOR_U_REG         0x13 // 0x14(chrome v,default 0x80) 0x13(chrome u,defaule 0x80)
#define TW9912_SENSOR_COLOR_V_REG         0x14 // 0x14(chrome v,default 0x80) 0x13(chrome u,defaule 0x80)
#define TW9912_SENSOR_CONTRAST_REG        0x11

const int TW9912_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        0xA0, 0x98, 0x90, 0x88, 0x80, 0x00, 0x08, 0x10, 0x18, 0x20
};

const int TW9912_SENSOR_COLOR_U_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int TW9912_SENSOR_COLOR_V_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int TW9912_SENSOR_CONTRAST_STEP_TAB[10] =
{        
        0x04, 0x14, 0x24, 0x34, 0x54, 0x64, 0x74, 0x84, 0x94, 0xA4
};


int sensor_para_set_brightness(int brightness)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                #if 1
                sensor_para_write(T527_PAGE0, T527_SENSOR_BRIGHT_REG, T527_SENSOR_BRIGHT_STEP_TAB[brightness]);
                #else
                sensor_para_write(T527_PAGE0, T527_SENSOR_BRIGHT_REG, brightness);
                #endif
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_BRIGHT_REG, TW9912_SENSOR_BRIGHT_STEP_TAB[brightness]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TP9950 )
        {
                sensor_para_write(0x00, TP9950_SENSOR_BRIGHT_REG, TP9950_SENSOR_BRIGHT_STEP_TAB[brightness]);
        }
}

int sensor_para_set_color(int color)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                #if 1
                sensor_para_write(T527_PAGE0, T527_SENSOR_COLOR_REG, T527_SENSOR_COLOR_STEP_TAB[color]);
                #else
                sensor_para_write(T527_PAGE0, T527_SENSOR_COLOR_REG, color);
                #endif
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_COLOR_U_REG, TW9912_SENSOR_COLOR_U_STEP_TAB[color]);
                sensor_para_write(0x00, TW9912_SENSOR_COLOR_V_REG, TW9912_SENSOR_COLOR_V_STEP_TAB[color]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TP9950 )
        {
                sensor_para_write(0x00, TP9950_SENSOR_COLOR_REG, TP9950_SENSOR_COLOR_STEP_TAB[color]);
        }
}

int sensor_para_set_contrast(int contrast)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                #if 1
                sensor_para_write(T527_PAGE0, T527_SENSOR_CONTRAST_REG, T527_SENSOR_CONTRAST_STEP_TAB[contrast]);
                #else
                sensor_para_write(T527_PAGE0, T527_SENSOR_CONTRAST_REG, contrast);
                #endif
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_CONTRAST_REG, T527_SENSOR_CONTRAST_STEP_TAB[contrast]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TP9950 )
        {
                sensor_para_write(0x00, TP9950_SENSOR_CONTRAST_REG, TP9950_SENSOR_CONTRAST_STEP_TAB[contrast]);
        }
}

#define T527_POWER_DOWN_REG1   0x0F
#define T527_POWER_DOWN_REG2   0xCA
#define T527_POWER_DOWN_REG3   0xE0

int sensor_para_power_down(int enable)
{
	#if 1
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                if( enable )
                {
                        // power off
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG1, 0x10);
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG2, 0x22);
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG3, 0xD1);
                }
                else
                {
                        // power on
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG1, 0x00);
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG2, 0x02);
                        sensor_para_write(T527_PAGE0, T527_POWER_DOWN_REG3, 0x00);
                }
        }
	#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MAX_BRIGHTNESS_STEP       9 //250             // 亮度最大等级
#define MIN_BRIGHTNESS_STEP       0             // 亮度最小等级
#define GAP_BRIGHTNESS_STEP       1             // 亮度调节步进

#define MAX_COLOR_STEP            9 //250             // 色度最大等级
#define MIN_COLOR_STEP            0             // 色度最小等级
#define GAP_COLOR_STEP            1             // 色度调节步进

#define MAX_CONTRAST_STEP         9 //250             // 对比度最大等级
#define MIN_CONTRAST_STEP         0             // 对比度最小等级
#define GAP_CONTRAST_STEP         1             // 对比度调节步进

static int brightness, color, contrast;

/**
 * @fn:		sensor_adjust_initial
 *
 * @brief:	参数初始化：开启sensor后从io文件读取当前亮度、色度参数
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_adjust_initial(void)
{
	char temp[20];
        // brightness
        API_Event_IoServer_InnerRead_All(VIDEO_DISPLAY_BRIGHT, temp);
        printf("sensor_adjust_initial(BRIGHT) = %d\n", atoi(temp));
        log_d("sensor_adjust_initial(BRIGHT) = %d\n", atoi(temp));
        brightness = atoi(temp);
        // color
        API_Event_IoServer_InnerRead_All(VIDEO_DISPLAY_COLOR, temp);
        printf("sensor_adjust_initial(COLOR) = %d\n", atoi(temp));
        log_d("sensor_adjust_initial(COLOR) = %d\n", atoi(temp));
        color = atoi(temp);
        // contrast
        API_Event_IoServer_InnerRead_All(VIDEO_DISPLAY_CONTRAST, temp);
        printf("sensor_adjust_initial(CONTRAST) = %d\n", atoi(temp));
        log_d("sensor_adjust_initial(CONTRAST) = %d\n", atoi(temp));
        contrast = atoi(temp);

        //initial
        sensor_para_set_brightness(brightness);
        sensor_para_set_color(color);
        sensor_para_set_contrast(contrast);
	return 0;
}

/**
 * @fn:		sensor_brightness_adjust
 *
 * @brief:	亮度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_brightness_adjust(int inc)
{
        char temp[20];
        if( inc )
        {
                brightness += GAP_BRIGHTNESS_STEP; if( brightness > MAX_BRIGHTNESS_STEP) brightness = MIN_BRIGHTNESS_STEP;
                sensor_para_set_brightness(brightness);
        }
        else
        {
                if( brightness <= MIN_BRIGHTNESS_STEP ) brightness = MAX_BRIGHTNESS_STEP; else brightness -= GAP_BRIGHTNESS_STEP;
                sensor_para_set_brightness(brightness);
        }
        sprintf(temp, "%d", brightness);
        API_Event_IoServer_InnerWrite_All(VIDEO_DISPLAY_BRIGHT, temp);
        printf("sensor_brightness_adjust->[%02x]",brightness);
        log_d("sensor_brightness_adjust->[%02x]",brightness);
        return brightness;
}

/**
 * @fn:		sensor_color_adjust
 *
 * @brief:	色度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_color_adjust(int inc)
{
        char temp[20];
        if( inc )
        {
                color += GAP_COLOR_STEP; if( color > MAX_COLOR_STEP) color = MIN_COLOR_STEP;
                sensor_para_set_color(color);
        }
        else
        {
                if( color <= MIN_COLOR_STEP ) color = MAX_COLOR_STEP; else color -= GAP_COLOR_STEP;
                sensor_para_set_color(color);
        }
        sprintf(temp, "%d", color);
        API_Event_IoServer_InnerWrite_All(VIDEO_DISPLAY_COLOR, temp);
        printf("sensor_color_adjust->[%02x]",color);
        log_d("sensor_color_adjust->[%02x]",color);
        return color;
}

/**
 * @fn:		sensor_contrast_adjust
 *
 * @brief:	对比度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_contrast_adjust(int inc)
{
        char temp[20];
        if( inc )
        {
                contrast += GAP_CONTRAST_STEP; if( contrast > MAX_CONTRAST_STEP) contrast = MIN_CONTRAST_STEP;
                sensor_para_set_contrast(contrast);
        }
        else
        {
                if( contrast <= MIN_CONTRAST_STEP ) contrast = MAX_CONTRAST_STEP; else contrast -= GAP_CONTRAST_STEP;
                sensor_para_set_contrast(contrast);
        }
        sprintf(temp, "%d", contrast);
        API_Event_IoServer_InnerWrite_All(VIDEO_DISPLAY_CONTRAST, temp);
        printf("sensor_contrast_adjust->[%02x]",contrast);
        log_d("sensor_contrast_adjust->[%02x]",contrast);
        return contrast;
}

/**
 * @fn:		sensor_contrast_adjust
 *
 * @brief:	对比度参数调节
 *
 * @param:	pbr - 亮度参数指针
 * @param:	pcl - 色度参数指针
 * @param:	pco - 对比度参数指针
 *
 * @return:     -1/err, 0/ok
 */
int get_sensor_cur_para(int* pbr, int* pcl, int* pco)
{
        *pbr = brightness;
        *pcl = color;
        *pco = contrast;

        printf("get current sensor orig para, brightness[%02x],color[%02x],contrast[%02x]",brightness,color,contrast);

        return 0;
}

/**
 * @fn:		sensor_set_pal_ntsc
 *
 * @brief:	设置制式
 *
 * @param:	pal0ntsc1 - 0/pal, 1/ntsc
 *
 * @return:     -1/err, 0/ok
 */
int sensor_set_pal_ntsc(int pal0ntsc1)
{
        SENSOR_PAL_NTSC para_set;

        if( sensor_para_fd == -1 )
        {
                printf("sensor para device have no openned\n");
                return -1;
        }

        para_set.pal0ntsc1  = pal0ntsc1;
        para_set.reserved   = 0;

        printf("------ sensor_set_pal_ntsc, value[%d] ------\n",pal0ntsc1);

        ioctl( sensor_para_fd, SENSOR_P0N1_SET, (unsigned long)&para_set );

        return 0;
}
