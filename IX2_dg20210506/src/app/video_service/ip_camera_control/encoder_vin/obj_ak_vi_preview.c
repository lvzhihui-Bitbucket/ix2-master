
/**
 * Description: 3路视频preview处理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
*/

#include <stdio.h>
#include <string.h>
#include "obj_ak_vi_preview.h"

typedef struct
{
	// state
	int					enable;		// 1-enable，0-disable
	int					vi_d;		// 显示窗口号
	int					vi_c;		// 通道号
	// input para
	int					vi_w;		// 视频输入宽度
	int					vi_h;		// 视频输入高度
	// output para
	int					vo_x;		// 视频预览x坐标
	int					vo_y;		// 视频预览y宽度
	int 				vo_w;		// 视频预览宽度
	int					vo_h;		// 视频输入高度
	
} VI_PREVIEW_INSTANCE_t;

VI_PREVIEW_INSTANCE_t  all_ak_vi_preview[MAX_PREVIEW_WINS] = 
{
	// channel 0
	{
		.enable		= 0,
		.vi_d		= 0,
		.vi_c		= 0,
		.vi_w		= 704,
		.vi_h		= 520,		
		.vo_x		= 0,
		.vo_y		= 0,
		.vo_w		= 1024,
		.vo_h		= 600,
	},
	// channel 1
	{
		.enable 	= 0,
		.vi_d		= 1,
		.vi_c		= 1,
		.vi_w		= 640,
		.vi_h		= 480,		
		.vo_x		= 0,
		.vo_y		= 0,
		.vo_w		= 512,
		.vo_h		= 300,
	},
	// channel 2
	{
		.enable 	= 0,
		.vi_d		= 2,
		.vi_c		= 2,
		.vi_w		= 320,
		.vi_h		= 240,		
		.vo_x		= 512,
		.vo_y		= 0,
		.vo_w		= 512,
		.vo_h		= 300,
	},
	{
		.enable 	= 0,
		.vi_d		= 2,
		.vi_c		= 2,
		.vi_w		= 320,
		.vi_h		= 240,		
		.vo_x		= 0,
		.vo_y		= 300,
		.vo_w		= 512,
		.vo_h		= 300,
	},
	{
		.enable 	= 0,
		.vi_d		= 2,
		.vi_c		= 2,
		.vi_w		= 320,
		.vi_h		= 240,		
		.vo_x		= 512,
		.vo_y		= 300,
		.vo_w		= 512,
		.vo_h		= 300,
	}
};

void ak_preview_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h )
{
	all_ak_vi_preview[0].vi_w = ch0_w;
	all_ak_vi_preview[0].vi_h = ch0_h;
	all_ak_vi_preview[1].vi_w = ch1_w;
	all_ak_vi_preview[1].vi_h = ch1_h;
	all_ak_vi_preview[2].vi_w = ch1_w/2;
	all_ak_vi_preview[2].vi_h = ch1_h/2;
}

static int preview_ref_cnt = 0;

static char* rgb565_buffer = NULL; //(char *)ak_mem_dma_alloc(MODULE_ID_APP, width*height*2);
static char* rgb888_buffer = NULL; //(char *)ak_mem_dma_alloc(MODULE_ID_APP, width*height*3);

int malloc_ch0_yuv2rgb_buffer(void)
{
	if( rgb565_buffer != NULL )	ak_mem_dma_free(rgb565_buffer);
	rgb565_buffer = (char *)ak_mem_dma_alloc(MODULE_ID_APP, 1024*600*2);

	if( rgb888_buffer != NULL )	ak_mem_dma_free(rgb888_buffer);
	rgb888_buffer = (char *)ak_mem_dma_alloc(MODULE_ID_APP, 1024*600*3);

	return 0;
}

int free_ch0_yuv2rgb_buffer(void)
{
	if( rgb888_buffer != NULL ) 
	{
		ak_mem_dma_free(rgb888_buffer);		// release 
		rgb888_buffer = NULL;
	}
	if( rgb565_buffer != NULL ) 
	{
		ak_mem_dma_free(rgb565_buffer);		// release 
		rgb565_buffer = NULL;
	}
	return 0;
}

/**
 * @fn:		vi_preview_init
 *
 * @brief:	preview初始化，使用安凯视频层的第4层
 *
 * @param:	layer_w - 预览视频层宽度
 * @param:	layer_h - 预览视频层高度
 *
 * @return: 0
 */
 #if 1
int vi_preview_init( int left,int top,int layer_w, int layer_h )
{
	if( !preview_ref_cnt )
	{
		#if 0
		struct ak_vo_layer_in video_layer;
		video_layer.create_layer.width  = layer_w;
		video_layer.create_layer.height = layer_h;
		video_layer.create_layer.left  	= left;
		video_layer.create_layer.top   	= top;
		video_layer.layer_opt          	= 0;
		video_layer.format             	= GP_FORMAT_RGB565;
		ak_vo_create_video_layer(&video_layer, AK_VO_LAYER_VIDEO_4);
		#endif
		SetLayerPos(AK_VO_LAYER_VIDEO_4,left,top,layer_w,layer_h);
	}
	preview_ref_cnt++;
	return 0;
}
 #else
 int vi_preview_init(int layer_w, int layer_h )
{
	if( !preview_ref_cnt )
	{
		struct ak_vo_layer_in video_layer;
		video_layer.create_layer.width  = layer_w;
		video_layer.create_layer.height = layer_h;
		video_layer.create_layer.left  	= 0;
		video_layer.create_layer.top   	= 0;
		video_layer.layer_opt          	= 0;
		video_layer.format             	= GP_FORMAT_RGB565;
		ak_vo_create_video_layer(&video_layer, AK_VO_LAYER_VIDEO_4);
	}
	preview_ref_cnt++;
	return 0;
}
 #endif

/**
 * @fn:		vi_preview_deinit
 *
 * @brief:	preview反初始化，释放显示层
 *
 * @param:	none
 *
 * @return: 0
 */
int vi_preview_deinit( void )
{
	if( preview_ref_cnt )
	{
		preview_ref_cnt--;
		if( !preview_ref_cnt )
		{
			//ak_vo_destroy_layer(AK_VO_LAYER_VIDEO_4);
			close_one_layer(AK_VO_LAYER_VIDEO_4);
		}
	}
	return 0;
}

/**
 * @fn:		vi_preview_start
 *
 * @brief:	启动一个preview实例窗口
 *
 * @param:	pins - 实例指针
 * @param:	vi_c - 视频通道
 *
 * @return: 0
 */
int vi_preview_start( VI_PREVIEW_INSTANCE_t *pins, int vi_c )
{
	int ret;

	printf("----------------vi_preview_start:,vi_c[%d]->vi_w[%d],vi_h[%d],vo_x[%d],vo_y[%d],vo_w[%d],vo_h[%d]----------------\n",
		vi_c,
		pins->vi_w,
		pins->vi_h,
		pins->vo_x,
		pins->vo_y,
		pins->vo_w,
		pins->vo_h );
	
	pins->vi_c	 = vi_c;
	#if 1
	vi_preview_init(pins->vo_x,pins->vo_y,pins->vo_w,pins->vo_h);
	#else
	vi_preview_init(1024,600);
	#endif
	pins->enable = 1;		// lzh_20231117 move here
	return 0;
}

/**
 * @fn:		vi_preview_stop
 *
 * @brief:	停止一个preview实例窗口
 *
 * @param:	pins - 实例指针
 *
 * @return: 0
 */
int vi_preview_stop( VI_PREVIEW_INSTANCE_t *pins )
{
	printf("----------------vi_preview_stop----------------\n");	
	pins->enable = 0;	
	vi_preview_deinit();
	return 0;
}

extern void ak_vo_refresh_cmd_set(int layer);

/**
 * @fn:		vi_preview_frame
 *
 * @brief:	获取一个preview实例的数据帧
 *
 * @param:	pins 	- 实例指针
 * @param:	pframe 	- 数据帧指针
 *
 * @return: -1/err, 0/ok
 */
// test-20230707
#if 0
struct ak_tde_cmd tde_cmd_param;
struct ak_tde_layer tde_layer_src = { 704 , 576 , 0 , 0 , 704 , 576 , 0 , GP_FORMAT_YUV420SP };
struct ak_tde_layer tde_layer_tgt = { 1024 , 600 , 0 , 0 , 1024 , 600 , 0 , GP_FORMAT_RGB888 };
static char* SrcDat = NULL; //[704*576*3/2];
static char* TarDat = NULL; //[1024*600*3];
char* convert_yuv420sp_to_rgb888(char* psrc, int len )
{
	memset( &tde_cmd_param , 0 , sizeof( struct ak_tde_cmd ) );	
	tde_cmd_param.opt = GP_OPT_SCALE;

	if( SrcDat == NULL )
		SrcDat = (char *)ak_mem_dma_alloc(MODULE_ID_VI, len); //704*576*3/2);
	if( TarDat == NULL )
		TarDat = (char *)ak_mem_dma_alloc(MODULE_ID_VI, 1024*600*3);

    ak_mem_dma_vaddr2paddr( SrcDat , ( unsigned long * )&tde_layer_src.phyaddr );
	memcpy(SrcDat,psrc,len);
    ak_mem_dma_vaddr2paddr( TarDat , ( unsigned long * )&tde_layer_tgt.phyaddr );	
	
    tde_cmd_param.tde_layer_src = tde_layer_src;
    tde_cmd_param.tde_layer_dst = tde_layer_tgt;	

    if( ak_tde_opt( &tde_cmd_param ) == 0 )
	{
		return TarDat;
		printf("===========================ok===============\n");
	}
	else
	{
		printf("===========================er===============\n");		
		return NULL;
	}
}
void close_yuv420sp_to_rgb888(void)
{
	if( SrcDat != NULL ) 
	{
		ak_mem_dma_free(SrcDat);
		SrcDat = NULL;
	}
	if( TarDat != NULL ) 
	{
		ak_mem_dma_free(TarDat);
		TarDat = NULL;
	}
}
#endif

void rgb888_to_rgb565(unsigned char * dest, unsigned char * src, int width, int height);

int image_scale(void* src, int fmt_src, int w_src, int h_src, void* dst, int fmt_dst, int w_dst, int h_dst)
{
	struct ak_tde_layer l_src, l_dst;

	memset( &l_src, 0, sizeof(struct ak_tde_layer));
	memset( &l_dst, 0, sizeof(struct ak_tde_layer));

	l_src.format_param 	= fmt_src;
	l_src.width			= w_src;
	l_src.height		= h_src;
	l_src.pos_width		= w_src;
	l_src.pos_height	= h_src;
    //ak_mem_dma_vaddr2paddr( src , &l_src.phyaddr );
	l_src.phyaddr = src;		// src already the physical address

	l_dst.format_param 	= fmt_dst;
	l_dst.width			= w_dst;
	l_dst.height		= h_dst;
	l_dst.pos_left		= 0;
	l_dst.pos_top		= 0;
	l_dst.pos_width		= w_dst;
	l_dst.pos_height	= h_dst;
    ak_mem_dma_vaddr2paddr( dst , &l_dst.phyaddr );

    return ak_tde_opt_scale( &l_src, &l_dst);
}

int vi_preview_frame( VI_PREVIEW_INSTANCE_t *pins, struct video_input_frame* pframe )
{
	int rgb888_w,rgb888_h;
	int width,height;
	int dst_layer;
	if( !pins->enable)
		return -1;

	struct ak_vo_obj	obj;
	memset((char*)&obj, 0, sizeof(struct ak_vo_obj));

	dst_layer=get_ds_show_layer(3);
	get_layer_pos(dst_layer-AK_VO_LAYER_VIDEO_1,&width,&height);
	// lzh_20230818_s
	#if 1
	if( width == 800 && height == 480 )
	{
		width = 640;
		height = 480;
	}
	#endif
	// lzh_20230818_e

	//printf("------------------obj.vo_layer.width[%d],height[%d],frame_len[%d]\n",pins->vi_w,pins->vi_h, pframe->vi_frame.len);

#if defined(ENABLE_RGB888_TO_RGB565)
	if( (rgb565_buffer != NULL) &&  (rgb888_buffer != NULL) )
	{
		rgb888_w = pins->vi_w;		// 80-90ms 11-12fps
		rgb888_h = pins->vi_h;		// resolution: 704*576
		//rgb888_w = width/2;			// 40-50ms 20-25fps
		//rgb888_h = height/2;			// resolution: 512*300
		//PrintCurrentTime(777);
		if( image_scale((void*)pframe->phyaddr, GP_FORMAT_YUV420SP, pins->vi_w, pins->vi_h, (void*)rgb888_buffer, GP_FORMAT_RGB888, rgb888_w, rgb888_h) == 0 )
		{
			//PrintCurrentTime(888);
			rgb888_to_rgb565(rgb565_buffer,rgb888_buffer,rgb888_w,rgb888_h);
			//PrintCurrentTime(999);

			obj.format 						= GP_FORMAT_RGB565;
			obj.cmd							= GP_OPT_SCALE; 	// GP_OPT_BLIT /* scale to screen	*/
			obj.vo_layer.width 				= rgb888_w;
			obj.vo_layer.height				= rgb888_h;
			/* pos and range from the src */
			obj.vo_layer.clip_pos.top		= 0;
			obj.vo_layer.clip_pos.left 		= 0;
			obj.vo_layer.clip_pos.width		= rgb888_w;
			obj.vo_layer.clip_pos.height	= rgb888_h;
			ak_mem_dma_vaddr2paddr(rgb565_buffer, &obj.vo_layer.dma_addr);
			
			/* pos and range for the dst layer to contain the src */
			obj.dst_layer.top				= 0;
			obj.dst_layer.left 				= 0;
			obj.dst_layer.width				= width;
			obj.dst_layer.height			= height;

			ak_vo_add_obj(&obj, dst_layer);
			ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_4));
		}
	}
	else
	{
#endif
		obj.format 						= GP_FORMAT_YUV420SP;
		#if 1
		obj.cmd							= GP_OPT_SCALE; 	/* scale to screen	*/
		#else
		obj.cmd 				= GP_OPT_SCALE|GP_OPT_COLORKEY;     /* scale to screen 	*/
		//obj->cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
		//obj->alpha 			= 10;
		obj.colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
		obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */
		#endif
		obj.vo_layer.width 				= pins->vi_w;	/* the real width	*/
		obj.vo_layer.height				= pins->vi_h; 	/* the real height	*/
		/* pos and range from the src */
#if !defined (PID_DX470_V25)
		obj.vo_layer.clip_pos.top		= 0;
		obj.vo_layer.clip_pos.left 		= 0;
		obj.vo_layer.clip_pos.width		= pins->vi_w;
		obj.vo_layer.clip_pos.height	= pins->vi_h;
		obj.vo_layer.dma_addr 			= pframe->phyaddr;		
#else
		//obj.vo_layer.clip_pos.top		= 0;	// 
		obj.vo_layer.clip_pos.top		= 6;	// cut top black edge
		obj.vo_layer.clip_pos.left 		= 0;
		obj.vo_layer.clip_pos.width		= pins->vi_w;
		//obj.vo_layer.clip_pos.height	= pins->vi_h; //
		obj.vo_layer.clip_pos.height	= pins->vi_h-6; // cut top black edge
		obj.vo_layer.dma_addr 			= pframe->phyaddr;		
#endif		
		/* pos and range for the dst layer to contain the src */
		obj.dst_layer.top				= 0;
		obj.dst_layer.left 				= 0;
		obj.dst_layer.width			= width;
		obj.dst_layer.height		= height;

		ak_vo_add_obj(&obj, dst_layer);
		//ak_vo_refresh_screen(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_4));
		ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_4));
#if defined(ENABLE_RGB888_TO_RGB565)
	}
#endif	
	return 0;	
}

//////////////////////////////////////////////////////////////////////////////

/**
 * @fn:		ak_vi_preview_set_input
 *
 * @brief:	配置一个preview的显示输入参数
 *
 * @param:	win 	- preview窗口
 * @param:	vi_w 	- 视频通道的宽度
 * @param:	vi_h 	- 视频通道的高度
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_set_input(int win, int vi_c, int vi_w, int vi_h)
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		all_ak_vi_preview[win].vi_c		= vi_c;
		all_ak_vi_preview[win].vi_w		= vi_w;
		all_ak_vi_preview[win].vi_h		= vi_h;
		return 0;
	}
}

/**
 * @fn:		ak_vi_preview_get_input
 *
 * @brief:	获取一个preview的显示参数
 *
 * @param:	win		- preview通道
 * @param:	vi_c 	- 显示窗口关联的视频通道
 * @param:	vi_w 	- 视频通道的宽度指针
 * @param:	vi_h 	- 视频通道的高度指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_get_input(int win, int* vi_c, int* vi_w, int* vi_h )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		if( vi_c != NULL ) *vi_c = all_ak_vi_preview[win].vi_c;
		if( vi_w != NULL ) *vi_w = all_ak_vi_preview[win].vi_w;
		if( vi_h != NULL ) *vi_h = all_ak_vi_preview[win].vi_h;
		return 0;
	}
}


/**
 * @fn:		ak_vi_preview_set_output
 *
 * @brief:	配置一个preview的显示输出参数
 *
 * @param:	win 	- preview窗口
 * @param:	vo_x 	- preview显示x坐标
 * @param:	vo_y 	- preview显示y坐标
 * @param:	vo_w 	- preview显示宽度
 * @param:	vo_h 	- preview显示高度
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_set_output(int win, int vo_x, int vo_y, int vo_w, int vo_h)
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		all_ak_vi_preview[win].vo_x		= vo_x;
		all_ak_vi_preview[win].vo_y		= vo_y;
		all_ak_vi_preview[win].vo_w		= vo_w;
		all_ak_vi_preview[win].vo_h 	= vo_h;
		return 0;
	}
}


/**
 * @fn:		ak_vi_preview_get
 *
 * @brief:	获取一个preview的显示参数
 *
 * @param:	win		- preview通道
 * @param:	vo_x 	- preview显示x坐标指针
 * @param:	vo_y 	- preview显示y坐标指针
 * @param:	vo_w 	- preview显示宽度指针
 * @param:	vo_h 	- preview显示高度指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_get_output(int win, int* vo_x, int* vo_y, int* vo_w, int* vo_h)
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		if( vo_x != NULL ) *vo_x = all_ak_vi_preview[win].vo_x;
		if( vo_y != NULL ) *vo_y = all_ak_vi_preview[win].vo_y;
		if( vo_w != NULL ) *vo_w = all_ak_vi_preview[win].vo_w;
		if( vo_h != NULL ) *vo_h = all_ak_vi_preview[win].vo_h;
		return 0;
	}
}

/**
 * @fn:		ak_vi_preview_start
 *
 * @brief:	启动一个preview通道
 *
 * @param:	win 	- preview通道
 * @param:	ch 		- 对应视频通道
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_start(int win, int ch )
{
	if( win >= MAX_PREVIEW_WINS || ch >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		Get_OneIpc_ShowPos(win,&all_ak_vi_preview[win].vo_x,&all_ak_vi_preview[win].vo_y,&all_ak_vi_preview[win].vo_w,&all_ak_vi_preview[win].vo_h);
		return vi_preview_start( &all_ak_vi_preview[win], ch );
	}
}

/**
 * @fn:		ak_vi_preview_stop
 *
 * @brief:	停止一个preview通道
 *
 * @param:	win 	- preview通道
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_stop( int win )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
		return vi_preview_stop( &all_ak_vi_preview[win] );
}

/**
 * @fn:		ak_vi_preview_get_state
 *
 * @brief:	得到一个通道的状态
 *
 * @param:	win 	- preview通道
 *
 * @return: -1/err, channel/ok
 */
int ak_vi_preview_get_state( int win )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
		return all_ak_vi_preview[win].enable;
}

/**
 * @fn:		ak_vi_preview_frame
 *
 * @brief:	获取一个preview通道的帧数据
 *
 * @param:	win 	- preview通道
 * @param:	pframe	- 数据帧指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_frame( int win, void* pframe )
{
	int i;
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	else
	{
		return vi_preview_frame( &all_ak_vi_preview[win], (struct video_input_frame*)pframe );
	}
}
