
/**
 * Description: 视频码流发送通用列表管理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
*/

#include "obj_ak_vi_userlist.h"

/**
 * @fn:		api_add_one_subscriber
 *
 * @brief:	生成一个发送节点类型，并添加到指定列表指针
 *
 * @param:	plist 				- 列表指针
 * @param:	subscribe_token 	- 订阅得到的token
 * @param:	subscribe_type 		- 订阅的type
 * @param:	subscribe_callback 	- 发送回调函数指针
 * @param:	channel 			- 发送通道号
 *
 * @return: -1/err, 0/ok
 */
int api_add_one_subscriber(struct list_head *plist, int subscribe_token, int subscribe_type, subscribe_callback_t subscribe_callback, int channel )
{
	subscribe_node_t *subscribe_node = malloc( sizeof(subscribe_node_t) );
	if(subscribe_node != NULL)
	{
		INIT_LIST_HEAD(&subscribe_node->one_node);
		subscribe_node->channel          	= channel;
		subscribe_node->subscribe_token		= subscribe_token;
		subscribe_node->subscribe_type		= subscribe_type;
		subscribe_node->subscribe_callback  = subscribe_callback;
		list_add_tail(&subscribe_node->one_node, plist); 
        return subscribe_node->subscribe_token;
	}
    else
        return -1;
}

/**
 * @fn:		api_del_one_subscriber
 *
 * @brief:	释放一个发送节点类型
 *
 * @param:	plist 				- 列表指针
 * @param:	subscribe_token 	- 需要释放的token
 *
 * @return: -1/err, 0/ok
 */
int api_del_one_subscriber(struct list_head *plist, int subscribe_token )
{
	int ret = -1;
	struct list_head *p_node,*p_node_temp;

	list_for_each_safe(p_node,p_node_temp,plist)
	{
		subscribe_node_t* pnode = list_entry(p_node,subscribe_node_t,one_node);
		if( pnode->subscribe_token == subscribe_token )
		{
			list_del_init(&pnode->one_node);
			free(pnode);
			ret = 0;
			break;
		}
	}
	return ret;
}

/**
 * @fn:		api_del_all_subscribers
 *
 * @brief:	释放当前列表的所有发送节点
 *
 * @param:	plist 				- 列表指针
 *
 * @return: -1/err, 0/ok
 */
int api_del_all_subscribers(struct list_head *plist )
{
	if( list_empty(plist) )
	{
		struct list_head *p_node,*p_node_temp;	
		list_for_each_safe(p_node,p_node_temp,plist)
		{
			subscribe_node_t* pnode = list_entry(p_node,subscribe_node_t,one_node);
			list_del_init(&pnode->one_node);
			free(pnode);
		}
	}
	return 0;
}

/**
 * @fn:		api_callback_subscribers
 *
 * @brief:	指定列表处理数据的回调函数
 *
 * @param:	plist 	- 列表指针
 * @param:	data 	- 待发送数据指针
 * @param:	len 	- 待发送数据长度
 *
 * @return: -1/err, 0/ok
 */
int api_callback_subscribers(struct list_head *plist,char *data,int len )
{	
	struct list_head *p_node,*p_node_temp;

	subscribe_callback_t p_callback;
	
	list_for_each_safe(p_node,p_node_temp,plist)
	{
		subscribe_node_t* pnode = list_entry(p_node,subscribe_node_t,one_node);
		p_callback = pnode->subscribe_callback;		
		if( p_callback != NULL )
		{	
			p_callback(pnode->channel,data,len);
		}
	}
    return 0;
}

