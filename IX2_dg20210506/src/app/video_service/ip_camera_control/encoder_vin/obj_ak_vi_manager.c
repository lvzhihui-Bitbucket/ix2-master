
/**
 * Description: 3路视频码流管理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 * 组件函数调用相关顺序说明：
 * 若配置更改，调用api_ak_vi_set_channel_resolution：	配置当前通道的分辨率
 * 若配置更改，调用api_ak_vi_set_channel_encoder：		配置当前通道的编码参数
 * 若配置更改，调用api_ak_vi_set_channel_preview：		配置当前通道的预览位置和大小
 * 
 * 
 * api_ak_vi_trans_subscriber：		订阅机制启动指定通道码流传输
 * api_ak_vi_trans_desubscriber：	订阅机制停止指定通道码流传输
 */

#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>
#include <list.h>

#include "obj_ak_vi_manager.h"
#include "obj_ak_vi_encoder.h"
#include "obj_ak_vi_preview.h"
#include "obj_ak_vi_camera.h"

#include "ak_common.h"
#include "ak_log.h"
#include "ak_common_video.h"
#include "ak_venc.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_vi.h"
#include "cJSON.h"
#include "obj_gpio.h"
#include "OSTIME.h"
#include "task_IoServer.h"

#ifndef AK_SUCCESS
#define AK_SUCCESS 			0
#endif

#ifndef AK_FAILED
#define AK_FAILED 			-1
#endif

#define DEF_FRAME_DEPTH		3
#define AK_VI_CH_MAX		3

typedef struct
{
	int state;							// 状态
	int dev_id;							// 设备号
	int vi_ch;							// 通道号
	int vi_id;							// 通道索引
	int video_w;						// 视频源宽度
	int video_h;						// 视频源高度
	int                 enc_user_token;	// 编码订阅token
	struct list_head    enc_user_list; 	// 编码订阅用户列表
	pthread_mutex_t     lock;			// 任务锁
	pthread_t 	        task_read_frame;// 任务句柄
	int					task_run;		// 任务运行标志
	int   				encoder_ins_id;	// 使能通道encoder实例
	int 				preview_ins_id;	// 使能通道preview实例
	int 				trans_mask_bit;	// 使能通道传输类型掩码
}ak_ch_vi_atrr_t;

pthread_mutex_t vi_dev_lock=PTHREAD_MUTEX_INITIALIZER;
static int vi_dev_ref_cnt = 0;
static int vi_dev_static_time = 0;
OS_TIMER vi_static_timer;
#define MAX_VI_STATIC_TIME		3000
#define MAX_VI_STATIC_TIME_GAP	(60*1000/25)	// 1 sec

#define AK_VI_DEV_MAX_CH        	3

#define isp_path_t527		        "/mnt/nand1-1/App/share/isp_t527_dvp-H1V0P0-2000-54M-2.conf"
#define isp_path_tw9912		        "/mnt/nand1-1/App/share/isp_tw9912_dvp-H1V0P1-2000-2.conf"
#define isp_path_pr2000		        "/mnt/nand1-1/App/share/isp_pr2000_dvp.conf"
#define isp_path_tp9950		        "/mnt/nand1-1/App/share/isp_tp9950_dvp_960x288.conf"

static ak_ch_vi_atrr_t  ak_ch_vi_atrr[AK_VI_DEV_MAX_CH] =
{
	// channel 0
	{
		.state				= 0,
		.vi_id				= 0,
		.vi_ch				= VIDEO_CHN0,
		.video_w			= 704,	//1280,		
		.video_h			= 520,	//720,
		.lock=PTHREAD_MUTEX_INITIALIZER,
	},
	// channel 1
	{
		.state				= 0,
		.vi_id				= 1,
		.vi_ch				= VIDEO_CHN1,
		.video_w			= 640,		
		.video_h			= 480,
		.lock=PTHREAD_MUTEX_INITIALIZER,
	},
	// channel 2
	{
		.state				= 0,
		.vi_id				= 2,
		.vi_ch				= VIDEO_CHN16,
		.video_w			= 320,		
		.video_h			= 240,
		.lock=PTHREAD_MUTEX_INITIALIZER,
	}
};

void ak_c_vi_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h )
{
	ak_ch_vi_atrr[0].video_w = ch0_w;
	ak_ch_vi_atrr[0].video_h = ch0_h;
	ak_ch_vi_atrr[1].video_w = ch1_w;
	ak_ch_vi_atrr[1].video_h = ch1_h;
	ak_ch_vi_atrr[2].video_w = ch1_w/2;
	ak_ch_vi_atrr[2].video_h = ch1_h/2;
}

/**
 * @fn:		vi_dev_open
 *
 * @brief:	open capture device
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
static int vi_dev_open(void)
{
	int dev_id = VIDEO_DEV0;
    /* 
     * step 0: global value initialize
     */
    int ret = -1;  

	pthread_mutex_lock(&vi_dev_lock);

	#if 0
	if( vi_dev_ref_cnt > 0 )
	{
		vi_dev_ref_cnt++;
		pthread_mutex_unlock(&vi_dev_lock);
		return 0;
	}
	#endif
	if( vi_dev_ref_cnt > 0||vi_dev_static_time>0 )
	{
		if(vi_dev_ref_cnt==0)
		{
			sensor_para_power_down(0);
			ak_vi_enable_dev(dev_id);
		}
		vi_dev_ref_cnt++;
		printf("===============vi_dev_open,vi_dev_ref_cnt[%d]====================\n",vi_dev_ref_cnt);
		vi_dev_static_time=MAX_VI_STATIC_TIME;
		if(strstr(GetCustomerizedName(),"SENSOR_N_P")!=NULL)
		{
			char sensor_n_p[5];
			API_Event_IoServer_InnerRead_All(VIDEO_FORMAT, sensor_n_p);
			sensor_set_pal_ntsc(atoi(sensor_n_p));
		}
		pthread_mutex_unlock(&vi_dev_lock);
		return 0;
	}
    /* open vi flow */

    /* 
     * step 1: open video input device
     */
#if !defined(PID_IX482) && !defined(PID_IXSE)&&!defined(PID_IX47)
	SENSOR_POWER_SET();
	usleep(20*1000);
#endif
	
    ret = ak_vi_open(dev_id);
    if (AK_SUCCESS != ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d open failed\n", dev_id);    
		pthread_mutex_unlock(&vi_dev_lock);
        return ret;
    }

	// open sensor para device, then read sensor type
	sensor_para_open();
	if(strstr(GetCustomerizedName(),"SENSOR_N_P")!=NULL)
	{
		char sensor_n_p[5];
		API_Event_IoServer_InnerRead_All(VIDEO_FORMAT, sensor_n_p);
		sensor_set_pal_ntsc(atoi(sensor_n_p));
	}
    /*
     * step 2: load isp config
     */
	unsigned char* ptr_isp_type = NULL;
	if( sensor_type_is_t527() )
		ptr_isp_type = isp_path_t527;
	else if( sensor_type_is_tw9912() )
		ptr_isp_type = isp_path_tw9912;
	else if( sensor_type_is_pr2000() )
		ptr_isp_type = isp_path_pr2000;
	else if( sensor_type_is_tp9950() )
		ptr_isp_type = isp_path_tp9950;
	ret = ak_vi_load_sensor_cfg(dev_id, ptr_isp_type);
    if (AK_SUCCESS != ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d load isp cfg [%s] failed!\n", dev_id, ptr_isp_type);    
		pthread_mutex_unlock(&vi_dev_lock);
        return ret;
    }

    /* 
     * step 3: get sensor support max resolution
     */
    RECTANGLE_S     res;                //max sensor resolution
    VI_DEV_ATTR     dev_attr;
    memset(&dev_attr, 0, sizeof(VI_DEV_ATTR));
    dev_attr.dev_id         = dev_id;
	#if 1	// test pr2000
	if( sensor_type_is_pr2000() )
	{
       	dev_attr.interf_mode 	= 0;
        dev_attr.data_path 		= 0;
        dev_attr.data_type 		= VI_DATA_TYPE_YUV420SP;
        //dev_attr.data_type 		= VI_DATA_TYPE_YUV420P;
        dev_attr.crop.left 		= 0;
        dev_attr.crop.top 		= 0;
        dev_attr.crop.width 	= 704;
        dev_attr.crop.height 	= 288;
        dev_attr.max_width 		= 704;
        dev_attr.max_height 	= 288;
        dev_attr.sub_max_width 	= 640;
        dev_attr.sub_max_height = 288;
	}
	#endif
    /* get sensor resolution */
    ret = ak_vi_get_sensor_resolution(dev_id, &res);
    if (ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "Can't get dev[%d]resolution\n", dev_id);
        ak_vi_close(dev_id);
	    pthread_mutex_unlock(&vi_dev_lock);	
        return ret;
    } 
    else 
    {
    	#ifdef ENABLE_RGB888_TO_RGB565
        dev_attr.crop.width 	= 704; //res.width;
        #else
		dev_attr.crop.width 	= res.width;
	#endif
        dev_attr.crop.height 	= res.height;
		dev_attr.crop.left		= get_sensor_crop_x();
		dev_attr.crop.top		= get_sensor_crop_y();
        ak_print_normal_ex(MODULE_ID_APP, "Get Sensor Resolution w[%d],h[%d],Cropx[%d],Cropy[%d]\n",dev_attr.crop.width, dev_attr.crop.height, dev_attr.crop.left, dev_attr.crop.top);
    }

	if( sensor_type_is_t527() )
	{
		#ifdef ENABLE_RGB888_TO_RGB565
		dev_attr.max_width      = 704; //res.width;
		#else
		dev_attr.max_width      = res.width;
		#endif
		dev_attr.max_height     = res.height;
		//dev_attr.sub_max_width  = 640;
		//dev_attr.sub_max_height = res.height;
		dev_attr.sub_max_width  = 320;
		dev_attr.sub_max_height = 240;
		dev_attr.frame_rate=25;
		//ak_c_vi_reso_reset(res.width,res.height, 640, 480);
		//ak_encoder_reso_reset(res.width,res.height, 640, 480);
		//ak_preview_reso_reset(res.width,res.height, 640, 480);
		#ifdef ENABLE_RGB888_TO_RGB565
		ak_c_vi_reso_reset(704,res.height, 320, 240);
		ak_encoder_reso_reset(704,res.height, 320, 240); // for APP
		ak_preview_reso_reset(704,res.height, 320, 240);
		#else
		ak_c_vi_reso_reset(res.width,res.height, 320, 240);
		ak_encoder_reso_reset(res.width,res.height, 320, 240); // for APP
		ak_preview_reso_reset(res.width,res.height, 320, 240);
		#endif
	}
	else if( sensor_type_is_tw9912() )
	{
		dev_attr.max_width      = res.width;
		dev_attr.max_height     = res.height;
		dev_attr.sub_max_width  = 320;	//640
		dev_attr.sub_max_height = 240;	//480

		ak_c_vi_reso_reset(res.width,res.height, 320, 240);
		ak_encoder_reso_reset(res.width,res.height, 320, 240); // for APP
		ak_preview_reso_reset(res.width,res.height, 320, 240);		
	}
	else if( sensor_type_is_pr2000() )
	{
		dev_attr.max_width      = 704;  //res.width;
		dev_attr.max_height     = 288;  //res.height;
		dev_attr.sub_max_width  = 320;	//640
		dev_attr.sub_max_height = 240;	//480
		ak_c_vi_reso_reset(704, 288, 320, 240);		// for channel init
		ak_encoder_reso_reset(704, 288, 320, 240);  // for APP
		ak_preview_reso_reset(704, 288, 320, 240);  // for monitor		
	}
	else if( sensor_type_is_tp9950() )
	{
		dev_attr.max_width      = 960;  //res.width;
		dev_attr.max_height     = 288;  //res.height;
		dev_attr.sub_max_width  = 320;	//640
		dev_attr.sub_max_height = 240;	//480
		ak_c_vi_reso_reset(960, 288, 320, 240);		// for channel init
		ak_encoder_reso_reset(960, 288, 320, 240);  // for APP
		ak_preview_reso_reset(960, 288, 320, 240);  // for monitor		
	}

    /* 
     * step 4: set vi device working parameters 
     * default parameters: 25fps, day mode
     */
    ret = ak_vi_set_dev_attr(dev_id, &dev_attr);
    if (ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d set device attribute failed!\n", dev_id);
        ak_vi_close(dev_id);
        pthread_mutex_unlock(&vi_dev_lock);	
        return ret;
    }

	VI_CHN_ATTR chn_attr = {0};
	int index;
	for( index = 0; index < AK_VI_DEV_MAX_CH; index++ )
	{
		ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[index];
		memset(&chn_attr, 0, sizeof(VI_CHN_ATTR));
		chn_attr.chn_id     	= pak_ch_vi_atrr->vi_ch;
		chn_attr.res.width  	= pak_ch_vi_atrr->video_w;
		chn_attr.res.height 	= pak_ch_vi_atrr->video_h;
		chn_attr.frame_depth 	= DEF_FRAME_DEPTH;
		/*disable frame control*/
		#ifdef ENABLE_RGB888_TO_RGB565
		chn_attr.frame_rate = 15;
		#else
		chn_attr.frame_rate = 25;
		#endif
		//chn_attr.frame_rate = 0;
		ret = ak_vi_set_chn_attr(pak_ch_vi_atrr->vi_ch, &chn_attr);
		if (ret) 
		{
			ak_print_error_ex(MODULE_ID_APP, "vi device set channel [%d] attribute failed!\n", pak_ch_vi_atrr->vi_ch);
			return ret;
		}
	}
	ret = ak_vi_enable_dev(dev_id);

	//_dev_ref_cnt=10001;
	vi_dev_ref_cnt++;
	//vi_dev_ref_cnt+=1000;
	vi_dev_static_time=MAX_VI_STATIC_TIME;
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_open[ref=%d]\n",vi_dev_ref_cnt);

	// initial sensor para
	sensor_adjust_initial();

	return ret;
}

/**
 * @fn:		vi_dev_close
 *
 * @brief:	close capture device
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
 #if 0
static int vi_dev_close(void)
{
	int dev_id = VIDEO_DEV0;
    int ret = -1;   
       
    pthread_mutex_lock(&vi_dev_lock);

	if( --vi_dev_ref_cnt > 0)
	{
		ret = 0;
	}
	else
	{
		ak_vi_disable_dev(dev_id);
		ret = ak_vi_close(dev_id);
		// close sensor para device
		sensor_para_close();
		SENSOR_POWER_RESET();
	}
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_close[ref=%d]\n",vi_dev_ref_cnt);

    return ret;
}
 #else
 static int vi_dev_close(void)
{
	int dev_id = VIDEO_DEV0;
    int ret = -1;   
       
    pthread_mutex_lock(&vi_dev_lock);

	if( --vi_dev_ref_cnt > 0)
	{
		ret = 0;
	}
	else if(vi_dev_static_time==0)
	{
		vi_dev_static_time = 0;
		ak_vi_disable_dev(dev_id);
		ret = ak_vi_close(dev_id);
		sensor_para_close();		
	}
	else
	{
		ak_vi_disable_dev(dev_id);
		sensor_para_power_down(1);	
		vi_static_timer_start();
	}
	
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_close[ref=%d]\n",vi_dev_ref_cnt);

    return ret;
}
 #endif
void vi_read_fail_deal(void)
{
	 pthread_mutex_lock(&vi_dev_lock);
	 #if 0
	 if( vi_dev_ref_cnt > 10000)
	 	vi_dev_ref_cnt-=10000;
	 #endif
	 vi_dev_static_time=0;
	 pthread_mutex_unlock(&vi_dev_lock);
	 
}
void vi_static_callback(void)
{
	int dev_id = VIDEO_DEV0;
	 pthread_mutex_lock(&vi_dev_lock);
	if(vi_dev_ref_cnt==0&&vi_dev_static_time>0)
	{
		//if(vi_dev_static_time>0)
		vi_dev_static_time--;
		if( vi_dev_static_time == 0&&vi_dev_ref_cnt==0)
		{
			ak_vi_disable_dev(dev_id);
			ak_vi_close(dev_id);
			
			sensor_para_close();		
			printf("====ak_vi delay closed!!=======================\n");
		}
	}
	
	pthread_mutex_unlock(&vi_dev_lock);

	if(vi_dev_static_time>0)
		OS_RetriggerTimer( &vi_static_timer );
}
int vi_static_timer_start(void)
{
	static unsigned char init_flag=0;
	if(init_flag==0)
	{
		OS_CreateTimer( &vi_static_timer, vi_static_callback, MAX_VI_STATIC_TIME_GAP);
		init_flag=1;
	}
	OS_RetriggerTimer( &vi_static_timer );	
}
/**
 * @fn:		vi_ch_read_frame
 *
 * @brief:	read one frame data
 *
 * @param:	arg - channel attribute ptr
 *
 * @return: none
 */
static void vi_ch_read_frame(void *arg)
{	
	int ret=0;
	struct video_input_frame frame;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr=(ak_ch_vi_atrr_t *)arg;

	//pak_ch_vi_atrr->task_run = 1;
	int read_fail_cnt=0;
	int frame_cnt=0;
	int encode_data_len=0;
	static int vi_error=0;
	//init_filters2("atadenoise=p=3:s=5",pak_ch_vi_atrr->video_w,pak_ch_vi_atrr->video_h,5);
	while(pak_ch_vi_atrr->task_run)
	{
		memset(&frame, 0x00, sizeof(frame));
		ret = ak_vi_get_frame(pak_ch_vi_atrr->vi_ch,&frame);
		if (!ret) 
		{
			read_fail_cnt=0;
			// preview callback
			if( pak_ch_vi_atrr->preview_ins_id != -1  )
			{
				//printf("111111channel[%d],ak_vi_get_frame---------len=%d,%08x\n",pak_ch_vi_atrr->vi_ch,frame.vi_frame.len,frame.phyaddr);
				//if((++frame_cnt%5)==0)
				//if( (++frame_cnt&1) == 0 )
				{
					//filters2_process(frame.vi_frame.data,frame.vi_frame.data,pak_ch_vi_atrr->video_w*pak_ch_vi_atrr->video_h);
					ak_vi_preview_frame( pak_ch_vi_atrr->preview_ins_id, (void*)&frame );
				}
			}
			/* send it to encode */
			if( pak_ch_vi_atrr->encoder_ins_id != -1 )
			{
			#if 1
				if((++frame_cnt%2)&&(pak_ch_vi_atrr->vi_id==0||(pak_ch_vi_atrr->vi_id==1&&IfWanRtpInit())))
				{
					struct video_stream *stream = ak_mem_alloc(MODULE_ID_APP, sizeof(struct video_stream));
					//if(If_trigger_key_frame(pak_ch_vi_atrr->encoder_ins_id))
					if(If_trigger_key_frame_delay(pak_ch_vi_atrr->encoder_ins_id))
					{
						ak_venc_request_idr(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id));
					}
						ret = ak_venc_encode_frame(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id), frame.vi_frame.data, frame.vi_frame.len, frame.mdinfo, stream);
						if (ret)
						{
							/* send to encode failed */
							ak_print_error_ex(MODULE_ID_APP, "send to encode failed\n");
						}
						else 
						{
							if(stream->len > 0)
							{
								#if	!defined(PID_DX470)&&!defined(PID_DX482)
								api_callback_subscribers(&pak_ch_vi_atrr->enc_user_list,stream->data,stream->len);
								#endif
								if(pak_ch_vi_atrr->vi_id==1)
								{
									rtp_sender_send_with_ts_wan(stream->data,stream->len,0,0);
									rtp_sender_send_with_ts_uni(stream->data,stream->len,0,0);
								}
								#if	defined(PID_DX470)||defined(PID_DX482)
								
								if(pak_ch_vi_atrr->vi_id==0)
								{
									API_Recording_mux( stream->data,stream->len, GetDxDs_Show_Win(), 0 );
									encode_data_len+=stream->len;
									#if defined(PID_DX470_V25)
									#else
									if(frame_cnt/2==45)
									{
										printf("!!!!!!frame_cnt/2==45:encode_data_len=%d\n",encode_data_len);
										if(encode_data_len<2200)
										{
											if(vi_error++<2)
											{
												vi_read_fail_deal();
											}
											else
											{
												HardwareRestar();
											}
										}
										else
										{
											vi_error=0;
										}
									}
									#endif
								}
								
								#endif
								
								//ak_venc_release_stream(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id), stream);
							}
							else
								ak_print_notice_ex(MODULE_ID_VENC, "encode err, maybe drop\n");
						//ret = stream->len;		
						ak_venc_release_stream(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id), stream);    
					}
					
					ak_mem_free(stream);
				}
			#endif
			}
			// release frame
			ak_vi_release_frame(pak_ch_vi_atrr->vi_ch, &frame);
			
		}
		else
		{
			// lzh_20230717_s
			if(++read_fail_cnt==3)
			//if(++read_fail_cnt==10)
			{
				printf("get frame failed!,cnt(%d)\n",read_fail_cnt);
				read_fail_cnt = 0;
			// lzh_20230717_e
				if(vi_error++<2)
				{
					vi_read_fail_deal();
				}
				else
				{
					HardwareRestar();
				}
			}
		    /* 
		     *	If getting too fast, it will have no data,
		     *	just take breath.
		     */
		    // ak_print_normal_ex(MODULE_ID_APP, "get frame failed!\n");
		    ak_sleep_ms(10);
		}
	}
	//free_filters2();
}

/**
 * @fn:		vi_ch_open
 *
 * @brief:	open one channel
 *
 * @param:	pak_ch_vi_atrr - channel attribute ptr
 *
 * @return: -1/err, 0/ok
 */
int malloc_ch0_yuv2rgb_buffer(void);
int free_ch0_yuv2rgb_buffer(void);

static int vi_ch_open(ak_ch_vi_atrr_t *pak_ch_vi_atrr)
{
	int ret;

	pak_ch_vi_atrr->task_run = 0;
	#ifdef ENABLE_RGB888_TO_RGB565
	if( pak_ch_vi_atrr->vi_ch == VIDEO_CHN0 ) malloc_ch0_yuv2rgb_buffer();
	#endif

	ak_vi_enable_chn(pak_ch_vi_atrr->vi_ch);	

	pthread_attr_t 	attr_thread;
	pthread_attr_init( &attr_thread );
	pak_ch_vi_atrr->task_run = 1;
	printf("!!!!!!!!!!!!!pak_ch_vi_atrr->dev_id=%d\n",pak_ch_vi_atrr->dev_id);
	if( pthread_create(&pak_ch_vi_atrr->task_read_frame,&attr_thread,(void*)vi_ch_read_frame, pak_ch_vi_atrr) != 0 )
	{
		printf( "Create task_H264_enc pthread error! \n" );
		return 0;
	}
	else
		return -1;
}

/**
 * @fn:		vi_ch_close
 *
 * @brief:	close one channel
 *
 * @param:	pak_ch_vi_atrr - channel attribute ptr
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_close(ak_ch_vi_atrr_t *pak_ch_vi_atrr)
{	
	if( pak_ch_vi_atrr->task_run )
	{
		pak_ch_vi_atrr->task_run = 0;
		pthread_join(pak_ch_vi_atrr->task_read_frame,NULL);
	}
	ak_vi_disable_chn(pak_ch_vi_atrr->vi_ch);
	#ifdef ENABLE_RGB888_TO_RGB565
	if( pak_ch_vi_atrr->vi_ch == VIDEO_CHN0 ) free_ch0_yuv2rgb_buffer();
	#endif
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @fn:		api_ak_vi_ch_close
 *
 * @brief:	close one channel
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_close( int channel )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr=&ak_ch_vi_atrr[channel];
	
	pthread_mutex_lock(&pak_ch_vi_atrr->lock);

	if(pak_ch_vi_atrr->state)
	{
		vi_ch_close(pak_ch_vi_atrr);
		api_vi_ch_enc_close(channel);
		api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
		vi_dev_close();
		pak_ch_vi_atrr->state = 0;
		printf("---------api_ak_vi_ch_close[%d] ok--\n",channel);
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return 0;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return -1;
	}
}

/**
 * @fn:		api_ak_vi_ch_open
 *
 * @brief:	open one channel
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_open( int channel )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	pthread_mutex_lock(&pak_ch_vi_atrr->lock);

	if( !pak_ch_vi_atrr->state )
	{
		if( vi_dev_open() == 0 )
		{
			printf("---------api_ak_vi_ch_open[%d] ok--\n",channel);
			INIT_LIST_HEAD(&pak_ch_vi_atrr->enc_user_list);
			pak_ch_vi_atrr->enc_user_token	= 0;
			pak_ch_vi_atrr->encoder_ins_id	= -1;
			pak_ch_vi_atrr->preview_ins_id	= -1;
			pak_ch_vi_atrr->trans_mask_bit	= -1;
			vi_ch_open(pak_ch_vi_atrr);	
			pak_ch_vi_atrr->state = 1;
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
			return 0;
		}
		else
			return -1;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return -1;
	}
}

/**
 * @fn:		api_ak_vi_ch_is_openned
 *
 * @brief:	check one channel is openned
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: 0/closed, 1/openned
 */
int api_ak_vi_ch_is_openned( int channel )
{
	int status;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&pak_ch_vi_atrr->lock);
	status = pak_ch_vi_atrr->state;
	pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
	return status;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @fn:		api_get_channel_from_resolution
 *
 * @brief:	get one channel index according resolution
 *
 * @param:	reso_w	- resolution width
 * @param:	reso_h	- resolution height
 *
 * @return: -1/err, 0-2/channel index
 */
int api_get_channel_from_resolution( int reso_w, int reso_h )
{
	int i, res_w,res_h;

	printf("input resolution: width=%d, height=%d\n",reso_w,reso_h);

	for( i = 0; i < AK_VI_CH_MAX; i++ )
	{
		api_ak_vi_get_channel_resolution(i, &res_w, &res_h );
		if( (reso_w == res_w) && (reso_h == res_h) )
			break;
	}
	if( i == AK_VI_CH_MAX )
	{
		printf("api_get_channel_from_resolution,NOT match!!\n");
		return -1;
	}
	else
	{
		printf("api_get_channel_from_resolution,match channel=%d\n",i);
		return i;
	}
}

/**
 * @fn:		api_ak_vi_set_channel_resolution
 *
 * @brief:	set one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width
 * @param:	res_h   - resolution height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_resolution(int channel, int res_w, int res_h )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	if( channel == 0 )		
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN0;
	else if( channel == 1 )
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN1;
	else
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN16;
	ak_ch_vi_atrr[channel].vi_id 	= channel;
	ak_ch_vi_atrr[channel].video_w	= res_w;
	ak_ch_vi_atrr[channel].video_h	= res_h;	
	return 0;
}

/**
 * @fn:		api_ak_vi_get_channel_resolution
 *
 * @brief:	get one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width ptr
 * @param:	res_h   - resolution height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_resolution(int channel, int* res_w, int* res_h )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	*res_w = ak_ch_vi_atrr[channel].video_w;
	*res_h = ak_ch_vi_atrr[channel].video_h;
	return 0;
}

/**
 * @fn:		api_ak_vi_set_channel_encoder
 *
 * @brief:	set one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps
 * @param:	enc_type- encoder type
 * @param:	profile - profile
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_encoder(int channel, int fps, int enc_type, int profile )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	if( ak_ch_vi_atrr[channel].encoder_ins_id != -1 )
	{
		ak_ch_vi_atrr[channel].vi_id = channel;
		api_vi_ch_enc_set(channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h,fps,enc_type,profile);
		return 0;	
	}
	else
		return -1;
}

/**
 * @fn:		api_ak_vi_get_channel_encoder
 *
 * @brief:	get one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps ptr
 * @param:	enc_type- encoder type ptr
 * @param:	profile - profile ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_encoder(int channel, int* fps, int* enc_type, int* profile )
{
	if( ak_ch_vi_atrr[channel].encoder_ins_id != -1 )
	{
		api_vi_ch_enc_get(channel,NULL,NULL,fps,enc_type,profile);
		return 0;	
	}
	else
		return -1;
}

/**
 * @fn:		api_ak_vi_set_preview
 *
 * @brief:	set one channel preview paras
 *
 * @param:	win		- win index (0-2)
 * @param:	dispx	- display start x
 * @param:	dispy	- display start y
 * @param:	dispw 	- display width
 * @param:	disph 	- display height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_preview(int win, int dispx, int dispy, int dispw, int disph )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	ak_vi_preview_set_output(win, dispx, dispy, dispw, disph);
	return 0;	
}

/**
 * @fn:		api_ak_vi_get_preview
 *
 * @brief:	get one channel preview paras
 *
 * @param:	channel	- channel index (0-2)
 * @param:	dispx	- display start x ptr
 * @param:	dispy	- display start y ptr
 * @param:	dispw 	- display width ptr
 * @param:	disph 	- display height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_preview(int win, int* dispx, int* dispy, int* dispw, int* disph )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	ak_vi_preview_get_output(win, dispx, dispy, dispw, disph);
	return 0;
}

/**
 * @fn:		api_ak_vi_ch_stream_encode
 *
 * @brief:	open one channel with encoder
 *
 * @param:	channel	- channel index (0-2)
 * @param:	enable	- encoder enable
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_encode(int channel, int enable )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( enable )
	{
		if( !api_ak_vi_ch_is_openned(channel) )
		{
			PrintCurrentTime(7777777);
			if( api_ak_vi_ch_open(channel) == -1 )
				return -1;
			PrintCurrentTime(888888);
		}
		if( api_ak_vi_ch_is_openned(channel) )
		{
			if( pak_ch_vi_atrr->encoder_ins_id == -1 )
			{
				
				if(api_vi_ch_enc_open(channel)==0)
				{
					pak_ch_vi_atrr->encoder_ins_id 	= channel;
					PrintCurrentTime(99999);
				}
			}
		}		
	}
	else
	{
		if( api_ak_vi_ch_is_openned(channel) )
		{
			if( pak_ch_vi_atrr->encoder_ins_id != -1 )
			{
				pak_ch_vi_atrr->encoder_ins_id 	= -1;
				if( pak_ch_vi_atrr->preview_ins_id == -1 )
				{
					api_ak_vi_ch_close(channel);
				}
				
				//pak_ch_vi_atrr->encoder_ins_id 	= -1;
				
			}
		}
	}
	return 0;
}

/**
 * @fn:		api_ak_vi_ch_stream_preview_on
 *
 * @brief:	open one channel with preview win
 *
 * @param:	channel	- channel index (0-2)
 * @param:	win		- preview win
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_preview_on( int channel, int win )
{
	int win_ch,win_state;
	if( channel >= AK_VI_CH_MAX || win >= MAX_PREVIEW_WINS )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( !api_ak_vi_ch_is_openned(channel) )
	{
		if( api_ak_vi_ch_open(channel) == -1 )
			return -1;
	}
	if( api_ak_vi_ch_is_openned(channel) )
	{
		win_state = ak_vi_preview_get_state(win);
		if( win_state )
		{
			ak_vi_preview_get_input(win, &win_ch, NULL, NULL);
			if( win_ch != channel )	
			{
				pak_ch_vi_atrr->preview_ins_id 	= -1;
				ak_vi_preview_stop(win);
				ak_vi_preview_set_input(win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
				ak_vi_preview_start(win, channel);
			}
		}
		else
		{
			ak_vi_preview_set_input(win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
			ak_vi_preview_start(win, channel);
		}
		pak_ch_vi_atrr->preview_ins_id 	= win;
	}
	return -1;
}

/**
 * @fn:		api_ak_vi_ch_stream_preview_off
 *
 * @brief:	close one channel
 *
 * @param:	channel	- channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_preview_off( int channel )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id != -1 )
		{
			ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			pak_ch_vi_atrr->preview_ins_id 	= -1;
			if( pak_ch_vi_atrr->encoder_ins_id == -1 )
				api_ak_vi_ch_close(channel);
		}
	}
	#ifdef ENABLE_RGB888_TO_RGB565
	// lzh_20230717_s
	usleep(200*1000);	// sleep awhile
	// lzh_20230717_e
	#endif
	return 0;
}
int api_ak_vi_ch_stream_preview_hide(int channel)
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id != -1 )
		{
			ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			pak_ch_vi_atrr->preview_ins_id 	= -1;
		}
	}
	return 0;
}
int api_ak_vi_ch_stream_preview_change_win(int channel,int new_win)
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id != -1 )
		{
			ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			pak_ch_vi_atrr->preview_ins_id 	= -1;
			
		}
		ak_vi_preview_set_input(new_win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
		ak_vi_preview_start(new_win, channel);
		pak_ch_vi_atrr->preview_ins_id 	= new_win;
	}
	return 0;
}
/**
 * @fn:		api_ak_vi_trans_subscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel				- channel index (0-2)
 * @param:	subcribe_type		- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_callback	- subscribed encoded stream process
 *
 * @return: -1/err, x/subscriber ok token
 */
int api_ak_vi_trans_subscriber(int channel, int subcribe_type, subscribe_callback_t subscribe_callback )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	api_ak_vi_ch_stream_encode(channel, 1);

	if( api_ak_vi_ch_is_openned(channel) )
	{
		pak_ch_vi_atrr->enc_user_token++;
		pak_ch_vi_atrr->trans_mask_bit |= (1<<subcribe_type);
		return api_add_one_subscriber(&pak_ch_vi_atrr->enc_user_list, pak_ch_vi_atrr->enc_user_token, subcribe_type, subscribe_callback, channel );
	}
	else
		return -1;
}

/**
 * @fn:		api_ak_vi_trans_desubscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel			- channel index (0-2)
  * @param:	subcribe_type	- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_token	- subscriber got token
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_trans_desubscriber(int channel, int subcribe_type, int subscribe_token )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	if( api_del_one_subscriber(&pak_ch_vi_atrr->enc_user_list, subscribe_token) == 0 )
	{
		pak_ch_vi_atrr->trans_mask_bit &= (~(1<<subcribe_type));
		if( pak_ch_vi_atrr->trans_mask_bit == 0 )
		{
			api_ak_vi_ch_stream_encode(channel,0);
		}
		return 0;
	}
	else
		return -1;
}

cJSON *GetViState(int ins)
{
	cJSON *state;
	if(ins>=3)
		return NULL;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(ak_ch_vi_atrr[ins].state == 0)
	{
		cJSON_AddStringToObject(state,"Vi_State","IDLE");
	}
	else 
	{
		cJSON_AddStringToObject(state,"Vi_State","RUNNING");
		
		cJSON_AddNumberToObject(state,"Video_w",ak_ch_vi_atrr[ins].video_w);
		cJSON_AddNumberToObject(state,"Video_h",ak_ch_vi_atrr[ins].video_h);

		if(ak_ch_vi_atrr[ins].preview_ins_id!=-1)
			cJSON_AddStringToObject(state,"Preview","Enable");
		else
			cJSON_AddStringToObject(state,"Preview","Disable");

		if(ak_ch_vi_atrr[ins].encoder_ins_id!=-1)
			cJSON_AddStringToObject(state,"Encoder","Enable");
		else
			cJSON_AddStringToObject(state,"Encoder","Disable");

		if( ak_ch_vi_atrr[ins].trans_mask_bit )
			cJSON_AddStringToObject(state,"Trans","Enable");
		else
			cJSON_AddStringToObject(state,"Trans","Disable");

		
			
		
	}
	
	
	return state;
}

int IfStartEncode(int ins)
{
	if(ak_ch_vi_atrr[ins].encoder_ins_id!=-1)
		return 1;
	return 0;
}
#if 1
static int trigger_key_frame_flag[3]={0};
void trigger_send_key_frame_ak(int ch)
{
	if(ch<3)
		trigger_key_frame_flag[ch]=1;
}

int If_trigger_key_frame(int ch)
{
	if(ch<3)
	{
		if(trigger_key_frame_flag[ch]==1)
		{
			trigger_key_frame_flag[ch]=0;
			return 1;
		}
	}
	return 0;
}
//#else
static int trigger_key_frame_flag2[3]={0};
void trigger_send_key_frame_delay(int ch,int delay)
{
	if(ch<3)
		trigger_key_frame_flag2[ch]=delay;
}

int If_trigger_key_frame_delay(int ch)
{
	if(ch<3)
	{
		
		if(trigger_key_frame_flag2[ch]>0)
		{
			if(--trigger_key_frame_flag2[ch]==0)
				return 1;
		}
	}
	return 0;
}

#endif
