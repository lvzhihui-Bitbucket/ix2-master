

#define MIN(a,b) (((a)<(b))?(a):(b))


//***************************************
/* Dither Tresshold for Red Channel */
static const unsigned char dither_tresshold_r[64] = {
  1, 7, 3, 5, 0, 8, 2, 6,
  7, 1, 5, 3, 8, 0, 6, 2,
  3, 5, 0, 8, 2, 6, 1, 7,
  5, 3, 8, 0, 6, 2, 7, 1,

  0, 8, 2, 6, 1, 7, 3, 5,
  8, 0, 6, 2, 7, 1, 5, 3,
  2, 6, 1, 7, 3, 5, 0, 8,
  6, 2, 7, 1, 5, 3, 8, 0
};

/* Dither Tresshold for Green Channel */
static const unsigned char dither_tresshold_g[64] = {
  1, 3, 2, 2, 3, 1, 2, 2,
  2, 2, 0, 4, 2, 2, 4, 0,
  3, 1, 2, 2, 1, 3, 2, 2,
  2, 2, 4, 0, 2, 2, 0, 4,

  1, 3, 2, 2, 3, 1, 2, 2,
  2, 2, 0, 4, 2, 2, 4, 0,
  3, 1, 2, 2, 1, 3, 2, 2,
  2, 2, 4, 0, 2, 2, 0, 4
};

/* Dither Tresshold for Blue Channel */
static const unsigned char dither_tresshold_b[64] = {
  5, 3, 8, 0, 6, 2, 7, 1,
  3, 5, 0, 8, 2, 6, 1, 7,
  8, 0, 6, 2, 7, 1, 5, 3,
  0, 8, 2, 6, 1, 7, 3, 5,

  6, 2, 7, 1, 5, 3, 8, 0,
  2, 6, 1, 7, 3, 5, 0, 8,
  7, 1, 5, 3, 8, 0, 6, 2,
  1, 7, 3, 5, 0, 8, 2, 6
};

/* Get 16bit closest color */
unsigned char closest_rb(unsigned char c) { 
  return (c >> 3 << 3); /* red & blue */
}
unsigned char closest_g(unsigned char c) {
  return (c >> 2 << 2); /* green */
}

/* RGB565 */
unsigned int RGB16BIT(unsigned char r, unsigned char g, unsigned char b) {
  return ((unsigned int)((r>>3)<<11)|((g>>2)<<5)|(b>>3));
}

/* Dithering by individual subpixel */
unsigned int dither_xy(
  int x, 
  int y, 
  unsigned char b, 
  unsigned char g, 
  unsigned char r
){
  /* Get Tresshold Index */
  unsigned char tresshold_id = ((y & 7) << 3) + (x & 7);

  r = closest_rb(
          MIN(r + dither_tresshold_r[tresshold_id], 0xff)
       );
  g = closest_g(
          MIN(g + dither_tresshold_g[tresshold_id], 0xff)
       );
  b = closest_rb(
          MIN(b + dither_tresshold_b[tresshold_id], 0xff)
       );
  return RGB16BIT(r, g, b);
}

void rgb888_to_rgb565(unsigned char * dest, unsigned char * src, int width, int height){
    int x, y;
    int dest_int;
    unsigned char tresshold_id;
    unsigned char b;
    unsigned char g; 
    unsigned char r;
    int temp = 0;
    int pos = 0;
    int pos1 = 0;

    for (y=0; y<height; y++)
    {
        temp = y * width;

        for (x=0; x<width; x++)
        {
            pos = temp + x;
            pos1 = pos * 3;

            tresshold_id = ((y & 7) << 3) + (x & 7);

            r = MIN(src[pos1+2] + dither_tresshold_r[tresshold_id], 0xff);
            g = MIN(src[pos1+1] + dither_tresshold_g[tresshold_id], 0xff);
            b = MIN(src[pos1] + dither_tresshold_b[tresshold_id], 0xff);
        
            dest[pos<<1] = (b >> 3) | ((g<<3)&0xE0);
            dest[(pos<<1)+1] = (r & 0xF8) | (g >> 5);
        }
    }
}






