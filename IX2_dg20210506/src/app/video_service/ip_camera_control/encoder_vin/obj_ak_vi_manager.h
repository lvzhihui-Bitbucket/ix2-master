
#ifndef _OBJ_AK_VI_MANAGE_H_
#define _OBJ_AK_VI_MANAGE_H_

#include "obj_ak_vi_encoder.h"
#include "obj_ak_vi_preview.h"
#include "obj_ak_vi_userlist.h"

#define     SUBSCRIBER_TYPE_UDP         0
#define     SUBSCRIBER_TYPE_RTP         1
#define     SUBSCRIBER_TYPE_REC         2

void ak_c_vi_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h );

/**
 * @fn:		api_ak_vi_set_channel_resolution
 *
 * @brief:	set one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width
 * @param:	res_h   - resolution height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_resolution(int channel, int res_w, int res_h );

/**
 * @fn:		api_ak_vi_get_channel_resolution
 *
 * @brief:	get one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width ptr
 * @param:	res_h   - resolution height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_resolution(int channel, int* res_w, int* res_h );

/**
 * @fn:		api_get_channel_from_resolution
 *
 * @brief:	get one channel index according resolution
 *
 * @param:	reso_w	- resolution width
 * @param:	reso_h	- resolution height
 *
 * @return: -1/err, 0-2/channel index
 */
int api_get_channel_from_resolution( int reso_w, int reso_h );

// encoder config
/**
 * @fn:		api_ak_vi_set_channel_encoder
 *
 * @brief:	set one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps
 * @param:	enc_type- encoder type
 * @param:	profile - profile
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_encoder(int channel, int fps, int enc_type, int profile );

/**
 * @fn:		api_ak_vi_get_channel_encoder
 *
 * @brief:	get one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps ptr
 * @param:	enc_type- encoder type ptr
 * @param:	profile - profile ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_encoder(int channel, int* fps, int* enc_type, int* profile );

/**
 * @fn:		api_ak_vi_set_preview
 *
 * @brief:	set one channel preview paras
 *
 * @param:	win		- win index (0-2)
 * @param:	dispx	- display start x
 * @param:	dispy	- display start y
 * @param:	dispw 	- display width
 * @param:	disph 	- display height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_preview(int win, int dispx, int dispy, int dispw, int disph );

/**
 * @fn:		api_ak_vi_get_preview
 *
 * @brief:	get one channel preview paras
 *
 * @param:	channel	- channel index (0-2)
 * @param:	dispx	- display start x ptr
 * @param:	dispy	- display start y ptr
 * @param:	dispw 	- display width ptr
 * @param:	disph 	- display height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_preview(int win, int* dispx, int* dispy, int* dispw, int* disph );

/**
 * @fn:		api_ak_vi_ch_stream_preview_on
 *
 * @brief:	open one channel with preview win
 *
 * @param:	channel	- channel index (0-2)
 * @param:	win		- preview win
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_preview_on( int channel, int win );

/**
 * @fn:		api_ak_vi_ch_stream_preview_off
 *
 * @brief:	close one channel
 *
 * @param:	channel	- channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_preview_off( int channel );

/**
 * @fn:		api_ak_vi_trans_subscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel				- channel index (0-2)
 * @param:	subcribe_type		- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_callback	- subscribed encoded stream process
 *
 * @return: -1/err, x/subscriber ok token
 */
int api_ak_vi_trans_subscriber(int channel, int subcribe_type, subscribe_callback_t subscribe_callback );

/**
 * @fn:		api_ak_vi_trans_desubscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel			- channel index (0-2)
  * @param:	subcribe_type	- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_token	- subscriber got token
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_trans_desubscriber(int channel, int subcribe_type, int subscribe_token );


#endif
