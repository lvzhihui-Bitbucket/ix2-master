
#ifndef _REC_TRANS_PROCESS_H
#define _REC_TRANS_PROCESS_H

/**
 * @fn:		api_rec_one_file_start
 *
 * @brief:	start one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 * @param:	period	        - record time
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_start( int channel, int file_type, char* file_name, int period );

/**
 * @fn:		api_rec_one_file_stop
 *
 * @brief:	stop one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_stop( int channel, int file_type, char* file_name );

#endif

