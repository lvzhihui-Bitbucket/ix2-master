
#ifndef _OBJ_AK_VI_CAMERA_H_
#define _OBJ_AK_VI_CAMERA_H_

#define T527_PAGE0   0x20
#define T527_PAGE1   0x21
#define T527_PAGE2   0x22
#define T527_PAGE3   0x23

#define SENSOR_TYPE_T527        1
#define SENSOR_TYPE_TW9912      2
#define SENSOR_TYPE_DC7120      3
#define SENSOR_TYPE_TP9950      4
#define SENSOR_TYPE_PR2000      5

typedef struct
{
   int type;
   int rev;
}SENSOR_CHECK_TYPE;

typedef struct
{
   int page;
   int reg;
   int value;
}SENSOR_IIC_PARA;

typedef struct
{
   int pal0ntsc1;
   int reserved;
}SENSOR_PAL_NTSC;

#define SENSOR_TYPE_GET	_IOR('V', 18,  SENSOR_CHECK_TYPE)
#define SENSOR_PARA_SET	_IOW('V', 19,  SENSOR_IIC_PARA)
#define SENSOR_PARA_GET	_IOWR('V', 20,  SENSOR_IIC_PARA)

#define SENSOR_P0N1_SET _IOW('V', 30,  SENSOR_PAL_NTSC)

int sensor_type_is_t527(void);
int sensor_type_is_tw9912(void);
int get_sensor_crop_x(void);
int get_sensor_crop_y(void);

/**
 * @fn:		sensor_para_open
 *
 * @brief:	开启sensor参数调节设备，开启摄像头设备调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_open(void);

/**
 * @fn:		sensor_para_close
 *
 * @brief:	关闭sensor参数调节设备，关闭摄像头设备时调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_close(void);

int sensor_adjust_initial(void);

int get_sensor_cur_para(int* pbr, int* pcl, int* pco);
int sensor_brightness_adjust(int inc);
int sensor_color_adjust(int inc);
int sensor_contrast_adjust(int inc);

int sensor_para_power_down(int enable);

int sensor_set_pal_ntsc(int pal0ntsc1);

#endif
