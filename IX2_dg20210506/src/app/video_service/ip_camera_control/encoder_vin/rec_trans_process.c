
/**
 * Description: 3路视频录像处理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
*/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <list.h>

#include "obj_ak_vi_userlist.h"
#include "obj_ak_vi_manager.h"
#include "cJSON.h"

#include "video_record.h"

typedef struct
{
	int					file_type;		// h264,h265
    char*               file_name;      // file name
	int					period;		    // save time
	int					time_cnt;		// time counter
} VD_REC_SAVE_T;

typedef struct
{
	struct list_head    one_node;	// 节点指针
	VD_REC_SAVE_T		target;		// 发送目标
	int					channel;	// 视频通道
} VD_REC_SAVE_NODE_t;

typedef struct
{
	int					token;		// 订阅token	
	int                 send_num;  	// 发送总数
	int                 send_sn;  	// 发送序列号
	struct list_head    send_list; 	// 发送列表	
	pthread_mutex_t     lock;		// 队列锁
} VD_REC_SAVE_LIST_T;

static VD_REC_SAVE_LIST_T	one_rec_save_list[3];

/**
 * @fn:		init_rec_save_list
 *
 * @brief:	3路REC存储列表初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
void init_rec_save_list(void)
{
	for( int i = 0; i < 3; i++ )
	{
		pthread_mutex_init( &one_rec_save_list[i].lock, 0);		
		INIT_LIST_HEAD(&one_rec_save_list[i].send_list);
		one_rec_save_list[i].send_num 	= 0;
		one_rec_save_list[i].send_sn 	= 0;
		one_rec_save_list[i].token 		= 0;
	}
}

/**
 * @fn:		rec_check_save_node
 *
 * @brief:	检测指定列表中发送端口的有效性，避免重复添加
 *
 * @param:	plist		    - 保存节点列表
 * @param:	channel		    - 保存通道号
 * @param:	file_type	    - 文件类型
 * @param:	file_name	    - 保存文件名
 *
 * @return: -1/err, 0/ok
 */
static int rec_check_save_node( struct list_head *plist, int channel, int file_type, char* file_name )
{
	int ret = -1;
	struct list_head *p_node;
	list_for_each(p_node,plist)
	{
		VD_REC_SAVE_NODE_t* pnode = list_entry(p_node,VD_REC_SAVE_NODE_t,one_node);
		if( (pnode != NULL) && (pnode->channel == channel) && (pnode->target.file_type == file_type) )
		{
			if( (strcmp( pnode->target.file_name, file_name)== 0) && (pnode->target.file_type == file_type) )
			{
				ret = 0;
				break;
			}
		}
	}
	return ret;
}

/**
 * @fn:		rec_add_one_save_node
 *
 * @brief:	REC存储文件添加
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	file_type	    - file type
 * @param:	file_name	    - file name
 * @param:	period	        - save time
 *
 * @return: -1/err, 0/ok
 */
static int rec_add_one_save_node( struct list_head *plist, int channel, int file_type, char* file_name, int period )
{
	int vd_type;
	VD_REC_SAVE_NODE_t *new_node = (VD_REC_SAVE_NODE_t*)malloc( sizeof(VD_REC_SAVE_NODE_t) );
	if(new_node != NULL)
	{
		INIT_LIST_HEAD(&new_node->one_node);
		new_node->channel 			= channel;
		new_node->target.file_type	= file_type;
		new_node->target.file_name	= malloc(strlen(file_name)+1);
        strcpy(new_node->target.file_name,file_name);
		new_node->target.period      = period;
		new_node->target.time_cnt    = 0;
        // add node process
		list_add_tail(&new_node->one_node, plist); 
        return 0;;
	}
    else
        return -1;
}

/**
 * @fn:		rec_del_one_save_node
 *
 * @brief:	REC存储文件释放
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	file_type	    - file type
 * @param:	file_name	    - file name
 *
 * @return: -1/err, 0/ok
 */
static int rec_del_one_save_node( struct list_head *plist, int channel, int file_type, char* file_name )
{
	int ret = -1;
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_REC_SAVE_NODE_t* pnode = list_entry(p_node,VD_REC_SAVE_NODE_t,one_node);
		if(  (pnode->channel == channel) && (pnode->target.file_type == file_type) )
		{
			if( (strcmp( pnode->target.file_name, file_name)== 0) && (pnode->target.file_type == file_type) )
			{
				free(pnode->target.file_name);
				pnode->target.file_name = NULL;
				// release node process
				list_del_init(&pnode->one_node);
				free(pnode);
				ret = 0;
				break;
			}
		}
	}
	return ret;
}

/**
 * @fn:		rec_del_all_save_node
 *
 * @brief:	REC存储端口释放
 *
 * @param:	plist		    - 发送节点列表
 *
 * @return: -1/err, 0/ok
 */
static int rec_del_all_save_node( struct list_head *plist )
{
	int ret = -1;
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_REC_SAVE_NODE_t* pnode = list_entry(p_node,VD_REC_SAVE_NODE_t,one_node);
		if( pnode != NULL )
		{
            free(pnode->target.file_name);
            pnode->target.file_name = NULL;
			// release node process
			list_del_init(&pnode->one_node);
			free(pnode);
		}
	}
	return 0;
}

/**
 * @fn:		rec_get_one_save_node
 *
 * @brief:	获取指定的存储节点
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	send_addr	    - send ip str
 * @param:	send_port	    - send port
 *
 * @return: NULL/err, VD_REC_SAVE_NODE_t/ok
 */
static VD_REC_SAVE_NODE_t* rec_get_one_save_node( struct list_head *plist, int channel, int file_type, char* file_name )
{
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_REC_SAVE_NODE_t* pnode = list_entry(p_node,VD_REC_SAVE_NODE_t,one_node);
		if(  (pnode->channel == channel) && (pnode->target.file_type == file_type)  )
		{
			if( (strcmp( pnode->target.file_name, file_name)== 0) && (pnode->target.file_type == file_type) )
				return pnode;
		}
	}
	return NULL;
}


/**
 * @fn:		save_rec_enc_data
 *
 * @brief:	recording callback
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	pbuf	        - data pointer
 * @param:	size    	    - data size
 *
 * @return: -1/err, 0/ok
 */
static void save_rec_enc_data( int channel, char *pbuf, int size )
{
	int send_fragment_len;

	VD_REC_SAVE_LIST_T *ptr_rec_save_list = &one_rec_save_list[channel];

	pthread_mutex_lock(&one_rec_save_list[channel].lock);

	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, &ptr_rec_save_list->send_list)
	{
		VD_REC_SAVE_NODE_t* pnode = list_entry(p_node,VD_REC_SAVE_NODE_t,one_node);
		API_Recording_mux( pbuf, size, channel+4, pnode->target.file_type );
		printf("save_rec_enc_data,channel[%d],len=[%d]\n",channel,size);
	}

	pthread_mutex_unlock(&one_rec_save_list[channel].lock);
}

/**
 * @fn:		api_rec_one_file_start
 *
 * @brief:	start one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 * @param:	period	        - record time
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_start( int channel, int file_type, char* file_name, int period )
{
	printf("rec add node: channel=%d,file_type=%d,file_name=%s,period=%d\n",channel,file_type,file_name,period);

	pthread_mutex_lock(&one_rec_save_list[channel].lock);

	if( rec_check_save_node(&one_rec_save_list[channel].send_list, channel, file_type, file_name ) != 0 )
	{
		if( !one_rec_save_list[channel].send_num )
		{
			one_rec_save_list[channel].token = api_ak_vi_trans_subscriber(channel, SUBSCRIBER_TYPE_REC, save_rec_enc_data);
			printf("token=%d\n",one_rec_save_list[channel].token);
		}
		if( rec_add_one_save_node( &one_rec_save_list[channel].send_list, channel, file_type, file_name, period ) == 0 )
		{
			API_RecordStart_mux( channel+4, file_type, 0, file_name, period, 0, NULL );
			one_rec_save_list[channel].send_num++;
			pthread_mutex_unlock(&one_rec_save_list[channel].lock);
			printf("rec add: one_rec_save_list[%d].send_num=%d\n",channel,one_rec_save_list[channel].send_num);
			return one_rec_save_list[channel].send_num;
		}
		else
		{
			printf("rec add err: trans desubscriber,token[%d]\n",one_rec_save_list[channel].token);
			api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_REC, one_rec_save_list[channel].token);
			pthread_mutex_unlock(&one_rec_save_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("!!!!!! rec_check_save_node is repeated !!!\n");
	}
	pthread_mutex_unlock(&one_rec_save_list[channel].lock);	
	return -1;
}

/**
 * @fn:		api_rec_one_file_stop
 *
 * @brief:	stop one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_stop( int channel, int file_type, char* file_name )
{
	int ret;

	printf("rec del node: channel=%d,file_type=%d,file_name=%s\n",channel,file_type,file_name);

	pthread_mutex_lock(&one_rec_save_list[channel].lock);
	if( one_rec_save_list[channel].send_num )
	{
		if( rec_del_one_save_node(&one_rec_save_list[channel].send_list, channel, file_type, file_name ) == 0 )
		{
            API_RecordEnd_mux(channel+4);

			one_rec_save_list[channel].send_num--;
			if( !one_rec_save_list[channel].send_num )
				ret = api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_REC, one_rec_save_list[channel].token);
			else
				ret = 0;
			pthread_mutex_unlock(&one_rec_save_list[channel].lock);
			printf("del: one_rec_save_list[%d].send_num=%d\n",channel,one_rec_save_list[channel].send_num);
			return ret;
		}
		else
		{
			printf("del err: one_rec_save_list[%d].send_num=%d\n",channel,one_rec_save_list[channel].send_num);
			pthread_mutex_unlock(&one_rec_save_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("del none\n");
		pthread_mutex_unlock(&one_rec_save_list[channel].lock);
		return -1;
	}
}

