
/**
 * Description: 3路视频UDP发送类型处理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
*/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <list.h>

//#include "video_subscriber.h"

#include "udp_fragment_opt.h"
#include "udp_trans_process.h"

#include "obj_ak_vi_userlist.h"
#include "obj_ak_vi_manager.h"
#include "cJSON.h"

typedef struct
{
	// udp type
	int					send_mode;			// 0:multicast,11:unicast
	int 				send_addr;			// 发送地址		
	unsigned short		send_port;			// 发送端口
	// udp socket
	int 				sock_fd;			// 发送socket
	struct sockaddr_in 	sock_addr;			// 发送socket addr		
	// udp packet
	udp_fragment_t		send_fragment;		// udp组包结构
	int					send_no;			// udp发送编号
	unsigned int 		last_Timestamp;		// udp时标
} VD_UDP_SEND_T;

typedef struct
{
	struct list_head    one_node;	// 节点指针
	VD_UDP_SEND_T		target;		// 发送目标
	int					channel;	// 视频通道
} VD_UDP_SEND_NODE_t;

typedef struct
{
	int					token;		// 订阅token	
	int                 send_num;  	// 发送总数
	int                 send_sn;  	// 发送序列号
	struct list_head    send_list; 	// 发送列表	
	pthread_mutex_t     lock;		// 队列锁
} VD_UDP_SEND_LIST_T;

static VD_UDP_SEND_LIST_T	one_udp_send_list[3];

extern unsigned long GetTickCount(void);

/**
 * @fn:		open_h264_sock
 *
 * @brief:	UDP发送端口生成
 *
 * @param:	ptr_send	- 发送节点指针
 *
 * @return: -1/err, 0/ok
 */
static int open_h264_sock( VD_UDP_SEND_T* ptr_send )
{
	init_send_fragment_head(&ptr_send->send_fragment);
	ptr_send->last_Timestamp = GetTickCount();	

	bzero( &ptr_send->sock_addr, sizeof(ptr_send->sock_addr) );	
	ptr_send->sock_addr.sin_family 			= AF_INET;
	ptr_send->sock_addr.sin_addr.s_addr 	= ptr_send->send_addr;
	ptr_send->sock_addr.sin_port 			= ptr_send->send_port;
	if( ( ptr_send->sock_fd = socket( PF_INET, SOCK_DGRAM, 0 ) ) == -1 )
	{
		printf("creating socket failed!\n");
		return -1;
	}
	else
	{
		printf("sock fd = %d\n",ptr_send->sock_fd);
		return 0;
	}
}

/**
 * @fn:		close_h264_sock
 *
 * @brief:	UDP发送端口释放
 *
 * @param:	ptr_send	- 发送节点指针
 *
 * @return: -1/err, 0/ok
 */
static int close_h264_sock( VD_UDP_SEND_T* ptr_send )
{
	if(ptr_send->sock_fd != 0 )
	{
		close( ptr_send->sock_fd  );
		ptr_send->sock_fd = 0;
	}
	return 0;
}

/**
 * @fn:		init_udp_send_list
 *
 * @brief:	3路UDP发送列表初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
void init_udp_send_list(void)
{
	for( int i = 0; i < 3; i++ )
	{
		pthread_mutex_init( &one_udp_send_list[i].lock, 0);		
		INIT_LIST_HEAD(&one_udp_send_list[i].send_list);
		one_udp_send_list[i].send_num 	= 0;
		one_udp_send_list[i].send_sn 	= 0;
		one_udp_send_list[i].token 		= 0;
	}
}

/**
 * @fn:		udp_check_send_node
 *
 * @brief:	检测指定列表中发送端口的有效性，避免重复添加
 *
 * @param:	plist		- 发送节点列表
 * @param:	channel		- 发送通道号
 * @param:	send_mode	- 发送类型
 * @param:	send_addr	- 发送地址
 * @param:	send_port	- 发送端口
 *
 * @return: -1/err, 0/ok
 */
static int udp_check_send_node( struct list_head *plist, int channel, int send_mode, int send_addr, unsigned short send_port )
{
	int ret = -1;
	struct list_head *p_node;
	list_for_each(p_node,plist)
	{
		VD_UDP_SEND_NODE_t* pnode = list_entry(p_node,VD_UDP_SEND_NODE_t,one_node);
		if( (pnode != NULL) && pnode->channel == channel && pnode->target.send_mode == send_mode && pnode->target.send_addr == send_addr && pnode->target.send_port == htons(send_port) )
		{
			ret = 0;
			break;
		}
	}
	return ret;
}

/**
 * @fn:		udp_add_one_send_node
 *
 * @brief:	UDP发送端口添加
 *
 * @param:	plist		- 发送节点列表
 * @param:	channel		- 发送通道号
 * @param:	send_mode	- 发送类型
 * @param:	send_addr	- 发送地址
 * @param:	send_port	- 发送端口
 *
 * @return: -1/err, 0/ok
 */
static int udp_add_one_send_node( struct list_head *plist, int channel, int send_mode, int send_addr, unsigned short send_port )
{
	VD_UDP_SEND_NODE_t *new_node = (VD_UDP_SEND_NODE_t*)malloc( sizeof(VD_UDP_SEND_NODE_t) );
	if(new_node != NULL)
	{
		INIT_LIST_HEAD(&new_node->one_node);
		new_node->channel 			= channel;
		new_node->target.send_mode	= send_mode;
		new_node->target.send_addr 	= send_addr;
		new_node->target.send_port 	= htons(send_port);
		open_h264_sock(&new_node->target);
		list_add_tail(&new_node->one_node, plist); 
        return 0;;
	}
    else
        return -1;
}

/**
 * @fn:		udp_del_one_send_node
 *
 * @brief:	UDP发送端口释放
 *
 * @param:	plist		- 发送节点列表
 * @param:	channel		- 发送通道号
 * @param:	send_mode	- 发送类型
 * @param:	send_addr	- 发送地址
 * @param:	send_port	- 发送端口
 *
 * @return: -1/err, 0/ok
 */
static int udp_del_one_send_node( struct list_head *plist, int channel, int send_mode, int send_addr, unsigned short send_port )
{
	int ret = -1;
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_UDP_SEND_NODE_t* pnode = list_entry(p_node,VD_UDP_SEND_NODE_t,one_node);
		if(  (pnode->channel == channel) && (pnode->target.send_mode == send_mode) && (pnode->target.send_addr == send_addr) && (pnode->target.send_port == htons(send_port) ) )
		{
			close_h264_sock(&pnode->target);
			list_del_init(&pnode->one_node);
			free(pnode);
			ret = 0;
			break;
		}
	}
	return ret;
}

/**
 * @fn:		send_udp_enc_data
 *
 * @brief:	UDP发送回调函数
 *
 * @param:	channel		- 发送通道号
 * @param:	pbuf		- 发送数据指针
 * @param:	size		- 发送数据大小
 *
 * @return: none
 */
static void send_udp_enc_data( int channel, char *pbuf, int size )
{
	int send_fragment_len;

	VD_UDP_SEND_LIST_T *ptr_udp_send_list = &one_udp_send_list[channel];

	pthread_mutex_lock(&one_udp_send_list[channel].lock);

	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, &ptr_udp_send_list->send_list)
	{
		VD_UDP_SEND_NODE_t* pnode = list_entry(p_node,VD_UDP_SEND_NODE_t,one_node);
		if( pnode->target.sock_fd )
		{
			pnode->target.last_Timestamp = GetTickCount();
			
			start_send_fragment_head(++ptr_udp_send_list->send_sn, &pnode->target.send_fragment,size);
			
			#if 1
			printf("s[%d]:frame_sn=%d,len=%d,t=%d\n", channel, 			\
						pnode->target.send_fragment.m_head.FrameNo,		\
						pnode->target.send_fragment.m_head.Framelen,	\
						pnode->target.send_fragment.m_head.Timestamp);
			#endif

			while( 1 )
			{
				send_fragment_len = pull_send_fragment_data(&pnode->target.send_fragment,pbuf);
				send_fragment_len += PACK_MARK_LEN;
				send_fragment_len += sizeof(AVPackHead);
				
				if( sendto(pnode->target.sock_fd, (unsigned char*)&pnode->target.send_fragment, send_fragment_len, 0, (struct sockaddr*)&pnode->target.sock_addr,sizeof(pnode->target.sock_addr))	== -1 )
				{
					printf("%s-%d:can not send data from socket! errno:%d,means:%s\n",__FILE__, __LINE__, errno,strerror(errno));
				}

				if( prepare_for_next_fragment(&pnode->target.send_fragment) != 0 )
					break;
			}
		}
	}

	pthread_mutex_unlock(&one_udp_send_list[channel].lock);
}

/**
 * @fn:		api_udp_add_one_send_node
 *
 * @brief:	add one send node, if the channel list have no send node, subscriber one udp token from encoder channel
 *
 * @param:	channel		- channel index (0-2)
 * @param:	send_mode	- send mode
 * @param:	send_addr	- send target ip addr
 * @param:	send_port	- send target ip port
 *
 * @return: -1/err, x/total target
 */
int api_udp_add_one_send_node( int channel, int send_mode, int send_addr, unsigned short send_port )
{
	printf("udp add node: channel=%d,send_mode=%d,send addr=%08x,send_port=%d\n",channel,send_mode,send_addr,send_port);

	pthread_mutex_lock(&one_udp_send_list[channel].lock);

	if( udp_check_send_node(&one_udp_send_list[channel].send_list, channel, send_mode, send_addr, send_port) != 0 )
	{
		if( !one_udp_send_list[channel].send_num )
		{
			one_udp_send_list[channel].token = api_ak_vi_trans_subscriber(channel, SUBSCRIBER_TYPE_UDP, send_udp_enc_data);
			printf("token=%d\n",one_udp_send_list[channel].token);
		}
		if( udp_add_one_send_node( &one_udp_send_list[channel].send_list, channel, send_mode, send_addr, send_port ) == 0 )
		{
			one_udp_send_list[channel].send_num++;
			pthread_mutex_unlock(&one_udp_send_list[channel].lock);
			printf("add: one_udp_send_list[%d].send_num=%d\n",channel,one_udp_send_list[channel].send_num);
			return one_udp_send_list[channel].send_num;
		}
		else
		{
			printf("add err: trans desubscriber,token[%d]\n",one_udp_send_list[channel].token);
			api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_UDP, one_udp_send_list[channel].token);
			pthread_mutex_unlock(&one_udp_send_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("!!!!!! udp_check_send_node is repeated !!!\n");
	}
	pthread_mutex_unlock(&one_udp_send_list[channel].lock);	
	return -1;
}

/**
 * @fn:		api_udp_del_one_send_node
 *
 * @brief:	delete one send node, if the channel list have no send node, desubscriber the udp token from encoder channel
 *
 * @param:	channel		- channel index (0-2)
 * @param:	send_mode	- send mode
 * @param:	send_addr	- send target ip addr
 * @param:	send_port	- send target ip port
 *
 * @return: -1/err, 0/ok
 */
int api_udp_del_one_send_node( int channel, int send_mode, int send_addr, unsigned short send_port )
{
	int ret;

	printf("udp del node: channel=%d,send_mode=%d,send addr=%08x,send_port=%d\n",channel,send_mode,send_addr,send_port);

	pthread_mutex_lock(&one_udp_send_list[channel].lock);
	if( one_udp_send_list[channel].send_num )
	{
		if( udp_del_one_send_node(&one_udp_send_list[channel].send_list, channel, send_mode, send_addr, send_port) == 0 )
		{
			one_udp_send_list[channel].send_num--;
			if( !one_udp_send_list[channel].send_num )
				ret = api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_UDP, one_udp_send_list[channel].token);
			else
				ret = 0;
			pthread_mutex_unlock(&one_udp_send_list[channel].lock);
			printf("del: one_udp_send_list[%d].send_num=%d\n",channel,one_udp_send_list[channel].send_num);
			return ret;
		}
		else
		{
			printf("del err: one_udp_send_list[%d].send_num=%d\n",channel,one_udp_send_list[channel].send_num);
			pthread_mutex_unlock(&one_udp_send_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("del none\n");
		pthread_mutex_unlock(&one_udp_send_list[channel].lock);
		return -1;
	}
}

cJSON *GetUdpTransList(int ins)
{
	cJSON *state,*node;
	struct list_head *p_node;
	int i;
	char addr[20];
	if(ins>=3)
		return NULL;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	pthread_mutex_lock(&one_udp_send_list[ins].lock);
	cJSON_AddNumberToObject(state,"List_Num",one_udp_send_list[ins].send_num);
	if(one_udp_send_list[ins].send_num>0)
	{
		cJSON *array= cJSON_AddArrayToObject(state,"List");
		if(array!=NULL)
		{
			list_for_each(p_node,&one_udp_send_list[ins].send_list)
			{
				VD_UDP_SEND_NODE_t* pnode = list_entry(p_node,VD_UDP_SEND_NODE_t,one_node);

				
				
				node=cJSON_CreateObject();
				if(node!=NULL)
				{
					cJSON_AddStringToObject(node,"Target_Addr",my_inet_ntoa( pnode->target.send_addr,addr));
					cJSON_AddNumberToObject(node,"Target_Port",ntohs(pnode->target.send_port));
					cJSON_AddItemToArray(array,node);
				}
			}
		}
	}
	pthread_mutex_unlock(&one_udp_send_list[ins].lock);
	
	return state;
}

