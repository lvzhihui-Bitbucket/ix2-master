
#include "task_Hal.h"
#include "task_Beeper.h"
//IX2_TEST #include "../task_VideoMenu/task_VideoMenu.h"
#include "task_Led.h"
#include "task_Power.h"
#include "task_debug_sbu.h"

#include "../vdp_uart.h" 
//#include "../task_Stack212/obj_L2Layer.h"

#include "ak_common.h"
#include "ak_log.h"
#include "ak_drv_ts.h"

pthread_t task_Hal;
pthread_t task_Hook;
int hal_fd;
/******************************************************************************
 * @fn      vtk_TaskInit_Hal()
 *
 * @brief   Task hal initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Hal( int priority )
{
	//create task
	if( pthread_create(&task_Hal,NULL,(void*)vtk_TaskProcessEvent_Hal,NULL) != 0 )
	{
		printf( "Create pthread error! \n" );
	}
	else
		printf( "Create vtk_TaskInit_Hal pthread ok! \n" );		
}

void vtk_Task_Hal_Join( void )
{
	//czn_20160628_s
	int reval=0,*repra = 0;
	
	reval = pthread_join( task_Hal, &repra );
	while(1)
	{
		usleep(2000000);
		printf("pthread_join_task_hal return reval=%d,repra=%d\n",reval,(int)repra);
	}
	//czn_20160628_e

}
 OS_TIMER 	sendMessageTimer;


void TimersendMessageTimerCallBack(void)
{
	static unsigned char beepType = 0;
	OS_RetriggerTimer(&sendMessageTimer);
	
	if(beepType++ < 10)
	{
		API_Beep(beepType);
	}
	else
	{
		beepType = 0;
		API_Beep(beepType);
	}
}
void GlobalResourceInit(void)
{

	hal_fd = open("/dev/ds2411",O_RDWR);
	if( hal_fd == -1 )
	{
		hal_fd = open("/dev/minictrl",O_RDWR);
		//printf("error open\n");
		//exit(-1);
	}
}
/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Hal()
 *
 * @brief   Task hal loop routine
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Hal( void )
{
	static KEY_DATA 		keylast={0};

	KEY_DATA 		key;
	HAL_MSG_TYPE	key_msg;
	
#if 1
	unsigned char          app_state;

	//API_LED_POWER_ON();				//lyx 20170731	
	//API_LedDisplay_BootFinish();	//lzh 20171021
	
	app_state = 1;
	api_uart_send_pack(UART_TYPE_N2S_REPOTR_APP_STATUS, &app_state, 1);    //lyx 20170731  通知STM8: APP已启动完成
	usleep(50*1000);
	api_uart_send_pack(UART_TYPE_N2S_REPOTR_APP_STATUS, &app_state, 1);    //lyx 20170731  通知STM8: APP已启动完成
	DateAndTimeInit();
	sleep(3);
	//ExtModule_Initial();
	#if 0	//IX2_TEST
	key_msg.head.msg_target_id	= MSG_ID_survey;
	key_msg.head.msg_source_id	= MSG_ID_hal;
	key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
	key_msg.head.msg_sub_type	= 0;
	key_msg.key.keyType 		= KEY_TYPE_BOOT;
	key_msg.key.keyData 		= 1;
	API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
	#endif
#else
	key_msg.head.msg_target_id	= MSG_ID_survey;
	key_msg.head.msg_source_id	= MSG_ID_hal;
	key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
	key_msg.head.msg_sub_type	= 0;
	key_msg.key.keyType 		= KEY_TYPE_TS;
	key_msg.key.keyStatus 		= TOUCHCLICK;
	key_msg.key.keyData			= 0x03;
	
	gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
	bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);
	API_add_message_to_suvey_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
#endif
#if 1	//IX821TEST
	struct ak_ts_event ts;
    int ret = ak_drv_ts_open();
    if(ret)
    {
        ak_print_error_ex(MODULE_ID_DRV, "ak_drv_ts_open fail.\n");
		printf("=============ak_drv_ts_open er==================\n");
    }
	else
	{
		ak_print_error_ex(MODULE_ID_DRV, "ak_drv_ts_open ok.\n");
		printf("=============ak_drv_ts_open ok==================\n");
	}
#endif

	while( 1 )
	{
		//ak_drv_wdt_feed();
		ret = ak_drv_ts_get_event(&ts, 30); //30);
		//printf("\n[get event:%d,%d]\n",ret,ts.map);
		if(ret == 0) 
		{
			if (ts.map & 1)
			{
				ak_print_normal(MODULE_ID_DRV, "get ts0:[%d , %d].\n", ts.info[0].x, ts.info[0].y); 
				key.keyType = KEY_TYPE_TP;
				if( (ts.info[0].x == -1) && (ts.info[0].y == -1) )
				{
					key.keyStatus = TOUCHCLICK;					
				}
				else
				{
					key.keyStatus = TOUCHPRESS;
					key.TpKey.x = ts.info[0].x;
					key.TpKey.y = ts.info[0].y;			
				}
				#if 1
				if( keylast.keyStatus != key.keyStatus )
				{
					keylast = key;
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key 				= key;
					gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
					//bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
				}
				#endif
				#if 0
				if( keylast.keyStatus != key.keyStatus )
				{
					keylast = key;
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key 				= key;
					gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
				}
				else if( (abs(keylast.TpKey.x-key.TpKey.x)>10) || (abs(keylast.TpKey.y-key.TpKey.y)>10) )	
				{
					key.keyStatus = TOUCHMOVE;
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key 				= key;
					gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) ); 
				}
				#endif
			}
		}
		else
		{
			if(keylast.keyStatus == TOUCHPRESS)
			{
				printf("11111111111\n");
				keylast.keyStatus = TOUCHCLICK;
				key=keylast;
				key_msg.head.msg_target_id	= MSG_ID_survey;
				key_msg.head.msg_source_id	= MSG_ID_hal;
				key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
				key_msg.head.msg_sub_type	= 0;
				key_msg.key 				= key;
				gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
				//bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);
				API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
			}
		}
	}
}

