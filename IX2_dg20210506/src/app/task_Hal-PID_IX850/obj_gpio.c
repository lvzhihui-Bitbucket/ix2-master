
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "obj_gpio.h"

#define GPIO_EXPORT			"/sys/class/gpio/export"
#define GPIO_UNEXPORT		"/sys/class/gpio/unexport"
#define GPIO_DIRECTION		"/sys/class/gpio/gpio%d/direction"
#define GPIO_VALUE			"/sys/class/gpio/gpio%d/value"


//static int gpio_output_pins[3]	={AMPMUTE_GPIO_PIN1,AMPMUTE_GPIO_PIN2,LCD_PWM_GPIO_PIN};
static int gpio_output_pins[1]	={AMPMUTE_GPIO_PIN};


void gpio_initial(void)
{
	char temp[201];	
	FILE *p=NULL;
	int i;

	// add output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}

	// set output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_output_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"out");
		fclose(p);	
	}

}

void gpio_deinitial(void)
{
	FILE *p=NULL;
	int i;
	// del output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_UNEXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}
}

int gpio_output(int pin, int hi)
{
#if 1
	char temp[201];	
	FILE *p=NULL;
	// set pin value
	snprintf(temp, 200, GPIO_VALUE, pin);
	printf("%s\n",temp);
	p = fopen(temp,"w");
	//if(p!=NULL)
		{
	fprintf(p,"%d",hi);
	fflush(p);
	fclose(p);
		}
#endif	
	return 0;
}

int gpio_input(int pin)
{
	int ret;
	char temp[201];	
	FILE *p=NULL;
	// read pin value
	snprintf(temp, 200, GPIO_VALUE, pin);
	p = fopen(temp,"r");
	// seek to start
	fseek(p , 0 , 0);
	fread(temp , 1, 1 ,p);
	temp[1]=0;
	ret = atoi(temp);		
	fclose(p);
	
	return ret;
}

