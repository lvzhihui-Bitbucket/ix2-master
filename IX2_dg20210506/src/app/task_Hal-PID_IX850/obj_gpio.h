
#ifndef _OBJ_GPIO_H_
#define _OBJ_GPIO_H_

#define AMPMUTE_GPIO_PIN1	76
#define AMPMUTE_GPIO_PIN2	41
#define LCD_PWM_GPIO_PIN	86
#define AMPMUTE_GPIO_PIN	33

#define AMPMUTE_SET1() 		gpio_output(AMPMUTE_GPIO_PIN1, 1)
#define AMPMUTE_RESET1() 	gpio_output(AMPMUTE_GPIO_PIN1, 0)
#define AMPMUTE_SET2() 		gpio_output(AMPMUTE_GPIO_PIN2, 1)
#define AMPMUTE_RESET2() 	gpio_output(AMPMUTE_GPIO_PIN2, 0)

#define LCD_PWM_SET() 		gpio_output(LCD_PWM_GPIO_PIN, 1)
#define LCD_PWM_RESET() 	gpio_output(LCD_PWM_GPIO_PIN, 0)

#define AMPMUTE_SET() 		gpio_output(AMPMUTE_GPIO_PIN, 1)
#define AMPMUTE_RESET() 	gpio_output(AMPMUTE_GPIO_PIN, 0)

void gpio_initial(void);
void gpio_deinitial(void);
int gpio_output(int pin, int hi);
int gpio_input(int pin);

#endif
