/**
  ******************************************************************************
  * @file    obj_ShellCmdTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Shell.h"
#include "obj_TableSurver.h"

int ShellCmd_Table(cJSON *cmd)
{
	int count = 0;
	char* ctrlStr = GetShellCmdInputString(cmd, 1);
	char* fileStr = GetShellCmdInputString(cmd, 2);
	cJSON* table = ShellGetJsonFromFile(fileStr);
	cJSON* Where = GetShellCmdInputJson(cmd, 3);

	if(ctrlStr == NULL || fileStr == NULL)
	{
		ShellCmdPrintf("Input error!!\n");
	}
	else if(!strcmp(ctrlStr, "count"))
	{
		ShellCmdPrintf("%d\n", API_TB_Count(table, Where));
	}
	else if(!strcmp(ctrlStr, "add"))
	{
		count = API_TB_Add(table, Where);
		ShellCmdPrintf("%d\n", count);
		if(count)
		{
			SetJsonToFile(fileStr, table);
		}
	}
	else if(!strcmp(ctrlStr, "del"))
	{
		count = API_TB_Delete(table, Where);
		ShellCmdPrintf("%d\n", count);
		if(count)
		{
			SetJsonToFile(fileStr, table);
		}
	}
	else if(!strcmp(ctrlStr, "update"))
	{
		cJSON* data = GetShellCmdInputJson(cmd, 4);
		count = API_TB_Update(table, Where, data);
		ShellCmdPrintf("%d\n", count);
		if(count)
		{
			SetJsonToFile(fileStr, table);
		}
		cJSON_Delete(data);
	}
	else if(!strcmp(ctrlStr, "select"))
	{
		cJSON* view = GetShellCmdInputJson(cmd, 4);
		if(view == NULL)
		{
			view = cJSON_CreateArray();
		}

		if(API_TB_SelectBySort(table, view, Where, 0) > 0)
		{
			ShellCmdPrintJson(view);
		}
		cJSON_Delete(view);
	}
	else if(!strcmp(ctrlStr, "load"))
	{
		cJSON* loadTable = API_TB_LoadFromFile(fileStr, NULL, NULL, NULL, NULL,NULL);
		ShellCmdPrintJson(loadTable);
		cJSON_Delete(loadTable);
	}
	else if(!strcmp(ctrlStr, "drop"))
	{
		API_TB_DropTemp(table);
		ShellCmdPrintJson(table);
		SetJsonToFile(fileStr, table);
	}

	cJSON_Delete(table);
	cJSON_Delete(Where);

	return 1;
}

int ShellCmd_RemoteTable(cJSON *cmd)
{
	int count = 0;
	int ip = inet_addr(GetShellCmdInputString(cmd, 1));
	char* name = GetShellCmdInputString(cmd, 2);
	char* ctrlStr = GetShellCmdInputString(cmd, 3);
	cJSON* Where = GetShellCmdInputJson(cmd, 4);

	if(ctrlStr == NULL || name == NULL || ip == -1)
	{
		ShellCmdPrintf("Input error!!\n");
	}
	else if(!strcmp(ctrlStr, "count"))
	{
		ShellCmdPrintf("%d\n", API_RemoteTableCount(ip, name, Where));
	}
	else if(!strcmp(ctrlStr, "add"))
	{
		count = API_RemoteTableAdd(ip, name, Where);
		ShellCmdPrintf("%d\n", count);
	}
	else if(!strcmp(ctrlStr, "del"))
	{
		count = API_RemoteTableDelete(ip, name, Where);
		ShellCmdPrintf("%d\n", count);
	}
	else if(!strcmp(ctrlStr, "update"))
	{
		cJSON* data = GetShellCmdInputJson(cmd, 5);
		count = API_RemoteTableUpdate(ip, name, Where, data);
		ShellCmdPrintf("%d\n", count);
		cJSON_Delete(data);
	}
	else if(!strcmp(ctrlStr, "select"))
	{
		cJSON* view = GetShellCmdInputJson(cmd, 5);
		if(view == NULL)
		{
			view = cJSON_CreateArray();
		}

		if(API_RemoteTableSelect(ip, name, view, Where, 0) > 0)
		{
			ShellCmdPrintJson(view);
		}
		cJSON_Delete(view);
	}
	cJSON_Delete(Where);

	return 1;
}

int ShellCmd_LocalTable(cJSON *cmd)
{
	int count = 0;
	char* name = GetShellCmdInputString(cmd, 1);
	char* ctrlStr = GetShellCmdInputString(cmd, 2);
	cJSON* Where = GetShellCmdInputJson(cmd, 3);

	if(ctrlStr == NULL || name == NULL)
	{
		ShellCmdPrintf("Input error!!\n");
	}
	else if(!strcmp(ctrlStr, "reload"))
	{
		ShellCmdPrintf("%s\n", API_TB_ReloadByName(name, NULL) ? "Success" : "Error");
	}
	else if(!strcmp(ctrlStr, "count"))
	{
		ShellCmdPrintf("%d\n", API_TB_CountByName(name, Where));
	}
	else if(!strcmp(ctrlStr, "add"))
	{
		count = API_TB_AddByName(name, Where);
		ShellCmdPrintf("%d\n", count);
	}
	else if(!strcmp(ctrlStr, "del"))
	{
		count = API_TB_DeleteByName(name, Where);
		ShellCmdPrintf("%d\n", count);
	}
	else if(!strcmp(ctrlStr, "update"))
	{
		cJSON* data = GetShellCmdInputJson(cmd, 4);
		count = API_TB_UpdateByName(name, Where, data);
		ShellCmdPrintf("%d\n", count);
		cJSON_Delete(data);
	}
	else if(!strcmp(ctrlStr, "select"))
	{
		cJSON* view = GetShellCmdInputJson(cmd, 4);
		if(view == NULL)
		{
			view = cJSON_CreateArray();
		}

		if(API_TB_SelectBySortByName(name, view, Where, 0) > 0)
		{
			ShellCmdPrintJson(view);
		}
		cJSON_Delete(view);
	}
	else if (!strcmp(ctrlStr, "rule"))
	{
		ShellCmdPrintJson(API_TB_ReplaceRuleByName(name, Where));
	}
	
	cJSON_Delete(Where);

	return 1;
}



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

