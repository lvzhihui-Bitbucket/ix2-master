/**
  ******************************************************************************
  * @file    obj_ShellCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Shell.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include <regex.h>

#ifdef true
#undef true
#endif
#define true ((cJSON_bool)1)

#ifdef false
#undef false
#endif
#define false ((cJSON_bool)0)


int ShellCmd_PublicInfo(cJSON * input)
{
	cJSON * value = NULL;
	char *command;
	char *paraName;

	command = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	if(!strcmp(command, "r"))
	{
		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	else if(!strcmp(command, "w"))
	{
		value = GetShellCmdInputJson(input, 3);
		API_PublicInfo_Write(paraName, value);
		cJSON_Delete(value);

		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	else
	{
		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	return 1;
}

int ShellCmd_Pwd(cJSON *cmd)
{
	char output[SHELL_OUTPUT_LEN];

	getcwd(output, SHELL_OUTPUT_LEN);
	ShellCmdPrintf("%s\n", output);
	return 1;
}

int ShellCmd_Cd(cJSON *cmd)
{
	char* path;
	path = GetShellCmdInputString(cmd, 1);
	path = (path ? path : "/");
	if(chdir(path) != 0)
	{
		ShellCmdPrintf("can't cd to %s: No such file or directory\n", path);
	}

	return 1;
}

static int ShellCmd_Os(cJSON *cmd)
{
	int ret = 0;
	char output[SHELL_OUTPUT_LEN+1] = {0};
	FILE* stream[2];
	int pid;
	int readLen, readIndex;

	int timeOut;

	cJSON *cmdtemp = cJSON_Duplicate(cmd, 1);

	if(!strcmp(GetShellCmdInputString(cmd, 1), "-t"))
	{
		timeOut = atoi(GetShellCmdInputString(cmd, 2));
		if(timeOut == 0 || timeOut > 120)
		{
			timeOut = 120;
		}
		cJSON_DeleteItemFromArray(cmdtemp, 2);
		cJSON_DeleteItemFromArray(cmdtemp, 1);
		cJSON_DeleteItemFromArray(cmdtemp, 0);
	}
	else
	{
		cJSON_DeleteItemFromArray(cmdtemp, 0);
		timeOut = 1;
	}
	
	pid = mypopen2("r", stream, cmdtemp);
	while(pid > 0)
	{
		readLen = myfread(stream, output, SHELL_OUTPUT_LEN, timeOut);
		if(readLen == -1)
		{
			ShellCmdPrintf("myfread error!\n");
			break;
		}
		else if(readLen >= 0)
		{
			output[readLen] = 0;
			ShellCmdPrintf(output);
			ret = 1;
			if(readLen < SHELL_OUTPUT_LEN)
			{
				break;
			}
		}
	}

	mypclose(stream, &pid);


	cJSON_Delete(cmdtemp);
	
	return ret;
}

static void JsonMultilevelConversion(const cJSON* input, cJSON* output)
{
    cJSON * current_element = NULL;

    if(input == NULL || input->child == NULL || output == NULL)
    {
        return;
    }

	current_element = input->child;
	while (current_element)
	{
		if(cJSON_IsObject(current_element))
		{
			JsonMultilevelConversion(current_element, output);
		}
		else if(current_element->string != NULL)
		{
			cJSON_AddItemToObject(output, current_element->string, cJSON_Duplicate(current_element, 1));
		}
		current_element = current_element->next;
	}
}

static int ShellCmd_Event(cJSON *cmd)
{
	char* eventStr = GetShellCmdInputString(cmd, 1);
	cJSON *event;

	if(eventStr == NULL)
	{
		ShellCmdPrintf("Please input event data.\n");
		return 0;
	}

	event = GetShellCmdInputJson(cmd, 1);
	if(event)
	{
		API_Event(eventStr);
		ShellCmdPrintJson(event);
		cJSON_Delete(event);
	}
	
	return 1;
}

static int ShellCmd_RegCheck(cJSON *cmd)
{
	int errorCode;
	int result = 0;
	regex_t regex;
	regmatch_t pm;
	char ebuf[128];

	char * pattern = GetShellCmdInputString(cmd, 1);
	char * checkString = GetShellCmdInputString(cmd, 2);

	ShellCmdPrintf("pattern=%s,checkString=%s\n", pattern, checkString);

	/* 编译正则表达式*/
	errorCode = regcomp(&regex, pattern, REG_EXTENDED|REG_NEWLINE);
	if (errorCode == 0)
	{
		/* 对每一行应用正则表达式进行匹配 */
		errorCode = regexec(&regex, checkString, 1, &pm, 0);
		if (errorCode == 0) 
		{
			/* 输出处理结果 */
			ShellCmdPrintf("pm.rm_so = %d, pm.rm_eo = %d\n", pm.rm_so, pm.rm_eo);
			result = 1;
		}
	}

	if(result == 0)
	{
		regerror(errorCode, &regex, ebuf, sizeof(ebuf));
		ShellCmdPrintf("regerror:%s\n", ebuf);
	}

	/* 释放正则表达式 */
	regfree(&regex);

	return result;
}

static int ShellCmd_Reboot(cJSON *cmd)
{
	char* dataString = GetShellCmdInputString(cmd, 1);
	ShellCmdPrintf("Shell:request reboot\n");
	HardwareRestar();
	return 1;
}

static int ShellCmd_Update(cJSON *cmd)
{
	char* ctrlStr = GetShellCmdInputString(cmd, 1);

	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Please input update parameter: start/cancel\n");
		return 0;
	}

	if(!strcmp(ctrlStr, "start"))
	{
		API_Event_UpdateStart(GetShellCmdInputString(cmd, 2), GetShellCmdInputString(cmd, 3), 0, "shell");
		ShellCmdPrintf("Start download\n");
	}
	else if(!strcmp(ctrlStr, "cancel"))
	{
		API_OtaCancel();
		ShellCmdPrintf("Cancel download\n");
	}

	return 1;
}

int ShellCmd_Io(cJSON * input)
{
	cJSON * value = NULL;
	char *command;
	char *paraName;

	command = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	if(!strcmp(command, "r"))
	{
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "w"))
	{
		cJSON* writeValue = cJSON_CreateObject();

		value = GetShellCmdInputJson(input, 3);
		if(cJSON_IsObject(value))
		{
			JsonMultilevelConversion(value, writeValue);
			cJSON_Delete(value);
		}
		else
		{
			cJSON_AddItemToObject(writeValue, paraName, value);
		}

		API_Para_Write_Public(writeValue);
		cJSON_Delete(writeValue);
	
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "default"))
	{
		ShellCmdPrintJson(API_Para_Read_Default(paraName));
	}
	else if(!strcmp(command, "restore"))
	{
		API_Para_Restore_Default(paraName);
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "check"))
	{
		value = GetShellCmdInputJson(input, 3);		
		ShellCmdPrintf("IO Check %s:%s is %s.\n", paraName, GetShellCmdInputString(input, 3), API_IO_Check(paraName, value) ? "true" : "false");
		cJSON_Delete(value);
	}
	else
	{
		ShellCmdPrintJson(API_Para_Read_Public(NULL));
	}
	return 1;
}

int ShellCmd_OldIo(cJSON * input)
{
	cJSON * value = NULL;
	char *command;
	char *paraName;
	char readValue[1000] = {0};

	command = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	if(strlen(paraName) == 4)
	{
		if(!strcmp(command, "r"))
		{
			if(API_Event_IoServer_InnerRead_All(paraName, readValue))
			{
				ShellCmdPrintf("Read %s error!", paraName);
			}
			else
			{
				ShellCmdPrintf("Read %s:%s", paraName, readValue);
			}
		}
		else if(!strcmp(command, "w"))
		{
			cJSON* writeValue = cJSON_CreateObject();

			value = GetShellCmdInputString(input, 3);
			if(API_Event_IoServer_InnerWrite_All(paraName, value))
			{
				ShellCmdPrintf("Write %s error!", paraName);
			}
			else
			{
				if(API_Event_IoServer_InnerRead_All(paraName, readValue))
				{
					ShellCmdPrintf("Write %s error!", paraName);
				}
				else
				{
					ShellCmdPrintf("Write %s:%s", paraName, readValue);
				}
			}
		}
	}
	else
	{
		ShellCmdPrintf("Para name %s error!", paraName);
	}
	return 1;
}

static int ShellCmd_Ftp(cJSON *cmd)
{
	char* option = GetShellCmdInputString(cmd, 1);
	char* server = GetShellCmdInputString(cmd, 2);
	char* fileName = GetShellCmdInputString(cmd, 3);
	char* storeDir = GetShellCmdInputString(cmd, 4);

	if(!strcmp(option, "download"))
	{
		//FTP_Download(server, fileName, storeDir);
		if(!strcmp(server, "cancel"))
		{
			ShellCmdPrintf("FTP stop download %s/%s.\n", storeDir, fileName);
			FTP_StopDownload(fileName, storeDir);
		}
		else
		{
			ShellCmdPrintf("FTP start download %s %s/%s.\n", server, storeDir, fileName);
			FTP_StartDownload(server, fileName, storeDir, NULL,  NULL);
		}
	}
	else if(!strcmp(option, "upload"))
	{
		if(!strcmp(GetShellCmdInputString(cmd, 5), "cancel"))
		{
			ShellCmdPrintf("FTP stop upload %s:%s/%s.\n", server, storeDir, fileName);
			FTP_StopUpload(server, fileName, storeDir);
		}
		else
		{
			ShellCmdPrintf("FTP start upload %s:%s/%s.\n", server, storeDir, fileName);
			FTP_StartUpload(server, fileName, storeDir, NULL,  NULL);
		}
	}
	else
	{
		cJSON* element;
		char cmdString[500] = {0};
		int i;
		int arraySize = cJSON_GetArraySize(cmd);
		for(i = 3; i < arraySize; i++)
		{
			strcat(cmdString, GetShellCmdInputString(cmd, i));
			strcat(cmdString, " ");
		}

		ShellCmdPrintf("FTP %s %s.\n", cmdString, (FTP_Url(GetShellCmdInputString(cmd, 1), GetShellCmdInputString(cmd, 2), cmdString) ? "error" : "success"));
	}
	return 1;
}

static int ShellCmd_CsvToJson(cJSON *cmd)
{
	char* file = GetShellCmdInputString(cmd, 1);
	cJSON* json = CsvToJson(file);
	if(json)
	{
		char* saveFile = GetShellCmdInputString(cmd, 2);
		if(saveFile)
		{
			SetJsonToFile(saveFile, json);
		}
		ShellCmdPrintJson(json);
		cJSON_Delete(json);
	}
	else
	{
		ShellCmdPrintf("%s error\n", file);
	}
	return 1;
}

int ShellCmd_AutoTest(cJSON *cmd);
int ShellCmd_Table(cJSON *cmd);
int ShellCmd_RemoteTable(cJSON *cmd);
int ShellCmd_LocalTable(cJSON *cmd);
int ShellCmd_MemSpace(cJSON *cmd);
int ShellCmd_IpcManage(cJSON *cmd);

int ShellCmdInit(void)
{
	ShellCmdAdd("pwd", ShellCmd_Pwd, "Print work dir");
	ShellCmdAdd("cd", ShellCmd_Cd, "Change dir");
	ShellCmdAdd("os", ShellCmd_Os, "System commands:\n\tBe executed by execvp().\n\tFor example:os [-t 5] ls -al [> 1.txt]");
	ShellCmdAdd("pb", ShellCmd_PublicInfo, "Read/write public information:\n\tFor example:pir r/w MFG_SN");
	ShellCmdAdd("tb", ShellCmd_Table, "For example:\n\ttb count/add/del/select/update/load/drop fileName Where ...");
	ShellCmdAdd("rtb", ShellCmd_RemoteTable, "For example:\n\trtb ip tbName count/add/del/select/update Where (View/record)");
	ShellCmdAdd("ltb", ShellCmd_LocalTable, "For example:\n\tltb tbName count/add/del/select/update Where (View/record)");
	ShellCmdAdd("test", ShellCmd_AutoTest, "For example:\n\ttest start /mnt/nfs/test.ts\n\ttest stop\n\ttest state");
	ShellCmdAdd("event", ShellCmd_Event, "For example:\n\tevent {\"EventName\":\"XXX\"}");
	ShellCmdAdd("reg", ShellCmd_RegCheck, "For example:\n\treg ^\\d[10]$ 0123456789");
	ShellCmdAdd("reboot", ShellCmd_Reboot, "For example:\n\treboot");
	ShellCmdAdd("update", ShellCmd_Update, "For example:\n\tupdate start 47.106.104.38 471000");
	ShellCmdAdd("oio", ShellCmd_OldIo, "For example:\n\toio r/w 1709 0");
	ShellCmdAdd("io", ShellCmd_Io, "For example:\n\tio r/w/default/restore MY_NAME");
	ShellCmdAdd("ftp", ShellCmd_Ftp, "For example:\n\tftp download 47.106.104.38 611-test /mnt/nand1-2/");
	ShellCmdAdd("csvtojson", ShellCmd_CsvToJson, "For example:\n\tcsvtojson /mnt/nand1-2/temp.csv");
	ShellCmdAdd("ipc", ShellCmd_IpcManage, "For example:\n\tipc search/login/show/close key2 key3 ...");
	#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
	ShellCmdAdd("memc", ShellCmd_MemSpace, "For example:\n\tmemc 10");
	#endif
}



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

