/**
  ******************************************************************************
  * @file    obj_MS_DeviceTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_MS_DeviceTable_H
#define _obj_MS_DeviceTable_H

// Define Object Property-------------------------------------------------------
#define MS_TABLE_KEY_MFG_SN				"MFG_SN"
#define MS_TABLE_KEY_DEVICE_TYPE		"DEVICE_TYPE"
#define MS_TABLE_KEY_GLOBAL				"GLOBAL"
#define MS_TABLE_KEY_LOCAL				"LOCAL"
#define MS_TABLE_KEY_BD_RM_MS			"BD_RM_MS"
#define MS_TABLE_KEY_NAME				"NAME"
#define MS_TABLE_KEY_R_NAME				"R_NAME"

#define MAX_NAME_LEN					20

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	MFG_SN[12+1];		//MFG_SN
	int		deviceType;			//设备类型
	char	Global[10+1];		//Global
	char	Local[6+1];			//Local
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	name1[20+1];		//name1
	char	R_Name[20+1];		//R_Name
} MS_ListRecord_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------





#endif


