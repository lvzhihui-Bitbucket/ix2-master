/**
  ******************************************************************************
  * @file    obj_CallCtrlTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CallCtrlTable_H
#define _obj_CallCtrlTable_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	local[6+1];			//local number
	char	global[10+1];		//global number
	char	name1[20+1];		//name1
	char	name2utf8[40+1];	//name2utf8
	int 	virtual_device;		//
	int 	ip_node_id;			//
	int 	dt_addr;			//
} CallCtrlRecord_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------

//加载表
void LoadCallCtrlTable(void);


//得到一个指定global_nbr的房号：
// paras:
// global_nbr:	输入的input
// bd_nbr:    	单元号
// rm_nbr：		房间号
// ms_nbr:  	主从房号
// return:
//  0:ok，-1:err
int get_bd_rm_ms_nbr_from_call_control_table( const char* global_nbr, char* bd_nbr,char* rm_nbr, char* ms_nbr );

// Define Object Function - Private---------------------------------------------

// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// ptr_call_ctrl_info: 得到的记录值
// return：
// 0: ok，-1: err
int get_call_control_record_items( int index,  CallCtrlRecord_T* ptr_call_ctrl_info);


//判断是否有呼叫控制表
//0:无表; 1:有表
int hasCallCtrlTable(void);

#endif


