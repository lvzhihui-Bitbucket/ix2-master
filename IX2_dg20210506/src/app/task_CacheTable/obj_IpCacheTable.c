/**
  ******************************************************************************
  * @file    obj_IpCacheTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_IpCacheTable.h"
#include "../task_io_server/obj_TableProcess.h"
#include "../task_io_server/task_IoServer.h"
#include "obj_GetIpByNumber.h"




one_vtk_table* newDeviceMapTable;
IpCacheTable_T* ipCacheTable;

int GetOneIpCacheFromDeviceTable(int index, IpCacheRecord_T* pRecord)
{
	one_vtk_dat* tempRecord;
	char data[200];
	int dataIndex, len;
	
	if(newDeviceMapTable == NULL)
	{
		return -1;
	}

	if(index >= newDeviceMapTable->record_cnt)
	{
		return -1;
	}
	
	tempRecord = get_one_vtk_record_without_keyvalue(newDeviceMapTable, index);

	//printf("index=%d, newDeviceMapTable->record_cnt = %d, tempRecord = %s\n", index, newDeviceMapTable->record_cnt, tempRecord->pdat);
	
	memset(pRecord, 0, sizeof(IpCacheRecord_T));

	//ȡBD
	len = 100;
	get_one_record_string(tempRecord, 0, data, &len);
	dataIndex = len;
	
	//ȡRM
	len = 100;
	get_one_record_string(tempRecord, 1, data + dataIndex, &len);
	dataIndex += len;

	//ȡMS
	len = 100;
	get_one_record_string(tempRecord, 2, data + dataIndex, &len);
	dataIndex += len;

	if(dataIndex != 10)
	{
		return -2;
	}
	else
	{
		memcpy(pRecord->BD_RM_MS, data, 10);
	}

	//ȡIP
	len = 100;
	get_one_record_string(tempRecord, 6, data, &len);
	data[len] = 0;
	pRecord->ip = inet_addr(data);
	
	return 0;
}


void LoadIpCacheTable(void)
{
	newDeviceMapTable = load_vtk_table_file(DEVICE_MAP_TABLE_NAME);
	if(newDeviceMapTable != NULL)
	{
		ipCacheTable = malloc(sizeof(IpCacheTable_T));
		ipCacheTable->pRecord = malloc(newDeviceMapTable->record_cnt*sizeof(IpCacheRecord_T));

		for(ipCacheTable->record_cnt = 0; ipCacheTable->record_cnt < newDeviceMapTable->record_cnt; ipCacheTable->record_cnt++)
		{
			GetOneIpCacheFromDeviceTable(ipCacheTable->record_cnt, ipCacheTable->pRecord + ipCacheTable->record_cnt);
		}
		free_vtk_table_file_buffer(newDeviceMapTable);
	}
	else
	{
		ipCacheTable = NULL;
	}
}

int AddOneCacheRecordToTable(IpCacheRecord_T record)
{
	int i;

	if(ipCacheTable == NULL)
	{
		return -1;
	}

	for(i = 0; i < ipCacheTable->record_cnt; i++)
	{
		if(!memcmp(record.BD_RM_MS, ipCacheTable->pRecord[i].BD_RM_MS, 10))
		{
			ipCacheTable->pRecord[i].ip = record.ip;
			return 0;
		}
	}

	return -2;
}

int SearchIpCacheTableByBdRmMs(char* bd_rm_ms, IpCacheRecord_T record[])
{
	int i;
	int len;
	int matchType;
	int matchCnt;
	

	if(ipCacheTable == NULL)
	{
		return 0;
	}
	
	len = strlen(bd_rm_ms);
	
	if(len != 8 && len != 10)
	{
		return 0;
	}

	if(len == 10)
	{
		if(bd_rm_ms[8] == '0' && bd_rm_ms[9] == '0')
		{
			//ƥ��8λ����
			len = 8;
		}
		else
		{
			//ƥ��10λ����
			len = 10;
		}
	}
	else
	{
		//ƥ��8λ����
		len = 8;
	}


	for(i = 0, matchCnt = 0; i < ipCacheTable->record_cnt; i++)
	{
		if(!memcmp(bd_rm_ms, ipCacheTable->pRecord[i].BD_RM_MS, len))
		{
			record[matchCnt++] = ipCacheTable->pRecord[i];

			//�����豸�����ﵽ����
			if(matchCnt == MAX_DEVICE)
			{
				break;
			}
		}
	}

	return matchCnt;
}

//�ж��Ƿ���ȫ���豸��
//0:�ޱ�; 1:�б�
int hasIpCacheTable(void)
{
	if(ipCacheTable == NULL)
	{
		return 0;
	}

	if(ipCacheTable->record_cnt == 0)
	{
		return 0;
	}

	return 1;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

