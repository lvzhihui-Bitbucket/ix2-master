
  
#include "obj_R8001_Table.h"
#include "obj_TableProcess.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_GetIpByNumber.h"
#include "task_IoServer.h"

one_vtk_table* r8001Table;
one_vtk_table* r8011Table = NULL;;//r8001 summary table
one_vtk_table* r8012Table = NULL;;//r8001 checklist table

void LoadR8001Table(void)
{
	r8001Table = load_vtk_table_file(R8001_TABLE_NAME);
	if(r8001Table == NULL)
	{
		char R8001TableKey[][40] = {R8001_KEY_BD_RM_MS, R8001_KEY_NAME1, R8001_KEY_LOCAL, R8001_KEY_GLOBAL, R8001_KEY_DEVICE_TYPE, R8001_KEY_MFG_SN, 
							     R8001_KEY_NAME2, R8001_KEY_IP_STATIC, R8001_KEY_IP_ADDR,R8001_KEY_IP_MASK,R8001_KEY_IP_GATEWAY};
		int i;

		r8001Table = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &r8001Table->lock, 0);		
		r8001Table->keyname_cnt = 11;
		r8001Table->pkeyname_len = malloc(r8001Table->keyname_cnt*sizeof(int));
		r8001Table->pkeyname = malloc(r8001Table->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< r8001Table->keyname_cnt; i++)
		{
			r8001Table->pkeyname_len[i] = 40;
			r8001Table->pkeyname[i].len = strlen(&R8001TableKey[i][0]);
			r8001Table->pkeyname[i].pdat = malloc(r8001Table->pkeyname[i].len);
			memcpy(r8001Table->pkeyname[i].pdat, &R8001TableKey[i][0], r8001Table->pkeyname[i].len);
		}
		r8001Table->precord = NULL;
		r8001Table->record_cnt = 0;
		save_vtk_table_file_buffer(r8001Table, 0, r8001Table->record_cnt, R8001_TABLE_NAME);
	}
}
void R8001TableDelete(void)
{
	free_vtk_table_file_buffer(r8001Table);
	r8001Table = NULL;
}
void R8001_format(void)
{
	delete_all_vtk_record(r8001Table);	
	SaveR8001TableToFile();

}
void R8001_Reload(void)
{
	R8001TableDelete();
	LoadR8001Table();
}

int get_r8001_record_items( int index,  CallNbr_T* call_nbr_info)
{
	one_vtk_dat* tempRecord;
	char data[200];
	int dataIndex, len;
	
	if(r8001Table == NULL)
	{
		return -1;
	}

	if(index >= r8001Table->record_cnt)
	{
		return -1;
	}
	
	tempRecord = get_one_vtk_record_without_keyvalue(r8001Table, index);
	
	memset(call_nbr_info, 0, sizeof(CallNbr_T));

	//ȡBD_RM_MS
	len = 10;
	get_one_record_string(tempRecord, 0, data, &len);
	len = len > 10 ? 10 : len;
	data[len] = 0;
	strcpy(call_nbr_info->BD_RM_MS, data);
	//ȡName
	len = 20;
	get_one_record_string(tempRecord, 1, data, &len);
	len = len > 20 ? 20 : len;
	data[len] = 0;
	strcpy(call_nbr_info->Name, data);
	//ȡLocal nbr
	len = 6;
	get_one_record_string(tempRecord, 2, data, &len);
	len = len > 6 ? 6 : len;
	data[len] = 0;
	strcpy(call_nbr_info->Local, data);
	//ȡGlobal nbr
	len = 10;
	get_one_record_string(tempRecord, 3, data, &len);
	len = len > 10 ? 10 : len;
	data[len] = 0;
	strcpy(call_nbr_info->Global, data);
	//ȡDEVICE_TYPE
	len = 2;
	get_one_record_string(tempRecord, 4, data, &len);
	len = len > 2 ? 2 : len;
	data[len] = 0;
	strcpy(call_nbr_info->device_type, data);
	//ȡR_Name
	len = 40;
	get_one_record_string(tempRecord, 6, data, &len);
	len = len > 40 ? 40 : len;
	data[len] = 0;
	strcpy(call_nbr_info->R_Name, data);
	//ȡIP_STATIC
	len = 6;
	get_one_record_string(tempRecord, 7, data, &len);
	len = len > 6 ? 6 : len;
	data[len] = 0;
	strcpy(call_nbr_info->ip_static, data);
	//ȡIP_ADDR
	len = 15;
	get_one_record_string(tempRecord, 8, data, &len);
	len = len > 15 ? 15 : len;
	data[len] = 0;
	strcpy(call_nbr_info->ip, data);
	//ȡIP_MASK
	len = 15;
	get_one_record_string(tempRecord, 9, data, &len);
	len = len > 15 ? 15 : len;
	data[len] = 0;
	strcpy(call_nbr_info->mask, data);
	//ȡIP_GATEWAY
	len = 15;
	get_one_record_string(tempRecord, 10, data, &len);
	len = len > 15 ? 15 : len;
	data[len] = 0;
	strcpy(call_nbr_info->gw, data);

	return 0;
	
}

int get_r8001_record_by_brm( char* bd_rm_ms,  CallNbr_T* call_nbr_info)
{
	int i, len;
	if(r8001Table->record_cnt == 0)
	{
		return 0;
	}

	len = strlen(bd_rm_ms);
	if(len != 8 && len != 10)
	{
		return 0;
	}
	for(i = 0; i < r8001Table->record_cnt; i++)
	{
		get_r8001_record_items( i,  call_nbr_info);
		
		// BD_RM_MS ƥ��
		if(!memcmp(bd_rm_ms, call_nbr_info->BD_RM_MS, len))
		{
			return 1;
		}
	}
	
	return 0;
}

int get_r8001_record_cnt(void)
{
	if(r8001Table == NULL)
	{
		return 0;
	}
	return(r8001Table->record_cnt);
}

int SaveR8001TableToFile(void)
{
	int keyname_index;
	
	keyname_index = get_keyname_index( r8001Table, R8001_KEY_BD_RM_MS);	
	
	AlphabeticalOrder_vtk_table(r8001Table, keyname_index);

	remove(R8001_TABLE_NAME);
	if(save_vtk_table_file_buffer(r8001Table, 0, r8001Table->record_cnt, R8001_TABLE_NAME))
	{
		return -1;
	}
	
	return 0;
}


//R8011 summary_report Table 
void CreateR8011Table(void)
{
	if(r8011Table == NULL)
	{
		char R8011TableKey[][40] = {R8001_KEY_BD_RM_MS, R8001_KEY_NAME1, R8001_KEY_LOCAL, R8001_KEY_GLOBAL, R8001_KEY_DEVICE_TYPE, R8001_KEY_MFG_SN, 
							     R8001_KEY_NAME2, R8001_KEY_IP_STATIC, R8001_KEY_IP_ADDR,R8001_KEY_IP_MASK,R8001_KEY_IP_GATEWAY};
		int i;

		r8011Table = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &r8011Table->lock, 0);		
		r8011Table->keyname_cnt = 11;
		r8011Table->pkeyname_len = malloc(r8011Table->keyname_cnt*sizeof(int));
		r8011Table->pkeyname = malloc(r8011Table->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< r8011Table->keyname_cnt; i++)
		{
			r8011Table->pkeyname_len[i] = 40;
			r8011Table->pkeyname[i].len = strlen(&R8011TableKey[i][0]);
			r8011Table->pkeyname[i].pdat = malloc(r8011Table->pkeyname[i].len);
			memcpy(r8011Table->pkeyname[i].pdat, &R8011TableKey[i][0], r8011Table->pkeyname[i].len);
		}
		r8011Table->precord = NULL;
		r8011Table->record_cnt = 0;
	}
}
void R8011TableDelete(void)
{
	free_vtk_table_file_buffer(r8011Table);
	r8011Table = NULL;
}

int SaveR8011TableToFile(void)
{
	int keyname_index;
	time_t t;
	struct tm *tblock; 
	char tempChar[100];
	
	keyname_index = get_keyname_index( r8011Table, R8001_KEY_BD_RM_MS);	
	
	AlphabeticalOrder_vtk_table(r8011Table, keyname_index);

	t = time(NULL); 
	tblock=localtime(&t);

	snprintf( tempChar,100,"/mnt/sdcard/R8011_%02d%02d%02d%02d%02d%02d.csv",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	
	if(save_vtk_table_file_buffer(r8011Table, 0, r8011Table->record_cnt, tempChar))
	{
		return -1;
	}
	
	return 0;
}

int creat_r8001_summary_report(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, ret;

	R8011TableDelete();
	CreateR8011Table();
	API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);

	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{
			one_vtk_dat vtkRecord;
			char data[200];
		
			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			if(devInfo.name2_utf8[0] == 0)
			{
				strcpy(devInfo.name2_utf8, "-");
			}
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",devInfo.BD_RM_MS, devInfo.name1,devInfo.Local, devInfo.Global, DeviceTypeToString(devInfo.deviceType),devInfo.MFG_SN, 
														 devInfo.name2_utf8, devInfo.IP_STATIC, devInfo.IP_ADDR, devInfo.IP_MASK, devInfo.IP_GATEWAY);
			
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(r8011Table, &vtkRecord);
			ret++;
		}
	}
	SaveR8011TableToFile();
	return ret;
}

//R8012 checklist_report Table 
void CreateR8012Table(void)
{
	if(r8012Table == NULL)
	{
		char R8012TableKey[][40] = {R8001_KEY_BD_RM_MS, R8001_KEY_NAME1, R8001_KEY_LOCAL, R8001_KEY_GLOBAL, R8001_KEY_DEVICE_TYPE, R8001_KEY_MFG_SN, 
							     R8001_KEY_NAME2, R8001_KEY_IP_STATIC, R8001_KEY_IP_ADDR,R8001_KEY_IP_MASK,R8001_KEY_IP_GATEWAY, R8001_KEY_CHECK_RESULT};
		int i;

		r8012Table = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &r8012Table->lock, 0);		
		r8012Table->keyname_cnt = 12;
		r8012Table->pkeyname_len = malloc(r8012Table->keyname_cnt*sizeof(int));
		r8012Table->pkeyname = malloc(r8012Table->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< r8012Table->keyname_cnt; i++)
		{
			r8012Table->pkeyname_len[i] = 40;
			r8012Table->pkeyname[i].len = strlen(&R8012TableKey[i][0]);
			r8012Table->pkeyname[i].pdat = malloc(r8012Table->pkeyname[i].len);
			memcpy(r8012Table->pkeyname[i].pdat, &R8012TableKey[i][0], r8012Table->pkeyname[i].len);
		}
		r8012Table->precord = NULL;
		r8012Table->record_cnt = 0;
	}
}
void R8012TableDelete(void)
{
	free_vtk_table_file_buffer(r8012Table);
	r8012Table = NULL;
}

int SaveR8012TableToFile(void)
{
	int keyname_index;
	time_t t;
	struct tm *tblock; 
	char tempChar[100];
	
	keyname_index = get_keyname_index( r8012Table, R8001_KEY_BD_RM_MS);	
	
	AlphabeticalOrder_vtk_table(r8012Table, keyname_index);
	t = time(NULL); 
	tblock=localtime(&t);

	snprintf( tempChar,100,"/mnt/sdcard/R8012_%02d%02d%02d%02d%02d%02d.csv",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);

	if(save_vtk_table_file_buffer(r8012Table, 0, r8012Table->record_cnt, tempChar))
	{
		return -1;
	}
	
	return 0;
}

int creat_r8001_checklist_report(void)
{
	int i;
	CallNbr_T call_nbr_info;
	GetIpRspData data;
	one_vtk_dat vtkRecord;
	char temp[200];
	char ret[30];
	if(r8001Table->record_cnt == 0)
	{
		return -1;
	}
	R8012TableDelete();
	CreateR8012Table();
	for(i = 0; i < r8001Table->record_cnt; i++)
	{
		get_r8001_record_items( i,  &call_nbr_info);
		if(API_GetIpNumberFromNet(call_nbr_info.BD_RM_MS, NULL, NULL, 2, 1, &data) == 0)
		{
			DeviceInfo devInfo;
			API_GetInfoByIp(data.Ip[0], 2, &devInfo);
			if(data.cnt > 1)
			{
				sprintf(ret,"Device Repeat");
			}
			else
			{
				if(strcmp(devInfo.BD_RM_MS,call_nbr_info.BD_RM_MS))
				{
					sprintf(ret,"Installer ERR");
				}
				else if(strcmp(devInfo.name1,call_nbr_info.Name))
				{
					sprintf(ret,"Installer ERR");
				}
				else if(strcmp(devInfo.Local,call_nbr_info.Local))
				{
					sprintf(ret,"Installer ERR");
				}
				else if(strcmp(devInfo.Global,call_nbr_info.Global))
				{
					sprintf(ret,"Installer ERR");
				}
				else
				{
					sprintf(ret,"Installer OK");
				}
			}
			snprintf(temp, 200, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",devInfo.BD_RM_MS, devInfo.name1,devInfo.Local, devInfo.Global, DeviceTypeToString(devInfo.deviceType),devInfo.MFG_SN, devInfo.name2_utf8,
														  devInfo.IP_STATIC, devInfo.IP_ADDR, devInfo.IP_MASK, devInfo.IP_GATEWAY,ret);
		}
		else
		{
			sprintf(ret,"Device Offline");
			snprintf(temp, 200, "%s,%s,%s,%s,%s,-,%s,%s,%s,%s,%s,%s",call_nbr_info.BD_RM_MS, call_nbr_info.Name, call_nbr_info.Local, call_nbr_info.Global, call_nbr_info.device_type, call_nbr_info.R_Name,
																call_nbr_info.ip_static, call_nbr_info.ip, call_nbr_info.mask, call_nbr_info.gw, ret);
		}
		
		vtkRecord.len = strlen(temp);

		vtkRecord.pdat = malloc(vtkRecord.len);
		memcpy(vtkRecord.pdat, temp, vtkRecord.len);
	
		add_one_vtk_record(r8012Table, &vtkRecord);

	}
	SaveR8012TableToFile();
	return 0;

}

void UpdateCallNbrByRes(CallNbr_T imCallNbr)
{
	SetSysVerInfo_name((strcmp(imCallNbr.R_Name, "-")&&imCallNbr.R_Name[0]) ? imCallNbr.R_Name : imCallNbr.Name);
	SetSysVerInfo_LocalNum(imCallNbr.Local);
	SetSysVerInfo_GlobalNum(imCallNbr.Global);
	//set ip
	char temp[20];
	char dhcpState;
	if(!strcmp(imCallNbr.ip_static,"DHCP"))//DHCP
	{
		API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
		if(atoi(temp) == 0)
		{
			dhcpState = 1;
			sprintf(temp, "%d", dhcpState);
			API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
			API_DHCP_SaveEnable(1);
		}
	}
	else		 //��̬
	{
		dhcpState = 0;
		sprintf(temp, "%d", dhcpState);
		API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
		API_DHCP_SaveEnable(0);
		SetSysVerInfo_IP(imCallNbr.ip);
		SetSysVerInfo_mask(imCallNbr.mask);
		SetSysVerInfo_gateway(imCallNbr.gw);
		SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		
	}
	
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

