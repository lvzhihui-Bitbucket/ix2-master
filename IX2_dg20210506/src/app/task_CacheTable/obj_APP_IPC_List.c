#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "obj_APP_IPC_List.h"
#include "obj_IPCTableSetting.h"
#include "define_file.h"
/*
{
    "IPC":{
    	"ID": "CAM1", 
    	"NAME":"CAM1", 
    	"IP": "192.168.243.6", 
    	"CH_ALL": 3, 
    	"User": "admin",
    	"PWD": "admin",
    	"CH0":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/0",
    		"width": 1920,
			"height": 1080,
    		},
    	"CH1":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/1",
    		"width": 640,
			"height": 480,
    		},
    	"CH2":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/2",
    		"width": 320,
			"height": 240,
    		},
    	},

};
*/

static APP_IPC_DeviceTable appIpcListTable = {.ipc = NULL};

int DeleteOneAppIpcListRecordByIdAndName(char* id, char* name)
{
	int i;
	int appIpcNum, saveAppIpcNum;
	cJSON *ipcs, *ipc;
	int result = 0;

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	appIpcNum = cJSON_GetArraySize(ipcs);
	saveAppIpcNum = appIpcNum;

	while(appIpcNum)
	{
		ipc = cJSON_GetArrayItem(ipcs, --appIpcNum);
		if(ipc != NULL)
		{
			if(id != NULL)
			{
				if(strcmp(id, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_ID))))
				{
					continue;
				}
			}

			if(name != NULL)
			{
				if(strcmp(name, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_NAME))))
				{
					continue;
				}
			}
				cJSON_DeleteItemFromArray(ipcs, appIpcNum);
				SaveAppIpcListToFile();
		}
	}

	if(saveAppIpcNum == cJSON_GetArraySize(ipcs))
	{
		result = -1;	//不需删除
	}
	else
	{
		SaveAppIpcListToFile();
		result = 0;	//已经存在，删除完成
	}

	return result;
}

int AddOrModifyOneAppIpcListRecord(APP_IPC_ONE_DEVICE record, int syncFlag)
{
	int i;
	int appIpcNum;
	cJSON *ipcs, *ipc;
	int result = 0;

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	appIpcNum = cJSON_GetArraySize(ipcs);
	
	for(i = 0; i < appIpcNum; i++)
	{
		ipc = cJSON_GetArrayItem(ipcs, i);
		if(ipc != NULL)
		{
			if(!strcmp(record.NAME, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_NAME))))
			{
				result = -1;
				if(strcmp(record.ID, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_ID))))
				{
					cJSON_ReplaceItemInObject(ipc, APP_IPC_ID, cJSON_CreateString(record.ID));
					result = 1;
				}

				if(strcmp(record.videoType, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_VIDEO_TYPE))))
				{
					cJSON_ReplaceItemInObject(ipc, APP_IPC_VIDEO_TYPE, cJSON_CreateString(record.videoType));
					result = 1;
				}

				if(result == 1 && syncFlag)
				{
					SaveAppIpcListToFile();
				}

				return result;
			}
		}
	}

	if(ipcs == NULL)
	{
		cJSON_AddItemToObject(appIpcListTable.ipc, APP_IPC_IPCS, ipcs = cJSON_CreateArray());	
	}

	ipc = cJSON_CreateObject();
	cJSON_AddStringToObject(ipc, APP_IPC_ID, record.ID);
	cJSON_AddStringToObject(ipc, APP_IPC_NAME, record.NAME);
	cJSON_AddStringToObject(ipc, APP_IPC_VIDEO_TYPE, record.videoType);
	cJSON_AddItemToArray(ipcs, ipc);

	if(syncFlag)
	{
		SaveAppIpcListToFile();
	}

	return result;	//添加成功
}

int AddOneAppIpcListFromIpcCacheTable(IPC_ONE_DEVICE record, int syncFlag)
{
	int videoType;
	
	APP_IPC_ONE_DEVICE appIpcRecord;

	strcpy(appIpcRecord.ID, record.ID);
	strcpy(appIpcRecord.NAME, record.NAME);
	strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H264);

	if(record.CH_DAT[record.CH_APP].vd_type == 1)
	{
		strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H265);
	}

	return AddOrModifyOneAppIpcListRecord(appIpcRecord, syncFlag);
}

char* CreateAppIpcListObject(int add_ds)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	char *string = NULL;
	int i,j;
	char name[20];
	#if 0
	if(add_ds)
	{
		int ds_cnt=0;
		char rm_addr[11];
		char ds_id[50];
		char ds_name[41];

		string = cJSON_Print(appIpcListTable.ipc);
		root = cJSON_Parse(string);
		devices = cJSON_GetObjectItem(root, APP_IPC_IPCS);
		free(string);

		ds_cnt=Get_MonRes_Num();
		if(ds_cnt==0)
			ds_cnt=Get_MonRes_Temp_Table_Num();
		for(i=0;i<ds_cnt;i++)
		{
			
			if(Get_MonRes_Num()>0)
			{
				Get_MonRes_Record(i,ds_name,rm_addr,NULL);
			}
			else
			{
				Get_MonRes_Temp_Table_Record(i,ds_name,rm_addr,NULL);
			}
			device = cJSON_CreateObject();
			sprintf(ds_id,"%s:%s", APP_IPC_IX_DS, rm_addr);
			cJSON_AddStringToObject(device, APP_IPC_ID, ds_id);
			cJSON_AddStringToObject(device, APP_IPC_NAME, ds_name);
			cJSON_AddStringToObject(device, APP_IPC_VIDEO_TYPE, IPC_VIDEO_TYPE_H264);
			
			cJSON_AddItemToArray(devices, device);
		}
		#if 0
		ds_cnt=Get_DxMonRes_Num();
		for(i=0;i<ds_cnt;i++)
		{
			Get_DxMonRes_Record(i,ds_name,rm_addr);
			device = cJSON_CreateObject();
			sprintf(ds_id,"%s:%s", APP_IPC_DT_DS, rm_addr);
			cJSON_AddStringToObject(device, APP_IPC_ID, ds_id);
			cJSON_AddStringToObject(device, APP_IPC_NAME, ds_name);
			cJSON_AddStringToObject(device, APP_IPC_VIDEO_TYPE, IPC_VIDEO_TYPE_H264);
			
			cJSON_AddItemToArray(devices, device);
		}
		#endif
		string = cJSON_Print(root);
		cJSON_Delete(root);
	}
	else
	#endif
	{
		string = cJSON_Print(appIpcListTable.ipc);
	}
	
	memset(appIpcListTable.md5Code, 0, 16);
	
	StringMd5_Calculate(string, appIpcListTable.md5Code);

	return string;
}



int ParseOneAppIpcListRecordObject(const char* json, APP_IPC_ONE_DEVICE *pOneIpcRecord)
{
	int status = 0;
	APP_IPC_ONE_DEVICE record;
	
	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}


	ParseJsonString(root, APP_IPC_ID, record.ID, 40);
	ParseJsonString(root, APP_IPC_NAME, record.NAME, 40);
	ParseJsonString(root, APP_IPC_VIDEO_TYPE, record.videoType, 5);

	*pOneIpcRecord = record;

	status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void SaveAppIpcListToFile(void)
{
	char* jsonString;
	jsonString = CreateAppIpcListObject(0);
	if(jsonString != NULL)
	{
		SetJsonStringToFile(APP_IPC_LIST_FILE_NAME, jsonString);
		free(jsonString);
	}
}

void GetAppIpcListFromFile(void)
{
	char* json;
	cJSON* ipcs;

	if(appIpcListTable.ipc != NULL)
	{
		cJSON_Delete(appIpcListTable.ipc);
		appIpcListTable.ipc = NULL;
	}

	json = GetJsonStringFromFile(APP_IPC_LIST_FILE_NAME);
	if(json != NULL)
	{
		appIpcListTable.ipc = cJSON_Parse(json);
		free(json);
	}

	if(appIpcListTable.ipc == NULL)
	{
		appIpcListTable.ipc = cJSON_CreateObject();
	}

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	if(ipcs == NULL)
	{
		cJSON_AddItemToObject(appIpcListTable.ipc, APP_IPC_IPCS, ipcs = cJSON_CreateArray());	
	}

	if(cJSON_GetArraySize(ipcs) == 0)
	{
		AddAppIpcFromLanIpcOrWlanIpc();
	}
}

const char* GetAppIpcListMD5Code(void)
{
	char* temp;
	
	temp = CreateAppIpcListObject(1);

	if(temp != NULL)
	{
		free(temp);
	}
	
	return appIpcListTable.md5Code;
}

