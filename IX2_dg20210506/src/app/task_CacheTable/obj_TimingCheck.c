/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "OSTIME.h"
#include <pthread.h>
#include "utility.h"

#define TIMING_CHECK_OBJ_MAX	100

//返回1：计时器要清零，返回0：继续计时， 返回2：删除定时器
typedef int (*TimeCheckCallbak)(int);

typedef struct
{	
	int	timing;			//计时
	int	timeLimit;		//定时
	TimeCheckCallbak	callback;
}TimingCheckObj_t;

typedef struct
{	
	TimingCheckObj_t	obj[TIMING_CHECK_OBJ_MAX];
	int objCnt;
	OS_TIMER timer;
	pthread_mutex_t	lock;	
}TimingCheckRun_t;

static TimingCheckRun_t timingCheckRun = {.lock = PTHREAD_MUTEX_INITIALIZER};

static int Add_TimingCheck(TimeCheckCallbak callback, int timeLimit);
static int Del_TimingCheck(TimeCheckCallbak callback);

int API_Add_TimingCheck(TimeCheckCallbak callback, int timeLimit);
int API_Del_TimingCheck(TimeCheckCallbak callback);

static void TimingCheckCallback(void)
{
	int i;
	int result = 0;
	int objCnt;
	int timing;
	int timeLimit;
	TimeCheckCallbak callback;

	OS_RetriggerTimer(&timingCheckRun.timer);

    pthread_mutex_lock(&timingCheckRun.lock);
	objCnt = timingCheckRun.objCnt;
    pthread_mutex_unlock(&timingCheckRun.lock);

	for(i = 0; i < objCnt; )
	{
		pthread_mutex_lock(&timingCheckRun.lock);
		timingCheckRun.obj[i].timing++;
		timing = timingCheckRun.obj[i].timing;
		callback = timingCheckRun.obj[i].callback;
		timeLimit = timingCheckRun.obj[i].timeLimit;
		pthread_mutex_unlock(&timingCheckRun.lock);

		result = 0;
		if((callback) && (timing >= timeLimit))
		{
			result = (*callback)(timing);
		}

		pthread_mutex_lock(&timingCheckRun.lock);
		switch (result)
		{
			case 1:		//计数清零
				timingCheckRun.obj[i++].timing = 0;
				break;
			case 2:		//删除回调
				Del_TimingCheck(callback);
				objCnt = timingCheckRun.objCnt;
				break;
			
			default:
				i++;
				break;
		}
		pthread_mutex_unlock(&timingCheckRun.lock);
	}
}

void TimingCheckInit(void)
{
	timingCheckRun.objCnt = 0;
	OS_CreateTimer(&timingCheckRun.timer, TimingCheckCallback, 1000/25);		//一秒检测一次
	OS_StartTimer(&timingCheckRun.timer);
}

static int Add_TimingCheck(TimeCheckCallbak callback, int timeLimit)
{
	int ret = 0;
	int i;

	if(timeLimit <= 0)
	{
		return ret;
	}
	
	for(i = 0; i < timingCheckRun.objCnt; i++)
	{
		if(timingCheckRun.obj[i].callback == callback)
		{
			timingCheckRun.obj[i].timing = 0;
			timingCheckRun.obj[i].timeLimit = timeLimit;
			break;
		}
	}

	if((i >= timingCheckRun.objCnt) && (timingCheckRun.objCnt < TIMING_CHECK_OBJ_MAX))
	{
		timingCheckRun.obj[timingCheckRun.objCnt].timing = 0;
		timingCheckRun.obj[timingCheckRun.objCnt].timeLimit = timeLimit;
		timingCheckRun.obj[timingCheckRun.objCnt].callback = callback;
		timingCheckRun.objCnt++;
		ret = 1;
	}
	return ret;
}

static int Del_TimingCheck(TimeCheckCallbak callback)
{
	int i, j;
	int ret = 0;

	for(i = 0; i < timingCheckRun.objCnt; i++)
	{
		if(timingCheckRun.obj[i].callback == callback)
		{
			if(i != timingCheckRun.objCnt-1)
			{
				for(j = i+1; j < timingCheckRun.objCnt; j++, i++)
				{
					timingCheckRun.obj[i] = timingCheckRun.obj[j];
				}
			}
			timingCheckRun.objCnt--;
			ret = 1;
			break;
		}
	}

	return ret;
}

int API_Add_TimingCheck(TimeCheckCallbak callback, int timeLimit)
{
	int ret;

    pthread_mutex_lock(&timingCheckRun.lock);
	ret = Add_TimingCheck(callback, timeLimit);
    pthread_mutex_unlock(&timingCheckRun.lock);

	return ret;
}

int API_Del_TimingCheck(TimeCheckCallbak callback)
{
	int ret;
    pthread_mutex_lock(&timingCheckRun.lock);
	ret = Del_TimingCheck(callback);
    pthread_mutex_unlock(&timingCheckRun.lock);

	return ret;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

