/**
  ******************************************************************************
  * @file    obj_ImNameListTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_ImNameListTable.h"
#include "obj_TableProcess.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "task_IoServer.h"
#include "obj_NetManagerInterface.h"

one_vtk_table* imNameListTable = NULL;


// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetImNameListRecordItems( int index,  IM_NameListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAMELIST_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( imNameListTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->name1,input_str);

		//R_NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAMELIST_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);
		*/
		return 0;
	}
	else
		return -1;
}

void LoadImNameListTable(void)
{
	imNameListTable = load_vtk_table_file(IM_NAME_LIST_TABLE_NAME);
	if(imNameListTable == NULL)
	{
		char ImTableKey[7][40] = {IM_TABLE_KEY_BD_RM_MS, IM_TABLE_KEY_GLOBAL, IM_TABLE_KEY_LOCAL, IM_TABLE_KEY_NAME, IM_TABLE_KEY_R_NAME, IM_TABLE_KEY_DEVICE_TYPE, IM_TABLE_KEY_REMARK};
		int i;

		imNameListTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &imNameListTable->lock, 0);		//czn_20190327
		imNameListTable->keyname_cnt = 7;
		imNameListTable->pkeyname_len = malloc(imNameListTable->keyname_cnt*sizeof(int));
		imNameListTable->pkeyname = malloc(imNameListTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< imNameListTable->keyname_cnt; i++)
		{
			imNameListTable->pkeyname_len[i] = 40;
			imNameListTable->pkeyname[i].len = strlen(&ImTableKey[i][0]);
			imNameListTable->pkeyname[i].pdat = malloc(imNameListTable->pkeyname[i].len);
			memcpy(imNameListTable->pkeyname[i].pdat, &ImTableKey[i][0], imNameListTable->pkeyname[i].len);
		}

		imNameListTable->record_cnt = 0;
		
		SaveImNamelistTableToFile();
	}
	else
	{
		DeleteSameNamelist_Record();
	}

}

void ImNameList_Table_Release(void)
{
	free_vtk_table_file_buffer(imNameListTable);
}


int GetImNameListRecordCnt(void)
{
	return (imNameListTable == NULL) ? 0 : imNameListTable->record_cnt;
}
//czn_20190221_s
void ImNameList_Reload(void)
{
	free_vtk_table_file_buffer(imNameListTable);
	//delete_all_vtk_record(imNameListTable);
	imNameListTable = NULL;
	LoadImNameListTable();
}
//czn_20190221_e
//重命名修改NameList的一条记录
void ImNamelist_Rename(int index, const char *pname)
{
	char recordBuffer[200] = {0};
	one_vtk_dat vtkRecord;
	IM_NameListRecord_T record;
	
	GetImNameListRecordItems(index,  &record);
	
	if(pname != NULL && strlen(pname) <= 20)
	{	
		strcpy(record.R_Name, pname);
	
		snprintf(recordBuffer, 200, "%s,%d,%s,%s,%s,%s,%s", record.MFG_SN, record.deviceType, record.Global, record.Local, record.BD_RM_MS,
													record.name1, record.R_Name);
		
		vtkRecord.len = strlen(recordBuffer);
		vtkRecord.pdat = recordBuffer;
		
		Modify_one_vtk_record(&vtkRecord, imNameListTable, index);
		
		SaveImNamelistTableToFile();
	}
}

//搜索监视列表的一条记录
int SearchImNamelistTableByBdRmMs(char* MFG_SN, IM_NameListRecord_T* pRecord)
{
	int i;
	IM_NameListRecord_T tempRecord;

	if(MFG_SN[0] == 0)
	{
		return -1;
	}
	
	for(i = 0; i < imNameListTable->record_cnt; i++)
	{
		GetImNameListRecordItems(i, &tempRecord);
		if(!strcmp(tempRecord.MFG_SN, MFG_SN))
		{
			*pRecord = tempRecord;
			return i;
		}
	}

	return -1;
}


//增加namelist的一条记录
int AddOneImNamelist_Record(IM_NameListRecord_T* pRecord)
{
	int index;
	IM_NameListRecord_T tempRecord;
	one_vtk_dat vtkRecord;
	char data[200];

	snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s", pRecord->MFG_SN, pRecord->deviceType, pRecord->Global, pRecord->Local, pRecord->BD_RM_MS,
												pRecord->name1, pRecord->R_Name);
	vtkRecord.len = strlen(data);
	//czn_20190221_s
	if(memcmp(pRecord->MFG_SN,"000000000000",12) == 0)
	{
		index = -1;
	}
	else
	{
		index = SearchImNamelistTableByBdRmMs(pRecord->MFG_SN, &tempRecord);
	}
	//czn_20190221_e
	
	if(index >= 0)
	{
		vtkRecord.pdat = data;

		Modify_one_vtk_record(&vtkRecord, imNameListTable, index);
		SaveImNamelistTableToFile();
		return 1;
	}
	else
	{
		vtkRecord.pdat = malloc(vtkRecord.len);
		
		if(vtkRecord.pdat == NULL)
		{
			return -1;
		}

		memcpy(vtkRecord.pdat, data, vtkRecord.len);

		add_one_vtk_record(imNameListTable, &vtkRecord);
		
		SaveImNamelistTableToFile();
		return 0;
	}

}


//按index删除namelist的一条记录
int DeleteOneImNamelist_Record(int index)
{
	IM_NameListRecord_T tempRecord;
	
	if(index >= 0 && index < imNameListTable->record_cnt)
	{
		delete_one_vtk_record(imNameListTable, index);

		SaveImNamelistTableToFile();
		return 0;
	}
	else
	{
		return -1;
	}
}
//删除相同房号namelist
int DeleteSameNamelist_Record(void)
{
	int i;
	IM_NameListRecord_T tempRecord;
	for(i = 0; i < imNameListTable->record_cnt; i++)
	{
		GetImNameListRecordItems(i, &tempRecord);
		if(!strcmp(tempRecord.BD_RM_MS, GetSysVerInfo_BdRmMs()))
		{
			delete_one_vtk_record(imNameListTable, i);
			return 0;
		}
	}
	return -1;
}


//清除namelist的所有记录
int ClearImNamelist_Table(void)
{
	delete_all_vtk_record(imNameListTable);

	SaveImNamelistTableToFile();
	
	return 0;
}

//搜索生成监视列表
int AddImNamelist_TableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, ret;
	
	API_SearchIpByFilter("0", TYPE_IM, SearchDeviceRecommendedWaitingTime, 30, &searchOnlineListData);
	
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{

			int index;
			IM_NameListRecord_T tempRecord;
			one_vtk_dat vtkRecord;
			char data[200];

			//从分机忽略掉
			if(atoi(devInfo.BD_RM_MS+8) > 1)
			{
				continue;
			}

			//本户的其他分机忽略掉
			if(!memcmp(devInfo.BD_RM_MS, GetSysVerInfo_BdRmMs(), 8))
			{
				continue;
			}

			index = SearchImNamelistTableByBdRmMs(devInfo.MFG_SN, &tempRecord);
			
			if(index >= 0)
			{
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, tempRecord.R_Name);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = data;
				
				Modify_one_vtk_record(&vtkRecord, imNameListTable, index);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, "-");
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = malloc(vtkRecord.len);
				memcpy(vtkRecord.pdat, data, vtkRecord.len);
			
				add_one_vtk_record(imNameListTable, &vtkRecord);
				ret++;
			}
		}
	}
		
	SaveImNamelistTableToFile();
	return ret;
}

//保存列表到文件
int SaveImNamelistTableToFile(void)
{
	int keyname_index;
	
	//R_NAME
	keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_R_NAME);	
	
	AlphabeticalOrder_vtk_table(imNameListTable, keyname_index);

	remove(IM_NAME_LIST_TABLE_NAME);
	
	if(save_vtk_table_file_buffer(imNameListTable, 0, imNameListTable->record_cnt, IM_NAME_LIST_TABLE_NAME))
	{
		return -1;
	}
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
one_vtk_table* imNameListTempTable = NULL;

void ImNamelistTempTableCreate(void)
{
	if(imNameListTempTable == NULL)
	{
		int i;
		char ImTableKey[7][40] = {IM_TABLE_KEY_BD_RM_MS, IM_TABLE_KEY_GLOBAL, IM_TABLE_KEY_LOCAL, IM_TABLE_KEY_NAME, IM_TABLE_KEY_R_NAME, IM_TABLE_KEY_DEVICE_TYPE, IM_TABLE_KEY_REMARK};

		imNameListTempTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &imNameListTempTable->lock, 0);
		
		imNameListTempTable->keyname_cnt = 7;
		imNameListTempTable->pkeyname_len = malloc(imNameListTempTable->keyname_cnt*sizeof(int));
		imNameListTempTable->pkeyname = malloc(imNameListTempTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< imNameListTempTable->keyname_cnt; i++)
		{
			imNameListTempTable->pkeyname_len[i] = 40;
			imNameListTempTable->pkeyname[i].len = strlen(&ImTableKey[i][0]);
			imNameListTempTable->pkeyname[i].pdat = malloc(imNameListTempTable->pkeyname[i].len);
			memcpy(imNameListTempTable->pkeyname[i].pdat, &ImTableKey[i][0], imNameListTempTable->pkeyname[i].len);
		}

		imNameListTempTable->precord = NULL;
		imNameListTempTable->record_cnt = 0;
	}
}

void ImNamelistTempTableDelete(void)
{
	free_vtk_table_file_buffer(imNameListTempTable);
	imNameListTempTable = NULL;
}


int GetImNamelistTempTableNum(void)
{
	return (imNameListTempTable == NULL) ? 0 : imNameListTempTable->record_cnt;
}

int GetImNameListTempRecordItems( int index,  IM_NameListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAMELIST_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( imNameListTempTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->name1,input_str);

		//R_NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAMELIST_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);
		*/

		return 0;
	}
	else
		return -1;
}

//搜索生成临时监视列表
int AddImNameListTempTableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, j, ret;
	IM_NameListRecord_T	record1, record2;
	one_vtk_dat vtkRecord;
	one_vtk_dat tempRecord;
	char data[200];
	char temp[20];

	ImNamelistTempTableDelete();
	ImNamelistTempTableCreate();
	//dh_20190827_s
	API_Event_IoServer_InnerRead_All(OverUnitIntercom,temp);//IM跨单元的Intercom使能
	if(atoi(temp))
	{
		API_SearchIpByFilter("9999", TYPE_IM, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);
	}
	else
	{
		API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_IM, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);
	}
	//dh_20190827_e
	
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{
			//从分机忽略掉
			if(atoi(devInfo.BD_RM_MS+8) > 1)
			{
				continue;
			}

			//本户的其他分机忽略掉
			if(!memcmp(devInfo.BD_RM_MS, GetSysVerInfo_BdRmMs(), 8))
			{
				continue;
			}
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			if(devInfo.name2_utf8[0] == 0)
			{
				strcpy(devInfo.name2_utf8, "-");
			}
			/*
			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}
			*/
			
			//snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
			//											devInfo.name1, "-");
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s", devInfo.BD_RM_MS, devInfo.Global, devInfo.Local, devInfo.name1,
														devInfo.name2_utf8, "-", "-");
			
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(imNameListTempTable, &vtkRecord);
			ret++;
		}
	}
	
	//按房号从小到大排序
	for (i = 0; i < imNameListTempTable->record_cnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < imNameListTempTable->record_cnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			GetImNameListTempRecordItems(j, &record1);
			GetImNameListTempRecordItems(j, &record2);
			if (atoi(record1.BD_RM_MS) > atoi(record2.BD_RM_MS))
			{

				tempRecord = imNameListTempTable->precord[j+1];
				imNameListTempTable->precord[j+1] = imNameListTempTable->precord[j];
				imNameListTempTable->precord[j] = tempRecord;
			}
		}
	}
	
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
one_vtk_table* imNameListUpdateTable = NULL;

void ImNamelistUpdateTableCreate(void)
{
	if(imNameListUpdateTable == NULL)
	{
		int i;
		char ImTableKey[7][40] = {IM_TABLE_KEY_BD_RM_MS, IM_TABLE_KEY_GLOBAL, IM_TABLE_KEY_LOCAL, IM_TABLE_KEY_NAME, IM_TABLE_KEY_R_NAME, IM_TABLE_KEY_DEVICE_TYPE, IM_TABLE_KEY_REMARK};

		imNameListUpdateTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &imNameListUpdateTable->lock, 0);
		
		imNameListUpdateTable->keyname_cnt = 7;
		imNameListUpdateTable->pkeyname_len = malloc(imNameListUpdateTable->keyname_cnt*sizeof(int));
		imNameListUpdateTable->pkeyname = malloc(imNameListUpdateTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< imNameListUpdateTable->keyname_cnt; i++)
		{
			imNameListUpdateTable->pkeyname_len[i] = 40;
			imNameListUpdateTable->pkeyname[i].len = strlen(&ImTableKey[i][0]);
			imNameListUpdateTable->pkeyname[i].pdat = malloc(imNameListUpdateTable->pkeyname[i].len);
			memcpy(imNameListUpdateTable->pkeyname[i].pdat, &ImTableKey[i][0], imNameListUpdateTable->pkeyname[i].len);
		}

		imNameListUpdateTable->precord = NULL;
		imNameListUpdateTable->record_cnt = 0;
	}
}

void ImNamelistUpdateTableDelete(void)
{
	free_vtk_table_file_buffer(imNameListUpdateTable);
	imNameListUpdateTable = NULL;
}


int GetImNamelistUpdateTableNum(void)
{
	return (imNameListUpdateTable == NULL) ? 0 : imNameListUpdateTable->record_cnt;
}

int GetImNameListUpdateRecordItems( int index,  IM_NameListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAMELIST_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( imNameListUpdateTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->name1,input_str);

		//R_NAME
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAMELIST_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( imNameListTable, IM_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAMELIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);
		*/

		return 0;
	}
	else
		return -1;
}

//搜索生成临时监视列表
int AddImNameListUpdateTableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, j, ret;
	IM_NameListRecord_T	record1, record2;
	one_vtk_dat tempRecord;
	one_vtk_dat vtkRecord;
	char data[200];
	char temp[20];
	int overUnitIntercom;
	
	ImNamelistUpdateTableDelete();
	ImNamelistUpdateTableCreate();
	//dh_20190827_s
	API_Event_IoServer_InnerRead_All(OverUnitIntercom,temp);//IM跨单元的Intercom使能
	overUnitIntercom = atoi(temp);

	API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);

	//dh_20190827_e
			
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		if(searchOnlineListData.data[i].deviceType == TYPE_GL)
		{
			if(memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 4)!=0 && memcmp("00000000", searchOnlineListData.data[i].BD_RM_MS, 8)!=0)
			{
				continue;
			}
			if(api_nm_if_judge_include_dev(searchOnlineListData.data[i].BD_RM_MS,searchOnlineListData.data[i].Ip)==0)
			{
				continue;
			}

			//本户的其他分机忽略掉
			if(!memcmp(searchOnlineListData.data[i].BD_RM_MS, GetSysVerInfo_BdRmMs(), 8))
			{
				continue;
			}
		}
		else if(searchOnlineListData.data[i].deviceType == TYPE_IM)
		{
			//不允许跨单元，并且单元号不等本机
			if(!overUnitIntercom && memcmp(searchOnlineListData.data[i].BD_RM_MS, GetSysVerInfo_bd(), 4))
			{
				continue;
			}
			
			if(api_nm_if_judge_include_dev(searchOnlineListData.data[i].BD_RM_MS,searchOnlineListData.data[i].Ip)==0)
			{
				continue;
			}

			
			//从分机忽略掉
			if(atoi(searchOnlineListData.data[i].BD_RM_MS+8) > 1)
			{
				if(atoi(searchOnlineListData.data[i].BD_RM_MS+8)<51)
					continue;
			}

			//本户的其他分机忽略掉
			if(!memcmp(searchOnlineListData.data[i].BD_RM_MS, GetSysVerInfo_BdRmMs(), 8))
			{
				continue;
			}
		}
		else
		{
			continue;
		}
		
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s",
				devInfo.BD_RM_MS[0] ? devInfo.BD_RM_MS : "-",
				devInfo.Global[0] ? devInfo.Global : "-",
				devInfo.Local[0] ? devInfo.Local : "-",
				devInfo.name1[0] ? devInfo.name1 : "-",
				devInfo.name2_utf8[0] ? devInfo.name2_utf8 : "-",
				"-",
				"-");
			
			vtkRecord.len = strlen(data);
		
			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(imNameListUpdateTable, &vtkRecord);
			ret++;
		}
	}
	
	//按房号从小到大排序
	for (i = 0; i < imNameListUpdateTable->record_cnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < imNameListUpdateTable->record_cnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			GetImNameListUpdateRecordItems(j, &record1);
			GetImNameListUpdateRecordItems(j+1, &record2);
			if (atoi(record1.BD_RM_MS) > atoi(record2.BD_RM_MS))
			{
				tempRecord = imNameListUpdateTable->precord[j+1];
				imNameListUpdateTable->precord[j+1] = imNameListUpdateTable->precord[j];
				imNameListUpdateTable->precord[j] = tempRecord;
			}
		}
	}
	return ret;
}

int UpdateImNameListTable(void)
{
	AddImNameListUpdateTableBySearching();
	
	if(CompareTable(imNameListTempTable, imNameListUpdateTable))
	{
		CopyTable(&imNameListTempTable, imNameListUpdateTable);
		return 1;
	}
	else
	{
		return 0;
	}
}

//dh_20190822_s
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
one_vtk_table* imIntercomCookieTable = NULL;
#define COOKIE_USER_TABLE_NUM 	100

void ImCookieTableCreate(void)
{
	imIntercomCookieTable = load_vtk_table_file(IM_COOKIE_TABLE_NAME);
	if(imIntercomCookieTable == NULL)
	{
		int i;
		char ImTableKey[4][40] = {IM_TABLE_KEY_BD_RM_MS, IM_TABLE_KEY_FAVERITE, IM_TABLE_KEY_NICK_NAME, IM_TABLE_KEY_NAME};

		imIntercomCookieTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &imIntercomCookieTable->lock, 0);
		
		imIntercomCookieTable->keyname_cnt = 4;
		imIntercomCookieTable->pkeyname_len = malloc(imIntercomCookieTable->keyname_cnt*sizeof(int));
		imIntercomCookieTable->pkeyname = malloc(imIntercomCookieTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< imIntercomCookieTable->keyname_cnt; i++)
		{
			imIntercomCookieTable->pkeyname_len[i] = 40;
			imIntercomCookieTable->pkeyname[i].len = strlen(&ImTableKey[i][0]);
			imIntercomCookieTable->pkeyname[i].pdat = malloc(imIntercomCookieTable->pkeyname[i].len);
			memcpy(imIntercomCookieTable->pkeyname[i].pdat, &ImTableKey[i][0], imIntercomCookieTable->pkeyname[i].len);
		}

		imIntercomCookieTable->precord = NULL;
		imIntercomCookieTable->record_cnt = 0;
		save_vtk_table_file_buffer(imIntercomCookieTable, 0, imIntercomCookieTable->record_cnt, IM_COOKIE_TABLE_NAME);
	}
}

void ImCookieTableTableDelete(void)
{
	free_vtk_table_file_buffer(imIntercomCookieTable);
	imIntercomCookieTable = NULL;
}

int SaveImCookieTableToFile(void)
{
	int keyname_index;
	
	keyname_index = get_keyname_index( imIntercomCookieTable, IM_TABLE_KEY_BD_RM_MS);	
	
	AlphabeticalOrder_vtk_table(imIntercomCookieTable, keyname_index);

	remove(IM_COOKIE_TABLE_NAME);
	
	if(save_vtk_table_file_buffer(imIntercomCookieTable, 0, imIntercomCookieTable->record_cnt, IM_COOKIE_TABLE_NAME))
	{
		return -1;
	}
	
	return 0;
}

int GetImCookieTableTableNum(void)
{
	return (imIntercomCookieTable == NULL) ? 0 : imIntercomCookieTable->record_cnt;
}
void GetImCookieRecordItems(int index, IM_CookieRecord_T* record)
{
	unsigned char 	input_str[MAX_NAMELIST_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( imIntercomCookieTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( imIntercomCookieTable, IM_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//FAVERITE
		keyname_index = get_keyname_index( imIntercomCookieTable, IM_TABLE_KEY_FAVERITE);		
		input_str_len = 1;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		record->faverite = atoi(input_str);
		
		//NICK_NAME
		keyname_index = get_keyname_index( imIntercomCookieTable, IM_TABLE_KEY_NICK_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->NickName,input_str);
		
		//NAME
		keyname_index = get_keyname_index( imIntercomCookieTable, IM_TABLE_KEY_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->name,input_str);
	}	
}
int GetImCookieFaveriteNum(char* index)
{
	int i;
	IM_CookieRecord_T record;
	int FaveriteNum = 0;
	for(i=0 ; i< imIntercomCookieTable->record_cnt; i++)
	{
		GetImCookieRecordItems(i,&record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}

int ImGetCookieItemByAddr(char* BD_RM_MS, IM_CookieRecord_T* record)
{
	int i;
	IM_CookieRecord_T tempRecord;
	if(imIntercomCookieTable->record_cnt == 0)
		return -1;
	//printf("ImGetCookieItemByAddr : %s\n",BD_RM_MS);
	for(i=0 ; i< imIntercomCookieTable->record_cnt; i++)
	{
		GetImCookieRecordItems(i,&tempRecord);
		if(!strcmp(tempRecord.BD_RM_MS, BD_RM_MS))
		{
			*record = tempRecord;
			return 0;
		}
	}
	return -1;
}

void ImCookieModify(char* BD_RM_MS, int Faverite, char *Nname, char *name)
{
	int i;
	char data[200] = {0};
	one_vtk_dat vtkRecord;
	IM_CookieRecord_T record;
	if(imIntercomCookieTable->record_cnt >= COOKIE_USER_TABLE_NUM)
		return;
	for(i=0 ; i< imIntercomCookieTable->record_cnt; i++)
	{
		GetImCookieRecordItems(i,  &record);
		if(!strcmp(record.BD_RM_MS, BD_RM_MS))
		{
			break;
		}
	}
	if(i < imIntercomCookieTable->record_cnt) //modify
	{
		if(Nname != NULL)
		{	
			if(!strcmp(Nname, "-") && !record.faverite)
			{
				delete_one_vtk_record(imIntercomCookieTable, i);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s", record.BD_RM_MS, record.faverite, Nname, record.name);			
				vtkRecord.len = strlen(data);
				vtkRecord.pdat = data;
				Modify_one_vtk_record(&vtkRecord, imIntercomCookieTable, i);
			}
		}
		else
		{
			if(!strcmp(record.NickName, "-") && !Faverite)
			{
				delete_one_vtk_record(imIntercomCookieTable, i);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s", record.BD_RM_MS, Faverite, record.NickName, record.name);			
				vtkRecord.len = strlen(data);
				vtkRecord.pdat = data;
				Modify_one_vtk_record(&vtkRecord, imIntercomCookieTable, i);
			}
		}
	}
	else		//add
	{
		snprintf(data, 200, "%s,%d,%s,%s", BD_RM_MS, Faverite, (Nname == NULL? "-": Nname), name);			
		vtkRecord.len = strlen(data);
		vtkRecord.pdat = malloc(vtkRecord.len);
		memcpy(vtkRecord.pdat, data, vtkRecord.len);
	
		add_one_vtk_record(imIntercomCookieTable, &vtkRecord);
	}
	SaveImCookieTableToFile();
}
//dh_20190822_e

int GetImNamelistGLNum(void)
{
	int num = 0;
	one_vtk_table *pimtb = NULL;
	int glnum = 0;
	one_vtk_dat *precord;
	int i;
	
	if(imNameListTable!=NULL&&imNameListTable->record_cnt>0)
	{
		num = imNameListTable->record_cnt;
		pimtb = imNameListTable;
		//printf("GetImNamelistGLNum 1111111111\n");
	}
	else if(imNameListTempTable!=NULL&&imNameListTempTable->record_cnt>0)
	{
		num = imNameListTempTable->record_cnt;
		pimtb = imNameListTempTable;
		//printf("GetImNamelistGLNum 2222222222\n");
	}
	if(pimtb!=NULL&&num>0)
	{
		//printf("GetImNamelistGLNum 33333333%d\n",num);
		int keyname_index = get_keyname_index( pimtb, IM_TABLE_KEY_BD_RM_MS);		
		int input_str_len = 10;	
		char input_str[20];
		
		
		for(i = 0;i<num;i++)
		{
			precord = get_one_vtk_record_without_keyvalue(pimtb, i);
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			//input_str_len = input_str_len > 10 ? 10 : input_str_len;
			//input_str[input_str_len] = 0;
			//printf("GetImNamelistGLNum 444444444%s\n",input_str);
			if(memcmp(input_str+4,"0000",4)!=0)
				break;
			glnum++;
			//printf("GetImNamelistGLNum 555555555\n");
		}
	}
	return glnum;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

