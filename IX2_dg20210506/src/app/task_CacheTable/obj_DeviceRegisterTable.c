/**
  ******************************************************************************
  * @file    obj_DeviceRegisterTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_DeviceRegisterTable.h"
#include "../task_io_server/obj_TableProcess.h"




one_vtk_table* deviceRegisterTable;

//加载设备注册表
void LoadDeviceRegisterTable(void)
{
	deviceRegisterTable = load_vtk_table_file(DEVICE_REGISTER_TABLE_NAME);
	if(deviceRegisterTable == NULL)
	{
		deviceRegisterTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &deviceRegisterTable->lock, 0);		//czn_20190327
		deviceRegisterTable->keyname_cnt = 7;
		deviceRegisterTable->pkeyname_len = malloc(deviceRegisterTable->keyname_cnt*sizeof(int));
		deviceRegisterTable->pkeyname = malloc(deviceRegisterTable->keyname_cnt*sizeof(one_vtk_dat));
		deviceRegisterTable->pkeyname_len[0] = 40;
		deviceRegisterTable->pkeyname_len[1] = 40;
		deviceRegisterTable->pkeyname_len[2] = 40;
		deviceRegisterTable->pkeyname_len[3] = 40;
		deviceRegisterTable->pkeyname_len[4] = 40;
		deviceRegisterTable->pkeyname_len[5] = 40;
		deviceRegisterTable->pkeyname_len[6] = 40;
		deviceRegisterTable->pkeyname[0].len = strlen("MFG_SN");
		deviceRegisterTable->pkeyname[0].pdat = malloc(deviceRegisterTable->pkeyname[0].len);
		memcpy(deviceRegisterTable->pkeyname[0].pdat, "MFG_SN", deviceRegisterTable->pkeyname[0].len);
		
		deviceRegisterTable->pkeyname[1].len = strlen("TYPE");
		deviceRegisterTable->pkeyname[1].pdat = malloc(deviceRegisterTable->pkeyname[1].len);
		memcpy(deviceRegisterTable->pkeyname[1].pdat, "TYPE", deviceRegisterTable->pkeyname[1].len);

		deviceRegisterTable->pkeyname[2].len = strlen("ADD_TIME");
		deviceRegisterTable->pkeyname[2].pdat = malloc(deviceRegisterTable->pkeyname[2].len);
		memcpy(deviceRegisterTable->pkeyname[2].pdat, "ADD_TIME", deviceRegisterTable->pkeyname[2].len);

		deviceRegisterTable->pkeyname[3].len = strlen("RM_ADDR");
		deviceRegisterTable->pkeyname[3].pdat = malloc(deviceRegisterTable->pkeyname[3].len);
		memcpy(deviceRegisterTable->pkeyname[3].pdat, "RM_ADDR", deviceRegisterTable->pkeyname[3].len);

		deviceRegisterTable->pkeyname[4].len = strlen("NAME");
		deviceRegisterTable->pkeyname[4].pdat = malloc(deviceRegisterTable->pkeyname[4].len);
		memcpy(deviceRegisterTable->pkeyname[4].pdat, "NAME", deviceRegisterTable->pkeyname[4].len);

		deviceRegisterTable->pkeyname[5].len = strlen("GLOBAL");
		deviceRegisterTable->pkeyname[5].pdat = malloc(deviceRegisterTable->pkeyname[5].len);
		memcpy(deviceRegisterTable->pkeyname[5].pdat, "GLOBAL", deviceRegisterTable->pkeyname[5].len);

		deviceRegisterTable->pkeyname[6].len = strlen("LOCAL");
		deviceRegisterTable->pkeyname[6].pdat = malloc(deviceRegisterTable->pkeyname[6].len);
		memcpy(deviceRegisterTable->pkeyname[6].pdat, "LOCAL", deviceRegisterTable->pkeyname[6].len);

		deviceRegisterTable->record_cnt = 0;
		
		save_vtk_table_file_buffer( deviceRegisterTable, 0, deviceRegisterTable->record_cnt, DEVICE_REGISTER_TABLE_NAME);
	}
}

static int VtkTableRecordToDeviceRegisterRecord(int index, DeviceRegisterRecord_T* pRecord)
{
	one_vtk_dat* tempRecord;
	char data[200];
	int dataIndex, len;
	
	if(deviceRegisterTable == NULL)
	{
		return -1;
	}

	if(index >= deviceRegisterTable->record_cnt)
	{
		return -1;
	}
	tempRecord = get_one_vtk_record_without_keyvalue(deviceRegisterTable, index);

	memset(pRecord, 0, sizeof(DeviceRegisterRecord_T));
	//取MFG_SN
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 0, data, &len);
	len = len > 12 ? 12 : len;
	data[len] = 0;
	strcpy(pRecord->MFG_SN, data);


	//取deviceType
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 1, data, &len);
	data[len] = 0;
	pRecord->deviceType = strtol(data,NULL,0);
	
	//取加入时间
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 2, data, &len);
	data[len] = 0;
	strcpy(pRecord->addTime, data);;

	//取BD_RM_MS
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 3, data, &len);
	len = len > 10 ? 10 : len;
	data[len] = 0;
	strcpy(pRecord->BD_RM_MS, data);
	
	//取name
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 4, data, &len);
	len = len > 20 ? 20 : len;
	data[len] = 0;
	strcpy(pRecord->name, data);

	//取Global
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 5, data, &len);
	len = len > 10 ? 10 : len;
	data[len] = 0;
	strcpy(pRecord->Global, data);

	//取Local
	len = 100; data[0] = 0;
	get_one_record_string(tempRecord, 6, data, &len);
	len = len > 6 ? 6 : len;
	data[len] = 0;
	strcpy(pRecord->Local, data);

	return 0;
}

static one_vtk_dat* DeviceRegisterRecordToVtkTableRecord(DeviceRegisterRecord_T record)
{
	char data[200];
	one_vtk_dat* ret;

	snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", record.MFG_SN, record.deviceType, record.addTime, record.BD_RM_MS, record.name, record.Global, record.Local);

	ret = malloc( sizeof(one_vtk_dat) );
	ret->len = strlen(data);
	ret->pdat = malloc(ret->len);
	memcpy(ret->pdat, data, ret->len);
	
	return ret;
}


//增加设备注册表的一条记录
int AddOneDeviceRegisterRecord(DeviceRegisterRecord_T record)
{
	int i;
	DeviceRegisterRecord_T tempRecord;
	one_vtk_dat* vtkRecord;

	vtkRecord = DeviceRegisterRecordToVtkTableRecord(record);
	
	for(i = 0; i < deviceRegisterTable->record_cnt; i++)
	{
		VtkTableRecordToDeviceRegisterRecord(i, &tempRecord);
		if(!memcmp(tempRecord.MFG_SN, record.MFG_SN, 12))
		{
			Modify_one_vtk_record(vtkRecord, deviceRegisterTable, i);
			free(vtkRecord->pdat);
			free(vtkRecord);

			remove(DEVICE_REGISTER_TABLE_NAME);
			save_vtk_table_file_buffer( deviceRegisterTable, 0, deviceRegisterTable->record_cnt, DEVICE_REGISTER_TABLE_NAME);
			return 0;
		}
	}

	add_one_vtk_record(deviceRegisterTable, vtkRecord);
	free(vtkRecord);

	remove(DEVICE_REGISTER_TABLE_NAME);
	save_vtk_table_file_buffer( deviceRegisterTable, 0, deviceRegisterTable->record_cnt, DEVICE_REGISTER_TABLE_NAME);
	return 0;
}


//读取设备注册表的一条记录
int GetOneDeviceRegisterRecord(int index, DeviceRegisterRecord_T* pRecord)
{
	DeviceRegisterRecord_T tempRecord;

	VtkTableRecordToDeviceRegisterRecord(index, &tempRecord);

	*pRecord = tempRecord;

	return 0;
}


//搜索设备注册表的一条记录
int SearchDeviceRegisterTableByMFG_SN(char* MFG_SN, DeviceRegisterRecord_T* pRecord)
{
	int i;
	DeviceRegisterRecord_T tempRecord;

	for(i = 0; i < deviceRegisterTable->record_cnt; i++)
	{
		GetOneDeviceRegisterRecord(i, &tempRecord);
		
		if(!memcmp(tempRecord.MFG_SN, MFG_SN, 12))
		{
			*pRecord = tempRecord;
			return i;
		}
	}

	return -1;
}

//删除设备注册表的一条记录
int DeleteOneDeviceRegisterRecord(char* MFG_SN, int index)
{
	DeviceRegisterRecord_T record;
	
	if(MFG_SN != NULL)
	{
		index = SearchDeviceRegisterTableByMFG_SN(MFG_SN, &record);
	}
	
	if(index >= 0 && index < deviceRegisterTable->record_cnt)
	{
		delete_one_vtk_record(deviceRegisterTable, index);

		remove(DEVICE_REGISTER_TABLE_NAME);
		save_vtk_table_file_buffer( deviceRegisterTable, 0, deviceRegisterTable->record_cnt, DEVICE_REGISTER_TABLE_NAME);
		return 0;
	}
	else
	{
		return -1;
	}
}

//清除设备注册表的所有记录
int ClearDeviceRegisterTable(void)
{
	delete_all_vtk_record(deviceRegisterTable);

	remove(DEVICE_REGISTER_TABLE_NAME);
	save_vtk_table_file_buffer( deviceRegisterTable, 0, deviceRegisterTable->record_cnt, DEVICE_REGISTER_TABLE_NAME);
	
	return 0;
}

//获取设备注册表记录数
int GetDeviceRegisterTableCnt(void)
{
	if(deviceRegisterTable == NULL)
	{
		return 0;
	}
	else
	{
		return deviceRegisterTable->record_cnt;
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

