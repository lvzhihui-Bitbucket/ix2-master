/**
  ******************************************************************************
  * @file    obj_ImNameListTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_ImNameListTable_H
#define _obj_ImNameListTable_H

// Define Object Property-------------------------------------------------------
#define IM_TABLE_KEY_MFG_SN				"MFG_SN"
#define IM_TABLE_KEY_DEVICE_TYPE		"DEVICE_TYPE"
#define IM_TABLE_KEY_GLOBAL				"GLOBAL"
#define IM_TABLE_KEY_LOCAL				"LOCAL"
#define IM_TABLE_KEY_BD_RM_MS			"BD_RM_MS"
#define IM_TABLE_KEY_NAME				"NAME"
#define IM_TABLE_KEY_R_NAME				"R_NAME"
#define IM_TABLE_KEY_REMARK			"REMARK"
#define IM_TABLE_KEY_NICK_NAME		"NICK_NAME" //昵称
#define IM_TABLE_KEY_FAVERITE			"FAVERITE"  //收藏

#define MAX_NAMELIST_NAME_LEN			40

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	MFG_SN[12+1];		//MFG_SN
	int		deviceType;			//设备类型
	char	Global[10+1];		//Global
	char	Local[6+1];			//Local
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	name1[20+1];		//name1
	char	R_Name[40+1];		//R_Name
	int		upTime;				//上线时间
} IM_NameListRecord_T;

//dh_20190822_s
typedef struct
{
	char	BD_RM_MS[10+1];		//BD_RM_MS
	int	faverite;					//收藏
	char	NickName[20+1];			//昵称
	char	name[20+1];			//name
}IM_CookieRecord_T;
//dh_20190822_e

#pragma pack()



// Define Object Function - Public----------------------------------------------

//加载表
void LoadImNameListTable(void);

// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetImNameListRecordItems( int index,  IM_NameListRecord_T* record);


//取记录数量
int GetImNameListRecordCnt(void);

//修改NameList的一条记录
int ModifyOneImNamelistRecord(IM_NameListRecord_T* nameListRecord, int index);

//保存列表到文件
int SaveImNamelistTableToFile(void);

int UpdateImNameListTable(void);

// Define Object Function - Private---------------------------------------------




#endif


