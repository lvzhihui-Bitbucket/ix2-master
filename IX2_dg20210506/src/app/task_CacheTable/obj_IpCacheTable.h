/**
  ******************************************************************************
  * @file    obj_IpCacheTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_IpCacheTable_H
#define _obj_IpCacheTable_H

#include "obj_PublicCmdDefine.h"

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	BD_RM_MS[11];	//BD_RM_MS
	int 	ip;				//ip地址
} IpCacheRecord_T;


typedef struct
{
	int					record_cnt;		//记录数
	IpCacheRecord_T*	pRecord;		//记录
} IpCacheTable_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------

//加载表
void LoadIpCacheTable(void);

//判断是否有全局设备表
//0:无表; 1:有表
int hasIpCacheTable(void);

//增加记录到ip cache表
int AddOneCacheRecordToTable(IpCacheRecord_T record);

//搜索ip cache表
int SearchIpCacheTableByBdRmMs(char* bd_rm_ms, IpCacheRecord_T record[]);


// Define Object Function - Private---------------------------------------------


#endif


