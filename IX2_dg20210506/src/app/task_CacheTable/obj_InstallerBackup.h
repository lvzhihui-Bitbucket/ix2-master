/**
  ******************************************************************************
  * @file    obj_InstallerBackup.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

  
#ifndef obj_InstallerBackup_H
#define obj_InstallerBackup_H

#define 	INSTALLER_BACKUP_FILE_HEAD						"FILE_HEAD"
#define 	INSTALLER_BACKUP_DEVICE_TYPE					"DEVICE_TYPE"
#define 	INSTALLER_BACKUP_MFG_SN							"MFG_SN"
#define 	INSTALLER_BACKUP_DEVICE_ADDR					"DEVICE_ADDR"
#define 	INSTALLER_BACKUP_ROOM_NUMBER					"ROOM_NUMBER"

#define 	INSTALLER_BACKUP_USER_GROUP						"USER_GROUP"

#define 	INSTALLER_BACKUP_IO_PARA						"IO_PARA"
#define 	INSTALLER_BACKUP_IO_NUM_INDEX					0
#define 	INSTALLER_BACKUP_IO_VALUE_INDEX					1


#define 	INSTALLER_BACKUP_FILE							"FILE"
#define 	INSTALLER_BACKUP_FILE_NAME_INDEX				0
#define 	INSTALLER_BACKUP_FILE_CONTENT_INDEX				1


#define 	INSTALLER_BACKUP_DELETE_FILE					"DELETE_FILE"



#endif
