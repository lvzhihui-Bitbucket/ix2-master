/*
  Copyright (c) 2009-2017 Dave Gamble and cJSON contributors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "cJSON.h"
#include "obj_InstallerBackup.h"
#include "utility.h"
#include "define_file.h"
#include "obj_ResSync.h"
#include "task_IxProxy.h"


static void RestoreJsonStringToFile(const char* filePath, const char* string)
{
	#define WRITE_BUFFER_LEN	200
	
	FILE	*file = NULL;
	int i, j, len;
	char writeBuff[WRITE_BUFFER_LEN];
	
	if((file=fopen(filePath,"w+")) != NULL)
	{
		fputs(string, file);

		fclose(file);
		
		snprintf(writeBuff, WRITE_BUFFER_LEN, "chmod 777 %s", filePath);
		system(writeBuff);
	}
}



static int InstallerBackupProcess(const char* srcFile, const char* bakFile)
{
	const cJSON *IO_PARA_S = NULL;
	const cJSON *IO_PARA = NULL;
	const cJSON *FILE_S = NULL;
	const cJSON *FILE = NULL;
	const cJSON *GROUP = NULL;
	cJSON *backupObject;
	int groupCnt, ioCnt, fileCnt;
	int i, j;
	
	char ioValue[1000];

	int ret;
	char* json;

	//参数错误
	if(srcFile == NULL || bakFile == NULL)
	{
		dprintf("Error:%s,%s\n", srcFile, bakFile);
		return -1;
	}
	
	json = GetJsonStringFromFile(srcFile);
	//没有config文件
	if(json != NULL)
	{
		backupObject = cJSON_Parse(json);
		free(json);
		
		if (backupObject != NULL)
		{
			cJSON_ReplaceItemInObjectCaseSensitive(backupObject, INSTALLER_BACKUP_FILE_HEAD, cJSON_CreateString(elog_port_get_time()));
			cJSON_ReplaceItemInObjectCaseSensitive(backupObject, INSTALLER_BACKUP_DEVICE_TYPE, cJSON_CreateString(GetSysVerInfo_type()));
			cJSON_ReplaceItemInObjectCaseSensitive(backupObject, INSTALLER_BACKUP_MFG_SN, cJSON_CreateString(GetSysVerInfo_Sn()));
			cJSON_ReplaceItemInObjectCaseSensitive(backupObject, INSTALLER_BACKUP_DEVICE_ADDR, cJSON_CreateString(GetSysVerInfo_BdRmMs()));
			groupCnt = cJSON_GetArraySize(backupObject);

			for(i = 0; i < groupCnt; i++)
			{
				GROUP = cJSON_GetArrayItem(backupObject, i);
				
				IO_PARA_S = cJSON_GetObjectItemCaseSensitive(GROUP, INSTALLER_BACKUP_IO_PARA);
				
				ioCnt = cJSON_GetArraySize(IO_PARA_S);
				for(j = 0; j < ioCnt; j++)
				{
					IO_PARA = cJSON_GetArrayItem(IO_PARA_S, j);
					
					if(API_Event_IoServer_InnerRead_All(IO_PARA->string, ioValue) == 0)
					{
						cJSON_ReplaceItemInObjectCaseSensitive(IO_PARA_S, IO_PARA->string, cJSON_CreateString(ioValue));
					}
					else
					{
						dprintf("Io read error: %s\n", IO_PARA->string);
					}
				}

				FILE_S = cJSON_GetObjectItemCaseSensitive(GROUP, INSTALLER_BACKUP_FILE);
				
				fileCnt = cJSON_GetArraySize(FILE_S);
				for(j = 0; j < fileCnt; j++)
				{
					//需要增加

				}
			}
			
			json = cJSON_Print(backupObject);
			
			MakeDir(INSTALLER_BAK_PATH);
			SetJsonStringToFile(bakFile, json);
			free(json);
			
			cJSON_Delete(backupObject);


			ret = 0;
		}
		else
		{
			dprintf("Json Error:%s\n", srcFile);
			ret = -3;
		}
	}
	//config文件打开正确
	else
	{
		dprintf("Has not file:%s\n", srcFile);
		ret = -2;
	}

	return ret;
}

int InstallerRestoreProcess(const char* restoreFile, int checkSn, int updateAddr)
{
	int ret;
	char* json;

	const cJSON *IO_PARA_S = NULL;
	const cJSON *IO_PARA = NULL;
	const cJSON *FILE_S = NULL;
	const cJSON *FILE = NULL;
	const cJSON *GROUP = NULL;
	cJSON *backupObject;

	const cJSON *deviceType = NULL;
		
	int groupCnt, ioCnt, fileCnt;
	int i, j, k;

	//dprintf("restoreFile=%s, checkSn=%d,updateAddr=%d\n", restoreFile, checkSn, updateAddr);
		
	json = GetJsonStringFromFile(restoreFile);
	//没有config文件
	if(json != NULL)
	{
		backupObject = cJSON_Parse(json);
		free(json);
		
		if (backupObject != NULL)
		{
			//设备类型不相等
			if(strcmp(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(backupObject, INSTALLER_BACKUP_DEVICE_TYPE)), GetSysVerInfo_type()))
			{
				dprintf("%s ERROR\n", INSTALLER_BACKUP_DEVICE_TYPE);
				ret = -3;
				goto RestoreOver;
			}
			
			//设备序列号不允许
			if(checkSn && strcmp(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(backupObject, INSTALLER_BACKUP_MFG_SN)), GetSysVerInfo_Sn()))
			{
				dprintf("Check SN ERROR:bak SN=%s, My SN=%s\n", cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(backupObject, INSTALLER_BACKUP_MFG_SN)), GetSysVerInfo_Sn());
				ret = -4;
				goto RestoreOver;
			}

			//更改房号
			if(updateAddr)
			{
				SetSysVerInfo_BdRmMs(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(backupObject, INSTALLER_BACKUP_DEVICE_ADDR)));
				dprintf("SetSysVerInfo_BdRmMs %s\n", cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(backupObject, INSTALLER_BACKUP_DEVICE_ADDR)));
			}

			groupCnt = cJSON_GetArraySize(backupObject);
			for(i = 4; i < groupCnt; i++)
			{
				GROUP = cJSON_GetArrayItem(backupObject, i);
				
				//更新房号和房号组
				if(!updateAddr && !strcmp(GROUP->string, INSTALLER_BACKUP_ROOM_NUMBER))
				{
					dprintf("Ignore %s.\n", INSTALLER_BACKUP_ROOM_NUMBER);
					continue;
				}

				IO_PARA_S = cJSON_GetObjectItemCaseSensitive(GROUP, INSTALLER_BACKUP_IO_PARA);
				
				ioCnt = cJSON_GetArraySize(IO_PARA_S);
				for(j = 0; j < ioCnt; j++)
				{
					IO_PARA = cJSON_GetArrayItem(IO_PARA_S, j);
					if(IO_PARA->valuestring[0] != 0)
					{
						API_Event_IoServer_InnerWrite_All(IO_PARA->string, IO_PARA->valuestring);
					}
				}

				FILE_S = cJSON_GetObjectItemCaseSensitive(GROUP, INSTALLER_BACKUP_FILE);
				
				fileCnt = cJSON_GetArraySize(FILE_S);
				for(j = 0; j < ioCnt; j++)
				{
					//需要增加

				}
			}
					
			ret = 0;
		}
		else
		{
			dprintf("Json Error:%s\n", restoreFile);
			ret = -2;
		}
	}
	//config文件打开正确
	else
	{
		dprintf("Has not file:%s\n", restoreFile);
		ret = -1;
	}

	RestoreOver:

	if(backupObject != NULL)
	{
		cJSON_Delete(backupObject);
	}

	if(ret == 0)
	{
		API_io_server_save_data_file();
		usleep(100*1000);
	}

	return ret;
}

char* GetBakFileByNumber(int bakNumber)
{
	char* filePath = NULL;

	switch(bakNumber)
	{
		case 0:
			if(IsFileExist(INSTALLER_BAK_Customerized_FILE))
			{
				filePath = INSTALLER_BAK_Customerized_FILE;
			}
			else
			{
				filePath = INSTALLER_BAK_CONFIG_FILE;
			}
			break;
		case 1:
			filePath = INSTALLER_FILE_BAK1;
			break;
		case 2:
			filePath = INSTALLER_FILE_BAK2;
			break;
		default:
			break;
	}
	
	return filePath;
}

int BackupInstallerBak(int bakSelect)
{
	return InstallerBackupProcess(GetBakFileByNumber(0), GetBakFileByNumber(bakSelect));
}

int IsInstallerBakExist(int bakSelect)
{
	return IsFileExist(GetBakFileByNumber(bakSelect));
}

void DeleteInstallerBak(int bakSelect)
{
	 remove(GetBakFileByNumber(bakSelect));
	 sync();
}

int RestoreBak(int bakSelect)
{
	int checkSn;
	int updateAddr;
	
	switch(bakSelect)
	{
		case 0:
			checkSn = 0;
			updateAddr = 1;
			break;
			
		case 1:
			checkSn = 1;
			updateAddr = 1;
			break;
			
		case 2:
			checkSn = 0;
			updateAddr = 0;
			break;
	}
	
	return InstallerRestoreProcess(GetBakFileByNumber(bakSelect), checkSn, updateAddr);
}

int RestoreByOtherDeviceBak1(int ip)
{	
	char bak1dir[100];
	char bak1fileName[100];
	char bak2dir[100];
	char bak2fileName[100];

	strcpy(bak1dir, GetBakFileByNumber(1));
	strcpy(bak1fileName, GetBakFileByNumber(1));
	strcpy(bak2dir, GetBakFileByNumber(2));
	strcpy(bak2fileName, GetBakFileByNumber(2));

	if(Api_TftpReadFileFromDevice(ip, FILE_MANAGE_RID, dirname(bak1dir), basename(bak1fileName), dirname(bak2dir), basename(bak2fileName)) == 0)
	{
		if(RestoreBak(2) == 0)
		{
			return 1;
		}
	}
	
	return 0;
}

//userOrInstaller  1：恢复用户参数默认值，0：恢复安装参数默认值
int RestoreDefaults(cJSON *ioValue, int userOrInstaller)
{
	int ret;
	const cJSON *IO_PARA_S = NULL;
	const cJSON *IO_PARA = NULL;
	const cJSON *GROUP = NULL;
	cJSON *backupObject;
	char* json;
	int groupCnt, paraCnt;
	int i, j;
	int hasThisPara;
	char* willDeleteItem[500];
	int willDeleteCnt;

	json = GetJsonStringFromFile(GetBakFileByNumber(0));
	//没有config文件
	if(json != NULL)
	{
		backupObject = cJSON_Parse(json);
		free(json);
		if (backupObject != NULL)
		{
			paraCnt = cJSON_GetArraySize(ioValue);
			for(willDeleteCnt = 0, j = 0; j < paraCnt; j++)
			{
				IO_PARA = cJSON_GetArrayItem(ioValue, j);
				if(IO_PARA != NULL)
				{
					groupCnt = cJSON_GetArraySize(backupObject);
					for(hasThisPara = 0, i = 0; i < groupCnt; i++)
					{
						GROUP = cJSON_GetArrayItem(backupObject, i);
						
						IO_PARA_S = cJSON_GetObjectItemCaseSensitive(GROUP, INSTALLER_BACKUP_IO_PARA);
						if(IO_PARA_S != NULL)
						{
							if(cJSON_HasObjectItem(IO_PARA_S, IO_PARA->string))
							{
								hasThisPara = 1;
								break;
							}
						}
					}
					
					if((hasThisPara && !userOrInstaller) || (!hasThisPara && userOrInstaller))
					{
						//标记即将被删除的参数
						willDeleteItem[willDeleteCnt++] = IO_PARA->string;
					}
				}
			}
			cJSON_Delete(backupObject);
			
			for(i = 0; i < willDeleteCnt; i++)
			{
				cJSON_DeleteItemFromObjectCaseSensitive(ioValue, willDeleteItem[i]);
			}
			
			ret = 0;
		}
		else
		{
			dprintf("Json Error:%s\n", GetBakFileByNumber(0));
			ret = -2;
		}
	}
	//config文件打开正确
	else
	{
		dprintf("Has not file:%s\n", GetBakFileByNumber(0));
		ret = -1;
	}
	
	return ret;
}


