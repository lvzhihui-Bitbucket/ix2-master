/**
  ******************************************************************************
  * @file    obj_OS_ListTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_OS_ListTable.h"
#include "obj_TableProcess.h"
#include "obj_GetIpByNumber.h"
#include "obj_GetInfoByIp.h"
#include "obj_GetAboutByIp.h"

one_vtk_table* OS_ListTempTable = NULL;

void OS_ListTempTableCreate(void)
{
	if(OS_ListTempTable == NULL)
	{
		int i;
		char OS_TableKey[10][40] = {OS_KEY_MFG_SN, OS_KEY_DEVICE_TYPE, OS_KEY_GLOBAL, OS_KEY_LOCAL, OS_KEY_BD_RM_MS, 
								   OS_KEY_NAME, OS_KEY_R_NAME, OS_KEY_LOCAL_SIP, OS_KEY_MON_CODE, OS_KEY_SIP_STATE};

		OS_ListTempTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &OS_ListTempTable->lock, 0);
		
		OS_ListTempTable->keyname_cnt = 10;
		OS_ListTempTable->pkeyname_len = malloc(OS_ListTempTable->keyname_cnt*sizeof(int));
		OS_ListTempTable->pkeyname = malloc(OS_ListTempTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< OS_ListTempTable->keyname_cnt; i++)
		{
			OS_ListTempTable->pkeyname_len[i] = 40;
			OS_ListTempTable->pkeyname[i].len = strlen(&OS_TableKey[i][0]);
			OS_ListTempTable->pkeyname[i].pdat = malloc(OS_ListTempTable->pkeyname[i].len);
			memcpy(OS_ListTempTable->pkeyname[i].pdat, &OS_TableKey[i][0], OS_ListTempTable->pkeyname[i].len);
		}

		OS_ListTempTable->precord = NULL;
		OS_ListTempTable->record_cnt = 0;
	}
}

void OS_ListTempTableDelete(void)
{
	free_vtk_table_file_buffer(OS_ListTempTable);
	OS_ListTempTable = NULL;
}


int GetOS_ListTempTableNum(void)
{
	if(OS_ListTempTable == NULL)
	{
		return 0;
	}
	else
	{
		return OS_ListTempTable->record_cnt;
	}
}

int GetOS_ListTempRecordItems( int index,  OS_ListRecord_T* record)
{
	unsigned char 	input_str[MAX_OS_LIST_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( OS_ListTempTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//MFG_SN
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_MFG_SN);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->MFG_SN,input_str);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_DEVICE_TYPE);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);

		//GLOBAL
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_GLOBAL);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_LOCAL);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Local,input_str, MAX_OS_LIST_NAME_LEN+1);
		
		//BD_RM_MS
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//NAME
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_NAME);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->name1,input_str);

		//R_NAME
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_R_NAME);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->R_Name,input_str);

		//OS_KEY_LOCAL_SIP
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_LOCAL_SIP);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->sipAccount,input_str);

		//OS_KEY_MON_CODE
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_MON_CODE);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strcpy(record->monCode,input_str);

		//OS_KEY_SIP_STATE
		keyname_index = get_keyname_index( OS_ListTempTable, OS_KEY_SIP_STATE);		
		input_str_len = MAX_OS_LIST_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		record->sipState = atoi(input_str);
		return 0;
	}
	else
		return -1;
}

//搜索生成临时监视列表
int AddOS_ListTempTableBySearching(void)
{
	GetIpRspData getIpdata;
	DeviceInfo devInfo;
	int i, ret;
	char temp[50];
	int sipState;
	char *pos1, *pos2;

	OS_ListTempTableDelete();
	OS_ListTempTableCreate();

	
	char BD_RM_MS[11];
	snprintf(BD_RM_MS, 11, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 2, 50, &getIpdata);
	
	
	for(i = 0, ret = 0; i < getIpdata.cnt; i++)
	{
		memcpy(BD_RM_MS, getIpdata.BD_RM_MS[i], 10);
		BD_RM_MS[10] = 0;
		if(atoi(BD_RM_MS+8) < 50)
		{
			continue;
		}
		if(api_nm_if_judge_include_dev(getIpdata.BD_RM_MS[i], getIpdata.Ip[i])==0)
		{
			 continue;
		}
		
		if(API_GetInfoByIp(getIpdata.Ip[i], 2, &devInfo) == 0)
		{
			one_vtk_dat vtkRecord;
			char data[200];
			
			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}

			sprintf(temp, "<ID=%03d>", ABOUT_ID_TransferState);
			API_GetAboutByIp(getIpdata.Ip[i], 2, temp);
			if((pos1 = strstr(temp, "Value=")) != NULL)
			{
				if((pos2 = strchr(temp, '>')) != NULL)
				{
					*pos2 = 0;
				}
				sipState = atoi(pos1 + strlen("Value="));
			}
			else
			{
				sipState = 0;
			}

			snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s,%s,%s,%d", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
														devInfo.name1, "-", devInfo.localSip, devInfo.monCode, sipState);
			
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(OS_ListTempTable, &vtkRecord);
			ret++;
		}

		
	}
	
	return ret;
}

int GetFreeOSNumber(void)
{
	int i;
	int number;
	int osCnt;
	int osNumber[100];
	OS_ListRecord_T data;

	osCnt = GetOS_ListTempTableNum();
	if(osCnt==0)
		return 51;
	if(osCnt >= 100)
	{
		return 50;
	}
	
	for(i = 0; i < osCnt; i++)
	{
		GetOS_ListTempRecordItems(i, &data);
		osNumber[i] = atoi(data.BD_RM_MS+8);
	}

	for(number = 51; number < 100; number++)
	{
		for(i = 0; i < osCnt; i++)
		{
			if(osNumber[i] == number)
			{
				break;
			}
		}

		if(i == osCnt)
		{
			break;
		}
	}

	return number;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

