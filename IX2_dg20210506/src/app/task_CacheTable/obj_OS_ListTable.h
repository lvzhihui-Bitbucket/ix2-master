/**
  ******************************************************************************
  * @file    obj_OS_ListTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_OS_ListTable_H
#define _obj_OS_ListTable_H

// Define Object Property-------------------------------------------------------
#define OS_KEY_MFG_SN				"MFG_SN"
#define OS_KEY_DEVICE_TYPE			"DEVICE_TYPE"
#define OS_KEY_GLOBAL				"GLOBAL"
#define OS_KEY_LOCAL				"LOCAL"
#define OS_KEY_BD_RM_MS				"BD_RM_MS"
#define OS_KEY_NAME					"NAME"
#define OS_KEY_R_NAME				"R_NAME"
#define OS_KEY_LOCAL_SIP			"SIP_ACCOUNT"
#define OS_KEY_MON_CODE				"MON_CODE"
#define OS_KEY_SIP_STATE			"SIP_STATE"

#define MAX_OS_LIST_NAME_LEN			50

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	MFG_SN[12+1];		//MFG_SN
	int		deviceType;			//设备类型
	char	Global[10+1];		//Global
	char	Local[6+1];			//Local
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	name1[20+1];		//name1
	char	R_Name[20+1];		//R_Name
	char	sipAccount[32+1];	//sip帐号
	char	monCode[4+1];		//
	int		sipState;			//
} OS_ListRecord_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------




#endif


