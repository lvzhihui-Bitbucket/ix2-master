/**
  ******************************************************************************
  * @file    obj_ioDataBackupTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_ioDataBackupTable.h"
#include "../task_io_server/obj_TableProcess.h"

#define IO_DATA_ADMIN_BACKUP_FILE_NAME					"/mnt/nand1-2/io_data_admin_backup_table.csv"
#define IO_DATA_USER_BACKUP_FILE_NAME					"/mnt/nand1-2/io_data_user_backup_table.csv"

one_vtk_table* ioDataAdminBackupTable = NULL;
one_vtk_table* ioDataUserBackupTable = NULL;

void IoDataBackupInit(void)
{
	ioDataAdminBackupTable = load_vtk_table_file(IO_DATA_ADMIN_BACKUP_FILE_NAME);
	ioDataUserBackupTable = load_vtk_table_file(IO_DATA_USER_BACKUP_FILE_NAME);
}


// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetIoDataBackupRecordItems(one_vtk_table* table, int index,  IoDataBackup_T* record)
{
	unsigned char 	input_str[MAX_IO_DATA_VALUE_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( table, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//PARA_ID
		keyname_index = get_keyname_index( table, PARA_ID);		
		input_str_len = MAX_IO_DATA_VALUE_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->paraId, input_str);
		
		//VALUE
		keyname_index = get_keyname_index( table, VALUE);		
		input_str_len = MAX_IO_DATA_VALUE_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->value, input_str);

		return 0;
	}
	else
		return -1;
}


int GetIoDataBackupRecordCnt(one_vtk_table* table)
{
	return (table == NULL ? 0 : table->record_cnt);
}

//重命名修改NameList的一条记录
void IoDataBackupModifyRecordCValue(one_vtk_table* table, int index, const char *pValue)
{
	char recordBuffer[200] = {0};
	one_vtk_dat vtkRecord;
	IoDataBackup_T record;
	
	GetIoDataBackupRecordItems(table, index,  &record);
	
	if(pValue != NULL && strlen(pValue) <= MAX_IO_DATA_VALUE_LEN)
	{	
		strcpy(record.value, pValue);
	
		snprintf(recordBuffer, 200, "%s,%s", record.paraId, record.value);
		
		vtkRecord.len = strlen(recordBuffer);
		vtkRecord.pdat = recordBuffer;
		
		Modify_one_vtk_record(&vtkRecord, table, index);
	}
}

int BackupIoDataBackupTable(one_vtk_table* table, const char* fileName)
{
	int i;
	IoDataBackup_T record;
	char recordBuffer[200] = {0};
	one_vtk_dat vtkRecord;
	
	if(fileName == NULL)
	{
		return 0;
	}
	
	for(i = 0; i < GetIoDataBackupRecordCnt(table); i++)
	{
		GetIoDataBackupRecordItems(table, i,  &record);
		API_Event_IoServer_InnerRead_All(record.paraId, record.value);
		snprintf(recordBuffer, 200, "%s,%s", record.paraId, record.value);
		
		vtkRecord.len = strlen(recordBuffer);
		vtkRecord.pdat = recordBuffer;
		Modify_one_vtk_record(&vtkRecord, table, i);
	}

	if(save_vtk_table_file_buffer(table, 0, GetIoDataBackupRecordCnt(table), fileName))
	{
		return -1;
	}
	
	return 0;
}

int RestoreIoDataBackupTable(const char* fileName)
{
	one_vtk_table* backupTable = NULL;
	int i;
	IoDataBackup_T record;
	
	if(fileName == NULL)
	{
		return 0;
	}

	
	backupTable = load_vtk_table_file(fileName);

	if(backupTable == NULL)
	{
		return 0;
	}

	for(i = 0; i < GetIoDataBackupRecordCnt(backupTable); i++)
	{
		GetIoDataBackupRecordItems(backupTable, i,  &record);
		
		API_Event_IoServer_InnerWrite_All(record.paraId, record.value);
	}
	
	API_io_server_save_data_file();

	free_vtk_table_file_buffer(backupTable);
	
	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

