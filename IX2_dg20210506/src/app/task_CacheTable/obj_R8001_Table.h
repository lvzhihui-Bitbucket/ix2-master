

#ifndef _obj_R8001_Table_H
#define _obj_R8001_Table_H

// Define Object Property-------------------------------------------------------

#define R8001_KEY_BD_RM_MS		"BD_RM_MS"
#define R8001_KEY_NAME1			"NAME"
#define R8001_KEY_LOCAL			"LOCAL"
#define R8001_KEY_GLOBAL			"GLOBAL"
#define R8001_KEY_DEVICE_TYPE		"DEVICE_TYPE"
#define R8001_KEY_MFG_SN			"MFG_SN"
#define R8001_KEY_NAME2			"NAME2_UTF8"
#define R8001_KEY_IP_STATIC		"IP_STATIC"
#define R8001_KEY_IP_ADDR			"IP_ADDR"
#define R8001_KEY_IP_MASK			"IP_MASK"
#define R8001_KEY_IP_GATEWAY		"IP_GATEWAY"
#define R8001_KEY_CHECK_RESULT	"CHECK_RESULT"

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	Name[21];			//Name
	char	R_Name[40+1];		//R_Name
	char	Local[6+1];			//Local
	char	Global[10+1];		//Global
	char device_type[3];	//DEVICE_TYPE
	char ip_static[7];		//IP_STATIC
	char ip[16];			//IP
	char mask[16];		//MASK
	char gw[16];			//GateWay
} CallNbr_T;

#pragma pack()



// Define Object Function - Public----------------------------------------------

//加载表
void LoadR8001Table(void);
void R8001TableDelete(void);
void R8001_format(void);
void R8001_Reload(void);

int get_r8001_record_items( int index,  CallNbr_T* call_nbr_info);
int get_r8001_record_cnt(void);
int get_r8001_record_by_brm( char* bd_rm_ms,  CallNbr_T* call_nbr_info);

int creat_r8001_summary_report(void);
int creat_r8001_checklist_report(void);



#endif


