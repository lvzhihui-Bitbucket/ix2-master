/**
  ******************************************************************************
  * @file    obj_CallCtrlTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_CallCtrlTable.h"
#include "../task_io_server/obj_TableProcess.h"



one_vtk_table* callCtrlTable;

void LoadCallCtrlTable(void)
{
	callCtrlTable = load_vtk_table_file(CALL_CONTROL_MAP_TABLE_NAME);
}

// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// ptr_call_ctrl_info: 得到的记录值
// return：
// 0: ok，-1: err
int get_call_control_record_items( int index,  CallCtrlRecord_T* ptr_call_ctrl_info)
{
	one_vtk_dat* tempRecord;
	char data[200];
	int dataIndex, len;
	
	if(callCtrlTable == NULL)
	{
		return -1;
	}

	if(index >= callCtrlTable->record_cnt)
	{
		return -1;
	}
	
	tempRecord = get_one_vtk_record_without_keyvalue(callCtrlTable, index);

	//printf("index=%d, newDeviceMapTable->record_cnt = %d, tempRecord = %s\n", index, newDeviceMapTable->record_cnt, tempRecord->pdat);
	
	memset(ptr_call_ctrl_info, 0, sizeof(CallCtrlRecord_T));

	//取BD
	len = 100;
	get_one_record_string(tempRecord, 0, data, &len);
	dataIndex = len;
	
	//取RM
	len = 100;
	get_one_record_string(tempRecord, 1, data + dataIndex, &len);
	dataIndex += len;

	//取MS
	len = 100;
	get_one_record_string(tempRecord, 2, data + dataIndex, &len);
	dataIndex += len;

	if(dataIndex != 10)
	{
		return -1;
	}
	else
	{
		memcpy(ptr_call_ctrl_info->BD_RM_MS, data, 10);
	}

	//取LOCAL_NBR
	len = 100;
	get_one_record_string(tempRecord, 3, data, &len);
	len = (len > 6 ? 6 : len);
	memcpy(ptr_call_ctrl_info->local, data, len);

	//取global
	len = 100;
	get_one_record_string(tempRecord, 4, data, &len);
	len = (len > 10 ? 10 : len);
	memcpy(ptr_call_ctrl_info->global, data, len);

	//取name1
	len = 100;
	get_one_record_string(tempRecord, 5, data, &len);
	len = (len > 20 ? 20 : len);
	memcpy(ptr_call_ctrl_info->name1, data, len);

	//取name2utf8
	len = 100;
	get_one_record_string(tempRecord, 6, data, &len);
	len = (len > 40 ? 40 : len);
	memcpy(ptr_call_ctrl_info->name2utf8, data, len);
	
	//取virtual_device
	len = 100;
	get_one_record_string(tempRecord, 7, data, &len);
	data[len] = 0;
	ptr_call_ctrl_info->virtual_device = atoi(data);

	//取ip_node_id
	len = 100;
	get_one_record_string(tempRecord, 8, data, &len);
	data[len] = 0;
	ptr_call_ctrl_info->ip_node_id = atoi(data);

	//取dt_addr
	len = 100;
	get_one_record_string(tempRecord, 9, data, &len);
	data[len] = 0;
	ptr_call_ctrl_info->dt_addr = atoi(data);

	return 0;
	
}


//得到一个指定global_nbr的房号：
// paras:
// global_nbr: 输入的input
// bd_nbr:    单元号
// rm_nbr：房间号
// ms_nbr:  主从房号
// return:
//  0:找到，1:有表查找不到， -1:无表
int get_bd_rm_ms_nbr_from_call_control_table( const char* global_nbr, char* bd_nbr,char* rm_nbr, char* ms_nbr )
{
	int i;
	CallCtrlRecord_T record;

	//无表
	if(callCtrlTable == NULL)
	{
		return -1;
	}

	//表记录数为0
	if(callCtrlTable->record_cnt == 0)
	{
		return -1;
	}
	
	for(i = 0; i < callCtrlTable->record_cnt; i++)
	{
		get_call_control_record_items( i,  &record);
		
		//global 匹配
		if(!strcmp(global_nbr, record.global))
		{
			memcpy(bd_nbr, record.BD_RM_MS, 4);
			memcpy(rm_nbr, record.BD_RM_MS+4, 4);
			memcpy(ms_nbr, record.BD_RM_MS+8, 2);
			bd_nbr[4] = 0;
			rm_nbr[4] = 0;
			ms_nbr[2] = 0;
			break;
		}

		//local 匹配
		else if(!strcmp(global_nbr, record.local) && !memcmp(record.BD_RM_MS, GetSysVerInfo_BdRmMs(), 4))
		{
			memcpy(bd_nbr, record.BD_RM_MS, 4);
			memcpy(rm_nbr, record.BD_RM_MS+4, 4);
			memcpy(ms_nbr, record.BD_RM_MS+8, 2);
			bd_nbr[4] = 0;
			rm_nbr[4] = 0;
			ms_nbr[2] = 0;
			break;
		}
	}

	if(i < callCtrlTable->record_cnt)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

//查找呼叫控制表中是否存在房号
// paras:
// bd_rm_ms: 输入的bd_rm_ms
// return:
//  1:找到, 0:找不到
int Find_bd_rm_ms_in_call_control_table( const char* bd_rm_ms)
{
	int i, len;
	CallCtrlRecord_T record;
	
	//无表
	if(callCtrlTable == NULL)
	{
		return 0;
	}

	//表记录数为0
	if(callCtrlTable->record_cnt == 0)
	{
		return 0;
	}

	len = strlen(bd_rm_ms);
	if(len != 8 && len != 10)
	{
		return 0;
	}
	
	for(i = 0; i < callCtrlTable->record_cnt; i++)
	{
		get_call_control_record_items( i,  &record);
		
		//global 匹配
		if(!memcmp(bd_rm_ms, record.BD_RM_MS, len))
		{
			return 1;
		}
	}
	
	return 0;
}


//判断是否有呼叫控制表
//0:无表; 1:有表
int hasCallCtrlTable(void)
{
	if(callCtrlTable == NULL)
	{
		return 0;
	}

	if(callCtrlTable->record_cnt == 0)
	{
		return 0;
	}

	return 1;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

