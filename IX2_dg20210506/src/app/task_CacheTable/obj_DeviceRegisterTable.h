/**
  ******************************************************************************
  * @file    obj_DeviceRegisterTable.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_DeviceRegisterTable_H
#define _obj_DeviceRegisterTable_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	MFG_SN[12+1];	//MFG_SN
	int		deviceType;		//设备类型
	char	BD_RM_MS[10+1];	//BD_RM_MS
	char	addTime[13];	//加入时间
	char	name[20+1];		//name
	char	Global[10+1];	//Global
	char	Local[6+1];		//Local
	int		upTime;			//上线时间
	int		ip;				//ip
} DeviceRegisterRecord_T;
#pragma pack()



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------


#endif


