#ifndef obj_APP_IPC_List_H
#define obj_APP_IPC_List_H

#include "cJSON.h"

#define IPC_VIDEO_TYPE_H264				"H264"
#define IPC_VIDEO_TYPE_H265				"H265"

#define APP_IPC_IPCS					"ipcs"
#define APP_IPC_ID						"ID"
#define APP_IPC_NAME					"NAME"
#define APP_IPC_VIDEO_TYPE				"VIDEO_TYPE"
#define APP_IPC_IX_DS					"IX_DS"
#define APP_IPC_DT_DS					"DT_DS"

/*
{
	 
    "IPC":[
    		{
    			"ID": "CAM1", 
				"NAME":"CAM1", 
				"IP": "192.168.243.6"
			},
			{
				"ID": "CAM1", 
				"NAME":"CAM1", 
				"IP": "192.168.243.6"
			}
		]

};
*/

typedef struct 
{
	char	ID[40];
	char	NAME[40];
	char	videoType[5];
} APP_IPC_ONE_DEVICE;

typedef struct
{
	char					md5Code[16];			//�б���MD5��
	cJSON*					ipc;
} APP_IPC_DeviceTable;


#endif

