/**
  ******************************************************************************
  * @file    obj_MS_DeviceTable.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "obj_MS_DeviceTable.h"
#include "obj_TableProcess.h"
#include "obj_GetIpByNumber.h"
#include "obj_GetInfoByIp.h"
#include "obj_NetManagerInterface.h"

one_vtk_table* msListTable = NULL;


// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetMsListRecordItems( int index,  MS_ListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( msListTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//MFG_SN
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);

		//GLOBAL
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_GLOBAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Global,input_str, MAX_NAME_LEN+1);
		
		//LOCAL
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_LOCAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Local,input_str, MAX_NAME_LEN+1);
		
		//BD_RM_MS
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//NAME
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->name1,input_str, MAX_NAME_LEN+1);

		//R_NAME
		keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_R_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->R_Name,input_str, MAX_NAME_LEN+1);

		return 0;
	}
	else
		return -1;
}

void LoadMsListTable(void)
{

	printf("11111%s:%d\n",__func__,__LINE__);
	msListTable = load_vtk_table_file(MS_LIST_TABLE_NAME);
	printf("11111%s:%d\n",__func__,__LINE__);
	if(msListTable == NULL)
	{
		printf("11111%s:%d\n",__func__,__LINE__);
		char MsTableKey[7][40] = {MS_TABLE_KEY_MFG_SN, MS_TABLE_KEY_DEVICE_TYPE, MS_TABLE_KEY_GLOBAL, MS_TABLE_KEY_LOCAL,
								  MS_TABLE_KEY_BD_RM_MS, MS_TABLE_KEY_NAME, MS_TABLE_KEY_R_NAME};
		int i;

		msListTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &msListTable->lock, 0);		//czn_20190327
		msListTable->keyname_cnt = 7;

		
		msListTable->pkeyname_len = malloc(msListTable->keyname_cnt*sizeof(int));
		msListTable->pkeyname = malloc(msListTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< msListTable->keyname_cnt; i++)
		{
			msListTable->pkeyname_len[i] = 40;
			msListTable->pkeyname[i].len = strlen(&MsTableKey[i][0]);
			msListTable->pkeyname[i].pdat = malloc(msListTable->pkeyname[i].len);
			memcpy(msListTable->pkeyname[i].pdat, &MsTableKey[i][0], msListTable->pkeyname[i].len);
		}
		msListTable->precord = NULL;
		msListTable->record_cnt = 0;

		printf("11111%s:%d\n",__func__,__LINE__);
		SaveMsListTableToFile();
		printf("11111%s:%d\n",__func__,__LINE__);
	}
	
}

void MSList_Table_Release(void)
{
	free_vtk_table_file_buffer(msListTable);
	msListTable = NULL;
}


int GetMSListRecordCnt(void)
{
	return (msListTable == NULL) ? 0 : msListTable->record_cnt;
}
//czn_20190221_s
void MSList_Reload(void)
{
	free_vtk_table_file_buffer(msListTable);
	LoadMsListTable();
}
//czn_20190221_e
//重命名修改MSList的一条记录
void MSlist_Rename(int index, const char *pname)
{
	char recordBuffer[200] = {0};
	one_vtk_dat vtkRecord;
	MS_ListRecord_T record;
	
	GetMsListRecordItems(index,  &record);
	
	if(pname != NULL && strlen(pname) <= 20)
	{	
		if(pname[0] == 0)
		{
			strcpy(record.R_Name, "-");
		}
		else
		{
			strcpy(record.R_Name, pname);
		}
	
		snprintf(recordBuffer, 200, "%s,0x%04x,%s,%s,%s,%s,%s", record.MFG_SN, record.deviceType, record.Global, record.Local, record.BD_RM_MS,
													record.name1, record.R_Name);
		
		vtkRecord.len = strlen(recordBuffer);
		vtkRecord.pdat = recordBuffer;
		
		Modify_one_vtk_record(&vtkRecord, msListTable, index);
		
		SaveMsListTableToFile();
	}
}

//搜索从机列表的一条记录
int SearchMSListTableByMFG_SN(char* MFG_SN, MS_ListRecord_T* pRecord)
{
	int i;
	MS_ListRecord_T tempRecord;

	if(MFG_SN[0] == 0)
	{
		return -1;
	}
	
	for(i = 0; i < msListTable->record_cnt; i++)
	{
		GetMsListRecordItems(i, &tempRecord);
		if(!strcmp(tempRecord.MFG_SN, MFG_SN))
		{
			*pRecord = tempRecord;
			return i;
		}
	}

	return -1;
}


//增加MSlist的一条记录
int AddOneMSlist_Record(MS_ListRecord_T* pRecord)
{
	int index;
	MS_ListRecord_T tempRecord;
	one_vtk_dat vtkRecord;
	char data[200];

	snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", pRecord->MFG_SN, pRecord->deviceType, pRecord->Global, pRecord->Local, pRecord->BD_RM_MS,
												pRecord->name1, pRecord->R_Name);
	vtkRecord.len = strlen(data);
	//czn_20190221_s
	if(memcmp(pRecord->MFG_SN,"000000000000",12) == 0)
	{
		index = -1;
	}
	else
	{
		index = SearchMSListTableByMFG_SN(pRecord->MFG_SN, &tempRecord);
	}
	//czn_20190221_e
	if(index >= 0)
	{
		vtkRecord.pdat = data;

		Modify_one_vtk_record(&vtkRecord, msListTable, index);
		SaveMsListTableToFile();
		return 1;
	}
	else
	{
		vtkRecord.pdat = malloc(vtkRecord.len);
		
		if(vtkRecord.pdat == NULL)
		{
			return -1;
		}

		memcpy(vtkRecord.pdat, data, vtkRecord.len);

		add_one_vtk_record(msListTable, &vtkRecord);
		
		SaveMsListTableToFile();
		return 0;
	}

}


//按index删除MSlist的一条记录
int DeleteOneMSlist_Record(int index)
{
	MS_ListRecord_T tempRecord;
	
	if(index >= 0 && index < msListTable->record_cnt)
	{
		delete_one_vtk_record(msListTable, index);

		SaveMsListTableToFile();
		return 0;
	}
	else
	{
		return -1;
	}
}

//清除namelist的所有记录
int ClearMSlist_Table(void)
{
	delete_all_vtk_record(msListTable);

	SaveMsListTableToFile();
	
	return 0;
}

//搜索生成从机列表
int AddMSlist_TableBySearching(void)
{
	GetIpRspData getIpdata;
	DeviceInfo devInfo;
	int i, ret;
	
	char BD_RM_MS[11];
	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 2, 50, &getIpdata);
	
	for(i = 0, ret = 0; i < getIpdata.cnt; i++)
	{
		if(API_GetInfoByIp(getIpdata.Ip[i], 2, &devInfo) == 0)
		{

			int index;
			MS_ListRecord_T tempRecord;
			one_vtk_dat vtkRecord;
			char data[200];
			

			index = SearchMSListTableByMFG_SN(devInfo.MFG_SN, &tempRecord);
			
			if(index >= 0)
			{
				snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, tempRecord.R_Name);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = data;
				
				Modify_one_vtk_record(&vtkRecord, msListTable, index);
			}
			else
			{
				snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, devInfo.name1);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = malloc(vtkRecord.len);
				memcpy(vtkRecord.pdat, data, vtkRecord.len);
			
				add_one_vtk_record(msListTable, &vtkRecord);
				ret++;
			}
		}
	}
	
	SaveMsListTableToFile();
	return ret;
}

int Get_MS_Record(int index, char *pname, char *bdRmMs)
{
	MS_ListRecord_T record_dat;
	
	if(msListTable->record_cnt <= index)
	{
		return -1;
	}

	GetMsListRecordItems(index, &record_dat);
	
	if(bdRmMs != NULL)
	{
		strcpy(bdRmMs, record_dat.BD_RM_MS);
	}
	
	if(pname != NULL)
	{
		strcpy(pname, strcmp(record_dat.R_Name, "-") ? record_dat.R_Name : record_dat.name1);
	}

	return 0;
}

//保存列表到文件
int SaveMsListTableToFile(void)
{
	int keyname_index;
	printf("11111%s:%d\n",__func__,__LINE__);
	//R_NAME
	keyname_index = get_keyname_index( msListTable, MS_TABLE_KEY_R_NAME);	
	printf("11111%s:%d\n",__func__,__LINE__);
	//按名字排序
	AlphabeticalOrder_vtk_table(msListTable, keyname_index);
	
	remove(MS_LIST_TABLE_NAME);
	printf("11111%s:%d\n",__func__,__LINE__);
	if(save_vtk_table_file_buffer(msListTable, 0, msListTable->record_cnt, MS_LIST_TABLE_NAME))
	{
		return -1;
	}
	printf("11111%s:%d\n",__func__,__LINE__);
	return 0;
}


/*********************************************************************************************************
**  temp table
*********************************************************************************************************/
one_vtk_table* msListTempTable = NULL;



// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetMsListTempRecordItems( int index,  MS_ListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( msListTempTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//MFG_SN
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);

		//GLOBAL
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_GLOBAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Global,input_str, MAX_NAME_LEN+1);
		
		//LOCAL
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_LOCAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Local,input_str, MAX_NAME_LEN+1);
		
		//BD_RM_MS
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//NAME
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->name1,input_str, MAX_NAME_LEN+1);

		//R_NAME
		keyname_index = get_keyname_index( msListTempTable, MS_TABLE_KEY_R_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->R_Name,input_str, MAX_NAME_LEN+1);

		return 0;
	}
	else
		return -1;
}

void CreateMsListTempTable(void)
{
	if(msListTempTable == NULL)
	{
		char MsTableKey[7][40] = {MS_TABLE_KEY_MFG_SN, MS_TABLE_KEY_DEVICE_TYPE, MS_TABLE_KEY_GLOBAL, MS_TABLE_KEY_LOCAL,
								  MS_TABLE_KEY_BD_RM_MS, MS_TABLE_KEY_NAME, MS_TABLE_KEY_R_NAME};
		int i;

		msListTempTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &msListTempTable->lock, 0);		//czn_20190327
		msListTempTable->keyname_cnt = 7;
		
		msListTempTable->pkeyname_len = malloc(msListTempTable->keyname_cnt*sizeof(int));
		msListTempTable->pkeyname = malloc(msListTempTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< msListTempTable->keyname_cnt; i++)
		{
			msListTempTable->pkeyname_len[i] = 40;
			msListTempTable->pkeyname[i].len = strlen(&MsTableKey[i][0]);
			msListTempTable->pkeyname[i].pdat = malloc(msListTempTable->pkeyname[i].len);
			memcpy(msListTempTable->pkeyname[i].pdat, &MsTableKey[i][0], msListTempTable->pkeyname[i].len);
		}
		msListTempTable->precord = NULL;
		msListTempTable->record_cnt = 0;
	}
}

void DeleteMsListTempTable(void)
{
	free_vtk_table_file_buffer(msListTempTable);
	msListTempTable = NULL;
}


int GetMSListTempRecordCnt(void)
{
	return (msListTempTable == NULL) ? 0 : msListTempTable->record_cnt;
}



//搜索生成从机列表
int AddTempMSlist_TableBySearching(void)
{
	GetIpRspData getIpdata;
	DeviceInfo devInfo;
	int i, ret;

	DeleteMsListTempTable();
	CreateMsListTempTable();
	
	char BD_RM_MS[11];
	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 2, 50, &getIpdata);
	
	for(i = 0, ret = 0; i < getIpdata.cnt; i++)
	{
		memcpy(BD_RM_MS, getIpdata.BD_RM_MS[i], 10);
		BD_RM_MS[10] = 0;
		if(atoi(BD_RM_MS+8) >= 50)
		{
			continue;
		}
		
		if(API_GetInfoByIp(getIpdata.Ip[i], 2, &devInfo) == 0)
		{

			int index;
			MS_ListRecord_T tempRecord;
			one_vtk_dat vtkRecord;
			char data[200];
			

			snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
														devInfo.name1, devInfo.name1);
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(msListTempTable, &vtkRecord);
			ret++;
		}
	}
	return ret;
}

/*********************************************************************************************************
**  temp table
*********************************************************************************************************/
one_vtk_table* msListUpdateTable = NULL;



// 从对照表中得到记录的所有字段数据
// paras:
// index: 指定表中的第几条记录
// record: 得到的记录值
// return：
// 0: ok，-1: err
int GetMsListUpdateRecordItems( int index,  MS_ListRecord_T* record)
{
	unsigned char 	input_str[MAX_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( msListUpdateTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//MFG_SN
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->MFG_SN,input_str, MAX_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		record->deviceType = atoi(input_str);

		//GLOBAL
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_GLOBAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Global,input_str, MAX_NAME_LEN+1);
		
		//LOCAL
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_LOCAL);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->Local,input_str, MAX_NAME_LEN+1);
		
		//BD_RM_MS
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//NAME
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->name1,input_str, MAX_NAME_LEN+1);

		//R_NAME
		keyname_index = get_keyname_index( msListUpdateTable, MS_TABLE_KEY_R_NAME);		
		input_str_len = MAX_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(record->R_Name,input_str, MAX_NAME_LEN+1);

		return 0;
	}
	else
		return -1;
}

void CreateMsListUpdateTable(void)
{
	if(msListUpdateTable == NULL)
	{
		char MsTableKey[7][40] = {MS_TABLE_KEY_MFG_SN, MS_TABLE_KEY_DEVICE_TYPE, MS_TABLE_KEY_GLOBAL, MS_TABLE_KEY_LOCAL,
								  MS_TABLE_KEY_BD_RM_MS, MS_TABLE_KEY_NAME, MS_TABLE_KEY_R_NAME};
		int i;

		msListUpdateTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &msListUpdateTable->lock, 0);		//czn_20190327
		msListUpdateTable->keyname_cnt = 7;

		msListUpdateTable->pkeyname_len = malloc(msListUpdateTable->keyname_cnt*sizeof(int));
		msListUpdateTable->pkeyname = malloc(msListUpdateTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< msListUpdateTable->keyname_cnt; i++)
		{
			msListUpdateTable->pkeyname_len[i] = 40;
			msListUpdateTable->pkeyname[i].len = strlen(&MsTableKey[i][0]);
			msListUpdateTable->pkeyname[i].pdat = malloc(msListUpdateTable->pkeyname[i].len);
			memcpy(msListUpdateTable->pkeyname[i].pdat, &MsTableKey[i][0], msListUpdateTable->pkeyname[i].len);
		}
		msListUpdateTable->precord = NULL;
		msListUpdateTable->record_cnt = 0;
	}
}

void DeleteMsListUpdateTable(void)
{
	free_vtk_table_file_buffer(msListUpdateTable);
	msListUpdateTable = NULL;
}


int GetMSListUpdateRecordCnt(void)
{
	return (msListUpdateTable == NULL) ? 0 : msListUpdateTable->record_cnt;
}



//搜索生成从机列表
int AddUpdateMSlist_TableBySearching(void)
{
	GetIpRspData getIpdata;
	DeviceInfo devInfo;
	int i, j, ret;
	MS_ListRecord_T record1, record2;
	one_vtk_dat tempRecord;

	DeleteMsListUpdateTable();
	CreateMsListUpdateTable();
	
	char BD_RM_MS[11];
	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());
	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 1, 50, &getIpdata);
	
	for(i = 0, ret = 0; i < getIpdata.cnt; i++)
	{
		memcpy(BD_RM_MS, getIpdata.BD_RM_MS[i], 10);
		BD_RM_MS[10] = 0;
		if(atoi(BD_RM_MS+8) >= 50)
		{
			continue;
		}
		if(api_nm_if_judge_include_dev(getIpdata.BD_RM_MS[i],getIpdata.Ip[i])==0)
		{
			continue;
		}
		if(API_GetInfoByIp(getIpdata.Ip[i], 2, &devInfo) == 0)
		{

			int index;
			MS_ListRecord_T tempRecord;
			one_vtk_dat vtkRecord;
			char data[200];
			

			snprintf(data, 200, "%s,0x%04x,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
														devInfo.name1, devInfo.name1);
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(msListUpdateTable, &vtkRecord);
			ret++;
		}
	}

	//按房号从小到大排序
	for (i = 0; i < msListUpdateTable->record_cnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < msListUpdateTable->record_cnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			GetMsListUpdateRecordItems(j, &record1);
			GetMsListUpdateRecordItems(j+1, &record2);
			if (atoi(record1.BD_RM_MS) > atoi(record2.BD_RM_MS))
			{
				tempRecord = msListUpdateTable->precord[j+1];
				msListUpdateTable->precord[j+1] = msListUpdateTable->precord[j];
				msListUpdateTable->precord[j] = tempRecord;
			}
		}
	}

	return ret;
}


int UpdateMsListTable(void)
{
	AddUpdateMSlist_TableBySearching();
	
	if(CompareTable(msListTempTable, msListUpdateTable))
	{
		CopyTable(&msListTempTable, msListUpdateTable);
		return 1;
	}
	else
	{
		return 0;
	}
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

