
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#include "uart.h"
#include "vdp_uart.h"
#include "slipframe.h"
#include "utility.h"

#include "fw_uart.h"

#include "task_survey.h"
#include "task_Hal.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#include "task_Power.h"

Serial_Task_Info 		ttyS0_sti;

unsigned char 	rcvByteCnt;		//�ֽڼ�����
unsigned char 	rcvData[100];	//��������֡����	
extern int STM8_CODE_VERSION;	//cao_20180604
unsigned char STM8_CONFIG_MODE = MODE_HAND_PLUG;//dh_20211014

COM_RCV_FLAG 	phComRcvFlag = COM_RCV_NONE;		
COM_TRS_FLAG 	phComTrsFlag = COM_TRS_NONE;
BUS_ERR  		stateError;			//���ߴ���״̬

// ֡��ʽ:
// [��ʼΪ: 0x02] [���ݳ���] [����1][����2][����N][�����][ֹͣλ:0x03]
// [���ݳ���] :  [����1]��[����N]��BYTE��
// [�����] :  [����1]��[����N]���ۼӺ�,  Ϊ1BYTE, ��Ȼ���?;

#define DT_UART_CMD_PACK_MIN_LEN				4			// ��С�������L��

#define DT_UART_CMD_PACK_HEAD_OFF				0			// ���^ƫ����
#define DT_UART_CMD_PACK_LEN_OFF				1			// ���L��ƫ����
#define DT_UART_CMD_PACK_CMD_OFF				2			// ������ƫ����
#define DT_UART_CMD_PACK_DAT_OFF				3			// ������ƫ����

#define	DT_UART_CMD_PACK_HEAD					0x02		// ���^
#define	DT_UART_CMD_PACK_TAIL					0x03		// ��β

#if 1
BUFFER_POOL HABufferPool;

//���ջ���ص�ָ��ǰ��?
unsigned short GetBufferLastPos(unsigned short iPos)
{
	if( iPos == 0 ) iPos = POOL_LENGTH-1; else iPos--;
	return iPos;
}
//���ջ���ص�ָ�����
unsigned short GetBufferNextPos(unsigned short iPos)
{
	iPos++; if( iPos >= POOL_LENGTH ) iPos = 0;
	return iPos;
}
//����������ݵĸ���?
unsigned short GetBufferLength(void)
{
	if( HABufferPool.ptr_i >= HABufferPool.ptr_o )
		return ( HABufferPool.ptr_i-HABufferPool.ptr_o);
	else
		return ( HABufferPool.ptr_i+POOL_LENGTH-HABufferPool.ptr_o);
}
// ���յ����ݽ��뻺���?
void PushIntoBufferPool(unsigned char data)
{
	HABufferPool.Buffer[HABufferPool.ptr_i] = data;					//���浱ǰ����
	HABufferPool.ptr_i = GetBufferNextPos(HABufferPool.ptr_i);		//����ָ����?
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )					//��������������򸲸ǵ���ɵ�����
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
}

//�ӻ����ȡ����?
uint8 PollOutBufferPool(unsigned char *pdata)
{
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )					//û�����ݿ�ȡ
	{
		return 0;
	}
	else
	{
		*pdata = HABufferPool.Buffer[HABufferPool.ptr_o];				//ȡ����ǰ����
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);		//���ָ���һ
		return 1;
	}
}

#endif

#define RCV_RESET_TIMER		500	//ms
OnePacket packet;
OS_TIMER timerRcvReset;

unsigned char UartCheckSum( char* pbuf, int len )
{
	unsigned char checksum = 0;
	int i;
	for( i = 0; i < len; i++ )
	{
		checksum += pbuf[i];
	}
	return checksum;
}	

/*******************************************************************************************
 * @fn:		ttyS0_rcv_handle
 *
 * @brief:	���ڽ������ݰ�����
 *
 * @param:  	*data	- ������ָ��
 * @param:  	len		- ���ݰ�����
 *
 * @return: 	none
*******************************************************************************************/
void ttyS0_rcv_handle(char *data, int len)
{
	unsigned char cks;
	int i;
	int dataLen;
	
#if 0
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	dprintf("uart recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	
	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}

	while( PollOutBufferPool(&packet.data[packet.len++]) )
	{
		OS_RetriggerTimer(&timerRcvReset);
		//��ͷ���?
		if(packet.data[0] != DT_UART_CMD_PACK_HEAD)
		{
			//��ͷ���Դ�ͷ��ʼ����
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		// get length
		if(packet.len <= DT_UART_CMD_PACK_LEN_OFF)
		{
			continue;
		}
		
		//���ݳ��ȼ��?
		dataLen = packet.data[DT_UART_CMD_PACK_LEN_OFF]+4;
		if(dataLen%4)
		{
			//���ݳ��Ȳ���4���������ݰ����?0x03��ֱ�����ȱ�4����
			dataLen = dataLen + (4 -dataLen%4);
		}
		//���ݰ���û������ɣ���������?
		if( dataLen >= PACKET_LENGTH )
		{
			packet.len = 0;
			dprintf("dt uart cmd pack recv too long!\n");
			continue;
		}
		
		//���ݰ���û������ɣ���������?
		if(packet.len < dataLen)
		{
			continue;
		}
		
		//�ж�У���?
		if(UartCheckSum( &packet.data[DT_UART_CMD_PACK_CMD_OFF], packet.data[DT_UART_CMD_PACK_LEN_OFF]) != packet.data[packet.data[DT_UART_CMD_PACK_LEN_OFF]+DT_UART_CMD_PACK_CMD_OFF])
		{
			//����Ͳ��Դ�ͷ��ʼ����?
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		//�жϰ�β
		for(i = packet.data[DT_UART_CMD_PACK_LEN_OFF]+DT_UART_CMD_PACK_CMD_OFF+1; i < dataLen; i++)
		{
			if(packet.data[i] != DT_UART_CMD_PACK_TAIL)
			{
				break;
			}
		}
		if(i < dataLen)
		{
			//��β���Դ�ͷ��ʼ����
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		api_uart_recv_callback(packet.data+DT_UART_CMD_PACK_CMD_OFF, packet.data[DT_UART_CMD_PACK_LEN_OFF]);
		packet.len = 0;
	}
	//û�����ݵ�ʱ��Ӷ���?1
	packet.len--;
}


int vdp_uart_send_data(char *data, int len)
{
#if 0
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) data;
		dprintf("uart send len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
#endif	

#if !defined(PID_IXSE)
	push_serial_data(&ttyS0_sti, data, len);
	//usleep(20*1000);
#endif

	return 0;
}

void RcvResetCallBack(void)
{
	packet.len = 0;
	OS_StopTimer(&timerRcvReset);
}

/*******************************************************************************************
 * @fn:		Init_vdp_uart
 *
 * @brief:	��ʼ��uart�˿�
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int Init_vdp_uart(void)
{
#if 0
#if defined(PID_IX47)
		if( !start_serial_task(&ttyS0_sti, "/dev/ttySAK2", 9600, 8, 'N', 1, &ttyS0_rcv_handle) )
#else
		if( !start_serial_task(&ttyS0_sti, "/dev/ttySAK2", 115200, 8, 'N', 1, &ttyS0_rcv_handle) )
#endif
#endif
	if( !start_serial_task(&ttyS0_sti, "/dev/ttySAK2", 9600, 8, 'N', 1, &ttyS0_rcv_handle) )
	{
	 	printf("--------------------------ttyS1 open  Success!\n");
	}
	else
	{
		printf("----------------------------start_serial_task failure!\n");
	}	

	HABufferPool.ptr_i = 0;
	HABufferPool.ptr_o = 0;
	packet.len = 0;
	OS_CreateTimer(&timerRcvReset, RcvResetCallBack, RCV_RESET_TIMER/25);
	return 0;
}

/*******************************************************************************************
 * @fn:		close_vdp_uart
 *
 * @brief:	�رմ��ڼ�������?
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int close_vdp_uart(void)
{
	stop_serial_task(&ttyS0_sti);
	return 0;
}


// �������ݰ�������
int api_uart_send_pack( unsigned char cmd, char* pbuf, unsigned int len )
{


	unsigned char cks;
	char send_buf[200];

	send_buf[DT_UART_CMD_PACK_HEAD_OFF] 		= DT_UART_CMD_PACK_HEAD;
	send_buf[DT_UART_CMD_PACK_LEN_OFF]			= len+1;
	send_buf[DT_UART_CMD_PACK_CMD_OFF]			= cmd;
	// ����checksum
	cks = cmd;
	if( len )
	{
		memcpy( send_buf+DT_UART_CMD_PACK_DAT_OFF, pbuf, len );	
		cks += UartCheckSum( pbuf, len );
	}
	send_buf[DT_UART_CMD_PACK_DAT_OFF+len] 		= cks;
	send_buf[DT_UART_CMD_PACK_DAT_OFF+len+1] 	= DT_UART_CMD_PACK_TAIL;
	
	return vdp_uart_send_data(send_buf,DT_UART_CMD_PACK_DAT_OFF+len+2);
}

// zzb_20171127
void api_notify_global_reset(void)
{
	dprintf("----------------------------I request restart!!!!!!!\n");
	api_uart_send_pack(UART_TYPE_N2S_RESET, NULL, 0); 
}

// ���յ��������ݰ��Ļص����� - ���͵�survey����
int api_uart_recv_callback( char* pbuf, unsigned int len )
{
	unsigned short logic_addr;
	unsigned char slave_addr;
	HAL_MSG_TYPE	key_msg;
	static int myAddr_init = 0;	//czn_20170818
	static int ts_3s_flag = 0;	//czn_20190216

	#if 0	
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) pbuf;
		printf("api_uart_recv_callback len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
		//PrintCurrentTime(101);
	#endif	
	
	switch( pbuf[0] )
	{
		// 1 = STM8���յ�����ָ��,��N329����; - ����ָ���?
		case UART_TYPE_S2N_REC_DT_CMD:


			COMM_RCV_FLAG_ENABLE(COM_RCV_DAT,BUS_ERR_NONE);
			rcvByteCnt = len-1;
			memcpy( rcvData, pbuf+1,rcvByteCnt );
			L2_LayerMonitorService();
			break;

		// 3 = STM8ִ�з���ָ��,  ���ط��ͽ����N329 - * ���ͽ��?
		case UART_TYPE_S2N_SND_RESULT:

			// lzh_20161207_s
			if( pbuf[1] == 0 )
				phComTrsFlag = COM_TRS_DAT;
			else
				phComTrsFlag = COM_TRS_ERR;				
			// lzh_20161207_e
			L2_LayerMonitorService();	

			break;

		// 6 = STM8��N329����:	 DIP״̬(�ı�) - 5λDIP���ص�ֵ
		case UART_TYPE_S2N_DIP_STATUS:
/*
		
			logic_addr = ((pbuf[1]&0x10)>>4)|((pbuf[1]&0x08)>>2)|((pbuf[1]&0x04))|((pbuf[1]&0x02)<<2)|((pbuf[1]&0x01)<<4);
			myAddr = 0x80|(logic_addr<<2);

			if(myAddr_init == 0)
			{
				myAddr_init = 1;
			}
			else
			{
				key_msg.head.msg_target_id	= MSG_ID_Menu;
				key_msg.head.msg_source_id	= MSG_ID_hal;
				key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
				key_msg.head.msg_sub_type	= 0;
				key_msg.key.keyType 		= KEY_TYPE_DIP;
				key_msg.key.keyData 		= myAddr;
				gettimeofday(&key_msg.happen_time,NULL);
				API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			}
*/
			
			break;
			
		// 9 = STM8��N329Ӧ��:	 DIP״̬  - 5λDIP���ص�ֵ
		case UART_TYPE_S2N_REPLY_DIP_STATUS:
/*
			logic_addr = ((pbuf[1]&0x10)>>4)|((pbuf[1]&0x08)>>2)|((pbuf[1]&0x04))|((pbuf[1]&0x02)<<2)|((pbuf[1]&0x01)<<4);
			
			API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slave_addr);
			if(slave_addr >= 4)
				slave_addr = 0;
			
			myAddr = 0x80|(logic_addr<<2)|slave_addr;
*/

			break;
			
		//  7 = STM8��N329����:	 DoorBell״̬(�ı�) - 0=�ͷ�; 1=���� 
		// 11 = STM8��N329Ӧ��:   DoorBell״̬	- 0=�ͷ�; 1=���� 
		case UART_TYPE_S2N_REPLY_DOORBELL_STATUS:
		case UART_TYPE_S2N_DOORBELL_STATUS:
			key_msg.head.msg_target_id	= MSG_ID_survey;
			key_msg.head.msg_source_id	= MSG_ID_hal;
			key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
			key_msg.head.msg_sub_type	= 0;
			key_msg.key.keyType 		= KEY_TYPE_DB;
			key_msg.key.keyData 		= pbuf[1];
			
			gettimeofday(&key_msg.happen_time,NULL);
			// zfz_20190605
			//API_add_message_to_suvey_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			break;
		// lzh_20161207_s
		// 14 = STM8֪ͨ���յ�ACK
		case UART_TYPE_S2N_BUS_ACK_RCV:
/*
			phComRcvFlag = COM_RCV_ACK;
			L2_LayerMonitorService();
			break;
*/
		// lzh_20161207_e

		case UART_TYPE_S2N_TOUCHKEY_STATUS:     //lyx 20170721

			key_msg.head.msg_target_id	= MSG_ID_survey;
			key_msg.head.msg_source_id	= MSG_ID_hal;
			key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
			key_msg.head.msg_sub_type	= 0;
			key_msg.key.keyType		= KEY_TYPE_TS;	
			key_msg.key.keyData              = pbuf[2];
		
			//czn_20190216_s
			if(pbuf[1] == KEY_STATUS_PRESS)
			{
				ts_3s_flag = 0;
				key_msg.key.keyStatus = TOUCHPRESS;
			}
			else if(pbuf[1] == KEY_STATUS_RELEASE)
			{	
				if(ts_3s_flag == 1)
				{
					ts_3s_flag = 0;
					break;
				}
				key_msg.key.keyStatus = TOUCHCLICK;
			}
			else if(pbuf[1] == KEY_STATUS_3SECOND)
			{
				ts_3s_flag = 1;
				key_msg.key.keyStatus = TOUCH_3SECOND;
			}
			//czn_20190216_e
			dprintf(" Touchkey: keyData=%d,keyStatus=%d\n",pbuf[2], pbuf[1]);

			gettimeofday(&key_msg.happen_time,NULL);			//key_msg.happen_time		= clock();		//czn_20170111
			//bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);     
			API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) ); 
			break;

		case UART_TYPE_S2N_NOTIFY_RESET:
			dprintf("-----%s will Reset in 2 seconds---------\n", GetSysVerInfo_name());
			break;
			
		//cao_20180604
		case UART_TYPE_STM8_VERSION_ASK:
			STM8_CODE_VERSION = pbuf[1];
			UpdateSwVer();
			break;
		// dh_20211014_s
		case UART_TYPE_GET_STM8_CONFIG_RSP:
			STM8_CONFIG_MODE = pbuf[1];
			api_stm8_mode_match(STM8_CONFIG_MODE);
			printf("-----UART_TYPE_GET_STM8_CONFIG_RSP %02x----\n", STM8_CONFIG_MODE);
			break;
		case UART_TYPE_READ_LINE_RSP:
			printf("----UART_TYPE_READ_LINE_RSP : %02x----\n", pbuf[1]);
			if(STM8_CONFIG_MODE ==MODE_HAND_PLUG)
			{
				//if(pbuf[1]&0x01)//Z1�ڽӵأ��ֱ�������
					api_plugswitch_status_update((pbuf[1]&0x02)>>1);
			}
			else if(STM8_CONFIG_MODE ==MODE_DJ4A) 
			{
				api_plugswitch_status_update((pbuf[1]&0x04)>>2);
			}
			else if(STM8_CONFIG_MODE ==MODE_ALARM4)
			{
				#if defined(PID_IX47)||defined(PID_IX482)
				RevAlarmingSenser_Event(pbuf[1]&0x0f);
				#endif
			}
			
			break;
		case UART_TYPE_LINE_IN_REPORT:
			if(STM8_CONFIG_MODE ==MODE_HAND_PLUG)
			{
				printf("----MODE_HAND_PLUG IN: %02x----\n", pbuf[1]);
				//if(pbuf[1]&0x01)
				{
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key.keyType 		= KEY_TYPE_HK;
					key_msg.key.keyData 		= (pbuf[1]&0x02)>>1;			
					gettimeofday(&key_msg.happen_time,NULL);
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
					api_plugswitch_status_change((pbuf[1]&0x02)>>1);
				}
			}
			else if(STM8_CONFIG_MODE ==MODE_ALARM4)
			{
				#if defined(PID_IX47)||defined(PID_IX482)
				RevAlarmingSenser_Event(pbuf[1]&0x0f);
				#endif
				printf("----MODE_ALARM4 IN: %02x----\n", pbuf[1]);
				key_msg.head.msg_target_id	= MSG_ID_survey;
				key_msg.head.msg_source_id	= MSG_ID_hal;
				key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
				key_msg.head.msg_sub_type	= 0;
				key_msg.key.keyType 		= KEY_TYPE_ALARM;
				key_msg.key.keyData 		= pbuf[1];			
				gettimeofday(&key_msg.happen_time,NULL);
				API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			}
			else if(STM8_CONFIG_MODE ==MODE_DJ4A) 
			{
				printf("----MODE_DJ4A IN: pbuf[1]=%02x--\n", pbuf[1]);

				if( (pbuf[1]&0x01) == 0 || (pbuf[1]&0x02) == 0 )
				{
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key.keyType 	= KEY_TYPE_TS;	
					key_msg.key.keyData 		 =((pbuf[1]&0x01) == 0)? KEY_UNLOCK : KEY_UNLOCK2 ;
					key_msg.key.keyStatus = TOUCHCLICK;
					gettimeofday(&key_msg.happen_time,NULL);
					printf("----MODE_DJ4A IN: keyData=%02x--\n", key_msg.key.keyData);
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
				}
				else
				{
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key.keyType 		= KEY_TYPE_HK;
					key_msg.key.keyData 		= (pbuf[1]&0x04)>>2;			
					gettimeofday(&key_msg.happen_time,NULL);
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
					api_plugswitch_status_change((pbuf[1]&0x04)>>2);
				}
			}
			break;
		// dh_20211014_e
		// lzh_20210727
		case UART_TYPE_FIRMWARE_S2M:
			api_fw_uart_upgrade_recv( pbuf+1, len-1 );
			break;

		case UART_TYPE_GetSn_RSP:
		case UART_TYPE_SetSn_RSP:
			UartRecvGetSnRsp(pbuf+1, len-1);
			break;
		case UART_TYPE_AMP_MUTE:
			printf("$$$$$$$UART_TYPE_AMP_MUTE\n");
			API_POWER_STACK_MUTE_ON();
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************************
 * @fn:		Ph_AckWrite
 *
 * @brief:	��������ACK�ź�
 *			
 * @param:  none
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
//lzh_20130413
unsigned char Ph_AckWrite(void)
{
	// lzh_20161207_s
	api_uart_send_pack(UART_TYPE_N2S_BUS_ACK_SND,NULL,0);
	dprintf("---------Ph_AckWrite\n");
	// lzh_20161207_e
	return 1;
}

/*******************************************************************************************
 * @fn:		Ph_DataWrite
 *
 * @brief:	������������
 *			
 * @param:  pdat - ����ָ��
 * @param:  len - ���ݳ���
 * @param:  uNew - ��ָ����?
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
//lzh_20130416
unsigned char Ph_DataWrite(unsigned char *pdat,unsigned char len,unsigned char uNew)
{
	unsigned char send_buf[200];
	send_buf[0] = 0x07;		// mac_type
	send_buf[1] = uNew;		// new/old type
	memcpy( send_buf+2, pdat, len );
	
	#ifdef VDP_PRINTF_DEBUG	
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) pdat;
		printf("Ph_DataWrite len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
	#endif
	API_POWER_STACK_MUTE_ON();
	if(get_power_dt_mute()==0)
		usleep(100*1000);
	api_uart_send_pack(UART_TYPE_N2S_SND_DT_CMD,send_buf,len+2);
		
	return 0;
}

/*******************************************************************************************
 * @fn:		Ph_PollingWriteResult
 *
 * @brief:	��ѯ��ǰ���ߣ��õ��������ݽ���?�ű�־������������ŵ�pdat�ĵ�ַ��
 *			
 * @param:  COM_TRS_FLAG flag - ���ͽ�����־
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
unsigned char Ph_PollingWriteResult(COM_TRS_FLAG *flag)
{
	*flag = COM_TRS_DAT;
	//��ȡ���ݺ�������?
	phComTrsFlag = COM_TRS_NONE;
	return 1;
}

/*******************************************************************************************
 * @fn:		Ph_PollingReadResult
 *
 * @brief:	��ѯ��ǰ���ߣ��õ�����ͨ�ű�־������������ŵ�pdat�ĵ�ַ��
 *			
 * @param:  pdat - ������ݵ�ָ��?
 * @param:	COM_RCV_FLAG flag - ����ͨ�ű�־
 *
 * @return: pdat�е���Ч���ݵĳ��ȣ����������򷵻�0
 *******************************************************************************************/
unsigned char Ph_PollingReadResult(unsigned char *pdat,COM_RCV_FLAG *flag)
{
	unsigned char i;

	if( phComRcvFlag == COM_RCV_DAT )
	{
		memcpy( pdat, rcvData, rcvByteCnt );
		i = rcvByteCnt;
		int j;
		#if 1
		printf("uart receive dt data:");
		for( j = 0; j<rcvByteCnt; j++ )
		{
			printf("0x%X ",rcvData[j]);
		}
		printf("\n");
		#endif
	}
	else
	{
		i = 0;
	}

	*flag = phComRcvFlag;
	//��ȡ���ݺ������??
	phComRcvFlag = COM_RCV_NONE;
	return i;	
}

int api_get_stm8_mode(void)
{
	return STM8_CONFIG_MODE;
}

int api_set_stm8_mode(void)
{
	unsigned char mode;
	char temp[10] = {0};
	API_Event_IoServer_InnerRead_All(ZportMode, temp);
	mode = atoi(temp)? (atoi(temp)==1? MODE_HAND_PLUG : (atoi(temp)==2? MODE_DJ4A : MODE_ALARM8)): MODE_ALARM4;
	STM8_CONFIG_MODE = mode;
	api_uart_send_pack(UART_TYPE_SET_STM8_CONFIG, &mode, 1);	
	return 0;
}

int api_stm8_mode_match(unsigned char stm8_mode)
{
	unsigned char mode;
	char temp[10] = {0};
	API_Event_IoServer_InnerRead_All(ZportMode, temp);
	mode = atoi(temp)? (atoi(temp)==1? MODE_HAND_PLUG : (atoi(temp)==2? MODE_DJ4A : MODE_ALARM8)): MODE_ALARM4;
	
	if(mode!=stm8_mode)
	{
		STM8_CONFIG_MODE = mode;
		api_uart_send_pack(UART_TYPE_SET_STM8_CONFIG, &mode, 1);	
	}

	return 0;
}


