/**
  ******************************************************************************
  * @file    define_file.h
  * @author  jia
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the define of GPIO use.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _define_file_H_
#define _define_file_H_
//#define DONOT_WRITE_FLASH

#define LOCAL_AUTOIP_SERVICE_FILE 						"/tmp/autoip_server_socket"

#define RING_Folder   									"/usr/rings/"
#define RING_DEFAULT_CFG_FILE   						"/usr/rings/default.txt"
#define WIFI_PATH 										"/usr/wifi/"
#define MCU_SIMULATE_FLASH_PATH 				"/usr/MCU_SimulateFlash.bin"

#define INSTALLER_BAK_CONFIG_FILE						"/mnt/nand1-1/App/res/backup0.cfg"
#define UPDATE_BAK_CONFIG_FILE							"/mnt/nand1-1/App/res/updatebackup.cfg"
#define IO_DATA_DEFAULT_FILE_NAME						"/mnt/nand1-1/App/res/io_data_default_table.csv"
#define EXT_MODULE_FILE_NAME							"/mnt/nand1-1/App/res/ExtModule.cfg"
#define PARA_MENU_FILE_NAME								"/mnt/nand1-1/App/res/ParaMenu_table.json"
#define MENU_LIST_TABLE_FILE_NAME						"/mnt/nand1-1/App/res/ListView.dat"
#define SIP_CONFIG_DEFAULT_FILE_NAME					"/mnt/nand1-1/App/res/sipcfg_default.cfg"
#define	SwUpgradeIni									"/mnt/nand1-1/App/res/SwUpgrade.ini"
#define PublicInfo_File_Name						"/mnt/nand1-1/App/res/PublicInformation.json"
#define IO_PARA_Path				            "/mnt/nand1-1/App/res/"
#define IO_PARA_Public_File_Name				"/mnt/nand1-1/App/res/IO_PARA_Public.json"
#define IO_Trim_FILE_NAME								"_PARA_Trim.json"
#define IO_Check_FILE_NAME							"_PARA_Check.json"

#define ROUTE_TEMP										"/tmp/route.sh"
#define AUTO_IP_DHCP_CLIENT_PATH						"/mnt/nand1-1/App/dhcpclient"
#define FTP_CLIENT_PATH									"/mnt/nand1-1/App/ftpclient"

#define MENU_FILE_PATH									"/mnt/nand1-1/Drv/res/"
#define MENU_V_FILE_PATH								"/mnt/nand1-1/Drv/resV/"
#define EXTEND_FONT1_NAME								"/mnt/nand1-1/Drv/res/unicode_font1.ttf"
#define INNER_FONT_NAME									"/mnt/nand1-1/Drv/res/wryh1624.bin"
#define JPEG_BACKGROUND									"/mnt/nand1-1/Drv/res/background.jpg"
#define MENU_LANG_FILE_PATH								"/mnt/nand1-1/Drv/res/"
#define MENU_LANG_MESG_FILE								"/mnt/nand1-1/Drv/res/menu_lang_mesg.def"

#define CustomerFileDir									"/mnt/nand1-2/Customerized/"
#define CustomerizedLanguageNandFolder					"/mnt/nand1-2/Customerized/language/"
#define INSTALLER_BAK_Customerized_FILE					"/mnt/nand1-2/Customerized/Installer/backup0.cfg"
#define CUSTOMERIAED_IO_TABLE							"/mnt/nand1-2/Customerized/para/customerized_io_data_table.csv"
#define CUSTOMERIAED_SIP_CONFIG_DEFAULT_FILE		"/mnt/nand1-2/Customerized/para/customerized_sipcfg_default.cfg"
#define CUSTOMERIAED_DXMonList							"/mnt/nand1-2/Customerized/para/DXMonList.json"
#define CUSTOMERIAED_DXNameList							"/mnt/nand1-2/Customerized/para/DXNameList.json"
#define IO_Cus_Path							        "/mnt/nand1-2/Customerized/para/"
#define IO_Cus_FILE_NAME								"_PARA_Cus.json"

// lzh_20210909_s
#define MENU_FILE_PATH_CUSTOM							"/mnt/nand1-2/Customerized/UI/CustomizedOutput/"
#define MENU_V_FILE_PATH_CUSTOM							"/mnt/nand1-2/Customerized/UI/CustomizedOutputV/"

#define LOGO_FILENAME										"/mnt/nand1-2/Customerized/UI/Logo.bmp"
#define LOGO_V_FILENAME									"/mnt/nand1-2/Customerized/UI/Logo_V.bmp"
#define ListenNoVideo_FILENAME										"/mnt/nand1-2/Customerized/UI/ListenNoVideo.jpg"
#define ListenNoVideo_V_FILENAME										"/mnt/nand1-2/Customerized/UI/ListenNoVideoV.jpg"
#define SOS_RELEASE_FILENAME									"/mnt/nand1-2/Customerized/UI/SOS_Release.bmp"
#define SOS_PRESS_FILENAME									"/mnt/nand1-2/Customerized/UI/SOS_Press.bmp"
#define SOS_RELEASE_V_FILENAME									"/mnt/nand1-2/Customerized/UI/SOS_Release_V.bmp"
#define SOS_PRESS_V_FILENAME									"/mnt/nand1-2/Customerized/UI/SOS_Press_V.bmp"
// lzh_20210909_e

#define INSTALLER_BAK_PATH								"/mnt/nand1-2/Backup/"
#define INSTALLER_FILE_BAK1								"/mnt/nand1-2/Backup/backup1.bak"
#define INSTALLER_FILE_BAK2								"/mnt/nand1-2/Backup/backup2.bak"
	

#define SettingFolder									"/mnt/nand1-2/Settings/"

#define IO_DATA_VALUE_NAME								"/mnt/nand1-2/Settings/io_data_value.json"
#define SIP_CONFIG_FILE_NAME							"/mnt/nand1-2/Settings/sipcfg.cfg"
#define	WIFI_SSID_FILE_NAME								"/mnt/nand1-2/Settings/wifi_ssid.cfg"
#define NEW_IO_DATA_VALUE_NAME						"/mnt/nand1-2/Settings/new_io_data_value.json"

#define MS_LIST_TABLE_NAME								"/mnt/nand1-2/Settings/MS_ListTable.csv"
#define IM_NAME_LIST_TABLE_NAME							"/mnt/nand1-2/Settings/ImNameListTable.csv"
#define DEVICE_REGISTER_TABLE_NAME						"/mnt/nand1-2/Settings/DeviceRegisterTable.csv"
#define CALL_CONTROL_MAP_TABLE_NAME						"/mnt/nand1-2/Settings/CallControlTable.csv"
#define DEVICE_MAP_TABLE_NAME							"/mnt/nand1-2/Settings/NewDeviceMapTable.csv"
#define WLAN_IPC_MON_LIST_FILE_NAME						"/mnt/nand1-2/Settings/WLAN_IPC_MonRes_table.csv"
#define VIDEO_PROXY_FILE_NAME							"/mnt/nand1-2/Settings/VideoProxy.cfg"
#define AlarmingPara_FILE_NAME							"/mnt/nand1-2/Settings/AlarmingPara.json"

#define NEW_IO_DATA_VALUE_NAME						"/mnt/nand1-2/Settings/new_io_data_value.json"


#define UserDataFolder									"/mnt/nand1-2/UserData/"

#define JPEG_STORE_DIR									"/mnt/nand1-2/UserData/photo/"
#ifdef DONOT_WRITE_FLASH
#define CALL_RECORD_FILE_NAME							"/mnt/sdcard/call_record_table.csv"
#else
#define CALL_RECORD_FILE_NAME							"/mnt/nand1-2/UserData/call_record_table.csv"
#endif
#define BUSINESS_LOG_FILE								"/mnt/nand1-2/UserData/business.log"
#ifdef DONOT_WRITE_FLASH
#define	Easylogger_FILE									"/mnt/sdcard/elog_file.log"
#else
#define	Easylogger_FILE									"/mnt/nand1-2/UserData/elog_file.log"
#endif
#define AlarmingRecord_FILE_NAME						"/mnt/nand1-2/UserData/AlarmingRecord.json"
#define	NDM_RegisterFileName					  "NDM_Register.json"
#define	NDM_LogFileName					        "NDM_Log.json"
#define	NDM_S_LogFileName					      "NDM_S_Log.json"
#define	ElogJsonFileName								"Elog.json"

#define CERT_FILE				                "/mnt/nand1-2/UserData/cert.json"
#define LOG_DIR									        "/mnt/nand1-2/UserData/log/"
#define	CERT_LOG_FileName					      "CertLog.json"
#define REBOOT_FLAG_PATH								"/mnt/nand1-2/UserData/reboot.temp"
#define DeviceTableFileName							"DeviceTable.txt"
#define	SearchResult_FILE								"/mnt/nand1-2/UserData/searchResult.txt"

#define JPEG_STORE_DIR2									"/mnt/nand1-2/UserData/Photo/"
#define	ImCallRecordFileName						"ImCallRecord.json"
#define	EventRecordFileName						"EventRecord.json"

#define UserResFolder									  "/mnt/nand1-2/UserRes/"
#define MON_LIST_FILE_NAME								"/mnt/nand1-2/UserRes/rid1014_VideoResource.csv"
#define NAME_LIST_FILE_NAME								"/mnt/nand1-2/UserRes/rid1012_IMNameListTable.csv"
#define NAME_LIST_BAK_FILE_NAME							"/mnt/nand1-2/UserRes/rid1012_IMNameListTable.csv.bak"
#define IPC_MON_LIST_FILE_NAME							"/mnt/nand1-2/UserRes/IPC_MonRes_table.csv"
#define R8001_TABLE_NAME								"/mnt/nand1-2/UserRes/R8001.csv"
#define IM_COOKIE_TABLE_NAME							"/mnt/nand1-2/UserRes/R8018.csv"
#define MON_COOKIE_TABLE_NAME							"/mnt/nand1-2/UserRes/R8019.csv"
#define RESOURCE_TABLE_NAME								"/mnt/nand1-2/UserRes/ResourceTable.csv"
#define IPC_SETUP_FILE_NAME								"/mnt/nand1-2/UserRes/IPC_Setup.cfg"
#define IPC_WLAN_SETUP_FILE_NAME						"/mnt/nand1-2/UserRes/IPC_WLAN_Setup.cfg"
#define APP_IPC_LIST_FILE_NAME			    "/mnt/nand1-2/UserRes/APP_IPC_List.cfg"
#define EXT_MODULE_KEY_TABLE_FILE_NAME	"/mnt/nand1-2/UserRes/ExtModuleKeyTable.csv"
#define PublicResFileDir								"/mnt/nand1-2/UserRes/res_public/"
#define ResFileDir										"/mnt/nand1-2/UserRes/res_file/"
#define 	IX1_ResFileDir				"/mnt/nand1-2/res_file/"

#define CERT_DIR							        "/mnt/nand1-2/UserRes/CERT/"
#define CERT_DCFG_SELF_NAME				    "dcfg_self.json"

#define TEMP_Folder   									"/mnt/nand1-2/temp/"

#define TIME_TEMP_FILE   								"/mnt/nand1-2/temp/timeTemp.txt"
#define IxDeviceFileDir									"/mnt/nand1-2/temp/ix_device/"
#define IxDeviceTempFileDir								"/mnt/nand1-2/temp/ix_device/temp/"
#define REBOOT_FLAG_PATH_OLD							"/mnt/nand1-2/temp/reboot.temp"
#define	RSWR_TEMPFOLDER									"/mnt/nand1-2/temp/rswr_temp/"
#define	RSRD_TEMPFOLDER									"/mnt/nand1-2/temp/rsrd_temp/"
#define TFTP_SERVER_DIR_DEFAULT	        				"/mnt/nand1-2/temp/"
#define TFTP_CLEINT_DIR_DEFAULT		      				"/mnt/nand1-2/temp/"

#define AUTO_AGING_CONFIG_FILE_NAME		  			"/mnt/nand1-2/UserData/aging.cfg"
#define AUTO_AGING_FLASH_CHECK_FILE_NAME		  "/mnt/nand1-2/UserData/check_flash.json"

#define NAND_DOWNLOAD_PATH								"/mnt/nand1-2/vtkDownload"
#define SHELL_PATH   									    "/mnt/nand1-2/shell/"

#define IX611_Update_Folder   									"/mnt/nand1-2/temp/update/"


#define SDCARD_DEVICE_FILE								    "/dev/mmcblk0p1"    //不能修改，修改程序会错
#define DISK_SDCARD										        "/mnt/sdcard"       //不能修改，修改程序会错

#define SDCARD_DOWNLOAD_PATH							    "/mnt/sdcard/vtkDownload"
#define SDCARD_RES_PATH									      "/mnt/sdcard/RES/"
#define VIDEO_STORE_DIR									      "/mnt/sdcard/video/"
#define SdUpgradePath									        "/mnt/sdcard/SdUpgrade"
#define CustomerizedLanguageSDCoardFolder			"/mnt/sdcard/Customerized/language/"
#define WIFI_TEST_FILE		                    "/mnt/sdcard/wifi_ssid.txt"
#define MovementTestConfigFile		            "/mnt/sdcard/MovementTest.cfg"
#define AUTO_AGING_FLASH_CHECK_FILE_SOURCE		  "/mnt/sdcard/check_flash.json"
#define SIP_CONFIG_TEST_FILE_NAME							"/mnt/sdcard/sipcfg_test.cfg"
#define LOG_TB_PATH							              "/mnt/sdcard/log_tb.txt"
#define menu_bmp_path                         "/mnt/sdcard/menu_bmp/"


#endif

