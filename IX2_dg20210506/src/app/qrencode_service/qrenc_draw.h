
#ifndef _QRENC_DRAW_H
#define _QRENC_DRAW_H

#include "qrencode.h"

void printf_qrencode( const char* str );

/*******************************************************************************************
 * @fn:		qrenc_draw_bitmap
 *
 * @brief:	显示一个二维码
 *
 * @param:  	str 		- 需要编码的字符串
// * @param:  	layer_id - 二维码显示的图层
 * @param:  	x		- 二维码显示的水平位置
 * @param:  	y		- 二维码显示的垂直位置
 * @param:  	scaler	- 二维码显示的放大倍数
 * @param:  	scaler	- 二维码显示的面板宽度
 * @param:  	scaler	- 二维码显示的面板高度
 *
 * @return: none
 *******************************************************************************************/
int qrenc_draw_bitmap( const char* str, int x, int y, int scaler, int panel_w, int panel_h );


#endif



