

#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "qrenc_draw.h"
#include "menu_resource.h"

// test qrencode
void printf_qrencode( const char* str )
{
	int i,j;
	QRcode* qrcode;	
	qrcode = QRcode_encodeString8bit(str, 2, QR_ECLEVEL_L);

	printf("printf_qrencode[%s],version=%d,width=%d\n\n",str,qrcode->version,qrcode->width);

	for(i=0;i<qrcode->width;i++)
	{
	    for(j=0;j<qrcode->width;j++)
	    {
	        if(qrcode->data[i*qrcode->width+j]&0x01)
	        {
	            printf("*");

	        }
	        else
	        {
	            printf(" ");
	        }
	    }
	    printf("\n");
	}
	printf("\n");
}


/*******************************************************************************************
 * @fn:		qrenc_draw_bitmap
 *
 * @brief:	显示一个二维码
 *
 * @param:  	str 		- 需要编码的字符串
 * @param:  	x		- 二维码显示的水平位置
 * @param:  	y		- 二维码显示的垂直位置
 * @param:  	scaler	- 二维码显示的放大倍数
 * @param:  	scaler	- 二维码显示的面板宽度
 * @param:  	scaler	- 二维码显示的面板高度
 *
 * @return: none
 *******************************************************************************************/
int qrenc_draw_bitmap( const char* str, int x, int y, int scaler, int panel_w, int panel_h )
{
	QRcode* qrcode;	
	int dotw,doth;
	int clearsx,clearsy;

	clearsx = x;
	clearsy = y;

	if( scaler == 0 ) scaler = 1;
	dotw = scaler;
	doth = scaler;

	qrcode = QRcode_encodeString8bit(str, 2, QR_ECLEVEL_L);
	
	// 自适应显示居中
	while( (qrcode->width*dotw >= panel_w) || (qrcode->width*doth >= panel_h) )
	{
		if( dotw > 1 ) 
			dotw -= 1;
		else
			break;
		if( doth > 1 ) 
			doth -= 1;
		else
			break;
	}
	x += ((panel_w-qrcode->width*dotw)/2);
	y += ((panel_h-qrcode->width*doth)/2);	
	
	SetOneOsdLayerRectColor( OSD_LAYER_STRING, clearsx, clearsy, panel_w, panel_h, COLOR_KEY);
		
	draw_scaler_bitmap( OSD_LAYER_STRING, x, y, qrcode->width, qrcode->width, COLOR_BLACK, COLOR_WHITE, qrcode->data, 0x01, dotw, doth );
	//update display
	UpdateOsdLayerBuf2(OSD_LAYER_STRING, clearsx,clearsy,panel_w, panel_h,0);
	return 0;
}



