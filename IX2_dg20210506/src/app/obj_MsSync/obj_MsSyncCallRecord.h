/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_MsSyncCallRecord_H
#define _obj_MsSyncCallRecord_H

#pragma pack(1)
// GetIpBy Number 指令包结构

typedef struct
{
	MsSyncMsgHead_T head;
	int record_type;
}MsSyncCallRecordMsg_T;
#if 0
typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				getDeviceCnt;		//等待接收几个
	GetIpRspData 	data;
} GetIpRsp__;
#endif
#pragma pack()

int API_MsSyncCallRecordCheckedMissCall(void);
void ReceiveMsSyncCallRecordReq(int target_ip, MsSyncCallRecordMsg_T* recv_msg);

#endif


