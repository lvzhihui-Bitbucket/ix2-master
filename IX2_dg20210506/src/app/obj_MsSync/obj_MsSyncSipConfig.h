/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_MsSyncSipConfig_H
#define _obj_MsSyncSipConfig_H

#include "obj_MsSyncCallScene.h"
#include "task_VideoMenu.h"
#include "MenuSipConfig_Business.h"	//czn20214028
#pragma pack(1)
// GetIpBy Number 指令包结构

typedef struct
{
	MsSyncMsgHead_T head;
}MsSyncSipConfigReqMsg_T;

typedef struct
{
	MsSyncMsgHead_T head;
	int InternetState;
	int SipReg_State;
	int SipReg_ErrCode;
	//SipCfg_T sipCfg;
}MsSyncSipConfigRspMsg_T;
#if 0
typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				getDeviceCnt;		//等待接收几个
	GetIpRspData 	data;
} GetIpRsp__;
#endif
#pragma pack()

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				verify_flag;
} MsSyncSipConfigWaitRsp_T;


int API_MsSyncSipConfig(int op_mode);		
void ReceiveMsSyncSipConfigReq(int target_ip, MsSyncSipConfigReqMsg_T* recv_msg);
void ReceiveMsSyncSipConfigRsp(int target_ip, MsSyncSipConfigRspMsg_T* recv_msg);

#endif


