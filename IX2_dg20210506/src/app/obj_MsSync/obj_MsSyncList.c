/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "../device_manage/obj_SYS_VER_INFO.h"
#include "../vtk_udp_stack/vtk_udp_stack_device_update.h"
#include "obj_MsSyncCallScene.h"
#include "obj_MsSyncList.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "../task_io_server/task_IoServer.h"
#include "../task_io_server/obj_TableProcess.h"


static MsSyncListWaitRsp_T MsSyncListWaitRsp[CLIENT_MAX_NUM]={0};
#define 	Max_MsSyncList_WaitTime	1

extern int GetImNameListRecordCnt(void);
extern int GetMSListRecordCnt(void);
extern int Get_MonRes_Num(void);
extern int Get_IPC_MonRes_Num(void);

extern void MonRes_ReLoad(void);
extern void IPC_MonRes_Reload(void);
extern void ImNameList_Reload(void);
extern void MSList_Reload(void);

extern int ClearImNamelist_Table(void);
extern int ClearMonRes_Table(void);
extern int ClearMSlist_Table(void);
extern int Clear_IPC_MonRes_Record(void);

const ListPathTable_T ListPathTable[] = 
{
	{MsSyncListId_Namelist,IM_NAME_LIST_TABLE_NAME,GetImNameListRecordCnt,ImNameList_Reload,ClearImNamelist_Table},
	{MsSyncListId_MsNamelist,MS_LIST_TABLE_NAME,GetMSListRecordCnt,MSList_Reload,ClearMSlist_Table},
	{MsSyncListId_MonitorList,MON_LIST_FILE_NAME,Get_MonRes_Num,MonRes_ReLoad,ClearMonRes_Table},
	{MsSyncListId_IPCList,IPC_MON_LIST_FILE_NAME,Get_IPC_MonRes_Num,IPC_MonRes_Reload,Clear_IPC_MonRes_Record},	
};

const ListPathTable_num = sizeof(ListPathTable)/sizeof(ListPathTable[0]);

const int MsSyncListRspMsg_Head = sizeof(MsSyncMsgHead_T)+28;
//const int MsSyncListRspMsg_FirstFragDataStart = 16;

char *GetListPathById(int list_id)
{
	int i;
	
	for(i = 0;i < ListPathTable_num;i++)
	{
		if(ListPathTable[i].list_id == list_id)
		{
			return ListPathTable[i].list_path;
		}
	}

	return NULL;
}

int GetListNumsById(int list_id)
{
	int i;
	
	for(i = 0;i < ListPathTable_num;i++)
	{
		if(ListPathTable[i].list_id == list_id)
		{
			if(ListPathTable[i].get_list_nums == NULL)
				return 0;
			return ListPathTable[i].get_list_nums();
		}
	}

	return 0;
}

void MsSyncListReLoad(int list_id)
{
	int i;
	
	for(i = 0;i < ListPathTable_num;i++)
	{
		if(ListPathTable[i].list_id == list_id)
		{
			if(ListPathTable[i].list_reload!= NULL)
				ListPathTable[i].list_reload();
			break;
		}
	}
}

void MsSyncListDeleteAll(int list_id)
{
	int i;
	
	for(i = 0;i < ListPathTable_num;i++)
	{
		if(ListPathTable[i].list_id == list_id)
		{
			if(ListPathTable[i].get_list_nums != NULL)
			{
				if(ListPathTable[i].get_list_nums() == 0)
					return;
			}
			if(ListPathTable[i].list_delete_all != NULL)
			{
				ListPathTable[i].list_delete_all();
				return;
			}
		}
	}
}
//发送By number搜索指令
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : 超时时间 单位S
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -2:超时,-3 传输错误-1,线程超限0,Master没有表  >0:查找完成
int API_MsSyncListReq(int list_id)
{
	MsSyncListReqMsg_T sendData;
	SYS_VER_INFO_T	sysVerInfoData;
	int i, timeCnt;
	int recvCnt;
	
	sysVerInfoData = GetSysVerInfo();

	

	memset(&sendData, 0, sizeof(sendData));

	sendData.head.rand = MyRand();
	sendData.head.op_code = MS_SYNC_OP_List;
	sendData.list_id = list_id;
	memcpy(sendData.head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,10);


	rejoin_multicast_group();
	for(i = 0;i < CLIENT_MAX_NUM&&MsSyncListWaitRsp[i].waitFlag == 1;i++);
	
	if(i >= CLIENT_MAX_NUM)
		return -1;
	
	MsSyncListWaitRsp[i].waitFlag = 1;
	MsSyncListWaitRsp[i].rand = sendData.head.rand;
	MsSyncListWaitRsp[i].verify_flag = 0;
	MsSyncListWaitRsp[i].data_offset = 0;
	MsSyncListWaitRsp[i].file_length = 0;
	MsSyncListWaitRsp[i].list_id = list_id;
	memset(MsSyncListWaitRsp[i].md5,0,16);
	recvCnt = 0;
	
	//strcpy(MsSyncListWaitRsp[i].file_path,file_path);
	
	int ixDeviceSelectNetwork = IX_DeviceSelectNetwork();
	
	if(ixDeviceSelectNetwork & 0x02)
	{
		sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	if(ixDeviceSelectNetwork & 0x01)
	{
		sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		api_udp_device_update_send_data_by_device(NET_WLAN0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	
	//api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	//usleep(UDP_WAIT_RSP_TIME*1000);	
	//api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
		
	timeCnt = Max_MsSyncList_WaitTime*1000/UDP_WAIT_RSP_TIME;
	
	while(MsSyncListWaitRsp[i].verify_flag == 0)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(recvCnt < MsSyncListWaitRsp[i].data_offset)
		{
			timeCnt = Max_MsSyncList_WaitTime*5*1000/UDP_WAIT_RSP_TIME;
			recvCnt = MsSyncListWaitRsp[i].data_offset;
		}
		else if(--timeCnt <= 0)
		{
			break;
		}
	}
	
	MsSyncListWaitRsp[i].waitFlag = 0;
	
	if(MsSyncListWaitRsp[i].verify_flag == 1)
	{
		MsSyncListReLoad(MsSyncListWaitRsp[i].list_id);
		return GetListNumsById(MsSyncListWaitRsp[i].list_id);
	}
	else if(MsSyncListWaitRsp[i].verify_flag == -1)
	{
		return -3;
	}
	else if(MsSyncListWaitRsp[i].verify_flag == -2)
	{
		MsSyncListDeleteAll(MsSyncListWaitRsp[i].list_id);
		return 0;
	}
	else
	{
		if(recvCnt== 0)
		{
			return -2;
		}
		return -3;
	}
	
}



//接收到By NUMBER搜索指令
void ReceiveMsSyncListReq(int target_ip, MsSyncListReqMsg_T* recv_msg)
{

	SYS_VER_INFO_T	sysVerInfoData;
	int myIp, check;
	int len;
	static int save_rand = 0;
	static int save_time = 0;
	static int save_ip = 0;
	int cur_time;
	char *file_path;
	char src_dir[80],src_file[80],tar_dir[80],tar_file[80];
	
	MsSyncListRspMsg_T sendData;
	int i;
	
	check = 0;
	sysVerInfoData = GetSysVerInfo();
	myIp = inet_addr(sysVerInfoData.ip);

	
	if(memcmp(recv_msg->head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,8)!=0 || memcmp(sysVerInfoData.ms,"01",2) != 0)
	{
		return;
	}
	
	cur_time = time(NULL);
	
	if(save_rand == recv_msg->head.rand && abs(save_time-cur_time) < 2 && save_ip == recv_msg->head.sourceIp)
	{
		return;
	}

	save_rand = recv_msg->head.rand;
	save_time = cur_time;
	save_ip = recv_msg->head.sourceIp;

	memset(&sendData, 0, sizeof(sendData));
	
	sendData.head.rand = recv_msg->head.rand;
	sendData.head.sourceIp = myIp;
	sendData.head.op_code = MS_SYNC_OP_List;
	sendData.list_id = recv_msg->list_id;
	
	if(GetListNumsById(recv_msg->list_id) <= 0)
	{
		sendData.result= 0;
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
		return;
	}
	
	file_path = GetListPathById(recv_msg->list_id);

	if(file_path == NULL)
	{
		sendData.result= 0;
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
		return;
	}

	FILE *pf = fopen(file_path,"r");
	int file_length;
	int frag_no = 0;
	//int file_offset = 0;

	if(pf == NULL)
	{
		sendData.result= 0;
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
		return;
	}

	fseek(pf,0,SEEK_END);
	file_length = ftell(pf);
	fseek(pf,0,SEEK_SET);
	fclose(pf);
	sendData.result= 1;
	sendData.file_length = file_length;
	api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
	for(i = strlen(file_path)-1;i >= 0;i--)
	{
		if(file_path[i] == '/')
		{
			memcpy(src_dir,file_path,i);
			src_dir[i] = 0;
			strcpy(src_file,file_path+i+1);
			strcpy(tar_dir,src_dir);
			strcpy(tar_file,src_file);
			strcat(tar_file,".tmp");
			break;
		}
	}
	if(file_path[i] != '/')
	{
		sendData.result= 0;
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
		return;	
	}
	
	if(api_server_tftp_trans(target_ip,src_dir,src_file,tar_dir,tar_file) == 0)
	{
		sendData.result= 2;
		sendData.file_length = file_length;
		
	}
	else
	{
		sendData.result= -1;
	}
	api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
	#if 0
	if(file_length <= MsSyncListMsgDataLength)
	{
		frag_no = 1;
	}
	else
	{
		 frag_no = (file_length/MsSyncListMsgDataLength)+((file_length%MsSyncListMsgDataLength)?1:0);
	}
	
	if(frag_no<=1)
	{
		sendData.head.fragment_flag = 0;
		sendData.frag_length = file_length;
		fread(sendData.data,1,sendData.frag_length,pf);
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, MsSyncListRspMsg_Head+ sendData.frag_length);
	}
	else
	{
		for(i=0;i < (frag_no-1);i++)
		{
			sendData.head.fragment_flag = (MsSyncPkt_Frag_En|(i*MsSyncListMsgDataLength));
			sendData.frag_length = MsSyncListMsgDataLength;
			fread(sendData.data,1,sendData.frag_length,pf);
			api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, MsSyncListRspMsg_Head+ sendData.frag_length);
			usleep(UDP_WAIT_RSP_TIME/5*1000);
		}
		sendData.head.fragment_flag = (MsSyncPkt_Frag_En|MsSyncPkt_Frag_Last|(i*MsSyncListMsgDataLength));
		sendData.frag_length = file_length - (i*MsSyncListMsgDataLength);
		fread(sendData.data,1,sendData.frag_length,pf);
		api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, MsSyncListRspMsg_Head+ sendData.frag_length);
	}
	fclose(pf);
	#endif
}

void ReceiveMsSyncListRsp(int target_ip, MsSyncListRspMsg_T* recv_msg)
{
	int i;
	int file_offset;
	char *file_path,file_tmp[100];
	FILE *pf;
	char *pdata;
	char md5[16];
	
	for(i = 0;i < CLIENT_MAX_NUM;i++)
	{
		if(MsSyncListWaitRsp[i].waitFlag)
		{
			if(MsSyncListWaitRsp[i].rand == recv_msg->head.rand)
			{
				break;
			}
		}
	}
	
	if(i >= CLIENT_MAX_NUM)
	{
		return;
	}
	
	//file_offset = (recv_msg->head.fragment_flag&MsSyncPkt_Offset_Mask);
#if 0
	if(file_offset == 0)
	{
		if(recv_msg->list_legth>0)
		{
			MsSyncListWaitRsp[i].file_length = recv_msg->list_legth;
			memcpy(MsSyncListWaitRsp[i].md5,recv_msg->md5,16);
		}
		else
		{
			MsSyncListWaitRsp[i].verify_flag = -2;
			return;
		}
	}
#endif
	file_path = GetListPathById(MsSyncListWaitRsp[i].list_id);
	
	if(file_path == NULL)
	{
		return;
	}
	
	sprintf(file_tmp,"%s.tmp",file_path);
	
	if(recv_msg->result == 0)
	{
		MsSyncListWaitRsp[i].verify_flag = -2;
		return;
	}
	else if(recv_msg->result == 1)
	{
		MsSyncListWaitRsp[i].file_length = recv_msg->file_length;
		MsSyncListWaitRsp[i].data_offset = 1;
	}
	else if(recv_msg->result == -1)
	{
		MsSyncListWaitRsp[i].verify_flag = -1;
	}
	else if(recv_msg->result == 2)
	{
		remove(file_path);
		char cmd_line[200];
		snprintf(cmd_line,199,"mv %s %s",file_tmp,file_path);
		system(cmd_line);
		sync();
		MsSyncListWaitRsp[i].verify_flag = 1;
	}
		
	#if 0
	
	pf = fopen(file_tmp,"a+");

	if(pf == NULL)
	{
		return;
	}
	pdata = recv_msg->data;
	
	fwrite(pdata,1,recv_msg->frag_length,pf);
	fclose(pf);

	MsSyncListWaitRsp[i].data_offset += recv_msg->frag_length;

	if(MsSyncListWaitRsp[i].data_offset == MsSyncListWaitRsp[i].file_length)
	{
		FileMd5_Calculate(file_tmp,md5);
		if(!memcmp(MsSyncListWaitRsp[i].md5,md5,16))
		{
			remove(file_path);
			char cmd_line[200];
			snprintf(cmd_line,199,"mv %s %s",file_tmp,file_path);
			system(cmd_line);
			sync();
			MsSyncListWaitRsp[i].verify_flag = 1;
		}
		else
		{
			remove(file_tmp);
			MsSyncListWaitRsp[i].verify_flag = -1;
		}
	}
	#endif
	
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

