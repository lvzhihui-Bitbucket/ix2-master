/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_MsSyncCallScene_H
#define _obj_MsSyncCallScene_H

#define	MsSyncPkt_Frag_En		(1<<31)
#define	MsSyncPkt_Frag_Last	(1<<30)
#define	MsSyncPkt_Offset_Mask	(~(MsSyncPkt_Frag_En|MsSyncPkt_Frag_Last))
#pragma pack(1)
// GetIpBy Number 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		op_code;
	char		source_bd_rm_ms[10+1];		//BD_RM_MS
	int		sourceIp;
	int		fragment_flag;
} MsSyncMsgHead_T;
typedef struct
{
	MsSyncMsgHead_T head;
	int new_callscene;
}MsSyncCallSceneSetMsg_T;
#if 0
typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				getDeviceCnt;		//等待接收几个
	GetIpRspData 	data;
} GetIpRsp__;
#endif
#pragma pack()

int API_MsSyncCallSceneSet(int new_callscene);
void ReceiveMsSyncCallSceneSetReq(int target_ip, MsSyncCallSceneSetMsg_T* recv_msg);
void ReceiveMsSyncReq(int target_ip, MsSyncMsgHead_T* recv_msg);

#endif


