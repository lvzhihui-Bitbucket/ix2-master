/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_MsSyncCallScene.h"
#include "obj_MsSyncCallRecord.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"


//static GetIpRsp getIpFromNet[CLIENT_MAX_NUM];



//发送By number搜索指令
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : 超时时间 单位S
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
int API_MsSyncCallRecordCheckedMissCall(void)
{
	MsSyncCallRecordMsg_T sendData;
	SYS_VER_INFO_T	sysVerInfoData;
	int i, timeCnt;
	
	sysVerInfoData = GetSysVerInfo();

	

	memset(&sendData, 0, sizeof(sendData));

	sendData.head.rand = MyRand();
	sendData.head.op_code = MS_SYNC_OP_CallRecord;
	sendData.record_type = 0;
	memcpy(sendData.head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,10);


	rejoin_multicast_group();
	int ixDeviceSelectNetwork = IX_DeviceSelectNetwork();

	for(i = 0; i < 2; i++) 
	{
		if(ixDeviceSelectNetwork & 0x02)
		{
			sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
			api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
		}
		
		if(ixDeviceSelectNetwork & 0x01)
		{
			sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
			api_udp_device_update_send_data_by_device(NET_WLAN0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
		}
		
		usleep(UDP_WAIT_RSP_TIME*1000);
	}
}



//接收到By NUMBER搜索指令
void ReceiveMsSyncCallRecordReq(int target_ip, MsSyncCallRecordMsg_T* recv_msg)
{
	SYS_VER_INFO_T	sysVerInfoData;
	int myIp, check;
	int len;
	static int save_rand = 0;
	static int save_time = 0;
	static int save_ip = 0;
	int cur_time;
	check = 0;
	sysVerInfoData = GetSysVerInfo();
	myIp = inet_addr(sysVerInfoData.ip);

	
	if(memcmp(recv_msg->head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,8)!=0)
	{
		return;
	}
	
	cur_time = time(NULL);
	
	if(save_rand == recv_msg->head.rand && abs(save_time-cur_time) < 2 && save_ip == recv_msg->head.sourceIp)
	{
		return;
	}

	save_rand = recv_msg->head.rand;
	save_time = cur_time;
	save_ip = recv_msg->head.sourceIp;
	
	if(recv_msg->record_type == 0)
	{
		call_record_allmiss_mark_read();
	}
}



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

