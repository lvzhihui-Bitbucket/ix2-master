/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "../device_manage/obj_SYS_VER_INFO.h"
#include "../vtk_udp_stack/vtk_udp_stack_device_update.h"
#include "obj_MsSyncCallScene.h"
#include "obj_MsSyncSipConfig.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "../task_io_server/task_IoServer.h"


//static GetIpRsp getIpFromNet[CLIENT_MAX_NUM];
static MsSyncSipConfigWaitRsp_T MsSyncSipConfigWaitRsp[CLIENT_MAX_NUM]={0};
#define 	Max_MsSyncSipConfig_WaitTime	1


static int MsSync_SipReg_State = 0;
static int MsSync_SipReg_ErrCode = 0;
static int MsSync_InternetState = 0;
//static SipCfg_T MsSync_sipCfg = {0};

int Get_MsSync_SipReg_State(void)
{
	return MsSync_SipReg_State;
}

int Get_MsSync_SipReg_ErrCode(void)
{
	return MsSync_SipReg_ErrCode;
}

SipCfg_T GetMsSync_sipCfg(void)
{
	//return MsSync_sipCfg;
}

int GetMsSyncInternetState(void)
{
	return MsSync_InternetState;
}

//发送By number搜索指令
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : 超时时间 单位S
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
//op_mode 1,同步执行，0，异步执行
int API_MsSyncSipConfig(int op_mode)		
{
	MsSyncSipConfigReqMsg_T sendData;
	SYS_VER_INFO_T	sysVerInfoData;
	int i, timeCnt;
	int recvCnt;
	
	sysVerInfoData = GetSysVerInfo();

	

	memset(&sendData, 0, sizeof(sendData));

	sendData.head.rand = MyRand();
	sendData.head.sourceIp = inet_addr(sysVerInfoData.ip);
	sendData.head.op_code = MS_SYNC_OP_SipConfig;

	memcpy(sendData.head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,10);


	rejoin_multicast_group();
	if(op_mode)
	{
		for(i = 0;i < CLIENT_MAX_NUM&&MsSyncSipConfigWaitRsp[i].waitFlag == 1;i++);
		
		if(i >= CLIENT_MAX_NUM)
			return -1;
		
		MsSyncSipConfigWaitRsp[i].waitFlag = 1;
		MsSyncSipConfigWaitRsp[i].rand = sendData.head.rand;
		MsSyncSipConfigWaitRsp[i].verify_flag = 0;
	}
	
	recvCnt = 0;
	
	int ixDeviceSelectNetwork = IX_DeviceSelectNetwork();
	
	if(ixDeviceSelectNetwork & 0x02)
	{
		sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	if(ixDeviceSelectNetwork & 0x01)
	{
		sendData.head.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		api_udp_device_update_send_data_by_device(NET_WLAN0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	
	//api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );
	//usleep(UDP_WAIT_RSP_TIME*1000);	
	//api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(MS_SYNC_REQ), (char*)&sendData, sizeof(sendData) );

	if(!op_mode)
	{
		usleep(20*1000);
		return 0;
	}
	
	timeCnt = Max_MsSyncSipConfig_WaitTime*1000/UDP_WAIT_RSP_TIME;
	
	while(MsSyncSipConfigWaitRsp[i].verify_flag == 0)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(--timeCnt <= 0)
		{
			break;
		}
	}
	
	MsSyncSipConfigWaitRsp[i].waitFlag = 0;
	
	if(MsSyncSipConfigWaitRsp[i].verify_flag == 1)
	{
		return 0;
	}
	else
	{
		return -2;
	}
}


extern SipCfg_T sipCfg;
//接收到By NUMBER搜索指令
void ReceiveMsSyncSipConfigReq(int target_ip, MsSyncSipConfigReqMsg_T* recv_msg)
{
	SYS_VER_INFO_T	sysVerInfoData;
	MsSyncSipConfigRspMsg_T sendData;
	int myIp, check;
	int len;
	static int save_rand = 0;
	static int save_time = 0;
	static int save_ip = 0;
	int cur_time;
	check = 0;
	sysVerInfoData = GetSysVerInfo();
	myIp = inet_addr(sysVerInfoData.ip);

	
	if(memcmp(recv_msg->head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,8)!=0||memcmp(sysVerInfoData.ms,"01",2) != 0)
	{
		return;
	}
	
	cur_time = time(NULL);
	
	if(save_rand == recv_msg->head.rand && abs(save_time-cur_time) < 2 && save_ip == recv_msg->head.sourceIp)
	{
		return;
	}

	save_rand = recv_msg->head.rand;
	save_time = cur_time;
	save_ip = recv_msg->head.sourceIp;
	

	memset(&sendData, 0, sizeof(sendData));
	
	sendData.head.rand = recv_msg->head.rand;
	sendData.head.sourceIp = myIp;
	sendData.head.op_code = MS_SYNC_OP_SipConfig;
	memcpy(sendData.head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,10);
	
	sendData.InternetState = GetInternetState();
	sendData.SipReg_State = Get_SipAccount_State();
	sendData.SipReg_ErrCode = Get_SipReg_ErrCode();
	//sendData.sipCfg = sipCfg;
		
	api_udp_device_update_send_data(target_ip, htons(MS_SYNC_RSP), (char*)&sendData, sizeof(sendData) );
}


void ReceiveMsSyncSipConfigRsp(int target_ip, MsSyncSipConfigRspMsg_T* recv_msg)
{
	int i;
	SYS_VER_INFO_T	sysVerInfoData;
	sysVerInfoData = GetSysVerInfo();
	
	if(memcmp(recv_msg->head.source_bd_rm_ms,sysVerInfoData.bd_rm_ms,8)!=0)
		return;
	
	MsSync_InternetState = recv_msg->InternetState;
	MsSync_SipReg_State = recv_msg->SipReg_State;
	MsSync_SipReg_ErrCode = recv_msg->SipReg_ErrCode;
	//MsSync_sipCfg = recv_msg->sipCfg;
	
	for(i = 0;i < CLIENT_MAX_NUM;i++)
	{
		if(MsSyncSipConfigWaitRsp[i].waitFlag)
		{
			if(MsSyncSipConfigWaitRsp[i].rand == recv_msg->head.rand)
			{
				break;
			}
		}
	}
	
	if(i >= CLIENT_MAX_NUM)
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MsSyncSipConfig);
	}
	else
	{
		MsSyncSipConfigWaitRsp[i].verify_flag = 1;
	}
	
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

