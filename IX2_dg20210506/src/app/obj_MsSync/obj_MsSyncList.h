/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_MsSyncList_H
#define _obj_MsSyncList_H

#include "obj_MsSyncCallScene.h"

#define MsSyncListMsgDataLength		500
enum
{
	MsSyncListId_Namelist = 0,
	MsSyncListId_MsNamelist,
	MsSyncListId_MonitorList,
	MsSyncListId_IPCList,
};


#pragma pack(1)
// GetIpBy Number 指令包结构
typedef struct
{
	MsSyncMsgHead_T head;
	int list_id;
}MsSyncListReqMsg_T;

typedef struct
{
	MsSyncMsgHead_T head;
	int list_id;
	int result;
	int file_length;
	#if 0
	int list_legth;
	int frag_length;
	char md5[16];
	//union
	//{
	unsigned char data[MsSyncListMsgDataLength];
	//};
	#endif
}MsSyncListRspMsg_T;



#pragma pack()

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				list_id;
	int				verify_flag;
	int				file_length;
	int				data_offset;
	char 			md5[16];
} MsSyncListWaitRsp_T;

typedef struct
{
	int		list_id;
	char		*list_path;
	int 	(*get_list_nums)(void);
	void 	(*list_reload)(void);
	void 	(*list_delete_all)(void);
}ListPathTable_T;

int API_MsSyncListReq(int list_id);
void ReceiveMsSyncListReq(int target_ip, MsSyncListReqMsg_T* recv_msg);
void ReceiveMsSyncListRsp(int target_ip, MsSyncListRspMsg_T* recv_msg);

#endif


