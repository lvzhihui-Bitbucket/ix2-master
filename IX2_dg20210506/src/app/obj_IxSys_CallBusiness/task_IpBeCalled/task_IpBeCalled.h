/**
  ******************************************************************************
  * @file    task_BeCalled.h
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_IpBeCalled_H
#define _task_IpBeCalled_H

//#include "task_Survey.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"

typedef enum
{
	IpBeCalled_IxSys,
	IpBeCalled_LinphoneCall,
	IpBeCalled_NewIxSys,
}IpBeCalled_CallType_t;

extern OBJ_BECALLED_STRU IpBeCalled_Obj;

uint8 Get_IpBeCalled_State(void);
void* vdp_ipbecalled_task( void* arg );

//#include "BSP.h"
//#include "task_Survey.h"

// Define Task Vars and Structures----------------------------------------------
	 //run.state
int API_IpBeCalled_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr);
void IpBeCalled_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);


#define API_IpBeCalled_Invite(call_type,s_addr)		API_IpBeCalled_Common(BECALLED_MSG_INVITE,call_type,s_addr,NULL)
#define API_IpBeCalled_Ack(call_type)					API_IpBeCalled_Common(BECALLED_MSG_ACK,call_type,NULL,NULL)
#define API_IpBeCalled_Cancel(call_type)				API_IpBeCalled_Common(BECALLED_MSG_CANCEL,call_type,NULL,NULL)
#define API_IpBeCalled_Bye(call_type)					API_IpBeCalled_Common(BECALLED_MSG_BYE,call_type,NULL,NULL)
#define API_IpBeCalled_ForceClose()					API_IpBeCalled_Common(BECALLED_MSG_FORCECLOSE,0,NULL,NULL)
#define API_IpBeCalled_Unlock1(call_type)				API_IpBeCalled_Common(BECALLED_MSG_UNLOCK1,call_type,NULL,NULL)
#define API_IpBeCalled_Unlock2(call_type)				API_IpBeCalled_Common(BECALLED_MSG_UNLOCK2,call_type,NULL,NULL)

#endif
