/**
  ******************************************************************************
  * @file    task_BeCalled.c
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>

#include "obj_BeCalled_State.h"
#include "task_CallServer.h"
#include "task_IpBeCalled.h"
//#include "task_Caller.h"
#include "obj_IpBeCalled_IxSys_Callback.h"
#include "obj_IpBeCalled_LinPhoneCall_Callback.h"
#include "obj_IpBeCalled_NewIxSys_Callback.h"


//软定时_Caller定时结束

#include "OSTIME.h"
#include "task_Ring.h"
#include "task_Power.h"
#include "obj_menu_data.h"
#include "task_Event.h"
#include "task_monitor.h"

OS_TIMER timer_ipbecalled;

Loop_vdp_common_buffer	vdp_ipbecalled_mesg_queue;
Loop_vdp_common_buffer	vdp_ipbecalled_sync_queue;

vdp_task_t	task_ipbecalled;

OBJ_BECALLED_STRU IpBeCalled_Obj;

void	IpBeCalled_Config_Data_Init(void);
void IpBeCalled_MenuDisplay_ToRing(void);
void IpBeCalled_MenuDisplay_ToAck(void);
void IpBeCalled_MenuDisplay_ToBye(void);
void IpBeCalled_MenuDisplay_ToWait(void);
void IpBeCalled_Timer_Callback(void);

#define IpBeCalled_Support_CallTypeNum	3

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOACK_CALLBACK[IpBeCalled_Support_CallTypeNum]={

	
	{IpBeCalled_IxSys, 				Callback_BeCalled_ToAck_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 				Callback_BeCalled_ToAck_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 				Callback_BeCalled_ToAck_NewIxSys,},

};



const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOBYE_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	{IpBeCalled_IxSys, 				Callback_BeCalled_ToBye_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 				Callback_BeCalled_ToBye_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 				Callback_BeCalled_ToBye_NewIxSys,},
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOREDIAL_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 				Callback_BeCalled_ToRedial_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 				Callback_BeCalled_ToRedial_LinPhoneCall,},	
	{IpBeCalled_NewIxSys, 				Callback_BeCalled_ToRedial_NewIxSys,},

		
};

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TORINGING_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToRinging_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToRinging_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToRinging_NewIxSys,},
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOTRANSFER_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			NULL,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			NULL,},
	{IpBeCalled_NewIxSys, 			NULL,},
	
	//{IpBeCalled_LinphoneCall, 		NULL,},
	
};
	

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOUNLOCK_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToUnlock_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToUnlock_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToUnlock_NewIxSys,},
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOTIMEOUT_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToTimeout_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToTimeout_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToTimeout_NewIxSys,},
};



const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOWAITING_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToWaiting_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToWaiting_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToWaiting_NewIxSys,},
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOERROR_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToError_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToError_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToError_NewIxSys,},
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPBECALLED_TOFORCECLOSE_CALLBACK[IpBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{IpBeCalled_IxSys, 			Callback_BeCalled_ToForceClose_IxSys,},	//小区主机呼叫分机
	{IpBeCalled_LinphoneCall, 			Callback_BeCalled_ToForceClose_LinPhoneCall,},
	{IpBeCalled_NewIxSys, 			Callback_BeCalled_ToForceClose_NewIxSys,},
};

/*------------------------------------------------------------------------
					BeCalled子任务初始化
------------------------------------------------------------------------*/
void IpBeCalled_Obj_Init(void)	//R_
{
	//状态(WAITING == 原STANDBY)
	IpBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
	IpBeCalled_Obj.timer_becalled = &timer_ipbecalled;
	IpBeCalled_Obj.task_becalled = &task_ipbecalled;
	
	IpBeCalled_Obj.Config_Data_Init = IpBeCalled_Config_Data_Init;

	IpBeCalled_Obj.MenuDisplay_ToRing= IpBeCalled_MenuDisplay_ToRing;
	IpBeCalled_Obj.MenuDisplay_ToAck = IpBeCalled_MenuDisplay_ToAck;
	IpBeCalled_Obj.MenuDisplay_ToBye = IpBeCalled_MenuDisplay_ToBye;
	IpBeCalled_Obj.MenuDisplay_ToWait = IpBeCalled_MenuDisplay_ToWait;

	IpBeCalled_Obj.support_calltype_num = IpBeCalled_Support_CallTypeNum;
	IpBeCalled_Obj.to_ack_callback	= TABLE_CALLTYPE_IPBECALLED_TOACK_CALLBACK;
	IpBeCalled_Obj.to_ring_callback = TABLE_CALLTYPE_IPBECALLED_TORINGING_CALLBACK;
	IpBeCalled_Obj.to_bye_callback = TABLE_CALLTYPE_IPBECALLED_TOBYE_CALLBACK;
	IpBeCalled_Obj.to_wait_callback = TABLE_CALLTYPE_IPBECALLED_TOWAITING_CALLBACK;
	IpBeCalled_Obj.to_forceclose_callback = TABLE_CALLTYPE_IPBECALLED_TOFORCECLOSE_CALLBACK;
	IpBeCalled_Obj.to_timeout_callback = TABLE_CALLTYPE_IPBECALLED_TOTIMEOUT_CALLBACK;
	IpBeCalled_Obj.to_error_callback = TABLE_CALLTYPE_IPBECALLED_TOERROR_CALLBACK;
	IpBeCalled_Obj.to_unclock_callback = TABLE_CALLTYPE_IPBECALLED_TOUNLOCK_CALLBACK;
	
	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_ipbecalled, IpBeCalled_Timer_Callback, 1000/25);
}



/*==============================================================================
					BeCalled_API
==============================================================================*/
/*uint8 API_BeCalled_Common(uint8 msg_type_temp, uint8 call_type_temp, 
						  uint16 address_s_temp,uint16 address_t_temp)	//R
{
	BECALLED_STRUCT	send_msg_to_BeCalled;	
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg_to_BeCalled.phone_type = PHONE_TYPE_BECALLED;
	send_msg_to_BeCalled.msg_type   = msg_type_temp;
	send_msg_to_BeCalled.address_s 	= address_s_temp;
	send_msg_to_BeCalled.address_t 	= address_t_temp;
	send_msg_to_BeCalled.call_type 	= call_type_temp;
//	if(OS_GetTaskID() == &tcb_phone)
//	{
//		vtk_TaskProcessEvent_BeCalled(&send_msg_to_BeCalled);
//		return 0;							  
//	}
	if (OS_Q_Put(&q_phone, &send_msg_to_BeCalled, sizeof(BECALLED_STRUCT)))
	{
		return 1;
	}
	
	return 0;
}
*/
/*------------------------------------------------------------------------
			Get_BeCalled_State
返回：
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_IpBeCalled_State(void)
{
	if (IpBeCalled_Obj.BeCalled_Run.state == BECALLED_WAITING)
	{
		return (0);
	}
	return (1);
}

/*------------------------------------------------------------------------
			Get_BeCalled_PartnerAddr
------------------------------------------------------------------------*/






void* vdp_ipbecalled_task( void* arg );

/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void ipbecalled_mesg_data_process(void *Msg,int len )	//R_
{
	bprintf("!!!ipbecalled state = %d,recv msg = %d\n",IpBeCalled_Obj.BeCalled_Run.state,((BECALLED_STRUCT *)Msg)->msg_type);
	vtk_TaskProcessEvent_BeCalled(&IpBeCalled_Obj,(BECALLED_STRUCT *)Msg);
}

//OneCallType	OneMyCallObject;

void init_vdp_ipbecalled_task(void)
{
	// 初始化一个呼叫对象	
	//memset( (char*)&OneMyCallObject, 0, sizeof(OneMyCallObject) );
	

	init_vdp_common_queue(&vdp_ipbecalled_mesg_queue, 1000, (msg_process)ipbecalled_mesg_data_process, &task_ipbecalled);
	init_vdp_common_queue(&vdp_ipbecalled_sync_queue, 100, NULL, 								  &task_ipbecalled);
	init_vdp_common_task(&task_ipbecalled, MSG_ID_IpBeCalled, vdp_ipbecalled_task, &vdp_ipbecalled_mesg_queue, &vdp_ipbecalled_sync_queue);
}

void exit_ipbecalled_task(void)
{
	
}

void* vdp_ipbecalled_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	IpBeCalled_Obj_Init();

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void IpBeCalled_Timer_Callback(void)
{
	BeCalled_Timer_Callback(&IpBeCalled_Obj);
	if(CallServer_Run.state==CallServer_Ring&&(CallServer_Run.call_type==IxCallScene11_Passive||CallServer_Run.call_type==IxCallScene1_Passive)&&IpBeCalled_Obj.BeCalled_Run.timer==6)
	{
		API_CallServer_CheckVideo();
		
	}
}
void IpBeCalled_Config_Data_Init(void)
{
	switch(IpBeCalled_Obj.BeCalled_Run.call_type)
	{
		//case IpBeCalled_MainCall:
		//case IpBeCalled_IntercomCall:
		//case IpBeCalled_InnerCall:
		//case IpBeCalled_LinphoneCall:	
		case IpBeCalled_IxSys:
			#if 0
			 if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = (CallServer_Run.caller_divert_state&0x7f)*2;
			 else
				IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			 #else
			  if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = (CallServer_Run.caller_divert_state&0x7f)*2+100;
			 else if(CallServer_Run.call_rule == CallRule_TransferIm)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 120;
			 else
				IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			 #endif
			IpBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			IpBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;

		case IpBeCalled_LinphoneCall:	
			IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			IpBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			IpBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;
		case IpBeCalled_NewIxSys:
			#if 0
			 if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = (CallServer_Run.caller_divert_state&0x7f)*2;
			 else
				IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			 #else
			  if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = CallServer_Run.caller_divert_state&0x7f;
			 else if(CallServer_Run.call_rule == CallRule_TransferIm)
			 	IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 120;
			 else
				IpBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			 #endif
			IpBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			IpBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;
	}
}
void IpBeCalled_MenuDisplay_ToRing(void)
{
	
}
void IpBeCalled_MenuDisplay_ToAck(void)
{
	
}
void IpBeCalled_MenuDisplay_ToBye(void)
{
	
}
void IpBeCalled_MenuDisplay_ToWait(void)
{
	
}

int API_IpBeCalled_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	BECALLED_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	
	#if 0
	if(msg_type == BECALLED_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	
	if(push_vdp_common_queue(task_ipbecalled.p_msg_buf, (char *)&send_msg, sizeof(BECALLED_STRUCT)) != 0)
	{
		return -1;
	}
	
	#if 0
	if(msg_type == BECALLED_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void IpBeCalled_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
