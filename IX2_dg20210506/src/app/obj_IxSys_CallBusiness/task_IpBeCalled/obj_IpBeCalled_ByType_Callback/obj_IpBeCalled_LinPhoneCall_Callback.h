#ifndef _obj_BeCalled_LinPhoneCall_Callback_H
#define _obj_BeCalled_LinPhoneCall_Callback_H

void Callback_BeCalled_ToRedial_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTransfer_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToRinging_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToAck_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToBye_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToWaiting_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToUnlock_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTimeout_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToError_LinPhoneCall(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToForceClose_LinPhoneCall(BECALLED_STRUCT *msg);
#endif