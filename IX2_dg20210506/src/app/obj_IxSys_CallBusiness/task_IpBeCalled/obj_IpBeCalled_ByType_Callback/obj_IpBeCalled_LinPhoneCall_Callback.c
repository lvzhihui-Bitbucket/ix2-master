/**
  ******************************************************************************
  * @file    obj_BeCalled_CdsCallSt_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2015.01.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  /*
#include "../../task_Phone.h"
#include "../task_BeCalled.h"
#include "obj_BeCalled_IPGVtkCall_Callback.h"
//#include "obj_Caller_IPGCallIPG_Callback.h"
*/
#include "task_CallServer.h" 
#include "obj_BeCalled_State.h"
#include "task_IpBeCalled.h"
#include "obj_IpBeCalled_LinPhoneCall_Callback.h"
#include "task_monitor.h"
#include "define_Command.h"
//#include "../../../task_Stack212/stack212.h"
//#include "../../../task_survey/obj_UnitSR.h"

//r_20150801
/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_BeCalled_ToRedial_LinPhoneCall(BECALLED_STRUCT *msg)
{
	;	//will_add
}

/*------------------------------------------------------------------------
						呼叫转译处理
------------------------------------------------------------------------*/
void Callback_BeCalled_ToTransfer_LinPhoneCall(BECALLED_STRUCT *msg)
{
	;
}

/*------------------------------------------------------------------------
							进入Ringing
1. 对小区主机: 发送TRYING
2. 对Analyze:  向Caller申请呼叫
------------------------------------------------------------------------*/



void Callback_BeCalled_ToRinging_LinPhoneCall(BECALLED_STRUCT *msg)	//R_
{
	#if 0
	uint8 SR_apply;
	SR_apply = API_SR_Request(CT14_INNER_CALL);
	if (SR_apply == SR_DISABLE)	//???,????
	{
		API_CallServer_InviteFail(CallServer_Run.call_type);
		API_linphonec_Close();
		return;
	}
	else if (SR_apply == SR_ENABLE_DEPRIVED)	//???,???
	{
		API_SR_Deprived();
	}
	else 
	{
		;	//???????
	}	
	SR_Routing_Create(CT14_INNER_CALL);
	#endif
}

/*------------------------------------------------------------------------
							进入ACK
1. 对小区主机: 发送OK_200(摘机)
------------------------------------------------------------------------*/
void Callback_BeCalled_ToAck_LinPhoneCall(BECALLED_STRUCT *msg)		//R_
{
	//IX2_TEST API_linphonec_Answer();
	//IX2_AD LoadAudioCaptureVol();
	//LoadAudioPlaybackVol();
	//IX2_AD LoadLinPhoneAudioPlaybackVol();
}

/*------------------------------------------------------------------------
				接收到消息(分机): 结束呼叫
1. 对小区主机: 发送呼叫结束指令
------------------------------------------------------------------------*/
void Callback_BeCalled_ToBye_LinPhoneCall(BECALLED_STRUCT *msg)		//R_
{
	//SR_Routing_Close(CT14_INNER_CALL);
	
	//IX2_AD API_talk_off();

	// lzh_20170605_s
	//IX2_TEST API_linphonec_Close();
	// lzh_20170605_e

	IpBeCalled_Obj.BeCalled_Run.timer = 0;
	OS_StopTimer(IpBeCalled_Obj.timer_becalled);
	
	IpBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;

}

/*------------------------------------------------------------------------
				接收到消息(): 结束呼叫					
------------------------------------------------------------------------*/
void Callback_BeCalled_ToWaiting_LinPhoneCall(BECALLED_STRUCT *msg)	//R_
{
	//SR_Routing_Close(CT14_INNER_CALL);
	
	//IX2_AD API_talk_off();

	//IX2_TEST API_linphonec_Close();
	
}

/*------------------------------------------------------------------------
						开锁处理
1. 对小区主机: 发送开锁指令
------------------------------------------------------------------------*/
void Callback_BeCalled_ToUnlock_LinPhoneCall(BECALLED_STRUCT *msg)	//R-
{
	
}

/*------------------------------------------------------------------------
					BeCalled呼叫超时(自动退出BeCalled)
------------------------------------------------------------------------*/
void Callback_BeCalled_ToTimeout_LinPhoneCall(BECALLED_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	int quit_flag =0;
	
	if(msg->msg_sub_type == BECALLED_TOUT_TIMEOVER)
	{	
		if(IpBeCalled_Obj.BeCalled_Run.state != BECALLED_WAITING)
		{
		
			//SR_Routing_Close(CT14_INNER_CALL);
			//Send_VtkUnicastCmd_StateNotice(DsAndGl,&BeCalled_Run.s_addr,&BeCalled_Run.t_addr,VTKU_CALL_STATE_BYE);
			//Send_ByeCmd_VtkUnicast(IpBeCalled_Obj.BeCalled_Run.s_addr);
			API_CallServer_Timeout(CallServer_Run.call_type);
			
			IpBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
		
			IpBeCalled_Obj.BeCalled_Run.timer = 0;
			
			OS_StopTimer(IpBeCalled_Obj.timer_becalled);

			//Set_IPCallLink_Status(CLink_Idle);
			//IX2_TEST API_linphonec_Close();
			//close_monitor_client();//czn_20161224	api_video_c_service_turn_off(); 

			//IX2_AD API_talk_off();

			//API_CallServer_IpBeCalledQuit();
		}
	}
	#if 0
	else if(msg->msg_head.msg_sub_type == BECALLED_TOUT_CHECKLINK)
	{
		if((BeCalled_Run.state == BECALLED_RINGING  || BeCalled_Run.state == BECALLED_ACK) && BeCalled_Run.timer > 3)
		{
			Global_Addr_Stru gaddr;
			gaddr.gatewayid 	= BeCalled_Run.s_addr.gatewayid;
			gaddr.ip 			= BeCalled_Run.s_addr.ip;
			gaddr.rt			= 0;
			gaddr.code		= 0;
			if(API_Get_Partner_Calllink_NewCmd(DsAndGl,&gaddr,&partner_call_link) == -1)
			{
				if(++BeCalled_Run.checklink_error	 >= 2)
				{
					quit_flag = 1;
					bprintf("--------quit0----------------------\n");
					goto BTOUT_CHECKLINK_END;
				}
			}
			else
			{
				BeCalled_Run.checklink_error = 0;
				
				if(partner_call_link.Status != CLink_AsCallServer && partner_call_link.Status != CLink_AsCaller)
				{
					bprintf("--------quit1 %d-------------\n",partner_call_link.Status);
					quit_flag = 1;
					goto BTOUT_CHECKLINK_END;
				}
				
				if(partner_call_link.Caller_Data.Call_Target.ip != GetLocalIp() || 
					partner_call_link.Caller_Data.Call_Target.rt != BeCalled_Run.t_addr.rt||
					partner_call_link.Caller_Data.Call_Target.code != BeCalled_Run.t_addr.code)
				{
					bprintf("--------quit2 ip=%08x,rt= %d code= %d-------------\n",partner_call_link.Caller_Data.Call_Target.ip,partner_call_link.Caller_Data.Call_Target.rt,partner_call_link.Caller_Data.Call_Target.code);
					quit_flag = 1;
				}
			}
	BTOUT_CHECKLINK_END:
			if(quit_flag == 1)
			{
				BeCalled_Run.state = BECALLED_WAITING;
		
				BeCalled_Run.timer = 0;
				
				OS_StopTimer(&timer_becalled);

				Set_IPCallLink_Status(CLink_Idle);

				close_monitor_client();//czn_20161224	api_video_c_service_turn_off(); 

				API_talk_off();

				API_CallServer_IpBeCalledQuit();
			}
		}
	}
	#endif
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_BeCalled_ToError_LinPhoneCall(BECALLED_STRUCT *msg)	
{
#if 0
	IpBeCalled_Business_Rps(msg,0);
	switch(BeCalled_ErrorCode)
	{
		case BECALLED_ERROR_INVITEFAIL:
			Send_VtkUnicastCmd_StateNotice(DsAndGl,&msg->s_addr,&msg->t_addr,VTKU_CALL_STATE_BYE);

			BeCalled_Run.state = BECALLED_WAITING;
	
			BeCalled_Run.timer = 0;
			OS_StopTimer(&timer_becalled);

			Set_IPCallLink_Status(CLink_Idle);

			close_monitor_client();//czn_20161224	api_video_c_service_turn_off(); 

			API_talk_off();
			break;
		case BECALLED_ERROR_UINTLINK_CLEAR:
		case BECALLED_ERROR_DTCALLER_QUIT:
			if(BeCalled_Run.state != BECALLED_WAITING)
			{
			
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&BeCalled_Run.s_addr,&BeCalled_Run.t_addr,VTKU_CALL_STATE_BYE);

				BeCalled_Run.state = BECALLED_WAITING;
			
				BeCalled_Run.timer = 0;
				
				OS_StopTimer(&timer_becalled);

				Set_IPCallLink_Status(CLink_Idle);

				close_monitor_client();//czn_20161224	api_video_c_service_turn_off(); 

				API_talk_off();

			}
			break;
	
	}
#endif
}

void Callback_BeCalled_ToForceClose_LinPhoneCall(BECALLED_STRUCT *msg)
{
	
	//if(BeCalled_Run.state != BECALLED_WAITING)
	{

		IpBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
	
		IpBeCalled_Obj.BeCalled_Run.timer = 0;

		OS_StopTimer(IpBeCalled_Obj.timer_becalled);

		//Set_IPCallLink_Status(CLink_Idle);
		//IX2_TEST API_linphonec_Close();

		
		//linphone_mon_lasttime_update(); 	//czn_20181119
		//close_monitor_client();//czn_20161224	api_video_c_service_turn_off(); 

		//API_talk_off();

	}
}
