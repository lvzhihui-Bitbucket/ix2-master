
#ifndef _LINPHONEC_BECALL_PROCESS_H
#define _LINPHONEC_BECALL_PROCESS_H

#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

void recv_linphone_invite(char* pbuf, int len);
void recv_linphone_dtmf(char input_dtmf);
void recv_linphone_close(void);
void dxdev_start_linphone(void);
void dxdev_close_linphone(void);
void dxdev_start_video(void);
void linphone_becalled_dtlink_create(void);
void linphone_becalled_timercallback(void);
#endif

