
#ifndef _LINPHONEC_INTERFACE_H
#define _LINPHONEC_INTERFACE_H

#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

typedef enum eXosip_event_type
{
	/* REGISTER related events */
	EXOSIP_REGISTRATION_NEW,           /**< announce new registration.       */
	EXOSIP_REGISTRATION_SUCCESS,       /**< user is successfully registred.  */
	EXOSIP_REGISTRATION_FAILURE,       /**< user is not registred.           */
	EXOSIP_REGISTRATION_REFRESHED,     /**< registration has been refreshed. */
	EXOSIP_REGISTRATION_TERMINATED,    /**< UA is not registred any more.    */

	/* INVITE related events within calls */
	EXOSIP_CALL_INVITE,            /**< announce a new call                    0x05 ist:step2*/
	EXOSIP_CALL_REINVITE,          /**< announce a new INVITE within call     */

	EXOSIP_CALL_NOANSWER,          /**< announce no answer within the timeout */
	EXOSIP_CALL_PROCEEDING,        /**< announce processing by a remote app   */
	EXOSIP_CALL_RINGING,           /**< announce ringback                     0x09 */
	EXOSIP_CALL_ANSWERED,          /**< announce start of call                0x0A */
	EXOSIP_CALL_REDIRECTED,        /**< announce a redirection                */
	EXOSIP_CALL_REQUESTFAILURE,    /**< announce a request failure            */
	EXOSIP_CALL_SERVERFAILURE,     /**< announce a server failure             */
	EXOSIP_CALL_GLOBALFAILURE,     /**< announce a global failure             */
	EXOSIP_CALL_ACK,               /**< ACK received for 200ok to INVITE      0x0F ist:step4*/

	EXOSIP_CALL_CANCELLED,         /**< announce that call has been cancelled 0x10 */
	EXOSIP_CALL_TIMEOUT,           /**< announce that call has failed         0x11 */

	/* request related events within calls (except INVITE) */
	EXOSIP_CALL_MESSAGE_NEW,              /**< announce new incoming request. */
	EXOSIP_CALL_MESSAGE_PROCEEDING,       /**< announce a 1xx for request. */
	EXOSIP_CALL_MESSAGE_ANSWERED,         /**< announce a 200ok  			  0x14 ist:step5*/
	EXOSIP_CALL_MESSAGE_REDIRECTED,       /**< announce a failure. */
	EXOSIP_CALL_MESSAGE_REQUESTFAILURE,   /**< announce a failure. */
	EXOSIP_CALL_MESSAGE_SERVERFAILURE,    /**< announce a failure. */
	EXOSIP_CALL_MESSAGE_GLOBALFAILURE,    /**< announce a failure. */

	EXOSIP_CALL_CLOSED,            /**< a BYE was received for this call      */

	/* for both UAS & UAC events */
	EXOSIP_CALL_RELEASED,             /**< call context is cleared.           0x1A ist:step6 */

	/* response received for request outside calls */
	EXOSIP_MESSAGE_NEW,              /**< announce new incoming request. 	0x1b - ist:step1*/
	EXOSIP_MESSAGE_PROCEEDING,       /**< announce a 1xx for request. */
	EXOSIP_MESSAGE_ANSWERED,         /**< announce a 200ok  				0x1d - ist:step3*/
	EXOSIP_MESSAGE_REDIRECTED,       /**< announce a failure. */
	EXOSIP_MESSAGE_REQUESTFAILURE,   /**< announce a failure. */
	EXOSIP_MESSAGE_SERVERFAILURE,    /**< announce a failure. */
	EXOSIP_MESSAGE_GLOBALFAILURE,    /**< announce a failure. */

	/* Presence and Instant Messaging */
	EXOSIP_SUBSCRIPTION_UPDATE,         /**< announce incoming SUBSCRIBE.      */
	EXOSIP_SUBSCRIPTION_CLOSED,         /**< announce end of subscription.     */

	EXOSIP_SUBSCRIPTION_NOANSWER,          /**< announce no answer              */
	EXOSIP_SUBSCRIPTION_PROCEEDING,        /**< announce a 1xx                  */
	EXOSIP_SUBSCRIPTION_ANSWERED,          /**< announce a 200ok                */
	EXOSIP_SUBSCRIPTION_REDIRECTED,        /**< announce a redirection          */
	EXOSIP_SUBSCRIPTION_REQUESTFAILURE,    /**< announce a request failure      */
	EXOSIP_SUBSCRIPTION_SERVERFAILURE,     /**< announce a server failure       */
	EXOSIP_SUBSCRIPTION_GLOBALFAILURE,     /**< announce a global failure       */
	EXOSIP_SUBSCRIPTION_NOTIFY,            /**< announce new NOTIFY request     */

	EXOSIP_SUBSCRIPTION_RELEASED,          /**< call context is cleared.        */

	EXOSIP_IN_SUBSCRIPTION_NEW,            /**< announce new incoming SUBSCRIBE.*/
	EXOSIP_IN_SUBSCRIPTION_RELEASED,       /**< announce end of subscription.   */

	EXOSIP_NOTIFICATION_NOANSWER,          /**< announce no answer              */
	EXOSIP_NOTIFICATION_PROCEEDING,        /**< announce a 1xx                  */
	EXOSIP_NOTIFICATION_ANSWERED,          /**< announce a 200ok                */
	EXOSIP_NOTIFICATION_REDIRECTED,        /**< announce a redirection          */
	EXOSIP_NOTIFICATION_REQUESTFAILURE,    /**< announce a request failure      */
	EXOSIP_NOTIFICATION_SERVERFAILURE,     /**< announce a server failure       */
	EXOSIP_NOTIFICATION_GLOBALFAILURE,     /**< announce a global failure       */

	EXOSIP_EVENT_COUNT                  	/**< MAX number of events              */
} eXosip_event_type_t;

// lzh_20181031_s
typedef enum _LinphoneRegistrationState{
	LinphoneRegistrationNone, /**<Initial state for registrations */
	LinphoneRegistrationProgress, /**<Registration is in progress */
	LinphoneRegistrationOk,	/**< Registration is successful */
	LinphoneRegistrationCleared, /**< Unregistration succeeded */
	LinphoneRegistrationFailed	/**<Registration failed */
}LinphoneRegistrationState;

typedef enum _LinphoneReason{
	LinphoneReasonNone,
	LinphoneReasonNoResponse, /**<No response received from remote*/
	LinphoneReasonBadCredentials, /**<Authentication failed due to bad or missing credentials*/
	LinphoneReasonDeclined, /**<The call has been declined*/
	LinphoneReasonNotFound, /**<Destination of the calls was not found.*/
	LinphoneReasonNotAnswered, /**<The call was not answered in time*/
	LinphoneReasonBusy /**<Phone line was busy */
}LinphoneReason;

// lzh_20181031_e

//lzh_20150423_s
#define COMBINING_IP_ADDR(IP1,IP2,IP3,IP4)	( ((int)IP1<<24)|((int)IP2<<16)|((int)IP3<<8)|IP4 )

//#define ACK 					0x00
#define S2C_INVITE_BY_IP		0x01
#define S2C_INVITE_BY_ACCOUNT	0x02
#define	S2C_ANSWER				0x03
#define S2C_TERMINATE			0x04
#define S2C_CANCEL				0x05
#define C2S_NOTIFY_CALL_STATE	0x10
#define C2S_DTMF_RECV			0x11
#define S2C_APPLY_SIP_ACCOUNT	0x20
#define C2S_REPLY_SIP_ACCOUNT	0x21
#define S2C_SET_SIP_ACCOUNT		0x22
#define	S2C_DELETE_SIP_ACCOUNT	0x23
#define S2C_STOP_SIP_ACCOUNT	0x24
#define S2C_START_SIP_ACCOUNT	0x25

// lzh_20180108_s
#define S2C_APPLY_HEARTBEAT		0x30
#define C2S_REPLY_HEARTBEAT		0x31
// lzh_20180108_e

#define S2C_APPLY_SET_VD_QUANT 0x40 

// lzh_20200707_s
#define S2C_APPLY_SET_VOUT_EN 	0x41
// lzh_20200707_e

// lzh_20201110_s
#define S2C_APPLY_SET_SIP_PORT 	0x42
// lzh_20201110_e

// lzh_20181101_s
#define S2C_APPLY_REGISTRTION_STATE		0x50
#define C2S_REPLY_REGISTRTION_STATE		0x51
// lzh_20181101_e

// lzh_20190624_s
#define S2C_APPLY_SNAPSHOT_ONE_JPEG		0x60
#define C2S_REPLY_SNAPSHOT_ONE_JPEG		0x61
// lzh_20190624_e

// lzh_20190723_s
#define S2C_REQ_PRESS_TO_TALK_STATE		0x70
#define C2S_RSP_PRESS_TO_TALK_STATE		0x71
// lzh_20190723_e

// lzh_20201201_s
#define S2C_REQ_UPDATE_MEDIA_STATE		0x80
#define C2S_RSP_UPDATE_MEDIA_STATE		0x81
// lzh_20201201_e

#define HEAD_FLAG				0x55AA
#define LINPHONEC_DAT_BUF_LEN	240

#pragma pack(1)
typedef struct vdp_linphonec_if_buffer_head_tag
{
	unsigned short	head;			//  	fixed 55aa
	unsigned char	type;			// 	
	unsigned char 	len;			// 数据包长度, 除开本身的所有字节
	int				ipAddr;			// ip地址，网络字节序，发送为本机地址；接收为目标方地址	
	unsigned char	buf[LINPHONEC_DAT_BUF_LEN];
} vdp_linphonec_if_buffer_head;

#pragma pack()

//lzh_20150423_e

int init_linphone_if_service( void );
int deinit_linphone_if_service(void);

int API_linphonec_Invite( char* regist_id );
int API_linphonec_Answer( void );
int API_linphonec_Close( void );
int API_linphonec_quit( void );

int API_linphonec_Register_SipCurAccount( char* proxy, char* identify, char* password );
int API_linphonec_Apply_SipCurAccount( void );
int API_linphonec_Delete_SipCurAccount( void );
int API_linphonec_Stop_SipCurAccount( void );
int API_linphonec_Start_SipCurAccount( void );
int API_linphonec_Set_VdQuant(int newLevel);
void Set_SipAccount_State(int new_state,int err_code);
int Get_SipAccount_State(void);
int Get_SipReg_ErrCode(void);

// lzh_20181101_s
int API_linphonec_apply_registration_state( void );
// lzh_20181101_e

#endif

