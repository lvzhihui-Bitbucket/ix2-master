#if 1	//IX2_TEST
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "linphone_interface.h"
#include "linphone_becall_process.h"
#include "task_CallServer.h"

#include "unix_socket.h"
#include "sys_msg_process.h"
//#include "../../task_monitor/task_LocalMonitor/task_LocalMonitor.h"
#include "define_Command.h"
#include "task_Power.h"
#include "task_IoServer.h"
#include "MenuSipConfig_Business.h"	//20210428
#include "elog.h"

#define MAX_LINPHONECALL_PW	10

typedef enum 
{
	LinPhone_BeCalled_Idle = 0,
	LinPhone_BeCalled_WaitInput	,
	LinPhone_BeCalled_Monitoring,
	LinPhone_BeCalled_Calling,
}LinPhone_BeCalled_State;

typedef struct
{
	int 			state;		
	time_t 		update_time;
	int 			input_length;
	char			input_buff[MAX_LINPHONECALL_PW];
}LinPhone_BeCalled_Run_Stru;

LinPhone_BeCalled_Run_Stru LinPhone_BeCalled_Run = 
{
	.state = 0,
	.update_time = 0
};

pthread_mutex_t linphone_becall_Lock = PTHREAD_MUTEX_INITIALIZER;
static int delay_answer_flag=0;
void delay_answer(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring&&delay_answer_flag==0)
	{
		if(time(NULL)-LinPhone_BeCalled_Run.update_time>=3)
		{
			delay_answer_flag=1;
			API_linphonec_Answer();
			
		}
			
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
}
void delay_im_answer(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	delay_answer_flag=1;
	API_linphonec_Answer();
	pthread_mutex_unlock(&linphone_becall_Lock);
}
void recv_linphone_invite(char* pbuf, int len)
{
#if 1
	uint8 SR_apply;
	extern int divertresolution_type;
	uint8 tranferset;
	
	dprintf("linphone_invite state=%d\n", LinPhone_BeCalled_Run.state); 
	
	pthread_mutex_lock(&linphone_becall_Lock);
	#if defined(PID_DX470)||defined(PID_DX482)
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Idle&&IfCallServerBusy()==0&&!DtSrInUse())
	#else
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Idle&&IfCallServerBusy()==0&&!IXBusiness_IsBusy())
	#endif
	{
		/*
		if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
		{
			char temp[20];
			
			API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
			tranferset = atoi(temp);
		}
		if(API_Business_Request(Business_State_SipWaitInput) == 0||tranferset== 1|| tranferset == 2)		//czn_20190107
		{
			API_Business_Close(Business_State_SipWaitInput);
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
		else
		*/
		{
			API_POWER_TALK_OFF();
			
			
			//LinPhone_BeCalled_Run.state = LinPhone_BeCalled_WaitInput;
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Monitoring;
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = time(NULL);
			//SipCfg_T* sip_cfg;
			//sip_cfg = GetSipConfig();
			//API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
			//usleep(100*1000);
			//LoadAudioCaptureVol();
			//LoadLinPhoneAudioPlaybackVol();
			SetAudioPlaybackMute();
			//usleep(1000000);
			if(strcmp(GetCustomerizedName(),"GATES")!=0)
				API_linphonec_Answer();
			else
				delay_answer_flag=0;
			//usleep(1000000);
			//API_linphonec_Set_VdQuant(divertresolution_type);
			//LoadAudioCaptureVol();
			//LoadAudioPlaybackVol();
			//LoadLinPhoneAudioPlaybackVol();
			SetAudioCaptureMute();
			
			SipCfg_T* sip_cfg;
			sip_cfg = GetSipConfig();
			//API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert,sip_cfg->divPwd);
			//usleep(10000*1000);
			API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);

			//LinphoneDxMonitorDsStart("0x34");
			//API_Event_DXMonitor_On(0x34,0);
			#if 0
			if(strcmp(GetCustomerizedName(),"GATES")==0)
				LinphoneDxMonitorDsStart("0x34");
			#endif

			log_i("have been invite by phone");
		}

	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		if(LinPhone_BeCalled_Run.input_length > 0 || abs(LinPhone_BeCalled_Run.update_time - time(NULL)) < 4)
		{
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_WaitInput;
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = time(NULL);
			
			//usleep(2000000);
			//API_linphonec_Answer();
			usleep(1000000);
			//LoadAudioCaptureVol();
			//LoadAudioPlaybackVol();
			//LoadLinPhoneAudioPlaybackVol();
		}
		else
		{
			API_Business_Close(Business_State_SipWaitInput);
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
	}
	else if(LinPhone_BeCalled_Run.state != LinPhone_BeCalled_Idle)
	{
		
		if(LinPhone_BeCalled_Run.input_length > 0 || abs(LinPhone_BeCalled_Run.update_time - time(NULL)) < 4)
		{
			//LinPhone_BeCalled_Run.state = LinPhone_BeCalled_WaitInput;
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = time(NULL);
			
			//usleep(2000000);
			//API_linphonec_Answer();
			usleep(1000000);
			//LoadAudioCaptureVol();
			//LoadAudioPlaybackVol();
			//LoadLinPhoneAudioPlaybackVol();
		}
		else
		{
			//API_Business_Close(Business_State_SipWaitInput);
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
			API_VtkMediaTrans_StopJpgPush();
		}
	}
	else
	{
		API_Business_Close(Business_State_SipWaitInput);
		API_linphonec_Close();
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;

	}
	#if 0
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		if(LinPhone_BeCalled_Run.input_length > 0 || abs(LinPhone_BeCalled_Run.update_time - time(NULL)) > 3)
		{
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_WaitInput;
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = time(NULL);
			
			//usleep(2000000);
			//API_linphonec_Answer();
			usleep(1000000);
		}
		else
		{
			//API_linphonec_Close();
			//LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
		LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
		LoadLinPhoneAudioPlaybackVol();
		API_linphonec_Answer();
		usleep(10000);
		API_linphonec_Set_VdQuant(divertresolution_type);
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		if(Mon_Run.state == MON_STATE_PHONE_ON)
		{
			API_LocalMonitor_Off();

			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_WaitInput;
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = time(NULL);
			//LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
			//LoadLinPhoneAudioPlaybackVol();
			usleep(2000000);
			API_linphonec_Answer();
			usleep(10000);
			API_linphonec_Set_VdQuant(divertresolution_type);
		}
		else
		{
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		
	}

	#endif
	pthread_mutex_unlock(&linphone_becall_Lock);
#endif
}

int LinPhone_BeCalled_Input_Verify(void)		//return 0= fail,1=monitor,2=call
{
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		char monitor_password[MAX_LINPHONECALL_PW] = {"1000"};
		char call_password[MAX_LINPHONECALL_PW] = {"2000"};
		int str_len;
		//will add load password from file
		// ...
		Get_SipConfig_CallCode(call_password);
		Get_SipConfig_MonCode(monitor_password);
		printf("!!!!!LinPhone_BeCalled_Input_Verify %d\n",LinPhone_BeCalled_Run.input_length);

		str_len = strlen(monitor_password);
		
		if(LinPhone_BeCalled_Run.input_length == str_len) 
		{
			if(memcmp(LinPhone_BeCalled_Run.input_buff + LinPhone_BeCalled_Run.input_length -str_len,monitor_password,str_len) == 0)
				return 1;
		}

		str_len = strlen(call_password);
		
		if(LinPhone_BeCalled_Run.input_length == str_len) 
		{
			printf("!!!!!LinPhone_BeCalled_Input_Verify call_password\n");
			if(memcmp(LinPhone_BeCalled_Run.input_buff + LinPhone_BeCalled_Run.input_length -str_len,call_password,str_len) == 0)
				return 2;
		}
	}

	return 0;
}

//cao_20200826 
int LinPhone_BeCalled_tcpcode_Verify(char code_type,char *code_buff,char code_len)		//code_type,0,moncode,1 callcode,2dtmfcode,//return 0= fail,1=monitor,2=call
{
	if(code_type == 0)
	{
		if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput||LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
		{
			char monitor_password[MAX_LINPHONECALL_PW] = {"1000"};
			int str_len;
			//will add load password from file
			// ...
			Get_SipConfig_MonCode(monitor_password);

			str_len = strlen(monitor_password);
			
			if(code_len == str_len) 
			{
				if(memcmp(code_buff,monitor_password,str_len) == 0)
					return 1;
			}
			//cao_20200826 close_linphonemon_monlink();
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
	}

	return 0;
}

void recv_linphone_dtmf(char input_dtmf)
{
	time_t cur_time;
	//printf("!!!!!!!!!!!recv_linphone_dtmf %c\n",input_dtmf);
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Idle && CallServer_Run.state == CallServer_Wait)
	 	return;
	
	cur_time = time(NULL);
	
	if(input_dtmf == '#')
	{
		if(LinPhone_BeCalled_Run.input_length == 0)
		{
			LinPhone_BeCalled_Run.update_time = cur_time;
			return;
		}
		
		if(abs(LinPhone_BeCalled_Run.update_time -cur_time) > 5)
		{
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = cur_time;
			return;
		}
		
		if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
		{
			pthread_mutex_lock(&linphone_becall_Lock);
			
			int verify_result = LinPhone_BeCalled_Input_Verify();
			
			if(verify_result == 1)	//start_monitor
			{
				
			}
			else if(verify_result == 2)	//start_call
			{	
				//czn_20190107_s
				API_Business_Close(Business_State_SipWaitInput);
				if(API_Business_Request(Business_State_BeSipCall) == 0)
				{
					API_linphonec_Close();
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
					return;
				}
				//czn_20190107_e
				API_POWER_TALK_OFF();
				if(API_CallServer_Invite(IxCallScene4,0,NULL) == 0)
				{
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Calling;
					usleep(700000);
					//LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
					//LoadLinPhoneAudioPlaybackVol();
				}
				else
				{
					API_linphonec_Close();
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
				}
			}
			pthread_mutex_unlock(&linphone_becall_Lock);
		}
	}
	else
	{
		int i;
		
		if(abs(LinPhone_BeCalled_Run.update_time -cur_time) > 5)
		{
			LinPhone_BeCalled_Run.input_length = 0;
		}
		
		LinPhone_BeCalled_Run.update_time = cur_time;
		
		if(LinPhone_BeCalled_Run.input_length >= MAX_LINPHONECALL_PW)
		{
			LinPhone_BeCalled_Run.input_length = MAX_LINPHONECALL_PW - 1;
			for(i = 0;i <  MAX_LINPHONECALL_PW - 1;i ++)
			{
				LinPhone_BeCalled_Run.input_buff[i] = LinPhone_BeCalled_Run.input_buff[i+1];
			}
		}

		LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length ++] = input_dtmf;
	}
#if 0
	time_t cur_time;
	
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Idle && CallServer_Run.state == CallServer_Wait)
	 	return;
	
	cur_time = time(NULL);
	
	if(input_dtmf == '#')
	{
		if(LinPhone_BeCalled_Run.input_length == 0)
		{
			LinPhone_BeCalled_Run.update_time = cur_time;
			return;
		}
		
		if(abs(LinPhone_BeCalled_Run.update_time -cur_time) > 5)
		{
			LinPhone_BeCalled_Run.input_length = 0;
			LinPhone_BeCalled_Run.update_time = cur_time;
			return;
		}
		#if 0
		if(CallServer_Run.state != CallServer_Wait)
		{
			if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '1')
			{
				API_POWER_TALK_OFF();
				API_CallServer_RemoteUnlock1(CallServer_Run.call_type,NULL);
				usleep(700000);
				API_POWER_UDP_TALK_ON();
				
			}
			else if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '2')
			{
				API_POWER_TALK_OFF();
				API_CallServer_RemoteUnlock2(CallServer_Run.call_type,NULL);
				usleep(700000);
				API_POWER_UDP_TALK_ON();
			}
			else if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '3')	//czn_20181114
			{
				//API_POWER_TALK_OFF();
				//API_CallServer_DivertAutoMuteTalk(CallServer_Run.call_type);
				//usleep(700000);
				//API_POWER_UDP_TALK_ON();
			}
			else if(LinPhone_BeCalled_Run.input_length >= 2 && memcmp(&LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -2],"66",2) == 0)
			{
				//API_POWER_TALK_OFF();
				API_CallServer_Redail(CallServer_Run.call_type);
				//usleep(700000);
				//API_POWER_UDP_TALK_ON();
			}
		}
		#endif
		if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
		{
			pthread_mutex_lock(&linphone_becall_Lock);
			
			int verify_result = LinPhone_BeCalled_Input_Verify();
			
			if(verify_result == 1)	//start_monitor
			{
				#if 0
				int result;
				API_POWER_TALK_OFF();
				result = API_Event_Monitor_Phone_On(DS1_ADDRESS);

				if(result == 0 || result == 0xff)
				{
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Monitoring;
					usleep(1500000);
					LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
					LoadLinPhoneAudioPlaybackVol();
				}
				else
				{
					API_linphonec_Close();
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
				}
				#endif
			}
			else if(verify_result == 2)	//start_call
			{
			#if 1
				
				API_POWER_TALK_OFF();
				if(API_CallServer_Invite(IxCallScene4,0,NULL) == 0)
				{
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Calling;
					usleep(700000);
					//LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
					//LoadLinPhoneAudioPlaybackVol();
				}
				else
				{
					API_linphonec_Close();
					LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
				}
			#endif
			}
			pthread_mutex_unlock(&linphone_becall_Lock);
		}
		#if 0
		else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
		{
			if(LinPhone_BeCalled_Run.input_length == 1)
			{
				if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '1')
				{
					API_POWER_TALK_OFF();
					API_LocalMonitor_Unlock1();
					usleep(700000);
					API_POWER_UDP_TALK_ON();
				}
				else if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '2')
				{
					API_POWER_TALK_OFF();
					API_LocalMonitor_Unlock2();
					usleep(700000);
					API_POWER_UDP_TALK_ON();
				}
				else if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '9')
				{
					API_LocalMonitor_Talk_On();
				}
				else if(LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length -1] == '0')
				{
					unsigned short addr_temp = Mon_Run.partner_IA;
					addr_temp++;
					if(addr_temp <DS1_ADDRESS || addr_temp>DS4_ADDRESS)
					{
						addr_temp = DS1_ADDRESS;
					}
					
					API_LocalMonitor_Next(addr_temp);
				}
			}
			else
			{
				if(LinPhone_BeCalled_Input_Verify() == 1)
				{
					API_POWER_VIDEO_ON();
				}
			}
		}
		else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
		{
			
		}

		LinPhone_BeCalled_Run.input_length = 0;
		LinPhone_BeCalled_Run.update_time = cur_time;
	}
	else
	{
		int i;
		
		if(abs(LinPhone_BeCalled_Run.update_time -cur_time) > 5)
		{
			LinPhone_BeCalled_Run.input_length = 0;
		}
		
		LinPhone_BeCalled_Run.update_time = cur_time;
		
		if(LinPhone_BeCalled_Run.input_length >= MAX_LINPHONECALL_PW)
		{
			LinPhone_BeCalled_Run.input_length = MAX_LINPHONECALL_PW - 1;
			for(i = 0;i <  MAX_LINPHONECALL_PW - 1;i ++)
			{
				LinPhone_BeCalled_Run.input_buff[i] = LinPhone_BeCalled_Run.input_buff[i+1];
			}
		}

		LinPhone_BeCalled_Run.input_buff[LinPhone_BeCalled_Run.input_length ++] = input_dtmf;
	}
	#endif
#endif
}

void recv_linphone_close(void)
{
#if 1
	pthread_mutex_lock(&linphone_becall_Lock);
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		API_Business_Close(Business_State_SipWaitInput);	//czn_20190107
		if(abs(LinPhone_BeCalled_Run.update_time -time(NULL)) > 3)
		{
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;

		API_talk_off();
		//SetAudioPlaybackMute();

		// lzh_20200707_s
		API_VtkMediaTrans_StopJpgPush();
		// lzh_20200707_e
		LinphoneIPCMonitorStop();
		LinphoneMonitorDsStop();
		LinphoneDxMonitorDsStop();
		log_i("close becall with phone");
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		Global_Addr_Stru addr;
		if(CallServer_Run.state != CallServer_Wait)
		{
			
			API_CallServer_RemoteBye(CallServer_Run.call_type,NULL);
		}
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
#endif
}
void linphone_becall_cancel(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	if(LinPhone_BeCalled_Run.state != LinPhone_BeCalled_Idle)
	{
		API_linphonec_Close();
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		API_VtkMediaTrans_StopJpgPush();
		LinphoneIPCMonitorStop();
		LinphoneMonitorDsStop();
		LinphoneDxMonitorDsStop();
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
}
void dxdev_start_linphone(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		API_linphonec_Close();
		usleep(3000000);
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
}

void dxdev_close_linphone(void)
{
	//pthread_mutex_lock(&linphone_becall_Lock);
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	//pthread_mutex_unlock(&linphone_becall_Lock);
}

void dxdev_start_video(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		API_linphonec_Close();
		usleep(3000000);
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
}

void linphone_becalled_dtlink_create(void)
{
	pthread_mutex_lock(&linphone_becall_Lock);
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
		API_linphonec_Close();
		usleep(3000000);
		LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		//LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		//LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
	}
	pthread_mutex_unlock(&linphone_becall_Lock);
}

void linphone_becalled_timercallback(void)
{
	//pthread_mutex_lock(&linphone_becall_Lock);
	time_t cur_time;
	
	
	
	 if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	 {
	 	cur_time = time(NULL);
		if((cur_time - LinPhone_BeCalled_Run.update_time) > 30)
		{
			API_linphonec_Close();
			LinPhone_BeCalled_Run.state = LinPhone_BeCalled_Idle;
		}
	 }
	 //pthread_mutex_unlock(&linphone_becall_Lock);
}

void Linphone_Send_OkAck(void)
{
	
}

void Linphone_Send_ErrorAck(void)
{
	
}
int recv_tcp_dtmf_process(char* code_buff)
{
	int ret = 0;
	int code_len = strlen(code_buff) - 1;
	
	if(CallServer_Run.state != CallServer_Wait)
	{
		if(!strcmp(code_buff, "1#"))
		{
			//API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
			API_CallServer_RemoteUnlock1(CallServer_Run.call_type,NULL);
			
		}
		else if(!strcmp(code_buff, "2#"))
		{
			//API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
			API_CallServer_RemoteUnlock2(CallServer_Run.call_type,NULL);
		}
		else if(!strcmp(code_buff, "3#")) //czn_20181114
		{
			//API_POWER_TALK_OFF();
			//API_CallServer_DivertAutoMuteTalk(CallServer_Run.call_type);
			//usleep(700000);
			//API_POWER_UDP_TALK_ON();
		}
		else if(!strcmp(code_buff, "9#")) //czn_20181114
		{
			//LoadAudioCaptureVol();
			//LoadLinPhoneAudioPlaybackVol();
			
		}
		else if(code_len >= 2 && !strcmp(&code_buff[code_len-2], "66#"))
		{
			//API_POWER_TALK_OFF();
			//API_CallServer_Redail(CallServer_Run.call_type);
			//usleep(700000);
			//API_POWER_UDP_TALK_ON();
		}
	}
	
		
	if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_WaitInput)
	{
	
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Monitoring)
	{
		
		if(!strcmp(code_buff, "9#"))
		{
			//LoadAudioCaptureVol();
			//LoadLinPhoneAudioPlaybackVol();
			#if defined(PID_DX470)||defined(PID_DX482)
			LinphoneDxMonitorDsTalkStart();
			#else
			DsAppTalkDeal(1);
			#endif
		}
		#if 0
		else if(!strcmp(code_buff, "1#"))
		{
			API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
		}
		else if(!strcmp(code_buff, "2#"))
		{
			API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
		}
		#endif
	}
	else if(LinPhone_BeCalled_Run.state == LinPhone_BeCalled_Calling)
	{
		
	}
	
	return ret;
}
#endif
