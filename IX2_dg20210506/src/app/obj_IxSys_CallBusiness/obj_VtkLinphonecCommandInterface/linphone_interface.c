
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#if 1	//IX2_TEST
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "linphone_interface.h"
#include "linphone_becall_process.h"
#include "task_CallServer.h"

#include "unix_socket.h"
//#include "../../task_monitor/task_LocalMonitor/task_LocalMonitor.h"
#include "define_Command.h"
#include "task_VideoMenu.h"
#include "obj_ThreadHeartBeatService.h"
#include "task_IoServer.h"
#include "task_debug_sbu.h"
#include "cJSON.h"
int API_linphonec_update_vdout_status(void);


unix_socket_t	linphonec_server_socket;

int SipReg_State = 0;
int SipReg_ErrCode = 0;
// lzh_20200707_s
int linphone_vout_enable = 0;
// lzh_20200707_e


int get_SipCurAccount( char* pdat, char* proxy, char* identify, char* password );


void linphonec_server_socket_recv_data(char* pbuf, int len);

int init_linphone_if_service( void )
{
	init_unix_socket(&linphonec_server_socket,1,LOCAL_PORT_FLAG,linphonec_server_socket_recv_data);		// 服务器端
	create_unix_socket_create(&linphonec_server_socket);
	usleep(100*1000);
	printf(" init_linphone_if_service ok \n");
	API_linphonec_update_vdout_status();
	return 0;
}

int deinit_linphone_if_service(void)
{
	deinit_unix_socket(&linphonec_server_socket);
	usleep(10*1000);
	printf(" deinit_linphone_if_service ok \n");
	return 0;
}

void linphonec_server_socket_recv_data(char* pbuf, int len)
{
#if 1
	vdp_linphonec_if_buffer_head* ptrLinphonecif_buffer = (vdp_linphonec_if_buffer_head*)pbuf;
	Global_Addr_Stru addr;
	static int Set_VdQuant_flag = 0;

	// lzh_20181101_s
	LinphoneReason				reg_error;
	LinphoneRegistrationState	reg_state;	
	// lzh_20181101_e

	char detail[LOG_DESC_LEN+1];
	
	dprintf("linphone recv type=%d,  buf[0]=%d\n", ptrLinphonecif_buffer->type, ptrLinphonecif_buffer->buf[0]); 
	
	if( ptrLinphonecif_buffer->head != HEAD_FLAG )
		return;
	
	// 得到呼叫启动事件
	if( ptrLinphonecif_buffer->type == C2S_NOTIFY_CALL_STATE )
	{
		
		
		printf("!!!!!!!!!!LINPHONEC notify cmd=[%d],len = %d!!!!!!!!!!!!!!!!!!!!\n", ptrLinphonecif_buffer->buf[0],len);
		//extern int divert_log_index;
		//snprintf(detail,60,"T%04d^Recv SIP cmd[%d]",divert_log_index,ptrLinphonecif_buffer->buf[0]);	
		//API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
		// 解析呼叫指令
		if( ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_INVITE )
		{
			// 得到呼叫启动事件
			printf(" .............................LINPHONEC Start Invite................... \n");
			//API_Event_Monitor_Phone_On(DS1_ADDRESS);
			recv_linphone_invite(pbuf,len);	//czn_20170922
		}
		//else if( (ptrLinphonecif_buffer->cmd == EXOSIP_CALL_RELEASED) || (ptrLinphonecif_buffer->cmd == EXOSIP_CALL_CLOSED) )//|| (ptrLinphonecif_buffer->cmd == EXOSIP_CALL_MESSAGE_NEW) )
		//else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_CLOSED) ||(ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_GLOBALFAILURE))
		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_CLOSED)||(ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_GLOBALFAILURE) )
		{
			if(CallServer_Run.state != CallServer_Wait)
			{
				addr.ip = 0;
				addr.gatewayid = 0;
				addr.rt = LIPHONE_RT;
				addr.code = 0;
				API_CallServer_RemoteBye(CallServer_Run.call_type,NULL);
			}
			#if 0
			else if(Mon_Run.state == MON_STATE_PHONE_ON && Mon_Run.timer >= 3)
			{
				API_LocalMonitor_Off();
			}
			#endif
			recv_linphone_close();		//czn_20170922
			
			// 得到呼叫结束事件
			printf(" .............................LINPHONEC Stop Invite................... \n");
		}

		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_RINGING) )
		{
			// lzh_20210616_s
			//if( CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Transfer  )
			if( CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Ring ||CallServer_Run.state == CallServer_Transfer  )
			// lzh_20210616_e
			{
				API_CallServer_InviteOk(CallServer_Run.call_type);
				
				#if defined(PID_DX470)||defined(PID_DX482)
				//API_OpenDxRtpVideo();
				#endif
			}
			printf(" .............................LINPHONEC RingRing................... \n");
		}

		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_ANSWERED) )
		{
		
			if(CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Ring || CallServer_Run.state == CallServer_Transfer)
			{
				addr.ip = 0;
				addr.gatewayid = 0;
				addr.rt = LIPHONE_RT;
				addr.code = 0;
				#if defined(PID_DX470)||defined(PID_DX482)
				API_CallServer_AppAck(CallServer_Run.call_type,NULL);
				#else
				API_CallServer_RemoteAck(CallServer_Run.call_type,NULL);
				#endif
			}
			if(CallServer_Run.state == CallServer_Ack)
			{
				//Set_Divert_Video_Flag();
			}
			printf(" .............................LINPHONEC ANSWERED................... \n");
		}
		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_CANCELLED)   )
		{
			if(CallServer_Run.state != CallServer_Wait)
			{
				addr.ip = 0;
				addr.gatewayid = 0;
				addr.rt = LIPHONE_RT;
				addr.code = 0;
				API_CallServer_RemoteBye(CallServer_Run.call_type,NULL);
			}
			printf(" .............................LINPHONEC CANCELLED................... \n");
		}
		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_REGISTRATION_SUCCESS) )
		{
			Set_SipAccount_State(1,0);
			// lzh_20181031_s  for manual register
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_GET_REGISTER_STATE_OK);					
			dprintf("###############sip reg successful!!!!!!\n");
			// lzh_20181031_e
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_SIP_ONLINE);
		}
		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_REGISTRATION_FAILURE) )
		{
			int err_code;
			err_code = (ptrLinphonecif_buffer->buf[1]<<8) | ptrLinphonecif_buffer->buf[2];
			Set_SipAccount_State(0,err_code);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_SIP_OFFLINE);
			dprintf("###############sip reg error ,error code =%d\n",err_code);
			// lzh_20181031_s  for manual register
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_GET_REGISTER_STATE_FAIL);					
			// lzh_20181031_e
		}
		else if( (ptrLinphonecif_buffer->buf[0] == EXOSIP_CALL_PROCEEDING) )
		{
			//recv_one_sip_rsp();
		}
	}	
	else if( ptrLinphonecif_buffer->type == C2S_REPLY_SIP_ACCOUNT )
	{
		char proxy_str[50];
		char identify[50];
		char password[50];
		int ret;
		ret = get_SipCurAccount( ptrLinphonecif_buffer->buf, proxy_str, identify, password );
		if( ret == 0 )
		{
			printf("application: sip account ok: proxy=%s,identify=%s,password=%s........\n",proxy_str,identify,password);			
		}
		else
		{
			printf("application: sip account err!\n");
		}
	}	
	else if(ptrLinphonecif_buffer->type == C2S_DTMF_RECV)
	{
		recv_linphone_dtmf(ptrLinphonecif_buffer->buf[0]);	//czn_20170922
		#if 0
		static char dtmf_save = 0;
		static time_t dtmf_save_time = 0;

		if(ptrLinphonecif_buffer->buf[0] == '#')
		{
			if(dtmf_save_time - time(NULL) <= 5)
			{
				if(CallServer_Run.state != CallServer_Wait)
				{
					addr.ip = 0;
					addr.gatewayid = 0;
					addr.rt = LIPHONE_RT;
					addr.code = 0;
					if(dtmf_save == '1')
					{
						API_CallServer_RemoteUnlock1(CallServer_Run.call_type,&addr);
					}
					else if(dtmf_save == '2')
					{
						API_CallServer_RemoteUnlock2(CallServer_Run.call_type,&addr);
					}
				}
				else if(Mon_Run.state == MON_STATE_PHONE_ON)
				{
					if(dtmf_save == '1')
					{
						API_LocalMonitor_Unlock1();
					}
					else if(dtmf_save == '2')
					{
						API_LocalMonitor_Unlock2();
					}
					else if(dtmf_save == '9')
					{
						API_LocalMonitor_Talk_On();
					}
					else if(dtmf_save == '0')
					{
						unsigned short addr_temp = Mon_Run.partner_IA;
						addr_temp++;
						if(addr_temp <DS1_ADDRESS || addr_temp>DS4_ADDRESS)
						{
							addr_temp = DS1_ADDRESS;
						}
						
						API_LocalMonitor_Next(addr_temp);
					}
				}
			}
			dtmf_save = 0;
		
		
		}
		else
		{
			dtmf_save = ptrLinphonecif_buffer->buf[0];
			dtmf_save_time = time(NULL);
		}
		#endif
		
	}
	else if(ptrLinphonecif_buffer->type == C2S_REPLY_HEARTBEAT)	//czn_20180109
	{
		RecvThreadHeartbeatReply(Linphone_Heartbeat_Mask);
		if(Set_VdQuant_flag == 0)
		{
			unsigned char value;
			Set_VdQuant_flag = 1;
			char temp[20];
			
			API_Event_IoServer_InnerRead_All(DivertVedioResolutionSet, temp);
			value = atoi(temp);
			API_linphonec_Set_VdQuant(value);
		}
		linphone_becalled_timercallback();
		// lzh_20181031_s
		//printf("==C2S_REPLY_HEARTBEAT: regflag=%d,state=%d,error=%d=====\n",ptrLinphonecif_buffer->buf[0],ptrLinphonecif_buffer->buf[1],ptrLinphonecif_buffer->buf[2]);
		if( ptrLinphonecif_buffer->buf[0] == 'A' )
		{
			reg_state = ptrLinphonecif_buffer->buf[1];
			reg_error = ptrLinphonecif_buffer->buf[2];

			if( reg_state == LinphoneRegistrationOk )
			{
				Set_SipAccount_State(1,0);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_SIP_ONLINE);
			}
			else if( reg_state == LinphoneRegistrationFailed )
			{
				Set_SipAccount_State(0,reg_error);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_SIP_OFFLINE);
			}
		}
		else if( ptrLinphonecif_buffer->buf[0] == 'B' )
		{
		}
		// lzh_20181031_e
	}
	// lzh_20181101_s
	else if(ptrLinphonecif_buffer->type == C2S_REPLY_REGISTRTION_STATE)
	{
		//printf("==C2S_REPLY_REGISTRTION_STATE: regflag=%d,state=%d,error=%d=====\n",ptrLinphonecif_buffer->buf[0],ptrLinphonecif_buffer->buf[1],ptrLinphonecif_buffer->buf[2]);
		if( ptrLinphonecif_buffer->buf[0] == 'A' )
		{
			reg_state = ptrLinphonecif_buffer->buf[1];
			reg_error = ptrLinphonecif_buffer->buf[2];

			if( reg_state == LinphoneRegistrationOk )
			{
				Set_SipAccount_State(1,0);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_GET_REGISTER_STATE_OK); 				
			}
			else if( reg_state == LinphoneRegistrationFailed )
			{
				Set_SipAccount_State(0,reg_error);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_GET_REGISTER_STATE_FAIL);				
			}	
		}
	}
	// lzh_20181101_e	
#endif
}

int get_SipCurAccount( char* pdat, char* proxy, char* identify, char* password )
{
	int	str_len;

	str_len = strlen(pdat);
	if( str_len == 0 ) return 1;	
	memcpy( proxy, pdat, str_len );
	proxy[str_len] = 0;
	pdat += str_len;
	pdat += 1;

	str_len = strlen(pdat);
	if( str_len == 0 ) return 2;	
	memcpy( identify, pdat, str_len );
	identify[str_len] = 0;
	pdat += str_len;
	pdat += 1;
	
	str_len = strlen(pdat);
	if( str_len == 0 ) return 3;	
	memcpy( password, pdat, str_len );
	password[str_len] = 0;
	pdat += str_len;
	pdat += 1;

	return 0;
}

void send_one_package(unsigned char type, int target_ip, char* pbuf,int len )
{
	vdp_linphonec_if_buffer_head linphonec_buf;

	if( len > LINPHONEC_DAT_BUF_LEN ) len = LINPHONEC_DAT_BUF_LEN;

	linphonec_buf.head		= HEAD_FLAG;
	linphonec_buf.type		= type;
	linphonec_buf.len 		= len;
	linphonec_buf.ipAddr	= target_ip;

	if( pbuf != NULL ) memcpy( linphonec_buf.buf, pbuf,len);
	
	unix_socket_send_data(&linphonec_server_socket, (char*)&linphonec_buf, linphonec_buf.len+sizeof(vdp_linphonec_if_buffer_head)-LINPHONEC_DAT_BUF_LEN);
	
	printf("application: Send one UnixSocket type=%d,target_ip=0x%08x,len=%d\n",linphonec_buf.type,linphonec_buf.ipAddr,linphonec_buf.len);
}


int API_linphonec_Invite( char* regist_id )
{
	char strLinphonecCmd[25];		//"call sip:xxx.xxx.xxx.xxx"
	
	dxdev_start_linphone();			//czn_20170922
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );

	// 得到呼叫启动命令
	sprintf(strLinphonecCmd, "call %s", regist_id);

	if( strstr( regist_id, "sip:" ) == NULL )
		send_one_package( S2C_INVITE_BY_ACCOUNT, 0, strLinphonecCmd, sizeof(strLinphonecCmd) );		
	else
		send_one_package( S2C_INVITE_BY_IP, 0, strLinphonecCmd, sizeof(strLinphonecCmd) );
	return 0;
}

int API_linphonec_Answer( void )
{
	char strLinphonecCmd[25];		//"call sip:xxx.xxx.xxx.xxx"
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );

	sprintf(strLinphonecCmd, "%s", "answer");

	send_one_package( S2C_ANSWER, 0, strLinphonecCmd, sizeof(strLinphonecCmd) );

	return 0;
}

int API_linphonec_Close( void )
{
	char strLinphonecCmd[25];

	usleep(100000); 	//lzh_20151102	
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );
	
	// 得到呼叫结束命令
	sprintf(strLinphonecCmd, "%s", "terminate");

	send_one_package( S2C_TERMINATE, 0, strLinphonecCmd, sizeof(strLinphonecCmd) );

	usleep(100000);	//wait linphnec close compeleted!

	dxdev_close_linphone();			//czn_20170922
	
	return 0;		
}

int API_linphonec_quit( void )
{
	char strLinphonecCmd[25];

	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );
	
	// 得到呼叫结束命令
	sprintf(strLinphonecCmd, "%s", "quit");

	send_one_package( S2C_CANCEL, 0, strLinphonecCmd, sizeof(strLinphonecCmd) );

	usleep(100000);	//wait linphnec close compeleted!
	
	return 0;		
}

int API_linphonec_Register_SipCurAccount( char* proxy, char* identify, char* password )
{
#if 1
	char identify_full[100]={0};
	char datLinphonecCmd[250]={0};
	char temp[250]={0};
	
	char *ptrLinphonecCmd = datLinphonecCmd;

	Set_SipAccount_State(0,0);
	
	API_Event_IoServer_InnerRead_All(SIP_ENABLE, (uint8*)temp);
	if(atoi(temp) == 0)
	{
		Set_SipAccount_State(2,0);
		return -1;
	}

	memset(datLinphonecCmd, ' ', sizeof(datLinphonecCmd) );		// 使用空格作为分割符

	memcpy( ptrLinphonecCmd, "register", strlen( "register" ) );
	ptrLinphonecCmd += strlen( "register" );
	ptrLinphonecCmd += 1;

	sprintf(identify_full,"sip:%s@%s",identify,proxy);
	memcpy( ptrLinphonecCmd, identify_full, strlen( identify_full ) );
	ptrLinphonecCmd += strlen( identify_full );
	ptrLinphonecCmd += 1;

	memcpy( ptrLinphonecCmd, proxy, strlen( proxy ) );
	ptrLinphonecCmd += strlen( proxy );
	ptrLinphonecCmd += 1;

	memcpy( ptrLinphonecCmd, password, strlen( password ) );
	ptrLinphonecCmd += strlen( password );
	ptrLinphonecCmd += 1;

	send_one_package( S2C_SET_SIP_ACCOUNT, 0, datLinphonecCmd, ptrLinphonecCmd - datLinphonecCmd );
#endif
	return 0;
}

int API_linphonec_Apply_SipCurAccount( void )
{
	send_one_package( S2C_APPLY_SIP_ACCOUNT, 0, NULL, 0 );	
	return 0;
}

int API_linphonec_Delete_SipCurAccount( void )
{
	send_one_package( S2C_DELETE_SIP_ACCOUNT, 0, NULL, 0 );		
	return 0;
}

int API_linphonec_Stop_SipCurAccount( void )
{
	send_one_package( S2C_STOP_SIP_ACCOUNT, 0, NULL, 0 );		
	return 0;
}

int API_linphonec_Start_SipCurAccount( void )
{
	send_one_package( S2C_START_SIP_ACCOUNT, 0, NULL, 0 );	
	return 0;	
}

void Set_SipAccount_State(int new_state,int err_code)
{
	SipReg_State = new_state;
	SipReg_ErrCode = err_code;
}

int API_linphonec_Apply_Heartbeat( void )		//czn_20180109
{
	send_one_package( S2C_APPLY_HEARTBEAT, 0, NULL, 0 );	
	return 0;
}

int API_linphonec_Set_VdQuant(int newLevel)
{
	send_one_package(S2C_APPLY_SET_VD_QUANT, newLevel, NULL, 0 );
	return 0;
}
	
int Get_SipAccount_State(void)
{
	return SipReg_State;
}

int Get_SipReg_ErrCode(void)
{
	return SipReg_ErrCode;
}

//czn20181030_s
//#include "../../wifi_service/task_WiFiConnect/task_WiFiConnect.h"
int Get_SipSer_State(void)	//=1 ok 0 fail 
{
	//WIFI_RUN wifiRun = GetWiFiState();
	
	if(Get_SipAccount_State() == 1)
	{
		return 1;//API_SpriteDisplay_XY( MAIN_SIP_CONFIG_X, MAIN_SIP_CONFIG_Y, SPRITE_SipSerOk);
	}
	else
	{	
		return 0;
	}
}
//czn20181030_e

// lzh_20181101_s
int API_linphonec_apply_registration_state(void)
{
	send_one_package(S2C_APPLY_REGISTRTION_STATE, 0, NULL, 0 );
	return 0;
}

int API_linphonec_apply_snapshot_one_jpeg( char* filename )
{
	char strLinphonecCmd[160];
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );

	sprintf(strLinphonecCmd, "%s %s", "snapshot", filename);

	send_one_package( S2C_APPLY_SNAPSHOT_ONE_JPEG, 0, strLinphonecCmd, strlen(strLinphonecCmd)+1 );

	return 0;
}

// lzh_20190723_s
int API_linphonec_apply_start_press_to_talk( int onflag, char* server_addr, char port )
{
#if 1
	char strLinphonecCmd[160];
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );
	
	sprintf(strLinphonecCmd, "%s %d", server_addr, port);
	
	send_one_package( S2C_REQ_PRESS_TO_TALK_STATE, onflag, strLinphonecCmd, strlen(strLinphonecCmd)+1 );
#endif	
	return 0;
}
// lzh_20190723_e

// lzh_20200707_s
int API_linphonec_is_vdout_enable(void)
{
	return linphone_vout_enable;
}
// lzh_20200707_e

// lzh_20201110_s
int API_linphonec_set_sip_port(short port)
{
	char strLinphonecCmd[160];
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );
	
	sprintf(strLinphonecCmd, "%s %d", "ports sip", port);
	
	send_one_package( S2C_APPLY_SET_SIP_PORT, 0, strLinphonecCmd, strlen(strLinphonecCmd)+1 );
	
	return 0;
}
// lzh_20201110_e


// lzh_20201201_s
int API_linphonec_apply_update_audio_video(int audio_update, int video_update)
{
	char strLinphonecCmd[160];
	
	memset(strLinphonecCmd, 0, sizeof(strLinphonecCmd) );
	
	sprintf(strLinphonecCmd, "%d %d", audio_update,video_update);
	
	send_one_package( S2C_REQ_UPDATE_MEDIA_STATE, 0, strLinphonecCmd, strlen(strLinphonecCmd)+1 );
	
	return 0;
}
// lzh_20201201_e

char* GetDivertAbilityPara(void)
{
	cJSON *root = NULL;
	char* string;
	char disp[50];
	root = cJSON_CreateObject();
	API_Event_IoServer_InnerRead_All(CallScene_SET, disp);
	printf("CallScene_SET=%s\n", disp);
	if(atoi(disp)==5)
		cJSON_AddNumberToObject(root, "DivertAbility", 0);
	else
	{
		API_Event_IoServer_InnerRead_All(SIP_ENABLE, disp);
		cJSON_AddNumberToObject(root, "DivertAbility", Get_SipAccount_State()&&atoi(disp));
	}
	//cJSON_AddNumberToObject(root, "DivertAbility", 1);

	string = cJSON_Print(root);
	if(string != NULL)
	{	
	printf("DivertAbility=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}
int ParseDivertAbilityPara(char *data)
{
	cJSON *JsonObject = cJSON_Parse(data);
	if(JsonObject==NULL)
		return -1;
	int temp;
	ParseJsonNumber(JsonObject,"DivertAbility",&temp);
	return temp;
}
#endif

// lzh_20210716_s
int audio_rtp_outside_flag = 1;
int API_linphonec_update_vdout_status(void)
{
	uint8 ioData = 1;
	//API_Event_IoServer_InnerRead_All(NewRTPServerEnable, &ioData);
	linphone_vout_enable = ioData ? 0 : 1;

	// lzh_20210702_s
	// set linphone VOUT_EN
	if( audio_rtp_outside_flag )
		send_one_package(S2C_APPLY_SET_VOUT_EN, linphone_vout_enable|0x02, NULL, 0 );	// |0x02 just set audio use outside rtp transfer
	else
		send_one_package(S2C_APPLY_SET_VOUT_EN, linphone_vout_enable, NULL, 0 );
	// lzh_20210702_e

	usleep(50*1000);
	// set linphone NEWREP_RECV_EN
	API_linphonec_apply_start_press_to_talk(!linphone_vout_enable,NULL,0);
	return 0;
}
// lzh_20210716_e

