/**
  ******************************************************************************
  * @file    task_Caller.h
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_DtCaller_H
#define _task_DtCaller_H

//#include "task_Survey.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"


typedef enum
{	
	DtCaller_MainCall,
	DtCaller_IntercomCall,					
	DtCaller_InnerCall,
  DtCaller_InnerBrdCall,
}DtCaller_CallType_t;

extern OBJ_CALLER_STRU DtCaller_Obj;

void init_vdp_dtcaller_task(void);
uint8 Get_DtCaller_State(void);	

int API_DtCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr);
void DtCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);

#define API_DtCaller_Invite(call_type)		API_DtCaller_Common(CALLER_MSG_INVITE,call_type,NULL,NULL)
#define API_DtCaller_Ring(call_type)		API_DtCaller_Common(CALLER_MSG_RINGING,call_type,NULL,NULL)
#define API_DtCaller_Ack(call_type)		API_DtCaller_Common(CALLER_MSG_ACK,call_type,NULL,NULL)
#define API_DtCaller_Cancel(call_type)			API_DtCaller_Common(CALLER_MSG_CANCEL,call_type,NULL,NULL)
#define API_DtCaller_Bye(call_type)				API_DtCaller_Common(CALLER_MSG_BYE,call_type,NULL,NULL)
#define API_DtCaller_ForceClose()					API_DtCaller_Common(CALLER_MSG_FORCECLOSE,0,NULL,NULL)
#endif
		