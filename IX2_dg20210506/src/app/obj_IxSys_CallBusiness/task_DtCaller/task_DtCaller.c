/**
  ******************************************************************************
  * @file    task_Called.c
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>



#include "obj_Caller_State.h"
#include "task_CallServer.h"
#include "task_DtCaller.h"
#include "obj_DtCaller_IntercomCall_Callback.h"
#include "obj_DtCaller_InnerCall_Callback.h"
#include "obj_DtCaller_InnerBrdCall_Callback.h"

#include "OSTIME.h"

OS_TIMER timer_dtcaller;

OBJ_CALLER_STRU DtCaller_Obj;

Loop_vdp_common_buffer	vdp_dtcaller_mesg_queue;
Loop_vdp_common_buffer	vdp_dtcaller_sync_queue;


vdp_task_t	task_dtcaller;
void* vdp_dtcaller_task( void* arg );

void DtCaller_Config_Data_Init(void);
void DtCaller_MenuDisplay_ToInvite(void);
void DtCaller_MenuDisplay_ToRing(void);
void DtCaller_MenuDisplay_ToAck(void);
void DtCaller_MenuDisplay_ToBye(void);
void DtCaller_MenuDisplay_ToWait(void);
void DtCaller_Timer_Callback(void);

#define DtCaller_Support_CallTypeNum	3


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOINVITE_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	{DtCaller_IntercomCall, 		Callback_DtCaller_ToInvite_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToInvite_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToInvite_InnerBrdCall,},

	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TORINGING_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 			Callback_Caller_ToRinging_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToRinging_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToRinging_InnerCall,},	
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToRinging_InnerBrdCall,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOACK_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToAck_VtkUnicastCall,},			//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToAck_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToAck_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToAck_InnerBrdCall,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOBYE_CALLBACK[]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 			Callback_Caller_ToBye_VtkUnicastCall,},			//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToBye_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToBye_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToBye_InnerBrdCall,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOWAITING_CALLBACK[]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToWaiting_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToWaiting_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToWaiting_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToWaiting_InnerBrdCall,},
	
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOUNLOCK_CALLBACK[]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 		Callback_Caller_ToUnlock_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToUnlock_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToUnlock_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToUnlock_InnerBrdCall,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOTIMEOUT_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToTimeout_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToTimeout_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToTimeout_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToTimeout_InnerBrdCall,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOERROR_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToError_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToError_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToError_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToError_InnerBrdCall,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOFORCECLOSE_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToForceClose_VtkUnicastCall,},		//小区主机呼叫分机
	{DtCaller_IntercomCall, 		Callback_DtCaller_ToForceClose_IntercomCall,},		
	{DtCaller_InnerCall,			Callback_DtCaller_ToForceClose_InnerCall,},
	{DtCaller_InnerBrdCall,			Callback_DtCaller_ToForceClose_InnerBrdCall,},
	
};


/*------------------------------------------------------------------------
			Caller_Task_Init
------------------------------------------------------------------------*/
void DtCaller_Obj_Init(void)	//R_
{
	//任务初始化
	DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
	DtCaller_Obj.timer_caller = &timer_dtcaller;
	DtCaller_Obj.task_caller = &task_dtcaller;
	
	DtCaller_Obj.Config_Data_Init = DtCaller_Config_Data_Init;

	DtCaller_Obj.to_invite_callback = TABLE_CALLTYPE_DTCALLER_TOINVITE_CALLBACK;
	DtCaller_Obj.to_ring_callback = TABLE_CALLTYPE_DTCALLER_TORINGING_CALLBACK; 
	DtCaller_Obj.to_ack_callback= TABLE_CALLTYPE_DTCALLER_TOACK_CALLBACK;
	DtCaller_Obj.to_bye_callback= TABLE_CALLTYPE_DTCALLER_TOBYE_CALLBACK;
	DtCaller_Obj.to_wait_callback= TABLE_CALLTYPE_DTCALLER_TOWAITING_CALLBACK;
	DtCaller_Obj.to_unclock_callback= TABLE_CALLTYPE_DTCALLER_TOUNLOCK_CALLBACK;
	DtCaller_Obj.to_timeout_callback= TABLE_CALLTYPE_DTCALLER_TOTIMEOUT_CALLBACK;
	DtCaller_Obj.to_error_callback= TABLE_CALLTYPE_DTCALLER_TOERROR_CALLBACK;
	DtCaller_Obj.to_forceclose_callback= TABLE_CALLTYPE_DTCALLER_TOFORCECLOSE_CALLBACK;
	DtCaller_Obj.to_redail_callback = NULL;		//czn_20171030
	
	DtCaller_Obj.support_calltype_num = DtCaller_Support_CallTypeNum;
	DtCaller_Obj.MenuDisplay_ToInvite = DtCaller_MenuDisplay_ToInvite;
	DtCaller_Obj.MenuDisplay_ToRing= DtCaller_MenuDisplay_ToRing;
	DtCaller_Obj.MenuDisplay_ToAck= DtCaller_MenuDisplay_ToAck;
	DtCaller_Obj.MenuDisplay_ToBye= DtCaller_MenuDisplay_ToBye;
	DtCaller_Obj.MenuDisplay_ToWait= DtCaller_MenuDisplay_ToWait;

	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_dtcaller, DtCaller_Timer_Callback, 1000/25);
}



/*------------------------------------------------------------------------
			Get_Caller_State
返回:
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_DtCaller_State(void)	//R
{
	if (DtCaller_Obj.Caller_Run.state==CALLER_WAITING)
	{
		return (0);
	}
	return (1);
}


/*------------------------------------------------------------------------
			Get_Caller_PartnerAddr
------------------------------------------------------------------------*/
//uint16 Get_Caller_PartnerAddr(void)	//R
//{
//	return (Caller_Run.partner_IA);
//}





/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void dtcaller_mesg_data_process(void *Msg,int len )	//R_
{
	bprintf("!!!dtcaller state = %d,recv msg = %d\n",DtCaller_Obj.Caller_Run.state,((CALLER_STRUCT *)Msg)->msg_type);
	vtk_TaskProcessEvent_Caller(&DtCaller_Obj,(CALLER_STRUCT *)Msg);
	
}

//OneCallType	OneMyCallObject;

void init_vdp_dtcaller_task(void)
{
	init_vdp_common_queue(&vdp_dtcaller_mesg_queue, 1000, (msg_process)dtcaller_mesg_data_process, &task_dtcaller);
	init_vdp_common_queue(&vdp_dtcaller_sync_queue, 100, NULL, 								  &task_dtcaller);
	init_vdp_common_task(&task_dtcaller, MSG_ID_DtCalller, vdp_dtcaller_task, &vdp_dtcaller_mesg_queue, &vdp_dtcaller_sync_queue);
}

void exit_dtcaller_task(void)
{
	
}

void* vdp_dtcaller_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	DtCaller_Obj_Init();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void DtCaller_Timer_Callback(void)
{
	Caller_Timer_Callback(&DtCaller_Obj);
}

void DtCaller_Config_Data_Init(void)
{
	switch(DtCaller_Obj.Caller_Run.call_type)
	{
		case DtCaller_IntercomCall:
			DtCaller_Obj.Caller_Config.limit_invite_time = 5;
			DtCaller_Obj.Caller_Config.limit_ringing_time = 40;
			DtCaller_Obj.Caller_Config.limit_ack_time = 90;
			DtCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
		case DtCaller_InnerCall:
		case DtCaller_InnerBrdCall:		
			DtCaller_Obj.Caller_Config.limit_invite_time = 90;
			DtCaller_Obj.Caller_Config.limit_ringing_time = 90;
			DtCaller_Obj.Caller_Config.limit_ack_time = 90;
			DtCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
	}
}
void DtCaller_MenuDisplay_ToInvite(void)
{

}

void DtCaller_MenuDisplay_ToRing(void)
{
}

void DtCaller_MenuDisplay_ToAck(void)
{
}
void DtCaller_MenuDisplay_ToBye(void)
{
}
void DtCaller_MenuDisplay_ToWait(void)
{
}

int API_DtCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	CALLER_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_dtcaller.p_msg_buf, (char *)&send_msg, sizeof(CALLER_STRUCT)) != 0)
	{
		return -1;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void DtCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

