#ifndef _obj_DtCaller_InnerBrdCall_Callback_H
#define _obj_DtCaller_InnerBrdCall_Callback_H

void Callback_DtCaller_ToRedial_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToInvite_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToRinging_InnerBrdCall(CALLER_STRUCT *msg)	;
void Callback_DtCaller_ToAck_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToBye_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToWaiting_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToUnlock_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToTimeout_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToError_InnerBrdCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToForceClose_InnerBrdCall(CALLER_STRUCT *msg);

#endif
