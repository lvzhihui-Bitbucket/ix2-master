/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */
#include "task_CallServer.h"
#include "obj_Caller_State.h"
#include "task_DtCaller.h"



#include "obj_DtCaller_InnerCall_Callback.h"
#include "define_Command.h"
#include "stack212.h"
#include "obj_UnitSR.h"

extern Global_Addr_Stru Register_DxIm_List[];

extern int Register_DxIm_Num;

/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_DtCaller_ToRedial_InnerBrdCall(CALLER_STRUCT *msg)
{
	;	//暂无处理
}

/*------------------------------------------------------------------------
						呼叫申请
------------------------------------------------------------------------*/
void Callback_DtCaller_ToInvite_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	uint8 SR_apply;
	SR_apply = API_SR_Request(CT15_INNER_BRD);
	if (SR_apply == SR_DISABLE)	//???,????
	{
		API_CallServer_InviteFail(CallServer_Run.call_type);
	}
	else if (SR_apply == SR_ENABLE_DEPRIVED)	//???,???
	{
		API_SR_Deprived();
	}
	else 
	{
		;	//???????
	}	
	
		
	  unsigned char buf[5];
        buf[0] = (unsigned char)((myAddr>>8)&0xff);		            // s_addr_h
        buf[1] = (unsigned char)(myAddr&0xff);		                // s_addr_l
        buf[2] = (unsigned char)((myAddr>>8)&0xff);	// t_addr_h
        buf[3] = (unsigned char)(myAddr&0xff);		// t_addr_l
       
        buf[4] = 1;     // 1 = 户内广播, 2 = 户间广播, 3 = 组群1广播, 4 = 组群2广播, 5 = 系统广播
       
        
        //API_Event_GoServer_ToWrite(COBID29_BROADCASTING, 5, buf);
       API_GroupValue_Write(29, 5, buf);

	SR_Routing_Create(CT15_INNER_BRD);
	#if 0
	Global_Addr_Stru gaddr;
		gaddr.gatewayid = 0;
		gaddr.ip = GetLocalIp();
		gaddr.rt = 0;
		gaddr.code = 0;
	#endif
	API_CallServer_InviteOk(CallServer_Run.call_type);
	
}

/*------------------------------------------------------------------------
						呼叫成功,进入Ringing
------------------------------------------------------------------------*/
void Callback_DtCaller_ToRinging_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	//will_add_czn
	/*if(SR_State.in_use == FALSE)
	{
		//链路创建
		//API_SR_CallerCreate();
		SR_Routing_Create(Business_Run.call_sub_type);
		
	}*/
	// lzh_20170328_s // 添加视频服务远程调试信息到DMR18s
	//char detail[LOG_DESC_LEN+1];
	//snprintf(detail,LOG_DESC_LEN+1,"[Dail](%03d,%04d) OK",Caller_Run.t_addr.gatewayid,Caller_Run.t_addr.rt*32+Caller_Run.t_addr.code);
	//API_add_log_item(0,"S_CSER",detail,NULL);
	// lzh_20170328_e
	//SR_Routing_Create(CT13_INTERCOM_CALL);
	
}

/*------------------------------------------------------------------------
						分机摘机,进入ACK
------------------------------------------------------------------------*/
void Callback_DtCaller_ToAck_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	
}

/*------------------------------------------------------------------------
						主机挂机,返回Waiting
------------------------------------------------------------------------*/

// lzh_20160127_s
#include "../../../video_service/video_object.h"
#include "../../../video_service/ip_camera_control/ip_camera_control.h"
#include "../../../video_service/ip_video_control/ip_video_control.h"
// lzh_20160127_e

void Callback_DtCaller_ToBye_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	usleep(1000000);		//czn_20190314
	SR_Routing_Close(CT15_INNER_BRD);
	 unsigned char buf[5];
        buf[0] = (unsigned char)((myAddr>>8)&0xff);		            // s_addr_h
        buf[1] = (unsigned char)(myAddr&0xff);		                // s_addr_l
        buf[2] = (unsigned char)((myAddr>>8)&0xff);	// t_addr_h
        buf[3] = (unsigned char)(myAddr&0xff);		// t_addr_l
       
       buf[4] = 101;     // 101 = 户内广播结束, 102 = 户间广播结束, 103 = 组群1广播结束, 104 = 组群2广播结束, 105 = 系统广播结束
      
        //API_Event_GoServer_ToWrite(COBID29_BROADCASTING, 5, buf);
        API_GroupValue_Write(29, 5, buf);

	DtCaller_Obj.Caller_Run.timer = 0;
	OS_StopTimer(DtCaller_Obj.timer_caller);
	DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
}

/*------------------------------------------------------------------------
				分机挂机(单元链路强制释放),返回Waitting
------------------------------------------------------------------------*/
void Callback_DtCaller_ToWaiting_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	
	//Send_VtkUnicastCmd_StateNoticeAck(DsAndGl,&Caller_Run.t_addr,1);
	//Set_IPCallLink_Status(CLink_Idle);
	SR_Routing_Close(CT15_INNER_BRD);
}


/*------------------------------------------------------------------------
						开锁处理
------------------------------------------------------------------------*/
void Callback_DtCaller_ToUnlock_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller呼叫超时处理(仅自身退出呼叫状态)						
------------------------------------------------------------------------*/
void Callback_DtCaller_ToTimeout_InnerBrdCall(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(DtCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			DtCaller_Obj.Caller_Run.timer = 0;
			OS_StopTimer(DtCaller_Obj.timer_caller);
			//czn_20170411_s
			unsigned char state_save;
			state_save = DtCaller_Obj.Caller_Run.state;
			DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
			//API_talk_off();

			//Set_IPCallLink_Status(CLink_Idle);
			if(state_save == CALLER_INVITE)
			{
				API_CallServer_InviteFail(CallServer_Run.call_type);
			}
			else
			{
				#if 1
				SR_Routing_Close(CT15_INNER_BRD);
			//API_Stack_APT_Without_ACK(myAddr,INNER_CLOSE);
			 	unsigned char buf[5];
			    	buf[0] = (unsigned char)((myAddr>>8)&0xff);		            // s_addr_h
			   	buf[1] = (unsigned char)(myAddr&0xff);		                // s_addr_l
			    	buf[2] = (unsigned char)((myAddr>>8)&0xff);	// t_addr_h
			   	buf[3] = (unsigned char)(myAddr&0xff);		// t_addr_l
			   
			   	buf[4] = 101;     // 101 = 户内广播结束, 102 = 户间广播结束, 103 = 组群1广播结束, 104 = 组群2广播结束, 105 = 系统广播结束
			  
			    	API_GroupValue_Write(29, 5, buf);
				API_CallServer_Timeout(CallServer_Run.call_type);	
				#endif
			}
		}

	}
	#if 0
	else if(msg->msg_head.msg_sub_type == CALLER_TOUT_CHECKLINK)
	{		
		if((Caller_Run.state == CALLER_RINGING  || Caller_Run.state == CALLER_ACK) && Caller_Run.timer > 3)
		{
			Global_Addr_Stru gaddr;
			gaddr.gatewayid	= Caller_Run.t_addr.gatewayid;
			gaddr.ip			= Caller_Run.t_addr.ip;
			gaddr.rt			= 0;
			gaddr.code		= 0;
			if(API_Get_Partner_Calllink_NewCmd(DsAndGl,&gaddr,&partner_call_link) == -1)
			{
				if(++Caller_Run.checklink_error	 >= 2)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
			}
			else
			{
				Caller_Run.checklink_error = 0;
				
				if(partner_call_link.Status != CLink_AsCallServer && partner_call_link.Status != CLink_AsBeCalled)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
				
				if(partner_call_link.BeCalled_Data.Call_Source.ip != GetLocalIp() || 
					partner_call_link.BeCalled_Data.Call_Source.rt != Caller_Run.s_addr.rt||
					partner_call_link.BeCalled_Data.Call_Source.code != Caller_Run.s_addr.code)
				{
					quit_flag = 1;
				}
			}
	CTOUT_CHECKLINK_END:
			if(quit_flag == 1)
			{
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				Set_IPCallLink_Status(CLink_Idle);
				
				API_CallServer_IpCallerQuit();
			}
			
		}
		
	}
	#endif
	
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_DtCaller_ToError_InnerBrdCall(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//关闭定时
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_DtCaller_ToForceClose_InnerBrdCall(CALLER_STRUCT *msg)
{
	int i;
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		
		
		DtCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//关闭定时
		DtCaller_Obj.Caller_Run.timer = 0;

		//Set_IPCallLink_Status(CLink_Idle);
		//API_talk_off();
	}
}
