#ifndef _obj_Caller_Transfer2Sip_Callback_H
#define _obj_Caller_Transfer2Sip_Callback_H

void Callback_Caller_ToRedial_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToInvite_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToRinging_Transfer2Sip(CALLER_STRUCT *msg)	;
void Callback_Caller_ToAck_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToBye_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToWaiting_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToUnlock_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToTimeout_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToError_Transfer2Sip(CALLER_STRUCT *msg);
void Callback_Caller_ToForceClose_Transfer2Sip(CALLER_STRUCT *msg);

extern int Sip2SipCount,Sip2SipInterval,Sip2SipRetryTime,Sip2SipVideoQuality;

#endif