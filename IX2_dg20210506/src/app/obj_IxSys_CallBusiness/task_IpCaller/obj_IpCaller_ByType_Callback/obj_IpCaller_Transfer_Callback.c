/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */

#include "../../task_CallServer/task_CallServer.h"
#include "../../obj_Caller_State.h"
#include "../task_IpCaller.h"

// lzh_20170605_s
#include "../../obj_VtkLinphonecCommandInterface/linphone_interface.h"
// lzh_20170605_e

#include "obj_IpCaller_Transfer_Callback.h"
//#include "../../../device_manage/obj_device_register/obj_device_register.h"
#include "../../../task_debug_sbu/task_debug_sbu.h"
//#include "../../../wifi_service/task_WiFiConnect/task_WiFiConnect.h"		//czn_20181114

void start_one_sip_req(void);
void recv_one_sip_rsp(void);
float get_sip_delay_stat(void);
void sip_delay_stat_init(void);

int Transfer_reinvite_flag = 0;

//static int divert_video_answer = 0;
int divert_redail = 0;
int divert_reanwer = 0;
int app_req_redail = 0;

int divert_total = 0,divert_ring = 0,divert_hook = 0,video_ok = 0,divert_video_answer = 0,divert_once_ring = 0,video_once_ok = 0;
int have_ring;
int have_hook;
int divert_log_index = 0;
 #if 0
void disp_divert_final(char *disp);

void Set_Divert_Video_Flag(void)
{
	//divert_video_answer = 1;
	if(divert_video_answer == 0)
	{
		divert_video_answer = 1;
		video_ok++;
		if(divert_reanwer == 0)
		{
			video_once_ok++;
		}
		char detail[LOG_DESC_LEN+1];
		snprintf(detail,LOG_DESC_LEN+1,"T%04d^Video connected",divert_log_index);	
		API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	}
}

/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_Caller_ToRedial_Transfer(CALLER_STRUCT *msg)
{
	char cmdbuf[40];
		//DivertCaller_Business_Rps(msg,0);
		app_req_redail = 1;
		API_linphonec_Close();
		usleep(500000);
		IpCaller_Obj.Caller_Run.state = CALLER_INVITE;
		IpCaller_Obj.Caller_Run.timer = 0;
		OS_RetriggerTimer(IpCaller_Obj.timer_caller);
		Get_SipConfig_DirvertAccount(cmdbuf);
		API_linphonec_Invite(cmdbuf);
		char detail[LOG_DESC_LEN+1];
		snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Req Redial",divert_log_index);	
		API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
}

/*------------------------------------------------------------------------
						呼叫申请
------------------------------------------------------------------------*/
void Callback_Caller_ToInvite_Transfer(CALLER_STRUCT *msg)	//R_
{
	unsigned char error_type;	//czn_20170411
	//will_add_czn
	/*uint8 SR_apply;
	SR_apply = API_SR_Request(CT08_DS_CALL_ST); 
						//链路忙,不可剥夺
	if (SR_apply == SR_DISABLE)
	{
		//will_add	//对小区主机_增加系统忙的应答
		return;
	}
	//链路忙,可剥夺
	else if (SR_apply == SR_ENABLE_DEPRIVED)	
	{
		API_SR_Deprived();	//复位系统链路状态字
	}
	//链路可直接使用
	else	 
	{	
		API_SR_Deprived();	//复位系统链路状态字	
	}*/
	//API_Stack_APT_Without_ACK(Caller_Run.target_addr&0x00ff,MAIN_CALL);
	//printf("___________________Caller_Run.tdev.ip%08x\n",Caller_Run.tdev.ipdev.ip);
	
	_IP_Call_Link_Run_Stru partner_call_link;
	Global_Addr_Stru gaddr;
	int i;
	
	
	//IpCaller_Respones(msg->msg_source_id,msg->msg_type,0);
	//return;
	API_talk_off();		//czn_201704010

	
	API_ClearLogViewWin();

	divert_total++;
	
	if(divert_log_index++ >= 9999)
		divert_log_index = 0;
	
	divert_redail = 0;
	divert_reanwer = 0;
	app_req_redail = 0;
	have_ring = 0;
	have_hook = 0;
	divert_video_answer = 0;
	app_req_redail = 0;

	// lzh_20170605_s
	
			char cmdbuf[40];
			//struct in_addr temp;
			//temp.s_addr = Register_LocalPhone_List[0].ip;  
			//snprintf(cmdbuf,30,"sip:%s",inet_ntoa(temp));
			//API_linphonec_Invite(cmdbuf);
			//Get_SipConfig_Account(cmdbuf);
			Get_SipConfig_DirvertAccount(cmdbuf);
			start_one_sip_req();
			API_linphonec_Invite(cmdbuf);
			usleep(10000);
			extern int divertresolution_type;
			API_linphonec_Set_VdQuant(divertresolution_type);
			//API_linphonec_Invite("12345678");
	
			Transfer_reinvite_flag = 0;
			
			char detail[LOG_DESC_LEN+1];
			snprintf(detail,LOG_DESC_LEN+1,"T%04d^Start:%s",divert_log_index,cmdbuf);	
			API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
			//czn_20181114_s
			WIFI_RUN wifiRun = GetWiFiState();
			snprintf(detail,LOG_DESC_LEN+1,"T%04d^ssid:%s,signal:%d",divert_log_index,wifiRun.curWifi.ESSID,wifiRun.curWifi.LEVEL);
			API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
			//czn_20181114_e
}

/*------------------------------------------------------------------------
						呼叫成功,进入Ringing
------------------------------------------------------------------------*/
void Callback_Caller_ToRinging_Transfer(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	//will_add_czn
	/*if(SR_State.in_use == FALSE)
	{
		//链路创建
		//API_SR_CallerCreate();
		SR_Routing_Create(Business_Run.call_sub_type);
		
	}*/
	// lzh_20170328_s // 添加视频服务远程调试信息到DMR18s
	//char detail[LOG_DESC_LEN+1];
	//snprintf(detail,LOG_DESC_LEN+1,"[Dail](%03d,%04d) OK",Caller_Run.t_addr.gatewayid,Caller_Run.t_addr.rt*32+Caller_Run.t_addr.code);
	//API_add_log_item(0,"S_CSER",detail,NULL);
	// lzh_20170328_e
	if(app_req_redail == 0)
	{
		if(have_ring == 0)
		{
			divert_ring++;
			if(divert_redail == 0)
			{
				divert_once_ring++;
			}
			have_ring = 1;
		}
	}
	
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Ring",divert_log_index);	
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	
}

/*------------------------------------------------------------------------
						分机摘机,进入ACK
------------------------------------------------------------------------*/
void Callback_Caller_ToAck_Transfer(CALLER_STRUCT *msg)	//R_
{
	int i;
	uint8 auto_mute_en = 0;
	API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);
	
	if(app_req_redail == 0)
	{
		if(have_hook == 0)
		{
			divert_hook++;
			have_hook = 1;
		}

		if(have_ring == 0)
		{
			divert_ring++;
			if(divert_redail == 0)
			{
				divert_once_ring++;
			}
			have_ring = 1;
		}
	}
	
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Answer",divert_log_index);	
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	
	IpCaller_Obj.Caller_Run.t_addr = msg->t_addr;
	sleep(2);
	API_linphonec_Answer();
	usleep(1000000);
	#if 1		//czn_20181114
	if(auto_mute_en == 0)
	{
		LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
		LoadLinPhoneAudioPlaybackVol();//LoadAudioPlaybackVol();
	}
	#endif
}

/*------------------------------------------------------------------------
						主机挂机,返回Waiting
------------------------------------------------------------------------*/

// lzh_20160127_s
#include "../../../video_service/video_object.h"
#include "../../../video_service/ip_camera_control/ip_camera_control.h"
#include "../../../video_service/ip_video_control/ip_video_control.h"
// lzh_20160127_e

void Callback_Caller_ToBye_Transfer(CALLER_STRUCT *msg)	//R_
{
	
	API_talk_off();

	// lzh_20170605_s
	API_linphonec_Close();
	// lzh_20170605_e

	IpCaller_Obj.Caller_Run.timer = 0;
	OS_StopTimer(IpCaller_Obj.timer_caller);
	
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;

	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Source Cancel",divert_log_index);	
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

	disp_divert_final(detail);
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
}

/*------------------------------------------------------------------------
				分机挂机(单元链路强制释放),返回Waitting
------------------------------------------------------------------------*/
void Callback_Caller_ToWaiting_Transfer(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	
	//Send_VtkUnicastCmd_StateNoticeAck(DsAndGl,&Caller_Run.t_addr,1);
	//Set_IPCallLink_Status(CLink_Idle);
	//Send_AckCmd_VtkUnicast(msg->t_addr);

	API_talk_off();

	API_linphonec_Close();

	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Cancel",divert_log_index);	
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

	disp_divert_final(detail);
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
}


/*------------------------------------------------------------------------
						开锁处理
------------------------------------------------------------------------*/
void Callback_Caller_ToUnlock_Transfer(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller呼叫超时处理(仅自身退出呼叫状态)						
------------------------------------------------------------------------*/
void Callback_Caller_ToTimeout_Transfer(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	char detail[LOG_DESC_LEN+1];
	
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(IpCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			
			//czn_20170411_s
			unsigned char state_save;
			state_save = IpCaller_Obj.Caller_Run.state;
			
			API_linphonec_Close();
			if(state_save == CALLER_INVITE)
			{
				if(Transfer_reinvite_flag == 0)
				{
					Transfer_reinvite_flag = 1;
					char cmdbuf[40];
					Get_SipConfig_DirvertAccount(cmdbuf);
					//API_linphonec_Invite(cmdbuf);
					//API_linphonec_Close();
					usleep(500000);
					API_linphonec_Invite(cmdbuf);
					IpCaller_Obj.Caller_Run.state = CALLER_INVITE;
					IpCaller_Obj.Caller_Run.timer = 0;
					OS_RetriggerTimer(IpCaller_Obj.timer_caller);

					snprintf(detail,LOG_DESC_LEN+1,"T%04d^ReInvite %d",divert_log_index,++divert_redail);	
					API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}
				else
				{
					IpCaller_Obj.Caller_Run.timer = 0;
					OS_StopTimer(IpCaller_Obj.timer_caller);
					
					IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
					API_talk_off();

					Set_IPCallLink_Status(CLink_Idle);
				
					API_CallServer_InviteFail(CallServer_Run.call_type);

					snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Not found",divert_log_index);	
					API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

					disp_divert_final(detail);
					API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}
			}
			else
			{
				if(IpCaller_Obj.Caller_Run.state == CALLER_RINGING)
				{
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target No answer",divert_log_index);	
					API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}
				else if(IpCaller_Obj.Caller_Run.state == CALLER_ACK)
				{
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^End: talkover",divert_log_index);	
					API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}

				disp_divert_final(detail);
				API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				
				IpCaller_Obj.Caller_Run.timer = 0;
				OS_StopTimer(IpCaller_Obj.timer_caller);
				
				IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				Set_IPCallLink_Status(CLink_Idle);
				//API_CallServer_IpCallerQuit();
				API_CallServer_Timeout(CallServer_Run.call_type);
			}
		}
		//czn_20170411_e
	}
	else if(msg->msg_sub_type == CALLER_TOUT_CHECKLINK)
	{		
		if(divert_video_answer == 0&&IpCaller_Obj.Caller_Run.state == CALLER_ACK && divert_reanwer<3)
		{
			if((IpCaller_Obj.Caller_Run.timer>0) && (IpCaller_Obj.Caller_Run.timer%6) == 0)
			{
				//char detail[LOG_DESC_LEN+1];
				//snprintf(detail,LOG_DESC_LEN+1,"[Divert]ReAnswer");	
				//API_add_log_item(0,"S_CSER",detail,NULL);
				API_linphonec_Answer();
				
				snprintf(detail,LOG_DESC_LEN+1,"T%04d^ReAnswer%d",divert_log_index,++divert_reanwer);	
				API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
			}
		}
	}
	#if 0
	else if(msg->msg_head.msg_sub_type == CALLER_TOUT_CHECKLINK)
	{		
		if((Caller_Run.state == CALLER_RINGING  || Caller_Run.state == CALLER_ACK) && Caller_Run.timer > 3)
		{
			Global_Addr_Stru gaddr;
			gaddr.gatewayid	= Caller_Run.t_addr.gatewayid;
			gaddr.ip			= Caller_Run.t_addr.ip;
			gaddr.rt			= 0;
			gaddr.code		= 0;
			if(API_Get_Partner_Calllink_NewCmd(DsAndGl,&gaddr,&partner_call_link) == -1)
			{
				if(++Caller_Run.checklink_error	 >= 2)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
			}
			else
			{
				Caller_Run.checklink_error = 0;
				
				if(partner_call_link.Status != CLink_AsCallServer && partner_call_link.Status != CLink_AsBeCalled)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
				
				if(partner_call_link.BeCalled_Data.Call_Source.ip != GetLocalIp() || 
					partner_call_link.BeCalled_Data.Call_Source.rt != Caller_Run.s_addr.rt||
					partner_call_link.BeCalled_Data.Call_Source.code != Caller_Run.s_addr.code)
				{
					quit_flag = 1;
				}
			}
	CTOUT_CHECKLINK_END:
			if(quit_flag == 1)
			{
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				Set_IPCallLink_Status(CLink_Idle);
				
				API_CallServer_IpCallerQuit();
			}
			
		}
		
	}
	#endif
	
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_Caller_ToError_Transfer(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//关闭定时
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_Caller_ToForceClose_Transfer(CALLER_STRUCT *msg)
{
	
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		
		IpCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//关闭定时
		IpCaller_Obj.Caller_Run.timer = 0;

		//Set_IPCallLink_Status(CLink_Idle);
		API_talk_off();
		
		// lzh_20170605_s
		API_linphonec_Close();
		// lzh_20170605_e
		
	}
}


////////////////////////////////////////////////////////
extern int divert_total_save,divert_ring_save,divert_hook_save,video_ok_save,divert_once_ring_save,video_once_ok_save;
int Get_Divert_TotalNum(void)
{
	if(!get_sip_call_test_state())
		return divert_total;
	else
		return (divert_total-divert_total_save);
}

int Get_Divert_SuccNum(void)
{	
	if(!get_sip_call_test_state())
		return divert_ring;
	else
		return (divert_ring-divert_ring_save);
}

int Get_Divert_HookNum(void)
{
	if(!get_sip_call_test_state())
		return divert_hook;
	else
		return (divert_hook-divert_hook_save);
}

int Get_Divert_VideoConnectNum(void)
{
	if(!get_sip_call_test_state())
		return video_ok;
	else
		return (video_ok-video_ok_save);
}

float Get_Divert_SuccPercent(void)
{
	float v1,v2,result;
	if(!get_sip_call_test_state())
	{
		v1 = (float)divert_ring;
		v2 = (float)divert_total;
	}
	else
	{
		v1 = (float)(divert_ring-divert_ring_save);
		v2 = (float)(divert_total-divert_total_save);
	}

	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

float Get_Divert_OnceSuccPercent(void)
{
	float v1,v2,result;
	if(!get_sip_call_test_state())
	{
		v1 = (float)divert_once_ring;
		v2 = (float)divert_total;
	}
	else
	{
		v1 = (float)(divert_once_ring-divert_once_ring_save);
		v2 = (float)(divert_total-divert_total_save);
	}

	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

float Get_Divert_VideoConnectPercent(void)
{
	float v1,v2,result;
	if(!get_sip_call_test_state())
	{
		v1 = (float)video_ok;
		v2 = (float)divert_hook;
	}
	else
	{
		v1 = (float)(video_ok-video_ok_save);
		v2 = (float)(divert_hook-divert_hook_save);
	}

	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

float Get_Divert_OnceVideoConnectPercent(void)
{
	float v1,v2,result;
	if(!get_sip_call_test_state())
	{
		v1 = (float)video_once_ok;
		v2 = (float)divert_hook;
	}
	else
	{
		v1 = (float)(video_once_ok-video_once_ok_save);
		v2 = (float)(divert_hook-divert_hook_save);
	}
	

	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

void disp_divert_final(char *disp)
{
	sprintf(disp,"T%04d^Result:T%d,Call(%s,%d)",divert_log_index,divert_log_index,(have_ring)?"S":"F",divert_redail);

	if(have_hook)
	{
		sprintf(disp+strlen(disp),",Talk(%s,%d)",(divert_video_answer)?"S":"F",divert_reanwer);
	}
}

extern char logfile_name[];
extern int	  logfile_line;
int siplog_start_line_save = 0;

void Divert_Stat_Init(void)	//czn_20181027
{
	FILE *pf = fopen(logfile_name,"r");
	char line_buff[LOG_DESC_LEN*2];
	char *pch1;
	char strbuff[LOG_DESC_LEN];
	char tindex;
	int start_stat;
	int line_cnt = 0;
	int save_index = -1;
	
	divert_total = 0;
	divert_ring = 0;
	divert_hook = 0;
	video_ok = 0;
	//divert_video_answer = 0;
	divert_once_ring = 0;
	video_once_ok = 0;
	siplog_start_line_save = 0;
	divert_log_index = 0;
	
	if(pf!=NULL)
	{
		line_buff[0] = 0;
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			line_cnt ++;
			if((pch1 = strstr(line_buff,Log_Devert_Title)) != NULL)
			{
				memcpy(strbuff,pch1+8,4);
				strbuff[4] = 0;
				tindex = atol(strbuff);
				
				//if(tindex == 0||tindex == 1)
				{
					//if(divert_total >1)
					{
						
						//siplog_start_line_save = line_cnt;
						if(strstr(line_buff,"StatClear") != NULL)
						{
							siplog_start_line_save = line_cnt;
							divert_log_index = tindex;
							save_index = tindex;
							//save_index = divert_log_index+1;
							//if(save_index >= 10000)
							//	save_index = 0;
							//siplog_start_line_save++;

							divert_total = 0;
							divert_ring = 0;
							divert_hook = 0;
							video_ok = 0;
							//divert_video_answer = 0;
							divert_once_ring = 0;
							video_once_ok = 0;
						}
						
					}
				}

				if(strstr(line_buff,"Result:")!=NULL)
				{
					//if(divert_total < tindex)
					if(save_index != tindex)
					{
						save_index = tindex;
						divert_total ++;
						divert_log_index = tindex;
						if((pch1 = strstr(line_buff,"Call("))!= NULL)
						{
							pch1 += strlen("Call(");
							if(*pch1 == 'S')
							{
								divert_ring++;
								pch1 += 2;
								if(*pch1 == '0')
								{
									divert_once_ring++;
								}
								if((pch1 = strstr(pch1,"Talk("))!= NULL)
								{
									divert_hook++;
									pch1 += strlen("Talk(");
									if(*pch1 == 'S')
									{
										video_ok++;
										pch1 += 2;
										if(*pch1 == '0')
										{
											video_once_ok++;
										}
									}
								}
								
							}
						}
					}
				}
			}
			
			line_buff[0] = 0;	
		}
		fclose(pf);
	}
}

void Divert_Stat_Clear(void)
{
	divert_total = 0;
	divert_ring = 0;
	divert_hook = 0;
	video_ok = 0;
	//divert_video_answer = 0;
	divert_once_ring = 0;
	video_once_ok = 0;

	siplog_start_line_save = logfile_line+1;

	
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^StatClear",divert_log_index);	
	API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

	sip_delay_stat_init();
}

int SipLog_Num;
int *SipLog_Index_Tb = NULL;

void Create_SipLog_Index_Tb(void)
{
	int SipLog_Index_max;
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	
	if(SipLog_Index_Tb  != NULL)
	{
		free(SipLog_Index_Tb);
		SipLog_Index_Tb = NULL;
	}
	SipLog_Num = 0;
	
	FILE *pf = fopen(logfile_name,"r");

	
	if(pf != NULL)
	{
		SipLog_Index_max = logfile_line - siplog_start_line_save;
		//printf("!!!!!!!!!!!!!logfile_line =%d,siplog_start_line_save =%d\n",logfile_line,siplog_start_line_save);
		if(SipLog_Index_max > 0)
		{
			SipLog_Index_Tb = malloc(sizeof(int)*SipLog_Index_max);

			if(SipLog_Index_Tb != NULL)
			{
				while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
				{
					if(++line_cnt >= siplog_start_line_save)
					{
						if(strstr(line_buff,Log_Devert_Title)!= NULL)
						{
							if(strstr(line_buff,"StatClear")== NULL)
							{
								
								SipLog_Index_Tb[SipLog_Num++] = line_cnt;

								if(SipLog_Num >= SipLog_Index_max)
									break;
							}
						}
					}
					//printf("!!!!!!!!!!!!!SipLog_Num=%d,line_cnt = %d,%s\n",SipLog_Num,line_cnt,line_buff);
				}
			}
		}
		fclose(pf);
	}

	//printf("!!!!!!!!!!!!!SipLog_Num=%d,line_cnt = %d\n",SipLog_Num,line_cnt);
}

void free_SipLog_Index_Tb(void)
{
	if(SipLog_Index_Tb  != NULL)
	{
		free(SipLog_Index_Tb);
		SipLog_Index_Tb = NULL;
	}
	SipLog_Num = 0;
}

int Get_SipLog_Num(void)
{
	return SipLog_Num;
}

int Get_One_SipLog(int index,char *log_str)
{
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	char strbuff[LOG_DESC_LEN];
	//int strindex = 0;
	char *pch1,*pch2,*str;
	int result = -1;
	
	if(SipLog_Index_Tb == NULL || index >= SipLog_Num)
	{
		return -1;
	}
	str = strbuff;
	
	FILE *pf = fopen(logfile_name,"r");
	index = SipLog_Num-1-index; 
	if(pf != NULL)
	{
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			if(++line_cnt == SipLog_Index_Tb[index])
			{
				if(strstr(line_buff,Log_Devert_Title)!= NULL)
				{
					pch1 = strstr(line_buff," ");
					
					if(pch1 != NULL)
					{
						pch1 += 3;
						memcpy(str,pch1,2);
						str += 2;
						pch1+=2;
						*str++ = '/';
						pch2 = strstr(pch1," ");
						memcpy(str,pch1,pch2-pch1+1);
						str += (pch2-pch1+1);
						pch2++;
						pch1 = strstr(pch2," ");//T0000^
						pch1 += 7;
						pch2 = strstr(pch1," ");

						memcpy(str,pch1,pch2-pch1);
						str += (pch2-pch1);
						*str = 0;
						str = strbuff;
						while(*str)
						{
							if(*str == '_')
							{
								*str = ' ';
							}
							str++;
						}
						strcpy(log_str,strbuff);
					}
					result = 0;
				}
				break;
			}
		}
		fclose(pf);
	}

	return result;
}



int sip_ser_delay_buff[10] ={-1,-1,-1,-1,-1,
							-1,-1,-1,-1,-1};
int sip_ser_req_time;

int sip_ser_delay_buff_index = 0;

void sip_delay_stat_init(void)
{
	int i;
	for(i = 0;i < 10;i ++)
	{
		sip_ser_delay_buff[i] = -1;
	}
	sip_ser_delay_buff_index = 0;
}

void start_one_sip_req(void)
{	
	sip_ser_req_time = time(NULL);

	if(++sip_ser_delay_buff_index>=10)
		sip_ser_delay_buff_index = 0;
	
	sip_ser_delay_buff[sip_ser_delay_buff_index] = -1;
	
}

void recv_one_sip_rsp(void)
{
	int cur_t = time(NULL);

	if(sip_ser_delay_buff_index < 10)
	{
		if(sip_ser_delay_buff[sip_ser_delay_buff_index] == -1)
		{
			sip_ser_delay_buff[sip_ser_delay_buff_index] = cur_t - sip_ser_req_time;

			//if(++sip_ser_delay_buff_index>=10)
			//	sip_ser_delay_buff_index = 0;
		}
	}
}

float get_sip_delay_stat(void)
{
	int i,j;
	float sum = 0;
	
	for(i = 0,j = 0;i < 10;i++)
	{
		if(sip_ser_delay_buff[i]>=0)
		{
			sum += sip_ser_delay_buff[i];
			j ++;
		}
	}
	
	if(j > 0)
	{
		sum = sum/(float)j;
		return sum;
	}
	return -1.0;
}
#endif
