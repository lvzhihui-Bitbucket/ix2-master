/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */
#include "../../task_CallServer/task_CallServer.h"
#include "../../obj_Caller_State.h"
#include "../task_IpCaller.h"



#include "obj_IpCaller_IxSys_Callback.h"

extern Global_Addr_Stru Register_DxIm_List[];

extern int Register_DxIm_Num;

extern Global_Addr_Stru Register_LocalPhone_List[];
extern int Register_LocalPhone_Num;
/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_Caller_ToRedial_IxSys(CALLER_STRUCT *msg)		//czn_20171030
{
	#if 0
	int i;
	if(CallServer_Run.call_type == IxCallScene2)
	{
		for(i = 0;i < Register_DxIm_Num;i ++)
		{
			Send_VtkUnicastCmd_SourceRedailReq(Register_DxIm_List[i]);
		}
	}
	else
	{
		Send_VtkUnicastCmd_SourceRedailReq(IpCaller_Obj.Caller_Run.t_addr);
	}
	#endif
}

/*------------------------------------------------------------------------
						呼叫申请
------------------------------------------------------------------------*/
void Callback_Caller_ToInvite_IxSys(CALLER_STRUCT *msg)	//R_
{
	unsigned char error_type;	//czn_20170411
	//will_add_czn
	/*uint8 SR_apply;
	SR_apply = API_SR_Request(CT08_DS_CALL_ST); 
						//链路忙,不可剥夺
	if (SR_apply == SR_DISABLE)
	{
		//will_add	//对小区主机_增加系统忙的应答
		return;
	}
	//链路忙,可剥夺
	else if (SR_apply == SR_ENABLE_DEPRIVED)	
	{
		API_SR_Deprived();	//复位系统链路状态字
	}
	//链路可直接使用
	else	 
	{	
		API_SR_Deprived();	//复位系统链路状态字	
	}*/
	//API_Stack_APT_Without_ACK(Caller_Run.target_addr&0x00ff,MAIN_CALL);
	//printf("___________________Caller_Run.tdev.ip%08x\n",Caller_Run.tdev.ipdev.ip);
	
	//_IP_Call_Link_Run_Stru partner_call_link;
	//Global_Addr_Stru gaddr;
	int i;


	for(i = 0;i < CallServer_Run.tdev_nums; i ++)
	{
		Send_InviteCmd_VtkUnicast_slave(CallServer_Run.target_dev_list[i]);
	}
	
	
}

/*------------------------------------------------------------------------
						呼叫成功,进入Ringing
------------------------------------------------------------------------*/
void Callback_Caller_ToRinging_IxSys(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	//will_add_czn
	/*if(SR_State.in_use == FALSE)
	{
		//链路创建
		//API_SR_CallerCreate();
		SR_Routing_Create(Business_Run.call_sub_type);
		
	}*/
	// lzh_20170328_s // 添加视频服务远程调试信息到DMR18s
	//char detail[LOG_DESC_LEN+1];
	//snprintf(detail,LOG_DESC_LEN+1,"[Dail](%03d,%04d) OK",Caller_Run.t_addr.gatewayid,Caller_Run.t_addr.rt*32+Caller_Run.t_addr.code);
	//API_add_log_item(0,"S_CSER",detail,NULL);
	// lzh_20170328_e
	
}

/*------------------------------------------------------------------------
						分机摘机,进入ACK
------------------------------------------------------------------------*/
void Callback_Caller_ToAck_IxSys(CALLER_STRUCT *msg)	//R_
{
	int i;
	
	Send_AckCmd_VtkUnicast(CallServer_Run.target_hook_dev);
	
	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		if(CallServer_Run.target_hook_dev.ip_addr!= CallServer_Run.target_dev_list[i].ip_addr)
		{
			SendRemoteCallLinkHoldReq(CallServer_Run.target_dev_list[i].ip_addr);	//czn_20190527
			
		}
	}
	
	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		if(CallServer_Run.target_hook_dev.ip_addr!= CallServer_Run.target_dev_list[i].ip_addr)
		{
			Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
		}
	}	
}

/*------------------------------------------------------------------------
						主机挂机,返回Waiting
------------------------------------------------------------------------*/

// lzh_20160127_s
#include "../../../video_service/video_object.h"
#include "../../../video_service/ip_camera_control/ip_camera_control.h"
#include "../../../video_service/ip_video_control/ip_video_control.h"
// lzh_20160127_e

void Callback_Caller_ToBye_IxSys(CALLER_STRUCT *msg)	//R_
{
	int i;
	
	IpCaller_Obj.Caller_Run.timer = 0;	//czn_20190117
	OS_StopTimer(IpCaller_Obj.timer_caller);
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
	
	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
		{
			Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
		}
	}	
	
	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		SendRemoteCallLinkFreeReq(CallServer_Run.target_dev_list[i].ip_addr);	//czn_20190527
	}
}

/*------------------------------------------------------------------------
				分机挂机(单元链路强制释放),返回Waitting
------------------------------------------------------------------------*/
void Callback_Caller_ToWaiting_IxSys(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	
	//Send_VtkUnicastCmd_StateNoticeAck(DsAndGl,&Caller_Run.t_addr,1);
	//Set_IPCallLink_Status(CLink_Idle);
	//Send_AckCmd_VtkUnicast(msg->t_addr);

	//API_talk_off();
	//API_linphonec_Close();
	int i;
	
	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
		if(CallServer_Run.target_hook_dev.ip_addr!= CallServer_Run.target_dev_list[i].ip_addr)
		{
			Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
		}
	}

	for(i = 0;i < CallServer_Run.tdev_nums;i ++)
	{
		SendRemoteCallLinkFreeReq(CallServer_Run.target_dev_list[i].ip_addr);	//czn_20190527
	}
}


/*------------------------------------------------------------------------
						开锁处理
------------------------------------------------------------------------*/
void Callback_Caller_ToUnlock_IxSys(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller呼叫超时处理(仅自身退出呼叫状态)						
------------------------------------------------------------------------*/
void Callback_Caller_ToTimeout_IxSys(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	int i;
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(IpCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			IpCaller_Obj.Caller_Run.timer = 0;
			OS_StopTimer(IpCaller_Obj.timer_caller);
			//czn_20170411_s
			unsigned char state_save;
			state_save = IpCaller_Obj.Caller_Run.state;
			IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
			//IX2_AD API_talk_off();

			Set_IPCallLink_Status(CLink_Idle);
			if(state_save == CALLER_INVITE)
			{
				API_CallServer_InviteFail(CallServer_Run.call_type);
			}
			else
 			{
 				//if(CallServer_Run.call_type == DxCallScene4)
 				{
					//Send_ByeCmd_VtkUnicast(IpCaller_Obj.Caller_Run.t_addr);
					for(i = 0;i < CallServer_Run.tdev_nums;i ++)
					{
						//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
						{
							Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
						}
						SendRemoteCallLinkFreeReq(CallServer_Run.target_dev_list[i].ip_addr);	//czn_20190527
					}	
					API_CallServer_Timeout(CallServer_Run.call_type);
				}
			}
		}
		//czn_20170411_e
	}
	#if 0
	else if(msg->msg_head.msg_sub_type == CALLER_TOUT_CHECKLINK)
	{		
		if((Caller_Run.state == CALLER_RINGING  || Caller_Run.state == CALLER_ACK) && Caller_Run.timer > 3)
		{
			Global_Addr_Stru gaddr;
			gaddr.gatewayid	= Caller_Run.t_addr.gatewayid;
			gaddr.ip			= Caller_Run.t_addr.ip;
			gaddr.rt			= 0;
			gaddr.code		= 0;
			if(API_Get_Partner_Calllink_NewCmd(DsAndGl,&gaddr,&partner_call_link) == -1)
			{
				if(++Caller_Run.checklink_error	 >= 2)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
			}
			else
			{
				Caller_Run.checklink_error = 0;
				
				if(partner_call_link.Status != CLink_AsCallServer && partner_call_link.Status != CLink_AsBeCalled)
				{
					quit_flag = 1;
					goto CTOUT_CHECKLINK_END;
				}
				
				if(partner_call_link.BeCalled_Data.Call_Source.ip != GetLocalIp() || 
					partner_call_link.BeCalled_Data.Call_Source.rt != Caller_Run.s_addr.rt||
					partner_call_link.BeCalled_Data.Call_Source.code != Caller_Run.s_addr.code)
				{
					quit_flag = 1;
				}
			}
	CTOUT_CHECKLINK_END:
			if(quit_flag == 1)
			{
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				Set_IPCallLink_Status(CLink_Idle);
				
				API_CallServer_IpCallerQuit();
			}
			
		}
		
	}
	#endif
	
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_Caller_ToError_IxSys(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//关闭定时
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_Caller_ToForceClose_IxSys(CALLER_STRUCT *msg)
{
	int i;
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		//if(msg->msg_type == CallServer_Msg_DtSrDisconnect)
		{
				for(i = 0;i < CallServer_Run.tdev_nums;i ++)
				{
					//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
					{
						Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
					}
				}	
		}
		
		IpCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//关闭定时
		IpCaller_Obj.Caller_Run.timer = 0;

		//Set_IPCallLink_Status(CLink_Idle);
		//IX2_AD API_talk_off();
	}
}
