#ifndef _obj_Caller_Transfer_Callback_H
#define _obj_Caller_Transfer_Callback_H

void Callback_Caller_ToRedial_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToInvite_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToRinging_Transfer(CALLER_STRUCT *msg)	;
void Callback_Caller_ToAck_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToBye_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToWaiting_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToUnlock_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToTimeout_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToError_Transfer(CALLER_STRUCT *msg);
void Callback_Caller_ToForceClose_Transfer(CALLER_STRUCT *msg);
#endif