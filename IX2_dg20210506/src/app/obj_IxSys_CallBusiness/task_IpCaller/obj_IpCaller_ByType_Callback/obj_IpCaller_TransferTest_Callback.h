#ifndef _obj_Caller_TransferTest_Callback_H
#define _obj_Caller_TransferTest_Callback_H

void Callback_Caller_ToRedial_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToInvite_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToRinging_TransferTest(CALLER_STRUCT *msg)	;
void Callback_Caller_ToAck_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToBye_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToWaiting_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToUnlock_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToTimeout_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToError_TransferTest(CALLER_STRUCT *msg);
void Callback_Caller_ToForceClose_TransferTest(CALLER_STRUCT *msg);

extern int SipTestCount,SipTestInterval,SipTestRetryTime,SipTestVideoQuality;

#endif