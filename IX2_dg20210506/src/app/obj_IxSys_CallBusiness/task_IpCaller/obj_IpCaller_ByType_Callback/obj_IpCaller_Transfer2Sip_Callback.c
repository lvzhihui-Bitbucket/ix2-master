/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */
#include "../../task_CallServer/task_CallServer.h"
#include "../../obj_Caller_State.h"
#include "../task_IpCaller.h"

// lzh_20170605_s
#include "../../obj_VtkLinphonecCommandInterface/linphone_interface.h"
// lzh_20170605_e

#include "obj_IpCaller_Transfer2Sip_Callback.h"
//#include "../../../device_manage/obj_device_register/obj_device_register.h"
#include "../../../task_debug_sbu/task_debug_sbu.h"
//#include "../../../wifi_service/task_WiFiConnect/task_WiFiConnect.h"		//czn_20181114
#include "elog_forcall.h"

void start_one_sip_req(void);
void recv_one_sip_rsp(void);
int get_sip_delay_stat(void);

extern int Transfer_reinvite_flag;

//static int divert_video_answer = 0;
extern int divert_redail;
extern int divert_reanwer;
extern int app_req_redail;

extern int divert_total,divert_ring,divert_hook,video_ok,divert_video_answer,divert_once_ring,video_once_ok;
extern int have_ring;
extern int have_hook;
extern int divert_log_index;

int Sip2SipCount,Sip2SipInterval,Sip2SipRetryTime,Sip2SipVideoQuality;

static void disp_divert_final(char *disp);
#if 0
void Set_Divert_Video_Flag(void)
{
	//divert_video_answer = 1;
	if(divert_video_answer == 0)
	{
		divert_video_answer = 1;
		video_ok++;
		if(divert_reanwer == 0)
		{
			video_once_ok++;
		}
		char detail[LOG_DESC_LEN+1];
		snprintf(detail,LOG_DESC_LEN+1,"T%04d^Video connected",divert_total);	
		API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	}
}
#endif
/*------------------------------------------------------------------------
						�ز�����
------------------------------------------------------------------------*/
void Callback_Caller_ToRedial_Transfer2Sip(CALLER_STRUCT *msg)
{
	char cmdbuf[40];
		//DivertCaller_Business_Rps(msg,0);
		app_req_redail = 1;
		API_linphonec_Close();
		usleep(500000);
		IpCaller_Obj.Caller_Run.state = CALLER_INVITE;
		IpCaller_Obj.Caller_Run.timer = 0;
		OS_RetriggerTimer(IpCaller_Obj.timer_caller);
		Get_SipConfig_DirvertAccount(cmdbuf);
		API_linphonec_Invite(cmdbuf);
		char detail[LOG_DESC_LEN+1];
		snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Req Redial",divert_log_index);	
		//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
}

/*------------------------------------------------------------------------
						��������
------------------------------------------------------------------------*/
void Callback_Caller_ToInvite_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	unsigned char error_type;	//czn_20170411
	//will_add_czn
	/*uint8 SR_apply;
	SR_apply = API_SR_Request(CT08_DS_CALL_ST); 
						//��·æ,���ɰ���
	if (SR_apply == SR_DISABLE)
	{
		//will_add	//��С������_����ϵͳæ��Ӧ��
		return;
	}
	//��·æ,�ɰ���
	else if (SR_apply == SR_ENABLE_DEPRIVED)	
	{
		API_SR_Deprived();	//��λϵͳ��·״̬��
	}
	//��·��ֱ��ʹ��
	else	 
	{	
		API_SR_Deprived();	//��λϵͳ��·״̬��	
	}*/
	//API_Stack_APT_Without_ACK(Caller_Run.target_addr&0x00ff,MAIN_CALL);
	//printf("___________________Caller_Run.tdev.ip%08x\n",Caller_Run.tdev.ipdev.ip);
	
	_IP_Call_Link_Run_Stru partner_call_link;
	Global_Addr_Stru gaddr;
	int i;
	
	
	//IpCaller_Respones(msg->msg_source_id,msg->msg_type,0);
	//return;
	//IX2_AD API_talk_off();		//czn_201704010

	#ifdef EN_SIP_LOG
	//API_ClearLogViewWin();
	
	divert_total++;
	
	if(divert_log_index++ >= 9999)
		divert_log_index = 0;
	
	divert_redail = 0;
	divert_reanwer = 0;
	app_req_redail = 0;
	have_ring = 0;
	have_hook = 0;
	divert_video_answer = 0;
	app_req_redail = 0;
	#endif
	
	Sip2SipCount = 1;
	Sip2SipInterval = 3;
	Sip2SipRetryTime = 12;
	Sip2SipVideoQuality = 1;
	
	Transfer_reinvite_flag = 0;

	char cmdbuf[40];
	Get_SipConfig_DirvertAccount(cmdbuf);
	printf("2222222222222222CallRule_TransferIm %s\n",cmdbuf);
	API_linphonec_update_vdout_status();
	usleep(200*1000);
	API_linphonec_Invite(cmdbuf);
	//log_i("start divert,acc:%s",cmdbuf);
	printf("1111111111111%s:%d\n",__func__,__LINE__);
}

/*------------------------------------------------------------------------
						���гɹ�,����Ringing
------------------------------------------------------------------------*/
void Callback_Caller_ToRinging_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	//will_add_czn
	/*if(SR_State.in_use == FALSE)
	{
		//��·����
		//API_SR_CallerCreate();
		SR_Routing_Create(Business_Run.call_sub_type);
		
	}*/
	// lzh_20170328_s // ������Ƶ����Զ�̵�����Ϣ��DMR18s
	//char detail[LOG_DESC_LEN+1];
	//snprintf(detail,LOG_DESC_LEN+1,"[Dail](%03d,%04d) OK",Caller_Run.t_addr.gatewayid,Caller_Run.t_addr.rt*32+Caller_Run.t_addr.code);
	//API_add_log_item(0,"S_CSER",detail,NULL);
	// lzh_20170328_e
	//API_OpenDxRtpVideo();
	if(app_req_redail == 0)
	{
		if(have_ring == 0)
		{
			divert_ring++;
			if(divert_redail == 0)
			{
				divert_once_ring++;
			}
			have_ring = 1;
		}
	}
	#ifdef EN_SIP_LOG
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Ring",divert_log_index);	
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	#endif
	//log_i("phone have ring");
	printf("1111111111111%s:%d\n",__func__,__LINE__);
}

/*------------------------------------------------------------------------
						�ֻ�ժ��,����ACK
------------------------------------------------------------------------*/
void Callback_Caller_ToAck_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	int i;
	if(app_req_redail == 0)
	{
		if(have_hook == 0)
		{
			divert_hook++;
			have_hook = 1;
		}

		if(have_ring == 0)
		{
			divert_ring++;
			if(divert_redail == 0)
			{
				divert_once_ring++;
			}
			have_ring = 1;
		}
	}
	#ifdef EN_SIP_LOG
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Answer",divert_log_index);	
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	#endif
	//log_i("phone have answer");
	IpCaller_Obj.Caller_Run.t_addr = msg->t_addr;
	sleep(2);
	API_linphonec_Answer();
	usleep(1000000);
	//IX2_AD LoadAudioCaptureVol();
	//IX2_AD LoadLinPhoneAudioPlaybackVol();//LoadAudioPlaybackVol();
	printf("1111111111111%s:%d\n",__func__,__LINE__);

}

/*------------------------------------------------------------------------
						�����һ�,����Waiting
------------------------------------------------------------------------*/

// lzh_20160127_s
#include "../../../video_service/video_object.h"
#include "../../../video_service/ip_camera_control/ip_camera_control.h"
#include "../../../video_service/ip_video_control/ip_video_control.h"
// lzh_20160127_e

void Callback_Caller_ToBye_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	
	//API_talk_off();

	API_linphonec_Close();
	API_VtkMediaTrans_StopJpgPush();

	IpCaller_Obj.Caller_Run.timer = 0;
	OS_StopTimer(IpCaller_Obj.timer_caller);
	
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
	#ifdef EN_SIP_LOG
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Source Cancel",divert_log_index);	
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	
	disp_divert_final(detail);
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	#endif
	printf("1111111111111%s:%d\n",__func__,__LINE__);
}

/*------------------------------------------------------------------------
				�ֻ��һ�(��Ԫ��·ǿ���ͷ�),����Waitting
------------------------------------------------------------------------*/
void Callback_Caller_ToWaiting_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	
	//Send_VtkUnicastCmd_StateNoticeAck(DsAndGl,&Caller_Run.t_addr,1);
	//Set_IPCallLink_Status(CLink_Idle);
	//Send_AckCmd_VtkUnicast(msg->t_addr);

	//API_talk_off();

	API_linphonec_Close();
	API_VtkMediaTrans_StopJpgPush();

	#ifdef EN_SIP_LOG
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Cancel",divert_log_index);	
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

	disp_divert_final(detail);
	//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
	#endif
	//log_i("divert end");
	printf("1111111111111%s:%d\n",__func__,__LINE__);
}


/*------------------------------------------------------------------------
						��������
------------------------------------------------------------------------*/
void Callback_Caller_ToUnlock_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller���г�ʱ����(�������˳�����״̬)						
------------------------------------------------------------------------*/
void Callback_Caller_ToTimeout_Transfer2Sip(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	char detail[LOG_DESC_LEN+1];
	
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(IpCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			
			//czn_20170411_s
			unsigned char state_save;
			state_save = IpCaller_Obj.Caller_Run.state;
			
			API_linphonec_Close();
			if(state_save == CALLER_INVITE)
			{
				#if 0
				if((Sip2SipRetryTime*divert_redail + IpCaller_Obj.Caller_Config.limit_invite_time) < 40)
				{
					Transfer_reinvite_flag = 1;
					char cmdbuf[40];
					Get_SipConfig_DirvertAccount(cmdbuf);
					API_linphonec_Invite(cmdbuf);
					API_linphonec_Close();
					usleep(500000);
					API_linphonec_Invite(cmdbuf);
					IpCaller_Obj.Caller_Run.state = CALLER_INVITE;
					IpCaller_Obj.Caller_Run.timer = 0;
					if((40-Sip2SipRetryTime*(divert_redail+2)) > Sip2SipRetryTime)
					{
						IpCaller_Obj.Caller_Config.limit_invite_time = Sip2SipRetryTime;
					}
					else
					{
						IpCaller_Obj.Caller_Config.limit_invite_time = 40-Sip2SipRetryTime*(divert_redail+1);
					}
					OS_RetriggerTimer(IpCaller_Obj.timer_caller);
					#ifdef EN_SIP_LOG
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^ReInvite %d",divert_log_index,++divert_redail);	
					//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
					#endif
				}
				else
				#endif
				{
					IpCaller_Obj.Caller_Run.timer = 0;
					OS_StopTimer(IpCaller_Obj.timer_caller);
					
					IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
					API_talk_off();

					Set_IPCallLink_Status(CLink_Idle);
				
					API_CallServer_InviteFail(CallServer_Run.call_type);
					#ifdef EN_SIP_LOG
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Not found",divert_log_index);	
					//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);

					disp_divert_final(detail);
					//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
					#endif
					//log_w("invite phone timeout");
					printf("1111111111111%s:%d\n",__func__,__LINE__);
				}
			}
			else
			{
				if(IpCaller_Obj.Caller_Run.state == CALLER_RINGING)
				{
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target No answer",divert_log_index);	
					//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}
				else if(IpCaller_Obj.Caller_Run.state == CALLER_ACK)
				{
					snprintf(detail,LOG_DESC_LEN+1,"T%04d^End: talkover",divert_log_index);	
					//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				}
				#ifdef EN_SIP_LOG
				disp_divert_final(detail);
				//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				#endif
				IpCaller_Obj.Caller_Run.timer = 0;
				OS_StopTimer(IpCaller_Obj.timer_caller);
				
				IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				Set_IPCallLink_Status(CLink_Idle);
				//API_CallServer_IpCallerQuit();
				API_CallServer_Timeout(CallServer_Run.call_type);
				printf("1111111111111%s:%d\n",__func__,__LINE__);
			}
		}
		//czn_20170411_e
	}
	else if(msg->msg_sub_type == CALLER_TOUT_CHECKLINK)
	{	
		if(IpCaller_Obj.Caller_Run.state == CALLER_INVITE)
			API_OpenDxRtpVideo();
		if(divert_video_answer == 0&&IpCaller_Obj.Caller_Run.state == CALLER_ACK && divert_reanwer<3)
		{
			if((IpCaller_Obj.Caller_Run.timer>0) && (IpCaller_Obj.Caller_Run.timer%6) == 0)
			{
				//char detail[LOG_DESC_LEN+1];
				//snprintf(detail,LOG_DESC_LEN+1,"[Divert]ReAnswer");	
				//API_add_log_item(0,"S_CSER",detail,NULL);
				API_linphonec_Answer();
				#ifdef EN_SIP_LOG
				snprintf(detail,LOG_DESC_LEN+1,"T%04d^ReAnswer%d",divert_log_index,++divert_reanwer);	
				//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				#endif
				printf("1111111111111%s:%d\n",__func__,__LINE__);
			}
		}
	}
}

/*------------------------------------------------------------------------
				���յ���Ϣ(): ������					
------------------------------------------------------------------------*/
void Callback_Caller_ToError_Transfer2Sip(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//�رն�ʱ
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_Caller_ToForceClose_Transfer2Sip(CALLER_STRUCT *msg)
{
	
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		
		IpCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//�رն�ʱ
		IpCaller_Obj.Caller_Run.timer = 0;

		//Set_IPCallLink_Status(CLink_Idle);
		 API_talk_off();
		
		// lzh_20170605_s
		API_linphonec_Close();
		// lzh_20170605_e
		
	}
}


