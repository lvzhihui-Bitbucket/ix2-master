/**
  ******************************************************************************
  * @file    task_Caller.h
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_IpCaller_H
#define _task_IpCaller_H

//#include "task_Survey.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"

typedef enum
{
	IpCaller_IxSys,	
	IpCaller_Transfer,
	IpCaller_TransferTest,
  // lzh_20210616_s
	IpCaller_Transfer2Sip,
  // lzh_20210616_e
}IpCaller_CallType_t;

extern OBJ_CALLER_STRU IpCaller_Obj;

void init_vdp_ipcaller_task(void);
uint8 Get_IpCaller_State(void);	

int API_IpCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr);
void IpCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);

#define API_IpCaller_Invite(call_type,t_addr)		API_IpCaller_Common(CALLER_MSG_INVITE,call_type,NULL,t_addr)
#define API_IpCaller_Ring(call_type,t_addr)		API_IpCaller_Common(CALLER_MSG_RINGING,call_type,NULL,t_addr)
#define API_IpCaller_Ack(call_type,t_addr)		API_IpCaller_Common(CALLER_MSG_ACK,call_type,NULL,t_addr)
#define API_IpCaller_Cancel(call_type)			API_IpCaller_Common(CALLER_MSG_CANCEL,call_type,NULL,NULL)
#define API_IpCaller_Bye(call_type,t_addr)				API_IpCaller_Common(CALLER_MSG_BYE,call_type,NULL,t_addr)
#define API_IpCaller_ForceClose()					API_IpCaller_Common(CALLER_MSG_FORCECLOSE,0,NULL,NULL)
#define API_IpCaller_Unlock1(call_type,t_addr)	API_IpCaller_Common(CALLER_MSG_UNLOCK1,call_type,NULL,t_addr)
#define API_IpCaller_Unlock2(call_type,t_addr)	API_IpCaller_Common(CALLER_MSG_UNLOCK2,call_type,NULL,t_addr)
#define API_IpCaller_Redail(call_type)				API_IpCaller_Common(CALLER_MSG_REDIAL,call_type,NULL,NULL)		//czn_20171030
#endif
		