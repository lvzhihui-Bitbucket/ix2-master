/**
  ******************************************************************************
  * @file    task_Called.c
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>



#include "obj_Caller_State.h"
#include "task_CallServer.h"
#include "task_IpCaller.h"
//#include "./obj_IpCaller_ByType_Callback/obj_IpCaller_MainCall_Callback.h"
#include "obj_IpCaller_IxSys_Callback.h"
#include "obj_IpCaller_Transfer_Callback.h"
#include "obj_IpCaller_TransferTest_Callback.h"
// lzh_20210616_s
#include "obj_IpCaller_Transfer2Sip_Callback.h"
// lzh_20210616_e

#include "OSTIME.h"

OS_TIMER timer_ipcaller;

OBJ_CALLER_STRU IpCaller_Obj;

Loop_vdp_common_buffer	vdp_ipcaller_mesg_queue;
Loop_vdp_common_buffer	vdp_ipcaller_sync_queue;


vdp_task_t	task_ipcaller;
void* vdp_ipcaller_task( void* arg );

void IpCaller_Config_Data_Init(void);
void IpCaller_MenuDisplay_ToInvite(void);
void IpCaller_MenuDisplay_ToRing(void);
void IpCaller_MenuDisplay_ToAck(void);
void IpCaller_MenuDisplay_ToBye(void);
void IpCaller_MenuDisplay_ToWait(void);
void IpCaller_Timer_Callback(void);

#define IpCaller_Support_CallTypeNum	3


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOINVITE_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	{IpCaller_IxSys, 			Callback_Caller_ToInvite_IxSys,},
	//{IpCaller_Transfer, 				Callback_Caller_ToInvite_Transfer,},
	{IpCaller_TransferTest, 			Callback_Caller_ToInvite_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 			Callback_Caller_ToInvite_Transfer2Sip,},
	// lzh_20210616_e
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TORINGING_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 				Callback_Caller_ToRinging_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 			Callback_Caller_ToRinging_IxSys,},
	//{IpCaller_Transfer, 			Callback_Caller_ToRinging_Transfer,},
	{IpCaller_TransferTest, 			Callback_Caller_ToRinging_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 			Callback_Caller_ToRinging_Transfer2Sip,},
	// lzh_20210616_e
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOACK_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 			Callback_Caller_ToAck_MainCall,},			//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToAck_IxSys,},
	//{IpCaller_Transfer, 		Callback_Caller_ToAck_Transfer,},
	{IpCaller_TransferTest, 		Callback_Caller_ToAck_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 		Callback_Caller_ToAck_Transfer2Sip,},
	// lzh_20210616_e	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOBYE_CALLBACK[]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 			Callback_Caller_ToBye_MainCall,},			//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToBye_IxSys,},
	//{IpCaller_Transfer, 		Callback_Caller_ToBye_Transfer,},
	{IpCaller_TransferTest, 		Callback_Caller_ToBye_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 		Callback_Caller_ToBye_Transfer2Sip,},
	// lzh_20210616_e		
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOWAITING_CALLBACK[]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToWaiting_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToWaiting_IxSys,},	
	//{IpCaller_Transfer, 	Callback_Caller_ToWaiting_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToWaiting_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToWaiting_Transfer2Sip,},
	// lzh_20210616_e			
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOUNLOCK_CALLBACK[]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 		Callback_Caller_ToUnlock_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToUnlock_IxSys,},
	//{IpCaller_Transfer, 	Callback_Caller_ToUnlock_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToUnlock_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToUnlock_Transfer2Sip,},
	// lzh_20210616_e				
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOTIMEOUT_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToTimeout_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToTimeout_IxSys,},
	//{IpCaller_Transfer, 	Callback_Caller_ToTimeout_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToTimeout_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToTimeout_Transfer2Sip,},
	// lzh_20210616_e				
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOERROR_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToError_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToError_IxSys,},
	//{IpCaller_Transfer, 	Callback_Caller_ToError_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToError_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToError_Transfer2Sip,},
	// lzh_20210616_e				
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOFORCECLOSE_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToForceClose_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToForceClose_IxSys,},
	//{IpCaller_Transfer, 	Callback_Caller_ToForceClose_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToForceClose_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToForceClose_Transfer2Sip,},
	// lzh_20210616_e					
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOREDAIL[IpCaller_Support_CallTypeNum]={		//czn_20171030
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToRedial_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 	Callback_Caller_ToRedial_IxSys,},
	//{IpCaller_Transfer, 	Callback_Caller_ToRedial_Transfer,},
	{IpCaller_TransferTest, 	Callback_Caller_ToRedial_TransferTest,},
	// lzh_20210616_s
	{IpCaller_Transfer2Sip, 	Callback_Caller_ToRedial_Transfer2Sip,},
	// lzh_20210616_e						
};
/*------------------------------------------------------------------------
			Caller_Task_Init
------------------------------------------------------------------------*/
void IpCaller_Obj_Init(void)	//R_
{
	//任务初始化
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
	IpCaller_Obj.timer_caller = &timer_ipcaller;
	IpCaller_Obj.task_caller = &task_ipcaller;
	
	IpCaller_Obj.Config_Data_Init = IpCaller_Config_Data_Init;

	IpCaller_Obj.to_invite_callback = TABLE_CALLTYPE_IPCALLER_TOINVITE_CALLBACK;
	IpCaller_Obj.to_ring_callback = TABLE_CALLTYPE_IPCALLER_TORINGING_CALLBACK; 
	IpCaller_Obj.to_ack_callback= TABLE_CALLTYPE_IPCALLER_TOACK_CALLBACK;
	IpCaller_Obj.to_bye_callback= TABLE_CALLTYPE_IPCALLER_TOBYE_CALLBACK;
	IpCaller_Obj.to_wait_callback= TABLE_CALLTYPE_IPCALLER_TOWAITING_CALLBACK;
	IpCaller_Obj.to_unclock_callback= TABLE_CALLTYPE_IPCALLER_TOUNLOCK_CALLBACK;
	IpCaller_Obj.to_timeout_callback= TABLE_CALLTYPE_IPCALLER_TOTIMEOUT_CALLBACK;
	IpCaller_Obj.to_error_callback= TABLE_CALLTYPE_IPCALLER_TOERROR_CALLBACK;
	IpCaller_Obj.to_forceclose_callback= TABLE_CALLTYPE_IPCALLER_TOFORCECLOSE_CALLBACK;
	IpCaller_Obj.to_redail_callback = TABLE_CALLTYPE_IPCALLER_TOREDAIL;		//czn_20171030

	IpCaller_Obj.support_calltype_num = IpCaller_Support_CallTypeNum;
	IpCaller_Obj.MenuDisplay_ToInvite = IpCaller_MenuDisplay_ToInvite;
	IpCaller_Obj.MenuDisplay_ToRing= IpCaller_MenuDisplay_ToRing;
	IpCaller_Obj.MenuDisplay_ToAck= IpCaller_MenuDisplay_ToAck;
	IpCaller_Obj.MenuDisplay_ToBye= IpCaller_MenuDisplay_ToBye;
	IpCaller_Obj.MenuDisplay_ToWait= IpCaller_MenuDisplay_ToWait;

	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_ipcaller, IpCaller_Timer_Callback, 1000/25);
}



/*------------------------------------------------------------------------
			Get_Caller_State
返回:
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_IpCaller_State(void)	//R
{
	if (IpCaller_Obj.Caller_Run.state==CALLER_WAITING)
	{
		return (0);
	}
	return (1);
}


/*------------------------------------------------------------------------
			Get_Caller_PartnerAddr
------------------------------------------------------------------------*/
//uint16 Get_Caller_PartnerAddr(void)	//R
//{
//	return (Caller_Run.partner_IA);
//}





/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void ipcaller_mesg_data_process(void *Msg,int len )	//R_
{
	bprintf("!!!ipcaller state = %d,call_type = %d,recv msg = %d\n",IpCaller_Obj.Caller_Run.state,((CALLER_STRUCT *)Msg)->call_type,((CALLER_STRUCT *)Msg)->msg_type);
	vtk_TaskProcessEvent_Caller(&IpCaller_Obj,(CALLER_STRUCT *)Msg);
	
}

//OneCallType	OneMyCallObject;

void init_vdp_ipcaller_task(void)
{
	init_vdp_common_queue(&vdp_ipcaller_mesg_queue, 1000, (msg_process)ipcaller_mesg_data_process, &task_ipcaller);
	init_vdp_common_queue(&vdp_ipcaller_sync_queue, 100, NULL, 								  &task_ipcaller);
	init_vdp_common_task(&task_ipcaller, MSG_ID_IpCalller, vdp_ipcaller_task, &vdp_ipcaller_mesg_queue, &vdp_ipcaller_sync_queue);
}

void exit_ipcaller_task(void)
{
	
}

void* vdp_ipcaller_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	IpCaller_Obj_Init();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void IpCaller_Timer_Callback(void)
{
	Caller_Timer_Callback(&IpCaller_Obj);
}

void IpCaller_Config_Data_Init(void)
{
	switch(IpCaller_Obj.Caller_Run.call_type)
	{
		case IpCaller_IxSys:
			IpCaller_Obj.Caller_Config.limit_invite_time = 5;
			IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
			IpCaller_Obj.Caller_Config.limit_ack_time = 90;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
		case IpCaller_Transfer:		//czn_20181114
			IpCaller_Obj.Caller_Config.limit_invite_time = 12;
			IpCaller_Obj.Caller_Config.limit_ringing_time = 30;
			IpCaller_Obj.Caller_Config.limit_ack_time = 180;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;

		case IpCaller_TransferTest:
			//IpCaller_Obj.Caller_Config.limit_invite_time = SipTestRetryTime;
			IpCaller_Obj.Caller_Config.limit_invite_time = 12;
			IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
			IpCaller_Obj.Caller_Config.limit_ack_time = 90;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;	

		// lzh_20210616_s
		case IpCaller_Transfer2Sip:
			IpCaller_Obj.Caller_Config.limit_invite_time = 30;
			IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
			IpCaller_Obj.Caller_Config.limit_ack_time = 90;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;	
		// lzh_20210616_e	
		
	}
}
void IpCaller_MenuDisplay_ToInvite(void)
{
#if 0
	extern Global_Addr_Stru Register_DxIm_List[];
	Set_IPCallLink_Status(CLink_AsCaller);
	Register_DxIm_List[0].ip = GatewayId_Trs2_IpAddr(Register_DxIm_List[0].gatewayid);
	Set_IPCallLink_CallerData(&Register_DxIm_List[0]);
#endif
}

void IpCaller_MenuDisplay_ToRing(void)
{
}

void IpCaller_MenuDisplay_ToAck(void)
{
	//Set_IPCallLink_CallerData(&CallServer_Run.t_addr);
}
void IpCaller_MenuDisplay_ToBye(void)
{
	//Set_IPCallLink_Status(CLink_Idle);
}
void IpCaller_MenuDisplay_ToWait(void)
{
	//Set_IPCallLink_Status(CLink_Idle);
}

int API_IpCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	CALLER_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_ipcaller.p_msg_buf, (char *)&send_msg, sizeof(CALLER_STRUCT)) != 0)
	{
		return -1;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void IpCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}



///////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

Global_Addr_Stru Register_DxIm_List[10] = 
{
		/*{
			100,
			0,
			30,
			1,
		},
		{
			108,
			0,
			30,
			1,
		},	
		{
			107,
			0,
			30,
			1,
		},	
		{
			106,
			0,
			30,
			1,
		},
		{
			105,
			0,
			30,
			1,
		},	
		{
			104,
			0,
			30,
			1,
		},
		{
			103,
			0,
			30,
			1,
		},
		{
			102,
			0,
			30,
			1,
		},	*/
		{
			201,
			0x65f3a8c0,
			30,
			1,
		},	
};

int Register_DxIm_Num = 0;//sizeof(Register_DxIm_List)/sizeof(Register_DxIm_List[0]);
Global_Addr_Stru Register_LocalPhone_List[10];
int Register_LocalPhone_Num = 0;
#if 0
void DevRegInfo_Trs2_GlobalAddr(int actmode)
{
	int i;
	DevReg_Info dev_info;
	int devreg_num;
	
	Register_DxIm_Num = 0;
	Register_LocalPhone_Num = 0;
	
	if(actmode == 1)//slavemode
	{
		if(Api_DxSlave_Get_MasterInfo(&dev_info) == 0)
		{
			Register_DxIm_List[Register_DxIm_Num].gatewayid = dev_info.node_id;
			Register_DxIm_List[Register_DxIm_Num].ip = dev_info.ip_addr;
			Register_DxIm_List[Register_DxIm_Num].rt = 30;
			Register_DxIm_List[Register_DxIm_Num].code = 0;
			Register_DxIm_Num++;
		}
	}
	devreg_num = Api_Get_RegDxDevNum();
	for(i = 0;i < devreg_num;i ++)
	{
		if(Api_Get_OneRegDxDevItem(i,&dev_info) == 0)
		{
			if(strstr(dev_info.dev_type,"Phone") != NULL)
			{
				Register_LocalPhone_List[Register_LocalPhone_Num].gatewayid = dev_info.node_id;
				Register_LocalPhone_List[Register_LocalPhone_Num].ip = dev_info.ip_addr;
				Register_LocalPhone_List[Register_LocalPhone_Num].rt = 31;
				Register_LocalPhone_List[Register_LocalPhone_Num].code = 0;
				Register_LocalPhone_Num++;
			}
			else
			{
				//Register_DxIm_List[i].gatewayid = (dev_info.ip_addr>>24);//
				if(actmode == 1)
				{
					if(dev_info.ip_addr == GetLocalIp())
						continue;
				}
				Register_DxIm_List[Register_DxIm_Num].gatewayid = dev_info.node_id;
				Register_DxIm_List[Register_DxIm_Num].ip = dev_info.ip_addr;
				Register_DxIm_List[Register_DxIm_Num].rt = 30;
				Register_DxIm_List[Register_DxIm_Num].code = 1;
				Register_DxIm_Num++;
			}
		}
		else
		{
			//Register_DxIm_Num = i;
			break;
		}
	}
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

