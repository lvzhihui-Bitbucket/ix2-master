#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "task_Led.h"
#include "vdp_uart.h"
#include "task_Ring.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#include "task_monitor.h"

#include "obj_IxCallScene1_Passive_process.h"
#include "obj_VideoProxySetting.h"
#include "obj_GetIpByNumber.h"
#include "task_Event.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;


void IxCallScene1_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeMainCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}
					
					//Load_CallRule_Para();
					// lzh_20180812_s					
					Load_CallRule_Para();
					// lzh_20180812_e					
													
					CallServer_Run.state = CallServer_Invite;	
					CallServer_Run.call_type = Msg_CallServer->call_type;
					//sleep(1);
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.source_dev = Msg_CallServer->source_dev;
					CallServer_Run.rule_act = 0;		//czn_20190116
					Get_SelfDevInfo(Msg_CallServer->source_dev.ip_addr, &CallServer_Run.target_hook_dev); 
					
					CallServer_Run.timer = time(NULL);
					
					API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);					
					
					// �ֻ�������Ϊ����ת����������
					if( CallServer_Run.call_rule != CallRule_TransferIm )
					{
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_Passive_MenuDisplay_ToRing();
					}
					else
					{
						CallServer_Run.rule_act = 1;
						//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
						//API_IpBeCalled_ForceClose();
						//CallServer_Run.state = CallServer_Transfer;	
						//IxCallScene1_Passive_MenuDisplay_ToTransfer();
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_Passive_MenuDisplay_ToRing();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene1_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
}

void IxCallScene1_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	int rate;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != IxCallScene1_Passive)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene1_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RingNoAck:
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_IpBeCalled_Ack(IpBeCalled_IxSys);
					
					IxCallScene1_Passive_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_DtSlaveAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					if(CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpBeCalled_Cancel(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					
					break;

				case CallServer_Msg_RemoteUnlock2:
					
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;

				case CallServer_Msg_Transfer:
					//printf("!!!!!!!!!!!!recv CallServer_Msg_Transfer msg\n\n\n");
					//API_IpBeCalled_ForceClose();
					CallServer_Run.rule_act = 1;
					//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					CallServer_Run.state = CallServer_Transfer;	
					IxCallScene1_Passive_MenuDisplay_ToTransfer();
					break;
				case CallServer_Msg_CheckVideo:
					
					if((rate=API_PublicInfo_Read_Int("VC_CH0_Bandwind"))<10&&get_video_client_rate(0)<100)
					{
						API_Remote_Event_NameAndMsg(CallServer_Run.source_dev.ip_addr, Event_VideoSerError, "CLIENT_RECV_ERR:%s",GetSysVerInfo_IP());
						Api_Ds_Show_Stop2(0);
						printf("1111111%s,%d\n",__func__,rate);
						usleep(1500*1000);
						if(open_dsmonitor_client(0,CallServer_Run.source_dev.ip_addr,REASON_CODE_CALL,150,Resolution_720P,0) == 0)
						{
							SetVideoProxyDsShowPos();
						}
					}
					else
					{
						printf("22222222%s,%d\n",__func__,rate);
					}
					break;	
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene1_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpBeCalled_Bye(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					if(CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpBeCalled_Cancel(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);	//czn_20191123
					break;

				case CallServer_Msg_LocalUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
	
}
void IxCallScene1_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene1_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipbecalled_type=0;
	switch(Msg_CallServer->msg_type)//czn_20190116
	{
		case CallServer_Msg_LocalBye:
			CallServer_Run.state = CallServer_TargetBye;
			//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
			API_IpBeCalled_Bye(IpBeCalled_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_RemoteBye:
			API_IpBeCalled_Cancel(IpBeCalled_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_Timeout:
			//API_DtBeCalled_ForceClose();
			//API_IpCaller_ForceClose();
			API_IpBeCalled_Cancel(IpBeCalled_IxSys);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;
	}
	#if 0
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene1_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					#ifdef DivertErrRecoverNormal
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						//dtbecalled_type = DtBeCalled_MainCall;
						//ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					else
					{
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						IxCallScene1_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						{
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);

					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	#endif
	
}


void IxCallScene1_Passive_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}

void GetIxVideoIpAddress(VIDEO_PROXY_JSON objJson)
{
	char address[11];
	int i;
	
	for(i = 0; i < 10 && objJson.ixDevice.addr[i]; i++)
	{
		address[i] = toupper(objJson.ixDevice.addr[i]);
	}
	address[i] = 0;

	//��IX�豸��Ƶ
	if(!strcmp(address, "NONE"))
	{
		CallServer_Run.ixVideoIp = 0;
	}
	//������е��豸�ṩ��Ƶ
	else if(address[0] == 0 || !strcmp(address, "MYSELF"))
	{
		CallServer_Run.ixVideoIp = CallServer_Run.source_dev.ip_addr;
	}
	//����IX�豸��Ƶ
	else
	{
		GetIpRspData data;
		if(API_GetIpNumberFromNet(objJson.ixDevice.addr, NULL, NULL, 2, 1, &data) == 0)
		{
			CallServer_Run.ixVideoIp = data.Ip[0];
		}
		else
		{
			CallServer_Run.ixVideoIp = 0;
		}
	}

}

void IxCallScene1_Passive_MenuDisplay_ToRing(void)//czn_20190127
{
	//char disp_name[21] = {0};
	//char para_buff[21];
	char rm_nbr[11]={0};
	char tempData[42]={0};
	int max_list,i;
	char *ch;
	char paraString[500]={0};
	VIDEO_PROXY_JSON objJson;
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			memo_vd_playback_stop();
			SetPipShowPos();
			CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IX_DEVICE;
			#if 0
			API_io_server_UDP_to_read_one_remote(CallServer_Run.source_dev.ip_addr, VIDEO_PROXY_SET, paraString);
			printf("-IxCallScene1_Passive--- paraString=%s\n", paraString);
			ParseVideoProxyObject(paraString, &objJson);
			if(!strcmp(objJson.type, "IPC"))
			{
				CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IPC;
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
				else
				{
					SetVideoProxyShow(objJson.ipcDevice.IP, objJson.ipcDevice.NAME, objJson.ipcDevice.USER, objJson.ipcDevice.PWD);
				}
			}
			else if(!strcmp(objJson.type, "IX"))
			{
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
			}

			GetIxVideoIpAddress(objJson);
			if(CallServer_Run.ixVideoIp != 0)
			{
				vd_printf("CallServer_Run.ixVideoIp = %0x08x\n", CallServer_Run.ixVideoIp);	
				if(open_dsmonitor_client(0,CallServer_Run.ixVideoIp,REASON_CODE_CALL,150,Resolution_720P,0) == 0)
				{
					SetVideoProxyDsShowPos();
				}
			}
			#endif
			//open_dsmonitor_client(0,,REASON_CODE_CALL,150,Resolution_720P,0);
			PrintCurrentTime(20000+__LINE__);
			if(open_dsmonitor_client(0,CallServer_Run.source_dev.ip_addr,REASON_CODE_CALL,150,Resolution_720P,0) == 0)
				{
					PrintCurrentTime(__LINE__);
					SetVideoProxyDsShowPos();
					PrintCurrentTime(__LINE__);
				}
			PrintCurrentTime(20000+__LINE__);
			API_talk_off();
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
			//API_VideoTurnOn();
			API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.source_dev));		//
			API_POWER_EXT_RING_ON();		//czn_20170809
			//#if !defined(PID_IXSE)
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			//#endif
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			#if 0
			if((tempData[0] = strlen(CallServer_Run.source_dev.name))>0)
			{
				memcpy(&tempData[1],CallServer_Run.source_dev.name,tempData[0]);
				strcpy(call_record_temp.name,CallServer_Run.source_dev.name);
			}
			else
			{
				tempData[0] = strlen(CallServer_Run.source_dev.bd_rm_ms);
				memcpy(&tempData[1],CallServer_Run.source_dev.bd_rm_ms,tempData[0]);
			}
			#endif
			
			/*
			strcpy(rm_nbr,CallServer_Run.source_dev.bd_rm_ms);
			
			ch = tempData+1;
			if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(rm_nbr, "0099", 4))
			{
				sprintf(ch,"DS%d",atol(rm_nbr+8));
				//strcpy(target_dev[i].name,record[i].BD_RM_MS+4);
			}
			else
			{
				//strcpy(target_dev[i].name,record[i].BD_RM_MS);
				strcpy(ch,"DS");
				memcpy(ch+2,rm_nbr,4);
				sprintf(ch+strlen(ch),"(%d)",atol(rm_nbr+8));
			}
			
			if(strlen(CallServer_Run.source_dev.name)>=1&&strcmp(CallServer_Run.source_dev.name, "-"))
			{
				strcat(ch," ");
				strcat(ch,CallServer_Run.source_dev.name);
			}
			*/
			
			ch = tempData+1;
			get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, ch);
			snprintf(call_record_temp.name,20,"%s",ch);
			//strcpy(call_record_temp.name,ch);
			
			tempData[0] = strlen(ch);
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			
			usleep(2000*1000);
			PrintCurrentTime(20000+__LINE__);
			if (CallingRecordProcess(tempData) == 0)
			{
				strncpy(call_record_temp.relation,tempData,40);
			}
			#if 0
			if(memo_video_record_start("BeCalled") == 0)
			{
				strncpy(call_record_temp.relation,one_vd_record.filename,40);
			}
			#endif
			break;
			
	
		default:
			break;
	}
		
	
}

void IxCallScene1_Passive_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//usleep(100*1000);
			int try_cnt=0;
			while(try_cnt++<100&&RingGetState()!=0)
			{
				usleep(50*1000);
			}
			API_talk_on_by_unicast(CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			
			API_Event_By_Name("EventBECallTalk");
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledTalkOn);
			call_record_temp.property	= NORMAL;
			break;
	
		default:
			break;
	}
	
}

void IxCallScene1_Passive_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			//close_monitor_client();
			Api_Ds_Show_Stop2(0);//close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			API_Event_By_Name("EventBECallBye");
			break;
	
		default:
			break;
	}
	
	//#if !defined(PID_IXSE)
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
	#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
	getRecFileProcess();
	#endif

}

void IxCallScene1_Passive_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			//close_monitor_client();
			Api_Ds_Show_Stop2(0);//close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			API_Event_By_Name("EventBECallBye");
			API_AutoTest_BecalledCancel();		//czn_20190412
			break;
	
		default:
			break;
	}

	
	//#if !defined(PID_IXSE)
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );	
	}
	#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
	getRecFileProcess();
	#endif

}

void IxCallScene1_Passive_MenuDisplay_ToTransfer(void)
{	
	//char disp_name[21] = {0};
	char tempData[42]={0};
	char *ch;
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:	
			if(CallServer_Run.call_rule == CallRule_TransferIm)
			{
				API_LedDisplay_CallDivert();
				//IX2_TEST API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//API_VideoTurnOn();
				//IX2_TEST API_RingStop();
				//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
				//#if !defined(PID_IXSE)
				call_record_temp.type 				= CALL_RECORD;
				call_record_temp.subType 			= IN_COMING;
				call_record_temp.property				= MISSED;
				call_record_temp.target_node			=0;// CallServer_Run.s_addr.gatewayid;		//czn_20170329
				call_record_temp.target_id			=0;// CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
				strcpy(call_record_temp.name,"---");
				strcpy(call_record_temp.input,"---");
				strcpy(call_record_temp.relation, "-");
				call_record_flag = 1;
				//#endif
				//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
				{
				//	strcpy(call_record_temp.name,disp_name);
				}
				ch = tempData+1;
				get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, ch);
				snprintf(call_record_temp.name,20,"%s",ch);
				//strcpy(call_record_temp.name,ch);

				tempData[0] = strlen(ch);
				//IX2_TEST API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			}
			else
			{
				API_LedDisplay_CallDivert();
				//IX2_TEST API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//IX2_TEST API_VideoTurnOff();
				//IX2_TEST API_RingStop();
				//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			}
			break;

		default:
			break;
	}
}

