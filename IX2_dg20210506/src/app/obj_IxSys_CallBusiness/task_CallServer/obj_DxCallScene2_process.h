#ifndef _obj_DxCallScene2_process_h
#define _obj_DxCallScene2_process_h


void DxCallScene2_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene2_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallScene2_MenuDisplay_ToInvite(void);
void DxCallScene2_MenuDisplay_ToRing(void);
void DxCallScene2_MenuDisplay_ToAck(void);
void DxCallScene2_MenuDisplay_ToBye(void);
void DxCallScene2_MenuDisplay_ToWait(void);
void DxCallScene2_MenuDisplay_ToTransfer(void);


#endif
