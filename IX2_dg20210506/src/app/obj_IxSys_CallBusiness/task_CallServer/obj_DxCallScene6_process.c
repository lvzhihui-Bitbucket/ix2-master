#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "vdp_uart.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"

#include "obj_DxCallScene6_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

void DxCallScene6_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			dtbecalled_type = DtBeCalled_InnerCall;
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					Load_CallRule_Para();
					if(CallServer_Run.call_rule == CallRule_NoDisturb)
					{
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					API_Business_Request(Business_State_BeInnerCall);
					if(Get_DtBeCalled_State() != 0)
					{
						API_DtBeCalled_ForceClose();
					}
					
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
					//will_add CallServer_Run.t_addr = local_addr;
					CallServer_Run.timer = time(NULL);
					API_DtBeCalled_Invite(dtbecalled_type);
					
					CallServer_Run.state = CallServer_Ring;
					DxCallScene6_MenuDisplay_ToRing();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				default:
					break;
			}
			break;
			
		
	
		default:
			break;
	}
		
	
	
}

void DxCallScene6_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
			
		case DxCallScene6_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
	
		default:
			break;
	}
}

void DxCallScene6_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			dtbecalled_type = DtBeCalled_InnerCall;
			
			if(CallServer_Run.call_type != DxCallScene6_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					#if 0
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene6_MenuDisplay_ToWait();
					}
					#endif
					
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						if(CallServer_Run.s_dt_addr== Msg_CallServer->partner_dt_addr)
						{
							if(!RingGetState())
							{
								API_RingPlay(RING_InnerCall);
								API_POWER_EXT_RING_ON();		//czn_20170809
							}
						}
						else
						{
							CallServer_Run.state = CallServer_Wait;
							DxCallScene2_MenuDisplay_ToWait();
						}
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene2_MenuDisplay_ToWait();
					}
					else
					{
						if(CallServer_Run.s_dt_addr== Msg_CallServer->partner_dt_addr)
						{
							//CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type);
						}
					}
					
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_DtBeCalled_Ack(dtbecalled_type);

					DxCallScene6_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					#if 0
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					
					CallServer_Run.timer = time(NULL);
					
					API_DtBeCalled_Ack(dtbecalled_type);
					DxCallScene6_MenuDisplay_ToAck();
					break;
					#endif
				case CallServer_Msg_DtSlaveAck:
					//CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_DtBeCalled_Cancel(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					API_DtBeCalled_Bye(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						
						bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
						CallServer_Run.state = CallServer_Wait;
					}
					
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;


				default:
					break;
			}
			break;
			
		
	
		default:
			break;
	}
	
}

void DxCallScene6_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			dtbecalled_type = DtBeCalled_InnerCall;
			
			if(CallServer_Run.call_type != DxCallScene6_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene6_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_DtBeCalled_Bye(dtbecalled_type);
						//API_IpCaller_Cancel(CallServer_Run.call_type);
						bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
						CallServer_Run.state = CallServer_Wait;
					}
					
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						
						bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
						CallServer_Run.state = CallServer_Wait;
					}
					
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);
					bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					
					bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
					CallServer_Run.state = CallServer_Wait;
					DxCallScene6_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
	
}
void DxCallScene6_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
			
		case DxCallScene6_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
	
		default:
			break;
	}
	
}

void DxCallScene6_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
			
		case DxCallScene6_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
	
		default:
			break;
	}
	
}

void DxCallScene6_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
			
		case DxCallScene6_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
	
		default:
			break;
	}
	
}

void DxCallScene6_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			API_LedDisplay_CallRing();
			break;
			
		case DxCallScene6_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			break;
	
		default:
			break;
	}
}


void DxCallScene6_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	short unicode_buff[40];
	int len;
	switch(CallServer_Run.call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOn);
			//API_VideoTurnOn();
			API_RingPlay(RING_InnerCall);
			API_POWER_EXT_RING_ON();		//czn_20170809
			#if 1
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;		//czn_20170329
			call_record_temp.target_id			= 0;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				API_GetOSD_StringWithID2(MESG_TEXT_Icon_013_InnerCall, unicode_buff, &len);
				unicode2utf8(unicode_buff,len/2,disp_name);
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			#endif
			break;
			
		
	
		default:
			break;
	}
		
	
}

void DxCallScene6_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene6_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			API_LedDisplay_CallTalk();
			API_RingStop();
			//if(GetCallInfo_JudgeWithLocalMenu() == 1)
			{
				//API_TalkOn();//API_POWER_TALK_ON();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);

				call_record_temp.property	= NORMAL;
			}
			API_POWER_TALK_ON();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			OpenDtTalk();
			//dx_audio_ds2dx_start();
			break;
			
	
	
		default:
			break;
	}
	
}

void DxCallScene6_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene6_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_BeInnerCall);
			break;
			
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallScene6_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene6_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_BeInnerCall);
			break;
			
	
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}
