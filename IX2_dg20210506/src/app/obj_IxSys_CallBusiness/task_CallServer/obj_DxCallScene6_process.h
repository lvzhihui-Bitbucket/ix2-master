#ifndef _obj_DxCallScene6_process_h
#define _obj_DxCallScene6_process_h


void DxCallScene6_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene6_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallScene6_MenuDisplay_ToInvite(void);
void DxCallScene6_MenuDisplay_ToRing(void);
void DxCallScene6_MenuDisplay_ToAck(void);
void DxCallScene6_MenuDisplay_ToBye(void);
void DxCallScene6_MenuDisplay_ToWait(void);


#endif
