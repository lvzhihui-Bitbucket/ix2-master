#ifndef _task_CallServer_h
#define _task_CallServer_h

#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"
#include "obj_UnitSR.h"
#include "obj_business_manage.h"
#include "onvif_discovery.h"
#include "video_multicast_common.h"
//efine DivertErrRecoverNormal
//typedef enum
//{
//	CallServer_Business = 0,
//	RecvMsg_Analyze,
//}CallServer_Msg_Type;
//
//
//typedef enum
//{
//	RecvMsg_VtkUnicast = 0,
//	RecvMsg_VtkMulticast,
//	RecvMsg_Sip,
//}RecvMsg_Type;

#define CALLSERVER_BUSINESS_LOG_LEVEL				9
#define CALLSERVER_ERROR_LOG_LEVEL				10

#define CallServer_Invite_LimitTime					12
#define CallServer_Ring_LimitTime					600
#define CallServer_Ack_LimitTime						600
#define CallServer_Bye_LimitTime						12


#define	CALLSERVER_RESPONSE_OK		(1<<6)
#define	CALLSERVER_RESPONSE_ERR		(1<<7)

#define DX432_RT		30
#define LIPHONE_RT		31

#if 0
typedef enum
{
	DxCallScene1 = 1,					//DOORCALL: DS-> IX-MASTER/IX-SLAVE 
	DxCallScene2,						//INTERCOM:  IX-ANOTHER -> IX-MASTER
	DxCallScene3,						//INTERCOM:  IX-MASTER -> IX-ANOTHER 
	DxCallScene4,						//INNERCALL:  IX-MASTER -> IX-SLAVE 
	DxCallScene5,						//INNERCALL:  IX-SLAVE -> IX-MASTER 
}DxCallScene_t;
#endif
typedef enum
{
	IxCallScene1_Active = 1,				//DOORCALL: DS-> IX-IM 
	IxCallScene1_Passive,				//DOORCALL: DS-> IX-IM
	IxCallScene2_Active,				//INTERCOM:  IX-IM -> IX-IM
	IxCallScene2_Passive,				//INTERCOM:  IX-IM -> IX-IM 
	IxCallScene3_Active,				//INNERCALL:  IX-IM -> IX-IM 
	IxCallScene3_Passive,				//INNERCALL:  IX-IM -> IX-IM 
	IxCallScene4,						//IX50 -> VirtualUser		//czn_20190107
	IxCallScene5,						//test:IX50 -> VirtualUser
	IxCallScene8,						//phoneCALL: Phone -> DX-MASTER 
	IxCallScene9,						//DOORCALL:DS->DT-IM(IPG)			//call_dtim
	IxCallScene10_Active,				//GLCALL:GL->IX-IM
	IxCallScene10_Passive,
	IxCallScene11_Passive,			//DS->IX-IM WITH LOCAL DIVERT

	DxCallScene1_Master,			//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
	DxCallScene1_Slave,
	DxCallScene2_Master,				//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
	DxCallScene2_Slave,
	DxCallScene3,						//INTERCOM: DX_Master -> DT-ANOTHER
	DxCallScene4_Master,				//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
	DxCallScene4_Slave,
	DxCallScene5_Master,						//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
	DxCallScene5_Slave,
	DxCallScene6_Master,						//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
	DxCallScene6_Slave,
	//DxCallScene7,						//INNERCALL: DX-SLAVE -> DX-MASTER -> DT-SLAVE
	DxCallScene8,						//phoneCALL: Phone -> DX-MASTER
	DxCallScene9,						//sip call test
	DxCallSceneBrd_Master,				//INNERCALLBRD: DX-Master ->  DT-SLAVE
	DxCallSceneBrd_Slave,

}DxCallScene_t;

typedef enum
{
	IxDevType_Ds = 0,
	IxDevType_Im,
}IxDevType_e;

typedef enum
{
	IxCallServer_ParaType1 = 1,				//BD_NBR + RM_NBR
}IxCallServer_ParaType_t;

typedef enum
{
	CallServer_Wait = 0,
	CallServer_Invite,
	CallServer_Ring,
	CallServer_Ack,
	CallServer_SourceBye,
	CallServer_TargetBye,
	CallServer_Transfer,
	CallServer_LeaveMsg,
	CallServer_RemoteAck,
}CallServer_State;

typedef enum
{
	CallRule_Normal = 0,
	CallRule_NoDisturb,
	CallRule_TransferIm,
	CallRule_TransferNoAck,
	CallRule_LeaveMsgIm,
	CallRule_LeaveMsgNoAck,
}CallRule_E;
#if 0
typedef enum
{
	CallServer_AsCallSource = 0,
	CallServer_AsCallTarget,
	CallServer_AsForceCallSource,
}CallServer_WorkMode;
#endif

typedef enum
{
	CallServer_Msg_Invite = 0,
	CallServer_Msg_InviteOk,
	CallServer_Msg_InviteFail,
	CallServer_Msg_RingNoAck,
	CallServer_Msg_RemoteRing,
	CallServer_Msg_LocalAck,
	CallServer_Msg_RemoteAck,
	CallServer_Msg_DtSlaveAck,
	CallServer_Msg_LocalBye,
	CallServer_Msg_RemoteBye,
	CallServer_Msg_ByeOk,
	CallServer_Msg_DtSrDisconnect,
	CallServer_Msg_NetCallLinkDisconnect,
	CallServer_Msg_Timeout,
	CallServer_Msg_LocalUnlock1,
	CallServer_Msg_LocalUnlock2,
	CallServer_Msg_RemoteUnlock1,
	CallServer_Msg_RemoteUnlock2,
	CallServer_Msg_Redail,			//czn_20171030
	CallServer_Msg_Transfer,
	CallServer_Msg_ThreadHeartbeatReq,
	CallServer_Msg_AppAck,
	CallServer_Msg_CheckVideo,
}CallServer_Msg_Type;

#pragma pack(1)

//czn_20180813_s
typedef struct
{
	int	ip_addr;
	char	dev_type[21];
	char bd_rm_ms[11];
	char global_nbr[11];
	char name[41];
}Call_Dev_Info;

#define MAX_CALL_TARGET_NUM		32
//czn_20180813_e

// lzh_20180910_s
typedef struct
{
	char			sip_server[16];
	char 			sip_account[20];
}sip_account_Info;

// lzh_20180910_e

#define  MAX_IMSLAVE_NUMS				10
#define MAX_CALLSERVER_PARABUF_LEN		20

typedef enum
{
	VIDEO_SOURCE_IX_DEVICE = 0,
	VIDEO_SOURCE_IPC,
}VIDEO_SOURCE;

typedef enum
{
	DISABLE = 0,
	ENABLE,
}VIDEO_VALID;


typedef struct
{
	unsigned char 		state;
	unsigned char 		call_type;
	time_t				timer;
	int					with_local_menu;
	int					call_rule;
	int					rule_act;		//0 = normal,1 = transfer, 2 = leave msg
	
	Call_Dev_Info		source_dev;		// 主叫方的设备信息
	Call_Dev_Info		target_hook_dev;// 被叫方的设备信息
	
	int					tdev_nums;		// 主叫方的目标设备个数
	Call_Dev_Info		target_dev_list[MAX_CALL_TARGET_NUM];	// 主叫方的目标设备信息
	
	// lzh_20180912_s
	int					caller_divert_state;		// 当前设备divert的状态
	sip_account_Info 	caller_master_sip_divert;	// 当前设备divert的sip帐号
	int					callee_divert_state;		// 应答设备divert的状态
	sip_account_Info 	callee_master_sip_divert;	// 应答设备divert的sip帐号
	int					callee_rsp_command_id;		// 当前需要应答命令的应答id
	// lzh_20180912_e
#pragma pack()

	VIDEO_SOURCE		videoSource;				// 当前视频来源IX或者IPC
	VIDEO_SOURCE 		defaultVideoSource;			// 默认视频来源IX或者IPC
	VIDEO_VALID			ixVideoSourceValid;			// IX视频源是否有效
	VIDEO_VALID 		ipcVideoSourceValid;		// IPC视频源是否有效
	int					ixVideoIp;					// IX视频IP地址
	char				ipcVideoName[42];			// IPC视频显示名字
	char				ixVideoName[42];			// IX视频显示名字
	onvif_login_info_t 	ipcInfo;
	int s_dt_addr;
	int	divert_video_ver;
}CallServer_Run_Stru;


extern CallServer_Run_Stru CallServer_Run;
extern int MasterOrSlaveSetting;
extern Global_Addr_Stru DxMaster_Addr;


typedef struct
{
	unsigned char 		msg_target_id;		// 消息的目标对象
	unsigned char 		msg_source_id;		// 消息的源对象
	unsigned char 		msg_type;			// 消息的类型
	unsigned char  		msg_sub_type;		 //消息的子类型
	unsigned char 		call_type;
	//Global_Addr_Stru 		partner_addr;
	Call_Dev_Info			source_dev;
	int					target_dev_num;
	int partner_dt_addr;
	union
	{
		Call_Dev_Info			target_dev_list[MAX_CALL_TARGET_NUM];
		sip_account_Info		sip_acc;
	};
	
	//Global_Addr_Stru 		partner_addr2;
}CALLSERVER_STRU;

#define CallServerMsgExtTargetList_Length		((int)&(((CALLSERVER_STRU*)0)->target_dev_list[0]))
#define CallServerWithTargetList_Length(Tar_Num)		((int)&(((CALLSERVER_STRU*)0)->target_dev_list[Tar_Num]))		//czn_20190527


typedef enum
{
	CALL_LINK_BUSY, 			
	CALL_LINK_ERROR1, 		
	CALL_LINK_ERROR2,		
	CALL_LINK_ERROR3,		
	
}CALL_ERR_CODE;




uint8 VdpCallServer_Remote_Data_Dispatch(uint8 *pbuf,uint8 len);
void CallServer_Business_Process(CALLSERVER_STRU *Msg_CallServer);

void CallServer_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer);

void CallServer_StateInvalid_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_MsgInvalid_Process(CALLSERVER_STRU *Msg_CallServer);


void CallServer_Timer_Callback(void);

//int API_CallServer_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru partner_addr,Global_Addr_Stru *partner_addr2);
int API_CallServer_Common(uint8 msg_type, uint8 call_type, Call_Dev_Info *source_dev,int target_num,void *target_list);
void CallServer_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);
int GetCallInfo_JudgeWithLocalMenu(void);
int GetCallInfo_JudgeIsLocalUnlock(void);
void DxCall_MenuDisplay_ToInvite(void);
void DxCall_MenuDisplay_ToRing(void);
void DxCall_MenuDisplay_ToAck(void);
void DxCall_MenuDisplay_ToBye(void);
void DxCall_MenuDisplay_ToWait(void);


#define API_CallServer_BeInvite(call_type,source_dev)								API_CallServer_Common(CallServer_Msg_Invite,call_type,source_dev,0,NULL)
#define API_CallServer_Invite(call_type,target_num,target_list)							API_CallServer_Common(CallServer_Msg_Invite,call_type,NULL,target_num,target_list)
#define API_CallServer_InviteOk(call_type)										API_CallServer_Common(CallServer_Msg_InviteOk,call_type,NULL,0,NULL)
#define API_CallServer_InviteFail(call_type)										API_CallServer_Common(CallServer_Msg_InviteFail,call_type,NULL,0,NULL)

#define API_CallServer_LocalAck(call_type)										API_CallServer_Common(CallServer_Msg_LocalAck,call_type,NULL,0,NULL)
#define API_CallServer_RemoteAck(call_type,target_dev)							API_CallServer_Common(CallServer_Msg_RemoteAck,call_type,NULL,1,target_dev)
#define API_CallServer_AppAck(call_type,target_dev)							API_CallServer_Common(CallServer_Msg_AppAck,call_type,NULL,1,target_dev)



#define API_CallServer_LocalBye(call_type)					API_CallServer_Common(CallServer_Msg_LocalBye,call_type,NULL,0,NULL)
#define API_CallServer_RemoteBye(call_type,target_dev)		API_CallServer_Common(CallServer_Msg_RemoteBye,call_type,NULL,1,target_dev)
#define API_CallServer_ByeOk(call_type,target_dev)			API_CallServer_Common(CallServer_Msg_ByeOk,call_type,NULL,1,target_dev)

#define API_CallServer_DtSr_Disconnect()						API_CallServer_Common(CallServer_Msg_DtSrDisconnect,CallServer_Run.call_type,NULL,0,NULL)
#define API_CallServer_NetCallLink_Disconnect()				API_CallServer_Common(CallServer_Msg_NetCallLinkDisconnect,0,NULL,0,NULL)

#define API_CallServer_LocalUnlock1(call_type)				API_CallServer_Common(CallServer_Msg_LocalUnlock1,call_type,NULL,0,NULL)
#define API_CallServer_LocalUnlock2(call_type)				API_CallServer_Common(CallServer_Msg_LocalUnlock2,call_type,NULL,0,NULL)
#define API_CallServer_RemoteUnlock1(call_type,target_dev)				API_CallServer_Common(CallServer_Msg_RemoteUnlock1,call_type,NULL,1,target_dev)
#define API_CallServer_RemoteUnlock2(call_type,target_dev)				API_CallServer_Common(CallServer_Msg_RemoteUnlock2,call_type,NULL,1,target_dev)

#define API_CallServer_RingNoAck(call_type)				API_CallServer_Common(CallServer_Msg_RingNoAck,call_type,NULL,0,NULL)
#define API_CallServer_Timeout(call_type)				API_CallServer_Common(CallServer_Msg_Timeout,call_type,NULL,0,NULL)

#define API_CallServer_Redail(call_type)				API_CallServer_Common(CallServer_Msg_Redail,call_type,NULL,0,NULL)	//czn_20171030

#define API_CallServer_Transfer(call_type)				API_CallServer_Common(CallServer_Msg_Transfer,call_type,NULL,0,NULL)
#define API_CallServer_CheckVideo()						API_CallServer_Common(CallServer_Msg_CheckVideo,CallServer_Run.call_type,NULL,0,NULL)
#endif
