#ifndef _obj_DxCallScene5_process_h
#define _obj_DxCallScene5_process_h


void DxCallScene5_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene5_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallScene5_MenuDisplay_ToInvite(void);
void DxCallScene5_MenuDisplay_ToRing(void);
void DxCallScene5_MenuDisplay_ToAck(void);
void DxCallScene5_MenuDisplay_ToBye(void);
void DxCallScene5_MenuDisplay_ToWait(void);
void DxCallScene5_MenuDisplay_ToInviteFail(int fail_type);


#endif
