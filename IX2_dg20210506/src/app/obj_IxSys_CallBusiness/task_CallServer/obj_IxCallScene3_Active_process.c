#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"

#include "obj_IxCallScene3_Active_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;


void IxCallScene3_Active_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{	
	int i;
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INNERCALL:  IX-MASTER -> IX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					//czn_20190107_s
					if(API_Business_Request(Business_State_InnerCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					//czn_20190107_e
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					#if 0
					if(Load_TargetAddr_ByPara(Msg_CallServer->para_type,Msg_CallServer->para_length,Msg_CallServer->para_buff) != 0)
					{
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						BEEP_ERROR();
						return;
					}
					#endif
					CallServer_Run.tdev_nums = Msg_CallServer->target_dev_num;
					for(i = 0;i < Msg_CallServer->target_dev_num;i++)
					{
						CallServer_Run.target_dev_list[i] = Msg_CallServer->target_dev_list[i];
					}
					Get_SelfDevInfo(CallServer_Run.target_dev_list[0].ip_addr , &CallServer_Run.source_dev);
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//will_add CallServer_Run.t_addr = local_addr;
					CallServer_Run.timer = time(NULL);
					//CallServer_Run.para_type = Msg_CallServer->para_type;
					//CallServer_Run.para_length = Msg_CallServer->para_length;
					//memcpy(CallServer_Run.para_buff,Msg_CallServer->para_buff,CallServer_Run.para_length);
					API_IpCaller_Invite(IpCaller_IxSys, NULL);
					IxCallScene3_Active_MenuDisplay_ToInvite();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
		
	
	
}

void IxCallScene3_Active_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			if(CallServer_Run.call_type != IxCallScene3_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					IxCallScene3_Active_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ring(IpCaller_IxSys, NULL);
					IxCallScene3_Active_MenuDisplay_ToRing();
					break;	

				case CallServer_Msg_InviteFail:
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					//IxCallScene3_MenuDisplay_ToWait();
					IxCallScene3_Active_MenuDisplay_ToInviteFail(1);
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ack(IpCaller_IxSys, NULL);
					IxCallScene3_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
}

void IxCallScene3_Active_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			
			if(CallServer_Run.call_type != IxCallScene3_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					IxCallScene3_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_RemoteAck:
					CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ack(IpCaller_IxSys, NULL);
					IxCallScene3_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene3_Active_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			if(CallServer_Run.call_type != IxCallScene3_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					IxCallScene3_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene3_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
	
}
void IxCallScene3_Active_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene3_Active_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene3_Active_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene3_Active_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			API_talk_off();
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallStart);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallStartting);
			API_Event_By_Name("EventCall");
			break;
			
		default:
			break;
	}
}

void IxCallScene3_Active_MenuDisplay_ToInviteFail(int fail_type)
{
	CALL_ERR_CODE errorCode;
	//czn_20190107_s
	API_Business_Close(Business_State_InnerCall);	
	//czn_20190107_e
	API_LedDisplay_CallClose();
	if(fail_type == 0)	//systembusy
	{
		errorCode = CALL_LINK_BUSY;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
	else	 //linkerror
	{
		errorCode = CALL_LINK_ERROR2;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
}


void IxCallScene3_Active_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			API_LedDisplay_CallRing();
			API_RingPlay(RING_InnerCall);//will_change
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOn);
			//#if !defined(PID_IXSE)
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= OUT_GOING;
			call_record_temp.property				= NORMAL;
			call_record_temp.target_node			=0;// CallServer_Run.t_addr.gatewayid;		//czn_20170329
			call_record_temp.target_id			=0;// CallServer_Run.t_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			//#endif
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.t_addr,disp_name) == 0)
			//if(CallServer_Run.para_type == IxCallServer_ParaType1)
			{
				#if 0
				if((tempData[0] = strlen(CallServer_Run.target_dev_list[0].name))>0)
				{
					memcpy(&tempData[1],CallServer_Run.target_dev_list[0].name,tempData[0]);
					strcpy(call_record_temp.name,CallServer_Run.target_dev_list[0].name);
				}
				else
				{
					tempData[0] = strlen(CallServer_Run.target_dev_list[0].bd_rm_ms);
					memcpy(&tempData[1],CallServer_Run.target_dev_list[0].bd_rm_ms,tempData[0]);
				}
				#endif
				strcpy(&tempData[1],"Inner Call");
				strcpy(call_record_temp.name,&tempData[1]);
				tempData[0] = strlen(&tempData[1]);
				
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				//strcpy(call_record_temp.name,disp_name);
			}
			break;
			
		default:
			break;
	}
		
	
}

void IxCallScene3_Active_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			API_RingStop();
			int try_cnt=0;
			while(try_cnt++<100&&RingGetState()!=0)
			{
				usleep(50*1000);
			}
			API_talk_on_by_unicast(CallServer_Run.target_hook_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOn);
			API_Event_By_Name("EventBECallTalk");
			//API_RingStop();
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);
			break;
			
		default:
			break;
	}
	
}

void IxCallScene3_Active_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another
			//czn_20190107_s
			API_Business_Close(Business_State_InnerCall);	
			//czn_20190107_e
			API_talk_off();
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			API_Event_By_Name("EventBECallBye");
			break;
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void IxCallScene3_Active_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene3_Active:					//INTERCOM: DX_SLAVE -> DX-MASTER -> DT-Another 
			//czn_20190107_s
			API_Business_Close(Business_State_InnerCall);	
			//czn_20190107_e
			API_talk_off();
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			API_Event_By_Name("EventBECallBye");
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}
