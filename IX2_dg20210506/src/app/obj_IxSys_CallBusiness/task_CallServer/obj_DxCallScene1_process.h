#ifndef _obj_DxCallScene1_process_h
#define _obj_DxCallScene1_process_h


void DxCallScene1_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene1_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallScene1_MenuDisplay_ToInvite(void);
void DxCallScene1_MenuDisplay_ToRing(void);
void DxCallScene1_MenuDisplay_ToAck(void);
void DxCallScene1_MenuDisplay_ToBye(void);
void DxCallScene1_MenuDisplay_ToWait(void);
void DxCallScene1_MenuDisplay_ToTransfer(void);
void DxCallScene1_MenuDisplay_DivertToLocalAck(void);
void DxCallScene1_MenuDisplay_ToRemoteAck(void);

#endif
