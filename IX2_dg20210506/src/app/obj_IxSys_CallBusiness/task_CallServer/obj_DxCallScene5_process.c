#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "vdp_uart.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"

#include "obj_DxCallScene5_process.h"


extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

void DxCallScene5_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtcaller_type=0;
	//uint8 slaveMaster;
	uint8 SR_apply;
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			dtcaller_type = DtCaller_InnerCall;
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT14_INNER_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					API_Business_Request(Business_State_InnerCall);
					if(Get_DtCaller_State() != 0)
					{
						API_DtCaller_ForceClose();
					}

					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
					CallServer_Run.timer = time(NULL);

					
					
					{
						API_DtCaller_Invite(dtcaller_type);
					}
					DxCallScene5_MenuDisplay_ToInvite();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void DxCallScene5_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
		
			dtcaller_type = DtCaller_InnerCall;
			if(CallServer_Run.call_type != DxCallScene5_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT14_INNER_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					DxCallScene5_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
				#if 0	
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ring(ipcaller_type, &CallServer_Run.t_addr);
					API_DtCaller_Ring(dtcaller_type, &CallServer_Run.t_addr);
					DxCallScene5_MenuDisplay_ToRing();
					break;	
				#endif
				
				case CallServer_Msg_InviteFail:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					DxCallScene5_MenuDisplay_ToWait();
					break;
				//case CallServer_Msg_DtSlaveAck:
				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					
					
					{
						API_DtCaller_Ack(dtcaller_type);
						
					}
					
					DxCallScene5_MenuDisplay_ToAck();
					break;
					
				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;
				
				case CallServer_Msg_RemoteBye:
					
					{
						API_DtCaller_Bye(dtcaller_type);
						
					}
					
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;


				default:
					break;
			}
			break;
			
		case DxCallScene5_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
	
		default:
			break;
	}
}

void DxCallScene5_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			if(CallServer_Run.call_type != DxCallScene5_Slave)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT14_INNER_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					DxCallScene5_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
			}
			break;
			
		case DxCallScene5_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			dtcaller_type = DtCaller_InnerCall;
			if(CallServer_Run.call_type != DxCallScene5_Slave)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene5_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RemoteAck:	
				case CallServer_Msg_DtSlaveAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL); 
					
					
					
					API_DtCaller_Ack(dtcaller_type);
					DxCallScene5_MenuDisplay_ToAck();
					break;
					
				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_DtCaller_Cancel(dtcaller_type);
					DxCallScene5_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//CallServer_Run.state = CallServer_SourceBye;
					//API_IpBeCalled_Cancel(CallServer_Run.call_type);
					//API_DtCaller_Cancel(CallServer_Run.call_type);
					//CallServer_Run.state = CallServer_Wait;
					
					{
						//API_IpBeCalled_Bye(ipbecalled_type);
						API_DtCaller_Bye(dtcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					
					
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpBeCalled_ForceClose();
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_IpBeCalled_ForceClose();
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void DxCallScene5_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			dtcaller_type = DtCaller_InnerCall;
			if(CallServer_Run.call_type != DxCallScene5_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT14_INNER_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					DxCallScene5_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;
				
				case CallServer_Msg_RemoteBye:
					
					{
						
						API_DtCaller_Cancel(dtcaller_type);
					}
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene5_MenuDisplay_ToWait();
					break;

				

				default:
					break;
			}
			break;
	
		default:
			break;
	}
	
	
}
void DxCallScene5_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallScene5_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallScene5_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallScene5_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallScene5_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallScene5_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallScene5_MenuDisplay_ToInvite(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	short unicode_buff[40];
	int len;
	switch(CallServer_Run.call_type)
	{
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallRing();
			API_RingPlay(RING_InnerCall);
			API_POWER_EXT_RING_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallStart);
			#if 1
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= OUT_GOING;
			call_record_temp.property				= NORMAL;
			call_record_temp.target_node			= 0;		//czn_20170329
			call_record_temp.target_id			= 0;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			//if(Get_DxMonRes_Name_ByAddr(CallServer_Run.s_dt_addr,disp_name)==0)
			{
				
				API_GetOSD_StringWithID2(MESG_TEXT_Icon_013_InnerCall, unicode_buff, &len);
				unicode2utf8(unicode_buff,len/2,disp_name);
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			#endif
			break;
			
		default:
			break;
	}
}

void DxCallScene5_MenuDisplay_ToInviteFail(int fail_type)
{
	CALL_ERR_CODE errorCode;
	API_Business_Close(Business_State_InnerCall);
	API_LedDisplay_CallClose();
	if(fail_type == 0)	//systembusy
	{
		errorCode = CALL_LINK_BUSY;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
	else	 //linkerror
	{
		errorCode = CALL_LINK_ERROR2;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
}


void DxCallScene5_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];

	switch(CallServer_Run.call_type)
	{
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallRing();
			break;
			
			
		default:
			break;
	}
		
	
}

void DxCallScene5_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallTalk();
			API_RingStop();
			//API_TimeLapseStart(COUNT_RUN_UP,0);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);
			//API_TalkOn();//API_POWER_TALK_ON();
			API_POWER_TALK_ON();
			OpenDtTalk();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			//dx_audio_ds2dx_start();
			break;
			
		
		default:
			break;
	}
	
}

void DxCallScene5_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_InnerCall);
			break;
			
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallScene5_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene5_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_InnerCall);
			break;
			
	
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}
