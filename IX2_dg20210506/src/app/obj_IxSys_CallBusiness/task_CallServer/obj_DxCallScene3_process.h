#ifndef _obj_DxCallScene3_process_h
#define _obj_DxCallScene3_process_h


void DxCallScene3_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallScene3_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallScene3_MenuDisplay_ToInvite(void);
void DxCallScene3_MenuDisplay_ToRing(void);
void DxCallScene3_MenuDisplay_ToAck(void);
void DxCallScene3_MenuDisplay_ToBye(void);
void DxCallScene3_MenuDisplay_ToWait(void);
void DxCallScene3_MenuDisplay_ToInviteFail(int fail_type);


#endif
