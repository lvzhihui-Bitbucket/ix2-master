#include <stdio.h>
#include "task_CallServer.h"
#include "../obj_BeCalled_State.h"
#include "../obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "../task_IpBeCalled/task_IpBeCalled.h"
#include "../task_IpCaller/task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "task_Led.h"
#include "task_Ring.h"
#include "task_Beeper.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#ifdef IX50_CALLSERVER
#include "obj_menu_data.h"
#endif
#include "obj_IxCallScene1_Active_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;


void IxCallScene1_Active_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	int i;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					
					//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					usleep(1000);
					
					IxCallScene1_Active_MenuDisplay_ToInvite();
					
					CallServer_Run.tdev_nums = Msg_CallServer->target_dev_num;
					for(i = 0;i < Msg_CallServer->target_dev_num;i++)
					{
						CallServer_Run.target_dev_list[i] = Msg_CallServer->target_dev_list[i];
					}
					Get_SelfDevInfo(0, &CallServer_Run.source_dev);
					#if 0
					if(Load_TargetAddr_ByPara(Msg_CallServer->para_type,Msg_CallServer->para_length,Msg_CallServer->para_buff) != 0)
					{
						//BEEP_ERROR();
						IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//will_add CallServer_Run.t_addr = local_addr;
					CallServer_Run.timer = time(NULL);
					//CallServer_Run.para_type = Msg_CallServer->para_type;
					//CallServer_Run.para_length = Msg_CallServer->para_length;
					//memcpy(CallServer_Run.para_buff,Msg_CallServer->para_buff,CallServer_Run.para_length);
					API_IpCaller_Invite(IpCaller_IxSys, NULL);
					//IxCallScene1_Active_MenuDisplay_ToInvite();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					//CallServer_Run.state = CallServer_Ring;
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene1_Active_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					IxCallScene1_Active_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ring(IpCaller_IxSys, NULL);
					IxCallScene1_Active_MenuDisplay_ToRing();
					break;	

				case CallServer_Msg_InviteFail:
					#if 0
					if(TargetAddrErr_Process(CallServer_Run.para_type,CallServer_Run.para_length,CallServer_Run.para_buff) ==0)
					{
						//will_add
					}
					#endif
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					//IxCallScene3_MenuDisplay_ToWait();
					IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					API_IpCaller_Ack(IpCaller_IxSys, NULL);
					IxCallScene1_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
}

void IxCallScene1_Active_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != IxCallScene1_Active)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_RemoteAck:
					CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ack(IpCaller_IxSys, NULL);
					IxCallScene1_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Active_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene1_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpCaller_Bye(IpCaller_IxSys,NULL);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
	
}
void IxCallScene1_Active_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene1_Active_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Active_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipbecalled_type=0;
	#if 0
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene1_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					#ifdef DivertErrRecoverNormal
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						//dtbecalled_type = DtBeCalled_MainCall;
						//ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					else
					{
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						IxCallScene1_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						{
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);

					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	#endif
	
}

#define Caller_Name_Disp_y			(38+111)
#define Caller_RoomId_Disp_y		(38+111*2)
#define Caller_State_Disp_y			(38+111*3)
#define Link_State_Str				"Linking..."
#define Ring_State_Str				"Calling..."
#define Talk_State_Str				"Talking..."

void IxCallScene1_Active_MenuDisplay_ToInvite(void)
{
	#ifdef IX50_CALLSERVER
	int tb_index;
	char name_buff[21];
	char input_buff[9];
	
	//switch(CallServer_Run.call_type)
	{
	//	case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			StartInitOneMenu(MENU_005_CALL,0,1);
			API_TimeLapseStart(COUNT_RUN_UP,0);
			
			#if 0
			if((tb_index = SearchNamelistByAddr(Caller_Run.target_addr)) < 0)
			{
				strcpy(name_buff,"Unkown User");
				snprintf(input_buff,20,"%04d",Caller_Run.target_addr.gatewayid);
			}
			else
			{
				GetOneNamelistInfo(tb_index,NULL,name_buff,input_buff,NULL);
			}
			//printf("!!!!SearchNamelistByAddr tb_index=%d,\n",tb_index);
			#endif
			//get_calldail_disp(8,CallServer_Run.para_buff,name_buff);
			if(strlen(CallServer_Run.target_dev_list[0].name)>0)
			{
				strcpy(name_buff,CallServer_Run.target_dev_list[0].name);
			}
			else
			{
				strcpy(name_buff,CallServer_Run.target_dev_list[0].bd_rm_ms);
			}
	
			API_OsdStringCenterDisplayExt(0, Caller_Name_Disp_y, COLOR_WHITE, name_buff, strlen(name_buff),0, STR_UTF8, 480, 40);
			
			//API_OsdStringCenterDisplayExt(0, Caller_RoomId_Disp_y, COLOR_WHITE, input_buff, strlen(input_buff),0, STR_UTF8, 480, 40);
			API_OsdStringClearExt(0, Caller_State_Disp_y, 480, 40);
			API_OsdStringCenterDisplayExt(0, Caller_State_Disp_y, COLOR_WHITE, Link_State_Str, strlen(Link_State_Str),0, STR_UTF8, 480, 40);
		//	break;
			
		//default:
		//	break;
	}
	#endif
}

void IxCallScene1_Active_MenuDisplay_ToInviteFail(int fail_type)
{
	#ifdef IX50_CALLSERVER
#if 0
	CALL_ERR_CODE errorCode;
	
	//API_LedDisplay_CallClose();
	if(fail_type == 0)	//systembusy
	{
		errorCode = CALL_LINK_BUSY;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
	else	 //linkerror
	{
		errorCode = CALL_LINK_ERROR2;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
#endif
	BEEP_ERROR();
	popDisplayLastMenu();
	#endif
}


void IxCallScene1_Active_MenuDisplay_ToRing(void)
{
	#ifdef IX50_CALLSERVER
	char disp_name[21] = {0};
	char tempData[42];
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
		#if 0
			if(CallServer_Run.rule_act == 1)
			{
				API_VideoTurnOff();
				break;
			}
			//API_LedDisplay_CallRing();
			API_VideoTurnOn();
			API_POWER_VIDEO_ON();
			usleep(1500000);

			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
			
			API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
			API_POWER_EXT_RING_ON();		//czn_20170809
			
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
			call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			#if 1
			if(memo_video_record_start("BeCalled") == 0)
			{
				strncpy(call_record_temp.relation,one_vd_record.filename,40);
			}
			#endif
		#endif
			API_TimeLapseStart(COUNT_RUN_UP,0);
	
			API_OsdStringClearExt(0, Caller_State_Disp_y, 480, 40);
			API_OsdStringCenterDisplayExt(0, Caller_State_Disp_y, COLOR_WHITE, Ring_State_Str, strlen(Ring_State_Str),0, STR_UTF8, 480, 40);
			break;
			
	
		default:
			break;
	}
		
	#endif
}

void IxCallScene1_Active_MenuDisplay_ToAck(void)
{
	#ifdef IX50_CALLSERVER
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			#if 0
			if(CallServer_Run.rule_act == 1)
			{
				usleep(1000000);
				API_POWER_VIDEO_ON();
				API_POWER_UDP_TALK_ON();
				call_record_temp.property	= NORMAL;
				break;
			}
			//API_LedDisplay_CallTalk();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			if(GetCallInfo_JudgeWithLocalMenu() == 1)
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledTalkOn);
				usleep(1000000);
				API_TalkOn();//API_POWER_TALK_ON();
				call_record_temp.property	= NORMAL;
			}
			else
			{	
				if(CallServer_Run.t_addr.rt == LIPHONE_RT)
				{
					API_VideoTurnOff();
					usleep(1000000);
					API_POWER_VIDEO_ON();
					API_POWER_UDP_TALK_ON();
				}
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			}
			#endif
			API_RingStop();
			API_talk_on_by_unicast(CallServer_Run.t_addr.ip,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_TimeLapseStart(COUNT_RUN_DN,90);

			API_OsdStringClearExt(0, Caller_State_Disp_y, 480, 40);
			API_OsdStringCenterDisplayExt(0, Caller_State_Disp_y, COLOR_WHITE, Talk_State_Str, strlen(Talk_State_Str),0, STR_UTF8, 480, 40);
			break;
	
		default:
			break;
	}
	#endif
	
}

void IxCallScene1_Active_MenuDisplay_ToBye(void)
{
	#ifdef IX50_CALLSERVER
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			#if 0
			//API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			API_POWER_VIDEO_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			//usleep(500000);
			API_VideoTurnOff();
			#endif
			API_talk_off();
			if(GetCurMenuCnt() == MENU_005_CALL)
			{
				popDisplayLastMenu();
			}
			break;
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
	#endif
}

void IxCallScene1_Active_MenuDisplay_ToWait(void)
{
	#ifdef IX50_CALLSERVER
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
		#if 0
			//API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			API_POWER_VIDEO_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			//usleep(500000);
			API_VideoTurnOff();
		#endif
			API_talk_off();
			if(GetCurMenuCnt() == MENU_005_CALL)
			{
				popDisplayLastMenu();
			}
			break;
	
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
	#endif

}

void IxCallScene1_Active_MenuDisplay_ToTransfer(void)
{	
	#ifdef IX50_CALLSERVER
	char disp_name[21] = {0};
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Active:	
			if(CallServer_Run.call_rule == CallRule_TransferIm)
			{
				//API_LedDisplay_CallDivert();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//API_VideoTurnOn(0);
				API_RingStop();
				API_POWER_EXT_RING_OFF();		//czn_20170809
				call_record_temp.type 				= CALL_RECORD;
				call_record_temp.subType 			= IN_COMING;
				call_record_temp.property				= MISSED;
				call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
				call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
				strcpy(call_record_temp.name,"---");
				strcpy(call_record_temp.input,"---");
				strcpy(call_record_temp.relation, "-");
				call_record_flag = 1;
				
				if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
				{
					strcpy(call_record_temp.name,disp_name);
				}
			}
			else
			{
				//API_LedDisplay_CallDivert();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				API_VideoTurnOff();
				API_RingStop();
				API_POWER_EXT_RING_OFF();		//czn_20170809
			}
			break;

		default:
			break;
	}
	#endif
}

