#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "vdp_uart.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "obj_DxCallSceneBrd_process.h"


extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

void DxCallSceneBrd_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 slaveMaster;
	uint8 SR_apply;
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			dtcaller_type = DtCaller_InnerBrdCall;
	
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT15_INNER_BRD);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					
					if(Get_DtCaller_State() != 0)
					{
						API_DtCaller_ForceClose();
					}

					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//CallServer_Run.s_addr = GetLocal_IpGlobalAddr();
					CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
					CallServer_Run.timer = time(NULL);

					
					
					API_DtCaller_Invite(dtcaller_type);

					DxCallSceneBrd_MenuDisplay_ToInvite();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				default:
					break;
			}
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			dtbecalled_type = DtBeCalled_InnerBrdCall;
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					
					
					if(Get_DtBeCalled_State() != 0)
					{
						API_DtBeCalled_ForceClose();
					}
		
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
					//will_add CallServer_Run.t_addr = local_addr;
					CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
					CallServer_Run.timer = time(NULL);
				
					if(slaveMaster == 0)
					{
						API_DtBeCalled_Invite(dtbecalled_type);
					}
					API_DtBeCalled_Ack(dtbecalled_type);
					//API_DtCaller_Ring(dtcaller_type, NULL);
					CallServer_Run.state = CallServer_Ack;
					DxCallSceneBrd_MenuDisplay_ToAck();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
			}

			break;
	
		default:
			break;
	}
		
	
	
}

void DxCallSceneBrd_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE

			dtcaller_type = DtCaller_InnerBrdCall;
			if(CallServer_Run.call_type != DxCallSceneBrd_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					#if 0
					SR_apply = API_SR_Request(CT15_INNER_BRD);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					#endif
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
						BEEP_ERROR();
						DxCallScene5_MenuDisplay_ToInviteFail(0);
					}
					
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
				#if 0	
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ring(ipcaller_type, &CallServer_Run.t_addr);
					API_DtCaller_Ring(dtcaller_type, &CallServer_Run.t_addr);
					DxCallScene5_MenuDisplay_ToRing();
					break;	
				#endif
				
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Ring(dtcaller_type);
					DxCallSceneBrd_MenuDisplay_ToAck();
					break;
					
				case CallServer_Msg_InviteFail:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					DxCallSceneBrd_MenuDisplay_ToInviteFail(0);
					break;
					
				case CallServer_Msg_DtSlaveAck:
				case CallServer_Msg_RemoteAck:
					
					break;
					
				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;
				
				case CallServer_Msg_RemoteBye:
					API_DtCaller_Cancel(dtcaller_type);
					
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
	
		default:
			break;
	}
}

void DxCallSceneBrd_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			break;
			
		default:
			break;
	}
	
}

void DxCallSceneBrd_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			dtcaller_type = DtCaller_InnerBrdCall;
			if(CallServer_Run.call_type != DxCallSceneBrd_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					#if 0
					SR_apply = API_SR_Request(CT14_INNER_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene5_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					#endif
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;
				
				case CallServer_Msg_RemoteBye:
					API_DtCaller_Cancel(dtcaller_type);
					
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
		
			dtbecalled_type = DtBeCalled_InnerBrdCall;
			
			if(CallServer_Run.call_type != DxCallSceneBrd_Slave)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene5_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_DtBeCalled_Bye(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					
					
					API_DtBeCalled_Cancel(dtbecalled_type);
						
					CallServer_Run.state = CallServer_Wait;
					
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					API_DtBeCalled_Cancel(dtbecalled_type);
						
					CallServer_Run.state = CallServer_Wait;
					
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_Cancel(dtbecalled_type);
						
					CallServer_Run.state = CallServer_Wait;
					
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtBeCalled_Cancel(dtbecalled_type);
						
					CallServer_Run.state = CallServer_Wait;
					
					DxCallSceneBrd_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			
			break;
	
		default:
			break;
	}
	
	
}
void DxCallSceneBrd_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallSceneBrd_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallSceneBrd_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			break;
			
		default:
			break;
	}
	
}

void DxCallSceneBrd_MenuDisplay_ToInvite(void)
{
	char disp_name[21] = {0};
	char tempData[42];

	switch(CallServer_Run.call_type)
	{
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallRing();
			//API_RingPlay(RING_SCENE_2);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallStart);

			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= OUT_GOING;
			call_record_temp.property				= NORMAL;
			call_record_temp.target_node			= 0;//CallServer_Run.t_addr.gatewayid;		//czn_20170329
			call_record_temp.target_id			= 0;//CallServer_Run.t_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 0;
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				strcpy(disp_name,"Inner Broadcast");
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			break;
			
		default:
			break;
	}
}

void DxCallSceneBrd_MenuDisplay_ToInviteFail(int fail_type)
{
	CALL_ERR_CODE errorCode;

	API_LedDisplay_CallClose();
	if(fail_type == 0)	//systembusy
	{
		errorCode = CALL_LINK_BUSY;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
	else	 //linkerror
	{
		errorCode = CALL_LINK_ERROR2;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
}


void DxCallSceneBrd_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];

	switch(CallServer_Run.call_type)
	{
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			
			break;
			
		default:
			break;
	}
		
	
}

void DxCallSceneBrd_MenuDisplay_ToAck(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	switch(CallServer_Run.call_type)
	{
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallTalk();
			//API_RingStop();
			//API_TimeLapseStart(COUNT_RUN_UP,0);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);
			//API_TalkOn();//API_POWER_TALK_ON();
			API_POWER_TALK_ON();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			dx_audio_ds2dx_start();
			OpenDtTalk();
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallTalk();
			//API_RingPlay(RING_SCENE_2);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeBrdCallOn);

			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= OUT_GOING;
			call_record_temp.property				= NORMAL;
			call_record_temp.target_node			= 0;//CallServer_Run.t_addr.gatewayid;		//czn_20170329
			call_record_temp.target_id			= 0;//CallServer_Run.t_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 0;
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				strcpy(disp_name,"Inner Broadcast");
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);
			//API_TalkOn();
			API_POWER_TALK_ON();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			dx_audio_ds2dx_start();
			OpenDtTalk();
			break;
		
		default:
			break;
	}
	
}

void DxCallSceneBrd_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_RingStop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			Set_IPCallLink_Status(CLink_Idle);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOff);
			break;
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallSceneBrd_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallSceneBrd_Master:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			API_RingStop();
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallOff);
			break;
			
		case DxCallSceneBrd_Slave:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			API_LedDisplay_CallClose();
			Set_IPCallLink_Status(CLink_Idle);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOff);
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}

extern unsigned char myAddr;
void CobId29_Callback_Broadcasting(unsigned char* data)		
{
	unsigned short sourceAddr;
	unsigned short targetAddr;
	unsigned char brdType;
	
	sourceAddr = data[0];
	sourceAddr = ((sourceAddr<<8)&0xff00) + data[1];
	targetAddr = data[2]++;
	targetAddr = ((targetAddr<<8)&0xff00) + data[3];
	brdType    = data[4];
	
	if( brdType == 1 ) // 1 = ????, 2 = ????, 3 = ??1??, 4 = ??2??, 5 = ????
	{
		if( (myAddr&0xfc) == (targetAddr&0xfc) )
		{
			API_CallServer_DXInvite(DxCallSceneBrd_Slave, NULL);
		}
	}
	else if( brdType == 101 ) // 
	{
		if( (myAddr&0xfc) == (targetAddr&0xfc) )
		{
			API_CallServer_RemoteBye(DxCallSceneBrd_Slave, NULL);
		}
		
	}
}
