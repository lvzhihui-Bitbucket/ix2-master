#ifndef _obj_IxCallScene4_process_h
#define _obj_IxCallScene4_process_h


void IxCallScene4_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene4_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene4_MenuDisplay_ToInvite(void);
void IxCallScene4_MenuDisplay_ToRing(void);
void IxCallScene4_MenuDisplay_ToAck(void);
void IxCallScene4_MenuDisplay_ToBye(void);
void IxCallScene4_MenuDisplay_ToWait(void);
void IxCallScene4_MenuDisplay_ToTransfer(void);


#endif
