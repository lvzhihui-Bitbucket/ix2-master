#ifndef _obj_IxCallScene5_process_h
#define _obj_IxCallScene5_process_h


void IxCallScene5_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene5_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene5_MenuDisplay_ToInvite(void);
void IxCallScene5_MenuDisplay_ToRing(void);
void IxCallScene5_MenuDisplay_ToAck(void);
void IxCallScene5_MenuDisplay_ToBye(void);
void IxCallScene5_MenuDisplay_ToWait(void);
void IxCallScene5_MenuDisplay_ToTransfer(void);


#endif
