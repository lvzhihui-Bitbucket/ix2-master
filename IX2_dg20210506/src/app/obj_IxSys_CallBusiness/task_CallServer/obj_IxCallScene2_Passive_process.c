#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"

#include "obj_IxCallScene2_Passive_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;


void IxCallScene2_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:				 //INTERCOM:   IX-ANOTHER -> IX-MASTER
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeIntercomCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}
					
					//Load_CallRule_Para();
					
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.source_dev = Msg_CallServer->source_dev;
					Get_SelfDevInfo(Msg_CallServer->source_dev.ip_addr, &CallServer_Run.target_hook_dev); 
					CallServer_Run.timer = time(NULL);
					//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
					//CallServer_Run.para_type = Msg_CallServer->para_type;
					//CallServer_Run.para_length = Msg_CallServer->para_length;
					//memcpy(CallServer_Run.para_buff,Msg_CallServer->para_buff,CallServer_Run.para_length);
					CallServer_Run.rule_act = 0;		//czn_20190116
					CallServer_Run.call_rule = CallRule_Normal;		
					API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					
					CallServer_Run.state = CallServer_Ring;
					IxCallScene2_Passive_MenuDisplay_ToRing();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene2_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}

void IxCallScene2_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene2_Passive:					//INTERCOM:   IX-ANOTHER -> IX-MASTER
			
			if(CallServer_Run.call_type != IxCallScene2_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene2_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RingNoAck:
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_IpBeCalled_Ack(IpBeCalled_IxSys);
					
					IxCallScene2_Passive_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_DtSlaveAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpBeCalled_Cancel(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					
					break;

				case CallServer_Msg_RemoteUnlock2:
					
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;
					
				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene2_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene2_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene2_Passive_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpBeCalled_Bye(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpBeCalled_Cancel(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					break;

				case CallServer_Msg_LocalUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
	
}
void IxCallScene2_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
#if 0
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_IntercomCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene2_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene2_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:	
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						IxCallScene2_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;
				}
			break;
			
		default:
			break;
	}

#endif
	
}


void IxCallScene2_Passive_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
				
		default:
			break;
	}
}


void IxCallScene2_Passive_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42] = {0};
	char rm_nbr[11]={0};
	char *ch;

	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_talk_off();
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOn);
			//API_VideoTurnOn();
			//API_RingPlay(RING_DS1);
			API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.source_dev));		//czn_20190118
			//API_POWER_EXT_RING_ON();		//czn_20170809
			//#if !defined(PID_IXSE)
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			//#endif
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			//if(CallServer_Run.para_type == IxCallServer_ParaType1)
			

			get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, tempData+1);
			snprintf(call_record_temp.name,20,"%s",&tempData[1]);
			//strcpy(call_record_temp.name,&tempData[1]);
			tempData[0] = strlen(&tempData[1]);
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallName, tempData, tempData[0]+1);
			break;
			
		default:
			break;
	}
		
	
}

void IxCallScene2_Passive_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_RingStop();
			int try_cnt=0;
			while(try_cnt++<100&&RingGetState()!=0)
			{
				usleep(50*1000);
			}
			API_talk_on_by_unicast(CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			//API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			API_Event_By_Name("EventBECallTalk");
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallTalkOn);
			call_record_temp.property	= NORMAL;
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Passive_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeIntercomCall);	
			//czn_20190107_e
			API_talk_off();
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOff);
			API_Event_By_Name("EventBECallBye");
			break;
			
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void IxCallScene2_Passive_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeIntercomCall);	
			//czn_20190107_e
			API_talk_off();
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOff);
			API_Event_By_Name("EventBECallBye");
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}

void IxCallScene2_Passive_MenuDisplay_ToTransfer(void)
{	
	char disp_name[21] = {0};
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene2_Passive:	
			if(CallServer_Run.call_rule == CallRule_TransferIm)
			{
				API_LedDisplay_CallDivert();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//API_VideoTurnOn();
				API_RingStop();
				//API_POWER_EXT_RING_OFF();		//czn_20170809
				//#if !defined(PID_IXSE)
				call_record_temp.type 				= CALL_RECORD;
				call_record_temp.subType 			= IN_COMING;
				call_record_temp.property				= MISSED;
				call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
				call_record_temp.target_id			= 0;//CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
				strcpy(call_record_temp.name,"---");
				strcpy(call_record_temp.input,"---");
				strcpy(call_record_temp.relation, "-");
				call_record_flag = 1;
				//#endif
				//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
				{
					//API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),1,STR_UTF8);
				//	strcpy(call_record_temp.name,disp_name);
				}
			}
			else
			{
				API_LedDisplay_CallDivert();
				API_VideoTurnOff();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				API_RingStop();
				//API_POWER_EXT_RING_OFF();		//czn_20170809
			}
			break;

		default:
			break;
	}
}
