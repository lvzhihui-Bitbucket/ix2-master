#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "vdp_uart.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "task_VideoMenu.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"

#include "obj_DxCallScene3_process.h"



extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

void DxCallScene3_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			dtcaller_type = DtCaller_IntercomCall;
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT13_INTERCOM_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene3_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					API_Business_Request(Business_State_IntercomCall);
					if(Get_DtCaller_State() != 0)
					{
						API_DtCaller_ForceClose();
					}

					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
					//will_add CallServer_Run.t_addr = local_addr;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Invite(dtcaller_type);
					DxCallScene3_MenuDisplay_ToInvite();
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
			}
			break;
			
		default:
			break;
	}
		
	
	
}

void DxCallScene3_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			dtcaller_type = DtCaller_IntercomCall;
			if(CallServer_Run.call_type != DxCallScene3)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT13_INTERCOM_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene3_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					BEEP_ERROR();
					DxCallScene3_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Ring(dtcaller_type);
					DxCallScene3_MenuDisplay_ToRing();
					break;	

				case CallServer_Msg_InviteFail:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					//DxCallScene3_MenuDisplay_ToWait();
					DxCallScene3_MenuDisplay_ToInviteFail(1);
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Ack(dtcaller_type);
					DxCallScene3_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
}

void DxCallScene3_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			dtcaller_type = DtCaller_IntercomCall;
			if(CallServer_Run.call_type != DxCallScene3)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT13_INTERCOM_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene3_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					DxCallScene3_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Ack(dtcaller_type);
					DxCallScene3_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;



				default:
					break;
			}
			break;
	
		default:
			break;
	}
	
}

void DxCallScene3_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtcaller_type=0;
	uint8 SR_apply;
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			dtcaller_type = DtCaller_IntercomCall;
			if(CallServer_Run.call_type != DxCallScene3)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					SR_apply = API_SR_Request(CT13_INTERCOM_CALL);
					if (SR_apply == SR_DISABLE)	//��·æ,���ɰ���
					{
						BEEP_ERROR();	//������ʾ��
						
						DxCallScene3_MenuDisplay_ToInviteFail(0);
						
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				        	return;
					}
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout(CallServer_Run.call_type);
					}
					DxCallScene3_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_DtCaller_Bye(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(dtcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene3_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
	
}
void DxCallScene3_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
	
		default:
			break;
	}
	
}

void DxCallScene3_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
			
		default:
			break;
	}
	
}

void DxCallScene3_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
			
		default:
			break;
	}
	
}


void DxCallScene3_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallStart);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallStartting);
			break;
			
		default:
			break;
	}
}

void DxCallScene3_MenuDisplay_ToInviteFail(int fail_type)
{
	CALL_ERR_CODE errorCode;
	API_Business_Close(Business_State_IntercomCall);
	API_LedDisplay_CallClose();
	if(fail_type == 0)	//systembusy
	{
		errorCode = CALL_LINK_BUSY;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
	else	 //linkerror
	{
		errorCode = CALL_LINK_ERROR2;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_CallError, (char*)&errorCode, sizeof(errorCode));
	}
}

void DxCallScene3_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	char *ch;
	
	switch(CallServer_Run.call_type)
	{
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			API_LedDisplay_CallRing();
			API_RingPlay(RING_Intercom);//will_change
			API_POWER_EXT_RING_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallOn);
			#if 1
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= OUT_GOING;
			call_record_temp.property				= NORMAL;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			
			ch = tempData+1;
			if(Get_DxNamelist_Name_ByAddr(CallServer_Run.s_dt_addr,ch)==0)
			{
				strcpy(call_record_temp.name,ch);
				//strcpy(ch,"CallScene11");
				tempData[0] = strlen(ch);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallName, tempData, tempData[0]+1);
			}
			#endif
			break;
			
		default:
			break;
	}
		
	
}

void DxCallScene3_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			API_LedDisplay_CallTalk();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallOn);
			
			API_RingStop();
			//API_TalkOn();//API_POWER_TALK_ON();
			API_POWER_TALK_ON();
			OpenDtTalk();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			//dx_audio_ds2dx_start();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallTalkOn);
			break;
	
		default:
			break;
	}
	
}

void DxCallScene3_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallOff);
			
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_IntercomCall);
			break;
		
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallScene3_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene3:						//INTERCOM: DX_Master -> DT-ANOTHER
			API_LedDisplay_CallClose();
			//API_TalkOff();//API_POWER_TALK_OFF();
			API_RingStop();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_IntercomCall);
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}
