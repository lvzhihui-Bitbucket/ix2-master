#ifndef _obj_IxCallScene11_Passive_process_h
#define _obj_IxCallScene11_Passive_process_h


void IxCallScene11_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene11_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene11_Passive_MenuDisplay_ToInvite(void);
void IxCallScene11_Passive_MenuDisplay_ToRing(void);
void IxCallScene11_Passive_MenuDisplay_ToAck(void);
void IxCallScene11_Passive_MenuDisplay_ToBye(void);
void IxCallScene11_Passive_MenuDisplay_ToWait(void);
void IxCallScene11_Passive_MenuDisplay_ToTransfer(void);
void IxCallScene11_Passive_MenuDisplay_ToRemoteAck(void);


#endif
              