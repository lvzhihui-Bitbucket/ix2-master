#ifndef _obj_IxCallScene10_Passive_process_h
#define _obj_IxCallScene10_Passive_process_h


void IxCallScene10_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene10_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene10_Passive_MenuDisplay_ToInvite(void);
void IxCallScene10_Passive_MenuDisplay_ToRing(void);
void IxCallScene10_Passive_MenuDisplay_ToAck(void);
void IxCallScene10_Passive_MenuDisplay_ToBye(void);
void IxCallScene10_Passive_MenuDisplay_ToWait(void);
void IxCallScene10_Passive_MenuDisplay_ToTransfer(void);


#endif
