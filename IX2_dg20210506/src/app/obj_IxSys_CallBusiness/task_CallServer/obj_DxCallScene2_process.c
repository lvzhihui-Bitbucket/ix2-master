#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "vdp_uart.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"

#include "obj_DxCallScene2_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

//Global_Addr_Stru GetLocal_IpGlobalAddr(void);

void DxCallScene2_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	//uint8 slaveMaster;
	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_IntercomCall;
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(Get_DtBeCalled_State() != 0)
					{
						API_DtBeCalled_ForceClose();
					}
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					
					Load_CallRule_Para();
					
					if(CallServer_Run.call_rule != CallRule_NoDisturb)
					{
						API_Business_Request(Business_State_BeIntercomCall);
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene2_MenuDisplay_ToRing();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					else
					{
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					}
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void DxCallScene2_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}

void DxCallScene2_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	//uint8 slaveMaster;
	
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_IntercomCall;
			//ipcaller_type = IpCaller_IntercomCall;
			
			if(CallServer_Run.call_type != DxCallScene2_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						if(CallServer_Run.s_dt_addr== Msg_CallServer->partner_dt_addr)
						{
							if(!RingGetState())
							{
								API_RingPlay(RING_Intercom);
								API_POWER_EXT_RING_ON();		//czn_20170809
							}
						}
						else
						{
							CallServer_Run.state = CallServer_Wait;
							DxCallScene2_MenuDisplay_ToWait();
						}
						//CallServer_Run.state = CallServer_Wait;
						//DxCallScene2_MenuDisplay_ToWait();
					}
					else
					{
						if(CallServer_Run.s_dt_addr== Msg_CallServer->partner_dt_addr)
						{
							//CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;

				case CallServer_Msg_RingNoAck:
					
					{
							CallServer_Run.state = CallServer_TargetBye;
							//API_DtBeCalled_Bye(dtbecalled_type);
							
							{
								API_DtBeCalled_Bye(dtbecalled_type);
							}
							
							CallServer_Run.state = CallServer_Wait;
							DxCallScene2_MenuDisplay_ToWait();
					}
					break;	
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					API_RingStop();
					usleep(400000);
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_DtBeCalled_Ack(dtbecalled_type);
					//API_IpCaller_Cancel(ipcaller_type);
					DxCallScene2_MenuDisplay_ToAck();
					break;	


				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					API_DtBeCalled_Bye(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						CallServer_Run.state = CallServer_Wait;
					}
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);

					CallServer_Run.state = CallServer_Wait;
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene2_MenuDisplay_ToWait();
					break;

				

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);
					break;


				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void DxCallScene2_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_IntercomCall;
			//ipcaller_type = IpCaller_IntercomCall;
			
			if(CallServer_Run.call_type != DxCallScene2_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene2_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_DtBeCalled_Bye(dtbecalled_type);
						//API_IpCaller_Cancel(CallServer_Run.call_type);
						bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
						CallServer_Run.state = CallServer_Wait;
					}
				
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
						CallServer_Run.state = CallServer_Wait;
					}
					
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					bprintf("!!!!!!!!!!!goto wait !!!!!!!!!!!!!!!!!!!!!!!!\n");
					CallServer_Run.state = CallServer_Wait;
					DxCallScene2_MenuDisplay_ToWait();
					break;

				
				case CallServer_Msg_LocalUnlock1:
					//if(CallServer_Run.with_local_menu & 0x01)
					{
						CallServer_Run.with_local_menu |= 0x80;
						API_DtBeCalled_Unlock1(dtbecalled_type);
					}
					break;

				case CallServer_Msg_LocalUnlock2:
					//if(CallServer_Run.with_local_menu & 0x01)
					{
						CallServer_Run.with_local_menu |= 0x80;
						API_DtBeCalled_Unlock2(dtbecalled_type);
					}
					break;

				

				default:
					break;
			}
			break;
			
		
			
		default:
			break;
	}
	
	
}
void DxCallScene2_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		case DxCallScene2_Slave:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void DxCallScene2_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		case DxCallScene2_Slave:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void DxCallScene2_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	
}


void DxCallScene2_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		case DxCallScene2_Slave:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}


void DxCallScene2_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	char *ch;

	switch(CallServer_Run.call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOn);
			//API_VideoTurnOn();
			//API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
			API_POWER_EXT_RING_ON();		//czn_20170809
			#if 1
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			
			ch = tempData+1;
			if(Get_DxNamelist_Name_ByAddr(CallServer_Run.s_dt_addr,ch)==0)
			{
				strcpy(call_record_temp.name,ch);
				//strcpy(ch,"CallScene11");
				tempData[0] = strlen(ch);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallName, tempData, tempData[0]+1);
			}
			#endif
			usleep(1000*1000);
			API_RingPlay(CallServer_Run.s_dt_addr==0x3c?RING_GU:RING_Intercom);
			break;
			
		
	
		default:
			break;
	}
		
	
}

void DxCallScene2_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			
			API_LedDisplay_CallTalk();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			
			{
				//API_TalkOn();//API_POWER_TALK_ON();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallTalkOn);
				call_record_temp.property	= NORMAL;
			}
			API_POWER_TALK_ON();
			OpenDtTalk();
			printf("===============================dx_audio_ds2dx_start turn on=======================\n");
			//dx_audio_ds2dx_start();
			break;
			
		
			
		default:
			break;
	}
	
}

void DxCallScene2_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_BeIntercomCall);
			break;
			
		
			
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallScene2_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{	
		case DxCallScene2_Master:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			//cao_20181020_s
			sleep(1);
			//cao_20181020_e
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomBeCallOff);
			API_TalkOff();
			API_POWER_TALK_OFF();
			dx_audio_ds2dx_stop();
			API_Business_Close(Business_State_BeIntercomCall);
			break;
			

			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}

void DxCallScene2_MenuDisplay_ToTransfer(void)
{	
	
}
