#ifndef _obj_DxCallSceneBrd_process_h
#define _obj_DxCallSceneBrd_process_h

void DxCallSceneBrd_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxCallSceneBrd_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxCallSceneBrd_MenuDisplay_ToInvite(void);
void DxCallSceneBrd_MenuDisplay_ToRing(void);
void DxCallSceneBrd_MenuDisplay_ToAck(void);
void DxCallSceneBrd_MenuDisplay_ToBye(void);
void DxCallSceneBrd_MenuDisplay_ToWait(void);
void DxCallSceneBrd_MenuDisplay_ToInviteFail(int fail_type);

#endif
