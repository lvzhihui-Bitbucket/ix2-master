#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "task_Beeper.h"
//IX2_TEST#include "../../task_VideoMenu/obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_ThreadHeartBeatService.h"
//#include "../../task_CacheTable/task_CacheTable.h"

OS_TIMER timer_callserver;

CallServer_Run_Stru CallServer_Run;

Loop_vdp_common_buffer	vdp_callserver_mesg_queue;
Loop_vdp_common_buffer	vdp_callserver_sync_queue;

CALL_RECORD_DAT_T call_record_temp;
int call_record_flag = 0;

vdp_task_t	task_callserver;
void* vdp_callserver_task( void* arg );

int MasterOrSlaveSetting = 0;
Global_Addr_Stru DxMaster_Addr={101,0,30,0};

// lzh_20180912_s
int Get_SipConfig_Account_info(char *server, char* divert_account, char* divert_passord, char* port );
// lzh_20180912_e

void Load_CallRule_Para(void)
{
	uint8 tranferset;
	uint8 slaveMaster;
	char temp[20];
	if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
	{
		API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
		tranferset = atoi(temp);
	}
	CallServer_Run.call_rule = CallRule_Normal;
	
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, temp);
	if(memcmp(GetSysVerInfo_ms(),"01",2)!=0 ||atoi(temp))
		slaveMaster = 1;
	else
		slaveMaster = 0;

	if(slaveMaster == 0)
	{
		if(tranferset == 0)
		{
			CallServer_Run.call_rule = CallRule_Normal;
		}
		if(tranferset == 1 ||tranferset == 2)
		{
			CallServer_Run.call_rule = CallRule_NoDisturb;
		}
		if(tranferset == 3)
		{
			CallServer_Run.call_rule = CallRule_TransferNoAck;
		}
		if(tranferset == 4)
		{
			CallServer_Run.call_rule = CallRule_TransferIm;
		}
	}
	else
	{
		if(tranferset == 0 || tranferset == 3 || tranferset == 4)
		{
			CallServer_Run.call_rule = CallRule_Normal;
		}
		
		if(tranferset == 1 ||tranferset == 2)
		{
			CallServer_Run.call_rule = CallRule_NoDisturb;
		}
	}
	CallServer_Run.rule_act = 0;
	// lzh_20180912_s	
	API_Event_IoServer_InnerRead_All(DivertTime, temp);
	slaveMaster = atoi(temp);
	
	if( CallServer_Run.call_rule == CallRule_TransferIm )
		CallServer_Run.caller_divert_state	= 0x80;
	else if( CallServer_Run.call_rule == CallRule_TransferNoAck )		
		CallServer_Run.caller_divert_state	= (0x80|slaveMaster);
	else
		CallServer_Run.caller_divert_state = 0;
	
	char sip_server[50];
	char sip_divert_accout[50];
	char sip_divert_password[50];
	char sip_port[50];	
	Get_SipConfig_Account_info(sip_server,sip_divert_accout,sip_divert_password,sip_port);
	strncpy(CallServer_Run.caller_master_sip_divert.sip_server,sip_server,16);
	strncpy(CallServer_Run.caller_master_sip_divert.sip_account,sip_divert_accout,20);	
	// lzh_20180912_e
}

void CallServer_Business_Process(CALLSERVER_STRU *Msg_CallServer)
{
	printf("!!!callserver state = %d,call_type = %d recv msg = %d\n",CallServer_Run.state,Msg_CallServer->call_type,Msg_CallServer->msg_type);
	if(Msg_CallServer->msg_type==CallServer_Msg_ThreadHeartbeatReq)		//czn_20190910
	{
		RecvThreadHeartbeatReply(CallServer_Heartbeat_Mask);
		return;
	}
	if(CallServer_Run.state!=CallServer_Wait&&CallServer_Run.call_type != Msg_CallServer->call_type&&(time(NULL)-CallServer_Run.timer)<60)
		return;
	switch(CallServer_Run.state)
	{
		case CallServer_Wait:
			CallServer_Wait_Process(Msg_CallServer);
			break;
			
		case CallServer_Invite:
			CallServer_Invite_Process(Msg_CallServer);
			break;
			
		case CallServer_Ring:
			CallServer_Ring_Process(Msg_CallServer);
			break;

		case CallServer_Ack:
			CallServer_Ack_Process(Msg_CallServer);
			break;	

		case CallServer_SourceBye:
			CallServer_SourceBye_Process(Msg_CallServer);
			break;	

		case CallServer_TargetBye:
			CallServer_TargetBye_Process(Msg_CallServer);
			break;

		case CallServer_Transfer:
			CallServer_Transfer_Process(Msg_CallServer);
			break;
		case CallServer_RemoteAck:
			CallServer_RemoteAck_Process(Msg_CallServer);
			break;
		default:
			CallServer_StateInvalid_Process(Msg_CallServer);
			break;
	}
}

void CallServer_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_Wait_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Wait_Process(Msg_CallServer);
			break;

		case IxCallScene2_Active:					
			IxCallScene2_Active_Wait_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Wait_Process(Msg_CallServer);
			break;	
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Wait_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Wait_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Wait_Process(Msg_CallServer);
			break;	
		#if 0
		case IxCallScene4:
			IxCallScene4_Wait_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_Wait_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_Wait_Process(Msg_CallServer);
			break;
		case DxCallScene2_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene2_Wait_Process(Msg_CallServer);
			break;
		case DxCallScene3:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene3_Wait_Process(Msg_CallServer);
			break;
		case DxCallScene5_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene5_Wait_Process(Msg_CallServer);
			break;
		case DxCallScene6_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene6_Wait_Process(Msg_CallServer);
			break;
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_Wait_Process(Msg_CallServer);
			break;
		case IxCallScene10_Passive:
			IxCallScene10_Passive_Wait_Process(Msg_CallServer);
			break;
	
		default:
			break;
	}
		
	
	
}

void CallServer_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Invite_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Invite_Process(Msg_CallServer);
			break;	

		#if 0
		case IxCallScene4:
			IxCallScene4_Invite_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_Invite_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_Invite_Process(Msg_CallServer);
			break;
			
		case DxCallScene2_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene2_Invite_Process(Msg_CallServer);
			break;
		case DxCallScene3:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene3_Invite_Process(Msg_CallServer);
			break;
		case DxCallScene5_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene5_Invite_Process(Msg_CallServer);
			break;
		case DxCallScene6_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene6_Invite_Process(Msg_CallServer);
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_Invite_Process(Msg_CallServer);
			break;		
		case IxCallScene10_Passive:
			IxCallScene10_Passive_Invite_Process(Msg_CallServer);
			break;	
		default:
			break;
	}
}

void CallServer_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Ring_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Ring_Process(Msg_CallServer);
			break;	

		#if 0
		case IxCallScene4:
			IxCallScene4_Ring_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_Ring_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_Ring_Process(Msg_CallServer);
			break;
		case DxCallScene2_Master:					
			DxCallScene2_Ring_Process(Msg_CallServer);
			break;
		case DxCallScene3:					
			DxCallScene3_Ring_Process(Msg_CallServer);
			break;
		case DxCallScene5_Master:					
			DxCallScene5_Ring_Process(Msg_CallServer);
			break;
		case DxCallScene6_Master:					
			DxCallScene6_Ring_Process(Msg_CallServer);
			break;
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_Ring_Process(Msg_CallServer);
			break;	
		case IxCallScene10_Passive:
			IxCallScene10_Passive_Ring_Process(Msg_CallServer);
			break;	
		default:
			break;
	}
	
}

void CallServer_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Ack_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Ack_Process(Msg_CallServer);
			break;
		#if 0
		case IxCallScene4:
			IxCallScene4_Ack_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_Ack_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_Ack_Process(Msg_CallServer);
			break;

		case DxCallScene2_Master:					
			DxCallScene2_Ack_Process(Msg_CallServer);
			break;
		case DxCallScene3:					
			DxCallScene3_Ack_Process(Msg_CallServer);
			break;
		case DxCallScene5_Master:					
			DxCallScene5_Ack_Process(Msg_CallServer);
			break;	
		case DxCallScene6_Master:					
			DxCallScene6_Ack_Process(Msg_CallServer);
			break;	
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_Ack_Process(Msg_CallServer);
			break;
		case IxCallScene10_Passive:
			IxCallScene10_Passive_Ack_Process(Msg_CallServer);
			break;	
		default:
			break;
	}
	
	
}
void CallServer_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_SourceBye_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_SourceBye_Process(Msg_CallServer);
			break;

		#if 0
		case IxCallScene4:
			IxCallScene4_SourceBye_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_SourceBye_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_SourceBye_Process(Msg_CallServer);
			break;
		case DxCallScene2_Master:					
			DxCallScene2_SourceBye_Process(Msg_CallServer);
			break;
		case DxCallScene3:					
			DxCallScene3_SourceBye_Process(Msg_CallServer);
			break;	
		case DxCallScene5_Master:					
			DxCallScene5_SourceBye_Process(Msg_CallServer);
			break;
		case DxCallScene6_Master:					
			DxCallScene6_SourceBye_Process(Msg_CallServer);
			break;		
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_SourceBye_Process(Msg_CallServer);
			break;	
		case IxCallScene10_Passive:
			IxCallScene10_Passive_SourceBye_Process(Msg_CallServer);
			break;
		default:
			break;
	}
	
}

void CallServer_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_TargetBye_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_TargetBye_Process(Msg_CallServer);
			break;

		#if 0
		case IxCallScene4:
			IxCallScene4_TargetBye_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_TargetBye_Process(Msg_CallServer);
			break;	

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_TargetBye_Process(Msg_CallServer);
			break;
		case DxCallScene2_Master:					
			DxCallScene2_TargetBye_Process(Msg_CallServer);
			break;
		case DxCallScene3:					
			DxCallScene3_TargetBye_Process(Msg_CallServer);
			break;
		case DxCallScene5_Master:					
			DxCallScene5_TargetBye_Process(Msg_CallServer);
			break;
		case DxCallScene6_Master:					
			DxCallScene6_TargetBye_Process(Msg_CallServer);
			break;
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_TargetBye_Process(Msg_CallServer);
			break;	
		case IxCallScene10_Passive:
			IxCallScene10_Passive_TargetBye_Process(Msg_CallServer);
			break;	
		default:
			break;
	}
	
}

void CallServer_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					
			IxCallScene1_Active_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Transfer_Process(Msg_CallServer);
			break;

		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Transfer_Process(Msg_CallServer);
			break;

		#if 0
		case IxCallScene4:
			IxCallScene4_Transfer_Process(Msg_CallServer);
			break;
		#endif
		case IxCallScene5:
			IxCallScene5_Transfer_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			DxCallScene1_Transfer_Process(Msg_CallServer);
			break;
		case DxCallSceneBrd_Master:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
		case DxCallSceneBrd_Slave:					//INNERCALL: DT-SLAVE -> DX-MASTER -> DX-SLAVE
			DxCallSceneBrd_Transfer_Process(Msg_CallServer);
			break;
		case IxCallScene10_Passive:
			IxCallScene10_Passive_Transfer_Process(Msg_CallServer);
			break;
		default:
			break;
	}
	
}

void CallServer_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
			
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_RemoteAck_Process(Msg_CallServer);
			break;

		case DxCallScene1_Master:					
			DxCallScene1_RemoteAck_Process(Msg_CallServer);
			break;
			
		default:
			break;
	}
	
}

void CallServer_ExceptCall_Process(CALLSERVER_STRU *Msg_CallServer)
{


}
/*------------------------------------------------------------------------
					Caller_StateInvalid_Process
入口:    msg_caller

处理:	不确定状态情况处理
返回:   无
		
------------------------------------------------------------------------*/
void CallServer_StateInvalid_Process(CALLSERVER_STRU *Msg_CallServer)	
{
	//char detail[LOG_DESC_LEN + 1];
	//snprintf(detail,LOG_DESC_LEN+1,"[E]StateInvalid_S%d",Caller_Run.state);
	//API_add_log_item(CALLER_ERROR_LOG_LEVEL,"S_CALL",detail,NULL);

	//Caller_Run.state = CALLER_WAITING;
}

/*------------------------------------------------------------------------
				Caller_MsgInvalid_Process
入口:    msg_caller

处理:	对不确定msg进行处理，或不操作

		
------------------------------------------------------------------------*/
void CallServer_MsgInvalid_Process(CALLSERVER_STRU *Msg_CallServer)	
{
  
}


/*------------------------------------------------------------------------
			CallServer_Timer_Callback
------------------------------------------------------------------------*/
void CallServer_Timer_Callback(void)	//R_
{
#if 0
	CALLSERVER_STRU	send_msg;
	unsigned int			timeout_flag;
	
	timeout_flag = 0;

	CallServer_Run.timer++;	//定时累加
		
	switch (CallServer_Run.state)
	{
		case CallServer_Invite:		//等待呼叫应答
			if (CallServer_Run.timer >= CallServer_Invite_LimitTime)
			{
				timeout_flag = 1;
			}
			break;

		case CallServer_Ring:	//等待被叫摘机	
			if (CallServer_Run.timer >= CallServer_Ring_LimitTime)
			{
				timeout_flag = 1;
			}
			break;
			
		case CallServer_Ack:		//等待被叫挂机
			if (CallServer_Run.timer >= CallServer_Ack_LimitTime)
			{
				timeout_flag = 1;
			}
			break;

		case CallServer_SourceBye:		//等待结束应答
		case CallServer_TargetBye:
			if (CallServer_Run.timer >= CallServer_Bye_LimitTime)
			{
				timeout_flag = 1;
			}
			break;
			
		default:				//状态异常处理
			timeout_flag = 1;
		  	break;
	}
        
	if (timeout_flag)
	{
		send_msg.msg_head.msg_type 		= CallServer_Msg_Timeout;
		send_msg.msg_head.msg_sub_type	= 0;	
		if (OS_Q_Put(&q_callserver, &send_msg, CALLSERVER_STRU_HEAD))
		{
			Caller_Run.timer--;
			OS_RetriggerTimer(&timer_callserver);	//队列溢出, 再次触发定时, 以便下次发送消息
		}
		else 
		{
			OS_StopTimer(&timer_callserver);		//消息压入队列成功, 关闭定时
			OS_SignalEvent(TASK_EVENT_CTRL_LOCAL_MSG,&tcb_callserver);
		}			
	}
	else
	{
	  	OS_RetriggerTimer(&timer_callserver);
	}
	#endif
}


/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/


void callserver_mesg_data_process(void *Msg,int len )	//R_
{
	
	//vtk_TaskProcessEvent_Caller(&DtCaller_Obj,(CALLER_STRUCT *)Msg);
	CallServer_Business_Process((CALLSERVER_STRU*)Msg);
}

//OneCallType	OneMyCallObject;

void init_vdp_callserver_task(void)
{
	CallServer_Run.state = CallServer_Wait;
	init_vdp_common_queue(&vdp_callserver_mesg_queue, 3500, (msg_process)callserver_mesg_data_process, &task_callserver);	//czn_20190527
	init_vdp_common_queue(&vdp_callserver_sync_queue, 1000, NULL, 								  &task_callserver);
	init_vdp_common_task(&task_callserver, MSG_ID_CallServer, vdp_callserver_task, &vdp_callserver_mesg_queue, &vdp_callserver_sync_queue);

	init_vdp_dtbecalled_task();
	init_vdp_dtcaller_task();
	init_vdp_ipbecalled_task();
	init_vdp_ipcaller_task();

	printf("======init_vdp_callserver_task===========\n");	
}

void exit_callserver_task(void)
{
	
}

void* vdp_callserver_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int Get_CallScene_ByPara(uint8 para_type,uint8 para_len,uint8 *para)
{
	if(para_type == IxCallServer_ParaType1)
	{
		char para_temp[5] = {0};
		
		memcpy(para_temp,para,4);

		if(strcmp(para_temp,"0000") == 0)
		{
			return IxCallScene1_Active;
		}
		else
		{
			return IxCallScene2_Active;
		}
	}

	return -1;
}

int Load_TargetAddr_ByPara(uint8 para_type,uint8 para_len,uint8 *para)
{
#if 0
	#if 0
	CallServer_Run.tdev_nums = 2;
	CallServer_Run.t_addr_ext[0].gatewayid = 0;
	CallServer_Run.t_addr_ext[0].ip = inet_addr("192.168.1.201");
	CallServer_Run.t_addr_ext[0].rt = 0;
	CallServer_Run.t_addr_ext[0].code = 0;
	CallServer_Run.t_addr_ext[1].gatewayid = 0;
	CallServer_Run.t_addr_ext[1].ip = inet_addr("192.168.1.202");
	CallServer_Run.t_addr_ext[1].rt = 0;
	CallServer_Run.t_addr_ext[1].code = 0;
	#endif
	
	Global_Addr_Stru addr;
	CallServer_Run.tdev_nums = 0;
	
	CACHE_TABLE_T data;
	bprintf("API_GetIpByBdRmMs start:%s\n",para);
	if(para_type == IxCallServer_ParaType1)
	{
		if(API_GetIpByBdRmMs(para, &data) == 0)
		{
			CallServer_Run.tdev_nums = 1;
			CallServer_Run.t_addr_ext[0].gatewayid = 0;
			CallServer_Run.t_addr_ext[0].ip = inet_addr(data.IP);
			CallServer_Run.t_addr_ext[0].rt = 0;
			CallServer_Run.t_addr_ext[0].code = 0;
			bprintf("API_GetIpByBdRmMs ok:%s\n",para);
			return 0;
		}
		else
		bprintf("API_GetIpByBdRmMs fail:%s\n",para);
	}
	return -1;
	#endif
}
int Get_SelfParaType1Info(uint8 *dev_type,uint8 *para_buff)
{
	if(dev_type != NULL)
	{
		*dev_type = IxDevType_Ds;
	}
	
	if(para_buff != NULL)
	{
		//sprintf(para_buff,"0001%04d",GetSysVerInfo().id);
		sprintf(para_buff,GetSysVerInfo_BdRmMs());
	}
	
	return 0;
}

void Get_SelfDevInfo(int ip, Call_Dev_Info *dev_info)
{
	memset(dev_info,0,sizeof(Call_Dev_Info));
	dev_info->ip_addr = GetLocalIpByDevice(GetNetDeviceNameByTargetIp(ip));
	strcpy(dev_info->bd_rm_ms,GetSysVerInfo_BdRmMs());
	//strcpy(dev_info->name,GetSysVerInfo_name());
	snprintf(dev_info->name, 21, "%s", GetSysVerInfo_name());
}

int Get_CallScene_BySourseInfo(Call_Dev_Info dev_info)		//czn_20190506
{
	int ms;
	ms = atol(dev_info.bd_rm_ms+8);
	
	if(memcmp(dev_info.bd_rm_ms+4,"9901",4) == 0 ||memcmp(dev_info.bd_rm_ms+4,"9902",4) == 0)
	{
		return IxCallScene1_Passive;
	}
	if(memcmp(dev_info.bd_rm_ms+4,"0000",4) == 0)
	{
		//ms = atol(dev_info.bd_rm_ms+8);
		if(ms >= 1&&ms <= 48)
			return IxCallScene1_Passive;
		if(ms >= 51&&ms <= 98)
			return IxCallScene10_Passive;
	}
	
	if(memcmp(dev_info.bd_rm_ms,GetSysVerInfo_BdRmMs(),8) == 0)
	{
		//ms = atol(dev_info.bd_rm_ms+8);
		if(ms >= 1&&ms <= 32)
			return IxCallScene3_Passive;
		if(ms >= 51&&ms <= 72)
			return IxCallScene1_Passive;
	}
	if(ms >= 1&&ms <= 32)
		return IxCallScene2_Passive;
	if(ms >= 51&&ms <= 72)		
		return IxCallScene1_Passive;
}

int TargetAddrErr_Process(uint8 para_type,uint8 para_len,uint8 *para)
{
	#if 0
	CACHE_TABLE_T data;
	//bprintf("API_GetIpByBdRmMs start:%s\n",para);
	if(para_type == IxCallServer_ParaType1)
	{
		if(API_GetIpByBdRmMsFromNet(para, &data) == 0)
		{
			return 0;
		}
		
	}
	#endif
	return -1;
}

int API_CallServer_Common(uint8 msg_type, uint8 call_type, Call_Dev_Info *source_dev,int target_num,void *target_list)//R
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.call_type		= call_type;
	
	if(source_dev != NULL)
	{
		send_msg.source_dev = *source_dev;
	}

	if(call_type == IxCallScene5)
	{
		if(target_list != NULL)
		{
			sip_account_Info *psip = (sip_account_Info*)target_list;
			send_msg.sip_acc = *psip;
		}

		send_len = CallServerWithTargetList_Length(3);//czn_20190527//sizeof(CALLSERVER_STRU);
	}
	else
	{
		if(target_num > 0 && target_list!= NULL)
		{
			Call_Dev_Info* pdev = (Call_Dev_Info*)target_list;
			if(target_num > MAX_CALL_TARGET_NUM)
				target_num = MAX_CALL_TARGET_NUM;
			send_msg.target_dev_num = target_num;
			for(i = 0; i < target_num;i++)
			{
				send_msg.target_dev_list[i] = pdev[i];//target_list[i];
			}
			send_len = CallServerWithTargetList_Length(target_num);//czn_20190527		//sizeof(CALLSERVER_STRU);
			//printf("111111CALLSERVER_STRU length=%d\n",CallServerWithTargetList_Length(32));
		}
		else
		{
			send_msg.target_dev_num = 0;
			send_len = CallServerMsgExtTargetList_Length;
		}
	}
	#if 0
	if(msg_type == CallServer_Msg_Invite && call_type == 0)
	{
		int call_scene;
		call_scene = Get_CallScene_ByPara(para_type,para_len,para);
		
		if(call_scene < 1)
			return -1;

		send_msg.call_type = call_scene;
	}
	#endif
	
	if(msg_type == CallServer_Msg_Invite)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				p_vdp_common_buffer pdb = 0;
				int pop_cnt = 0;
				while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
				{
					purge_vdp_common_queue(ptask ->p_syc_buf);
				}
			}
		}
	}

	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	if(msg_type == CallServer_Msg_Invite && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			rev_len = 5;
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	

	return 0;
}

int API_CallServer_DXInvite(uint8 call_type, int partner_dt_addr)//R
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= CallServer_Msg_Invite;
	send_msg.call_type		= call_type;
	send_msg.partner_dt_addr=partner_dt_addr;
	

	
		
	send_msg.target_dev_num = 0;
	send_len = CallServerMsgExtTargetList_Length;

	ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	
	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			rev_len = 5;
			if(WaitForBusinessACK(ptask->p_syc_buf,CallServer_Msg_Invite,rev,&rev_len,1000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	

	return 0;
}

void CallServer_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}

int GetCallInfo_JudgeWithLocalMenu(void)
{
	return (CallServer_Run.with_local_menu&0x01)? 1 : 0;
}

int GetCallInfo_JudgeIsLocalUnlock(void)
{
	return (CallServer_Run.with_local_menu&0x80)? 1 : 0;
}

void Send_CallEvent(char *event_type)
{

}

extern one_vtk_table* nameListTable;

int Get_CallPartnerName_ByAddr(Global_Addr_Stru addr,char *name)
{
	#if 0
	if(CallServer_Run.call_type == IxCallScene2_Master || CallServer_Run.call_type == DxCallScene2_Slave)
	{
		addr.rt = 0;
	}
	#endif
	#if 0
	if(addr.rt == 10)
	{
		if(Get_MonResName_ByAddr(DS1_ADDRESS+addr.code,name) != 0)
		{
			sprintf(name,"DS-%d",addr.code+1);
		}
		return 0;
	}
	else if(addr.rt == 0)
	{
		int name_keyindex,logicaddr_index,i,str_len;
		char str_buff[21];
		one_vtk_dat		*precord;
		if(nameListTable != NULL)
		{
			name_keyindex = get_keyname_index(nameListTable, "NAME");
			logicaddr_index = get_keyname_index(nameListTable, "ADDR");
			//czn_20160812_e
			if(name_keyindex == -1  || logicaddr_index == -1)
				return -1;
			
			
			for(i = 0;i < nameListTable->record_cnt;i++)
			{
				precord = get_one_vtk_record_without_keyvalue(nameListTable, i);
				
				if(precord == NULL)
					return -1;
				
				str_len = 8;
				get_one_record_string( precord, logicaddr_index, str_buff, &str_len );
				str_buff[str_len] = 0;

				if(addr.code == atol(str_buff))
				{
					str_len = 8;
					get_one_record_string( precord, name_keyindex, name, &str_len );
					name[str_len] = 0;
					break;
				}
			}
		}
		
		if(nameListTable == NULL || i >= nameListTable->record_cnt)
		{
			sprintf(name,"IM%d",addr.code);
		}
		return 0;
	}
	else if(addr.rt == DX432_RT)
	{
		strcpy(name,"InnerCall");
		return 0;
	}
	else if(addr.rt >= 1 && addr.rt <= 3)
	{
		strcpy(name,"InnerCall");
		return 0;
	}
	else if(addr.rt == 16)
	{
		strcpy(name,"Guard Station");
		return 0;
	}
	else if(addr.rt == LIPHONE_RT)
	{
		strcpy(name,"PhoneCall");
		return 0;
	}
	#endif
	return -1;
}
//czn_20190118_s
Ring_Scene_Type Get_CallRingScene_ByAddr(Call_Dev_Info dev_info)
{
	Ring_Scene_Type result = RING_DS;
	DeviceTypeAndArea_T type_area;
	type_area = GetDeviceTypeAndAreaByNumber(dev_info.bd_rm_ms);

	if( type_area.type == TYPE_DS )
	{
		if( type_area.area == CommonArea )
		{
			result = RING_CDS;
		}
		else
		{
			result = RING_DS;
		}
	}
	if( type_area.type == TYPE_OS )
	{
		result = RING_OS;
	}
	else if( type_area.type == TYPE_IM || type_area.type == TYPE_GL )
	{
		if( type_area.type == TYPE_IM && type_area.area == SameRoom )
		{
			result = RING_InnerCall;
		}
		else
		{
			result = RING_Intercom;
		}
	}
	return result;
}
//czn_20190118_e
uint8 IfCallServerBusy(void)
{
	return (CallServer_Run.state != 0);//(CallServer_Run.state != 0 || Mon_Run.state != 0);
}

int API_CallserverThreadHeartbeatReq( void )	//czn_20190910
{
	VDP_MSG_HEAD 	msg;

	msg.msg_source_id 	= MSG_ID_CallServer; //GetMsgIDAccordingPid(pthread_self());	
	//video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.msg_target_id 	= MSG_ID_CallServer;
	msg.msg_type		= CallServer_Msg_ThreadHeartbeatReq;
	msg.msg_sub_type	= 0;

	push_vdp_common_queue( task_callserver.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD) );
	return 0;
}
                                                                                                        