#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_DtBeCalled.h"
//#include "task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

#include "task_Ring.h"
#include "task_Power.h"
#include "vdp_uart.h"
#include "task_Led.h"
#include "task_Ring.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
//#include "vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"

#include "obj_DxCallScene1_process.h"
#include "stack212.h"		//czn_20181114
#include "MenuSipConfig_Business.h"
#include "task_DXMonitor.h"
//#include "../../task_VideoMenu/MenuStruct/MENU_038_SipConfig.h"		//czn_20190703

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

static int remote_ack_flag=0;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

// lzh_20191107_s
int API_PushAppWakeup_connect(char *ip_str,char *acc,char *pwd,char *related_acc);
// lzh_20191107_e 

void DxCallScene1_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	//uint8 slaveMaster;
	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);

	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					if(Get_DtBeCalled_State() != 0)
					{
						API_DtBeCalled_ForceClose();
					}
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					remote_ack_flag=0;
					//API_Event_Maincall_Close();
					Load_CallRule_Para();
					
					//#ifdef DivertErrRecoverNormal
					//if((Get_SipAccount_State() == 0 && Get_SipReg_ErrCode() != 401)&&(CallServer_Run.call_rule == CallRule_TransferNoAck||CallServer_Run.call_rule == CallRule_TransferIm))
					if((Get_SipSer_State() != 1)&&(CallServer_Run.call_rule == CallRule_TransferNoAck||CallServer_Run.call_rule == CallRule_TransferIm))
					{
						CallServer_Run.call_rule = CallRule_Normal;
					}
					//#endif	
					
					if(CallServer_Run.call_rule == CallRule_Normal || CallServer_Run.call_rule == CallRule_TransferNoAck)
					{
						
						dtbecalled_type = DtBeCalled_MainCall;
						//will_add ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type);
						API_Business_Request(Business_State_BeMainCall);
						#if 0
						if(slaveMaster == 0)
						{
							API_IpCaller_Invite(ipcaller_type, NULL);
						}
						#endif
						CallServer_Run.state = CallServer_Ring;
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
						DxCallScene1_MenuDisplay_ToRing();
						
					}
					#if 1
					else if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						
						printf("11111111111111111CallRule_TransferIm\n");
						dtbecalled_type = DtBeCalled_MainCall;
						//will_add ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						CallServer_Run.s_dt_addr= Msg_CallServer->partner_dt_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type);
						
						CallServer_Run.rule_act = 1;
						CallServer_Run.state = CallServer_Ring;
						SipCfg_T *sip_cfg;
						sip_cfg = GetSipConfig();
						API_VtkMediaTrans_StartJpgPush(9,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
						API_IpCaller_Invite(IpCaller_Transfer2Sip,NULL);
						API_Business_Request(Business_State_BeMainCall);
						//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
						//API_IpBeCalled_ForceClose();
							
						DxCallScene1_MenuDisplay_ToRing();
						//DivertStateNotice_SimuDivert(CallServer_Run.source_dev);
						
						//api_ak_vi_ch_stream_encode(1,1);
					#if 0
						#if 0
						SipCfg_T sip_cfg;
						// lzh_20191107_s
						sip_cfg = GetSipConfig();
						API_PushAppWakeup_connect(sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
						// lzh_20191107_e	
						#endif
						if(CallServer_Run.divert_with_local == 0)
						{
							dtbecalled_type = DtBeCalled_MainCall;
							ipcaller_type = IpCaller_Transfer;
							
							CallServer_Run.state = CallServer_Invite;
							CallServer_Run.call_type = Msg_CallServer->call_type;
							CallServer_Run.with_local_menu = 1;
							CallServer_Run.s_addr = Msg_CallServer->partner_addr;
							//will_add CallServer_Run.t_addr = local_addr;
							CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
							API_IpCaller_Invite(ipcaller_type, NULL);
							CallServer_Run.rule_act = 1;
							CallServer_Run.state = CallServer_Transfer;
							DxCallScene1_MenuDisplay_ToTransfer();
							CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
						}
						else
						{
							dtbecalled_type = DtBeCalled_MainCall;
							ipcaller_type = IpCaller_TransferWithLocal;
				
							CallServer_Run.state = CallServer_Invite;
							CallServer_Run.call_type = Msg_CallServer->call_type;
							CallServer_Run.with_local_menu = 1;
							CallServer_Run.s_addr = Msg_CallServer->partner_addr;
							//will_add CallServer_Run.t_addr = local_addr;
							CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
							//if(slaveMaster == 0)
							{
								API_IpCaller_Invite(ipcaller_type, NULL);
							}
							
							CallServer_Run.state = CallServer_Ring;
							CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
							DxCallScene1_MenuDisplay_ToRing();
						}
					#endif
					}
					#endif
					else
					{
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					}
					break;

				default:
					break;
			}
			break;
			
		
	
		default:
			break;
	}
		
	
	
}

void DxCallScene1_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
}

void DxCallScene1_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 slaveMaster;
	unsigned char auto_mute_en = 0;
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type=IpCaller_Transfer2Sip;
			#if 0	//will_add
			if(CallServer_Run.rule_act == 1)
			{
				ipcaller_type = IpCaller_Transfer;
				API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);
			}
			else
			{
				if(CallServer_Run.divert_with_local == 1)
				{
					ipcaller_type = IpCaller_TransferWithLocal;
					API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);
				}
				else
				{
					ipcaller_type = IpCaller_MainCall;
				}
			}
			#endif
			if(CallServer_Run.call_type != DxCallScene1_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					#if 0 //will_add
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							if(!RingGetState())
							{
								API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
								API_POWER_EXT_RING_ON();		//czn_20170809
								//will_add API_IpCaller_Redail(ipcaller_type);
							}
						}
						else
						{
							CallServer_Run.state = CallServer_Wait;
							DxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					#endif
					break;
					
				case CallServer_Msg_InviteOk:
					//CallServer_Run.state = CallServer_Ring;
					//CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					//API_IpCaller_Ring(ipcaller_type,NULL);
					//DxCallScene1_MenuDisplay_ToRing();
					API_IpCaller_Ring(IpCaller_Transfer2Sip,NULL);
					break;	
					
				case CallServer_Msg_RingNoAck:
					
					//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
					#if 0	//will_add
					if(CallServer_Run.call_rule == CallRule_TransferNoAck && CallServer_Run.rule_act == 0)
					{
							CallServer_Run.timer = time(NULL);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.with_local_menu = 0;
							DxCallScene1_MenuDisplay_ToTransfer();
							printf("#############cancel local call %d\n",time(NULL));
							usleep(2000000);
							printf("$$$$$$$$$$$$$$$$$$$$$$$$start transfer %d\n",time(NULL));
							#if 0
							// lzh_20191107_s
							SipCfg_T sip_cfg;
							sip_cfg = GetSipConfig();
							API_PushAppWakeup_connect(sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
							// lzh_20191107_e	
							#endif
							API_IpCaller_Invite(IpCaller_Transfer, NULL);
							CallServer_Run.rule_act = 1;
							CallServer_Run.state = CallServer_Transfer;
							AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,DtBeCalled_Obj.BeCalled_Run.s_addr.code + 0x34,APCI_DIVERT_START_INFORM,1,0,NULL);
							usleep(200000);
							//API_SendNewCmdWithoutAck(CallServer_Run.s_addr.code+0x34,APCI_DIVERT_START_INFORM,0,NULL);
							API_DtBeCalled_Ack(dtbecalled_type);	//czn_20170815	
					}
					else
					#endif
					if( CallServer_Run.call_rule == CallRule_TransferNoAck && CallServer_Run.rule_act==0)
					{
						CallServer_Run.state = CallServer_Transfer;
						CallServer_Run.rule_act = 1;
						API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Invite(IpCaller_Transfer2Sip,NULL);
						DxCallScene1_MenuDisplay_ToTransfer();
						//DivertStateNotice_Divert(CallServer_Run.source_dev);
						SipCfg_T *sip_cfg;
						sip_cfg = GetSipConfig();
						API_VtkMediaTrans_StartJpgPush(9,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
						//api_ak_vi_ch_stream_encode(1,1);
					}
					else
					{
							CallServer_Run.state = CallServer_TargetBye;
							//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
							//if(slaveMaster == 0)
							{
								API_DtBeCalled_Bye(dtbecalled_type);
							}
							//else
							//{
							//	API_DtBeCalled_Cancel(dtbecalled_type);
							//}
							//API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
							DxCallScene1_MenuDisplay_ToWait();
					}
					break;
				case CallServer_Msg_RemoteBye:
					#if 0
					if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_DtBeCalled_Cancel(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
					}
					else if(Msg_CallServer->target_dev_num==0)
					#endif
					if( CallServer_Run.call_rule == CallRule_TransferNoAck && CallServer_Run.rule_act==1)
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_DtBeCalled_Bye(DtBeCalled_MainCall);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
					}
					break;	
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					API_RingStop();
					usleep(400000);
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 

					//CallServer_Run.t_addr.rt = DX432_RT;
					
					API_DtBeCalled_Ack(dtbecalled_type);
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					DxCallScene1_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_AppAck:
					CallServer_Run.state = CallServer_RemoteAck;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					API_IpCaller_Ack(IpCaller_Transfer2Sip,NULL);
					API_DtBeCalled_Ack(DtBeCalled_MainCall);
					
					DxCallScene1_MenuDisplay_ToRemoteAck();
					#if 0
					if(CallServer_Run.rule_act == 0)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);

						if(CallServer_Run.t_addr.rt == LIPHONE_RT)
						{
							if(auto_mute_en == 0)	
							{
								API_DtBeCalled_Ack(dtbecalled_type);
								API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
								DxCallScene1_MenuDisplay_ToAck();
							}
							else
							{
								API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
								//API_VideoTurnOff_KeepPower();//czn_20180514
								//usleep(1000000);
								//API_POWER_VIDEO_ON();
								//API_POWER_VIDEO_ON();
							}
							CallServer_Run.rule_act = 1;
						}
						else
						{
							API_DtBeCalled_Ack(dtbecalled_type);
							API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
							DxCallScene1_MenuDisplay_ToAck();
						}
						
						
					}
					else
					{
						if(auto_mute_en == 0)			//czn_20181114
						{
							CallServer_Run.state = CallServer_Ack;
							CallServer_Run.with_local_menu = 0;
							CallServer_Run.t_addr = Msg_CallServer->partner_addr;
							CallServer_Run.timer = time(NULL);
							
							API_DtBeCalled_Ack(dtbecalled_type);
							API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
							DxCallScene1_MenuDisplay_ToAck();
						}
						else
						{
							CallServer_Run.state = CallServer_Ack;		//czn_20181210
							API_POWER_VIDEO_ON();
							API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						}
					}
					#endif
					break;
				#if 0	
				case CallServer_Msg_DivertAutoMuteTalk:		//czn_20181114
					if(auto_mute_en == 1)// &&CallServer_Run.rule_act)			//czn_20181114
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						DxCallScene1_MenuDisplay_ToAck();
						
						LoadLinPhoneAudioCaptureVol();
						LoadLinPhoneAudioPlaybackVol();
						
						CallServer_Run.rule_act = 1;
					}
					break;	
				#endif
				case CallServer_Msg_RemoteAck:
				case CallServer_Msg_DtSlaveAck:
					//CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_DtBeCalled_Cancel(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;
				#if 0
				case CallServer_Msg_RemoteBye:
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						if(Msg_CallServer->partner_addr.ip == GetLocalIp())
						{
							CallServer_Run.state = CallServer_SourceBye;
							API_DtBeCalled_Cancel(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						else
						{
							if(Msg_CallServer->partner_addr.rt == LIPHONE_RT && CallServer_Run.rule_act == 0)
								return;
							
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						DxCallScene1_MenuDisplay_ToWait();
					}
					#else
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					{
						if(Msg_CallServer->partner_addr.rt == LIPHONE_RT && CallServer_Run.rule_act == 0)
							return;
						
						CallServer_Run.state = CallServer_TargetBye;
						API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					DxCallScene1_MenuDisplay_ToWait();
					#endif
					break;
				#endif
				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;
				
				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					API_RingStop();
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					usleep(400000);
					API_DtBeCalled_Unlock1(dtbecalled_type);
					break;

				case CallServer_Msg_LocalUnlock2:
					API_RingStop();
					CallServer_Run.with_local_menu |= 0x80;

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					usleep(400000);
					API_DtBeCalled_Unlock2(dtbecalled_type);
					break;
				#if 0
				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
					break;
				#endif
				#if 0
				case CallServer_Msg_DivertForceToLocal:
					if(CallServer_Run.rule_act == 1)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						//DxCallScene1_MenuDisplay_ToWait();
						//usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Ack(dtbecalled_type);
						
						//API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ack;
						//DxCallScene1_MenuDisplay_ToRing();
						DxCallScene1_MenuDisplay_DivertToLocalAck();
					}
					break;
				#endif
				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
}

void DxCallScene1_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	unsigned char auto_mute_en = 0;		//czn_20181210
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_MainCall;
			#if 0
			if(CallServer_Run.rule_act == 1)
			{
				ipcaller_type = IpCaller_Transfer;
				API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);	//czn_20181210
			}
			else
			{
				//ipcaller_type = IpCaller_MainCall;
				if(CallServer_Run.divert_with_local == 1)
				{
					ipcaller_type = IpCaller_TransferWithLocal;
					API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);
				}
				else
				{
					ipcaller_type = IpCaller_MainCall;
				}
			}
			#endif
			if(CallServer_Run.call_type != DxCallScene1_Master)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;
				#if 0	
				case CallServer_Msg_DivertAutoMuteTalk:		//czn_20181210
					if(auto_mute_en == 1 &&CallServer_Run.rule_act)			//czn_20181114
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						DxCallScene1_MenuDisplay_ToAck();
						
						LoadLinPhoneAudioCaptureVol();
						LoadLinPhoneAudioPlaybackVol();
					}
					break;		
				#endif
				case CallServer_Msg_LocalBye:
					if(CallServer_Run.with_local_menu & 0x01)
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_DtBeCalled_Bye(dtbecalled_type);
						//API_IpCaller_Cancel(CallServer_Run.call_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else 
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_DtBeCalled_Bye(dtbecalled_type);
						//API_IpCaller_Bye(ipcaller_type,&Msg_CallServer->partner_addr);
						CallServer_Run.state = CallServer_Wait;
					}
					DxCallScene1_MenuDisplay_ToWait();
					break;
				#if 0
				case CallServer_Msg_RemoteBye:
					
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else 	//if(Msg_CallServer->partner_addr.ip == CallServer_Run.t_addr.ip)
					{
						CallServer_Run.state = CallServer_TargetBye;
						API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Bye(ipcaller_type,&Msg_CallServer->partner_addr);
						CallServer_Run.state = CallServer_Wait;
					}
					DxCallScene1_MenuDisplay_ToWait();
					break;
				#endif
				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_DtBeCalled_Bye(dtbecalled_type);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					//API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtBeCalled_Bye(dtbecalled_type);
					//API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					//if(CallServer_Run.with_local_menu & 0x01)		//czn_20181027
					{
						CallServer_Run.with_local_menu |= 0x80;
						API_DtBeCalled_Unlock1(dtbecalled_type);

						//API_Beep(BEEP_TYPE_DL1);
						API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					}
					break;

				case CallServer_Msg_LocalUnlock2:
					//if(CallServer_Run.with_local_menu & 0x01)		//czn_20181027
					{
						CallServer_Run.with_local_menu |= 0x80;
						API_DtBeCalled_Unlock2(dtbecalled_type);

						//API_Beep(BEEP_TYPE_DL1);
						API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					}
					break;
				#if 0
				case CallServer_Msg_RemoteUnlock1:
					//if((!(CallServer_Run.with_local_menu & 0x01)) && memcmp(&CallServer_Run.t_addr,&Msg_CallServer->partner_addr,sizeof(Global_Addr_Stru)) == 0)
					{
						CallServer_Run.with_local_menu &= ~0x80;
						API_DtBeCalled_Unlock1(dtbecalled_type);
						API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					}
					break;

				case CallServer_Msg_RemoteUnlock2:
					//if((!(CallServer_Run.with_local_menu & 0x01)) && memcmp(&CallServer_Run.t_addr,&Msg_CallServer->partner_addr,sizeof(Global_Addr_Stru)) == 0)
					{
						CallServer_Run.with_local_menu &= ~0x80;
						API_DtBeCalled_Unlock2(dtbecalled_type);
						API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					}
					break;

				case CallServer_Msg_Redail:
					if(CallServer_Run.rule_act == 1)
					{
						API_IpCaller_Redail(ipcaller_type);
						CallServer_Run.state = CallServer_Transfer;
						CallServer_Run.timer = time(NULL); 
					}
					break;
				#endif
				#if 0
				case CallServer_Msg_DivertForceToLocal:
					if(CallServer_Run.rule_act == 1)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						//DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Ack(dtbecalled_type);
						
						//API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ack;
						//DxCallScene1_MenuDisplay_ToRing();
						DxCallScene1_MenuDisplay_DivertToLocalAck();
					}
					break;
				#endif
				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
	
}

void DxCallScene1_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer)
{
	SipCfg_T* sip_cfg;
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != DxCallScene1_Master)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							DxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_RemoteAck:
		
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_DtBeCalled_Bye(DtBeCalled_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					#if 0
					if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					else if(Msg_CallServer->target_dev_num==0)
					#endif
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_DtBeCalled_Bye(DtBeCalled_MainCall);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_DtBeCalled_Bye(DtBeCalled_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;
				#if 0
				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;
				#endif
				case CallServer_Msg_RemoteUnlock1:
					API_DtBeCalled_Unlock1(DtBeCalled_MainCall);
					break;

				case CallServer_Msg_RemoteUnlock2:
					API_DtBeCalled_Unlock2(DtBeCalled_MainCall);
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;
				#if 0
				case CallServer_Msg_Transfer:
					//printf("!!!!!!!!!!!!recv CallServer_Msg_Transfer msg\n\n\n");
					//API_IpBeCalled_ForceClose();
					CallServer_Run.rule_act = 1;
					//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					CallServer_Run.state = CallServer_Transfer;	
					IxCallScene11_Passive_MenuDisplay_ToTransfer();
					break;
				#endif	
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}
void DxCallScene1_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void DxCallScene1_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
}

void DxCallScene1_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipbecalled_type=0;
	SipCfg_T* sip_cfg;
	switch(Msg_CallServer->msg_type)//czn_20190116
	{
		case CallServer_Msg_Timeout:
			//API_DtBeCalled_ForceClose();
			//API_IpCaller_ForceClose();
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_DtBeCalled_Cancel(DtBeCalled_MainCall);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			DxCallScene1_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_InviteOk:	
			CallServer_Run.state = CallServer_Ring;
			API_IpCaller_Ring(IpCaller_Transfer2Sip,NULL);
			//sip_cfg = GetSipConfig();
			//API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
			break;
		case CallServer_Msg_InviteFail:	
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_DtBeCalled_Cancel(DtBeCalled_MainCall);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			DxCallScene1_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_LocalBye:
			CallServer_Run.state = CallServer_TargetBye;
			//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_DtBeCalled_Bye(DtBeCalled_MainCall);
			CallServer_Run.state = CallServer_Wait;
			DxCallScene1_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_RemoteBye:
			#if 0
			if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
			{
				API_IpCaller_Cancel(IpCaller_Transfer2Sip);
				API_DtBeCalled_Cancel(IpBeCalled_NewIxSys);
				CallServer_Run.state = CallServer_Wait;
				DxCallScene1_MenuDisplay_ToWait();
			}
			else if(Msg_CallServer->target_dev_num==0)
			#endif
			{
				CallServer_Run.state = CallServer_TargetBye;
				//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
				API_IpCaller_Cancel(IpCaller_Transfer2Sip);
				API_DtBeCalled_Bye(DtBeCalled_MainCall);
				CallServer_Run.state = CallServer_Wait;
				DxCallScene1_MenuDisplay_ToWait();
			}
			break;
		case CallServer_Msg_AppAck:
			CallServer_Run.state = CallServer_RemoteAck;
			CallServer_Run.with_local_menu = 1;
			CallServer_Run.timer = time(NULL); 
			API_IpCaller_Ack(IpCaller_Transfer2Sip,NULL);
			API_DtBeCalled_Ack(DtBeCalled_MainCall);
			
			DxCallScene1_MenuDisplay_ToRemoteAck();
			break;
		case CallServer_Msg_RemoteAck:
		case CallServer_Msg_DtSlaveAck:
			//CallServer_Run.with_local_menu = 0;
			//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
			API_DtBeCalled_Cancel(DtBeCalled_MainCall);
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			CallServer_Run.state = CallServer_Wait;
			DxCallScene1_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_LocalUnlock1:
			CallServer_Run.with_local_menu |= 0x80;
			API_DtBeCalled_Unlock1(DtBeCalled_MainCall);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
			break;

		case CallServer_Msg_LocalUnlock2:
			CallServer_Run.with_local_menu |= 0x80;
			API_DtBeCalled_Unlock2(DtBeCalled_MainCall);

			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
			break;

		case CallServer_Msg_RemoteUnlock1:
			API_DtBeCalled_Unlock1(DtBeCalled_MainCall);
			break;

		case CallServer_Msg_RemoteUnlock2:
			API_DtBeCalled_Unlock2(DtBeCalled_MainCall);
			break;
	}
	
	
}
#if 0
void DxCallScene1_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	unsigned char auto_mute_en;
	API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, (uint8*)&auto_mute_en);
	switch(Msg_CallServer->call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							DxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					DxCallScene1_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					#ifdef DivertErrRecoverNormal
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene1_MenuDisplay_ToRing();
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					else
					{
						API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
					}
					#else
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						if(auto_mute_en == 0)			//czn_20181114
						{
							CallServer_Run.state = CallServer_Ack;
							CallServer_Run.with_local_menu = 0;
							CallServer_Run.t_addr = Msg_CallServer->partner_addr;
							CallServer_Run.timer = time(NULL);
							
							API_DtBeCalled_Ack(dtbecalled_type);
							API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
							DxCallScene1_MenuDisplay_ToAck();
						}
						else
						{
							CallServer_Run.state = CallServer_Ack;		//czn_20181210
							API_POWER_VIDEO_ON();
							API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						}
					}
					break;

				case CallServer_Msg_DivertAutoMuteTalk:		//czn_20181114
					if(auto_mute_en == 1)			//czn_20181114
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						DxCallScene1_MenuDisplay_ToAck();

						LoadLinPhoneAudioCaptureVol();
						LoadLinPhoneAudioPlaybackVol();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						{
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						DxCallScene1_MenuDisplay_ToWait();
					}
					#else
					{
						CallServer_Run.state = CallServer_TargetBye;
						API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					DxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);

					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;
				case CallServer_Msg_DivertForceToLocal:
					if(CallServer_Run.rule_act == 1)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						//DxCallScene1_MenuDisplay_ToWait();
						//usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Ack(dtbecalled_type);
						
						//API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ack;
						//DxCallScene1_MenuDisplay_ToRing();
						DxCallScene1_MenuDisplay_DivertToLocalAck();
					}
					break;
				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
}

#endif
void DxCallScene1_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
			
	
		default:
			break;
	}
}
#if 0
void AutoUnlockProcess(void)
{
	if(GetAutoUnlock())
	{
		//sleep(2);
		#ifndef FETAS_VER
		API_DtBeCalled_Unlock1(DtBeCalled_MainCall);
		usleep(100*1000);
		#endif
		API_DtBeCalled_Unlock2(DtBeCalled_MainCall);
	}
}
#endif
void DxCallScene1_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	char name_temp2[42];
	char *ch;
	//SipCfg_T sip_cfg;
	
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			if(CallServer_Run.call_rule == CallRule_TransferNoAck||CallServer_Run.call_rule == CallRule_TransferIm)
			{
				alsa_write_all();
			 	au_alsa_app_init();

			}
			API_RingPlay(RING_DT_DS1+(CallServer_Run.s_dt_addr-DS1_ADDRESS));
			API_POWER_EXT_RING_ON();
			memo_vd_playback_stop();
			SetPipShowPos();
			API_POWER_VIDEO_ON();
			#if 0
			if(CallServer_Run.rule_act == 1)
			{
				//API_VideoTurnOff_KeepPower();		//czn_20180514
				#if 0
				call_record_temp.type 				= CALL_RECORD;
				call_record_temp.subType 			= IN_COMING;
				call_record_temp.property				= MISSED;
				call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
				call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
				strcpy(call_record_temp.name,"---");
				strcpy(call_record_temp.input,"---");
				strcpy(call_record_temp.relation, "-");
				call_record_flag = 1;
				
				if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
				{
					strcpy(call_record_temp.name,disp_name);
				}
				#endif
				

				sip_cfg = GetSipConfig();
				API_VtkMediaTrans_StartJpgPush(0,sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
				break;
			}
			#endif
			//API_POWER_BUSINESS_MUTE_ON();
			printf("API_VideoTurnOn(00000)\n");
			API_Event_IoServer_InnerRead_All(PIP_Disable,disp_name);
			if(GetWlanIpcNum()>0&&atoi(disp_name)==0)
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);				
				SetOnePipShow(0);
			}
			API_VideoTurnOn(0);
			if(GetWlanIpcNum()==0||atoi(disp_name)==1)
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
			API_LedDisplay_CallRing();
			usleep(500000);
			
			
			//API_POWER_EXT_RING_ON();		//czn_20170809
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			
			ch = tempData+1;
			if(Get_DxMonRes_Name_ByAddr(CallServer_Run.s_dt_addr,ch)==0)
			{
				sprintf(disp_name,"0x%02x",CallServer_Run.s_dt_addr);
				if( GetMonFavByAddr(disp_name, NULL, name_temp2) == 1 )
				{
					if(strcmp(name_temp2, "-")!=0)
						strcpy(ch, name_temp2);
				}
				strcpy(call_record_temp.name,ch);
				//strcpy(ch,"CallScene11");
				tempData[0] = strlen(ch);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			}
			usleep(2500*1000);
			#if	defined(PID_DX470)||defined(PID_DX482)
			if (DxCallingRecordProcess(call_record_temp.name,tempData) == 0)
			{
				strncpy(call_record_temp.relation,tempData,40);
			}
			#endif
			#if 0
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
			call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			#if 1
			usleep(1500*1000);
			if((CallServer_Run.call_rule == CallRule_TransferIm&&CallServer_Run.divert_with_local == 1))
			{
				//BEEP_PhoneCall();
				API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
			}
			else
			{
				API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
			}
			
			usleep(2000*1000);
			if(memo_video_record_start("BeCalled") == 0)
			{
				strncpy(call_record_temp.relation,one_vd_record.filename,40);
			}
			AutoUnlockProcess();
			#endif
			if((CallServer_Run.call_rule == CallRule_TransferIm&&CallServer_Run.divert_with_local == 1))
			{
				sip_cfg = GetSipConfig();
				API_VtkMediaTrans_StartJpgPush(0,sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
			}
			#endif
			break;
			
	
		default:
			break;
	}
		
	
}

void DxCallScene1_MenuDisplay_ToAck(void)
{
	//SipCfg_T sip_cfg;	
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
		#if 0
			if(CallServer_Run.rule_act == 1)
			{
				//API_VtkMediaTrans_RecordSourceChange(1);
				//API_VideoTurnOff();
				//usleep(1000000);
				
				API_POWER_VIDEO_ON();
				API_POWER_UDP_TALK_ON();
				call_record_temp.property	= NORMAL;

				if(CallServer_Run.rule_act == 1)
				{
							//czn_20190702	
					sip_cfg = GetSipConfig();
					API_VtkMediaTrans_StartJpgPush(0,sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
				}
				break;
			}
			if(CallServer_Run.divert_with_local == 1&&CallServer_Run.t_addr.rt == LIPHONE_RT)
			{
				if(API_linphonec_is_vdout_enable()==1)
					API_VideoTurnOff_KeepPower();
				usleep(1000000);
				sip_cfg = GetSipConfig();
				API_VtkMediaTrans_StartJpgPush(0,sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
				//API_VtkMediaTrans_RecordSourceChange(1);
			}
		#endif
			API_LedDisplay_CallTalk();
			BEEP_PhoneCall_Stop();
			API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			if(1) //GetCallInfo_JudgeWithLocalMenu() == 1)
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledTalkOn);
				usleep(1000000);
				
				// lzh_20220415_s
				//API_TalkOn();//API_POWER_TALK_ON();
				API_POWER_TALK_ON();
				OpenDtTalk();
				printf("===============================dx_audio_ds2dx_start turn on=======================\n");
				//dx_audio_ds2dx_start();
				// lzh_20220415_e
				call_record_temp.property	= NORMAL;		
				if(remote_ack_flag==0)
				{
					au_alsa_app_close();
				}
			}
			else
			{	
			#if 0
				//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
				if(CallServer_Run.t_addr.rt == LIPHONE_RT)
				{
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
					//API_VideoTurnOff_KeepPower();//czn_20180514
					//usleep(1000000);
					//API_POWER_VIDEO_ON();
					API_POWER_UDP_TALK_ON();
				}
				else
				{
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
				}
			#endif
			}
			break;
			
	
		default:
			break;
	}
	
}

void DxCallScene1_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			if(remote_ack_flag==0)
			{
				au_alsa_app_close();
			}
			//amp_force_disable_reset();
			API_POWER_DX_APP_TALK_OFF();
			API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			API_POWER_VIDEO_OFF();
			BEEP_PhoneCall_Stop();
			API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			//usleep(500000);
			API_VideoTurnOff();
			API_VtkMediaTrans_StopJpgPush();
			API_Business_Close(Business_State_BeMainCall);
			break;
			
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void DxCallScene1_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			if(remote_ack_flag==0)
			{
				au_alsa_app_close();
			}
			//amp_force_disable_reset();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			API_POWER_DX_APP_TALK_OFF();
			API_CloseDxRtpVideo();//api_ak_vi_ch_stream_encode(1,0);
			API_VtkMediaTrans_StopJpgPush();
			API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			API_POWER_VIDEO_OFF();
			BEEP_PhoneCall_Stop();
			API_RingStop();
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			usleep(100000);
			API_VideoTurnOff();
			API_VtkMediaTrans_StopJpgPush();

			// lzh_20220415_s
			printf("===============================turn off=======================\n");
			dx_audio_ds2dx_stop();
			dx_audio_ds2app_stop();
			// lzh_20220415_e
			API_Business_Close(Business_State_BeMainCall);
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}
void DxCallScene1_MenuDisplay_ToRemoteAck(void)
{
	int try_cnt=0;
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			remote_ack_flag=1;
			//au_service_set_type(2);
			//API_talk_on_by_type(2,CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				API_RingStop();
			//usleep(1000*1000);
			//API_POWER_EXT_RING_OFF();		//czn_20170809
			
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PhoneTalkOn);
			//call_record_temp.property	= NORMAL;
			//DivertStateNotice_DivertAck(CallServer_Run.source_dev);
			//dsmonitor_client_change_resolution(0,CallServer_Run.source_dev.ip_addr,Resolution_240P);

			// lzh_20220415_s
			API_POWER_TALK_ON();
			OpenAppTalk();
			printf("===============================dx_audio_ds2app_start turn on=======================\n");
			//dx_audio_ds2app_start();
			try_cnt=0;
			while(try_cnt++<100&&RingGetState()!=0)
			{
				usleep(50*1000);
			}
			
			dx_audio_ds2app_start();
			API_VideoTurnOffKeepPwr();
			//API_VideoTurnOff();
			//api_ak_vi_ch_stream_preview_off(0);
			//api_ak_vi_ch_stream_encode(0,0);
			usleep(50*1000);

			
			usleep(50*1000);
			// close spk
			// set usage:
			// step1:  AMP_MUTE_PowerCtrl()
			// step2:  amp_force_disable_set()
			//AMP_MUTE_PowerCtrl(0);
			//amp_force_disable_set();
			// lzh_20220415_e
			API_POWER_DX_APP_TALK_ON();
			break;
	
		default:
			break;
	}
	
}

void DxCallScene1_MenuDisplay_ToTransfer(void)
{	
	char disp_name[21] = {0};
	SipCfg_T sip_cfg;
	
	switch(CallServer_Run.call_type)
	{
		case DxCallScene1_Master:	
			
			{
				API_LedDisplay_CallDivert();
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//API_VideoTurnOff();
				BEEP_PhoneCall_Stop();
				API_RingStop();
				//API_POWER_EXT_RING_OFF();		//czn_20170809
				//API_TalkOff();

				//sip_cfg = GetSipConfig();
				//API_VtkMediaTrans_StartJpgPush(0,sip_cfg.serverIp,sip_cfg.account,sip_cfg.password,sip_cfg.divert);
			}
			break;

		default:
			break;
	}
}

#if 0
void DxCallScene1_MenuDisplay_DivertToLocalAck(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DivertToLocalAckWait);
	API_LedDisplay_CallClose();
	API_TalkOff();
	API_POWER_TALK_OFF();
	API_POWER_VIDEO_OFF();
	API_RingStop();
	API_POWER_EXT_RING_OFF();		//czn_20170809
		//API_VideoTurnOff();
	//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
		//usleep(500000);
	API_VideoTurnOff();
	usleep(1000000);
	printf("API_VideoTurnOn(11111111)\n");
	API_VideoTurnOn();
	API_POWER_VIDEO_ON();
	usleep(1200000);

	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
	if(call_record_flag == 0)
	{
		call_record_temp.type 				= CALL_RECORD;
		call_record_temp.subType 			= IN_COMING;
		call_record_temp.property				= MISSED;
		call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
		call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
		strcpy(call_record_temp.name,"---");
		strcpy(call_record_temp.input,"---");
		strcpy(call_record_temp.relation, "-");
		call_record_flag = 1;
	}

	if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
	{
		tempData[0] = strlen(disp_name);
		memcpy(&tempData[1], disp_name, tempData[0]);
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
		strcpy(call_record_temp.name,disp_name);
	}
	
	if(!strcmp(call_record_temp.relation,"-"))
	{
		if(memo_video_record_start("BeCalled") == 0)
		{
			strncpy(call_record_temp.relation,one_vd_record.filename,40);
		}
	}
	
	DxCallScene1_MenuDisplay_ToAck();
}
#endif
