/**
  ******************************************************************************
  * @file    obj_BeCalled_CdsCallSt_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2015.01.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "task_CallServer.h" 
#include "obj_BeCalled_State.h"
#include "task_DtBeCalled.h"
#include "obj_DtBeCalled_MainCall_Callback.h"
#include "define_Command.h"
#include "stack212.h"
#include "task_Power.h"
/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToRedial_MainCall(BECALLED_STRUCT *msg)
{
	;	//will_add
}

/*------------------------------------------------------------------------
						呼叫转译处理
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToTransfer_MainCall(BECALLED_STRUCT *msg)
{
	;
}

/*------------------------------------------------------------------------
							进入Ringing
1. 对小区主机: 发送TRYING
2. 对Analyze:  向Caller申请呼叫
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToRinging_MainCall(BECALLED_STRUCT *msg)	//R_
{
	//IPG_SendDS_Trying(Business_Run.call_sub_type,Business_Run.para_type,&(Business_Run.sdev),&(Business_Run.tdev));
	//IPG_Translation_ToDs(SUB_RECEIPT);
	//API_Stack_APT_Without_ACK(DtBeCalled_Obj.BeCalled_Run.s_addr.code + 0x34,SUB_RECEIPT);		//czn_20170415
	//API_POWER_VIDEOTX_ON();
}

/*------------------------------------------------------------------------
							进入ACK
1. 对小区主机: 发送OK_200(摘机)
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToAck_MainCall(BECALLED_STRUCT *msg)		//R_
{
	//API_POWER_BUSINESS_MUTE_ON();		
	//usleep(200000);
	//向小区主机发送OK_200指令(告知小区主机_呼叫成功)
	
	//IPG_Translation_ToDs(ST_TALK);
	if(CallServer_Run.state != CallServer_RemoteAck&&CallServer_Run.state != CallServer_Ack)
	{
		unsigned char ext_data[2]={0x55,0xaa};
		API_Stack_APT_Without_ACK_Data(CallServer_Run.s_dt_addr,ST_TALK,2,ext_data); 
	}
	else
		API_Stack_APT_Without_ACK(CallServer_Run.s_dt_addr,ST_TALK);
#if 0	//will_add
	if(GetPowerAudioState() != POWER_ON)
	{
		OS_Delay(100);
		API_POWER_AUDIO_ON();
	}
	API_POWER_VIDEOTX_ON();
#endif
	//音视频开启确认
//	if (GetPowerVideoState() != POWER_ON)
//	{
//		OS_Delay(100);		
//		Work_On();
//	}
}

/*------------------------------------------------------------------------
				接收到消息(分机): 结束呼叫
1. 对小区主机: 发送呼叫结束指令
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToBye_MainCall(BECALLED_STRUCT *msg)		//R_
{
	//API_POWER_BUSINESS_MUTE_ON();		
	//usleep(500000);
	//sleep(2);
	//向小区主机发送BYE_BECALLED指令(结束呼叫)
	
	//IPG_Translation_ToDs(ST_CLOSE);
	usleep(1000*1000);	//czn_20190304
	API_Stack_APT_Without_ACK(CallServer_Run.s_dt_addr,ST_CLOSE);
	//BeCalled_Run.state = BECALLED_WAITING;
	
	
//	
#if 0	//will_add
	if(GetPowerVideoTxState() == POWER_ON)
	{
		OS_Delay(100);
		API_POWER_VIDEOTX_OFF();
	}
	if(GetPowerAudioState() == POWER_ON)
	{
		OS_Delay(100);
		API_POWER_AUDIO_OFF();
	}
#endif	
	//API_CallServer_DtStateNoticeAckOk();
	DtBeCalled_Obj.BeCalled_Run.timer = 0;
	OS_StopTimer(DtBeCalled_Obj.timer_becalled);
	DtBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
}

/*------------------------------------------------------------------------
				接收到消息(): 结束呼叫					
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToWaiting_MainCall(BECALLED_STRUCT *msg)	//R_
{
#if 0	//will_add
	if(GetPowerVideoTxState() == POWER_ON)
	{
		OS_Delay(100);
		API_POWER_VIDEOTX_OFF();
	}
	if(GetPowerAudioState() == POWER_ON)
	{
		OS_Delay(100);
		API_POWER_AUDIO_OFF();
	}
#endif	
}

/*------------------------------------------------------------------------
						开锁处理
1. 对小区主机: 发送开锁指令
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToUnlock_MainCall(BECALLED_STRUCT *msg)	//R-
{
	//API_POWER_BUSINESS_MUTE_ON();
	//usleep(200000);
	if(msg->msg_type == BECALLED_MSG_UNLOCK1)
	{
		//向小区主机发送开锁1指令
		//Gateway_SendDS_Command(CdsCallIPG_Run.cds_addr, CdsCallIPG_Run.st_addr, ST_UNLOCK);
		//IPG_Translation_ToDs(ST_UNLOCK);//API_Stack_APT_Without_ACK(BeCalled_Run,ST_UNLOCK);
		API_Stack_APT_Without_ACK(CallServer_Run.s_dt_addr,ST_UNLOCK);
	}
	else if(msg->msg_type == BECALLED_MSG_UNLOCK2)
	{
		//向小区主机发送开锁2指令
		//Gateway_SendDS_Command(CdsCallIPG_Run.cds_addr, CdsCallIPG_Run.st_addr, ST_UNLOCK_SECOND);
		//API_Stack_APT_Without_ACK(CdsCallIPG_Run.cds_addr,ST_UNLOCK_SECOND);
		//IPG_Translation_ToDs(ST_UNLOCK_SECOND);//API_Stack_APT_Without_ACK(BeCalled_Run.partner_source,ST_UNLOCK_SECOND);
		API_Stack_APT_Without_ACK(CallServer_Run.s_dt_addr,ST_UNLOCK_SECOND);
	}
}

/*------------------------------------------------------------------------
					BeCalled呼叫超时(自动退出BeCalled)
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToTimeout_MainCall(BECALLED_STRUCT *msg)	//R_
{
	if(DtBeCalled_Obj.BeCalled_Run.state == BECALLED_RINGING)
	{
		API_CallServer_RingNoAck(CallServer_Run.call_type);
		DtBeCalled_Obj.BeCalled_Run.timer = 0;
		OS_RetriggerTimer(DtBeCalled_Obj.timer_becalled);
	}
	else
	{
		API_Stack_APT_Without_ACK(CallServer_Run.s_dt_addr,ST_CLOSE);
		API_CallServer_Timeout(CallServer_Run.call_type);
		DtBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
		DtBeCalled_Obj.BeCalled_Run.timer = 0;
		OS_StopTimer(DtBeCalled_Obj.timer_becalled);
	}
#if 0	//will_add	
	//API_CallServer_DtBeCalledQuit();
//
//	if(GetPowerVideoTxState() == POWER_ON)
//	{
//		OS_Delay(100);
		API_POWER_VIDEOTX_OFF();
//	}
//	if(GetPowerAudioState() == POWER_ON)
//	{
//		OS_Delay(100);
		API_POWER_AUDIO_OFF();
//	}
//	if (GetPowerVideoState() == POWER_ON)
//	{
//		Work_Off();
//	}		
#endif	
	//发消息给Analyze
	//API_CdsCallIPG_BeCalled_Timeout();
//	API_Phone_BeCalled_Timeout(2,BeCalled_Run.call_type);
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_DtBeCalled_ToError_MainCall(BECALLED_STRUCT *msg)	
{
#if 0
	switch(BeCalled_ErrorCode)
	{
		case BECALLED_ERROR_INVITEFAIL:
			//IPG_Translation_ToDs(ST_CLOSE);
			API_Stack_APT_Without_ACK(BeCalled_Run.s_addr.code + 0x34,ST_CLOSE);
			break;
		case BECALLED_ERROR_IPCALLER_QUIT:
			if(BeCalled_Run.state != BECALLED_WAITING)
			{
				//IPG_Translation_ToDs(ST_CLOSE);
				API_Stack_APT_Without_ACK(BeCalled_Run.s_addr.code + 0x34,ST_CLOSE);

				BeCalled_Run.state = BECALLED_WAITING;
				
				BeCalled_Run.timer = 0;
				OS_StopTimer(&timer_becalled);

				if(GetPowerAudioState() == POWER_ON)
				{
					OS_Delay(100);
					API_POWER_AUDIO_OFF();
				}
			}
			break;
		case BECALLED_ERROR_UINTLINK_CLEAR:
			if(BeCalled_Run.state != BECALLED_WAITING)
			{
				BeCalled_Run.state = BECALLED_WAITING;
				
				BeCalled_Run.timer = 0;
				OS_StopTimer(&timer_becalled);

				if(GetPowerAudioState() == POWER_ON)
				{
					OS_Delay(100);
					API_POWER_AUDIO_OFF();
				}
			}
			break;
	}
#endif
}

void Callback_DtBeCalled_ToForceClose_MainCall(BECALLED_STRUCT *msg)	
{
	//if(BeCalled_Run.state != BECALLED_WAITING)
	{
		//if(SR_State.in_use == TRUE)//will_add
		{
			//IPG_Translation_ToDs(ST_CLOSE);
			//API_Stack_APT_Without_ACK(DtBeCalled_Obj.BeCalled_Run.s_addr.code + 0x34,ST_CLOSE);
		}

		DtBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
		
		DtBeCalled_Obj.BeCalled_Run.timer = 0;
		OS_StopTimer(DtBeCalled_Obj.timer_becalled);
#if 0	//will_add
		if(GetPowerAudioState() == POWER_ON)
		{
			OS_Delay(100);
			API_POWER_AUDIO_OFF();
		}
#endif
	}
}

