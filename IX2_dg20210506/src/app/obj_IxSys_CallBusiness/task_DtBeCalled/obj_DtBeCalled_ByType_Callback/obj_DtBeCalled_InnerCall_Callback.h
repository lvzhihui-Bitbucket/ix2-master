#ifndef _Obj_DtBeCalled_InnerCall_Callback_H
#define _Obj_DtBeCalled_InnerCall_Callback_H

void Callback_DtBeCalled_ToRedial_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTransfer_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToRinging_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToAck_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToBye_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToWaiting_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToUnlock_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTimeout_InnerCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToError_InnerCall(BECALLED_STRUCT *msg);	
void Callback_DtBeCalled_ToForceClose_InnerCall(BECALLED_STRUCT *msg);
#endif