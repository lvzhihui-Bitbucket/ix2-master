#ifndef _Obj_DtBeCalled_InnerBrdCall_Callback_H
#define _Obj_DtBeCalled_InnerBrdCall_Callback_H

void Callback_DtBeCalled_ToRedial_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTransfer_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToRinging_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToAck_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToBye_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToWaiting_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToUnlock_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTimeout_InnerBrdCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToError_InnerBrdCall(BECALLED_STRUCT *msg);	
void Callback_DtBeCalled_ToForceClose_InnerBrdCall(BECALLED_STRUCT *msg);

#endif
