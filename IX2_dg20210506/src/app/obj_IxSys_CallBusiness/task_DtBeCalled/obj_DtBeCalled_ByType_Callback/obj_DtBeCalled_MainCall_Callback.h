#ifndef _Obj_DtBeCalled_MainCall_Callback_H
#define _Obj_DtBeCalled_MainCall_Callback_H

void Callback_DtBeCalled_ToRedial_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTransfer_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToRinging_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToAck_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToBye_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToWaiting_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToUnlock_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTimeout_MainCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToError_MainCall(BECALLED_STRUCT *msg);	
void Callback_DtBeCalled_ToForceClose_MainCall(BECALLED_STRUCT *msg);
#endif
