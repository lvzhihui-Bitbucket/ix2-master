#ifndef _Obj_DtBeCalled_IntercomCall_Callback_H
#define _Obj_DtBeCalled_IntercomCall_Callback_H

void Callback_DtBeCalled_ToRedial_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTransfer_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToRinging_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToAck_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToBye_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToWaiting_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToUnlock_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToTimeout_IntercomCall(BECALLED_STRUCT *msg);
void Callback_DtBeCalled_ToError_IntercomCall(BECALLED_STRUCT *msg);	
void Callback_DtBeCalled_ToForceClose_IntercomCall(BECALLED_STRUCT *msg);
#endif