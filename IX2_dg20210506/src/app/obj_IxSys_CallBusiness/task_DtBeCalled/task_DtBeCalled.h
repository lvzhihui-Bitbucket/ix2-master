/**
  ******************************************************************************
  * @file    task_BeCalled.h
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_DtBeCalled_H
#define _task_DtBeCalled_H

//#include "task_Survey.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"
#include "obj_BeCalled_State.h"

typedef enum
{
	DtBeCalled_MainCall,																
	DtBeCalled_IntercomCall,					
	DtBeCalled_InnerCall,
  DtBeCalled_InnerBrdCall,
}DtBeCalled_CallType_t;

extern OBJ_BECALLED_STRU DtBeCalled_Obj;

uint8 Get_DtBeCalled_State(void);
void* vdp_Dtbecalled_task( void* arg );


//#include "BSP.h"
//#include "task_Survey.h"

// Define Task Vars and Structures----------------------------------------------
	 //run.state
int API_DtBeCalled_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr);
void DtBeCalled_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);

#define API_DtBeCalled_Invite(call_type)		API_DtBeCalled_Common(BECALLED_MSG_INVITE,call_type,NULL,NULL)
#define API_DtBeCalled_Ack(call_type)					API_DtBeCalled_Common(BECALLED_MSG_ACK,call_type,NULL,NULL)
#define API_DtBeCalled_Cancel(call_type)				API_DtBeCalled_Common(BECALLED_MSG_CANCEL,call_type,NULL,NULL)
#define API_DtBeCalled_Bye(call_type)					API_DtBeCalled_Common(BECALLED_MSG_BYE,call_type,NULL,NULL)
#define API_DtBeCalled_ForceClose()					API_DtBeCalled_Common(BECALLED_MSG_FORCECLOSE,0,NULL,NULL)
#define API_DtBeCalled_Unlock1(call_type)				API_DtBeCalled_Common(BECALLED_MSG_UNLOCK1,call_type,NULL,NULL)
#define API_DtBeCalled_Unlock2(call_type)				API_DtBeCalled_Common(BECALLED_MSG_UNLOCK2,call_type,NULL,NULL)

//#define API_DtBeCalled_VideoSourceNext() 			API_DtBeCalled_Common(BECALLED_MSG_VD_SRC_NEXT , NULL,NULL,NULL)


#endif

