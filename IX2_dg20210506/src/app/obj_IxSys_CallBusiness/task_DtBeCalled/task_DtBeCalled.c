/**
  ******************************************************************************
  * @file    task_BeCalled.c
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>



//#include "task_Caller.h"
#include "obj_BeCalled_State.h"
#include "task_CallServer.h"
#include "task_DtBeCalled.h"
#include "obj_DtBeCalled_MainCall_Callback.h"
#include "obj_DtBeCalled_IntercomCall_Callback.h"
#include "obj_DtBeCalled_InnerCall_Callback.h"
#include "obj_DtBeCalled_InnerBrdCall_Callback.h"
#include "task_Ring.h"
#include "task_Power.h"
//#include "../../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "obj_menu_data.h"
#include "MENU_public.h"

//软定时_Caller定时结束

#include "OSTIME.h"

OS_TIMER timer_dtbecalled;

Loop_vdp_common_buffer	vdp_dtbecalled_mesg_queue;
Loop_vdp_common_buffer	vdp_dtbecalled_sync_queue;

vdp_task_t	task_dtbecalled;

OBJ_BECALLED_STRU DtBeCalled_Obj;

void DtBeCalled_Config_Data_Init(void);
void DtBeCalled_MenuDisplay_ToRing(void);
void DtBeCalled_MenuDisplay_ToAck(void);
void DtBeCalled_MenuDisplay_ToBye(void);
void DtBeCalled_MenuDisplay_ToWait(void);
void DtBeCalled_Timer_Callback(void);

#define DtBeCalled_Support_CallTypeNum	4

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOACK_CALLBACK[DtBeCalled_Support_CallTypeNum]={

	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToAck_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToAck_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToAck_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToAck_InnerBrdCall,},

};



const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOBYE_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToBye_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToBye_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToBye_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToBye_InnerBrdCall,},
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOREDIAL_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			NULL,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		NULL,},
	{DtBeCalled_InnerCall, 			NULL,},
	{DtBeCalled_InnerBrdCall, 		NULL,},
	
};

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TORINGING_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToRinging_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToRinging_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToRinging_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToRinging_InnerBrdCall,},
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOTRANSFER_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			NULL,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		NULL,},
	{DtBeCalled_InnerCall, 			NULL,},
	{DtBeCalled_InnerBrdCall, 		NULL,},
	
};
	

const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOUNLOCK_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToUnlock_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToUnlock_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToUnlock_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToUnlock_InnerBrdCall,},
	
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOTIMEOUT_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToTimeout_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToTimeout_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToTimeout_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToTimeout_InnerBrdCall,},
	
	
};



const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOWAITING_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToWaiting_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToWaiting_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToWaiting_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToWaiting_InnerBrdCall,},
	
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOERROR_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToError_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToError_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToError_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToError_InnerBrdCall,},
	
	
};


const STRUCT_BECALLED_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTBECALLED_TOFORCECLOSE_CALLBACK[DtBeCalled_Support_CallTypeNum]={
	//calltype					//callback
	
	{DtBeCalled_MainCall, 			Callback_DtBeCalled_ToForceClose_MainCall,},	//小区主机呼叫分机
	{DtBeCalled_IntercomCall, 		Callback_DtBeCalled_ToForceClose_IntercomCall,},
	{DtBeCalled_InnerCall, 			Callback_DtBeCalled_ToForceClose_InnerCall,},
	{DtBeCalled_InnerBrdCall, 		Callback_DtBeCalled_ToForceClose_InnerBrdCall,},
	
	
};

/*------------------------------------------------------------------------
					BeCalled子任务初始化
------------------------------------------------------------------------*/
void DtBeCalled_Obj_Init(void)	//R_
{
	//状态(WAITING == 原STANDBY)
	DtBeCalled_Obj.BeCalled_Run.state = BECALLED_WAITING;
	//DtBeCalled_Obj.BeCalled_Run.camOn = 0;
	//DtBeCalled_Obj.BeCalled_Run.camId = 0;
	DtBeCalled_Obj.timer_becalled = &timer_dtbecalled;
	DtBeCalled_Obj.task_becalled = &task_dtbecalled;
	
	DtBeCalled_Obj.Config_Data_Init = DtBeCalled_Config_Data_Init;

	DtBeCalled_Obj.MenuDisplay_ToRing= DtBeCalled_MenuDisplay_ToRing;
	DtBeCalled_Obj.MenuDisplay_ToAck = DtBeCalled_MenuDisplay_ToAck;
	DtBeCalled_Obj.MenuDisplay_ToBye = DtBeCalled_MenuDisplay_ToBye;
	DtBeCalled_Obj.MenuDisplay_ToWait = DtBeCalled_MenuDisplay_ToWait;

	DtBeCalled_Obj.support_calltype_num = DtBeCalled_Support_CallTypeNum;
	DtBeCalled_Obj.to_ack_callback	= TABLE_CALLTYPE_DTBECALLED_TOACK_CALLBACK;
	DtBeCalled_Obj.to_ring_callback = TABLE_CALLTYPE_DTBECALLED_TORINGING_CALLBACK;
	DtBeCalled_Obj.to_bye_callback = TABLE_CALLTYPE_DTBECALLED_TOBYE_CALLBACK;
	DtBeCalled_Obj.to_wait_callback = TABLE_CALLTYPE_DTBECALLED_TOWAITING_CALLBACK;
	DtBeCalled_Obj.to_forceclose_callback = TABLE_CALLTYPE_DTBECALLED_TOFORCECLOSE_CALLBACK;
	DtBeCalled_Obj.to_timeout_callback = TABLE_CALLTYPE_DTBECALLED_TOTIMEOUT_CALLBACK;
	DtBeCalled_Obj.to_error_callback = TABLE_CALLTYPE_DTBECALLED_TOERROR_CALLBACK;
	DtBeCalled_Obj.to_unclock_callback = TABLE_CALLTYPE_DTBECALLED_TOUNLOCK_CALLBACK;
	
	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_dtbecalled, DtBeCalled_Timer_Callback, 1000/25);
}



/*==============================================================================
					BeCalled_API
==============================================================================*/
/*uint8 API_BeCalled_Common(uint8 msg_type_temp, uint8 call_type_temp, 
						  uint16 address_s_temp,uint16 address_t_temp)	//R
{
	BECALLED_STRUCT	send_msg_to_BeCalled;	
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg_to_BeCalled.phone_type = PHONE_TYPE_BECALLED;
	send_msg_to_BeCalled.msg_type   = msg_type_temp;
	send_msg_to_BeCalled.address_s 	= address_s_temp;
	send_msg_to_BeCalled.address_t 	= address_t_temp;
	send_msg_to_BeCalled.call_type 	= call_type_temp;
//	if(OS_GetTaskID() == &tcb_phone)
//	{
//		vtk_TaskProcessEvent_BeCalled(&send_msg_to_BeCalled);
//		return 0;							  
//	}
	if (OS_Q_Put(&q_phone, &send_msg_to_BeCalled, sizeof(BECALLED_STRUCT)))
	{
		return 1;
	}
	
	return 0;
}
*/
/*------------------------------------------------------------------------
			Get_BeCalled_State
返回：
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_DtBeCalled_State(void)
{
	if (DtBeCalled_Obj.BeCalled_Run.state == BECALLED_WAITING)
	{
		return (0);
	}
	return (1);
}

/*------------------------------------------------------------------------
			Get_BeCalled_PartnerAddr
------------------------------------------------------------------------*/






void* vdp_dtbecalled_task( void* arg );

/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void dtbecalled_mesg_data_process(void *Msg,int len )	//R_
{
	bprintf("!!!dtbecalled state = %d,recv msg = %d\n",DtBeCalled_Obj.BeCalled_Run.state,((BECALLED_STRUCT *)Msg)->msg_type);
	vtk_TaskProcessEvent_BeCalled(&DtBeCalled_Obj,(BECALLED_STRUCT *)Msg);
}

//OneCallType	OneMyCallObject;

void init_vdp_dtbecalled_task(void)
{
	// 初始化一个呼叫对象	
	//memset( (char*)&OneMyCallObject, 0, sizeof(OneMyCallObject) );
	

	init_vdp_common_queue(&vdp_dtbecalled_mesg_queue, 1000, (msg_process)dtbecalled_mesg_data_process, &task_dtbecalled);
	init_vdp_common_queue(&vdp_dtbecalled_sync_queue, 100, NULL, 								  &task_dtbecalled);
	init_vdp_common_task(&task_dtbecalled, MSG_ID_DtBeCalled, vdp_dtbecalled_task, &vdp_dtbecalled_mesg_queue, &vdp_dtbecalled_sync_queue);
}

void exit_dtbecalled_task(void)
{
	
}

void* vdp_dtbecalled_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	DtBeCalled_Obj_Init();

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void DtBeCalled_Timer_Callback(void)
{
	BeCalled_Timer_Callback(&DtBeCalled_Obj);
}

void DtBeCalled_Config_Data_Init(void)
{
	switch(DtBeCalled_Obj.BeCalled_Run.call_type)
	{
		case DtBeCalled_MainCall:
			if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			{
				uint8 value;
				
				//API_Event_IoServer_InnerRead_All(DivertTime, (uint8*)&value);
				//DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = value;
				DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = CallServer_Run.caller_divert_state&0x7f;
			}
			else
			{
				DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			}
			DtBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			DtBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;
		case DtBeCalled_IntercomCall:
			if(CallServer_Run.call_rule == CallRule_TransferNoAck)
			{
				DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = 30;
			}
			else
			{
				DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			}
			DtBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			DtBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;
		case DtBeCalled_InnerCall:
		case DtBeCalled_InnerBrdCall:
			DtBeCalled_Obj.BeCalled_Config.limit_ringing_time = 40;
			DtBeCalled_Obj.BeCalled_Config.limit_ack_time = 90;
			DtBeCalled_Obj.BeCalled_Config.limit_bye_time = 5;
			break;
	}
}

void DtBeCalled_MenuDisplay_ToRing(void)
{

}
void DtBeCalled_MenuDisplay_ToAck(void)
{
	
}
void DtBeCalled_MenuDisplay_ToBye(void)
{
	
}
void DtBeCalled_MenuDisplay_ToWait(void)
{
	
}

void Display_Call_Source(void)
{
#if 0
	uint8 addr_temp;
	uint8 disp_name[41] = {0};	
	
	
    if(DtBeCalled_Obj.BeCalled_Run.camOn)
    {
    	snprintf(disp_name, 41, "CM%d", DtBeCalled_Obj.BeCalled_Run.camId - MEM_CAMERA_ID5 + 1);
    }
	else
	{
		Get_CallPartnerName_ByAddr(DtBeCalled_Obj.BeCalled_Run.s_addr, disp_name);
	}	
	
	API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CLEAR_STATE_W, CLEAR_STATE_H);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),CallMenuDisp_Name_Font,STR_UTF8, 0);
#endif
}

#if 0
unsigned char DtBecalled_Next(void)
{
	
#if 0
    unsigned short nextAddr;
    unsigned char camQty = 0;

    API_Event_IoServer_InnerRead_All(CAM_QUANTITY, (uint8*)&camQty);
        
    if(!DtBeCalled_Obj.BeCalled_Run.camOn)	 // video source = door station
    {
        if( camQty )
        {                
            nextAddr = MEM_CAMERA_ID5;
        }          
        else
        {
            BEEP_ERROR();
            return 1;
        }
    }
    else // video source = cammera
    {
        if( (DtBeCalled_Obj.BeCalled_Run.camId - MEM_CAMERA_ID5) < (camQty-1) )
        {
            nextAddr = DtBeCalled_Obj.BeCalled_Run.camId + 1;
        }
        else
        {
            nextAddr = DtBeCalled_Obj.BeCalled_Run.s_addr.code + 1;
        }            
    }
    
    if( VideoClient_Request(nextAddr, REASON_CODE_MON, 600) )	//申请失败
    {
        if( (nextAddr >= MEM_CAMERA_ID5) ) // camera
        {
            if(DtBeCalled_Obj.BeCalled_Run.camOn)
            {
                DtBeCalled_Obj.BeCalled_Run.camOn = 0;
                nextAddr = DtBeCalled_Obj.BeCalled_Run.s_addr.code + 1;
                VideoClient_Request(nextAddr, REASON_CODE_MON, 600);
            }
            else
            {
                BEEP_ERROR();
                return 1;
            }
        }
    }
    else
    {
        if( (nextAddr >= MEM_CAMERA_ID5) ) // camera
        {
            DtBeCalled_Obj.BeCalled_Run.camOn = 1;
            DtBeCalled_Obj.BeCalled_Run.camId = nextAddr;
        }
        else
        {
            DtBeCalled_Obj.BeCalled_Run.camOn = 0;
        }
    }
    API_VideoTurnOff();

    //显示监视的设备ID
    Display_Call_Source();
    sleep(1);
    API_VideoTurnOn();
    return 0;
#else
    unsigned short nextAddr;
    unsigned char camQty = 0;
    unsigned char index = 0;
	unsigned char disp_name[41];

	camQty = Get_DT_DS_or_CAM_Num(DT_CAMERA);

	if(!DtBeCalled_Obj.BeCalled_Run.camOn)	 // video source = door station
	{
		if( camQty )
		{			
			Get_DT_DS_or_CAM_Record(DT_CAMERA, 0, &nextAddr, disp_name);
		}		   
		else
		{
			BEEP_ERROR();
			return 1;
		}
	}
	else // video source = cammera
	{
		
		index = Get_MonRes_DT_DS_or_CAM_Index_ByAddr(DT_CAMERA, DtBeCalled_Obj.BeCalled_Run.camId);
		
		if(index < 0)
		{
			BEEP_ERROR();
			return 1;
		}
		
		if(++index >= camQty)
		{
			nextAddr = DtBeCalled_Obj.BeCalled_Run.s_addr.code + 1;
		}
		else
		{
			Get_DT_DS_or_CAM_Record(DT_CAMERA, index, &nextAddr, disp_name);
		}
	}

	if( VideoClient_Request(nextAddr, REASON_CODE_MON, 600) )	//申请失败
	{
		if( (nextAddr >= MEM_CAMERA_ID5) ) // camera
		{
            if(DtBeCalled_Obj.BeCalled_Run.camOn)
            {
                DtBeCalled_Obj.BeCalled_Run.camOn = 0;
                DtBeCalled_Obj.BeCalled_Run.camId = 0;
                nextAddr = DtBeCalled_Obj.BeCalled_Run.s_addr.code + 1;
                VideoClient_Request(nextAddr, REASON_CODE_MON, 600);
				Get_MonResName_ByAddr(nextAddr,disp_name);
            }
            else
            {
                BEEP_ERROR();
                return 1;
            }
		}
	}
	else
	{
		if( (nextAddr >= MEM_CAMERA_ID5) ) // camera
		{
			DtBeCalled_Obj.BeCalled_Run.camOn = 1;
			DtBeCalled_Obj.BeCalled_Run.camId = nextAddr;
		}
		else
		{
			DtBeCalled_Obj.BeCalled_Run.camOn = 0;
			DtBeCalled_Obj.BeCalled_Run.camId = 0;
			Get_CallPartnerName_ByAddr(DtBeCalled_Obj.BeCalled_Run.s_addr, disp_name);
		}
	}
	
	ClearFishEyeAndQswSprite();		
	
	//显示监视的设备ID
	API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CLEAR_STATE_W, CLEAR_STATE_H);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),CallMenuDisp_Name_Font,STR_UTF8, 0);
	return 0;
#endif
}

void DisplayBecallVideoName(void)
{
	unsigned char disp_name[41];

	if(DtBeCalled_Obj.BeCalled_Run.camOn)
	{
		Get_MonResName_ByAddr(DtBeCalled_Obj.BeCalled_Run.camId, disp_name);
	}
	else
	{
		Get_CallPartnerName_ByAddr(DtBeCalled_Obj.BeCalled_Run.s_addr, disp_name);
	}
	
	API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CLEAR_STATE_W, CLEAR_STATE_H);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),CallMenuDisp_Name_Font,STR_UTF8, 0);
}


#endif
int API_DtBeCalled_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	BECALLED_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	#if 0
	if(msg_type == BECALLED_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_dtbecalled.p_msg_buf, (char *)&send_msg, sizeof(BECALLED_STRUCT)) != 0)
	{
		return -1;
	}
	#if 0
	if(msg_type == BECALLED_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void DtBeCalled_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
