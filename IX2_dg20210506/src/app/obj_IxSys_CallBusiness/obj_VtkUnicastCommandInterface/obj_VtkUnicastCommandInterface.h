

#ifndef _obj_CommandInterface_h
#define _obj_CommandInterface_h


#include "RTOS.h"
#include "utility.h"
#include "task_CallServer.h"


// Define Task Vars and Structures----------------------------------------------
#define CMD_INTERFACE_ASCALLER			0
#define CMD_INTERFACE_ASBECALLED		1

#define CMD_SOURCE_UNICAST				0
#define CMD_SOURCE_MULTICAST			1
#define CMD_SOURCE_SIP					2

#define CALL_STATE_TALK					0
#define CALL_STATE_BYE					1

#define CALL_STATE2_SIMU_DIVERT			0
#define CALL_STATE2_DIVERT					1
#define CALL_STATE2_DIVERT_RING			2
#define CALL_STATE2_DIVERT_ACK			3
#define CALL_STATE2_DIVERT_FAIL			4

#define VTK_CMD_LINK_1001				0x1001
#define VTK_CMD_UNLINK_1002				0x1002
#define VTK_CMD_MON_1004				0x1004
#define VTK_CMD_DIAL_1003				0x1003
#define VTK_CMD_STATE_1005				0x1005
#define VTK_CMD_NT_1006					0x1006
#define VTK_CMD_EXIT_1008				0x1008
#define VTK_CMD_UNLOCK_E003			0xE003			//czn_20161008
#define VTK_CMD_REDAIL_E011			0xE011			//czn_20171030
#define	VTK_CMD_TRANSFER_E012		0xE012
#define VTK_CMD_DIAL2_1023				0x1023
#define VTK_CMD_STATE2_1025			0x1025

#define VTK_CMD_OLDMULTALK_REQ			0x1009

#define VTK_CMD_LISTENTALK_REQ		0XE013
    
#define VTK_CMD_LINK_REP_1081			0x1081
#define VTK_CMD_UNLINK_REP_1082			0x1082
#define VTK_CMD_MON_REP_1084			0x1084
#define VTK_CMD_DIAL_REP_1083			0x1083
#define VTK_CMD_STATE_REP_1085			0x1085
#define VTK_CMD_NT_REP_1086				0x1086
#define VTK_CMD_EXIT_REP_1088			0x1088
#define VTK_CMD_UNLOCK_REP_E083		0xE083			//czn_20161008
#define VTK_CMD_TRANSFER_REP_E092		0xE092
#define VTK_CMD_DIAL2_REP_10A3			0x10A3

#define VTK_CMD_OLDMULTALK_REP			0x1089
#define VTK_CMD_LISTENTALK_REP		0XE093

// lzh_20160503_s
//联网呼叫类型定义
#define DsAndGl					0x30		//单元主机(小区主机)与中心	
#define DmAndSt					0x31		//小区主机与分机
#define GlAndSt					0x32		//中心与分机
#define StAndSt					0x33		//分机与分机
#define GlMonMr					0x34		//中心监视主机
#define GlAndGl					0x35		//中心与中心
#define GLMonMr_new				0x38		// 新增Mon_type,区别c5-ipc
//连接回复包
#define LINK_BUSY				0x30		//连线应答_忙
#define LINK_SUCC				0x31		//连线应答_成功
#define LINK_FAIL				0x32		//连线应答_失败
#define LINK_ERROR				0x33		//连线应答_错误
#define LINK_TX_FAIL			0x34		//中心连线_通信无应答
#define LINK_ZLB_FAIL			0x35		//中心连线_无指令回复包

//主机地址定义
#define  DS1_ADD_L				0xD0		//主机0或主机1地址_低字节	
#define  DS2_ADD_L				0xD1		//主机2地址_低字节
#define  DS3_ADD_L				0xD2		//主机3地址_低字节
#define  DS4_ADD_L				0xD3		//主机4地址_低字节
#define  DS5_ADD_L				0xD4		//主机5地址_低字节
#define  DS6_ADD_L				0xD5		//主机6地址_低字节
#define  DS7_ADD_L				0xD6		//主机7地址_低字节
#define  DS8_ADD_L				0xD7		//主机8地址_低字节
//中心地址定义(房号为0000)
#define  GL_ADD_H				0x00		//高字节
#define  GL_ADD_L				0x39		//低字节
// lzh_20160503_e

//czn_20160516_s
typedef struct
{
	uint8 state;
	uint8 call_type;
	int    sn_save;
}CMD_INTERFACE_RUN_STRU;



#pragma pack(1)

typedef struct
{
	uint8 dev_type;
	uint8 para_type;
	uint8 para_buff[MAX_CALLSERVER_PARABUF_LEN+1];
}DevInfo_t;


typedef struct
{
	unsigned short		call_type;			// 呼叫类型
	unsigned short	 	call_code;			// 呼叫状态
	unsigned short 		rspstate;			// 被叫方返回类型 0x00:  正常, 0x01：设备忙，禁止呼叫, 0x02：免扰，禁止呼叫, 0x80：立即转呼, 0x80-0xF0：延时转呼，时间为1-200秒, 0xF1-0xFF：保留
	Call_Dev_Info 		source_dev;			// 主叫方设备信息
	Call_Dev_Info 		target_dev;			// 被叫方设备信息
	sip_account_Info 	master_sip_divert;	// 被叫方转呼sip帐号
}VtkUnicastCmd_Stru;

#define VtkUnicastCmd_Stru_WithoutSip_Length		((int)&(((VtkUnicastCmd_Stru*)0)->master_sip_divert.sip_server[0]))		//czn_20190610

//czn_20190216_s
typedef struct
{
	Call_Dev_Info 		target_dev;			// 被叫方设备信息
}CmdCallbackReq_Stru;
typedef struct
{
	int result;			
}CmdCallbackRsp_Stru;
//czn_20190216_e

//czn_20160516_e
#pragma pack()
// Define API-------------------------------------------------------------------
int API_Cmd_Analyzer(uint8 source_type,unsigned short node_id,int source_ip,int cmd,int sn,uint8 *pkt , uint16 len);	//czn_20170328
int Send_InviteCmd(void);	
int Send_RingCmd(void);
int Send_200okCmd(void);
int Send_ByeCmd(void);
int Send_AckCmd(void);
//czn_020160422
int API_Get_Partner_Calllink_NewCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link);
int Send_UnlinkCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link);

void Set_CmdInterface_State(uint8 newstate);
	#define Set_CmdInterface_AsCaller() 		Set_CmdInterface_State(CMD_INTERFACE_ASCALLER)
	#define Set_CmdInterface_AsBecalled() 	Set_CmdInterface_State(CMD_INTERFACE_ASBECALLED)
int Send_UnlockCmd(Global_Addr_Stru *t_addr,unsigned char locknum);		//czn_20161008	

int Send_DivertStateCmd_VtkUnicast(Call_Dev_Info target_info,int state);
#define DivertStateNotice_SimuDivert(target_info)	Send_DivertStateCmd_VtkUnicast(target_info,CALL_STATE2_SIMU_DIVERT)
#define DivertStateNotice_Divert(target_info)		Send_DivertStateCmd_VtkUnicast(target_info,CALL_STATE2_DIVERT)
#define DivertStateNotice_DivertRing(target_info)	Send_DivertStateCmd_VtkUnicast(target_info,CALL_STATE2_DIVERT_RING)
#define DivertStateNotice_DivertAck(target_info)		Send_DivertStateCmd_VtkUnicast(target_info,CALL_STATE2_DIVERT_ACK)
#define DivertStateNotice_DivertFail(target_info)		Send_DivertStateCmd_VtkUnicast(target_info,CALL_STATE2_DIVERT_FAIL)

#endif
		
