/**
  ******************************************************************************
  * @file    task_CommandInterface.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.04.19
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_VtkUnicastCommandInterface.h"
#include "obj_IP_Call_Link.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "task_CallServer.h"
#include "task_IoServer.h"
#include "task_Beeper.h"
#ifdef IX50_CALLSERVER
#include "task_Unlock.h"
#endif

CMD_INTERFACE_RUN_STRU	CmdInterface_Run;
//czn_020160422
//czn_20160516

void Set_CmdInterface_State(uint8 newstate)
{
	CmdInterface_Run.state = newstate;
}

int API_VtkUnicastCmd_Analyzer(unsigned short node_id,int source_ip,int cmd,int sn,uint8 *pkt , uint16 len)	//czn_20170328
{
	int rev = -1;
	VtkUnicastCmd_Stru *VtkUnicastCmd = (VtkUnicastCmd_Stru *)pkt;
	uint16 vtkcmd;
	Global_Addr_Stru gaddr,taddr;
	uint8 intercomen;
	Call_Dev_Info self_info;
	int i;
	int callscene;
	uint8 tranferset;
	char temp[20];
	switch(cmd)
	{
		case VTK_CMD_DIAL_1003:
			
				//if(CallServer_Run.state == CallServer_Wait)
				{
					gaddr.gatewayid 	= node_id;	
					gaddr.ip			= source_ip;
					
					if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
					{
						
						API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
						tranferset = atoi(temp);
					}
					if(tranferset== 1 || tranferset == 2)
					//if( !GetDoNotDisturbState() ) 	//���������Ź���
					{
						break;
					}
					API_Event_IoServer_InnerRead_All(BabyRoomEn, temp);
					if(atoi(temp)==1)
					{
						break;
					}	
					
					Get_SelfDevInfo(source_ip, &self_info);
					if(strcmp(VtkUnicastCmd->target_dev.bd_rm_ms,self_info.bd_rm_ms) == 0)
					{
						if((callscene = Get_CallScene_BySourseInfo(VtkUnicastCmd->source_dev))> 0)
						{
							linphone_becall_cancel();
							//czn_20190107_s
							if(callscene == IxCallScene1_Passive)	//czn_20190111
							{
								if(API_Business_Request(Business_State_BeMainCall) == 0 && (CallServer_Run.state != CallServer_Wait || CallServer_Run.source_dev.ip_addr != source_ip))
									break;
							}
							else if(callscene == IxCallScene2_Passive||callscene ==IxCallScene10_Passive)
							{
								//char temp[20];
							
								API_Event_IoServer_InnerRead_All(IntercomEnable, temp);
								if(!atoi(temp))
								{
									break;
								}
								
								if(API_Business_Request(Business_State_BeIntercomCall) == 0)
									break;
							}
							else if(callscene == IxCallScene3_Passive)
							{
								if(API_Business_Request(Business_State_BeInnerCall) == 0)
									break;
							}
							//czn_20190107_e
							SetBusinessPartner(VtkUnicastCmd->source_dev.bd_rm_ms);
							CallServer_Run.callee_rsp_command_id = sn;
							API_CallServer_BeInvite(callscene,&VtkUnicastCmd->source_dev);
							//usleep(1000*1000);
							//API_CallServer_LocalAck(CallServer_Run.call_type);
							API_Event_By_Name("EventBECall");
						}
					}
					
					rev = 0;
				}
				
			break;
		case VTK_CMD_DIAL2_1023:
			if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
			{
				//char temp[20];
				API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
				tranferset = atoi(temp);
			}
			if(tranferset== 1 || tranferset == 2)
			//if( !GetDoNotDisturbState() ) 	//���������Ź���
			{
				break;
			}
			API_Event_IoServer_InnerRead_All(BabyRoomEn, temp);
			if(atoi(temp)==1)
			{
				break;
			}	
			
			Get_SelfDevInfo(source_ip, &self_info);
			if(strcmp(VtkUnicastCmd->target_dev.bd_rm_ms,self_info.bd_rm_ms) == 0)
			{
				if((callscene = Get_CallScene_BySourseInfo(VtkUnicastCmd->source_dev))> 0)
				{
					linphone_becall_cancel();
					//czn_20190107_s
					if(callscene == IxCallScene1_Passive)	//czn_20190111
					{
						if(API_Business_Request(Business_State_BeMainCall) == 0 && (CallServer_Run.state != CallServer_Wait || CallServer_Run.source_dev.ip_addr != source_ip))
							break;
						callscene=IxCallScene11_Passive;
					}
					
					//czn_20190107_e
					SetBusinessPartner(VtkUnicastCmd->source_dev.bd_rm_ms);
					CallServer_Run.callee_rsp_command_id = sn;
					API_CallServer_BeInvite(callscene,&VtkUnicastCmd->source_dev);
					//usleep(1000*1000);
					//API_CallServer_LocalAck(CallServer_Run.call_type);
					API_Event_By_Name("EventBECall");
				}
			}
			
			rev = 0;
			break;

		case VTK_CMD_DIAL_REP_1083:
			if(CallServer_Run.state == CallServer_Invite )//&& CallServer_Run.t_addr.ip == source_ip)
			{
				for(i = 0;i < CallServer_Run.tdev_nums; i ++)
				{
					if(CallServer_Run.target_dev_list[i].ip_addr == source_ip)
					{
						API_CallServer_InviteOk(CallServer_Run.call_type);
						break;
					}
				}
				
				rev = 0;
			}
			
			break;

		case VTK_CMD_STATE_1005:
			if(VtkUnicastCmd->call_code == CALL_STATE_TALK)
			{
				printf("11111111111%s:%d\n",__func__,__LINE__);
				if(CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Ring)
				{

					API_CallServer_RemoteAck(CallServer_Run.call_type,&VtkUnicastCmd->target_dev);
					rev = 0;
				}
			}
			else if(VtkUnicastCmd->call_code == CALL_STATE_BYE)
			{
				printf("11111111111%s:%d\n",__func__,__LINE__);
				if(CallServer_Run.state != CallServer_Wait)
				{
					printf("11111111111%s:%d\n",__func__,__LINE__);
					if(VtkUnicastCmd->source_dev.ip_addr == source_ip)
					{
						API_CallServer_RemoteBye(CallServer_Run.call_type,&VtkUnicastCmd->source_dev);
					}
					else
					{
						API_CallServer_RemoteBye(CallServer_Run.call_type,&VtkUnicastCmd->target_dev);
					}
					rev = 0;
				}
			}
			break;
		case VTK_CMD_STATE2_1025:
			if(CallServer_Run.state != CallServer_Wait)
			{
				if(VtkUnicastCmd->call_code == CALL_STATE2_SIMU_DIVERT)
				{

				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT)
				{

				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT_RING)
				{

				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT_ACK)
				{

				}
			}
			break;
			
		case VTK_CMD_STATE_REP_1085:
			if(CallServer_Run.state == CallServer_SourceBye || CallServer_Run.state == CallServer_TargetBye)
			{
				if(VtkUnicastCmd->source_dev.ip_addr == source_ip)
					API_CallServer_ByeOk(CallServer_Run.call_type, &VtkUnicastCmd->source_dev);
				else
					API_CallServer_ByeOk(CallServer_Run.call_type, &VtkUnicastCmd->target_dev);
				rev = 0;
			}
			break;

		case VTK_CMD_UNLOCK_E003:
			#if 0
			if(CallServer_Run.state != CallServer_Wait)
			{
				gaddr.gatewayid 	= node_id;	
				gaddr.ip			= source_ip;
				gaddr.rt			= 0;
				gaddr.code		= 0;
				if(VtkUnicastCmd->call_code == 0)
				{
					API_CallServer_RemoteUnlock1(CallServer_Run.call_type, &gaddr);
				}
				else if(VtkUnicastCmd->call_code == 1)
				{
					API_CallServer_RemoteUnlock2(CallServer_Run.call_type, &gaddr);
				}
			}
			#endif
			#ifdef IX50_CALLSERVER
			if(VtkUnicastCmd->call_code == 0)
			{
				BEEP_CONFIRM();
				API_Unlock1(MSG_UNLOCK_SOURCE_KEY);	
			}
			else
			{
				BEEP_CONFIRM();
				API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
			}
			#endif
			break;

		case VTK_CMD_TRANSFER_E012:
			printf("!!!!!!!!!!!!recv VTK_CMD_TRANSFER_E012 cmd\n\n\n");
			API_CallServer_Transfer(CallServer_Run.call_type);
			break;

		case CMD_CALL_MON_LINK_MODIFY_REQ:
			Recv_CallLinkMondifyReq(pkt,len);
			break;			
	}
			
			
	return rev;
}

// lzh_20180912_s
// ���з�����
int Send_InviteCmd_VtkUnicast_master(Call_Dev_Info target_info)
{
#if 0	
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	//VtkUnicastCmd.cmd_type 		= VTK_CMD_DIAL_1003>>8;
	//VtkUnicastCmd.cmd_sub_type 	= VTK_CMD_DIAL_1003 & 0xff; 
	VtkUnicastCmd.source_idh		= 0;
	VtkUnicastCmd.source_idl		= 0;
	VtkUnicastCmd.call_code		= 0;
	VtkUnicastCmd.target_idh		= 0;
	VtkUnicastCmd.target_idl		= 0;
	
	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = target_info;
 
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL_1003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
#else
	VtkUnicastCmd_Stru VtkUnicastCmd_req;
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;
	
	int	response_len = sizeof(VtkUnicastCmd_Stru);

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_req.source_dev 	= CallServer_Run.source_dev;
	VtkUnicastCmd_req.target_dev 	= target_info;
	
	if( api_udp_c5_ipc_send_req(target_info.ip_addr, VTK_CMD_DIAL_1003, (char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru), (char*)&VtkUnicastCmd_rsp, &response_len) == 0 )
	{		
		CallServer_Run.callee_divert_state 		= VtkUnicastCmd_rsp.rspstate;
		CallServer_Run.callee_master_sip_divert = VtkUnicastCmd_rsp.master_sip_divert;
		return 0;
	}
	else
	{
		return -1;
	}
#endif 
}

// ���з�����
int Send_InviteCmd_VtkUnicast_slave(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd_req;

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_req.source_dev 	= CallServer_Run.source_dev;
	VtkUnicastCmd_req.target_dev 	= target_info;
	
	return api_udp_c5_ipc_send_data_without_ack(target_info.ip_addr,VTK_CMD_DIAL_1003,(char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru));
}

// ���з�Ӧ��
int Send_RingCmd_VtkUnicast(Call_Dev_Info target_info)
{
	//czn_20190412_s
	if(Send_AutoTestCall_Rsp(target_info) == 1)
		return 0;
	//czn_20190412_e
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;

	memset( (char*)&VtkUnicastCmd_rsp, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_rsp.source_dev 		= CallServer_Run.source_dev;
	VtkUnicastCmd_rsp.target_dev 		= CallServer_Run.target_hook_dev;
	VtkUnicastCmd_rsp.rspstate			= CallServer_Run.caller_divert_state;
	VtkUnicastCmd_rsp.master_sip_divert = CallServer_Run.caller_master_sip_divert;

	// ����Ҫ��ͬ��Ӧ���id
	//api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL_REP_1083,(char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru));
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		//czn_20190610
		api_udp_c5_ipc_send_rsp( target_info.ip_addr, VTK_CMD_DIAL_REP_1083, CallServer_Run.callee_rsp_command_id, (char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru) );
	else
		api_udp_c5_ipc_send_rsp( target_info.ip_addr, VTK_CMD_DIAL_REP_1083, CallServer_Run.callee_rsp_command_id, (char*)&VtkUnicastCmd_rsp,VtkUnicastCmd_Stru_WithoutSip_Length);
	return 0;
}

int Send_Ring2Cmd_VtkUnicast(Call_Dev_Info target_info)
{
	//czn_20190412_s
	if(Send_AutoTestCall_Rsp(target_info) == 1)
		return 0;
	//czn_20190412_e
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;

	memset( (char*)&VtkUnicastCmd_rsp, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_rsp.source_dev 		= CallServer_Run.source_dev;
	VtkUnicastCmd_rsp.target_dev 		= CallServer_Run.target_hook_dev;
	VtkUnicastCmd_rsp.rspstate			= CallServer_Run.caller_divert_state;
	VtkUnicastCmd_rsp.master_sip_divert = CallServer_Run.caller_master_sip_divert;

	// ����Ҫ��ͬ��Ӧ���id
	//api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL_REP_1083,(char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru));
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		//czn_20190610
		api_udp_c5_ipc_send_rsp( target_info.ip_addr, VTK_CMD_DIAL2_REP_10A3, CallServer_Run.callee_rsp_command_id, (char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru) );
	else
		api_udp_c5_ipc_send_rsp( target_info.ip_addr, VTK_CMD_DIAL2_REP_10A3, CallServer_Run.callee_rsp_command_id, (char*)&VtkUnicastCmd_rsp,VtkUnicastCmd_Stru_WithoutSip_Length);
	return 0;
}


int Send_TalkCmd_VtkUnicast(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	VtkUnicastCmd.call_code			= CALL_STATE_TALK;	
	VtkUnicastCmd.source_dev 		= CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev 		= CallServer_Run.target_hook_dev;

	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}

int Send_DivertStateCmd_VtkUnicast(Call_Dev_Info target_info,int state)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	VtkUnicastCmd.call_code			= state;	
	VtkUnicastCmd.source_dev 		= CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev 		= CallServer_Run.target_hook_dev;

	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE2_1025,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}
int Send_ByeCmd_VtkUnicast(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_code		= CALL_STATE_BYE;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;

	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}

int Send_ByeCmd_VtkUnicast2(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_code		= CALL_STATE_BYE;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;

	api_udp_c5_ipc_send_data_without_ack(target_info.ip_addr,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}

int Send_AckCmd_VtkUnicast(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.rspstate			= 1;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;
	//api_udp_c5_ipc_send_rsp_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,CmdInterface_Run.sn_save,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}
//czn_020160422_s
int API_Get_Partner_Calllink_NewCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link)
{
#if 0
	VtkUnicastCmd_Stru VtkUnicastCmd;
	int rlen = sizeof(_IP_Call_Link_Run_Stru);
	int target_ip;

	target_ip = target_addr->ip;

	//VtkUnicastCmd.cmd_type 		= VTK_CMD_LINK_1001>> 8;
	//VtkUnicastCmd.cmd_sub_type 	= VTK_CMD_LINK_1001& 0xff; 

	// lzh_20160503_s
	VtkUnicastCmd.call_type		= DsAndGl;		// ��ͬ�� GlMonMr ���ɣ����Ӻͺ��е�link��Ҫ����
	// lzh_20160503_e
	VtkUnicastCmd.target_idh		= target_addr->rt;
	VtkUnicastCmd.target_idl		= target_addr->code;
	char *rbuf = (char *)malloc(rlen);

	if(rbuf == NULL)
	{
		return -1;
	}
	//if(api_udp_c5_ipc_send_req1(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru))==-1)
	// lzh_20160512_s
	//if(api_udp_c5_ipc_send_req(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,rbuf,&rlen) == -1)
	//czn_20170328	if(api_udp_c5_ipc_send_req(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),rbuf,&rlen) == -1)
	if(api_udp_c5_ipc_send_req_by_nodeid(target_addr->gatewayid,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),rbuf,&rlen) == -1)
	// lzh_20160512_e
	{
		bprintf("api_udp_c5_ipc_send_req\n");
		free(rbuf);
		return -1;
	}
	
	memcpy(partner_call_link,rbuf,sizeof(_IP_Call_Link_Run_Stru));

	free(rbuf);
#endif
	return 0;
	
	
}

int Send_UnlinkCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link)		
{
#if 0
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	if(partner_call_link->Status == CLink_Idle)
	{
		return -1;
	}
	
	//VtkUnicastCmd.cmd_type 		= VTK_CMD_UNLINK_1002>> 8;
	//VtkUnicastCmd.cmd_sub_type 	= VTK_CMD_UNLINK_1002& 0xff;
	VtkUnicastCmd.call_type		= DsAndGl;
	// lzh_20160512_s
	//if(api_udp_c5_ipc_send_req(target_addr->ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
	//czn_20170328	if(api_udp_c5_ipc_send_req(target_addr->ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
	if(api_udp_c5_ipc_send_req_by_nodeid(target_addr->gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
	// lzh_20160512_e
	{
		return -1;
	}
	if(partner_call_link->Status == CLink_AsBeCalled)
	{
		bprintf("Status == CLink_AsBeCalled %08x,%08x\n",partner_call_link->BeCalled_Data.Call_Source.ip,GetLocalIp());
		//if(partner_call_link->BeCalled_Data.Call_Source.ip !=GetLocalIp())
		{
			// lzh_20160512_s		
			//if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->BeCalled_Data.Call_Source.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e			
			{
				return -1;
			}

		}
	}

	if(partner_call_link->Status == CLink_AsCaller)
	{
		bprintf("Status == CLink_AsCaller %08x,%08x\n",partner_call_link->Caller_Data.Call_Target.ip,GetLocalIp());
		//czn_20170328	if(partner_call_link->Caller_Data.Call_Target.ip !=GetLocalIp())
		{
			// lzh_20160512_s			
			//if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->Caller_Data.Call_Target.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e				
			{
				return -1;
			}

		}
	}
	
	if(partner_call_link->Status == CLink_AsCallServer)
	{
		bprintf("Status == CLink_AsCallServer %08x,%08x\n",partner_call_link->Caller_Data.Call_Target.ip,GetLocalIp());
		//czn_20170328	if(partner_call_link->BeCalled_Data.Call_Source.ip !=GetLocalIp() && partner_call_link->BeCalled_Data.Call_Source.ip != target_addr->ip)
		if(partner_call_link->BeCalled_Data.Call_Source.gatewayid != target_addr->gatewayid)
		{
			// lzh_20160512_s		
			//if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->BeCalled_Data.Call_Source.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e			
			{
				return -1;
			}

		}
		//czn_20170328	if(partner_call_link->Caller_Data.Call_Target.ip !=GetLocalIp() && partner_call_link->Caller_Data.Call_Target.ip != target_addr->ip)
		if(partner_call_link->Caller_Data.Call_Target.gatewayid != target_addr->gatewayid)
		{
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)		
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->Caller_Data.Call_Target.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			{
				return -1;
			}

		}

		
	}

	if(partner_call_link->Status == CLink_HaveLocalCall)
	{
		//czn_will_add
	}
#endif
	return 0;
}
//czn_020160422_e

//czn_20161008_s
int Send_UnlockCmd_VtkUnicast(Call_Dev_Info target_info,unsigned char locknum)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	VtkUnicastCmd.call_type		= DsAndGl;
	VtkUnicastCmd.call_code		= locknum;
	
	//return api_udp_c5_ipc_send_data_by_nodeid(t_addr.gatewayid,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}
//czn_20161008_e

int Send_VtkUnicastCmd_SourceRedailReq(Call_Dev_Info target_info)		//czn_20171030
{

	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	VtkUnicastCmd.call_type = 0;
	#if 0
	if(CallServer_Run.call_type == DxCallScene2)
	{
		VtkUnicastCmd.source_idh		= 0;
	}
	else
	{
		VtkUnicastCmd.source_idh		= CallServer_Run.s_addr.rt;
	}
	#endif
	VtkUnicastCmd.call_code		= 0;
	
	//return api_udp_c5_ipc_send_data_by_nodeid(t_addr->gatewayid,VTK_CMD_REDAIL_E011,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return api_udp_c5_ipc_send_data_without_ack(target_info.ip_addr,VTK_CMD_REDAIL_E011,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}

// lzh_20180912_e
//czn_20190216_s
int Send_CmdCallbackReq(int	req_dev,Call_Dev_Info target_dev)
{
	CmdCallbackReq_Stru req_cmd;
	CmdCallbackRsp_Stru rsp_cmd;
	
	int response_len = sizeof(CmdCallbackRsp_Stru);
	
	req_cmd.target_dev = target_dev;
	
	if( api_udp_c5_ipc_send_req(req_dev, CMD_CALLBACK_REQ, (char*)&req_cmd,sizeof(CmdCallbackReq_Stru), (char*)&rsp_cmd, &response_len) == 0 )
	{		
		return rsp_cmd.result;
	}	

	return -1;	
}

int Send_CmdCallbackRsp(int rsp_ip,int rsp_id,int result)
{
	CmdCallbackRsp_Stru rsp_cmd; 

	rsp_cmd.result = result;

	api_udp_c5_ipc_send_rsp( rsp_ip, CMD_CALLBACK_RSP, rsp_id, (char*)&rsp_cmd,sizeof(CmdCallbackRsp_Stru) );
	
	return 0;
}
//czn_20190216_e
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

