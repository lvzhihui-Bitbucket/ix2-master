/**
  ******************************************************************************
  * @file    obj_Caller_State.h
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Caller_State_H
#define _obj_Caller_State_H


#include "RTOS.h"
#include "vtk_udp_stack_class.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"

// Define Object Property-------------------------------------------------------
	 //CALLER状态机
#define CALLER_WAITING			0
#define CALLER_INVITE			1
#define CALLER_RINGING			2
#define CALLER_ACK				3
#define CALLER_BYE				4
	
	//第几把锁
#define	CALLER_UNLOCK1			1
#define	CALLER_UNLOCK2			2


#define MAX_CALLER_MSGBUF_LEN		20
// Define task interface-------------------------------------------------------------


typedef struct {
	unsigned char 		msg_target_id;		// 消息的目标对象
	unsigned char 		msg_source_id;		// 消息的源对象
	unsigned char 		msg_type;			// 消息的类型
	unsigned char  		msg_sub_type;		 //消息的子类型
	unsigned char 		call_type;
	Global_Addr_Stru 		s_addr;
	Global_Addr_Stru 		t_addr;
} CALLER_STRUCT ;



//Caller_Run
typedef struct CALLER_RUN_STRU
{	
	unsigned char			state;					//呼叫状态
	unsigned char			call_type;				//呼叫类型
	Global_Addr_Stru 		s_addr;
	Global_Addr_Stru 		t_addr;
	unsigned short		timer;					//呼叫定时
	unsigned short		checklink_error;
}	CALLER_RUN;

typedef struct CALLER_CONFIG_STRU	//bdu_zxj
{	
	uint8	limit_invite_time;
	uint16	limit_ringing_time;
	uint16	limit_ack_time;
	uint8	limit_bye_time;
}	CALLER_CONFIG;

typedef struct
{
	uint8 	call_type;
	void 	(*callback)(CALLER_STRUCT *msg);
} STRUCT_CALLER_CALLTYPE_CALLBACK;

typedef struct 
{
	CALLER_RUN 			Caller_Run;
	CALLER_CONFIG 		Caller_Config;
	OS_TIMER 			*timer_caller;
	vdp_task_t			*task_caller;
		
	int					support_calltype_num;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_invite_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_ring_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_ack_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_bye_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_wait_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_unclock_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_error_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_timeout_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_forceclose_callback;
	STRUCT_CALLER_CALLTYPE_CALLBACK		*to_redail_callback;		//czn_20171030
	void 	(*Config_Data_Init)(void);
	void 	(*MenuDisplay_ToInvite)(void);
	void 	(*MenuDisplay_ToRing)(void);
	void 	(*MenuDisplay_ToAck)(void);
	void 	(*MenuDisplay_ToBye)(void);
	void 	(*MenuDisplay_ToWait)(void);
}OBJ_CALLER_STRU;

#define CALLER_STRUCT_BASIC_LENGTH	((unsigned int)&(((CALLER_STRUCT*)0)->ext_buf[0]))
		//msg_type
typedef enum
{
	CALLER_MSG_INVITE	= 1,
	CALLER_MSG_RINGING,
	CALLER_MSG_ACK,
	CALLER_MSG_CANCEL,
	CALLER_MSG_BYE,
	CALLER_MSG_TRANSFER,
	CALLER_MSG_ERROR,
	CALLER_MSG_REDIAL,
	CALLER_MSG_UNLOCK1,
	CALLER_MSG_UNLOCK2,
	CALLER_MSG_TIMEOUT,
	CALLER_MSG_GETSTATE,
	CALLER_MSG_FORCECLOSE,
}IP_CALLER_MSG_TYPE;

typedef enum
{
	CALLER_TOUT_TIMEOVER = 0,
	CALLER_TOUT_CHECKLINK,
}IP_CALLER_TIMEOUTMSG_SUBTYPE;

typedef enum
{
	CALLER_ERROR_INVITEFAIL			= 1,
	CALLER_ERROR_DTCALLER_QUIT,
	CALLER_ERROR_DTBECALLED_QUIT,
	CALLER_ERROR_UINTLINK_CLEAR,
}IP_CALLER_ERROR_TYPE;	//Caller_Run

extern uint8 Caller_UnlockId;
extern uint8 Caller_ErrorCode;
	//摘机模式
#define MN_PICKUP	0
#define SJ_PICKUP	1

// Define Task 2 items----------------------------------------------------------
//void vtk_TaskInit_Caller(void);	
void vtk_TaskProcessEvent_Caller(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT	*msg_caller);
//void init_vdp_caller_task(void);

// Define Task others-----------------------------------------------------------
void Caller_Timer_Callback(OBJ_CALLER_STRU *caller_obj);	
void Caller_Waiting_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_Invite_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_Ringing_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_Ack_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_Bye_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_StateInvalid_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);
void Caller_MsgInvalid_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller);


// Define Object Function - Public---------------------------------------------
uint8 Caller_To_Invite(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
uint8 Caller_To_Redial(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Ringing(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Ack(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Bye(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Waiting(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Unlock(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Timeout(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_Error(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
void Caller_To_ForceClose(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg);
// Define Object Function - Private---------------------------------------------


// Define Object Function - Other-----------------------------------------------


#endif

