

#ifndef _obj_ip_mon_vres_map_table_h
#define _obj_ip_mon_vres_map_table_h

#define MAX_MONRES_NAME_LEN				40

#define DS_TABLE_KEY_MFG_SN				"MFG_SN"
#define DS_TABLE_KEY_DEVICE_TYPE		"DEVICE_TYPE"
#define DS_TABLE_KEY_GLOBAL				"GLOBAL"
#define DS_TABLE_KEY_LOCAL				"LOCAL"
#define DS_TABLE_KEY_BD_RM_MS			"BD_RM_MS"
#define DS_TABLE_KEY_NAME				"NAME"
#define DS_TABLE_KEY_R_NAME				"R_NAME"
#define DS_TABLE_KEY_LOCAL_SIP			"SIP_ACCOUNT"
#define DS_TABLE_KEY_MON_CODE			"MON_CODE"
#define DS_TABLE_KEY_REMARK				"REMARK"
#define DS_TABLE_KEY_NICK_NAME			"NICK_NAME" //昵称
#define DS_TABLE_KEY_FAVERITE			"FAVERITE"  //收藏
#define DS_TABLE_KEY_MON_PWD			"MON_PWD"

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	char	MFG_SN[12+1];		//MFG_SN
	int		deviceType;			//设备类型
	char	Global[10+1];		//Global
	char	Local[6+1];			//Local
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	res_name[MAX_MONRES_NAME_LEN+1];		//name1
	char	R_Name[MAX_MONRES_NAME_LEN+1];			//R_Name
	char	localSip[32+1];							//local sip
	char	monCode[4+1];							//monitor code
	int		enable;
	char	monPwd[10+1];							//monitor code
} MonRes_Entry_Stru;

//dh_20190822_s
typedef struct
{
	char	BD_RM_MS[10+1];		//BD_RM_MS
	int	faverite;					//收藏
	char	NickName[20+1];			//昵称
	char	name[20+1];			//name
}MON_CookieRecord_T;
//dh_20190822_e

#pragma pack()


void MonRes_Table_Init(void);
int Get_MonRes_Record(int index, char *pname, char *bdRmMs, int* deviceType);

#endif

