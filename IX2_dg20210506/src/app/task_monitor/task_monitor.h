

#ifndef _TASK_MONITOR_
#define _TASK_MONITOR_

#include "task_survey.h"

#define MSG_TYPE_DsMonitor_Show_Start  		0
#define MSG_TYPE_DsMonitor_Show_Stop  		1

typedef struct
{
	VDP_MSG_HEAD head;
	int  ins_num;
	char bdRmMs[11];
	char dsname[41];
	int  win_id;
	//int	rtpMonitor;
}DsMonitor_Show_Msg_t;

typedef struct
{
	VDP_MSG_HEAD head;
	int  ins_num;
}DsMonitor_Stop_Msg_t;

typedef struct
{
	int		dsIp;
	char 	bdRmMs[11];
	char 	dsName[41];		
	int		talk_flag;
	int		target_ip;	
	int		state;
	int		rtpMonitor;
	int 		resolution;
}Mon_Ds_Menu_Para_t;


#define REASON_CODE_EVENT	0x00	//事件
#define REASON_CODE_CALL	0x20	//呼叫
#define REASON_CODE_MON		0x40	//监视
#define REASON_CODE_MEM		0x60	//留影

typedef enum
{
	MONITOR_IDLE,
	MONITOR_LOCAL,
	MONITOR_REMOTE,
} MONITOR_STATE;


typedef struct
{
	MONITOR_STATE	state;				// 状态
	int				target_ip;			// 远程监视ip
	int				device_id;			// 远程设备号
	int				period;				// 监视时间
	unsigned short	apply_type;
} monitor_sbu;

int Api_Ds_Show(int ins_num, int win_id, char *bdRmMs, char *name);	
int Api_Ds_Show_Stop(int ins_num);	

int open_dsmonitor_client(int ins,          int ip,unsigned short apply_type, int t_time,int resolution,int rtpMonitor);
int close_dsmonitor_client( int ins );

#endif

