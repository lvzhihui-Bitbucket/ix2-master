#include "task_survey.h"
#include "obj_IPDeviceTable.h"
#include "obj_ResourceTable.h"
#include "obj_ip_mon_vres_map_table.h"
#include "define_Command.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "task_IoServer.h"
#include "MENU_public.h"
#include "obj_NetManagerInterface.h"

one_vtk_table* videoResourceTable = NULL;

int get_monres_table_record_items(int index, MonRes_Entry_Stru* pMonRes_Entry)
{
	unsigned char 	input_str[MAX_MONRES_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( videoResourceTable, index);
	
	if( precord != NULL )
	{
		int keyname_index;
		
		//BD_RM_MS
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 6 ? 6 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->res_name,input_str);

		//R_NAME
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(pMonRes_Entry->MFG_SN,input_str, MAX_MONRES_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		pMonRes_Entry->deviceType = atoi(input_str);

		//local sip
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_LOCAL_SIP);		
		input_str_len = 32;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(pMonRes_Entry->localSip,input_str, 32+1);
		
		//mon code
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_MON_CODE);		
		input_str_len = 4;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(pMonRes_Entry->monCode,input_str, 4+1);
		*/
		return 0;
	}
	else
		return -1;
}

void MonRes_Table_Init(void)
{
	videoResourceTable = load_vtk_table_file(MON_LIST_FILE_NAME);
	if(videoResourceTable == NULL)
	{
		char DSTableKey[7][40] = {DS_TABLE_KEY_BD_RM_MS, DS_TABLE_KEY_GLOBAL, DS_TABLE_KEY_LOCAL, DS_TABLE_KEY_NAME, 
								   DS_TABLE_KEY_R_NAME, DS_TABLE_KEY_DEVICE_TYPE, DS_TABLE_KEY_REMARK};
		int i;

		videoResourceTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &videoResourceTable->lock, 0);		//czn_20190327
		videoResourceTable->keyname_cnt = 7;
		videoResourceTable->pkeyname_len = malloc(videoResourceTable->keyname_cnt*sizeof(int));
		videoResourceTable->pkeyname = malloc(videoResourceTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< videoResourceTable->keyname_cnt; i++)
		{
			videoResourceTable->pkeyname_len[i] = 40;
			videoResourceTable->pkeyname[i].len = strlen(&DSTableKey[i][0]);
			videoResourceTable->pkeyname[i].pdat = malloc(videoResourceTable->pkeyname[i].len);
			memcpy(videoResourceTable->pkeyname[i].pdat, &DSTableKey[i][0], videoResourceTable->pkeyname[i].len);
		}

		videoResourceTable->record_cnt = 0;
		videoResourceTable->precord = NULL;		//czn_20190221
		
		SaveMonReslistTableToFile();
	}

}

void MonRes_Table_Release(void)
{
	free_vtk_table_file_buffer(videoResourceTable);
}


int Get_MonRes_Num(void)
{
	return videoResourceTable->record_cnt;
}

int Get_MonRes_Record(int index, char *pname, char *bdRmMs, int* deviceType)
{
	MonRes_Entry_Stru record_dat;
	
	if(videoResourceTable->record_cnt <= index)
	{
		return -1;
	}

	get_monres_table_record_items(index, &record_dat);
	
	if(bdRmMs != NULL)
	{
		strcpy(bdRmMs, record_dat.BD_RM_MS);
	}
	
	if(pname != NULL)
	{
		if(strcmp(record_dat.R_Name, "-"))
		{
			strcpy(pname, record_dat.R_Name);
		}
		else
		{
			strcpy(pname, record_dat.res_name);
		}
	}

	if(deviceType != NULL)
	{
		*deviceType = record_dat.deviceType;
	}
	return 0;
}

void MonRes_Rename(int index, const char *pname)
{
	MonRes_Entry_Stru data;
	char recordBuffer[200] = {0};
	one_vtk_dat vtkRecord;

	get_monres_table_record_items(index, &data);

	
	if(pname != NULL && strlen(pname) <= 20)
	{	
		strcpy(data.R_Name, pname);

		if(data.MFG_SN[0] == 0)
		{
			strcpy(data.MFG_SN, "-");
		}

		if(data.Global[0] == 0)
		{
			strcpy(data.Global, "-");
		}

		if(data.Local[0] == 0)
		{
			strcpy(data.Local, "-");
		}

		if(data.BD_RM_MS[0] == 0)
		{
			strcpy(data.BD_RM_MS, "-");
		}

		if(data.res_name[0] == 0)
		{
			strcpy(data.res_name, "-");
		}

		if(data.R_Name[0] == 0)
		{
			strcpy(data.R_Name, "-");
		}

		if(data.localSip[0] == 0)
		{
			strcpy(data.localSip, "-");
		}

		if(data.monCode[0] == 0)
		{
			strcpy(data.monCode, "-");
		}
	
		snprintf(recordBuffer, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", data.MFG_SN, data.deviceType, data.Global, data.Local, data.BD_RM_MS,
													data.res_name, data.R_Name, data.localSip, data.monCode);
		
		vtkRecord.len = strlen(recordBuffer);
		vtkRecord.pdat = recordBuffer;
		
		Modify_one_vtk_record(&vtkRecord, videoResourceTable, index);
		
		SaveMonReslistTableToFile();
	}
}

//搜索监视列表的一条记录
int SearchMonResTableByBdRmMs(char* MFG_SN, MonRes_Entry_Stru* pRecord)
{
	int i;
	MonRes_Entry_Stru tempRecord;

	if(MFG_SN[0] == 0)
	{
		return -1;
	}

	for(i = 0; i < videoResourceTable->record_cnt; i++)
	{
		get_monres_table_record_items(i, &tempRecord);
		
		if(!strcmp(tempRecord.MFG_SN, MFG_SN))
		{
			*pRecord = tempRecord;
			return i;
		}
	}

	return -1;
}


//增加监视列表的一条记录
int AddOneMonRes_Record(MonRes_Entry_Stru* pRecord)
{
	int index;
	MonRes_Entry_Stru tempRecord;
	one_vtk_dat vtkRecord;
	char data[100];

	index = SearchMonResTableByBdRmMs(pRecord->MFG_SN, &tempRecord);

	if(pRecord->MFG_SN[0] == 0)
	{
		strcpy(pRecord->MFG_SN, "-");
	}
	
	if(pRecord->Global[0] == 0)
	{
		strcpy(pRecord->Global, "-");
	}
	
	if(pRecord->Local[0] == 0)
	{
		strcpy(pRecord->Local, "-");
	}
	
	if(pRecord->BD_RM_MS[0] == 0)
	{
		strcpy(pRecord->BD_RM_MS, "-");
	}
	
	if(pRecord->res_name[0] == 0)
	{
		strcpy(pRecord->res_name, "-");
	}
	
	if(pRecord->R_Name[0] == 0)
	{
		strcpy(pRecord->R_Name, "-");
	}
	
	if(pRecord->localSip[0] == 0)
	{
		strcpy(pRecord->localSip, "-");
	}
	
	if(pRecord->monCode[0] == 0)
	{
		strcpy(pRecord->monCode, "-");
	}

	snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", pRecord->MFG_SN, pRecord->deviceType, pRecord->Global, pRecord->Local, pRecord->BD_RM_MS,
												pRecord->res_name, pRecord->R_Name, pRecord->localSip, pRecord->monCode);
	vtkRecord.len = strlen(data);
	
	
	if(index >= 0)
	{
		vtkRecord.pdat = data;

		Modify_one_vtk_record(&vtkRecord, videoResourceTable, index);
	}
	else
	{
		vtkRecord.pdat = malloc(vtkRecord.len);
		
		if(vtkRecord.pdat == NULL)
		{
			return -1;
		}

		memcpy(vtkRecord.pdat, data, vtkRecord.len);

		add_one_vtk_record(videoResourceTable, &vtkRecord);
	}
	
	SaveMonReslistTableToFile();
	return 0;

}


//按index删除监视列表的一条记录
int DeleteOneMonRes_Record(int index)
{
	if(index >= 0 && index < videoResourceTable->record_cnt)
	{
		delete_one_vtk_record(videoResourceTable, index);

		SaveMonReslistTableToFile();
		return 0;
	}
	else
	{
		return -1;
	}
}

//清除监视列表的所有记录
int ClearMonRes_Table(void)
{
	delete_all_vtk_record(videoResourceTable);

	SaveMonReslistTableToFile();	
	return 0;
}

//搜索生成监视列表
int AddMonRes_TableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, ret;
	
	API_SearchIpByFilter("0", TYPE_DS, SearchDeviceRecommendedWaitingTime, 30, &searchOnlineListData);
	
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		//不是本栋并且不是小区主机的不添加
		if(memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 4) && memcmp("00000000", searchOnlineListData.data[i].BD_RM_MS, 8))
		{
			continue;
		}

		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{

			int index;
			MonRes_Entry_Stru tempRecord;
			one_vtk_dat vtkRecord;
			char data[100];
			
			index = SearchMonResTableByBdRmMs(devInfo.MFG_SN, &tempRecord);

			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}
			
			
			if(index >= 0)
			{
				if(tempRecord.R_Name[0] == 0)
				{
					strcpy(tempRecord.R_Name, "-");
				}
				
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, tempRecord.R_Name, devInfo.localSip, devInfo.monCode);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = data;
				
				Modify_one_vtk_record(&vtkRecord, videoResourceTable, index);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, "-", devInfo.localSip, devInfo.monCode);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = malloc(vtkRecord.len);
				memcpy(vtkRecord.pdat, data, vtkRecord.len);
			
				add_one_vtk_record(videoResourceTable, &vtkRecord);
				ret++;
			}
		}
	}

	
	API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_OS, SearchDeviceRecommendedWaitingTime, 30, &searchOnlineListData);
	
	for(i = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		//不是自家的不添加
		if(memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 8))
		{
			continue;
		}
		
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{

			int index;
			MonRes_Entry_Stru tempRecord;
			one_vtk_dat vtkRecord;
			char data[100];
			
			index = SearchMonResTableByBdRmMs(devInfo.MFG_SN, &tempRecord);

			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}
			
			
			if(index >= 0)
			{
				if(tempRecord.R_Name[0] == 0)
				{
					strcpy(tempRecord.R_Name, "-");
				}
				
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, tempRecord.R_Name, devInfo.localSip, devInfo.monCode);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = data;
				
				Modify_one_vtk_record(&vtkRecord, videoResourceTable, index);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
															devInfo.name1, "-", devInfo.localSip, devInfo.monCode);
				vtkRecord.len = strlen(data);

				vtkRecord.pdat = malloc(vtkRecord.len);
				memcpy(vtkRecord.pdat, data, vtkRecord.len);
			
				add_one_vtk_record(videoResourceTable, &vtkRecord);
				ret++;
			}
		}
	}

	SaveMonReslistTableToFile();

	return ret;
}

//保存列表到文件
int SaveMonReslistTableToFile(void)
{
	int keyname_index;
	
	//R_NAME
	keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_R_NAME);	
	
	AlphabeticalOrder_vtk_table(videoResourceTable, keyname_index);

	remove(MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( videoResourceTable, 0, videoResourceTable->record_cnt, MON_LIST_FILE_NAME);
	
	return 0;
}

//czn_20190221_s
void MonRes_ReLoad(void)
{
	free_vtk_table_file_buffer(videoResourceTable);
	videoResourceTable = NULL;
	MonRes_Table_Init();
}
//czn_20190221_e


/*///////////////////////////////////////////////////////////////////////////////////////////////////////
临时监视列表
///////////////////////////////////////////////////////////////////////////////////////////////////////*/
one_vtk_table* videoResourceTempTable = NULL;

int get_monres_temp_table_record_items(int index, MonRes_Entry_Stru* pMonRes_Entry)
{
	unsigned char 	input_str[MAX_MONRES_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( videoResourceTempTable, index);
	
	if( precord != NULL )
	{
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 6 ? 6 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->res_name,input_str);

		//R_NAME
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(pMonRes_Entry->MFG_SN,input_str, MAX_MONRES_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		pMonRes_Entry->deviceType = atoi(input_str);
		*/
		//local sip
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_LOCAL_SIP);		
		input_str_len = 32;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 32 ? 32 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->localSip,input_str);
		//mon code
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_MON_CODE);		
		input_str_len = 4;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 4 ? 4 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->monCode,input_str);
		
		//mon pwd
		keyname_index = get_keyname_index( videoResourceTempTable, DS_TABLE_KEY_MON_PWD);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->monPwd,input_str);

		return 0;
	}
	else
		return -1;
}

void MonRes_Temp_Table_Create(void)
{
	if(videoResourceTempTable == NULL)
	{
		char DSTableKey[10][40] = {DS_TABLE_KEY_BD_RM_MS, DS_TABLE_KEY_GLOBAL, DS_TABLE_KEY_LOCAL, DS_TABLE_KEY_NAME, DS_TABLE_KEY_R_NAME, 
								DS_TABLE_KEY_DEVICE_TYPE, DS_TABLE_KEY_REMARK, DS_TABLE_KEY_LOCAL_SIP, DS_TABLE_KEY_MON_CODE, DS_TABLE_KEY_MON_PWD};
		int i;

		videoResourceTempTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &videoResourceTempTable->lock, 0);
		
		videoResourceTempTable->keyname_cnt = 10;
		videoResourceTempTable->pkeyname_len = malloc(videoResourceTempTable->keyname_cnt*sizeof(int));
		videoResourceTempTable->pkeyname = malloc(videoResourceTempTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< videoResourceTempTable->keyname_cnt; i++)
		{
			videoResourceTempTable->pkeyname_len[i] = 40;
			videoResourceTempTable->pkeyname[i].len = strlen(&DSTableKey[i][0]);
			videoResourceTempTable->pkeyname[i].pdat = malloc(videoResourceTempTable->pkeyname[i].len);
			memcpy(videoResourceTempTable->pkeyname[i].pdat, &DSTableKey[i][0], videoResourceTempTable->pkeyname[i].len);
		}
		
		videoResourceTempTable->precord = NULL;
		videoResourceTempTable->record_cnt = 0;
	}
}

void MonRes_Temp_Table_Delete(void)
{
	free_vtk_table_file_buffer(videoResourceTempTable);
	videoResourceTempTable = NULL;
}


int Get_MonRes_Temp_Table_Num(void)
{
	return (videoResourceTempTable == NULL) ? 0 : videoResourceTempTable->record_cnt;
}

int Get_MonRes_Temp_Table_Record(int index, char *pname, char *bdRmMs, int* deviceType)
{
	MonRes_Entry_Stru record_dat;
	
	if(videoResourceTempTable == NULL)
	{
		return -1;
	}
	
	if(videoResourceTempTable->record_cnt <= index)
	{
		return -1;
	}

	get_monres_temp_table_record_items(index, &record_dat);
	
	if(bdRmMs != NULL)
	{
		strcpy(bdRmMs, record_dat.BD_RM_MS);
	}
	
	if(pname != NULL)
	{
		if(strcmp(record_dat.R_Name, "-"))
		{
			strcpy(pname, record_dat.R_Name);
		}
		else
		{
			strcpy(pname, record_dat.res_name);
		}
	}

	if(deviceType != NULL)
	{
		*deviceType = record_dat.deviceType;
	}

	return 0;
}


//搜索生成临时监视列表
int AddMonRes_Temp_TableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, j, ret;
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	BD_RM_MS1[10+1];		//BD_RM_MS
	char temp[20];

	MonRes_Temp_Table_Delete();
	MonRes_Temp_Table_Create();
	
	//dh_20190827_s
	API_SearchIpByFilter("9999", TYPE_DS, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);
	API_Event_IoServer_InnerRead_All(OverUnitMonitor,temp);//允许 全范围监视
	//dh_20190827_e
	
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		//非全范围监视只 添加本栋以及小区主机
		if(!atoi(temp) && memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 4) && memcmp("00000000", searchOnlineListData.data[i].BD_RM_MS, 8))
		{
			continue;
		}
		
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{
			one_vtk_dat vtkRecord;
			char data[200];
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			if(devInfo.name2_utf8[0] == 0)
			{
				strcpy(devInfo.name2_utf8, "-");
			}
			/*
			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			*/
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}

			if(memcmp(devInfo.monPwd, "PWD", 3))
			{
				strcpy(devInfo.monPwd, "-");
			}
			else
			{
				strcpy(devInfo.monPwd, devInfo.monPwd+3);
			}
						
			//snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
			//											devInfo.name1, "-", devInfo.localSip, devInfo.monCode);
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", devInfo.BD_RM_MS, devInfo.Global, devInfo.Local, devInfo.name1,
														devInfo.name2_utf8, "-", "-", devInfo.localSip, devInfo.monCode, devInfo.monPwd);
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(videoResourceTempTable, &vtkRecord);
			ret++;
		}
	}
	
	API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_OS, SearchDeviceRecommendedWaitingTime, 30, &searchOnlineListData);
	
	for(i = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		//不是自家的不添加
		if(memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 8))
		{
			continue;
		}
		
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 2, &devInfo) == 0)
		{
			one_vtk_dat vtkRecord;
			char data[200];
			
			if(devInfo.BD_RM_MS[0] == 0)
			{
				strcpy(devInfo.BD_RM_MS, "-");
			}
			
			if(devInfo.Global[0] == 0)
			{
				strcpy(devInfo.Global, "-");
			}
			
			if(devInfo.Local[0] == 0)
			{
				strcpy(devInfo.Local, "-");
			}
			
			if(devInfo.name1[0] == 0)
			{
				strcpy(devInfo.name1, "-");
			}
			if(devInfo.name2_utf8[0] == 0)
			{
				strcpy(devInfo.name2_utf8, "-");
			}
			/*
			if(devInfo.MFG_SN[0] == 0)
			{
				strcpy(devInfo.MFG_SN, "-");
			}
			*/
			if(devInfo.localSip[0] == 0)
			{
				strcpy(devInfo.localSip, "-");
			}
			
			if(devInfo.monCode[0] == 0)
			{
				strcpy(devInfo.monCode, "-");
			}
			
			if(memcmp(devInfo.monPwd, "PWD", 3))
			{
				strcpy(devInfo.monPwd, "-");
			}
			else
			{
				strcpy(devInfo.monPwd, devInfo.monPwd+3);
			}
			
			
			//snprintf(data, 200, "%s,%d,%s,%s,%s,%s,%s,%s,%s", devInfo.MFG_SN, devInfo.deviceType, devInfo.Global, devInfo.Local, devInfo.BD_RM_MS,
			//											devInfo.name1, "-", devInfo.localSip, devInfo.monCode);
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", devInfo.BD_RM_MS, devInfo.Global, devInfo.Local, devInfo.name1,
														devInfo.name2_utf8, "-", "-", devInfo.localSip, devInfo.monCode, devInfo.monPwd);
			vtkRecord.len = strlen(data);

			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
		
			add_one_vtk_record(videoResourceTempTable, &vtkRecord);
			ret++;
		}	
	}

	//按房号从小到大排序
	for (i = 0; i < videoResourceTempTable->record_cnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < videoResourceTempTable->record_cnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			Get_MonRes_Temp_Table_Record(j, NULL, BD_RM_MS, NULL);
			Get_MonRes_Temp_Table_Record(j, NULL, BD_RM_MS1, NULL);
			if (atoi(BD_RM_MS) > atoi(BD_RM_MS1))
			{
				one_vtk_dat tempRecord;

				tempRecord = videoResourceTempTable->precord[j+1];
				videoResourceTempTable->precord[j+1] = videoResourceTempTable->precord[j];
				videoResourceTempTable->precord[j] = tempRecord;
			}
		}
	}
	
	return ret;
}




/*///////////////////////////////////////////////////////////////////////////////////////////////////////
临时监视列表
///////////////////////////////////////////////////////////////////////////////////////////////////////*/
one_vtk_table* videoResourceUpdateTable = NULL;

int get_monres_update_table_record_items(int index, MonRes_Entry_Stru* pMonRes_Entry)
{
	unsigned char 	input_str[MAX_MONRES_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( videoResourceUpdateTable, index);
	
	if( precord != NULL )
	{
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->BD_RM_MS,input_str);

		//GLOBAL
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_GLOBAL);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Global,input_str);
		
		//LOCAL
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_LOCAL);		
		input_str_len = 6;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 6 ? 6 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->Local,input_str);
		
		//NAME
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->res_name,input_str);

		//R_NAME
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_R_NAME);		
		input_str_len = 40;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 40 ? 40 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(pMonRes_Entry->R_Name,input_str);
		/*
		//MFG_SN
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_MFG_SN);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		strncpy(pMonRes_Entry->MFG_SN,input_str, MAX_MONRES_NAME_LEN+1);
		
		//DEVICE_TYPE
		keyname_index = get_keyname_index( videoResourceTable, DS_TABLE_KEY_DEVICE_TYPE);		
		input_str_len = MAX_MONRES_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;

		pMonRes_Entry->deviceType = atoi(input_str);
		*/
		//local sip
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_LOCAL_SIP);		
		input_str_len = 32;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 32 ? 32 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->localSip,input_str);
		
		//mon code
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_MON_CODE);		
		input_str_len = 4;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 4 ? 4 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->monCode,input_str);
		
		//mon pwd
		keyname_index = get_keyname_index( videoResourceUpdateTable, DS_TABLE_KEY_MON_PWD);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;

		strcpy(pMonRes_Entry->monPwd,input_str);
		return 0;
	}
	else
		return -1;
}

void MonRes_Update_Table_Create(void)
{
	if(videoResourceUpdateTable == NULL)
	{
		char DSTableKey[10][40] = {DS_TABLE_KEY_BD_RM_MS, DS_TABLE_KEY_GLOBAL, DS_TABLE_KEY_LOCAL, DS_TABLE_KEY_NAME, DS_TABLE_KEY_R_NAME, 
								DS_TABLE_KEY_DEVICE_TYPE, DS_TABLE_KEY_REMARK, DS_TABLE_KEY_LOCAL_SIP, DS_TABLE_KEY_MON_CODE, DS_TABLE_KEY_MON_PWD};
		int i;

		videoResourceUpdateTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &videoResourceUpdateTable->lock, 0);
		
		videoResourceUpdateTable->keyname_cnt = 10;
		videoResourceUpdateTable->pkeyname_len = malloc(videoResourceUpdateTable->keyname_cnt*sizeof(int));
		videoResourceUpdateTable->pkeyname = malloc(videoResourceUpdateTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< videoResourceUpdateTable->keyname_cnt; i++)
		{
			videoResourceUpdateTable->pkeyname_len[i] = 40;
			videoResourceUpdateTable->pkeyname[i].len = strlen(&DSTableKey[i][0]);
			videoResourceUpdateTable->pkeyname[i].pdat = malloc(videoResourceUpdateTable->pkeyname[i].len);
			memcpy(videoResourceUpdateTable->pkeyname[i].pdat, &DSTableKey[i][0], videoResourceUpdateTable->pkeyname[i].len);
		}
		
		videoResourceUpdateTable->precord = NULL;
		videoResourceUpdateTable->record_cnt = 0;
	}
}

void MonRes_Update_Table_Delete(void)
{
	free_vtk_table_file_buffer(videoResourceUpdateTable);
	videoResourceUpdateTable = NULL;
}

int Get_MonRes_Update_Table_Num(void)
{
	return (videoResourceUpdateTable == NULL) ? 0 : videoResourceUpdateTable->record_cnt;
}

int Get_MonRes_Update_Table_Record(int index, char *pname, char *bdRmMs, int* deviceType)
{
	MonRes_Entry_Stru record_dat;
	
	if(videoResourceUpdateTable == NULL)
	{
		return -1;
	}
	
	if(videoResourceUpdateTable->record_cnt <= index)
	{
		return -1;
	}

	get_monres_update_table_record_items(index, &record_dat);

	if(bdRmMs != NULL)
	{
		strcpy(bdRmMs, record_dat.BD_RM_MS);
	}
	
	if(pname != NULL)
	{
		if(strcmp(record_dat.R_Name, "-"))
		{
			strcpy(pname, record_dat.R_Name);
		}
		else
		{
			strcpy(pname, record_dat.res_name);
		}
	}

	if(deviceType != NULL)
	{
		*deviceType = record_dat.deviceType;
	}

	return 0;
}


//搜索生成临时监视列表
int AddMonRes_Update_TableBySearching(void)
{
	SearchIpRspData searchOnlineListData;
	DeviceInfo devInfo;
	int i, j, ret;
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	BD_RM_MS1[10+1];		//BD_RM_MS
	char temp[20];
	one_vtk_dat vtkRecord;
	char data[200];

	MonRes_Update_Table_Delete();
	MonRes_Update_Table_Create();
	
	//dh_20190827_s
	if(API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData)==-1)
		return -1;
	API_Event_IoServer_InnerRead_All(OverUnitMonitor,temp);//允许 全范围监视
	//dh_20190827_e
	
	for(i = 0, ret = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		if(searchOnlineListData.data[i].deviceType == TYPE_DS)
		{
			//非全范围监视只 添加本栋以及小区主机
			if(!atoi(temp) && memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 4) && memcmp("00000000", searchOnlineListData.data[i].BD_RM_MS, 8))
			{
				continue;
			}
			if(api_nm_if_judge_include_dev(searchOnlineListData.data[i].BD_RM_MS,searchOnlineListData.data[i].Ip)==0)
			{
				continue;
			}
		}
		else if(searchOnlineListData.data[i].deviceType == TYPE_OS)
		{
			//不是自家的不添加
			if(memcmp(GetSysVerInfo_BdRmMs(), searchOnlineListData.data[i].BD_RM_MS, 8))
			{
				continue;
			}
			if(api_nm_if_judge_include_dev(searchOnlineListData.data[i].BD_RM_MS,searchOnlineListData.data[i].Ip)==0)
			{
				continue;
			}
		}
		//其他类型不是监视类型
		else
		{
			continue;
		}
		
		if(API_GetInfoByIp(searchOnlineListData.data[i].Ip, 1, &devInfo) == 0)
		{
			snprintf(data, 200, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
			devInfo.BD_RM_MS[0] ? devInfo.BD_RM_MS : "-",
			devInfo.Global[0] ? devInfo.Global : "-",
			devInfo.Local[0] ? devInfo.Local : "-",
			devInfo.name1[0] ? devInfo.name1 : "-",
			devInfo.name2_utf8[0] ? devInfo.name2_utf8 : "-",
			"-",
			"-",
			devInfo.localSip[0] ? devInfo.localSip : "-",
			devInfo.monCode[0] ? devInfo.monCode : "-",
			memcmp(devInfo.monPwd, "PWD", 3) ? "-" : (devInfo.monPwd+3));

			
			vtkRecord.len = strlen(data);
		
			vtkRecord.pdat = malloc(vtkRecord.len);
			memcpy(vtkRecord.pdat, data, vtkRecord.len);
			add_one_vtk_record(videoResourceUpdateTable, &vtkRecord);
			ret++;
		}		
	}
	
	//按房号从小到大排序
	for (i = 0; i < videoResourceUpdateTable->record_cnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < videoResourceUpdateTable->record_cnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			Get_MonRes_Update_Table_Record(j, NULL, BD_RM_MS, NULL);
			Get_MonRes_Update_Table_Record(j+1, NULL, BD_RM_MS1, NULL);
			if (atoi(BD_RM_MS) > atoi(BD_RM_MS1))
			{
				one_vtk_dat tempRecord;

				tempRecord = videoResourceUpdateTable->precord[j+1];
				videoResourceUpdateTable->precord[j+1] = videoResourceUpdateTable->precord[j];
				videoResourceUpdateTable->precord[j] = tempRecord;
			}
		}
	}

	return ret;
}

int UpdateVideoResourceTable(void)
{
	AddMonRes_Update_TableBySearching();
	if(CompareTable(videoResourceUpdateTable, videoResourceTempTable))
	{
		CopyTable(&videoResourceTempTable, videoResourceUpdateTable);
		return 1;
	}
	return 0;
}

int UpdateOnlineMonListTable(void)
{
	int ret = 0;
	int i;
	IPC_RECORD ipcRecord; 
	
	extern one_vtk_table* ipcMonListTable;
	extern one_vtk_table* ipcTempTable;

	//搜索在线主机
	if(AddMonRes_Update_TableBySearching()==-1)
		return 0;
	
	if(Get_MonRes_Num())
	{
		if(CompareTable(videoResourceUpdateTable, videoResourceTable))
		{
			CopyTable(&videoResourceTempTable, videoResourceUpdateTable);
			ret = 1;
		}
	}
	else
	{
		if(CompareTable(videoResourceUpdateTable, videoResourceTempTable))
		{
			CopyTable(&videoResourceTempTable, videoResourceUpdateTable);
			ret = 1;
		}
	}
	
	IPC_Temp_Table_Delete();
	IPC_Temp_Table_Create();

	//搜索在线IPC
	for(i = 0; i < Get_IPC_MonRes_Num(); i++)
	{
		Get_IPC_MonRes_Record(i, &ipcRecord);
		if(ipc_check_online(ipcRecord.devUrl) == 0)
		{
			Add_IPC_Temp_Record(&ipcRecord);
		}
		ret = 1;
	}
	
	if(CompareTable(ipcMonListTable, ipcTempTable))
	{
		ret = 1;
	}
	
	return ret;
}
//dh_20190822_s
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
one_vtk_table* imMonCookieTable = NULL;
#define COOKIE_USER_TABLE_NUM 	100

void MonCookieTableCreate(void)
{
	imMonCookieTable = load_vtk_table_file(MON_COOKIE_TABLE_NAME);
	if(imMonCookieTable == NULL)
	{
		int i;
		char ImTableKey[4][40] = {DS_TABLE_KEY_BD_RM_MS, DS_TABLE_KEY_FAVERITE, DS_TABLE_KEY_NICK_NAME, DS_TABLE_KEY_NAME};

		imMonCookieTable = malloc(sizeof(one_vtk_table));
		
		pthread_mutex_init( &imMonCookieTable->lock, 0);
		
		imMonCookieTable->keyname_cnt = 4;
		imMonCookieTable->pkeyname_len = malloc(imMonCookieTable->keyname_cnt*sizeof(int));
		imMonCookieTable->pkeyname = malloc(imMonCookieTable->keyname_cnt*sizeof(one_vtk_dat));
		for(i = 0; i< imMonCookieTable->keyname_cnt; i++)
		{
			imMonCookieTable->pkeyname_len[i] = 40;
			imMonCookieTable->pkeyname[i].len = strlen(&ImTableKey[i][0]);
			imMonCookieTable->pkeyname[i].pdat = malloc(imMonCookieTable->pkeyname[i].len);
			memcpy(imMonCookieTable->pkeyname[i].pdat, &ImTableKey[i][0], imMonCookieTable->pkeyname[i].len);
		}

		imMonCookieTable->precord = NULL;
		imMonCookieTable->record_cnt = 0;
		save_vtk_table_file_buffer(imMonCookieTable, 0, imMonCookieTable->record_cnt, MON_COOKIE_TABLE_NAME);
	}
}

void MonCookieTableTableDelete(void)
{
	free_vtk_table_file_buffer(imMonCookieTable);
	imMonCookieTable = NULL;
}

int SaveMonCookieTableToFile(void)
{
	int keyname_index;
	
	keyname_index = get_keyname_index( imMonCookieTable, DS_TABLE_KEY_BD_RM_MS);	
	
	AlphabeticalOrder_vtk_table(imMonCookieTable, keyname_index);

	remove(MON_COOKIE_TABLE_NAME);
	
	if(save_vtk_table_file_buffer(imMonCookieTable, 0, imMonCookieTable->record_cnt, MON_COOKIE_TABLE_NAME))
	{
		return -1;
	}
	
	return 0;
}

int GetMonCookieTableTableNum(void)
{
	return (imMonCookieTable == NULL) ? 0 : imMonCookieTable->record_cnt;
}
void GetMonCookieRecordItems(int index, MON_CookieRecord_T* record)
{
	unsigned char 	input_str[MAX_MONRES_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( imMonCookieTable, index);
	
	if( precord != NULL )
	{
	
		int keyname_index;

		//BD_RM_MS
		keyname_index = get_keyname_index( imMonCookieTable, DS_TABLE_KEY_BD_RM_MS);		
		input_str_len = 10;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 10 ? 10 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->BD_RM_MS,input_str);

		//FAVERITE
		keyname_index = get_keyname_index( imMonCookieTable, DS_TABLE_KEY_FAVERITE);		
		input_str_len = 1;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		record->faverite = atoi(input_str);
		
		//NICK_NAME
		keyname_index = get_keyname_index( imMonCookieTable, DS_TABLE_KEY_NICK_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->NickName,input_str);
		
		//NAME
		keyname_index = get_keyname_index( imMonCookieTable, DS_TABLE_KEY_NAME);		
		input_str_len = 20;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str_len = input_str_len > 20 ? 20 : input_str_len;
		input_str[input_str_len] = 0;
		strcpy(record->name,input_str);
	}	
}
int GetMonCookieFaveriteNum(char* index)
{
	int i;
	MON_CookieRecord_T record;
	int FaveriteNum = 0;
	for(i=0 ; i< imMonCookieTable->record_cnt; i++)
	{
		GetMonCookieRecordItems(i,&record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}

int MonGetCookieItemByAddr(char* BD_RM_MS, MON_CookieRecord_T* record)
{
	int i;
	MON_CookieRecord_T tempRecord;
	if(imMonCookieTable->record_cnt == 0)
		return -1;
	//printf("ImGetCookieItemByAddr : %s\n",BD_RM_MS);
	for(i=0 ; i< imMonCookieTable->record_cnt; i++)
	{
		GetMonCookieRecordItems(i,&tempRecord);
		if(!strcmp(tempRecord.BD_RM_MS, BD_RM_MS))
		{
			*record = tempRecord;
			return 0;
		}
	}
	return -1;
}

void MonCookieModify(char* BD_RM_MS, int Faverite, char *Nname, char *name)
{
	int i;
	char data[200] = {0};
	one_vtk_dat vtkRecord;
	MON_CookieRecord_T record;
	if(imMonCookieTable->record_cnt >= COOKIE_USER_TABLE_NUM)
		return;
	for(i=0 ; i< imMonCookieTable->record_cnt; i++)
	{
		GetMonCookieRecordItems(i,  &record);
		if(!strcmp(record.BD_RM_MS, BD_RM_MS))
		{
			break;
		}
	}
	if(i < imMonCookieTable->record_cnt) //modify
	{
		if(Nname != NULL)
		{	
			if(!strcmp(Nname, "-") && !record.faverite)
			{
				delete_one_vtk_record(imMonCookieTable, i);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s", record.BD_RM_MS, record.faverite, Nname, record.name);			
				vtkRecord.len = strlen(data);
				vtkRecord.pdat = data;
				Modify_one_vtk_record(&vtkRecord, imMonCookieTable, i);
			}
		}
		else
		{
			if(!strcmp(record.NickName, "-") && !Faverite)
			{
				delete_one_vtk_record(imMonCookieTable, i);
			}
			else
			{
				snprintf(data, 200, "%s,%d,%s,%s", record.BD_RM_MS, Faverite, record.NickName, record.name);			
				vtkRecord.len = strlen(data);
				vtkRecord.pdat = data;
				Modify_one_vtk_record(&vtkRecord, imMonCookieTable, i);
			}
		}
	}
	else		//add
	{
		snprintf(data, 200, "%s,%d,%s,%s", BD_RM_MS, Faverite, (Nname == NULL? "-": Nname), name);			
		vtkRecord.len = strlen(data);
		vtkRecord.pdat = malloc(vtkRecord.len);
		memcpy(vtkRecord.pdat, data, vtkRecord.len);
	
		add_one_vtk_record(imMonCookieTable, &vtkRecord);
	}
	SaveMonCookieTableToFile();
}
//dh_20190822_e

