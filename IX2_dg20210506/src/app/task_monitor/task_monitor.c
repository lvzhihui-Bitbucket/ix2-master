

#include "vtk_udp_stack_c5_ipc_cmd.h"
//#include "../task_CommandInterface/obj_CommandInterface.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "task_debug_sbu.h"
#include "ip_video_cs_control.h"
#include "task_monitor.h"

#include "task_Relay.h"			//lyx 20170612
#include "task_VideoMenu.h"
#include "task_Power.h"	
#include "obj_GetIpByNumber.h"
#include "MENU_public.h"

pthread_mutex_t MonitorObj_Lock = PTHREAD_MUTEX_INITIALIZER;	

Loop_vdp_common_buffer	vdp_ds_monitor_inner_queue;
Loop_vdp_common_buffer	vdp_ds_monitor_sync_queue;
vdp_task_t				task_ds_monitor;

int ds_stop_timer[4]={0};

void vdp_ds_monitor_inner_data_process(char* msg_data, int len);
void* vdp_ds_monitor_task( void* arg );

void vtk_taskInit_ds_monitor(int priority)
{	
	init_vdp_common_queue(&vdp_ds_monitor_inner_queue, 500, (msg_process)vdp_ds_monitor_inner_data_process, &task_ds_monitor);
	init_vdp_common_queue(&vdp_ds_monitor_sync_queue, 100, NULL,								  			&task_ds_monitor);
	init_vdp_common_task(&task_ds_monitor, MSG_ID_DS_MONITOR, vdp_ds_monitor_task, &vdp_ds_monitor_inner_queue, &vdp_ds_monitor_sync_queue);
}

void exit_vdp_ds_monitor_task(void)
{
	exit_vdp_common_queue(&vdp_ds_monitor_inner_queue);
	exit_vdp_common_queue(&vdp_ds_monitor_sync_queue);
	exit_vdp_common_task(&task_ds_monitor);
}

void* vdp_ds_monitor_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	//Ipc_Business_init();
	
	while( ptask->task_run_flag )
	{
		//
		//size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 1000);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		else
		{
			pthread_mutex_lock(&MonitorObj_Lock);
			int i;
			for(i=0;i<4;i++)
			{
				if(ds_stop_timer[i]>0)
				{
					
					
					if(--ds_stop_timer[i]==0)
					{
						api_ds_stop(i);
					}
					else
					{
						char key_buff[50];
						sprintf(key_buff,"VC_CH%d_Bandwind",i);
						if(API_PublicInfo_Read_Int(key_buff)<100&&get_video_client_rate(i)<100)
						{
							ds_stop_timer[i]=0;
							api_ds_stop(i);
						}
					}
				}
			}
			pthread_mutex_unlock(&MonitorObj_Lock);
		}
	}
	return 0;
}

void vdp_ds_monitor_inner_data_process(char* msg_data,int len)
{
	DsMonitor_Show_Msg_t* pMsgDsShow = (DsMonitor_Show_Msg_t*)msg_data;
	switch (pMsgDsShow->head.msg_type)
	{
		case MSG_TYPE_DsMonitor_Show_Start:
			api_ds_show(pMsgDsShow->ins_num, pMsgDsShow->win_id, pMsgDsShow->bdRmMs, pMsgDsShow->dsname);
		break;
		case MSG_TYPE_DsMonitor_Show_Stop:		
			ds_stop_timer[pMsgDsShow->ins_num]=3;
			#ifdef PID_IXSE
            		Clear_ds_show_layer(pMsgDsShow->ins_num);
			#endif
		break;
	}
}

int Api_Ds_Show(int ins_num, int win_id, char *bdRmMs, char *name)	
{
	DsMonitor_Show_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_DS_MONITOR;
	msg.head.msg_type 		= MSG_TYPE_DsMonitor_Show_Start;
	msg.head.msg_sub_type 	= 0;
	msg.ins_num = ins_num;
	msg.win_id = win_id;
	strcpy(msg.bdRmMs,bdRmMs);
	strcpy(msg.dsname,name);

	char temp[5];
	API_Event_IoServer_InnerRead_All(MonitorDsDis, temp);
	if(atoi(temp))
		return -1;
	
	return push_vdp_common_queue(&vdp_ds_monitor_inner_queue, (char*)&msg, sizeof(msg));
}

int Api_Ds_Show_Stop(int ins_num)	
{
	DsMonitor_Stop_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_DS_MONITOR;
	msg.head.msg_type 		= MSG_TYPE_DsMonitor_Show_Stop;
	msg.head.msg_sub_type 	= 0;
	msg.ins_num = ins_num;
	return push_vdp_common_queue(&vdp_ds_monitor_inner_queue, (char*)&msg, sizeof(msg));
}

int Api_Ds_Show_Stop2(int ins_num)	
{
	pthread_mutex_lock(&MonitorObj_Lock);
	//if(Ds_Menu_Para[ins_num].state != MONITOR_IDLE)
	{
		ds_stop_timer[ins_num]=3;
	}
	pthread_mutex_unlock(&MonitorObj_Lock);

	return 0;
}

Mon_Ds_Menu_Para_t Ds_Menu_Para[4];

int Api_Ds_Show_BeClose(int ins_num)
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins_num].state != MONITOR_IDLE)
	{
		api_ds_stop(ins_num);
	}
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}



int GetDsIp(int win_id)
{
	return Ds_Menu_Para[win_id].dsIp;
}
void GetDs_MonName(int win_id, char* name)
{
	strcpy(name, Ds_Menu_Para[win_id].dsName);
}

int GetDsTalkState(int win_id)
{
	return Ds_Menu_Para[win_id].talk_flag;
}
void SetDsTalkState(int win_id, int state)
{
	Ds_Menu_Para[win_id].talk_flag = state;
}

int api_ds_show(int ins_num, int win_id, char *bdRmMs, char *name)	
{
	int x,y,w,h;	
	int monitor_result;
	GetIpRspData data;

	LoadSpriteDisplay(1, win_id);

	strcpy(Ds_Menu_Para[ins_num].bdRmMs,bdRmMs);
	get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name, Ds_Menu_Para[ins_num].dsName);

	if(API_GetIpNumberFromNet(bdRmMs, NULL, NULL, 2, 1, &data) != 0)
	{
		printf("API_GetIpNumberFromNet fail:%s \n",bdRmMs);
		API_Beep(BEEP_TYPE_DI3);
		LoadSpriteDisplay(2, win_id);
		return -1;
	}
	Ds_Menu_Para[ins_num].dsIp = data.Ip[0];
	if((monitor_result = open_dsmonitor_client(ins_num, Ds_Menu_Para[ins_num].dsIp,REASON_CODE_MON,600,Resolution_720P,0)) != 0)		
	{
		API_Beep(BEEP_TYPE_DI3);
		if(monitor_result == 1 || monitor_result == -1)
		{
			//API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError1);
		}
		else
		{
			//API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError2);
		}
		//usleep(2000000);
		//API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		return -1;
	}
	Get_OneIpc_ShowPos(win_id,&x,&y,&w,&h);
	Set_ds_show_pos(ins_num, x, y, w, h);
	SetWinDeviceName(ins_num, Ds_Menu_Para[ins_num].dsName);
	SetWinVdType(ins_num, 0);
	SetWinMonState(ins_num, 1);
	SetWinDeviceType(ins_num, 0);
	LoadSpriteDisplay(0, win_id);
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStart);

	return 0;
}

int api_ds_stop(int ins_num)	
{
	close_dsmonitor_client(ins_num);
	if(GetDsTalkState(ins_num))
	{
		SetDsTalkState(ins_num, 0);
		DsTalkClose(ins_num);
	}
}


int CheckDsOnoff(int ip,int resolution)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(Ds_Menu_Para[i].target_ip == ip&&Ds_Menu_Para[i].resolution== resolution)
			return -1;
	}
	return 0;
}

int open_dsmonitor_client(int ins,          int ip,unsigned short apply_type, int t_time,int resolution,int rtpMonitor)
{
	//send_ip_mon_link_req
	int link_result;	// 0/ok, -1/�豸�쳣��1/proxy ok, 2/�豸������
	int remote_ip;
	unsigned char remote_dev_id;
	int target_ip;

	API_Event_By_Name("EventMonOpen");

	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins].state != MONITOR_IDLE)
	{
		if(Ds_Menu_Para[ins].target_ip==ip&&Ds_Menu_Para[ins].resolution== resolution&&Ds_Menu_Para[ins].rtpMonitor== rtpMonitor)
		{
			ds_stop_timer[ins]=0;
			pthread_mutex_unlock(&MonitorObj_Lock);
			return 0;
		}
		else
			api_ds_stop(ins);
		//return -1;
	}
	if(CheckDsOnoff(ip,resolution) == -1)
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		return -1;
	}
	
	link_result = send_ip_mon_link_req( ip,apply_type,1, &remote_ip, &remote_dev_id );
	// ��������
	if( link_result == 0 ||apply_type==REASON_CODE_CALL)
	{
		if( api_video_client_turn_on(ins, ip_video_multicast, remote_ip, remote_dev_id, t_time,resolution) == 0 )
		{
			Ds_Menu_Para[ins].state = MONITOR_REMOTE;
			Ds_Menu_Para[ins].target_ip = remote_ip;
			Ds_Menu_Para[ins].rtpMonitor=rtpMonitor;
			Ds_Menu_Para[ins].resolution=resolution;
			ds_stop_timer[ins]=0;
			dprintf(" api_video_client_turn_on 0x%08x:%d ok!\n",remote_ip,remote_dev_id);
			pthread_mutex_unlock(&MonitorObj_Lock);
			return 0;
		}
		else
		{
			pthread_mutex_unlock(&MonitorObj_Lock);
			return 3;
		}
	}
	// �豸����
	else if( link_result == -1 )
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		dprintf("mon link req er!\n");
		return 1;
	}
	// ����������
	else
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		dprintf("mon link req ng!\n");
		return 2;			
	}
}

int get_dsmonitor_client_rtp(int ins)
{
	return Ds_Menu_Para[ins].rtpMonitor;
}
int close_dsmonitor_client( int ins )
{
	API_Event_By_Name("EventMonClose");
	api_one_video_client_desubscribe_req_ext(ins,Ds_Menu_Para[ins].target_ip,Ds_Menu_Para[ins].resolution,1);
	api_video_client_turn_off(ins);
	Ds_Menu_Para[ins].state = MONITOR_IDLE;
	Ds_Menu_Para[ins].target_ip = 0;
	Ds_Menu_Para[ins].resolution=-1;
	ds_stop_timer[ins]=0;
	return 0;
}
int LinphoneMonitorDsStart(char *addr)
{
	GetIpRspData data;
	if(API_GetIpNumberFromNet(addr, NULL, NULL, 2, 1, &data) != 0)
	{
		printf("API_GetIpNumberFromNet fail:%s \n",addr);
		return -1;
	}
	if(API_Business_Request(Business_State_MonitorDs)!=1)
		return -1;
	char temp[5];
	int res=0;
	API_Event_IoServer_InnerRead_All(AppVideoRes, temp);
	res = atoi(temp);
	if((open_dsmonitor_client(1, data.Ip[0],REASON_CODE_MON,600,res?Resolution_480P:Resolution_240P,1)) != 0)		
	{
		API_Business_Close(Business_State_MonitorDs);
		return -1;
	}
	return 0;
}
int LinphoneMonitorDsStop(void)
{
	DsTalkClose(1);
	close_dsmonitor_client(1);//Api_Ds_Show_Stop(1);
	API_Business_Close(Business_State_MonitorDs);
	return 0;
}
int DsUnlock1Deal(int ins)
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd;
		
	
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= 0;
	
		api_udp_c5_ipc_send_data(Ds_Menu_Para[ins].target_ip,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}

int DsUnlock2Deal(int ins)
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd;
	
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= 1;
	
		api_udp_c5_ipc_send_data(Ds_Menu_Para[ins].target_ip,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}

int DsTalkDeal(int ins)
{
	int rev = -1;
	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
		int len = sizeof(VtkUnicastCmd_Stru);
	
		//VtkUnicastCmd.call_type		= 0x34;
		//VtkUnicastCmd.call_code		= 1;
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= CALL_STATE_TALK;
		if( api_udp_c5_ipc_send_req( Ds_Menu_Para[ins].target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
		{
			if(rsp_cmd.rspstate == 0)
			{
				API_talk_on_by_unicast(Ds_Menu_Para[ins].target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
				rev = 0;
			}
		}
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return rev;
}
int DsAppTalkDeal(int ins)
{
	int rev = -1;
	pthread_mutex_lock(&MonitorObj_Lock);
	if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
		int len = sizeof(VtkUnicastCmd_Stru);
	
		//VtkUnicastCmd.call_type		= 0x34;
		//VtkUnicastCmd.call_code		= 1;
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= CALL_STATE_TALK;
		if( api_udp_c5_ipc_send_req( Ds_Menu_Para[ins].target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
		{
			if(rsp_cmd.rspstate == 0)
			{
				API_talk_on_by_type(IX_IX2_APP,Ds_Menu_Para[ins].target_ip,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
				rev = 0;
			}
		}
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return rev;
}
int DsTalkClose(int ins)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_BYE;
	if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
		api_udp_c5_ipc_send_data(Ds_Menu_Para[ins].target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	API_talk_off();
	return 0;
}


monitor_sbu	one_monitor = {0};

// �ͻ��˽ӿ�
int is_monitor_client_busy(void)
{
	//int cs_service_state = api_get_video_cs_service_state();
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state != MONITOR_IDLE)
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		return 1;
	}
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}

int open_monitor_client_local(unsigned short vres_id)
{
	return -1;
}

int open_monitor_client_remote( int ip,unsigned short gw_id, unsigned short apply_type,unsigned char vres_id,int t_time,int *pmap_ip,unsigned char *pmap_vres_id)
{
	//send_ip_mon_link_req
	int link_result;	// 0/ok, -1/�豸�쳣��1/proxy ok, 2/�豸������
	int remote_ip;
	unsigned char remote_dev_id;
	int target_ip;
	
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state != MONITOR_IDLE)
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		return -1;
	}
	
	if(ip == 0)
	{
		target_ip = GatewayId_Trs2_IpAddr(gw_id);	//czn_20170118
	}
	else
	{
		target_ip = ip;
	}
	link_result = send_ip_mon_link_req( target_ip,apply_type,vres_id, &remote_ip, &remote_dev_id );
	// ��������
	if( link_result == 0 )
	{
		if( api_video_c_service_turn_on( ip_video_multicast, remote_ip, remote_dev_id, t_time ) == 0 )
		{
			one_monitor.state = MONITOR_REMOTE;
			one_monitor.target_ip = remote_ip;
			one_monitor.device_id = remote_dev_id;
			one_monitor.period = t_time;
			one_monitor.apply_type = REASON_CODE_MON;
	
			if(pmap_ip != NULL)
			{
				*pmap_ip = remote_ip;
			}
			if(pmap_vres_id != NULL)
			{
				*pmap_vres_id = remote_dev_id;
			}
			dprintf("mon link 0x%08x:%d req ok!\n",remote_ip,remote_dev_id);
			pthread_mutex_unlock(&MonitorObj_Lock);
			return 0;
		}
		else
		{
			pthread_mutex_unlock(&MonitorObj_Lock);
			return 3;
		}
	}
	// �豸����
	else if( link_result == -1 )
	{
		dprintf("mon link req er!\n");
		pthread_mutex_unlock(&MonitorObj_Lock);
		return 1;
	}
	// ����������
	else
	{
		dprintf("mon link req ng!\n");
		pthread_mutex_unlock(&MonitorObj_Lock);
		return 2;			
	}
}

int close_monitor_client( void )
{
	pthread_mutex_lock(&MonitorObj_Lock);
	api_video_c_service_turn_off();
	API_talk_off();		//czn_20170109
	one_monitor.state = MONITOR_IDLE;
	one_monitor.target_ip = 0;
	one_monitor.device_id = 0;
	one_monitor.period = 0;
	pthread_mutex_unlock(&MonitorObj_Lock);

	return 0;
}
int monitor_video_client_beoff(void)		//czn_20170112
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state == MONITOR_REMOTE && one_monitor.apply_type == REASON_CODE_MON)
	{
		API_talk_off();	//czn_20170109
		one_monitor.state = MONITOR_IDLE;
		one_monitor.target_ip = 0;
		one_monitor.device_id = 0;
		one_monitor.period = 0;
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);

    	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
	#if 0
	if(GetCurMenuCnt() == MENU_028_MONITOR2)
	{
		popDisplayLastMenu();
	}
	#endif
	
	return 0;
}

int monitor_client_montocall(void)			//czn_20170112
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state != MONITOR_IDLE)
	{
		api_video_c_service_turn_off();
		API_talk_off();	//czn_20170109
	
		one_monitor.state = MONITOR_IDLE;
		one_monitor.target_ip = 0;
		one_monitor.device_id = 0;
		one_monitor.period = 0;
	}
	pthread_mutex_unlock(&MonitorObj_Lock);
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
	#if 0
	if(GetCurMenuCnt() == MENU_028_MONITOR2)
	{
		popDisplayLastMenu();
	}
	#endif
	return 0;
}
// �������˽ӿ�
int is_monitor_server_busy( void )
{	
	pthread_mutex_lock(&MonitorObj_Lock);
	int cs_service_state = api_get_video_cs_service_state();
	
	if(cs_service_state == VIDEO_CS_SERVER ||cs_service_state == VIDEO_CS_BOTH)
	{
		pthread_mutex_unlock(&MonitorObj_Lock);
		return 1;
	}
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}
int get_monitor_server_all_link( void )
{
	return (get_total_subscriber_list());
}
int get_monitor_server_link_info( int user_cnt, monitor_sbu* pMonDat )
{
	
}
int close_monitor_server( void )
{
	dprintf("close_monitor_server!\n");
	pthread_mutex_lock(&MonitorObj_Lock);
	api_video_s_service_turn_off();
	API_talk_off();		//czn_20170109
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}


//czn_20160516
int ip_mon_command_process(UDP_MSG_TYPE *pUdpType)
{
	VtkUnicastCmd_Stru* pSBUDat = (VtkUnicastCmd_Stru*)pUdpType->pbuf;	
	//short vtkcmd = (pSBUDat->cmd_type<<8) |pSBUDat->cmd_sub_type;		
	// ��������ҵ��
	switch(pUdpType->cmd)
	{
		case VTK_CMD_UNLOCK_E003:
			API_Relay_ON_Timer(3);      //lyx 20170612
			break;
		default:
			break;
	}
	return 0;
}

int Monitor_DtUnlockReq_Deal(void)
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd;
		
	
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= 0;
	
		api_udp_c5_ipc_send_data(one_monitor.target_ip,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}

int Monitor_DtUnlock2Req_Deal(void)
{
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd;
	
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= 1;
	
		api_udp_c5_ipc_send_data(one_monitor.target_ip,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return 0;
}
//czn_20170109_s
int Monitor_DtTalkReq_Deal(void)	//czn_20190529
{
	int rev = -1;
	pthread_mutex_lock(&MonitorObj_Lock);
	if(one_monitor.state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
		int len = sizeof(VtkUnicastCmd_Stru);
	
		//VtkUnicastCmd.call_type		= 0x34;
		//VtkUnicastCmd.call_code		= 1;
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= CALL_STATE_TALK;
		if( api_udp_c5_ipc_send_req( one_monitor.target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
		{
			if(rsp_cmd.rspstate == 0)
			{
				//API_POWER_TALK_ON();  
				API_talk_on_by_unicast(one_monitor.target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
				rev = 0;
			}
		}
		#if 0
		if(api_udp_c5_ipc_send_data(one_monitor.target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru))==0)
		{
			API_POWER_TALK_ON();  
			API_talk_on_by_unicast(one_monitor.target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
		}
		#endif
	}
	
	pthread_mutex_unlock(&MonitorObj_Lock);
	return rev;
}
//czn_20170109_e
int Monitor_DtTalkCloseReq_Deal(void)	//czn_20190529
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	//VtkUnicastCmd.call_type		= 0x34;
	//VtkUnicastCmd.call_code		= 1;
	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_BYE;
	if(one_monitor.state == MONITOR_REMOTE)
		api_udp_c5_ipc_send_data(one_monitor.target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}

int Monitor_TalkReq(int target_ip)	//czn_20190529
{
	int rev = -1;

	VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
	int len = sizeof(VtkUnicastCmd_Stru);

	//VtkUnicastCmd.call_type		= 0x34;
	//VtkUnicastCmd.call_code		= 1;
	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_TALK;
	if( api_udp_c5_ipc_send_req(target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
	{
		if(rsp_cmd.rspstate == 0)
		{
			//API_POWER_TALK_ON();  
			API_talk_on_by_unicast(target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
			rev = 0;
		}
	}
	
	return rev;
}
//czn_20170109_e
int Monitor_TalkCloseReq(int target_ip)	//czn_20190529
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	//VtkUnicastCmd.call_type		= 0x34;
	//VtkUnicastCmd.call_code		= 1;
	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_BYE;
	api_udp_c5_ipc_send_data(target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}
#if 0
int get_monitor_state(void)
{
	return one_monitor.state;
}

// return: 0/ok, -1/state err, 1/start video service err
int open_monitor_local(void)
{
	int monitor_time;
	// ��ȡ����ʱ�����
	monitor_time = 100;

	if( one_monitor.state == MONITOR_IDLE )
	{
		// �����豸
		// one_monitor.device_id
	
		if( API_VIDEO_S_SERVICE_TURN_ON_TEST(monitor_time) == 0 )
		{
			one_monitor.state = MONITOR_LOCAL;
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
		return -1;
}

// return: 0/ok, -1/state err, 1/start video service err
int open_monitor_remote( int ip )
{
	int monitor_time;
	// ��ȡ����ʱ�����
	monitor_time = 100;

	if( one_monitor.state == MONITOR_IDLE )
	{
		one_monitor.target_ip	= ip;	
	
		// �����豸
		// one_monitor.device_id

		if( api_video_c_service_turn_on( ip_video_multicast, ip, 0, monitor_time ) == 0 )
		{
			one_monitor.state 		= MONITOR_REMOTE;
			one_monitor.target_ip	= ip;			
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
		return -1;
}

// return: 0/ok, -1/state err, 1/stop video service err
int close_monitor( void )
{
	if( one_monitor.state != MONITOR_IDLE )
	{
		if( one_monitor.state == MONITOR_LOCAL )
		{
			if( api_video_s_service_close() == 0 )
			{
				one_monitor.state = MONITOR_IDLE;
				return 0;
			}
			else
			{
				return 1;
			}			
		}
		else
		{
			// �ر��豸״̬
			//VtkUnicastRsp.code
		
			if( api_video_c_service_turn_off() == 0 )
			{
				one_monitor.state = MONITOR_IDLE;
				return 0;
			}
			else
			{
				return 1;
			}						
		}
	}
	else
		return -1;
}
#endif

int dsmonitor_client_change_resolution(int ins, int ip,int resolution)
{
	if(Ds_Menu_Para[ins].state == MONITOR_IDLE||Ds_Menu_Para[ins].target_ip!=ip)
	{
		return -1;
	}	
	return api_video_client_change_resolution_req(ins,ip,resolution);
}
