//czn_20160708_s
#include "./task_survey/task_survey.h"	
#include "./task_survey/sys_msg_process.h"
#include "./task_io_server/obj_TableProcess.h"
#include "task_debug_sbu/task_debug_sbu.h"
//czn_20160708_e
#include "./task_io_server/obj_ResourceTable.h"		//czn_20160827
#include "./task_monitor/obj_ip_mon_vres_map_table.h"		//czn_20161216
//IX2_TEST #include "./task_VideoMenu/task_VideoMenu.h"
#include "./task_io_server/task_IoServer.h"

#include "utility.h"
#include "./device_manage/obj_SYS_VER_INFO.h"
#include "cJSON.h"

/****************************************************************************************************************************
 * @fn:		init_vdp_common_queue
 *
 * @brief:	��ʼ��vdpͨ�����ݶ���
 *
 * @param:  	pobj 			- ���ж���
 * @param:  	qsize 			- ���д�С
 * @param:  	powner 			- ���е�ӵ����
 * @param:	   	process		   	- ���е����ݴ�������
 				
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int init_vdp_common_queue(p_Loop_vdp_common_buffer pobj, int qsize, msg_process process, void* powner )
{
	// create embOS queue
	pobj->QSize		= qsize;
	pobj->pQBuf		= (unsigned char*)malloc(qsize);
	OS_Q_Create( &pobj->embosQ, pobj->pQBuf, qsize );

	pobj->process	= process;
	pobj->powner 	= powner;

	if( pobj->pQBuf == NULL ) printf("malloc err!\n"); //else printf("malloc addr = 0x%08x\n",pobj->pQBuf);
		
	return 0;
}

/****************************************************************************************************************************
 * @fn:		exit_vdp_common_queue
 *
 * @brief:	�ͷ�һ������
 *
 * @param:  	pobj 			- ���ж��� 
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int exit_vdp_common_queue( p_Loop_vdp_common_buffer pobj )
{
	free( pobj->pQBuf );
	return 0;
}

/****************************************************************************************************************************
 * @fn:		push_vdp_common_queue
 *
 * @brief:	��vdpͨ�ö����м�����Ϣ����
 *
 * @param:  	pobj 	- ���ж���
 * @param:  	data 	- ����ָ��
 * @length:  	length 	- ���ݳ���
 *
 * @return: 	0/full, !0/current data pointer
****************************************************************************************************************************/
int push_vdp_common_queue( p_Loop_vdp_common_buffer pobj, char *data, unsigned int length)		//czn_20170712
{
	return OS_Q_Put (&pobj->embosQ, data, length);	//0: ok, 1: full
}

/****************************************************************************************************************************
 * @fn:		pop_vdp_common_queue
 *
 * @brief:	�Ӷ����еõ�����ָ��
 *
 * @param:  	pobj 		- ���ж���
 * @param:  	pdb 		- ����ָ��
 * @param:  	timeout 	- ��ʱʱ�䣬msΪ��λ
 *
 * @return: 	size of data
****************************************************************************************************************************/
int pop_vdp_common_queue(p_Loop_vdp_common_buffer pobj, p_vdp_common_buffer* pdb, int timeout)
{
	if( !timeout )
		return OS_Q_GetPtr( &pobj->embosQ,  (void*)pdb );
	else
		return OS_Q_GetPtrTimed( &pobj->embosQ, (void*)pdb, timeout );
}

/****************************************************************************************************************************
 * @fn:		purge_vdp_common_queue
 *
 * @brief:	�������Ķ���
 *
 * @param:  	pobj 		- ���ж���
 *
 * @return: 	size of data
****************************************************************************************************************************/
int purge_vdp_common_queue(p_Loop_vdp_common_buffer pobj)
{
	OS_Q_Purge( &pobj->embosQ );
	return 0;
}

void* vdp_public_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

/****************************************************************************************************************************
 * @fn:		init_vdp_common_task
 *
 * @brief:	��ʼ��һ������
 *
 * @param:  	ptask 			- �������?
 * @param:  	task_id			- ����ID
 * @param:  	pthread			- ��������̷߳�����?
 * @param:	   	msg_buf		   	- ��������?���� - �������첽��Ϣ
 * @param:	   	syc_buf		   	- ������?������ - ������ͬ����Ϣ
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int init_vdp_common_task( vdp_task_t* ptask, int task_id, void* (*pthread)(void*), p_Loop_vdp_common_buffer msg_buf, p_Loop_vdp_common_buffer syc_buf )
{
	ptask->task_id 			= task_id;
	ptask->p_msg_buf		= msg_buf;
	ptask->p_syc_buf		= syc_buf;
	ptask->task_pid			= 0;
	ptask->task_run_flag 	= 1;
	if( pthread  != NULL )
	{
		if( pthread_create(&ptask->task_pid, 0, pthread, ptask) != 0 )
		{
			eprintf("Create task thread Failure,%s\n", strerror(errno));
		}
		else
		{
			dprintf("Create task thread pid = %lu\n", ptask->task_pid);	
		}
	}	
	return 0;
}

/****************************************************************************************************************************
 * @fn:		exit_vdp_common_task
 *
 * @brief:	����һ������
 *
 * @param:  	ptask 			- �������?
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int exit_vdp_common_task( vdp_task_t* ptask )
{
	ptask->task_run_flag = 0;
	pthread_join( ptask->task_pid, NULL );
	return 0;	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int select_ex(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	int r;
	do
	{
		r = select(nfds, readfds, writefds, exceptfds, timeout);
	}while((-1 == r) && (EINTR == errno));
	return r;
}

int ioctlex(int fd, int request, void * arg)
{
	int r;
	do
	{
		r = ioctl(fd, request, arg);
	}while((-1 == r) && (EINTR == errno));
	return r;
}

int sem_wait_ex(sem_t *p_sem, int semto)
{
	struct timeval tv;
	struct timespec ts;
	int ret;
	
	if(semto > 0)
	{
		if(gettimeofday(&tv, 0) == -1)
		{
			printf("clock_gettime call failure!msg:%s\n", strerror(errno));
		}
		ts.tv_sec = (tv.tv_sec + (semto / 1000 ));
		ts.tv_nsec = (tv.tv_usec  + ((semto % 1000) * 1000)) * 1000;
		ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
		ts.tv_nsec %= 1000 * 1000 * 1000;
		
		while(((ret = sem_timedwait(p_sem, &ts)) != 0) && (errno == EINTR));
		if(errno == ETIMEDOUT)
		{
			ret = 0;
		}
	}
	else
	{
		while(((ret = sem_wait(p_sem)) != 0) && (errno == EINTR));
	}	
	return ret;
}

int sem_wait_ex2(sem_t *p_sem, int semto)
{
	struct timeval tv;
	struct timespec ts;
	int ret;
	
	if(semto > 0)
	{
		if(gettimeofday(&tv, 0) == -1)
		{
			printf("clock_gettime call failure!msg:%s\n", strerror(errno));
		}
		ts.tv_sec = (tv.tv_sec + (semto / 1000 ));
		ts.tv_nsec = (tv.tv_usec  + ((semto % 1000) * 1000)) * 1000;
		ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
		ts.tv_nsec %= 1000 * 1000 * 1000;
		
		ret = sem_timedwait(p_sem, &ts);
	}
	else
	{
		while(((ret = sem_wait(p_sem)) != 0) && (errno == EINTR));
	}	
	return ret;
}

int set_block_io(int fd, int is_block)
{
	int socket_flags;

	socket_flags = fcntl(fd, F_GETFL, 0);
	if(socket_flags < 0)
	{
		printf("Cant Not Get Socket Flags!\n");
		return -1;
	}
	if(is_block)
	{
		if(fcntl(fd, F_SETFL, (socket_flags & ~O_NONBLOCK)) < 0)
		{
			printf("Cant Not Set Socket Flags Block!\n");
			return -1;
		}
	}
	else
	{
		if(fcntl(fd, F_SETFL, (socket_flags | O_NONBLOCK)) < 0)
		{
			printf("Cant Not Set Socket Flags Non_Block!\n");
			return -1;
		}
	}
	return 0;
}

int get_format_time(char *tstr)
{
	int len;
	time_t t;
	t = time(NULL);	
	strcpy(tstr, ctime(&t));
	len = strlen(tstr);
	tstr[len-1] = '\0';
	return len;
}
      
#define MAC_BCAST_ADDR      (unsigned char *) "\xff\xff\xff\xff\xff\xff"   
#define ETH_INTERFACE       "eth0"   
  
struct arpMsg 
{  
	struct ethhdr ethhdr;       		/* Ethernet header */  
	unsigned short htype;              	/* hardware type (must be ARPHRD_ETHER) */  
	unsigned short ptype;              	/* protocol type (must be ETH_P_IP) */  
	unsigned char  hlen;               	/* hardware address length (must be 6) */  
	unsigned char  plen;               	/* protocol address length (must be 4) */  
	unsigned short operation;          /* ARP opcode */  
	unsigned char  sHaddr[6];          /* sender's hardware address */  
	unsigned char  sInaddr[4];         /* sender's IP address */  
	unsigned char  tHaddr[6];          	/* target's hardware address */  
	unsigned char  tInaddr[4];         /* target's IP address */  
	unsigned char  pad[18];            	/* pad for min. Ethernet payload (60 bytes) */  
};  
  
struct server_config_t 
{  
	unsigned int server;       		/* Our IP, in network order */  
	unsigned int start;        		/* Start address of leases, network order */  
	unsigned int end;          		/* End of leases, network order */  
	struct option_set *options; 	/* List of DHCP options loaded from the config file */  
	char *interface;       	 		/* The name of the interface to use */  
	int ifindex;            			/* Index number of the interface to use */  
	unsigned char arp[6];       		/* Our arp address */  
	unsigned long lease;        		/* lease time in seconds (host order) */  
	unsigned long max_leases;   	/* maximum number of leases (including reserved address) */  
	char remaining;         			/* should the lease file be interpreted as lease time remaining, or * as the time the lease expires */  
	unsigned long auto_time;    	/* how long should udhcpd wait before writing a config file.  * if this is zero, it will only write one on SIGUSR1 */  
	unsigned long decline_time;     	/* how long an address is reserved if a client returns a * decline message */  
	unsigned long conflict_time;    	/* how long an arp conflict offender is leased for */  
	unsigned long offer_time;   	/* how long an offered address is reserved */  
	unsigned long min_lease;    	/* minimum lease a client can request*/  
	char *lease_file;  
	char *pidfile;  
	char *notify_file;      			/* What to run whenever leases are written */  
	unsigned int siaddr;       		/* next server bootp option */  
	char *sname;            			/* bootp server name */  
	char *boot_file;        			/* bootp boot file option */  
};    
  
struct server_config_t server_config;
  
  
/*
interface 	- �����豸���� �ӿ�
ifindex	- �������� 
addr		- ����IP��ַ 
arp		- ����arp��ַ
*/  
int read_interface(char *interface, int *ifindex, u_int32_t *addr, unsigned char *arp)  
{  
	int fd;  
	/*ifreq�ṹ������/usr/include/net/if.h����������ip��ַ������ӿڣ�����MTU�Ƚӿ���Ϣ�ġ� 
	���а�����һ���ӿڵ����ֺ;������ݡ������Ǹ������壬�п�����IP��ַ���㲥��ַ���������룬MAC�ţ�MTU���������ݣ��� 
	ifreq������ifconf�ṹ�С���ifconf�ṹͨ���������������нӿڵ���Ϣ�ġ� 
	*/  
	struct ifreq ifr;  
	struct sockaddr_in *our_ip;  

	memset(&ifr, 0, sizeof(struct ifreq));  
	
	/*����һ��socket������SOCK_RAW��Ϊ�˻�ȡ������������IP�����ݣ�      IPPROTO_RAW�ṩӦ�ó�������ָ��IPͷ���Ĺ��ܡ�    */  
	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) 
	{  
		ifr.ifr_addr.sa_family = AF_INET;  
		
		/*���������͸�ֵ��ifr_name*/  
		strcpy(ifr.ifr_name, interface);  
  
		if (addr) 
		{  
			/*SIOCGIFADDR���ڼ����ӿڵ�ַ*/  
			if (ioctl(fd, SIOCGIFADDR, &ifr) == 0) 
			{
				/*��ȡ����IP��ַ��addr��һ��ָ��õ�ַ��ָ��?*/  
				our_ip = (struct sockaddr_in *) &ifr.ifr_addr;  
				*addr = our_ip->sin_addr.s_addr;  
				printf("%s (our ip) = %s\n", ifr.ifr_name, inet_ntoa(our_ip->sin_addr));  
			} 
			else 
			{  
				printf("SIOCGIFADDR failed, is the interface up and configured?: %s\n",  strerror(errno));  
				return -1;  
            		}  
		}
  
		/*SIOCGIFINDEX���ڼ����ӿ�����*/  
		if (ioctl(fd, SIOCGIFINDEX, &ifr) == 0) 
		{  
			printf("adapter index %d\n", ifr.ifr_ifindex);  
			/*ָ��ifindex ��ȡ����*/  
			*ifindex = ifr.ifr_ifindex;  
		} 
		else 
		{  
			printf("SIOCGIFINDEX failed!: %s\n", strerror(errno));  
			return -1;  
		}  
		
		/*SIOCGIFHWADDR���ڼ���Ӳ����ַ*/  
		if (ioctl(fd, SIOCGIFHWADDR, &ifr) == 0) 
		{  
			/*����ȡ��Ӳ����ַ���Ƶ��ṹserver_config������arp[6]������*/  
			memcpy(arp, ifr.ifr_hwaddr.sa_data, 6);  
			printf("adapter hardware address %02x:%02x:%02x:%02x:%02x:%02x\n",  arp[0], arp[1], arp[2], arp[3], arp[4], arp[5]);  
		} 
		else 
		{  
			printf("SIOCGIFHWADDR failed!: %s\n", strerror(errno));  
			return -1;  
		}  
	}  
	else 
	{  
		printf("socket failed!: %s\n", strerror(errno));  
		return -1;  
	}  
	close(fd);  
	return 0;  
}  
  
  
/*
yiaddr 	- Ŀ��IP��ַ
ip		- ����IP��ַ
mac		- ����mac��ַ
interface	- ��������
*/  
int arpping(u_int32_t yiaddr, u_int32_t ip, unsigned char *mac, char *interface)  
{  
	int timeout 	= 2;  
	int optval 	= 1;  
	int s;                      		/* socket */  
	int rv 		= 0;                 		/* return value */  
	struct sockaddr 	addr; 	/* for interface name */  
	struct arpMsg 	arp;  
	fd_set 			fdset;  
	struct timeval 	tm;  
	time_t 			prevTime;
  
	/*socket����һ��arp��*/  
	if ((s = socket (PF_PACKET, SOCK_PACKET, htons(ETH_P_ARP))) == -1) 
	{  
		eprintf("Could not open raw socket\n");  
		return -1;  
	}  
      
	/*�����׽ӿ�����Ϊ�㲥�������arp���ǹ㲥�����������?*/  
	if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval)) == -1) 
	{  
		eprintf("Could not setsocketopt on raw socket\n");  
		close(s);  
		return -1;  
	}
  
	/* ��arp���ã����ﰴ��arp���ķ�װ��ʽ��ֵ���ɣ����http://blog.csdn.net/wanxiao009/archive/2010/05/21/5613581.aspx */  
	memset(&arp, 0, sizeof(arp));  
	
	memcpy(arp.ethhdr.h_dest, MAC_BCAST_ADDR, 6);  	/* MAC DA */  
	memcpy(arp.ethhdr.h_source, mac, 6);        			/* MAC SA */  
	
	arp.ethhdr.h_proto 	= htons(ETH_P_ARP);     		/* protocol type (Ethernet) */  
	arp.htype 			= htons(ARPHRD_ETHER);        	/* hardware type */  
	arp.ptype 			= htons(ETH_P_IP);            		/* protocol type (ARP message) */
	arp.hlen 				= 6;                   				/* hardware address length */  
	arp.plen 				= 4;                   				/* protocol address length */  
	arp.operation 			= htons(ARPOP_REQUEST);       	/* ARP op code */  
	
	*((u_int *) arp.sInaddr) = ip;          					/* source IP address */  
	memcpy(arp.sHaddr, mac, 6);         					/* source hardware address */  
	*((u_int *) arp.tInaddr) = yiaddr;      					/* target IP address */  
  
	memset(&addr, 0, sizeof(addr));  
	strcpy(addr.sa_data, interface);  
	
	/*����arp����*/  
	if( sendto(s, &arp, sizeof(arp), 0, &addr, sizeof(addr)) < 0 ) 
	{
		eprintf("arp send error: %s\n", strerror(errno));  
		rv = 0;  
	}
	/* ����select�������ж�·�ȴ�*/  
	tm.tv_usec = 0;
	time(&prevTime);
	while( timeout > 0 ) 
	{
		FD_ZERO(&fdset);
		FD_SET(s, &fdset);		
		tm.tv_sec = timeout;  
		
		if( select(s + 1, &fdset, (fd_set *) NULL, (fd_set *) NULL, &tm) < 0 ) 
		{  
			eprintf("Error on ARPING request: %s\n", strerror(errno));  
			if (errno != EINTR) rv = 0;  
		} 
		else if( FD_ISSET(s, &fdset) ) 
		{  
			if( recv(s, &arp, sizeof(arp), 0) < 0 )
			{
				dprintf("received one reply err.....\n");  
			}
			else			
			{
				dprintf("received one reply ok.....\n");  
			}
			/*�������? htons(ARPOP_REPLY) bcmp(arp.tHaddr, mac, 6) == 0 *((u_int *) arp.sInaddr) == yiaddr ���߶�Ϊ�棬��ARPӦ����Ч,˵�������ַ���ѽ����ڵ�?*/  
			if( (arp.operation == htons(ARPOP_REPLY)) &&  (bcmp(arp.tHaddr, mac, 6) == 0)  &&  (*((u_int *) arp.sInaddr) == yiaddr) ) 
			{  
				dprintf("Valid arp reply receved for this address\n");  
				rv = 1;  
				break;  
			}  
		}  
		timeout -= (time(NULL) - prevTime);
		time(&prevTime);  
	}  
	close(s);  
	return rv;  
}  
  
  
int check_ip(u_int32_t addr)  
{  
	struct in_addr temp;  
  
	if( arpping(addr, server_config.server, server_config.arp, ETH_INTERFACE) > 0 )  
	{  
		temp.s_addr = addr;  
		dprintf("%s belongs to someone, reserving it for %ld seconds\n",  inet_ntoa(temp), server_config.conflict_time);  
		return 1;  
	}  
	else  
	    return 0;  
}  
  
  
int check_ip_repeat( int ip )  
{         
	int ret = 0;

	/*����̫���ӿں�������ȡһЩ������Ϣ*/  
	if (read_interface(ETH_INTERFACE, &server_config.ifindex,  &server_config.server, server_config.arp) < 0)  
	{  
		exit(0);  
	}  
	  
	/*IP���?��*/  
	ret = check_ip(ip);
	if( ret <= 0)  
	{  
		dprintf("IP:%08x can use\n", ip);   
	}  
	else  
	{  
		dprintf("IP:%08x conflict\n", ip);  
	} 	  
	return ret;  
}  

#define IP_STR_MAX_LENGTH	16
#define MAC_STR_MAX_LENGTH	18

int GetLocalIpByDevice(char* net_device_name)
{  
	char* pIP;
	int sock_get_ip;  
	int ip;
	struct   ifreq ifr_ip;   
	
	char netCardName[10];

	if(net_device_name == NULL)
	{
		net_device_name = NET_ETH0;
	}
	strcpy(netCardName, net_device_name);
	
	if ((sock_get_ip=socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{  
		eprintf("socket create failse...GetLocalIp!\n");  
		return -1;  
	}  
	memset(&ifr_ip, 0, sizeof(ifr_ip));
	strncpy(ifr_ip.ifr_name, netCardName, sizeof(ifr_ip.ifr_name) - 1);     
	if( ioctl( sock_get_ip, SIOCGIFADDR, &ifr_ip) < 0 )     
	{     
		close( sock_get_ip ); 
		return -1;
	}       

	// �õ�ip��ַ���ַ���
	pIP = inet_ntoa(((struct sockaddr_in *)&ifr_ip.ifr_addr)->sin_addr);

	// �õ������ֽ���IP ��ַ
	ip = htonl(inet_network(pIP) );

	close( sock_get_ip ); 
	
	return ip; 
}  



char* my_inet_ntoa(int ip_n, char* ip_a)
{
	struct in_addr temp_addr;
	
	temp_addr.s_addr = ip_n;

	if(ip_a != NULL)
	{
		strcpy(ip_a, inet_ntoa(temp_addr));
	}
	
	return ip_a;
}

char* my_inet_ntoa2(int ip_n)
{
	struct in_addr temp_addr;
	static char tempIP[16];
	
	temp_addr.s_addr = ip_n;

	strcpy(tempIP, inet_ntoa(temp_addr));
	
	return tempIP;
}


int ConvertIpStr2IpInt( const char* ipstr, int* ipint )
{
	struct in_addr s;  						// IPv4��ַ�ṹ��
	inet_pton(AF_INET, ipstr, (void *)&s);
	*ipint = s.s_addr;
	return 0;
}

int ConvertIpInt2IpStr( int ipint, char* ipstr  )
{
	struct in_addr s;  						// IPv4��ַ�ṹ��

	s.s_addr = ipint;

	inet_ntop(AF_INET, (void *)&s, ipstr, 16);
		
	return 0;
}

int socket_bind_to_device(int sockfd, char *net_dev_name)
{
	int m_loop;
	struct ifreq ifr;

	if(net_dev_name == NULL)
	{
		return -1;
	}
	
	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", sockfd);
		return -1;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n", sockfd);
		return -1;
	}
	
	return 0;
}
int create_trs_udp_socket(char *net_dev_name)
{
	int m_loop;
	int sockfd = -1;
	struct ifreq ifr;
	struct sockaddr_in self_addr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n");
		goto create_udp_socket_error;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		goto create_udp_socket_error;
	}
	
	//Bind Self Address Port
	memset(&self_addr, 0, sizeof(struct sockaddr_in)); 
	self_addr.sin_family = AF_INET; 
	self_addr.sin_addr.s_addr = INADDR_ANY;
	self_addr.sin_port = 0; 
	if(bind(sockfd, (struct sockaddr *)&self_addr, sizeof(struct sockaddr_in)) == -1)
	{     
		eprintf("bind call failure!msg:%s\n", strerror(errno));
		goto create_udp_socket_error;
	}

	return sockfd;
	
	create_udp_socket_error:
	if(sockfd != -1)
	{
		close(sockfd);
	}

	
	return -1;	
}


int create_rcv_udp_socket(char *net_dev_name, unsigned short prot, int block )
{
	int m_loop;
	int sockfd = -1;
	struct ifreq ifr;
	struct sockaddr_in self_addr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n");
		goto create_udp_socket_error;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		goto create_udp_socket_error;
	}
	
	//Set Asynchronous IO
	if(set_block_io(sockfd, block))
	{
		eprintf("Set Asynchronous IO Failure!\n");
		goto create_udp_socket_error;
	}
	//Bind Self Address Port
	memset(&self_addr, 0, sizeof(struct sockaddr_in)); 
	self_addr.sin_family = AF_INET; 
	self_addr.sin_addr.s_addr = INADDR_ANY;
	self_addr.sin_port = htons(prot); 
	if(bind(sockfd, (struct sockaddr *)&self_addr, sizeof(struct sockaddr_in)) == -1)
	{     
		eprintf("bind call failure!msg:%s\n", strerror(errno));
		goto create_udp_socket_error;
	}
	
	//Dont Recv Loop Data
	#if 1
	m_loop = 1;
	if(setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, &m_loop, sizeof(int)) == -1)
	{
		printf("Setsockopt IPPROTO_IP::IP_MULTICAST_LOOP Failure!\n");
		goto create_udp_socket_error;
	}
	#endif
	
 	return sockfd;
 	
create_udp_socket_error:
	if(sockfd != -1)
	{
		close(sockfd);
	}
	return -1;
}

int join_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr)
{
	struct ifreq ifr;
	struct ip_mreq mreq;

	//Get Loacl IP Addr
	memset(ifr.ifr_name, 0, IFNAMSIZ);	
	sprintf(ifr.ifr_name,"%s", net_dev_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)//ʹ�� SIOCGIFADDR ��ȡ�ӿڵ�ַ
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto add_multicast_group_error;
	}
	
	//Added The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; //inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));	// ������ַ����mcg_addr�鲥��
	if(setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)
	{
		printf("=============================Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Failure!==================================\n");
		goto add_multicast_group_error;
	}	
	printf("============================Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Successful!====================================\n");
	
	return 0;
add_multicast_group_error:
	return -1;
}

int join_multicast_group_ip(int socket_fd, int mcg_addr, int addr)
{
	struct ip_mreq mreq;

	//Added The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; 	//inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = addr;		// inet_addr(addr);
	if(setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)
	{
		printf("Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Failure!\n");
		return -1;
	}	
	return 0;
}

int leave_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr)
{
	struct ifreq ifr;
	struct ip_mreq mreq;

	//Get Loacl IP Addr
	sprintf(ifr.ifr_name,"%s", net_dev_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto leave_multicast_group_error;
	}
	//Leave The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; //inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));
	if(setsockopt(socket_fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)		// ������ַ�뿪mcg_addr�鲥��
	{
		printf("Setsockopt IPPROTO_IP::IP_DROP_MEMBERSHIP Failure!\n");
		goto leave_multicast_group_error;
	}
	printf("Setsockopt IPPROTO_IP::IP_DROP_MEMBERSHIP ok!\n");
	
	return 0;
leave_multicast_group_error:
	return -1;
}

int send_comm_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length)
{
	int ret;

	ret = sendto(sock_fd, data, length, 0, (struct sockaddr*)&sock_target_addr,sizeof(struct sockaddr_in));
	
	if( ret == -1 )
	{
		eprintf("can not send data from socket! errno:%d,means:%s\n",errno,strerror(errno));
		return -1;
	}
	
	printf("send one package: sock = %d, ip = 0x%x, port = %d....\n",sock_fd,htonl(sock_target_addr.sin_addr.s_addr),htons(sock_target_addr.sin_port));
	
	return ret;
}

#if 0
/* Obtain a backtrace and print it to @code{stdout}. */
void print_trace (void)
{
	void *array[10];
	size_t size;
	char **strings;
	size_t i;
	size = backtrace(array,10);
	strings = backtrace_symbols(array, size);
	if( NULL == strings )
	{
		printf("backtrace_symbols");
	}
	else
	{
		printf( "Obtained %zd stack frames.\n", size);

		for (i = 0; i < size; i++)
			printf ("%s\n", strings[i]);

		free (strings);
		strings = NULL;
	}
}

static int backtrace_arm(void **BUFFER, int SIZE)
{
	volatile int n = 0;
	volatile int *p;
	volatile int *q;
	volatile int ebp1;
	volatile int eip1;
	volatile int i = 0;
	p = &n;
	ebp1 = p[4];
	eip1 = p[6];
	fprintf(stderr, "backtrace_arm addr: 0x%0x, param1: 0x%0x, param2: 0x%0x\n", backtrace_arm, &BUFFER, &SIZE);
	fprintf(stderr, "n addr is 0x%0x\n", &n);
	fprintf(stderr, "p addr is 0x%0x\n", &p);
	for (i = 0; i < SIZE; i++)
	{
		fprintf(stderr, "ebp1 is 0x%0x, eip1 is 0x%0x\n", ebp1, eip1);
		BUFFER[i] = (void *)eip1;
		p = (int*)ebp1;
		q = p - 5;
		eip1 = q[5];
		ebp1 = q[2];
		if (ebp1 == 0 || eip1 == 0)
			break;
	}
	fprintf(stderr, "total level: %d\n", i);
	return i;
}

#endif

#if 0
////////////////////////////////////////////////////////////////////////////////////////////////////
#include <ucontext.h>
#include <dlfcn.h>

// lzh_20191021_s
#if 0
typedef struct
{
	const char *dli_fname;  /* File name of defining object.  */
	void *dli_fbase;        /* Load address of that object.  */
	const char *dli_sname;  /* Name of nearest symbol.���纯����*/
	void *dli_saddr;        /* Exact value of nearest symbol.���纯������ʼ��ַ*/
} Dl_info;
#endif
// lzh_20191021_e

struct ucontext_ce123 
{
	unsigned long     	uc_flags;
	struct ucontext  	*uc_link;
	stack_t       		uc_stack;
	struct sigcontext 	uc_mcontext;
	sigset_t      		uc_sigmask;   /* mask last for extensibility */
}ucontext_ce123_;


struct sigframe_ce123 
{
	struct sigcontext 	sc;				//����һ��Ĵ���������?
	unsigned long 		extramask[1];
	unsigned long 		retcode;//���淵�ص�ַ
	//struct aux_sigframe aux __attribute__((aligned(8)));
}sigframe_ce123;


int backtrace_ce123 (void **array, int size)
{
	if (size <= 0)
		return 0;
	
	int *fp = 0, *next_fp = 0;
	int cnt = 0;
	int ret = 0;
	
	__asm__(
	
		"mov %0, fp\n"
		: "=r"(fp)
	);
	array[cnt++] = (void *)(*(fp-1));
	next_fp = (int *)(*(fp-3));
	while((cnt <= size) && (next_fp != 0))
	{
		array[cnt++] = (void *)*(next_fp - 1);
		next_fp = (int *)(*(next_fp-3));
	}
	ret = ((cnt <= size)?cnt:size);
	printf("Backstrace (%d deep)\n", ret);
	return ret;	
}



char ** backtrace_symbols_ce123 (void *const *array, int size)
{
	#define WORD_WIDTH 8
	Dl_info info[size];
	int status[size];
	int cnt;
	size_t total = 0;
	char **result;

	/* Fill in the information we can get from `dladdr'.  */
	for (cnt = 0; cnt < size; ++cnt)
	{
		//status[cnt] = _dl_addr(array[cnt], &info[cnt]);
		status[cnt] = dladdr(array[cnt], &info[cnt]);
		if( status[cnt] && info[cnt].dli_fname && info[cnt].dli_fname[0] != '\0' )
			/* We have some info, compute the length of the string which will be"<file-name>(<sym-name>) [+offset].  */
			total += (strlen (info[cnt].dli_fname ?: "")
				+ (info[cnt].dli_sname ? strlen (info[cnt].dli_sname) + 3 + WORD_WIDTH + 3 : 1)
				+ WORD_WIDTH + 5);
		else
			total += 5 + WORD_WIDTH;
	}
	/* Allocate memory for the result.  */
	result = (char **) malloc (size * sizeof (char *) + total);
	if (result != NULL)
	{
		char *last = (char *) (result + size);
		for (cnt = 0; cnt < size; ++cnt)
		{
			result[cnt] = last;
			if (status[cnt] && info[cnt].dli_fname && info[cnt].dli_fname[0] != '\0')
			{
				char buf[20];
				if (array[cnt] >= (void *) info[cnt].dli_saddr)
					sprintf (buf, "+%#lx", \
						(unsigned long)(array[cnt] - info[cnt].dli_saddr));
				else
					sprintf (buf, "-%#lx", \
						(unsigned long)(info[cnt].dli_saddr - array[cnt]));
				
				last += 1 + sprintf (last, "%s%s%s%s%s[%p]",
					info[cnt].dli_fname ?: "",
					info[cnt].dli_sname ? "(" : "",
					info[cnt].dli_sname ?: "",
					info[cnt].dli_sname ? buf : "",
					info[cnt].dli_sname ? ") " : " ",
					array[cnt]);
			}
			else
				last += 1 + sprintf (last, "[%p]", array[cnt]);
		}
		assert (last <= (char *) result + size * sizeof (char *) + total);
	}
	
	return result;
}




/* SIGSEGV�źŵĴ�������������ջ����ӡ�����ĵ��ù�ϵ*/
void DebugBacktrace(unsigned int sn , siginfo_t  *si , void *ptr)
{
	if(NULL != ptr)
	{
		printf("\n\nunhandled page fault (%d) at: 0x%08x\n", si->si_signo,si->si_addr);
		
		struct ucontext_ce123 *ucontext = (struct ucontext_ce123 *)ptr;
		int pc = ucontext->uc_mcontext.arm_pc;
		
		void *pc_array[1]; 
		pc_array[0] = pc;
		char **pc_name = backtrace_symbols_ce123(pc_array, 1);
		printf("%d: %s\n", 0, *pc_name);
		
		#define SIZE 100
		void *array[SIZE];
		int size, i;
		char **strings;
		size = backtrace_ce123(array, SIZE);
		strings = backtrace_symbols_ce123(array, size); 

		if( strings != NULL )
		{
			for( i=0; i<size; i++) 
			{
				printf("==%s==\n",strings[i]);
			}
			free(strings);
			strings = NULL;
		}
	}
}
#endif

int mypopen2(const char* type, FILE* fp[2], cJSON* cmd)
{
	//mypopen ��������ֵ����0����shell����Ľ���ID,���ظ�ֵ�������ִ���
	int i;
	int pfd[2];
	int pfderr[2];
	pid_t pid;
	int Ret;
	int arraySize = cJSON_GetArraySize(cmd);

	char** arg;

	arg = malloc((arraySize + 1) * sizeof(char*));

	//��׼��������ܵ��ļ�����
	if(pipe(pfd)<0) return -1;

	if(pipe(pfderr)<0)
	{
		close(pfd[0]);
		close(pfd[1]);
		return -1;
	}

	if((pid = fork()) < 0)
	{
		return -1;
	}
	else if(pid == 0)
	{
		if(*type == 'r')
		{
			if(pfd[1] != STDOUT_FILENO)
			{
				Ret = dup2(pfd[1], STDOUT_FILENO);
			}

			if(pfderr[1] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[1], STDERR_FILENO);
			}
			
		}
		else
		{
			if(pfd[0] != STDIN_FILENO)
			{
				Ret = dup2(pfd[0], STDIN_FILENO);
			}

			if(pfderr[0] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[0], STDERR_FILENO);
			}
		}

		for(i = 0; i < arraySize; i++)
		{
			arg[i] = cJSON_GetStringValue(cJSON_GetArrayItem(cmd, i));
		}
		arg[i] = NULL;

		execvp(arg[0], arg);
		free(arg);

		_exit(0);
	}
	else
	{
		if(*type == 'r')
		{
			close(pfd[1]);
			close(pfderr[1]);
			if((fp[0] = fdopen(pfd[0], type)) == NULL)
			{
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[0], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}
		}
		else
		{
			close(pfd[0]);
			close(pfderr[0]);
			if((fp[0] = fdopen(pfd[1], type)) == NULL)
			{
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[1], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}
		}
	}

	return pid;
}
//lzh_20160704_s
char tftp_local_file[100];
char tftp_ip_str[20];
int start_tftp_download( int ip_addr, char* filename )
{
	FILE* fd   = NULL;
	char tftp_str[200]={'\0'};

	memset( tftp_local_file, 0, 200);
	memset( tftp_ip_str, 0, 20);
	
	ConvertIpInt2IpStr( ip_addr,tftp_ip_str );
	snprintf(tftp_local_file,100,"/mnt/nand1-2/tftp/%s",filename);
	
	snprintf(tftp_str,200,"tftp -g -r %s -l %s -b 65464 %s",filename,tftp_local_file,tftp_ip_str);
	printf("start_tftp_download -- %s\n",tftp_str);

	if( (fd = popen( tftp_str, "r" )) == NULL )
	{
		printf( "start_tftp_download error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	printf("start_tftp_download -- over\n");

	return 0;
}

int get_tftp_file_checksum( int* pfile_len, int* pfile_cks )
{
	int len;
	int cks = 0;
	int i,j,blocks,remain,bytes;
	unsigned char buffer[512];
	// ����checksum
	FILE *file;
	file = fopen(tftp_local_file, "rb");
	if( file == NULL )
	{
		return -1;
	}	
	fseek(file, 0, SEEK_END);
	len = ftell(file);

	blocks = len/512;
	remain = len%512;

	fseek(file, 0, SEEK_SET);
	
	for( i = 0; i < blocks+1; i++ )
	{
		bytes = ((i==blocks)?remain:512);
		fread(buffer, 1, bytes, file);
		for( j = 0; j < bytes; j++ )
		{
			cks += buffer[j];
		}
	}
	fclose(file);
	*pfile_len = len;
	*pfile_cks = cks;
	return 0;
}

//lzh_20160704_e


int start_updatefile_and_reboot(int ip_addr,char* tarfilename)		//czn_20161221
{

	char cmd_line[251];
	char filepath[81] = {0};

	int filenamelen,i;

	filenamelen = strlen(tarfilename);
	
	for(i = 0; i<filenamelen;i++)
	{
		if(tarfilename[filenamelen-1 - i] == '/')
			break;
	}
	
	if(strcmp(&tarfilename[filenamelen - 3],".gz") == 0)
	{
		snprintf(cmd_line,250,"tar xzvf %s -C /mnt/nand1-2/tftp/",tarfilename);
		memcpy(filepath,&tarfilename[filenamelen - i ],i-3);
	}
	else if(strcmp(&tarfilename[filenamelen - 4],".zip") == 0)
	{
		snprintf(cmd_line,250,"unzip -o %s -d /mnt/nand1-2/tftp/",tarfilename);
		memcpy(filepath,&tarfilename[filenamelen - i ],i-4);
	}
	else
	{
		remove(tarfilename);
	
		return -1;
	}

		
	if(system(cmd_line) != 0)
	{

		remove(tarfilename);
	
		return -1;
	}

	UpgradeIni_Parser_Stru ini_parser;
	snprintf(cmd_line,250,"/mnt/nand1-2/tftp/%s/upgrade.ini",filepath);
	if(UpgradeIni_Parser(&ini_parser,cmd_line) != 0)
	{

		remove(tarfilename);
	
		return -1;
	}
	
	if(ini_parser.n329fw_cnt > 0)
	{
		for(i = 0;i < ini_parser.n329fw_cnt; i++)
		{
			snprintf(cmd_line,250,"cp /mnt/nand1-2/tftp/%s/firmware/%s   %s",filepath,ini_parser.n329fw_Items[i].fwname,ini_parser.n329fw_Items[i].update_path);
			if(system(cmd_line) != 0)
			{
				UpgradeIni_Free(&ini_parser);

				remove(tarfilename);
				
				return -1;
			}
		}
	}
	if(ini_parser.reset_enble)
	{
		FILE *pfile = fopen("/mnt/nand1-2/tftp/HaveUpdated","wb");
		if(pfile == NULL)
		{
			UpgradeIni_Free(&ini_parser);

			remove(tarfilename);
			
			bprintf("creat update flag fail\n");
			return -1;
		}
		bprintf("serverip = %08x\n",ip_addr);
		fwrite(&ip_addr,4,1,pfile);
		fclose(pfile);
	}
	
	/*if(ini_parser.have_stm32fw && ini_parser.stm32fw_update_enble)
	{
		snprintf(cmd_line,250,"/mnt/nand1-2/tftp/%s/firmware/%s",filepath,ini_parser.stm32fwname);
		FILE *pstmbin = fopen(cmd_line,"rb");
		
		if(pstmbin != NULL)
		{
			if(Stm32FwUpdate(pstmbin) == -1)
			{
				fclose(pstmbin);
				//system("rm -r /mnt/nand1-2/tftp/DT-IPG.bin");
				UpgradeIni_Free(&ini_parser);
				return -1;
			}
			//system("rm -r /mnt/nand1-2/tftp/DT-IPG.bin");
			
			fclose(pstmbin);
			//exit(0);//czn_test
		}
	}
	else*/
	{
		if(ini_parser.reset_enble)
		{
			remove(tarfilename);
			usleep(50000);	
			if(system("reboot") != 0)
			{
				UpgradeIni_Free(&ini_parser);
				bprintf("reboot fail\n");
				return -1;
			}
		}
		else
		{
			UpgradeIni_Free(&ini_parser);
			remove(tarfilename);
			return 1; //reset_disable 
		}
	
	}
	
	UpgradeIni_Free(&ini_parser);
	
	remove(tarfilename);
	
	return 0;
}

//czn_20170213_s
#include <dirent.h>
#include <sys/resource.h>
#define FWTEMPPATH			"/mnt/nand1-2/fwtemp"
#define RM_FWTEMPPATH		"rm -r /mnt/nand1-2/fwtemp"

extern char FW_Ver_Save[30];	//czn_20170327

int IsHaveUpdateNewRs(void)	//czn_20170327
{
#if 0
	FILE *pfile = fopen("/mnt/nand1-2/HaveUpdatedNewRs","r");
	char buff[30];
	int server_ip;
	int rid = 0;
	
	if(pfile == NULL)
	{
		return -1;
	}
	
	if(fgets(buff,30,pfile) != NULL)
	{
		server_ip = inet_addr(buff);
	}
	if(fgets(buff,30,pfile) != NULL)	
	{
		rid = atol(buff);
	}
	
	if(fgets(FW_Ver_Save,18,pfile) == NULL)	
	{
		FW_Ver_Save[0] = 0;
	}

	fclose(pfile);
	remove("/mnt/nand1-2/HaveUpdatedNewRs");
	sync();
	
	sleep(20);
	RsTrs_FwUpdateDoneReport(server_ip,rid,0);
#endif
	return 0;
}

int start_updatefile_and_reboot_fornewrs(char* tarfilename)	//czn_20161220
{

	return 0;
}
//czn_20170213_e

// lzh_20210804_s
static msg_process g_fw_uart_call_back = NULL;
void register_fw_uart_callback(msg_process process)
{
	g_fw_uart_call_back = process;
}
// lzh_20210804_e

int start_updatefile_and_reboot_forsdcard(int fw_place,char* tarfilename)	//czn_20181219
{
	struct dirent *entry;
    DIR *dir;
	
	char FWTEMPSDPATH[81] = {0};
	char RM_FWTEMPSDPATH[81] = {0};
	char filepath[81] = {0};
	
	char cmd_line[251];
	
	UpgradeIni_Parser_Stru ini_parser = {0};

	int filenamelen,i;
	int ret = -1;

	snprintf(FWTEMPSDPATH, 81, "/mnt/%s/fwtemp", fw_place ? "nand1-2" : "sdcard");
	snprintf(RM_FWTEMPSDPATH, 81, "rm -r /mnt/%s/fwtemp", fw_place ? "nand1-2" : "sdcard");
	
	system(RM_FWTEMPSDPATH);
	
	MakeDir(FWTEMPSDPATH);
	
	filenamelen = strlen(tarfilename);
	if(strcmp(&tarfilename[filenamelen - 3],".gz") == 0)
	{
		snprintf(cmd_line,250,"tar xzvf %s -C %s/",tarfilename, FWTEMPSDPATH);
	}

	else //if(strcmp(&tarfilename[filenamelen - 4],".zip") == 0)
	{
		snprintf(cmd_line,250,"unzip -o %s -d %s/",tarfilename, FWTEMPSDPATH);
	}

	if(system(cmd_line) != 0)
	{
		goto UpdateOver;
	}
	
    dir = opendir(FWTEMPSDPATH);
    while ((entry=readdir(dir)) != NULL)
	{
		if(entry->d_name[0]!='.')
		{
			strncpy(filepath, entry->d_name,80);
			break;
		}
	}
	closedir(dir);

	snprintf(cmd_line,250,"chmod 777 -R %s/", FWTEMPSDPATH);
	system(cmd_line);
	

	snprintf(cmd_line,250,"%s/%s/upgrade.ini", FWTEMPSDPATH, filepath);

	if(UpgradeIni_Parser(&ini_parser,cmd_line) != 0)
	{
		goto UpdateOver;
	}
	
	if(ini_parser.n329fw_cnt > 0)
	{
		for(i = 0;i < ini_parser.n329fw_cnt; i++)
		{
			snprintf(cmd_line,100,"%s/%s/firmware/%s",FWTEMPSDPATH,filepath,ini_parser.n329fw_Items[i].fwname);

			if(FwUpadte_FileCopy(cmd_line,ini_parser.n329fw_Items[i].update_path) != 0)
			{
				goto UpdateOver;
			}
		}		
	}

	// lzh_20210804_s
	if( ini_parser.have_stm32fw && ini_parser.stm32fw_update_enble )
	{
		snprintf(cmd_line,250,"%s/%s/firmware/%s",FWTEMPSDPATH,filepath,ini_parser.stm32fwname);
		UpgradeIni_Free(&ini_parser);
		system(RM_FWTEMPSDPATH);
		if( api_fw_uart_upgrade_start_file( cmd_line, 0, g_fw_uart_call_back ) == -1 )
		{
			ret = -1;
		}
		else
		{
			ret = 8;
		}
		goto UpdateOver;
	}
	// lzh_20210804_e

	if(ini_parser.reset_enble)
	{
		ret = 0;
	}
	else
	{
		ret = 1; //reset_disable 
	}

	UpdateOver:
	
		UpgradeIni_Free(&ini_parser);
		system(RM_FWTEMPSDPATH);
		sync();

	return ret;
	
}

// 0 ����������1 ����
int ProductionTestUpdate(void)
{
	UpgradeIni_Parser_Stru ini_parser;
	char cmd_line[251];
	int i;
	int ret = 0;
	
	if(UpgradeIni_Parser(&ini_parser,"/mnt/sdcard/IX471_ProductionTest/upgrade.ini") != 0)
	{
		return ret;
	}
	
	if(ini_parser.n329fw_cnt > 0)
	{
		for(i = 0;i < ini_parser.n329fw_cnt; i++)
		{
			snprintf(cmd_line,200,"/mnt/sdcard/IX471_ProductionTest/firmware/%s",ini_parser.n329fw_Items[i].fwname);
			if(FwUpadte_FileCopy(cmd_line,ini_parser.n329fw_Items[i].update_path) != 0)
			{
				break;
			}
		}
	}

	if(ini_parser.reset_enble)
	{
		
		#include "device_manage/obj_SYS_VER_INFO.h"
		FILE	*file = NULL;
		char	buff[200+1] = {0};
		SYS_VER_INFO_T sysInfo;
		
		sysInfo = GetSysVerInfo();
		
		file=fopen("/mnt/sdcard/IX471_ProductionTest/temp.txt","w");
		
		if(file != NULL)
		{
			snprintf( buff, 200, "%s\r\n", sysInfo.sn);
			fputs(buff,file);
			fclose(file);
		}
		ret = 1;
	}
	
	sync();
	UpgradeIni_Free(&ini_parser);

	return ret;
}

extern Updater_Run_Stru 	Updater_Run; 

typedef enum
{
	Configfile_Unkown	= 0,
	Configfile_GatewayTb,
	Configfile_RoomTb,
	Configfile_CdsNlTb,
	Configfile_DsNlTb,
	Configfile_ImNlTb,
	Configfile_ImGlMapTb,
	Configfile_Ds1NlTb,
	Configfile_Ds2NlTb,
	Configfile_Ds3NlTb,
	Configfile_Ds4NlTb,
}Configfig_Type;

Configfig_Type Get_Configfig_Type(char *filename)
{

	if(strcmp(filename,"ip_map_table.csv") == 0)
	{
		return Configfile_GatewayTb;
	}
	if(strcmp(filename,"device_map_table.csv") == 0)
	{
		return Configfile_RoomTb;
	}
	if(strcmp(filename,"cds_namelist_table.csv") == 0)
	{
		return Configfile_CdsNlTb;
	}
	if(strcmp(filename,"ds_general_namelist_table.csv") == 0)
	{
		return Configfile_DsNlTb;
	}
	if(strcmp(filename,"im_namelist_table.csv") == 0)
	{
		return Configfile_ImNlTb;
	}
	if(strcmp(filename,"imglmap_table.csv") == 0)
	{
		return Configfile_ImGlMapTb;
	}
	if(strcmp(filename,"ds1_namelist_table.csv") == 0)
	{
		return Configfile_Ds1NlTb;
	}
	if(strcmp(filename,"ds2_namelist_table.csv") == 0)
	{
		return Configfile_Ds2NlTb;
	}
	if(strcmp(filename,"ds3_namelist_table.csv") == 0)
	{
		return Configfile_Ds3NlTb;
	}
	if(strcmp(filename,"ds4_namelist_table.csv") == 0)
	{
		return Configfile_Ds4NlTb;
	}
	return Configfile_Unkown;
}

int IsHaveUpdate(int *ip_addr)
{
	FILE *pfile = fopen("/mnt/nand1-2/tftp/HaveUpdated","rb");

	if(pfile == NULL)
	{
		return -1;
	}

	fread(ip_addr,4,1,pfile);

	fclose(pfile);

	int err;
	usleep(50000);
	err = system("rm -r /mnt/nand1-2/tftp");
	bprintf("rm -r tftp result=%d\n",err);
	usleep(50000);
	err = system("mkdir /mnt/nand1-2/tftp");
	bprintf("mkdir tftp result=%d\n",err);

	sync();	//czn_20161221
	return 0;
}




UpgradeIniKey_Type Judge_UpgradeIniKeyType(char *buff)
{
	if(strstr( buff, "[DEVICE TYPE]" ) != NULL)
	{
		return UpgradeIniKey_DeviceType;
	}

	if(strstr( buff, "[FIRMWARE VER]" ) != NULL || strstr( buff, "[FIREWARE VER]" ) != NULL)
	{
		return UpgradeIniKey_FwVer;
	}

	if(strstr( buff, "[FIRMWARE PARSER]" ) != NULL || strstr( buff, "[FIREWARE PARSER]" ) != NULL)
	{
		return UpgradeIniKey_FwParser;
	}

	if(strstr( buff, "[FIRMWARE RESET]" ) != NULL || strstr( buff, "[FIREWARE RESET]" ) != NULL)
	{
		return UpgradeIniKey_FwReset;
	}

	if(strstr( buff, "[STM32]" ) != NULL)
	{
		return UpgradeIniKey_Stm32;
	}
	
	if(strstr( buff, "[END]" ) != NULL)
	{
		return UpgradeIniKey_End;
	}
	
	return UpgradeIniKey_Unkown;
}

int Judge_IsSpaceLine(char *buff)
{
	while(*buff != 0)
	{
		if(*buff != ' ' && *buff != '\r'&&*buff != '\n')
		{
			return 0;
		}
		buff ++;
	}

	return 1;
}

int Delete_Linebreak(char *buff,int str_len)
{
	//bprintf("%s len=%d\n",buff,str_len);
	while(str_len > 0)
	{
		if(buff[str_len -1] == '\r' || buff[str_len -1] == '\n')
		{
			buff[str_len -1] = 0;
			str_len --;
			//bprintf("delete happen\n");
		}
		else
		{
			break;
		}
	}
	//bprintf("delete return len=%d\n",str_len);
	return str_len;
}

void Delete_StrHeadTailSpace(char *str)
{
	int head_s_len = 0,tail_s_len = 0;
	int str_len = strlen(str);
	int i;
	for(i = 0;i < str_len;i ++)
	{
		if(str[i] != ' ')
			break;
		
		head_s_len ++;
	}

	for(i = 0;i < str_len;i ++)
	{
		if(str[str_len - 1 - i] != ' ')
			break;
		
		tail_s_len ++;
	}

	if(head_s_len > 0 || tail_s_len > 0)
	{
		str_len = str_len - head_s_len -tail_s_len;
		for(i = 0;i <  str_len;i++)
		{
			str[i] = str[head_s_len+i];
		}
		str[str_len] = '\0';
	}
}

int Load_FwParserItems(UpgradeIni_Parser_Stru *ini_parser,char *buff,int str_len)
{
	int i,cpy_len;
	void	*p;
	//ȥ���ո�
	for(i = 0; buff[i] != 0; )
	{
		if(isspace(buff[i]))
		{
			strcpy(&buff[i], &buff[i+1]);
			continue;
		}
		i ++;
	}
	
	//str_len = strlen(buff);
	for(i = 0;i < (str_len -1);i ++)
	{
		if(buff[i] == '=')
			break;
	}
	if(i < (str_len -1))
	{
		if(ini_parser->n329fw_cnt == 0)
		{
			p = malloc(sizeof(N329fw_Stru));
			if(p == NULL)
			{
				return -1;
			}
		}
		else
		{
			p = realloc(ini_parser->n329fw_Items,sizeof(N329fw_Stru)*(ini_parser->n329fw_cnt+1));

			if(p == NULL)
			{
				ini_parser->n329fw_cnt = 0;
				free(ini_parser->n329fw_Items);
				return -1;
			}
		}
		ini_parser->n329fw_Items = (N329fw_Stru *)p;
		memset(&ini_parser->n329fw_Items[ini_parser->n329fw_cnt],0,sizeof(N329fw_Stru));
		
		cpy_len = (i > (SERVER_FILE_NAME-1) ? (SERVER_FILE_NAME-1):i);
		memcpy(ini_parser->n329fw_Items[ini_parser->n329fw_cnt].fwname,buff,cpy_len);
		ini_parser->n329fw_Items[ini_parser->n329fw_cnt].fwname[cpy_len] = 0;
		Delete_StrHeadTailSpace(ini_parser->n329fw_Items[ini_parser->n329fw_cnt].fwname);
		strncpy(ini_parser->n329fw_Items[ini_parser->n329fw_cnt].update_path,&buff[i+1], (SERVER_FILE_NAME*2-1));

		for(i = 0;i < strlen(ini_parser->n329fw_Items[ini_parser->n329fw_cnt].update_path);i++)
		{
			if(ini_parser->n329fw_Items[ini_parser->n329fw_cnt].update_path[i] == '\\')
			{
				ini_parser->n329fw_Items[ini_parser->n329fw_cnt].update_path[i] = '/';
			}
		}
		
		ini_parser->n329fw_cnt ++;	
	}

	return 0;
}
int Load_Stm32UpdateItems(UpgradeIni_Parser_Stru *ini_parser,char *buff,int str_len)
{
	int i,item_type = 0;
	void	*p;
	
	if(strstr(buff,"fw")!=NULL)
	{
		item_type = 1;
	}
	else if(strstr(buff,"update")!=NULL)
	{
		item_type = 2;
	}
	if(item_type != 0)
	{
		for(i = 0;i < (str_len -1);i ++)
		{
			if(buff[i] == '=')
				break;
		}

		if(i < (str_len -1))
		{
			if(item_type == 1)
			{
				ini_parser->have_stm32fw = 1;
				strncpy(ini_parser->stm32fwname,&buff[i+1], SERVER_FILE_NAME-1);
			}
			else
			{
				if(strstr(&buff[i+1],"enable")!=NULL)
				{
					ini_parser->stm32fw_update_enble = 1;
				}
				else
				{
					ini_parser->stm32fw_update_enble = 0;
				}
			}
		}
	}

	return 0;
	
}

int UpgradeIni_Parser(UpgradeIni_Parser_Stru *ini_parser, char* inifilename)
{
	FILE  *pfile = NULL;
	char buff[101];
	int	str_len;

	memset(ini_parser,0,sizeof(UpgradeIni_Parser_Stru));

	if( (pfile=fopen(inifilename,"r")) == NULL )
	{
		return -1;
	}

	//UpgradeIniKey_DeviceType
	fseek( pfile, 0, SEEK_SET);
	for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
	{
		str_len = strlen(buff);
		if(str_len >= 100)
		{
			fclose(pfile);
			return -1;
		}
		
		if(str_len > 0)
		{
			if(Judge_IsSpaceLine(buff) != 0)
			{
				continue;
			}
			if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_DeviceType)
			{
				for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
				{
					str_len = strlen(buff);
					
					if(str_len >= 100)
					{
						fclose(pfile);
						return -1;
					}
					
					if(str_len > 0)
					{
						str_len = Delete_Linebreak(buff,str_len);
						if(Judge_IsSpaceLine(buff) == 0)
						{
							if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_Unkown)
							{
								strncpy(ini_parser->device_type,buff,DEVICE_TYPE_MAX_LEN-1);
							}
							break;
						}
					}
				}
				break;
			}
		}
	}
	//UpgradeIniKey_FwVer
	fseek( pfile, 0, SEEK_SET);
	for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
	{
		str_len = strlen(buff);
		if(str_len >= 100)
		{
			fclose(pfile);
			return -1;
		}
		
		if(str_len > 0)
		{
			if(Judge_IsSpaceLine(buff) != 0)
			{
				continue;
			}
			if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_FwVer)
			{
				for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
				{
					str_len = strlen(buff);
					if(str_len >= 100)
					{
						fclose(pfile);
						return -1;
					}
					
					if(str_len > 0)
					{
						str_len = Delete_Linebreak(buff,str_len);
						if(Judge_IsSpaceLine(buff) == 0)
						{
							if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_Unkown)
							{
								strncpy(ini_parser->fw_ver,buff,FW_VERSION_MAX_LEN-1);
							}
							break;
						}
					}
				}
				break;
			}
		}
	}

	//UpgradeIniKey_FwParser
	fseek( pfile, 0, SEEK_SET);
	for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
	{
		str_len = strlen(buff);
		if(str_len >= 100)
		{
			fclose(pfile);
			return -1;
		}
		
		if(str_len > 0)
		{
			if(Judge_IsSpaceLine(buff) != 0)
			{
				continue;
			}
			if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_FwParser)
			{
				for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
				{
					str_len = strlen(buff);
					if(str_len >= 100)
					{
						fclose(pfile);
						return -1;
					}
					//str_len = Delete_Linebreak(buff,str_len);
					if(str_len > 0)
					{
						str_len = Delete_Linebreak(buff,str_len);
						if(Judge_IsSpaceLine(buff) == 0)
						{
							if(Judge_UpgradeIniKeyType(buff) != UpgradeIniKey_Unkown)
							{
								break;
							}
							if(Load_FwParserItems(ini_parser,buff,str_len) == -1)
							{
								fclose(pfile);
								return -1;
							}
						}
					}
				}
				break;
			}
		}
	}

	//UpgradeIniKey_FwReset
	fseek( pfile, 0, SEEK_SET);
	for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
	{
		str_len = strlen(buff);
		if(str_len >= 100)
		{
			fclose(pfile);
			return -1;
		}
		
		if(str_len > 0)
		{
			if(Judge_IsSpaceLine(buff) != 0)
			{
				continue;
			}
			if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_FwReset)
			{
				for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
				{
					str_len = strlen(buff);
					if(str_len >= 100)
					{
						fclose(pfile);
						return -1;
					}
					//str_len = Delete_Linebreak(buff,str_len);
					if(str_len > 0)
					{
						str_len = Delete_Linebreak(buff,str_len);
						if(Judge_IsSpaceLine(buff) == 0)
						{
							if(Judge_UpgradeIniKeyType(buff) != UpgradeIniKey_Unkown)
							{
								break;
							}
							if(strstr(buff,"ENABLE") != NULL)
							{
								if(strstr(buff,"1") != NULL)
								{
									ini_parser->reset_enble = 1;
								}
								else
								{
									ini_parser->reset_enble = 0;
								}
								break;
							}
							
						}
					}
				}
				break;
			}
		}
	}

	//UpgradeIniKey_stm32
	fseek( pfile, 0, SEEK_SET);
	for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
	{
		str_len = strlen(buff);
		if(str_len >= 100)
		{
			fclose(pfile);
			return -1;
		}
		
		if(str_len > 0)
		{
			if(Judge_IsSpaceLine(buff) != 0)
			{
				continue;
			}
			if(Judge_UpgradeIniKeyType(buff) == UpgradeIniKey_Stm32)
			{
				for( memset(buff,0,101);fgets(buff,100,pfile) != NULL;memset(buff,0,101))
				{
					str_len = strlen(buff);
					if(str_len >= 100)
					{
						fclose(pfile);
						return -1;
					}
					//str_len = Delete_Linebreak(buff,str_len);
					if(str_len > 0)
					{
						str_len = Delete_Linebreak(buff,str_len);
						if(Judge_IsSpaceLine(buff) == 0)
						{
							if(Judge_UpgradeIniKeyType(buff) != UpgradeIniKey_Unkown)
							{
								break;
							}
							Load_Stm32UpdateItems(ini_parser,buff,str_len);
						}
					}
				}
				break;
			}
		}
	}

	fclose(pfile);
	
	return 0;	
}

int UpgradeIni_Free(UpgradeIni_Parser_Stru *ini_parser)
{
	if(ini_parser->n329fw_cnt > 0 && ini_parser->n329fw_Items != NULL)
	{
		free((void*)(ini_parser->n329fw_Items));
		ini_parser->n329fw_Items = NULL;
	}

	return 0;
}

void Printf_UpgradeIni_Parser_Stru(UpgradeIni_Parser_Stru *ini_parser)
{
	int i = 0;
	printf("DEVICE TYPE = %s\n",ini_parser->device_type);
	for(i = 0; ini_parser->device_type[i] != 0;i++)
	{
		printf("%02x ",ini_parser->device_type[i]);
	}
	printf("strlen= %d\n",strlen(ini_parser->device_type));
	printf("FIREWARE VER = %s\n",ini_parser->fw_ver);
	printf("FIREWARE PARSER cnt = %d\n",ini_parser->n329fw_cnt);
	
	for(i = 0;i <ini_parser->n329fw_cnt;i ++)
	{
		printf("%s = %s\n",ini_parser->n329fw_Items[i].fwname,ini_parser->n329fw_Items[i].update_path);
	}
	
	printf("FIREWARE RESET enble = %d\n",ini_parser->reset_enble);
	if(ini_parser ->have_stm32fw == 1)
	{
		printf("stm32fw name = %s,update en = %d\n",ini_parser->stm32fwname,ini_parser->stm32fw_update_enble);
	}
	else
	{
		printf("have no stm32fw\n");
	}
}

//czn_20160827_s
int UpdateResourceTable(void)
{
	int i;
	char filepath[RESOURCE_FILE_LEN+1]={0},prefilepath[RESOURCE_FILE_LEN+1]={0},cmd_line[250]={0};
	
	for(i = 0;i < Updater_Run.configfile_cnt;i ++)
	{
		memset(prefilepath,0,sizeof(prefilepath));
		
		snprintf(filepath,RESOURCE_FILE_LEN,"/mnt/nand1-2/%s",Updater_Run.resource_record[i].filename);
		
		if(API_ResourceTb_AddOneRecord(Updater_Run.resource_record[i].rid,0, filepath, prefilepath) != 0)
		{
			return -1;
		}
		
		if(strlen(prefilepath) > 0)
		{
			remove(prefilepath);
		}
		
		snprintf(cmd_line,250,"mv /mnt/nand1-2/tftp/%s   %s",Updater_Run.resource_record[i].filename,filepath);
	
		if(system(cmd_line) != 0)
		{
			return -1;
		}
	}
	
	API_ResourceTb_Flush_Immediate();
	
	return 0;
}

int UpdateFwAndResource(void)
{
	int havefwupdtae = 0,alltbupdate = 0,update_result,i;
	ResourceTb_OneRecord_Stru onerecord;
	
	for(i = 0;i < Updater_Run.configfile_cnt;i ++)
	{
		if(Updater_Run.resource_record[i].rid == RID0000_FIRMWIRE)
		{
			havefwupdtae = 1;
		}
		else
		{
			if(API_ResourceTb_GetOneRecord(Updater_Run.resource_record[i].rid, &onerecord) != 0)
			{
				return -1;
			}
			
			switch(Updater_Run.resource_record[i].rid)
			{
				case RID1000_IO_PARAMETER:
					break;
				case RID1001_TB_ROOM:
				case RID1002_TB_GATEWAY:
					if(alltbupdate == 0)
					{
						free_ip_device_table();
						load_ip_device_table();
						alltbupdate = 1;
					}
					break;
					//czn_20161216_s
				case RID1014_MON_VRES_MAP:
					//MonRes_Table_Release();		//czn_20170408++
					//MonRes_Table_Init();
					break;
					//czn_20161216_e
			}
		}
	}
	
	API_ResourceTb_Flush_Immediate();
	
	if(havefwupdtae)
	{
		if(API_ResourceTb_GetOneRecord(RID0000_FIRMWIRE, &onerecord) != 0)
		{
			return -1;
		}
		return start_updatefile_and_reboot(Updater_Run.server_ip_addr,onerecord.filename);
	}
	
	return 1;
}

//czn_20160827_e
//czn_20160708_e

//czn_20161008_s
OS_TIMER timer_NwStatus_Polling;
void NetworkStatus_Polling_Init(void)
{
	OS_CreateTimer(&timer_NwStatus_Polling, NetworkStatus_Polling, 500/20);
	OS_RetriggerTimer(&timer_NwStatus_Polling);
}
//czn_20161221_s
int GetNetworkStatus(void)		// 1 link 0 unlink
{
	char timeServer[50];
	
	API_Event_IoServer_InnerRead_All(TIME_SERVER, (uint8*)&timeServer);
	
	if(PingNetwork(timeServer))
	{	
		return 0;
	}
	else
	{
		return 1;
	}
}
//czn_20161221_e
void NetworkStatus_Polling(void)
{
	FILE *pf = popen("ifconfig eth0","r");
	int prtline = 0;
	char linestr[250];
	int	nw_status = 0;
	static int nw_status_save = 0xff;
	
	if(pf == NULL)
	{
		bprintf("open pipe error\n");
		OS_RetriggerTimer(&timer_NwStatus_Polling);
		return;
	}
	while(fgets(linestr,250,pf) != NULL)
	{
		if(strstr(linestr,"RUNNING") != NULL)
		{
			nw_status = 1;
			break;
		}
	}
	if(nw_status != nw_status_save)
	{
		nw_status_save = nw_status;
		if(nw_status == 1)
		{
			//API_Led_NormalOn(LED_NET);
		}
		else
		{
			//API_Led_NormalOff(LED_NET);
		}
	}
	pclose(pf);
	
	OS_RetriggerTimer(&timer_NwStatus_Polling);
}
//

//
#include "md5.h"  

int FileMd5_Calculate(char *file_path,unsigned char *file_md5)
{
	MD5_CTX md5;  
	int file_length,i,read_len;
	unsigned char buff[512];
	
	FILE *pf = fopen(file_path,"rb");
	
	if(pf == NULL)
	{
		return -1;
	}
	
	fseek(pf,0,SEEK_END);
	file_length = ftell(pf);
	fseek(pf,0,SEEK_SET);
	
	MD5Init(&md5);
	
	i = 0;
	while(i<file_length)
	{
		read_len = fread(buff,1,sizeof(buff),pf);
		if(read_len <= 0 || read_len>sizeof(buff))
		{
			fclose(pf);
			return -1;
		}	

		MD5Update(&md5,buff,read_len);  
		i += read_len;
	}
	fclose(pf);
    	                
	MD5Final(&md5,file_md5); 

	return 0;
}

int StringMd5_Calculate(char *str,unsigned char *file_md5)	//czn_20170922
{
	MD5_CTX md5;  
	int str_length,i,read_len;
	//unsigned char buff[512];
	
	if(str == NULL)
	{
		return -1;
	}

	str_length = strlen(str);
	
	
	MD5Init(&md5);
	
	i = 0;
	
	while(i<str_length)
	{
		read_len = ((i + 512) > str_length) ? str_length : 512;
		MD5Update(&md5,str+i,read_len);  
		i += read_len;
	}
    	                
    	MD5Final(&md5,file_md5); 
		
	return 0;
}
//czn_20161008_e

int PrintCurrentTime(int num)
{	
#if 1
	struct timeval tv;	
	if( gettimeofday(&tv, 0) == -1 )		
		printf("clock_gettime call failure!msg:%s\n\r", strerror(errno));	
	else		
		printf("::cur time %d=%d.%d\n\r",num,tv.tv_sec,tv.tv_usec);	
	return 0;
#endif
}

long long time_since_last_call(long long last)
{	
	long long current;
	struct timeval tv;	
	gettimeofday(&tv, 0);
	current = tv.tv_sec*1000000 + tv.tv_usec;	
	if( last == -1 )
		return current;
	else
		return (current-last);
}

const char* FileCopy_ExtDealList[] =
{
	"/mnt/nand1-1/videodoor.bin",
	"/mnt/nand1-1/lib_romfs.bin",
	"/mnt/nand1-2/lib_romfs.bin",
	/*
	"/mnt/nand1-1/conprog.bin",
	"/mnt/nand1-1/dt_ipg",
	*/
};

#define BspFile_KeyWord				"conprog.bin"
#define Delete_KeyWord				"DELETE"
#define NandLoader_DefaultPath		"N32926_NANDLoader_240MHz_Fast_Logo.bin"
#define BspUddateIniPath			"/mnt/nand1-2/bsp_update_tool/NANDWRITER.ini"
#define BspUddateStartShPath			"/mnt/nand1-2/bsp_update_tool/update.sh"


int SetMtdWritePara(char *nandloader_path,char *bsp_path)
{
	FILE *pf = fopen(BspUddateIniPath,"w");
	
	if(pf == NULL)
	{
		eprintf("%s\n",strerror(errno));
		return -1;
	}	
	
	fprintf(pf,"[NandLoader File Name]\n");
	fprintf(pf,"%s\n",nandloader_path);
	fprintf(pf,"\n");

	fprintf(pf,"[Execute File Name]\n");
	fprintf(pf,"%s\n",bsp_path);
	fprintf(pf,"\n");

	fprintf(pf,"[Execute Image Number]\n");
	fprintf(pf,"1\n");
	fprintf(pf,"\n");

	fprintf(pf,"[Execute Image Address]\n");
	fprintf(pf,"0\n");

	fclose(pf);
}

int StartMtdWritePara(void)
{
	system("ls -l");
	usleep(3000000);
	if(system(BspUddateStartShPath) != 0)
	{
		eprintf("%s\n",strerror(errno));
		return -1;
	}
	system("ls -l");
	usleep(3000000);
	return 0;
}

int GetFileSystemType(void)	//return -1-err, 0-fat,1-yaffs
{
	char buff[200];
	
	FILE *pf = popen("mount |grep 'yaffs2'","r");

	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(buff,200,pf) != NULL)
	{
		if(strstr(buff,"yaffs2")!= NULL)
		{
			pclose(pf);
			return 1;
		}
	}
	
	pclose(pf);
	
	return 0;
}

int BspUpdate_Process(char *nandloader_path,char *src,char *dst)
{
	int FileSystem_Type;
	
	FileSystem_Type = GetFileSystemType();

	if(FileSystem_Type == -1)
		return -1;

	if(FileSystem_Type == 0)
	{
		char cmd_line[200];
		printf("BspUpdate_Process:fat copy:%s\n",dst);
			
		snprintf(cmd_line,200,"mv %s  %s",src,dst);

		if(system(cmd_line) != 0)
		{
			eprintf("%s\n",strerror(errno));
			return -1;
		}
	}
	else if(FileSystem_Type == 1)
	{
		if(SetMtdWritePara(nandloader_path,src) < 0)
		{
			return -1;
		}

		if(StartMtdWritePara() < 0)
		{
			return -1;
		}
	}

	return 0;		
}

int FileCopy_ExtDealList_Num = sizeof(FileCopy_ExtDealList)/sizeof(char *);

void FwUpadte_DeleteDirOrFile(char *dst)
{
	struct stat buf;
	char cmd_line[200];
	int len;
	
	len = strlen(dst);
	
	stat (dst, &buf);
	
	//���Ŀ����Ŀ�?
	if(S_ISDIR(buf.st_mode))
	{
		//ɾ���ļ���
		snprintf(cmd_line,200,"rm -r %s", dst);
	}
	//���Ŀ�����ļ�?
	else if(S_ISREG(buf.st_mode))
	{
		//ɾ���ļ�
		snprintf(cmd_line,200,"rm %s", dst);
	}
		
	system(cmd_line);
}

int FwUpadte_FileCopy(char *src,char *dst)
{
	int i;
	char cmd_line[200];
	char *pch;
	
	if(src == NULL || dst == NULL)
	{
		eprintf("%s\n",strerror(errno));
		return -1;
	}
	
	if((pch = strstr(src,Delete_KeyWord)) != NULL)
	{
		if(!strcmp(pch,Delete_KeyWord))	
		{
			printf("FwUpadte_remove:%s\n",dst);
			
			FwUpadte_DeleteDirOrFile(dst);
			return 0;
		}
	}
	
	for(i = 0;i < FileCopy_ExtDealList_Num;i ++)
	{
		//printf("dst %d:%s,list[%d] %d:%s\n",strlen(dst),dst,i,strlen(FileCopy_ExtDealList[i]),FileCopy_ExtDealList[i]);
		if(strstr(dst,FileCopy_ExtDealList[i]) != NULL)
			break;
	}
	
	if(strstr(dst,BspFile_KeyWord) != NULL)
	{
		BspUpdate_Process(NandLoader_DefaultPath,src,dst);
	}
	else if(i >= FileCopy_ExtDealList_Num)	// normal deal
	{
		printf("FwUpadte_FileCopy normal:%s\n",dst);
			
		FwUpadte_DeleteDirOrFile(dst);
		
		snprintf(cmd_line,200,"mv %s  %s",src,dst);
		if(system(cmd_line) != 0)
		{
			eprintf("%s\n",strerror(errno));
			return -1;
		}
	}
	else
	{
		printf("FwUpadte_FileCopy Ext:%s\n",dst);
		int src_len,dst_len,copy_len,read_len,write_len;
		FILE *d_pf = fopen(FileCopy_ExtDealList[i],"rb+");
		if(d_pf == NULL)
		{
			eprintf("%s\n",strerror(errno));
			return -1;
		}
		
		FILE *s_pf = fopen(src,"rb");
		if(s_pf == NULL)
		{
			fclose(d_pf);
			eprintf("%s\n",strerror(errno));
			return -1;
		}
		
		fseek(d_pf,0,SEEK_END);
		dst_len = ftell(d_pf);
		fseek(d_pf,0,SEEK_SET);

		fseek(s_pf,0,SEEK_END);
		src_len = ftell(s_pf);
		fseek(s_pf,0,SEEK_SET);

		char buff[512];
		for(copy_len = 0;copy_len < src_len;)
		{
			read_len = fread(buff,1,512,s_pf);
			
			if(read_len == 0)
				break;

			write_len = fwrite(buff,1,read_len,d_pf);
		
			copy_len += write_len;
			
			if(read_len != write_len)
				break;
		}

		if(copy_len != src_len)
		{
			fclose(s_pf);
			fclose(d_pf);
			eprintf("%s",strerror(errno));
			return -1;
		}

		if(src_len < dst_len)
		{
			int fd = fileno(d_pf);
			if(fd <= 0)
			{
				fclose(s_pf);
				fclose(d_pf);
				eprintf("%s\n",strerror(errno));
				return -1;
			}
			
			if(ftruncate(fd,src_len) == -1)
			{
				fclose(s_pf);
				fclose(d_pf);
				eprintf("%s\n",strerror(errno));
				return -1;
			}	
		}

		fclose(s_pf);
		fclose(d_pf);
		remove(src);
	}
	
	return 0;
}

#include "task_Beeper.h"
#include "task_Led.h"
int Api_FwUpdate_UseSdCard(void)
{
	#define sd_fw_update_path		"/mnt/sdcard/Ix471_FwUpdate"

	char sd_fw_update_file[200];

	struct dirent *entry;
    	DIR *dir;
		
	if(Judge_SdCardLink() == 0)
	{
		BEEP_ERROR();
		return -2;
	}	
	
    	dir = opendir(sd_fw_update_path);
	if(dir == NULL)
	{
		BEEP_ERROR();
		return -2;
	}
    	while ((entry=readdir(dir))!=NULL)
	{
		if(strstr(entry->d_name,".zip")  != NULL)
		{
			//strncpy(filepath,entry->d_name,80);
			snprintf(sd_fw_update_file,200,"%s/%s",sd_fw_update_path,entry->d_name);
			break;
		}
	}
		
	closedir(dir);

	if(entry == NULL)
	{
		//BEEP_ERROR();
		BEEP_ERROR();
		return -2;
	}	
	
	BEEP_CONFIRM();
	//IX2_TEST API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_FW_UpdateStart);
	API_LedDisplay_FwUpdate();		//czn_20170926		//API_LED_POWER_FLASH_IRREGULAR();//API_LED_MENU_FLASH_SLOW();
	
	int update_result = start_updatefile_and_reboot_forsdcard(0,sd_fw_update_file);//czn_20181219
	
	API_LedDisplay_FwUpdateFinish();		//czn_20170926			API_LED_POWER_ON();
	if(update_result == 0)
	{
		BEEP_CONFIRM();
		//API_LED_MENU_OFF();
		//API_LED_POWER_FLASH_SLOW();
		usleep(1000000);
		system("reboot");
		return 0;
	}
	else if(update_result == -1)
	{
		BEEP_ERROR();
		return -1;
	}
	BEEP_CONFIRM();
	
	return 0;
}


//czn_20181219_s
#include <sys/statfs.h>
int getDiscSize( char *path, int *freeSize, int *totalSize ) 
{
	 struct statfs myStatfs;
	 if( statfs(path, &myStatfs) == -1 ) 
		  return -1;
	 
	 //娉ㄦ槑long long锛岄伩靝嶆暟鎹孩靑�??
	 *freeSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_bfree) / (long long) 1024));
	 *totalSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_blocks) /(long long) 1024));

	 return 0;
}
//czn_20181219_e

//czn_20190221_s
int getpid_bycmdline(char* cmdline)
{
	char cmd[200];
	snprintf(cmd,200,"ps |grep '%s' |grep -v 'grep'",cmdline);
	printf("getpid_bycmdline start:%s\n",cmd);
	FILE *pgetpid = popen(cmd,"r");
	if(pgetpid == NULL)
		return -1;
	if(fgets(cmd,200,pgetpid) == NULL)
	{
		pclose(pgetpid);
		return -1;
	}
	pclose(pgetpid);
	printf("getpid_bycmdline fgets:%s\n",cmd);
	int str_len = strlen(cmd);
	int i;
	for(i = 0; i < str_len;i++)
	{
		if(cmd[i] == ' ')
			continue;
		
		if(cmd[i] < '0' || cmd[i] > '9')
			break;
	}
	
	str_len = i+1;
	
	if(str_len == 0)
		return -1;
	
	cmd[str_len] = 0;

	int pid = atol(cmd);

	
	return pid;
}
//czn_20190221_e

int GetCpuMem_Polling(char* free, char* cpu)
{
	FILE* stream[2];
	int pid;
	char linestr[251] = {0};
	char *pch1,*pch2,*pch3;
	pid = mypopen("r", stream, "top", NULL);
	
	if(pid > 0)
	{
		if(myfread(stream, linestr, 250, 2) == -1)
		{
			//printf("myfread error!\n");
		}
		else
		{
			//printf("GetCpuMem linestr=%s\n", linestr);
			#if 0
			strtok(linestr,"\n");
			while(pch1 = strtok(NULL,"\n"))
			{
				if(strstr(pch1, "used, "))
				{
					pch1  += strlen("used, ");
					if(pch2 = strstr(pch1, " free"))
					{
						pch2[0] = 0;
						strcpy(free, pch1);
						printf("GetCpuMem free=%s\n", free);
					}
				}
				else if(strstr(pch1, "CPU:"))
				{
					if(pch2 = strstr(pch1, " sys"))
					{
						pch2[0] = 0;
						strcpy(cpu, pch1);
						printf("GetCpuMem cpu=%s\n", cpu);
					}
				}
			}
			#else
			strtok(linestr,"\n");
			while(pch1 = strtok(NULL,"\n"))
			{
				if(strstr(pch1, "CPU:"))
				{
					if(pch2 = strstr(pch1, " sys"))
					{
						pch2[0] = 0;
						strcpy(cpu, pch1);
						printf("GetCpuMem cpu=%s\n", cpu);
					}
				}
			}
			
			if(pch1 = strstr(linestr, "used, "))
			{
				pch1  += strlen("used, ");
				if(pch2 = strstr(pch1, " free"))
				{
					pch2[0] = 0;
					strcpy(free, pch1);
					printf("GetCpuMem free=%s\n", free);
				}
			}
			#endif
		}

	}
	mypclose(stream, &pid);

}

static float CPU_Ratio=0.0;
void CPU_Ratio_Measure(void)
{
	static uint32 last_total=0;
	static uint32 last_user=0;
	uint32 cur_total=0;
	uint32 cur_user=0;
	static uint32 timer=0;
	float total_diff;
	float user_diff;
	uint32 cpu_time[20];
	int time_cnt=0;
	char *strok_buff;
	char *pos1;
	char *pos2;
	//if(++timer<=10)
	//	return;
	//timer=0;
	char buff[500];
	FILE *pf=fopen("/proc/stat","r");
	if(pf==NULL)
	{
		printf("can't open stat file\n");
		return;
	}
	//GetMemAvailable();
	while(fgets(buff,500,pf)!=NULL)
	{
		if((pos1=strstr(buff,"cpu"))!=NULL)
		{
			pos2 = strtok_r(pos1+4," \t\n\r",&strok_buff);
			while(pos2!=NULL)
			{
				if(strcmp(pos2," \t\n\r")!=0)
				{
					cpu_time[time_cnt]=atoi(pos2);
					//printf("time %d=%d\n",time_cnt,cpu_time[time_cnt]);
					cur_total+=cpu_time[time_cnt];
					if(time_cnt<3)
						cur_user+=cpu_time[time_cnt];
					time_cnt++;
				}
				pos2 = strtok_r(NULL," \t\n\r",&strok_buff);
			}
			
			break;
		}
	}
	fclose(pf);
	if(time_cnt>3)
	{
		total_diff=(float)(cur_total-last_total);
		user_diff=(float)(cur_user-last_user);
		CPU_Ratio=user_diff/total_diff;
		printf("CPU_Ratio=%.02f\n",CPU_Ratio*100.0);
		last_total=cur_total;
		last_user=cur_user;
	}
	//CpuRatioDisplay();
}
float GetCPURatio(void)
{
	return CPU_Ratio*100.0;
}
int GetMemAvailable(void)
{
	char buff[500];
	char *strok_buff;
	char *pos1;
	char *pos2;
	int MemAvailable;
	FILE *pf=fopen("/proc/meminfo","r");
	if(pf==NULL)
	{
		printf("can't open memifo file\n");
		return;
	}
	while(fgets(buff,500,pf)!=NULL)
	{
		if((pos1=strstr(buff,"MemAvailable:"))!=NULL)
		{
			pos2 = strtok_r(pos1+strlen("MemAvailable:")," \t\n\r",&strok_buff);
			if(pos2!=NULL)
			{
				MemAvailable=atoi(pos2);
				printf("MemAvailable:%d kb\n",MemAvailable);
			}
			
			break;
		}
	}
	fclose(pf);

	if(MemAvailable<15000)
		printf("!!!!!!!!!MemAvailable only:%d kb",MemAvailable);
	return MemAvailable;
}

char* GetCurrentTime(char* buffer, int len, const char* formate)
{
    time_t rawtime;
    struct tm* info;

    time(&rawtime);

    info = localtime(&rawtime);

    strftime(buffer, len, formate, info);

    return buffer;
}