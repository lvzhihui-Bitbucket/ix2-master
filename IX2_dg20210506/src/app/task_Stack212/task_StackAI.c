
/**
  ******************************************************************************
  * @file    task_StackAI.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of task_StackAI.c
  */ 

#include "stack212.h"
#include "obj_APCIService.h"
#include "obj_AiLayer.h"

#include "task_IoServer.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
//#include "../task_io_server/vdp_IoServer_State.h"		
#include "task_survey.h"
#include "vdp_uart.h"
#include "task_Power.h"

extern AI_STAT aiStatus;
extern unsigned char l2BusMonEnable;
extern STACK_REPORT_CONTROL stackReport;
//�����ص���ʱ������ lzh_20130325
OS_TIMER TimerMute;

Loop_vdp_common_buffer	vdp_StackAI_Trs_queue;
Loop_vdp_common_buffer	vdp_StackAI_Rcv_queue;
Loop_vdp_common_buffer	vdp_StackAI_sync_queue;
vdp_task_t				task_StackAI;


// lzh_20160601_e

void* vdp_StackAI_task( void* arg );
	
void vtk_TaskInit_StackAI(int priority)
{
	init_vdp_common_queue(&vdp_StackAI_Trs_queue, 500, NULL,&task_StackAI);
	init_vdp_common_queue(&vdp_StackAI_Rcv_queue, 500, NULL,&task_StackAI);
	init_vdp_common_queue(&vdp_StackAI_sync_queue, 500, NULL,&task_StackAI);
	dprintf("vdp_StackAI_task starting............\n");
	usleep(100000);
	init_vdp_common_task(&task_StackAI, MSG_ID_StackAI, vdp_StackAI_task, NULL, &vdp_StackAI_sync_queue);
	dprintf("vdp_StackAI_task starting............\n");
	char buff[5];
	int addr;
	API_Event_IoServer_InnerRead_All(DX_IM_ADDR, buff);
	addr=atoi(buff);
	if(addr<1||addr>31)
		addr=0;
	myAddr = 0x80|(addr<< 2);
	printf("==============vtk_TaskInit_StackAI===========myAddr[%02x]",myAddr);
	//myAddr = 0x84; 
	printf("==============vtk_TaskInit_StackAI===========myAddr fixed[%02x]",myAddr);
	L2_Initial(myAddr,0x00,L2NotifyCallbackRcvProcess,L2NotifyCallbackTrsEchoProcess);
	if(API_Event_IoServer_InnerRead_All(DX_AREA_CODE, buff)==0)
		AREA_CODE=atoi(buff);

	// 8 = N329��STM8��ȡDIP״̬
	api_uart_send_pack(UART_TYPE_N2S_APPLY_DIP_STATUS, NULL, 0);

	//���ɷ��ͳ�ʱ��ʱ�� //lzh_20130325
	OS_CreateTimer(&TimerMute,TImerMuteCallBack,500/25);
	dprintf("vdp_StackAI_task starting............\n");
}


void exit_vdp_StackAI_task(void)
{
	exit_vdp_common_queue(&vdp_StackAI_Trs_queue);
	exit_vdp_common_queue(&vdp_StackAI_Rcv_queue);
	exit_vdp_common_queue(&vdp_StackAI_sync_queue);
	exit_vdp_common_task(&task_StackAI);
}

void* vdp_StackAI_task( void* arg )
{
	OS_TASK_EVENT MyEvents;
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;

	while( ptask->task_run_flag )
	{
		//等待接收或是发送事件
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_RCV_QUEUE|OS_TASK_EVENT_STACK_TRS_QUEUE,500);
		// 1. 处理接收到协议栈的数据
		//if( MyEvents&OS_TASK_EVENT_STACK_RCV_QUEUE )
		{
			RcvQueueProcess();
		}
		
		// 2. �������������ת����Ϣ
		if( MyEvents&OS_TASK_EVENT_STACK_TRS_QUEUE )
		{
			AVOID_NOISE_ENABLE;	//czn_20170817
			
			//usleep(100*1000);
			TrsQueueProcess();
			AVOID_NOISE_DISABLE;
		}
	}
	return 0;
}



void TimerMuteRetrigger(void)
{
	OS_RetriggerTimer(&TimerMute);	
}
void TImerMuteCallBack(void)
{
 	SPK_MuteOff();
}

/*******************************************************************************************
 * @fn��	CheckTrsStatusAndWaitingForTrsResult
 *
 * @brief:	����Ƿ�Ϊ����״̬�����ȴ����͵Ľ��
 * 			1)���ͽ�������Ҫ����Ƿ��͸�����EventӦ��
 * 			2)���ͽ�������Ҫ���ת���������Ƿ���Ҫ�ȴ�receipt����
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void CheckTrsStatusAndWaitingForTrsResult(void)
{	
	while( aiStatus == AI_STAT_WRITE )
	{
		OS_TASK_EVENT MyEvents;
		// �ȴ���������¼�(��ֹ��������ʱ0.6��)
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_TRS_RESULT,600);	//lzh_20130112
		
		// ��ȡ���ͽ��������idle״̬
		AI_LayerTrsPolling();
		
		// ���ݷ��ͽ���ظ�Event
		if( MyEvents&OS_TASK_EVENT_STACK_TRS_RESULT )
		{
			TrsResultResponseCallback(1);
		}
		else
		{
			TrsResultResponseCallback(0);
		}		
	}
}

/*******************************************************************************************
 * @fn��	RcvQueueProcess
 *
 * @brief:	���ն������ݴ���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void RcvQueueProcess(void)
{
	//lzh_20140508_s	
	//char len,myData[sizeof(StackFrameQ_TaskIO)];	
	char len,myData[sizeof(StackFrameQ_StackIO)];	
    //lzh_20140508_e
	void *ptrData;		
	StackFrameQ_StackIO *ptrStackData;	
    //lzh_20140508_s	
    StackFrameQ_StackIO_LongData *ptrStackLongData;
    //lzh_20140508_e
	
	//��ȡ��Ϣ��������
	while(len = pop_vdp_common_queue( &vdp_StackAI_Rcv_queue, &ptrData, 1))
	{
		printf("RcvQueueProcess,len[%d]\n",len);
		memcpy(&myData,ptrData,len);
		purge_vdp_common_queue(&vdp_StackAI_Rcv_queue);	
		if( myData[0] == OS_Q_STACK_FRAME_STACK_RCV )
		{
			ptrStackData = (StackFrameQ_StackIO*)myData;
			//1.1�������ͣ���������֡
			if( ptrStackData->datType == STACK_CMD_RECEIVE_NORMAL )
			{
				//1.1.1 ��ָ���
                 //lzh_20140508_s
                ptrStackLongData = (StackFrameQ_StackIO_LongData*)ptrStackData;
				if( (ptrStackLongData->datBuf.head.length.bitLDatCtrl.LFlag0000 == 0 ) && (ptrStackLongData->datBuf.head.length.bitLDatCtrl.LFlag10==2) )
                {
					StackAPCILongDataInputProcess(&ptrStackLongData->datBuf);
                }
				else if( ptrStackData->datBuf.head.length.bit.newcmd )
				//if( ptrStackData->datBuf.head.length.bit.newcmd )
                //lzh_20140508_e
				{
					StackAPCIInputProcess(&ptrStackData->datBuf);	
				}			
				//1.1.2 ��ָ���
				else
				{
					//����ָ�����ת����Survey����
					StackOldCommandInputProcess(&ptrStackData->datBuf);
				}
			}					
		}
		//���з��͵ȴ����ͽ�� lzh_20130410
		while( aiStatus == AI_STAT_WRITE )
		{
			OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_TRS_RESULT,600);
			AI_LayerTrsPolling();
		}		
	}	
}

/*******************************************************************************************
 * @fn��	TrsQueueProcess
 *
 * @brief:	���Ͷ������ݴ���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
#define WAIT_AWHILE_WHEN_BUSY	200
void TrsQueueProcess(void)
{
	unsigned char ret;
	char len,myData[sizeof(StackFrameQ_TaskIO)];	
	void *ptrData;		
	StackFrameQ_TaskIO 	*ptrTaskData;

	//��ȡ��Ϣ��������
	while(len = pop_vdp_common_queue( &vdp_StackAI_Trs_queue, &ptrData, 1))
	{
		memcpy(&myData,ptrData,len);
		purge_vdp_common_queue(&vdp_StackAI_Trs_queue);	
	        // lzh_20130123	//zxj_bj
	        //cao_20161105 Block_Get();
        
		if((myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_A) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_B)  )
		{
			ptrTaskData = (StackFrameQ_TaskIO*)myData;
			
			//2.1ת����ָ�stack
			if( !ptrTaskData->cmdType )
			{
				//����ת�����������͵õ��Ƿ�Ϊ�㲥����
				if( (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A ) ||
					(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B ) )
				{
					ret = 1;
					if( !StackOldCommandOutputProcess(ptrTaskData,1) )
					{
						usleep(1000*WAIT_AWHILE_WHEN_BUSY);
						ret = StackOldCommandOutputProcess(ptrTaskData,1);
					}
				}
				else
				{
					ret = 1;
					if( !StackOldCommandOutputProcess(ptrTaskData,0) )
					{
						usleep(1000*WAIT_AWHILE_WHEN_BUSY);
						ret = StackOldCommandOutputProcess(ptrTaskData,0);
					}
				}	
				if( ret )
				{
					//������ҪEventӦ���TCBָ��
					if( (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B) || (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_B) )
					{
						//��Ҫ�ȴ�Ӧ����Ҫ��������
						if( myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B )
						{
							SetWaitResponse(ptrTaskData->ptrTCB,ptrTaskData->cmd,1);
						}
						else
						{
							SetWaitResponse(ptrTaskData->ptrTCB,ptrTaskData->cmd,0);
						}
					}
					else
					{
						//����Ҫ�ȴ�Ӧ����Ҫ��������
						if( myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A )
						{
							SetWaitResponse(0,ptrTaskData->cmd,1);
						}
						else
						{
							SetWaitResponse(0,ptrTaskData->cmd,0);						
						}
					}						
				}
			}
			//2.1ת����ָ�stack
			else
			{
				ret = 1;
				if( !StackNewCommandOutputProcess(ptrTaskData) )
				{
					usleep(1000*WAIT_AWHILE_WHEN_BUSY);	
					ret = StackNewCommandOutputProcess(ptrTaskData);
				}
				if( ret )
				{
					SetWaitResponse(ptrTaskData->ptrTCB,0,0);
				}
			}					
		}
		//��ⷢ�ͽ��
		CheckTrsStatusAndWaitingForTrsResult();
	}	
}

/*******************************************************************************************
 * @fn��	AI_PushOneCmdIntoQueue
 *
 * @brief:	AI�ṩAPI -- ����һ���������
 *
 * @param:  msgtype -- ��Ϣ���ͣ�addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_PushOneCmdIntoQueue(unsigned char msgtype,unsigned short addr,unsigned char cmd, unsigned char newCmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121124
{
	StackFrameQ_TaskIO CmdPushInForSend;

	CmdPushInForSend.msgType	= msgtype;							//���͵���Ϣ����
	CmdPushInForSend.ptrTCB		= OS_GetTaskID();					//���������ID
	CmdPushInForSend.addr		= addr;								//Ŀ���ַ
	CmdPushInForSend.cmdType	= newCmd;							//0:��ָ������,1:��ָ������
	CmdPushInForSend.cmd		= cmd;								//ָ��
	CmdPushInForSend.len		= len;								//���ݳ���
	
	if( len ) 
	{
		memcpy(CmdPushInForSend.dat,ptrDat,CmdPushInForSend.len);
	}
	
	unsigned char ret;
	ret =  push_vdp_common_queue(&vdp_StackAI_Trs_queue,(unsigned char*)&CmdPushInForSend,10+CmdPushInForSend.len);	//stack_stm32
	
	OS_SignalEvent(OS_TASK_EVENT_STACK_TRS_QUEUE,&task_StackAI);
	
	return ret;	
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithoutEventAck
 *
 * @brief:	AI�ṩAPI -- ���͹㲥�������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/

unsigned char AI_BoardCastWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A,addr,cmd,0,len,ptrDat);
	return 1;
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithEventAck
 *
 * @brief:	AI�ṩAPI -- ���͹㲥������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_BoardCastWithEventAck(unsigned short addr, unsigned char cmd, unsigned char len, unsigned char *ptrDat )	//lzh_20121122
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_B,addr,cmd,0,len,ptrDat);

	//Power����ȴ�Ӧ���ڼ�����Լ�������
	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
	
	return 1;
}

/*******************************************************************************************
 * @fn��	AI_AppointWithoutEventAck
 *
 * @brief:	AI�ṩAPI -- ����ָ���������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_AppointWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,addr,cmd,0,len,ptrDat);
	return 1;	
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithEventAck
 *
 * @brief:	AI�ṩAPI -- ����ָ��������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_AppointWithEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B,addr,cmd,0,len,ptrDat);
	
	//Power����ȴ�Ӧ���ڼ�����Լ�������
	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
	
	return 1;	
}

// lyx 20160325
/*******************************************************************************************
 * @fn��	AI_AppointFastLinkWithEventAck
 *
 * @brief:	AI�ṩAPI -- ���ٲ����ⲿ�豸���ߣ����ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ
 *
 * @return: 0/ off line ��1/ on line
 *******************************************************************************************/
unsigned char AI_AppointFastLinkWithEventAck(unsigned short addr)
{
	OS_TASK_EVENT MyEvents;

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,1);

	printf("AI_AppointFastLinkWithEventAck addr=%02x\n", addr);
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B,addr,APCI_FAST_LINKING,1,0,NULL);
	
	// wait envet until receive event or timeout
	MyEvents = OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,1000);
	// lzh_20181116_s
#if 1
	if( check_aiTrsFlag_is_AI_TRS_IO_R_WAIT_ACON() )
		return 1;
	else
		return 0;
#else
	if (MyEvents & TASK_STACK_EVENT_ACK_OK) // ok
	{
		
		return 1;
	}
	else
	{
		return 0;
	}
#endif
	// lzh_20181116_e
}

//API for IoServer
/*******************************************************************************************
 * @fn��	API_PropertyValue_Read
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��ȡ����������ȴ�EventӦ��
 *
 * @param:  property_id -- ��ȡ������ID��no_of_elem -- ��ȡ�����Ը�����start_index -- ��ȡ�����Կ�ʼƫ������device_address -- ��ȡ���豸��ַ
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Read(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address)
{
	unsigned char dattmp[4];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
  
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_READ,1,4,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
	{	
		return 1;
	}
}

/*******************************************************************************************
 * @fn��	API_PropertyValue_Response
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��Ӧ��ȡ��������Ļظ������ȴ�EventӦ��
 *
 * @param:  property_id -- ��Ӧ��ȡ������ID��no_of_elem -- ��Ӧ��ȡ�����Ը�����start_index -- ��Ӧ��ȡ�����Կ�ʼƫ������device_address -- ��Ӧ���豸��ַ��ptr_Data -- ��Ӧ��ȡ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Response(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data)
{
  	unsigned char i,j;
	unsigned char dattmp[MAX_FRAME_LENGTH];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
	for( i = 4,j = 0; (j < no_of_elem) && (i < MAX_FRAME_LENGTH); i++,j++ )
	{
	  	dattmp[i] = ptr_Data[j];
	}
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_RESPONSE,1,i,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
	{	
		return 1;
	}
}

/*******************************************************************************************
 * @fn��	API_PropertyValue_Write
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��Ӧ��ȡ��������Ļظ������ȴ�EventӦ��
 *
 * @param:  property_id -- д�������ID��no_of_elem -- д������Ը�����start_index -- д������Կ�ʼƫ������device_address -- д����豸��ַ��ptr_Data -- д���ȡ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Write(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data)
{
  	unsigned char i,j;
	unsigned char dattmp[MAX_FRAME_LENGTH];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
	for( i = 4,j = 0; (j < no_of_elem) && (i < MAX_FRAME_LENGTH); i++,j++ )
	{
	  	dattmp[i] = ptr_Data[j];
	}
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_WRITE,1,i,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
	{	
		return 1;
	}
}

//API for GoServer lzh_20121212
/*******************************************************************************************
 * @fn��	API_GroupValue_Read
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��ȡ��������
 *
 * @param:  group_id -- ���ַͨ����
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Read(unsigned short group_id)
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_READ,1,0,NULL);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
	
	return 0;
}

/*******************************************************************************************
 * @fn��	API_GroupValue_Response
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��Ӧ��ȡ��������Ļظ�
 *
 * @param:  group_id -- ����ַͨ���ţ�length -- ��չ���ݳ��ȣ�ptr_Data -- ��Ӧ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Response(unsigned short group_id, unsigned char length, unsigned char *ptr_Data)
{  
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_RESPONSE,1,length,ptr_Data);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
	
	return 0;
}

/*******************************************************************************************
 * @fn��	API_GroupValue_Write
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��Ӧ��ȡ��������Ļظ�
 *
 * @param:  group_id -- ����ַͨ���ţ�length -- ��չ���ݳ��ȣ�ptr_Data -- ��Ӧ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Write(unsigned short group_id, unsigned char length, unsigned char *ptr_Data)
{
	printf("API_GroupValue_Write group_id=%d\n", group_id);
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_WRITE,1,length,ptr_Data);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER, 5000);
	
	return 0;
}
void innerBroadCast_Start(void)
{
	  unsigned char buf[5];
	  myAddr+=1;
        buf[0] = (unsigned char)((myAddr>>8)&0xff);		            // s_addr_h
        buf[1] = (unsigned char)(myAddr&0xff);		                // s_addr_l
        buf[2] = (unsigned char)((myAddr>>8)&0xff);	// t_addr_h
        buf[3] = (unsigned char)(myAddr&0xff);		// t_addr_l
       
        buf[4] = 1;     // 1 = ���ڹ㲥, 2 = ����㲥, 3 = ��Ⱥ1�㲥, 4 = ��Ⱥ2�㲥, 5 = ϵͳ�㲥
       
        
        API_GroupValue_Write(29, 5, buf);
	sleep(2);
	 myAddr-=1;	
}
void API_FishEye_Mode(unsigned short	partner_IA, unsigned char Mode)
{
	extern unsigned char myAddr;
	
	unsigned char group_value[5];
    group_value[0] = 0x00;			//����ַ_H
    group_value[1] = myAddr;		//����ַ_L
    group_value[2] = 0x00;		    //
    group_value[3] = partner_IA; 	//Ŀ���ַ
    group_value[4] = Mode;		    //
    //API_Event_GoServer_ToWrite(COBID42_FISH_EYE_CONTROL, 5, group_value);	
	API_GroupValue_Write(GA42_FISH_EYE_CONTROL, 5, group_value);
}

void API_ChangeUnlock_Para(void)
{
	char data_buf[2];
	char temp[10];
	API_Event_IoServer_InnerRead_All( UNLOCK1_TIMING,temp);
	
	data_buf[0] = atoi(temp)+0x30;	
	API_Event_IoServer_InnerRead_All( UNLOCK1_MODE, temp );
	data_buf[1] = atoi(temp)+0x30;
	API_Stack_BRD_Without_ACK_Data(CAMERA_SET,2,data_buf);
}
void Addr_Report_mcu(void)
{
	char buff[5];
	char io_ret;
	if((io_ret=API_Event_IoServer_InnerRead_All(DX_IM_ADDR, buff))!=0)
	{
		usleep(500*1000);
		io_ret=API_Event_IoServer_InnerRead_All(DX_IM_ADDR, buff);
	}
	if(io_ret==0)
	{
		io_ret=atoi(buff);
		if(io_ret<1||io_ret>31)
			io_ret=0;
		io_ret = (0x80|(io_ret<< 2));
		api_uart_send_pack(UART_TYPE_ReportAddr, &io_ret, 1);
	}
}
const unsigned short unlock_go_addr_list[] = 
{
	GA13_UNLOCK1_CONTROL,
	GA11_UNLOCK2_CONTROL,
	GA16_UNLOCK3_CONTROL,
	GA18_UNLOCK4_CONTROL,
	GA20_UNLOCK5_CONTROL,
	GA22_UNLOCK6_CONTROL,
	GA24_UNLOCK7_CONTROL,
	GA26_UNLOCK8_CONTROL
};
const unsigned char unlock_go_addr_list_len = sizeof(unlock_go_addr_list)/sizeof(unlock_go_addr_list[0]);

int IdleUnlockReq_Process(int dsno,int lockno)
{
	unsigned short unlock_go_addr;
	unsigned char group_value[2];
	
	//if(rsp_msg.obj_id != 0)
	if(lockno==0)
		API_Stack_APT_Without_ACK(DS1_ADDRESS+dsno,ST_UNLOCK);
	else
		API_Stack_APT_Without_ACK(DS1_ADDRESS+dsno,ST_UNLOCK_SECOND);
	#if 0

	unlock_go_addr = unlock_go_addr_list[dsno*2 + lockno];
	group_value[0] = 1;
	group_value[1] = 0;
	API_GroupValue_Write(unlock_go_addr,2,group_value);
	#endif
	return 0;
}