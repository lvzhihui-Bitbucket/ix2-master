
#ifndef _OBJ_APCISERVICE_H
#define _OBJ_APCISERVICE_H

//协议栈接口层APCI定义
#define APCI_GROUP_VALUE_READ           0x00    
#define APCI_GROUP_VALUE_RESPONSE       0x10    //0x1x
#define APCI_GROUP_VALUE_WRITE          0x20    //0x2x
		//组地址(GA)定义
	#define	GA1_VIDEO_REQUEST	1	//视频服务申请
	#define GA2_VIDEO_SERVICE	2	//视频服务状态报告
	#define GA3_MEM_PLAYBACK	3	//留影播放操作(上翻,下翻,删除等)
	#define GA4_MEM_REPORT		4	//留影信息报告(未读留影数,总留影数)
	#define GA5_MEM_ORDER		5	//命令留影
	#define GA6_RTC_SERVICE		6	//时钟同步发出
	#define GA7_RTC_CLIENT		7	//时钟同步申请
	#define GA8_RTC_SET			8	//时钟设置
	#define GA9_LIGHT_CONTROL	9	//楼梯灯控制
	#define GA10_LIGHT_REPORT	10	//楼梯灯状态报告
	#define GA15_HEARTBEAT		15	//心跳服务
	#define GA29_BROADCASTING				29	//广播式呼叫
	#define GA40_QSW_QUARTER	40	//?BDU?????
	#define GA41_FISH_EYE_REPORT	41	//如安装的为鱼眼摄像机, 则开视频时, 发出报告, 告知为鱼眼摄像机		//20150608
	#define GA42_FISH_EYE_CONTROL	42	//对鱼眼摄像机进行显示控制: 全屏或9分割								//20150608

	#define GA11_UNLOCK2_CONTROL	11	//??2??
	#define GA12_UNLOCK2_REPORT		12	//??2????(??)	  
	#define GA13_UNLOCK1_CONTROL	13	//??1??
	#define GA14_UNLOCK1_REPORT		14	//??1????(??)
						//czn_20170109_s
	#define GA15_HEARTBEAT			15	//??
	#define GA16_UNLOCK3_CONTROL	16	//??3??
	#define GA17_UNLOCK3_REPORT		17	//??3????(??)
	#define GA18_UNLOCK4_CONTROL	18	//??4??
	#define GA19_UNLOCK4_REPORT		19	//??4????(??)
	#define GA20_UNLOCK5_CONTROL	20	//??5??
	#define GA21_UNLOCK5_REPORT		21	//??5????(??)
	#define GA22_UNLOCK6_CONTROL	22	//??6??
	#define GA23_UNLOCK6_REPORT		23	//??6????(??)
	#define GA24_UNLOCK7_CONTROL	24	//??7??
	#define GA25_UNLOCK7_REPORT		25	//??7????(??)
	#define GA26_UNLOCK8_CONTROL	26	//??8??
	#define GA27_UNLOCK8_REPORT		27	//??8????(??)

#define APCI_PROPERTY_VAULE_READ        0xF5
#define APCI_PROPERTY_VAULE_RESPONSE    0xF6
#define APCI_PROPERTY_VAULE_WRITE       0xF7
#define APCI_INDIVIDUAL_ADDR_WRITE      0x30
#define APCI_INDIVIDUAL_ADDR_READ       0x31
#define APCI_INDIVIDUAL_ADDR_RESPONSE   0x32
#define APCI_PROG_STATE_READ            0x75
#define APCI_PROG_STATE_RESPONSE        0x76
#define APCI_PROG_STATE_WRITE           0x77

#define APCI_INDIVIDUAL_ADDR_LINK       0x60    //0x6x
#define APCI_RESTART                    0xE0    //system reset
#define APCI_BLOCK                      0xD0    //单字节

// 20171109
#define APCI_RM_REQUEST                 0x40
#define APCI_RM_REP_REQUEST             0x41
#define APCI_RM_QUIT                    0x42
#define APCI_RM_OK                      0x43
#define APCI_RM_EXIT                    0x44
#define APCI_RM_UP                      0x45
#define APCI_RM_DOWN                    0x46
#define APCI_RM_LAST                    0x47
#define APCI_RM_NEXT                    0x48
#define APCI_RM_DISPLAY                 0x49
#define APCI_RM_ENTER_INPUT             0x4A
#define APCI_RM_EXIT_INPUT              0x4B
#define APCI_RM_INPUT_RES               0x4C

#define APCI_DIVERT_GET_ACOUNT		    0x50 // 分机向IPG获取SIP账号及密码
#define APCI_DIVERT_RSP_GET_ACOUNT		0x51 // IPG向分机回复SIP账号及密码
#define APCI_DIVERT_START			    0x52 // 分机向IPG发送启动转呼命令，使用默认账号
#define APCI_DIVERT_RSP_START			0x53 // IPG向分机回复转呼命令，告知成功与否
#define APCI_DIVERT_START_SPEC		    0x54 // 分机向IPG发送启动转呼命令，使用指定账号
#define APCI_DIVERT_RSP_START_SPEC		0x55 // IPG向分机回复转呼命令，告知成功与否
#define APCI_DIVERT_RESET			    0x56 // VSIP服务器状态清除

#define APCI_DIVERT_START_INFORM 		0x57	//czn_20181117

#define APCI_RM_M_REQ                   0x80
#define APCI_RM_M_RSP                   0x81
#define APCI_RM_M_QUIT                  0x82
#define APCI_RM_M_UP                    0x83
#define APCI_RM_M_DN                    0x84
#define APCI_RM_M_LAST                  0x85
#define APCI_RM_M_NEXT                  0x86
#define APCI_RM_M_EXIT                  0x87
#define APCI_RM_M_OK                    0x88
#define APCI_RM_M_TP                    0x89

#define APCI_DIVERT_SYNC                0xF0
#define APCI_FE_ANGLE_OFFSET            0xF1
#define APCI_DS_QTY_SYNC                0xF2
#define APCI_CAM_QTY_SYNC               0xF3

#define APCI_IPC_SEARCH_ON              0xFB
#define APCI_IPG_LINK 					0xFC
#define APCI_IPG_STATE 					0xFD
#define	APCI_FAST_LINKING				0xFE // lyx 20160325

// Define Object Property

// Define Object Function - Private

//处理协议栈穿上来的新指令
unsigned char StackAPCIInputProcess(void *ptrMsgDat);
//处理任务需要转发的新指令
unsigned char StackNewCommandOutputProcess(void *ptrMsgDat);

// lzh_20140508_s
unsigned char StackAPCILongDataInputProcess(void *ptrMsgDat);
// lzh_20140508_e

// Define Object Function - Public

//处理IO Server请求发送的服务：读取参数
unsigned char API_PropertyValue_Read_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address);
//处理IO Server请求发送的服务：响应参数
unsigned char API_PropertyValue_Response_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);
//处理IO Server请求发送的服务：写入参数
unsigned char API_PropertyValue_Write_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);

#endif
