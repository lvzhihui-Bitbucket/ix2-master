
/**
  ******************************************************************************
  * @file    ai_layer.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of ai_layer.c
  ******************************************************************************
  *	@Э��Ӧ�ò㴦������
  *	@ͨ�ø�ʽ���ݰ��Ĵ�����
  *		1)IO Server APCIָ���
  *		2)��ָ���
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "stack212.h"
#include "obj_AiLayer.h"

AI_STAT aiStatus;
AI_ERR	aiError;

AI_TRS_STAT aiTrsFlag;
AI_RCV_STAT aiRcvFlag;

/*******************************************************************************************
 * @fn��	L2NotifyCallbackRcvProcess
 *
 * @brief:	Ӧ�ýӿڲ��ṩ����·��Ľ��ջص�����,Ӧ�ýӿڽ��������ݲ��Ҹ���APCI�ַ���Ϣ����ͬ��APPӦ��
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2NotifyCallbackRcvProcess(void)
{
	AI_LayerRcvService();
}

/*******************************************************************************************
 * @fn��	L2NotifyCallbackTrsEchoProcess
 *
 * @brief:	Ӧ�ýӿڲ��ṩ����·��ķ��ͷ����źŻص�����,
 *			Ӧ�ýӿڽ��������ݲ��Ҹ���APCI�ַ���Ϣ����ͬ��APPӦ��
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2NotifyCallbackTrsEchoProcess(void)
{
	OS_SignalEvent(OS_TASK_EVENT_STACK_TRS_RESULT,&task_StackAI);		
}

/*******************************************************************************************
 * @fn��	AI_LayerRcvService
 *
 * @brief:	Ӧ�ýӿڲ���շ���
 *			vtk_TaskProcessEvent_StackAI�е���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void AI_LayerRcvService(void)
{		
	L2_RCV_FLAG myFlag;	
	StackFrameQ_StackIO myData;

    //lzh_20140508_s
    StackFrameQ_StackIO_LongData *ptrMyData;
    ptrMyData = (StackFrameQ_StackIO_LongData*)&myData;
    //lzh_20140508_e	
	
	if( !L2_DataRead((unsigned char*)&myData.datBuf,&myFlag) )
	{
		printf("len==0,exit\n");
		return;
	}

	switch( myFlag )
	{
		case R_RCV_NONE:	//���շ�����
			break;
		//lzh_20130413
		case R_BLK_IND:
			//��������block
			//Block_Hold();	//�յ�block����󣬼���hold
			break;
		case R_DAT_IND:		//��·����յ�����֡���������ݵ���Ϣ�ظ�IO Server����
			myData.msgType = OS_Q_STACK_FRAME_STACK_RCV;
			myData.datType = STACK_CMD_RECEIVE_NORMAL;
			
            //lzh_20140508_s
            if( (ptrMyData->datBuf.head.length.bitLDatCtrl.LFlag0000 == 0) && (ptrMyData->datBuf.head.length.bitLDatCtrl.LFlag10 == 2) )
            {
                push_vdp_common_queue(&vdp_StackAI_Rcv_queue,&myData,sizeof(AI_HEAD) + ptrMyData->datBuf.ldata.long_len.long_len_bit.llen + 1 + 2);
            }
			else if( myData.datBuf.head.length.bit.newcmd )
			//if( myData.datBuf.head.length.bit.newcmd )
            //lzh_20140508_e
				push_vdp_common_queue(&vdp_StackAI_Rcv_queue,(char*)&myData,sizeof(AI_HEAD) + myData.datBuf.head.length.bit.len + 2 );
			else
				push_vdp_common_queue(&vdp_StackAI_Rcv_queue,(char*)&myData,sizeof(AI_HEAD) + myData.datBuf.head.length.byte + 2 );
			
			OS_SignalEvent(OS_TASK_EVENT_STACK_RCV_QUEUE,&task_StackAI);
			break;
		case R_ERR:			//��·����յ�����ָ��
			break;
	}
}

/*******************************************************************************************
 * @fn��	AI_LayerTrsPolling
 *
 * @brief:	Ӧ�ýӿڷ������ݺ�Polling����״̬
 *			HAL_Drivers�е���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void AI_LayerTrsPolling(void)
{
	L2_TRS_FLAG tmpTResult;
	
	aiStatus = AI_STAT_IDLE;
	
	L2_PollingWriteResult(&tmpTResult);
	//printf("AI_LayerTrsPolling ret=%d\n", tmpTResult);
	switch( tmpTResult )
	{
		case T_TRS_NONE:
			aiTrsFlag = AI_TRS_NONE;
			break;
		case T_BLK_REQ:
			aiTrsFlag = AI_TRS_BLK_OVER;
			break;
		case T_DAT_REQ:
			aiTrsFlag = AI_TRS_IO_R_WAIT_LCON;
			break;
			
		case T_DAT_CON:
			aiTrsFlag = AI_TRS_IO_R_WAIT_ACON;
			break;
		case T_ERR:
			aiTrsFlag = AI_TRS_OVER_ERR;
			break;
	}
}

// lzh_20181116_s
int check_aiTrsFlag_is_AI_TRS_IO_R_WAIT_ACON(void)
{
	return (aiTrsFlag == AI_TRS_IO_R_WAIT_ACON)?1:0;
}
// lzh_20181116_e

/*******************************************************************************************
 * @fn:		SendOneOldCommand
 *
 * @brief:	����һ����ָ���ʽͨ������
 *
 * @param:  boardcast - 0/�ǹ㲥���1/�㲥���2-n/����ָ��
 * @param:  target_addr - Ŀ���ַ
 * @param:	cmd - ����
 * @param:  ptrDat - ����ָ��
 * @param:  len - ���ݳ���
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char SendOneOldCommand(unsigned char boardcast,unsigned char target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len)
{
	unsigned char i,ret;
	AI_DATA myData;

	if( aiStatus != AI_STAT_IDLE )
	{
		aiError = AI_TRS_WAIT;
		return 0;
	}
	
	//�����쳣����
	if( len > MAX_FRAME_LENGTH ) len = MAX_FRAME_LENGTH;
		
	//�������
	myData.head.sa 					= L2_GetMyAddress();
	myData.head.da 					= target_addr;
	myData.head.sada				= 0;
	myData.head.cmd					= cmd;
	
	//lzh_20130909	//�����ָ��(��ָ���)���ܷ��ͳ���15byte������
	/*
	myData.head.length.bit.ack_req 	= 0;
	myData.head.length.bit.daf 		= 0;
	myData.head.length.bit.newcmd 	= 0;
	myData.head.length.bit.rep	 	= 0;	
	myData.head.length.bit.len 		= len;
	*/
	myData.head.length.byte = len;
	myData.head.length.bit.newcmd 	= 0;
	
	for( i = 0; i < len; i++ )
	{
		myData.data[i] = *ptrDat++;
	}

	//����L2����
	UpdateL2DataWrite( (unsigned char*)&myData );
	
	//lzh_20130425
	if( boardcast == 2 )	//SubModel
	{
		ret = L2_DataWrite( MIN_REPLAY_SUBMODEL, 0 );		
	}
	else if( boardcast == 1 )
	{
		ret = L2_BoardcastWrite();
	}
	else
	{
		ret = L2_DataWrite( MAX_BUS_IDLE_TIME, 0 );
	}
	
	if( ret )
	{			
		aiStatus = AI_STAT_WRITE;
		return 1;
	}
	else
	{
		aiError = AI_TRS_WAIT;
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		SendOneNewCommand
 *
 * @brief:	����һ����ָ���ʽͨ������
 *
 * @param:  daf - 0/ �豸��ַ��1/ ���ַ
 * @param:  ack - 0/ ����Ӧ��1/ ��ҪӦ��
 * @param:  rsp - 0/ ָʾ����  1/ Ӧ���  ��Ӱ��mac��ʱ��
 * @param:  target_addr - Ŀ���ַ
 * @param:	cmd - ����
 * @param:  ptrDat - ����ָ��
 * @param:  len - ���ݳ���
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char SendOneNewCommand(unsigned char daf, unsigned char rsp, unsigned char ack, unsigned short target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len)
{
	unsigned char i;
	MAC_TYPE mac;
	AI_DATA myData;
	
	if( aiStatus != AI_STAT_IDLE )
	{
		aiError = AI_TRS_WAIT;
		return 0;
	}
	
	//�����쳣����
	if( len > MAX_FRAME_LENGTH ) len = MAX_FRAME_LENGTH;
		
	//�������
	myData.head.sa 					= L2_GetMyAddress();
	myData.head.da 					= target_addr;
	myData.head.sada				= ( (L2_GetMyAddress()>>4)&0xf0 ) | (target_addr>>8);	  	
	myData.head.cmd					= cmd;
	myData.head.length.bit.ack_req 	= ack;
	myData.head.length.bit.daf 		= daf;		//0: �豸��ַ��1�����ַ
	myData.head.length.bit.newcmd 	= 1;
	myData.head.length.bit.rep	 	= 0;	
	myData.head.length.bit.len 		= len;
	for( i = 0; i < len; i++ )
	{
		myData.data[i] = *ptrDat++;
	}

	//����L2����
	UpdateL2DataWrite( (unsigned char*)&myData );
	
	//lzh_201430425
	mac = rsp?MIN_INTERVAL_SEND_RLY:MIN_INTERVAL_SEND_ALY;
		
//	if( L2_DataWrite( mac, 1 ) )	
	if( L2_DataWrite( mac, 1|(ack?0x80:0) ) )
	{			
		aiStatus = AI_STAT_WRITE;
		return 1;
	}
	else
	{
		aiError = AI_TRS_WAIT;
		return 0;
	}	
}

