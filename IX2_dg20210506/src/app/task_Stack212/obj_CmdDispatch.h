
#ifndef _OBJ_CMDDISPATCH_H
#define _OBJ_CMDDISPATCH_H

#include "utility.h"

typedef struct
{
	vdp_task_t 		*ptrTCBResponse;		//回复 Event ACK的任务ID指针
	unsigned char 	reEnable;				//重发使能标志
	unsigned char 	reSendCnt;				//等待 Receipt/Receipt_Single的重发次数
	unsigned char 	reBroadcast;			//0：重发为指定命令，1：重发为广播命令
} WAIT_RESPONSE;

extern WAIT_RESPONSE WaitResponse;

// Define Object Property

// Define Object Function - Private

unsigned char WhichReceiptShouldBeGiven(unsigned char cmd);

void SetWaitResponse(vdp_task_t *ptrTCB,unsigned char cmd,unsigned char brd);
void ResetWaitResponse(void);
void TrsResultResponseCallback(unsigned char rspOK);

// Define Object Function - Public

//处理协议栈传上来的旧命令
unsigned char StackOldCommandInputProcess(void *ptrMsgDat);

//处理task需要转发的旧指令
unsigned char StackOldCommandOutputProcess(void *ptrMsgDat,unsigned char brd);

#endif
