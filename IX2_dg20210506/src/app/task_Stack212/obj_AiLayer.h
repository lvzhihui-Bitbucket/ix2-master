/**
  ******************************************************************************
  * @file    ai_layer.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of ai_layer.c
  ******************************************************************************
  *	@协议应用层处理程序
  *	@通用格式数据包的处理：
  *		1)IO Server APCI指令处理
  *		2)旧指令处理
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _A_LEVEL_H
#define _A_LEVEL_H

#include "RTOS.h"
#include "obj_L2Layer.h"

// Define Object Property

//协议栈接口层发送到链路层的信息头结构定义
#pragma pack(1)	//stm32_stack
typedef struct
{
	//B1
	unsigned char sa;
	//B2
	unsigned char da;
	//B3
	unsigned char sada;
	//B4
	unsigned char cmd;
	//B5
	union
	{
		unsigned char byte;
		struct
		{
			unsigned char len:4;		//bit3.2.1.0:	长度计数
			unsigned char ack_req:1;	//bit4:	        ACK请求，	0/无请求ACK，1/请求目标地址的ACK
			unsigned char rep:1;		//bit5:	        首次发送，	0/重新发送，1/首次发送
			unsigned char newcmd:1;		//bit6:		    新指令包格式
			unsigned char daf:1;		//bit7: 		地址类型， 0/设备地址，1/组地址
		}bit;
        // lzh_20140508_s
		struct
		{
			unsigned char LFlag0000:4;	//bit3.2.1.0:	新指令长指令的标识(0000)
			unsigned char ack_req:1;	//bit4:	        ACK请求，	0/无请求ACK，1/请求目标地址的ACK
			unsigned char rep:1;		//bit5:	        首次发送，	0/重新发送，1/首次发送
			unsigned char LFlag10:2;	//bit7.6: 		新指令长指令的标识(10)
		}bitLDatCtrl;
		// lzh_20140508_e
	}length;
} AI_HEAD;

//应用层与链路层的交互结构
typedef struct
{
	AI_HEAD head;
	unsigned char data[MAX_FRAME_LENGTH];
} AI_DATA;

// lzh_20140508_s
typedef struct
{
	AI_HEAD head;
	struct
	{
		union
		{
			unsigned char llength;
			struct
			{
				unsigned char llen:7;
				unsigned char ldaf:1;
			}long_len_bit;
		}long_len;
		unsigned char dat[MAX_FRAME_LENGTH-1];
	}ldata;
} AI_LONG_DATA;
// lzh_20140508_e

#pragma pack()	//stm32_stack


//回调函数定义，用于用户扩展功能
typedef struct
{
	void (*BlockHookFunction)(void);
	unsigned char flag;
} AI_HOOK;

//应用层工作模式定义
typedef enum
{
	AI_STAT_IDLE = 0,	//读数据模式
	AI_STAT_WRITE,		//写数据模式
} AI_STAT;

//应用层接收模式定义
typedef enum
{
	AI_RCV_NONE = 0,		//获取数据状态
	//读
	AI_RCV_IO_TRS_RES,		//接收到IO的IND后进入发送RES状态
	AI_RCV_IO_WAIT_RCON,	//等待发送RES完成后的RCON
	AI_RCV_IO_OVER,			//接收IO数据结束
	//接收到GO更新命令处理流程
	AI_RCV_GO_W_IND,		//接收GO写更新命令
	AI_RCV_GO_W_IND_OVER,	//接收GO写更新命令结束
	//接收到GO申请命令处理流程
	AI_RCV_GO_R_REQ,		//接收GO读请求命令
	AI_RCV_GO_R_REQ_RES,	//等待GO Server取数据
	AI_RCV_GO_R_REQ_RCON,	//等待发送Res数据结束
	AI_RCV_GO_R_REQ_OVER,	//接收GO读请求结束
	//接收到块命令
	AI_RCV_BLK_IND,			//接收到块命令
	AI_RCV_BLK_OVER,		//接收到块命令处理结束
} AI_RCV_STAT;

//应用层发送模式定义
typedef enum
{
	AI_TRS_NONE = 0,			//未发送数据状态
	//发送IO读申请命令处理流程
	AI_TRS_IO_R_WAIT_LCON,		//发送了IO申请命令后，等待LCON
	AI_TRS_IO_R_WAIT_ACON,		//发送确认后，等待目标方的业务信号（必须有）
	AI_TRS_IO_R_DAT_OVER,		//发送数据结束，等待取走状态退出
	//发送GO更新命令处理流程	
	AI_TRS_IO_WAIT_LCON,		//发送了IO数据后，等待LCON
	AI_TRS_IO_DAT_OVER,		//发送数据结束，等待取走状态退出
	
	//发送GO申请命令处理流程
	AI_TRS_GO_WAIT_LCON,		//发送了GO申请命令后，等待LCON
	AI_TRS_GO_WAIT_ACON,		//发送确认后，等待目标方的业务信号（可以没有）
	AI_TRS_GO_DAT_OVER,		//发送数据结束，等待取走状态退出
	//发送GO更新命令处理流程
	AI_TRS_GO_UPDATE,		//GO更新后同步到其他设备
	AI_TRS_GO_UPDATE_OVER,	//GO更新后同步到其他设备结束
	//块数据
	AI_TRS_BLK_WAIT_LCON,	//发送了块命令后，等待LCON
	AI_TRS_BLK_OVER,		//发送了块命令后，等待取走状态
	//
	AI_TRS_OVER_ERR,
} AI_TRS_STAT;

//应用层错误数据类型
typedef enum
{	AI_ERR_NONE = 0,	//数据帧正常
	AI_LCON_TIMEROUT,	//发送申请数据包后等待LCON超时
	AI_ACON_TIMEROUT,	//发送申请数据包后等待ACON超时
	AI_TRS_WAIT,		//链路层发送等待中
	AI_L2_ERROR,		//链路层错误
} AI_ERR;

//应用层接收运行结果（与用户进程通信标志）
typedef enum
{
	AI_R_RCV_NONE = 0,	//无收发数据
	AI_R_BLK_IND,		//应用层接收到块控制指令
	AI_R_DAT_IND,		//应用层接收到数据帧
	AI_R_ERR,			//应用层接收到错误指令
} AI_RCV_FLAG;

//应用层发送运行结果（与用户进程通信标志）
typedef enum
{
	AI_T_TRS_NONE = 0,	//无收发数据
	AI_T_BLK_REQ,		//应用层发送了块控制指令
	AI_T_DAT_LCON,		//应用层发送了数据申请
	AI_T_DAT_ACON,		//应用层接收到ACON应答
	AI_T_ERR,			//链路层发送数据错误
} AI_TRS_FLAG;

extern AI_STAT aiStatus;

// Define Object Function - Private

// Define Object Function - Public

//协议栈应用接口层提供给链路层的接收回调函数
void L2NotifyCallbackRcvProcess(void);
//协议栈应用接口层提供给链路层的发送返回信号回调函数
void L2NotifyCallbackTrsEchoProcess(void);

//协议栈应用接口层接收服务
void AI_LayerRcvService(void);
//协议栈应用接口层发送服务
void AI_LayerTrsPolling(void);

//应用接口层通信接口服务

//发送一个旧指令格式命令
unsigned char SendOneOldCommand(unsigned char boardcast,unsigned char target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len);
//发送一个新指令格式命令
unsigned char SendOneNewCommand(unsigned char daf, unsigned char rsp, unsigned char ack, unsigned short target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len);

//lzh_20130328 //发送一个ACK测试信号
unsigned char SendOneNewAck(void);
//lzh_20130413	//发送一个block信号
unsigned char SendOneBlock(void);
#endif
