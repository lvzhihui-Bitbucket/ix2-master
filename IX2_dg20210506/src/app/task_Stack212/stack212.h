/**
  ******************************************************************************
  * @file    stack212.h
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of stack
  ******************************************************************************
  * @注意
  *	@该头文件为协议栈的引用和配置头文件，使用说明如下：
  *	@1.需要调用协议栈的地方需要引用该头文件: 
		1)main.c中需要加入vtk_TaskProcessEvent_StackAI任务，调用vtk_TaskInit_StackAI( taskID++ );
		2)hal_board_cfg.h中加入如下语句,定义需要协议栈功能：
			#ifndef HAL_STACK_212
			#define HAL_STACK_212 OTRUE
			#endif
		  hal_drivers.c中加入如下语句：	
			加入头文件：
			#if (defined HAL_STACK_212) && (HAL_STACK_212 == OTRUE)
			  #include "stack212.h"
			#endif
			//加入Polling函数
			#if (defined HAL_STACK_212) && (HAL_STACK_212 == OTRUE)
				AI_LayerTrsPolling(); 
			#endif
		3)TimeBase.c中需要调用STACK212_MONITOR_SERVICE来启动采样
  *	@2.该头文件中定义了调试信息的数据结构，可以输出调试统计信息
  *	@3.环境变量的定义
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _STACK212_H
#define _STACK212_H

#include "define_Command.h"
#include "obj_AiLayer.h"
#include "obj_L2Layer.h"
#include "obj_APCIService.h"
#include "obj_CmdDispatch.h"
#include "task_StackAI.h"

/********************************************************************************************************
//协议栈通信功能相关定义
********************************************************************************************************/
extern uint8 vtk_TaskID_StackAI;
extern uint8 Survey_Task_ID;

//IO Server的任务ID重定义
#define IO_SERVER_TASK_ID	0 //ram_IoServer.id
//Survey的任务重定义
#define SURVEY_TASK_ID		0 //Survey_Task_ID

#define OS_TASK_EVENT_RESPONSE_OK		(1<<0)
#define OS_TASK_EVENT_RESPONSE_ER		(1<<1)
/********************************************************************************************************
//调试统计功能相关定义
********************************************************************************************************/
#define STACK_DEBUG_REPORT

#define MSG_UARTCOMM_SENDLONG  0x05
#define MSG_UARTCOMM_SENDSHORT 0x06
#define MSG_UARTCOMM_RECV      0x07
extern uint8 Rs485_TaskID;

//环境变量
extern unsigned char EOS_Stack_Trace;
extern unsigned char EOS_Stack_BusMon;
//UART处理中心的任务号和消息类型
#define STACK_UART_PROCESS_TASK_ID		vtk_TaskID_Rs485 //	TestMsgUartReportApp_TaskID		//UartComm_TaskID
#define STACK_UART_PROCESS_TASK_MSG_T	MSG_UARTCOMM_SENDSHORT  //UART_PROCESS_DEBUG				//MSG_UARTCOMM_SENDSHORT
#define STACK_UART_PROCESS_TASK_MSG_R	MSG_UARTCOMM_RECV

//UART通信中心任务的消息类型定义：
//状态控制
#define TP1_EOS_STAT_CONTROL		0x01		
#define TP1_EOS_STAT_CONTROL_SUB1		0x01	//启动EOS统计，参数1：0/停止性能分析，1/启动性能分析
#define TP1_EOS_STAT_CONTROL_SUB2		0x02	//停止EOS统计
#define TP1_EOS_STAT_CONTROL_START		0x03	//启动Stack跟踪，参数1：设置BusMon，0/不启动，1/启动
#define TP1_EOS_STAT_CONTROL_STOP		0x04	//停止Stack跟踪
#define TP1_EOS_STAT_CONTROL_SUB3		0x05	//获取当前状态，
												//参数1：0/未启动统计，1/启动统计；
												//参数2：0/未启动性能分析，1/启动性能分析
												//参数3：上报时间间隔；1～60秒，default：3秒
												//参数4：0/未启动Stack跟踪，1/启动Stack跟踪
												//参数5：0/未启动BusMon模式，1/启动BusMon模式
//任务状态统计
#define	TP2_EOS_TASK				0x02
#define TP2_EOS_TASK_STAT				0x01
#define TP2_EOS_TASK_EOS_STAT			0x02

//协议栈统计
#define TP3_EOS_STACK				0x03
#define TP3_EOS_STACK_SEND_RESULT		0x01	//发送数据及结果
#define TP3_EOS_STACK_INDICATION		0x02	//接收到的数据
#define TP3_EOS_STACK_ERROR				0x03	//错误结果
#define TP3_EOS_STACK_BUSMON			0x04	//BUSMON状态下信息
#define TP3_EOS_STACK_PH_BUSSTATE		0x05	//物理层状态信息

//应用接口层服务(PC发送)
#define TP4_EOS_SERVICE_INDICATION	0x04
#define TP4_EOS_SERVICE_INDICATION_SUB1	0x01

//调试信息
#define TP5_EOS_DEBUG				0x05
#define TP5_EOS_DEBUG_PRINTF			0x01

//PC消息
#define TP6_EOS_PC				0x06
#define TP6_EOS_PC_TO_DEVICE			0x01
#define TP6_EOS_PC_FM_DEVICE			0x02

//属性类型
#define TP7_EOS_PROTERTY		0x07
#define TP7_EOS_PROTERTY_READ			0x01
#define TP7_EOS_PROTERTY_WRITE			0x02

//PC询问任务名称
#define TP8_EOS_TASK_NAME		0x08
#define TP8_EOS_TASK_NAME_SUB1			0x01

//获取EOS基本信息
#define TP9_EOS_BASE			0x09
#define TP9_EOS_BASE_SUB1				0x01	//参数1：index(1/设备类型，2/CPU，3/Clock，4/BSP)，参数2：DP（STRING，14chars）

//发送给UART通信中心任务的消息结构：
typedef struct
{
	//消息头
	osal_event_vtk_t hdr;
	//协议栈消息类型
	unsigned char MType;
	unsigned char SType;
	//协议栈消息内容
	unsigned char DAT[MAX_FRAME_LENGTH]; 
} STACK_REPORT_MSG;

//L2_STATE REPORT SEND&RESULT
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 sendresult;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_SEND_RESULT;

//L2_STATE REPORT INDICATION
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_INDICATION;

//L2_STATE REPORT ERROR
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	L2_ERR err;
} STACK_REPORT_MSG_TRS_ERROR;

//L2_STATE REPORT BUSMON
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_BUS_MON;

//协议栈报告发送控制:
typedef struct
{
	unsigned char enable;
	unsigned char ack;
} STACK_REPORT_CONTROL;

//Print Debug 
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 DAT[MAX_FRAME_LENGTH];
} PRINT_DEBUG_MSG;

//PC Command
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 DAT[MAX_FRAME_LENGTH];
} PC_COMMAND_MSG;

void TP3_EOS_STACK_Ph_BusState(void);
void TP3_EOS_STACK_L2_Report(unsigned char type);

#endif
