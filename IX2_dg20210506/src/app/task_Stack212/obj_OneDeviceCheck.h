/**
  ******************************************************************************
  * @file    obj_OneDeviceCheck.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.06.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_OneDeviceCheck_H
#define _obj_OneDeviceCheck_H

#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"

// Define Object Property-------------------------------------------------------
#define IM_MODE_NORMAL						0x01
#define IM_MODE_DJ							0x02
#define IM_MODE_ROUTER						0x03	

#define INSTRUCTION_TYPE_MR_LINKING_ST		0x01
#define INSTRUCTION_TYPE_ST_LINKING_MR		0x02
#define INSTRUCTION_TYPE_IO_SERVER			0x03	


// Define Object Function - Public----------------------------------------------
uint8 GetPhysicalByLogica(uint16 logicAddr, uint16* physicAddr, uint8* instruction);
uint8 DeviceLinking_Standard_Processing(uint16 logicAddr);
void InstructionCheckResponse(void);

// Define Object Function - Private---------------------------------------------
uint8 InstructionCheck(uint16 physical, uint8 cmd);

#endif
