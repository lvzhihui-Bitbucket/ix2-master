
#ifndef _L2_LEVEL_H
#define _L2_LEVEL_H

#define BROADCAST_ADDR		0x38
#define BROADCAST_CMD_LOW		0x40		//20121126
#define BROADCAST_CMD_HIGH		0x6f		//20121126

#define DR_SYS_CMD_START		0x7b		//lzh_20121203
#define DR_SYS_CMD_END			0x7e		//lzh_20121203

//#define MAX_FRAME_LENGTH	(24)		//链路层的数据长度
#define MAX_FRAME_LENGTH	(24+60)		//链路层的数据长度	// lzh_20181117

// Define Object Property

typedef unsigned char L2_ADDR_TYPE;
typedef unsigned char L2_GRP_TYPE;
typedef void (*ptrFuncion)(void);

extern L2_ADDR_TYPE myAddr;
extern int AREA_CODE;
//lzh_20130416
extern unsigned char F_L2_SEND_NEW_CMD;

#define L_Ctrl_Block	0x27	//0020,0111B

//链路层MAC控制类型
typedef enum
{
	MAX_BUS_IDLE_TIME = 0,
	MIN_INTERVAL_SEND_ACK,
	MAX_INTERVAL_WAIT_ACK,
	MAX_INTERVAL_BETWEEN_NOR,
	MAX_INTERVAL_BETWEEN_BRD,
	MAX_INTERVAL_SEND_BLK,
	MIN_INTERVAL_SEND_RLY,
	MIN_INTERVAL_SEND_ALY,
	//lzh_20140425
	MIN_REPLAY_SUBMODEL,
}MAC_TYPE;

//链路层工作模式定义
typedef enum
{
	L2_STAT_READ = 0,	//读数据模式
	L2_STAT_WRITE = 1,		//写数据模式
} L2_STAT;

//链路层接收模式定义
typedef enum
{
	L2_RCV_NONE = 0,		//获取数据状态
	L2_RCV_TRS_ACK_WAIT,	//等待发送ACK完成
	L2_RCV_OVER,			//接收数据结束，等待取走数据和状态
} L2_RCV_STAT;

//链路层发送模式定义
typedef enum
{
	L2_TRS_NONE = 0,		//未发送数据状态
	//正常数据发送流程
	L2_TRS_DAT_WAIT,		//发送了数据后，等待物理层应答
	L2_TRS_RCV_ACK_DELAY,	//发送确认后，等待目标方ACK信号时需要延时一会避开干扰
	L2_TRS_RCV_ACK,			//发送确认后，等待目标方ACK信号
	L2_TRS_DAT_OVER,		//发送数据结束，等待取走状态退出
	//块数据	
	L2_TRS_BLK_WAIT,		//发送了块命令后，等待物理层应答
	L2_TRS_BLK_OVER,		//发送了块命令后，等待取走数据
	L2_TRS_BLK_TO,			//取走数据后，等待超时后返回
	//广播数据
	L2_TRS_BRD_WAIT,		//发送了广播数据后，等待物理层应答
	L2_TRS_BRD_REP_WAIT,	//发送广播命令后，延时一会再发送第二次
	L2_TRS_BRD_REP,			//发送了广播数据后，再次发送数据
	L2_TRS_BRD_OVER,		//发送2次结束，等待取走状态后退出
	//独立发送ACK数据		//lzh_20130328
	L2_TRS_ACK_WAIT,
} L2_TRS_STAT;

//链路层错误数据类型
typedef enum
{	L2_ERR_NONE = 0,		//数据帧正常
	L2_ACK_TIMEROUT,		//发送申请数据包后等待ACK超时
	L2_LRC_ERROR,			//数据帧校验错误
	L2_LEN_ERROR,			//数据帧长度异常??
	L2_MAC_DISABLE,			//媒体访问禁止
	L2_PH_BUS_ERROR,		//物理层错误
	L2_WRITE_WAIT,			//发送数据后的等待状态
} L2_ERR;

//链路层数据帧格式
typedef struct
{
    //callback for frame just available
    ptrFuncion   frameCallback;
    //整个数据包长度  
	unsigned char pack_len;
	//SOURCE ADDRESS
	L2_ADDR_TYPE sa;
	//TARGET ADDRESS
	L2_ADDR_TYPE da;
    //apci
	unsigned char cmd;
    //checksum
	unsigned char cks;
	//LENGTH
	union
	{
		unsigned char byte;
		struct
		{
			unsigned char len:4;		//bit3.2.1.0:	长度计数
			unsigned char ack_req:1;	//bit4:	        ACK请求，	0/无请求ACK，1/请求目标地址的ACK
			unsigned char rep:1;		//bit5:	        首次发送，	0/重新发送，1/首次发送
			unsigned char newcmd:1;		//bit6:		    新指令包格式
			unsigned char daf:1;		//bit7: 		地址类型， 0/设备地址，1/组地址
		}bit;
		// lzh_20140508_s
		struct
		{
			unsigned char LFlag0000:4;	//bit3.2.1.0:	新指令长指令的标识(0000)
			unsigned char ack_req:1;	//bit4:	        ACK请求，	0/无请求ACK，1/请求目标地址的ACK
			unsigned char rep:1;		//bit5:	        首次发送，	0/重新发送，1/首次发送
			unsigned char LFlag10:2;	//bit7.6: 		新指令长指令的标识(10)
		}bitLDatCtrl;
		// lzh_20140508_e						
	}length;
    //EXTEND data
	union
	{
		//OLD EXTEND DATA TYPE
		unsigned char old_dat[MAX_FRAME_LENGTH];
		//NEW EXTEND DATA TYPE
		struct
		{
			unsigned char sada;
			unsigned char dat[MAX_FRAME_LENGTH-1];
		}new_type;
		// lzh_20140508_s
		struct
		{
			unsigned char sada;
			union
			{
				unsigned char llength;
				struct
				{
					unsigned char llen:7;
					unsigned char ldaf:1;
				}long_len_bit;
			}long_len;
			unsigned char dat[MAX_FRAME_LENGTH-2];
		}long_type;
		// lzh_20140508_e				
	}ext_dat;
} L2_FRAME;

//重发对象定义
typedef struct
{
	unsigned char enable;	//是否有效
	unsigned char times;	//重发次数
	unsigned short interval;	//重发时间间隔
} L2_RETRS;

//链路层接收运行结果（与应用层通信标志）
typedef enum
{
	R_RCV_NONE = 0,	//无收发数据
	R_BLK_IND,		//链路层接收到块控制指令
	R_DAT_IND,		//链路层接收到数据帧
	R_ERR,			//链路层接收到错误指令
} L2_RCV_FLAG;

//链路层发送运行结果（与应用层通信标志）
typedef enum
{
	T_TRS_NONE = 0,	//无收发数据
	T_BLK_REQ,		//链路层发送了快控制指令
	T_DAT_REQ,		//链路层发送了数据申请
	T_DAT_CON,		//链路层接收到ACK应答
	T_ERR,			//链路层发送数据错误
	T_ACK_TST,		//lzh_20130328	链路层发送了ACK测试
} L2_TRS_FLAG;


#define L2_RCV_FLAG_ENABLE(a,b)	{ l2ComRcvFlag = a; l2Error = b; }
#define L2_TRS_FLAG_ENABLE(a,b)	{ l2ComTrsFlag = a; l2Error = b; }

// Define Object Function - Private

//MAC控制
void MACTimingReset(void);
void MACProcess(MAC_TYPE type);
//重发机制
void L2ReTrsInit(unsigned char en,MAC_TYPE type);
unsigned char IsJust2ReTranslate(void);
//链路层监控服务（LLC服务）
void L2_LayerMonitorService(void);

unsigned char L2_IsNeedAck(void);
unsigned char L2_IsNewCmdType(void);
void L2_SetCmdType(unsigned char type);
unsigned char GetChecksum(L2_FRAME *ptrL2Frame,unsigned char save_cmp);

// Define Object Function - Public

//链路层初始化
void L2_Initial(L2_ADDR_TYPE LocalAddr,L2_GRP_TYPE GroupAddr,ptrFuncion rcvCallback,ptrFuncion trsCallback);

void UpdateL2DataWrite(unsigned char *ptrDat);

unsigned char L2_DataWrite(	MAC_TYPE macType, unsigned char uNew );
unsigned char L2_BoardcastWrite( void );

unsigned char L2_DataRead(unsigned char *pdat,L2_RCV_FLAG *flag);			//轮询读取数据和通信标志，返回读取的有效数据长度

unsigned char L2_PollingWriteResult(L2_TRS_FLAG *flag);						//轮询写入数据后的结果

unsigned char L2_BlkWrite(void);	//启动发送块控制信号

void L2_BusMonEnable(void);
void L2_BusMonDisable(void);
L2_ERR L2_GetLastError(void);			//得到总线错误状态
L2_ADDR_TYPE L2_GetMyAddress(void);		//得到本设备物理地址
L2_GRP_TYPE L2_GetMyGrpAddress(void);	//得到本设备组地址

// lzh_20130328
unsigned char L2_AckWrite(void);
// lzh_20130415
void L2_WaitForRceiveOverAndWrite(void);


//stack_stm32
void L2_LayerRead(void);
void L2_LayerWrite(void);
void TImerTrsStatusCallBack(void);


#endif
