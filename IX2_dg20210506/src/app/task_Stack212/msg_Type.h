
#ifndef _MSG_TYPE_H
#define _MSG_TYPE_H

typedef struct
{
  uint8  event;
  uint8  srcid;
} osal_event_vtk_t;

typedef struct
{
  uint8 MsgType;
  uint8 SourceTaskID;
  uint8 SourceTaskEventMark;
  uint8 event;
} osal_event_msg_t;

typedef struct {
  uint8  event;
} msgpkt_uartcomm_msg_t;

typedef struct {
  uint8  event;
  uint8  taskid;
  uint16 eventmark;
  uint16 code;
  uint8  len;
  uint8 *p_data;
} msgpkt_uartcomm_sendlongcmd_t;
  
typedef struct {
  uint8  event;
  uint8 taskid;
  uint16 eventmark;
  uint8  len;
  uint16 code;
} msgpkt_uartcomm_sendshortcmd_t;



typedef struct {
  uint8  event;
  uint8  len;
  uint16 code;
} msgpkt_uartcomm_cmdrecved_t;

//system event flag，can not be used
#define EVENT_ALL_DISABLE			0xff
#define EVENT_OK_DISABLE			0xf0
#define EVENT_ER_DISABLE			0x0f

//bit-index convert to bit
#define BIT(a)					( (uint16)1<<a )
	
// 2 bit-index merge to one special BCD byte, eg: if bit1(a),bit10(b) to be merged, the merged result is 0x1A
#define EVENT_MERGE(a,b)		( (a<<4)|b )

//according to one special Event BCD byte, set event ok flag
#define EVENT_GET_OK(a)			( ((a&EVENT_OK_DISABLE) == EVENT_OK_DISABLE)?0:BIT((a>>4)&0x0f) )
//according to one special Event BCD byte, set event er flag
#define EVENT_GET_ER(a)			( ((a&EVENT_ER_DISABLE) == EVENT_ER_DISABLE)?0:BIT(a&0x0f) )


/***************************************************************************************************
 * SYSTEM MESSAGE DEFINE ZONE - LZH RANGE[100-149]
 **************************************************************************************************/
/* 
 * @菜单对象开关消息类型
 */
#define SYS_MSG_VIDEO_OBJ_ON	100
#define SYS_MSG_VIDEO_OBJ_OFF	101
/* 
 * @菜单对象开关消息类型
 */
#define SYS_MSG_MENU_OBJ_ON		102
#define SYS_MSG_MENU_OBJ_OFF	103
/* 
 * @OSD显示消息类型
 */
#define SYS_MSG_OSD_DISPLAY			104

/* 
 * @协议栈转发非GO/IO命令消息类型
 */
#define SYS_MSG_STACK_CMD_BOARDCAST		105
#define SYS_MSG_STACK_CMD_APOINT		106
//协议栈转发GO命令消息类型
#define SYS_MSG_STACK_CMD_GO			107
//协议栈转发IO命令消息类型
#define SYS_MSG_STACK_CMD_IO			108

#define STACK_CMD_RECEIVE_NORMAL		1

/***************************************************************************************************
 * SYSTEM MESSAGE DEFINE ZONE - ZFZ RANGE[150-199]
 **************************************************************************************************/



/***************************************************************************************************
 * SYSTEM MESSAGE DEFINE ZONE - ZXJ RANGE[200-249]
 **************************************************************************************************/

#endif
