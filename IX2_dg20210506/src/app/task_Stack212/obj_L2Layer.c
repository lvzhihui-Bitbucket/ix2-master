
/**
  ******************************************************************************
  * @file    l2_layer.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of l2_layer.c
  ******************************************************************************
  *	@Э����·�㴦������
  *	@һ������֡�Ŀɿ����ͻ��ǽ���
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "stack212.h"
#include "vdp_uart.h"
#include "obj_L2Layer.h"

#define MICRO_L2_NORMAL_TRS_OVER	{		\
			l2TrsFrameBuf.frameCallback();	\
			l2Trs = L2_TRS_NONE;			\
			l2Status = L2_STAT_READ; }

// lzh_20140508_s
//unsigned char cmdType = 0;		//�������ͣ�0/��ָ�1/��ָ��
unsigned char cmdType = 0;		//�������ͣ�0/��ָ�1/��ָ��, 2/�³�ָ��
// lzh_20140508_e

L2_ADDR_TYPE myAddr=0x80;
int AREA_CODE=0;

volatile L2_RCV_FLAG l2ComRcvFlag;			//��·�����ͨ�ű�־���壬��·�㸺��SET��Ӧ�ò㸺��RESET
volatile L2_TRS_FLAG l2ComTrsFlag;			//��·�㷢��ͨ�ű�־���壬��·�㸺��SET��Ӧ�ò㸺��RESET

COM_RCV_FLAG phTmpRcvCom;
COM_TRS_FLAG phTmpTrsCom;

MAC_TYPE	l2MacType;
L2_STAT 		l2Status;
L2_ERR		l2Error;
L2_FRAME	l2TrsFrameBuf;
L2_FRAME	l2RcvFrameBuf;

L2_RETRS	l2ReTrs;
L2_RCV_STAT	l2Rcv;
L2_TRS_STAT	l2Trs;

OS_U32 macTiming;		//ý����ƶ�ʱ

unsigned char l2BusMonEnable = 0;	// lzh_20231116

OS_TIMER TimerTrs;

//lzh_20130416
unsigned char F_L2_SEND_NEW_CMD = 0;

//MAC���Ƶ����� lzh_20130415
const unsigned short MAC_ACCESS_TYPE_TAB[] =
{
	80,			//MAX_BUS_IDLE_TIME 		- ���������еȴ�ʱ�䣨��λ��25ms��,֮�󶼿��Է�������			//Min - 0
	4,			//MIN_INTERVAL_SEND_ACK 	- ����ACK��Ҫ�ȴ����߿��е���Сʱ�䣨��λ��ms��					//Min - 1 (�յ����ݰ������ACKӦ��)
	20,			//MAX_INTERVAL_WAIT_ACK		- ����APPLY��������ȴ�ACKӦ������ʱ��������λ��ms��		//Min - 2 (�������ݰ���ȴ�ACK��ʱ��)
	30,			//MAX_INTERVAL_BETWEEN_NOR	- ͨ������֡�������͵�ʱ��������λ��ms��						//Min - 3 (�ط��������ݰ�)
	40,			//MAX_INTERVAL_BETWEEN_BRD	- �㲥����֡�������͵�ʱ��������λ��ms��						//Min - 4 (�ط��㲥���ݰ�)
	50,			//MAX_INTERVAL_SEND_BLK		- ����BLOCK��ʱ��												//Min - 5 (����block)
	60,			//MIN_INTERVAL_SEND_RLY		- ����REPLY����֡��Ҫ�ȴ����߿��е���Сʱ�䣨��λ��ms��			//Min - 6 (��ָ��Ļظ����ݰ�)
	70,			//MIN_INTERVAL_SEND_ALY		- ����APPLY����������Ҫ�ȴ����߿��е���Сʱ�䣨��λ��ms��		//Min - 7 (��ָ����������ݰ�)
	//����ָ��mac����
	16,			//MIN_REPLAY_SUBMODEL		- ��Ծ�����DMR11��DT591�Ļظ�SubReceipt��ʱ��					//Min - 8 (��ָ���SubModel)
	//lzh_20140508_s
	20,
	//lzh_20140508_e		
};

/*******************************************************************************************
 * @fn:		L2_Initial
 *
 * @brief:	��·���ʼ��
 *			1.��·��ͨ�ű�־��λ
 *			2.��·��״̬����λ
 *			3.��·�����״̬����λ
 *			4.��·�㷢��״̬����λ
 * 			5.�ط�����λ
 *			6.MAC��λ
 *			7.�رռ��ģʽ
 *
 * @param:  LocalAddr - ����������ַ
 * @param:  L2_GRP_TYPE - ��������ַ
 * @param:  rcvCallback - �������ݻص�����
 * @param:  trsCallback - �������ݻص�����
 *
 * @return: none
 *******************************************************************************************/
void L2_Initial(L2_ADDR_TYPE LocalAddr,L2_GRP_TYPE GroupAddr,ptrFuncion rcvCallback,ptrFuncion trsCallback)
{
	//��·��ͨ�ű�־��λ
	l2ComRcvFlag = R_RCV_NONE;
	l2ComTrsFlag = T_TRS_NONE;
	//��·��״̬����λ
	l2Status = L2_STAT_READ;	
	//��·�����״̬����λ
	l2Rcv = L2_RCV_NONE;
	//��·�㷢��״̬����λ
	l2Trs = L2_TRS_NONE;
	//�ط�����λ
	L2ReTrsInit(0,MAX_BUS_IDLE_TIME);
	//MAC��λ
	l2MacType = MAX_BUS_IDLE_TIME;
	//��ʼ��������ַ�����ַ

	//myAddr = LocalAddr;
	
	OS_CreateTimer(&TimerTrs, TImerTrsStatusCallBack, 800/25);	//czn_20170809
	
    //��ʼ���ص�����
    l2TrsFrameBuf.frameCallback = trsCallback;
    l2RcvFrameBuf.frameCallback = rcvCallback;
}

/*******************************************************************************************
 * @fn��	TImerTrsStatusCallBack
 *
 * @brief:	����״̬�ĳ�ʱ��ʱ�������δ���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void TImerTrsStatusCallBack(void)
{
	OS_StopTimer(&TimerTrs);
	L2_LayerMonitorService();  	
}



/*******************************************************************************************
 * @fn:		MACTimingReset
 *
 * @brief:	MAC��ʱ��λ
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void MACTimingReset(void)
{
	//macTiming  = OS_GetTime32();
}

/*******************************************************************************************
 * @fn:		MACProcess
 *
 * @brief:	ý����ʿ���
 *
 * @param:  type - ý���������
 *
 * @return: none
 *******************************************************************************************/
void MACProcess(MAC_TYPE type)
{
  	OS_U32 macPeriod;

	l2MacType = type;

#if 0	
	macPeriod = OS_GetTime32() - macTiming;
	
	if( macPeriod < MAC_ACCESS_TYPE_TAB[type] )
	{
		OS_Delay( MAC_ACCESS_TYPE_TAB[type] - macPeriod );
	}
#endif

}

/*******************************************************************************************
 * @fn:		L2ReTrsInit
 *
 * @brief:	�����ط������ʼ��
 *
 * @param:  en - �ط�����
 * @param:  type - ý���������
 *
 * @return: 0 �������ʣ�1 ��������
 *******************************************************************************************/
void L2ReTrsInit(unsigned char en,MAC_TYPE type)
{
	l2ReTrs.enable = en;
	l2ReTrs.interval = MAC_ACCESS_TYPE_TAB[type];
	l2ReTrs.times = 1;
}

/*******************************************************************************************
 * @fn:		IsJust2ReTranslate
 *
 * @brief:	�ж��Ƿ���Ҫ�ط�
 *
 * @param:  none
 *
 * @return: 0 ���ط����ʣ�1 �����ط�
 *******************************************************************************************/
unsigned char IsJust2ReTranslate(void)
{
	if( l2ReTrs.enable )
	{
		if( l2ReTrs.times )
		{
			l2ReTrs.times--;
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_LayerRead
 *
 * @brief:	��·�����ݶ�״̬����
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2_LayerRead(void)
{
	//ɨ�跢�ͽ��
	Ph_PollingWriteResult(&phTmpTrsCom);  

	//printf("L2_LayerRead,l2Rcv=%d---\n",l2Rcv);
	
	switch( l2Rcv )
	{
		case L2_RCV_NONE:
			
			l2RcvFrameBuf.pack_len = Ph_PollingReadResult((unsigned char*)&l2RcvFrameBuf.sa,&phTmpRcvCom);

			printf("l2RcvFrameBuf.pack_len=%d,phTmpRcvCom=%d---\n",l2RcvFrameBuf.pack_len,phTmpRcvCom);
			
			//�õ���Ч����
			if( phTmpRcvCom == COM_RCV_DAT )
			{
				if( l2RcvFrameBuf.sa == L_Ctrl_Block )
				{
					//lzh_20130413					
					//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
					L2_RCV_FLAG_ENABLE(R_BLK_IND,L2_ERR_NONE)
					l2Rcv = L2_RCV_OVER;
					goto l2_rcv_over_process;						
				}
				else
				{
					//��ַƥ��: ����������ַ����ҪӦ��
					#if 0
					if( L2_IsNeedAck() && (!l2BusMonEnable) )	//lzh_20130403
					{
						//����ACKǰ��ʱ4ms lzh_20130425
						//OS_Delay( MAC_ACCESS_TYPE_TAB[MIN_INTERVAL_SEND_ACK] );
						//����ACKӦ��
						Ph_AckWrite();
						l2Rcv = L2_RCV_TRS_ACK_WAIT;
					}
					else
					#endif
					{
						//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
						L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)
						l2Rcv = L2_RCV_OVER;
						goto l2_rcv_over_process;
					}
				}
			}
			else if( phTmpRcvCom == COM_RCV_ERR )
			{
				//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
				L2_RCV_FLAG_ENABLE(R_ERR,L2_PH_BUS_ERROR)
				l2Rcv = L2_RCV_OVER;
				
				goto l2_rcv_over_process;					
			}		  
		  	break;
		case L2_RCV_TRS_ACK_WAIT:
			//�ȴ��Ƿ������
			// lzh_20161207_s			
			//if( phTmpTrsCom == COM_TRS_ACK )
			if( phTmpTrsCom == COM_TRS_DAT )
			// lzh_20161207_e
			{
				//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
				L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)
				l2Rcv = L2_RCV_OVER;	
				goto l2_rcv_over_process;
			}
			else
			{
				//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
				L2_RCV_FLAG_ENABLE(R_ERR,L2_ERR_NONE)
				l2Rcv = L2_RCV_OVER;	
				goto l2_rcv_over_process;			  	
			}
			break;
		case L2_RCV_OVER:
			l2_rcv_over_process:
			//������ϻص���������Ӧ�ýӿڲ��ȡ����
			l2RcvFrameBuf.frameCallback();				
			//Ӧ�ò�ȡ���󷵻ص�����״̬
			l2Rcv = L2_RCV_NONE;	  
			break;
	}

}

/*******************************************************************************************
 * @fn:		L2_LayerWrite
 *
 * @brief:	��·������д״̬����
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/			
void L2_LayerWrite(void)
{
	//����ͬʱ��Ҫ��ɨ���յ�����
	l2RcvFrameBuf.pack_len = Ph_PollingReadResult((unsigned char*)&l2RcvFrameBuf.sa,&phTmpRcvCom);	
	if( phTmpRcvCom == COM_RCV_DAT )
	{
		L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)	//czn_20170809
		l2RcvFrameBuf.frameCallback();	
		//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
		L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
		l2Trs = L2_TRS_DAT_OVER;	
		//goto L2_Normal_Trs_Over;
		MICRO_L2_NORMAL_TRS_OVER
	}
	//printf("l2Trs===============%d\n");
 	switch( l2Trs )
	{
		//1.�ȴ��鷢�����	  
		case L2_TRS_BLK_WAIT:
			//ɨ�跢�ͽ��
			Ph_PollingWriteResult(&phTmpTrsCom);
			//�ȴ��Ƿ������
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				L2_TRS_FLAG_ENABLE(T_BLK_REQ,L2_ERR_NONE)
				l2Trs = L2_TRS_BLK_OVER;
				goto L2_Block_Trs_Over;
			}
			else if( phTmpTrsCom == COM_TRS_ERR )
			{
				//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_BLK_REQ,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BLK_OVER;					
				goto L2_Block_Trs_Over;
			}
			break;
		case L2_TRS_BLK_OVER:
			L2_Block_Trs_Over:
			//lzh_20130413
			//������ϻص���������Ӧ�ýӿڲ��ȡ���ݷ��ͽ��
			l2TrsFrameBuf.frameCallback();																		  
			//�Ǽ�ͨ�Ž�������뷢�ͽ���״̬
			l2Trs = L2_TRS_NONE;
			l2Status = L2_STAT_READ;				  	
		  	break;

		//2.�ȴ��㲥�������		
		case L2_TRS_BRD_WAIT:
			//ɨ���һ�η��ͽ��
			Ph_PollingWriteResult(&phTmpTrsCom);
			//�ȴ��Ƿ������
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				//��ѯ�Ƿ���Ҫ���͵ڶ���
				if( l2ReTrs.enable && (!l2BusMonEnable) )	//lzh_20130403
				{				
					//�ȴ�40ms���͵ڶ���
					//OS_Delay(l2ReTrs.interval);
					Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 0 );					
					l2Trs = L2_TRS_BRD_REP;
				}
				else
				{
					//�Ǽ�ͨ�Ž��(�ѷ�����������)�������뷢�ͽ���״̬
					L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)
					l2Trs = L2_TRS_BRD_OVER;
					goto L2_Boardcast_Trs_Over;
				}
			}
			else// if( phTmpTrsCom == COM_TRS_ERR )
			{
				//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BRD_OVER;				
				goto L2_Boardcast_Trs_Over;
			}		  
		  	break;
		case L2_TRS_BRD_REP:
			//ɨ�跢�ͽ��
			Ph_PollingWriteResult(&phTmpTrsCom);
			//�ȴ��Ƿ������
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				//�Ǽ�ͨ�Ž��(�ѷ�����������)�������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)					
				l2Trs = L2_TRS_BRD_OVER;
				goto L2_Boardcast_Trs_Over;
			}
			else// if( phTmpTrsCom == COM_TRS_ERR )
			{
				//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BRD_OVER;					
				goto L2_Boardcast_Trs_Over;
			}				  
			break;
		case L2_TRS_BRD_OVER:
			L2_Boardcast_Trs_Over:
			//������ϻص���������Ӧ�ýӿڲ��ȡ���ݷ��ͽ��
			l2TrsFrameBuf.frameCallback();																		  
			//�Ǽ�ͨ�Ž�������뷢�ͽ���״̬
			l2Trs = L2_TRS_NONE;					
			l2Status = L2_STAT_READ;				  	
		  	break;
			
		//3.�ȴ����ݷ������
		case L2_TRS_DAT_WAIT:
			//ɨ�跢�ͽ��
			Ph_PollingWriteResult(&phTmpTrsCom);
			//�ȴ��Ƿ������
			//printf("phTmpTrsCom = %d ,l2ReTrs.enable =%d,l2BusMonEnable = %d\n",phTmpTrsCom,l2ReTrs.enable,l2BusMonEnable);
			// lzh_20181116_s
			if(  phTmpTrsCom == COM_TRS_DAT_HAVE_ACK )
			{
				L2_TRS_FLAG_ENABLE(T_DAT_CON,L2_ERR_NONE)
				l2Trs = L2_TRS_DAT_OVER;
				MICRO_L2_NORMAL_TRS_OVER				
			}
			else if( phTmpTrsCom == COM_TRS_DAT )
			//if( phTmpTrsCom == COM_TRS_DAT )
			// lzh_20181116_e
			{
				//��ѯ�Ƿ���ҪACKӦ���ź�
				if( l2ReTrs.enable && (!l2BusMonEnable) )	//lzh_20130403
				{
					//ResetRcv();
					l2Trs = L2_TRS_RCV_ACK;	//������ɺ����ȴ�ACK�ź�״̬	
					//������ʱ����10ms�󴥷�
					// lzh_20161207_s
					OS_SetTimerPeriod(&TimerTrs,MAC_ACCESS_TYPE_TAB[MAX_INTERVAL_WAIT_ACK]/25+3);		//czn_20170816
					OS_RetriggerTimer(&TimerTrs);
					//printf("OS_RetriggerTimer(&TimerTrs);\n");
					// lzh_20161207_e
				}
				else
				{
					//�Ǽ�ͨ�Ž��(�ѷ�����������)�������뷢�ͽ���״̬
					L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)
					l2Trs = L2_TRS_DAT_OVER;
					//goto L2_Normal_Trs_Over;
					MICRO_L2_NORMAL_TRS_OVER					
				}
			}
			else //if( phTmpTrsCom == COM_TRS_ERR )
			{
				//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_DAT_OVER;	
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER				
			}		  
			break;
		case L2_TRS_RCV_ACK:
			//�ȴ�Ŀ�귽��ACK�ź�
			if( phTmpRcvCom == COM_RCV_ACK )
			{					
				l2ReTrs.enable = 0;
				//�Ǽ�ͨ�Ž�����յ�Ӧ���źţ��������뷢�ͽ���״̬
				L2_TRS_FLAG_ENABLE(T_DAT_CON,L2_ERR_NONE)
				l2Trs = L2_TRS_DAT_OVER;
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER				
			}
			else
			{				
				//�Ƿ���Ҫ�ط�
				if( IsJust2ReTranslate() )
				{	
					//printf("Ph_DataWrite;\n");
					Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 1 );
					//�ٴν��뷢������
					l2Trs = L2_TRS_DAT_WAIT;
					l2Error = L2_ERR_NONE;
				}
				else if( !l2ReTrs.times )
				{
					//�Ǽ�ͨ�Ž��������ʧ�ܣ��������뷢�ͽ���״̬
					L2_TRS_FLAG_ENABLE(T_ERR,L2_ACK_TIMEROUT)
					l2Trs = L2_TRS_DAT_OVER;
					//goto L2_Normal_Trs_Over;
					MICRO_L2_NORMAL_TRS_OVER					
				}
			}			  
			break;			
		case L2_TRS_DAT_OVER:
			//L2_Normal_Trs_Over:
			MICRO_L2_NORMAL_TRS_OVER
			break;
			
		//lzh_20130328: ��������ACK����
		case L2_TRS_ACK_WAIT:
			//�ȴ��Ƿ������
			if( phTmpTrsCom == COM_TRS_ACK )
			{
				//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
				L2_TRS_FLAG_ENABLE(T_ACK_TST,L2_ERR_NONE)
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER					
			}
			else
			{
				//��ͨ�ű�־������ȴ�Ӧ�ò�ȡ��״̬
				L2_TRS_FLAG_ENABLE(T_ERR,L2_ERR_NONE)
				//goto L2_Normal_Trs_Over;			  	
				MICRO_L2_NORMAL_TRS_OVER					
			}
			break;
	}
}

/*******************************************************************************************
 * @fn:		L2_LayerMonitorService
 *
 * @brief:	��·���ط������ά��״̬����������Ľ��ջ��Ƿ��ͽ������������ã�
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2_LayerMonitorService(void)
{	
	switch( l2Status )
	{
		//������״̬
		case L2_STAT_READ:
		  	L2_LayerRead();
			break;
			
		//д����״̬
		case L2_STAT_WRITE:
		  	L2_LayerWrite();
			break;

		defalut:
			printf("!!!!!!!!!!!!!!!!!!!dt bus test l2Status = %d\n",(int)l2Status);			
			l2Status = L2_STAT_READ;
			break;
	}	
}

/*******************************************************************************************
 * @fn:		GetChecksum
 *
 * @brief:	����һ�����ݰ���checksum
 *			
 * @param:  ptrL2Frame - ֡�������ݰ�ָ��
 * @param:  save_cmp - 1 ����CKS��0 �õ�CKS�ȽϵĽ��
 *
 * @return: none
 *******************************************************************************************/
unsigned char GetChecksum(L2_FRAME *ptrL2Frame,unsigned char save_cmp)
{
	unsigned char i,checksum;
	//�����¾�ָ���
    	// lzh_20140508_s
	if( (ptrL2Frame->length.bitLDatCtrl.LFlag0000 == 0) && (ptrL2Frame->length.bitLDatCtrl.LFlag10 == 2) )
	{
	  	checksum = ptrL2Frame->sa ^ ptrL2Frame->da ^ ptrL2Frame->cmd ^ ptrL2Frame->length.byte ^ ptrL2Frame->ext_dat.long_type.sada ^ ptrL2Frame->ext_dat.long_type.long_len.llength;
		for( i = 0; i < ptrL2Frame->ext_dat.long_type.long_len.long_len_bit.llen; i++ )
		{
			checksum ^= ptrL2Frame->ext_dat.long_type.dat[i];
		}
		checksum ^= 0xff;		
	}
	else if( ptrL2Frame->length.bit.newcmd )
	{
	//if( ptrL2Frame->length.bit.newcmd )		
	//{
	// lzh_20140508_e	
	  	checksum = ptrL2Frame->sa ^ ptrL2Frame->da ^ ptrL2Frame->cmd ^ ptrL2Frame->length.byte;
		for( i = 0; i < ptrL2Frame->length.bit.len; i++ )
		{
			checksum ^= ptrL2Frame->ext_dat.old_dat[i];
		}
		checksum ^= 0xff;
	}
	else
	{
	  	checksum = ptrL2Frame->sa + ptrL2Frame->da + ptrL2Frame->cmd;
		if( ptrL2Frame->length.byte >= 5 )
		{
		  	checksum += ptrL2Frame->length.byte;
			for( i = 0; i < ptrL2Frame->length.byte-5; i++ )
			{
				checksum += ptrL2Frame->ext_dat.old_dat[i];
			}
		}
	}
	//czn_20180831_s
	if( (ptrL2Frame->cmd == MAIN_CALL) || (ptrL2Frame->cmd == MAIN_CALL_GROUP) || (ptrL2Frame->cmd == MAIN_CALL_ONCE_L) )
	{
		checksum += AREA_CODE; 
	}
	//czn_20180831_e
	if( save_cmp )
	{
		ptrL2Frame->cks = checksum;
		return 1;
	}
	else
	{
		return( (ptrL2Frame->cks == checksum)?1:0 );
	}
}

/*******************************************************************************************
 * @fn:		UpdateL2DataWrite
 *
 * @brief:	����L2�ķ������ݻ�����
 *			
 * @param:  ptrDat  - ת�����ݵ�ַָ�룬��Ҫת��ΪAI_DATA���ݸ�ʽ
 *
 * @return: none
 *******************************************************************************************/
void UpdateL2DataWrite(unsigned char *ptrDat)
{
	unsigned char i;
	AI_DATA *ptrAI;

    //lzh_20140508_s
	AI_LONG_DATA *ptrAILong;    
	ptrAILong = (AI_LONG_DATA *)ptrDat;
    //lzh_20140508_e

	ptrAI = (AI_DATA *)ptrDat;
    
	//��ʼ����������
	l2TrsFrameBuf.sa				    	= ptrAI->head.sa;
	l2TrsFrameBuf.da				    	= ptrAI->head.da;
	l2TrsFrameBuf.cmd				    	= ptrAI->head.cmd;
	
    //�����¾�ָ������
    //lzh_20140508_s
    if( (ptrAILong->head.length.bitLDatCtrl.LFlag0000 == 0) && (ptrAILong->head.length.bitLDatCtrl.LFlag10 == 2) )
    {
        l2TrsFrameBuf.sa				    				= ptrAILong->head.sa;
        l2TrsFrameBuf.da				    				= ptrAILong->head.da;
        l2TrsFrameBuf.cmd				    				= ptrAILong->head.cmd;
        l2TrsFrameBuf.ext_dat.long_type.sada				= ptrAILong->head.sada;

        l2TrsFrameBuf.ext_dat.long_type.long_len.llength	= ptrAILong->ldata.long_len.long_len_bit.llen;
        l2TrsFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf	= ptrAILong->ldata.long_len.long_len_bit.ldaf;

        //�����ֶ�ֱ��copy
        l2TrsFrameBuf.length.byte							= ptrAILong->head.length.byte;

        //��������
        for( i = 0; i < ptrAILong->ldata.long_len.long_len_bit.llen; i++ )
        {
            l2TrsFrameBuf.ext_dat.long_type.dat[i] = ptrAILong->ldata.dat[i];
        }

        //���ܳ�����Ҫ��7����ָ�����ݰ�ͷ
        l2TrsFrameBuf.pack_len = ptrAILong->ldata.long_len.long_len_bit.llen + 7;

        //��������������
        L2_SetCmdType(2);
    }
    else if( ptrAI->head.length.bit.newcmd )
    //if( ptrAI->head.length.bit.newcmd )
    //lzh_20140508_e
    {
		l2TrsFrameBuf.ext_dat.new_type.sada	= ptrAI->head.sada;
		l2TrsFrameBuf.length.byte			= ptrAI->head.length.byte;
		
		//�״η���
		l2TrsFrameBuf.length.bit.rep = 1;
		
		//���ȼ�����չ��ַһ���ֽ�		
		l2TrsFrameBuf.length.bit.len += 1;
		
		//��������
		for( i = 0; i < ptrAI->head.length.bit.len; i++ )
		{		
			l2TrsFrameBuf.ext_dat.new_type.dat[i] = ptrAI->data[i];
		}
		
		//���ܳ�����Ҫ��6�������ֽ�
		l2TrsFrameBuf.pack_len = ptrAI->head.length.bit.len + 6;				
		
		//��������������
		L2_SetCmdType(1);
    }
    else
    {
		//����չ����
		l2TrsFrameBuf.length.byte = 0;
		
		//����չ������Ҫ���ϻ������ݺͳ��ȱ�����5���ֽ�
		if( ptrAI->head.length.byte ) 
		{
			l2TrsFrameBuf.length.byte	= ptrAI->head.length.byte + 5;
			l2TrsFrameBuf.pack_len 	= l2TrsFrameBuf.length.byte;
		}
		else
			l2TrsFrameBuf.pack_len 	= 4;
			
		//�������ݲ��õ�checksum
		for( i = 0; i < ptrAI->head.length.byte; i++ )
		{		
			l2TrsFrameBuf.ext_dat.old_dat[i] = ptrAI->data[i];
		}
				
		//�������������
		L2_SetCmdType(0);
    }
	//����checksum
	GetChecksum(&l2TrsFrameBuf,1); 	
}
static int l2LastSendTime=0;


/*******************************************************************************************
 * @fn:		L2_DataWrite
 *
 * @brief:	���뷢��һ�������ݰ�
 *			
 * @param:  macType	- MAC��ʱ����
 * @param:	uNew 	- ������ָ��
 *
 * @return: none
 *******************************************************************************************/
unsigned char L2_DataWrite(	MAC_TYPE macType, unsigned char uNew )
{
	//���ڷ��ͻ��Ǽ��ģʽ�²���������
	if( l2Status != L2_STAT_READ )
	{
		 if((time(NULL)-l2LastSendTime)<10)
		 {
			l2Error = L2_WRITE_WAIT;	
			return 0;
		 }
		MICRO_L2_NORMAL_TRS_OVER
	}
	
	Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, uNew );
	l2LastSendTime=time(NULL);
	F_L2_SEND_NEW_CMD = uNew;
		
	//�����ط���־
	if( l2TrsFrameBuf.length.bit.newcmd )
	{
		l2TrsFrameBuf.length.bit.rep = 0;
		GetChecksum(&l2TrsFrameBuf,1);
	}
	//���뷢������
	l2Status = L2_STAT_WRITE;
	l2Trs = L2_TRS_DAT_WAIT;
	l2Error = L2_ERR_NONE;
		//��ʼ���ط�����
        //lzh_s_20131023        ��ָ������ط�
        if( uNew )
			// lzh_20170109_s
			//L2ReTrsInit(l2TrsFrameBuf.length.bit.ack_req,MAX_INTERVAL_BETWEEN_NOR);
			L2ReTrsInit(0,MAX_INTERVAL_BETWEEN_NOR);
			// lzh_20170109_e
        else
		    L2ReTrsInit(0,MAX_INTERVAL_BETWEEN_NOR);
        //lzh_e_20131023
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_BoardcastWrite
 *
 * @brief:	���뷢��һ�㲥���ݰ�
 *			
 * @param:  none
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
unsigned char L2_BoardcastWrite( void )
{
	//���ڷ��ͻ��Ǽ��ģʽ�²���������
	if( l2Status != L2_STAT_READ )
	{
		 if((time(NULL)-l2LastSendTime)<10)
		 {
			l2Error = L2_WRITE_WAIT;	
			return 0;
		 }
		 MICRO_L2_NORMAL_TRS_OVER
	}
	
	Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 0 );
	l2LastSendTime=time(NULL);
	F_L2_SEND_NEW_CMD = 0;
		
	//�����ط���־
	if( l2TrsFrameBuf.length.bit.newcmd )
	{
		l2TrsFrameBuf.length.bit.rep = 0;
		GetChecksum(&l2TrsFrameBuf,1);
	}
	//���뷢������
	l2Status = L2_STAT_WRITE;
	l2Trs = L2_TRS_BRD_WAIT;
	l2Error = L2_ERR_NONE;
	//��ʼ���ط�����,�㲥ָ����Ҫ�ط�
	L2ReTrsInit(1,MAX_INTERVAL_BETWEEN_BRD);
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_PollingWriteResult
 *
 * @brief:	��ѯ��ǰ��·�㣬�õ��������ݽ��ͨ�ű�־������������ŵ�pdat�ĵ�ַ��
 *			����������ķ��ͽ������ΪLLC�Ŀ����߼�
 *
 * @param:  flag - ����״ָ̬��
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
unsigned char L2_PollingWriteResult(L2_TRS_FLAG *flag)
{
	*flag = l2ComTrsFlag;
	//��ȡ���ݺ������־
	l2ComTrsFlag = T_TRS_NONE;
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_DataRead
 *
 * @brief:	��ѯ��·�㣬�õ�����ͨ�ű�־������������ŵ�pdat�ĵ�ַ��
 *
 * @param:  pdat - ������ݵ�ָ�룻/
 * @param:  flag  - ����ͨ�ű�־
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
unsigned char L2_DataRead(unsigned char *pdat,L2_RCV_FLAG *flag)
{
	unsigned char i,len=0;
	AI_DATA *ptrAI;
    // lzh_20140508_s
	AI_LONG_DATA *ptrAI_LDat;
	// lzh_20140508_e
	
	ptrAI = (AI_DATA *)pdat;
    // lzh_20140508_s
	ptrAI_LDat = (AI_LONG_DATA *)pdat;
	// lzh_20140508_e	
	
	if( l2ComRcvFlag == R_DAT_IND )
	{
		printf("==========L2_DataRead, len = %d, l2RcvFrameBuf.sa[%02x],l2RcvFrameBuf.da[%02x],myAddr[%02x]==========\n",l2RcvFrameBuf.pack_len, l2RcvFrameBuf.sa, l2RcvFrameBuf.da, myAddr);
		//�ж���ָ��Ǿ�ָ��
		if( l2RcvFrameBuf.pack_len == 4 )
		{
			l2RcvFrameBuf.length.byte = 0;
			L2_SetCmdType(0);
		}
        // lzh_20140508_s
		else if( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) )
		{
			L2_SetCmdType(2);
		}
		// lzh_20140508_e			
		else if( !l2RcvFrameBuf.length.bit.newcmd )
		{
			L2_SetCmdType(0);
		}
		else
			L2_SetCmdType(1);

		
		//��ַƥ��: ���״̬���������ַ������������ַ���㲥��ַ���ϴ�����
		if( l2BusMonEnable || 
		   ( l2RcvFrameBuf.length.bit.daf && l2RcvFrameBuf.length.bit.newcmd ) ||	//lzh_20121212
			 //lzh_20150729_s				 
			( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) && l2RcvFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf ) ||
			//lzh_20150729_e			   
		   ( l2RcvFrameBuf.da == myAddr ) || 
		   ( (l2RcvFrameBuf.da&0xfc) == (myAddr&0xfc) ) || 
			   //Դ��ַ�ͱ�����ַ��ͬҲ��Ҫ�ϴ�: ��ӷֻ�ժ���ȷ��͸�������ST-TALK������
		   ( (l2RcvFrameBuf.sa&0xfc) == (myAddr&0xfc) ) || 
		   ( l2RcvFrameBuf.da == BROADCAST_ADDR ) ||
		   ( (l2RcvFrameBuf.cmd >= BROADCAST_CMD_LOW) && (l2RcvFrameBuf.cmd <= BROADCAST_CMD_HIGH) ) ||		//�㲥ָ�������ҲҪ�ϴ�����			 
 		   ( ((l2RcvFrameBuf.da&0x40)==(myAddr&0x40))&&((l2RcvFrameBuf.cmd>=DR_SYS_CMD_START)&&(l2RcvFrameBuf.cmd<=DR_SYS_CMD_END)) ) //lzh_20121203 :������ϵͳ�������:Ŀ���ַΪ0x80��0xc0,����Ϊ��������������			   
		  )
		{
			//�Ƚ�checksum�Ƿ���ȷ
			if( GetChecksum(&l2RcvFrameBuf,0) )
			{
                // lzh_20140508_s
				if( cmdType == 2 )
				{
					ptrAI_LDat->head.sa 	= l2RcvFrameBuf.sa;
					ptrAI_LDat->head.da 	= l2RcvFrameBuf.da;
					ptrAI_LDat->head.sada 	= l2RcvFrameBuf.ext_dat.long_type.sada;
					ptrAI_LDat->head.cmd	= l2RcvFrameBuf.cmd;
					//�õ���ָ��Ŀ�����
					ptrAI_LDat->head.length.byte = l2RcvFrameBuf.length.byte;
					//�õ���ָ�����չ���ݰ�����
					ptrAI_LDat->ldata.long_len.llength = l2RcvFrameBuf.ext_dat.long_type.long_len.llength;
					
					for( i = 0; i < ptrAI_LDat->ldata.long_len.long_len_bit.llen; i++ )
					{
						ptrAI_LDat->ldata.dat[i] = l2RcvFrameBuf.ext_dat.long_type.dat[i];
					}
					//����ͷ7���ֽڵĳ�����ͷ+��չ���ݸ���
					len = 7 + ptrAI_LDat->ldata.long_len.long_len_bit.llen;
				}
				//��ָ��
				else if( cmdType == 1)
				//if( cmdType )
				// lzh_20140508_e
				{
					ptrAI->head.sa 		= l2RcvFrameBuf.sa;
					ptrAI->head.da 		= l2RcvFrameBuf.da;
					ptrAI->head.sada 	= l2RcvFrameBuf.ext_dat.new_type.sada;
					ptrAI->head.cmd		= l2RcvFrameBuf.cmd;
					
					//������չ���ݵĳ��ȣ���ָ����Ҫ��ȥһ����չ��ַ�ĳ���			
					if( l2RcvFrameBuf.length.bit.len )
						ptrAI->head.length.byte = l2RcvFrameBuf.length.byte - 1;
					else
						ptrAI->head.length.byte	= l2RcvFrameBuf.length.byte;
					
					for( i = 0; i < ptrAI->head.length.bit.len; i++ )
					{
						ptrAI->data[i] = l2RcvFrameBuf.ext_dat.new_type.dat[i];
					}
					//����ͷ6���ֽ�+��չ���ݸ���
					len = 5 + ptrAI->head.length.bit.len;										
				}
				else
				{
					ptrAI->head.sa 		= l2RcvFrameBuf.sa;
					ptrAI->head.da 		= l2RcvFrameBuf.da;
					ptrAI->head.sada 	= 0;
					ptrAI->head.cmd		= l2RcvFrameBuf.cmd;
					//������չ���ݵĳ��ȣ���ָ����Ҫ��ȥǰ5���̶��ֽ�
					//lzh_20121120
					if( l2RcvFrameBuf.length.byte >= 5 )
					{
						ptrAI->head.length.byte = l2RcvFrameBuf.length.byte - 4;	//������ַ��������
						ptrAI->data[0] = l2RcvFrameBuf.length.byte;
						for( i = 0; i < ptrAI->head.length.byte-1; i++ )
						{
							ptrAI->data[i+1] = l2RcvFrameBuf.ext_dat.old_dat[i];
						}
					}
					else
						ptrAI->head.length.byte = 0;
					//����ͷ4���ֽ�+��չ���ݸ���				
					len = 4 + ptrAI->head.length.byte;						
				}
			}
			else
			{
				printf("===============checksum err1===================\n");
				L2_RCV_FLAG_ENABLE(R_ERR,L2_LRC_ERROR)
			}
		}
		//�Ǳ�����ַ���ϴ�
		else
		{
			//�Ƚ�checksum�Ƿ���ȷ
			if( GetChecksum(&l2RcvFrameBuf,0) )
			{			
				printf("===============pack not useful===================\n");
				L2_RCV_FLAG_ENABLE(R_RCV_NONE,L2_ERR_NONE)
			}
			else
			{
				printf("===============checksum err2===================\n");
				L2_RCV_FLAG_ENABLE(R_ERR,L2_LRC_ERROR)
			}
		}
	}
	//lzh_20130413
	if( l2ComRcvFlag == R_BLK_IND )
	{
		len = 1;
	}	
	*flag = l2ComRcvFlag;
	//��ȡ���ݺ������־
	l2ComRcvFlag = R_RCV_NONE;
	
	//lzh_20130403
	if( l2BusMonEnable )
		return 0;
	else
		return len;
}

/*******************************************************************************************
 * @fn:		L2_BlkWrite
 *
 * @brief:	��д����
 *
 * @param:  none
 *
 * @return: 1 ���ͳɹ���0 ����ʧ��
 *******************************************************************************************/
unsigned char L2_BlkWrite(void)
{
	uint8 block_temp;	//lzh_20130323
	
	//���ڷ��ͻ��Ǽ��ģʽ�²���������
	if( l2Status != L2_STAT_READ )
	{
		l2Error = L2_WRITE_WAIT;
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_GetLastError
 *
 * @brief:	�õ���·�����״̬
 *
 * @param:  none
 *
 * @return: l2Error ����״̬
 *******************************************************************************************/
L2_ERR L2_GetLastError(void)
{
	return l2Error;
}

/*******************************************************************************************
 * @fn:		L2_GetMyAddress
 *
 * @brief:	���ص�ǰ�豸��������ַ
 *
 * @param:  none
 *
 * @return: myAddr ������ַ
 *******************************************************************************************/
L2_ADDR_TYPE L2_GetMyAddress(void)
{
	return myAddr;
}

/*******************************************************************************************
 * @fn:		L2_IsNewCmdType
 *
 * @brief:	�õ���ǰָ������
 *
 * @param:  none
 *
 * @return: cmdType ָ������
 *******************************************************************************************/
unsigned char L2_IsNewCmdType(void)
{
	return cmdType;
}

/*******************************************************************************************
 * @fn:		L2_SetCmdType
 *
 * @brief:	����ʱ���õ�ǰָ��ϵͳ
 *
 * @param:  cmdType - ָ������
 *
 * @return: none
 *******************************************************************************************/
void L2_SetCmdType(unsigned char type)
{
	cmdType = type;
}

//lzh_20130328
/*******************************************************************************************
 * @fn:		L2_AckWrite
 *
 * @brief:	Ackд����
 *
 * @param:  none
 *
 * @return: 1 ���ͳɹ���0 ����ʧ��
 *******************************************************************************************/
unsigned char L2_AckWrite(void)
{
	//���ڷ��ͻ��Ǽ��ģʽ�²���������
	if( l2Status != L2_STAT_READ )
	{		
		l2Error = L2_WRITE_WAIT;
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_IsNeedAck
 *
 * @brief:	�жϽ��������Ƿ���ҪӦ��
 *
 * @param:  none
 *
 * @return: 1 ��ҪӦ��0 ����ҪӦ��
 *******************************************************************************************/
unsigned char L2_IsNeedAck(void)
{
    // lzh_20140508_s
	if( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) )
	{		
		if( (!l2BusMonEnable) && (!l2RcvFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf) && (l2RcvFrameBuf.da == myAddr) && l2RcvFrameBuf.length.bit.ack_req )
		{
			//�Ƚ�checksum�Ƿ���ȷ
		  	return GetChecksum(&l2RcvFrameBuf,0);
		}
		else
			return 0;
	}
	else if( l2RcvFrameBuf.length.bit.newcmd )
	//if( l2RcvFrameBuf.length.bit.newcmd )
	// lzh_20140508_e
	{
		//��ַƥ��: ���״̬���������ַ������������ַ���㲥��ַ���ϴ�����
		if( (!l2BusMonEnable) && (!l2RcvFrameBuf.length.bit.daf) && (l2RcvFrameBuf.da == myAddr) && l2RcvFrameBuf.length.bit.ack_req )
		{
			//�Ƚ�checksum�Ƿ���ȷ
		  	return GetChecksum(&l2RcvFrameBuf,0);
		}
		else
			return 0;
	}
	else
		return 0;
}


