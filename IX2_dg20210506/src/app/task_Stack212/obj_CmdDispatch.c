
/**
  ******************************************************************************
  * @file    obj_CmdDispatch.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of obj_CmdDispatch.c
  ******************************************************************************
  */ 

#include "stack212.h"
#include "task_IoServer.h"
//#include "obj_BeCalled_data.h" // 20140819

extern MAC_TYPE	l2MacType;

//Ӧ�ò�ȴ�Ӧ������������
WAIT_RESPONSE WaitResponse;

//�յ�Э��ջָ�����Ҫ����sub_receiptӦ���ָ���
const unsigned char SEND_SUB_RECEIPT_CMD_TAB[] =		//will add
{
	MAIN_CALL,
	MAIN_CALL_ONCE_L,
	MAIN_CALL_TEST,
	LET_DIDONG_ONCE,
	LET_WORK,
	LET_TALKING,
};

//�յ�Э��ջָ�����Ҫ����receiptӦ���ָ���
const unsigned char SEND_RECEIPT_CMD_TAB[] =			//will add
{
	//INTERCOM_CALL,	//czn_20170601
};

//ת���������Ҫ�ȴ�receipt����sub_receiptӦ���ָ���
const unsigned char WAIT_RECEIPT_CMD_TAB[] =			//will add
{
	ST_TALK,			//receipt
	ST_UNLOCK,			//receipt
	ST_CLOSE,			//receipt
	ST_LINK_MR,			//receipt	
	MON_ON,				//receipt
	MON_OFF,			//receipt
	MON_ON_APPOINT,		//receipt
	MON_OFF_APPOINT,	//receipt
	ST_UNLOCK_SECOND,	//receipt
	
	INTERCOM_CALL,		//receipt
};

/*******************************************************************************************
 * @fn��	WhichReceiptShouldBeSend
 *
 * @brief:	�յ�Э��ջָ��󣬽����Ƿ���Ҫ����Ӧ��
 *
 * @param:  cmd - 	���������Ƿ���Ҫ����Receipt �� Receipt_Single Ӧ��
 *
 * @return: 0/����Ӧ��1/����receiptӦ��2/����sub_receiptӦ��
 *******************************************************************************************/
unsigned char WhichReceiptShouldBeGiven(unsigned char cmd)
{	
	unsigned char i;
	//�����������Ƿ���Ҫ����receiptӦ��
	for( i = 0; i < sizeof(SEND_RECEIPT_CMD_TAB); i++ )
	{
		if( SEND_RECEIPT_CMD_TAB[i] == cmd )
		{
			return 1;
		}
	}
	//�����������Ƿ���Ҫ����sub_receiptӦ��
	for( i = 0; i < sizeof(SEND_SUB_RECEIPT_CMD_TAB); i++ )
	{
		if( SEND_SUB_RECEIPT_CMD_TAB[i] == cmd )
		{
			return 2;
		}
	}
	return 0;
}

/*******************************************************************************************
 * @fn��	SetWaitResponse
 *
 * @brief:	ת��ָ�������Ӧ����ƣ�
 *			1��ptrTCB��	����Event��WaitResponse.ptrTCBResponse��ָ�������
 *			2��cmd��	���������Ƿ���Ҫ�ȴ�Receipt �� Receipt_Single Ӧ��
 *
 * @param:  ptrTCB - ��Ҫ�ظ�Event������ID
 * @param:  cmd - 	��������Ƿ���ҪReceipt �� Receipt_Single Ӧ��
 * @param:  brd - 	�ط�������Ϊ�㲥����Ƕ������0�����㣬1���㲥��
 *
 * @return: none
 *******************************************************************************************/
void SetWaitResponse(vdp_task_t *ptrTCB,unsigned char cmd,unsigned char brd)
{	
	//��Ҫ�ظ�Event ACK��task ID
	WaitResponse.ptrTCBResponse = ptrTCB;
	
	//�����Ҫ�ظ�receipt ACK������
	unsigned char i;
	for( i = 0; i < sizeof(WAIT_RECEIPT_CMD_TAB); i++ )
	{
		if( WAIT_RECEIPT_CMD_TAB[i] == cmd )
			break;		
	}
	
	//ƥ��ɹ����ʼ���ط�����
	if( i != sizeof(WAIT_RECEIPT_CMD_TAB) )
	{
		WaitResponse.reEnable = 1;
		WaitResponse.reSendCnt = 2;			//lzh_20130410
		WaitResponse.reBroadcast = brd;
	}
}

/*******************************************************************************************
 * @fn��	ResetWaitResponse
 *
 * @brief:	��λ�ط�����
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void ResetWaitResponse(void)
{	
	WaitResponse.reEnable = 0;
	WaitResponse.reSendCnt = 0;	
	WaitResponse.reBroadcast = 0;
}

/*******************************************************************************************
 * @fn��	TrsResultResponseCallback
 *
 * @brief:	��Է��ͽ��������ӦTCB�Ļظ�
 *
 * @param:  rspOK - 0/err, 1/OK
 *
 * @return: none
 *******************************************************************************************/
void TrsResultResponseCallback(unsigned char rspOK)
{	
	// 1. ���ͽ������Event ACK
	if( WaitResponse.ptrTCBResponse )
	{
		if( rspOK )
			OS_SignalEvent(OS_TASK_EVENT_RESPONSE_OK,WaitResponse.ptrTCBResponse);
		else
			OS_SignalEvent(OS_TASK_EVENT_RESPONSE_ER,WaitResponse.ptrTCBResponse);
		
		WaitResponse.ptrTCBResponse = 0;
	}
	
	// 2. �ȴ�Receipt or Receipt_Single��1�볬ʱ���ط�����
	if( WaitResponse.reEnable && WaitResponse.reSendCnt )
	{
		OS_TASK_EVENT MyEvents;
		
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_RCV_QUEUE,1000);	//czn_20170816++++
		
		// �յ����ݰ����������յ���ReceiptӦ���λ
		if( MyEvents&OS_TASK_EVENT_STACK_RCV_QUEUE )
		{
			RcvQueueProcess();
		}
		//�Ƿ��ط����ݰ�
		if( WaitResponse.reSendCnt-- )
		{
			// �����ط�����Ϊ�㲥���Ƕ���
			unsigned char result;
			if( WaitResponse.reBroadcast )
			{
				result = L2_BoardcastWrite();
			}
			else
			{
				//lzh_20130425
				result = L2_DataWrite(l2MacType,F_L2_SEND_NEW_CMD);
			}			
			// �ط�����Ľ�������ʹ���ֹͣ�ط�			
			if( !result )
			{
				ResetWaitResponse();
			}			
			else
			{
				aiStatus = AI_STAT_WRITE;
			}
		}
	}
}

/*******************************************************************************************
 * @fn:		StackOldCommandInputProcess
 *
 * @brief:	����Э��ջ�������ľ�ָ��
 *
 * @param:  ptrMsgDat - ��Ϣ����ָ��
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char ttetemp1;
unsigned char ttetemp2;
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "task_DXMonitor.h"
unsigned char StackOldCommandInputProcess(void *ptrMsgDat)
{	
	AI_DATA *ptrCMdIo = (AI_DATA *)ptrMsgDat;
	uint8 tranferset;
	char buff[5];
	switch( ptrCMdIo->head.cmd )
	{
		// ����������ָ�ֱ�Ӵ���
		case MR_LINKING_ST:
			printf("=====================old link add[%02x],myaddr[%02x]===================\n",ptrCMdIo->head.da,myAddr);
			if( myAddr == ptrCMdIo->head.da )
			{
				//zfz 20140819
				#if 1
				if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
				{
					API_Event_IoServer_InnerRead_All(CallScene_SET, buff);
					tranferset = atoi(buff);
				}
				if(tranferset== 1 || tranferset == 2)
				//if( !GetDoNotDisturbState() ) 	//���������Ź���
				{
					break;
				}
				#endif
				//if( !GetDoNotDisturbState() ) 	//���������Ź���
				{
					SendOneOldCommand(2,ptrCMdIo->head.sa,SUB_MODEL,0,0);
					API_Event_Maincall_Close();
				}
			}
			break;
			
		// �յ������Ļظ������ֹͣ�ط����ƣ����ҷ��͵�����
		case RECEIPT_SINGLE:
			//ds_mode =  DS_SINGLE;	//��������������
		case RECEIPT:		
			printf("=====================old RECEIPT add[%02x],myaddr[%02x]===================\n",ptrCMdIo->head.da,myAddr);

			if( myAddr == ptrCMdIo->head.da )
			{
				InstructionCheckResponse();	//czn_20170717
				//��λ�ط���������
				ResetWaitResponse();
				//API_Survey_MsgOldCommand_4byte(ptrCMdIo->head.sa,ptrCMdIo->head.cmd);
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da, ptrCMdIo->head.cmd,0,NULL);
			}
			break;
			
		// ����Namelist����
		case NAMELIST_ON:
			API_NamelistUpdate_Start();
			break;
			
		// Namelist���½���
		case NAMELIST_OFF:
			API_NamelistUpdate_End();
			break;
			
		// Namelist��������
		case NAMELIST_BROAD:

		
//lzh_20121203			if( ptrCMdIo->head.length.bit.len > 5 )
			if( (ptrCMdIo->head.length.bit.newcmd && ptrCMdIo->head.length.bit.len > 5) || (!ptrCMdIo->head.length.bit.newcmd && ptrCMdIo->head.length.byte > 5) )	//lzh_20121203
				
				
			{
				//externd data: [0]:Package Length; [1]:NameList Number; [2...]NameList Data
				API_NameListUpdate_List( ptrCMdIo->data[1]-0x30, ptrCMdIo->data[0]-5, ptrCMdIo->data+1 );
			}
			break;
			
		default:
			//MAIN_CALL ������ˣ�����Ŀ���ַ����չ��ַ����С�ڱ�������չ��ַ
			if( (ptrCMdIo->head.cmd == MAIN_CALL ) && ( (ptrCMdIo->head.da&0x03) > (myAddr&0x03)) )
				break;

			//INTERCOM_HJ_CALL ������ˣ�
			// 1. ���е�ַΪ0x3c,�ұ�����ַ��0x3c
			// 2. ����Ŀ���0x3c, ���е�ַ������ַ���벻ͬ�ڱ���������ַ
			if( (ptrCMdIo->head.cmd == INTERCOM_CALL ) )
			{
				if( ( ( ptrCMdIo->head.da==0x3c ) && (myAddr != 0x3c) ) 
				   	|| ( ( ptrCMdIo->head.da!=0x3c ) && (ptrCMdIo->head.sa&0xfc)==(myAddr&0xfc) )	//lzh_20121120
				  )
				{
					ttetemp1 = ptrCMdIo->head.sa;
					ttetemp2 = ptrCMdIo->head.da;
					break;
				}
                
                //zfz 20140819
//                if( GetDoNotDisturbState() || GetIntercomDisableFlag() ) 	//�������Ź���
//                {
//                    break;
//                }
			}

			//MAIN_CALL_ONCE_L ����ϵͳ������������ˣ�Ŀ���ַ�Ĳ���Ϊ0x80�Ҳ���Ϊ0x80
			if( (ptrCMdIo->head.cmd == MAIN_CALL_ONCE_L ) && ( (myAddr!=0x80)&&(myAddr!=0xc0)) )
			{
				break;
			}
			
			//MAIN_CALL_GROUP ����ϵͳȺ����������ˣ�Ŀ���ַ��Ϊ0x80����0xC0
			if( (ptrCMdIo->head.cmd == MAIN_CALL_GROUP ) && ( (myAddr==0x80)||(myAddr==0xc0)) )
				{
					break;
				}
				
			//S_CANCEL ������ˣ�����Ŀ���ַ���ڱ����ĵ�ַ
			if( (ptrCMdIo->head.cmd == S_CANCEL ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;			

			//ASK_VOLTAGE ������ˣ�����Ŀ���ַ���ڱ����ĵ�ַ
			if( (ptrCMdIo->head.cmd == ASK_VOLTAGE ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;

			//MAIN_CALL_TEST ������ˣ�����Ŀ���ַ���ڱ����ĵ�ַ
			if( (ptrCMdIo->head.cmd == MAIN_CALL_TEST ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;

			//��Ҫ������Ӧ��Ӧ��
			if( WhichReceiptShouldBeGiven(ptrCMdIo->head.cmd) == 1 )
			{
				if( myAddr == ptrCMdIo->head.da )
				{
					SendOneOldCommand(0,ptrCMdIo->head.sa,RECEIPT,0,0);
				}
			}
			else if( WhichReceiptShouldBeGiven(ptrCMdIo->head.cmd) == 2 )
			{
				if( myAddr == ptrCMdIo->head.da )
				{				
					SendOneOldCommand(0,ptrCMdIo->head.sa,SUB_RECEIPT,0,0);					
				}
			}
			//�����ϴ�
            // 20131021
			//API_Survey_MsgOldCommand_4byte(ptrCMdIo->head.sa,ptrCMdIo->head.cmd);
            if( ptrCMdIo->head.length.byte )	//czn_20170518
			{
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da,ptrCMdIo->head.cmd,ptrCMdIo->head.length.byte-1,ptrCMdIo->data+1);	//����Ŀ���ַ
			}				
			else
			{
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da, ptrCMdIo->head.cmd,0,NULL);	//����Ŀ���ַ
			}            
			break;
	}

	return TRUE;
}

/*******************************************************************************************
 * @fn:		StackOldCommandOutputProcess
 *
 * @brief:	����task��Ҫת���ľ�ָ��
 *
 * @param:  ptrMsgDat - ��Ϣ����ָ��
 * @param:  brd - 0:�������1:�㲥����
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char StackOldCommandOutputProcess(void *ptrMsgDat,unsigned char brd)
{
	StackFrameQ_TaskIO *ptrCmdOutputForSend;
	
	ptrCmdOutputForSend = (StackFrameQ_TaskIO*)ptrMsgDat;

	return( SendOneOldCommand(brd,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len) );
}
