/**
  ******************************************************************************
  * @file    task_StackAI.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.09.20
  * @brief   This file contains the functions of task_StackAI.c
  */ 

#ifndef _TASK_STACKAI_H
#define _TASK_STACKAI_H

#include "msg_Type.h"
#include "obj_L2Layer.h"
#include "obj_AiLayer.h"
#include "task_survey.h"

// Define Task EventFlags
/*
事件定义：Stack转发完数据，通知AI
 */
 
 //lzh_20130413
//回复正确事件标志
#define TASK_STACK_EVENT_ACK_OK			( 1 << 1 )
//回复错误事件标志
#define TASK_STACK_EVENT_ACK_ER			( 1 << 0 )

//接收开始事件标志
#define TASK_STACK_EVENT_RCV_START		( 1 << 2 )
//发送开始事件标志
#define TASK_STACK_EVENT_TRS_START		( 1 << 3 )
//发送完成时间标志
#define TASK_STACK_EVENT_TRS_STOP		( 1 << 4 )

#define OS_TASK_EVENT_STACK_TRS_RESULT		(1<<2)
#define OS_TASK_EVENT_STACK_TRS_QUEUE		(1<<3)
#define OS_TASK_EVENT_STACK_RCV_QUEUE		(1<<4)

// Define Task Vars and Structures

/*------------------------------------------------------------------------------------------
//task_AI的对协议栈或是任务的数据帧收发队列
------------------------------------------------------------------------------------------*/
/*
消息定义：Task需要发送数据，通知AI转发(进AI队列)，对应数据结构为：StackFrameQ_TaskIO
 */
#define OS_Q_STACK_FRAME_TASK_TRS_BRD_A		0x01	//无需应答
#define OS_Q_STACK_FRAME_TASK_TRS_BRD_B		0x02	//需要应答
#define OS_Q_STACK_FRAME_TASK_TRS_APT_A		0x03	//无需应答
#define OS_Q_STACK_FRAME_TASK_TRS_APT_B		0x04	//需要应答

//lzh_20130328
#define OS_Q_STACK_FRAME_TASK_TRS_ACK_A		0x05	//发送测试ACK
//lzh_20130413
#define OS_Q_STACK_FRAME_TASK_TRS_BLK_A		0x06	//发送block
/*
消息定义：Stack接收到数据，通知AI(进AI队列)，对应的数据结构：StackFrameQ_StackIO
 */
#define OS_Q_STACK_FRAME_STACK_RCV			0x04

#pragma pack(1)	//stm32_stack
//task_AI的对其它任务的交互数据结构
typedef struct
{
	//消息类型
	unsigned char msgType;
	//源任务地址
	vdp_task_t *ptrTCB;
	//目标地址(Output)/源地址(Input)
	unsigned short addr;
	//新旧指令
	unsigned char cmdType;	//0:old,1:new
	//指令
	unsigned char cmd;
	//数据包长度
	unsigned char len;
	//数据
	unsigned char dat[MAX_FRAME_LENGTH];
	
} StackFrameQ_TaskIO;

//task_AI的对协议栈的交互数据结构
typedef struct
{
	//消息类型
	unsigned char msgType;
	//数据类型
	unsigned char datType;
	//数据
	AI_DATA	datBuf;
	
} StackFrameQ_StackIO;

//lzh_20140508_s
typedef struct
{
	//消息类型
	unsigned char msgType;
	//数据类型
	unsigned char datType;
	//数据
	AI_LONG_DATA  datBuf;
	
} StackFrameQ_StackIO_LongData;
//lzh_20140508_e

#pragma pack()	//stm32_stack


// Define Task 3 items
extern Loop_vdp_common_buffer	vdp_StackAI_Trs_queue;
extern Loop_vdp_common_buffer	vdp_StackAI_Rcv_queue;
extern Loop_vdp_common_buffer	vdp_StackAI_sync_queue;

void vtk_TaskInit_StackAI( int Priority );

// Define Task others
void CheckTrsStatusAndWaitingForTrsResult(void);
void RcvQueueProcess(void);
void TrsQueueProcess(void);
unsigned char AI_PushOneCmdIntoQueue(unsigned char msgtype,unsigned short addr,unsigned char cmd, unsigned char newCmd, unsigned char len, unsigned char *ptrDat);	//lzh_20121124
// Define Task public API
unsigned char AI_BoardCastWithEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat);		//lzh_20121122
unsigned char AI_BoardCastWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat);	//lzh_20121122
unsigned char AI_AppointWithEventAck(unsigned short addr,unsigned char cmd,  unsigned char len, unsigned char *ptrDat);		//lzh_20121122
unsigned char AI_AppointWithoutEventAck(unsigned short addr,unsigned char cmd,  unsigned char len, unsigned char *ptrDat);	//lzh_20121122	

#define API_Stack_BRD_Without_ACK(cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A, BROADCAST_ADDR, cmd, 0, 0, NULL )

#define API_Stack_BRD_With_ACK(cmd) \
		AI_BoardCastWithEventAck(BROADCAST_ADDR, cmd, 0, NULL )

#define API_Stack_APT_Without_ACK(addr,cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, addr, cmd, 0, 0, NULL )

#define API_Stack_APT_With_ACK(addr,cmd) \
		AI_AppointWithEventAck(addr, cmd, 0, NULL )

//lzh_20121122
#define API_Stack_BRD_Without_ACK_Data(cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A, BROADCAST_ADDR, cmd, 0, len, ptr_dat )

#define API_Stack_APT_Without_ACK_Data(addr,cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, addr, cmd, 0, len, ptr_dat )

//lzh_20121124
unsigned char API_PropertyValue_Read(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address);
unsigned char API_PropertyValue_Response(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);
unsigned char API_PropertyValue_Write(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);

//lzh_20121128
//lzh_20130325
void TimerMuteRetrigger(void);

#define AVOID_NOISE_ENABLE	    SPK_MuteOn();

#define AVOID_NOISE_DISABLE	    TimerMuteRetrigger();	//SPK_MuteOff();	//lzh_20130325

//lzh_20121212
unsigned char API_GroupValue_Read(unsigned short group_id);
unsigned char API_GroupValue_Response(unsigned short group_id, unsigned char length, unsigned char *ptr_Data);
unsigned char API_GroupValue_Write(unsigned short group_id, unsigned char length, unsigned char *ptr_Data);

//lzh_20130328
#define API_Stack_BRD_ACK() \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_ACK_A, BROADCAST_ADDR, 0, 0, 0, NULL )

//lzh_20130413
#define API_Stack_BRD_BLK() \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BLK_A, BROADCAST_ADDR, 0, 0, 0, NULL )

#define API_Stack_New_APT(addr,cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B, addr, cmd, 1, 0, NULL )


#define API_Stack_New_APT_Data(addr,cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B, addr, cmd, 1, len, ptr_dat )

//stack_stm32
void TImerMuteCallBack(void);
			
			
#endif


