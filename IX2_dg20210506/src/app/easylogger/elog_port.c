/*
 * This file is part of the EasyLogger Library.
 *
 * Copyright (c) 2015, Armink, <armink.ztl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Portable interface for linux.
 * Created on: 2015-04-28
 */

#include "elog.h"
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

#ifdef ELOG_FILE_ENABLE
#include "elog_file.h"
#endif
static pthread_mutex_t output_lock;

/**
 * EasyLogger port initialize
 *
 * @return result
 */
ElogErrCode elog_port_init(void) {
    ElogErrCode result = ELOG_NO_ERR;

    pthread_mutex_init(&output_lock, NULL);

#ifdef ELOG_FILE_ENABLE
    elog_file_init();
#endif

    return result;
}

/**
 * EasyLogger port deinitialize
 *
 */
void elog_port_deinit(void) {
#ifdef ELOG_FILE_ENABLE
    elog_file_deinit();
#endif

    pthread_mutex_destroy(&output_lock);
}


/**
 * output log port interface
 *
 * @param log output of log
 * @param size log size
 */
void elog_port_output(uint8_t level, const char *log, size_t size) {

    char* info;

    /* output to terminal */
#ifdef ELOG_TERMINAL_ENABLE
    if(level < my_elog_get_printf_lvl())
    {
        printf("%.*s", (int)size, log);
    }
#endif

#ifdef ELOG_FILE_ENABLE
    /* write the file */
    if(level < my_elog_get_file_lvl())
    {
        elog_file_write(log, size);
    }
#else 
    if(level < my_elog_get_file_lvl())
    {
        #include "obj_TableSurver.h"
        if(log)
        {
            cJSON* record = cJSON_CreateObject();
            char level[2] = {0};
            char timeAndTid[201] = {0};
            char tid[200] = {0};
            info = malloc(size);
            int index = 0;

	        sscanf(log, "%[^/]/[%[^]]]", level, timeAndTid);
	        sscanf(timeAndTid, "%*s tid:%s", tid);

            cJSON_AddStringToObject(record, "LEVEL", level);
            cJSON_AddStringToObject(record, "TIME", get_time_string());
            cJSON_AddStringToObject(record, "TID", tid);

            if(info)
            {
                index = strlen(timeAndTid)+5;
                strncpy(info, log+index, size - index);
                index = size - index;
                info[index] = 0;
                while(index >= 0)
                {
                    if(info[index] == '\n')
                    {
                        info[index] = 0;
                    }
                    else if(info[index] != 0)
                    {
                        break;
                    }
                    index --;
                }
                cJSON_AddStringToObject(record, "INFO", info);
                free(info);
            }
            API_TB_AddByName(TB_NAME_Elog, record);
            cJSON_Delete(record);
        }
    }
#endif

    if(level < my_elog_get_udp_lvl())
    {
        Udp25007UnlimitedSend(my_elog_get_udp_ip(), log, size);
    }

    if(level < my_elog_get_ix_lvl())
    {
        info = malloc(size+1);
        if(info)
        {
            strncpy(info, log, size);
            info[size] = 0;
            ShellReport(info);
            free(info);
        }
    }
}

/**
 * output lock
 */
void elog_port_output_lock(void) {
    pthread_mutex_lock(&output_lock);
}

/**
 * output unlock
 */
void elog_port_output_unlock(void) {
    pthread_mutex_unlock(&output_lock);
}


/**
 * get current time interface
 *
 * @return current time
 */
const char *elog_port_get_time(void) {
    static char cur_system_time[24] = { 0 };
    char msec[5];

    time_t cur_t;
    struct tm cur_tm;

    time(&cur_t);
    localtime_r(&cur_t, &cur_tm);

    struct timeval tv;	
	gettimeofday(&tv, 0);
    snprintf(msec, 5, ":%03d", tv.tv_usec/1000);

    //strftime(cur_system_time, sizeof(cur_system_time), "%Y-%m-%d %T", &cur_tm);
    strftime(cur_system_time, sizeof(cur_system_time), "%T", &cur_tm);

    strcat(cur_system_time, msec);

    return cur_system_time;
}

/**
 * get current process name interface
 *
 * @return current process name
 */
const char *elog_port_get_p_info(void) {
    static char cur_process_info[10] = { 0 };

    snprintf(cur_process_info, 10, "pid:%04d", getpid());

    return cur_process_info;
}

/**
 * get current thread name interface
 *
 * @return current thread name
 */
const char *elog_port_get_t_info(void) {
    static char cur_thread_info[30] = { 0 };

    snprintf(cur_thread_info, 20, "tid:%s", OS_GetTaskName());

    return cur_thread_info;
}
