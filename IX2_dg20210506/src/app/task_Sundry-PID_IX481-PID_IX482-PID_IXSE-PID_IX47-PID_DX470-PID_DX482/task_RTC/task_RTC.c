/**
  ******************************************************************************
  * @file    task_RTC.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the task_RTC
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "task_Sundry.h"
#include "obj_RTC.h"
#include "task_RTC.h"


// timer
OS_TIMER timerRtc;



/******************************************************************************
 * @fn      vtk_TaskInit_Rtc()
 *
 * @brief   Task rtc initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Rtc(void)
{
	RtcInit();    
	// create timer, and start
	OS_CreateTimer(&timerRtc,TimerRtcCallBack,3000);
	OS_RetriggerTimer(&timerRtc );
}

int RTCResponse( unsigned char msg_id);

/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Rtc()
 *
 * @brief   Task Rtc loop routine
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Rtc(MsgRtc *msgRtc)
{  
    unsigned char rtcServer;
    switch(msgRtc->msgType)
    {
        case MSG_RTC_SET:
            RtcSyncReceive(msgRtc->rtcSyncData);
            if( msgRtc->head.msg_source_id != MSG_ID_SUNDRY )
            {
			RTCResponse(msgRtc->head.msg_source_id);
			dprintf( "RTCResponse  msg_source_id = %d\n",msgRtc->head.msg_source_id);
            }
            break;
        case MSG_RTC_SEND:            
            //API_Event_IoServer_InnerRead_All( RTC_SERVER_ENABLE, (unsigned char*)&(rtcServer) );
            if( rtcServer )
            {
                RtcSyncSend();
            }
            break;
            
        case MSG_RTC_TIMER_EVT:
            RtcTimerEventProcess();
            break;
    }    
}


/******************************************************************************
 * @fn      API_RtcSyncSend()
 *
 * @brief   Provide an application interface for other task to send rtc sync
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 *****************************************************************************/
unsigned char API_RtcSyncSend(void)
{
	MsgRtc msgRTC;

	msgRTC.head.msg_source_id 	= 0;
	msgRTC.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRTC.head.msg_type 		= MSG_TYPE_RTC;
	msgRTC.head.msg_sub_type 	= 0;
	msgRTC.msgType				= MSG_RTC_SEND;

	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRTC, sizeof(MsgRtc));
	return 1;
}


/******************************************************************************
 * @fn      API_RtcSet()
 *
 * @brief   Provide an application interface for other task to set rtc
 *
 * @param   iRtcData - rtc data to be set to the rtc unit
 *
 * @return  0 - false, 1 - ok
 *****************************************************************************/
unsigned char API_RtcSet(unsigned char msg_id, RtcTypeDef iRtcData)
{
	MsgRtc msgRTC;
	unsigned char data[10];
	int dataLen;
	int ret;
	
	msgRTC.head.msg_source_id 	= msg_id;
	msgRTC.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRTC.head.msg_type 		= MSG_TYPE_RTC;
	msgRTC.head.msg_sub_type 	= 0;
	msgRTC.msgType				= MSG_RTC_SET;
	msgRTC.rtcSyncData			= iRtcData;

	// user of the api in the same task, handle the msg derectly
	if ( msg_id == MSG_ID_SUNDRY)
	{
		vtk_TaskProcessEvent_Rtc( (MsgRtc*)&msgRTC );
		return 1;
	}
    // in diffrent task, push in Q and wait event
	else
	{
		// 压入本地队列
		push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRTC, sizeof(MsgRtc));
    
		vdp_task_t* pTask = GetTaskAccordingMsgID(msg_id);
		
		// 等待ack应答, 否则再发送一次
		ret = WaitForBusinessACK( pTask->p_syc_buf, MSG_TYPE_RTC,  data, &dataLen, 5000 );
		if(ret > 0)
		{
			dprintf( "wait for RTC set OK. dataLen = %d\n",dataLen);
			return 1;
		}
		else
		{
			dprintf( "wait for RTC timeout.\n");
			return 0;
		}
	}
}

/*******************************************************************************
 * @fn      TimerRtcCallBack()
 *
 * @brief   timer call back to handle sync send
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void TimerRtcCallBack(void)
{    
	MsgRtc msgRTC;

	msgRTC.head.msg_source_id 	= 0;
	msgRTC.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRTC.head.msg_type 		= MSG_TYPE_RTC;
	msgRTC.head.msg_sub_type 	= 0;
	msgRTC.msgType				= MSG_RTC_TIMER_EVT;

	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRTC, sizeof(MsgRtc));
	
	OS_RetriggerTimer(&timerRtc);
}

int RTCResponse( unsigned char msg_id)
{
	MsgRtc  msgRTC;

	vdp_task_t* pTask = GetTaskAccordingMsgID(msg_id);
	
	msgRTC.head.msg_target_id 	= msg_id;
	msgRTC.head.msg_source_id	= MSG_ID_SUNDRY;			
	msgRTC.head.msg_type		= (MSG_TYPE_RTC|COMMON_RESPONSE_BIT);
	msgRTC.head.msg_sub_type	= 0;
	
	push_vdp_common_queue(pTask->p_syc_buf, (char*)&msgRTC, 4);
	return 1;
}

