/**
  ******************************************************************************
  * @file    hd_rtc.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the headers of the hd_rtc .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/

#ifndef HD_RTC_H
#define HD_RTC_H

typedef struct
{
	unsigned char RTC_Hours;
	unsigned char RTC_Minutes;
	unsigned char RTC_Seconds;
	unsigned char RTC_H12;
}RTC_TimeTypeDef; 

typedef struct
{
	unsigned char RTC_WeekDay; 
	unsigned char RTC_Month;
	unsigned char RTC_Date; 
	unsigned char RTC_Year; 
}RTC_DateTypeDef;

typedef struct 
{
	RTC_TimeTypeDef	time_t;
	RTC_DateTypeDef	date_t;
}RTC_TIME;
// declare function

void hd_RtcInit(void);
void hd_RtcSetDate(RTC_DateTypeDef iDate);
RTC_DateTypeDef hd_RtcGetDate(void);
void hd_RtcSetTime(RTC_TimeTypeDef iTime);
RTC_TimeTypeDef hd_RtcGetTime(void);

void hd_N329SetTime(RTC_TIME iTime);
RTC_TIME hd_N329GetTime(void);

#endif

