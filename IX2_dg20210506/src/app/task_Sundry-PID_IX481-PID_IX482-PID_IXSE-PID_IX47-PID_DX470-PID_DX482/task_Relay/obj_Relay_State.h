/**
  ******************************************************************************
  * @file    obj_Relay_State.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Relay_State_H
#define _obj_Relay_State_H

#define NORMAL_OPEN	0
#define NORMAL_CLOSE	1


// Define Object Property-------------------------------------------------------
typedef struct RELAY_RUN_STRU	
{	unsigned char		relay_mode;
	unsigned char		relay_state;
	unsigned short	relay_timer_count;
	unsigned short	relay_timer;
}	RELAY_RUN;
extern RELAY_RUN Relay_Run;

// Define Object Function - Public----------------------------------------------
void Relay_Open_Timer(unsigned char relay_timer);
void Relay_Close(void);
unsigned char Get_Relay_State(void);


// Define Object Function - Private---------------------------------------------


#endif

