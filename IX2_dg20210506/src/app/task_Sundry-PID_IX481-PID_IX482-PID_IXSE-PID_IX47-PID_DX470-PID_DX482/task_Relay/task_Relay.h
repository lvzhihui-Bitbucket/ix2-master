/**
  ******************************************************************************
  * @file    task_Relay.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Relay_H
#define _task_Relay_H

#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "obj_Relay_State.h"
#include "task_survey.h"	


// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
typedef struct MSG_RELAY_STRU	
{	
	VDP_MSG_HEAD 	head;
	unsigned char	msg_type;					
	unsigned char	relay_timer;					
}	MSG_RELAY;
//msg_type
#define MSG_TYPE_RELAY_OPEN			0
#define MSG_TYPE_RELAY_CLOSE		1


// Define Task 2 items----------------------------------------------------------
	//����ʱ
extern OS_TIMER relayTiming;

void vtk_TaskInit_Relay(void);


// Define Task others-----------------------------------------------------------
void vtk_TaskProcessEvent_Relay(MSG_RELAY *msgRelay);
void Relay_Timer_Off(void);

// Define API-------------------------------------------------------------------
void API_Relay_Timer(unsigned char msg_type_temp,unsigned char relay_timer);

#define API_Relay_ON_Timer(time)	API_Relay_Timer(MSG_TYPE_RELAY_OPEN,time)
#define API_Relay_ON()				API_Relay_Timer(MSG_TYPE_RELAY_OPEN,0)
#define API_Relay_OFF()				API_Relay_Timer(MSG_TYPE_RELAY_CLOSE,0)


#endif
