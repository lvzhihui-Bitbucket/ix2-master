/**
  ******************************************************************************
  * @file    hd_Power.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the hd_Power
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_Sundry.h"
#include "task_survey.h"
#include "task_Hal.h"
#include "hd_Power.h"
#include "task_Power.h"
#include "obj_Power.h"
#include "hal_gpio_def.h"
#include "vdp_uart.h"

#include "elog.h"

extern PowerStruct power;
extern OS_TIMER timerBeepPower;
extern int hal_fd;
static void AMP_MUTE_PowerCtrl(uint8 iEnable);
/***********************************************************************
 * @fn      hd_PowerInit()
 *
 * @brief   
 *
 * @param   
 *
 * @return  none
 ************************************************************************/
void hd_PowerInit(void)
{
	TFT_PowerCtrl(0);
	VDD12_PowerCtrl(0);
	AMP_MUTE_PowerCtrl(0);
	AP324_PowerCtrl(0);
	MIC_MUTE_PowerCtrl(0);
	POW324_PowerCtrl(0);
	NT329_MUTE_PowerCtrl(0);
	AU_RLCON_SWITCH(0);
	ExtRing_PowerCtrl(0);
}


/***********************************************************************
 * @fn      TFT_PowerCtrl()
 *
 * @brief   TFT power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void TFT_PowerCtrl( unsigned char iEnable )
{	
	int powerOn;
	
	powerOn = iEnable? 1:0;
	
	//dprintf("Power >>>TFT power = %d.\n", powerOn);
	
	//ioctl( hal_fd, BACK_LIGHT_ENABLE, &powerOn );
	//ioctl( hal_fd, LCD_VCOM_CTL, &powerOn );            //lyx_20171019
}


/***********************************************************************
 * @fn      VDD12_PowerCtrl()   12V����
 *
 * @brief   VDD12 power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void VDD12_PowerCtrl(uint8 iEnable)
{
	int powerOn;

	powerOn = iEnable? 1:0;
	
	printf("Power >>>12V power = %d.\n", powerOn);
	
	api_uart_send_pack(UART_TYPE_AU_1124_PWR_CTRL, &powerOn, 1);
}

/***********************************************************************
 * @fn      AMP_MUTE_PowerCtrl()
 *
 * @brief   AMP power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
// lzh_20220427_s  -- for DX transfer to APP
// set usage:
// step1:  AMP_MUTE_PowerCtrl()
// step2:  amp_force_disable_set()
// reset usage:
// step1:  amp_force_disable_reset()
// step2:  AMP_MUTE_PowerCtrl()
static int amp_force_disable = 0;
static void amp_force_disable_set(void)
{
	amp_force_disable = 1;
}
static void amp_force_disable_reset(void)
{
	amp_force_disable = 0;
}

static void AMP_MUTE_PowerCtrl(uint8 iEnable)
{
	int powerOn;

	powerOn = iEnable? 1:0;

	dprintf("Power >>>(AMP)mute power = %d.\n", powerOn);
	log_d("Power >>>(AMP)mute power = %d.\n", powerOn);
	
	if( amp_force_disable )
	{
		dprintf("Power >>>(AMP)mute power already force disable\n");
		log_d("Power >>>(AMP)mute power already force disable\n");
		AMPMUTE_RESET();
		return;
	}

	if(powerOn)
		AMPMUTE_SET();
	else
		AMPMUTE_RESET();	
}
// lzh_20220427_e

/***********************************************************************
 * @fn      AU_RLCON_SWITCH()
 *
 * @brief   Audio swith: 0/DX-DS, 1/APP-DS  
 *
 * @param   iEnable  - 0 or 1
 *
 * @return  none
 ************************************************************************/
void AU_RLCON_SWITCH( unsigned char iEnable )
{	
	char powerOn;

	powerOn = iEnable? 1:0;
	
	printf("AU_RLCON_SWITCH to %d.\n", powerOn);
	log_d("AU_RLCON_SWITCH to %d.\n", powerOn);
	
	api_uart_send_pack(UART_TYPE_AUOUT_SWITCH, &powerOn, 1);
	usleep(100*1000);
	powerOn = iEnable? 1:0;
	api_uart_send_pack(UART_TYPE_AUOUT_SWITCH, &powerOn, 1);
}

/***********************************************************************
 * @fn      AP324_PowerCtrl()
 *
 * @brief   AP324 power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void AP324_PowerCtrl(uint8 iEnable)
{
	int powerOn;

	powerOn = iEnable? 1:0;
	
	//dprintf("Power >>>AP324 power = %d.\n", powerOn);
	
	//ioctl( hal_fd, AP324_ENABLE, &powerOn );
}

/***********************************************************************
 * @fn      MIC_MUTE_PowerCtrl()
 *
 * @brief   MIC_MUTE power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void MIC_MUTE_PowerCtrl(uint8 iEnable)
{
	int powerOn;

	powerOn = iEnable? 0:1;
	
	//dprintf("Power >>>MIC_MUTE power = %d.\n", powerOn);
	
	//ioctl( hal_fd, MIC_MUTE_ENABLE, &powerOn );
}

/***********************************************************************
 * @fn      POW324_PowerCtrl()
 *
 * @brief   POW324 power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void POW324_PowerCtrl(uint8 iEnable)
{
	int powerOn;

	powerOn = iEnable? 1:0;
	
	//dprintf("Power >>>POW324 power = %d.\n", powerOn);
	
	//ioctl( hal_fd, POW324_ENABLE, &powerOn );
}

/***********************************************************************
 * @fn      NT329_MUTE_PowerCtrl()
 *
 * @brief   NT329_MUTE power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void NT329_MUTE_PowerCtrl(uint8 iEnable)		//czn_20170522
{
	int powerOn;

	powerOn = iEnable? 0:1;
	
	//dprintf("Power >>>NT329_MUTE power = %d.\n", powerOn);
	
	//ioctl( hal_fd, NT329_MUTE_ENABLE, &powerOn );
}

/***********************************************************************
 * @fn      ExtRing_PowerCtrl()
 *
 * @brief   ExtRing power supply enable or disable 
 *
 * @param   iEnable  - enable or disable
 *
 * @return  none
 ************************************************************************/
void ExtRing_PowerCtrl( unsigned char iEnable )
{	
	int  powerOn;
	
	powerOn = iEnable? 1:0;
	
	printf("Power >>>ExtRing power = %d.\n", powerOn);

	
	// 5 = N329��STM8����: ExtRinger - 0=OFF; 1=ON
	api_uart_send_pack(UART_TYPE_N2S_EXTRINGER_CTRL, &powerOn, 1);
	//ioctl( hal_fd, EXT_RING_CTL, &powerOn );      			//lyx_20170921
	
}
#if defined(PID_DX470)||defined(PID_DX482)
OS_TIMER dt_stack_mute_timer;
void dt_stack_mute_timer_callback(void)
{
	API_POWER_STACK_MUTE_OFF();
}
int dt_stack_mute_timer_start(void)
{
	static unsigned char init_flag=0;
	if(init_flag==0)
	{
		OS_CreateTimer( &dt_stack_mute_timer, dt_stack_mute_timer_callback, 600/25);
		init_flag=1;
	}
	OS_RetriggerTimer( &dt_stack_mute_timer );	
}
#endif
/***********************************************************************
 * @fn      PowerOption()
 *
 * @brief   Judge the requst power event should change the power or not,
            and process the power action
 *
 * @param   iPowerType  - requst power type
 * @param   iOptionType - requst power option, on or off 
 *
 * @return  delayTime
 ************************************************************************/

/*
					TFT		12V		RELAY		RL-CON		LM324		NJU7084

	POWER_VIDEO	=		=		=
	
	POWER_MENU		=

	POWER_TALK				=								=			=

	POWER_RING															=

	POWER_BEEP															=

*/

unsigned char PowerOption(unsigned char iPowerType, unsigned char iOptionType)
{
#if 1
	switch( iPowerType )
	{
		case POWER_TFT:	
			power.tftState = iOptionType;
			break;

		case POWER_TALK:
			if(iOptionType&TALK_ANALOG_POWER_ON)
			{
				power.talkState |= TALK_ANALOG_POWER_ON;
			}
			else if(iOptionType&TALK_DIGITAL_POWER_ON)	
			{
				power.talkState |= TALK_DIGITAL_POWER_ON;
			}
			else
			{
				power.talkState = iOptionType;
			}
			break;

		case POWER_RING:
			power.ringState = iOptionType;
			break;

		case POWER_BEEP:
			power.beepState = iOptionType;
			break;

		case POWER_VIDEO:
			power.videoState = iOptionType;
			break;
		case POWER_EXT_RING:
			power.extRingState = iOptionType;
			ExtRing_PowerCtrl(power.extRingState);
			return;
		#if	defined(PID_DX470)||defined(PID_DX482)
		case POWER_STACK_MUTE:
			//printf("@@@@@@@POWER_STACK_MUTE %d\n",iOptionType);
			//power.dt_stack_state=0;
			#if 1
			power.dt_stack_state= iOptionType;
			if(power.dt_stack_state)
			{
				if(power.talkState)
					AMP_MUTE_PowerCtrl(0);
				dt_stack_mute_timer_start();
				return;
			}
			#endif
			break;
		case POWER_DX_APP_TALK:
			if(iOptionType)
			{
				printf("1111111111POWER_DX_APP_TALK on\n");
				amp_force_disable_reset();
				AMP_MUTE_PowerCtrl(0);
				amp_force_disable_set();
				return;
			}
			else
			{
				printf("222222222POWER_DX_APP_TALK on\n");
				amp_force_disable_reset();
				return;
			}
			break;
		#endif
	}

	// ????
	#if defined(PID_DX470)||defined(PID_DX482)
	VDD12_PowerCtrl(power.talkState);
	#else
	VDD12_PowerCtrl(power.videoState|power.talkState|power.extRingState); 
	#endif
	// ????
	#if defined(PID_DX470)||defined(PID_DX482)

	if(power.dt_stack_state&&power.talkState)
	{
		AMP_MUTE_PowerCtrl(0);
		//return;
	}
	else
		AMP_MUTE_PowerCtrl(power.talkState||power.ringState||power.beepState);
	#else
	AMP_MUTE_PowerCtrl(power.talkState||power.ringState||power.beepState);	//czn_20170522
	#endif
	// ????
	MIC_MUTE_PowerCtrl(power.talkState&TALK_ANALOG_POWER_ON);//czn_20170522
	// lcd??
	TFT_PowerCtrl( power.tftState);		//czn_20170522
	// ?????
	AP324_PowerCtrl(power.talkState&TALK_ANALOG_POWER_ON);
	POW324_PowerCtrl((power.talkState&TALK_DIGITAL_POWER_ON));
	NT329_MUTE_PowerCtrl(power.talkState&TALK_DIGITAL_POWER_ON);
#else
    switch( iPowerType )
    {
        case POWER_TFT:	
		if( iOptionType == POWER_ON )
		{
			if( power.tftState != POWER_ON )
			{
				power.tftState = POWER_ON;
				TFT_PowerCtrl(1);
			}
		}
		else		
		{
			if( power.tftState != POWER_OFF )
			{
				power.tftState = POWER_OFF;
				TFT_PowerCtrl(0);
			}
		}
		dprintf("Power >>>power.tftState = %d.\n", power.tftState );
		break;
		
        case POWER_TALK:	
		if( iOptionType == TALK_ANALOG_POWER_ON )
		{
			if( power.talkState != TALK_ANALOG_POWER_ON )
			{
				power.talkState = TALK_ANALOG_POWER_ON;
				VDD12_PowerCtrl(1);
				AMP_MUTE_PowerCtrl(1);
				AP324_PowerCtrl(1);
				MIC_MUTE_PowerCtrl(1);
				POW324_PowerCtrl(0);
				NT329_MUTE_PowerCtrl(0);
			}
		}
		else if( iOptionType == TALK_DIGITAL_POWER_ON )
		{
			if( power.talkState != TALK_DIGITAL_POWER_ON )
			{
				power.talkState = TALK_DIGITAL_POWER_ON;
				VDD12_PowerCtrl(1);
				AMP_MUTE_PowerCtrl(1);
				AP324_PowerCtrl(1);
				MIC_MUTE_PowerCtrl(1);
				POW324_PowerCtrl(1);
				NT329_MUTE_PowerCtrl(1);
			}
		}
		else
		{
			if( power.talkState != POWER_OFF )
			{
				power.talkState = POWER_OFF;

				if((!power.ringState) && (!power.beepState))
				{
					VDD12_PowerCtrl(0);
					AMP_MUTE_PowerCtrl(0);
				}
				AP324_PowerCtrl(0);
				MIC_MUTE_PowerCtrl(0);
				POW324_PowerCtrl(0);
				NT329_MUTE_PowerCtrl(0);
			}
		}
		dprintf("Power >>>power.talkState = %d.\n", power.talkState );
	        break;
			
        case POWER_RING:
		if( iOptionType == POWER_ON )
		{
			if( power.ringState != POWER_ON )
			{
				power.ringState = POWER_ON;
				VDD12_PowerCtrl(1);
				AMP_MUTE_PowerCtrl(1);
			}
		}
		else
		{
			if( power.ringState != POWER_OFF )
			{
				power.ringState = POWER_OFF;

				if ((power.talkState != TALK_ANALOG_POWER_ON) && (power.talkState != TALK_DIGITAL_POWER_ON) && (power.beepState != POWER_ON) )  // 20140819
				{
					VDD12_PowerCtrl(0);
					AMP_MUTE_PowerCtrl(0);
				} 
			}
		}
		dprintf("Power >>>power.ringState = %d.\n", power.ringState );
		break;

        case POWER_BEEP:
		if( iOptionType == POWER_ON )
		{
			if( power.beepState != POWER_ON )
			{
				power.beepState = POWER_ON;
				VDD12_PowerCtrl(1);
				AMP_MUTE_PowerCtrl(1);
    			}
		}
		else
		{
			if( power.beepState != POWER_OFF )
			{
				power.beepState = POWER_OFF;
				if ((power.talkState != TALK_ANALOG_POWER_ON) && (power.talkState != TALK_DIGITAL_POWER_ON) && (power.ringState != POWER_ON) )  // 20140819
				{
					VDD12_PowerCtrl(0);
					AMP_MUTE_PowerCtrl(0);
				} 
			}
		}
		break;
    }
#endif
	return 1;
}

int get_power_dt_mute(void)
{
	#if defined(PID_DX470)||defined(PID_DX482)
	return power.dt_stack_state;
	#else
	return 0;
	#endif
}

