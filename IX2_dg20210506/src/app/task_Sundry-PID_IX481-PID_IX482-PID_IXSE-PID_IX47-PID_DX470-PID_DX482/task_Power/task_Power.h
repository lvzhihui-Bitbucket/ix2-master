/**
  ******************************************************************************
  * @file    task_Power.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_Power.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef POWER_TASK_H
#define POWER_TASK_H

#include "RTOS.h"
#include "obj_Power.h"
#include "hd_Power.h"
#include "obj_ExtRing.h"


#include "task_survey.h"
#include "task_Ring.h"

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/

/*******************************************************************************
                        Define message structure
*******************************************************************************/
#pragma pack(1)
typedef struct
{
  VDP_MSG_HEAD head;
  unsigned char msgMainType;
  unsigned char msgType;
  unsigned char subMsg;
//  OS_TASK *sourceTaskTCB;
} MsgPower;
#pragma pack()
/*******************************************************************************
                              Define Msg Type  
*******************************************************************************/
#define POWER_TFT       		0
#define POWER_TALK      		1
#define POWER_RING      		2
#define POWER_BEEP      		3
#define POWER_VIDEO		4
#define POWER_EXT_RING	5
#define POWER_STACK_MUTE		20
#define POWER_DX_APP_TALK		21

/*******************************************************************************
                               Define subMsg
*******************************************************************************/
#define POWER_OFF       				0x00
#define POWER_ON        				0x01
#define TALK_ANALOG_POWER_ON       0x01
#define TALK_DIGITAL_POWER_ON       0x02

/*******************************************************************************
                            Declare task 2 items
*******************************************************************************/
//extern OS_TASK TCB_Power;
void vtk_TaskInit_Power( void );


/*******************************************************************************
                      Task application interface
*******************************************************************************/
unsigned char API_PowerRequst(unsigned char iMsgType, unsigned char iSubMsg);
#define API_POWER_TALK_ON()         	API_PowerRequst( POWER_TALK, TALK_ANALOG_POWER_ON )
#define API_POWER_TALK_OFF()        	API_PowerRequst( POWER_TALK, POWER_OFF )
#define API_POWER_DX_APP_TALK_ON()         	API_PowerRequst( POWER_DX_APP_TALK, POWER_ON)
#define API_POWER_DX_APP_TALK_OFF()        	API_PowerRequst( POWER_DX_APP_TALK, POWER_OFF )
#define API_POWER_UDP_TALK_ON()   	API_PowerRequst( POWER_TALK, TALK_DIGITAL_POWER_ON )
#define API_POWER_RING_ON()         	API_PowerRequst( POWER_RING, POWER_ON )
#define API_POWER_RING_OFF()        	API_PowerRequst( POWER_RING, POWER_OFF )
#define API_POWER_BEEP_ON()         	API_PowerRequst( POWER_BEEP, POWER_ON )
#define API_POWER_BEEP_OFF()        	API_PowerRequst( POWER_BEEP, POWER_OFF )

#define API_POWER_TFT_ON()          	API_PowerRequst( POWER_TFT, POWER_ON )
#define API_POWER_TFT_OFF()         	API_PowerRequst( POWER_TFT, POWER_OFF )

#define API_POWER_VIDEO_ON()          API_PowerRequst( POWER_VIDEO, POWER_ON )
#define API_POWER_VIDEO_OFF()         API_PowerRequst( POWER_VIDEO, POWER_OFF )

#define API_POWER_STACK_MUTE_ON()          API_PowerRequst( POWER_STACK_MUTE, POWER_ON )
#define API_POWER_STACK_MUTE_OFF()          API_PowerRequst( POWER_STACK_MUTE, POWER_OFF )

//#define API_POWER_EXT_RING_ON()	API_PowerRequst( POWER_EXT_RING, POWER_ON )
//#define API_POWER_EXT_RING_OFF()	API_PowerRequst( POWER_EXT_RING, POWER_OFF )

#if defined(PID_DX470)||defined(PID_DX482)
#define API_POWER_EXT_RING_ON() 		API_ExtRingCtrl(EXT_RING_ON)
#define API_POWER_EXT_RING_OFF()		API_ExtRingCtrl(EXT_RING_OFF)
#else
#define API_POWER_EXT_RING_ON() 		{if(api_get_stm8_mode()  ==MODE_DJ4A) api_plugswitch_control_update(3,0);  else API_ExtRingCtrl(EXT_RING_ON);}
#define API_POWER_EXT_RING_OFF()		{if(api_get_stm8_mode()  ==MODE_DJ4A) api_plugswitch_control_update(0,0);  else API_ExtRingCtrl(EXT_RING_OFF);}
#endif

/*******************************************************************************
                      Declare task other functions
*******************************************************************************/
void TimerBeepPowerCallBack();
void vtk_TaskProcessEvent_Power(MsgPower *msgPower);


#endif

