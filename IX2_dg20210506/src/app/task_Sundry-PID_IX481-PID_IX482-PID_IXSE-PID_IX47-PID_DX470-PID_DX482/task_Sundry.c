/**
  ******************************************************************************
  * @file    task_Sundry.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_Sundry
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "task_Sundry.h"

Loop_vdp_common_buffer	vdp_sundry_mesg_queue;
Loop_vdp_common_buffer	vdp_sundry_sync_queue;
vdp_task_t				task_sundry;

void vdp_sundry_mesg_data_process(char* msg_data, int len);
void* vdp_sundry_task( void* arg );


void vtk_TaskInit_sundry(int priority)
{
	init_vdp_common_queue(&vdp_sundry_mesg_queue, 3000, (msg_process)vdp_sundry_mesg_data_process, &task_sundry);
	init_vdp_common_queue(&vdp_sundry_sync_queue, 100, NULL, &task_sundry);
	init_vdp_common_task(&task_sundry, MSG_ID_SUNDRY, vdp_sundry_task, &vdp_sundry_mesg_queue, &vdp_sundry_sync_queue);

	vtk_TaskInit_Beep();
	vtk_TaskInit_Ring();
	//vtk_TaskInit_Rtc();		//cao_20151118

	vtk_TaskInit_Power();	//zxj_add_20151124
	vtk_TaskInit_Led();		//zxj_add_20151124
	//vtk_TaskInit_Relay();
	//vtk_TaskInit_Talk();	//zxj_add_20151124	

	//API_LED_POWER_FLASH_IRREGULAR();	 //lyx 20170731	

	//API_LED_TALK_FLASH_SLOW();
	//API_LED_UNLOCK_FLASH_FAST();
	//API_LED_POWER_FLASH_FAST();
	printf("======vtk_TaskInit_sundry ok===========\n");	
	
}

void exit_vdp_sundry_task(void)
{
	exit_vdp_common_queue(&vdp_sundry_mesg_queue);
	exit_vdp_common_queue(&vdp_sundry_sync_queue);
	exit_vdp_common_task(&task_sundry);	
}

void* vdp_sundry_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			//printf(">>>>>>>>>>>>>>>vdp_sundry_task process<<<<<<<<<<<<<<\n");
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		//printf(">>>>>>>>>>>>>>>vdp_sundry_task<<<<<<<<<<<<<<\n");
	}
	return 0;
}

void vdp_sundry_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pMsgSundry = (VDP_MSG_HEAD*)msg_data;
	switch (pMsgSundry->msg_type)
	{
		case MSG_TYPE_RING:
			vtk_TaskProcessEvent_RingCtrl( (RING_STRUCT*)pMsgSundry );
			break;
		case MSG_TYPE_BEEPER:
			vtk_TaskProcessEvent_Beep( (BEEP_STRUCT*)pMsgSundry );
			break;
		case MSG_TYPE_RTC:
			//vtk_TaskProcessEvent_Rtc( (MsgRtc*)pMsgSundry );
			break;

		case MSG_TYPE_LED:
			vtk_TaskProcessEvent_Led((MsgLed *)pMsgSundry);
			break;
		case MSG_TYPE_TALK:		//czn_20170803
			//vtk_TaskProcessEvent_Talk((MsgTalk *)pMsgSundry);
			break;
		case MSG_TYPE_POWER:
			vtk_TaskProcessEvent_Power((MsgPower *)pMsgSundry);
			break;
		case MSG_TYPE_RELAY:
			//vtk_TaskProcessEvent_Relay((MSG_RELAY *)pMsgSundry);
			break;
			
		default:
			break;
	}
}

