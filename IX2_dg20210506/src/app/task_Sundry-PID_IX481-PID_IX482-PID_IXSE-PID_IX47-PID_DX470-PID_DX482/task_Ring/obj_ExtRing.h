
#ifndef OBJ_EXT_RING_H
#define OBJ_EXT_RING_H

void ExtRingInit(void);
void ExitRingCtrl(unsigned char onOff);
void ExtRingTimerEventHandler(void);

#endif