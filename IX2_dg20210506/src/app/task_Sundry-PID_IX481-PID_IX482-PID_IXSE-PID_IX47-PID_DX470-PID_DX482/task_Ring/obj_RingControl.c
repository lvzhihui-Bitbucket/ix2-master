/**
  ******************************************************************************
  * @file    obj_RingControl.c
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of obj_RingControl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
//#include "RTOS.h"
#include "task_Sundry.h"
//#include "task_Power.h"
#include "task_Ring.h"
#include "obj_RingControl.h"
#include "obj_RingConfig.h"
//#include "../../task_RingPlayer/task_player.h"
#include "task_survey.h"
#include "task_Power.h"	//zxj_add_20151124
#include "vdp_uart.h"


#define CYCLE_STATE_STOP   0
#define CYCLE_STATE_PLAY   1

#define SRESCENDO_TIME_INTERVAL    1000


static RingCtrl_TypeDef ringCtrl;
static OS_TIMER timer_CountLimit;  //���ڰ�������������
static OS_TIMER timer_TimeLimit;  //���ڰ�ʱ����������
static OS_TIMER timer_Crescendo;//����������ǿ


static void Timer_CountLimit_Callback(void)
{
	if (ringCtrl.RingCount >= ringCfg.RingCountLimit)
	{
		//��Ҫֱ�ӵ���RingStop(),�����ڹ�powerʱ,��timer�е���power��API,�Ӷ���Ϊʹ��OS_PutMail()����������;
		//cao_20181020_s
		OS_StopTimer(&timer_CountLimit);
		//cao_20181020_e
		API_RingStop();
		return;
	}

	if (ringCtrl.RingCycleState == CYCLE_STATE_PLAY)//����Timer ISR֮ǰ����"CYCLE_STATE_PLAY"״̬
	{
		ringCtrl.RingCycleState = CYCLE_STATE_STOP;
		API_WavePlayer_Stop();
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleGap/25);		//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
	else //����Timer ISR֮ǰ����"CYCLE_STATE_STOP"״̬
	{
		ringCtrl.RingCycleState = CYCLE_STATE_PLAY;
		API_WavePlayer_Play(ringCfg.RingTune);
		ringCtrl.RingCount++;
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleTimeLimit/25);	//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
}

static void Timer_TimeLimit_Callback(void)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_TIME_CYCLE;
	msgRing.subMsg				= 0;

	// ѹ�뱾�ض���
	if (push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT)) != 0)
	{
		printf("-----------API_RingStop:push_vdp_common_queue just full-----------\n");		
	}
}
#if 0
void Ring_Time_Cycle(void)
{
	if (ringCtrl.RingTime >= (1000 * ringCfg.RingTimeLimit))	//czn_20170113		//if (ringCtrl.RingTime >= ((1000/25) * ringCfg.RingTimeLimit))
	{
		//��Ҫֱ�ӵ���RingStop(),�����ڹ�powerʱ,��timer�е���power��API,�Ӷ���Ϊʹ��OS_PutMail()����������;
		//cao_20181020_s
		OS_StopTimer(&timer_TimeLimit);
		//cao_20181020_e
		API_RingStop();
		return;
	}

	if (ringCtrl.RingCycleState == CYCLE_STATE_PLAY)//����Timer ISR֮ǰ����"CYCLE_STATE_PLAY"״̬
	{
		ringCtrl.RingCycleState = CYCLE_STATE_STOP;
		API_WavePlayer_Stop();
		ringCtrl.RingTime += ringCfg.RingCycleGap;
		OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleGap/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}
	else //����Timer ISR֮ǰ����"CYCLE_STATE_STOP"״̬
	{
		ringCtrl.RingCycleState = CYCLE_STATE_PLAY;
		API_WavePlayer_Play(ringCfg.RingTune);
		ringCtrl.RingTime += ringCfg.RingCycleTimeLimit;
		OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}

}
#else
void Ring_Time_Cycle(void)
{
	if (ringCtrl.RingTime >= (1000 * ringCfg.RingTimeLimit))	//czn_20170113		//if (ringCtrl.RingTime >= ((1000/25) * ringCfg.RingTimeLimit))
	{
		//��Ҫֱ�ӵ���RingStop(),�����ڹ�powerʱ,��timer�е���power��API,�Ӷ���Ϊʹ��OS_PutMail()����������;
		//cao_20181020_s
		OS_StopTimer(&timer_TimeLimit);
		//cao_20181020_e
		API_RingStop();
		return;
	}

	if (ringCtrl.RingCycleState == CYCLE_STATE_PLAY)//����Timer ISR֮ǰ����"CYCLE_STATE_PLAY"״̬
	{
		if(IfWavePlayer_Idle())
		{
			ringCtrl.RingCycleState = CYCLE_STATE_STOP;
			API_WavePlayer_Stop();
			ringCtrl.RingTime += ringCfg.RingCycleGap;
			OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleGap/25);
			OS_RetriggerTimer(&timer_TimeLimit);
		}
		else
		{
			ringCtrl.RingTime += 200;
			//OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
			OS_SetTimerPeriod(&timer_TimeLimit, 200/25);
			OS_RetriggerTimer(&timer_TimeLimit);

		}
	}
	else //����Timer ISR֮ǰ����"CYCLE_STATE_STOP"״̬
	{
		ringCtrl.RingCycleState = CYCLE_STATE_PLAY;
		API_WavePlayer_Play(ringCfg.RingTune);
		ringCtrl.RingTime += 200;
		//OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
		OS_SetTimerPeriod(&timer_TimeLimit, 200/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}

}
#endif
static void Timer_Crescendo_Callback(void)
{
	if( RingGetState() != RING_STATE_PLAY )
	{
		return;
	}
	ringCtrl.RingCrescendoCnt++;
	API_WavePlayer_SetVolume(ringCtrl.RingCrescendoCnt);
	if(ringCtrl.RingCrescendoCnt < ringCfg.RingVolume)
	{
		OS_RetriggerTimer(&timer_Crescendo);//��������ʱ
	}
}


void RingCtrl_Init(void)
{
	//��������ʱ(δ����)
	OS_CreateTimer(&timer_CountLimit, Timer_CountLimit_Callback, 1);
	OS_CreateTimer(&timer_TimeLimit, Timer_TimeLimit_Callback, 1);
	OS_CreateTimer(&timer_Crescendo, Timer_Crescendo_Callback, SRESCENDO_TIME_INTERVAL/25);
}

/**
 * @brief   Start ring
 * @param   None
 * @return  None
 */
void RingCtrl_Start(void)
{
	//�����Դ
	#ifndef PID_IXSE
	API_POWER_RING_ON();
	#endif
	#if 0
	if((ringCfg.RingCrescendo == 1) && (ringCfg.RingVolume > 1))
	{
		ringCtrl.RingCrescendoCnt = 1;//������ǿ�Ǵ��߼�����1��ʼ
		API_WavePlayer_SetVolume(ringCtrl.RingCrescendoCnt);
		OS_RetriggerTimer(&timer_Crescendo);//��������ʱ
	}
	else // in case of NOT crescendo effect or ringCfg.RingVolume is 0 or 1
	{
	
		API_WavePlayer_SetVolume(ringCfg.RingVolume);
	}
	#endif
	API_WavePlayer_SetVolume(ringCfg.RingVolume);
	// start ring
	RingSetState(RING_STATE_PLAY);	//���ű�־
	ringCtrl.RingCycleState = CYCLE_STATE_PLAY;

	//ringCfg.RingTune = 1;
	API_WavePlayer_Play(ringCfg.RingTune);     //��������

	if (ringCfg.RingCtrlPriority == PRIORITY_COUNT_LIMIT)
	{
		ringCtrl.RingCount = 1;
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_CountLimit);//��������ʱ
	}
	else if (ringCfg.RingCtrlPriority == PRIORITY_TIME_LIMIT)
	{
		ringCtrl.RingTime = ringCfg.RingCycleTimeLimit;
		//OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
		OS_SetTimerPeriod(&timer_TimeLimit, 200/25);
		OS_RetriggerTimer(&timer_TimeLimit);//��������?
	}
	if(api_get_stm8_mode()  ==MODE_DJ4A)
		api_plugswitch_control_update(3,0);
}

/**
 * @brief   Stop ring
 * @param   None
 * @return  None
 */
void RingCtrl_Stop(unsigned char powerOff)
{
	OS_StopTimer(&timer_CountLimit);
	OS_StopTimer(&timer_TimeLimit);
	API_WavePlayer_Stop();
	if( powerOff )
	{
		API_POWER_RING_OFF();
	}

	RingSetState(RING_STATE_STOP);
	ringCtrl.RingCycleState   = CYCLE_STATE_STOP;
	ringCtrl.RingCount        = 0;
	ringCtrl.RingTime         = 0;
	ringCtrl.RingCrescendoCnt = 0;
}

/**
 * @brief   Get the running ring state
 * @param   None
 * @return  ringCtrl.RingState
 */
uint8 RingGetState(void)
{
	return ringCtrl.RingState;
}



/**
 * @brief   change the the ring running state
 * @param   state RING_STATE_PLAY or RING_STATE_STOP
 * @return  None
 */
void RingSetState(uint8 state)
{
	ringCtrl.RingState = state;
}

