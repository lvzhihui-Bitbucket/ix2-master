/**
  ******************************************************************************
  * @file    task_Ring.c
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of task_Ring
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "task_Sundry.h"
#include "task_Ring.h"
#include "obj_RingControl.h"
#include "obj_RingConfig.h"
#include "task_VideoMenu.h"

#include "obj_ExtRing.h"

/**
  * @brief  Task Ring initialize
  * @param  None
  * @retval None
  */
void vtk_TaskInit_Ring( void )
{
	RingCtrl_Init();
	ExtRingInit();     //lyx_20170921
	dprintf("Ring >>>vtk_TaskInit_Ring.\n");
}

/**
  * @brief  Task Ring loop routine
  * @param  msgRing
  * @retval None
  */
void vtk_TaskProcessEvent_RingCtrl(RING_STRUCT *msgRing)
{
	switch (msgRing->cmd)
	{
		case RING_PLAY:
			//dprintf("Ring>>>ring play. \n");
			if(RingGetState() == RING_STATE_PLAY)
			{
				dprintf("Ring>>>RingCtrl_Stop!!!. \n");
				RingCtrl_Stop(0);				
			}
			Ring_LoadScene(msgRing->subMsg);	//װ�س�������
			RingCtrl_Start();		
			break;
		case RING_STOP:
			dprintf("Ring>>>ring stop. \n");
			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_RING_STOP);
			RingCtrl_Stop(1);
			break;

		case RING_STOP_KEEP_PWR:
			//dprintf("Ring>>>ring stop and keep power. \n");
			RingCtrl_Stop(0);
			break;

		case RING_SET_TUNE:
			//dprintf("Ring>>>ring set tune. \n");
			if(RingGetState() == RING_STATE_PLAY)
			{
				RingCtrl_Stop(0);
			}
			Ring_ConfigTryTuneScene(msgRing->subMsg);
			RingCtrl_Start();
			break;

		case RING_SET_VOLUME:
			//dprintf("Ring>>>ring set volume. \n");
			if	(RingGetState() == RING_STATE_PLAY)
			{
				Ring_SetVolume(msgRing->subMsg);
			}
			else
			{
				Ring_ConfigTryVolumeScene(msgRing->subMsg);
				RingCtrl_Start();
			}
			break;

		//lyx_20170921_s	
	    	case EXT_RING:
		        if( (msgRing->subMsg == EXT_RING_OFF) || (msgRing->subMsg == EXT_RING_ON) )
		        {
		            	ExitRingCtrl(msgRing->subMsg);
		        }
		        else if( msgRing->subMsg == EXT_RING_TIMER_EVENT )
		        {
		            	ExtRingTimerEventHandler();
		        }
		        break;
		//lyx_20170921_e	
		case RING_TIME_CYCLE:
			if(RingGetState() == RING_STATE_PLAY)
			{
				Ring_Time_Cycle();
			}
			break;
		
		default:
			break;
	}
}

/**
  * @brief  ������������
  * @param  scene It should be one of following:
 *           @ RING_SCENE_0,
 *           @ RING_SCENE_1,
 *           @ RING_SCENE_2,
 *           @ RING_SCENE_3,
 *           @ RING_SCENE_4,
 *           @ RING_SCENE_5,
 *           @ RING_SCENE_6,
  * @retval None
  */
void API_RingPlay(Ring_Scene_Type scene)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_PLAY;
	msgRing.subMsg				= scene;
	// ѹ�뱾�ض���
	if (push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT)) != 0)
	{
		printf("-----------API_RingPlay:push_vdp_common_queue just full-----------\n");		
	}

}

/**
  * @brief  ����ֹͣ����
  * @param  None
  * @retval None
  */
void API_RingStop(void)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_STOP;
	msgRing.subMsg				= 0;

	// ѹ�뱾�ض���
	if (push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT)) != 0)
	{
		printf("-----------API_RingStop:push_vdp_common_queue just full-----------\n");		
	}
}

/**
  * @brief  ����ֹͣ����,���ص�Դ
  * @param  None
  * @retval None
  */
void API_RingStopKeepPwr(void)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_STOP_KEEP_PWR;
	msgRing.subMsg				= 0;

	// ѹ�뱾�ض���
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT));
}

/**
  * @brief  ����
  * @param  iTune
  * @retval None
  */
void API_RingTuneSelect(unsigned char iTune)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_SET_TUNE;
	msgRing.subMsg				= iTune;

	// ѹ�뱾�ض���
	if (push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT)) != 0)
	{
		printf("-----------API_RingPlay:push_vdp_common_queue just full-----------\n");		
	}
}

/**
  * @brief  ����
  * @param  iVol
  * @retval None
  */
void API_RingVolSelect(unsigned char iVol)
{
	RING_STRUCT msgRing;

 	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;
	msgRing.cmd					= RING_SET_VOLUME;
	msgRing.subMsg				= iVol;

	// ѹ�뱾�ض���
	if (push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT)) != 0)
	{
		printf("-----------API_RingPlay:push_vdp_common_queue just full-----------\n");		
	}
}


//lyx_20170921_s
void API_ExtRingCtrl(ExtRing_Type msg)
{
	RING_STRUCT msgRing;

	msgRing.head.msg_source_id 	= 0;
	msgRing.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= 0;

	msgRing.cmd = EXT_RING;
	msgRing.subMsg = msg;
	
	// ѹ�뱾�ض���
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgRing, sizeof(RING_STRUCT));
}
//lyx_20170921_e

