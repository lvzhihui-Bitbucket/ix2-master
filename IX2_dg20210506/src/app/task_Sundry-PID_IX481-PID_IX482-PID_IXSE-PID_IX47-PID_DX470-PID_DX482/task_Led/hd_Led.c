/**
  ******************************************************************************
  * @file    hd_Led.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the hd_Led
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "task_Hal.h"
#include "hd_Led.h"
#include "../../task_survey/task_survey.h"
#include "../../hal_gpio_def.h"
#include "../../vdp_uart.h"


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern int hal_fd;

void hd_LedInit(void)
{
	
}


/***********************************************************************
 * @fn      hdLedTalkCtrl()
 *
 * @brief   ledTransfer control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedRedCtrl(unsigned char iOnOff)
{
	//int power;
	unsigned char buff[2];
#ifdef PID_IXSE
	if( !iOnOff )
	{
		RGB_R_TURN_SET()
	}
	else
	{
		RGB_R_TURN_RESET()
	}
#else
	if( !iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_RED_CTL, &power );

		buff[0] = MSG_LED_ON;
		buff[1] = 0x01;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);

	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_RED_CTL, &power );

		buff[0] = MSG_LED_OFF;
		buff[1] = 0x01;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);

	}
#endif	
}

/***********************************************************************
 * @fn      hdLedUnlockCtrl()
 *
 * @brief   ledAlarm control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedGreenCtrl(unsigned char iOnOff)
{
	unsigned char buff[2];
	//int power;
#ifdef PID_IXSE
	if( !iOnOff )
	{
		RGB_G_TURN_SET()
	}
	else
	{
		RGB_G_TURN_RESET()
	}
#else	
	if( !iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_GREEN_CTL, &power );

		buff[0] = MSG_LED_ON;
		buff[1] = 0x02;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);

	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_GREEN_CTL, &power );

		buff[0] = MSG_LED_OFF;
		buff[1] = 0x02;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
#endif	
}


/***********************************************************************
 * @fn      hdLedPowerCtrl()
 *
 * @brief   ledPower control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedBlueCtrl(unsigned char iOnOff)
{
	unsigned char buff[2];
	//int power;
#ifdef PID_IXSE
	if( !iOnOff )
	{
		RGB_B_TURN_SET()
	}
	else
	{
		RGB_B_TURN_RESET()
	}
#else	
	if( !iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_BLUE_CTL, &power );

		buff[0] = MSG_LED_ON;
		buff[1] = 0x04;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);

	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_BLUE_CTL, &power );

		buff[0] = MSG_LED_OFF;
		buff[1] = 0x04;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
#endif	
}


/***********************************************************************
 * @fn      hdLedMenuCtrl()
 *
 * @brief   ledMenu control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedMenuCtrl(unsigned char iOnOff)
{
	//int power;
	uint8 buff[2];
	
	if( iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_MENU_SET, &power );
		buff[0] = MSG_LED_ON;
		buff[1] = LED_MENU;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_MENU_SET, &power );
		buff[0] = MSG_LED_OFF;
		buff[1] = LED_MENU;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}


/***********************************************************************
 * @fn      hdLedUpCtrl()
 *
 * @brief   ledUp control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedUpCtrl(unsigned char iOnOff)
{
	//int power;
	uint8 buff[2];
	
	if( iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_UP_SET, &power );
		buff[0] = MSG_LED_ON;
		buff[1] = LED_UP;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_UP_SET, &power );
		buff[0] = MSG_LED_OFF;
		buff[1] = LED_UP;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}


/***********************************************************************
 * @fn      hdLedDownCtrl()
 *
 * @brief   ledDown control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedDownCtrl(unsigned char iOnOff)
{
	//int power;
	uint8 buff[2];
	
	if( iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_DOWN_SET, &power );
		buff[0] = MSG_LED_ON;
		buff[1] = LED_DOWN;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_DOWN_SET, &power );
		buff[0] = MSG_LED_OFF;
		buff[1] = LED_DOWN;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}


/***********************************************************************
 * @fn      hdLedBackCtrl()
 *
 * @brief   ledBack control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedBackCtrl(unsigned char iOnOff)
{
	//int power;
	uint8 buff[2];
	
	if( iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_BACK_SET, &power );
		buff[0] = MSG_LED_ON;
		buff[1] = LED_BACK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_BACK_SET, &power );
		buff[0] = MSG_LED_OFF;
		buff[1] = LED_BACK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}

/***********************************************************************
 * @fn      hdLedNoDisturbCtrl()
 *
 * @brief   ledNoDisturb control
 *
 * @param   iOnOff  - 1/on 0/off
 *
 * @return  none
 ************************************************************************/
void hdLedNoDisturbCtrl(unsigned char iOnOff)
{
	//int power;
	uint8 buff[2];
	
	if( iOnOff )
	{
		//power = 1;
		//ioctl( hal_fd, LED_NO_DISTURB_SET, &power );
		buff[0] = MSG_LED_ON;
		buff[1] = LED_DISTURB;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else
	{
		//power = 0;
		//ioctl( hal_fd, LED_NO_DISTURB_SET, &power );
		buff[0] = MSG_LED_OFF;
		buff[1] = LED_DISTURB;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}



void hdLedTalkFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];

	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_TALK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_TALK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
		
}



void hdLedUnlockFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];
	
	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_UNLOCK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_UNLOCK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}

}


void hdLedPowerFlashCtrl(unsigned char ledCtrlType)
{

	uint8 buff[2];
	
	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_POWER;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_POWER;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_IRREGULAR)
	{
		printf("------LED_FLASH_IRREGULAR------\r\n");
		buff[0] = MSG_LED_FLASH_IRREGULAR;
		buff[1] = LED_POWER;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}

}


void hdLedMenuFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];
	
	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_MENU;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_MENU;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}

}



void hdLedUpFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];

	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_UP;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_UP;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}


void hdLedDownFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];

	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_DOWN;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_DOWN;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}

}

void hdLedBackFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];

	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_BACK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_BACK;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
}


void hdLedNoDisturbFlashCtrl(unsigned char ledCtrlType)
{
	uint8 buff[2];

	if(ledCtrlType == LED_FLASH_FAST)
	{
		buff[0] = MSG_LED_FLASH_FAST;
		buff[1] = LED_DISTURB;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}
	else if(ledCtrlType == LED_FLASH_SLOW)
	{

		buff[0] = MSG_LED_FLASH_SLOW;
		buff[1] = LED_DISTURB;
		api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
	}

}



