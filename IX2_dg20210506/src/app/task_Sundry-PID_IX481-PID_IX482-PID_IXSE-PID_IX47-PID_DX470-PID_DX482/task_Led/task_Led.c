/**
  ******************************************************************************
  * @file    task_Led.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the task_Led
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "task_Sundry.h"
#include "obj_Led.h"
#include "task_Led.h"

/******************************************************************************
 * @fn      vtk_TaskInit_Led()
 *
 * @brief   Task Led initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Led(void)
{
	LedInit();
}

/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Led()
 *
 * @brief   Task Led loop routine
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Led(MsgLed *msgLed)
{
 	LedMessageProcessing( msgLed->msgLedType, msgLed->ledSelect ,msgLed->time);
}

/******************************************************************************
 * @fn      API_LedControl()
 *
 * @brief   Provide an application interface for other task to control led
 *
 * @param   msgLedType  should be:
 *          1) LED_CTRL_TYPE_OFF
 *          2) LED_CTRL_TYPE_ON
 *          3) LED_CTRL_TYPE_FAST_FLASH
 *          4) LED_CTRL_TYPE_DOUBLE_FLASH
 *          
 * @param   ledSelect - should be:
 *          1)LED_SELECT_TAKL,
 *          2)LED_SELECT_UNLOCK,
 *          3)LED_SELECT_ALL,
 *
 * @param   iTime -   led timing
 *
 * @return  1 - false, 0 - ok
 *****************************************************************************/
unsigned char API_LedControl(uint8 msgLedType, uint8 ledSelect, uint16 time)
{
	MsgLed buf;

	// push in buf
	buf.head.msg_source_id 	= 0;
	buf.head.msg_target_id 	= MSG_ID_SUNDRY;
	buf.head.msg_type 		= MSG_TYPE_LED;
	buf.head.msg_sub_type 	= 0;
	buf.msgLedType = msgLedType;
	buf.ledSelect = ledSelect;
	buf.time = time;
	
	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue,  &buf, sizeof(MsgLed));
	
    return (0);
}


