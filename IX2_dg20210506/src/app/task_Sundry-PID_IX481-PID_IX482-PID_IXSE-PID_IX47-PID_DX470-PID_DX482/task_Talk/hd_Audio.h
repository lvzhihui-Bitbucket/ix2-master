/**
  ******************************************************************************
  * @file    hd_Audio.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the hd_Audio.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef __HD_AUDIO_H
#define __HD_AUDIO_H


#define MUTE_OFF    0
#define MUTE_ON     1

#define NJW1124_IDLE	0
#define NJW1124_TALK	1



void HD_Audio_Init(void);
void MICMuteCtrl(uint8 mic_mute_contorl);
void SPKMuteCtrl(uint8 spk_mute_contorl);
void NJW1124Direct(uint8 status_1124);

#endif

