/**
  ******************************************************************************
  * @file    obj_PT2259.h
  * @author  zhangxijun
  * @version V1.0.0
  * @date    2013.03.19
  * @brief   This file contains the headers of the obj_PT2259
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef OBJ_PT2259_H
#define OBJ_PT2259_H

/*******************************************************************************
                         Define object property
*******************************************************************************/
//#define SDA_0_PT2259()		{GPIO_ResetBits(SDA_GPIO_PORT, SDA_GPIO_PIN);i_delay_pt2259();}
//#define SDA_1_PT2259()		{GPIO_SetBits(SDA_GPIO_PORT, SDA_GPIO_PIN);i_delay_pt2259();}
//#define SCL_0_PT2259()		{GPIO_ResetBits(SCL_GPIO_PORT, SCL_GPIO_PIN);i_delay_pt2259();}
//#define SCL_1_PT2259()		{GPIO_SetBits(SCL_GPIO_PORT, SCL_GPIO_PIN);i_delay_pt2259();}
//#define READ_SDA_PT2259		(GPIO_ReadInputDataBit(SDA_GPIO_PORT, SDA_GPIO_PIN)?1:0)

#define PT2259_WR			0x88

#define REG_2CH_1DB			0xd0		//bit3.2.1.0(16 step)
#define REG_2CH_10DB		0xe0		//bit2.1.0(8 step)
#define REG_LCH_1DB			0xa0		//bit3.2.1.0(16 step)
#define REG_LCH_10DB		0xb0		//bit2.1.0(8 step)
#define REG_RCH_1DB			0x20		//bit3.2.1.0(16 step)
#define REG_RCH_10DB		0x30		//bit2.1.0 set(8step)
#define REG_CLEAR_ALL		0xf0
#define REG_MUTE_SEL		0x74		//bit1.0 set
#define NO_MUTE				0
#define RCH_MUTE			1
#define LCH_MUTE			2
#define ALL_MUTE			3
//
#define MIN_ATTENUATION		0
#define MAX_ATTENUATION		79

//PT2259 channel defing
#define CHANNEL_MIC			0
#define CHANNEL_SPK			1
#define CHANNEL_ALL			2


/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
uint8 ConvertSpkVolumnData(uint8 spk_volume_select);
uint8 ConvertMicVolumnData(uint8 mic_volume_select);
void PT2259_Reset(void);
void PT2259_Set(uint8 channel_select, uint8 db_pt2259);

/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/


#endif


