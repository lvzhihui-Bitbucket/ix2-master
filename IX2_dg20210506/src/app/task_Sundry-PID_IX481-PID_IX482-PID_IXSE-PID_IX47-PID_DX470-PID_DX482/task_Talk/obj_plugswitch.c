
#include "alsa.h"
#include "obj_plugswitch.h"
#include "IP_Audio_Capture.h"

#include "vdp_uart.h"
#include "task_VideoMenu.h"
#include "task_Power.h"

extern int api_uart_send_pack( unsigned char cmd, char* pbuf, unsigned int len );

static int g_plugswitch_control = G_PLUGSWITCH_CONTROL_IDLE;
static int g_plugswitch_level = 0;		// 手柄状态检测，0/插簧提起-LINEIN，1/插簧挂起-MIC

static int g_handle_power = 0;			// 手柄电源控制，0/关电，1/开电
static int g_handle_mute = 0;			// 手柄静音控制，0/通话，1/静音
static int g_ampspk_mute = 0;
static int g_handle_auin = 0;			// 手柄MIC输入控制，0/auin to 免提，1/auin to手柄

void g_handle_audioin_ctrl(int on)
{
	char temp[2];
	temp[0] = on;
	api_uart_send_pack(UART_TYPE_AUIN_SWITCH_REQ, temp, 1);
	g_handle_auin = on;
	dprintf("g_handle_audioin_ctrl =%d \n", g_handle_auin);
}

void g_handle_power_ctrl(int on)
{
	char temp[2];
	temp[0] = on;
	api_uart_send_pack(UART_TYPE_LINEIN_POWER_SET_REQ, temp, 1);
	g_handle_power = on;
}

void g_handle_mute_ctrl(int on)
{
#if 1
	char temp[2];
	if(api_get_stm8_mode() ==MODE_HAND_PLUG)
	{
		temp[0] = (on<<2) | 0x40;
		api_uart_send_pack(UART_TYPE_WRITE_LINE, temp, 1);	
	}
	g_handle_mute = on;
#endif	
}

void g_ampspk_mute_ctrl(int on)
{
	g_ampspk_mute = on;
	if( on )		
		API_POWER_TALK_ON();
	else
		API_POWER_TALK_OFF();
}

// 得到当前插簧状态
int api_get_plugswitch_status(void)
{
	return g_plugswitch_level;
}


// 回复更新当前插簧状态，主要用于申请当前stm8的插簧状态
//extern void LineInControl(unsigned int linein);
int api_plugswitch_status_update(unsigned int linein)
{
	g_plugswitch_level = linein ^ 0x01;
	// direct set kernal status by ioctl,NOT set it with alsa params 
//	LineInControl(linein);
	
	dprintf("api_plugswitch_status_update =%d \n", g_plugswitch_level);
	return g_plugswitch_level;
}

// 插簧状态改变登记机广播通知
int api_plugswitch_status_change( unsigned int linein)
{
	// 更新插簧状态
	g_plugswitch_level = linein ^ 0x01;
	// direct set kernal status by ioctl,NOT set it with alsa params 
//	LineInControl(linein);

	dprintf("api_plugswitch_status_change =%d \n", g_plugswitch_level);
	// 发送广播消息
	if( g_plugswitch_level )
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP);		
	}
	else
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN);
	}
}

// 插簧业务控制：业务调用来刷新音频的通道切换
// status: 0/idle,1/mianti,2/handle
int	api_plugswitch_control_update( int status, int au_update )
{
	g_plugswitch_control = status;
	// 状态控制刷新
	switch( g_plugswitch_control )
	{
		case G_PLUGSWITCH_CONTROL_IDLE:
			g_handle_power_ctrl(0);
			g_handle_mute_ctrl(0);
			g_ampspk_mute_ctrl(0);
			g_handle_audioin_ctrl(0);
			break;
		case G_PLUGSWITCH_CONTROL_MIANTI:
			g_handle_power_ctrl(0);
			g_handle_mute_ctrl(1);
			g_ampspk_mute_ctrl(1);
			g_handle_audioin_ctrl(0);
			break;
		case G_PLUGSWITCH_CONTROL_HANDLE:
			g_handle_audioin_ctrl(1);
			g_handle_power_ctrl(1);
			g_handle_mute_ctrl(0);
			g_ampspk_mute_ctrl(0);
			break;
		case G_PLUGSWITCH_CONTROL_RING:
			g_handle_power_ctrl(1);	// handle power on
			g_handle_mute_ctrl(0);	// handle mic mute
			g_ampspk_mute_ctrl(0);	// handle spk open
			g_handle_audioin_ctrl(0);
			break;
	}
	
}



