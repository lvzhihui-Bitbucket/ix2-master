
#ifndef _OBJ_PLUGSWITCH_H_
#define _OBJ_PLUGSWITCH_H_

#define G_PLUGSWITCH_CONTROL_IDLE		0		// 待机状态
#define G_PLUGSWITCH_CONTROL_MIANTI		1		// 免提状态
#define G_PLUGSWITCH_CONTROL_HANDLE		2		// 手柄状态
//lzh_20200113_s
#define G_PLUGSWITCH_CONTROL_RING		3		// 手柄振铃状态
//lzh_20200113_e

// 得到当前插簧状态
int api_get_plugswitch_status(void);

// 申请更新当前插簧状态，主要用于申请当前stm8的插簧状态，调用后业务延时一会，等待状态刷新
int api_plugswitch_status_apply(void);

// 回复更新当前插簧状态，主要用于申请当前stm8的插簧状态
int api_plugswitch_status_update(unsigned int linein);

// 插簧业务控制：业务调用来刷新音频的通道切换
int	api_plugswitch_control_update( int status, int au_update );

#endif



