/**
  ******************************************************************************
  * @file    task_Beeper.c
  * @author  WGC
  * @version V1.0.0
  * @date    2012.10.25
  * @brief   This file contains the functions of task_Beeper
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "hd_Beeper.h"
#include "task_Beeper.h"
#include "task_Sundry.h"
#include "task_survey.h"


timer_t timer;
struct sigevent evp;
struct itimerspec it;

OS_TIMER timer_beep_control;


/*------------------------------------------------------------------------
						BEEP????(???)
------------------------------------------------------------------------*/
void Timer_Beep_Control( void )
{
   	BEEP_STRUCT msgBeep;
	
	beep_run.count++;

	if(beep_run.state != BEEP_ON && beep_run.type == BEEP_TYPE_PHONECALL)		//czn_20170228
	{
		Beep_Off();
		return;
	}

	//BEEP_GAP => BEEP_SOUND
	if( beep_run.stage == BEEP_GAP )
	{
		beep_run.stage = BEEP_SOUND;
		OS_SetTimerPeriod( &timer_beep_control, (*(ptrBeepRomTab[beep_run.type] + beep_run.count)) );
		OS_RetriggerTimer( &timer_beep_control );		
		Sound_Enable();
	}
	else
	{
		if (beep_run.count < *ptrBeepRomTab[beep_run.type])
		{
			beep_run.stage = BEEP_GAP;			
			OS_SetTimerPeriod( &timer_beep_control, (*(ptrBeepRomTab[beep_run.type] + beep_run.count)) );
			OS_RetriggerTimer( &timer_beep_control );			
			Sound_Disable();
		}
		else if(beep_run.type == BEEP_TYPE_PHONECALL)		//czn_20170228
		{
			beep_run.stage = BEEP_GAP;
			beep_run.count = 0;
			OS_SetTimerPeriod( &timer_beep_control,2000/25 );
			OS_RetriggerTimer( &timer_beep_control );
			Sound_Disable();
		}
		else
		{
			Beep_Off();		// should be first called - 不能被打断
		}
	}	
}

/*------------------------------------------------------------------------
						task_init
------------------------------------------------------------------------*/
void vtk_TaskInit_Beep( void )
{	
	Beep_Init();
	
	pthread_mutex_init (&beep_run.lock, NULL);
	beep_run.state = BEEP_OFF;
	beep_run.task = NULL;
	
	OS_CreateTimer( &timer_beep_control, Timer_Beep_Control, 1000);
	
	dprintf("Beep >>>vtk_TaskInit_Beep.\n");
}

/*------------------------------------------------------------------------
						task_process
------------------------------------------------------------------------*/
void vtk_TaskProcessEvent_Beep( BEEP_STRUCT *msgBeep )
{
	switch ( msgBeep->beeptype )
	{
		case BEEP_TYPE_DI1:
		case BEEP_TYPE_DI2:
		case BEEP_TYPE_DI3:
		case BEEP_TYPE_DI4:
		case BEEP_TYPE_DI5:
		case BEEP_TYPE_DI8:
		case BEEP_TYPE_DL1:
		case BEEP_TYPE_DL2:
		case BEEP_TYPE_DL1DI1:
		case BEEP_TYPE_DI1DL1:
		case BEEP_TYPE_PHONECALL:	//czn_20180228
				Beep_On(msgBeep->beeptype);
		    	break;

		case TIMER_BEEP_STOP:
		    	Beep_Off();
		    	break;

		default:
		    	break;
	}
}

/*------------------------------------------------------------------------
							API_Beep
??:
		Beep_type:   	BEEP_KEY / BEEP_CONFIRM / BEEP_ERROR


??:
		BEEP

??: ?
------------------------------------------------------------------------*/
void API_Beep( uint8 beep_type )		//cao_20151118
{
	BEEP_STRUCT msgBeep;

	msgBeep.head.msg_source_id 	= 0;
	msgBeep.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgBeep.head.msg_type 		= MSG_TYPE_BEEPER;
	msgBeep.head.msg_sub_type 	= 0;
	msgBeep.beeptype 			= beep_type;

	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgBeep, sizeof(BEEP_STRUCT));
}

