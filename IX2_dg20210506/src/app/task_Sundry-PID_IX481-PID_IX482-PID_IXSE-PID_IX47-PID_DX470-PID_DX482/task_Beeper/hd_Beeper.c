/**
  ******************************************************************************
  * @file    hd_beeper.c
  * @author  WGC
  * @version V1.0.0
  * @date    2012.10.25
  * @brief   This file contains the functions of hd_beeper
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
//#include "task_survey.h"
#include "task_Beeper.h"
#include "hd_Beeper.h"
#include "task_Hal.h"
#include "task_survey.h"
#include "vdp_uart.h"

#include "MENU_public.h"
#include "task_Power.h"

//#define BPT	5
#define BPT		2	//BEEP timer base just 25ms

//BEEP?????
const uint8 DI1[2]		= {1,	BPT};					//??50ms
const uint8 DI2[4]		= {3,	BPT,BPT*2,BPT};			//??50ms,??100ms,??50ms
const uint8 DI3[6]		= {5,	BPT,BPT*2,BPT,BPT*2,BPT};		//??50ms,??100ms,??50ms,??100ms,??50ms
const uint8 DI4[8]		= {7,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};	//??50ms,??100ms,??50ms,??100ms,??50ms,??100ms,??50ms
const uint8 DI5[10]		= {9,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};					//??50ms,??100ms......??50ms,??100ms,??50ms
const uint8 DI8[16]		= {15,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};	//??50ms,??100ms......??50ms,??100ms,??50ms
const uint8 DL1[2]		= {1,	BPT*10};				//??500ms
const uint8 DL2[4]		= {3,	BPT*10,	BPT*2,	BPT*10};		//??500ms,??100ms,??500ms
const uint8 DL1DI1[4]	= {3,	BPT*10,	BPT*2,	BPT};			//??500ms,??100ms,??50ms
const uint8 DI1DL1[4]	= {3,	BPT*2,	BPT*2,	BPT*10};		//??50ms,??100ms,??500ms
const uint8 PHONECALL[4]	= {3,	BPT*40,	BPT*2,	BPT*40};

const uint8* const ptrBeepRomTab[] =
{
	(const uint8*)DI1,
	(const uint8*)DI2,
	(const uint8*)DI3,
	(const uint8*)DI4,
	(const uint8*)DI5,
	(const uint8*)DI8,
	(const uint8*)DL1,
	(const uint8*)DL2,
	(const uint8*)DL1DI1,
	(const uint8*)DI1DL1,
	(const uint8*)PHONECALL,
};

BEEP_RUN beep_run;
extern int hal_fd;
/*------------------------------------------------------------------------
						Beep_init
------------------------------------------------------------------------*/
void Beep_Init( void )
{
	char cmd[100];
	strcpy(cmd,"echo set 330 165 > /dev/buzzer ");
	system(cmd);
	sync();
	//Sound_Disable();
}

/*------------------------------------------------------------------------
						Sound_Enable
------------------------------------------------------------------------*/
void Sound_Enable( void )
{
	char cmd[100];
	strcpy(cmd,"echo start > /dev/buzzer ");
	system(cmd);
	sync();
	//printf("Beep  >>>beep_run.state = %d.\n", pwn_on );
	#if 0
	if( get_pane_type() == IX482 || get_pane_type() == IX47 || get_pane_type() == IX470V )
	{
		char cmd[100];
		strcpy(cmd,"echo start > /dev/buzzer ");
		system(cmd);
		sync();
	}
	else
	{
		int pwn_on = 1;
		api_uart_send_pack(UART_TYPE_BEEP_C, &pwn_on, 1); 
	}
	#endif
	//ioctl( hal_fd, PWN_OUT_SET, &pwn_on );
}

/*------------------------------------------------------------------------
						Sound_Off
------------------------------------------------------------------------*/
void Sound_Disable( void )
{
	char cmd[100];
	strcpy(cmd,"echo stop > /dev/buzzer ");
	system(cmd);
	sync();
	//printf("Beep  >>>beep_run.state = %d.\n", pwn_off );
	#if 0
	if( get_pane_type() == IX482 || get_pane_type() == IX47 || get_pane_type() == IX470V )
	{
		char cmd[100];
		strcpy(cmd,"echo stop > /dev/buzzer ");
		system(cmd);
		sync();
	}
	else
	{
		int pwn_off = 0;
		api_uart_send_pack(UART_TYPE_BEEP_C, &pwn_off, 1); 
	}
	#endif
	//ioctl( hal_fd, PWN_OUT_SET, &pwn_off );
}

static void Task_Beep_Control( void )
{
	uint8 	state;
	uint8 	type;
	uint8 	count;
	uint8 	stage;
	int taskFlag;

	pthread_mutex_lock(&beep_run.lock);
	taskFlag = beep_run.taskFlag;
	pthread_mutex_unlock(&beep_run.lock);

	while(taskFlag)
	{
		pthread_mutex_lock(&beep_run.lock);
		state = beep_run.state;
		type = beep_run.type;
		stage = beep_run.stage;
		count = beep_run.count;
		pthread_mutex_unlock(&beep_run.lock);

		if(state == BEEP_ON)
		{

			usleep((*(ptrBeepRomTab[type] + count)) * 20 * 1000);
			
			pthread_mutex_lock(&beep_run.lock);
			count = ++beep_run.count;
			pthread_mutex_unlock(&beep_run.lock);
			
			//BEEP_GAP => BEEP_SOUND
			if(stage == BEEP_GAP )
			{
				pthread_mutex_lock(&beep_run.lock);
				beep_run.stage = BEEP_SOUND;
				Sound_Enable();
				pthread_mutex_unlock(&beep_run.lock);
			}
			else
			{
				if (count < *ptrBeepRomTab[type])
				{
					pthread_mutex_lock(&beep_run.lock);
					beep_run.stage = BEEP_GAP;			
					Sound_Disable();
					pthread_mutex_unlock(&beep_run.lock);
				}
				else if(type == BEEP_TYPE_PHONECALL)		//czn_20170228
				{
					pthread_mutex_lock(&beep_run.lock);
					beep_run.stage = BEEP_GAP;
					beep_run.count = 0;
					Sound_Disable();
					pthread_mutex_unlock(&beep_run.lock);
				}
				else
				{
					Beep_Off();
				}
			}	
		}
		else
		{
			usleep(20 * 1000);
			beep_run.taskFlag--;
		}
	}
	
	pthread_mutex_lock(&beep_run.lock);
	beep_run.task = NULL;
	pthread_mutex_unlock(&beep_run.lock);
}

/*------------------------------------------------------------------------
						??Beep
------------------------------------------------------------------------*/
void Beep_On( uint8 beep_type )
{
	pthread_mutex_lock(&beep_run.lock);
	beep_run.state = BEEP_ON;
	beep_run.type = beep_type;
	beep_run.count = 1;
	beep_run.taskFlag = 5*1000/20;		//5秒之后停止beeper控制线程
	
	beep_run.stage = BEEP_SOUND;
	Sound_Enable();
	//create task
	if(beep_run.task == NULL)
	{
		pthread_create(&beep_run.task, NULL, (void*)Task_Beep_Control,NULL);
	}
	
	if(beep_run.task == NULL)
	{
		OS_SetTimerPeriod( &timer_beep_control, (*(ptrBeepRomTab[beep_run.type] + beep_run.count)) );
		OS_RetriggerTimer( &timer_beep_control );
	}
	pthread_mutex_unlock(&beep_run.lock);

	API_POWER_BEEP_ON();
}

/*------------------------------------------------------------------------
						Beep_Off
------------------------------------------------------------------------*/
void Beep_Off( void )
{	
	API_POWER_BEEP_OFF();
	//AMPMUTE_RESET();

	pthread_mutex_lock(&beep_run.lock);
	if(beep_run.task == NULL)
	{
		OS_StopTimer( &timer_beep_control );
	}
	Sound_Disable();	
	beep_run.state = BEEP_OFF;	
	pthread_mutex_unlock(&beep_run.lock);
}

/*------------------------------------------------------------------------
						Get_BeepState
------------------------------------------------------------------------*/
uint8 Get_BeepState( void )
{
	return (beep_run.state);
}


