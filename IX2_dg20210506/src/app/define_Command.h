/**
  ******************************************************************************
  * @file    define_Command.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _define_Command_H
#define _define_Command_H


/*========================================================================
						与指令相关定义.read
========================================================================*/
//留影模式
#define	MEMO_AUTO				0
#define MEMO_MANUAL				1

//广播地址
#define	BROADCAST_ADDRESS		0x38

//第一个主机地址
#define	DS1_ADDRESS				0x34
//第二个主机地址
#define	DS2_ADDRESS				0x35
//第三个主机地址
#define	DS3_ADDRESS				0x36
//第四个主机地址
#define	DS4_ADDRESS				0x37

#define	IPG_ADDRESS				0x2C

//本地中心地址
#define	LOCAL_GL_ADDRESS		0x3C

//组群(0~15)留影地址
#define MEMO_GROUP_0_15_ADDRESS		0x50
#define MEMO_GROUP_16_31_ADDRESS	0x54

//主机模式
#define DS_SYSTEM				0			//系统型
#define DS_SINGLE				1			//单户型

//指令模式
#define MODE_OLD_COM			0			//旧指令
#define MODE_NEW_COM			1			//新指令

//呼叫类型代码
#define	CT01_GL_CALL_ST			1		//管理中心呼叫分机
#define	CT02_GL_CALL_DS			2		//管理中心呼叫单元主机
#define	CT03_GL_CALL_CDS		3		//管理中心呼叫小区主机
#define	CT04_GL_MONITOR_DS		4		//管理中心监视单元主机
#define	CT05_GL_MONITOR_CDS		5		//管理中心监视小区主机

#define CT06_CDS_CALL_ST		6		//小区主机呼叫分机
#define CT07_CDS_CALL_GL		7		//小区主机呼叫管理中心

#define CT08_DS_CALL_ST			8		//单元主机呼叫分机
#define CT09_DS_CALL_GL			9		//单元主机呼叫管理中心

#define CT10_ST_CALL_GL			10		//分机呼叫管理中心
#define	CT11_ST_MONITOR			11		//分机监视
#define	CT12_ST_MEM_PLAY		12		//分机查询留影(公共留影)
#define CT13_INTERCOM_CALL		13		//户户通话
#define	CT14_INNER_CALL			14		//户内通话
#define CT15_INNER_BRD          15      //户内广播通话
#define CT16_INTERCOM_BRD       16      //户间广播通话

//task_event
#define	RESPONSE_OK			(1<<1)
#define	RESPONSE_ERR		(1<<0)


/*========================================================================
							指令定义.read
========================================================================*/
#define E_VOLTAGE				0x30
#define E_MODEL_EDITION			0x31
#define NAMELIST_APPOINT		0x32
#define E_COLLIGATE				0x33

#define TPC_SET_TEL_NBR			0x34		//S->TPC	long cmd
#define TPC_GET_TEL_NBR			0x35		//S->TPC	long cmd
#define TPC_DAT_TEL_NBR			0x36		//TPC->S	long cmd
#define TPC_DIVERT_START		0x37		//S->M		long cmd
#define TPC_MSG_DEBUG			0x38		//TPC-S		long cmd

#define NAMELIST_BROAD			0x40
#define MON_TIMER				0x41
#define CAMERA_SET				0x42
#define CLOCK_SET				0x43
#define SET_SERIAL_NUM          0x44 // 20151126

#define STATE_RESET				0x50		
#define DEVICE_CALL_GL_ON		0x51
#define DEVICE_CALL_GL_OFF		0x52
#define GL_CALL_DEVICE_ON		0x53
#define GL_CALL_DEVICE_OFF		0x54
#define MR_CALL_ST_ON			0x55		//主机呼叫分机状态通报
#define MR_CALL_ST_OFF			0x56		//主机呼叫分机状态通报
#define ST_MON_ON				0x57		//分机监视状态通报
#define ST_MON_OFF				0x58		//分机监视状态通报
#define ST_CALL_ST_ON			0x59		//户户通话状态通报
#define ST_CALL_ST_OFF			0x5a		//户户通话状态通报
#define INTERCOM_ON				0x5b		//户内通话状态通报
#define INTERCOM_OFF			0x5c		//户内通话状态通报
#define NAMELIST_ON				0x5d		//NameList数据群发开始
#define NAMELIST_OFF			0x5e		//NameList数据群发结束	
#define LAOHUA_ON1				0x5f
#define LAOHUA_ON2				0x60
#define LAOHUA_ON3				0x61
#define MR_CALL_ST_TALK			0x62		//主机呼叫分机进入通话状态通报
#define LAOHUA_OFF				0x63
#define AE_RESTART				0x64		//强制复位

#define MAIN_CALL				0x70		//主机呼叫分机
#define S_CANCEL				0x71		//主机取消呼叫分机
#define MR_LINKING_ST			0x72		//主机检测分机是否在线
#define ASK_VOLTAGE_TEST		0x73		
#define ASK_VOLTAGE				0x74
#define ASK_MODEL_EDITION		0x75
#define RECEIPT					0x76		//主机的应答指令(系统型主机)
#define MOVE_MON				0x77
#define MAIN_CALL_TEST			0x78
#define ASK_COLLIGATE			0x79
#define ASK_VOLTAGE_STATE		0x7a
#define MAIN_CALL_GROUP			0x7b		//单户型主机呼叫组群从分机
#define MAIN_CANCEL_GROUP		0x7c		//单户型主机发命令,使未摘机之分机退出呼叫
#define MAIN_CALL_ONCE_L		0x7d		//单户型主机呼叫组群主分机
#define RECEIPT_SINGLE			0x7e		//主机的应答指令(单户型主机)

#define ST_TALK					0x80
#define ST_UNLOCK				0x81
#define ST_CLOSE				0x82
#define SUB_RECEIPT				0x83		//分机的应答指令
#define STATE_DISVISIT			0x84		
#define ST_LINK_MR				0x85		//分机检测主机是否在线
#define MON_ON					0x86
#define MON_OFF					0x87
#define MON_ON_APPOINT			0x88
#define MON_OFF_APPOINT			0x89
#define VOLTAGE_OK				0x8a
#define VOLTAGE_ERROR			0x8b
#define LET_MR_CALLME			0x8c		//使主机呼叫本设备
#define ST_UNLOCK_SECOND		0x8d		
#define ST_ASK_NAMELIST			0x8e		//使主机发送NameList数据

#define INNER_CALL				0x90		//启动户内通话
#define INNER_TALK				0x91		//户内通话_被叫摘机
#define INNER_CLOSE				0x92		//户内通话_挂机

#define INTERCOM_CALL			0x98		//启动户户通话
#define INTERCOM_TALK			0x99		//户户通话_被叫摘机
#define INTERCOM_CLOSE			0x9a		//户户通话_挂机

#define CALL_TRANSFER_ON		0xA0		//S->S: 
#define CALL_TRANSFER_TALK		0xA1		//S->S: 

#define LET_DIDONG_ONCE			0xa8		//让分机再振铃一次	
#define LET_WORK				0xA9		//让分机进入监视状态(不开音频)
#define LET_TALKING				0xAA		//让分机进入通话状态(相当于摘机动作)


//zxj_20130405	//如果确认使用GO, 则以下3条定义无用
#define PLAYBACK_LAST			0xB0
#define PLAYBACK_NEXT			0xB1
#define PLAYBACK_DELETE			0xB2


#define A_PropertyValue_Read		0xe0
#define A_PropertyValue_Write		0xe1
#define A_PropertyValue_Response	0xe2

#define SUB_MODEL				0xf0
#define SUB_MODEL_1				0xf1
#define FUTE_AL_MSG				0xf2
#define FUTD_AL_MSG				0xf3
#define FUTE_AL					0xf4
#define FUTE_MSG				0xf5


#define TPC_IM_KEEP_CALLING_STATE	0xF6    //TPC-M  short cmd
#define GP_LINKING					0xF8	//S->TPC	short cmd
#define GP_RECEIPT					0xF9	//TPC->S	short cmd








#endif
