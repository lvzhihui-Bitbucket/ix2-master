
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>

#include "task_CallServer.h"
#include "obj_IPCTableSetting.h"
#include "vtk_media_tcp_transfer.h"
#include "obj_APP_IPC_List.h"
#include "elog.h"
#include "task_DXMonitor.h"

int Api_media_transfer_cennect_req2(char *ip_str,char *acc,char *pwd,char *related_acc, short *pauport, short *pvdport)
{
	int  err, num;
	int media_trans_socket_fd;

	COMMU_PACK_ACCOUNT_REQ2 send_pkt;
	COMMU_PACK_ACCOUNT_RSP2 recv_pkt;
	int recv_len;
	//MyRand();
	srand(time(NULL));
	
	memset(&send_pkt,0,sizeof(COMMU_PACK_ACCOUNT_REQ2));
	memset(&recv_pkt,0,sizeof(COMMU_PACK_ACCOUNT_RSP2));
	
	send_pkt.head.cmd_type = DEV_ACCOUNT_REQ2;
	send_pkt.head.dev_type = ACCOUNT_TYPE_MONITOR;
	strcpy(send_pkt.account,acc);
	strcpy(send_pkt.password,pwd);
	strcpy(send_pkt.related_account,related_acc);
	// connect
	int retry_cnt = 4;
	//err = connect_with_timeout( ip_str, VTK_MEDIA_TRANSFER_PORT, 3, &media_trans_socket_fd);
	printf(">>>>Api_media_transfer_cennect_req2>>>>>>>%s>>>>>\n",ip_str);
	char* ptr = strstr(ip_str,":");
	if( ptr != NULL ) ptr[0] = '\0';
	while(retry_cnt > 0 && (err = connect_with_timeout( ip_str, VTK_MEDIA_TRANSFER_PORT, 5, &media_trans_socket_fd))!=0)		//czn_20190827
	{
		usleep(1000*1000);
		retry_cnt --;
	}
	
	if(err == 0)
	{
		int setflag = 1;
		//setsockopt(media_trans_socket_fd, IPPROTO_TCP, TCP_NODELAY, &setflag, sizeof(int));
		int flags = fcntl(media_trans_socket_fd, F_GETFL, 0);
		if (flags < 0) 
		{
			printf("Get flags error:%s\n", strerror(errno));
			close(media_trans_socket_fd);
			return -1;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(media_trans_socket_fd, F_SETFL, flags) < 0) 
		{
			printf("Set flags error:%s\n", strerror(errno));
			close(media_trans_socket_fd);
			return -1;
		}
		
 		struct timeval tv_out;
	    	tv_out.tv_sec = 3;
	    	tv_out.tv_usec = 0;
	    	setsockopt(media_trans_socket_fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		
		printf("connect ok! [server:%s]\n",ip_str);
	
		// send
		#if 1
		struct timeval tv;
		fd_set fds;
		int ret;
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;	// 100ms
		if((ret = select( media_trans_socket_fd + 1, NULL, &fds, NULL, &tv ))>0)
		{
			//num = write(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ));
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = send(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ2),MSG_DONTWAIT);
			else
			{
				close(media_trans_socket_fd);
				return 0;
			}
		}
		else
		{
			close(media_trans_socket_fd);
			return 0;
		}
		#else
		num = write(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ));
		#endif

		printf("1-------------------------------------------\n");
		#if 0
		if (num <= 0) {
			printf("send error\n");
			return 2;
		}
		#endif
		// recv
		recv_len = sizeof(COMMU_PACK_ACCOUNT_RSP2);
		#if 1
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 6;
		tv.tv_usec = 0;
		if((ret = select( media_trans_socket_fd + 1, &fds,NULL, NULL, &tv ))>0)	
		{
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = recv(media_trans_socket_fd, (char*)&recv_pkt, recv_len, MSG_DONTWAIT);
			else
			{
				close(media_trans_socket_fd);
				return 0;
			}
			
		}
		else
		{
			close(media_trans_socket_fd);
			return 0;
		}
		#else
		num = recv(media_trans_socket_fd, (char*)&recv_pkt, recv_len, 0);
		#endif

		printf("2-------------------------------------------\n");

		if (num > 0) // success
		{
			//*precv_len = num;
			//return_value = 0;
			printf("send and recv over\n");	
			if(recv_pkt.head.cmd_type == DEV_ACCOUNT_RSP2 && recv_pkt.result == 0)
			{
				*pauport = recv_pkt.au_port;
				*pvdport = recv_pkt.vd_port;				
				printf("register to myMediaProxy server ok,cur audio port just %d\n",recv_pkt.au_port);
				return media_trans_socket_fd;
			}
		}
		else
		{
			//return_value = 3;
			printf("recv error\n");
		}
		
		usleep(10*1000);
		close( media_trans_socket_fd );	

	}
	else
	{
		media_trans_socket_fd =0;
	}
	
	return 0;
}
int Api_media_transfer_account_req2(int media_trans_socket_fd,char *ip_str,char *acc,char *pwd,char *related_acc, short *pauport, short* pvdport)
{
	int  err, num;
	//int media_trans_socket_fd;

	COMMU_PACK_ACCOUNT_REQ2 send_pkt;
	COMMU_PACK_ACCOUNT_RSP2 recv_pkt;
	int recv_len;
	//MyRand();
	srand(time(NULL));
	
	memset(&send_pkt,0,sizeof(COMMU_PACK_ACCOUNT_REQ2));
	memset(&recv_pkt,0,sizeof(COMMU_PACK_ACCOUNT_RSP2));
	
	send_pkt.head.cmd_type = DEV_ACCOUNT_REQ2;
	send_pkt.head.dev_type = ACCOUNT_TYPE_MONITOR;
	strcpy(send_pkt.account,acc);
	strcpy(send_pkt.password,pwd);
	strcpy(send_pkt.related_account,related_acc);
	{
		// send		
		struct timeval tv;
		fd_set fds;
		int ret;
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;	// 100ms
		if((ret = select( media_trans_socket_fd + 1, NULL, &fds, NULL, &tv ))>0)
		{
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = send(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ2),MSG_DONTWAIT);
			else
				return 0;
		}
		else
			return 0;
	

		
		// recv
		recv_len = sizeof(COMMU_PACK_ACCOUNT_RSP2);
		
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;
		if((ret = select( media_trans_socket_fd + 1, &fds,NULL, NULL, &tv ))>0)	
		{
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = recv(media_trans_socket_fd, (char*)&recv_pkt, recv_len, MSG_DONTWAIT);
			else
				return 0;
			
		}
		else
			return 0;
		

			//printf("2-------------------------------------------\n");

		if (num > 0) // success
		{
			//*precv_len = num;
			//return_value = 0;
			printf("send and recv over\n");	
			if(recv_pkt.head.cmd_type == DEV_ACCOUNT_RSP2 && recv_pkt.result == 0)
			{
				*pauport = recv_pkt.au_port;
				*pvdport = recv_pkt.vd_port;
				printf("register to myMediaProxy server ok,cur audio port just %d\n",recv_pkt.au_port);
				return media_trans_socket_fd;
			}
		}
		else
		{
			//return_value = 3;
			printf("recv error\n");
		}
		
		usleep(10*1000);
		close( media_trans_socket_fd );	
	}	
	return 0;
}

int Api_media_transfer_discennect(int media_trans_socket_fd)
{
	if(media_trans_socket_fd != 0)
	{
		close( media_trans_socket_fd );
		media_trans_socket_fd=0;
	}
}

int API_PushAppWakeup_connect(char *ip_str,char *acc,char *pwd,char *related_acc)
{
	return API_VtkMediaTrans_VtkPushReq(ip_str,acc,pwd,related_acc);
}

int GetVtkTcpCtrlCmdDataLen(COMMU_PACK_CONTROL_REQ* pPacket)
{
	int len = 0;
	int codeLen;
	COMMU_IPC_LIST_REQ* pIpcListReqPacket;
	COMMU_IPC_LIST_RSP* pIpcListRspPacket;
	
	COMMU_PACK_MEDIA_STORE_PUSH* pMediaStorePushReqPacket;
	COMMU_PACK_MEDIA_STORE_PUSH_RSP* pMediaStorePushRspPacket;
	
	COMMU_PACK_MEDIA_STORE_POP_RSP* pMediaStorePopRspPacket;
	COMMU_PACK_MEDIA_TRANSFER_IN* pMediaTransferInReqPacket;

	//COMMU_LAMPON_REQ *pLamponReq;

	dprintf("GetVtkTcpCtrlCmdDataLen pPacket->head.cmd_type=0x%04x, pPacket->ctrl_cmd=%d\n", pPacket->head.cmd_type, pPacket->ctrl_cmd);

	
	switch(pPacket->head.cmd_type)
	{
		case DEV_ACCOUNT_REQ:
			len = sizeof(COMMU_PACK_ACCOUNT_REQ);
			break;
		case DEV_ACCOUNT_RSP:
			len = sizeof(COMMU_PACK_ACCOUNT_RSP);
			break;

		case DEV_ACCOUNT_REQ2:
			len = sizeof(COMMU_PACK_ACCOUNT_REQ2);
			break;
		case DEV_ACCOUNT_RSP2:
			len = sizeof(COMMU_PACK_ACCOUNT_RSP2);
			break;
			
		case DEV_MEDIA_STORE_PUSH:
			pMediaStorePushReqPacket = (COMMU_PACK_MEDIA_STORE_PUSH*)pPacket;
			len = sizeof(COMMU_PACK_MEDIA_STORE_PUSH) - MEDIA_TRANS_BUFF_MAX + pMediaStorePushReqPacket->media_len;
			break;
			
		case DEV_MEDIA_STORE_PUSH_RSP:
			pMediaStorePushRspPacket = (COMMU_PACK_MEDIA_STORE_PUSH_RSP*)pPacket;
			len = sizeof(COMMU_PACK_MEDIA_STORE_PUSH_RSP) - sizeof(pMediaStorePushRspPacket->results) + pMediaStorePushRspPacket->media_len;
			break;

		case DEV_CONTROL_REQ:
			switch(pPacket->ctrl_cmd)
			{
				case DEV_CONTROL_UNLOCK:
					len = 8;
					break;
				case DEV_CONTROL_MONCODE:
				case DEV_CONTROL_CALLCODE:
				case DEV_CONTROL_DTMFCODE:
					for(codeLen = 0; codeLen < 20; codeLen++)
					{
						if(pPacket->ctrl_dat[codeLen] == '#')
						{
							codeLen++;
							break;
						}
						#if defined(PID_DX470)||defined(PID_DX482)
						if(codeLen==4&&pPacket->ctrl_cmd==DEV_CONTROL_MONCODE)
						{
							pPacket->ctrl_dat[codeLen]='#';
							codeLen++;
							break;
						}	
						#endif
					}
					len = sizeof(pPacket->head) + sizeof(pPacket->ctrl_cmd) + codeLen;
					break;
				case Get_IPC_RES_LIST:
				case MONITOR_IPC_SW:
					pIpcListReqPacket = (COMMU_IPC_LIST_REQ*)pPacket;
					len = sizeof(pIpcListReqPacket->head) + sizeof(pIpcListReqPacket->ctrl_cmd) + sizeof(pIpcListReqPacket->data_len) + pIpcListReqPacket->data_len;
					break;
				case APP_LAMPON:
					len=sizeof(COMMU_LAMPON_REQ);
					break;
			}
			break;
		case DEV_CONTROL_RSP:
			switch(pPacket->ctrl_cmd)
			{
				case DEV_CONTROL_UNLOCK:
					len = 10;
					break;
				case DEV_CONTROL_MONCODE:
				case DEV_CONTROL_CALLCODE:
				case DEV_CONTROL_DTMFCODE:
					for(codeLen = 0; codeLen < 20; codeLen++)
					{
						if(pPacket->ctrl_dat[codeLen] == '#')
						{
							codeLen++;
							break;
						}
					}
					len = sizeof(pPacket->head) + sizeof(pPacket->ctrl_cmd) + codeLen + 2;
					break;
				case Get_IPC_RES_LIST:
				case MONITOR_IPC_SW:
					pIpcListRspPacket = (COMMU_IPC_LIST_RSP*)pPacket;
					len = sizeof(pIpcListRspPacket->head) + sizeof(pIpcListRspPacket->ctrl_cmd) + sizeof(pIpcListRspPacket->result) + sizeof(pIpcListRspPacket->data_len) + pIpcListRspPacket->data_len;
					break;
			}
			break;


		case DEV_MEDIA_STORE_POP_REQ:
			len = sizeof(COMMU_PACK_MEDIA_STORE_POP_REQ);
			break;
			
		case DEV_MEDIA_STORE_POP_RSP:
			pMediaStorePopRspPacket = (COMMU_PACK_MEDIA_STORE_POP_RSP*)pPacket;
			len = sizeof(COMMU_PACK_MEDIA_STORE_POP_RSP) - MEDIA_TRANS_BUFF_MAX + pMediaStorePopRspPacket->media_len;
			break;

		case DEV_MEDIA_TRANSFER_IN:
			pMediaTransferInReqPacket = (COMMU_PACK_MEDIA_TRANSFER_IN*)pPacket;
			len = sizeof(COMMU_PACK_MEDIA_TRANSFER_IN) - 1 + pMediaTransferInReqPacket->media_len;
			break;
			
		case DEV_MEDIA_TRANSFER_OUT:
			pMediaTransferInReqPacket = (COMMU_PACK_MEDIA_TRANSFER_OUT*)pPacket;
			len = sizeof(COMMU_PACK_MEDIA_TRANSFER_OUT) - 1 + pMediaTransferInReqPacket->media_len;
			break;
			
		case APP_CTRL_REQ:
			len = sizeof(COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_REQ);
			break;
		case APP_CTRL_RSP:
			len = sizeof(COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_RSP);
			break;
		case SERVER_NOTIFY2DEVICE_TYPE:
			len = sizeof(COMMU_PACK_SERVER_NOTIFY);
			break;
	}
	
	return len;
}

int RecvTry_VtkTcpCtrlReq(int media_trans_socket_fd)
{
	int recv_len;
	int recv_pos;
	int pack_len;
	int num;
	char recv_data[VTK_TCP_BUFF_MAX];
	fd_set fdr;
	struct timeval timeout;
	
	COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_REQ* ptrAppPack;
	COMMU_PACK_SERVER_NOTIFY *ptrPack;
	COMMU_PACK_CONTROL_REQ* pRecv_pkt;

	FD_ZERO(&fdr);
	
	FD_SET(media_trans_socket_fd, &fdr);
	
	timeout.tv_sec = 0;
	timeout.tv_usec = 1000*100;
	int rc = select(media_trans_socket_fd + 1, &fdr, NULL, NULL, &timeout);
	if(rc<0||!FD_ISSET(media_trans_socket_fd, &fdr))
		return 0;
	recv_len = TCPCTRL_PACK_HEAD_LENTH;
	num = recv(media_trans_socket_fd, recv_data, recv_len, MSG_DONTWAIT);
	if(num == recv_len)
	{
		recv_pos = num;
		recv_len = VTK_TCP_BUFF_MAX - recv_pos;
		num = recv(media_trans_socket_fd, recv_data + recv_pos, recv_len, MSG_DONTWAIT);
		if(num == 0)
		{
			API_VtkMediaTrans_TcpDisconnect();
			return -1;
			//StopVtkMediaTcpCtrl();
		}
		else 
		{
			recv_len = (num > 0 ? num : 0) + recv_pos;

			#if 1
					int i;
					char* pchar;
					pchar = (char*)recv_data;
					
					printf("11111111111111111111111111111RecvTry_VtkTcpCtrlReq recv_len=%d\n",recv_len);
					for(i=0;i<recv_len;i++)
						printf("%02x ", pchar[i]);
					printf("\n");
			#endif
			
			for(recv_pos = 0; recv_pos < recv_len; recv_pos += pack_len)
			{
				dprintf("222222222222222222222222222222 recv_pos = %d\n", recv_pos);
				pRecv_pkt = (COMMU_PACK_CONTROL_REQ*)((char*)&recv_data[recv_pos]);
				pack_len = GetVtkTcpCtrlCmdDataLen(pRecv_pkt);
				if(pack_len <= 0)	//不识别数据包
				{
					break;
				}
				
				if(pack_len + recv_pos <= recv_len)
				{
					#if 1
							pchar = (char*)pRecv_pkt;
							dprintf("33333333333333333333333333333TcpCtrl packet len=%d\n", pack_len);
							for(i=0;i<pack_len;i++)
								printf("%02x ", pchar[i]);
							printf("\n");
					#endif
					
					API_VtkMediaTrans_TcpCtrl((char*)pRecv_pkt, pack_len);
				}
				#if defined(PID_DX470)||defined(PID_DX482)
				else if(pRecv_pkt->ctrl_cmd==DEV_CONTROL_MONCODE&&pack_len + recv_pos <= (recv_len+1))
				{
					#if 1
							pchar = (char*)pRecv_pkt;
							dprintf("5555555TcpCtrl packet len=%d\n", pack_len);
							for(i=0;i<pack_len;i++)
								printf("%02x ", pchar[i]);
							printf("\n");
					#endif
					
					API_VtkMediaTrans_TcpCtrl((char*)pRecv_pkt, pack_len);
				}
				#endif
			}
		}
	}
	else if(num == 0)
	{
		API_VtkMediaTrans_TcpDisconnect();
		//StopVtkMediaTcpCtrl();
		return -1;
	}

	return 0;
}

int send_VtkTcpCtrlRsp(int media_trans_socket_fd,short ctrl_code,char *dat,int dat_len)
{
	COMMU_PACK_CONTROL_RSP send_pkt;
	send_pkt.head.cmd_type = DEV_CONTROL_RSP;
	send_pkt.ctrl_cmd = ctrl_code;
	memcpy(send_pkt.ctrl_dat,dat,dat_len);

	printf("send_VtkTcpCtrlRsp,ctrl_code[%02x],len=%d\n",ctrl_code,dat_len);
		
	return send(media_trans_socket_fd, (char *)&send_pkt,TCPCTRL_PACK_HEAD_LENTH+dat_len,0);
}

int send_VtkTcpCtrlReq(int media_trans_socket_fd,short ctrl_code,char *dat,int dat_len)
{
	COMMU_PACK_CONTROL_REQ send_pkt;
	
	send_pkt.head.cmd_type = DEV_CONTROL_REQ;
	send_pkt.ctrl_cmd = ctrl_code;
	memcpy(send_pkt.ctrl_dat,dat,dat_len);
		
	return send(media_trans_socket_fd, (char *)&send_pkt,TCPCTRL_PACK_HEAD_LENTH+dat_len,0);
}

int send_VtkTcpReq(int media_trans_socket_fd, short cmd_type, short ctrl_code, char *dat, int dat_len)
{
	char send_buffer[VTK_TCP_BUFF_MAX];
	int send_len;
	
	COMMU_PACK_CONTROL_REQ send_pkt;
	
	send_pkt.head.cmd_type = cmd_type;
	send_pkt.head.dev_type = 0;
	send_pkt.ctrl_cmd = ctrl_code;

	send_len = sizeof(send_pkt.head) + sizeof(send_pkt.ctrl_cmd);
	
	memcpy(send_buffer, (char *)&send_pkt, send_len);
	memcpy(send_buffer + send_len, dat, dat_len);
	send_len += dat_len;
	
#if 0
	int i;

	printf("send_VtkTcpReq send_len=%d\n",send_len);
	for(i=0;i<send_len;i++)
		printf("%02x ", send_buffer[i]);
	printf("\n");
#endif
		
	return send(media_trans_socket_fd, send_buffer, send_len,0);
}

int DivertUnlockCtrlDebounce(short ctrl_source,short lock_id) //ctrl_source 0,dtmf 1,tcp 1,ok,else fail
{
	static int lock1ctrl_src_save=-1;
	static int lock2ctrl_src_save=-1;
	static int lock1ctrl_time_save = 0;
	static int lock2ctrl_time_save = 0;
	int *src_save,*time_save;
	int cur_time = time(NULL);

	if(lock_id == 2)
	{
		src_save = &lock2ctrl_src_save;
		time_save = &lock2ctrl_time_save;
	}
	else
	{
		src_save = &lock1ctrl_src_save;
		time_save = &lock1ctrl_time_save;
	}

	if((ctrl_source == src_save && (cur_time-*time_save)>2)||(ctrl_source != src_save && (cur_time-*time_save)>3))
	{
		*src_save = ctrl_source;
		*time_save = cur_time;
		return 1;
	}

	return 0;
}

int send_VtkTcpCtrlIPCRsp(int media_trans_socket_fd,short ctrl_code, short result, char *dat, int dat_len)
{
	COMMU_IPC_LIST_RSP ipcRsp;
	char ctrlRspDat[VTK_TCP_BUFF_MAX];
	int ctrlRspLen;

	ipcRsp.head.dev_type = 0;
	ipcRsp.head.cmd_type = DEV_CONTROL_RSP;
	ipcRsp.ctrl_cmd = ctrl_code;
	ipcRsp.result = result;
	ipcRsp.data_len = dat_len;
	ctrlRspLen = 0;
	ipcRsp.pData = NULL;
	
	//DEV_CONTROL_RSP
	memcpy(ctrlRspDat+ctrlRspLen, (char*)&ipcRsp.head.cmd_type, sizeof(ipcRsp.head.cmd_type));
	ctrlRspLen += sizeof(ipcRsp.head.cmd_type);
	
	//DEVICE_TYPE
	memcpy(ctrlRspDat+ctrlRspLen, (char*)&ipcRsp.head.dev_type, sizeof(ipcRsp.head.dev_type));
	ctrlRspLen += sizeof(ipcRsp.head.dev_type);
	
	//CMD
	memcpy(ctrlRspDat+ctrlRspLen, (char*)&ipcRsp.ctrl_cmd, sizeof(ipcRsp.ctrl_cmd));
	ctrlRspLen += sizeof(ipcRsp.ctrl_cmd);
		
	//RESULT
	memcpy(ctrlRspDat+ctrlRspLen, (char*)&ipcRsp.result, sizeof(ipcRsp.result));
	ctrlRspLen += sizeof(ipcRsp.result);
	
	//DATA_LEN
	memcpy(ctrlRspDat+ctrlRspLen, (char*)&ipcRsp.data_len, sizeof(ipcRsp.data_len));
	ctrlRspLen += sizeof(ipcRsp.data_len);
		
	//DATA
	if(dat != NULL && dat_len != 0)
	{
		memcpy(ctrlRspDat+ctrlRspLen, dat, dat_len);
		ctrlRspLen += dat_len;
	}
	
/*	
	printf("send_VtkTcpCtrlIPCRsp ctrlRspLen = %d, dat_len=%d\n", ctrlRspLen, dat_len);
	
	int i;
	for(i=0;i<ctrlRspLen;i++)
		printf("%02x ", ctrlRspDat[i]);
	
	printf("\n");
*/

	return send(media_trans_socket_fd, ctrlRspDat, ctrlRspLen, 0);	
}

void vtkTcpControlReq_Process(int media_trans_socket_fd,short ctrl_code,char *dat, int dat_len)
{
	short *pdat16 = (short*)dat;
	char *pdat8;
	short rsp_dat[64];
	char code_buff[20];
	int code_len;
	char temp[20];
	char ds_addr[11];

	IPC_ONE_DEVICE oneIpcRecord;
	APP_IPC_ONE_DEVICE oneAppIpcListRecord;
	COMMU_IPC_LIST_REQ ipcReq;
	COMMU_IPC_LIST_RSP ipcRsp;
	char ctrlRspDat[VTK_TCP_BUFF_MAX];
	if(strcmp(GetCustomerizedName(),"GATES")!=0)
		delay_im_answer();
	switch(ctrl_code)
	{
		case DEV_CONTROL_UNLOCK:
			if(IfCallServerBusy()==1)
			{
				if(pdat16[0] == 2)
				{
					//API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
					API_CallServer_RemoteUnlock2(CallServer_Run.call_type,NULL);
				}
				else
				{
					//API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
					API_CallServer_RemoteUnlock1(CallServer_Run.call_type,NULL);
				}
			}
			else
			{
				#if defined(PID_DX470)||defined(PID_DX482)
				if(pdat16[0] == 2)
				{
					API_DXMonitor_Unlock2();
				}
				else
				{
					API_DXMonitor_Unlock1();
				}
				#else
				if(pdat16[0] == 2)
				{
					DsUnlock2Deal(1);
				}
				else
				{
					DsUnlock1Deal(1);
				}
				
				#endif
				#if 0
				if(pdat16[0] == 2)
				{
					//API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
					//API_CallServer_RemoteUnlock1(CallServer_Run.call_type,NULL);
					API_LocalMonitor_Unlock2();
				}
				else
				{
					//API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
					//API_CallServer_RemoteUnlock2(CallServer_Run.call_type,NULL);
					API_LocalMonitor_Unlock1();
				}
				#endif
			}
			rsp_dat[0]=pdat16[0]; 
			rsp_dat[1]=0;
			send_VtkTcpCtrlRsp(media_trans_socket_fd,DEV_CONTROL_UNLOCK,(char*)rsp_dat,sizeof(short)*2);
			break;
		case DEV_CONTROL_MONCODE:
			#if 0
			for(code_len= 0;code_len < dat_len&&code_len<20;code_len++)
			{
				code_buff[code_len] = dat[code_len];
				if(dat[code_len]=='#')
				{
					break;
				}
				
			}
			if(dat[code_len] == '#')
			{
				rsp_dat[0]=0;
				pdat8=(char*)&rsp_dat[1];
				memcpy(pdat8,code_buff,code_len+1);
				
				rsp_dat[(code_len+1)/2+(code_len+1)%2]=(LinPhone_BeCalled_tcpcode_Verify(0,code_buff,code_len)==1)?0:1;
				
				send_VtkTcpCtrlRsp(media_trans_socket_fd,DEV_CONTROL_MONCODE,(char*)rsp_dat,((code_len+1)/2+(code_len+1)%2+1)*2);
				if(rsp_dat[0] == 0)
				{
					//LinPhone_BeCalled_Start_Monitor();
				}
			}
			#endif
			for(code_len= 0;code_len < dat_len&&code_len<20;code_len++)
			{
				code_buff[code_len] = dat[code_len];
				if(dat[code_len]=='#')
				{
					break;
				}
				
			}
			if(dat[code_len] == '#')
			{
				rsp_dat[0]=0;
				pdat8=(char*)&rsp_dat[1];
				memcpy(pdat8,code_buff,code_len+1);
				log_d("phone mon code [%s]",code_buff);
				rsp_dat[(code_len+1)/2+(code_len+1)%2]=0;//(LinPhone_BeCalled_tcpcode_Verify(0,code_buff,code_len)==1)?0:1;
				if(memcmp(code_buff,"0x",2)==0)
				{
					//linphone_becall_start_dsmon();
					//API_VtkMediaTrans_StartViRtp();
					code_buff[4]=0;
					if(LinphoneDxMonitorDsStart(code_buff)==0)
						ipcRsp.result=0;
					else
						rsp_dat[0]=1;
					#if 0
					if(strcmp(GetCustomerizedName(),"GATES")==0)
					{
						API_OpenDxRtpVideo();
						trigger_send_key_frame_delay(1,8);
					}
					#endif
					//log_i("phone monitor DS %s %s",oneAppIpcListRecord.NAME,ipcRsp.result==0? "ok":"fail");
				}
				else if(memcmp(code_buff,"OS",2)==0)
				{
					code_buff[4]=0;
					memcpy(temp,GetSysVerInfo_BdRmMs(),8);
					temp[8]=0;
					sprintf(ds_addr,"%s%02d",temp,atoi(code_buff+2)+50);
					if(LinphoneMonitorDsStart(ds_addr)==0)
					{
						rsp_dat[0]=0;
					}
					else
						rsp_dat[0]=1;
				}
				else if(memcmp(code_buff,"DS",2)==0)
				{
					code_buff[4]=0;
					memcpy(temp,GetSysVerInfo_bd(),4);
					memcpy(temp+4,"0000",4);
					temp[8]=0;
					sprintf(ds_addr,"%s%02d",temp,atoi(code_buff+2));
					if(LinphoneMonitorDsStart(ds_addr)==0)
					{
						rsp_dat[0]=0;
					}
					else
						rsp_dat[0]=1;
				}
				else if(memcmp(code_buff,"CS",2)==0)
				{
					code_buff[4]=0;
					//memcpy(temp,GetSysVerInfo_bd(),4);
					memcpy(temp,"00000000",8);
					temp[8]=0;
					sprintf(ds_addr,"%s%02d",temp,atoi(code_buff+2));
					if(LinphoneMonitorDsStart(ds_addr)==0)
					{
						rsp_dat[0]=0;
					}
					else
						rsp_dat[0]=1;
				}
				send_VtkTcpCtrlRsp(media_trans_socket_fd,DEV_CONTROL_MONCODE,(char*)rsp_dat,((code_len+1)/2+(code_len+1)%2+1)*2);
				if(rsp_dat[0] == 0)
				{
					//LinPhone_BeCalled_Start_Monitor();
				}
				
			}
			break;

		case DEV_CONTROL_CALLCODE:
			for(code_len= 0;code_len < dat_len&&code_len<20;code_len++)
			{
				code_buff[code_len] = dat[code_len];
				if(dat[code_len]=='#')
				{
					break;
				}
				
			}
			if(dat[code_len] == '#')
			{
				rsp_dat[0]=0;
				pdat8=(char*)&rsp_dat[1];
				memcpy(pdat8,code_buff,code_len+1);
				
				rsp_dat[(code_len+1)/2+(code_len+1)%2]=(LinPhone_BeCalled_tcpcode_Verify(1,code_buff,code_len)==1)?0:1;
				
				send_VtkTcpCtrlRsp(media_trans_socket_fd,DEV_CONTROL_CALLCODE,(char*)rsp_dat,((code_len+1)/2+(code_len+1)%2+1)*2);
				if(rsp_dat[0] == 0)
				{
					//LinPhone_BeCalled_Start_Call();
				}
			}
			break;
		case DEV_CONTROL_DTMFCODE:
			for(code_len= 0; code_len < dat_len && code_len < 20; code_len++)
			{
				code_buff[code_len] = dat[code_len];
				if(dat[code_len]=='#')
				{
					code_buff[code_len+1] = 0;
					break;
				}
			}

			printf("DEV_CONTROL_DTMFCODE dat_len=%d, code_len=%d, code_buff=%s\n", dat_len, code_len, code_buff);
			
			rsp_dat[0]=pdat16[0];
			pdat8 = (char *)&rsp_dat[1];
			memcpy(pdat8, code_buff, code_len+1);
			pdat8[code_len+1] = 0;//recv_tcp_dtmf_process(code_buff);
			//pdat8[code_len+1] = recv_tcp_dtmf_process(code_buff);

			send_VtkTcpCtrlRsp(media_trans_socket_fd,DEV_CONTROL_DTMFCODE,(char*)rsp_dat, code_len+3);
			recv_tcp_dtmf_process(code_buff);
			break;
			
		case Get_IPC_RES_LIST:
			//LinphoneDxMonitorDsStop();
			
			//if(strcmp(GetCustomerizedName(),"GATES")==0)
			//	API_CloseDxRtpVideo();
			ipcReq.data_len = dat[0] + (dat[1] << 8) + (dat[2] << 16) + (dat[3] << 24);
			ipcRsp.data_len = 0;
		
			if(ipcReq.data_len < 16)
			{
				//md5码不相同，更新列表
				ipcRsp.result = 1;
			}
			else
			{
				ipcReq.pData = &dat[4];
				//md5码相同，不用更新列表
				if(!memcmp(ipcReq.pData, GetAppIpcListMD5Code(), 16))
				{
					ipcRsp.result = 0;
					ipcRsp.data_len = 0;
				}
				//md5码不相同，更新列表
				else
				{
					ipcRsp.result = 1;
				}
			}
			
			
			if(ipcRsp.result)
			{
				ipcRsp.pData = CreateAppIpcListObject(1);
		
				//DATA
				memcpy(ctrlRspDat+ipcRsp.data_len, GetAppIpcListMD5Code(), 16);
				ipcRsp.data_len += 16;
				
				if(ipcRsp.pData != NULL)
				{
					memcpy(ctrlRspDat+ipcRsp.data_len, ipcRsp.pData, strlen(ipcRsp.pData));
					ipcRsp.data_len += strlen(ipcRsp.pData);
					free(ipcRsp.pData);
				}
			}
			
			dprintf("Get_IPC_RES_LIST GetAppIpcListMD5Code=");
			for(code_len = 0; code_len < 16; code_len++)
				printf("%02x", ctrlRspDat[code_len]);
			printf("\n");
			
			dprintf("Get_IPC_RES_LIST AppIpcList = \n%s", ctrlRspDat+16);
			
			send_VtkTcpCtrlIPCRsp(media_trans_socket_fd, Get_IPC_RES_LIST, ipcRsp.result, ctrlRspDat, ipcRsp.data_len);
			log_i("phone update IPC list");
			break;
		case MONITOR_IPC_SW:
			#if defined(PID_IX850)
			#else
			//if(strcmp(GetCustomerizedName(),"GATES")==0)
			//	API_CloseDxRtpVideo();//LinphoneDxMonitorDsStop();
			ipcReq.data_len = dat[0] + (dat[1] << 8) + (dat[2] << 16) + (dat[3] << 24);
			ipcReq.pData = dat+4;
		
			ipcRsp.result = 1;
		
			dprintf("MONITOR_IPC_SW ipcReq.data_len=%d\n", ipcReq.data_len);
			dprintf("MONITOR_IPC_SW ipcReq.pData=%s\n", ipcReq.pData);
			
			if(ParseOneAppIpcListRecordObject(ipcReq.pData, &oneAppIpcListRecord))
			{
				if(memcmp(oneAppIpcListRecord.ID,"IX_DS:",strlen("IX_DS:"))==0)
				{
					if(LinphoneMonitorDsStart(oneAppIpcListRecord.ID+strlen("IX_DS:"))==0)
						ipcRsp.result=0;
					log_i("phone monitor DS %s %s",oneAppIpcListRecord.NAME,ipcRsp.result==0? "ok":"fail");
				}
				else if(memcmp(oneAppIpcListRecord.ID,"DT_DS:",strlen("DT_DS:"))==0)
				{
					if(LinphoneDxMonitorDsStart(oneAppIpcListRecord.ID+strlen("DT_DS:"))==0)
						ipcRsp.result=0;
					log_i("phone monitor DS %s %s",oneAppIpcListRecord.NAME,ipcRsp.result==0? "ok":"fail");
				}
				else
				{
					if(GetIpcRecordByNameOrId(oneAppIpcListRecord.NAME, oneAppIpcListRecord.ID, &oneIpcRecord) == 0)
					{
						if(LinphoneIPCMonitorStart(oneIpcRecord) == 0)
						{
							ipcRsp.result = 0;
						}
						log_i("phone monitor IPC %s %s",oneAppIpcListRecord.NAME,ipcRsp.result==0? "ok":"fail");
					}
					else if(GetWlanIpcRecordByNameOrId(oneAppIpcListRecord.NAME, oneAppIpcListRecord.ID, &oneIpcRecord) == 0)
					{
						if(LinphoneIPCMonitorStart(oneIpcRecord) == 0)
						{
							ipcRsp.result = 0;
						}
						log_i("phone monitor WLAN IPC %s %s",oneAppIpcListRecord.NAME,ipcRsp.result==0? "ok":"fail");
					}	
				}
			}
						
			send_VtkTcpCtrlIPCRsp(media_trans_socket_fd, MONITOR_IPC_SW, ipcRsp.result, NULL, 0);
			#endif
			break;
		case APP_LAMPON:
			
			rsp_dat[0]=pdat16[0]; 
			rsp_dat[1]=0;
			send_VtkTcpCtrlRsp(media_trans_socket_fd,APP_LAMPON,(char*)rsp_dat,sizeof(short)*2);
			#if defined(PID_DX470)||defined(PID_DX482)
			LightIconProcess();
			#endif
			break;
	}
}


// input:
// ip_str - server ip address
// acc - local account
// pwd - local password
// related_acc - to be pushed account
// presult - push result sync. 0/推送OK，1/帐号或密码错误，2/设备类型不存在，3/推送失败
// return:
// >0: tcp connect handle, other: error
int Api_device_connect_push_req(const char *ip_str, const char *acc, const char *pwd, const char *related_acc, short* presult )
{
	int  err, num;
	int media_trans_socket_fd;

	COMMU_PACK_ACCOUNT_PUSH_REQ send_pkt;
	COMMU_PACK_ACCOUNT_PUSH_RSP recv_pkt;
	int recv_len;
	//MyRand();
	srand(time(NULL));
	
	memset(&send_pkt,0,sizeof(COMMU_PACK_ACCOUNT_PUSH_REQ));
	memset(&recv_pkt,0,sizeof(COMMU_PACK_ACCOUNT_PUSH_RSP));
	
	send_pkt.head.cmd_type = DEV_ACCOUNT_PUSH_REQ;
	send_pkt.head.dev_type = ACCOUNT_TYPE_MONITOR;
	strcpy(send_pkt.account,acc);
	strcpy(send_pkt.password,pwd);
	strcpy(send_pkt.related_account,related_acc);
	// connect
	int retry_cnt = 4;
	while(retry_cnt > 0 && (err = connect_with_timeout( ip_str, VTK_MEDIA_TRANSFER_PORT, 5, &media_trans_socket_fd))!=0)
	{
		sleep(1);
		retry_cnt --;
	}
	
	if(err == 0)
	{
		int setflag = 1;
		//setsockopt(media_trans_socket_fd, IPPROTO_TCP, TCP_NODELAY, &setflag, sizeof(int));
		int flags = fcntl(media_trans_socket_fd, F_GETFL, 0);
		if (flags < 0) 
		{
			printf("Get flags error:%s\n", strerror(errno));
			close(media_trans_socket_fd);
			return -1;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(media_trans_socket_fd, F_SETFL, flags) < 0) 
		{
			printf("Set flags error:%s\n", strerror(errno));
			close(media_trans_socket_fd);
			return -1;
		}
		
 		struct timeval tv_out;
	    	tv_out.tv_sec = 3;
	    	tv_out.tv_usec = 0;
	    	setsockopt(media_trans_socket_fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		
		// send
		#if 1
		struct timeval tv;
		fd_set fds;
		int ret;
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;	// 100ms
		if((ret = select( media_trans_socket_fd + 1, NULL, &fds, NULL, &tv ))>0)
		{
			//num = write(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ));
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = send(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_PUSH_REQ),MSG_DONTWAIT);
			else
				return 0;
		}
		else
			return 0;
		#else
		num = write(media_trans_socket_fd, (char *)&send_pkt, sizeof(COMMU_PACK_ACCOUNT_REQ));
		#endif

		#if 0
		if (num <= 0) {
			printf("send error\n");
			return 2;
		}
		#endif
		// recv
		recv_len = sizeof(COMMU_PACK_ACCOUNT_PUSH_RSP);
		#if 1
		FD_ZERO(&fds);
		FD_SET(media_trans_socket_fd,&fds);
			
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		if((ret = select( media_trans_socket_fd + 1, &fds,NULL, NULL, &tv ))>0)	
		{
			if( FD_ISSET( media_trans_socket_fd, &fds ) )
				num = recv(media_trans_socket_fd, (char*)&recv_pkt, recv_len, MSG_DONTWAIT);
			else
				return 0;
			
		}
		else
			return 0;
		#else
		num = recv(media_trans_socket_fd, (char*)&recv_pkt, recv_len, 0);
		#endif

		if (num > 0) // success
		{
			//*precv_len = num;
			//return_value = 0;
			printf("send and recv over\n");	
			if(recv_pkt.head.cmd_type == DEV_ACCOUNT_PUSH_RSP )
			{
				*presult = recv_pkt.result;
				printf("Api_device_connect_push_req result: %d\n",recv_pkt.result);
				return media_trans_socket_fd;
			}
		}
		else
		{
			//return_value = 3;
			printf("recv error\n");
		}
		
		usleep(10*1000);
		close( media_trans_socket_fd );	

	}
	else
	{
		media_trans_socket_fd =0;
	}
	
	return 0;
}


