
#ifndef _vtk_media_tcp_transfer_H_
#define _vtk_media_tcp_transfer_H_

#define ACCOUNT_TYPE_MONITOR		0
#define ACCOUNT_TYPE_APP			1
#define ACCOUNT_TYPE_MAX			2
#define DEV_ACCOUNT_REQ  			0x1000
#define DEV_ACCOUNT_RSP  			0x1001
#define DEV_ACCOUNT_REQ2  			0x1002
#define DEV_ACCOUNT_RSP2 			0x1003

#define DEV_ACCOUNT_PUSH_REQ  		0x1004
#define DEV_ACCOUNT_PUSH_RSP  		0x1005

#define DEV_CONTROL_REQ				0x1010
#define DEV_CONTROL_RSP				0x1011
#define DEV_MEDIA_STORE_PUSH		0x1020
#define DEV_MEDIA_STORE_PUSH_RSP	0x1021
#define DEV_MEDIA_STORE_POP_REQ 	0x1022
#define DEV_MEDIA_STORE_POP_RSP		0x1023
#define DEV_MEDIA_TRANSFER_IN		0x1030
#define DEV_MEDIA_TRANSFER_OUT		0x1031

#define SERVER_NOTIFY2DEVICE_TYPE	0x2000

//#pragma pack(push)

#define MEDIA_TRANS_BUFF_MAX		1200
#define VTK_TCP_BUFF_MAX			1200

#define VTK_MEDIA_TRANSFER_PORT		8850

#define DEV_CONTROL_UNLOCK			1
#define DEV_CONTROL_MONCODE			2
#define DEV_CONTROL_CALLCODE		3
#define DEV_CONTROL_DTMFCODE		4
#define Get_IPC_RES_LIST			5
#define MONITOR_IPC_SW				6
#define APP_LAMPON				7


#define APP_CTRL_REQ				0x4000
#define APP_CTRL_RSP				0x4001

#define RTP_CHANNEL_CHANGE			1
// lzh_20201201_s
#define MEDIA_ERR_NOTIFY_REQ_CODE	3	// app->im
#define MEDIA_ERR_NOTIFY_RSP_CODE	4	// im->app
// lzh_20201201_e



void vtkTcpControlReq_Process(int media_trans_socket_fd,short ctrl_code,char *dat,int dat_len);
int RecvTry_VtkTcpCtrlReq(int media_trans_socket_fd);


#pragma pack(2)  
typedef struct 
{	
	short	cmd_type;	
	short	dev_type;	
}COMMU_PACK_HEAD;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	char				account[16];	
	char				password[16];	
	char				related_account[16];
}COMMU_PACK_ACCOUNT_REQ;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	char				account[16];	
	char				password[16];
	char				related_account[16];	
	short				au_port;
	short				vd_port;
	short				result;
}COMMU_PACK_ACCOUNT_RSP;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	char				account[32];	
	char				password[16];	
	char				related_account[32];
}COMMU_PACK_ACCOUNT_REQ2;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	char				account[32];	
	char				password[16];
	char				related_account[32];	
	short				au_port;
	short				vd_port;
	short				result;
}COMMU_PACK_ACCOUNT_RSP2;

typedef struct
{
	COMMU_PACK_HEAD		head;
	char				account[32];
	char				password[16];
	char				related_account[32];
	int					reserve;
}COMMU_PACK_ACCOUNT_PUSH_REQ;

typedef struct 
{
	COMMU_PACK_HEAD		head;
	char				account[32];
	char				password[16];
	char				related_account[32];
	int					reserve;
	short				result;		// 0/推送OK，1/帐号或密码错误，2/设备类型不存在，3/推送失败
}COMMU_PACK_ACCOUNT_PUSH_RSP;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	char				ctrl_dat[128];
}COMMU_PACK_CONTROL_REQ;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	char				ctrl_dat[128];
}COMMU_PACK_CONTROL_RSP;

#define TCPCTRL_PACK_HEAD_LENTH		((int)(&(((COMMU_PACK_CONTROL_REQ*)0)->ctrl_dat[0])))

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				media_type;	
	short				media_sn;
	short				media_len;	
	char				media_dat[MEDIA_TRANS_BUFF_MAX];
}COMMU_PACK_MEDIA_STORE_PUSH;

typedef struct 
{
	COMMU_PACK_HEAD		head;
	short				media_type;
	short				media_sn;
	short				media_len;
	short				results;
}COMMU_PACK_MEDIA_STORE_PUSH_RSP;

#define MEDIA_STORE_PUSH_HEAD_LENTH		((int)(&(((COMMU_PACK_MEDIA_STORE_PUSH*)0)->media_dat[0])))

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				media_type;
	short				media_sn;
}COMMU_PACK_MEDIA_STORE_POP_REQ;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				media_type;	
	short				media_sn;
	short				media_len;
	char				media_dat[MEDIA_TRANS_BUFF_MAX];
}COMMU_PACK_MEDIA_STORE_POP_RSP;
#define MEDIA_STORE_POP_HEAD_LENTH		((int)(&(((COMMU_PACK_MEDIA_STORE_POP_RSP*)0)->media_dat[0])))

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				media_type;
	short				media_len;	
	char				media_dat[1];
}COMMU_PACK_MEDIA_TRANSFER_IN;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				media_type;	
	short				media_len;	
	char				media_dat[1];
}COMMU_PACK_MEDIA_TRANSFER_OUT;

#define JPG_TRANS_BUFF_MAX			(MEDIA_TRANS_BUFF_MAX-12)
#define JPG_TRANS_BUFF_HEAD_LENTH	12

typedef struct
{
	short send_rand;
	int   total_len;
	short total_pack;
	short pack_id;
	short pack_lenth;
	char pack_dat[JPG_TRANS_BUFF_MAX];
}MEDIA_TYPE0_SEND_STRU;

typedef struct
{
	COMMU_PACK_HEAD		head;
	short				notify_cmd;
	short				notify_dat;
}COMMU_PACK_SERVER_NOTIFY;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	short				DATA_LEN;
	short				VIDEO_CHANNEL;
	short				AUDIO_CHANNEL;
	short				RESERVE;
}COMMU_PACK_RTP_CTRL_REQ;


typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	int					data_len;
	char*				pData;
}COMMU_IPC_LIST_REQ;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	short				result;
	int					data_len;
	char*				pData;
}COMMU_IPC_LIST_RSP;


// lzh_20201207_s
typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				notify_cmd;
	short				data_len;
	short				video_status;
	short				audio_status;
	short				reserve;
}COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_REQ;

typedef struct
{
	COMMU_PACK_HEAD		head;	
	short				notify_cmd;
	short				data_len;
	short				video_status;
	short				audio_status;
	short				reserve;
	short				result;	
}COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_RSP;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	short				lamp_id;
}COMMU_LAMPON_REQ;

typedef struct 
{	
	COMMU_PACK_HEAD		head;	
	short				ctrl_cmd;
	short				lamp_id;
	short				result;
}COMMU_LAMPON_RSP;
// lzh_20201207_e

#pragma pack()

#endif

