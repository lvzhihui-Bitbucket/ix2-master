
#include "task_Hal.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "task_Power.h"
#include "task_debug_sbu.h"

#include "vdp_uart.h" 
//#include "../task_Stack212/obj_L2Layer.h"

#include "ak_common.h"
#include "ak_log.h"
#include "ak_drv_ts.h"

pthread_t task_Hal;
pthread_t task_Hook;
int hal_fd;
static unsigned char app_state = 0;

/******************************************************************************
 * @fn      vtk_TaskInit_Hal()
 *
 * @brief   Task hal initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Hal( int priority )
{
	//create task
	if( pthread_create(&task_Hal,NULL,(void*)vtk_TaskProcessEvent_Hal,NULL) != 0 )
	{
		printf( "Create pthread error! \n" );
	}
	else
		printf( "Create vtk_TaskInit_Hal pthread ok! \n" );		
}

void vtk_Task_Hal_Join( void )
{
	//czn_20160628_s
	int reval=0,*repra = 0;
	
	reval = pthread_join( task_Hal, &repra );
	while(1)
	{
		usleep(2000000);
		printf("pthread_join_task_hal return reval=%d,repra=%d\n",reval,(int)repra);
	}
	//czn_20160628_e

}
 OS_TIMER 	sendMessageTimer;


void TimersendMessageTimerCallBack(void)
{
	static unsigned char beepType = 0;
	OS_RetriggerTimer(&sendMessageTimer);
	
	if(beepType++ < 10)
	{
		API_Beep(beepType);
	}
	else
	{
		beepType = 0;
		API_Beep(beepType);
	}
}

#ifdef PID_IXSE
#include <linux/input.h>
static pthread_t task_tpkey;
static int TouchKey_Timer(int timing)
{
    if(timing >= 3)
    {
        TouchKeyProcess(2, 0x9e);
        return 2;
    }
    return 0;
}
void TouchKeyProcess(int keystate, int keycode)
{
	HAL_MSG_TYPE	key_msg;
	key_msg.head.msg_target_id	= MSG_ID_survey;
	key_msg.head.msg_source_id	= MSG_ID_hal;
	key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
	key_msg.head.msg_sub_type	= 0;
	key_msg.key.keyType 	= KEY_TYPE_TS;	
	key_msg.key.keyData 		 =(keycode == 0x9e)? KEY_UNLOCK : KEY_TALK ;
	if(keystate == 1)
	{
		key_msg.key.keyStatus = TOUCHPRESS;
		if(keycode == 0x9e)
		{
			API_Add_TimingCheck(TouchKey_Timer, 1);
		}
	}
	else if(keystate == 0)
	{	
		key_msg.key.keyStatus = TOUCHCLICK;
		if(keycode == 0x9e)
		{
			API_Del_TimingCheck(TouchKey_Timer);
		}
	}
	else if(keystate == 2)
	{
		key_msg.key.keyStatus = TOUCH_3SECOND;
	}
	gettimeofday(&key_msg.happen_time,NULL);
	API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
}
void read_cap1203_touchkey_thread( void )
{
    struct input_event in;
	while(1)
	{
		if( read(hal_fd, &in, sizeof(struct input_event)) > 0) 
		{
			if(in.type == EV_KEY) 
			{
				printf("read touch key[0x%02x],state[%d]\n",in.code, in.value);
				TouchKeyProcess(in.value, in.code);
			}
		}
		else
		{
			printf("read_cap1203_touchkey_thread ERROR!!!!!!!!!!!!\n");
			break;		
		}
	}
}
static pthread_t task_doorkey;
static int doorkey_status = -1;
void read_doorkey_thread( void )
{
	HAL_MSG_TYPE	key_msg;
	int doorkey_tmp;
	while(1)
	{
		if(app_state)
		{
			doorkey_tmp = gpio_input( DOORBELL_GPIO_PIN );
			if( doorkey_tmp == gpio_input( DOORBELL_GPIO_PIN ) && doorkey_tmp != doorkey_status )
			{
				doorkey_status = doorkey_tmp;
				printf("-----doorkey_status = %d =====\n", doorkey_status);
				key_msg.head.msg_target_id	= MSG_ID_survey;
				key_msg.head.msg_source_id	= MSG_ID_hal;
				key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
				key_msg.head.msg_sub_type	= 0;
				key_msg.key.keyType 		= KEY_TYPE_DB;
				key_msg.key.keyData         = doorkey_status==0? 1 : 0;
				gettimeofday(&key_msg.happen_time,NULL);
				API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			}
		}
		
		usleep(200*1000);
	}
}

static pthread_t task_alarm;
static int alarm_store = -1;
int alarm_fd;
/*
paras notice:
in.value	- bit0: a1 level, bit1: a2 level, bit2: a3 level, bit3: a4 level
*/
void read_alarm_thread( void )
{
	HAL_MSG_TYPE	key_msg;
    struct input_event in;
	while(1)
	{
		if( read(alarm_fd, &in, sizeof(struct input_event)) > 0) 
		{
			if(in.type == EV_REL) 
			{
				#if defined PID_IXSE_V2
				printf("222222222read alarm status, last[0x%08x], cur[0x%08x], \n",alarm_store, in.value);
				#else
				printf("000000000read alarm status, last[0x%08x], cur[0x%08x], \n",alarm_store, in.value);
				#endif
				if(alarm_store!=in.value)
				{
					alarm_store = in.value;
					RevAlarmingSenser_Event(alarm_store&0x0f);
						key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key.keyType 		= KEY_TYPE_ALARM;
					key_msg.key.keyData 		= alarm_store;			
					gettimeofday(&key_msg.happen_time,NULL);
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
				}
			}
		}
		else
		{
			printf("read_alarm_thread ERROR!!!!!!!!!!!!\n");
			close(alarm_fd);
			system("insmod /usr/modules/alarm_drv.ko");
			usleep(100*1000);
			alarm_fd = open("/dev/input/event3",O_RDWR);
			printf("reopen alarm fd[%d]\n",alarm_fd);
		}
	}
}

int get_ixse_alarm_fd(void)
{
	return (alarm_store&0x0f);
}

#endif

void GlobalResourceInit(void)
{
#if !defined(PID_IXSE)
	hal_fd = open("/dev/ds2411",O_RDWR);
	if( hal_fd == -1 )
	{
		hal_fd = open("/dev/minictrl",O_RDWR);
		//printf("error open\n");
		//exit(-1);
	}
#else
	hal_fd = open("/dev/input/event1",O_RDWR);
	if( hal_fd != -1 )
	{
		printf("open dev input event1 \n");
		pthread_create(&task_tpkey,NULL,(void*)read_cap1203_touchkey_thread,NULL);
	}
	
	pthread_create(&task_doorkey,NULL,(void*)read_doorkey_thread,NULL);

	system("insmod /usr/modules/alarm_drv.ko");
	usleep(100*1000);
	alarm_fd = open("/dev/input/event2",O_RDWR);
	if( alarm_fd != -1 )
	{
		printf("open dev input event3 \n");
		pthread_create(&task_alarm,NULL,(void*)read_alarm_thread,NULL);
	}

	MCU_SimulateFlashInit();
#endif
}
/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Hal()
 *
 * @brief   Task hal loop routine
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Hal( void )
{
	static KEY_DATA 		keylast={0};

	KEY_DATA 		key;
	HAL_MSG_TYPE	key_msg;
	
#if 1
	//unsigned char          app_state;

	//API_LED_POWER_ON();				//lyx 20170731	
	API_LedDisplay_BootFinish();	//lzh 20171021
	
    app_state = 1;	

#if !defined(PID_IXSE)
	api_uart_send_pack(UART_TYPE_N2S_REPOTR_APP_STATUS, &app_state, 1);    //lyx 20170731  ֪ͨSTM8: APP���������
	usleep(50*1000);
	api_uart_send_pack(UART_TYPE_N2S_REPOTR_APP_STATUS, &app_state, 1);    //lyx 20170731  ֪ͨSTM8: APP���������
#endif

	DateAndTimeInit();

	key_msg.head.msg_target_id	= MSG_ID_survey;
	key_msg.head.msg_source_id	= MSG_ID_hal;
	key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
	key_msg.head.msg_sub_type	= 0;
	key_msg.key.keyType 		= KEY_TYPE_BOOT;
	key_msg.key.keyData 		= 1;
	API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );

#if !defined(PID_IXSE)
	api_uart_send_pack(UART_TYPE_GET_STM8_CONFIG, NULL, 0);	//dh_20211014
	usleep(50*1000);
	api_uart_send_pack(UART_TYPE_READ_LINE, NULL, 0);		//dh_20211014
#endif
	
	#if defined(PID_DX470)||defined(PID_DX482)
	usleep(50*1000);
	Addr_Report_mcu();
	#endif
	
#else
	key_msg.head.msg_target_id	= MSG_ID_survey;
	key_msg.head.msg_source_id	= MSG_ID_hal;
	key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
	key_msg.head.msg_sub_type	= 0;
	key_msg.key.keyType 		= KEY_TYPE_TS;
	key_msg.key.keyStatus 		= TOUCHCLICK;
	key_msg.key.keyData			= 0x03;
	
	gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
	bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);
	API_add_message_to_suvey_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
#endif
	struct ak_ts_event ts;
    int ret = ak_drv_ts_open();
    if(ret)
    {
        ak_print_error_ex(MODULE_ID_DRV, "ak_drv_ts_open fail.\n");
		printf("=============ak_drv_ts_open er==================\n");
    }
	else
	{
		ak_print_error_ex(MODULE_ID_DRV, "ak_drv_ts_open ok.\n");
		printf("=============ak_drv_ts_open ok==================\n");
	}
	//struct timeval lastTime;	
	while( 1 )
	{
		//ak_drv_wdt_feed();
		ret = ak_drv_ts_get_event(&ts, -1); //30);
		//printf("\n[get event:%d,%d]\n",ret,ts.map);
		if(ret == 0) 
		{
			if (ts.map & 1)
			{
				ak_print_normal(MODULE_ID_DRV, "get ts0:[%d , %d].\n", ts.info[0].x, ts.info[0].y); 
				key.keyType = KEY_TYPE_TP;
				if( (ts.info[0].x == -1) && (ts.info[0].y == -1) )
				{
					key.keyStatus = TOUCHCLICK;					
				}
				else
				{
					key.TpKey.x = ts.info[0].x;
					key.TpKey.y = ts.info[0].y;	
					key.keyStatus = TOUCHPRESS;
					#if 0
					if( (abs(keylast.TpKey.x-key.TpKey.x)>5) || (abs(keylast.TpKey.y-key.TpKey.y)>5) )	
					{
						gettimeofday(&lastTime, 0);
						bprintf("get lastTime = %d:%d\n",lastTime.tv_sec,lastTime.tv_usec);
					}
					else
					{
						gettimeofday(&key_msg.happen_time,NULL);
						bprintf("get happen_time = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);
						int diff;
						diff = (key_msg.happen_time.tv_sec - lastTime.tv_sec)*1000000 + key_msg.happen_time.tv_usec - lastTime.tv_usec;
						if(diff >= 500*1000)
						{
							bprintf("TOUCHLONGCLICK!!!\n");
							key.keyStatus = TOUCHLONGCLICK;
						}
					}
					#endif
				}
				
				if( keylast.keyStatus != key.keyStatus )
				{
					keylast = key;
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key 				= key;
					gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );	
				}
				else if( (abs(keylast.TpKey.x-key.TpKey.x)>10) || (abs(keylast.TpKey.y-key.TpKey.y)>10) )	
				{
					key.keyStatus = TOUCHMOVE;
					key_msg.head.msg_target_id	= MSG_ID_survey;
					key_msg.head.msg_source_id	= MSG_ID_hal;
					key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
					key_msg.head.msg_sub_type	= 0;
					key_msg.key 				= key;
					gettimeofday(&key_msg.happen_time,NULL);//key_msg.happen_time		= clock();		//czn_20170111
					API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) ); 
				}
			}
		}
	}
}

