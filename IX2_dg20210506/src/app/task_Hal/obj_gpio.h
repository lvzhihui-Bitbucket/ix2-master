
#ifndef _OBJ_GPIO_H_
#define _OBJ_GPIO_H_

#ifdef PID_IX47
#define AMPMUTE_GPIO_PIN	33

#elif defined( PID_DX470 )

#define AMPMUTE_GPIO_PIN	33
#define LCDPWER_GPIO_PIN	41
#define LCD_POWER_SET()			gpio_output(LCDPWER_GPIO_PIN, 1)
#define LCD_POWER_RESET()		gpio_output(LCDPWER_GPIO_PIN, 0)
#if defined PID_DX470_V25
#define LCD_RESET_PIN	    40
#define LCD_RESET_SET()			gpio_output(LCD_RESET_PIN, 1)
#define LCD_RESET_RESET()		gpio_output(LCD_RESET_PIN, 0)
#endif

#elif defined( PID_IX482 )
#define AMPMUTE_GPIO_PIN	33

#elif defined( PID_IXSE )
#define RGBR_GPIO_PIN	    69
#define RGBG_GPIO_PIN	    70
#define RGBB_GPIO_PIN	    71
#define AMPMUTE_GPIO_PIN	74
#define LCD_PWM_GPIO_PIN	76
#define DOORBELL_GPIO_PIN	72

#if defined( PID_IXSE_V2 )
#define A8_CTR_GPIO_PIN	    9
#define EXT_RING_SET()	        {gpio_output(A8_CTR_GPIO_PIN, 0);}
#define EXT_RING_RESET()	    {gpio_output(A8_CTR_GPIO_PIN, 1);}
#define LCDPWER_GPIO_PIN	68
#define LCD_POWER_SET()			gpio_output(LCDPWER_GPIO_PIN, 1)
#define LCD_POWER_RESET()		gpio_output(LCDPWER_GPIO_PIN, 0)
#define LCD_RESET_PIN	    75
#define LCD_RESET_SET()			gpio_output(LCD_RESET_PIN, 1)
#define LCD_RESET_RESET()		gpio_output(LCD_RESET_PIN, 0)
#endif

#define LCD_PWM_SET() 		    gpio_output(LCD_PWM_GPIO_PIN, 1)
#define LCD_PWM_RESET() 	    gpio_output(LCD_PWM_GPIO_PIN, 0)
#define RGB_R_TURN_SET()	    {gpio_output(RGBR_GPIO_PIN, 0);}
#define RGB_R_TURN_RESET()	    {gpio_output(RGBR_GPIO_PIN, 1);}
#define RGB_G_TURN_SET()	    {gpio_output(RGBG_GPIO_PIN, 0);}
#define RGB_G_TURN_RESET()	    {gpio_output(RGBG_GPIO_PIN, 1);}
#define RGB_B_TURN_SET()	    {gpio_output(RGBB_GPIO_PIN, 0);}
#define RGB_B_TURN_RESET()	    {gpio_output(RGBB_GPIO_PIN, 1);}


#elif defined( PID_DX482 )
#define AMPMUTE_GPIO_PIN	33
#define LCDPWER_GPIO_PIN	41
#define LCD_POWER_SET()			gpio_output(LCDPWER_GPIO_PIN, 1)
#define LCD_POWER_RESET()		gpio_output(LCDPWER_GPIO_PIN, 0)

#else
#define AMPMUTE_GPIO_PIN	76
#define LCD_PWM_GPIO_PIN	86
#define LCD_PWM_SET() 		gpio_output(LCD_PWM_GPIO_PIN, 1)
#define LCD_PWM_RESET() 	gpio_output(LCD_PWM_GPIO_PIN, 0)
#endif


#if !defined( PID_IX482 ) && !defined( PID_IXSE )&& !defined(PID_IX47) 	

#if defined PID_DX470_V25
#define SENSOR_POWER_GPIO_PIN  	42
#else
#define SENSOR_POWER_GPIO_PIN  	25
#endif
#define SENSOR_POWER_SET()			gpio_output(SENSOR_POWER_GPIO_PIN, 1)
#define SENSOR_POWER_RESET()		gpio_output(SENSOR_POWER_GPIO_PIN, 0)

#if defined PID_DX470_V25
#define RLCON_GPIO_PIN  	10
#else
#define RLCON_GPIO_PIN  	32
#endif
#define RLCON_SET()			gpio_output(RLCON_GPIO_PIN, 1)
#define RLCON_RESET()		gpio_output(RLCON_GPIO_PIN, 0)
#endif

#define AMPMUTE_SET() 		gpio_output(AMPMUTE_GPIO_PIN, 1)
#define AMPMUTE_RESET() 	gpio_output(AMPMUTE_GPIO_PIN, 0)


void gpio_initial(void);
void gpio_deinitial(void);
int gpio_output(int pin, int hi);
int gpio_input(int pin);

#endif
