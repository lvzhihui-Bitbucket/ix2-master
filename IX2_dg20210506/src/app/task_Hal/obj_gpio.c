
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "obj_gpio.h"

#define GPIO_EXPORT			"/sys/class/gpio/export"
#define GPIO_UNEXPORT		"/sys/class/gpio/unexport"
#define GPIO_DIRECTION		"/sys/class/gpio/gpio%d/direction"
#define GPIO_PULLENABLE		"/sys/class/gpio/gpio%d/input_enable"
#define GPIO_VALUE			"/sys/class/gpio/gpio%d/value"


#ifdef PID_IX481
static int gpio_output_pins[2]	={AMPMUTE_GPIO_PIN,LCD_PWM_GPIO_PIN};
#elif defined(PID_IXSE)

#if defined PID_IXSE_V2
static int gpio_output_pins[8]	={AMPMUTE_GPIO_PIN,LCD_PWM_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN,A8_CTR_GPIO_PIN,LCDPWER_GPIO_PIN,LCD_RESET_PIN};
static int gpio_input_pins[1]	={DOORBELL_GPIO_PIN};
#else
static int gpio_output_pins[5]	={AMPMUTE_GPIO_PIN,LCD_PWM_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN};
static int gpio_input_pins[1]	={DOORBELL_GPIO_PIN};
#endif

#else
#if	defined(PID_DX470)||defined(PID_DX482)
#if defined PID_DX470_V25
static int gpio_output_pins[5]	={AMPMUTE_GPIO_PIN,RLCON_GPIO_PIN,SENSOR_POWER_GPIO_PIN,LCDPWER_GPIO_PIN,LCD_RESET_PIN};
#else
static int gpio_output_pins[4]	={AMPMUTE_GPIO_PIN,RLCON_GPIO_PIN,SENSOR_POWER_GPIO_PIN,LCDPWER_GPIO_PIN};
#endif

#else
static int gpio_output_pins[2]	={AMPMUTE_GPIO_PIN};//gpio_output_pins[3]	={AMPMUTE_GPIO_PIN,RLCON_GPIO_PIN,SENSOR_POWER_GPIO_PIN};
#endif
#endif
void gpio_initial(void)
{
	char temp[201];	
	FILE *p=NULL;
	int i;

	// add output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}
	// set output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_output_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"out");
		fclose(p);	
	}

	#if	defined(PID_DX470)||defined(PID_DX482)
	//LCD_POWER_RESET();	// not process
	#endif	

#ifdef PID_IXSE
	RGB_R_TURN_RESET()
	RGB_B_TURN_RESET()
	// add input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_input_pins[i]);
		fclose(p);		
	}
	// set input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_input_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"in");
		fclose(p);	
	}
	// pull down enable
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		if( gpio_input_pins[i] != 0 )
		{
			snprintf(temp, 200, GPIO_PULLENABLE, gpio_input_pins[i]);
			p = fopen(temp,"w");
			fprintf(p,"1");
			fclose(p);	
		}	
	}
	LCD_RESET_SET();
#endif
}

void gpio_deinitial(void)
{
	FILE *p=NULL;
	int i;
	// del output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_UNEXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}
}

int gpio_output(int pin, int hi)
{
	char temp[201];	
	FILE *p=NULL;
	// set pin value
	//printf("!!!!!!!!!!!gpio_output:pin=%d,hi=%d\n",pin,hi);
	snprintf(temp, 200, GPIO_VALUE, pin);
	p = fopen(temp,"w");
	fprintf(p,"%d",hi);
	fclose(p);
	
	return 0;
}

int gpio_input(int pin)
{
	int ret;
	char temp[201];	
	FILE *p=NULL;
	// read pin value
	snprintf(temp, 200, GPIO_VALUE, pin);
	p = fopen(temp,"r");
	// seek to start
	fseek(p , 0 , 0);
	fread(temp , 1, 1 ,p);
	temp[1]=0;
	ret = atoi(temp);		
	fclose(p);
	
	return ret;
}

