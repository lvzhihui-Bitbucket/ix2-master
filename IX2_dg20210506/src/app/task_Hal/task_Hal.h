/**
  ******************************************************************************
  * @file    task_Hal.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.09.20
  * @brief   This file contains the headers of the power task_Power.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef TASK_HAL_H
#define TASK_HAL_H

#include "task_survey.h"
#include "obj_gpio.h"

extern int hal_fd;

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/* ---------------------- KEY_TYPE ---------------------*/
#define KEY_TYPE_PH				0 // physical key
#define KEY_TYPE_TS				1 // touch sense key
#define KEY_TYPE_HK				2 // hook key
#define KEY_TYPE_DB				3 // door bell
#define KEY_TYPE_TP				4 // touch pad key
#define KEY_TYPE_DIP			5 // DIP
#define KEY_TYPE_UDP			6 // UDP msg 
#define KEY_TYPE_BOOT			7 // 
#define KEY_TYPE_ALARM			8 // 
/* ---------------------- KEY_INDEX ---------------------*/

// touch key
#define KEY_TALK				0x01	
#define KEY_UNLOCK				0x02
#define KEY_POWER           	0x03
#define KEY_MENU                0x04       
#define KEY_UP                  0x05
#define KEY_DOWN                0x06
#define KEY_BACK                0x07
#define KEY_NO_DISTURB          0x08


// hook
#define KEY_KOOK				12

// door bell
#define KEY_DOOR_BELL			13

/* ---------------------- TOUCH_KEY_STATUS ---------------------*/
#define KEY_STATUS_PRESS		0
#define KEY_STATUS_RELEASE		1
#define KEY_STATUS_3SECOND		2

/* ---------------------- HOOK_STATUS ---------------------*/
#define HOOK_STATUS_UP			0
#define HOOK_STATUS_DOWN		1

/* ---------------------- TOUCH_PANEL_STATUS ---------------------*/
#define TOUCHNONE				0
#define TOUCHPRESS				1
#define TOUCH_3SECOND			2	//czn_20190216		//#define TOUCHMOVED				2
#define TOUCHEND				3
#define TOUCHCLICK				4
#define TOUCHDOUBLECLICK		5
#define TOUCHLONGCLICK			6
#define TOUCHMOVE				10
#define TOUCHMOVEUP				11
#define TOUCHMOVEDOWN			12
#define TOUCHMOVELEFT			13
#define TOUCHMOVERIGHT			14

#pragma pack(1)

typedef struct TP_POINT_
{
	unsigned short x;
	unsigned short y;
} TP_POINT;

typedef struct
{
	unsigned char keyType;		//key type
	unsigned char keyData;		//touch key data
	unsigned char keyStatus;	//public key status
	unsigned char keyStatus2;	//public key status		
	TP_POINT	  TpKey;		//touch panel coord
}KEY_DATA;

typedef struct 
{
	VDP_MSG_HEAD	head;
	struct timeval	happen_time;	//czn_20170111
	KEY_DATA		key;
} HAL_MSG_TYPE;

#define HAL_TYPE_TOUCH_KEY				1
#define HAL_TYPE_BRD					2	// 广播消息
#define HAL_TYPE_BRD_WITH_DATA			3	// 广播消息带数据

#pragma pack()

/*******************************************************************************
                        Define message structure
*******************************************************************************/


/*******************************************************************************
                              Define msg type  
*******************************************************************************/
#define MSG_KEY_DOWN_PRESS          	0
#define MSG_KEY_DOWN_RELEASE        	1
#define MSG_KEY_DOWN_3SECOND        2

#define MSG_KEY_UP_PRESS            	3
#define MSG_KEY_UP_RELEASE          	4
#define MSG_KEY_UP_3SECOND          	5

#define MSG_KEY_LEFT_PRESS          	6
#define MSG_KEY_LEFT_RELEASE        	7
#define MSG_KEY_LEFT_3SECOND        	8

#define MSG_KEY_RIGHT_PRESS         	9
#define MSG_KEY_RIGHT_RELEASE       	10
#define MSG_KEY_RIGHT_3SECOND       11

#define MSG_KEY_ESC_PRESS           	12
#define MSG_KEY_ESC_RELEASE         	13
#define MSG_KEY_ESC_3SECOND         	14

#define MSG_KEY_MONITOR_PRESS       	15
#define MSG_KEY_MONITOR_RELEASE     16
#define MSG_KEY_MONITOR_3SECOND   17

#define MSG_KEY_UNLOCK_PRESS        	18
#define MSG_KEY_UNLOCK_RELEASE      	19
#define MSG_KEY_UNLOCK_3SECOND     20

#define MSG_KEY_HOOK_UP             	21
#define MSG_KEY_HOOK_DOWN           	22

#define MSG_KEY_DOOR_BELL_PRESS     23

#define MSG_KEY_DIP_PRESS           	24

#define MSG_KEY_MENU_PRESS          	25
#define MSG_KEY_MENU_RELEASE        	26
#define MSG_KEY_MENU_3SECOND        27

#define MSG_KEY_TALK_PRESS          	28
#define MSG_KEY_TALK_RELEASE        	29
#define MSG_KEY_TALK_3SECOND        	30

#define MSG_KEY_LIGHT_PRESS         	31
#define MSG_KEY_LIGHT_RELEASE       	32
#define MSG_KEY_LIGHT_3SECOND       	33

#define MSG_KEY_UNLOCK2_PRESS       	34
#define MSG_KEY_UNLOCK2_RELEASE     35
#define MSG_KEY_UNLOCK2_3SECOND   36

/*******************************************************************************
                            Declare task 2 items
*******************************************************************************/
void GlobalResourceInit(void);

void vtk_TaskInit_Hal( int priority );
void vtk_TaskProcessEvent_Hal( void );
void vtk_TaskProcessEvent_Hal2( void );
void SendKeyMsg( KEY_DATA iKeyMsg );
void vtk_Task_Hal_Join( void );

#endif
