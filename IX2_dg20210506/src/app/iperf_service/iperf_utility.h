
#ifndef _IPERF_UTILITY_H_
#define _IPERF_UTILITY_H_

#include <stdio.h>
#include <libgen.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#define IPERF_BASE_STATUS_IDLE    	0
#define IPERF_BASE_STATUS_INIT    	1
#define IPERF_BASE_STATUS_RUN     	2
#define IPERF_BASE_STATUS_OVER		3

typedef void* (*msg_callback_func)(int type, char* pbuf, int len);

#define MAX_CMD_STR_LEN	    100
typedef struct
{
    int                     status;
    pthread_t				iperf_pid;
	pthread_attr_t 		    iperf_attr;    
	char					cmd[MAX_CMD_STR_LEN];
	msg_callback_func		process;	
} IPERF_BASE_INS_T;

/*****************************************************************************************************************
@function:
	iperf_base_start
@parameters:
    pins:       one instance ptr
    cmd:        shell command
	msg_cb:	    message callback process

@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int iperf_base_start(IPERF_BASE_INS_T* pins, char* cmd, msg_callback_func msg_cb );

/*****************************************************************************************************************
@function:
	iperf_base_stop
@parameters:
    pins:       one instance ptr
@return:
	0/ok, -1/error
******************************************************************************************************************/
int iperf_base_stop(IPERF_BASE_INS_T* pins);


typedef struct
{	
	int					IpAddr;				//Net device ip（4）
	unsigned short		subAddr;			//Sub device addr（2）
	//
	int					iperf_server;
	int					iperf_port;
	int					iperf_times;
	int					iperf_interval;	
	int					result;
}IPERF_UDP_REQ_T;

typedef struct
{	
	int					IpAddr;				//Net device ip（4）
	unsigned short		subAddr;			//Sub device addr（2）
	//
	int					iperf_server;
	int					iperf_port;
	int					iperf_times;
	int					iperf_interval;	
	int					result;
}IPERF_UDP_RSP_T;


int request_iperf_client_start(int target_ip, int server, short port, int times, int interval);
void response_iperf_client_start(void *pdata);

///////////////////////////////////////////////////////////////////////////////////////////////

#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER		1		// Kbytes/sec
#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_AVG		2		// Kbytes/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_PER		3		// Kbytes/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_AVG		4		// Kbytes/sec
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_PER		5		// (0%)
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_AVG		6		// (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_PER		7		// Kbytes/sec (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_AVG		8		// Kbytes/sec (0%)

int iperf_message_anaylizer( int total, char* pinbuf, int in_len, int out_type, char* poutbuf, int out_len );

#endif
