
#include "utility.h"
#include "iperf_utility.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"

#define BUFFER_CNT_MAX	                4
#define BUFFER_LEN_MAX	                50

static int kill_task(char* task_name)
{
	FILE *fp;
	char cmdbuf[BUFFER_LEN_MAX];
	char buffer[BUFFER_CNT_MAX][BUFFER_LEN_MAX];
	int account = 0;
	int i;
	snprintf(cmdbuf, sizeof(cmdbuf), "ps |grep %s", task_name);
	if( (fp = popen(cmdbuf, "r") ) == NULL)
	{
		printf("popen[%s] error!\n",task_name);
		return -1;
	}
	while( !feof(fp) )
	{
		for( i = 0; i < BUFFER_CNT_MAX; i++ ) memset(buffer[i], '\0', BUFFER_LEN_MAX);
		fscanf(fp, "%s %s %s %s", buffer[0], buffer[1], buffer[2], buffer[3]);
		if( strcmp( buffer[3], task_name ) == 0)
		{
			snprintf(cmdbuf, sizeof(cmdbuf), "kill %s", buffer[0]);
			system(cmdbuf);
			account ++;
		}
	}
	printf("total %d tasks be killed!\n", account);
	pclose(fp);
	return 0;
}

int iperf_wait_over( IPERF_BASE_INS_T* pins )
{
    // wait last over
    if( pins->status != IPERF_BASE_STATUS_IDLE )
    {
		int wait_to = 0;
		while(1)
		{
			if( pins->status != IPERF_BASE_STATUS_IDLE )
			{
				usleep(1000*1000);
				if( ++wait_to >= 30 )	// 30s
                {
                    pins->status = IPERF_BASE_STATUS_IDLE;
                    printf( "iperf_wait_over timeout !!!\n"); 
					return 1;
                }
 			}
			else
                break;
 		}
    }
    return 0;
}

static void callback_for_notify(IPERF_BASE_INS_T* pins, char* pbuf, int len )
{
    if( pins->process != NULL )
    {
        (*pins->process)(0,(void*)pbuf,len);
    }
}

#define IPERF_MAX_LINE_CHARS    250
void* task_iperf_base_process(void* arg )
{
	IPERF_BASE_INS_T* pins = (IPERF_BASE_INS_T*)arg;

	int ret = -1;
	FILE *fp = NULL;
	char iperf_line[IPERF_MAX_LINE_CHARS];
	
	printf( "task_iperf_base_process[%s]----\n",pins->cmd);
	
	if( (fp = popen( pins->cmd, "r" )) == NULL )
	{
		printf( "start iperf error \n" );
    	pins->status = IPERF_BASE_STATUS_IDLE;
        return NULL;
	}
	
	pins->status = IPERF_BASE_STATUS_RUN;

	if( fgets( iperf_line, IPERF_MAX_LINE_CHARS, fp ) == NULL )
	{
			callback_for_notify(pins, "connect failed: Connection refused", strlen("connect failed: Connection refused") );
	}
	else
	{
		while( fgets( iperf_line, IPERF_MAX_LINE_CHARS, fp ) != NULL )
		{
			callback_for_notify(pins, iperf_line, strlen(iperf_line) );
		}
	}
	
	pclose( fp );
    fp = NULL;	

    pins->status = IPERF_BASE_STATUS_IDLE;

	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ );

    return NULL;
}

/*****************************************************************************************************************
@function:
	iperf_base_start
@parameters:
    pins:       one instance ptr
    cmd:        shell command
	msg_cb:	    message callback process

@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int iperf_base_start(IPERF_BASE_INS_T* pins, char* cmd, msg_callback_func msg_cb )
{
	// wait idle
	iperf_wait_over(pins);

    // init
    pins->status    = IPERF_BASE_STATUS_INIT;
    memset( pins->cmd, 0, sizeof(pins->cmd));
    strcpy(pins->cmd,cmd);
    pins->process   = msg_cb;

    // start
	pthread_attr_init( &pins->iperf_attr );
	pthread_attr_setdetachstate(&pins->iperf_attr,PTHREAD_CREATE_DETACHED);
 	if( pthread_create(&pins->iperf_pid, 0, task_iperf_base_process, (void*)pins) )
	{
        pins->status = IPERF_BASE_STATUS_IDLE;
        printf( "api_iperf_client_start api_iperf_base_start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_base_start start ok !!!\n"); 
    return 0;
}

/*****************************************************************************************************************
@function:
	iperf_base_stop
@parameters:
    pins:       one instance ptr
@return:
	0/ok, -1/error
******************************************************************************************************************/
int iperf_base_stop(IPERF_BASE_INS_T* pins)
{
    kill_task("iperf");
	// wait idle
	iperf_wait_over(pins);
    printf( "api_iperf_base_stop start ok !!!\n"); 
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int request_iperf_client_start(int target_ip, int server, short port, int times, int interval)
{
	IPERF_UDP_REQ_T request;
	IPERF_UDP_RSP_T response = {0};
	unsigned int rlen = sizeof(IPERF_UDP_RSP_T);
	request.IpAddr 			= target_ip;
	request.subAddr 		= 0;
	request.iperf_server 	= server;
	request.iperf_port	 	= port;
	request.iperf_times		= times;
	request.iperf_interval	= interval;

	if(api_udp_c5_ipc_send_req(target_ip, CMD_IPERF_START_REQ, (char*)&request, sizeof(IPERF_UDP_REQ_T), (char*)&response, &rlen) == 0)
	{
		printf("!*!*!*!&!!!!!!ip = %08x saddr = %04x result = %d\n",response.IpAddr,response.subAddr,response.result);
		if(request.IpAddr ==  response.IpAddr  && response.result == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 2;	//ipdevice offline 
	}
}

void response_iperf_client_start(void *pdata)
{	
	char ip_str[20];
	UDP_MSG_TYPE* pudp = (UDP_MSG_TYPE*)pdata;
	IPERF_UDP_REQ_T *pRequest = (IPERF_UDP_REQ_T*)pudp->pbuf;
	IPERF_UDP_RSP_T response;

	memcpy(&response, pRequest, sizeof(IPERF_UDP_RSP_T));		

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		// response
		response.result = 0;		
		api_udp_c5_ipc_send_rsp( pRequest->iperf_server, pudp->cmd|0x80, pudp->id, (char*)&response, sizeof(IPERF_UDP_RSP_T));
		usleep(300*100);
		// start iperf client
		memset( ip_str, 0, 20);	
		ConvertIpInt2IpStr( pRequest->iperf_server,ip_str );

		printf("api_iperf_udp_client_start,server[%s]\n",ip_str);

		api_iperf_udp_client_start(ip_str,pRequest->iperf_port,pRequest->iperf_times,pRequest->iperf_interval,0,NULL);
	}
	else
	{
		response.result = 1;		
		api_udp_c5_ipc_send_rsp( pRequest->iperf_server, pudp->cmd|0x80, pudp->id, (char*)&response, sizeof(IPERF_UDP_RSP_T));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
tcp_client:
  3] 29.0-30.0 sec  4.63 MBytes  38.8 Mbits/sec
[  3]  0.0-30.0 sec   150 MBytes  42.0 Mbits/sec
udp server:
[  3] 28.0-29.0 sec   128 KBytes  1.05 Mbits/sec   0.230 ms    0/   89 (0%)
[  3] 29.0-30.0 sec   128 KBytes  1.05 Mbits/sec   0.007 ms    0/   89 (0%)
[  3]  0.0-30.0 sec  3.75 MBytes  1.05 Mbits/sec   0.087 ms    0/ 2677 (0%)
*/
#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER		1		// Mbits/sec
#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_AVG		2		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_PER		3		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_AVG		4		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_PER		5		// (0%)
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_AVG		6		// (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_PER		7		// Mbits/sec (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_AVG		8		// Mbits/sec (0%)

int iperf_message_get_band( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	strpos1 = strstr(pinbuf,"ytes");
	strpos2 = strstr(pinbuf,"/sec");	
	strlen = strpos2 - strpos1 -2;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1+4, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_time( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	strpos1 = strstr(pinbuf,"]");
	strpos2 = strstr(pinbuf," sec");
	strlen = strpos2 - strpos1;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1+2, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_lose( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	strpos1 = strstr(pinbuf,"(");
	strpos2 = strstr(pinbuf,")");
	strlen = strpos2 - strpos1+1;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_connect_failed( char* pinbuf, char* poutbuf )
{
	char *strpos1;
	strpos1 = strstr(pinbuf,"connect failed");
	if( strpos1 != NULL )
	{
		strncpy( poutbuf, strpos1, strlen("connect failed"));
		return strlen("connect failed");
	}
	else
		return 0;
}

int iperf_message_anaylizer( int total, char* pinbuf, int in_len, int out_type, char* poutbuf, int out_len )
{
	char strtime[30];
	char strband[30];
	char strlose[30];

	memset( strtime, 0, 30);
	memset( strband, 0, 30);
	memset( strlose, 0, 30);
	memset( poutbuf, 0, out_len);

	if( out_type == IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER || out_type == IPERF_MSG_TCP_CLIENT_BANDWIDTH_AVG )
	{
		if( iperf_message_get_connect_failed(pinbuf,strlose) != 0 )
		{
			strcat(poutbuf,strlose);
			strcat(poutbuf,"               ");
			printf("connect status,outbuf[%s]\n",poutbuf);
		}
		else
		{
			iperf_message_get_time(pinbuf,strtime);
			iperf_message_get_band(pinbuf,strband);

			strcat(poutbuf,strtime);
			strcat(poutbuf,strband);
			strcat(poutbuf,"   ");

			if( strlen(poutbuf) < 10 ) strcat(poutbuf,"                 ");

			printf("time[%s],band[%s],outbuf[%s]\n",strtime,strband,poutbuf);
		}
	}
	else if( out_type == IPERF_MSG_UDP_SERVER_BAND_LOSE_PER || out_type == IPERF_MSG_UDP_SERVER_BAND_LOSE_AVG )
	{
			iperf_message_get_time(pinbuf,strtime);
			iperf_message_get_band(pinbuf,strband);
			iperf_message_get_lose(pinbuf,strlose);

			memset( poutbuf, 0, out_len);

			strcat(poutbuf,strtime);
			strcat(poutbuf,strband);
			strcat(poutbuf,strlose);
			strcat(poutbuf,"   ");

			if( strlen(poutbuf) < 10 ) strcat(poutbuf,"                 ");

			printf("time[%s],band[%s],lose[%s], outbuf[%s]\n",strtime,strband,strlose,poutbuf);
	}

	return 0;
}


