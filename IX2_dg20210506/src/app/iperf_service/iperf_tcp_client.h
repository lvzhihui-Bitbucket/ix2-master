
#ifndef _IPERF_TCP_CLIENT_H_
#define _IPERF_TCP_CLIENT_H_

#include "iperf_utility.h"

/*****************************************************************************************************************
@function:
	api_iperf_tcp_client_start
@parameters:
	server:	    server host			eg: vdpconnect.com, 47.91.88.33
	port:	    server port			eg: 6789
	period:		send time			eg: 3 (max 30)
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	callback msg mode	eg: 0/bandwidth message per interval, 1/average bandwidth message in period
	msg_cb:	    message callback process
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_tcp_client_start( char* server, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb );

/*****************************************************************************************************************
@function:
	api_iperf_tcp_client_stop
@parameters:
    none
@return:
	0/ok, -1/error
******************************************************************************************************************/
int api_iperf_tcp_client_stop( void );

#endif
