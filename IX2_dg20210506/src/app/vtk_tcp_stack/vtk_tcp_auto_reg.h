
#ifndef _VTK_TCP_AUTO_REG_H
#define _VTK_TCP_AUTO_REG_H

extern char* VTK_AUTO_REG_SERVER_IPADD0;
extern char* VTK_AUTO_REG_SERVER_IPADD1;
extern char* VTK_AUTO_REG_SERVER_IPADD2;

extern char* VTK_AUTO_REG_SERVER_IP0;
extern char* VTK_AUTO_REG_SERVER_IP1;
extern char* VTK_AUTO_REG_SERVER_IP2;


#define VTK_AUTO_REG_SERVER_PORT		8848

#define		ACCOUNT_MANAGE_CMD_NONE				0 
#define 	ACCOUNT_MANAGE_CMD_REGISTER			1
#define 	ACCOUNT_MANAGE_CMD_UNREGISTER		2
#define 	ACCOUNT_MANAGE_CMD_CHANGEPSW		3
#define 	ACCOUNT_MANAGE_CMD_CLEARUL			4
#define 	ACCOUNT_MANAGE_CMD_CLEARUL2			5	// 密码为字符串类型
#define 	ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT	6	// 密码为字符串类型

typedef struct _AUTOREG_REQ_
{	
	int		cmd;			// 0: unreg, 1: reg, 2: change password, 3: clear ul contracts
	int		state;			// 0
	char 	sip_account[16];
	int		sip_password;
}AUTOREG_REQ;

typedef struct _AUTOREG_RSP_
{
	int		cmd;			// 0: unreg, 1: reg, 2: change password, 3: clear ul contracts
	int		state;			// 0
	char 	sip_account[16];
	int		sip_password;
}AUTOREG_RSP;

// lzh_20180726_s
typedef struct _CHANGE_PASSWORD_REQ_
{	
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	char	sip_new_password[16];
	int		checksum;
}CHANGE_PASSWORD_REQ;

typedef struct _CHANGE_PASSWORD_RSP_
{
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_old_password[16];
	char	sip_new_password[16];
	int		checksum;
}CHANGE_PASSWORD_RSP;


typedef struct _UNREGISTER_REQ_
{	
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}UNREGISTER_REQ;

typedef struct _UNREGISTER_RSP_
{
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}UNREGISTER_RSP;

typedef struct _CLEAR_UL_REQ_
{	
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}CLEAR_UL_REQ;

typedef struct _CLEAR_UL_RSP_
{
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}CLEAR_UL_RSP;

typedef struct _RESTORE_DEFAULT_REQ_
{	
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}RESTORE_DEFAULT_REQ;

typedef struct _RESTORE_DEFAULT_RSP_
{
	int		cmd;
	int		state;			// 0
	char 	sip_account[20];
	char	sip_password[16];
	int		checksum;	
}RESTORE_DEFAULT_RSP;

// lzh_20180726_e

typedef struct _SERVICE_PACK_INFO_
{
	union
	{
			AUTOREG_REQ	auto_reg_req;
			AUTOREG_RSP	auto_reg_rsp;			
			// lzh_20180726_s
			CHANGE_PASSWORD_REQ change_psw_req;
			CHANGE_PASSWORD_RSP change_psw_rsq;
			CLEAR_UL_REQ		clear_ul_req;
			CLEAR_UL_RSP		clear_ul_rsp;			
			UNREGISTER_REQ		unregister_req;
			UNREGISTER_RSP		unregister_rsp;			
			RESTORE_DEFAULT_REQ	restore_default_req;
			RESTORE_DEFAULT_RSP	restore_default_rsp;			
			// lzh_20180726_e			
	};
}SERVICE_PACK_INFO;

/*
cmd:				command
server_ip:		server
sip_account:		sip account string
sip_psw:			sip account password
return:			-1/net err, 0/reg ok, 1/already reg
*/
int remote_account_manage( int cmd, char* server_ip, char* sip_account, char* sip_psw, char* sip_psw2 );
#define auto_register_to_server0(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_REGISTER,VTK_AUTO_REG_SERVER_IP0,sip_account,sip_psw,NULL )

#define auto_register_to_server1(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_REGISTER,VTK_AUTO_REG_SERVER_IP1,sip_account,sip_psw,NULL )

#define auto_register_to_server2(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_REGISTER,VTK_AUTO_REG_SERVER_IP2,sip_account,sip_psw,NULL )

#define auto_clearul_to_server0(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL,VTK_AUTO_REG_SERVER_IP0,sip_account,sip_psw,NULL )

#define auto_clearul_to_server1(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL,VTK_AUTO_REG_SERVER_IP1,sip_account,sip_psw,NULL )

#define auto_clearul_to_server2(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL,VTK_AUTO_REG_SERVER_IP2,sip_account,sip_psw,NULL )

// lzh_20180726_s
#define change_passord_to_server0(sip_account, sip_old_psw, sip_new_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_CHANGEPSW,VTK_AUTO_REG_SERVER_IP0,sip_account,sip_old_psw,sip_new_psw )
#define change_passord_to_server1(sip_account, sip_old_psw, sip_new_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_CHANGEPSW,VTK_AUTO_REG_SERVER_IP1,sip_account,sip_old_psw,sip_new_psw )
#define change_passord_to_server2(sip_account, sip_old_psw, sip_new_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_CHANGEPSW,VTK_AUTO_REG_SERVER_IP2,sip_account,sip_old_psw,sip_new_psw )

#define clear_locations_to_server0(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,VTK_AUTO_REG_SERVER_IP0,sip_account,sip_psw,NULL )
#define clear_locations_to_server1(sip_account, sip_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,VTK_AUTO_REG_SERVER_IP1,sip_account,sip_psw,NULL )
#define clear_locations_to_server2(sip_account, sip_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,VTK_AUTO_REG_SERVER_IP2,sip_account,sip_psw,NULL )

#define restore_default_psw_to_server0(sip_account, sip_psw) \
		remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,VTK_AUTO_REG_SERVER_IP0,sip_account,sip_psw,NULL )
#define restore_default_psw_to_server1(sip_account, sip_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,VTK_AUTO_REG_SERVER_IP1,sip_account,sip_psw,NULL )
#define restore_default_psw_to_server2(sip_account, sip_psw) \
			remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,VTK_AUTO_REG_SERVER_IP2,sip_account,sip_psw,NULL )

/*
// 0：更改密码ok
// -1：数据包校验错误
// -2：账号不存在
// -3：帐号密码错误
// -4：数据库操作无效
// -5：不能连接到服务器
// -6：服务器返回的数据包checksum错误	
*/
void printf_change_divert_psw_result( char* sip_server, int result );

/*
// 0-x：清除UL个数
// -1：数据包校验错误
// -2：账号不存在
// -3：账号已存在, 密码错误
// -4：数据库操作无效
// -5：不能连接到服务器
// -6：服务器返回的数据包checksum错误	
*/			
void printf_clear_url_result( char* sip_server, int result );

/*
// 0：账号已存在，恢复了缺省密码
// 1：账号不存在，创建了新账号
// -1：数据包校验错误
// -2：账号已存在, 已为缺省密码
// -3：数据库操作无效	
// -4：不能连接到服务器
// -5：服务器返回的数据包checksum错误	
*/
void printf_restore_default_psw_result( char* sip_server, int result );

// lzh_20180726_e

#endif



