
#ifndef _VTK_UDP_STACK_DEVICE_UPDATE_H
#define _VTK_UDP_STACK_DEVICE_UPDATE_H

#include "vtk_udp_stack_class_ext.h"

#define PUBLIC_UDP_CMD_REQ_DATA_LEN				500
#define PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN		950

#define	VDP_PRINTF_DEVICE_UPDATE

#ifdef	VDP_PRINTF_DEVICE_UPDATE
#define	dev_update_printf(fmt,...)	printf("[DECVICE_UPDATE]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	dev_update_printf(fmt,...)
#endif


#define CLIENT_MAX_NUM		5					//�������5���ͻ���ͬʱ������ָͬ��

#define DEVICE_TYPE								"DT-IPG24"			// �豸����

#define DEVICE_SEARCH_MULTICAST_ADDR			"236.6.6.1"			// �鲥��ַ
#define DEVICE_SEARCH_UDP_BOARDCAST_PORT		25007			// �鲥�����������豸

#define DEVICE_RING_REQUEST						0x9A00			// �豸ring ����
#define DEVICE_SEARCH_CMD_READ_REQ				0x9A01			// �豸������������
#define DEVICE_SEARCH_CMD_READ_RSP				0x9A02			// �豸��������Ӧ��
#define DEVICE_SEARCH_CMD_WRITE_REQ				0x9B01			// �豸������������
#define DEVICE_SEARCH_CMD_WRITE_RSP				0x9B02			// �豸��������Ӧ��
#define DEVICE_REQUEST_WRITE_NODE_ID_REQ			0x9C02			// �豸����NODE ID����
#define DEVICE_REQUEST_WRITE_NODE_ID_RSP			0x9C03			// �豸����NODE IDӦ��
#define DEVICE_TIP_REQUEST							0x9D01			// �豸tip ����
#define DEVICE_IP_HEART_BEAT_SERVICE			0x9F01			//IP�豸��������

#define GET_IP_BY_NUMBER_REQ					0xA001			//GET�豸IP BY NUMBER����
#define GET_IP_BY_NUMBER_RSP					0xA002			//GET�豸IP BY NUMBERӦ��

#define GET_IP_BY_MFG_SN_REQ					0xA003			//GET�豸IP BY MGF_SN����
#define GET_IP_BY_MFG_SN_RSP					0xA004			//GET�豸IP BY MGF_SNӦ��

#define GET_INFO_BY_IP_REQ						0xA005			//��ȡ�豸��Ϣ����
#define GET_INFO_BY_IP_RSP						0xA006			//��ȡ�豸��ϢӦ��

#define SEARCH_IP_BY_FILTER_REQ					0xA007			//SEARCH�豸IP BY MGF_SN����
#define SEARCH_IP_BY_FILTER_RSP					0xA008			//SEARCH�豸IP BY MGF_SNӦ��

#define PROG_INFO_BY_IP_REQ						0xA009			//д�豸��Ϣ����
#define PROG_INFO_BY_IP_RSP						0xA00A			//дȡ�豸��ϢӦ��

#define GET_ABOUT_BY_IP_REQ						0xA00B			//��ȡ�豸ABOUT��Ϣ����
#define GET_ABOUT_BY_IP_RSP						0xA00C			//��ȡ�豸ABOUT��ϢӦ��

#define CMD_REMOTE_UPGRADE_REQ					0xA00D			//Զ�̸�������
#define CMD_REMOTE_UPGRADE_RSP					0xA00E			//Զ�̸���Ӧ��

#define CMD_REMOTE_SOFTWARE_UPDATE_REQ			0xA00F			//Զ��update ����		//czn_20190506
#define CMD_REMOTE_SOFTWARE_UPDATE_RSP			0xA010			//Զ��update Ӧ��

#define MS_SYNC_REQ					0xA011	//czn_20190221		
#define MS_SYNC_RSP						0xA012
	#define MS_SYNC_OP_SetCallScene		0
	#define MS_SYNC_OP_List					1
	#define MS_SYNC_OP_CallRecord			2
	#define MS_SYNC_OP_SipConfig			3

#define CMD_SIP_CONFIG_REQ						0xA013			//sip ��������
#define CMD_SIP_CONFIG_RSP						0xA014			//sip ����Ӧ��

#define GET_GetCallBtnBindingState_REQ			0xA015			//��ȡ�豸��״̬����
#define GET_GetCallBtnBindingState_RSP			0xA016			//��ȡ�豸��״̬Ӧ��

#define CallBtnBinding_REQ						0xA017			//�豸��״̬����
#define CallBtnBinding_RSP						0xA018			//�豸��״̬Ӧ��


#define CMD_ASK_DEBUG_STATE_REQ					0xA019			//ѯ���豸�Ƿ���debugģʽ����
#define CMD_ASK_DEBUG_STATE_RSP					0xA01A			//ѯ���豸�Ƿ���debugģʽӦ��

#define CMD_EXIT_DEBUG_STATE_REQ				0xA01B			//�˳�debugģʽ

//czn_20190520_s
#define CMD_RES_UPLOAD_REQ						0xA01C
#define CMD_RES_UPLOAD_RSP						0xA01D	
#define CMD_RES_DOWNLOAD_REQ					0xA01E	
#define CMD_RES_DOWNLOAD_RSP					0xA01F
#define CMD_RES_TRANS_RESULT					0xA020
//czn_20190520_e

#define SEARCH_IP_BY_FILTER2_REQ				0xA021			//SEARCH�豸IP
#define CMD_CHANGE_INSTALL_PWD_REQ				0xA022			//�����޸Ĺ�������
#define CMD_CHANGE_INSTALL_PWD_RSP				0xA023			//�����޸Ĺ�������

#define CMD_BACKUP_AND_RESTORE_REQ				0xA024			//�ָ��ͱ�������
#define CMD_BACKUP_AND_RESTORE_RSP				0xA025			//�ָ��ͱ���Ӧ��

#define CMD_UNLOCK_STATE_REPORT					0xA026			//����״̬�㱨

#define SEARCH_IX_PROXY_REQ						0xA027			//SEARCH_IX_PROXY_REQ
#define SEARCH_IX_PROXY_RSP						0xA028			//SEARCH_IX_PROXY_REQ

#define SEARCH_IX_PROXY_LINKED_REQ				0xA029			//SEARCH_IX_PROXY_LINKED_REQ
#define SEARCH_IX_PROXY_LINKED_RSP				0xA02A			//SEARCH_IX_PROXY_LINKED_REQ

#define IX_REQ_Prog_Call_Nbr_REQ				0xA02B			//IX�豸���뷿������
#define IX_REQ_Prog_Call_Nbr_RSP				0xA02C			//IX�豸���뷿�������Ӧ��

#define IX_REQ_Tip_REQ							0xA02D			//IX�豸tip����
#define IX_REQ_Tip_RSP							0xA02E			//IX�豸tipӦ��

#define IX_REQ_Ring_REQ							0xA02F			//IX�豸Ring����
#define IX_REQ_Ring_RSP							0xA030			//IX�豸RingӦ��

#define IX_PROG_IP_ADDR_REQ						0xA031			//IX�豸�޸�IX�豸IP��ַ����
#define IX_PROG_IP_ADDR_RSP						0xA032			//IX�豸�޸�IX�豸IP��ַӦ��

#define IX_Report_REQ							0xA033			//IX�豸��������
#define IX_Report_RSP							0xA034			//IX�豸����Ӧ��

#define IX_DEVICE_RES_DOWNLOAD_REQ				0xA035			//Զ��RES_DOWNLOAD ����
#define IX_DEVICE_RES_DOWNLOAD_RSP				0xA036			//Զ��RES_DOWNLOAD Ӧ��

#define GET_MERGE_INFO_BY_IP_REQ				0xA037			//��ȡ�豸��Ϣ����
#define GET_MERGE_INFO_BY_IP_RSP				0xA038			//��ȡ�豸��ϢӦ��

#define NOTICE_IXMINI_REQ						0xA039			//����֪ͨ���ݿ�����
#define NOTICE_IXMINI_RSP						0xA03A			//����֪ͨ���ݿ�����Ӧ��

#define SettingFromR8001_REQ					0xA03B			//SettingFromR8001����
#define SettingFromR8001_RSP					0xA03C			//SettingFromR8001Ӧ��
	
#define SetTimeServer_REQ						0xA03D			//����ʱ�����������
#define SetTimeServer_RSP						0xA03E			//����ʱ�������Ӧ��

#define Backup_REQ								0xA03F			//��������
#define Backup_RSP								0xA040			//����Ӧ��

#define Restore_REQ								0xA041			//�ָ�����
#define Restore_RSP								0xA042			//�ָ�Ӧ��

#define FileManagement_REQ						0xA043			//�ļ���������
#define FileManagement_RSP						0xA044			//�ļ��ָ�Ӧ��

#define TFTP_WRITE_FILE_REQ						0xA045			//TFTP�����ļ�
#define TFTP_WRITE_FILE_RSP						0xA046			//

#define TFTP_READ_FILE_REQ						0xA047			//TFTP�����ļ�
#define TFTP_READ_FILE_RSP						0xA048			//

#define TFTP_CHECK_FILE_REQ						0xA049			//TFTP�����ļ�
#define TFTP_CHECK_FILE_RSP						0xA04A			//

#define SHELL_REQ								0xA04B			//shell请求
#define SHELL_RSP								0xA04C			//shell应答

#define GET_PB_BY_IP_REQ						0xA04F			//获取设备看板信息请求
#define GET_PB_BY_IP_RSP						0xA050			//获取设备看板信息应答

#define GET_RW_IO_BY_IP_REQ						0xA051			//读写IO参数请求
#define GET_RW_IO_BY_IP_RSP						0xA052			//读写IO参数应答

#define EVENT_REQ								0xA053			//Event请求
#define EVENT_RSP								0xA054			//Event应答

#define UPDATE_DEV_TABLE_REQ					0xA055			//请求更新设备表
#define UPDATE_DEV_TABLE_RSP					0xA056			//请求更新设备表应答
#define UPDATE_DEV_TABLE_INFORM					0xA057			//设备表更新通知

#define TABLE_PROCESS_REQ						0xA058			//远程表处理请求
#define TABLE_PROCESS_RSP						0xA059			//远程表处理应答

#define SET_DIVERT_STATE_REQ					0xA05A			//设置转呼状态请求
#define SET_DIVERT_STATE_RSP					0xA05B			//设置转呼状态应答

#define GET_QR_CODE_REQ							0xA05C			//获取二维码字符串请求
#define GET_QR_CODE_RSP							0xA05D			//获取二维码字符串应答

#define IX_Ring2_REQ							0xA05E			//Ring2请求
#define IX_Ring2_RSP							0xA05F			//Ring2应答

#define MUL_CTRL_BY_SIPACC_REQ					0x8888
#define MUL_CTRL_BY_SIPACC_RSP					0x8889

#define UDP_WAIT_RSP_TIME						20				//�ȴ�Ӧ��ʱ�� ms
#define WLAN_SendTimes							3				//WiFi�·��ͼ���
#define LAN_SendTimes							2				//Lan�·��ͼ���

#pragma pack(1)

//ip head struct define
typedef struct
{
	char 	s;
	char 	w;
	char 	i;
	char 	t;
	char 	c;
	char 	h;
} device_search_head;

typedef struct
{
	unsigned short	cmd;
} device_search_cmd;

// �豸�����������ݰ�
typedef struct
{
	char		req_source_zero[6];		// ����6���ֽ�0
	int		req_source_ip;			// ����PC�˵�ip��ַ
	char		req_target_zero[6];		// ����6���ֽ�0
	int		req_target_ip_seg;		// ����PC�˵�ip��ַ���ڵ�����
	char		mfgSn[12];				//���к�
	uint16	ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];			// �豸����
} device_search_read_req;

// �豸����Ӧ�����ݰ�
typedef struct
{
	char		req_source_zero[6];		// ����6���ֽ�0
	int		req_source_ip;			// ����PC�˵�ip��ַ
	char		req_target_zero[6];		// ����6���ֽ�0
	int		rsp_target_ip;				// Ӧ���豸��ip��ַ
	int		rsp_target_mask;			// Ӧ���豸��mask����
	int		rsp_target_gateway;		// Ӧ���豸������
	char		rsp_target_mac[6];		// Ӧ���豸��mac��ַ
	char		mfgSn[12];				//���к�
	uint16	ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];			// �豸����
	uint8	version;					
	uint8	ipCfgLimit;				
} device_search_read_rsp;

// �豸�����������ݰ�
typedef struct
{
	int		req_target_old_ip;		// ָ���豸��ԭip��ַ
	int		req_target_new_ip;		// ָ���豸����ip��ַ
	int		req_target_new_mask;	// ָ���豸����mask����
	int		req_target_new_gateway;	// ָ���豸��������
	char		req_target_new_mac[6];	// ָ���豸����mac
	char		mfgSn[12];				//���к�
	uint16	ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];			// �豸����
} device_search_write_req;

// �豸����Ӧ�����ݰ�
typedef struct
{
	int		rsp_target_old_ip;		// Ӧ���豸��ԭip��ַ
	int		rsp_target_new_ip;		// Ӧ���豸����ip��ַ
	int		rsp_target_new_mask;	// Ӧ���豸����mask����
	int		rsp_target_new_gateway;	// Ӧ���豸��������
	char		rsp_target_new_mac[6];	// Ӧ���豸����mac
	char		mfgSn[12];				//���к�
	uint16	ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];			// �豸����
} device_search_write_rsp;

typedef struct
{
	device_search_head	head;
	device_search_cmd	cmd;
	union
	{
		char						dat[1];
		device_search_read_req		read_req;
		device_search_read_rsp		read_rsp;
		device_search_write_req		write_req;
		device_search_write_rsp		write_rsp;		
	}buf;
} device_search_package;

// �豸����дnode id
typedef struct
{
	uint8		requestFlag[8];			//������
	char		mfgSn[12];				//���к�
	uint16		ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];			// �豸����
	char		mac[6];					//mac
	int			upgradedTime;			//����ʱ��
} WRITE_NODE_ID_REQ_T;

// �豸Ӧ��дnode id
typedef struct
{
	uint8		requestFlag[8];			//������
	char		mfgSn[12];				//���к�
	uint16		ipNodeId;				//�ڵ�ID
	char		ipDeviceType[18];		// �豸����
	char		mac[6];					//mac
	int			upgradedTime;			//����ʱ��
	uint8		result;
} WRITE_NODE_ID_RSP_T;

typedef struct
{
	int				rand;											//�����
	int				sourceIp;										//Դip
	char			data[PUBLIC_UDP_CMD_REQ_DATA_LEN+1];			//information
} PublicUdpCmdReq;

typedef struct
{
	int						rand;				//�����
	int						sourceIp;			//Դip
	int						result;				//result
} PublicUdpCmdRsp;

typedef struct
{
	int				waitFlag;			//�ȴ���Ӧ���
	int				rand;				//�����
	int				sourceIp;			//Դip
	int				result;
}PublicUdpCmdRun;

typedef struct
{
	int				rand;											//�����
	int				sourceIp;										//Դip
	int				dataLen;										//��������ݳ���
	char			data[PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN];
} PublicMulRspUdpCmdReq;

typedef struct
{
	int						rand;				//�����
	int						sourceIp;			//Դip
	int						packetNo;			//Ӧ������
	int						packetTotal;		//Ӧ������
	int						result;				//result
	int 					dataLen;			//��������ݳ���
	char					data[PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN];
} PublicMulRspUdpCmdRsp;

typedef int (*MulRspCmdProcess)(const char*, char**, int*);
typedef struct
{
	int					no;					//Ӧ������
	int					dataLen;			//���ݳ���
	char*				data;				//����Ӧ������
}PublicMulRspPacket;

typedef struct
{
	int					waitFlag;			//�ȴ���Ӧ���
	int					rand;				//�����
	int					sourceIp;			//Դip
	int					result;
	MulRspCmdProcess	process;			//��������
	int					packetCnt;			//Ӧ������
	sem_t 				sem;
	PublicMulRspPacket*	packet				//Ӧ���					
}PublicMulRspUdpCmdRun;

typedef struct
{
	int				rand;				//�����
	int				sourceIp;			//Դip
}IXReq_T;
#pragma pack()

typedef struct
{
	udp_comm_rt 		udp;				//	udp����ģ��ʵ��
	send_sem_id_array	waitrsp_array;		// 	ҵ��Ӧ��ͬ������	
}device_update_instance_t;

// ��ʼ��udp:25000�ķ���ʵ��
int init_device_update_instance( void );

// ͨ��udp:25007�˿ڷ�������
int api_udp_device_update_send_data(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len );
int api_udp_device_update_send_data_by_device(char* net_device_name, int target_ip, unsigned short cmd, const char* pbuf, unsigned int len );

// ͨ��udp:25007�˿ڷ������ݰ��󣬲��ȴ�ҵ��Ӧ�𣬵õ�ҵ��Ӧ�������
int api_udp_device_update_send_req(int target_ip, unsigned short cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen);

// ���յ�udp:25007�˿����ݰ��������ҵ��Ӧ��
int api_udp_device_update_send_rsp( int target_ip, unsigned short cmd, int id, const char* pbuf, unsigned int len );

// ���յ�udp:25007�˿����ݰ��Ļص�����
int api_udp_device_update_recv_callback(int target_ip, unsigned short cmd, char* pbuf, unsigned int len );



void DeviceSearchReadProcess(int target_ip, device_search_read_req* pReadReq);
void DeviceSearchWriteProcess(int target_ip, device_search_write_req* pWriteReq);
void DeviceWriteNodeIdProcess(int target_ip, WRITE_NODE_ID_REQ_T* pWriteNodeIdReq);


#endif

