
#ifndef _VTK_UDP_STACK_C5_IPC_CMD_H
#define _VTK_UDP_STACK_C5_IPC_CMD_H

#include "vtk_udp_stack_class.h"

#define	C5_IPC_CMD_RCV_PORT	25000
#define C5_IPC_CMD_TRS_PORT	25000

#define	VDP_PRINTF_C5_IPC

#ifdef	VDP_PRINTF_C5_IPC
#define	c5_ipc_printf(fmt,...)	printf("[C5-IPC]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	c5_ipc_printf(fmt,...)
#endif

#define CMD_CALL_MON_LINK_REQ			0x1001
#define CMD_CALL_MON_LINK_RSP			0x1081

#define CMD_CALL_MON_LINK_MODIFY_REQ			0x1021		//czn_20190527
#define CMD_CALL_MON_LINK_MODIFY_RSP			0x10A1

#define CMD_CALL_MON_UNLINK_REQ			0x1002
#define CMD_CALL_MON_UNLINK_RSP			0x1082

#define CMD_NET_MANAGE_REQ				0x8001		// 网络管理命令请求
#define CMD_NET_MANAGE_RSP				0x8081		// 网络管理命令应答

// 摄像机调节指令
#define CMD_CAM_REMOTE_ADJUST_REQ		0x0010		// 远程调节摄像头申请
#define CMD_CAM_REMOTE_ADJUST_RSP		0x0090		// 远程调节摄像头应答
//czn_20170803_s
#define CMD_CAM_REMOTE_GETPARA_REQ		0x0011		// 
#define CMD_CAM_REMOTE_GETPARA_RSP		0x0091		// 
//czn_20170803_e
// lzh_20160622_s
#define CMD_DEVICE_TYPE_CODE_VER_REQ		0x9001		// 远程查询设备类型，及code版本信息
#define CMD_DEVICE_TYPE_CODE_VER_RSP		0x9081		// 远程查回复设备类型，及code版本信息

#define CMD_DEVICE_TYPE_DOWNLOAD_START_REQ	0x9002		// 
#define CMD_DEVICE_TYPE_DOWNLOAD_START_RSP	0x9082		// 

#define CMD_DEVICE_TYPE_DOWNLOAD_STOP_REQ	0x9003		// 
#define CMD_DEVICE_TYPE_DOWNLOAD_STOP_RSP	0x9083		// 

#define CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_REQ	0x9004		// 
#define CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_RSP	0x9084		// 

#define CMD_DEVICE_TYPE_UPDATE_START_REQ	0x9005		// 
#define CMD_DEVICE_TYPE_UPDATE_START_RSP	0x9085		// 

#define CMD_DEVICE_TYPE_UPDATE_OVER_REQ		0x9006		// 
#define CMD_DEVICE_TYPE_UPDATE_OVER_RSP		0x9086		// 
// lzh_20160622_e

//cao_20170304

//设备在线检测
#define CMD_DEVIC_SINGLE_LINKING_REQ				0x2001		// 单设备在线申请
#define CMD_DEVIC_SINGLE_LINKING_RSP				0x2081		//单设备在线回复

#define CMD_DEVIC_MULTIPLE_LINKING_START_REQ	0x2002		// 多设备在线检测开始申请
#define CMD_DEVIC_MULTIPLE_LINKING_START_RSP	0x2082		//多设备在线检测开始回复

#define CMD_DEVIC_MULTIPLE_LINKING_REPORT_REQ	0x2003		// 多设备在线检测报告申请
#define CMD_DEVIC_MULTIPLE_LINKING_REPORT_RSP	0x2083		//多设备在线检测报告回复

//设备重启
#define CMD_DEVIC_REBOOT_REQ				0xE001		//通知设备重启申请
#define CMD_DEVIC_REBOOT_RSP				0xE081		//通知设备重启回复
//设备恢复出厂设置
#define CMD_DEVIC_RECOVER_REQ			0xE002		//申请设备恢复出厂设置
#define CMD_DEVIC_RECOVER_RSP			0xE082		//回复设备恢复出厂设置
//czn_20170712_s
#define CMD_MON_OPENCMR_REQ			0xE021		//
#define CMD_MON_CLOSECMR_REQ			0xE022
#define CMD_MON_TALK_REQ				0xE023
#define CMD_MON_UNLOCK_REQ			0xE024
#define CMD_MON_LINK_REQ				0xE025
#define CMD_MON_QSW_RESID				0xE026
#define CMD_MON_OPENNEXTCMR_REQ		0xE027
//czn_20170712_e

#define CMD_DOORBELL_REQ				0xE031	//czn_20170809
//cao_20170304

#define CMD_CALLBACK_REQ				0xE032	//czn_20190216
#define CMD_CALLBACK_RSP				0xE0B2	
//cao_20170304
//czn_20190221_s
#define CMD_DXFILETRS_SERVER_REQ			0xE041
#define CMD_DXFILETRS_SERVER_RSP			0xE0C1
#define CMD_DXFILETRS_CLEINT_REQ			0xE042
#define CMD_DXFILETRS_CLEINT_RSP			0xE0C2
#define CMD_DXFILETRS_VERIFY_REQ			0xE043
#define CMD_DXFILETRS_VERIFY_RSP			0xE0C3
//czn_20190221_e

//czn_20190412_s
#define CMD_AUTOTEST_START_REQ			0xE051
#define CMD_AUTOTEST_START_RSP			0xE0D1
#define CMD_AUTOTEST_CALL_REQ				0xE052
#define CMD_AUTOTEST_CALL_RSP				0xE0D2
#define CMD_AUTOTEST_LOG_REQ				0xE053
#define CMD_AUTOTEST_LOG_RSP				0xE0D3
#define CMD_AUTOTEST_STATE_REQ			0xE054
#define CMD_AUTOTEST_STATE_RSP			0xE0D4
//czn_20190412_e

#define CMD_ALARMING_RECORD			0xE061

// 20190521
#define CMD_RF_CARD_ADD_REQ		0xB001
#define CMD_RF_CARD_ADD_RSP		0xB081
#define CMD_RF_CARD_SET_REQ		0xB002
#define CMD_RF_CARD_SET_RSP		0xB082
#define CMD_RF_CARD_GET_REQ		0xB003
#define CMD_RF_CARD_GET_RSP		0xB083
#define CMD_RF_CARD_DEL_REQ		0xB004
#define CMD_RF_CARD_DEL_RSP		0xB084

// lzh_20210918_s
#define CMD_IPERF_START_REQ		0xC001
#define CMD_IPERF_START_RSP		0xC081
// lzh_20210918_e
#define CMD_OLDMULTALK_REQ			0x1009
#define CMD_LISTENTALK_REQ		0XE013
#define CMD_OLDMULTALK_REP			0x1089
#define CMD_LISTENTALK_REP		0XE093

typedef struct
{
	udp_comm_rt 		udp;				//	udp交互模板实例
	int 				send_cmd_sn;		//	发送命令包序列号
	send_sem_id_array	waitrsp_array;		// 	业务应答同步队列	
}c5_ipc_instance_t;

// 初始化udp:25000的服务实例
int init_c5_ipc_instance( void );

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_c5_ipc_send_data( int target_ip, int cmd, const char* pbuf, unsigned int len );

// 通过udp:25000端口发送数据包后，并等待业务应答，得到业务应答的数据
int api_udp_c5_ipc_send_req(int target_ip, int cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen);

// 接收到udp:25000端口数据包后给出的业务应答
int api_udp_c5_ipc_send_rsp( int target_ip, int cmd, int id, const char* pbuf, unsigned int len );

// 接收到udp:25000端口数据包的回调函数 - 发送到survey处理
int api_udp_c5_ipc_recv_callback(int target_ip, int cmd, int sn, char* pbuf, unsigned int len );

// 接收到udp:25000端口数据包的回调函数 - 发送到survey处理
int api_udp_transfer_recv_callback(char* pbuf, unsigned int len );
int api_udp_transfer_send_data(int target_ip, const char* pbuf, unsigned int len );

//czn_20170328_s
int vtk_business_udp_stack_nodeid_trs2_ipaddr(unsigned short node_id);
unsigned short vtk_business_udp_stack_ipaddr_trs2_nodeid(int ip_addr);
//czn_20170328_e
#endif

