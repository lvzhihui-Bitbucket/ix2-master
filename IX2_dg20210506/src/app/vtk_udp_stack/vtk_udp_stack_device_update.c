
#include "vtk_udp_stack_device_update.h"
#include "obj_Get_Ph_MFG_SN.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_IPHeartBeatService.h"
#include "obj_GetIpByNumber.h"
#include "obj_GetInfoByIp.h"
#include "obj_GetIpByMFG_SN.h"
#include "obj_SearchIpByFilter.h"
#include "obj_ProgInfoByIp.h"
#include "obj_GetAboutByIp.h"
#include "obj_RemoteUpgrade.h"
#include "obj_MsSyncCallScene.h"		//czn_20190221
#include "obj_OS_SipCMD.h"
#include "obj_CallBtnBinding.h"
#include "obj_DebugCmd.h"
#include "obj_BackupAndRestoreCmd.h"
#include "obj_UnlockReport.h"
#include "obj_SearchIxProxy.h"
#include "obj_IX_Req_Call_Nbr.h"
#include "obj_IX_Req_Ring.h"
#include "obj_ProgIpByIp.h"
#include "obj_IX_Report.h"
#include "obj_GetMergeInfoByIp.h"
#include "obj_SettingFromR8001.h"
#include "obj_SetTimeServer.h"
#include "obj_MulCtrlBySipAcc.h"
#include "obj_NetManagerInterface.h"
#include "task_Shell.h"




device_update_instance_t		one_device_update_ins;
Loop_vdp_common_buffer			device_update_msg_queue;
pthread_t						device_update_process_pid;

device_update_instance_t		wlan_one_device_update_ins;
Loop_vdp_common_buffer			wlan_device_update_msg_queue;
pthread_t						wlan_device_update_process_pid;


extern int send_one_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length);
extern int push_udp_common_queue( p_loop_udp_common_buffer pobj, p_udp_common_buffer data, unsigned int length);		//czn_20170712
extern int pop_udp_common_queue(p_loop_udp_common_buffer pobj, p_udp_common_buffer* pdb, int timeout);
extern int purge_udp_common_queue(p_loop_udp_common_buffer pobj);

int 		device_update_udp_recv_anaylasis( char* buf, int len );

void* 		device_update_msg_process( void* arg );
void* 		wlan_device_update_msg_process(void* arg );

int 		device_update_inner_recv_anaylasis( char* buf, int len );



// udp通信初始化
int init_device_update_instance( void )
{
	
	init_one_udp_comm_rt_buff( 		&one_device_update_ins.udp, 15000,	device_update_udp_recv_anaylasis, 1500, NULL );	//czn_20170712
	init_one_udp_comm_rt_type_ext( 	&one_device_update_ins.udp, "device_update eth0", UDP_RT_TYPE_MULTICAST,DEVICE_SEARCH_UDP_BOARDCAST_PORT, DEVICE_SEARCH_UDP_BOARDCAST_PORT,DEVICE_SEARCH_MULTICAST_ADDR, NET_ETH0);

	// init business rsp wait array
	init_one_send_array(&one_device_update_ins.waitrsp_array);
	
 	init_vdp_common_queue(&device_update_msg_queue,100,(msg_process)device_update_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&device_update_process_pid, 0, device_update_msg_process, &device_update_msg_queue) )
	{
		dev_update_printf("Create task thread Failure,%s\n", strerror(errno));
	}

	SetSocketRcvBufferSize(one_device_update_ins.udp.sock_rcv_fd, 600*1024);

	dev_update_printf("eth0 init_one_device_update ok!...............\n");	
	
	//wlan0
	init_one_udp_comm_rt_buff( 		&wlan_one_device_update_ins.udp, 15000,	device_update_udp_recv_anaylasis, 1500, NULL );	//czn_20170712
	init_one_udp_comm_rt_type_ext( 	&wlan_one_device_update_ins.udp, "device_update wlan0", UDP_RT_TYPE_MULTICAST,DEVICE_SEARCH_UDP_BOARDCAST_PORT, DEVICE_SEARCH_UDP_BOARDCAST_PORT,DEVICE_SEARCH_MULTICAST_ADDR, NET_WLAN0);

	// init business rsp wait array
	init_one_send_array(&wlan_one_device_update_ins.waitrsp_array);
	
 	init_vdp_common_queue(&wlan_device_update_msg_queue,100,(msg_process)device_update_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&wlan_device_update_process_pid, 0, wlan_device_update_msg_process, &wlan_device_update_msg_queue) )
	{
		dev_update_printf("Create task thread Failure,%s\n", strerror(errno));
	}

	//SetSocketRcvBufferSize(wlan_one_device_update_ins.udp.sock_rcv_fd, 1200*1024);

	dev_update_printf("wlan0 init_one_device_update ok!...............\n");	

	return 0;
}

// lzh_20171008_s
int rejoin_multicast_group(void)
{
	lan_rejoin_multicast_group();
	wlan_rejoin_multicast_group();
	return 0;
}
// lzh_20171008_e

int lan_rejoin_multicast_group(void)
{
	int ret;
	leave_multicast_group(one_device_update_ins.udp.net_device_name, one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );
	usleep(10*1000);	
	ret = join_multicast_group(one_device_update_ins.udp.net_device_name, one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );

	return ret;
}

int wlan_rejoin_multicast_group(void)
{
	int ret;
	
	leave_multicast_group(wlan_one_device_update_ins.udp.net_device_name, wlan_one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );
	usleep(10*1000);	
	ret = join_multicast_group(wlan_one_device_update_ins.udp.net_device_name, wlan_one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );

	return ret;
}


#define DEVICE_UPDATE_POLLING_MS		100
#define DEVICE_UPDATE_POLLING_SHORT_MS	5
void* device_update_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int pollingTime = DEVICE_UPDATE_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb,pollingTime );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			pollingTime = DEVICE_UPDATE_POLLING_MS;
		}
		else
		{
			pollingTime = DEVICE_UPDATE_POLLING_SHORT_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &one_device_update_ins.waitrsp_array, pollingTime);
	}	
	return 0;	
}

void* wlan_device_update_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int pollingTime = DEVICE_UPDATE_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb,pollingTime );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			pollingTime = DEVICE_UPDATE_POLLING_MS;
		}
		else
		{
			pollingTime = DEVICE_UPDATE_POLLING_SHORT_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &wlan_one_device_update_ins.waitrsp_array, pollingTime );
	}	
	return 0;	
}

// inner接收命令业务分析
int device_update_inner_recv_anaylasis( char* buf, int len )
{
	return 0;
}

// udp接收命令
int device_update_udp_recv_anaylasis( char* buf, int len)
{	
	ext_pack_buf *pbuf = (ext_pack_buf*)buf;
	

	device_search_package	*package	= (device_search_package*)pbuf->dat;
	int						dat_len		= len - 8 - sizeof(device_search_head)-sizeof(device_search_cmd);

	if(!strncmp(&package->head, "SWITCH", 6))
	{		
		api_udp_device_update_recv_callback(pbuf->ip,htons(package->cmd.cmd),package->buf.dat,dat_len);
	}
	else
	{
		if(!GetShellTaskState())
		{
			API_ShellCommand(pbuf->ip, SourceUDP, "task -o");
		}
		
		int stringLen = 0;
		char datStr[IP8210_BUFFER_MAX+1];
		stringLen = (len-8 > IP8210_BUFFER_MAX ? IP8210_BUFFER_MAX : len-8);
		memcpy(datStr, pbuf->dat, stringLen);
		datStr[stringLen] = 0;
		API_ShellCommand(pbuf->ip, SourceUDP, datStr);
	}
	return 0;
}

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_device_update_send_data(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	device_update_instance_t*		pOne_device_update_ins;
		
	ext_pack_buf			send_pack;
	device_search_package*	psend_pack = (device_search_package*)send_pack.dat;	
	int i;

	char ipChar[16];
	char *netDeviceName;
	int sendTimes;

	// initial head
	psend_pack->head.s 	= 'S';
	psend_pack->head.w 	= 'W';
	psend_pack->head.i 	= 'I';
	psend_pack->head.t 	= 'T';
	psend_pack->head.c 	= 'C';
	psend_pack->head.h 	= 'H';
	// cmd
	psend_pack->cmd.cmd	= cmd;
	// dat
	memcpy( psend_pack->buf.dat, pbuf, len );
	// len
	send_pack.len	= sizeof(device_search_head) + sizeof(device_search_cmd) + len;

	// ip
	send_pack.ip	= target_ip;

	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);

	// 加入发送队列
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(api_nm_if_get_net_mode() == NM_MODE_LAN)
		{
			lan_rejoin_multicast_group();
			sendTimes = LAN_SendTimes;
			pOne_device_update_ins = &one_device_update_ins;
		}
		else
		{
			wlan_rejoin_multicast_group();
			sendTimes = WLAN_SendTimes;
			pOne_device_update_ins = &wlan_one_device_update_ins;
		}
	}
	else if(!strcmp(netDeviceName, NET_WLAN0))
	{
		pOne_device_update_ins = &wlan_one_device_update_ins;
		sendTimes = WLAN_SendTimes;
	}
	else if(!strcmp(netDeviceName, NET_ETH0))
	{
		pOne_device_update_ins = &one_device_update_ins;
		sendTimes = LAN_SendTimes;
	}

	for(i = 0; i < sendTimes; i++)
	{
		push_udp_common_queue( &(pOne_device_update_ins->udp.tmsg_buf), (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
		usleep(5*1000);
	}

	return 0;
}

int api_udp_device_update_send_data_by_device(char* net_device_name, int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	ext_pack_buf			send_pack;
	device_search_package*	psend_pack = (device_search_package*)send_pack.dat;	

	char ipChar[16];

	// initial head
	psend_pack->head.s 	= 'S';
	psend_pack->head.w 	= 'W';
	psend_pack->head.i 	= 'I';
	psend_pack->head.t 	= 'T';
	psend_pack->head.c 	= 'C';
	psend_pack->head.h 	= 'H';
	// cmd
	psend_pack->cmd.cmd	= cmd;
	// dat
	memcpy( psend_pack->buf.dat, pbuf, len );
	// len
	send_pack.len	= sizeof(device_search_head) + sizeof(device_search_cmd) + len;

	// ip
	send_pack.ip	= target_ip;

	// 加入发送队列
	if(!strcmp(net_device_name, NET_ETH0))
	{
		//printf( "api_udp_device_update_send_data_by_device device=%s, ip = %s, len=%d, cmd=0x%04X\n", NET_ETH0, my_inet_ntoa(target_ip, ipChar), len, cmd);
		push_udp_common_queue( &one_device_update_ins.udp.tmsg_buf, (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
	}
	else if(!strcmp(net_device_name, NET_WLAN0))
	{
		//printf( "api_udp_device_update_send_data_by_device device=%s, ip = %s, len=%d, cmd=0x%04X\n", NET_WLAN0, my_inet_ntoa(target_ip, ipChar), len, cmd);
		push_udp_common_queue( &wlan_one_device_update_ins.udp.tmsg_buf, (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
	}

	return 0;
}

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_device_update_send_data2(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	IXReq_T* pReqData;
	
	pReqData = (IXReq_T*)pbuf;
	
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(api_nm_if_get_net_mode() == NM_MODE_LAN)
		{
			pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		}
		else
		{
			pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		}
	}
	else
	{
		pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
	}
	
	api_udp_device_update_send_data(target_ip, cmd,  pbuf, len);
	return 0;
}

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_device_update_send_data3(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	device_update_instance_t*		pOne_device_update_ins;
		
	ext_pack_buf			send_pack;
	device_search_package*	psend_pack = (device_search_package*)send_pack.dat;	
	int i;

	char ipChar[16];
	char *netDeviceName;
	int sendTimes;

	// initial head
	psend_pack->head.s 	= 'S';
	psend_pack->head.w 	= 'W';
	psend_pack->head.i 	= 'I';
	psend_pack->head.t 	= 'T';
	psend_pack->head.c 	= 'C';
	psend_pack->head.h 	= 'H';
	// cmd
	psend_pack->cmd.cmd	= cmd;
	// dat
	memcpy( psend_pack->buf.dat, pbuf, len );
	// len
	send_pack.len	= sizeof(device_search_head) + sizeof(device_search_cmd) + len;

	// ip
	send_pack.ip	= target_ip;

	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);

	// 加入发送队列
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(api_nm_if_get_net_mode() == NM_MODE_LAN)
		{
			lan_rejoin_multicast_group();
			sendTimes = 1;
			pOne_device_update_ins = &one_device_update_ins;
		}
		else
		{
			wlan_rejoin_multicast_group();
			sendTimes = 1;
			pOne_device_update_ins = &wlan_one_device_update_ins;
		}
	}
	else if(!strcmp(netDeviceName, NET_WLAN0))
	{
		pOne_device_update_ins = &wlan_one_device_update_ins;
		sendTimes = 1;
	}
	else if(!strcmp(netDeviceName, NET_ETH0))
	{
		pOne_device_update_ins = &one_device_update_ins;
		sendTimes = 1;
	}

	for(i = 0; i < sendTimes; i++)
	{
		push_udp_common_queue( &(pOne_device_update_ins->udp.tmsg_buf), (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
		usleep(5*1000);
	}

	return 0;
}


// 通过udp:25007端口发送数据包后，并等待业务应答，得到业务应答的数据
int api_udp_device_update_send_req(int target_ip, unsigned short cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen)
{	
	return 0;
}

// 接收到udp:25007端口数据包后给出的业务应答
int api_udp_device_update_send_rsp( int target_ip, unsigned short cmd, int id, const char* pbuf, unsigned int len )
{	
	return 0;
}


// 接收到udp:25007端口数据包的回调函数
int api_udp_device_update_recv_callback(int target_ip, unsigned short cmd, char* pbuf, unsigned int len )
{
	static int saveLen;
	static int saveTarget_ip;
	static int saveCmd;
	static char saveBuf[1024] = {0};
	static long long saveLastTime = 0;
	long long currentTime;

	currentTime = time_since_last_call(-1);

	//100毫秒内，相同包不处理
	if(saveLen == len && saveTarget_ip == target_ip && saveCmd == cmd && !memcmp(saveBuf, pbuf, len > 1024 ? 1024 : len) && currentTime - saveLastTime < 100* 1000)
	{
		return 0;
	}

	saveLen = len;
	saveTarget_ip = target_ip;
	saveCmd = cmd;
	saveLastTime = currentTime;
	memcpy(saveBuf, pbuf, len > 1024 ? 1024 : len);

	#if 0
		int i;
		char ipChar[16];
		printf( "recv udp port 25007, ip=%s, len=%d, cmd=0x%04X\n", my_inet_ntoa(target_ip, ipChar), len, cmd);
		printf("pbuf = ");
		for(i = 0; i<len; i++)
			printf("%02x ", pbuf[i]);
		printf("\n");
	#endif
	char *netDeviceName;
	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);
	if(strcmp(netDeviceName,NET_WLAN0)==0)
		ResetNetworkWLANCheck();
	else
		ResetNetworkCardCheck();
	
	
	switch( cmd )
	{
		case DEVICE_SEARCH_CMD_READ_REQ:
			DeviceSearchReadProcess(target_ip, (device_search_read_req*)pbuf);
			break;
			
		case DEVICE_SEARCH_CMD_WRITE_REQ:
			DeviceSearchWriteProcess(target_ip, (device_search_write_req*)pbuf);		
			break;

		case DEVICE_REQUEST_WRITE_NODE_ID_REQ:
			DeviceWriteNodeIdProcess(target_ip, (WRITE_NODE_ID_REQ_T*)pbuf);
			break;

		case DEVICE_IP_HEART_BEAT_SERVICE:
			//ReceiveHeartBeat();
			break;
		case GET_IP_BY_NUMBER_REQ:
			ReceiveGetIpByNumberFromNetReq(target_ip, (GetIpByNumberReq*)pbuf);
			break;
		case GET_IP_BY_NUMBER_RSP:
			ReceiveGetIpByNumberFromNetRsp(target_ip, (GetIpByNumberRsp*)pbuf);
			break;

		case GET_IP_BY_MFG_SN_REQ:
			ReceiveGetIpByMFG_SNReq(target_ip, (GetIpByMFG_SNReq*)pbuf);
			break;
		case GET_IP_BY_MFG_SN_RSP:
			ReceiveGetIpByMFG_SNRsp(target_ip, (GetIpByMFG_SNRsp*)pbuf);
			break;
			
		case GET_INFO_BY_IP_REQ:
			ReceiveGetInfoReq(target_ip, (GetInfoByIpReq*)pbuf);
			break;
		case GET_INFO_BY_IP_RSP:
			ReceiveGetInfoRsp(target_ip, (GetInfoByIpRsp*)pbuf, len);
			break;

		case PROG_INFO_BY_IP_REQ:
			ReceiveProgInfoReq((ProgInfoByIpReq*)pbuf);
			break;
		case PROG_INFO_BY_IP_RSP:
			ReceiveProgInfoRsp((ProgInfoByIpRsp*)pbuf);
			break;

		case SEARCH_IP_BY_FILTER_REQ:
			ReceiveSearchIpByFilterReq(target_ip, (SearchIpReq*)pbuf);
			break;
			
		case SEARCH_IP_BY_FILTER_RSP:
			ReceiveSearchIpByFilterRsp((SearchIpRsp*)pbuf);
			break;
		
		case GET_ABOUT_BY_IP_REQ:
			ReceiveGetAboutReq(target_ip, (GetAboutByIpReq*)pbuf);
			break;
		case GET_ABOUT_BY_IP_RSP:
			ReceiveGetAboutRsp((GetAboutByIpRsp*)pbuf);
			break;
		case CMD_REMOTE_UPGRADE_REQ:
			ReceiveRemoteUpgradeReq(target_ip, (RemoteUpgradeReq*) pbuf);
			break;
			
		case CMD_REMOTE_UPGRADE_RSP:
			ReceiveRemoteUpgradeRsp((RemoteUpgradeRsp*) pbuf);
			break;

			//czn_20190221_s	
		#if !defined(PID_DX470)&&!defined(PID_DX482)
		case MS_SYNC_REQ:		
			ReceiveMsSyncReq(target_ip,(MsSyncMsgHead_T*)pbuf);
			break;
		case MS_SYNC_RSP:	
			ReceiveMsSyncRsp(target_ip,(MsSyncMsgHead_T*)pbuf);
			break;
		#endif
		case CMD_SIP_CONFIG_REQ:
			ReceiveSipConfigCmdReq(target_ip, (SipCfgCmdReq*) pbuf);
			break;
			
		case CMD_SIP_CONFIG_RSP:
			ReceiveSipConfigCmdRsp((SipCfgCmdRsp*) pbuf);
			break;
		case GET_GetCallBtnBindingState_REQ:
			GetCallBtnBindingStateReq(target_ip, (GetCallBtnBindingStateReq_t*) pbuf);
			break;
				
		case GET_GetCallBtnBindingState_RSP:
			GetCallBtnBindingStateRsp((GetCallBtnBindingStateRsp_t*)pbuf);
			break;

		case CallBtnBinding_REQ:
			CallBtnBindingReq(target_ip, (CallBtnBindingReq_t*) pbuf);
			break;
				
		case CallBtnBinding_RSP:
			CallBtnBindingRsp((CallBtnBindingRsp_t*)pbuf);
			break;

		case CMD_REMOTE_SOFTWARE_UPDATE_REQ:		//czn_20190506
			Recieve_RemoteAllDevSoftwareUpdateReq(pbuf);
			break;	
				
		case CMD_ASK_DEBUG_STATE_REQ:
			ReceiveAskDebugStateReq(target_ip, (DebugStateAskReq*)pbuf);
			break;
			
		case CMD_ASK_DEBUG_STATE_RSP:
			ReceiveAskDebugStateRsp((DebugStateAskRsp*)pbuf);
			break;
		case CMD_EXIT_DEBUG_STATE_REQ:
			ReceiveExitDebugStateReq();
			break;
			
		case CMD_RES_UPLOAD_REQ:		//czn_20190520
		case CMD_RES_UPLOAD_RSP:
		case CMD_RES_DOWNLOAD_REQ:
		case CMD_RES_DOWNLOAD_RSP:
		case CMD_RES_TRANS_RESULT:
			RecvResSyncCmd_Process(target_ip,cmd,pbuf);
			break;
		case SEARCH_IP_BY_FILTER2_REQ:
			ReceiveSearchIpByFilter2Req(target_ip, (SearchIpReq2*)pbuf);
			break;
			
		case CMD_CHANGE_INSTALL_PWD_REQ:
			ChangeInstallPwdReq(target_ip, (DebugStateAskReq*)pbuf);
			break;
		case CMD_CHANGE_INSTALL_PWD_RSP:
			ChangeInstallPwdRsp((DebugStateAskRsp*)pbuf);
			break;
			
		case CMD_BACKUP_AND_RESTORE_REQ:
			ReceiveBackupAndRestoreCmdReq(target_ip, (BackupAndRestoreCmdReq*)pbuf);
			break;
			
		case CMD_BACKUP_AND_RESTORE_RSP:
			ReceiveBackupAndRestoreCmdRsp((BackupAndRestoreCmdRsp*)pbuf);
			break;
			
		case CMD_UNLOCK_STATE_REPORT:
			ReceiveUnlockReport((UnlockReport_T*)pbuf);
			break;

		case SEARCH_IX_PROXY_REQ:
			ReceiveSearchIxProxyReq(target_ip, (SearchIxProxyReq*)pbuf, len);
			break;

		case SEARCH_IX_PROXY_LINKED_REQ:
			ReceiveSearchIxProxyLinkedReq(target_ip, (SearchIxProxyReq*)pbuf);
			break;
			
		case SEARCH_IX_PROXY_LINKED_RSP:
			ReceiveIxProxyLinkedRsp((SearchIxProxyRsp*)pbuf);
			break;
			
		case IX_REQ_Prog_Call_Nbr_REQ:
			ReceiveIxReqCallNbrCmdReq((IxReqCallNbrReq*)pbuf);
			break;
		
		case IX_REQ_Prog_Call_Nbr_RSP:
			ReceiveIxReqCallNbrCmdRsp((IxReqCallNbrRsp*)pbuf);
			break;

		case IX_REQ_Ring_REQ:
			ReceiveIxReqRingCmdReq((IxReqRingReq*)pbuf);
			break;

		case IX_REQ_Ring_RSP:
			ReceiveIxReqRingCmdRsp((IxReqRingRsp*)pbuf);
			break;

		case IX_PROG_IP_ADDR_REQ:
			ReceiveIxProgIpAddrCmdReq((IxProgIpAddrReq*)pbuf);
			break;
			
		case IX_PROG_IP_ADDR_RSP:
			ReceiveIxProgIpAddrCmdRsp((IxProgIpAddrRsp*)pbuf);
			break;
		case IX_Report_REQ:
			ReceiveIxReportCmdReq((IxReportReq*)pbuf);
			break;
		case IX_Report_RSP:
			ReceiveIxReportCmdRsp((IxReportRsp*)pbuf);
			break;
		
		case Backup_REQ:
		case Restore_REQ:
		case IX_DEVICE_RES_DOWNLOAD_REQ:
			ReceivePublicUdpCmdReq(cmd, (PublicUdpCmdReq*)pbuf);
			break;

		case TFTP_WRITE_FILE_REQ:
		case TFTP_READ_FILE_REQ:
		case FileManagement_REQ:
		case TFTP_CHECK_FILE_REQ:
			ReceivePublicMulRspUdpCmdReq(cmd, (PublicMulRspUdpCmdReq*)pbuf, len);
			break;

		case TFTP_WRITE_FILE_RSP:
		case TFTP_READ_FILE_RSP:
		case FileManagement_RSP:
		case TFTP_CHECK_FILE_RSP:
			ReceivePublicMulRspUdpCmdRsp(cmd, (PublicMulRspUdpCmdRsp*)pbuf);
			break;

		case Backup_RSP:
		case Restore_RSP:
		case IX_DEVICE_RES_DOWNLOAD_RSP:
			ReceivePublicUdpCmdRsp(cmd, (PublicUdpCmdRsp*)pbuf);
			break;

		case GET_MERGE_INFO_BY_IP_REQ:
			ReceiveGetMergeInfoReq(target_ip, (GetMergeInfoByIpReq*)pbuf);
			break;
			
		case GET_MERGE_INFO_BY_IP_RSP:
			ReceiveGetMergeInfoRsp((GetMergeInfoByIpRsp*)pbuf);
			break;
			
		case SettingFromR8001_REQ:
			ReceiveSettingFromR8001Req(target_ip, (SettingFromR8001Req*)pbuf);
			break;
			
		case SettingFromR8001_RSP:
			ReceiveSettingFromR8001Rsp((SettingFromR8001Rsp*)pbuf);
			break;
		
		case SetTimeServer_REQ:
			ReceiveSetTimeServerCmdReq((SetTimeServer_T*)pbuf);
			break;	

		case SetTimeServer_RSP:
			ReceiveSetTimeServerCmdRsp((SetTimeServer_T*)pbuf);
			break;

		case MUL_CTRL_BY_SIPACC_REQ:
			MulCtrlBySipAccReqProcess(target_ip, (MulCtrlBySipAcc_Req_T*)pbuf);
			break;

		case EVENT_REQ:
		case GET_PB_BY_IP_REQ:
		case GET_RW_IO_BY_IP_REQ:
		case IX_Ring2_REQ:
		case TABLE_PROCESS_REQ:
		case SET_DIVERT_STATE_REQ:
		case GET_QR_CODE_REQ:
		case UPDATE_DEV_TABLE_REQ:
			ReceivePublicUnicastCmdReq(NET_ETH0, cmd, pbuf, len);
			break;
		case EVENT_RSP:
		case GET_PB_BY_IP_RSP:
		case GET_RW_IO_BY_IP_RSP:
		case IX_Ring2_RSP:
		case TABLE_PROCESS_RSP:
		case SET_DIVERT_STATE_RSP:
		case GET_QR_CODE_RSP:
		case UPDATE_DEV_TABLE_RSP:
			ReceivePublicUnicastCmdRsp(cmd, pbuf);
			break;

		case UPDATE_DEV_TABLE_INFORM:
			IXS_ProxyReceiveUpdateTbInform(target_ip, pbuf);
			break;
		//czn_20190221_e
/*
		case DEVREG_CMD_REGISTER_APPLY:
		case DEVREG_CMD_REGISTER_REPLY:
		case DEVREG_CMD_REGISTER_COMFIRM:
		case DEVREG_CMD_REGISTER_DISCOVERY:
		case DEVREG_CMD_REGISTER_NOTIFY:
		case DEVREG_CMD_REGISTER_BROADCAST:
		case DEVREG_CMD_REGISTER_UPDATE:	
		case LINKSTATE_CMD_BROADCAST:		//czn_20170712	
			//DeviceRegister_Cmd_Process(target_ip,cmd,pbuf);
			api_udp_c5_ipc2_recv_callback(target_ip, cmd,0,pbuf,len );
			break;
*/
	}
	return 0;
}

void DeviceSearchReadProcess(int target_ip, device_search_read_req* pReadReq)
{
	device_search_read_rsp		readRsp;
	SYS_VER_INFO_T	sysVerInfoDate;
	uint8 i;

	sysVerInfoDate = GetSysVerInfo();
	
	memcpy(readRsp.mfgSn, sysVerInfoDate.sn, 12);

	readRsp.ipNodeId = sysVerInfoDate.id;
	//判断sn匹配
	for(i = 0; pReadReq->mfgSn[i] != 0 && i < 12; i++)
	{
		if(memcmp(readRsp.mfgSn, pReadReq->mfgSn, 12))
		{
			//不匹配，结束
			return 0;
		}
		else
		{
			//匹配继续
			break;
		}
	}

	//判断nodeId匹配
	if(ntohs(pReadReq->ipNodeId) != 0)
	{
		if(readRsp.ipNodeId != ntohs(pReadReq->ipNodeId))
		{
			//不匹配，结束
			return 0;
		}
	}
	
	//判断ipDeviceType匹配
	for(i = 0; pReadReq->ipDeviceType[i] != 0 && i < 18; i++)
	{
		if(memcmp(DEVICE_TYPE, pReadReq->ipDeviceType, sizeof(DEVICE_TYPE)))
		{
			//不匹配，结束
			return 0;
		}
		else
		{
			//匹配继续
			break;
		}
	}

	memcpy( readRsp.req_source_zero, pReadReq->req_source_zero, 6);
	memcpy( readRsp.req_target_zero, pReadReq->req_target_zero, 6);
	readRsp.req_source_ip		= pReadReq->req_source_ip;

	readRsp.rsp_target_ip		= inet_addr(sysVerInfoDate.ip);
	readRsp.rsp_target_gateway	= inet_addr(sysVerInfoDate.gw);
	readRsp.rsp_target_mask = inet_addr(sysVerInfoDate.mask);

	GetLocalMacByDevice(NET_ETH0, readRsp.rsp_target_mac);
	
	memset(readRsp.ipDeviceType, 0, 18);
	strcpy(readRsp.ipDeviceType, DEVICE_TYPE);
	
	readRsp.version = 1;
	readRsp.ipCfgLimit = 1;
	readRsp.ipNodeId = htons(readRsp.ipNodeId);
	
	dev_update_printf( "send ip=%08x, dat_len=%d,cmd=%04x\n %s %s", target_ip,sizeof(device_search_read_rsp),htons(DEVICE_SEARCH_CMD_READ_RSP),__DATE__,__TIME__);
	
	api_udp_device_update_send_data( target_ip, htons(DEVICE_SEARCH_CMD_READ_RSP), (char*)&readRsp, sizeof(device_search_read_rsp) );

}


void DeviceSearchWriteProcess(int target_ip, device_search_write_req* pWriteReq)
{
	device_search_write_rsp		writeRsp;
	SYS_VER_INFO_T	sysVerInfoDate;
	int tempInt;
	char mac[6], temp[20];
	uint8 i;
	
	sysVerInfoDate = GetSysVerInfo();
	
	memcpy(writeRsp.mfgSn, sysVerInfoDate.sn, 12);
	writeRsp.ipNodeId = sysVerInfoDate.id;
	
	//判断sn匹配
	for(i = 0; pWriteReq->mfgSn[i] != 0 && i < 12; i++)
	{
		if(memcmp(writeRsp.mfgSn, pWriteReq->mfgSn, 12))
		{
			//不匹配，结束
			return 0;
		}
		else
		{
			//匹配继续
			break;
		}
	}
	//判断nodeId匹配
	if(ntohs(pWriteReq->ipNodeId) != 0)
	{
		if(writeRsp.ipNodeId != ntohs(pWriteReq->ipNodeId))
		{
			//不匹配，结束
			return 0;
		}
	}
	//判断ipDeviceType匹配
	for(i = 0; pWriteReq->ipDeviceType[i] != 0 && i < 18; i++)
	{
		if(memcmp(DEVICE_TYPE, pWriteReq->ipDeviceType, strlen(DEVICE_TYPE)))
		{
			//不匹配，结束
			return 0;
		}
		else
		{
			break;
		}
	}
	
	if(pWriteReq->req_target_new_ip)
	{
		tempInt = pWriteReq->req_target_new_ip;
		snprintf(temp, 20, "%03d.%03d.%03d.%03d", tempInt&0xFF, (tempInt>>8)&0xFF, (tempInt>>16)&0xFF, (tempInt>>24)&0xFF);
		strcpy(sysVerInfoDate.ip, temp);
	}
	
	if(pWriteReq->req_target_new_gateway)
	{
		tempInt = pWriteReq->req_target_new_gateway;
		snprintf(temp, 20, "%03d.%03d.%03d.%03d", tempInt&0xFF, (tempInt>>8)&0xFF, (tempInt>>16)&0xFF, (tempInt>>24)&0xFF);
		strcpy(sysVerInfoDate.gw, temp);
	}
	
	if(pWriteReq->req_target_new_mask)
	{
		tempInt = pWriteReq->req_target_new_mask;
		snprintf(temp, 20, "%03d.%03d.%03d.%03d", tempInt&0xFF, (tempInt>>8)&0xFF, (tempInt>>16)&0xFF, (tempInt>>24)&0xFF);
		strcpy(sysVerInfoDate.mask, temp);
	}
	
	memcpy(mac, pWriteReq->req_target_new_mac, 6);
	
	if((mac[0] != 0) || (mac[1] != 0) || (mac[2] != 0) || (mac[3] != 0) || (mac[4] != 0) || (mac[5] != 0))
	{
		snprintf(sysVerInfoDate.mac, 20, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		strcpy(sysVerInfoDate.mask, temp);
	}
	
	SetSysVerInfo(sysVerInfoDate);
	sysVerInfoDate = GetSysVerInfo();
	
	writeRsp.rsp_target_old_ip = pWriteReq->req_target_old_ip;
	writeRsp.rsp_target_new_ip = inet_addr(sysVerInfoDate.ip);
	writeRsp.rsp_target_new_mask = inet_addr(sysVerInfoDate.mask);
	writeRsp.rsp_target_new_gateway = inet_addr(sysVerInfoDate.gw);;
	GetLocalMacByDevice(NET_ETH0, writeRsp.rsp_target_new_mac);
	
	writeRsp.ipNodeId = htons(writeRsp.ipNodeId);
	
	memset(writeRsp.ipDeviceType, 0, 18);
	memcpy(writeRsp.ipDeviceType, DEVICE_TYPE, strlen(DEVICE_TYPE));
	
	api_udp_device_update_send_data( target_ip, htons(DEVICE_SEARCH_CMD_WRITE_RSP), (char*)&writeRsp, sizeof(device_search_write_rsp) );
}


void DeviceWriteNodeIdProcess(int target_ip, WRITE_NODE_ID_REQ_T* pWriteNodeIdReq)
{
	WRITE_NODE_ID_RSP_T			writeNodeIdRsp;
	SYS_VER_INFO_T	sysVerInfoDate;
	
	sysVerInfoDate = GetSysVerInfo();
	
	//序列号不一样不允许改
	if(memcmp(sysVerInfoDate.sn, pWriteNodeIdReq->mfgSn, 12))
	{
		return 0;
	}
	
	memcpy(&writeNodeIdRsp, pWriteNodeIdReq, sizeof(WRITE_NODE_ID_REQ_T));
	
	/*
	if(API_GetDipSetIpNodeId() == 0)
	{
		//更改node id
		sysVerInfoDate.id = ntohs(pWriteNodeIdReq->ipNodeId);
		SetSysVerInfo(sysVerInfoDate);
		sysVerInfoDate = GetSysVerInfo();
		writeNodeIdRsp.result = 0;	//0：正常 1：错误
	}
	else
	*/
	{
		writeNodeIdRsp.result = 1;	//0：正常 1：错误
	}
	writeNodeIdRsp.ipNodeId = htons(sysVerInfoDate.id);
	
	api_udp_device_update_send_data(target_ip, htons(DEVICE_REQUEST_WRITE_NODE_ID_RSP), (char*)&writeNodeIdRsp, sizeof(WRITE_NODE_ID_RSP_T));
}

int Udp25007UnlimitedSend(int target_ip, const char* pbuf, int len)
{
	device_update_instance_t*		pOne_device_update_ins;
	ext_pack_buf					send_pack;
	int ret;
		
	int sendLen;

	if(target_ip == 0 || target_ip == -1)
	{
		return -1;
	}

	send_pack.ip	= target_ip;

	char *netDeviceName;
	// ip

	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);

	// 加入发送队列
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(api_nm_if_get_net_mode() == NM_MODE_LAN)
		{
			lan_rejoin_multicast_group();
			pOne_device_update_ins = &one_device_update_ins;
		}
		else
		{
			wlan_rejoin_multicast_group();
			pOne_device_update_ins = &wlan_one_device_update_ins;
		}
	}
	else if(!strcmp(netDeviceName, NET_WLAN0))
	{
		pOne_device_update_ins = &wlan_one_device_update_ins;
	}
	else if(!strcmp(netDeviceName, NET_ETH0))
	{
		pOne_device_update_ins = &one_device_update_ins;
	}

	for(sendLen = 0; sendLen < len; sendLen += send_pack.len)
	{
		send_pack.len = (len - sendLen > IP8210_BUFFER_MAX) ? IP8210_BUFFER_MAX : len - sendLen;
		memcpy(send_pack.dat, pbuf + sendLen, send_pack.len);
		
		for(ret = 0; ret < 3 && push_udp_common_queue( &(pOne_device_update_ins->udp.tmsg_buf), (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len ); ret++)
		{
			dprintf("UDP send buffer limit.\n");
			usleep(100*1000);
		}

		if(send_pack.len > IP8210_BUFFER_MAX)
		{
			usleep(100*1000);
		}
	}

	return 0;
}

int api_udp_shell_send_data(int target_ip, const char* pbuf)
{
	return Udp25007UnlimitedSend(target_ip, pbuf, strlen(pbuf));
}


