
#include "../task_io_server/task_IoServer.h"
#include "vtk_udp_stack_io_server.h"
#include "../device_manage/obj_IPHeartBeatService.h"
#include "obj_FileTrsCmd.h"


udp_io_server_instance_t		wlan_one_udp_io_server_ins;
Loop_vdp_common_buffer			wlan_udp_io_server_msg_queue;
pthread_t						wlan_udp_io_server_process_pid;

udp_io_server_instance_t		one_udp_io_server_ins;
Loop_vdp_common_buffer			udp_io_server_msg_queue;
pthread_t						udp_io_server_process_pid;

void* 							udp_io_server_msg_process( void* arg );
void* 							wlan_udp_io_server_msg_process(void* arg );

int 							udp_io_server_inner_recv_anaylasis( char* buf, int len );

extern int send_one_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length);
extern int push_udp_common_queue( p_loop_udp_common_buffer pobj, p_udp_common_buffer data, unsigned int length);		//czn_20170712
extern int pop_udp_common_queue(p_loop_udp_common_buffer pobj, p_udp_common_buffer* pdb, int timeout);
extern int purge_udp_common_queue(p_loop_udp_common_buffer pobj);

int udp_io_server_udp_recv_anaylasis( char* buf, int len );
int wlan_udp_io_server_udp_recv_anaylasis( char* buf, int len );

// udp通信初始化
int init_udp_io_server_instance( void )
{
	
	init_one_udp_comm_rt_buff( 	&one_udp_io_server_ins.udp, 2500,	udp_io_server_udp_recv_anaylasis, 2500, NULL );		//czn_20161008
	init_one_udp_comm_rt_type( 	&one_udp_io_server_ins.udp, "udp_io_server  eth0", UDP_RT_TYPE_UNICAST,UDP_IO_SERVER_UDP_PORT, UDP_IO_SERVER_UDP_PORT,NULL, NET_ETH0);

	// init business rsp wait array
	init_one_send_array(&one_udp_io_server_ins.waitrsp_array);
	
 	init_vdp_common_queue(&udp_io_server_msg_queue,100,(msg_process)udp_io_server_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&udp_io_server_process_pid, 0, udp_io_server_msg_process, &udp_io_server_msg_queue) )
	{
		udp_io_server_printf("Create task thread Failure,%s\n", strerror(errno));
	}
	
	udp_io_server_printf("init_one_device_update ok!\n");	

	init_one_udp_comm_rt_buff( 	&wlan_one_udp_io_server_ins.udp, 2500,	wlan_udp_io_server_udp_recv_anaylasis, 2500, NULL );		//czn_20161008
	init_one_udp_comm_rt_type( 	&wlan_one_udp_io_server_ins.udp, "udp_io_server wlan0", UDP_RT_TYPE_UNICAST,UDP_IO_SERVER_UDP_PORT, UDP_IO_SERVER_UDP_PORT,NULL, NET_WLAN0);

	// init business rsp wait array
	init_one_send_array(&wlan_one_udp_io_server_ins.waitrsp_array);
	
 	init_vdp_common_queue(&wlan_udp_io_server_msg_queue,100,(msg_process)udp_io_server_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&wlan_udp_io_server_process_pid, 0, wlan_udp_io_server_msg_process, &wlan_udp_io_server_msg_queue) )
	{
		udp_io_server_printf("Create task thread Failure,%s\n", strerror(errno));
	}
	
	udp_io_server_printf("init_one_device_update ok!\n");	
	return 0;
}

#define UDP_IO_SERVER_POLLING_MS		100
#define UDP_IO_SERVER_POLLING_SHORT_MS	10
void* udp_io_server_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int ioServerMsgProcess = UDP_IO_SERVER_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb, ioServerMsgProcess );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			ioServerMsgProcess = UDP_IO_SERVER_POLLING_SHORT_MS;
		}
		else
		{
			ioServerMsgProcess = UDP_IO_SERVER_POLLING_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &one_udp_io_server_ins.waitrsp_array, ioServerMsgProcess );
	}	
	return 0;	
}

void* wlan_udp_io_server_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int ioServerMsgProcess = UDP_IO_SERVER_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb,ioServerMsgProcess );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			ioServerMsgProcess = UDP_IO_SERVER_POLLING_SHORT_MS;
		}
		else
		{
			ioServerMsgProcess = UDP_IO_SERVER_POLLING_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &wlan_one_udp_io_server_ins.waitrsp_array, ioServerMsgProcess );
	}	
	return 0;	
}

// inner接收命令业务分析
int udp_io_server_inner_recv_anaylasis( char* buf, int len )
{
	return 0;
}

// udp接收命令
int udp_io_server_udp_recv_anaylasis( char* buf, int len )
{	
	// lzh_20160811_s
	len -= sizeof(baony_head);
	buf += sizeof(baony_head);
	// lzh_20160811_e

	target_head	*ptarget = (target_head*)buf;
	
	// 1、匹配到业务应答的数据包通过触发信号同步到任务的等待队列中
	// 2、未匹配到业务应答的数据包发送给系统survey处理
	if( trig_one_business_recv_array( &one_udp_io_server_ins.waitrsp_array, ptarget->id, ptarget->cmd,buf+sizeof(target_head), len-sizeof(target_head) ) < 0)
	{
		api_udp_io_serverc_recv_callback(ptarget->ip, ptarget->cmd, ptarget->id, buf+sizeof(target_head), len-sizeof(target_head) );
	}

	return 0;
}

int wlan_udp_io_server_udp_recv_anaylasis( char* buf, int len )
{	
	// lzh_20160811_s
	len -= sizeof(baony_head);
	buf += sizeof(baony_head);
	// lzh_20160811_e

	target_head	*ptarget = (target_head*)buf;
	
	// 1、匹配到业务应答的数据包通过触发信号同步到任务的等待队列中
	// 2、未匹配到业务应答的数据包发送给系统survey处理
	if( trig_one_business_recv_array( &wlan_one_udp_io_server_ins.waitrsp_array, ptarget->id, ptarget->cmd,buf+sizeof(target_head), len-sizeof(target_head) ) < 0)
	{
		api_udp_io_serverc_recv_callback(ptarget->ip, ptarget->cmd, ptarget->id, buf+sizeof(target_head), len-sizeof(target_head) );
	}

	return 0;
}

// 通过udp:25008端口发送数据，不等待业务应答
int api_udp_io_server_send_data( int target_ip, int cmd, const char* pbuf, unsigned int len )
{
	udp_io_server_instance_t*		pOne_udp_io_server_ins;
	
	if(!strcmp(GetNetDeviceNameByTargetIp(target_ip), NET_ETH0))
	{
		pOne_udp_io_server_ins = &one_udp_io_server_ins;
	}
	else
	{
		pOne_udp_io_server_ins = &wlan_one_udp_io_server_ins;
	}

	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pOne_udp_io_server_ins->udp), target_ip, cmd, ++(pOne_udp_io_server_ins->send_cmd_sn), 1, (char*)pbuf, len);
	// 等待通信应答
	if(presend_sem > 0)
	{
		if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
		{
			return -1;
		}
	}
	return 0;
}

// 通过udp:25008端口发送数据包后，并等待业务应答，得到业务应答的数据
int api_udp_io_server_send_req(int target_ip, int cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen)
{	
	udp_io_server_instance_t*		pOne_udp_io_server_ins;
	
	if(!strcmp(GetNetDeviceNameByTargetIp(target_ip), NET_ETH0))
	{
		pOne_udp_io_server_ins = &one_udp_io_server_ins;
	}
	else
	{
		pOne_udp_io_server_ins = &wlan_one_udp_io_server_ins;
	}
	
	int send_cmd_sn = ++(pOne_udp_io_server_ins->send_cmd_sn);
	
	// 加入等待业务应答队列	 - 等待接收的命令和序列号需要和发送的命令和序列号先联系
	sem_t *pwaitrsp_sem = join_one_business_recv_array( &(pOne_udp_io_server_ins->waitrsp_array),send_cmd_sn, cmd+0x80,BUSINESS_RESEND_TIME, prbuf, prlen );
	
	// 加入数据发送队列，等待ack应答
	sem_t *presend_sem = one_udp_comm_trs_api( &(pOne_udp_io_server_ins->udp), target_ip, cmd, send_cmd_sn, 1, psbuf, slen);

	if( pwaitrsp_sem > 0 && presend_sem > 0 )
	{
		// 等待服务器通信应答
		if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
		{
			dele_one_business_recv_array( &(pOne_udp_io_server_ins->waitrsp_array), send_cmd_sn, cmd+0x80 );
			bprintf("S_UUDP:NO_COMMU_ACK\n");
			return -1;
		}
		// 等待binuess应答	
		if( sem_wait_ex2(pwaitrsp_sem, BUSINESS_WAIT_TIMEPUT+100) != 0 )
		{
			bprintf("S_UUDP:NO_BUSINESS_ACK\n");
			return -1;
		}
	}
	else
	{
		bprintf("S_UUDP:SEM_ERR\n");
		return -1;
	}
	if( prlen == NULL )
		bprintf("S_UUDP:HAVE_BUSINESS_ACK_PKT size %d\n" ,0);
	else
		bprintf("S_UUDP:HAVE_BUSINESS_ACK_PKT size %d\n" ,*prlen);	

	return 0;
}

// 接收到udp:25007端口数据包后给出的业务应答
int api_udp_io_server_send_rsp( int target_ip, int cmd, int id, const char* pbuf, unsigned int len )
{	
	udp_io_server_instance_t*		pOne_udp_io_server_ins;
	
	if(!strcmp(GetNetDeviceNameByTargetIp(target_ip), NET_ETH0))
	{
		pOne_udp_io_server_ins = &one_udp_io_server_ins;
	}
	else
	{
		pOne_udp_io_server_ins = &wlan_one_udp_io_server_ins;
	}

	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pOne_udp_io_server_ins->udp), target_ip, cmd, id, 5, (char*)pbuf, len);

	// 等待通信应答
	if(presend_sem > 0)
	{
		if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
		{
			return -1;
		}
	}
	return 0;
}


// 接收到udp:25000端口数据包的回调函数 - 发送到survey处理
int api_udp_io_serverc_recv_callback(int target_ip, int cmd, int sn, char* pbuf, unsigned int len )
{
	//czn_20161008_s
	switch(cmd)
	{
		case RSTRS_CMD_SENDREQ:
		case RSTRS_CMD_SENDONEPACK:	
		case RSTRS_CMD_SENDEND:
		case RSTRS_CMD_SENDCANCEL:
		case RSTRS_CMD_READREQ:
		case RSTRS_CMD_READONEPACK:
		case RSTRS_CMD_READEND:
		case RSTRS_CMD_READCANCEL:
		case RSTRS_CMD_FWUPDATEREQ:
		case RSTRS_CMD_GETFWVER:
		case RSTRS_CMD_FILECHKREQ:		//czn_20170206
			return LocalRsTrsCmd_Process(target_ip,cmd,sn,pbuf,len);
	}
	//czn_20161008_e

	char *netDeviceName;
	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);
	if(strcmp(netDeviceName,NET_WLAN0)==0)
		ResetNetworkWLANCheck();
	else
		ResetNetworkCardCheck();

	UDP_IO_CMD_t	receiveCmd;
	IoRemote_S 		ioRemoteData;

	memcpy(&receiveCmd, pbuf, (len) > sizeof(receiveCmd) ? sizeof(receiveCmd) : (len));

	ioRemoteData.head.msg_source_id = MSG_ID_IOServer;
	ioRemoteData.head.msg_target_id = MSG_ID_IOServer;
	ioRemoteData.targetIp 			= target_ip;
	ioRemoteData.cmd 				= cmd;
	ioRemoteData.id					= sn;
	ioRemoteData.addr				= receiveCmd.device_address;
	
	switch(cmd)
	{
		case UDP_IO_SERVER_CMD_R:
			strcpy(ioRemoteData.msg_dat.property_id, receiveCmd.property_id);
			ioRemoteData.msg_dat.result		= 1;
			ioRemoteData.head.msg_type		= IO_SERVER_UDP_BE_READ_BY_REMOTE;
			ioRemoteData.msg_dat.ptr_type	= receiveCmd.ptr_type;
			strcpy(ioRemoteData.msg_dat.ptr_data, receiveCmd.realData);
			return API_io_server_UDP_CMD_process((char*)&ioRemoteData, sizeof(ioRemoteData)-IO_DATA_LENGTH+strlen(ioRemoteData.msg_dat.ptr_data)+1);
			
		case UDP_IO_SERVER_CMD_W:
			strcpy(ioRemoteData.msg_dat.property_id, receiveCmd.property_id);
			ioRemoteData.msg_dat.result		= 1;
			ioRemoteData.msg_dat.ptr_type	= receiveCmd.ptr_type;
			strcpy(ioRemoteData.msg_dat.ptr_data, receiveCmd.realData);
			ioRemoteData.head.msg_type		= IO_SERVER_UDP_BE_WRITE_BY_REMOTE;
			return API_io_server_UDP_CMD_process((char*)&ioRemoteData, sizeof(ioRemoteData)-IO_DATA_LENGTH+strlen(ioRemoteData.msg_dat.ptr_data)+1);

		case UDP_IO_SERVER_CMD_DEFAULT:
			ioRemoteData.msg_dat.result		= 1;
			ioRemoteData.head.msg_type		= IO_SERVER_UDP_BE_INVITE_FACTORY_DEFAULT;
			return API_io_server_UDP_CMD_process((char*)&ioRemoteData, sizeof(ioRemoteData)-IO_DATA_LENGTH);

		case UDP_IO_SERVER_CMD_ONE_DEFAULT:
			ioRemoteData.msg_dat.result 	= 1;
			ioRemoteData.head.msg_type		= IO_SERVER_UDP_BE_WRITE_DEFAULT_REMOTE;
			strcpy(ioRemoteData.msg_dat.property_id, receiveCmd.property_id);
			strcpy(ioRemoteData.msg_dat.ptr_data, receiveCmd.realData);
			return API_io_server_UDP_CMD_process((char*)&ioRemoteData, sizeof(ioRemoteData)-IO_DATA_LENGTH+strlen(ioRemoteData.msg_dat.ptr_data));

		default:
			return -1;
	}
}

