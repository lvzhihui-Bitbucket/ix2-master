

#ifndef __sntp_time__
#define __sntp_time__

#define JAN_1ST_1900  2415021 //从元年到1900年1月1日的天数值.

//Representation of an NTP timestamp
struct NtpTimePacket
{
    unsigned int m_dwInteger;
	unsigned int m_dwFractional;
};



//The mandatory part of an NTP packet
struct NtpBasicInfo
{
	char m_LiVnMode;
	char m_Stratum;
	char m_Poll;
	char m_Precision;
	int m_RootDelay;
	int m_RootDispersion;
	char m_ReferenceID[4];
	//系统时钟最后一次被设定或更新的时间。
	struct NtpTimePacket m_ReferenceTimestamp; 
	//NTP请求报文离开发送端时发送端的本地时间。
	struct NtpTimePacket m_OriginateTimestamp; 
	//NTP请求报文到达接收端时接收端的本地时间。
	struct NtpTimePacket m_ReceiveTimestamp; 
	//应答报文离开应答者时应答者的本地时间。
	struct NtpTimePacket m_TransmitTimestamp; 
};




//The optional part of an NTP packet
struct NtpAuthenticationInfo
{
	unsigned int m_KeyID;
	char m_MessageDigest[16];
};



//The Full NTP packet
struct NtpFullPacket
{
	struct NtpBasicInfo          m_Basic;
	struct NtpAuthenticationInfo m_Auth;
};





/*******************************
discription:
  当地时间转格林时间。
parameter:
  pTvSec--输入、输出，转换前后的时间秒值。
*******************************/
static int local_time_to_utc(void *pTvSec);




/*******************************************
description:
  把格林时间mTime转换成utc时间。
parameter:
  tmTime--输入，格林时间。
  puiSeconds--输出，输出的ntp时间，秒值。
*******************************************/
static int create_ntp_time(const struct tm tmTime, unsigned int *puiSeconds);




/*************************************************
discription:
  计算当前从1900年1月1日到指定日期一共有多少天。
parameter:
  iYear--输入，年。
  iMonth--输入，月。
  iDay--输入，日。
return:
  1900年1月1日到指定日期一共有多少天。
*************************************************/
static unsigned int get_julian_day(int iYear, int iMonth, int iDay);




/***************************************
description:
  获取系统当前格林时间。
parameter:
  puiSec--输出，当前系统ntp时间秒值。
***************************************/
int get_current_ntp_time(unsigned int *puiSec);



#if 0
/********************************************
description:
  设置系统时区。
parameter:
  iTZMinutesWest--输入，当前时间相对格林时间的时差。
********************************************/
int set_timezone(int iTZMinutesWest);
#endif

/*********************************************************************
description:  设置系统时区，，大于0表示为东时区，小于等于0为西时区
parameter:  	timezone--输入，当前时间相对格林时间的时区
return:		0
*********************************************************************/
int set_timezone(int timezone );

#endif


