

#include <stdio.h>  
#include <unistd.h>  
#include <string.h>  
#include <time.h>  
#include <sys/time.h>  
#include <netinet/in.h>  
#include <sys/socket.h>  
#include <arpa/inet.h>  
#include <netdb.h>  

#include "sntp_c.h"  

#include "unix_socket.h"

#define LOCAL_SNTP_SERVICE_FILE 	"/tmp/sntp_server_socket"
unix_socket_t	sntp_cliet_socket;

pthread_mutex_t sntp_client_lock;

sem_t			sntp_client_sem;
int				sync_time_result = -1;
int				sync_time_serverip = 0;

// lzh_20181030_s
SNTP_SERVER_PACK_T sntp_server_response;

#define JAN_1970    		2208988800UL        /* 1970 - 1900 in seconds */  

int sync_time_rsp_done( STNP_Header *pstnp, int ntp_server_ip )  
{
    time_t t1,t2,t3,t4,dis;  
	t1 = ntohl(pstnp->OriTimeInt);
    t2 = ntohl(pstnp->RecvTimeInt);  
    t3 = ntohl(pstnp->TranTimeInt);  
    time(&t4);  
    t4+=JAN_1970;  
      
    dis = ( (t2-t1)+(t3-t4) )/2;  
    //if(dis<=0)  
    //   printf("local time is faster then server %d seconds\n", (int)-dis);  
    //else  
    //    printf("local time is slower then server %d seconds\n", (int)dis);  
      
    struct timeval 	tv;  
    struct timezone tzTimeZone;
	
    gettimeofday (&tv, &tzTimeZone);  
    tv.tv_sec+=dis;  

	// 北京时间比格林尼治时间早8小时，接收到的utc时间需要加上8*60*60秒
	tv.tv_sec += (tzTimeZone.tz_minuteswest*60);

	struct in_addr temp;
	temp.s_addr = ntp_server_ip;
    printf("from %s get sync time: %s", inet_ntoa(temp),ctime(&tv.tv_sec)); 
      
    settimeofday (&tv, NULL);  

	// 设置硬件时钟
	// set_system_date_time();

	FILE* fd   = NULL;
	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		//printf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	  
    return 0;  	
}

// lzh_20181030_e

void sntp_client_socket_recv_data(char* pbuf, int len);

int init_sntp_client( void )
{
	pthread_mutex_init(&sntp_client_lock, 0);
	init_unix_socket(&sntp_cliet_socket,0,LOCAL_SNTP_SERVICE_FILE,sntp_client_socket_recv_data);		// 服务器端
	create_unix_socket_create(&sntp_cliet_socket);
	usleep( 100000 );

	dprintf("init_sntp_client OK............\n");
	return 0;
}

int deinit_sntp_client(void)
{
	deinit_unix_socket(&sntp_cliet_socket);
	return 0;
}

//czn_20161014_s
void sntp_client_socket_recv_data(char* pbuf, int len)
{
	// lzh_20181030_s
	// SNTP_CTRL_T* ptr_sntp_ctrl = (SNTP_CTRL_T*)pbuf;
	SNTP_SERVER_PACK_T* ptr_sntp_server = (SNTP_SERVER_PACK_T*)pbuf;
	switch( ptr_sntp_server->cmd )
	// lzh_20181030_e		
	{
		case SNTP_CTRL_UDP_SYNC_TIME_RSP:
			printf("rcv SNTP_CTRL_UDP_SYNC_TIME_RSP rsp\n");
			// lzh_20181030_s
#if 0
			printf("ptr_sntp_ctrl->target_ip = 0x%08x, sync_time_serverip = 0x%08x\n",ptr_sntp_ctrl->target_ip, sync_time_serverip);
			//if(ptr_sntp_ctrl->target_ip == sync_time_serverip)
			{
				sync_time_result = 0;
				sem_post(&sntp_client_sem);
			}
#else
			sntp_server_response = *ptr_sntp_server;
			printf("sntp_server_response.ntp_server_ip = 0x%08x, sync_time_serverip = 0x%08x\n",sntp_server_response.ntp_server_ip, sync_time_serverip);
			if( sntp_server_response.ntp_server_ip == sync_time_serverip )
				sync_time_result = 0;
			else
				sync_time_result = 1;
			sem_post(&sntp_client_sem);
#endif
			// lzh_20181030_e
			break;	

	}	
}

int sync_time_from_sntp_server( int sntp_server_ip, int timezone )
{
	int				ret;
	SNTP_CTRL_T 	temp;

	pthread_mutex_lock(&sntp_client_lock);
	
	temp.cmd		= SNTP_CTRL_UDP_SYNC_TIME_REQ;
	temp.target_ip	= sntp_server_ip;
	temp.timezone	= timezone;

	sync_time_serverip = sntp_server_ip;
	sem_init(&sntp_client_sem,0,0);
	
	unix_socket_send_data( &sntp_cliet_socket, (char*)&temp, sizeof(SNTP_CTRL_T));
	
	if( sem_wait_ex2(&sntp_client_sem, 5000) != 0 )
	{
		ret = -1;
	}
	else
		ret = sync_time_result;
	
	// lzh_20181030_s
	if( ret == 0 )
	{
		printf("----sync_time_from_sntp_server ok!-----\n");
		sync_time_rsp_done(&sntp_server_response.ntp_data,sntp_server_response.ntp_server_ip);
		usleep(100*1000);
		SetTimeUpdateFlag(1);
		SaveDateAndTimeToFile();
		//UpdateLocalTimeToStm8l();
		//system("date");
	}
	else
		printf("----sync_time_from_sntp_server error!-----\n");
	// lzh_20181030_e

	pthread_mutex_unlock(&sntp_client_lock);

	return ret;
	//usleep(1000*1000);	// send once again  sntp连续申请需要1s延时，否则129.6.15.29 服务器会第二次发送一个不同时区的时间?
	//unix_socket_send_data( &sntp_cliet_socket, (char*)&temp, sizeof(SNTP_CTRL_T));
}


