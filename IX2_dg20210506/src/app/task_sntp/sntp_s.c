

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>


#include "sntp_s.h"

/*******************************
discription:
  当地时间转格林时间。
parameter:
  pTvSec--输入、输出，转换前后的时间秒值。
*******************************/
static int local_time_to_utc(void *pTvSec)
{
    struct timezone tzTimeZone;
	struct timeval tvTiemValue;
	unsigned int uiSecond = 0;
	
	time_t *pTvSecH = (time_t *)pTvSec;
	uiSecond = *(unsigned int*)pTvSecH;
	
	//获取本地时区.
	gettimeofday(&tvTiemValue, &tzTimeZone);
	//本地时间转换成格林时间.
	uiSecond -= ((tzTimeZone.tz_minuteswest) * 60); 
	
	*pTvSecH = (time_t)uiSecond;
	return 0;
}





/*************************************************
discription:
  计算当前从1900年1月1日到指定日期一共有多少天。
parameter:
  iYear--输入，年。
  iMonth--输入，月。
  iDay--输入，日。
return:
  1900年1月1日到指定日期一共有多少天。
*************************************************/
static unsigned int get_julian_day(int iYear, int iMonth, int iDay)
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
  
  if (iMonth > 2) 
    iMonth = iMonth - 3;
  else 
  {
    iMonth = iMonth + 9;
    iYear = iYear - 1;
  }
  x = iYear / 100;
  y = iYear - 100 * x;
  z = (146097 * x) / 4 + (1461 * y) / 4 + (153 * iMonth + 2) / 5 + iDay + 1721119;
  
  return z;
}





/*******************************************
description:
  把格林时间mTime转换成utc时间。
parameter:
  tmTime--输入，格林时间。
  puiSeconds--输出，输出的ntp时间，秒值。
*******************************************/
static int create_ntp_time(const struct tm tmTime, unsigned int *puiSeconds)
{
  //Currently this function only operates correctly in 
  //the 1900 - 2036 primary epoch defined by NTP
  unsigned int uiJD = 0;
  if(puiSeconds == NULL)
  {
      printf("create_ntp_time() error!\n");
	  return -1;
  }
  uiJD = get_julian_day((1900 + tmTime.tm_year)
  						,( 1 + tmTime.tm_mon)
  						, tmTime.tm_mday);
  
  uiJD -= JAN_1ST_1900;
  uiJD = (uiJD * 24) + tmTime.tm_hour;
  uiJD = (uiJD * 60) + tmTime.tm_min;
  uiJD = (uiJD * 60) + tmTime.tm_sec;
  
  *puiSeconds = uiJD;
  return 0;
}




/***************************************
description:
  获取系统当前格林时间。
parameter:
  puiSec--输出，当前系统ntp时间秒值。
***************************************/
int get_current_ntp_time(unsigned int *puiSec)
{
  //the 1900 - 2036 primary epoch defined by NTP
  	time_t timeSec;
	struct tm *pTm;
	unsigned int uiSeconds = 0;
    //取得当地时间，该时间秒数值从1970年开始。
  	time(&timeSec);
	
	#if (0)
    pTm=localtime(&timeSec); 
	printf("get_current_ntp_time local:%d-%02d-%02d %02d:%02d:%02d\n"  
		   ,(1900+pTm->tm_year),( 1+pTm->tm_mon), pTm->tm_mday  
	       ,pTm->tm_hour, pTm->tm_min, pTm->tm_sec);
	#endif
	
	//根据时区，将当地时间转格林时间。
	local_time_to_utc(&timeSec); 

	//把格林时间秒转换成年月日。
    pTm=localtime(&timeSec); 
	
	#if (0)
	printf("utc:%d-%02d-%02d %02d:%02d:%02d\n"  
		   ,(1900+pTm->tm_year),( 1+pTm->tm_mon), pTm->tm_mday  
	       ,pTm->tm_hour, pTm->tm_min, pTm->tm_sec);
	#endif
	
	//把格林时间年月日，转成秒值，该秒值从1900年开始。
	create_ntp_time(*pTm, &uiSeconds);

	*puiSec = uiSeconds;
	return 0;
}


#if 0
/********************************************
description:
  设置系统时区。
parameter:
  iTZMinutesWest--输入，当前时间相对格林时间的时差。
********************************************/
int set_timezone(int iTZMinutesWest)
{
	struct timezone tzTimeZone;
	struct timeval tvTimeValue;
	
	if(0 != (iTZMinutesWest % 60))
	{
		printf("number of minutes is invalid!\n");
		return -1;
	}
	
	tzTimeZone.tz_minuteswest = iTZMinutesWest;
	tzTimeZone.tz_dsttime = 0;
	
	gettimeofday(&tvTimeValue, NULL);
	settimeofday(&tvTimeValue,&tzTimeZone);

	return 0;
}
#else
/*********************************************************************
description:  得到系统时区，每个时区为1小时，大于0表示为东时区，小于等于0为西时区
parameter:  	none
return:		当前时间相对格林时间的时区
*********************************************************************/
int get_timezone(void)
{
	struct timezone tzTimeZone;
	struct timeval tvTimeValue;
	int timezone;
	
	gettimeofday(&tvTimeValue, &tzTimeZone);

	timezone = tzTimeZone.tz_minuteswest/60;
	
	return timezone;
}

/*********************************************************************
description:  设置系统时区，，大于0表示为东时区，小于等于0为西时区
parameter:  	timezone--输入，当前时间相对格林时间的时区
return:		0
*********************************************************************/
int set_timezone(int timezone )
{
	struct timezone tzTimeZone;
	struct timeval tvTimeValue;
	
	tzTimeZone.tz_minuteswest = timezone*60;	// 得到分钟数
	tzTimeZone.tz_dsttime = 0;
	
	gettimeofday(&tvTimeValue, NULL);
	settimeofday(&tvTimeValue,&tzTimeZone);

	return 0;
}

#endif



