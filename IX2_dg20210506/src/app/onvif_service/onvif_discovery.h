

#ifndef _ONVIF_DISCOVERY_H_
#define _ONVIF_DISCOVERY_H_

#include "obj_ipc_manager.h"

int onvif_device_discovery(void);
int one_ipc_Login(const char* device_url, const char* username, const char* password, onvif_login_info_t* info);
int GetNameByIp(const char* pIP, char* devName);

#endif
