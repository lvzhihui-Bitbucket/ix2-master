#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "soapH.h"
#include "wsdd.nsmap"
#include "soapStub.h"

#include <fcntl.h>  
#include <sys/socket.h>  
#include <sys/types.h>  
//#include <uuid/uuid.h>  
#include <netinet/in.h>  

#include <dlfcn.h>

#include "wsseapi.h"
#include "threads.h"
#include "wsaapi.h"
#include "smdevp.h"
#include "mecevp.h"

#include "base64.h"
#include "onvif_discovery.h"

#define ONVIF_USER			"admin"
#define ONVIF_PASSWORD		"1234abcd"  		// HK,dahua
const char HIKVISION_profile1[]="rtsp://admin:1234abcd@192.168.243.24:554/Streaming/Channels/102?transportmode=unicast&profile=Profile_2";


/*
ONVIF中不管是客户端还是设备端，最先实现的接口都是关于能力的那个接口，
在客户端实现的函数名也就是[soap_call___tds__GetServiceCapabilities]通过获取的接口才知道设备具有那些能力，能够进行那些操作，
服务端最基本的也需要实现这接口，让客户端知道设备支持那些基本操作。但是当设备端作了加密处理的话，即使你实现了这些接口，也不能正常获取到参数的，
所以需要在获取设备参数之前，每次都需要作鉴权处理，这也是为什么我在前篇搜索的例程中把ONVIF_Initsoap这个接口函数里面的操作单独用一个函数接口来完成的原因。
下面，我们就需要在这个接口函数里处理鉴权的问题了首先ONVIF鉴权是有两种方式的，一种是通过实现soap_wsse_add_UsernameTokenDigest函数，
然后又实现soap_wsse_add_UsernameTokenText等一些复杂的函数接口，我第一次也是用这个鉴权方式做，但是太麻烦，而且最重要的是鉴权还是失败的，
最后无奈网上各种问，终于发现了一个很简单直观的方法，虽然还是不明白为什么，但是确认达到了鉴权的效果了,
在前篇中已经介绍过Onvif_Initsoap这个接口了鉴权的主要操作也是在这个接口里作的认证，具体鉴权实现如下：
*/

typedef struct  
{  
    char username[64];  
    char password[32];  
}UserInfo_S;  

static void ONVIF_GenrateDigest(unsigned char *pwddigest_out, unsigned char *pwd, char *nonc, char *time)  
{  
   const unsigned char *tdist;  
    unsigned char dist[1024] = {0};  
    char tmp[1024] = {0};  
    unsigned char bout[1024] = {0};  
    strcpy(tmp,nonc);  
    base64_64_to_bits((char*)bout, tmp);  
    sprintf(tmp,"%s%s%s",bout,time,pwd);  
    SHA1((const unsigned char*)tmp,strlen((const char*)tmp),dist);  
    tdist = dist;  
    memset(bout,0x0,1024);  
    base64_bits_to_64(bout,tdist,(int)strlen((const char*)tdist));  
    strcpy((char *)pwddigest_out,(const char*)bout);  
}  
  
void ONVIF_soap_wsse_add_UsernameTokenDigest0(struct soap *soap, UserInfo_S* pUserInfo)
{
	#if 0
	soap->header->wsse__Security = (struct _wsse__Security *)malloc(sizeof(struct _wsse__Security));  
	memset(soap->header->wsse__Security, 0 , sizeof(struct _wsse__Security));  

	soap->header->wsse__Security->UsernameToken = (struct _wsse__UsernameToken *)calloc(1,sizeof(struct _wsse__UsernameToken));  
	soap->header->wsse__Security->UsernameToken->Username = (char *)malloc(64);  
	memset(soap->header->wsse__Security->UsernameToken->Username, '\0', 64);  

	soap->header->wsse__Security->UsernameToken->Nonce = (char*)malloc(64);  
	memset(soap->header->wsse__Security->UsernameToken->Nonce, '\0', 64);  
	strcpy(soap->header->wsse__Security->UsernameToken->Nonce,"LKqI6G/AikKCQrN0zqZFlg=="); //注意这里  

	soap->header->wsse__Security->UsernameToken->wsu__Created = (char*)malloc(64);  
	memset(soap->header->wsse__Security->UsernameToken->wsu__Created, '\0', 64);  
	strcpy(soap->header->wsse__Security->UsernameToken->wsu__Created,"2010-09-16T07:50:45Z");  

	strcpy(soap->header->wsse__Security->UsernameToken->Username, pUserInfo->username);  
	soap->header->wsse__Security->UsernameToken->Password = (struct _wsse__Password *)malloc(sizeof(struct _wsse__Password));  
	soap->header->wsse__Security->UsernameToken->Password->Type = (char*)malloc(128);  
	memset(soap->header->wsse__Security->UsernameToken->Password->Type, '\0', 128);  
	strcpy(soap->header->wsse__Security->UsernameToken->Password->Type, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest");  
	soap->header->wsse__Security->UsernameToken->Password->__item = (char*)malloc(128);  
	ONVIF_GenrateDigest((unsigned char*)soap->header->wsse__Security->UsernameToken->Password->__item, (unsigned char*)pUserInfo->password,soap->header->wsse__Security->UsernameToken->Nonce,soap->header->wsse__Security->UsernameToken->wsu__Created);  	
	#endif
}

void ONVIF_soap_wsse_add_UsernameTokenDigest(struct soap *soap, char* username, char* password)
{
	UserInfo_S stUserInfo;
    strcpy(stUserInfo.username, username);  
    strcpy(stUserInfo.password, password);  
	ONVIF_soap_wsse_add_UsernameTokenDigest0(soap,&stUserInfo);
}


// 设备鉴权：  鉴权的实现可以很简单也可以很难，这里笔者使用的是gsoap提供的方法：直接调用即可：
// soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);  


// 获取能力：soap 的http消息通信，参考代码：
int UserGetCapabilities(struct soap *soap  ,struct __wsdd__ProbeMatches *resp,  struct _tds__GetCapabilities *capa_req,struct _tds__GetCapabilitiesResponse *capa_resp)  
{  
	int ret = 0;
    capa_req->Category = (enum tt__CapabilityCategory *)soap_malloc(soap, sizeof(int));  
    capa_req->__sizeCategory = 1;  
    *(capa_req->Category) = (enum tt__CapabilityCategory)(tt__CapabilityCategory__Media);  
  
    capa_resp->Capabilities = (struct tt__Capabilities*)soap_malloc(soap,sizeof(struct tt__Capabilities)) ;  
  
    //soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);  
	ONVIF_soap_wsse_add_UsernameTokenDigest(soap,ONVIF_USER, ONVIF_PASSWORD);

    printf("\n--------------------Now Gettting Capabilities NOW --------------------\n");  
  
    int result = soap_call___tds__GetCapabilities(soap, resp->wsdd__ProbeMatches->ProbeMatch->XAddrs, NULL, capa_req, capa_resp);  
  
    if (soap->error)  
    {  
            printf("[%s][%d]--->>> soap error: %d, %s, %s\n", __func__, __LINE__, soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
            int retval = soap->error;  
			ret = -1;
    }  
    else  
    {  
        if(capa_resp->Capabilities==NULL)  
        {  
            printf(" GetCapabilities  failed!  result=%d \n",result);  
			ret = -1;
        }  
        else  
        {  
            printf(" Media->XAddr=%s \n", capa_resp->Capabilities->Media->XAddr);  
            printf(" Media->StreamingCapabilities->RTPMulticast=%d \n", capa_resp->Capabilities->Media->StreamingCapabilities->RTPMulticast?1:0);
            printf(" Media->StreamingCapabilities->RTP_USCORETCP=%d \n", capa_resp->Capabilities->Media->StreamingCapabilities->RTP_USCORETCP?1:0);

            printf(" Extension->DeviceIO->VideoSources=%d \n", capa_resp->Capabilities->Extension->DeviceIO->VideoSources?1:0);
            printf(" Extension->DeviceIO->VideoOutputs=%d \n", capa_resp->Capabilities->Extension->DeviceIO->VideoOutputs?1:0);
            printf(" Extension->DeviceIO->AudioSources=%d \n", capa_resp->Capabilities->Extension->DeviceIO->AudioSources?1:0);
            printf(" Extension->DeviceIO->AudioOutputs=%d \n", capa_resp->Capabilities->Extension->DeviceIO->AudioOutputs?1:0);
            printf(" Extension->DeviceIO->RelayOutputs=%d \n", capa_resp->Capabilities->Extension->DeviceIO->RelayOutputs?1:0);					
            printf(" Extension->DeviceIO->__size=%d \n", capa_resp->Capabilities->Extension->DeviceIO->__size);
            printf(" Extension->DeviceIO->__any=%s \n", capa_resp->Capabilities->Extension->DeviceIO->__any);

            printf(" Extension->Display=%s \n", capa_resp->Capabilities->Extension->Display);		
            printf(" Extension->Recording=%s \n", capa_resp->Capabilities->Extension->Recording);
            printf(" Extension->Search=%s \n", capa_resp->Capabilities->Extension->Search);
            printf(" Extension->Replay=%s \n", capa_resp->Capabilities->Extension->Replay);
            printf(" Extension->Receiver =%s \n", capa_resp->Capabilities->Extension->Receiver );
            printf(" Extension->AnalyticsDevice =%s \n", capa_resp->Capabilities->Extension->AnalyticsDevice );
            printf(" Extension->Extensions =%s \n", capa_resp->Capabilities->Extension->Extensions );
            printf(" Extension->__size=%d \n", capa_resp->Capabilities->Extension->__size);
            printf(" Extension->__any=%s \n", capa_resp->Capabilities->Extension->__any);
			
        }  
    } 
	return ret;
}  

// 获取媒体信息Profile：soap 的http消息通信，参考代码：
int UserGetProfiles(struct soap *soap,struct _trt__GetProfiles *trt__GetProfiles,  struct _trt__GetProfilesResponse *trt__GetProfilesResponse ,struct _tds__GetCapabilitiesResponse *capa_resp)  
{  
	int ret = 0;
    int result=0 ;  
  
    printf("\n-------------------Getting Onvif Devices Profiles--------------\n");  
    //soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);  
	ONVIF_soap_wsse_add_UsernameTokenDigest(soap,ONVIF_USER, ONVIF_PASSWORD);

    result = soap_call___trt__GetProfiles(soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetProfiles, trt__GetProfilesResponse);  
    if (result==-1)  
    //NOTE: it may be regular if result isn't SOAP_OK.Because some attributes aren't supported by server.  
    //any question email leoluopy@gmail.com  
    {  
        printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
        result = soap->error;  
		ret = -1;
    }  
    else
	{  
        printf("-------------------Profiles Get OK--------------\n");  
        if(trt__GetProfilesResponse->Profiles!=NULL)  
        {  
			printf("Profiles[0] Name:%s  \n",trt__GetProfilesResponse->Profiles[0].Name);  
			printf("Profiles[1] Name:%s  \n",trt__GetProfilesResponse->Profiles[1].Name);    
			printf("Profiles[0] Taken:%s\n",trt__GetProfilesResponse->Profiles[0].token);  
			printf("Profiles[1] Taken:%s\n",trt__GetProfilesResponse->Profiles[1].token);  

			// video information
			printf("Profiles[0] VideoSourceConfiguration->Name:%s\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->Name);  
			printf("Profiles[1] VideoSourceConfiguration->Name:%s\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->Name);  

			printf("Profiles[0] VideoSourceConfiguration->Taken:%s\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->token);  
			printf("Profiles[1] VideoSourceConfiguration->Taken:%s\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->token);  

			printf("Profiles[0] VideoSourceConfiguration->SourceToken:%s\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->SourceToken);  
			printf("Profiles[1] VideoSourceConfiguration->SourceToken:%s\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->SourceToken);  

			printf("Profiles[0] VideoSourceConfiguration->UseCount:%d\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->UseCount);  
			printf("Profiles[1] VideoSourceConfiguration->UseCount:%d\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->UseCount);  

			printf("Profiles[0] VideoSourceConfiguration->Bounds->x:%d\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->Bounds->x);  
			printf("Profiles[1] VideoSourceConfiguration->Bounds->x:%d\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->Bounds->x);  
			 
			printf("Profiles[0] VideoSourceConfiguration->Bounds->y:%d\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->Bounds->y);  
			printf("Profiles[1] VideoSourceConfiguration->Bounds->y:%d\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->Bounds->y);  

			printf("Profiles[0] VideoSourceConfiguration->Bounds->height :%d\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->Bounds->height );  
			printf("Profiles[1] VideoSourceConfiguration->Bounds->height :%d\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->Bounds->height );  

			printf("Profiles[0] VideoSourceConfiguration->Bounds->width  :%d\n",trt__GetProfilesResponse->Profiles[0].VideoSourceConfiguration->Bounds->width  );  
			printf("Profiles[1] VideoSourceConfiguration->Bounds->width  :%d\n",trt__GetProfilesResponse->Profiles[1].VideoSourceConfiguration->Bounds->width  );  

			// encoder information
			printf("Profiles[0] VideoEncoderConfiguration ->Name:%s\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration ->Name);  
			printf("Profiles[1] VideoEncoderConfiguration ->Name:%s\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration ->Name);  

			printf("Profiles[0] VideoEncoderConfiguration ->Taken:%s\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration ->token);  
			printf("Profiles[1] VideoEncoderConfiguration ->Taken:%s\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration ->token);  

			printf("Profiles[0] VideoEncoderConfiguration ->UseCount:%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->UseCount);  
			printf("Profiles[1] VideoEncoderConfiguration ->UseCount:%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->UseCount);  

			printf("Profiles[0] VideoEncoderConfiguration ->Quality :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->Quality );  
			printf("Profiles[1] VideoEncoderConfiguration ->Quality :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->Quality );  
			 
			printf("Profiles[0] VideoEncoderConfiguration ->Resolution->Height :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->Resolution->Height );  
			printf("Profiles[1] VideoEncoderConfiguration ->Resolution->Height :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->Resolution->Height );  

			printf("Profiles[0] VideoEncoderConfiguration ->Resolution->Width  :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->Resolution->Width  );  
			printf("Profiles[1] VideoEncoderConfiguration ->Resolution->Width  :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->Resolution->Width  );  

			printf("Profiles[0] VideoEncoderConfiguration ->RateControl->FrameRateLimit   :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->RateControl->FrameRateLimit   );  
			printf("Profiles[1] VideoEncoderConfiguration ->RateControl->FrameRateLimit   :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->RateControl->FrameRateLimit   );  
			
			printf("Profiles[0] VideoEncoderConfiguration ->RateControl->EncodingInterval    :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->RateControl->EncodingInterval    );  
			printf("Profiles[1] VideoEncoderConfiguration ->RateControl->EncodingInterval    :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->RateControl->EncodingInterval    );  

			printf("Profiles[0] VideoEncoderConfiguration ->RateControl->BitrateLimit     :%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->RateControl->BitrateLimit     );  
			printf("Profiles[1] VideoEncoderConfiguration ->RateControl->BitrateLimit     :%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->RateControl->BitrateLimit     );  

			printf("Profiles[0] VideoEncoderConfiguration ->H264->H264Profile:%d,GovLength=%d\n",trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->H264->H264Profile,trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->H264->GovLength);
			printf("Profiles[1] VideoEncoderConfiguration ->H264->H264Profile:%d,GovLength=%d\n",trt__GetProfilesResponse->Profiles[1].VideoEncoderConfiguration->H264->H264Profile,trt__GetProfilesResponse->Profiles[0].VideoEncoderConfiguration->H264->GovLength);	
			
		}  
        else
		{  
            printf("Profiles Get inner Error\n"); 
			ret = -1;			
        }  
    }  
    printf("Profiles Get Procedure over\n\n");  

  	return ret;
}  


// 获取RTSP的URI：soap 的http消息通信，参考代码：
int UserGetUri(struct soap *soap,int channel, struct _trt__GetStreamUri *trt__GetStreamUri,struct _trt__GetStreamUriResponse *trt__GetStreamUriResponse,  
         struct _trt__GetProfilesResponse *trt__GetProfilesResponse,struct _tds__GetCapabilitiesResponse *capa_resp)  
{  
	int ret = 0;
    int result=0 ;  

	if( channel > 1 ) channel = 1;

    printf("\n\n---------------Getting chanel %d rtsp url----------------\n",channel);  

	//enum tt__StreamType { tt__StreamType__RTP_Unicast = 0, tt__StreamType__RTP_Multicast = 1 };
    trt__GetStreamUri->StreamSetup = (struct tt__StreamSetup*)soap_malloc(soap,sizeof(struct tt__StreamSetup));//初始化，分配空间  
    trt__GetStreamUri->StreamSetup->Stream = 0;//stream type  
  
	//enum tt__TransportProtocol { tt__TransportProtocol__UDP = 0, tt__TransportProtocol__TCP = 1, tt__TransportProtocol__RTSP = 2, tt__TransportProtocol__HTTP = 3 };
    trt__GetStreamUri->StreamSetup->Transport = (struct tt__Transport *)soap_malloc(soap, sizeof(struct tt__Transport));//初始化，分配空间  
    trt__GetStreamUri->StreamSetup->Transport->Protocol = 0;  
    trt__GetStreamUri->StreamSetup->Transport->Tunnel = 0;  
    trt__GetStreamUri->StreamSetup->__size = 1;  
    trt__GetStreamUri->StreamSetup->__any = NULL;  
    trt__GetStreamUri->StreamSetup->__anyAttribute =NULL;  
  
  
    trt__GetStreamUri->ProfileToken = trt__GetProfilesResponse->Profiles[channel].token ;  
  
    //soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);  
	ONVIF_soap_wsse_add_UsernameTokenDigest(soap,ONVIF_USER, ONVIF_PASSWORD);

    soap_call___trt__GetStreamUri(soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetStreamUri, trt__GetStreamUriResponse);  
  
  
    if (soap->error) 
	{  
	    printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
	    result = soap->error;    
		ret = -1;
    }  
    else
	{  
        printf("RTSP Addr Get Done is :%s \n",trt__GetStreamUriResponse->MediaUri->Uri);  
    }  
	return ret;
}  


// soapStub.h中定义：
// SOAP_FMAC5 int SOAP_FMAC6 soap_call___tds__GetServices(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _tds__GetServices *tds__GetServices, struct _tds__GetServicesResponse *tds__GetServicesResponse);
// 参数const char* soap_endpoint参数指的是媒体服务地址
int UserGetServices(struct soap *soap, struct _tds__GetServices *tds__GetServices, struct _tds__GetServicesResponse *tds__GetServicesResponse, struct _tds__GetCapabilitiesResponse *capa_resp)
{  
	int ret = 0;
    int result=0 ;  

    printf("\n\n---------------Getting Services----------------\n");  

	soap_call___tds__GetServices(soap, capa_resp->Capabilities->Media->XAddr, NULL, tds__GetServices, tds__GetServicesResponse);

    if (soap->error) 
	{  
		printf("soap error: %d, %s, %s\n\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{  
        printf("tds__GetServicesResponse->Service[0].XAddr: %s \n",tds__GetServicesResponse->Service[0].XAddr);  
    }  
	return ret;
}

// soapStub.h中定义：
// SOAP_FMAC5 int SOAP_FMAC6 soap_call___tmd__GetVideoSources(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _trt__GetVideoSources *trt__GetVideoSources, struct _trt__GetVideoSourcesResponse *trt__GetVideoSourcesResponse);
int UserGetVideoSources(struct soap *soap, struct _trt__GetVideoSources *trt__GetVideoSources, struct _trt__GetVideoSourcesResponse *trt__GetVideoSourcesResponse, struct _tds__GetCapabilitiesResponse *capa_resp)
{  
	int ret = 0;
    int result=0 ;  

    printf("\n\n---------------Getting video Services----------------\n");  

	soap_call___tmd__GetVideoSources(soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetVideoSources, trt__GetVideoSourcesResponse);

    if (soap->error) 
	{  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{  
        printf("trt__GetVideoSourcesResponse->__sizeVideoSources: %d \n",trt__GetVideoSourcesResponse->__sizeVideoSources);  
        printf("trt__GetVideoSourcesResponse->VideoSources[0].Framerate : %d \n",trt__GetVideoSourcesResponse->VideoSources[0].Framerate);  
        printf("trt__GetVideoSourcesResponse->VideoSources[0].Resolution->Height : %d \n",trt__GetVideoSourcesResponse->VideoSources[0].Resolution->Height);  
        printf("trt__GetVideoSourcesResponse->VideoSources[0].Resolution->Width  : %d \n",trt__GetVideoSourcesResponse->VideoSources[0].Resolution->Width );  
        printf("trt__GetVideoSourcesResponse->VideoSources[0].token : %s\n",trt__GetVideoSourcesResponse->VideoSources[0].token );  

		if( trt__GetVideoSourcesResponse->VideoSources[0].Imaging != NULL )
		{
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Brightness[0] : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Brightness[0] );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->ColorSaturation[0] : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->ColorSaturation[0] );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Contrast[0] : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Contrast[0] );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Sharpness[0] : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->Sharpness[0] );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->BacklightCompensation->Mode : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->BacklightCompensation->Mode );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->BacklightCompensation->Level : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->BacklightCompensation->Level );  

			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WideDynamicRange->Mode : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WideDynamicRange->Mode );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WideDynamicRange->Level : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WideDynamicRange->Level );  

			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->Mode : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->Mode );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->CrGain : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->CrGain );  
			printf("trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->CbGain : %d\n",trt__GetVideoSourcesResponse->VideoSources[0].Imaging->WhiteBalance->CbGain ); 
		}
    }  
    printf("---------------Getting video Services ok----------------\n");  
	return ret;
}

// soapStub.h中定义：
// SOAP_FMAC5 int SOAP_FMAC6 soap_call___trt__GetVideoEncoderConfigurationOptions(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _trt__GetVideoEncoderConfigurationOptions *trt__GetVideoEncoderConfigurationOptions, struct _trt__GetVideoEncoderConfigurationOptionsResponse *trt__GetVideoEncoderConfigurationOptionsResponse);
int UserGetVideoEncoderConfigurationOptions(struct soap *soap, int channel, struct _trt__GetVideoEncoderConfigurationOptions *trt__GetVideoEncoderConfigurationOptions, struct _trt__GetVideoEncoderConfigurationOptionsResponse *trt__GetVideoEncoderConfigurationOptionsResponse, struct _trt__GetProfilesResponse *trt__GetProfilesResponse, struct _tds__GetCapabilitiesResponse *capa_resp,struct _trt__GetStreamUriResponse *trt__GetStreamUriResponse, struct _tds__GetServicesResponse *tds__GetServicesResponse )
{
	int ret = 0;
	int result=0 ;  

	if( channel > 1 ) channel = 1;

    printf("\n\n---------------Getting chanel %d Video Encoder Configuration Options----------------\n",channel);  

	trt__GetVideoEncoderConfigurationOptions->ConfigurationToken = trt__GetProfilesResponse->Profiles[channel].VideoEncoderConfiguration->token;

	soap_call___trt__GetVideoEncoderConfigurationOptions( soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetVideoEncoderConfigurationOptions, trt__GetVideoEncoderConfigurationOptionsResponse );
	//soap_call___trt__GetVideoEncoderConfigurationOptions( soap, trt__GetStreamUriResponse->MediaUri->Uri, NULL, trt__GetVideoEncoderConfigurationOptions, trt__GetVideoEncoderConfigurationOptionsResponse );
	//soap_call___trt__GetVideoEncoderConfigurationOptions( soap, tds__GetServicesResponse->Service[0].XAddr, NULL, trt__GetVideoEncoderConfigurationOptions, trt__GetVideoEncoderConfigurationOptionsResponse );
	
    if (soap->error) 
	{  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{  
        printf("Options->QualityRange=(%d-%d)\n",trt__GetVideoEncoderConfigurationOptionsResponse->Options->QualityRange->Min,trt__GetVideoEncoderConfigurationOptionsResponse->Options->QualityRange->Max);
        printf("Options->H264->__sizeResolutionsAvailable%d\n",trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->__sizeResolutionsAvailable);

		struct tt__VideoResolution *ResolutionsAvailable;
        ResolutionsAvailable = trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->ResolutionsAvailable;
        int count = trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->__sizeResolutionsAvailable;
		int i;
        for( i=0; i<count; i++)
        {
            printf("Options->H264->ResolutionsAvailable[%d]=(%d x %d)\n",i,ResolutionsAvailable->Width,ResolutionsAvailable->Height);
            ResolutionsAvailable++;
        }        
        printf(" Options->H264->GovLengthRange=(%d ~ %d) \n",           trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->GovLengthRange->Min,trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->GovLengthRange->Max);
        printf(" Options->H264->FrameRateRange=(%d ~ %d) \n",           trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->FrameRateRange->Min,trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->FrameRateRange->Max);
        printf(" Options->H264->EncodingIntervalRange=(%d ~ %d)\n",     trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->EncodingIntervalRange->Min,trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->EncodingIntervalRange->Max);
        printf(" Options->H264->__sizeH264ProfilesSupported=%d \n",     trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->__sizeH264ProfilesSupported);
        printf(" Options->H264->H264ProfilesSupported=%d \n",           *trt__GetVideoEncoderConfigurationOptionsResponse->Options->H264->H264ProfilesSupported);
        if(trt__GetVideoEncoderConfigurationOptionsResponse->Options->Extension!=NULL)
		{
            printf(" Options->Extension->H264->BitrateRange=(%d ~ %d) \n",  trt__GetVideoEncoderConfigurationOptionsResponse->Options->Extension->H264->BitrateRange->Min,trt__GetVideoEncoderConfigurationOptionsResponse->Options->Extension->H264->BitrateRange->Max);
		}
    }  
	return ret;
}

// soapStub.h中定义：
//SOAP_FMAC5 int SOAP_FMAC6 soap_call___trt__GetVideoEncoderConfigurations(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _trt__GetVideoEncoderConfigurations *trt__GetVideoEncoderConfigurations, struct _trt__GetVideoEncoderConfigurationsResponse *trt__GetVideoEncoderConfigurationsResponse);
int UserGetVideoEncoderConfigurations(struct soap *soap, struct _trt__GetVideoEncoderConfigurations *trt__GetVideoEncoderConfigurations, struct _trt__GetVideoEncoderConfigurationsResponse *trt__GetVideoEncoderConfigurationsResponse, struct _trt__GetProfilesResponse *trt__GetProfilesResponse, struct _tds__GetCapabilitiesResponse *capa_resp )
{
	int ret = 0;
	int result=0 ;  

    printf("\n\n---------------Getting all Video Encoder Configurations----------------\n");  

	soap_call___trt__GetVideoEncoderConfigurations( soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetVideoEncoderConfigurations, trt__GetVideoEncoderConfigurationsResponse );

    if (soap->error) {  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{ 
        printf("trt__GetVideoEncoderConfigurationsResponse->__sizeConfigurations=%d\n",trt__GetVideoEncoderConfigurationsResponse->__sizeConfigurations);
		int i;
		for( i = 0; i < trt__GetVideoEncoderConfigurationsResponse->__sizeConfigurations; i++ )
		{
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].Name:%s\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].Name); 		
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].Taken:%s\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].token);  
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].UseCount:%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].UseCount);  
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].Quality :%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].Quality );  		 
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].Resolution->Height :%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].Resolution->Height );  
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].Resolution->Width  :%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].Resolution->Width  );  
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].RateControl->FrameRateLimit   :%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].RateControl->FrameRateLimit);  	
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].RateControl->EncodingInterval    :%d\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].RateControl->EncodingInterval);  
			printf("trt__GetVideoEncoderConfigurationsResponse->Configurations[%d].RateControl->BitrateLimit     :%d\n\n",i,trt__GetVideoEncoderConfigurationsResponse->Configurations[i].RateControl->BitrateLimit);  
		}
    }  	
	return ret;
}

// soapStub.h中定义：
//SOAP_FMAC5 int SOAP_FMAC6 soap_call___trt__GetVideoEncoderConfiguration(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _trt__GetVideoEncoderConfiguration *trt__GetVideoEncoderConfiguration, struct _trt__GetVideoEncoderConfigurationResponse *trt__GetVideoEncoderConfigurationResponse);
int UserGetVideoEncoderConfiguration(struct soap *soap, int channel, struct _trt__GetVideoEncoderConfiguration *trt__GetVideoEncoderConfiguration, struct _trt__GetVideoEncoderConfigurationResponse *trt__GetVideoEncoderConfigurationResponse, struct _trt__GetProfilesResponse *trt__GetProfilesResponse, struct _tds__GetCapabilitiesResponse *capa_resp )
{
	int ret = 0;
	int result=0 ;  

	if( channel > 1 ) channel = 1;

    printf("\n\n---------------Getting chanel %d Video Encoder Configuration----------------\n",channel);  

	trt__GetVideoEncoderConfiguration->ConfigurationToken = trt__GetProfilesResponse->Profiles[channel].VideoEncoderConfiguration->token;

	soap_call___trt__GetVideoEncoderConfiguration( soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetVideoEncoderConfiguration, trt__GetVideoEncoderConfigurationResponse );

    if (soap->error) {  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{ 
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->Name:%s\n",trt__GetVideoEncoderConfigurationResponse->Configuration->Name);  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->Taken:%s\n",trt__GetVideoEncoderConfigurationResponse->Configuration->token);  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->UseCount:%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->UseCount);  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->Quality :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->Quality );  		 
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->Resolution->Height :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->Resolution->Height );  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->Resolution->Width  :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->Resolution->Width  );  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->FrameRateLimit   :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->FrameRateLimit);  	
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->EncodingInterval    :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->EncodingInterval);  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->BitrateLimit     :%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->RateControl->BitrateLimit);  
		printf("trt__GetVideoEncoderConfigurationResponse->Configuration->H264->H264Profile:%d\n",trt__GetVideoEncoderConfigurationResponse->Configuration->H264->H264Profile);  
		printf("\n");
    }  	
	return ret;
}

// soapStub.h中定义：
//SOAP_FMAC5 int SOAP_FMAC6 soap_call___trt__SetVideoEncoderConfiguration(struct soap *soap, const char *soap_endpoint, const char *soap_action, struct _trt__SetVideoEncoderConfiguration *trt__SetVideoEncoderConfiguration, struct _trt__SetVideoEncoderConfigurationResponse *trt__SetVideoEncoderConfigurationResponse);
int UserSetVideoEncoderConfiguration(struct soap *soap, int channel, struct tt__VideoResolution *Resolution,struct tt__VideoRateControl *RateControl, struct _trt__GetProfilesResponse *trt__GetProfilesResponse, struct _tds__GetCapabilitiesResponse *capa_resp )
{
	int ret = 0;
	int result=0 ; 

	struct _trt__GetVideoEncoderConfiguration gtrt__GetVideoEncoderConfiguration;
	struct _trt__GetVideoEncoderConfigurationResponse gtrt__GetVideoEncoderConfigurationResponse;

	struct _trt__SetVideoEncoderConfiguration strt__SetVideoEncoderConfiguration;
	struct _trt__SetVideoEncoderConfigurationResponse strt__SetVideoEncoderConfigurationResponse;

	if( channel > 1 ) channel = 1;

	memset(&gtrt__GetVideoEncoderConfiguration, 0x00, sizeof(struct _trt__GetVideoEncoderConfiguration));
    memset(&gtrt__GetVideoEncoderConfigurationResponse, 0x00, sizeof(struct _trt__GetVideoEncoderConfigurationResponse));

    printf("\n\n---------------Setting chanel %d Video Encoder Configuration----------------\n",channel);  

	gtrt__GetVideoEncoderConfiguration.ConfigurationToken = trt__GetProfilesResponse->Profiles[channel].VideoEncoderConfiguration->token;

	soap_call___trt__GetVideoEncoderConfiguration( soap, capa_resp->Capabilities->Media->XAddr, NULL, &gtrt__GetVideoEncoderConfiguration, &gtrt__GetVideoEncoderConfigurationResponse );

    if (soap->error) 
	{  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;
    }  
    else
	{
		printf("first get video encoder configuration ok!\n");
	}

	memset(&strt__SetVideoEncoderConfiguration, 0x00, sizeof(struct _trt__SetVideoEncoderConfiguration));
    memset(&strt__SetVideoEncoderConfigurationResponse, 0x00, sizeof(struct _trt__SetVideoEncoderConfigurationResponse));

    strt__SetVideoEncoderConfiguration.ForcePersistence = xsd__boolean__true_;
    strt__SetVideoEncoderConfiguration.Configuration    = gtrt__GetVideoEncoderConfigurationResponse.Configuration;

	if( Resolution != NULL )
	{
        strt__SetVideoEncoderConfiguration.Configuration->Resolution->Width  = Resolution->Width;
        strt__SetVideoEncoderConfiguration.Configuration->Resolution->Height = Resolution->Height;
    }

	if( RateControl != NULL )
	{
        strt__SetVideoEncoderConfiguration.Configuration->RateControl->FrameRateLimit  = RateControl->FrameRateLimit;
        strt__SetVideoEncoderConfiguration.Configuration->RateControl->EncodingInterval = RateControl->EncodingInterval;
        //strt__SetVideoEncoderConfiguration.Configuration->RateControl->BitrateLimit = RateControl->BitrateLimit;
    }
	
	strt__SetVideoEncoderConfiguration.Configuration->H264->H264Profile = 0;   // lzh_20180316

	soap_call___trt__SetVideoEncoderConfiguration( soap, capa_resp->Capabilities->Media->XAddr, NULL, &strt__SetVideoEncoderConfiguration, &strt__SetVideoEncoderConfigurationResponse );

    if (soap->error) {  
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
		result = soap->error;    
		ret = -1;		
    }  
    else
	{
		printf("then set video encoder configuration ok!,width=%d,height=%d,FrameRateLimit=%d,EncodeInterval=%d,H264Profile=%d\n",Resolution->Width,Resolution->Height,RateControl->FrameRateLimit,RateControl->EncodingInterval,strt__SetVideoEncoderConfiguration.Configuration->H264->H264Profile);
    }  	
	return ret;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int get_rtsp_url_from_device_psw1( struct soap *soap, const char* device_addr, const char* username, const char* password, onvif_login_info_t* info)
{
	int i;
	int result = 0;
	int ret = 0;
	int ch;

	// GetServices获取多个媒体地址

	soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 		

	struct _tds__GetServices tds__GetServices;
	struct _tds__GetServicesResponse tds__GetServicesResponse;	   
	tds__GetServices.IncludeCapability = 0;
	soap_call___tds__GetServices(soap,device_addr,NULL, &tds__GetServices, &tds__GetServicesResponse);	
	if(soap->error)
	{ 
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));
		ret = -1;
	}
	else
	{
		printf("[1]Getting Onvif Devices Profiles--sizeService=%d-----\n", tds__GetServicesResponse.__sizeService);	
		for(i=0; i<tds__GetServicesResponse.__sizeService; i++)
		{
			if (tds__GetServicesResponse.Service[i].Namespace != NULL )
			{
				if( (strcmp(tds__GetServicesResponse.Service[i].Namespace, "http://www.onvif.org/ver10/media/wsdl") == 0) || 
					(strcmp(tds__GetServicesResponse.Service[i].Namespace, "http://www.onvif.org/ver20/media/wsdl") == 0) ) 
				{
					break;
				}
			}
		}
		if(i<tds__GetServicesResponse.__sizeService)
		{
			printf("  media_addr[%d] %s\n", i, tds__GetServicesResponse.Service[i].XAddr);

			// 获取媒体信息Profile：
			struct _tr2__GetProfiles tr2__GetProfiles;
			struct _tr2__GetProfilesResponse tr2__GetProfilesResponse;	
						
			soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 		

			tr2__GetProfiles.__sizeType = 1;
			tr2__GetProfiles.Token = NULL;	
			tr2__GetProfiles.Type = NULL;					
			if (soap_call___tr2__GetProfiles(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &tr2__GetProfiles, &tr2__GetProfilesResponse)==-1)  
			{  
				printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
				ret = -1;
			}  
			else
			{  
				if( tr2__GetProfilesResponse.Profiles == NULL )  
				{
					printf("get profiles response error...\n");
					ret = -1;
				}
				else
				{  
					// 获取RTSP的URI:
					struct _tr2__GetStreamUri tr2__GetStreamUri;
					struct _tr2__GetStreamUriResponse tr2__GetStreamUriResponse;
									
					tr2__GetStreamUri.Protocol = (char *)soap_malloc(soap, 128*sizeof(char));//
					if( NULL == tr2__GetStreamUri.Protocol )
					{
						printf("soap_malloc is error\n");
						ret = -1;
					}

					tr2__GetStreamUri.ProfileToken = (char *)soap_malloc(soap, 128*sizeof(char));//
					if (NULL == tr2__GetStreamUri.ProfileToken )
					{
						printf("soap_malloc is error\n");
						ret = -1;
					}							

					strcpy(tr2__GetStreamUri.Protocol, "tcp");
					info->ch_cnt = tr2__GetProfilesResponse.__sizeProfiles;
					printf("<<<<<sizeProfiles=%d>>>>>\n", info->ch_cnt);  
					for(ch=0; ch < info->ch_cnt; ch++)
					{
						char *pos1;
												  
						tr2__GetStreamUri.ProfileToken = tr2__GetProfilesResponse.Profiles[ch].token ;  
					
						soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 				  
					
						soap_call___tr2__GetStreamUri(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &tr2__GetStreamUri, &tr2__GetStreamUriResponse);
									  
						if (soap->error) 
						{  
							printf("tr2 soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
							result = -2;
							goto soaperror;
						}  
						else
						{  
							printf("chanel %d RTSP Addr Get Done is :%s \n",ch,tr2__GetStreamUriResponse.Uri); 
							// store main channel rtsp url:
							pos1 = strstr(tr2__GetStreamUriResponse.Uri, "rtsp://");
							if(pos1)
							{
								pos1 += strlen("rtsp://");
								sprintf(info->dat[ch].rtsp_url, "rtsp://%s:%s@%s", username, password, pos1);
							}
							else
							{
								printf("tr2__GetStreamUriResponse.Uri == ERR\n");									
								ret = -1;
							}
						}  

						if( tr2__GetProfilesResponse.Profiles[ch].Configurations != NULL )
						{
							if( tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder != NULL )
							{
								if( tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->Resolution != NULL )
								{
									info->dat[ch].height = tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->Resolution->Height;
									printf("===height is %d&====\n",info->dat[ch].height);								
									info->dat[ch].width = tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->Resolution->Width;																					
									printf("===width is %d&====\n",info->dat[ch].width); 
									
								}
								if( tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->RateControl != NULL )
								{
									info->dat[ch].bitrate = tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->RateControl->BitrateLimit;
									info->dat[ch].framerate = tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder->RateControl->FrameRateLimit;
									printf("===[ch=%d]bitrate[%d] framerate[%d]====\n",ch,info->dat[ch].bitrate, info->dat[ch].framerate);	
									info->dat[ch].vd_type = -1;
								}
							}
							else
							{
								printf("tr2__GetProfilesResponse.Profiles[ch].Configurations->VideoEncoder == NULL\n");									
								result = -1;
							}
						}
						else
						{
							printf("tr2__GetProfilesResponse.Profiles[channel].Configurations == NULL\n");									
							result = -1;
						}
					}
			soaperror:	
					if((result == -2) || (result == -1))
					{
						if(result == -2)
						{
							struct _trt__GetStreamUri trt__GetStreamUri;
							struct _trt__GetStreamUriResponse trt__GetStreamUriResponse;
							trt__GetStreamUri.StreamSetup = (struct tt__StreamSetup*)soap_malloc(soap,sizeof(struct tt__StreamSetup));//初始化，分配空间  
							trt__GetStreamUri.StreamSetup->Stream = 0;//stream type  
							trt__GetStreamUri.StreamSetup->Transport = (struct tt__Transport *)soap_malloc(soap, sizeof(struct tt__Transport));//初始化，分配空间	
							trt__GetStreamUri.StreamSetup->Transport->Protocol = 1;  
							trt__GetStreamUri.StreamSetup->Transport->Tunnel = 0;	
							trt__GetStreamUri.StreamSetup->__size = 1;	
							trt__GetStreamUri.StreamSetup->__any = NULL;  
							trt__GetStreamUri.StreamSetup->__anyAttribute =NULL; 

							for(ch=0; ch < info->ch_cnt; ch++)
							{
								char *pos1;
							  
								trt__GetStreamUri.ProfileToken = tr2__GetProfilesResponse.Profiles[ch].token; 
								soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 				  
								soap_call___trt__GetStreamUri(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &trt__GetStreamUri, &trt__GetStreamUriResponse);
								if (soap->error) 
								{  
									printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
									ret = -1;
								}  
								else
								{  
									printf("chanel %d RTSP Addr Get Done is :%s \n",ch,trt__GetStreamUriResponse.MediaUri->Uri); 
									// store main channel rtsp url:
									pos1 = strstr(trt__GetStreamUriResponse.MediaUri->Uri, "rtsp://");
									if(pos1)
									{
										pos1 += strlen("rtsp://");
										sprintf(info->dat[ch].rtsp_url, "rtsp://%s:%s@%s", username, password, pos1);
									}
								}  
							}
							soap_delete(soap,trt__GetStreamUri.StreamSetup);							
							soap_delete(soap,trt__GetStreamUri.StreamSetup->Transport);	

						}

						struct _trt__GetProfiles trt__GetProfiles;
						struct _trt__GetProfilesResponse trt__GetProfilesResponse;	
						soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 		
						
						if (soap_call___trt__GetProfiles(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &trt__GetProfiles, &trt__GetProfilesResponse)==-1)  
						{  
							printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
							ret = -1;
						}
						else
						{
							if( trt__GetProfilesResponse.Profiles == NULL )  
							{
								printf("get profiles response error...\n");
								ret = -1;
							}
							else
							{
								printf("<<<<<trt sizeProfiles=%d>>>>>\n", trt__GetProfilesResponse.__sizeProfiles);  
								for(ch=0; ch < trt__GetProfilesResponse.__sizeProfiles; ch++)
								{
									if(trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration !=NULL)
									{
										if( trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution != NULL )
										{
											info->dat[ch].height = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution->Height;
											printf("===height2 is %d====\n",info->dat[ch].height);								
											info->dat[ch].width = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution->Width;
											printf("===width2 is %d====\n",info->dat[ch].width);								
										}
										if( trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl != NULL )
										{
											info->dat[ch].bitrate = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl->BitrateLimit;
											info->dat[ch].framerate = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl->FrameRateLimit;
											info->dat[ch].vd_type = -1;
											//info->dat[ch].vd_type = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Encoding;
											printf("===[ch=%d]bitrate[%d] framerate[%d]====\n",ch, info->dat[ch].bitrate, info->dat[ch].framerate);								
										}
									}
									else
										printf("trt__GetProfilesResponse.Profiles[ch].Configurations == NULL\n"); 
								}
							}
						}

					}
					
					soap_delete(soap,tr2__GetStreamUri.Protocol);							
					soap_delete(soap,tr2__GetStreamUri.ProfileToken);							
				}
			}
		}
	}
 	return ret;
}
int get_rtsp_url_from_device_psw2( struct soap *soap, const char* device_addr, const char* username, const char* password, onvif_login_info_t* info)
{
	struct _tds__GetCapabilities capa_req;
	struct _tds__GetCapabilitiesResponse capa_resp;	
	int ret = 0;
	int ch;
	
	ret = 0;
    capa_req.Category = (enum tt__CapabilityCategory *)soap_malloc(soap, sizeof(int));  
    capa_req.__sizeCategory = 1;	
    *(capa_req.Category) = (enum tt__CapabilityCategory)(tt__CapabilityCategory__Media);    
    capa_resp.Capabilities = (struct tt__Capabilities*)soap_malloc(soap,sizeof(struct tt__Capabilities)) ;  

    soap_wsse_add_UsernameTokenDigest(soap,"user", username, password);    

    int result = soap_call___tds__GetCapabilities(soap, device_addr, NULL, &capa_req, &capa_resp);  
  
    if (soap->error)  
    {  
        printf("[%s][%d]--->>> soap error: %d, %s, %s\n", __func__, __LINE__, soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
        int retval = soap->error;  
		ret = -1;
    }  
    else  
    {  
        if( capa_resp.Capabilities==NULL )  
        {  
            printf(" GetCapabilities  failed!  result=%d \n",result);  
			ret = -1;						
        }  
        else  
        {

            printf(" Media->XAddr=%s \n", capa_resp.Capabilities->Media->XAddr);  
            printf(" Media->StreamingCapabilities->RTPMulticast=%d \n", capa_resp.Capabilities->Media->StreamingCapabilities->RTPMulticast?1:0);
            printf(" Media->StreamingCapabilities->RTP_USCORETCP=%d \n", capa_resp.Capabilities->Media->StreamingCapabilities->RTP_USCORETCP?1:0);

			if( capa_resp.Capabilities->Extension != NULL )
			{
				if( capa_resp.Capabilities->Extension->DeviceIO != NULL )
				{
		            printf(" Extension->DeviceIO->VideoSources=%d \n", capa_resp.Capabilities->Extension->DeviceIO->VideoSources?1:0);
		            printf(" Extension->DeviceIO->VideoOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->VideoOutputs?1:0);
		            printf(" Extension->DeviceIO->AudioSources=%d \n", capa_resp.Capabilities->Extension->DeviceIO->AudioSources?1:0);
		            printf(" Extension->DeviceIO->AudioOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->AudioOutputs?1:0);
		            printf(" Extension->DeviceIO->RelayOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->RelayOutputs?1:0);					
		            printf(" Extension->DeviceIO->__size=%d \n", capa_resp.Capabilities->Extension->DeviceIO->__size);
		            printf(" Extension->DeviceIO->__any=%s \n", capa_resp.Capabilities->Extension->DeviceIO->__any);
				}
	            printf(" Extension->Display=%s \n", capa_resp.Capabilities->Extension->Display);		
	            printf(" Extension->Recording=%s \n", capa_resp.Capabilities->Extension->Recording);
	            printf(" Extension->Search=%s \n", capa_resp.Capabilities->Extension->Search);
	            printf(" Extension->Replay=%s \n", capa_resp.Capabilities->Extension->Replay);
	            printf(" Extension->Receiver =%s \n", capa_resp.Capabilities->Extension->Receiver );
	            printf(" Extension->AnalyticsDevice =%s \n", capa_resp.Capabilities->Extension->AnalyticsDevice );
	            printf(" Extension->Extensions =%s \n", capa_resp.Capabilities->Extension->Extensions );
	            printf(" Extension->__size=%d \n", capa_resp.Capabilities->Extension->__size);
	            printf(" Extension->__any=%s \n", capa_resp.Capabilities->Extension->__any);
			}

			// 获取媒体信息Profile：
			struct _trt__GetProfiles trt__GetProfiles;
			struct _trt__GetProfilesResponse trt__GetProfilesResponse;	
			
			printf("\n-------------------[2]Getting Onvif Devices Profiles--------------\n");	

			soap_wsse_add_UsernameTokenDigest(soap,"user", username, password);			
		
			result = soap_call___trt__GetProfiles(soap, capa_resp.Capabilities->Media->XAddr, NULL, &trt__GetProfiles, &trt__GetProfilesResponse);
			if (result==-1)  
			{  
				printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
				result = soap->error;  
				ret = -1;
			}  
			else
			{  
				if( trt__GetProfilesResponse.Profiles == NULL )  
				{
					printf("get profiles response error...\n");
					ret = -1;
				}
				else
				{  
					// 获取RTSP的URI:
					struct _trt__GetStreamUri trt__GetStreamUri;
					struct _trt__GetStreamUriResponse trt__GetStreamUriResponse;
									
					//enum tt__StreamType { tt__StreamType__RTP_Unicast = 0, tt__StreamType__RTP_Multicast = 1 };
					trt__GetStreamUri.StreamSetup = (struct tt__StreamSetup*)soap_malloc(soap,sizeof(struct tt__StreamSetup));//初始化，分配空间  
					trt__GetStreamUri.StreamSetup->Stream = 0;//stream type  
				  
					//enum tt__TransportProtocol { tt__TransportProtocol__UDP = 0, tt__TransportProtocol__TCP = 1, tt__TransportProtocol__RTSP = 2, tt__TransportProtocol__HTTP = 3 };
					trt__GetStreamUri.StreamSetup->Transport = (struct tt__Transport *)soap_malloc(soap, sizeof(struct tt__Transport));//初始化，分配空间	
					trt__GetStreamUri.StreamSetup->Transport->Protocol = 0;  
					trt__GetStreamUri.StreamSetup->Transport->Tunnel = 0;	
					trt__GetStreamUri.StreamSetup->__size = 1;  
					trt__GetStreamUri.StreamSetup->__any = NULL;  
					trt__GetStreamUri.StreamSetup->__anyAttribute =NULL;  

					info->ch_cnt = trt__GetProfilesResponse.__sizeProfiles;
					printf("<<<<<sizeProfiles=%d>>>>>\n", info->ch_cnt);  
					for(ch=0; ch < info->ch_cnt; ch++)
					{
						char *pos1;
					  
						trt__GetStreamUri.ProfileToken = trt__GetProfilesResponse.Profiles[ch].token ;  
					
						soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 				  
					
						soap_call___trt__GetStreamUri(soap, capa_resp.Capabilities->Media->XAddr, NULL, &trt__GetStreamUri, &trt__GetStreamUriResponse);
									  
						if (soap->error) 
						{  
							printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
							result = soap->error;	 
							ret = -1;
						}  
						else
						{  
							printf("chanel %d RTSP Addr Get Done is :%s \n",ch,trt__GetStreamUriResponse.MediaUri->Uri); 
							// store main channel rtsp url:
							pos1 = strstr(trt__GetStreamUriResponse.MediaUri->Uri, "rtsp://");
							if(pos1)
							{
								pos1 += strlen("rtsp://");
								sprintf(info->dat[ch].rtsp_url, "rtsp://%s:%s@%s", username, password, pos1);
							}
							else
							{
								ret = -1;
							}
						}  
						if(trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration !=NULL)
						{
							if( trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution != NULL )
							{
								info->dat[ch].height = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution->Height;
								printf("===height2 is %d====\n",info->dat[ch].height);								
								info->dat[ch].width = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Resolution->Width;
								printf("===width2 is %d====\n",info->dat[ch].width);								
							}
							if( trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl != NULL )
							{
								info->dat[ch].bitrate = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl->BitrateLimit;
								info->dat[ch].framerate = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->RateControl->FrameRateLimit;
								info->dat[ch].vd_type = -1;
								//info->dat[ch].vd_type = trt__GetProfilesResponse.Profiles[ch].VideoEncoderConfiguration->Encoding;
								printf("===[ch=%d]bitrate[%d] framerate[%d]====\n",ch, info->dat[ch].bitrate, info->dat[ch].framerate);								
							}
						}
						else
							printf("trt__GetProfilesResponse.Profiles[ch].Configurations == NULL\n"); 								
					}
					if(!info->ch_cnt)
						ret = -1;
					
					soap_delete(soap,trt__GetStreamUri.StreamSetup);							
					soap_delete(soap,trt__GetStreamUri.StreamSetup->Transport);															
				}  				
			}  
		}  
	}  	
	soap_delete(soap,capa_req.Category);							
	soap_delete(soap,capa_resp.Capabilities);	
 	return ret;
}


//

void GetRandMessageId(char *pReturnChar)
{
	unsigned char macaddr[6];  
	unsigned int Flagrand;  	
	srand((int)time(0));  
	Flagrand = rand()%9000 + 8888;	
	macaddr[0] = 0x1; macaddr[1] = 0x2; macaddr[2] = 0x3; macaddr[3] = 0x4; macaddr[4] = 0x5; macaddr[5] = 0x6;	
	sprintf(pReturnChar,"urn:uuid:%ud68a-1dd2-11b2-a105-%02X%02X%02X%02X%02X%02X",   
			Flagrand, macaddr[0], macaddr[1], macaddr[2], macaddr[3], macaddr[4], macaddr[5]);  

}

int GetDeviceName(const char * Profile, char *name)
{
	char *pos1, *pos2;
	char temp[3] = {0};
	int len = 0;
	int i, j;
	
	if(Profile == NULL)
	{
		return -1;
	}
	
	pos1 = strstr(Profile, "onvif://www.onvif.org/name/");
	if(pos1 != NULL)
	{
		pos1 = pos1+strlen("onvif://www.onvif.org/name/");
		pos2 = strchr(pos1, ' ');
		if(pos2 != NULL)
		{
			len = strlen(pos1)-strlen(pos2);
		}
		else
		{
			len = strlen(pos1);
		}
		
		for(i = 0, j =0; i < len; i++)
		{
			if(pos1[i] != '%')
			{
				name[j++] = pos1[i];
			}
			else
			{
				temp[0] = pos1[++i];
				temp[1] = pos1[++i];
				name[j++] = strtol(temp,NULL,16);
			}
		}
		name[j] = 0;
		return 0;
	}

	return -1;
}

int one_ipc_Login(const char* device_url, const char* username, const char* password, onvif_login_info_t* info)
{
	char deviceUrl[100] = {0};
	int result = -1;  
	wsdd__ProbeType req;
	struct __wsdd__ProbeMatches resp;
	wsdd__ScopesType sScope;
	struct SOAP_ENV__Header header;  
	struct soap *soap;

	soap = soap_new();  
	if(NULL == soap )  
	{  
		printf("sopa new error\r\n");  
		return result;  
	}  

	soap->recv_timeout = 1;  
	soap_set_namespaces(soap, namespaces);  
	soap_default_SOAP_ENV__Header(soap, &header);  

	//uuid_t uuid;
	char guid_string[100]={0};
	
	GetRandMessageId(guid_string);

	header.wsa__MessageID = guid_string; 
	header.wsa__To = "urn:schemas-xmlsoap-org:ws:2005:04:discovery";  
	header.wsa__Action = "http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe";  
	
	soap->header = &header;

	snprintf(deviceUrl, 100, "http://%s/onvif/device_service", device_url);
	//printf("[onvif_get_rtsp_rul_by_deviceUrl by 2!!!]\n");
	//result = get_rtsp_url_from_device_psw2( soap, deviceUrl, username, password, info);
	//if(result == -1)
	{
		printf("[onvif_get_rtsp_rul_by_deviceUrl by 1!!!]\n");
		result = get_rtsp_url_from_device_psw1( soap, deviceUrl, username, password, info);
		if(result == -1)
		{
			printf("[onvif_get_rtsp_rul_by_deviceUrl by 2!!!]\n");
			result = get_rtsp_url_from_device_psw2( soap, deviceUrl, username, password, info);
		}
	}

	soap_destroy(soap); 
	soap_end(soap); 
	soap_free(soap);
	
	return result; 
	
}


int onvif_device_discovery(void)
{
	int result = 0;  
	wsdd__ProbeType req;
	struct __wsdd__ProbeMatches resp;
	wsdd__ScopesType sScope;
	struct SOAP_ENV__Header header;  
	struct soap *soap;

	soap = soap_new();  
	if(NULL == soap )  
	{  
		printf("sopa new error\r\n");  
		return -1;  
	}  

	soap->recv_timeout = 2;  
	soap_set_namespaces(soap, namespaces);  
	soap_default_SOAP_ENV__Header(soap, &header);  

	//uuid_t uuid;
	char guid_string[100]={0};

	GetRandMessageId(guid_string);

	// uuid:39ade894-2df3-50b2-a205-00001b1cfe39
	header.wsa__MessageID =guid_string;  
	//header.wsa__MessageID = LONGSE_MSG_ID;
	
	
	header.wsa__To = "urn:schemas-xmlsoap-org:ws:2005:04:discovery";  
	header.wsa__Action = "http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe";  
	
	soap->header = &header;  
	
	// #define SOAP_NAMESPACE_OF_wsdd	"http://schemas.xmlsoap.org/ws/2005/04/discovery"	
	soap_default_wsdd__ScopesType(soap, &sScope);  
	sScope.__item = "";  // http://schemas.xmlsoap.org/ws/2005/04/discovery
	soap_default_wsdd__ProbeType(soap, &req);  
	req.Scopes = &sScope;  
	//req.Types = ""; //"dn:NetworkVideoTransmitter";
	req.Types = "tdn:NetworkVideoTransmitter";

	int i = 0;  	
	// 组播命令探测onvif设备
    result = soap_send___wsdd__Probe(soap, "soap.udp://239.255.255.250:3702", NULL, &req);  

	//printf("[%d] soap_send___wsdd__Probe result = %d\n", __LINE__,result);

	int ret = 0;
	//char device_addr[250];
	//char rtsp_m_url[250];
	//char rtsp_s_url[250];
	//onvif_dis_dat_t onvif_dat_temp;

	ipc_discovery_device_init();	
	
    while(result == SOAP_OK)  
    {  
		//soap_wsse_add_UsernameTokenDigest(soap,"user", username, password);    
    
		result = soap_recv___wsdd__ProbeMatches(soap, &resp);  
		if(result == SOAP_OK)  
		{  
			if(soap->error)  
			{  
				printf("soap error 1: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
				result = soap->error;  
				ret = -1;
			}  
			else  
			{  
				if( resp.wsdd__ProbeMatches == NULL )
				{
					ret = -1;
				}
				else
				{

					char *pos1, *pos2;
					char deviceName[100] = {0};
					char deviceIp[40] = {0};
					char deviceUrl[100] = {0};
					int len;

					if( resp.wsdd__ProbeMatches->ProbeMatch == NULL )
					{
						printf("wsdd__ProbeMatches->ProbeMatch just null\n"); 
						ret = -1;
					}
					else
					{
						if( resp.wsdd__ProbeMatches->ProbeMatch->Scopes == NULL )
						{
							printf("resp.wsdd__ProbeMatches->ProbeMatch->Scopes just null\n"); 
							ret = -1;
						}
						else
						{
							if( resp.wsdd__ProbeMatches->ProbeMatch->Scopes->__item == NULL )
							{
								printf("resp.wsdd__ProbeMatches->ProbeMatch->Scopes->__item just null\n"); 
								ret = -1;
							}
							else
							{
								GetDeviceName(resp.wsdd__ProbeMatches->ProbeMatch->Scopes->__item, deviceName);
								
								printf("resp.wsdd__ProbeMatches->ProbeMatch->XAddrs=[%s]\n",resp.wsdd__ProbeMatches->ProbeMatch->XAddrs); 

								for(pos1 = resp.wsdd__ProbeMatches->ProbeMatch->XAddrs, pos1 = strstr(pos1, "http://"); pos1 != NULL; pos1 = strstr(pos1, "http://"))
								{
									pos1 = pos1+strlen("http://");
									pos2 = strstr(pos1, "/onvif/device_service");
									len = strlen(pos1) - strlen(pos2);
									memcpy(deviceIp, pos1, len);
									break;
									// check URL port data
									#if 0
									pos1 = deviceIp;
 									pos2 = strstr(pos1, ":");
									if( pos2 != NULL )
									{
										// get ip str;
										pos2[0] = 0;
									}
									#endif
								}
								printf("[%d] deviceIp: %s, deviceName: %s\n", __LINE__,deviceIp, deviceName); 

								//snprintf(deviceUrl, 100, "http://%s/onvif/device_service", deviceIp);

								ipc_discovery_add_addr(deviceIp,deviceName);
							}
						}
					}
				}
			}  
		}  
		else if (soap->error)  
		{  
			printf("[%d] soap error 2: %d, %s, %s\n", __LINE__, soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
			result = soap->error; 
			ret	= -1;
		}  
    } 

	soap_destroy(soap); 
	soap_end(soap); 
	soap_free(soap);
	
	return result; 
	
}


int GetNameByIp(const char* pIP, char* devName)
{
	int result = 0;  
	wsdd__ProbeType req;
	struct __wsdd__ProbeMatches resp;
	wsdd__ScopesType sScope;
	struct SOAP_ENV__Header header;  
	struct soap *soap;
	int ip[4] = {-1};
	int i;
	char guid_string[100]={0};
	
	sscanf(pIP, "%d.%d.%d.%d",&ip[0], &ip[1], &ip[2], &ip[3]);

	for(i = 0; i < 4; i++)
	{
		if(ip[i]>=0 && ip[i] <= 255)
		{
			continue;
		}
		else
		{
			return -1;
		}
	}
	
	soap = soap_new();  
	if(NULL == soap )  
	{  
		printf("sopa new error\r\n");  
		return -1;  
	}  

	soap->recv_timeout = 1;  
	soap_set_namespaces(soap, namespaces);  
	soap_default_SOAP_ENV__Header(soap, &header);  

	GetRandMessageId(guid_string);

	header.wsa__MessageID =guid_string;  
	
	
	header.wsa__To = "urn:schemas-xmlsoap-org:ws:2005:04:discovery";  
	header.wsa__Action = "http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe";  
	
	soap->header = &header;  
	
	// #define SOAP_NAMESPACE_OF_wsdd	"http://schemas.xmlsoap.org/ws/2005/04/discovery"	
	soap_default_wsdd__ScopesType(soap, &sScope);  
	sScope.__item = "";  // http://schemas.xmlsoap.org/ws/2005/04/discovery
	soap_default_wsdd__ProbeType(soap, &req);  
	req.Scopes = &sScope;  
	//req.Types = ""; //"dn:NetworkVideoTransmitter";
	req.Types = "tdn:NetworkVideoTransmitter";
	

	char discoveryUrl[100];
	char deviceIp[40] = {0};
	char *pos1, *pos2;
	strcpy(deviceIp, pIP);
	pos1 = deviceIp;
	pos2 = strstr(pos1, ":");
	if( pos2 != NULL )
	{
		// get ip str;
		pos2[0] = 0;
	}
	snprintf(discoveryUrl, 100, "soap.udp://%s:3702", deviceIp);
    result = soap_send___wsdd__Probe(soap, discoveryUrl, NULL, &req);  

	printf("soap_send___wsdd__Probe ip = %s, result = %d\n",deviceIp, result );

    if(result == SOAP_OK)  
    {  
		result = soap_recv___wsdd__ProbeMatches(soap, &resp);  
		if(result == SOAP_OK)  
		{  
			if(soap->error)  
			{  
				printf("soap error 1: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
				result = soap->error;  
			}  
			else  
			{  
				result = GetDeviceName(resp.wsdd__ProbeMatches->ProbeMatch->Scopes->__item, devName);
				printf("deviceName: %s\n", devName);
			}  
		} 
		else
		{
			printf("soap_recv___wsdd__ProbeMatches result = %d\n", result );		
		}
    } 

	soap_destroy(soap); 
	soap_end(soap); 
	soap_free(soap);
	
	return result; 
	
}



#if 0

int get_rtsp_url_from_device_addr_with_psw1( struct soap *soap, const char* device_addr, const char* username, const char* password, const int channel, onvif_login_info_t* info)
{
	struct _tds__GetCapabilities capa_req;
	struct _tds__GetCapabilitiesResponse capa_resp; 
	int ret = 0;

	// GetServices获取多个媒体地址
	int i;
	int result;

	soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 		

	struct _tds__GetServices tds__GetServices;
	struct _tds__GetServicesResponse tds__GetServicesResponse;	   
	tds__GetServices.IncludeCapability = 0;
	soap_call___tds__GetServices(soap,device_addr,NULL, &tds__GetServices, &tds__GetServicesResponse);	
	if(soap->error)
	{ 
		printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));
		ret = -1;
	}
	else
	{
		for(i=0; i<tds__GetServicesResponse.__sizeService; i++)
		{
			if (tds__GetServicesResponse.Service[i].Namespace != NULL )
			{
				if( (strcmp(tds__GetServicesResponse.Service[i].Namespace, "http://www.onvif.org/ver10/media/wsdl") == 0) || 
					(strcmp(tds__GetServicesResponse.Service[i].Namespace, "http://www.onvif.org/ver20/media/wsdl") == 0) ) 
				{
					printf("  media_addr[%d] %s\n", i, tds__GetServicesResponse.Service[i].XAddr);

					// 获取媒体信息Profile：
					struct _tr2__GetProfiles tr2__GetProfiles;
					struct _tr2__GetProfilesResponse tr2__GetProfilesResponse;	
					
					printf("\n-------------------[1]Getting Onvif Devices Profiles--------------\n");	
					
					soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 		

					tr2__GetProfiles.__sizeType = 1;
					tr2__GetProfiles.Token = NULL;	
					tr2__GetProfiles.Type = NULL;					
					result = soap_call___tr2__GetProfiles(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &tr2__GetProfiles, &tr2__GetProfilesResponse);
					if (result==-1)  
					{  
						printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
						result = soap->error;  
						ret = -1;
					}  
					else
					{  
						printf("get profiles response sizeProfiles=%d\n",tr2__GetProfilesResponse.__sizeProfiles);
						
						if( tr2__GetProfilesResponse.Profiles == NULL )  
						{
							printf("get profiles response error...\n");
							ret = -1;
						}
						else
						{  
							int k;
							for( k = 0; k< tr2__GetProfilesResponse.__sizeProfiles; k++ )
							{
								printf( "Profiles Name:%s  \n",tr2__GetProfilesResponse.Profiles[k].Name); 
								printf( "Profiles Taken:%s\n",tr2__GetProfilesResponse.Profiles[k].token);
							}						
							// 获取RTSP的URI:
							struct _tr2__GetStreamUri tr2__GetStreamUri;
							struct _tr2__GetStreamUriResponse tr2__GetStreamUriResponse;
											
							tr2__GetStreamUri.Protocol = (char *)soap_malloc(soap, 128*sizeof(char));//
							if( NULL == tr2__GetStreamUri.Protocol )
							{
								printf("soap_malloc is error\n");
								ret = -1;
							}

							tr2__GetStreamUri.ProfileToken = (char *)soap_malloc(soap, 128*sizeof(char));//
							if (NULL == tr2__GetStreamUri.ProfileToken )
							{
								printf("soap_malloc is error\n");
								ret = -1;
							}							

							strcpy(tr2__GetStreamUri.Protocol, "tcp");
							//strcpy(tr2__GetStreamUri.ProfileToken, tr2__GetProfilesResponse.Profiles[channel].token);
							
							printf("<<<<<channel = %d, sizeProfiles=%d>>>>>\n", channel,tr2__GetProfilesResponse.__sizeProfiles);  
							
							if( channel < tr2__GetProfilesResponse.__sizeProfiles )
							{
								char *pos1;
							  
								tr2__GetStreamUri.ProfileToken = tr2__GetProfilesResponse.Profiles[channel].token ;  
							
								soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 				  
							
								soap_call___tr2__GetStreamUri(soap, tds__GetServicesResponse.Service[i].XAddr, NULL, &tr2__GetStreamUri, &tr2__GetStreamUriResponse);
											  
								if (soap->error) 
								{  
									printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
									result = soap->error;	 
									ret = -1;
								}  
								else
								{  
									printf("chanel %d RTSP Addr Get Done is :%s \n",channel,tr2__GetStreamUriResponse.Uri); 
									// store main channel rtsp url:
									pos1 = strstr(tr2__GetStreamUriResponse.Uri, "rtsp://");
									if(pos1)
									{
										pos1 += strlen("rtsp://");
										sprintf(info->rtsp_url, "rtsp://%s:%s@%s", username, password, pos1);
									}
									else
									{
										ret = -1;
									}
								}  

								if( tr2__GetProfilesResponse.Profiles[channel].Configurations != NULL )
								{
									printf("tr2__GetProfilesResponse.Profiles[channel].Configurations != NULL\n");
									if( tr2__GetProfilesResponse.Profiles[channel].Configurations->VideoEncoder != NULL )
									{
										printf("tr2__GetProfilesResponse.Profiles[channel].Configurations != NULL1\n");
										if( info->height = tr2__GetProfilesResponse.Profiles[channel].Configurations->VideoEncoder->Resolution )
										{
											printf("tr2__GetProfilesResponse.Profiles[channel].Configurations != NULL2\n");
											info->height = tr2__GetProfilesResponse.Profiles[channel].Configurations->VideoEncoder->Resolution->Height;
											printf("===height is %d&====\n",info->height);								
											info->width = tr2__GetProfilesResponse.Profiles[channel].Configurations->VideoEncoder->Resolution->Width;																					
											printf("===width is %d&====\n",info->width); 
											
											info->channel = channel;
											strcpy(info->device_url, device_addr);
										}
										//else
										//	ret = -1;
									}
									//else
									//	ret = -1;
								}
								else
								{
									printf("tr2__GetProfilesResponse.Profiles[channel].Configurations == NULL\n");									
									ret = -1;
								}									
							}
							else
							{
								ret = -1;
							}
							soap_delete(soap,tr2__GetStreamUri.Protocol);							
							soap_delete(soap,tr2__GetStreamUri.ProfileToken);							
						}
					}
				}
			}
		}
	}
 	return ret;
}
int get_rtsp_url_from_device_addr_with_psw2( struct soap *soap, const char* device_addr, const char* username, const char* password, const int channel, onvif_login_info_t* info)
{
	struct _tds__GetCapabilities capa_req;
	struct _tds__GetCapabilitiesResponse capa_resp;	
	int ret = 0;

	ret = 0;
    capa_req.Category = (enum tt__CapabilityCategory *)soap_malloc(soap, sizeof(int));  
    capa_req.__sizeCategory = 1;	
    *(capa_req.Category) = (enum tt__CapabilityCategory)(tt__CapabilityCategory__Media);    
    capa_resp.Capabilities = (struct tt__Capabilities*)soap_malloc(soap,sizeof(struct tt__Capabilities)) ;  

    soap_wsse_add_UsernameTokenDigest(soap,"user", username, password);    
	strcpy(info->user, username);
	strcpy(info->psw, password);

    int result = soap_call___tds__GetCapabilities(soap, device_addr, NULL, &capa_req, &capa_resp);  
  
    if (soap->error)  
    {  
        printf("[%s][%d]--->>> soap error: %d, %s, %s\n", __func__, __LINE__, soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
        int retval = soap->error;  
		ret = -1;
    }  
    else  
    {  
        if( capa_resp.Capabilities==NULL )  
        {  
            printf(" GetCapabilities  failed!  result=%d \n",result);  
			ret = -1;						
        }  
        else  
        {

            printf(" Media->XAddr=%s \n", capa_resp.Capabilities->Media->XAddr);  
            printf(" Media->StreamingCapabilities->RTPMulticast=%d \n", capa_resp.Capabilities->Media->StreamingCapabilities->RTPMulticast?1:0);
            printf(" Media->StreamingCapabilities->RTP_USCORETCP=%d \n", capa_resp.Capabilities->Media->StreamingCapabilities->RTP_USCORETCP?1:0);

			if( capa_resp.Capabilities->Extension != NULL )
			{
				if( capa_resp.Capabilities->Extension->DeviceIO != NULL )
				{
		            printf(" Extension->DeviceIO->VideoSources=%d \n", capa_resp.Capabilities->Extension->DeviceIO->VideoSources?1:0);
		            printf(" Extension->DeviceIO->VideoOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->VideoOutputs?1:0);
		            printf(" Extension->DeviceIO->AudioSources=%d \n", capa_resp.Capabilities->Extension->DeviceIO->AudioSources?1:0);
		            printf(" Extension->DeviceIO->AudioOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->AudioOutputs?1:0);
		            printf(" Extension->DeviceIO->RelayOutputs=%d \n", capa_resp.Capabilities->Extension->DeviceIO->RelayOutputs?1:0);					
		            printf(" Extension->DeviceIO->__size=%d \n", capa_resp.Capabilities->Extension->DeviceIO->__size);
		            printf(" Extension->DeviceIO->__any=%s \n", capa_resp.Capabilities->Extension->DeviceIO->__any);
				}
	            printf(" Extension->Display=%s \n", capa_resp.Capabilities->Extension->Display);		
	            printf(" Extension->Recording=%s \n", capa_resp.Capabilities->Extension->Recording);
	            printf(" Extension->Search=%s \n", capa_resp.Capabilities->Extension->Search);
	            printf(" Extension->Replay=%s \n", capa_resp.Capabilities->Extension->Replay);
	            printf(" Extension->Receiver =%s \n", capa_resp.Capabilities->Extension->Receiver );
	            printf(" Extension->AnalyticsDevice =%s \n", capa_resp.Capabilities->Extension->AnalyticsDevice );
	            printf(" Extension->Extensions =%s \n", capa_resp.Capabilities->Extension->Extensions );
	            printf(" Extension->__size=%d \n", capa_resp.Capabilities->Extension->__size);
	            printf(" Extension->__any=%s \n", capa_resp.Capabilities->Extension->__any);
			}

			// 获取媒体信息Profile：
			struct _trt__GetProfiles trt__GetProfiles;
			struct _trt__GetProfilesResponse trt__GetProfilesResponse;	
			
			printf("\n-------------------[2]Getting Onvif Devices Profiles--------------\n");	

			soap_wsse_add_UsernameTokenDigest(soap,"user", username, password);			
		
			result = soap_call___trt__GetProfiles(soap, capa_resp.Capabilities->Media->XAddr, NULL, &trt__GetProfiles, &trt__GetProfilesResponse);
			if (result==-1)  
			{  
				printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
				result = soap->error;  
				ret = -1;
			}  
			else
			{  
				if( trt__GetProfilesResponse.Profiles == NULL )  
				{
					printf("get profiles response error...\n");
					ret = -1;
				}
				else
				{  
					// 获取RTSP的URI:
					struct _trt__GetStreamUri trt__GetStreamUri;
					struct _trt__GetStreamUriResponse trt__GetStreamUriResponse;
									
					//enum tt__StreamType { tt__StreamType__RTP_Unicast = 0, tt__StreamType__RTP_Multicast = 1 };
					trt__GetStreamUri.StreamSetup = (struct tt__StreamSetup*)soap_malloc(soap,sizeof(struct tt__StreamSetup));//初始化，分配空间  
					trt__GetStreamUri.StreamSetup->Stream = 0;//stream type  
				  
					//enum tt__TransportProtocol { tt__TransportProtocol__UDP = 0, tt__TransportProtocol__TCP = 1, tt__TransportProtocol__RTSP = 2, tt__TransportProtocol__HTTP = 3 };
					trt__GetStreamUri.StreamSetup->Transport = (struct tt__Transport *)soap_malloc(soap, sizeof(struct tt__Transport));//初始化，分配空间	
					trt__GetStreamUri.StreamSetup->Transport->Protocol = 0;  
					trt__GetStreamUri.StreamSetup->Transport->Tunnel = 0;	
					trt__GetStreamUri.StreamSetup->__size = 1;  
					trt__GetStreamUri.StreamSetup->__any = NULL;  
					trt__GetStreamUri.StreamSetup->__anyAttribute =NULL;  

					printf("<<<<<channel = %d, sizeProfiles=%d>>>>>\n", channel,trt__GetProfilesResponse.__sizeProfiles);  
				
					if( channel < trt__GetProfilesResponse.__sizeProfiles )
					{
						char *pos1;
					  
						trt__GetStreamUri.ProfileToken = trt__GetProfilesResponse.Profiles[channel].token ;  
					
						soap_wsse_add_UsernameTokenDigest(soap,"user", username, password); 				  
					
						soap_call___trt__GetStreamUri(soap, capa_resp.Capabilities->Media->XAddr, NULL, &trt__GetStreamUri, &trt__GetStreamUriResponse);
									  
						if (soap->error) 
						{  
							printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));  
							result = soap->error;	 
							ret = -1;
						}  
						else
						{  
							printf("chanel %d RTSP Addr Get Done is :%s \n",channel,trt__GetStreamUriResponse.MediaUri->Uri); 
							// store main channel rtsp url:
							pos1 = strstr(trt__GetStreamUriResponse.MediaUri->Uri, "rtsp://");
							if(pos1)
							{
								pos1 += strlen("rtsp://");
								sprintf(info->rtsp_url, "rtsp://%s:%s@%s", username, password, pos1);
							}
							else
							{
								ret = -1;
							}
						}  
						
						printf("===chanel %d rtsp_url is %s====\n",channel,info->rtsp_url);								
						info->height = trt__GetProfilesResponse.Profiles[channel].VideoEncoderConfiguration->Resolution->Height;
						printf("===height2 is %d====\n",info->height);								
						info->width = trt__GetProfilesResponse.Profiles[channel].VideoEncoderConfiguration->Resolution->Width;
						printf("===width2 is %d====\n",info->width);								

						info->channel = channel;
						strcpy(info->device_url, device_addr);

						
					}
					else
						ret = -1;
					
					soap_delete(soap,trt__GetStreamUri.StreamSetup);							
					soap_delete(soap,trt__GetStreamUri.StreamSetup->Transport);															
				}  				
			}  
		}  
	}  	
	soap_delete(soap,capa_req.Category);							
	soap_delete(soap,capa_resp.Capabilities);	
 	return ret;
}

int onvif_get_rtsp_rul_by_deviceUrl(const char* device_url, const char* username, const char* password, const int channel, onvif_login_info_t* info)
{
	int result = -1;  
	wsdd__ProbeType req;
	struct __wsdd__ProbeMatches resp;
	wsdd__ScopesType sScope;
	struct SOAP_ENV__Header header;  
	struct soap *soap;

	soap = soap_new();  
	if(NULL == soap )  
	{  
		printf("sopa new error\r\n");  
		return result;  
	}  

	soap->recv_timeout = 1;  
	soap_set_namespaces(soap, namespaces);  
	soap_default_SOAP_ENV__Header(soap, &header);  

	//uuid_t uuid;
	char guid_string[100]={0};
	GetRandMessageId(guid_string);

	header.wsa__MessageID = guid_string; 
	header.wsa__To = "urn:schemas-xmlsoap-org:ws:2005:04:discovery";  
	header.wsa__Action = "http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe";  

	
	soap->header = &header;

	printf("\n[onvif_get_rtsp_rul_by_deviceUrl]\n");
	//result = get_rtsp_url_from_device_addr_with_psw( soap, device_url, username, password, channel, info);
	result = get_rtsp_url_from_device_addr_with_psw2( soap, device_url, username, password, channel, info);
	if(result == -1)
	{
		result = get_rtsp_url_from_device_addr_with_psw1( soap, device_url, username, password, channel, info);
		if(result == -1)
			result = get_rtsp_url_from_device_addr_with_psw2( soap, device_url, username, password, channel, info);
	}
	soap_destroy(soap); 
	soap_end(soap); 
	soap_free(soap);
	
	return result; 
	
}
#endif



