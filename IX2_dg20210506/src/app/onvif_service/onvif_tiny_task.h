
/*
lvzhihui - 20180316
*/

#ifndef _ONVIF_TINY_TASK_H_
#define _ONVIF_TINY_TASK_H_

#include <signal.h>
#include <pthread.h>

#define USE_THREAD_DETACHED_MODE

typedef  int (*callback_init)(void* arg);	
typedef  void (*callback_proc)(void* arg);
typedef  void (*callback_exit)(void* arg);

typedef struct
{
	int					flag;			// 运行标志
	
	pthread_mutex_t 	lock;			// 任务锁
	pthread_t 			pid;			// 任务id
	pthread_attr_t 		pattr;			// 任务属性

	callback_init 		cb_init;		// 任务启动前的初始化
	callback_proc		cb_proc;		// 任务运行的服务处理
	callback_exit 		cb_exit;		// 任务结束后的处理

	void*				pusr_dat;		// 用户数据指针
	
} one_tiny_task_t;

// init one tiny task and run
int one_tiny_task_start( one_tiny_task_t *pins, callback_init cb_init, callback_proc cb_proc, callback_exit cb_exit, void* pusr_dat );

// check one tiny task just running
int one_tiny_task_is_running( one_tiny_task_t *pins );

// stop one tiny task
int one_tiny_task_stop( one_tiny_task_t *pins );

#endif


