/**
  ******************************************************************************
  * @file    task_ListUpdate.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef TASK_ListUpdate_H
#define TASK_ListUpdate_H

#include "task_survey.h"		

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/

#define MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE  		0
#define MSG_TYPE_UPDATE_NAMELIST_TABLE  			1
#define MSG_TYPE_UPDATE_MSLIST_TABLE  				2

void vtk_TaskInit_listUpdate(int priority);



#endif
