/**
  ******************************************************************************
  * @file    task_ListUpdate.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ListUpdate
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "task_ListUpdate.h"
#include "task_VideoMenu.h"


Loop_vdp_common_buffer	vdp_listUpdate_mesg_queue;
Loop_vdp_common_buffer	vdp_listUpdate_sync_queue;
vdp_task_t				task_listUpdate;

void vdp_listUpdate_mesg_data_process(char* msg_data, int len);
void* vdp_listUpdate_task( void* arg );


void vtk_TaskInit_listUpdate(int priority)
{
	init_vdp_common_queue(&vdp_listUpdate_mesg_queue, 500, (msg_process)vdp_listUpdate_mesg_data_process, &task_listUpdate);
	init_vdp_common_queue(&vdp_listUpdate_sync_queue, 100, NULL, &task_listUpdate);
	init_vdp_common_task(&task_listUpdate, MSG_ID_ListUpdate, vdp_listUpdate_task, &vdp_listUpdate_mesg_queue, &vdp_listUpdate_sync_queue);
	
}

void exit_vdp_listUpdate_task(void)
{
	exit_vdp_common_queue(&vdp_listUpdate_mesg_queue);
	exit_vdp_common_queue(&vdp_listUpdate_sync_queue);
	exit_vdp_common_task(&task_listUpdate);	
}

void* vdp_listUpdate_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_listUpdate_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pMsgListUpdate = (VDP_MSG_HEAD*)msg_data;
	#if defined(PID_IX850)
	#else
	BusySpriteDisplay(1);
	#endif

	switch (pMsgListUpdate->msg_type)
	{
		case MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE:
			//if(UpdateVideoResourceTable())
			if(UpdateOnlineMonListTable())
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorListUpdate);
			}
			break;

		case MSG_TYPE_UPDATE_NAMELIST_TABLE:
			if(UpdateImNameListTable())
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_NameListUpdate);
			}
			break;
		case MSG_TYPE_UPDATE_MSLIST_TABLE:
			if(UpdateMsListTable())
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MSListUpdate);
			}
			break;			
			
		default:
			break;
	}
	#if defined(PID_IX850)
	#else
	BusySpriteDisplay(0);
	#endif
}

void API_ListUpdate(int msgType)
{
	VDP_MSG_HEAD msg;

	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_ListUpdate;
	msg.msg_type 		= msgType;
	msg.msg_sub_type 	= 0;
	
	// 压入本地队列
	push_vdp_common_queue(&vdp_listUpdate_mesg_queue, (char*)&msg, sizeof(msg));
}


