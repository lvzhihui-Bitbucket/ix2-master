#include "MENU_public.h"

unsigned char year[5] = {0};
unsigned char mon[3] = {0};
unsigned char day[3] = {0};
unsigned char hour[3] = {0};
unsigned char min[3] = {0};
unsigned char second[3] = {0};
unsigned char inputSet[9] = {0};
int	ReadDateTime(uint8 dateOrTime)	
{
//#define RTC_TIME

	unsigned char h;
	unsigned char mi;
	unsigned char s;
	unsigned char y;
	unsigned char mo;
	unsigned char d;

	// Get the current date time
	if(dateOrTime)
	{
#if 0
		RTC_DateTypeDef date;
		date	= RtcGetDate();
		y 	= BcdToBin(date.RTC_Year);
		mo 	= BcdToBin(date.RTC_Month);
		d 	= BcdToBin(date.RTC_Date);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);

		//printf("get localtime %d-%d-%d \n", tblock->tm_year, tblock->tm_mon, tblock->tm_mday);
		
		y = ((tblock->tm_year > 100) ? tblock->tm_year -100 : 0);
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init year
		snprintf(year,5,"20%02d",y);	
		// init month
		snprintf(mon,3,"%02d",mo);	
		// init day
		snprintf(day,3,"%02d",d);	
	}
	else
	{
#if 0
		RTC_TimeTypeDef time;
		time	= RtcGetTime();
		h	= BcdToBin(time.RTC_Hours);
		mi	= BcdToBin(time.RTC_Minutes);
		s	= BcdToBin(time.RTC_Seconds);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);
		
		y = tblock->tm_year-100;
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init hour
		snprintf(hour,3,"%02d",h);	
		// init minute
		snprintf(min,3,"%02d",mi);	
		// init minute
		snprintf(second,3,"%02d",s);	
	}
}


pthread_mutex_t 	receiveLocalTimeLock = PTHREAD_MUTEX_INITIALIZER;
time_t receiveLocalTime;

time_t GetLocalTimeFromStm8l(void)
{
	int i;
	time_t ret = -1;
	
	receiveLocalTime = -1;
	api_uart_send_pack(UART_TYPE_LOCAL_TIME_GET, NULL, 0);
	
	for(i = 10; i > 0; i--)
	{
		usleep(100*1000);
		pthread_mutex_lock(&receiveLocalTimeLock);
		ret = receiveLocalTime;
		pthread_mutex_unlock(&receiveLocalTimeLock);
		if(ret >= 0)
		{
			break;
		}
	}
	printf("receiveLocalTime = %d, i = %d\n", ret, i);
	return ret;	
}

void ReceiveLocalTimeFromStm8l(unsigned char* buff)
{
	pthread_mutex_lock(&receiveLocalTimeLock);
	receiveLocalTime = buff[3]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[2]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[1]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[0];
	pthread_mutex_unlock(&receiveLocalTimeLock);
	printf("receiveLocalTime = %d, receiveLocalTime = %d\n", receiveLocalTime, receiveLocalTime);
}


void UpdateLocalTimeToStm8l(void)
{
#if 0
	unsigned char buff[4];

	time_t t;
	t = time(NULL); 

	buff[0] = t&0xFF;
	buff[1] = (t>>8)&0xFF;
	buff[2] = (t>>16)&0xFF;
	buff[3] = (t>>24)&0xFF;

	printf("UpdateLocalTimeToStm8l buff[0] = 0x%x, buff[1] = 0x%x, buff[2] = 0x%x,buff[3] = 0x%x\n", buff[0], buff[1],buff[2],buff[3]);
	
	api_uart_send_pack(UART_TYPE_LOCAL_TIME_UPDATE, buff, 4);
#endif	
}

void DateAndTimeInit(void)
{
	FILE 			*fd = NULL;
	char buff[200+1] = {0};
	char buff2[200+1] = {0};
	unsigned char tempYear[5] = {0};
	unsigned char tempMon[3] = {0};
	unsigned char tempDay[3] = {0};
	unsigned char tempHour[3] = {0};
	unsigned char tempMin[3] = {0};
	unsigned char tempSecond[3] = {0};
	int curtime;
	int saveTime;
	int temp;
	int updateFlag;

	ReadDateTime(0);
	ReadDateTime(1);
	
	if( (fd=fopen(TIME_TEMP_FILE,"r")) == NULL )
	{
		return;
	}
	
	usleep(100*1000);
	
	if(fgets(buff,200,fd) != NULL)
	{
		sscanf(buff,"%*[^\"]\"%[0-9]-%[0-9]-%[0-9] %[0-9]:%[0-9]:%[0-9]\"", tempYear,tempMon,tempDay,tempHour,tempMin,tempSecond);
	}
	printf("%s\n", buff);

	if(fgets(buff2,200,fd) != NULL)
	{
		sscanf(buff2,"%*[^=]=%d", &updateFlag);
	}
	printf("updateFlag=%d\n", updateFlag);

	fclose(fd);
	
	printf("curtime %s-%s-%s %s-%s-%s\n", year, mon, day, hour, min, second);
	printf("saveTime %s-%s-%s %s-%s-%s\n", tempYear, tempMon, tempDay, tempHour, tempMin, tempSecond);
	SetTimeUpdateFlag(updateFlag);
	if(atoi(year)<2015)
	{
		DateSave("20150201");
	}
	#if 0
	curtime = atoi(min) + atoi(hour)*60 + atoi(day)*24*60 + atoi(mon)*30*24*60 + atoi(year)*12*30*24*60;
	saveTime = atoi(tempMin) + atoi(tempHour)*60 + atoi(tempDay)*24*60 + atoi(tempMon)*30*24*60 + atoi(tempYear)*12*30*24*60;
	
	temp = curtime - saveTime;
	
	printf("temp = curtime - saveTime -> %d = %d - %d\n", temp, curtime, saveTime);

	time_t t_stm8 = GetLocalTimeFromStm8l();
	time_t t_now = time(NULL);

	printf("t_stm8 = %d, t_now = %d\n", t_stm8, t_now);

	//如果stm8时间为0，则表明刚上电
	if(t_stm8 == 0 || t_stm8 == -1)
	{
		//如果RTC时间比文件时间快一个小时以内，则RTC的电还没掉完，此时使用RTC时间，并更新到文件
		if(temp >= 0 && temp <= 60)
		{
			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
		//如果RTC的电已经掉完，此时使用文件时间，但是不显示出来
		else
		{
			if( (fd = popen( buff, "r" )) == NULL)
			{
				return;
			}		
			pclose( fd );
			
			if( (fd = popen( "hwclock -w", "r" )) == NULL)
			{
				return;
			}
			pclose( fd );
			
			SaveDateAndTimeToFile();
		}

	}
	//如果stm8时间不为0，则表明机器刚重启
	else
	{
		//本地RTC时间比stm8时间快一分钟以内，说明RTC时间正确, 使用RTC时间
		if(t_now - t_stm8 >= 0 && t_now - t_stm8 <= 60)
		{
			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
		//RTC时间错误，使用STM8时间
		else
		{
			unsigned char h;
			unsigned char mi;
			unsigned char s;
			unsigned char y;
			unsigned char mo;
			unsigned char d;

			struct tm *tblock;	
		
			tblock=localtime(&t_stm8);
			
			y = tblock->tm_year-100;
			mo = tblock->tm_mon+1;
			d = tblock->tm_mday;
			h = tblock->tm_hour;
			mi = tblock->tm_min;
			s = tblock->tm_sec;

			
			// init year
			snprintf(year,5,"20%02d",y);	
			// init month
			snprintf(mon,3,"%02d",mo);	
			// init day
			snprintf(day,3,"%02d",d);	
			// init hour
			snprintf(hour,3,"%02d",h);	
			// init minute
			snprintf(min,3,"%02d",mi);	
			// init minute
			snprintf(second,3,"%02d",s);	
			
			snprintf( buff, 200,  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

			if( (fd = popen( buff, "r" )) == NULL )
			{
				return;
			}		
			pclose( fd );
			
			if( (fd = popen( "hwclock -w", "r" )) == NULL )
			{
				return;
			}
			pclose( fd );

			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
	}
	#endif
}

void SaveDateAndTimeToFile(void)
{
	FILE 			*file = NULL;
	char buff[200+1];
	if( (file=fopen(TIME_TEMP_FILE,"w+")) == NULL )
	{
		return NULL;
	}
	
	ReadDateTime(1);
	ReadDateTime(0);

	snprintf( buff, 200,  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	fputs(buff, file);

	snprintf( buff, 200,  "update falg=%d\n", GetTimeUpdateFlag());

	fputs(buff, file);

	fclose(file);

	sync();
}

//此函数每一分钟调用一次
void OneHourSaveDateAndTime(void)
{
	static int timeCnt = 0;
	uint8 enable;
	uint8 timeZone;
	char timeServer[50] = {0};
	static int timeZone_init=0;
	timeCnt++;

	//60分钟保存到文件
	if(!(timeCnt%60))
	{
		SaveDateAndTimeToFile();
		UpdateLocalTimeToStm8l();
	}

	//隔8小时同步时间
	if(timeCnt >= 8*60)
	{
		timeCnt = 0;
		char temp[20];
		API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, temp);
		enable = atoi(temp);
		if(enable)
		{
			API_Event_IoServer_InnerRead_All(TimeZone, temp);
			timeZone = atoi(temp);
			API_Event_IoServer_InnerRead_All(TIME_SERVER, (uint8*)&timeServer);
			sync_time_from_sntp_server(inet_addr(timeServer), timeZone-12);
		}		
	}
	else if(timeCnt&&timeZone_init==0)
	{
		char buff[20]={0};
		API_Event_IoServer_InnerRead_All(TimeZone, buff);
		set_timezone(atoi(buff)- 12);
		timeZone_init=1;
	}
}


//cao_20181020_e

int set_system_date_time(uint8 dateOrTime)
{
	FILE* fd   = NULL;
	char buf[100] = {0};
	
	if(dateOrTime)
	{
		memcpy(year, inputSet, 4);
		memcpy(mon, inputSet+4, 2);
		memcpy(day, inputSet+6, 2);
		ReadDateTime(0);
	}
	else
	{
		memcpy(hour, inputSet, 2);
		memcpy(min, inputSet+2, 2);
		memcpy(second, "00", 2);
		ReadDateTime(1);
	}
	snprintf( buf, sizeof(buf),  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	

	dprintf( "%s\n",buf);
	
	if( (fd = popen( buf, "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}		
	pclose( fd );

	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	
	dprintf( "set_system_date_time ok!\n" );

	//cao_201081020_s
	SetTimeUpdateFlag(1);
	SaveDateAndTimeToFile();
	//cao_201081020_e
	
	return 0;
}

int RtcSet_ParaCheckDate(void)
{
	int para_len,para_value;
	
	memcpy(year, inputSet, 4);
	memcpy(mon, inputSet+4, 2);
	memcpy(day, inputSet+6, 2);

	if(strlen(inputSet) != 8)
	{
		return -1;
	}
	
	para_len = strlen(year);
	if(para_len != 4)
	{
		return -1;
	}
	para_value = atol(year);
	//if(para_value > 2037 || para_value < 2005)
	if(para_value < 2000)
	{
		return -1;
	}

	para_len = strlen(mon);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(mon);
	if(para_value > 12)
	{
		return -1;
	}

	para_len = strlen(day);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(day);
	if(para_value > 31)
	{
		return -1;
	}
	return 0;
}

int RtcSet_ParaCheckTime(void)
{
	int para_len,para_value;
	
	memcpy(hour, inputSet, 2);
	memcpy(min, inputSet+2, 2);
	
	if(strlen(inputSet) != 4)
	{
		return -1;
	}

	para_len = strlen(hour);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(hour);
	if(para_value > 23)
	{
		return -1;
	}

	para_len = strlen(min);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(min);
	if(para_value > 59)
	{
		return -1;
	}

	para_len = strlen(second);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(second);
	if(para_value > 59)
	{
		return -1;
	}

	return 0;
}


