#include "MENU_public.h"
int SetIpAddress(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	//dhcpState = atoi(temp);
	//if(dhcpState == 0)
	if(atoi(temp)	==0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_IP(tmepIp);
		WriteStaticIpToMyConfig(GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		SetNetWork(NULL, GetSysVerInfo_IP(), NULL, NULL);
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}

int SetSubNetMask(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	//dhcpState = atoi(temp);
	//if(dhcpState == 0)
	if(atoi(temp)	==0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_mask(tmepIp);
		WriteStaticIpToMyConfig(GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		SetNetWork(NULL, NULL, GetSysVerInfo_mask(), NULL);
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}

int SetGateway(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	//dhcpState = atoi(temp);
	//if(dhcpState == 0)
	if(atoi(temp)	==0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_gateway(tmepIp);
		WriteStaticIpToMyConfig(GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		SetNetWork(NULL, NULL, NULL, GetSysVerInfo_gateway());
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}
