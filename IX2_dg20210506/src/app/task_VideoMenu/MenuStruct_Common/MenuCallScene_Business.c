#include "MENU_public.h"

int NoDisturbSetting = 0;		//czn_20170805
OS_TIMER NoDisturbTimer;
time_t NoDisturb8H_StartTime;
int Get_NoDisturb8H_RemainTime(int *remain_hour,int *remain_min)
{
	unsigned char hour;
	unsigned char minute;
	unsigned char week;
	unsigned char year;
	unsigned char day;
	unsigned char month;
	
	struct tm tm_temp;
	time_t start_time,cur_time;
	int hold_min,remain_allmin;
	
	#if 0
	minute = NoDisturb8H_StartTime.RTC_Minutes;
	hour = NoDisturb8H_StartTime.RTC_Hours;
	week = NoDisturb8H_StartDate.RTC_WeekDay;
	year = NoDisturb8H_StartDate.RTC_Year;
	day = NoDisturb8H_StartDate.RTC_Date;
	month = NoDisturb8H_StartDate.RTC_Month;
	
	
	memset(&tm_temp,0,sizeof(struct tm));
	tm_temp.tm_hour = BcdToBin(hour);
	tm_temp.tm_min = BcdToBin(minute);
	tm_temp.tm_wday = BcdToBin(week);
	tm_temp.tm_year = BcdToBin(year);
	tm_temp.tm_mday = BcdToBin(day);
	tm_temp.tm_mon = BcdToBin(month);

	start_time = mktime(&tm_temp);

	RTC_DateTypeDef curdate = RtcGetDate();
	RTC_TimeTypeDef curtime = RtcGetTime();

	minute = curtime.RTC_Minutes;
	hour = curtime.RTC_Hours;
	week = curdate.RTC_WeekDay;
	year = curdate.RTC_Year;
	day = curdate.RTC_Date;
	month = curdate.RTC_Month;

	memset(&tm_temp,0,sizeof(struct tm));
	tm_temp.tm_hour = BcdToBin(hour);
	tm_temp.tm_min = BcdToBin(minute);
	tm_temp.tm_wday = BcdToBin(week);
	tm_temp.tm_year = BcdToBin(year);
	tm_temp.tm_mday = BcdToBin(day);
	tm_temp.tm_mon = BcdToBin(month);

	cur_time = mktime(&tm_temp);
	#else
	start_time = NoDisturb8H_StartTime;
	cur_time = time(NULL);
	#endif
	hold_min = (cur_time - start_time)/60;
	
	if(hold_min < 0)
	{
		if(remain_hour != NULL && remain_min!= NULL)
		{
			*remain_hour = 0;
			*remain_min = 0;
		}
		return 0;
	}

	//remain_allmin = 5 - hold_min;
	remain_allmin = 60*8 - hold_min;

	if(remain_allmin <= 0)
	{
		if(remain_hour != NULL && remain_min!= NULL)
		{
			*remain_hour = 0;
			*remain_min = 0;
		}
		return 0;
	}

	if(remain_hour != NULL && remain_min!= NULL)
	{
		*remain_hour = remain_allmin/60;
		*remain_min = remain_allmin%60;
	}

	return remain_allmin;
}

int Get_NoDisturbSetting(void)
{
	if(NoDisturbSetting == 1)
	{
		if(Get_NoDisturb8H_RemainTime(NULL,NULL) == 0)
			NoDisturbSetting = 0; 
	}
	return NoDisturbSetting;
}

void NoDisturbTimer_Callback(void)
{
	if(NoDisturbSetting == 1)
	{
		if(Get_NoDisturb8H_RemainTime(NULL,NULL) == 0)
		{
			NoDisturbSetting = 0; 
			API_LedDisplay_CloudBeDisturbed(); 	//czn_20170926		//API_LED_DISTURB_OFF();
			OS_StopTimer(&NoDisturbTimer);
		}
		else
		{
			OS_RetriggerTimer(&NoDisturbTimer);
		}
		return;
	}

	OS_StopTimer(&NoDisturbTimer);
}

int Set_NoDisturbSetting(int CallScene)
{
	static int nodisturbtimer_init = 0;
	
	if(nodisturbtimer_init == 0)
	{
		nodisturbtimer_init = 1;
		OS_CreateTimer(&NoDisturbTimer, NoDisturbTimer_Callback, 60000/25);
	}
	
	if(CallScene == 1 || CallScene == 2)
	{
		NoDisturbSetting = CallScene;

		if(NoDisturbSetting == 1)
		{
			// Get the current date time
			#if 0
			NoDisturb8H_StartDate = RtcGetDate();
			NoDisturb8H_StartTime= RtcGetTime();
			#else
			NoDisturb8H_StartTime = time(NULL);
			#endif

			API_LedDisplay_NotDisturb();		//czn_20170926		//API_LED_DISTURB_ON();
			OS_RetriggerTimer(&NoDisturbTimer);
		}
		else
		{
			API_LedDisplay_NotDisturb();		//czn_20170926		//API_LED_DISTURB_ON();
			OS_StopTimer(&NoDisturbTimer);
		}
	}
	else
	{
		NoDisturbSetting = 0;
		API_LedDisplay_CloudBeDisturbed();		//czn_20170926		API_LED_DISTURB_OFF();
		OS_StopTimer(&NoDisturbTimer);
	}

	return NoDisturbSetting;
}