#include "MENU_public.h"
#include "MENU_NM_Main.h"
#include "MenuInstallIp_Business.h"
#include "obj_NetManagerInterface.h"
#include "task_WiFiConnect.h"

#define ICON_IX_NET_MODE 	1
#define ICON_LAN_SETTING 	2
#define ICON_WLAN_SETTING 3


const IconAndText_t NM_MainSet[] = 
{
	{ICON_IX_NET_MODE,					MESG_TEXT_IX_NET_MODE},
	{ICON_LAN_SETTING,					MESG_TEXT_LAN_SETTING},
	{ICON_WLAN_SETTING,					MESG_TEXT_WLAN_SETTING},
};

const unsigned char NM_MainSetNum = sizeof(NM_MainSet)/sizeof(NM_MainSet[0]);

void NM_Main_NetModeSet(int select)
{
	if(select==0)
	{
		api_nm_if_set_net_mode(NM_MODE_LAN);
	}
	else
	{
		api_nm_if_set_net_mode(NM_MODE_WLAN);
	}
}

void MenuListGetNMMainPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, stringIndex;
	int list_start;
	char val_buff[80] = {0};
	int recordLen, unicode_len;
	char unicode_buf[200];
	char time[40];
	char node[40];
	
	pDisp->dispCnt = 0;
	
	
		
		
		list_start = currentPage*onePageListMax;
		
		for(i = 0; i < onePageListMax; i++ )
		{
			if((currentPage*onePageListMax+i)>=NM_MainSetNum)
				return;
			API_GetOSD_StringWithID(NM_MainSet[i+currentPage*onePageListMax].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			switch(NM_MainSet[i+currentPage*onePageListMax].iCon)
			{
				case ICON_IX_NET_MODE:
					if(api_nm_if_get_net_mode() == NM_MODE_LAN)		//FOR_INDEXA
						API_GetOSD_StringWithID(MESG_TEXT_NETMODE_LAN, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
					else
						API_GetOSD_StringWithID(MESG_TEXT_NETMODE_WLAN, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
					break;
				case ICON_LAN_SETTING:
					switch(GetIpActionType())
					{
						case 1:
							sprintf(val_buff, "%s%s", api_nm_if_get_lan_ip(), "-D");
							break;
							
						case 2:
							sprintf(val_buff, "%s%s", api_nm_if_get_lan_ip(), "-A");
							break;
							
						case 3:
							sprintf(val_buff, "%s%s", api_nm_if_get_lan_ip(), "-S");
							break;
							
						default:
							sprintf(val_buff, "%s%s", api_nm_if_get_lan_ip(), "-U");
							break;
					}
					unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
					break;
				case ICON_WLAN_SETTING:
					if(wifiRun.wifiSwitch&&wifiRun.wifiConnect==2)
					{
						sprintf(val_buff, "%s%s", api_nm_if_get_wlan_ip(), "-D");
						unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
					}
					else
					{
						unicode_len=0;
					}
					break;
				default:
					unicode_len=0;
					break;
			}
			if(unicode_len>0)
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].valLen= unicode_len;
			pDisp->dispCnt++;
		}

	
}

void MENU_NM_Main_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit = ListPropertyDefault();
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 120;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 330;
	}
	listInit.valEn=1;
	
	//OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	#if defined(PID_IX850)
	#else
	API_MenuIconDisplaySelectOn(ICON_029_IP_ADDR);
	#endif
	//API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_029_IP_ADDR, 1, 0);
	//DisplayInstallIpPageIcon(installIpPageSelect);

	listInit.listCnt= NM_MainSetNum;
	API_GetOSD_StringWithID2(MESG_TEXT_ICON_047_IpAddress,unicode_buf,&unicode_len);
	//API_GetOSD_StringWithIcon(ICON_029_IP_ADDR,unicode_buf,&unicode_len);
	listInit.fun = MenuListGetNMMainPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;

	InitMenuList(listInit);
}

void MENU_NM_Main_Exit(void)
{
	MenuListDisable(0);
}

void MENU_NM_Main_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int len;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
			#if defined(PID_IX850)
			#else
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;
				case ICON_030_CALL_NUMBER:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 0);
					break;
				case ICON_049_Parameter:
					EnterInstallSubMenu(MENU_077_ParaGroup, 0);
					break;
					
				case ICON_045_Upgrade:
					EnterInstallSubMenu(MENU_066_FwUpgrade, 0);
					break;
					
				case ICON_031_OnsiteTools:
					SetSearchBdNumber(GetSysVerInfo_bd());
					SetSearchDeviceType(0);
					SetSearchMaxnum(0);
					EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
					break;		
			#endif
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						if(listIcon.mainIcon >= NM_MainSetNum)
						{
							return;
						}
						switch(NM_MainSet[listIcon.mainIcon].iCon)
						{
							case ICON_IX_NET_MODE:
								#if defined(PID_IX850)
								#else
								API_GetOSD_StringWithID(MESG_TEXT_NETMODE_LAN, NULL, 0, NULL, 0,&publicSettingDisplay[0][1], &len);
								publicSettingDisplay[0][0]=len;

								API_GetOSD_StringWithID(MESG_TEXT_NETMODE_WLAN, NULL, 0, NULL, 0,&publicSettingDisplay[1][1], &len);
								publicSettingDisplay[1][0]=len;
							
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_IX_NET_MODE, 2, api_nm_if_get_net_mode(), NM_Main_NetModeSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								#endif
								break;
							case ICON_LAN_SETTING:
								#if defined(PID_IX850)
								StartInitOneMenu(MENU_NM_LanSetting,0,0);
								#else
								EnterInstallSubMenu(MENU_NM_LanSetting, 0);
								#endif
								break;
							case ICON_WLAN_SETTING:
								#if defined(PID_IX850)
								StartInitOneMenu(MENU_NM_WlanSetting,0,0);
								#else
								EnterInstallSubMenu(MENU_NM_WlanSetting, 0);
								#endif
								break;	
							
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}



