
#include "MENU_public.h"
#include "MENU_NM_WifiInformation.h"
#include "task_WiFiConnect.h"

#define	WIFI_INFO_ICON_MAX		5
#define	WIFI_SSID_MAX_LEN		40
#define	WIFI_PWD_MAX_LEN		40

int wifiInfoIconSelect;
int wifiInfoPageSelect;

extern int wifiSelectNum;
uint8 ssid[WIFI_SSID_MAX_LEN+1];
uint8 pwd[WIFI_PWD_MAX_LEN+1];

int wifiHadConnected;
int wifiAutoConnect;

#define ICON_WifiSSID 			1
#define ICON_WifiPWD			2
#define ICON_WifiAutoConnect		3
#define ICON_WifiConnect			4

#define MESG_TEXT_ICON_WifiConnect		MESG_TEXT_ICON_035_WifiConnect

IconAndText_t wifiInfoIconTable[] = 
{
	{ICON_WifiSSID,					MESG_TEXT_ICON_033_WifiSSID},    
	{ICON_WifiPWD,					MESG_TEXT_ICON_034_WifiPWD},
	{ICON_WifiAutoConnect, 			MESG_TEXT_ICON_WifiAutoConnect},
	{ICON_WifiConnect,				MESG_TEXT_ICON_035_WifiConnect},
};

const int wifiInfoIconNum = sizeof(wifiInfoIconTable)/sizeof(wifiInfoIconTable[0]);



void MenuListGetNMWifiInfoPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, stringIndex;
	int list_start;
	char val_buff[80] = {0};
	int recordLen, unicode_len;
	char unicode_buf[200];
	char time[40];
	char node[40];
	
	pDisp->dispCnt = 0;
	
	
		
		
		list_start = currentPage*onePageListMax;
		
		for(i = 0; i < onePageListMax; i++ )
		{
			if((currentPage*onePageListMax+i)>=wifiInfoIconNum)
				return;
			API_GetOSD_StringWithID(wifiInfoIconTable[i+currentPage*onePageListMax].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			switch(wifiInfoIconTable[i+currentPage*onePageListMax].iCon)
			{
				case ICON_WifiSSID:
					//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, ssid, strlen(ssid), 1, STR_UTF8, bkgd_w-x);
					//if(strlen(ssid))
					//{
					//	DisplayWifiQuality(x+DISPLAY_VALUE_X_DEVIATION, y+3, wifiRun.wifiData.WifiInfo[wifiSelectNum-1].LEVEL);
					//}
					unicode_len = 2*api_ascii_to_unicode(ssid,strlen(ssid),unicode_buf);
					break;
				case ICON_WifiAutoConnect:
					//ListSelect(i, wifiAutoConnect);
					if(wifiAutoConnect)
						pDisp->disp[pDisp->dispCnt].preSprType =PRE_SPR_SELECT;
					unicode_len=0;
					break;
				case ICON_WifiPWD:
					//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, "********************************", strlen(pwd), 1, STR_UTF8, bkgd_w-x);
					unicode_len = 2*api_ascii_to_unicode("********************************", strlen(pwd),unicode_buf);
					break;
				
				default:
					unicode_len=0;
					break;
			}
			if(unicode_len>0)
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].valLen= unicode_len;
			pDisp->dispCnt++;
		}

	
}

int NM_WiFiInfoInputPwd(char * code)
{
	strcpy(pwd,code);
	return 1;
}

int NM_WiFiInfoInputSsid(char * code)
{
	strcpy(ssid,code);
	return 1;
}

void MENU_NM_WifiInformation_Init(int uMenuCnt)
{
	uint16 x, y, i;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2;
	WIFI_RUN wifiRun = GetWiFiState();
	
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit = ListPropertyDefault();
	if(GetLastNMenu() == MENU_NM_WlanSetting)
	{
		ssid[0] = 0;
		pwd[0] = 0;
		if(wifiSelectNum != 0)
		{
			strcpy(ssid, wifiRun.wifiData.WifiInfo[wifiSelectNum-1].ESSID);
			for(x = 0; x < 3; x++)
			{
				if(!strcmp(ssid, wifiRun.saveSsid[x]))
				{
					strcpy(pwd, wifiRun.savePwd[x]);
					break;
				}
			}
		}
	}

	if(wifiSelectNum == 1)
	{
		if(wifiRun.wifiConnect == 0)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,MESG_TEXT_WIFI_Disconnected,1, 0);
			wifiHadConnected = 0;
		}
		else if(wifiRun.wifiConnect == 1)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,MESG_TEXT_WIFI_Connecting,1, 0);
			wifiHadConnected = 1;
		}
		else
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,MESG_TEXT_WIFI_Connected,1, 0);
			wifiHadConnected = 2;
		}
	}
	else if(wifiSelectNum > 1)
	{
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,MESG_TEXT_WIFI_Disconnected,1, 0);
		wifiHadConnected = 0;
	}

	for(i = 0; i < wifiInfoIconNum; i++)
	{
		if(wifiInfoIconTable[i].iCon == ICON_WifiConnect)
		{
			if(wifiHadConnected == 2)
			{
				wifiInfoIconTable[i].iConText = MESG_TEXT_ICON_WifiDisconnect;
			}
			else
			{
				wifiInfoIconTable[i].iConText = MESG_TEXT_ICON_WifiConnect;
			}
		}
	}
	
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textOffset = 0;
		listInit.textValOffset = 180;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 330;
	}
	listInit.selEn=1;
	listInit.valEn=1;
	
	//OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	#if defined(PID_IX850)
	#else
	API_MenuIconDisplaySelectOn(ICON_029_IP_ADDR);
	#endif
	//API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_029_IP_ADDR, 1, 0);
	//DisplayInstallIpPageIcon(installIpPageSelect);
	listInit.listCnt= wifiInfoIconNum;
	API_GetOSD_StringWithID(MESG_TEXT_WIFI_Input, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
	//API_GetOSD_StringWithIcon(ICON_029_IP_ADDR,unicode_buf,&unicode_len);
	listInit.fun = MenuListGetNMWifiInfoPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	wifiAutoConnect = 1;
	InitMenuList(listInit);
	
	//API_MenuIconDisplaySelectOn(ICON_027_wireless);
	//wifiInfoPageSelect = 0;
	//DisplayWifiInfoPageIcon(wifiInfoPageSelect);
}

void MENU_NM_WifiInformation_Exit(void)
{
	MenuListDisable(0);
}

void MENU_NM_WifiInformation_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	WIFI_RUN wifiRun = GetWiFiState();
	LIST_ICON listIcon;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					//popDisplayLastMenu();
					EnterInstallSubMenu(MENU_NM_WlanSetting, 0);
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						if(listIcon.mainIcon >= wifiInfoIconNum)
						{
							return;
						}
						switch(wifiInfoIconTable[listIcon.mainIcon].iCon)
						{
								
							case ICON_WifiSSID:
								if(wifiSelectNum == 0)
								{
									//EnterKeypadMenu(KEYPAD_CHAR, INPUT_WIFI_SSID, ssid, WIFI_SSID_MAX_LEN, COLOR_WHITE, NULL, 0);
									EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiSSID, ssid, WIFI_SSID_MAX_LEN, COLOR_WHITE, NULL, 1, NM_WiFiInfoInputSsid);
								}
								break;
							case ICON_WifiPWD:
								//EnterKeypadMenu(KEYPAD_CHAR, INPUT_WIFI_PWD, pwd, WIFI_PWD_MAX_LEN, COLOR_WHITE, NULL, 0);
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, pwd, WIFI_SSID_MAX_LEN, COLOR_WHITE, NULL, 1, NM_WiFiInfoInputPwd);
								break;
								
							case ICON_WifiAutoConnect:
								wifiAutoConnect = !wifiAutoConnect;
								//ListSelect(2, wifiAutoConnect);
								MenuListDiplayPage();
								if(!wifiAutoConnect)
								{
									API_WifiDeleteSSID(ssid, pwd);
								}
								break;

							case ICON_WifiConnect:
								//popDisplayLastNMenu(2);
								EnterInstallSubMenu(MENU_NM_WlanSetting, 0);
								if(wifiHadConnected == 2)
								{
									API_WifiDisCNCT();
								}
								else
								{
									if(wifiAutoConnect)
									{
										API_WifiAddSSID(ssid, pwd);
									}
									API_WifiDisCNCT();
									API_WifiCNCT(ssid, pwd);
								}
								break;
						}
					}
					break;	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
	
}


