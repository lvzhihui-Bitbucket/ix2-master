
#include "MENU_NM_WlanSetting.h"
#include "task_WiFiConnect.h"


#define	WLAN_SETTING_ICON_MAX	5
#define	SSID_LEN_MAX			50

int wlanSettingIconSelect;
int wlanSettingPage;
int wifiSelectNum;
static char  wifiSaveSsid[ESSID_LEN];


int GetWifiQuality(uint8 quality)
{
	int level=0;
	level|=quality;
	if(quality&0x80)
		level|=0xffffff00;
	if(level >= -55)
	{
		return SUF_SPR_Wifi_QUALITY4;
	}
	else if(level >= -70)
	{
		
		return SUF_SPR_Wifi_QUALITY3;
	}
	else if(level >= -85)
	{
		
		return SUF_SPR_Wifi_QUALITY2;
	}
	else
	{

		return SUF_SPR_Wifi_QUALITY1;
	}

	#if 0
	if(quality > 80)
	{
		//API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY4);
		//return SPRITE_MainWifi_QUALITY4;
		return SUF_SPR_Wifi_QUALITY4;
	}
	else if(quality > 50)
	{
		//API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY3);
		//return SPRITE_MainWifi_QUALITY3;
		return SUF_SPR_Wifi_QUALITY3;
	}
	else if(quality > 30)
	{
		//API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY2);
		//return SPRITE_MainWifi_QUALITY2;
		return SUF_SPR_Wifi_QUALITY2;
	}
	else if(quality > 0)
	{
		//API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY1);
		//return SPRITE_MainWifi_QUALITY1;
		return SUF_SPR_Wifi_QUALITY1;
	}
	else
	{
		//API_SpriteClose( x, y, SPRITE_WIFI_QUALITY4);
		return SUF_SPR_NONE;
	}
	#endif
}




void MenuListGetWlanSettingPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, stringIndex;
	int list_start;
	char val_buff[80] = {0};
	int recordLen, unicode_len;
	char unicode_buf[200];
	char time[40];
	char node[40];
	int listNum;
	pDisp->dispCnt = 0;
	
	listNum = (wifiRun.wifiSwitch) ? (wifiRun.wifiData.DataCnt+2) : 1;
		
		
	list_start = currentPage*onePageListMax;
	
	for(i = 0; i < onePageListMax; i++ )
	{

		if(currentPage*onePageListMax+i < listNum)
		{
			#if 1
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			if(i == 0 && currentPage == 0)
			{
				API_GetOSD_StringWithID(MESG_TEXT_Icon_029_WifiSwitch, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
				API_GetOSD_StringWithID(wifiRun.wifiSwitch ? MESG_TEXT_WIFI_ON : MESG_TEXT_WIFI_OFF, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].valLen= unicode_len;
			}
			else if(i == 1 && currentPage == 0)
			{
				//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_WIFI_Input, 1, 0);
				API_GetOSD_StringWithID(MESG_TEXT_WIFI_Input, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
				pDisp->disp[pDisp->dispCnt].valLen= 0;
			}
			else
			{
			#if 1
				char* pwifistr;
				int len;
				int color;
				int j;
				
				pwifistr = wifiRun.wifiData.WifiInfo[i+currentPage*onePageListMax-2].ESSID;
				
				if((currentPage== 0) && (i == 2) && (wifiRun.wifiConnect == 2) && (!strcmp(wifiRun.curWifi.ESSID, pwifistr)))
				{
					//ListSelect(i, 1);
					pDisp->disp[pDisp->dispCnt].preSprType =PRE_SPR_SELECT;

				}

				for(color = DISPLAY_LIST_COLOR, j = 0; j < 3; j++)
				{
					if(!strcmp(pwifistr, wifiRun.saveSsid[j]))
					{
						color = DISPLAY_STATE_COLOR;
						break;
					}
				}
			
				len = (strlen(pwifistr) > SSID_LEN_MAX) ? SSID_LEN_MAX : strlen(pwifistr);
				
				//API_OsdStringDisplayExt(x, y, color, pwifistr, len,1,STR_UTF8, 0);
				pDisp->disp[pDisp->dispCnt].sufSprType= GetWifiQuality(wifiRun.wifiData.WifiInfo[i+currentPage*onePageListMax-2].LEVEL);
				unicode_len = 2*api_ascii_to_unicode(pwifistr,len,unicode_buf);
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
				pDisp->disp[pDisp->dispCnt].valLen= 0;
			#endif
			}
			pDisp->dispCnt++;
			#endif
		}
		
	}

	
}
int NM_WiFiInputPwdConnect(char * code)
{
	WIFI_RUN wifiRun = GetWiFiState();
	
	strcpy(wifiSaveSsid, wifiRun.wifiData.WifiInfo[wifiSelectNum].ESSID);
	
	API_WifiAddSSID(wifiSaveSsid, code);
	API_WifiCNCT(wifiSaveSsid, code);
	return 1;
}

static void NM_ConnectWiFi(int index)
{
	int j;
	WIFI_RUN wifiRun = GetWiFiState();
	char *pwifistr;
	char saveid[41];
	char savepwd[41];

	if(index >= wifiRun.wifiData.DataCnt)
	{
		return;
	}

	pwifistr = wifiRun.wifiData.WifiInfo[index].ESSID;

	#if 0
	//�Ѿ�����
	if(!strcmp(pwifistr, wifiRun.curWifi.ESSID))
	{
		return;
	}
	//δ����
	else
	#endif
	{
		
		for(j = 0; j < 3; j++)
		{
			if(!strcmp(pwifistr, wifiRun.saveSsid[j]))
			{
				break;
			}
		}

		//δ����, ���ڱ�����б�����
		if(j < 3)
		{
			strcpy(saveid,wifiRun.saveSsid[j]);
			strcpy(savepwd,wifiRun.savePwd[j]);
			//API_WifiAddSSID(wifiRun.saveSsid[j], wifiRun.savePwd[j]);
			//API_WifiCNCT(saveid, savepwd);
			EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, NULL, 40, COLOR_WHITE, wifiRun.savePwd[j], 1, NM_WiFiInputPwdConnect);
		}
		//δ����, ��Ҫ��������
		else
		{
			wlanSettingPage=MenuListGetCurrentPage();
			#if defined(PID_IX850)
			#else
			EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, NULL, 40, COLOR_WHITE, NULL, 1, NM_WiFiInputPwdConnect);
			#endif
		}
	}
}
void NM_WiFiConnectFailedProcess(const char* data)
{
	char wifiPwd[WIFI_PWD_LENGTH];
	
	ParseWifiJsonData(data, wifiSaveSsid, wifiPwd);
	#if defined(PID_IX850)
	#else
	EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, NULL, 40, COLOR_WHITE, NULL, 1, NM_WiFiInputPwdConnect);
	#endif
}

LIST_INIT_T listInit;
char unicode_buf[100];
static int wlan_setting_init=0;
void WlanSettingInit(void)
{
	wlan_setting_init=1;
}
void MENU_NM_WlanSetting_Init(int uMenuCnt)
{
	WIFI_RUN wifiRun = GetWiFiState();
	int x, y, i, j;
	POS pos;
	SIZE hv;
	int unicode_len;
	//char unicode_buf[200];
	
	#if 0
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_WirelessSetting,1, 0);
	DisplayWiFiList(wlanSettingPage);
	#endif
	#if defined(PID_IX850)
	#else
	API_MenuIconDisplaySelectOn(ICON_029_IP_ADDR);
	#endif
	
	#if defined(PID_IX850)
	if(GetLastNMenu() == MENU_097_NM_MAIN||GetLastNMenu() == MENU_001_MAIN)
	#else
	if(GetLastNMenu() == MENU_125_NM_MAIN||GetLastNMenu() == MENU_011_SETTING || GetLastNMenu() == MENU_001_MAIN||wlan_setting_init)
	#endif
	{
		wlan_setting_init=0;
		listInit = ListPropertyDefault();
		if(get_pane_type() == 7)
		{
			listInit.listType = TEXT_10X1;
			listInit.textOffset = 0;
			listInit.textValOffset = 150;
		}		
		else
		{
			listInit.listType = TEXT_6X1;
			listInit.textValOffset = 400;
		}
		listInit.selEn=1;
		listInit.ediEn=1;
		listInit.valEn=1;
		
		listInit.fun = MenuListGetWlanSettingPage;
		API_GetOSD_StringWithID(MESG_TEXT_WLAN_SETTING, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
		listInit.titleStr = unicode_buf;
		listInit.titleStrLen = unicode_len;
		
		wlanSettingIconSelect = 0;
		wlanSettingPage = 0;
		
		if(wifiRun.wifiSwitch)
		{
			API_WifiSearch();
			listInit.listCnt= 2;
		}
		else
		{
			listInit.listCnt= 1;
		}
		listInit.currentPage = 0;
	}
	else
	{
		//listInit.listCnt= NM_installIpSetNum;
		listInit.currentPage = wlanSettingPage;
	}
	
	printf("--------------------MENU_NM_WlanSetting_Init--------1-----------------\n");
	InitMenuList(listInit);
	printf("--------------------MENU_NM_WlanSetting_Init--------2-----------------\n");
}

void MENU_NM_WlanSetting_Exit(void)
{
	MenuListDisable(0);
}

void MENU_NM_WlanSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	WIFI_RUN wifiRun = GetWiFiState();
	POS pos;
	SIZE hv;
	int x, y;
	char display[100];
	LIST_ICON listIcon;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{	
			#if defined(PID_IX850)
			#else
				case ICON_200_Return:
					if(get_pane_type()==6 || get_pane_type()==7)
					{
						popDisplayLastMenu();
					}
					else
					{
						if(JudgeIfWlanDevice())
							EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
						else
							popDisplayLastMenu();
					}					
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
				case ICON_030_CALL_NUMBER:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 0);
					break;
				case ICON_049_Parameter:
					EnterInstallSubMenu(MENU_077_ParaGroup, 0);
					break;
					
				case ICON_045_Upgrade:
					EnterInstallSubMenu(MENU_066_FwUpgrade, 0);
					break;
					
				case ICON_031_OnsiteTools:
					SetSearchBdNumber(GetSysVerInfo_bd());
					SetSearchDeviceType(0);
					SetSearchMaxnum(0);
					EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
					break;
			#endif
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						if(listIcon.mainIcon >= listInit.listCnt)
						{
							return;
						}
						if(listIcon.mainIcon == 0)
						{
							if(wifiRun.wifiSwitch)
							{
								API_WifiClose();
							}
							else
							{
								API_WifiOpen();
								//API_WifiSearch();
							}
						}
						else if(listIcon.mainIcon == 1)
						{
							if(wifiRun.wifiSwitch)
							{
								//StartInitOneMenu(MENU_016_WIFI_INFO, 0, 1);
								wifiSelectNum=listIcon.mainIcon-1;
								EnterInstallSubMenu(MENU_NM_WifiInfo, 0);
							}
						}
						else if(listIcon.mainIcon < wifiRun.wifiData.DataCnt+2)
						{
							if(wifiRun.wifiSwitch)
							{
								wifiSelectNum=listIcon.mainIcon-2;
								NM_ConnectWiFi(listIcon.mainIcon-2);
							}
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		if( get_pane_type() == 5  ||get_pane_type() == 7 )
		{
			x = 200;
			y = 950;
		}
		else if( get_pane_type() == 3)//IX850
		{
			x = 0+DISPLAY_DEVIATION_X;
			y = 80;
		}
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47
		{
			x = 332;
			y = 550;
		}
		else
		{
			x = 332;
			y = 550;
		}	
	
		API_OsdStringClearExt(x, y,bkgd_w-x,CLEAR_STATE_H);

		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_CLOSE:
			case MSG_7_BRD_SUB_WIFI_OPEN:
				//DisplayWiFiList(wlanSettingPage);
				MenuListDiplayPage();
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCHING:
				BusySpriteDisplay(1);
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Searching,1, 0);
				//DisplayWiFiList(wlanSettingPage);
				MenuListDiplayPage();
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCH_OVER:
				BusySpriteDisplay(0);
				//DisplayWiFiList(wlanSettingPage);
				listInit.listCnt = (wifiRun.wifiSwitch) ? (wifiRun.wifiData.DataCnt+2) : 1;
				InitMenuList(listInit);
				break;
			case MSG_7_BRD_SUB_WIFI_CONNECTING:
				BusySpriteDisplay(1);
				snprintf(display, 100, "%s ", wifiRun.connectingSsid);
				API_OsdStringClearExt(x, y,bkgd_w-x,CLEAR_STATE_H);
				API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Connecting,1, display, NULL, 0);
				//DisplayWiFiList(wlanSettingPage);
				MenuListDiplayPage();
				break;
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
				BusySpriteDisplay(0);
				break;
				
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
				BusySpriteDisplay(0);
				ReorderWiFiList();
				snprintf(display, 100, "%s ", wifiRun.curWifi.ESSID);
				API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Connected,1, display, NULL, 0);
				wlanSettingPage = 0;
				//DisplayWiFiList(wlanSettingPage);
				listInit.currentPage = wlanSettingPage;
				InitMenuList(listInit);
				break;
			case MSG_7_BRD_SUB_WIFI_CONNECT_FAILED:
				NM_WiFiConnectFailedProcess(GetMenuInformData(arg));
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

