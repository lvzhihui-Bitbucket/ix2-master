#include "MENU_public.h"
#include "MENU_NM_InstallIp.h"
#include "MenuInstallIp_Business.h"
#include "obj_NetManagerInterface.h"

#define	InstallIp_ICON_MAX	5
int installIpIconSelect;
int installIpPageSelect;
char installIpInput[21];

#define ICON_IPAssigned			1
#define ICON_IPAddress			2
#define ICON_IPSubnetMask		3
#define ICON_IPGateway			4
#define ICON_AddressHealthCheck	5


const IconAndText_t NM_installIpSet[] = 
{
	{ICON_IPAssigned,					MESG_TEXT_ICON_094_IPAssigned},
	{ICON_IPAddress,					MESG_TEXT_ICON_095_IPAddress},
	{ICON_IPSubnetMask,					MESG_TEXT_ICON_096_IPSubnetMask},
	{ICON_IPGateway,					MESG_TEXT_ICON_097_IPGateway},
	//{ICON_AddressHealthCheck,			MESG_TEXT_ICON_098_AddressHealthCheck},
};

const unsigned char NM_installIpSetNum = sizeof(NM_installIpSet)/sizeof(NM_installIpSet[0]);

void NM_LAN_IPAssignedSet(int select)
{
	char segment[16];
	char gw[16];
	char mask[16];
	if(select==0)
	{
		API_Event_IoServer_InnerRead_All(IP_Address, segment);
		API_Event_IoServer_InnerRead_All(DefaultRoute, gw);
		API_Event_IoServer_InnerRead_All(SubnetMask, mask);
		api_nm_if_set_lan_mode(LAN_MODE_STATIC,segment,gw,mask);
	}
	else
	{
		api_nm_if_get_lan_autoip_segment(segment);
		api_nm_if_get_lan_autoip_gw(gw);
		api_nm_if_get_lan_autoip_mask(mask);
		api_nm_if_set_lan_mode(LAN_MODE_DHCP_AUTOIP,segment,gw,mask);
	}
}

void MenuListGetInstallIpPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, stringIndex;
	int list_start;
	char val_buff[80] = {0};
	int recordLen, unicode_len;
	char unicode_buf[200];
	char time[40];
	char node[40];
	
	pDisp->dispCnt = 0;
	
	
		
		
		list_start = currentPage*onePageListMax;
		
		for(i = 0; i < onePageListMax; i++ )
		{
			if((currentPage*onePageListMax+i)>=NM_installIpSetNum)
				return;	
			API_GetOSD_StringWithID(NM_installIpSet[i+currentPage*onePageListMax].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			switch(NM_installIpSet[i+currentPage*onePageListMax].iCon)
			{
				case ICON_IPAssigned:
						//sprintf(display, "%s", dhcpState ? "DHCP&AUTO" : "STATIC");
					//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					if(api_nm_if_get_lan_mode() == LAN_MODE_STATIC)		//FOR_INDEXA
						API_GetOSD_StringWithID(MESG_TEXT_CusIpStatic, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
					else
					{
						//sprintf(val_buff, "%s","DHCP&AUTO");
						//unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
						API_GetOSD_StringWithID(MESG_TEXT_CusDHCPAUTOIP, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
					}
					break;
				case ICON_IPAddress:
					strcpy(val_buff, api_nm_if_get_lan_ip());
					unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
					break;
				case ICON_IPSubnetMask:
					strcpy(val_buff, api_nm_if_get_lan_mask());
					unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
					break;
				case ICON_IPGateway:
					strcpy(val_buff, api_nm_if_get_lan_gw());
					unicode_len = 2*api_ascii_to_unicode(val_buff,strlen(val_buff),unicode_buf);
					break;
				case ICON_AddressHealthCheck:
					unicode_len=0;
					break;
				default:
					unicode_len=0;
					break;
			}
			if(unicode_len>0)
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].valLen= unicode_len;
			pDisp->dispCnt++;
		}

	
}

void MENU_NM_LanSetting_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit = ListPropertyDefault();
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 120;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 330;
	}	
	listInit.valEn=1;
	
	//OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	#if defined(PID_IX850)
	#else
	API_MenuIconDisplaySelectOn(ICON_029_IP_ADDR);
	#endif
	//API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_029_IP_ADDR, 1, 0);
	//DisplayInstallIpPageIcon(installIpPageSelect);
	listInit.listCnt= NM_installIpSetNum;
	//API_GetOSD_StringWithIcon(ICON_029_IP_ADDR,unicode_buf,&unicode_len);
	API_GetOSD_StringWithID(MESG_TEXT_LAN_SETTING, NULL, 0, NULL, 0, unicode_buf, &unicode_len);
	listInit.fun = MenuListGetInstallIpPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;

	InitMenuList(listInit);
}

void MENU_NM_LanSetting_Exit(void)
{
	MenuListDisable(0);
}

void MENU_NM_LanSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char* pbCertState = API_PublicInfo_Read_String("CERT_State");
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
			#if defined(PID_IX850)
			#else
				case ICON_200_Return:
					if(JudgeIfWlanDevice())
						EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
					else
						popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;	
				
				case ICON_030_CALL_NUMBER:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 0);
					break;
				case ICON_049_Parameter:
					EnterInstallSubMenu(MENU_077_ParaGroup, 0);
					break;
					
				case ICON_045_Upgrade:
					EnterInstallSubMenu(MENU_066_FwUpgrade, 0);
					break;
					
				case ICON_031_OnsiteTools:
					SetSearchBdNumber(GetSysVerInfo_bd());
					SetSearchDeviceType(0);
					SetSearchMaxnum(0);
					EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
					break;	
			#endif
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						if(listIcon.mainIcon >= NM_installIpSetNum)
						{
							return;
						}
						switch(NM_installIpSet[listIcon.mainIcon].iCon)
						{
							case ICON_IPAssigned:
								#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
								if(pbCertState && !strcmp(pbCertState, "Delivered"))
								{
									CmdRingReq("CERT Delivered, please delete.");
									StartInitOneMenu(MENU_157_CertSetup,0,1);
									return;
								}
								#endif
								#if defined(PID_IX850)
								#else
								if(1)
								{
									#define MAX_SET_NUM 	2		//FOR_INDEXA
									int i, j;
									//char tempChar[MAX_SET_NUM][31] = {"STATIC", "DHCP&AUTO"};
									API_GetOSD_StringWithID(MESG_TEXT_CusIpStatic, NULL, 0, NULL, 0,&publicSettingDisplay[0][1], &i);
									publicSettingDisplay[0][0]=i;
									API_GetOSD_StringWithID(MESG_TEXT_CusDHCPAUTOIP, NULL, 0, NULL, 0,&publicSettingDisplay[1][1], &i);
									publicSettingDisplay[1][0]=i;
									#if 0
									for(i = 1; i < MAX_SET_NUM; i++)
									{
										for(publicSettingDisplay[i][0] = strlen(tempChar[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
										{
											publicSettingDisplay[i][j] = tempChar[i][j/2];
										}
									}
									#endif
									EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_094_IPAssigned, MAX_SET_NUM, api_nm_if_get_lan_mode(), NM_LAN_IPAssignedSet);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								}
								#endif
								break;
							case ICON_IPAddress:
								#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
								if(pbCertState && !strcmp(pbCertState, "Delivered"))
								{
									CmdRingReq("CERT Delivered, please delete.");
									StartInitOneMenu(MENU_157_CertSetup,0,1);
									return;
								}
								#endif
								if(api_nm_if_get_lan_mode() == LAN_MODE_STATIC)
								{
									#if defined(PID_IX850)
									#else
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_095_IPAddress, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_IP(), 1, SetIpAddress);
									#endif
								}
								break;
							case ICON_IPSubnetMask:
								#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
								if(pbCertState && !strcmp(pbCertState, "Delivered"))
								{
									CmdRingReq("CERT Delivered, please delete.");
									StartInitOneMenu(MENU_157_CertSetup,0,1);
									return;
								}
								#endif
								if(api_nm_if_get_lan_mode() == LAN_MODE_STATIC)
								{
									#if defined(PID_IX850)
									#else
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_096_IPSubnetMask, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_mask(), 1, SetSubNetMask);
									#endif
								}
								break;
							case ICON_IPGateway:
								#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
								if(pbCertState && !strcmp(pbCertState, "Delivered"))
								{
									CmdRingReq("CERT Delivered, please delete.");
									StartInitOneMenu(MENU_157_CertSetup,0,1);
									return;
								}
								#endif
								if(api_nm_if_get_lan_mode() == LAN_MODE_STATIC)
								{
									#if defined(PID_IX850)
									#else
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_097_IPGateway, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_gateway(), 1, SetGateway);
									#endif
								}
								break;
							case ICON_AddressHealthCheck:
								#if defined(PID_IX850)
								#else
								StartInitOneMenu(MENU_095_AddressHealthCheck,0,1);
								#endif
								break;
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}



