#include "MENU_public.h"
static int EnterInstaller_Flag = 0;
static int EnterInstaller_time = 0;

void timer_EixtInstaller_callback(void)
{
	if(!EnterInstaller_Flag)
	{
		return;
	}
	
	if(EnterInstaller_time != 0)
	{
		if(--EnterInstaller_time == 0)
		{
			ExitInstallerMode();
		}
	}
}

void EnterInstallerMode(void)
{
	#if defined(PID_DX470)||defined(PID_DX482)
	EnterInstaller_time = 5;
	#else
	EnterInstaller_time = 60;
	#endif
	EnterInstaller_Flag = 1;
}

void ExitInstallerMode(void)
{
	EnterInstaller_Flag  = 0;
	EnterInstaller_time = 0;
}

int JudgeIsInstallerMode(void)
{
	return EnterInstaller_Flag;
}
