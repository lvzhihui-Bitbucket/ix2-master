
#ifndef _MenuBackFwUpgrade_Business_H
#define _MenuBackFwUpgrade_Business_H
typedef enum
{
	FwUpgrade_State_Uncheck = 0,
	FwUpgrade_State_Checking,
	FwUpgrade_State_CheckOk,
	FwUpgrade_State_Downloading,
	FwUpgrade_State_Intalling,
	FwUpgrade_State_Canceling,
	FwUpgrade_State_Intalled,
}FwUpgrade_State_e;

extern int 	BackFwUpgrade_State;
extern int	BackFwUpgrade_Ser_Select;
extern char BackFwUpgrade_Ser_Disp[5][40];
extern char BackFwUpgrade_Code[7];
extern int BackFwUpgradeLine2StrId;
extern int BackFwUpgradeLine3StrId;
extern int BackFwUpgradeLine4StrId;
extern char BackFwUpgradeLine2ValueStr[40];
extern char BackFwUpgradeLine3ValueStr[40];
extern char BackFwUpgradeLine4ValueStr[40];
extern int  BackFwUpgrade_ReomteIp;
extern int  BackFwUpgrade_RebootTime;

#endif

