
#ifndef _MenuInstallIp_Business_H
#define _MenuInstallIp_Business_H
int SetIpAddress(const char* ip);
int SetSubNetMask(const char* ip);
int SetGateway(const char* ip);
#endif

