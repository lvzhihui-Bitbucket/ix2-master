#include "MENU_public.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#if defined(PID_IX850)
SearchIpRspData searchOnlineListData;
#else
extern SearchIpRspData searchOnlineListData;
#endif
SearchIpRspData unhealthyDevice;
static int unhealthyFlag;
static int waitCheckFlag;

void SingleOutUnhealthyDevice(void)
{
	int i, j;
	int addFlag;
	
	unhealthyDevice.deviceCnt = 0;

		
	for(i = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		for(j = 0, addFlag = 0; j < searchOnlineListData.deviceCnt; j++)
		{
			if(i != j)
			{
				if( searchOnlineListData.data[i].Ip == searchOnlineListData.data[j].Ip || 
					!strcmp(searchOnlineListData.data[i].BD_RM_MS, searchOnlineListData.data[j].BD_RM_MS))
				{
					unhealthyDevice.data[unhealthyDevice.deviceCnt++] = searchOnlineListData.data[i];
					addFlag = 1;
					break;
				}
			}
		}

		if(addFlag == 0)
		{
			if( searchOnlineListData.data[i].Ip == inet_addr(GetSysVerInfo_IP()) || 
				!strcmp(searchOnlineListData.data[i].BD_RM_MS, GetSysVerInfo_BdRmMs()))
			{
				unhealthyDevice.data[unhealthyDevice.deviceCnt++] = searchOnlineListData.data[i];
			}		
		}
	}

	for (i = 0; i < unhealthyDevice.deviceCnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < unhealthyDevice.deviceCnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			if (atoi(unhealthyDevice.data[j].BD_RM_MS) > atoi(unhealthyDevice.data[j+1].BD_RM_MS))
			{
				SearchOneDeviceData tempData;

				tempData = unhealthyDevice.data[j+1];
				unhealthyDevice.data[j+1] = unhealthyDevice.data[j];
				unhealthyDevice.data[j] = tempData;
			}
		}
	}
}
void SetMenuWaitCheckFlag(int flag)
{
	waitCheckFlag = flag;
}

int GetMenuWaitCheckFlag(void)
{
	return waitCheckFlag;
}

void SetUnhealthyFlag(int flag)
{
	unhealthyFlag = flag;
}

int CheckIPExist(void)
{
	Dhcp_Autoip_CheckIP();
	SetMenuWaitCheckFlag(1);
	while(GetMenuWaitCheckFlag())
	{
		int i = 20;
		
		usleep(100*1000);
		if(i <= 0)
		{
			SetMenuWaitCheckFlag(0);
			SetUnhealthyFlag(0);
			break;
		}
	}


	return unhealthyFlag;
}
