#include "MENU_public.h"
#include "MenuSetCallTune_Business.h"
int	 		ringNum;
RingList 	*pRingList = NULL;
uint8 LoadRingPlayList(const char* fileName)
{
	char 			linestr[200];
	char 			buffer[200];
	RingList 		tempOneRing;
	FILE 			*pf;
	int 			tempRingNum = 0;
	int				ringId;

	ringNum = 0;
	
	snprintf(linestr, 200, "awk '/<*>/' %s | sed 's/<//g' | sed 's/>//g' | sed 's/\"//g' | awk '{print $1, $3}'", fileName);
	
	pf = popen(linestr, "r");

	if(pf == NULL)
	{
		return ringNum;
	}
	
	while(fgets(buffer, 200, pf) != NULL)
	{
		sscanf(buffer,"%d %20s",&ringId, tempOneRing.name);
		
		if(ringId > 0)
		{
			tempRingNum++;
		}
	}

	pclose(pf);
	
	pf = popen(linestr, "r");

	if(pf == NULL)
	{
		return ringNum;
	}

	pRingList = malloc(tempRingNum*sizeof(RingList));
	if(pRingList == NULL)
	{
		pclose(pf);
		return ringNum;
	}
	
	while(fgets(buffer, 200, pf) != NULL)
	{
		sscanf(buffer,"%d %20s", &ringId, tempOneRing.name);
		
		tempOneRing.id = ringId;
		if(ringId > 0)
		{
			memcpy((char*)(&pRingList[ringNum++]), (char*)&tempOneRing, sizeof(RingList));
		}
	}

	pclose(pf);
	
	return ringNum;
}

unsigned char GetRingId(int index)
{
	unsigned char ret = 0;
	
	if(index < ringNum)
	{
		ret = pRingList[index].id;
	}
	return ret;
}
