#include "MENU_public.h"

int FwUpgrade_Code_Verify(char *code)
{
	API_Event_IoServer_InnerWrite_All(FWUPGRADE_FW_CODE, (uint8*)code);
	return 1;
}

void get_SwUpgradeAppCode(char *code)
{
	char buff[100];
	char 	*pos1,*pos2;
	int		processState;	//处理状态
	int 	i, j;
	char sys_type[7]={0};
	char dev_type[7]={0};
	char cus_type[7]={0};
	
	//strcpy(code,"948100");
	//return;
	FILE *pf = fopen(SwUpgradeIni,"r");
	
	if(pf == NULL)
	{
		return;
	}
	
	for(processState = 0, memset(buff, 0, 100+1); feof(pf) == 0; memset(buff, 0, 100+1))
	{
		fgets(buff,100,pf);
		//去掉前面空格
		for(pos1 = buff; isspace(*pos1); pos1++);
		if(pos1!=buff)
		strcpy(buff, pos1);
		
		//去掉注释
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 = 0;
				break;
			}
		}

		//去掉等号后面空格
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1+1; *pos2!= 0 && isspace(*pos2); pos2++);
				if(pos2 != (pos1+1))
				strcpy(pos1+1, pos2);
			}
		}

		//去掉等号前面空格
		for(pos1 = buff+1; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; pos2 >= buff && isspace(*pos2); pos2--);
				if(pos2 != (pos1-1))
				strcpy(pos2+1, pos1);
			}
		}

		//去掉最后的空格
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;

		//printf("%s\n", buff);
		switch(processState)
		{
			case 0:
				if(strstr( buff, "[App code]" ) != NULL)
				{
					processState = 1;
				}
				#if 0
				if(strstr( buff, "[System type]" ) != NULL)
				{
					processState = 1;
				}
				else if(strstr( buff, "[Device type]")!= NULL)
				{
					processState = 2;
				}
				else if(strstr(buff, "[Customer type]")!= NULL)
				{
					processState = 3;
				}
				#endif
				break;
			case 1:
				if(strlen(buff) > 0)
				{
					strcpy(sys_type,buff);
					processState = 0;
				}
				break;
			case 2:
				if(strlen(buff) > 0)
				{
					strcpy(dev_type,buff);
					processState = 0;
				}
				break;
			case 3:
				//if(strlen(buff) > 0)
				{
					strcpy(cus_type,buff);
					processState = 0;
				}
				break;	
				
		}
	}
	fclose(pf);
	//sprintf(buff,"%s%s%s",dev_type,cus_type,sys_type);
	
	if(FwUpgrade_Code_Verify(sys_type) == 1)
	{
		strcpy(code,sys_type);
	}
}

#define SwUpgradeSer1	"2easyip.com"
#define SwUpgradeSer2	"2easyip.cn"
char RemoteUpdateServer[16]={0};
void SetRemoteUpdateServer(char *ser)
{
	strcpy(RemoteUpdateServer,ser);
}

void DisableRemoteUpdateServer(void)
{
	memset(RemoteUpdateServer,0,16);
}
int get_SwUpgradeSer(char *server_ip)
{
	//FwUpgrade_Ser_Select=select;
	
	uint8 temp,str_buff[5];
	char ch[21];

	if(RemoteUpdateServer[0]!=0)
	{
		strcpy(server_ip,RemoteUpdateServer);
		return 0;
	}
	
	API_Event_IoServer_InnerRead_All(FWUPGRADE_SER_SELECT, (uint8*)str_buff);
	temp = atoi(str_buff);
	//FwUpgrade_Ser_Select = temp;
	API_Event_IoServer_InnerRead_All(FWUPGRADE_OTHER_SER, (uint8*)ch);
	
	if(temp == 0)
	{
		strcpy(server_ip,ch);
	}
	else if(temp == 1)
	{
		//strcpy(FwUpgrade_Ser_Disp,"Server1[47.91.88.33]");
		//strcpy(FwUpgrade_server_ip,"47.91.88.33");
		strcpy(server_ip,"47.91.88.33");
		#if 0
		if(GetWifiConnected())
		{
			parse_remote_server(SwUpgradeSer1, NULL, server_ip);
		}
		#endif
	}
	else if(temp == 2)
	{
		strcpy(server_ip,"47.106.104.38");
		#if 0
		if(GetWifiConnected())
		{
			parse_remote_server(SwUpgradeSer2, NULL, server_ip);
		}
		#endif
	}
	else if(temp == 3)
	{
		//sprintf(FwUpgrade_Ser_Disp,"Server3[%s]",FwUpgrade_other_server);
		//strcpy(FwUpgrade_Ser_Disp,"Server3[192.168.2.60]");
		strcpy(server_ip,"192.168.2.60");
		//strcpy(FwUpgrade_other_server,other_server);
	}
	else if(temp == 4)
	{
		return -1;
	}

	return 0;
}

int get_upgrade_text_info1(char *file_name,char *fw_info,char *bref_info,int *file_length,char *app_code,char *app_ver,char *bsp_ver,char *res_ver)//czn_20190320
{
	#define PROCESS_END		100
	
	char 	buff[100+1] = {0};
	char 	*pos1,*pos2;
	int		processState;	//处理状态
	int 	i, j;
	

	if(fw_info!=NULL)
		*fw_info=0;
	if(bref_info!=NULL)
		*bref_info = 0;
	if(file_length!=NULL)
		*file_length = 0;
	if(app_code!=NULL)
		*app_code =0;
	if(app_ver!=NULL)
		*app_ver=0;
	if(bsp_ver!=NULL)
		*bsp_ver=0;
	if(res_ver!=NULL)
		*res_ver =0;

	//rcpy(fw_info,"ix50");
	//rcpy(bref_info,"IX50_Firmware");
	//ile_length = 24094572;
	printf("get_upgrade_text_info1: %s\n", file_name);
	FILE *pf = fopen(file_name,"r");

	if(pf == NULL)
	{
		return -1;
	}
	
	for(processState = 0, memset(buff, 0, 100+1); fgets(buff,100,pf) != NULL && processState != PROCESS_END; memset(buff, 0, 100+1))
	{
		//去掉前面空格
		for(pos1 = buff; isspace(*pos1); pos1++);
		if(pos1!=buff)
		strcpy(buff, pos1);
		
		//去掉注释
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 = 0;
				break;
			}
		}

		//去掉等号后面空格
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1+1; *pos2!= 0 && isspace(*pos2); pos2++);
				if(pos2 != (pos1+1))
				strcpy(pos1+1, pos2);
			}
		}

		//去掉等号前面空格
		for(pos1 = buff+1; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; pos2 >= buff && isspace(*pos2); pos2--);
				if(pos2 != (pos1-1))
				strcpy(pos2+1, pos1);
			}
		}

		//去掉最后的空格
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;

		printf("%s\n", buff);
		switch(processState)
		{
			case 0:
				if(strstr( buff, "[Device type]" ) != NULL)
				{
					processState = 1;
				}
				else if(strstr( buff, "[Code info]" )!= NULL)
				{
					processState = 2;
				}
				else if(strstr( buff, "[Code size]" )!= NULL)
				{
					processState = 3;
				}
				else if(strstr( buff, "[App Code]" )!= NULL)
				{
					processState = 4;
				}
				else if(strstr( buff, "[App Ver]" )!= NULL)
				{
					processState = 5;
				}
				else if(strstr( buff, "[Bsp Ver]" )!= NULL)
				{
					processState = 6;
				}
				else if(strstr( buff, "[Res Ver]" )!= NULL)
				{
					processState = 7;
				}
				break;
			case 1:
				if(strlen(buff) > 0)
				{
					if(fw_info!=NULL)
					strcpy(fw_info,buff);
					processState = 0;
				}
				break;
			case 2:
				if(strlen(buff) > 0)
				{
					if(bref_info!= NULL)
					strcpy(bref_info,buff);
					processState = 0;
				}
				break;
			case 3:
				if(strlen(buff) > 0)
				{
					if(file_length!=NULL)
					*file_length = atoi(buff);//strcpy(bref_info,buff);
					processState = 0;
				}
				break;	
			case 4:
				if(strlen(buff) > 0)
				{	
					if(app_code!=NULL)
					strcpy(app_code,buff);
					processState = 0;
				}
				break;
			case 5:
				if(strlen(buff) > 0)
				{
					if(app_ver!=NULL)
					strcpy(app_ver,buff);
					processState = 0;
				}
				break;
			case 6:
				if(strlen(buff) > 0)
				{
					if(bsp_ver!=NULL)
					strcpy(bsp_ver,buff);
					processState = 0;
				}
				break;	
			case 7:
				if(strlen(buff) > 0)
				{
					if(res_ver!=NULL)
					strcpy(res_ver,buff);
					processState = 0;
				}
				break;	
				
		}
	}
	
	fclose(pf);
	//printf("%s %s,%d,%d\n",fw_info,bref_info,file_length,processState);
	//if(processState != PROCESS_END)
	//	return -1;
	
	return 0;
}

