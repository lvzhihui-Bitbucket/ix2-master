
#ifndef _MenuUdpResDownload_Business_H
#define _MenuUdpResDownload_Business_H

typedef struct
{
	char	server[40+1];
	char	resId[4+1];
	char	fileName[40+1];
	char*	state;
	char	notice[40+1];
	int		fileSize;
	int		downloadFileSize;
	int		downloadFlag;
	int 	reportIp[10];
	int 	reportCnt;
	
}RES_UDP_DOWNLOAD_DATA_T;

#endif

