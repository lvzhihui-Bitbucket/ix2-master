#include "MENU_public.h"
#include "MenuSipConfig_Business.h"
#include "obj_ip_mon_vres_map_table.h"
// lzh_20181101_s
//static int timeout_update_registration_state = 0;
// lzh_20181101_e

// lzh_20181031_s
//void manual_register_enable(void);
//void manual_register_disable(void);
//int get_manual_register_count(void);
// lzh_20181031_e

char* VTK_AUTO_REG_SERVER_IPADD0 = NULL;
char* VTK_AUTO_REG_SERVER_IPADD1 = NULL;
char* VTK_AUTO_REG_SERVER_IPADD2 = NULL;

char* VTK_AUTO_REG_SERVER_IP0 = NULL;
char* VTK_AUTO_REG_SERVER_IP1 = NULL;
char* VTK_AUTO_REG_SERVER_IP2 = NULL;

SipCfg_T sipCfg = {.serverToIpFlag = -1};

int LoadSipConfigFile(void)
{
	char 	buff[READ_BUFF_LEN+1] = {0};
	char 	*pos1,*pos2;
	int 	strLen;
	FILE 	*file = NULL;
	int 	ret = 0;
	int 	defaultFlag = 0;
	int size;
	int gotostart=0;
	LoadSipStart:
	if((file=fopen(SIP_CONFIG_TEST_FILE_NAME,"r")) == NULL)
	{
		file=fopen(SIP_CONFIG_FILE_NAME,"r");
		if(file!=NULL)
		{
			fseek(file,0, SEEK_END);
			
			size=ftell(file);
			if(size<=0)
			{
				fclose(file);
			}
			else
			{
				fseek(file,0, SEEK_SET);
			}
		}
		
		if(file==NULL||size<=0)
		{
			if( (file=fopen(CUSTOMERIAED_SIP_CONFIG_DEFAULT_FILE,"r")) == NULL )
			{
				if( (file=fopen(SIP_CONFIG_DEFAULT_FILE_NAME,"r")) == NULL )
				{
					printf("2 open %s error!\n", SIP_CONFIG_DEFAULT_FILE_NAME);
					ret = -1;
					goto SipConfigLoadOver;
				}
			}
			{
				defaultFlag = 1;
				api_get_default_sip_local_username(sipCfg.account);
				api_get_default_sip_local_password(sipCfg.password);
				api_get_default_sip_divert_username(sipCfg.divert);
				api_get_default_sip_divert_password(sipCfg.divPwd);
			}
		}
	}

	
	for(memset(buff, 0, READ_BUFF_LEN+1); fgets(buff,READ_BUFF_LEN,file) != NULL; memset(buff, 0, READ_BUFF_LEN+1))
	{
		//ȥ��ǰ��ո�?
		for(pos1 = buff; isspace(*pos1); pos1++);
		strcpy(buff, pos1);
		
		//ȥ��ע��
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 == 0;
			}
		}

		//ȥ���Ⱥź���ո�?
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']' || *pos1 == ']')
			{
				for(pos2 = pos1+1; isspace(*pos2); pos2++);
				strcpy(pos1+1, pos2);
			}
		}

		//ȥ���Ⱥ�ǰ��ո�?
		for(pos1 = buff; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; isspace(*pos2); pos2--);
				strcpy(pos2+1, pos1);
			}
		}

		//ȥ�����Ŀո�
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;



		//server
		pos1 = strstr( buff, "[server]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				memcpy(sipCfg.server, pos2+1, strLen);
				sipCfg.server[strLen] = 0;
			}
			continue;
		}

		//port
		pos1 = strstr( buff, "[port]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				memcpy(sipCfg.port, pos2+1, strLen);
				sipCfg.port[strLen] = 0;
			}
			continue;
		}

		if(defaultFlag == 0)
		{
			//divert
			pos1 = strstr( buff, "[divert]" );
			if( pos1 != NULL )
			{
				pos2 = strchr( pos1, '=' );
				if( pos2 != NULL )
				{
					strLen = strlen(pos2+1);
					strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
					memcpy(sipCfg.divert, pos2+1, strLen);
					sipCfg.divert[strLen] = 0;
				}
				continue;
			}

			//div_pwd
			pos1 = strstr( buff, "[div_pwd]" );
			if( pos1 != NULL )
			{
				pos2 = strchr( pos1, '=' );
				if( pos2 != NULL )
				{
					strLen = strlen(pos2+1);
					strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
					memcpy(sipCfg.divPwd, pos2+1, strLen);
					sipCfg.divPwd[strLen] = 0;
				}
				continue;
			}

			//account
			pos1 = strstr( buff, "[account]" );
			if( pos1 != NULL )
			{
				pos2 = strchr( pos1, '=' );
				if( pos2 != NULL )
				{
					strLen = strlen(pos2+1);
					strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
					memcpy(sipCfg.account, pos2+1, strLen);
					sipCfg.account[strLen] = 0;
				}
				continue;
			}

			//password
			pos1 = strstr( buff, "[password]" );
			if( pos1 != NULL )
			{
				pos2 = strchr( pos1, '=' );
				if( pos2 != NULL )
				{
					strLen = strlen(pos2+1);
					strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
					memcpy(sipCfg.password, pos2+1, strLen);
					sipCfg.password[strLen] = 0;
				}
				continue;
			}
		}

		//monCode
		pos1 = strstr( buff, "[monCode]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				memcpy(sipCfg.monCode, pos2+1, strLen);
				sipCfg.monCode[strLen] = 0;
			}
			continue;
		}
		
		//callCode
		pos1 = strstr( buff, "[callCode]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				memcpy(sipCfg.callCode, pos2+1, strLen);
				sipCfg.callCode[strLen] = 0;
			}
			continue;
		}

		//serverIp
		pos1 = strstr( buff, "[serverIp]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				memcpy(sipCfg.serverIp, pos2+1, strLen);
				sipCfg.serverIp[strLen] = 0;
			}
			continue;
		}


		//VTK_AUTO_REG_SERVER_IPADD0
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IPADD0]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IPADD0 == NULL)
				{
					VTK_AUTO_REG_SERVER_IPADD0 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IPADD0 = realloc(VTK_AUTO_REG_SERVER_IPADD0, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IPADD0, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IPADD0[strLen] = 0;
			}
			continue;
		}
		
		//VTK_AUTO_REG_SERVER_IPADD1
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IPADD1]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IPADD1 == NULL)
				{
					VTK_AUTO_REG_SERVER_IPADD1 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IPADD1 = realloc(VTK_AUTO_REG_SERVER_IPADD1, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IPADD1, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IPADD1[strLen] = 0;
			}
			continue;
		}
		
		//VTK_AUTO_REG_SERVER_IPADD2
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IPADD2]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IPADD2 == NULL)
				{
					VTK_AUTO_REG_SERVER_IPADD2 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IPADD2 = realloc(VTK_AUTO_REG_SERVER_IPADD2, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IPADD2, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IPADD2[strLen] = 0;
			}
			continue;
		}

		//VTK_AUTO_REG_SERVER_IP0
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IP0]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IP0 == NULL)
				{
					VTK_AUTO_REG_SERVER_IP0 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IP0 = realloc(VTK_AUTO_REG_SERVER_IP0, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IP0, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IP0[strLen] = 0;
			}
			continue;
		}


		//VTK_AUTO_REG_SERVER_IP1
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IP1]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IP1 == NULL)
				{
					VTK_AUTO_REG_SERVER_IP1 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IP1 = realloc(VTK_AUTO_REG_SERVER_IP1, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IP1, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IP1[strLen] = 0;
			}
			continue;
		}


		//VTK_AUTO_REG_SERVER_IP2
		pos1 = strstr( buff, "[VTK_AUTO_REG_SERVER_IP2]" );
		if( pos1 != NULL )
		{
			pos2 = strchr( pos1, '=' );
			if( pos2 != NULL )
			{
				strLen = strlen(pos2+1);
				strLen = ((strLen > MAX_STR-1) ? MAX_STR-1 : strLen);
				if(VTK_AUTO_REG_SERVER_IP2 == NULL)
				{
					VTK_AUTO_REG_SERVER_IP2 = malloc(strLen+1);
				}
				else
				{
					VTK_AUTO_REG_SERVER_IP2 = realloc(VTK_AUTO_REG_SERVER_IP2, strLen+1);
				}
				memcpy(VTK_AUTO_REG_SERVER_IP2, pos2+1, strLen);
				VTK_AUTO_REG_SERVER_IP2[strLen] = 0;
			}
			continue;
		}
	}
	
	fclose(file);

	SipConfigLoadOver:

	if(VTK_AUTO_REG_SERVER_IPADD0 == NULL)
	{
		VTK_AUTO_REG_SERVER_IPADD0 = malloc(1);
		VTK_AUTO_REG_SERVER_IPADD0[0] = 0;
	}
	
	if(VTK_AUTO_REG_SERVER_IPADD1 == NULL)
	{
		VTK_AUTO_REG_SERVER_IPADD1 = malloc(1);
		VTK_AUTO_REG_SERVER_IPADD1[0] = 0;
	}
	
	if(VTK_AUTO_REG_SERVER_IPADD2 == NULL)
	{
		VTK_AUTO_REG_SERVER_IPADD2 = malloc(1);
		VTK_AUTO_REG_SERVER_IPADD2[0] = 0;
	}
	
	if(VTK_AUTO_REG_SERVER_IP0 == NULL)
	{
		VTK_AUTO_REG_SERVER_IP0 = malloc(1);
		VTK_AUTO_REG_SERVER_IP0[0] = 0;
	}
	
	if(VTK_AUTO_REG_SERVER_IP1 == NULL)
	{
		VTK_AUTO_REG_SERVER_IP1 = malloc(1);
		VTK_AUTO_REG_SERVER_IP1[0] = 0;
	}
	
	if(VTK_AUTO_REG_SERVER_IP2 == NULL ||strlen(VTK_AUTO_REG_SERVER_IP2)<3)
	{
		if(gotostart==0)
		{
			gotostart=1;
			remove(SIP_CONFIG_FILE_NAME);
			goto LoadSipStart;
		}
		VTK_AUTO_REG_SERVER_IP2 = malloc(1);
		VTK_AUTO_REG_SERVER_IP2[0] = 0;
	}
	
	SaveSipServerToServerIp();
	
	return ret;
		
}
int SaveSipPort(void)
{
#if !defined(PID_IXSE)
	printf(">>>>>set linphone sip port[%s]<<<<<<\n",sipCfg.port);
	API_linphonec_set_sip_port(atoi(sipCfg.port));
#endif	
}
//���õ�ǰ���԰�id��		
int SaveSipConfigFile(void)
{
	FILE	*file = NULL;
	char	buff[READ_BUFF_LEN+1] = {0};
	if(strlen(VTK_AUTO_REG_SERVER_IP2)==0||strlen(sipCfg.server)==0)
	{
		remove(SIP_CONFIG_FILE_NAME);
		sync();
		HardwareRestar();
		while(1);
	}
		
	if( (file=fopen(SIP_CONFIG_FILE_NAME,"w")) == NULL)
	{
		return -1;
	}

	snprintf( buff, READ_BUFF_LEN, "# sipcfg.cfg\r\n\r\n");
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[server]		= %s\r\n", sipCfg.server);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[port]		    = %s\r\n", sipCfg.port);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[divert]		= %s\r\n", sipCfg.divert);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[account]		= %s\r\n", sipCfg.account);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[password]	    = %s\r\n", sipCfg.password);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[monCode]		= %s\r\n", sipCfg.monCode);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[callCode]	    = %s\r\n", sipCfg.callCode);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[div_pwd]		= %s\r\n", sipCfg.divPwd);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[serverIp]	    = %s\r\n", sipCfg.serverIp);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD0]		= %s\r\n", VTK_AUTO_REG_SERVER_IPADD0);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD1]		= %s\r\n", VTK_AUTO_REG_SERVER_IPADD1);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD2]		= %s\r\n", VTK_AUTO_REG_SERVER_IPADD2);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP0]		    = %s\r\n", VTK_AUTO_REG_SERVER_IP0);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP1]		    = %s\r\n", VTK_AUTO_REG_SERVER_IP1);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP2]		    = %s\r\n", VTK_AUTO_REG_SERVER_IP2);
	fputs(buff,file);

	fclose(file);
	sync();
	
	// lzh_20201110_s
	SaveSipPort();
	// lzh_20201110_e	
	return 0;
}

int SaveSipDefaultConfigFile(SipCfg_T sipCfg_temp)
{
	FILE	*file = NULL;
	char	buff[READ_BUFF_LEN+1] = {0};
		
	if( (file=fopen(SIP_CONFIG_DEFAULT_FILE_NAME,"w")) == NULL)
	{
		return -1;
	}
	if(strcmp(sipCfg_temp.server,VTK_AUTO_REG_SERVER_IP2)!=0)
		parse_remote_server(sipCfg_temp.server, NULL, sipCfg_temp.serverIp);
	
	snprintf( buff, READ_BUFF_LEN, "# sipcfg.cfg\r\n\r\n");
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[server]		= %s\r\n", sipCfg_temp.server);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[port]		    = %s\r\n", sipCfg_temp.port);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[divert]		= %s\r\n", "test-id");
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[account]		= %s\r\n", "test-id");
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[password]	    = %s\r\n", "88888");
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[monCode]		= %s\r\n", sipCfg_temp.monCode);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[callCode]	    = %s\r\n", sipCfg_temp.callCode);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[div_pwd]		= %s\r\n", "88888");
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "[serverIp]	    = %s\r\n", sipCfg_temp.serverIp);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD0]		= %s\r\n", VTK_AUTO_REG_SERVER_IPADD0);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD1]		= %s\r\n", VTK_AUTO_REG_SERVER_IPADD1);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IPADD2]		= %s\r\n", sipCfg_temp.serverIp);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP0]		    = %s\r\n", VTK_AUTO_REG_SERVER_IP0);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP1]		    = %s\r\n", VTK_AUTO_REG_SERVER_IP1);
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "[VTK_AUTO_REG_SERVER_IP2]		    = %s\r\n", sipCfg_temp.server);
	fputs(buff,file);

	fclose(file);
	sync();
	
	// lzh_20201110_s
	SaveSipPort();
	// lzh_20201110_e	
	return 0;
}


void SipConfig_Init(void)
{
	if(LoadSipConfigFile() == 0)
	{
#if !defined(PID_IXSE)
		API_linphonec_set_sip_port(atoi(sipCfg.port));
		API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);		//czn_20181227
#endif		
	}
	else
	{
		//API_linphonec_quit();
		//API_linphonec_Register_SipCurAccount("000.000.000.000","0000","0000");
		printf("SipConfig_Init 4444444444444444444444444444444\n");
	}
}

// lzh_20180912_s
int Get_SipConfig_Account_info(char *server, char* divert_account, char* divert_passord, char* port )
{
	strncpy(server,sipCfg.serverIp,MAX_STR);
	strncpy(divert_account,sipCfg.divert,MAX_STR);
	strncpy(divert_account,sipCfg.divert,MAX_STR);
	strncpy(divert_passord,sipCfg.divPwd,MAX_STR);
	strncpy(port,sipCfg.port,MAX_STR);
	return 0;
}
// lzh_20180912_e

int Get_SipConfig_Account(char *paccount)
{
	strncpy(paccount,sipCfg.account,MAX_STR);
	return 0;
}

int Get_SipConfig_DirvertAccount(char *paccount)
{
	strncpy(paccount,sipCfg.divert,MAX_STR);
	return 0;
}
int Get_SipConfig_CallCode(char *paccount)
{
	strcpy(paccount,sipCfg.callCode);
	return 0;
}

int Get_SipConfig_MonCode(char *paccount)
{
	strcpy(paccount,sipCfg.monCode);
	return 0;
}

char* Get_SipConfig_Server(void)
{
	if(sipCfg.serverToIpFlag == 0)
	{
		return sipCfg.serverIp;
	}
	else
	{
		return sipCfg.server;
	}
}

int SipConfigRestoreDefault(void)
{
	uint8 tempChar[100];
	
	snprintf(tempChar, 100, "rm %s\n", SIP_CONFIG_FILE_NAME);
	system(tempChar);

	LoadSipConfigFile();

	api_get_default_sip_local_username(sipCfg.account);
	api_get_default_sip_local_password(sipCfg.password);
	api_get_default_sip_divert_username(sipCfg.divert);
	api_get_default_sip_divert_password(sipCfg.divPwd);
	SaveSipConfigFile();

	
}
void ReloadSipConfigFile(void)
{
	api_get_default_sip_local_username(sipCfg.account);
	api_get_default_sip_local_password(sipCfg.password);
	api_get_default_sip_divert_username(sipCfg.divert);
	api_get_default_sip_divert_password(sipCfg.divPwd);
	// lzh_20171025_s

	//auto_register_to_server(sipCfg.account, sipCfg.password, sipCfg.divert, sipCfg.divPwd);
	auto_clearul_to_server1(sipCfg.account, sipCfg.password);
	auto_clearul_to_server1(sipCfg.divert, sipCfg.divPwd);
	auto_clearul_to_server2(sipCfg.account, sipCfg.password);
	auto_clearul_to_server2(sipCfg.divert, sipCfg.divPwd);							
	auto_register_to_server1(sipCfg.account, sipCfg.password);
	auto_register_to_server1(sipCfg.divert, sipCfg.divPwd);														
	auto_register_to_server2(sipCfg.account, sipCfg.password);
	auto_register_to_server2(sipCfg.divert, sipCfg.divPwd);														
	// lzh_20171025_e

	
	LoadSipConfigFile();
	api_get_default_sip_local_username(sipCfg.account);
	api_get_default_sip_local_password(sipCfg.password);
	SaveSipConfigFile();
	API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);		//czn_20181227

	// lzh_20201110_s
	SaveSipPort();
	// lzh_20201110_e	
}

// lzh_20180727_s
int change_password_of_divert_account(void)
{
	int result;
	int i;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];
	

	for(i = 0; sipCfg.divPwd_change[i] != 0; i++)
	{
		if(sipCfg.divPwd_change[i] < '0' || sipCfg.divPwd_change[i] > '9')
		{
			BEEP_ERROR();
			return 0;
		}
	}
	
	API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

	parse_remote_server(VTK_AUTO_REG_SERVER_IP0, NULL, ipStr1);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP1, NULL, ipStr2);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP2, NULL, ipStr3);

	if( !strcmp(ipStr1, sipCfg.serverIp) || 
	    !strcmp(ipStr2, sipCfg.serverIp) ||
	    !strcmp(ipStr3, sipCfg.serverIp) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP0, sipCfg.server) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP1, sipCfg.server) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP2, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD0, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD1, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD2, sipCfg.server)
	)
	{
		result = remote_account_manage( ACCOUNT_MANAGE_CMD_CHANGEPSW, sipCfg.serverIp,sipCfg.divert,sipCfg.divPwd,sipCfg.divPwd_change);
		if( result == 0 )
		{
			strcpy( sipCfg.divPwd, sipCfg.divPwd_change );
			printf("change_password_of_divert_account ok! pwd = %s, changePwd = %s\n", sipCfg.divPwd, sipCfg.divPwd_change);
			BEEP_CONFIRM();
		}
		else
		{
			BEEP_ERROR();
			printf("change_password_of_divert_account fail! pwd = %s, changePwd = %s\n", sipCfg.divPwd, sipCfg.divPwd_change);		
		}
	}
	// ���˼ҵķ���������
	else
	{
		strcpy( sipCfg.divPwd, sipCfg.divPwd_change );
	}

	API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	return 1;
}

//IO�����ָ�����Ҫ������ʾ
int change_password_of_divert_account2(void)
{
	int result;
	int i;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];
	

	for(i = 0; sipCfg.divPwd_change[i] != 0; i++)
	{
		if(sipCfg.divPwd_change[i] < '0' || sipCfg.divPwd_change[i] > '9')
		{
			return -1;
		}
	}
	
	parse_remote_server(VTK_AUTO_REG_SERVER_IP0, NULL, ipStr1);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP1, NULL, ipStr2);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP2, NULL, ipStr3);

	if( !strcmp(ipStr1, sipCfg.serverIp) || 
	    !strcmp(ipStr2, sipCfg.serverIp) ||
	    !strcmp(ipStr3, sipCfg.serverIp) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP0, sipCfg.server) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP1, sipCfg.server) ||
	    !strcmp(VTK_AUTO_REG_SERVER_IP2, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD0, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD1, sipCfg.server) ||
		!strcmp(VTK_AUTO_REG_SERVER_IPADD2, sipCfg.server)
	)
	{
		result = remote_account_manage( ACCOUNT_MANAGE_CMD_CHANGEPSW, sipCfg.serverIp,sipCfg.divert,sipCfg.divPwd,sipCfg.divPwd_change);
		if( result == 0 )
		{
			strcpy( sipCfg.divPwd, sipCfg.divPwd_change );
			printf("change_password_of_divert_account ok! pwd = %s, changePwd = %s\n", sipCfg.divPwd, sipCfg.divPwd_change);
		}
		else
		{
			printf("change_password_of_divert_account fail! pwd = %s, changePwd = %s\n", sipCfg.divPwd, sipCfg.divPwd_change);		
		}
	}
	// ���˼ҵķ���������
	else
	{
		strcpy( sipCfg.divPwd, sipCfg.divPwd_change );
	}

	return result;
}
// lzh_20180727_e

int SaveSipServerToServerIp(void)
{
	if(sipCfg.serverToIpFlag != 0)
	{
		sipCfg.serverToIpFlag = parse_remote_server(sipCfg.server, NULL, sipCfg.serverIp);
		
		if(sipCfg.serverToIpFlag == 0)
		{
			SaveSipConfigFile();
		}
	}
	
	return sipCfg.serverToIpFlag;
}

void AutoRegistration(void)		//czn_20181227
{
	SaveSipServerToServerIp();
	if(Get_SipAccount_State() != 1)
	{
		API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);		
	}
}


int SaveSipServer(void)
{
	API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	sipCfg.serverToIpFlag = -1;
	SaveSipServerToServerIp();
	API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

	return 1;
}


void create_ixds_qrcode(char index,char *ds_addr,char*ds_name,char *qr_code)
{
	char type[5];
	int ds_index;
	if(memcmp(ds_addr,"00000000",8)==0)
	{
		strcpy(type,"CS");
		ds_index=atoi(ds_addr+8);
	}
	else if(memcmp(ds_addr+4,"0000",4)==0)
	{
		strcpy(type,"DS");
		ds_index=atoi(ds_addr+8);
	}
	else
	{
		strcpy(type,"OS");
		ds_index=atoi(ds_addr+8)-50;
	}
	snprintf(qr_code, 200, "<item>%d,%s,%s,%s,%s%02d</item>",  index, ds_name,sipCfg.account, "11", type,ds_index);
}
// lzh_20180804_s
char* create_sipcfg_qrcode(void)
{
	int i;
	MonRes_Entry_Stru data;
	int len, tempLen, maxLen;
	char QRcodeList[200];

	sipCfg.qrcode[0] = 0;

	snprintf(QRcodeList,200,"%s%s%s","<TYPE>","IX","</TYPE>");
	strcat(sipCfg.qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<VER>","2.0","</VER>");
	strcat(sipCfg.qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<USER>",sipCfg.divert,"</USER>");
	strcat(sipCfg.qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW>",sipCfg.divPwd,"</PSW>");	
	strcat(sipCfg.qrcode, QRcodeList);
	
	//snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sipCfg.serverToIpFlag == 0 ? sipCfg.serverIp : sipCfg.server,"</DOMAIN>");	
	snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sipCfg.server,"</DOMAIN>");
	strcat(sipCfg.qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<HOME ID>",sipCfg.account,"</HOME ID>");		
	strcat(sipCfg.qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW2>",sipCfg.password,"</PSW2>");	
	strcat(sipCfg.qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<CALL CODE>",sipCfg.callCode,"</CALL CODE>");
	strcat(sipCfg.qrcode, QRcodeList);

	strcat(sipCfg.qrcode, "<string-array name=\"List\">");

	len = strlen(sipCfg.qrcode);
	maxLen = 2000-strlen("</string-array>");
	#if	(!defined(PID_DX470))&&(!defined(PID_DX482))
	//#ifndef PID_DX470
	// zfz_20190617 start
	//if(Get_MonRes_Num() == 0)
	if(Get_MonRes_Num() == 0 && Get_MonRes_Temp_Table_Num() == 0)
	{
		AddMonRes_Temp_TableBySearching();

		//printf( "Get_MonRes_Temp_Table_Num() = %d\n", Get_MonRes_Temp_Table_Num() );
		for(i = 0; i < Get_MonRes_Temp_Table_Num(); i++)
		{
			get_monres_temp_table_record_items(i, &data);
			//snprintf(QRcodeList, 200, "<item>%d,%s,%s,%s,%s,%s</item>", i+1, strcmp(data.R_Name, "-") ? data.R_Name : data.res_name, data.localSip, "11", data.monCode, data.monPwd);
			create_ixds_qrcode(i+1,data.BD_RM_MS,strcmp(data.R_Name, "-") ? data.R_Name : data.res_name,QRcodeList);
			tempLen = strlen(QRcodeList);
			if(len + tempLen < maxLen)
			{
				strcat(sipCfg.qrcode, QRcodeList);
				len += tempLen;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		for(i = 0; i < Get_MonRes_Num(); i++)
		{
			get_monres_table_record_items(i, &data);
			//snprintf(QRcodeList, 200, "<item>%d,%s,%s,%s,%s,%s</item>", i+1, strcmp(data.R_Name, "-") ? data.R_Name : data.res_name, data.localSip, "11", data.monCode, data.monPwd);
			
			create_ixds_qrcode(i+1,data.BD_RM_MS,strcmp(data.R_Name, "-") ? data.R_Name : data.res_name,QRcodeList);
			tempLen = strlen(QRcodeList);
			if(len + tempLen < maxLen)
			{
				strcat(sipCfg.qrcode, QRcodeList);
				len += tempLen;
			}
			else
			{
				break;
			}
		}
		for(; i < Get_MonRes_Temp_Table_Num()+Get_MonRes_Num(); i++)
		{
			get_monres_temp_table_record_items(i-Get_MonRes_Num(), &data);
			//snprintf(QRcodeList, 200, "<item>%d,%s,%s,%s,%s,%s</item>", i+1, strcmp(data.R_Name, "-") ? data.R_Name : data.res_name, data.localSip, "11", data.monCode, data.monPwd);
			create_ixds_qrcode(i+1,data.BD_RM_MS,strcmp(data.R_Name, "-") ? data.R_Name : data.res_name,QRcodeList);
			tempLen = strlen(QRcodeList);
			if(len + tempLen < maxLen)
			{
				strcat(sipCfg.qrcode, QRcodeList);
				len += tempLen;
			}
			else
			{
				break;
			}
		}
		API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
	}
	// end
	#endif
	strcat(sipCfg.qrcode, "</string-array>");

	return sipCfg.qrcode;
}

char* create_dx2_sipcfg_qrcode(void)
{
	char QRcodeList[200];
	//char sip_ser[80];
	//char master_acc[40];
	//char master_pwd[40];
	//char im_pwd[40];
	//char im_acc[40];
	//get_sip_master_ser(sip_ser);
	//get_sip_master_acc(master_acc);
	//get_sip_master_pwd(master_pwd);
	//if(get_sip_im_acc_from_tb(im_addr,im_acc,im_pwd)<0)
	//	return NULL;
	
	//char *qrcode= malloc(2000);
	//if(qrcode==NULL)
	//	return  NULL;
	char *qrcode=NULL;
	qrcode=sipCfg.qrcode;

	qrcode[0] = 0;

	snprintf(QRcodeList,200,"%s%s%s","<TYPE>","IX","</TYPE>");
	strcat(qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<VER>","2.0","</VER>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<USER>",sipCfg.divert,"</USER>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW>",sipCfg.divPwd,"</PSW>");	
	strcat(qrcode, QRcodeList);
	
	//snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sipCfg.serverToIpFlag == 0 ? sipCfg.serverIp : sipCfg.server,"</DOMAIN>");	
	snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sipCfg.server,"</DOMAIN>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<HOME ID>",sipCfg.account,"</HOME ID>");		
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW2>",sipCfg.password,"</PSW2>");	
	strcat(qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<CALL CODE>",sipCfg.callCode,"</CALL CODE>");
	strcat(qrcode, QRcodeList);

	strcat(qrcode, "<string-array name=\"List\">");

	//len = strlen(qrcode);
	//maxLen = 2000-strlen("</string-array>");
	int ds_index;
	char ds_name[41];
	char ds_addr[10];
	char mon_code[10];
	for(ds_index=0;ds_index<Get_DxMonRes_Num();ds_index++)
	{
		Get_DxMonRes_Record2(ds_index,ds_name,ds_addr);
		snprintf(QRcodeList, 200, "<item>%d,%s,%s,%s,%s</item>",  ds_index+1, ds_name,sipCfg.account, "11", ds_addr);
		strcat(qrcode, QRcodeList);
	}
	
	strcat(qrcode, "</string-array>");

	return qrcode;
}

int SipConfigManualRegistration(void)
{
	int ret;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];
	
	parse_remote_server(VTK_AUTO_REG_SERVER_IP0, NULL, ipStr1);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP1, NULL, ipStr2);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP2, NULL, ipStr3);
	
	dprintf("serverIp=%s, account=%s, password=%s, divert=%s, divPwd=%s\n", sipCfg.serverIp, sipCfg.account, sipCfg.password, sipCfg.divert, sipCfg.divPwd);
	
	if(!strcmp(ipStr1, sipCfg.serverIp) || 
	   !strcmp(ipStr2, sipCfg.serverIp) ||
	   !strcmp(ipStr3, sipCfg.serverIp))
	{
		ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.account, sipCfg.password,NULL );
		ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.divert, sipCfg.divPwd,NULL );
	}
	else
		ret = -1;	// other server apply
	
	API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);

	return ret;
}

int SipConfigUseDefault(void)
{
	int ret;
	
	api_get_default_sip_local_username(sipCfg.account);
	api_get_default_sip_local_password(sipCfg.password);
	api_get_default_sip_divert_username(sipCfg.divert);
	api_get_default_sip_divert_password(sipCfg.divPwd);
	
	strcpy(sipCfg.server, VTK_AUTO_REG_SERVER_IP2);
	sipCfg.serverToIpFlag = -1;
	SaveSipServerToServerIp();
	strcpy(sipCfg.monCode, "1000");
	strcpy(sipCfg.callCode, "2000");
	strcpy(sipCfg.port,"5069");

	SaveSipConfigFile();	// lzh_20180112
	
	// lzh_20180802

	clear_locations_to_server0(sipCfg.account, sipCfg.password);
	clear_locations_to_server0(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server0(sipCfg.account, sipCfg.password);
	restore_default_psw_to_server0(sipCfg.divert, sipCfg.divPwd);

	clear_locations_to_server1(sipCfg.account, sipCfg.password);
	clear_locations_to_server1(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server1(sipCfg.account, sipCfg.password);
	restore_default_psw_to_server1(sipCfg.divert, sipCfg.divPwd);

	clear_locations_to_server2(sipCfg.account, sipCfg.password);
	clear_locations_to_server2(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server2(sipCfg.account, sipCfg.password);
	ret = restore_default_psw_to_server2(sipCfg.divert, sipCfg.divPwd);
	// 4
	API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);

	
	if( (ret == 1) || (ret == 0) )	// ok :  �������˺�, �ָ���ȱʡ����
	{
		ret = 0;
	}
	else if(ret == -2)		// �˺�������ͬ���Ѵ�����
	{
		ret = 0;
	}
	else
	{
		ret = -1;
	}
	//czn_20181227
}

SipCfg_T* GetSipConfig(void)
{
	return &sipCfg;
}
