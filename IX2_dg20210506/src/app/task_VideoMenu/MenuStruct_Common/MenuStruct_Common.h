#ifndef MenuStruct_Common_h
#define MenuStruct_Common_h

#if defined(PID_IX850)
#define MENU_NM_MAIN 			MENU_097_NM_MAIN
#define MENU_NM_LanSetting		MENU_098_NM_LanSetting
#define MENU_NM_WlanSetting		MENU_099_NM_WlanSetting
#else
#define MENU_NM_MAIN 			MENU_125_NM_MAIN
#define MENU_NM_LanSetting		MENU_126_NM_LanSetting
#define MENU_NM_WlanSetting		MENU_127_NM_WlanSetting
#define MENU_NM_WifiInfo		MENU_128_NM_WifiInfo
#endif


#endif