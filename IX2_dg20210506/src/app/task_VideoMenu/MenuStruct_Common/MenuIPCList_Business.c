#include "MENU_public.h"

one_vtk_table* ipcMonListTable = NULL;
#if defined(PID_IX850)
int ipcDhcpEnable=1;
#else
extern int ipcDhcpEnable;
#endif
void IPC_MonRes_Table_Init(void)
{
	char temp[20];
	
	API_Event_IoServer_InnerRead_All(IPC_DHCP_Enable, temp);
	ipcDhcpEnable = atoi(temp) ? 1 : 0;
	
	ipcMonListTable = load_vtk_table_file(IPC_MON_LIST_FILE_NAME);
	if(ipcMonListTable == NULL)
	{
		ipcMonListTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &ipcMonListTable->lock, 0);		//czn_20190327
		ipcMonListTable->keyname_cnt = 7;
		ipcMonListTable->pkeyname_len = malloc(ipcMonListTable->keyname_cnt*sizeof(int));
		ipcMonListTable->pkeyname = malloc(ipcMonListTable->keyname_cnt*sizeof(one_vtk_dat));
		ipcMonListTable->pkeyname_len[0] = 40;
		ipcMonListTable->pkeyname_len[1] = 40;
		ipcMonListTable->pkeyname_len[2] = 40;
		ipcMonListTable->pkeyname_len[3] = 1;
		ipcMonListTable->pkeyname_len[4] = 100;
		ipcMonListTable->pkeyname_len[5] = 40;
		ipcMonListTable->pkeyname_len[6] = 1;
		ipcMonListTable->pkeyname[0].len = strlen("IPC_NAME");
		ipcMonListTable->pkeyname[0].pdat = malloc(ipcMonListTable->pkeyname[0].len);
		memcpy(ipcMonListTable->pkeyname[0].pdat, "IPC_NAME", ipcMonListTable->pkeyname[0].len);
		
		ipcMonListTable->pkeyname[1].len = strlen("USER_NAME");
		ipcMonListTable->pkeyname[1].pdat = malloc(ipcMonListTable->pkeyname[1].len);
		memcpy(ipcMonListTable->pkeyname[1].pdat, "USER_NAME", ipcMonListTable->pkeyname[1].len);

		ipcMonListTable->pkeyname[2].len = strlen("USER_PWD");
		ipcMonListTable->pkeyname[2].pdat = malloc(ipcMonListTable->pkeyname[2].len);
		memcpy(ipcMonListTable->pkeyname[2].pdat, "USER_PWD", ipcMonListTable->pkeyname[2].len);

		ipcMonListTable->pkeyname[3].len = strlen("CHANNEL");
		ipcMonListTable->pkeyname[3].pdat = malloc(ipcMonListTable->pkeyname[3].len);
		memcpy(ipcMonListTable->pkeyname[3].pdat, "CHANNEL", ipcMonListTable->pkeyname[3].len);

		ipcMonListTable->pkeyname[4].len = strlen("DEVICE_URL");
		ipcMonListTable->pkeyname[4].pdat = malloc(ipcMonListTable->pkeyname[4].len);
		memcpy(ipcMonListTable->pkeyname[4].pdat, "DEVICE_URL", ipcMonListTable->pkeyname[4].len);

		ipcMonListTable->pkeyname[5].len = strlen("IPC_ID");
		ipcMonListTable->pkeyname[5].pdat = malloc(ipcMonListTable->pkeyname[5].len);
		memcpy(ipcMonListTable->pkeyname[5].pdat, "IPC_ID", ipcMonListTable->pkeyname[5].len);

		ipcMonListTable->pkeyname[6].len = strlen("FAVERITE");
		ipcMonListTable->pkeyname[6].pdat = malloc(ipcMonListTable->pkeyname[6].len);
		memcpy(ipcMonListTable->pkeyname[6].pdat, "FAVERITE", ipcMonListTable->pkeyname[6].len);

		ipcMonListTable->record_cnt = 0;
		
		save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);

		//printf_one_table(ipcMonListTable);
	}
}

int Get_IPC_MonRes_Num(void)
{
	if(ipcMonListTable == NULL)
	{
		return 0;
	}

	return ipcMonListTable->record_cnt;
}

//dh_20191204_s
int Get_IPC_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_IPC_MonRes_Num(); i++)
	{
		Get_IPC_MonRes_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

//czn_20190221_s
void IPC_MonRes_Reload(void)
{
	free_vtk_table_file_buffer(ipcMonListTable);
	IPC_MonRes_Table_Init();
}
//czn_20190221_e
int Get_IPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(ipcMonListTable == NULL || ipcMonListTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(ipcMonListTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	//printf("Get_IPC_MonRes_Record index=%d\n", index);
	//printf_one_table(ipcMonListTable);
	
	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);	

	#if 0
	memcpy(recordBuffer, pRecord->pdat, pRecord->len);

	//printf("Get_IPC_MonRes_Record = %s\n", recordBuffer);
	
	strcpy(strTable[i++], strtok(recordBuffer,","));
	while(pos1 = strtok(NULL,",")) strcpy(strTable[i++], pos1);
	
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#endif
	return 0;
}

int Modify_IPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	//snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id);
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	//printf("Modify_IPC_MonRes_Record = %s\n", data);
	Modify_one_vtk_record(&record, ipcMonListTable, index);
	
	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Delete_IPC_MonRes_Record(int index)
{
	delete_one_vtk_record(ipcMonListTable, index);

	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Add_IPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	//snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id);
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(ipcMonListTable, &record);

	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Add_Or_Modify_IPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	int i;
	IPC_RECORD record;

	for(i = 0; i < Get_IPC_MonRes_Num(); i++)
	{
		Get_IPC_MonRes_Record(i, &record);

		if(ipcDhcpEnable)
		{
			if(!strcmp(record.id, ipcRecord->id))
			{
				Modify_IPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
		else
		{
			if(!strcmp(record.devUrl, ipcRecord->devUrl))
			{
				Modify_IPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
	}
	
	Add_IPC_MonRes_Record(ipcRecord);
	
	return 0;
}

int Clear_IPC_MonRes_Record(void)
{
	int i;
	
	for(i=0; i<ipcMonListTable->record_cnt; i++)
	{
		if(ipcMonListTable->precord[i].pdat != NULL)
		{
			free(ipcMonListTable->precord[i].pdat);
			ipcMonListTable->precord[i].pdat = NULL;
		}
	}
	
	release_one_vtk_tabl(ipcMonListTable);
	remove(IPC_MON_LIST_FILE_NAME);
	
	IPC_MonRes_Table_Init();
	
	save_vtk_table_file_buffer(ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);

	return 0;
}

one_vtk_table* ipcTempTable = NULL;


void IPC_Temp_Table_Delete(void)
{
	free_vtk_table_file_buffer(ipcTempTable);
	ipcTempTable = NULL;
}

void IPC_Temp_Table_Create(void)
{
	char temp[20];
	
	if(ipcTempTable == NULL)
	{
		ipcTempTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &ipcTempTable->lock, 0);
		ipcTempTable->keyname_cnt = 7;
		ipcTempTable->pkeyname_len = malloc(ipcTempTable->keyname_cnt*sizeof(int));
		ipcTempTable->pkeyname = malloc(ipcTempTable->keyname_cnt*sizeof(one_vtk_dat));
		ipcTempTable->pkeyname_len[0] = 40;
		ipcTempTable->pkeyname_len[1] = 40;
		ipcTempTable->pkeyname_len[2] = 40;
		ipcTempTable->pkeyname_len[3] = 1;
		ipcTempTable->pkeyname_len[4] = 100;
		ipcTempTable->pkeyname_len[5] = 40;
		ipcTempTable->pkeyname_len[6] = 1;
		ipcTempTable->pkeyname[0].len = strlen("IPC_NAME");
		ipcTempTable->pkeyname[0].pdat = malloc(ipcTempTable->pkeyname[0].len);
		memcpy(ipcTempTable->pkeyname[0].pdat, "IPC_NAME", ipcTempTable->pkeyname[0].len);
		
		ipcTempTable->pkeyname[1].len = strlen("USER_NAME");
		ipcTempTable->pkeyname[1].pdat = malloc(ipcTempTable->pkeyname[1].len);
		memcpy(ipcTempTable->pkeyname[1].pdat, "USER_NAME", ipcTempTable->pkeyname[1].len);

		ipcTempTable->pkeyname[2].len = strlen("USER_PWD");
		ipcTempTable->pkeyname[2].pdat = malloc(ipcTempTable->pkeyname[2].len);
		memcpy(ipcTempTable->pkeyname[2].pdat, "USER_PWD", ipcTempTable->pkeyname[2].len);

		ipcTempTable->pkeyname[3].len = strlen("CHANNEL");
		ipcTempTable->pkeyname[3].pdat = malloc(ipcTempTable->pkeyname[3].len);
		memcpy(ipcTempTable->pkeyname[3].pdat, "CHANNEL", ipcTempTable->pkeyname[3].len);

		ipcTempTable->pkeyname[4].len = strlen("DEVICE_URL");
		ipcTempTable->pkeyname[4].pdat = malloc(ipcTempTable->pkeyname[4].len);
		memcpy(ipcTempTable->pkeyname[4].pdat, "DEVICE_URL", ipcTempTable->pkeyname[4].len);

		ipcTempTable->pkeyname[5].len = strlen("IPC_ID");
		ipcTempTable->pkeyname[5].pdat = malloc(ipcTempTable->pkeyname[5].len);
		memcpy(ipcTempTable->pkeyname[5].pdat, "IPC_ID", ipcTempTable->pkeyname[5].len);
		
		ipcTempTable->pkeyname[6].len = strlen("FAVERITE");
		ipcTempTable->pkeyname[6].pdat = malloc(ipcTempTable->pkeyname[6].len);
		memcpy(ipcTempTable->pkeyname[6].pdat, "FAVERITE", ipcTempTable->pkeyname[6].len);

		ipcTempTable->record_cnt = 0;
	}
}

int Get_IPC_Temp_Num(void)
{
	if(ipcTempTable == NULL)
	{
		return 0;
	}

	return ipcTempTable->record_cnt;
}

int Get_IPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(ipcTempTable == NULL || ipcTempTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(ipcTempTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#if 0
	memcpy(recordBuffer, pRecord->pdat, pRecord->len);

	//printf("Get_IPC_MonRes_Record = %s\n", recordBuffer);
	
	strcpy(strTable[i++], strtok(recordBuffer,","));
	while(pos1 = strtok(NULL,",")) strcpy(strTable[i++], pos1);
	
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#endif
	return 0;
}

int Add_IPC_Temp_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(ipcTempTable, &record);
	return 0;
}

//dh_20191204_s
int Get_IPC_Temp_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_IPC_Temp_Num(); i++)
	{
		Get_IPC_Temp_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

int Modify_IPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	//printf("Modify_IPC_MonRes_Record = %s\n", data);
	Modify_one_vtk_record(&record, ipcTempTable, index);
	
	return 0;
}
