#include "MENU_public.h"
#include "cJSON.h"
#include "MenuUdpResDownload_Business.h"

char* CreateResUdpDownloadJsonData(RES_UDP_DOWNLOAD_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "server", data->server);
	cJSON_AddStringToObject(root, "resId", data->resId);
	cJSON_AddStringToObject(root, "fileName", data->fileName);
	cJSON_AddNumberToObject(root, "fileSize", data->fileSize);
	cJSON_AddNumberToObject(root, "downloadFileSize", data->downloadFileSize);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

int ParseResUdpDownloadReqJsonData(const char* json, RES_UDP_DOWNLOAD_DATA_T* pData)
{
    int status = 0;
	

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "server", pData->server, 40);
	ParseJsonString(root, "resId", pData->resId, 4);
	ParseJsonString(root, "fileName", pData->fileName, 40);
	ParseJsonNumber(root, "fileSize", &(pData->fileSize));
	ParseJsonNumber(root, "downloadFileSize", &(pData->downloadFileSize));

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}
