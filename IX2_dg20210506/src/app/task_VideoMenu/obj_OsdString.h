/**
  ******************************************************************************
  * @file    obj_String.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the obj_String.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef OBJ_OSD_STRING_H
#define OBJ_OSD_STRING_H

/*
#define OSD_String(x,y,color,pstr) \
	T107_DisplayString(0,1,x,y,color,pstr)

#define OSD_String_Rom(x,y,color,pstr) \
	T107_DisplayString(1,1,x,y,color,pstr)
*/

void OSD_StringDisplay(int uCol,int uRow,int uColor,int uBgColor, const unsigned char *ptrStr);
//void OSD_StringClear(int uCol,int uRow,int uLen);
void OSD_StringClear(int uCol,int uRow,const unsigned char *ptrStr);


//void OSD_StringDisplayExt(int x,int y,int uColor, int uBgColor, const unsigned char *ptrStr, int str_len, int fnt_type, int format, int width);

void OSD_StringClearExt(int x,int y, int width, int heigh);

void OSD_StringDisplayWithID(int x,int y,int uColor, int uBgColor, int str_id, int fnt_type, int width);
void OSD_StringDisplayWithID_Str(int x,int y,int uColor, int uBgColor, int str_id, int fnt_type, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, int width);
void OSD_StringDisplayWithIcon(int x,int y,int uColor, int uBgColor, int icon, int fnt_type, int width);

#if 0
void OSD_StringDisplay2(unsigned char uCol,unsigned char uRow,unsigned char uColor,const unsigned char *ptrStr,unsigned char size, unsigned char gap );
void OSD_StringClear2(unsigned char uCol,unsigned char uRow,unsigned char uLen,unsigned char size, unsigned char gap );
#endif

#endif

