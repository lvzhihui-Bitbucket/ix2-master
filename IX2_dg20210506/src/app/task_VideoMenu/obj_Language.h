/**
  ******************************************************************************
  * @file    obj_Language.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power obj_Language.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#ifndef OBJ_LANGUAGE_H
#define OBJ_LANGUAGE_H

/*******************************************************************************
                         Define object property
*******************************************************************************/
#define	MAX_LANGUAGE_NUMS	16
//单个语言包表示结构

typedef struct
{
    unsigned char UsableNum;
    unsigned char CurSelNum;
    unsigned char LanguageIndex[MAX_LANGUAGE_NUMS];
}LanguageType;


/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
void LanguageInit(void);
unsigned char GetMaxUsableLanguageNum(void);
unsigned char GetCurLanguage(void);
unsigned char GetCurLanguageIndex(unsigned char cur);
unsigned char SetCurLanguage(unsigned char LangIndex);
/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/






#endif
