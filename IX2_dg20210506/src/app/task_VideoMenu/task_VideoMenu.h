
#ifndef _GUI_VIDEO_MENU_H
#define  _GUI_VIDEO_MENU_H

#include "task_survey.h"

#include "menu_resource.h"

#include "obj_Menu.h"
#include "obj_Video.h"
#include "obj_ImageAdjust.h"
#include "obj_OsdString.h"
#include "obj_OsdLabel.h"
#include "obj_OsdExtLabel.h"
#include "obj_OsdDateTime.h"
#include "obj_OsdTimeLapse.h"
#include "obj_menu_data.h"

#include "obj_LogView.h"
#include "define_file.h"

extern Loop_vdp_common_buffer	vdp_video_menu_mesg_queue;

typedef enum
{
    MSG_VIDEO = 0,
    MSG_MENU,
    MSG_CURSOR,
    MSG_LABEL,
    MSG_LABEL_XY,
    MSG_STRING,
    MSG_STRING_EXT,
    MSG_TIME_LAPSE,
    MSG_DATE_TIME,
	MSG_EXT_LABEL,
	//
	MSG_ICON_PRESS,
	MSG_ICON_RELEASE,
	MSG_SPRITE,
	//
	MSG_LOGVIEW,
	// lzh_20161230_s
	MSG_MENU2,
	// lzh_20161230_e
	MSG_ICON_DISP_ON,
	MSG_ICON_DISP_OFF,
	// lzh_20170630
	MSG_UPDATE_ENABLE,
	MSG_UPDATE_DISABLE,
	MSG_DISP_BKGD,
	MSG_GET_ICON_POS,
	MSG_MENU_SWITCH,
	MSG_MENU_CLOSE,
	// lzh_20181016_s
	MSG_MENU_DISP_CACHE,
	// lzh_20181016_e	
	MSG_MENU_ThreadHeartReq,
	MSG_STRING_EXT2,
	MSG_PROG_BAR,

	MSG_FILL_COLOR,
	MSG_STRING_EXT_MULTI_LINE,
	MSG_UPDATE_REGION,
	MSG_SPRITE_MULTI,
	MSG_MENU_DISP_RM,

	
} MsgVideoMenuType;

#define LOG_VIEW_TYPE_TURN_OFF		0
#define LOG_VIEW_TYPE_TURN_ON		1
#define LOG_VIEW_TYPE_ADD_LOG_DATA	2
#define LOG_VIEW_TYPE_CLEAR_WIN		3	//czn_20190412

//#define LOGO_FILENAME		"/mnt/nand1-2/Customerized/UI/Logo.bmp"
//#define LOGO_V_FILENAME		"/mnt/nand1-2/Customerized/UI/Logo_V.bmp"

#pragma pack(1)

// lzh_20161103_s
typedef struct
{
	VDP_MSG_HEAD	head;	
	VideoOptType 	opt;
	int				win;
} video_type;
// lzh_20161103_e

typedef struct
{
	VDP_MSG_HEAD	head;	
	unsigned char	menu_id;
	unsigned char 	state;
	// lzh_20161230_s
	unsigned char	sub_menu_id;
	// lzh_20161230_e
} video_menu_type;

#define MAX_STR_DISP_LEN	250
#define MAX_STR_DISP_LINE_MAX	20

typedef struct
{
	VDP_MSG_HEAD	head;
	int				x;
	int				y;
	int				width;
	int				height;
	int				layerId;	
	int				fg;	
	int				bg;
	int				format;
	int				fnt_type;
	int				str_id;
	int				pre_str_len;
	int				lst_str_len;
	int				str_len;
	char			str_dat[MAX_STR_DISP_LEN];
} string_type;

typedef struct
{
	VDP_MSG_HEAD	head;
	int				bkgdcnt;
	int				x;
	int				y;
} sprite_type;

typedef struct
{
	VDP_MSG_HEAD	head;
	LableType		type;
	int				x;
	int				y;
	int				fg;	
	int				bg;
} Label_type;

//cao_20151118
typedef struct
{
	VDP_MSG_HEAD	head;
	MsgDateTimeType 	control;
} DateTime_type;

typedef struct
{
	VDP_MSG_HEAD	head;
	TimeLapseMode 	control;
	unsigned int	inittime;
} TimeLapse_type;


#define LOG_VIEW_MAX_STR	(MAX_STR_DISP_LEN+1)
typedef struct
{
	VDP_MSG_HEAD	head;
	int				str_len;
	char			str_dat[LOG_VIEW_MAX_STR];
} LogView_type;

typedef struct
{
	VDP_MSG_HEAD	head;
	uint16 			icon;
	POS 			pos;
} GetIconPos_type;

typedef struct
{
	VDP_MSG_HEAD	head;
	int uMenu;
	int subMenu;
	int pushstack;
} MenuSwitchType;

// lzh_20181016_s
typedef struct
{
	VDP_MSG_HEAD	head;
	int menu_id;
	int layer_id;
	int cache_num;
	int x;
	int y;
	int width;
	int height;
} menu_disp_cache_type;
// lzh_20181016_e

typedef struct
{
	VDP_MSG_HEAD	head;
	int				x;
	int				y;
	int 			width;
	int 			height;
	int				bg;
} progbar_type;


typedef struct
{
	VDP_MSG_HEAD	head;
	int				layerId;
	int				x;
	int				y;
	int				xsize;
	int				ysize;
	int				color;	
	int				option;	
	int 			alpha;
}MenuFillColorMsg_t;


typedef struct
{
	int				x;
	int				y;
	int				fg;	
	int 			uBgColor;
	int				width;
	int				height;
	int 			format;
	int				fnt_type;
	int				align;
	int				str_len;
	char			str_dat[MAX_STR_DISP_LEN];
}MenuOneString_T;

typedef struct
{
	int 				dataCnt;
	MenuOneString_T 	data[MAX_STR_DISP_LINE_MAX];
}MenuStrMulti_T;


typedef struct
{
	VDP_MSG_HEAD	head;
	MenuStrMulti_T	list;
}MenuStrMultiMsg_t;

typedef struct
{
	VDP_MSG_HEAD	head;
	int				x;
	int				y;
	int				width;
	int				height;
	int				enable;
	int				alpha;
} MenuUpdateRegion_T;

typedef struct
{
	int				x;
	int				y;
	int				width;
	int				height;
	int				layerId;
	int				spriteId;
	int				align;
	int				enable;
	int 			alpha;
}MenuOneSprite_T;

typedef struct
{
	int 				dataCnt;
	MenuOneSprite_T 	data[MAX_STR_DISP_LINE_MAX];
}MenuSpriteMulti_T;

typedef struct
{
	int time;
	char msg[450];
}Ring2Msg_t;

typedef struct
{
	VDP_MSG_HEAD		head;
	MenuSpriteMulti_T	sprite;
}MenuSpriteMultiMsg_t;

#define MAX_RM_CMD_LEN  64
typedef struct
{
	//��Ϣͷ
	VDP_MSG_HEAD	head;
	unsigned char disp_buf[MAX_RM_CMD_LEN+1];
} MsgRmStruct;


#pragma pack()


#define MSG_7_BRD_SUB_WIFI_OPEN				0x01		//wifi��
#define MSG_7_BRD_SUB_WIFI_CLOSE			0x02		//wifi�ر�
#define MSG_7_BRD_SUB_WIFI_DISCONNECT		0x03		//wifi�Ͽ�
#define MSG_7_BRD_SUB_WIFI_CONNECTING		0x04		//wifi��������
#define MSG_7_BRD_SUB_WIFI_CONNECTED		0x05		//wifi�������
#define MSG_7_BRD_SUB_WIFI_SEARCHING		0x06		//wifi��������
#define MSG_7_BRD_SUB_WIFI_SEARCH_OVER		0x07		//wifi��������

#define MSG_7_BRD_SUB_RECORD_NOTICE			0x08		// ¼���־��ʾ
#define MSG_7_BRD_SUB_PLYBAK_STEP			0x09		// ��Ӱ��������
#define MSG_7_BRD_SUB_MONITOR_TIMEROVER		0x0A

#define MSG_7_BRD_SUB_SLAVE_REGISTER_SCCESS			11
#define MSG_7_BRD_SUB_SLAVE_REGISTER_TIMEOUT		12
#define MSG_7_BRD_SUB_SLAVE_REGISTER_APPLY			13
#define MSG_7_BRD_SUB_SLAVE_DISCOVERY_OVER			14
#define MSG_7_BRD_SUB_SLAVE_DISCOVERY_ONE			15

#define MSG_7_BRD_SUB_RECV_QSW_RESID				16		//czn_20170720

#define MSG_7_BRD_SUB_SIP_ONLINE						17
#define MSG_7_BRD_SUB_SIP_OFFLINE						18

#define MSG_7_BRD_SUB_MASTERSTATE_CHANGE			19
#define MSG_7_BRD_SUB_HAVE_MISSCALL					20		//czn_20170805
#define MSG_7_BRD_SUB_RING_STOP						21		//czn_20170808
#define MSG_7_BRD_SUB_INFORM_FISH_EYE				22		
#define MSG_7_BRD_SUB_FW_UPDATE_PERCENTAGE			23		
#define MSG_7_BRD_SUB_MonitorOn						24		
#define MSG_7_BRD_SUB_MonitorOff					25		
#define MSG_7_BRD_SUB_MonitorTalkOn					26		
#define MSG_7_BRD_SUB_MonitorTalkOff				27		
#define MSG_7_BRD_SUB_BecalledOn					28		
#define MSG_7_BRD_SUB_BecalledOff					29		
#define MSG_7_BRD_SUB_BecalledTalkOn				30		
#define MSG_7_BRD_SUB_BecalledTalkOff				31		
#define MSG_7_BRD_SUB_CallTalkOn					32		
#define MSG_7_BRD_SUB_CallTalkOff					33		
#define MSG_7_BRD_SUB_CallOn						34		
#define MSG_7_BRD_SUB_CallOff						35		
#define MSG_7_BRD_SUB_MonitorStart					36	
#define MSG_7_BRD_SUB_MonitorStartting				37	
#define MSG_7_BRD_SUB_MonitorName					38	
#define MSG_7_BRD_SUB_MonitorError					39
#define MSG_7_BRD_SUB_MonitorUnlock1				40		
#define MSG_7_BRD_SUB_MonitorUnlock2				41
#define MSG_7_BRD_SUB_BecalledName					42

#define MSG_7_BRD_SUB_IntercomCallStart				43	
#define MSG_7_BRD_SUB_IntercomCallStartting			44	
#define MSG_7_BRD_SUB_IntercomCallOn				45	
#define MSG_7_BRD_SUB_IntercomCallName				46	
#define MSG_7_BRD_SUB_IntercomCallTalkOn			47	
#define MSG_7_BRD_SUB_IntercomCallOff				48	
#define MSG_7_BRD_SUB_IntercomBeCallOn				49	
#define MSG_7_BRD_SUB_IntercomBeCallOff				50	

#define MSG_7_BRD_SUB_InnerCallStart				51	
#define MSG_7_BRD_SUB_InnerCallStartting			52	
#define MSG_7_BRD_SUB_InnerCallOn					53	
#define MSG_7_BRD_SUB_InnerCallName					54	
#define MSG_7_BRD_SUB_InnerCallTalkOn				55	
#define MSG_7_BRD_SUB_InnerCallOff					56	
#define MSG_7_BRD_SUB_InnerBeCallOn					57	
#define MSG_7_BRD_SUB_InnerBeCallOff				58	

#define MSG_7_BRD_SUB_BeCalledDivert				59	
#define MSG_7_BRD_SUB_CallError						60	
#define MSG_7_BRD_SUB_BeCalledUnlock1				61	
#define MSG_7_BRD_SUB_BeCalledUnlock2				62	
#define MSG_7_BRD_SUB_FW_UpdateStart				63	
#define MSG_7_BRD_SUB_INFORM_AUTO_AGING				64
#define MSG_7_BRD_SUB_INFORM_AUTO_AGING_TALK		65
#define MSG_7_BRD_SUB_INFORM_ENTER_AUTO_AGING		66
#define MSG_7_BRD_SUB_MovementTestStart				67
#define MSG_7_BRD_SUB_MovementTestNext				68
#define MSG_7_BRD_SUB_MovementTestLED				69
#define MSG_7_BRD_SUB_IPC_MonitorOff				70
#define MSG_7_BRD_SUB_StopOneSipCallTest			71		//20181017
#define MSG_7_BRD_SUB_DivertToLocalAckWait			72		//czn_20181027
#define MSG_7_BRD_SUB_DivertTestOn					73
#define MSG_7_BRD_SUB_GET_REGISTER_STATE_OK			74
#define MSG_7_BRD_SUB_GET_REGISTER_STATE_FAIL		75
#define MSG_7_BRD_SUB_MANUAL_REGISTER_TIMEOUT		76
//czn_20181219_s
#define MSG_7_BRD_SUB_OtaClientIfc_ActOk				77		
#define MSG_7_BRD_SUB_OtaClientIfc_Error				78
#define MSG_7_BRD_SUB_OtaClientIfc_Translating			79
#define MSG_7_BRD_SUB_OtaClientIfc_Translated			80
//czn_20181219_e
#define MSG_7_BRD_SUB_WaitingRemoteDeviceRebootTimeCnt	81
#define MSG_7_BRD_SUB_OtaOperation						82		
#define MSG_7_BRD_SUB_OtaReturn							83		
#define MSG_7_BRD_SUB_OtaOperationTimeCnt				84

//czn_20190221_s
#define MSG_7_BRD_SUB_MsSyncSipConfig					85
#define MSG_7_BRD_SUB_MsSyncCallScene					86
//czn_20190221_e
#define MSG_7_BRD_SUB_AutotestStateUpate				87		//czn_20190412	

#define MSG_7_BRD_SUB_RecvSoftwareUpdateReq			88		//czn_20190506
#define MSG_7_BRD_SUB_MonitorListUpdate				89	
#define MSG_7_BRD_SUB_NameListUpdate				90	
#define MSG_7_BRD_SUB_MSListUpdate					91
#define MSG_7_BRD_SUB_OSBinding							92
// lzh_20191009_s
#define MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP			93
#define MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN			94
// lzh_20191009_e
#define MSG_7_BRD_SUB_UNLOCK_SUCESSFULL				95
#define MSG_7_BRD_SUB_UNLOCK_UNSUCESSFULL			96

#define MSG_7_BRD_SUB_GLCallName					97			//czn_20191128
#define MSG_7_BRD_SUB_GLCallTalkOn					98		
#define MSG_7_BRD_SUB_GLCallOn						99	
#define MSG_7_BRD_SUB_GLCallVdOn					100
#define MSG_7_BRD_SUB_GLCallOff						101

#define MSG_7_BRD_SUB_RING_REQ						102
#define MSG_7_BRD_SUB_PROG_CALL_NBR					103

#define MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_START		104

#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_START			105
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_ERROR			106
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_RATES			107
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_END				108
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_START		109
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_ERROR		110
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_OK		111
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_WAITING_REBOOT	112

#define MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_CANCEL			113
#define MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_CANCEL			114

#define MSG_7_BRD_SUB_WIFI_CONNECT_FAILED				115		//wifiʧ��

#define MSG_7_BRD_SUB_DVR_SCHEDULE						116
#define MSG_7_BRD_SUB_DVR_SPACE_CHECK					117
#define MSG_7_BRD_SUB_DVR_STOP							118

#define MSG_7_BRD_SUB_PhoneTalkOn						119
// lzh_20201201_s
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_ok	120
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_er	121
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_run	122
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_er	123
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_ok	124
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_cancel_ok	125
#define MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_cancel_er	126

// lzh_20201201_e

#define MSG_7_BRD_SUB_GetCpuMemUse						127
#define MSG_7_BRD_SUB_IpcOpenRtspErr					128

#define MSG_7_BRD_SUB_ListenTalkStart				129
#define MSG_7_BRD_SUB_ListenTalkClose				130

#define	MSG_7_BRD_SUB_InnerBeBrdCallOn				131	

#define MSG_7_BRD_SUB_TestIxMonitorOn				133	

#define MSG_7_BRD_SUB_RING2_REQ						134

#define MSG_7_BRD_SUB_DIVERT_STATE					135

#define MSG_7_BRD_Armed_Delay						140
#define MSG_7_BRD_MsgClose_TouchClick				141
#define MSG_7_BRD_AlarmingHappen					142
#define MSG_7_BRD_AlarmingSenserUpdate				143
#define MSG_7_BRD_Disarmed_Delay					144
#define MSG_7_BRD_IconSosChange					145

#define MSG_7_BRD_LightStateChange					146
#define MSG_7_BRD_UnlcokEvent						147

extern int KeyBeepDisable;

void vtk_TaskInit_video_menu(int priority);
void exit_vdp_video_menu_task(void);

int MenuDisplayAck_Rsp( unsigned char msg_id, unsigned char msg_type );

int API_MenuDisplay( unsigned char iMenuId );
int API_MenuClose( void );
POS API_GetIconPos(uint16 iconId );

int API_MenuIconPress( void );
int API_MenuIconRelease( void );

int API_OsdStringDisplayExt( int x, int y, int fgcolor, unsigned char* string, int strlen, int fnt_type,int format, int width);
int API_OsdStringDisplayExtWithBackColor( int x, int y, int fgcolor, int bgcolor, unsigned char* string, int strlen, int fnt_type,int format );
int API_OsdStringClearExt(int x, int y, int width, int height);

int API_OsdUnicodeStringDisplayWithIdAndAsc( int x, int y, int fgcolor, int str_id, int fnt_type, const char* preAscStr, const char* lstAscStr, int width);

#define API_OsdUnicodeStringDisplay( x, y, fgcolor, str_id, fnt_type, width)		API_OsdUnicodeStringDisplayWithIdAndAsc( x, y, fgcolor, str_id, fnt_type, NULL, NULL, width)

int API_OsdUnicodeStringDisplayWithIdAndAsc2( int x, int y, int layerId, int fgcolor, int bgcolor, int str_id, int fnt_type, const char* preAscStr, const char* lstAscStr, int xsize, int ysize);
#define API_OsdUnicodeStringDisplay2( x, y, fgcolor, str_id, fnt_type, width, height)		API_OsdUnicodeStringDisplayWithIdAndAsc2( x, y, OSD_LAYER_STRING, fgcolor, COLOR_KEY, str_id, fnt_type, NULL, NULL, width, height)


int API_OsdStringDisplayExt2( int x, int y, int layerId, int fgcolor, int bgcolor, unsigned char* string, int strlen, int fnt_type,int format, int xsize, int ysize);

void OSD_StringDisplayWithIcon(int x,int y,int uColor, int uBgColor, int icon, int fnt_type, int width);
#define API_OsdUnicodeStringDisplayWithIcon( x, y, fgcolor, icon, fnt_type, width)	OSD_StringDisplayWithIcon(x, y, fgcolor, COLOR_KEY, icon, 2, width);//fnt_type

void GetOSD_StringWithIcon(int icon, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, char*pStr, int *pLen);
#define API_GetOSD_StringWithIcon(icon, pStr, pLen)	GetOSD_StringWithIcon(icon, NULL, 0, NULL, 0, pStr, pLen)

void API_GetOSD_StringWithID(int str_id, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, char*pStr, int *pLen);
#define API_GetOSD_StringWithID2(strId, pStr, pLen)	API_GetOSD_StringWithID(strId, NULL, 0, NULL, 0, pStr, pLen)


//int API_OsdStringDisplay( int x, int y, int fgcolor, unsigned char* string );
//int API_OsdStringDisplayWithBG( int x, int y, int fgcolor, int bgcolor, unsigned char* string );

//int API_OsdStringClear(int x, int y, unsigned char* string);

int API_LabelShow( LableType iLabelIndex );
int API_LabelHide( LableType iLabelIndex ); 

int API_LabelShow_XY( int col, int row, int color, LableType iLabelIndex );
int API_LabelHide_XY( int col, int row, LableType iLabelIndex );

int API_SpriteDisplay_XY( int x, int y, int iSpriteId );
int API_SpriteClose( int x, int y, int iSpriteId );
int API_SpriteCloseNoBG( int x, int y, int iSpriteId );

unsigned char API_TimeLapseStart( TimeLapseMode iCountMode, unsigned int iStartTime );
unsigned char API_TimeLapseShow( void );
unsigned char API_TimeLapseHide( void );
unsigned char API_TimeLapseStop(void);
unsigned char API_DateTimeShow(void);
unsigned char API_DateTimeHide(void);
unsigned char API_FixedCurIconCursor(unsigned char curIconCursor);

///////////////////////////////////////////////////////////////////////////////////////////////////
//log view interface
///////////////////////////////////////////////////////////////////////////////////////////////////
int API_GetOsdLogViewState( void );
int API_SetOsdLogViewState( int onoff );
int API_SwitchOsdLogViewState( void );
int API_OsdLogViewAddRec( unsigned char* string );
int API_MenuIconDisplaySelectOn( uint16 icon );
int API_MenuIconDisplaySelectOff( uint16 icon );

void API_GetOSD_StringWithID(int str_id, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, char*pStr, int *pLen);

int API_EnableOsdUpdate( void );
int API_DisableOsdUpdate( void );
void API_add_Inform_to_VideoMenu_queue(int inform);
void API_add_Inform_with_data_to_VideoMenu_queue(int inform, char* data, int len);

// lzh_20181016_s
void API_menu_display_cache_push_string( int menu_id, int cache_num, int x, int y, int width, int height );
void API_menu_display_cache_pop_string( int menu_id, int cache_num );
// lzh_20181016_e
// lzh_20181108_s
int MenuDisplayAck_Rsp_with_result( unsigned char msg_id, unsigned char msg_type, unsigned char result );
int API_create_bmpfile_for_menu( int menu_id, int sub_id, int sub2_id );
int API_menu_create_bitmap_file( int menu_index, int sub_index, int sub2_index );
// lzh_20181108_e
int API_ProgBarDisplay( int x, int y, int sx, int sy, int color );
int API_ProgBarClose( int x, int y, int sx, int sy);
void API_VideoMenu_Ring2(char* msg, int time);

#endif


