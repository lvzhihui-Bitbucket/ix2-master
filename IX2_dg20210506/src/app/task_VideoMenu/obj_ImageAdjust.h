
#ifndef _OBJ_IMAGEADJUST_H
#define _OBJ_IMAGEADJUST_H

/*******************************************************************************************
 * @fn：	Video_ImageParaSet
 *
 * @brief:	视频亮度和色度逻辑参数设置
 *
 * @param:   	logBright - 亮度逻辑值，logColor - 色度逻辑, logContrast - 对比度逻辑值
 *
 * @return: 	none
 *******************************************************************************************/
void Video_ImageParaSet(unsigned char logBright, unsigned char logColor, unsigned char logContrast);

/*******************************************************************************************
 * @fn：	Video_ImageScaleSet
 *
 * @brief:	视频scaler 设置
 *
 * @param:  	scale - scaler模式
 *
 * @return: 	none
 *******************************************************************************************/
void Video_ImageScaleSet(VIDEO_SCALE scale);

#endif

