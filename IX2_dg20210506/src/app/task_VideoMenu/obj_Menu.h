
#ifndef _OBJ_MENU_H
#define _OBJ_MENU_H

unsigned char Menu_Display(int menu_cnt);
// lzh_20161230_s
unsigned char Menu_Display2(int menu_cnt, int sub_menu_cnt, int clr_scr );
// lzh_20161230_e

void Menu_Close(void);

int Menu_Sprite_Display( int x, int y, int Sprite_cnt );
int Menu_Sprite_Close( int x, int y, int Sprite_cnt, int mode );

#endif



