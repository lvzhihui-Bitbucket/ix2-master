/**
  ******************************************************************************
  * @file    ObjLable.c
  * @author  Lv
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the function of Lable Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include "obj_OsdExtLabel.h"
#include "obj_OsdIntLabel.h"
#include "obj_OsdLabel.h"

typedef enum
{
	EXT,
	INT,
} LabelMode;

typedef struct
{
	LabelMode	 	type;	//LABEL显示方式（内部或是外部）
	int 			col;	//显示起始列
	int			 	row;	//显示起始行
	int			 	color;	//显示颜色
	int			 	index;	//内部或是外部的index
} LABEL_ATTR;

    
const LABEL_ATTR LabelAttrTab[] =
{	
    // type 1
    {	EXT,      16,     0,    COLOR_RED,	       (unsigned char)EL_Down_Index,   	    		},
    {	EXT,      10,     5,    COLOR_GREEN,	(unsigned char)EL_Up_Index,   	    			},
    {	EXT,      15,     2,    COLOR_GREEN,	(unsigned char)EL_Missed_Index,         		},
    {	EXT,      15,     3,    COLOR_GREEN,	(unsigned char)EL_Paly_Index,				},
    {	EXT,      0,       3,    COLOR_GREEN,	(unsigned char)EL_LEFT_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_RIGHT_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_MIDDLE_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_LEFT_UP_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_RIGHT_UP_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_LEFT_DN_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_RIGHT_DN_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_MID_DOWN_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_MID_UP_Index,				},
    {	EXT,      0,     3,    COLOR_GREEN,	(unsigned char)EL_HOME_Index,				},
    // type 2
    {	EXT,      21,     0,    COLOR_RED,	       (unsigned char)EL_Unlock1_Index,    			},//26
    {	EXT,      23,     0,    COLOR_RED,	       (unsigned char)EL_Unlock2_Index,   	    		},//23
    {	EXT,      26,     0,    COLOR_RED,	       (unsigned char)EL_Light_Index,   	    			},//21
    {	EXT,      10,     5,    COLOR_GREEN,	(unsigned char)EL_Bussy_Index,   	    		},
    {	EXT,      0,       5,    COLOR_GREEN,	(unsigned char)EL_Tick_Index,    	    			},
    {	EXT,      15,     1,    COLOR_GREEN,	(unsigned char)EL_Menu_Index,           		},
    {	EXT,      29,     2,    COLOR_GREEN,	(unsigned char)EL_Transfer_Index,         		},
    {	EXT,      15,     3,    COLOR_GREEN,	(unsigned char)EL_Do_Not_Disturb_Index,		},
    {	EXT,      15,     4,    COLOR_GREEN,	(unsigned char)EL_Lock_Index,           		},     
    {	EXT,      14,     0,    COLOR_RED,	       (unsigned char)EL_Alarm_Index,    			},
    {	EXT,      14,     0,    COLOR_RED,	       (unsigned char)EL_Current_Index,    			},    
    {	EXT,      19,     0,    COLOR_RED,	       (unsigned char)EL_Tlak_Index,    				},
    {	EXT,      19,     0,    COLOR_RED,	       (unsigned char)EL_Tip_Index,    				},
    {	EXT,      19,     0,    COLOR_RED,	       (unsigned char)EL_Tip1_Index,    				},
    {	EXT,      19,     0,    COLOR_RED,	       (unsigned char)EL_Voice_Index,    				},
    // type 3
    {	EXT,      34,     5,    COLOR_WHITE,     (unsigned char)EL_Big_Light_1_Index, 		},
    {	EXT,      34,     6,    COLOR_WHITE,     (unsigned char)EL_Big_Light_2_Index,    		},
    {	EXT,      27,     5,    COLOR_WHITE,	(unsigned char)EL_Big_Busy_1_Index,   	    	},
    {	EXT,      27,     6,    COLOR_WHITE,	(unsigned char)EL_Big_Busy_2_Index,   	    	},
    {	EXT,      20,     5,    COLOR_WHITE,	(unsigned char)EL_Big_Disturb_1_Index,		},
    {	EXT,      20,     6,    COLOR_WHITE,	(unsigned char)EL_Big_Disturb_2_Index,    		},
    {	EXT,      20,     5,    COLOR_WHITE,	(unsigned char)EL_Big_Transfer_1_Index,  		},
    {	EXT,      20,     6,    COLOR_WHITE,	(unsigned char)EL_Big_Transfer_2_Index,		},
    {	EXT,      20,     5,    COLOR_WHITE,	(unsigned char)EL_Big_Voice_1_Index,  		},
    {	EXT,      20,     6,    COLOR_WHITE,	(unsigned char)EL_Big_Voice_2_Index,		},
    // type 7      
    {	EXT,       0,      3,    COLOR_RED,	    	(unsigned char)EL_Norma_Index,	    			},
    {	EXT,       6,      3,    COLOR_RED,	    	(unsigned char)EL_Bright_Index,	    			},
    {	EXT,      13,     3,    COLOR_RED,	    	(unsigned char)EL_Soft_Index,	    			},
    {	EXT,      20,     3,    COLOR_RED,	    	(unsigned char)EL_User_Index,	    			},
    {	EXT,      18,     0,    COLOR_RED,	    	(unsigned char)EL_Delete_Index,	    			},
    {	EXT,      32,     0,    COLOR_RED,	    	(unsigned char)EL_Record_Index,	    			},
    {	EXT,      18,     0,    COLOR_RED,	    	(unsigned char)EL_Connated_Index,	    		},
    // type 10      
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_0_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_1_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_2_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_3_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_4_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_5_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_6_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_7_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_8_Index,	    		},    
    {	EXT,      34,     0,    COLOR_RED,	    	(unsigned char)EL_Progress_9_Index,	    		},        
    // type 11        	,
    {	EXT,      3,      2,    COLOR_WHITE,	(unsigned char)EL_Loading_Files_Index,       	},
    {	EXT,      0,      5,    COLOR_WHITE,    	(unsigned char)EL_Refuse_Calls_In_Index, 		},
    {	EXT,      8,      2,    COLOR_WHITE,	(unsigned char)EL_Missed_Calls_Index,		},
    {	EXT,      8,      3,    COLOR_RED,	    	(unsigned char)EL_No_Sim_Card_Index,		},
    {	EXT,      8,      5,    COLOR_WHITE,    	(unsigned char)EL_Not_Connect_Index,     		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Login_Failed_Index,        	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Calling_Index,        	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Call_End_Index,        	},
    // type 18 
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Update_Display_Driver_Index,	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Failed_To_Send_Sms_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Notify_Transfer_Phone_Index,	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_1_Unlock_Error_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_2_Unlock_Error_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_3_Unlock_Error_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_4_Unlock_Error_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Send_Sos_Sms_Index,  		 	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Sms_Sent_Out_Index,  			},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Format_SD_Card_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Update_Software_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_1_Opened_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_2_Opened_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_3_Opened_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_4_Opened_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Please_Wait_Index, 			},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_Opened_Index,  			},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Restore_To_Default_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Copy_Pictures_To_SD_Index,  	},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Failed_Index,  					},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Succeed_Index,  				}, 
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Have_No_Files_Index,  			},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Have_No_This_File_Index,  		},     
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_MON_SELECT_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Receive_Time_Index,  		},  
   	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Send_Time_Index,  		},  
   	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_NOT_DISTURB_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_TRANSFER_1_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_VOICE_PROMPT_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_NAMELIST_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_CALL_RECORD_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_Rename_Index,  		},  
   	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Camera_Rename_Index,  		},  
	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Monitor_Time_Set_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Ring_Tone_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Volume_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_Ring_Mode_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Date_Time_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Date_Sync_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Language_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_ABOUT_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Local_Address_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Video_Standard_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_System_Verson_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Display_Driver_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_FONT_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_UI_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_SD_INFO_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_SD_Card_Index,  		},  
   	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Video_Capacity_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Video_Usage_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_FLASH_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Image_Capacity_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Image_Usage_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_TRANSFER_DEVCE_INFO_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_DEVICE_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_SIM_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_NETWORK_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_SIGNAL_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_NAME_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_DAY_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_NIGHT_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_SELECT_RECEIVER_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_TEL_NUM1_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_TEL_NUM2_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_TEL_NUM3_Index,  		},  
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_VISITOR_MESSAGE_Index,  		}, 
    	 {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_KEYPAD_Index,  		}, 
     {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_1_Motion_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_2_Motion_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_3_Motion_Index,  		},
    {	EXT,      12,     5,    COLOR_GREEN,	(unsigned char)EL_Door_4_Motion_Index,  		},
   	 // 内部字模        
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_DS1,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_DS2,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_DS3,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_DS4,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_CM1,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_CM2,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_CM3,				},
    {	INT,      0,      0,    COLOR_GREEN,	(unsigned char)INT_LABEL_CM4,				},
    // type 5
    {	EXT,      0,     6,    COLOR_WHITE,     (unsigned char)EL_Image_Index, 			},
    {	EXT,      0,     6,    COLOR_WHITE,     (unsigned char)EL_Video_Index,    			},
    {	EXT,      16,     0,    COLOR_WHITE,	(unsigned char)EL_Pause_Index,   	    		},  
};

/*******************************************************************************************
 * @fn:		Lable_Show
 *
 * @brief:	显示lable
 *
 * @param:  labelnum - lable编号
 *
 * @return: none
 *******************************************************************************************/
void Lable_Show(LableType labelnum)
{   
	if( labelnum >= LABEL_MAX )
		return;
	
	if( LabelAttrTab[labelnum].type == INT )
	{
		Show_Int_Label( LabelAttrTab[labelnum].row, LabelAttrTab[labelnum].col,LabelAttrTab[labelnum].color,(IntLableType)LabelAttrTab[labelnum].index);		
	}
	else
	{		
		Show_Ext_Label( LabelAttrTab[labelnum].row, LabelAttrTab[labelnum].col,LabelAttrTab[labelnum].color,(ExtLableType)LabelAttrTab[labelnum].index);		
	}
}

/*******************************************************************************************
 * @fn:		Lable_Hide
 *
 * @brief:	清除lable
 *
 * @param:  row 行 col 列 color 颜色 labelnum - lable编号
 *
 * @return: none
 *******************************************************************************************/
void Lable_Hide(LableType labelnum)
{
	if( labelnum >= LABEL_MAX )
		return;
	
	if( LabelAttrTab[labelnum].type == INT )
	{
		Hide_Int_Label( LabelAttrTab[labelnum].row, LabelAttrTab[labelnum].col,(IntLableType)LabelAttrTab[labelnum].index);		
	}
	else
	{		
		Hide_Ext_Label( LabelAttrTab[labelnum].row, LabelAttrTab[labelnum].col,(ExtLableType)LabelAttrTab[labelnum].index);
	}
}

/*******************************************************************************************
 * @fn:		Lable_Show_XY
 *
 * @brief:	显示lable
 *
 * @param:  	row - 起始行，col - 起始列,  color -  颜色,  labelnum - lable编号
 *
 * @return: 	none
 *******************************************************************************************/
void Lable_Show_XY( int row, int col, int color, LableType labelnum )
{   
	if( labelnum >= LABEL_MAX )
	{
		return;
	}
	
	dprintf("label show num = %d,x=%d,y=%d\n",labelnum,col,row);
		
	Show_Ext_Label( row, col, color, (ExtLableType)LabelAttrTab[labelnum].index );

}

/*******************************************************************************************
 * @fn:		Lable_Hide
 *
 * @brief:	清除lable
 *
 * @param: 	row - 起始行，col - 起始列,  labelnum - lable编号
 *
 * @return: 	none
 *******************************************************************************************/
void Lable_Hide_XY( int row, int col, LableType labelnum )
{
	if( labelnum >= LABEL_MAX )
	{
		return;
	}
	
	Hide_Ext_Label( row, col, (ExtLableType)LabelAttrTab[labelnum].index );
}

