/**
  ******************************************************************************
  * @file    ObjTimeLapse.c
  * @author  Lv
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the function of TimeLapse Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "../../os/RTOS.h"
#include "../../os/OSTIME.h"
#include "obj_OsdTimeLapse.h"
#include "task_VideoMenu.h"

TimeLapseData timeLapse;

#define TIME_LAPSE_COLOR			COLOR_BLUE
#define TIME_LAPSE_DISP_POS_X		10		//czn_2070313
#define TIME_LAPSE_DISP_POS_Y		8

void TimeLapseInit(void)
{
	// create timer, not star
	OS_CreateTimer(&timeLapse.TimeLapseTimer,TimeLapseTimerCallBack,1000/25);
}

/*******************************************************************************************
 * @fn:		TimeLapseStart
 *
 * @brief:	启动TimeLapse
 *
 * @param:  iCountMode - 启动模式，iVal - 时间长度（秒为单位）
 *
 * @return: none
 *******************************************************************************************/
void TimeLapseStart(TimeLapseMode iCountMode, unsigned int iVal)
{
	timeLapse.countMode = iCountMode;
	timeLapse.timeVal = iVal;
	timeLapse.cntMenu = GetCurMenuCnt();
	timeLapse.hide  = 0;
	//start timer for time lapse update
	OS_RetriggerTimer(&timeLapse.TimeLapseTimer);
	//update display
	//OSD_UpdateTimeLapseDisp(1);		
}

/*******************************************************************************************
 * @fn:		TimeLapseShow
 *
 * @brief:	继续显示TimeLapse
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void TimeLapseShow(void)
{
	OSD_UpdateTimeLapseDisp(1);
}


/*******************************************************************************************
 * @fn:		TimeLapseStop
 *
 * @brief:	停止TimeLapse，清除显示
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void TimeLapseStop(void)
{
   	OS_StopTimer(&timeLapse.TimeLapseTimer);
	//OSD_UpdateTimeLapseDisp(0);
}

/*******************************************************************************************
 * @fn:		TimeLapseHide
 *
 * @brief:	不停止TimeLapse，清除显示
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void TimeLapseHide(void)
{
	OSD_UpdateTimeLapseDisp(0);
}

/*******************************************************************************************
 * @fn:		OSD_UpdateTimeLapseDisp
 *
 * @brief:	显示TimeLapse.timeVal
 *
 * @param:  uClear - 0/清除，1/显示
 *
 * @return: none
 *******************************************************************************************/
void OSD_UpdateTimeLapseDisp( unsigned char uClear )
{
	unsigned char i,min_h,min_l,sec_h,sec_l;
	unsigned char buf[6];

	if( !uClear )
	{
		buf[0] = ' ';
		buf[1] = ' ';
		buf[2] = ' ';
		buf[3] = ' ';
		buf[4] = ' ';
		timeLapse.hide = 1;
	}	
	else
	{
		min_h = timeLapse.timeVal/60;
		min_l = min_h%10;
		min_h = min_h/10;
		sec_h = timeLapse.timeVal%60;
		sec_l = sec_h%10;
		sec_h = sec_h/10;
		//显示分钟
		buf[0] = min_h+'0';
		buf[1] = min_l+'0';
		buf[2] = ':';
		//显示秒钟
		buf[3] = sec_h+'0';
		buf[4] = sec_l+'0';
		timeLapse.hide = 0;
	}	
	buf[5] = 0;
	//LZH_DEBUG_S 20151127
	if( (!timeLapse.hide) && timeLapse.cntMenu == GetCurMenuCnt())		//czn_20170313
	{
		//OSD_StringClearExt(TIME_LAPSE_DISP_POS_X,TIME_LAPSE_DISP_POS_Y,80,40);
		OSD_StringDisplayExt(TIME_LAPSE_DISP_POS_X,TIME_LAPSE_DISP_POS_Y,TIME_LAPSE_COLOR, COLOR_KEY, buf, strlen(buf), 1, STR_UTF8, 0);
		//OSD_StringDisplay(TIME_LAPSE_DISP_POS_X,TIME_LAPSE_DISP_POS_Y,TIME_LAPSE_COLOR, COLOR_KEY, buf);
	}
	//LZH_DEBUG_E 20151127
}

/*******************************************************************************
 * @fn      TimeLapseMsgSend()
 *
 * @brief   time lapse update msg push to task
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void TimeLapseMsgSend(void)
{
	TimeLapse_type msg_buf;	

	msg_buf.head.msg_source_id 	= MSG_ID_Menu; 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_TIME_LAPSE;
	msg_buf.control				= TIME_LPSE_UPDATE;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(TimeLapse_type) );
}

/*******************************************************************************
 * @fn      TimeLapseTimerCallBack()
 *
 * @brief   timer timelapse time out callback
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void TimeLapseTimerCallBack(void)
{
    if( timeLapse.countMode == COUNT_RUN_UP )
    {
        timeLapse.timeVal++;            	
    }
    else
    {
        if( timeLapse.timeVal > 0 )
        {
            timeLapse.timeVal--;
        }
    }    
    if( !timeLapse.hide )
    {
        TimeLapseMsgSend();
    }
    OS_RetriggerTimer(&timeLapse.TimeLapseTimer);
}
