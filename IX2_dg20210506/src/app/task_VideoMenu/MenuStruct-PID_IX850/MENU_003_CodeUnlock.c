
#include "MENU_003_CodeUnlock.h"
//#include "../obj_Survey_Keyboard.h"
#include "task_Beeper.h"
#include "task_Unlock.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_IpCacheTable.h"
//#include "obj_VirtualUserTable.h"
#include "obj_UnlockReport.h"

#define MAX_UNLOCKINPUT_LEN	20
#define UNLOCK_PWD_LEN		4

#define UNLOCK_INPUT_PROMPT		"Enter unlock code"

#define PROMPT_DISP_H		30
#define INPUT_DISP_H		80
#define input_sprite_left		100
#define input_sprite_width	20
#define input_sprite_shift		10

int mic_setting_flag = 0;
int spk_setting_flag = 0;

typedef struct
{
	int input_len;
	char input_buff[MAX_UNLOCKINPUT_LEN + 1];
}Unlock_Input_Run_Stru;

Unlock_Input_Run_Stru Unlock_Input_Run;

const KeyProperty Menu3_Input_Symbol_Tab[]=
{
	{ICON_020_kb_digit0,			'0'},	// 0
	{ICON_021_kb_digit1,			'1'},
	{ICON_022_kb_digit2,			'2'},
	{ICON_023_kb_digit3,			'3'},
	{ICON_024_kb_digit4,			'4'},
	{ICON_025_kb_digit5,			'5'},	
	{ICON_026_kb_digit6,			'6'},
	{ICON_027_kb_digit7,			'7'},
	{ICON_028_kb_digit8,			'8'},
	{ICON_029_kb_digit9,			'9'},
};         
   
const int Menu3_Input_Symbol_Tab_Size = sizeof( Menu3_Input_Symbol_Tab ) / sizeof( Menu3_Input_Symbol_Tab[0] );


void Input_Clear(void)
{
	#if 0
	char disp[20];
	/*int i;
	for(i = 0;i < MAX_UNLOCKINPUT_LEN;i++)
	{
		API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(i*2+1)+input_sprite_width*i, INPUT_DISP_H, SPRITE_PW_NOINPUT);	
	}*/
	Unlock_Input_Run.input_len = 0;
	memset(Unlock_Input_Run.input_buff, 0,MAX_UNLOCKINPUT_LEN+1);
	//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
	get_password_disp(Unlock_Input_Run.input_len,Unlock_Input_Run.input_buff,disp);
	API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
	#endif

}
//call num(input_len - 4) + psw(4) 
int PrivatePsw_Verify(void)
{
	char code[11]={0};
	IpCacheRecord_T record[32]; // zfz_20190611
	char display[20];
	char *pos1, *pos2;

	if(Unlock_Input_Run.input_len < UNLOCK_PWD_LEN+1)
		return 0;
	API_Event_IoServer_InnerRead_All(CODE_UNLOCK_DISABLE, display);
	if(atoi(display))
		return 0;
	
	printf("111111111111111111111\n");
	
	memcpy(code,Unlock_Input_Run.input_buff,Unlock_Input_Run.input_len-4);
	//printf("call num input: %s\n",code);
	// zfz_20190611 start
	int repCnt,i;
	repCnt = API_GetIpByInput(code, record);
	if( repCnt > 32 )
	{
		repCnt = 32;
	}
	if(repCnt)// >0 获取到房号IP
	{				
		for( i=0; i<repCnt; i++ )
		{
			if( strcmp(&(record[i].BD_RM_MS[8]), "01") == 0 )
			{
				break;
			}
		}
		if( i == repCnt )
		{
			i = 0;
		}
		sprintf(display, "<ID=%s>", PRIVATE_UNLOCK_CODE);//获取私有密码
		API_io_server_UDP_to_read_remote(record[i].ip, 0xFFFFFFFF, display);
		// end
		pos1 = strstr(display, "Value=");
		if(pos1 != NULL)
		{
			pos2 = strchr(pos1, '>');
			if(pos2 != NULL)
			{
				int len;
				len = ((int)(pos2-pos1))-strlen("Value=");
				memcpy(code, pos1+strlen("Value="), len);
				code[len] = 0;
				//printf("PrivatePassword: %s\n",code);
				if(!strcmp(code,Unlock_Input_Run.input_buff+(Unlock_Input_Run.input_len-4)))
				{
					API_UnlockReport(record[i].ip, LOCK_1, UNLOCK_SUCESSFULL);
					return 1;
				}
			}
		}
	}
	
	API_UnlockReport(record[i].ip, LOCK_1, UNLOCK_UNSUCESSFULL);
	return 0;
}

int VMPrivatePsw_Verify(void)
{
#if 0
	char code[5]={0};
	char display[20];
	VirtualUserRecord_T virtualUserRecord;
	int tb_index;

	if(Unlock_Input_Run.input_len < UNLOCK_PWD_LEN+1)
		return 0;
	API_Event_IoServer_InnerRead_All(CODE_UNLOCK_DISABLE, display);
	if(atoi(display))
		return 0;
	
	memcpy(code,Unlock_Input_Run.input_buff,Unlock_Input_Run.input_len-4);
	//printf("call num input: %s\n",code);
	// zfz_20190611 start
	
	if((tb_index=SearchVirtualUserTableByInput(code, &virtualUserRecord)) >= 0)
	{
		
		if(!strcmp(virtualUserRecord.managePwd,Unlock_Input_Run.input_buff+(Unlock_Input_Run.input_len-4)))
		{
			if(memcmp("0000",GetSysVerInfo_bd(),4)== 0||memcmp("01",GetSysVerInfo_ms(),2)!= 0)
				return 0;
			SetvirtualUserEditRecord(virtualUserRecord);
			SetvirtualUserEditIndex(tb_index);
			if(virtualUserRecord.type == TYPE_VM || virtualUserRecord.type == TYPE_IM_BAK)
			{
				StartInitOneMenu(MENU_039_VirtualUserEdit1,0,0);
			}
			else
			{
				StartInitOneMenu(MENU_087_DTIMEdit,0,0);
			}
			return 1;
		}
		if(!strcmp(virtualUserRecord.lockPwd,Unlock_Input_Run.input_buff+(Unlock_Input_Run.input_len-4)))
		{
			//BEEP_CONFIRM();
			popDisplayLastMenu();
			API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
			return 1;
		}
	}

	return 0;
#endif
}

void Unlock_Password_Verify(void)
{
#if 0
	char unlock1_code[MAX_UNLOCKINPUT_LEN + 1] = {"1111"};
	char unlock2_code[MAX_UNLOCKINPUT_LEN + 1] = {"1234"};
	char display[50];
	char temp[20];
	int vol_temp;
	SYS_VER_INFO_T sysInfo;
	int ret;
	
	
	//if(Unlock_Input_Run.input_len < MAX_UNLOCKINPUT_LEN)
	//	return;

	/*int i;
	for(i = 0;i < MAX_UNLOCKINPUT_LEN;i++)
	{
		API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(i*2+1)+input_sprite_width*i, INPUT_DISP_H, SPRITE_PW_NOINPUT);	
	}

	if(strcmp(Unlock_Input_Run.input_buff,unlock1_code) == 0)
	{
		BEEP_CONFIRM();
		API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
	}
	else if(strcmp(Unlock_Input_Run.input_buff,unlock2_code) == 0)
	{
		BEEP_CONFIRM();
		API_Unlock2(MSG_UNLOCK_SOURCE_KEY);
	}
	
	else if(strcmp(Unlock_Input_Run.input_buff,"9088") == 0)
	{
		Api_FwUpdate_UseSdCard();
	}
	else if(strcmp(Unlock_Input_Run.input_buff,"1088") == 0)
	{
		mic_setting_flag = 1;
		API_Event_IoServer_InnerRead_All(MicVolumeSet, temp);
		snprintf(display, 50, "mic setting:%02d", atoi(temp));
		API_OsdStringCenterDisplayExt(0, 680, COLOR_BLACK, display, strlen(display),0,STR_UTF8,480,45);
	}
	else if(strcmp(Unlock_Input_Run.input_buff,"2088") == 0)
	{
		spk_setting_flag = 1;
		API_Event_IoServer_InnerRead_All(SpeakVolumeSet, temp);
		snprintf(display, 50, "spk setting:%02d", atoi(temp));
		API_OsdStringCenterDisplayExt(0, 680, COLOR_BLACK, display, strlen(display),0,STR_UTF8,480,45);
	}
	else if(strcmp(Unlock_Input_Run.input_buff,"3088") == 0)
	{
		//spk_setting_flag = 1;
		char bd_rm_ms[11];
		API_Event_IoServer_InnerRead_All(BD_RM_MS_NUM, (uint8*)bd_rm_ms);
		snprintf(display, 50, "bd_rm_ms:%s",bd_rm_ms);
		API_OsdStringCenterDisplayExt(0, 680, COLOR_BLACK, display, strlen(display),0,STR_UTF8,480,45);
	}
	else if(strcmp(Unlock_Input_Run.input_buff,"4088") == 0)
	{
		sysInfo = GetSysVerInfo();
		if(GetNetworkStatus() == 1)
		{
			//mesgTextId = MESG_TEXT_linked;
			//snprintf(content, 40,"%s  ",sysInfo.ip);
			snprintf(display, 50, "IP:%s Linked",sysInfo.ip);
		}
		else
		{
			snprintf(display, 50, "IP:%s Unlinked",sysInfo.ip);
		}
		API_OsdStringCenterDisplayExt(0, 680, COLOR_BLACK, display, strlen(display),0,STR_UTF8,480,45);
	}
	else if(strcmp(Unlock_Input_Run.input_buff,"5088") == 0)
	{
		API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
		if(atoi(temp))
		{
			API_DHCP_SaveEnable(0);
			SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
			ResetNetWork();
			snprintf(display, 50, "DHCP&AUTO:Disable");
		}
		else
		{
			API_DHCP_SaveEnable(1);
			snprintf(display, 50, "DHCP&AUTO:Enable");
		}
		API_OsdStringClearExt(0, 680, 480, 45);
		sleep(1);
		API_OsdStringCenterDisplayExt(0, 680, COLOR_BLACK, display, strlen(display),0,STR_UTF8,480,45);
	}
	*/
	#if 1
	ret = InputCode_Verify(Unlock_Input_Run.input_buff);
	if(ret == -1)
	{
		return;
	}
	else if(ret == 1)
	{
		StartInitOneMenu(MENU_006_INPUT_PASSWORD,0,0);
		return;
	}
	#endif
	#if 0
	if(SettingPwd_Verify(Unlock_Input_Run.input_buff)==1)
	{
		return;
	}
	#endif
	if(PrivatePsw_Verify())
	{
		//BEEP_CONFIRM();
		popDisplayLastMenu();
		API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
		return;
	}
	else if(VMPrivatePsw_Verify())	//czn_20190715
	{
		return;
	}		
	else
	{
		BEEP_ERROR();
	}
	
//	Unlock_Input_Run.input_len = 0;
//	memset(Unlock_Input_Run.input_buff, 0,MAX_UNLOCKINPUT_LEN+1);
	Input_Clear();
#endif
}

void MENU_003_CODEUNLOCK_Init(int uMenuCnt)
{
	#if 1
	char disp[100];
	int disp_len;
	if(DialMenuMode ==1)
	{
		API_GetOSD_StringWithID(MESG_TEXT_Keypad_Prompt, NULL, 0, NULL, 0,disp, &disp_len);
		API_OsdStringCenterDisplayExt(0, PROMPT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp,disp_len,0,STR_UNICODE,480,40);
		
		Input_Clear();

		mic_setting_flag = 0;
		spk_setting_flag = 0;

		Update_KeypadIconBackspace(Unlock_Input_Run.input_len);		//czn_20190506

		
		API_SpriteDisplay_XY(GetIconXY(ICON_008_kb_unlock).x,GetIconXY(ICON_008_kb_unlock).y,SPRITE_KeypadUnlock);
	}
	else
	{
		API_DisableOsdUpdate();
		//Unlock_Input_Run.input_len = 0;
		//memset(Unlock_Input_Run.input_buff, 0,MAX_UNLOCKINPUT_LEN+1);
		//API_OsdStringCenterDisplayExt(0, PROMPT_DISP_H, COLOR_BLACK, UNLOCK_INPUT_PROMPT, strlen(UNLOCK_INPUT_PROMPT),0,STR_UTF8,480,40);
		API_GetOSD_StringWithID(MESG_TEXT_Keypad_Prompt, NULL, 0, NULL, 0,disp, &disp_len);
		API_OsdStringCenterDisplayExt(0, PROMPT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp,disp_len,0,STR_UNICODE,480,40);
		/*int i;
		for(i = 0;i < MAX_UNLOCKINPUT_LEN;i++)
		{
			API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(i*2+1)+input_sprite_width*i, INPUT_DISP_H, SPRITE_PW_NOINPUT);	
		}*/
		Input_Clear();

		mic_setting_flag = 0;
		spk_setting_flag = 0;

		Update_KeypadIconBackspace(Unlock_Input_Run.input_len);		//czn_20190506

		API_EnableOsdUpdate();
	}
	#endif
}

void MENU_003_CODEUNLOCK_Exit(void)
{
	
}

void MENU_003_CODEUNLOCK_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i;
	uint8 vol_temp;
	char display[50];

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}

	else if( pglobal_win_msg->type == MSG_2_TKEY )	 
	{
		switch(GetCurIcon())
		{
			//case ICON_019_kb_backspace:
			case ICON_004_kb_return:				
				popDisplayLastMenu();				
				break;
			#if 0
			case ICON_007_kb_call:
			case ICON_008_kb_unlock:
				if(Unlock_Input_Run.input_len == UNLOCK_PWD_LEN)
				{
					//DH 20190508
					API_Event_IoServer_InnerRead_All(CODE_UNLOCK_DISABLE, display);
					if(!atoi(display))
					{
						API_Event_IoServer_InnerRead_All(UnlockPassword, (uint8*)display);
						if(!memcmp(display,Unlock_Input_Run.input_buff,UNLOCK_PWD_LEN))
						{
							Input_Clear();
							//BEEP_CONFIRM();
							popDisplayLastMenu();
							API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
						}
					}
					return;
				}
				Unlock_Password_Verify();
				break;
			#endif	
			case ICON_019_kb_backspace:
				if(pglobal_win_msg->status == TOUCHCLICK)		//czn_20190516
				{
					#if 0
					if(mic_setting_flag == 1 || spk_setting_flag == 1)
					{
						popDisplayLastMenu();
						mic_setting_flag = 0;
						spk_setting_flag = 0;
						return;
					}
					#endif
					if(Unlock_Input_Run.input_len > 0)
					{
						if(KPBackspaceMode == 1)
						{
							Unlock_Input_Run.input_len = 0;
							Unlock_Input_Run.input_buff[0] = 0;
						}
						else
						{
							Unlock_Input_Run.input_buff[--Unlock_Input_Run.input_len] = 0;
						}
						//Unlock_Input_Run.input_buff[--Unlock_Input_Run.input_len] = 0;
						//API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(Unlock_Input_Run.input_len*2+1)+input_sprite_width*Unlock_Input_Run.input_len, INPUT_DISP_H, SPRITE_PW_NOINPUT);
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						get_password_disp(Unlock_Input_Run.input_len,Unlock_Input_Run.input_buff,display);
						API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, display, strlen(display),0,STR_UTF8,480,40);
						
						if(Unlock_Input_Run.input_len == 0)
							Update_KeypadIconBackspace(Unlock_Input_Run.input_len);		//czn_20190506
					}
					else
					{
						popDisplayLastMenu();
					}
				}
				#if 0
				else if(pglobal_win_msg->status == TOUCHLONGPRESS)	
				{
					if(Unlock_Input_Run.input_len > 0)
					{
						Unlock_Input_Run.input_len = 0;
						Unlock_Input_Run.input_buff[0] = 0;
						//API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(Unlock_Input_Run.input_len*2+1)+input_sprite_width*Unlock_Input_Run.input_len, INPUT_DISP_H, SPRITE_PW_NOINPUT);
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						get_password_disp(Unlock_Input_Run.input_len,Unlock_Input_Run.input_buff,display);
						API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, display, strlen(display),0,STR_UTF8,480,40);
						
						//if(Unlock_Input_Run.input_len == 0)
						Update_KeypadIconBackspace(Unlock_Input_Run.input_len);		//czn_20190506
					}
				}
				#endif
				SetKeyBroadMode();
				break;	
				
			default:	
				SetKeyBroadMode();
				for( i = 0;i < Menu3_Input_Symbol_Tab_Size; i++)
				{
					if(GetCurIcon() == Menu3_Input_Symbol_Tab[i].icon)
					{
						
						if(Unlock_Input_Run.input_len < MAX_UNLOCKINPUT_LEN)
						{
							//printf("GetCurIcon() = %d,i = %d char = %c\n",GetCurIcon(),i,Menu2_Input_Symbol_Tab[i].iCon
							//API_SpriteDisplay_XY(input_sprite_left+input_sprite_shift*(Unlock_Input_Run.input_len*2+1)+input_sprite_width*Unlock_Input_Run.input_len, INPUT_DISP_H, SPRITE_PW_HAVEINPUT);
							Unlock_Input_Run.input_buff[Unlock_Input_Run.input_len++] = Menu3_Input_Symbol_Tab[i].key_value;
							//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
							get_password_disp(Unlock_Input_Run.input_len,Unlock_Input_Run.input_buff,display);
							API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, display, strlen(display),0,STR_UTF8,480,40);
							#if 0
							if(Unlock_Input_Run.input_len == UNLOCK_PWD_LEN)
							{
								//DH 20190508
								char pwd_cmp[5];
								API_Event_IoServer_InnerRead_All(UnlockPwdVerifyMode, pwd_cmp);
								if(atoi(pwd_cmp)==1)
									return;
								API_Event_IoServer_InnerRead_All(CODE_UNLOCK_DISABLE, pwd_cmp);
								if(!atoi(pwd_cmp))
								{
									API_Event_IoServer_InnerRead_All(UnlockPassword, (uint8*)pwd_cmp);
									if(!memcmp(pwd_cmp,Unlock_Input_Run.input_buff,UNLOCK_PWD_LEN))
									{
										Input_Clear();
										//BEEP_CONFIRM();
										popDisplayLastMenu();
										API_Unlock1(MSG_UNLOCK_SOURCE_KEY);
									}
								}
							}
							if(SettingPwd_Verify(Unlock_Input_Run.input_buff,Unlock_Input_Run.input_len)==1)
							{
								return;
							}
							#endif
							if(Unlock_Input_Run.input_len == 1)
								Update_KeypadIconBackspace(Unlock_Input_Run.input_len);		//czn_20190506
						}

						return;
					}
				}
				//DefaultPublicIconProcessing();
				break;
		}	
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
}


