
#ifndef _MENU_PUBLIC_H
#define _MENU_PUBLIC_H

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "task_Hal.h"

#include "obj_menu_data.h"
//#include "../sys_msg_process.h"
#include "vdp_uart.h"
//#include "MENU_ShortcutTable.h"


#include "ip_video_cs_control.h"
#include "IP_Audio_Control.h"

#include "obj_VideoProxySetting.h"
#include "task_IoServer.h"
#include "obj_SYS_VER_INFO.h"
#include "task_Led.h"
#include "obj_LockProperty.h"

#include "obj_sip_account_default.h"
#include "vtk_tcp_auto_reg.h"
#include "linphone_interface.h"

//#include "MENU_019_Keypad.h"
#include "MENU_Keyboard.h"

// lzh_20191031
#include "obj_MsSyncList.h"		//czn_20190221
#include "task_ListUpdate.h"	
#include "vtk_udp_stack_device_update.h"
#include "MenuStruct_Resource.h"
#include "obj_MenuList.h"
#include "MenuStruct_Common.h"

#define IX481H 		0
#define IX481V 		1
#define IX482  		2
#define IX850		3
#define IX47		4
#define IXTYPE_MAX	5

int get_pane_type(void);

#define READ_BUFF_LEN				300

#define CLEAR_STATE_H			40
#define DISPLAY_DEVIATION_X			32

#define DISPLAY_TITLE_COLOR			COLOR_WHITE
#define DISPLAY_LIST_COLOR			COLOR_SLIVER_GRAY
#define DISPLAY_STATE_COLOR			COLOR_RED


//��������ҳ����ʾλ��
#define MENU_SCHEDULE_POS_X		0
#define MENU_SCHEDULE_POS_Y		0


#define SHORTCUT_TYPE_NONE				0
#define SHORTCUT_TYPE_MONITOR			1
#define SHORTCUT_TYPE_NAMELIST			2
#define SHORTCUT_TYPE_INNERCALL			3
#define SHORTCUT_TYPE_GUARD_STATION		4
#define SHORTCUT_TYPE_MONITOR_IPC		5



#define CallMenuDisp_Name_Font			0
#define CallMenuDisp_Name_Color			COLOR_WHITE
#define CallMenuDisp_Name_x				100
#define CallMenuDisp_Name_y				10	//czn_20171002
#define CallMenuDisp_Talking_x			280
#define CallMenuDisp_Talking_y			8
#define CallMenuDisp_Unlock_x			305
#define CallMenuDisp_Unlock_y			8
#define CallMenuDisp_Record_x			330
#define CallMenuDisp_Record_y			8
#define CALL_SYSTEM_SPRITE_INFORM_X		230
#define CALL_SYSTEM_SPRITE_INFORM_Y		193

#define CALLER_SYSTEM_SPRITE_INFORM_X(x)		(((x)-180)/2)
#define CALLER_SYSTEM_SPRITE_INFORM_Y(y)		(((y)-94)/2)

#define SYSTEM_SPRITE_INFORM_X(x)				(((x)-180)/2)
#define SYSTEM_SPRITE_INFORM_Y(y)				(((y)-94)/2)


extern LOCK_PROPERTY_T lockProperty;

typedef void (*DispListPage)(int);
extern char publicSettingDisplay[30][101];

uint16 GetCurIcon(void);
unsigned short GetLastNMenu(void);
//POS OSD_GetIconXY(uint16 iconNum);
//#define GetIconXY(iconNum)		OSD_GetIconXY(iconNum)
//POS GetIconXY(uint16 iconNum);
void SaveIcon(unsigned short icon); 
unsigned short GetSavedIcon(void); 

POS GetIconXYByTableNum(int table);
void DisplaySchedule(int x, int y, int curPage, int pageNum);
void PublicListUpProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage);
void PublicListDownProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage);
void PublicListRefreshProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage);
void PublicPageUpProcess(int *pageSelect, int maxIcon, int listNum, DispListPage DispPage);
void PublicPageDownProcess(int *pageSelect, int maxIcon, int listNum, DispListPage DispPage);

void PublicUpProcess(int *iconSelect, int maxIcon);
void PublicDownProcess(int *iconSelect, int maxIcon);
void ListSelect(uint8 line, uint8 onOff);
void UpdateOkSpriteNotify(void);
void BusySpriteDisplay(uint8 enable);
void EnterPublicSettingMenu(int settingSelect, int title, int maxNum, int settingValue, void (*UpdateValue)(int));

void publicSettingSelectFlagDisplay(void);
void EnterSettingMenu(FuncMenuType subMenu, int pushstack);
unsigned char GetRingId(int index);


typedef struct list_head  ListHead;

typedef struct
{
	int initFlag;
	int nodeNum;
}ListRun;

//��ʼ������ͷ
void InitList(ListHead *pList, ListRun *pListRun);
//���б�ͷ���ӽڵ�
void AddListNodeHead(ListHead *pList, void* pNewNode );
//���б�β���ӽڵ�
void AddListNodeTail(ListHead *pList, void* pNewNode );
//ɾ���ڵ�
void DelListNode( void* pNewNode );
//�ж������Ƿ�Ϊ��
int IsListEmpty(ListHead *pList);

void MENU_032_SlaveRegisterTimerProcess(int timeCnt);
void MENU_034_MasterRegisterTimerProcess(int timeCnt);
void MENU_033_MasterDeviceManageTimerProcess(int timeCnt);

typedef struct
{
	int iCon;
	int iConText;
}IconAndText_t;

typedef struct
{
	char name[41];
	char userName[41];
	char userPwd[41];
	int channel;
	char devUrl[101];
	char id[41];
	int faverite;
}IPC_RECORD;

typedef enum
{
	VIDEO_BINDING_NONE = 0,
	VIDEO_BINDING_IX_DEVICE,
	VIDEO_BINDING_IPC,
	VIDEO_BINDING_IPC_LIST,
}VIDEO_BINDING_TYPE;


#endif

