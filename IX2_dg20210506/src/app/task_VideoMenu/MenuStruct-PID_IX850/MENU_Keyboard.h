/**
  ******************************************************************************
  * @file    MENU_Keyboard.h
  * @author  caozhenbin
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the headers of the obj_Survey_Keyboard.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef OBJ_KBD_H
#define OBJ_KBD_H


/*******************************************************************************
                            Define Object Property 
*******************************************************************************/
#define	KEYBOARD_DISPLAY_X		        25
#define	KEYBOARD_DISPLAY_Y	    	    58 

#define KP_DISP_CHAR_W				20
#define KP_DISP_CHAR_H				40

#define	MAX_DISPLAY_ROW		        6
#define	MAX_DISPLAY_COL	    	    46 
#define	MAX_INPUTBUF_SIZE           100

#define	KEYPAD_CHAR                 1
#define	KEYPAD_NUM                  2
#define	KEYPAD_EDIT                 3

#define	BACKSPACE_KEY_VALUE         8
#define	SET_HIGHLIGHT_KEY_VALUE     255
#define	CAPSLOCK_KEY_VALUE          254
#define	TURN_TO_NUM_KEY_VALUE       253
#define	TURN_TO_CHAR_KEY_VALUE      252
#define	RETURN_KEY_VALUE            251
#define OK_KEY_VALUE                250

//czn_20181109_s
#define DIALKB_NUM		0
#define DIALKB_CHAR		1
//czn_20181109_e

typedef int (*available_check_func)(unsigned char* pbuf);

typedef struct
{
    unsigned char dispStartRow;
    unsigned char dispStartCol;
    unsigned char dispMaxRow;
    unsigned char dispMaxCol;
    int 		  dispColor;
    unsigned char inputSize; 
    unsigned char highLight;
    unsigned char dispFullFlag;
    unsigned char cursor;
    unsigned char capsLock;
    unsigned char inputbuf[MAX_INPUTBUF_SIZE + 1];
    unsigned char char_num;
	available_check_func	call_back_ptr;
}KbdProperty;

typedef struct
{
	int icon;
    int key_value;
} KeyProperty;

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
unsigned char API_KeyboardInit(unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
							   unsigned short dispMaxCol, int color, unsigned char* pString, unsigned char highLight, available_check_func call_back_ptr );

void API_KeyboardInputStart(void);
void API_KeyboardInputModeChange(void);
unsigned char API_KeyboardInputOneKey(unsigned char Keypad_type,int icon);

int API_KeyboardInputGetcapsLock( void );
int API_KeyboardInputDataDump( char* pbuffer );

#endif



