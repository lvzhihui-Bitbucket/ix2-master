#include "MENU_public.h"
extern void EmptyFunction(void);
const void* MENU_FUNCTION_TAB[][3] = 
{
	NULL,							NULL,								NULL,
	MENU_001_MAIN_Init, 			MENU_001_MAIN_Process,				MENU_001_MAIN_Exit,
	//EmptyFunction,					EmptyFunction,						EmptyFunction,
	MENU_002_DIAL_Init,				MENU_002_DIAL_Process,				MENU_002_DIAL_Exit,
	MENU_003_CODEUNLOCK_Init,		MENU_003_CODEUNLOCK_Process,		MENU_003_CODEUNLOCK_Exit,


};
const uint16 NotCtrlPressReleaseDispIcon[] = {
	-1,
#if 0
	ICON_203_Monitor,
	ICON_204_quad,
	ICON_205_CallScene,
	ICON_206_Namelist,
	ICON_017_MissedCalls,
	ICON_018_IncomingCalls,
	ICON_019_OutgoingCalls,
	ICON_020_playback,
	ICON_024_CallTune,
	ICON_025_general,
	ICON_026_InstallerSetup,
	ICON_027_SipConfig,
	ICON_029_IP_ADDR,
	ICON_030_CALL_NUMBER,
	ICON_031_OnsiteTools,
	ICON_032_System,
	ICON_033_Manager,
	ICON_034_ListView,
	ICON_035_ListAdd,
	ICON_036_ListDelete,
	ICON_037_ListEdit,
	ICON_038_ListCheck,
	//ICON_400_ListSync,	//temp //czn_20190525
	ICON_051_ExternalUnit,
	ICON_052_IM_Process,
	ICON_053_OS_Process,
	ICON_054_IPC_Process,
	ICON_218_PublicSetTitle,
	ICON_229_ShortcutSet1,
	ICON_230_ShortcutSet2,
	ICON_231_ShortcutSet3,
	ICON_232_ShortcutSet4,
	ICON_256_Edit,
	ICON_265_IPC_Monitor,
	ICON_266_IPC_Edit,
	ICON_267_IPC_Delete,
	ICON_269_MonListSelect,
	ICON_270_MS_List,
	ICON_040_AboutPage1,
	ICON_041_AboutPage2,
	ICON_042_AboutPage3,
	ICON_043_AboutPage4,
	ICON_044_AboutPage5,
	ICON_045_Upgrade,
	ICON_048_Device,
	ICON_049_Parameter,
	ICON_050_Reboot,
	ICON_IdCard_List, // 20190521
	ICON_IdCard_Add,
	ICON_IdCard_DelAll,
	ICON_CallRecordDelAll, // zfz_20190601
	ICON_379_Shortcut5, // zfz_20190601
#endif
};

const uint16 NotCtrlPressReleaseDispIconNum = sizeof(NotCtrlPressReleaseDispIcon)/sizeof(uint16);

int MainMenuMode = 0;			//0,std ,1 pjm 2 tlj
int MainMenuBdMapH = 0;		
int MainMenuBdTextH = 0;					//
int MainMenuCodeUnlockTextH = 0;		
int MainMenuTextColor= 0;		

int DialMenuMode=0;			//0,std, 1pjm				
int DialMenuActSpriteid = 0;			
int KeyBeepDisable = 0;

//int CodeMenuMode = 0;		//0,std, 1pjm
//int CodeMenuInputDispH = 0;		

int KPBackspaceMode = 0;		//0,std, 1pjm

int VillaMode = 0;

void Load_Menu_CustomerizedPara(void)
{
#if 0
	char buff[10];
	if(API_Event_IoServer_InnerRead_All(MAINMENU_MODE, buff)==0)
	{
		MainMenuMode = atoi(buff);
	}
	if(MainMenuMode !=0)
	{
		if(API_Event_IoServer_InnerRead_All(MAINMENU_BUILDINGMAP_HIGH, buff)==0)
		{
			MainMenuBdMapH = atoi(buff);
		}
		if(API_Event_IoServer_InnerRead_All(MAINMENU_BUILDINGTEXT_HIGH, buff)==0)
		{
			MainMenuBdTextH = atoi(buff);
		}
		if(API_Event_IoServer_InnerRead_All(MAINMENU_CODEUNLOCKTEXT_HIGH, buff)==0)
		{
			MainMenuCodeUnlockTextH = atoi(buff);
		}
		if(API_Event_IoServer_InnerRead_All(MAINMENU_TEXT_COLOR, buff)==0)
		{
			MainMenuTextColor = atoi(buff);
		}
	}
	if(API_Event_IoServer_InnerRead_All(DIALMENU_MODE, buff)==0)
	{
		DialMenuMode = atoi(buff);
	}
	if(DialMenuMode !=0)
	{
		if(API_Event_IoServer_InnerRead_All(DIALMENU_ACT_SPRITEID, buff)==0)
		{
			DialMenuActSpriteid = atoi(buff);
		}
	}
	#if 0
	if(API_Event_IoServer_InnerRead_All(CODEMENU_MODE, buff)==0)
	{
		CodeMenuMode = atoi(buff);
	}
	
	if(CodeMenuMode !=0)
	{
		if(API_Event_IoServer_InnerRead_All(CODEMENU_INPUTDISP_HIGH, buff)==0)
		{
			CodeMenuInputDispH = atoi(buff);
		}
	}
	#endif
	if(API_Event_IoServer_InnerRead_All(KP_BACKSPACE_MODE, buff)==0)
	{
		KPBackspaceMode = atoi(buff);
	}
	#if 0		//20210428
	if(API_Event_IoServer_InnerRead_All(VillaModeEn, buff)==0)
	{
		VillaMode = atoi(buff);
	}
	if(API_Event_IoServer_InnerRead_All(KeyBeepDis, buff)==0)
	{
		KeyBeepDisable = atoi(buff);
	}
	#endif
#endif
}

static int KeyBroadMode = 0;
int JustIsKeyBroadMode(void)
{
	int rev = KeyBroadMode;
	KeyBroadMode = 0;
	return rev;
}

void SetKeyBroadMode(void)
{
	KeyBroadMode = 1;
}

void ReturnMainMenu(void)
{
#if 0
	if(VillaMode==1)
	{
		if(GetCurMenuCnt() != MENU_094_VillaMain)
		{
			ResetMenuStack();
			StartInitOneMenu(MENU_094_VillaMain,0,1);
		}
	}
	else
#endif
	{
		if(GetCurMenuCnt() != MENU_001_MAIN)
		{
			ResetMenuStack();
			StartInitOneMenu(MENU_001_MAIN,0,1);
		}
	}
}

int screen_style_flag=0;


void Set_ScreenAutoStyle_ByNightvision(uint8 night_vision)
{
	char buff[5];
	int Screen_Contrast;
	//PowerOption( 6, 0 );
	API_Event_IoServer_InnerRead_All(Screen_Contrast_Mode, buff);
	
	Screen_Contrast = atoi(buff);
	printf("!!!!!!!!!!!! Screen_Contrast%d,night_v%d\n",Screen_Contrast,night_vision);
	if(Screen_Contrast != 0)
	{
		//API_NightVisionAutoCheck(0);//OS_StopTimer(&timer_ScreenAutoStyle);
		return;
	}
 	if(night_vision == 0 && screen_style_flag == 1)
 	{
		screen_style_flag = 0;
		if(GetCurMenuCnt() == 1)
		{
			// lzh_20190608_s
			ResetMenuExtBufferAll();
			// lzh_20190608_1
			StartInitOneMenu(1,0,0);
		}
	}
	else if(night_vision == 1 && screen_style_flag == 0)
	{
		screen_style_flag = 1;
		if(GetCurMenuCnt() == 1)
		{
			// lzh_20190608_s
			ResetMenuExtBufferAll();
			// lzh_20190608_1
			StartInitOneMenu(1,0,0);
		}
	}
}

void Load_ScreenStyle(void)	//czn_20190430
{
#if 0
	char buff[5];
	int Screen_Contrast;
	
	API_Event_IoServer_InnerRead_All(Screen_Contrast_Mode, buff);
	Screen_Contrast = atoi(buff);
	if(Screen_Contrast == 0)
	{
		screen_style_flag = 0;
		API_NightVisionAutoCheck(1);//OS_RetriggerTimer(&timer_ScreenAutoStyle);
	}
	else if(Screen_Contrast == 1)
	{
		screen_style_flag = 0;
		API_NightVisionAutoCheck(0);//OS_StopTimer(&timer_ScreenAutoStyle);
	}
	else
	{
		screen_style_flag = 1;
		API_NightVisionAutoCheck(0);//OS_StopTimer(&timer_ScreenAutoStyle);
	}
#endif
}

int Get_CurScreenStyle(void)
{
	return screen_style_flag;
}

void Set_CurScreenStyle(int new_style)
{
	screen_style_flag = new_style;
}

POS GetIconXY(uint16 iconNum)
{
	POS OSD_GetIconXY(uint16 iconNum);
	return OSD_GetIconXY(iconNum);
}

int get_password_disp(int para_len,char *para_buff,char *disp)
{
	if( GetLastNMenu() == MENU_022_Pwd)
	{
		para_len = para_len > MAX_CODE_LEN ? MAX_CODE_LEN : para_len;
		memset(disp, 0, MAX_CODE_LEN+1);
		
		if(para_len == MAX_CODE_LEN)
		{
			memcpy(disp, "****************", para_len);
		}
		else
		{
			memcpy(disp, "****************", para_len);
			strcat(disp, "_");
		}
	}
	else
	{
		para_len = para_len > MAX_PASSWORD_LEN ? MAX_PASSWORD_LEN : para_len;
		memset(disp, 0, MAX_PASSWORD_LEN+1);
		
		if(para_len == MAX_PASSWORD_LEN)
		{
			memcpy(disp, "****************", para_len);
		}
		else
		{
			memcpy(disp, "****************", para_len);
			strcat(disp, "_");
		}
	}
}
void* GetMenuInformData(void* arg)
{
	return (void*)( ((char*)arg) + sizeof(SYS_WIN_MSG) );
}

void BusySpriteDisplay(uint8 enable)
{
#if 0
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_201_PageDown, &pos, &hv);
	if(enable)
	{
		API_SpriteDisplay_XY(bkgd_w-32, pos.y+(hv.v - pos.y)/2, SPRITE_BusyState);
	}
	else
	{
		API_SpriteClose(bkgd_w-32, pos.y+(hv.v - pos.y)/2, SPRITE_BusyState);
	}
#endif
}

