
#ifndef _MenuStruct_Resource_H
#define _MenuStruct_Resource_H


typedef enum
{

	MENU_000_RETURN = 0, // �����menu��������
    MENU_001_MAIN,
    MENU_002_DIAL,
    MENU_003_CODEUNLOCK,
    MENU_004_NAMECALL,
    MENU_005_CALL,		
	MENU_006_INPUT_PASSWORD,		
	MENU_007_SETTING,
	MENU_008_DateAndTime,
	MENU_009_LANGUAGE,
	MENU_010_SIP_CONFIG,
	MENU_011_SetInstaller,
	MENU_012_INSTALL_CALL_NUM,
	MENU_013_INSTALL_IP,
	MENU_014_PUBLIC_SETTING,
	MENU_015_ABOUT,

	
	MENU_017_KEYPAD_CHAR=17,
	MENU_018_KEYPAD_NUM,	
	MENU_019_KEYPAD,

	MENU_021_IdCard=21,
	MENU_022_Pwd,
	MENU_023_INSTALL_OnsiteTools=23,    
	MENU_024_SEARCH_AND_PROG,
	MENU_025_SEARCH_ONLINE,
	MENU_026_ONLINE_MANAGE,
	MENU_027_PROG_ONLINE,
	MENU_028_RegistrationList,
	MENU_029_LIST_MANAGE,
	MENU_030_CallNamelist,

	//czn_20181110_s
	MENU_031_DIAL_NUM,
	MENU_032_DIAL_CHAR,
	MENU_033_Help_Msg,
	//czn_20181110_e

	MENU_034_FwUpgrade,	//czn_20181205
	MENU_035_FwUpgradeServer,
	MENU_036_InternetTime,
	MENU_037_SystemSetting,
	MENU_038_VirtualUserList,
  	MENU_039_VirtualUserEdit1,
	MENU_040_VirtualUserEdit2,
	MENU_041_RestoreToDefault,
	MENU_042_GetDeviceInfo,
	MENU_043_Online_Reboot,
	MENU_044_OnlineFwUpgrade,
	MENU_045_BackFwUpgrade,
	MENU_046_ParaManage,
	MENU_047_OnePara,
	MENU_048_ParaPublicComboBox,
	MENU_049_RemoteParaManage,
	MENU_050_OneRemotePara,
	MENU_051_RemoteManagePara,
	MENU_052_ManagePara,
	MENU_053_BTNList,	//czn_20190315
	MENU_054_BTNListSearch,	//czn_20190403
	MENU_055_EnterInstaller,	//czn_20190425
	MENU_056_InstallerMode,
	MENU_057_PwdOption,
	MENU_058_InstallerCallSub,
	MENU_059_SoftwareUpgrade,
	MENU_060_PING,
	MENU_061_PING_DISPLAY,
	MENU_062_AddOneCard,
	MENU_063_UserSelect,
	MENU_064_ViewFilter,
	MENU_065_CardList,
	MENU_066_CardData,
	MENU_067_CardSync,
	MENU_068_CallRecord, // zfz_20190611
	MENU_069_RES_Manger,
	MENU_070_RES_Select,
	MENU_071_AdvancedTools,
	MENU_072_Call_Nbr_Reset,
	MENU_073_ResSyncSelect,
	MENU_074_ResView,
	MENU_075_ResInfo,
	MENU_076_Autotest,
	MENU_077_AutotestDevSelect,
	MENU_078_AutotestLogView,
	MENU_079_CallTestStat,
	MENU_080_AutotestTools,
	MENU_081_VirtualUserAdd,	//czn_20190715
	MENU_082_VirtualUserManager,
	MENU_083_VirtualUserSync,
	MENU_084_UnlockSetting,
	MENU_085_RestoreAndBackup,
	MENU_086_QuickAccess,
	MENU_087_DTIMEdit,
	MENU_088_DTIMList,
	MENU_089_DTIMAdd,
	MENU_090_BackupAndRestore2,
	MENU_091_BackupList,
	MENU_092_RestoreList,
	MENU_093_GlList,
	MENU_094_VillaMain,
	MENU_095_MulLangKeyPad,
	MENU_096_MulLangKeyPad_Char,
	MENU_097_NM_MAIN,
	MENU_098_NM_LanSetting,
	MENU_099_NM_WlanSetting,
	MENU_DAT_MAX,
    MENU_255_NONE = 0xff,
} FuncMenuType;

void MENU_001_MAIN_Init(int uMenuCnt);
void MENU_001_MAIN_Exit(void);
void MENU_001_MAIN_Process(void* arg);

void MENU_002_DIAL_Init(int uMenuCnt);
void MENU_002_DIAL_Exit(void);
void MENU_002_DIAL_Process(void* arg);

void MENU_003_CODEUNLOCK_Init(int uMenuCnt);
void MENU_003_CODEUNLOCK_Exit(void);
void MENU_003_CODEUNLOCK_Process(void* arg);

#define	ICON_001_dial    	            1
#define	ICON_002_name_call				2
#define	ICON_003_code_unlock            3
#define	ICON_004_kb_return              4
#define	ICON_005_kb_search				5
#define	ICON_006_kb_change              6
#define	ICON_007_kb_call	            7
#define	ICON_008_kb_unlock	            8
#define	ICON_009_call_close	            9
#define	ICON_010_nl_return				10
#define	ICON_011_nl_pagedn	            11
#define	ICON_012_nl_pageup              12
#define	ICON_013_nl_list1				13
#define	ICON_014_nl_list2   	        14
#define	ICON_015_nl_list3				15
#define	ICON_016_nl_list4               16
#define	ICON_017_nl_list5	            17
#define	ICON_018_nl_list6				18
#define	ICON_019_kb_backspace			19
#define	ICON_020_kb_digit0              20
#define	ICON_021_kb_digit1           	21
#define	ICON_022_kb_digit2              22
#define	ICON_023_kb_digit3              23
#define	ICON_024_kb_digit4              24
#define	ICON_025_kb_digit5              25
#define	ICON_026_kb_digit6         		26
#define	ICON_027_kb_digit7              27
#define	ICON_028_kb_digit8              28
#define	ICON_029_kb_digit9             	29
//#define	ICON_043_kb_OK             		43

#define	ICON_031_list1_process			30
#define	ICON_032_list2_process			31
#define	ICON_033_list3_process			32
#define	ICON_034_list4_process			33
#define	ICON_035_list5_process			34

//czn_20181110_s
#define	ICON_035_kb_jkl           		35
#define	ICON_036_kb_mno           		36
#define	ICON_037_kb_pqrs           		37
#define	ICON_038_kb_tuv            		38
#define	ICON_039_kb_wxyz           		39
#define	ICON_040_kb_char            		40
#define	ICON_041_kb_num           		41
#define	ICON_042_kb_space            		42
#define	ICON_043_kb_OK             		43
#define	ICON_044_kb_symbol            		44
#define	ICON_045_kb_abc           		45
#define	ICON_046_kb_def            		46
#define	ICON_047_kb_ghi            		47
//czn_20181110_e
#define	ICON_048_Help           		48
#define	ICON_049_Settings          		49
#define	ICON_050_Home          			50
#define	ICON_051_short_list1          	51
#define	ICON_052_short_list2          	52
#define	ICON_053_short_list3          	53
#define	ICON_054_short_list4          	54
#define	ICON_055_short_list5          	55
#define	ICON_056_short_list6          	56
#define	ICON_057_mainHelp          		57

//czn_20190327_s
#define	ICON_058_ScreenStyle          		58
#define	ICON_059_MainTimeLine          	59
#define	ICON_060_MainLogoLine         	60
//czn_20190327_e
//czn_20190327_s
#define	ICON_061_MainSelRight			61
#define	ICON_062_MainSelLeft			62
//czn_20190327_e

#define	ICON_063_ListAction				63			//czn_20190425
//czn_20190506_s
#define	ICON_064_FilterEdit				64				
//czn_20190506_e

#define	ICON_065_ListChange				65
#define	ICON_QuickAccess1				66
#define	ICON_QuickAccess2				67
#define	ICON_QuickAccess3				68
#define	ICON_QuickAccess4				69
#define	ICON_QuickAccess5				70
#define	ICON_QuickAccess6				71
#define	ICON_QuickAccess7				72
#define	ICON_QuickAccess8				73
#define	ICON_QuickAccess9				74
#define	ICON_QuickAccess10				75
#define	ICON_QuickAccess11				76
#define	ICON_QuickAccess12				77
#define	ICON_QuickAccess13				78
#define	ICON_QuickAccess14				79
#define	ICON_QuickAccess15				80

// lzh_20180823_s
#define	ICON_101_Key00      101
#define	ICON_102_Key01      102
#define	ICON_103_Key02      103
#define	ICON_104_Key03      104
#define	ICON_105_Key04      105
#define	ICON_106_Key05      106
#define	ICON_107_Key06      107
#define	ICON_108_Key07      108
#define	ICON_109_Key08      109
#define	ICON_110_Key09      110
#define	ICON_111_Key10      111
#define	ICON_112_Key11      112
#define	ICON_113_Key12      113
#define	ICON_114_Key13      114
#define	ICON_115_Key14      115
#define	ICON_116_Key15      116
#define	ICON_117_Key16      117
#define	ICON_118_Key17      118
#define	ICON_119_Key18      119
#define	ICON_120_Key19      120
#define	ICON_121_Key20      121
#define	ICON_122_Key21      122
#define	ICON_123_Key22      123
#define	ICON_124_Key23      124
#define	ICON_125_Key24      125
#define	ICON_126_Key25      126
#define	ICON_127_Key26      127
#define	ICON_128_Key27      128
#define	ICON_129_Key28      129
#define	ICON_130_Key29      130
#define	ICON_131_Key30      131
#define	ICON_132_Key31      132
#define	ICON_133_Key32      133
#define	ICON_134_Key33      134
#define	ICON_135_Key34      135
#define	ICON_136_Key35      136
#define	ICON_137_Key36      137
#define	ICON_138_Key37      138
#define	ICON_139_Key38      139
#define	ICON_140_Key39      140
#define	ICON_141_Key40      141
#define	ICON_142_Key41      142
#define	ICON_143_Key42      143
#define	ICON_144_Key43      144
#define	ICON_145_Key44      145
#define	ICON_146_Key45      146
#define	ICON_147_Key46      147
#define	ICON_148_Key47      148
#define	ICON_149_Key48      149
#define	ICON_150_Key49      150
#define	ICON_151_Key50      151
#define	ICON_152_Key51      152
#define	ICON_153_Key52      153
#define	ICON_154_Key53      154
#define	ICON_155_Key54      155
#define	ICON_156_Key55      156
#define	ICON_157_Key56      157
#define	ICON_158_Key57      158
#define	ICON_159_Key58      159
#define	ICON_160_Key59      160
#define	ICON_161_Key60      161
#define	ICON_162_Key61      162
#define	ICON_163_Key62      163
#define	ICON_164_Key63      164
#define	ICON_165_Key64      165
#define	ICON_166_Key65      166
#define	ICON_167_Key66      167
#define	ICON_168_Key67      168
#define	ICON_169_Key68      169
#define	ICON_170_Key69      170
#define	ICON_171_Key70      171
#define	ICON_172_Key71      172
#define	ICON_173_Key72      173
#define	ICON_174_Key73      174
#define	ICON_175_Key74      175
#define	ICON_176_Key75      176
#define	ICON_177_Key76      177
#define	ICON_178_Key77      178
#define	ICON_179_Key78      179
#define	ICON_180_KeyEdit	180
// lzh_20180823_e
#define	ICON_ParaGroup0     181
#define	ICON_ParaGroup1     182
#define	ICON_ParaGroup2     183
#define	ICON_ParaGroup3     184
#define	ICON_ParaGroup4     185
#define	ICON_ParaGroup5		186
//czn_20190425_s
#define	ICON_OnsiteManagePwdFree	187
#define	ICON_AlwaysInInstaller		188
#define	ICON_ExitInstaller			189
#define	ICON_OnlineDev				190
#define	ICON_Setting				191
#define	ICON_CallRecord				192
//czn_20190425_e

// lzh_20180905_s

#define	ICON_DateAndTime            	200
#define	ICON_Language	          		201
#define	ICON_SipConfig	          		202
#define	ICON_Installer	          		203
#define	ICON_About	          			204

#define	ICON_SipServer					205
#define	ICON_SipPort					206
#define	ICON_SipDivert					207
#define	ICON_SipAccount					208
#define	ICON_SipPassword				209
#define	ICON_SipUseDefault				210
#define	ICON_RemoteMonCode				211
#define	ICON_RemoteCallCode				212
#define	ICON_SipDivPwd					213
#define	ICON_ReRegister					214

#define	ICON_IpAddress					215
#define	ICON_CallNumber					216	
#define	ICON_OnsiteTools				217	
#define	ICON_Parameter					218
#define	ICON_ListManager				219

#define	ICON_RM_ADDR					220	
#define	ICON_MS_Number					222
#define	ICON_Name						223 	
#define	ICON_Global_Nbr					224
#define	ICON_Local_Nbr					225

#define	ICON_IPAssigned					226		
#define	ICON_IPAddress					227
#define	ICON_IPSubnetMask				228 	
#define	ICON_IPGateway					229	
#define	ICON_AddressHealthCheck			230
#define	ICON_LISTDisplayOption			231

//czn_20181205_s
#define	ICON_FwUpgrade_Server				232
#define	ICON_FwUpgrade_DLCode				233
#define	ICON_FwUpgrade_CodeInfo				234
#define	ICON_FwUpgrade_Notice				235
#define	ICON_FwUpgrade_Operation			236
#define	ICON_FwUpgrade						237
//czn_20181205_e
#define	ICON_InternetTime					238
#define	ICON_TimeServer						239
#define	ICON_TimeUpdate						240

#define	ICON_SPKVolume						241
#define	ICON_MICVolume						242
#define	ICON_RingVolume						243
#define	ICON_VirtualUserSipPwd				244
#define	ICON_ManagePassword					245
#define	ICON_DivertAccountRegister			246
#define	ICON_DS_Number						247
#define	ICON_RestoreToDefault				248

#define	ICON_ProgCall_Nbr					249
#define	ICON_Reboot							250

#define	ICON_DeviceRegistration				251
#define	ICON_SearchAndProg					252
#define	ICON_VirtualUser					253

#define	ICON_BD_NBR							254
#define	ICON_Device							255
#define	ICON_SearchTime						256
#define	ICON_Search							257

#define	ICON_AddFromRegistrations			258
#define	ICON_AddBySearchOnline				259
#define	ICON_UpdateFromOnlineDevice			260
#define	ICON_CallNamelist					261

#define	ICON_DateMode						262
#define	ICON_TimeMode						263
#define	ICON_Date							264
#define	ICON_Time							265
#define	ICON_TimeZone						266
#define	ICON_TimeAutoUpdate					267

#define	ICON_RestoreGeneralData 			268
#define	ICON_RestoreUserData				269
#define	ICON_RestoreWirelessData			270
#define	ICON_RestoreInstallerData			271
#define	ICON_RestoreAndBackup				272

#define	ICON_DeviceInfo						273
#define	ICON_ParaId							274
#define	ICON_ParaName						275
#define	ICON_ParaDesc						276
#define	ICON_ParaValue						277
//czn_20190403_s
#define	ICON_BTNListManager					278
//czn_20190403_e
// lzh_20180905_e
#define	ICON_AllDevGotoUpdate					279			//czn_20190506

#define	ICON_AccessPwd					280				
#define	ICON_AccessId						281
#define	ICON_AccessPwdOption				282
#define	ICON_AccessPwdEdit					283
#define	ICON_AccessPwdOption1				284
#define	ICON_AccessPwdOption2				285
#define	ICON_AccessPwdOption3				286
#define	ICON_AccessPwdOption4				287
#define	ICON_ADD_CARD						288
#define	ICON_VIEW_CARD					289
#define	ICON_MANAGE_CARD					290

#define	ICON_PING_IP						291
#define	ICON_PING_TIMEOUT					292
#define	ICON_PING_COUNT						293
#define	ICON_PING_SIZE						294
#define	ICON_PING							295
#define	ICON_CARD_ROOM					296
#define	ICON_CARD_NAME					297
#define	ICON_CARD_ID						298
#define	ICON_CARD_DATE					299
#define	ICON_CARD_TO_SD					300
#define	ICON_CARD_FROM_SD				301
#define	ICON_CARD_TO_SYNC				302
#define	ICON_CARD_FROM_SYNC				303
#define	ICON_CARD_FORMAT					304

#define  ICON_RES_Manger					305
#define ICON_ADVANCED_TOOLS				306
#define ICON_BATCH_CALL_NBR				307
#define ICON_BATCH_RES						308
#define ICON_BATCH_PARA					309
#define ICON_BATCH_REBOOT					310
#define ICON_BATCH_PWD					311
#define ICON_BATCH_CHECK					312
#define ICON_BATCH_SUMMARY				313
#define ICON_BATCH_AUTO_TEST				314
#define ICON_BATCH_WIFI					315
#define ICON_InstallerPwdEdit					316
#define	ICON_Autotest						317
#define	ICON_AutotestStatistics				318
#define 	ICON_AutotestLogClear				319
#define 	ICON_ViewAutotestLog				320
#define 	ICON_AutotestLogCopy				321
#define	ICON_AutotestSource					322
#define	ICON_AutotestTarget					323
#define	ICON_AutotestCount					324
#define	ICON_AutotestInterval				325
#define	ICON_AutotestStart					326
#define ICON_Enable_RES						327
#define ICON_VirtualUserUnlockPwd			328		//czn_20190715
#define ICON_VirtualUserSipTest				329
#define ICON_VirtualUserSipInfo				330
#define ICON_CreatVMFromRes				331

#define	ICON_ParaGroupUnlock     		332
#define	ICON_ParaGroupOther     		333

#define	ICON_OperationSelect			334
#define	ICON_RangeSelect				335
#define	ICON_BackupSelect				336
#define	ICON_Location					337
#define	ICON_Start						338

#define ICON_Backup						339
#define ICON_RestoreFromBackup			340
#define ICON_RestoreFactorySettings		341
#define ICON_RestoreUserSettingOnly		342

#define ICON_ScreenSetting				343
#define ICON_HardwareSetting			344
#define ICON_VoicePromptSetting			345
#define ICON_NetworkSetting				346
#define ICON_InputAndFormatSetting		347

#define ICON_ManagePwdEdit				348
	
#define	ICON_IpNode    					349
#define	ICON_DtAddr    					350
#define	ICON_AddImNum   				351
#define	ICON_DTIM    					352

#define	ICON_VoiceLanguage    			353
#define	ICON_MenuLanguage    			354

#define	ICON_400_Guard1   			400
#define	ICON_401_Guard2    			401
#define	ICON_402_VillaDial    	            402
#define	ICON_403_MLKPResSelect		403
#define	ICON_404_MLKPKey01    			404
#define	ICON_405_MLKPKey02    			405
#define	ICON_406_MLKPKey03    			406
#define	ICON_407_MLKPKey04    			407
#define	ICON_408_MLKPKey05    			408
#define	ICON_409_MLKPKey06    			409
#define	ICON_410_MLKPKey07    			410
#define	ICON_411_MLKPKey08    			411
#define	ICON_412_MLKPKey09    			412
#define	ICON_413_MLKPKey10    			413
#define	ICON_414_MLKPKey11    			414
#define	ICON_415_MLKPKey12    			415
#define	ICON_416_MLKPKey13    			416
#define	ICON_417_MLKPKey14    			417
#define	ICON_418_MLKPKey15    			418
#define	ICON_419_MLKPKey16    			419
#define	ICON_420_MLKPKey17    			420
#define	ICON_421_MLKPKey18    			421
#define	ICON_422_MLKPKey19    			422
#define	ICON_423_MLKPKey20    			423
#define	ICON_424_MLKPKey21    			424
#define	ICON_425_MLKPKey22    			425
#define	ICON_426_MLKPKey23    			426
#define	ICON_427_MLKPKey24    			427
#define	ICON_428_MLKPKey25    			428
#define	ICON_429_MLKPKey26    			429
#define	ICON_430_MLKPKey27    			430
#define	ICON_431_MLKPKey28    			431
#define	ICON_432_MLKPKey29    			432
#define	ICON_433_MLKPKey30    			433
#define	ICON_434_MLKPKey31    			434
#define	ICON_435_MLKPKey32    			435
#define	ICON_436_MLKPKey33    			436
#define	ICON_437_MLKPKey34   			437
#define	ICON_438_MLKPKey35    			438
#define	ICON_439_MLKPKey36    			439
#define	ICON_440_MLKPKey37    			440
#define	ICON_441_MLKPKey38    			441

#define	ICON_AC_Number						500
#define	ICON_GL_NBR					501

#define 	ICON_800_KeyState				800
#define	ICON_888_ListView				888

	// sprite define
#define SPRITE_INPUT_FOCUS_ENABLE		48
#define SPRITE_INPUT_FOCUS_DISABLE		49
#define SPRITE_INPUT_FOCUS_MONTH		50
#define SPRITE_INPUT_FOCUS_YEAR			51
#define SPRITE_RECORDING				52
#define SPRITE_TALKING					53
#define SPRITE_UNLOCK					54
#define SPRITE_MONITOR					55
#define SPRITE_KEYPAD_HELP				56
#define SPRITE_USER						57
#define SPRITE_DOORSTATION				58
#define SPRITE_VIDEORESOURCE			59
#define SPRITE_IM_LIST					60
#define SPRITE_MANUAL_RECORD			61
#define SPRITE_RECORD_DETAIL			62
#define SPRITE_CALL_RECORD				63
#define SPRITE_ABOUT					64
#define SPRITE_LINK_BOX					65
#define SPRITE_FAIL_BOX					66
#define SPRITE_UNLOCK_BOX				67
#define SPRITE_FAIL2_BOX				68	//czn_20170328

#define SPRITE_075_Call					75
#define SPRITE_076_Confirm				76
#define SPRITE_077_Update				77
#define SPRITE_078_Delete				78
#define SPRITE_079_Save					79

#define SPRITE_WIFI_QUALITY1			80
#define SPRITE_WIFI_QUALITY2			81
#define SPRITE_WIFI_QUALITY3			82
#define SPRITE_WIFI_QUALITY4			83
#define SPRITE_SELECT					84
#define SPRITE_NOT_SELECT				85

#define SPRITE_ScheduleBottomShort		87
#define SPRITE_ScheduleBottom			88
#define SPRITE_ScheduleTop				89
#define SPRITE_MissCall_OFF				90
#define SPRITE_NoDisturb_OFF			91
#define SPRITE_Transfer_OFF				92
#define SPRITE_MainWifi_OFF				93
#define SPRITE_MainWifi_QUALITY1		94
#define SPRITE_MainWifi_QUALITY2		95
#define SPRITE_MainWifi_QUALITY3		96
#define SPRITE_MainWifi_QUALITY4		97
#define SPRITE_BUSY						98
#define SPRITE_SWITCH_HELP				99
#define SPRITE_CONFIRM					100
#define SPRITE_IF_CONFIRM				101
#define SPRITE_MissCall_ON				102
#define SPRITE_NoDisturb_ON				103
#define SPRITE_Transfer_ON				104
#define SPRITE_MainWifi_Close			105
#define SPRITE_BusyState				106
#define SPRITE_SystemBusy				114
#define SPRITE_UNLOCK2					115
//czn_20190121_s
#define SPRITE_SipSerOk					116
#define SPRITE_SipSerFail				117
#define SPRITE_SipSerCaution			118
//czn_20190121_e
#define SPRITE_CALL_FAIL				123
#define SPRITE_HidePageTurning			126
#define SPRITE_GeneralSetting					128		//czn_20190730
#define SPRITE_PW_HAVEINPUT				134
#define SPRITE_PW_NOINPUT				133

#define SPRITE_NetNotExit				135
#define SPRITE_NetOK					136
#define SPRITE_NetCaution				137
#define SPRITE_ListChangeName			138
#define SPRITE_ListChangeRoom			139
#define SPRITE_CallBusyState			140

#define SPRITE_BUILDING_MAP				200
#define SPRITE_LOGO						201

//czn_20190327_s
#define SPRITE_MainSchedule1					202
#define SPRITE_MainSchedule2					203
#define SPRITE_MainSchedule3					204
#define SPRITE_MainFuncSetting					205
#define SPRITE_MainFuncHelp						206
//czn_20190327_e

#define SPRITE_MainUnlock						207		//czn_20190506

#define SPRITE_InstallerModeBar						208
//czn_20190506_s
#define SPRITE_KeypadBackspace						209
#define SPRITE_KeypadExit							210
//czn_20190506_e
//czn_20190613_s
#define SPRITE_KeypadCall							211
#define SPRITE_KeypadUnlock							212
//czn_20190613_e
//czn_20170313_s
#define SPRITE_VMCALL								213
#define SPRITE_KeypadExit2							214

#define SPRITE_KeypadSharp							215
#define SPRITE_SmallKeypadCall							216
#define SPRITE_SmallKeypadSharp							217

#define SPRITE_MLKPPage1							218
#define SPRITE_MLKPPage2							219

#define SPRITE_800_PageDwon				180
#define SPRITE_801_PageDwonS			181
#define SPRITE_802_PageUp				182
#define SPRITE_803_PageUpS				183

#define BmpPage_Help					1
#define BmpPage_Info					2


#define PromptBox_Linking				1
#define PromptBox_Fail					2
#define PromptBox_Unlock				3
#define PromptBox_Fail2					4	//czn_20170328

#define ACT_DEVINFO_POS_X				75
#define ACT_DEVINFO_POS_Y				8
#define ACT_DEVINFO_LENGTH			20

#define TALKING_SPRITE_POS_X			252
#define TALKING_SPRITE_POS_Y			8

#define UNLOCK_SPRITE_POS_X			274
#define UNLOCK_SPRITE_POS_Y			8

#define RECORDING_SPRITE_POS_X		296
#define RECORDING_SPRITE_POS_Y		8

extern int MainMenuMode;			//0,std ,1 pjm 2 tlj
extern int MainMenuBdMapH;		
extern int MainMenuBdTextH;					//
extern int MainMenuCodeUnlockTextH;		
extern int MainMenuTextColor;		

extern int DialMenuMode;			//0,std, 1pjm				
extern int DialMenuActSpriteid;			

//extern int CodeMenuMode;		//0,std, 1pjm
//extern int CodeMenuInputDispH;	
extern int KPBackspaceMode;
extern int VillaMode;
extern int KeyBeepDisable;

#define DISPLAY_TITLE_COLOR			COLOR_BLACK//(Get_CurScreenStyle()?COLOR_WHITE:COLOR_BLACK)				//czn_20190327
#define DISPLAY_LIST_COLOR				COLOR_SLIVER_GRAY//(Get_CurScreenStyle()?COLOR_WHITE:COLOR_SLIVER_GRAY)//COLOR_SLIVER_GRAY		//czn_20190327
#define DISPLAY_KEYBOARD_COLOR		COLOR_BLACK//	(Get_CurScreenStyle()?COLOR_WHITE:COLOR_BLACK)	
#define DISPLAY_CALLING_COLOR			COLOR_BLACK//(Get_CurScreenStyle()?COLOR_WHITE:COLOR_BLACK)	
#define DISPLAY_STATE_COLOR			COLOR_RED

#define MAX_PASSWORD_LEN	20
#define MAX_CODE_LEN		4
POS GetIconXY(uint16 iconNum);
#endif

