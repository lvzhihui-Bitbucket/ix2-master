
#ifndef _OBJ_MENU_LIST_H
#define _OBJ_MENU_LIST_H


//触摸屏坐标转显示坐标
#define TouchToDisplayX(touchX) 				(1280 - (touchX))
#define TouchToDisplayY(touchY) 				(800 - (touchY))

#define GetIconTypeSelH(iconHeight) 			((iconHeight)/3)

#define GetIconTypeFunSpiY(startY, height) 		((startY) + (height)*3/8)

//控制框的位置和大小
#define MENU_LIST_boxX							0
#define MENU_LIST_boxY							0
#define MENU_LIST_boxWidth						480
#define MENU_LIST_boxHeight						800
#define MENU_LIST_titleHeight					116

#define MENU_LIST_textBoxX						390
#define MENU_LIST_textBoxY						140
#define MENU_LIST_textBoxWidth					870
#define MENU_LIST_textBoxHeight					542

//行数列数等
#define MENU_LIST_ITEM_MAX						(10)
#define MENU_LIST_CHAR_MAX						200
#define MENU_LIST_VAL_CHAR_MAX					60

#define MENU_LIST_VAL_COLOR						COLOR_RED
#define MENU_LIST_STR_COLOR						COLOR_SLIVER_GRAY
#define MENU_LIST_REV_COLOR						COLOR_LIST_MENU_ICON

#define MENU_LIST_TEXT_SIZE_BIG					1
#define MENU_LIST_TEXT_SIZE_SMALL				2

#define MENU_LIST_YTPE_LIST_STR_OFFSET_X		32

//进度条位置定义
#define MENU_LIST_scheduleBotW					35
#define MENU_LIST_scheduleBotH					542

#define MENU_LIST_scheduleTopW					33
#define MENU_LIST_scheduleTopH					89

//特殊ICON坐标定义
#define MENU_LIST_upX							875
#define MENU_LIST_upY							0
#define MENU_LIST_upWidth						202
#define MENU_LIST_upHeight						113

#define MENU_LIST_downX							1078
#define MENU_LIST_downY							0
#define MENU_LIST_downWidth						202
#define MENU_LIST_downHeight					113

//特殊ICON号定义
#define MENU_LIST_ICON_UP						(-2)
#define MENU_LIST_ICON_DOWN						(-3)
#define MENU_LIST_ICON_NONE						(-1)

#define MENU_LIST_SUB_ICON_FUN					0
#define MENU_LIST_SUB_ICON_SEL					1
#define MENU_LIST_SUB_ICON_EDI					2

#define NONE_ICON_PIC							0

#define MENU_LIST_ICON_PIC_DOWN					SPRITE_800_PageDwon
#define MENU_LIST_ICON_PIC_DOWNS				SPRITE_801_PageDwonS
#define MENU_LIST_ICON_PIC_UP					SPRITE_802_PageUp
#define MENU_LIST_ICON_PIC_UPS					SPRITE_803_PageUpS

#define MENU_LIST_ICON_PIC_LIST					SPRITE_VideoBindingIPCList

#define MENU_LIST_ScheduleBottom				SPRITE_ScheduleBottom
#define MENU_LIST_ScheduleTop					SPRITE_ScheduleTop

typedef enum
{
	ICON_PIC_NONE,
	ICON_PIC_UP,
	ICON_PIC_DOWN,
	ICON_PIC_IPC,
} ICON_PIC_TYPE;

typedef struct
{
    ICON_PIC_TYPE iconType;

    int preSprId;				//最左边sprite id
    int sufSprId;				//最右边sprite id
    
    int strLen;					//显示字符长度
    int valLen;					//显示字符长度
    
    char str[MENU_LIST_CHAR_MAX];	//一页的显示内容
    char val[MENU_LIST_VAL_CHAR_MAX];	//一页的显示内容
}ICON_DISP_T;

typedef struct
{
	int dispCnt;
    ICON_DISP_T disp[MENU_LIST_ITEM_MAX];
} LIST_DISP_T;

//定义获取一页显示数据的回调函数
typedef  void (*MenuListGetDisplay)(int startIndex, int num, LIST_DISP_T* disp);

typedef struct
{
    int mainIcon;											//主ICON
    int subIcon;											//副ICON
} LIST_ICON;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	int spiAlign;

	//str显示参数
    int strx;
	int stry;
	int strw;
	int strh;
	int strAlign;
	
	//vlaue显示参数
    int vlax;
	int vlay;
	int vlaw;
	int vlah;
	int vlaAlign;
} ICON_FUN_T;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	//选中显示起始位置
	int spiAlign;
} ICON_SEL_T;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	//选中显示起始位置
	int spiAlign;
} ICON_EDI_T;

typedef struct
{
	int icon;
	
	int	revColor;			//反显颜色
	ICON_PIC_TYPE iconType;

	int x;
	int y;
	int w;
	int h;
	
    ICON_FUN_T fun;
    ICON_SEL_T sel;
    ICON_EDI_T edi;
} LIST_ICON_PROPERTY_T;

typedef struct
{
	ICON_PIC_TYPE iconType;
    int selFpic;
    int selBpic;
    int funFpic;
    int funBpic;
    int ediFpic;
    int ediBpic;
} ICON_TYPE_TO_PIC;


typedef enum
{
	ICON_1X2,
	ICON_1X3,
	ICON_2X2,
	ICON_2X3,
    ICON_2X4,
    
	TEXT_5X1,
	TEXT_5X2,
	TEXT_6X1,
	TEXT_10X1,
} LIST_TYPE;
	
typedef enum
{
	ICON_TYPE,
	TEXT_TYPE,
} LIST_DISP_TYPE;

typedef struct
{
	LIST_TYPE listType;
	int listIconEn;
	int selEn;
	int ediEn;
	int valEn;
	int textValOffset;
	int textOffset;
	int valColor;
	int textColor;
	int valAlign;
	int textAlign;
	unsigned int listCnt;
	MenuListGetDisplay fun;
	char* titleStr;
	int titleStrLen;
	int currentPage;
} LIST_INIT_T;

typedef struct
{
    LIST_TYPE listType;										//list 类型  TEXT_LIST， ICON_LIST
	LIST_DISP_TYPE dispType;								//显示类型

	//控制框定义
	int boxX;
	int boxY;
	int boxWidth;
	int boxHeight;


	//文本框定义
    int textBoxPositionX;									//文本框位置
    int textBoxPositionY;									//文本框位置
	int textBoxWidth;										//文本框宽
	int textBoxHeight;										//文本框高

	
	int titleHeight;										//标题栏高
	int lineHeight;											//每行的高度
	int lineWidth;											//每行的宽度
	
	int selEn;												//开启选择ICON
	int ediEn;												//开启编辑ICON
	int listIconEn;											//是否响应ICON
	int valEn;												//是否有值显示

	int titleStr[MENU_LIST_CHAR_MAX];						//标题显示字符
	int titleStrLen;										//标题字符长度
	
	//上翻页ICON
	LIST_ICON_PROPERTY_T up;

	//下翻页ICON
	LIST_ICON_PROPERTY_T down;

	//List ICON
	LIST_ICON_PROPERTY_T list[MENU_LIST_ITEM_MAX];

	int iconList[MENU_LIST_ITEM_MAX+2];
	int iconListCnt;

	//滚动条
	int scheduleBotW;
	int scheduleBotH;
	int scheduleBotSpriteId;
	int scheduleTopW;
	int scheduleTopH;
	int scheduleTopSpriteId;

    int columnNum;											//列数
    int lineNum;											//行数
    
    int textColor;											//字体颜色
	int valColor;
    int textSize;											//字号
    
    int textListOffsetX;									//列表显示偏移X
	int textValOffset;
	int valAlign;
	int textAlign;
    
    int maxPage;											//最大页
    int listNum;											//list条数
    int currentPage;										//当前页
    int onePageListMax;										//每页最大list数量

    MenuListGetDisplay getDisplay;
    
} LIST_PROPERTY;

//初始化对象，并显示第一页，带进需要随时改变的参数
int InitMenuList(LIST_INIT_T listInit);
void MenuListDiplayPage(void);

LIST_INIT_T ListPropertyDefault(void);


//当大ICON按下的处理
LIST_ICON MenuListIconProcess(int x, int y);

//当大ICON释放的处理
LIST_ICON MenuListIconClick(int x, int y);

//回到首页
void ResetMenuListCurrentPage(void);

//跳转到指定页数
int MenuListSetCurrentPage(int page);

//获取当前页码
int MenuListGetCurrentPage(void);

//翻页option 0-下翻， 1-上翻
void MenuListPageUpOrDown(int option);

//显示菜单列表
static void DisplayOnePage(void);

static void DisplayTitle(void);
static void DisplayTurnPageIcon(void);
static void DisplayListIcon(LIST_DISP_T display);

static ICON_TYPE_TO_PIC GetPicByIconType(ICON_PIC_TYPE iconType);
static void SetRowAndColumnAndTextSize(LIST_TYPE listType);
static void CreateIcons(int onePageListMax);
static void DisplayPageNum(void);


//反显或取消反显ICON
static int SelectMenuListIcon(int enable, LIST_ICON icon);

//通过触摸坐标获取ICON值
static LIST_ICON MenuListGetIcon(int x, int y);

//显示进度条
static void MenuListDisplaySchedule(int x, int y, int curPage, int pageNum);


//获取一条列表的大小和位置
static LIST_ICON_PROPERTY_T GetOneIconProperty(int mainIcon);


#endif



