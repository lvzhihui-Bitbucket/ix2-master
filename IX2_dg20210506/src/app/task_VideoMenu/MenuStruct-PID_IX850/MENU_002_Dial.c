
#include "MENU_002_Dial.h"
//#include "../../task_Phone/task_Caller/task_Caller.h"
#include "define_Command.h"
//#include "../obj_Survey_Keyboard.h"
#include "task_CallServer.h"
#include "obj_IpCacheTable.h"
#include "task_VoicePrompt.h"
//#include "obj_VirtualUserTable.h"
#include "obj_ImNameListTable.h"

static void SwitchDialKbType(unsigned char type);

OS_TIMER Timer_DialKbKeep;

static void Callback_Timer_DialKbKeep(void);
//czn_20190516_s
void InputCtrl_ParameterInit(void);
void InputCtrl_InputHaveChange(void);
void InputCtrl_InputAdd(void);
void InputCtrl_InputBackSpace(void);
void InputCtrl_InputSwitchKbType(void);
void InputCtrl_HaveDail(void);
void InputCtrl_Timer_CallBack(void);
void InputCtrl_NoInputTimeout_Process(void);
//void InputCtrl_NoInputTimerStop(void);
//czn_20190516_e


#define MAX_DAILINPUT_LEN	10
#define DAIL_INPUT_PROMPT		"Enter room code"

#define PROMPT_DISP_H		30
#define INPUT_DISP_H		80

long calc_timeval_difference(struct timeval t1,struct timeval t2);

void SetKeypadMenu_Type(int type);
int GetKeypadMenu_Type(void);

typedef struct
{
	int input_len;
	char input_buff[MAX_DAILINPUT_LEN + 1];
	int enterDialKbFlag;
	int DialKbType;
	int DialKbMaxInput;
	char *callback_input;
	int input_char_state;//0,idle,1 keep
	int input_char_index;
	int input_char_keepicon;
	struct timeval	input_char_time;
}Dail_Input_Run_Stru;

Dail_Input_Run_Stru Dail_Input_Run;
// lzh_20180903_s
Dail_Input_Run_Stru Dail_Input_Run_last = {0};		//czn_20190116

// zfz_20190611
void SetDialInputBuff(char* str)
{
	strcpy(Dail_Input_Run.input_buff,str);
	Dail_Input_Run.input_len = strlen(Dail_Input_Run.input_buff);
}


void Update_KeypadIconBackspace(int input_len);

void Dail_Input_Run_dump(Dail_Input_Run_Stru* ptr_tar, Dail_Input_Run_Stru* ptr_src)
{
	ptr_tar->input_len = ptr_src->input_len;
	memcpy( ptr_tar->input_buff, ptr_src->input_buff, MAX_DAILINPUT_LEN + 1 );
}
// lzh_20180903_e

const KeyProperty Menu2_Input_Symbol_Tab[]=
{
	{ICON_020_kb_digit0,			'0'},	// 0
	{ICON_021_kb_digit1,			'1'},
	{ICON_022_kb_digit2,			'2'},
	{ICON_023_kb_digit3,			'3'},
	{ICON_024_kb_digit4,			'4'},
	{ICON_025_kb_digit5,			'5'},	
	{ICON_026_kb_digit6,			'6'},
	{ICON_027_kb_digit7,			'7'},
	{ICON_028_kb_digit8,			'8'},
	{ICON_029_kb_digit9,			'9'},
};   
  
const int Menu2_Input_Symbol_Tab_Size = sizeof( Menu2_Input_Symbol_Tab ) / sizeof( Menu2_Input_Symbol_Tab[0] );

typedef struct
{
   int icon;
    char  *ch;
} DialKbCharProperty;

const DialKbCharProperty Menu2_Input_Char_Tab[]=
{
	{ICON_044_kb_symbol,			".-"},	// 0
	{ICON_045_kb_abc,			"abc"},
	{ICON_046_kb_def,			"def"},
	{ICON_047_kb_ghi,			"ghi"},
	{ICON_035_kb_jkl,			"jkl"},
	{ICON_036_kb_mno,			"mno"},	
	{ICON_037_kb_pqrs,			"pqrs"},
	{ICON_038_kb_tuv,			"tuv"},
	{ICON_039_kb_wxyz,			"wxyz"},
	{ICON_042_kb_space,			" "},
};   
const int Menu2_Input_Char_Tab_Size = sizeof( Menu2_Input_Char_Tab ) / sizeof( Menu2_Input_Char_Tab[0] );

int get_calldail_disp(int para_len,char *para_buff,char *disp)
{
#if 0
	char bd_disp[5]={0};
	char rm_disp[5]={0};
	memset(bd_disp,' ',4);
	memset(rm_disp,' ',4);

	if(para_len > 0)
	{
		if(para_len <= 4)
		{
			memcpy(bd_disp+4-para_len,para_buff,para_len);
		}
		else
		{
			memcpy(bd_disp,para_buff,4);
			memcpy(rm_disp,para_buff+4,para_len-4);
		}
	}
	

	strcpy(disp,bd_disp);
	strcat(disp,"-");
	strcat(disp,rm_disp);
#else
	para_len = para_len > 10 ? 10 : para_len;
	memset(disp, 0, 11);
	
	if(para_len == 10)
	{
		memcpy(disp, para_buff, para_len);
	}
	else
	{
		memcpy(disp, para_buff, para_len);
		strcat(disp, "_");
	}
#endif
	return 0;
}

int DialMenu_StartCall(void)
{
	#if 0
	Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];		//czn_20190506
	IpCacheRecord_T record[MAX_CALL_TARGET_NUM];
	int i, getNum;
	char rm_addr[11];
	char input_buff[20];
	Dail_Input_Run.input_buff[Dail_Input_Run.input_len] = 0;	//czn_20190116
	char *pch=strstr(Dail_Input_Run.input_buff,"#");
	if(pch!=NULL)
	{
		
		int bd_len = pch-Dail_Input_Run.input_buff;
		int rm_len =Dail_Input_Run.input_len-bd_len-1;
		if(bd_len>4||rm_len==0||rm_len>4)
		{
			strcpy(input_buff,Dail_Input_Run.input_buff);
		}
		else
		{
			for(i=0;i<Dail_Input_Run.input_len;i++)
			{
				if(i!=bd_len&&(Dail_Input_Run.input_buff[i]>'9'||Dail_Input_Run.input_buff[i]<'0'))
				{
					strcpy(input_buff,Dail_Input_Run.input_buff);
					break;
				}
			}
			if(i==Dail_Input_Run.input_len)
			{
				memset(input_buff,'0',8);
				input_buff[8]=0;
				memcpy(input_buff+4-bd_len,Dail_Input_Run.input_buff,bd_len);
				memcpy(input_buff+8-rm_len,pch+1,rm_len);
			}
		}
	}
	else
	{
		strcpy(input_buff,Dail_Input_Run.input_buff);
	}
	if(JudgeIsInstallerMode())		//czn_20190506
	{
		Set_InstallerCallSub_ParaBuff(input_buff);
		StartInitOneMenu(MENU_058_InstallerCallSub,0,1);
		return 0;
	}
	
	memset(target_dev, 0, MAX_CALL_TARGET_NUM * sizeof(Call_Dev_Info));		//czn_20190506
	memset(record, 0, MAX_CALL_TARGET_NUM * sizeof(IpCacheRecord_T));
	DialBusySpriteDisplay(1);		//czn_20190516
	getNum = API_GetIpByInput(input_buff, record);
	if(getNum == 0)
		getNum = API_GetIpByInput(input_buff, record);
	DialBusySpriteDisplay(0);
	
	
	if(getNum == 0)
	{
		VirtualUserRecord_T virtualUserRecord;
		memset(&virtualUserRecord,0,sizeof(VirtualUserRecord_T));
		//printf("33333333333333333SearchVirtualUserTableByInput start\n");
		if(SearchVirtualUserTableByInput(input_buff, &virtualUserRecord) < 0)
		{
			//printf("44444444444444SearchVirtualUserTableByInput fail\n");
			API_Voice_CallFailInexistent();
			return -1;
		}
		//printf("555555555555SearchVirtualUserTableByInput ok\n");
		//printf("1111111virtualUserRecord g:%s,l:%s\n",virtualUserRecord.global,virtualUserRecord.local);
		if(virtualUserRecord.type == TYPE_VM||virtualUserRecord.type==TYPE_IM_BAK)
		{
			sip_account_Info sip_acc;
			
			//增加启动呼叫
			//如果 record.otherAccount="-" 则使用 record.account
			memset(&sip_acc,0,sizeof(sip_account_Info));
			memset(&target_dev[0],0,sizeof(Call_Dev_Info));
			strcpy(target_dev[0].bd_rm_ms,virtualUserRecord.RM_ADDR);
			//czn_20190123_s
			sprintf(target_dev[0].name,"(%s)",Dail_Input_Run.input_buff);
			
			#if 0
			if(!strcmp(GetSysVerInfo_bd(), "0099"))
			{
				sprintf(target_dev[0].name+strlen(target_dev[0].name), "V%d", atoi(virtualUserRecord.RM_ADDR+4));
			}
			else
			{
				sprintf(target_dev[0].name+strlen(target_dev[0].name), "V%s", virtualUserRecord.RM_ADDR);
			}
			
			if(strcmp(virtualUserRecord.name,"-"))
			{
				strcat(target_dev[0].name," ");
				strcat(target_dev[0].name,virtualUserRecord.name);
			}
			#endif
			strcpy(rm_addr,virtualUserRecord.RM_ADDR);
			if(strlen(virtualUserRecord.RM_ADDR)==8)
				strcat(rm_addr,"50");
			get_device_addr_and_name_disp_str(0, rm_addr, NULL, NULL,(strcmp(virtualUserRecord.name,"-")==0?NULL:virtualUserRecord.name), target_dev[0].name);	
			//czn_20190123_e
			
			strcpy(sip_acc.sip_account,virtualUserRecord.account);		//czn_20190715
		
			
			API_CallServer_Invite2(IxCallScene4,1,&target_dev[0],&sip_acc);
			
			return 0;
		}
		else
		{
			//memset(&sip_acc,0,sizeof(sip_account_Info));
			memset(&target_dev[0],0,sizeof(Call_Dev_Info));
			strcpy(target_dev[0].bd_rm_ms,virtualUserRecord.RM_ADDR);
			//czn_20190123_s
			sprintf(target_dev[0].name,"(%s)",Dail_Input_Run.input_buff);
			
			strcpy(rm_addr,virtualUserRecord.RM_ADDR);
			if(strlen(virtualUserRecord.RM_ADDR)==8)
				strcat(rm_addr,"49");
			get_device_addr_and_name_disp_str(0, rm_addr, NULL, NULL,(strcmp(virtualUserRecord.name,"-")==0?NULL:virtualUserRecord.name), target_dev[0].name);	
			
			
			Global_Addr_Stru dt_im_addr;
			dt_im_addr.gatewayid = virtualUserRecord.dt_node;
			dt_im_addr.ip = 0;
			dt_im_addr.rt = 0;
			//memcpy(para_buff, nameRecord.BD_RM_MS+4, 4);
			//para_buff[4]=0;
			dt_im_addr.code = virtualUserRecord.dt_addr;
			if(API_CallServer_Invite3(IxCallScene9,1,&target_dev[0],&dt_im_addr)<0)
			{
				//API_Voice_SysBusy();		//czn_20190624
				return -1;
			}
			else
			{
				//API_Voice_CallSucc();
				StartInitOneMenu(MENU_005_CALL,0,1);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				return 0;
			}
		}
	}
	else
	{
		getNum = getNum > MAX_CALL_TARGET_NUM ? MAX_CALL_TARGET_NUM : getNum;		//czn_20190506
		IM_NameListRecord_T nameRecord;
		char nametemp[21] ={0};
		
		for(i = 0;i < GetImNameListRecordCnt();i++)
		{
			if(GetImNameListRecordItems(i, &nameRecord) != 0)
			{
				continue;
			}
		
			
			if(memcmp(record[0].BD_RM_MS+4,"0000",4)==0)
			{
				if(memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,10)!=0)
					continue;
			}
			else
			{
				if(memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,8)!=0)
					continue;
			}
			//if(!memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,8))
			{
				if(strcmp(nameRecord.R_Name, "-")&&strlen(nameRecord.R_Name)>0)
				{
					strcat(nametemp,nameRecord.R_Name);
				}
				else if(strlen(nameRecord.name1)>=1&&strcmp(nameRecord.name1, "-"))
				{
					strcat(nametemp,nameRecord.name1);
				}
				break;
			}
		}
		if(i == GetImNameListRecordCnt()&&GetGlListRecordCnt()&&memcmp(record[0].BD_RM_MS+4,"0000",4)==0)
		{
			for(i = 0;i < GetGlListRecordCnt();i++)
			{
				if(GetGlListRecordItems(i, &nameRecord) != 0)
				{
					continue;
				}
			
				
				if(atoi(record[0].BD_RM_MS+8)>=51)
				{
					if(memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,10)!=0)
						continue;
				}
				else
				{
					if(memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,8)!=0)
						continue;
				}
				//if(!memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,8))
				{
					if(strcmp(nameRecord.R_Name, "-")&&strlen(nameRecord.R_Name)>0)
					{
						strcat(nametemp,nameRecord.R_Name);
					}
					else if(strlen(nameRecord.name1)>=1&&strcmp(nameRecord.name1, "-"))
					{
						strcat(nametemp,nameRecord.name1);
					}
					break;
				}
			}
		}
		for(i = 0; i < getNum; i++)
		{
			memcpy(target_dev[i].bd_rm_ms,record[i].BD_RM_MS, 10);
			target_dev[i].ip_addr = record[i].ip;
			//czn_20190127_s
			//sprintf(target_dev[i].name,"(%s)",Dail_Input_Run.input_buff);
			get_device_addr_and_name_disp_str(0, record[i].BD_RM_MS, NULL, NULL,nametemp, target_dev[i].name);	
			#if 0
			record[i].BD_RM_MS[8] = 0;
			//if(memcmp(record[i].BD_RM_MS,GetSysVerInfo_bd(),4) == 0)
			if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(record[i].BD_RM_MS, "0099", 4))
			{
				sprintf(target_dev[i].name+strlen(target_dev[i].name),"IM%d",atol(record[i].BD_RM_MS+4));
				//strcpy(target_dev[i].name,record[i].BD_RM_MS+4);
			}
			else
			{
				//strcpy(target_dev[i].name,record[i].BD_RM_MS);
				strcat(target_dev[i].name,"IM(");
				memcpy(target_dev[i].name+strlen(target_dev[i].name),record[i].BD_RM_MS,4);
				strcat(target_dev[i].name,")");
				strcat(target_dev[i].name,record[i].BD_RM_MS+4);
			}
			if(nametemp[0]!=0)
			{
				strcat(target_dev[i].name," ");
				strcat(target_dev[i].name,nametemp);
			}
			#endif
			//czn_20190127_e
		}
		
		if(API_CallServer_Invite(IxCallScene1_Active, getNum, target_dev) < 0)
		{
			API_Voice_SysBusy();		//czn_20190624
			return -1;
		}
		else
		{
			API_Voice_CallSucc();
			StartInitOneMenu(MENU_005_CALL,0,1);
			API_TimeLapseStart(COUNT_RUN_UP,0);
			return 0;
		}
	}

	#endif
}
int GetVillaCallNum(void)
{
	#if 0
	if(API_Event_IoServer_InnerRead_All(VillaCallNum, Dail_Input_Run.input_buff)==0)
	{
		Dail_Input_Run.input_len=strlen(Dail_Input_Run.input_buff);

		return 0;
	}
	return -1;
	#endif
}
////////// test ////////////////
char test_str[20]={0};
int check_data_test(unsigned char* pbuf)
{
	return 0;
}
static int my_keyboard_result = 0;
int sip_account_manual_register(void);
int sip_account_use_default(void);
void test_set_transfer_enable(int enable);

////////// test ////////////////

void MENU_002_DIAL_Init(int uMenuCnt)
{
	static int DialKbKeep_timer_init = 0;
	if(GetKeypadMenu_Type() == 1)
	{
		MENU_003_CODEUNLOCK_Init(uMenuCnt);
		return;
	}
	API_DisableOsdUpdate();
	#if 1
	
	char disp[100];
	int disp_len;

	if(DialKbKeep_timer_init == 0)
	{
		DialKbKeep_timer_init= 1;
		OS_CreateTimer(&Timer_DialKbKeep, Callback_Timer_DialKbKeep, 1000/25);
	}
	//if(Dail_Input_Run.enterDialKbFlag == 1)
	{
		//API_DisableOsdUpdate();
		//API_OsdStringCenterDisplayExt(0, PROMPT_DISP_H, COLOR_BLACK, DAIL_INPUT_PROMPT, strlen(DAIL_INPUT_PROMPT),0,STR_UTF8,480,40);
		#if 0
		if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0)
		{
			API_SpriteDisplay_XY(300, PROMPT_DISP_H, SPRITE_SmallKeypadSharp);
			API_OsdUnicodeStringDisplayWithIdAndAsc2(0,PROMPT_DISP_H,2,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtBdNo,0,NULL,NULL,298,40);
			API_OsdUnicodeStringDisplayWithIdAndAsc2(342,PROMPT_DISP_H,0,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtTail,0,NULL,NULL,138,40);	
		}
		else
		#endif
		{
			API_GetOSD_StringWithID(MESG_TEXT_Keypad_Prompt, NULL, 0, NULL, 0,disp, &disp_len);
			API_OsdStringCenterDisplayExt(0, PROMPT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp,disp_len,0,STR_UNICODE,480,40);
		}

		get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
		
		API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
		//API_EnableOsdUpdate();
		//Dail_Input_Run.input_len = 0;
		//memset(Dail_Input_Run.input_buff, 0,MAX_DAILINPUT_LEN+1);
	}
	Update_KeypadIconBackspace(Dail_Input_Run.input_len);

	//Dail_Input_Run.input_buff[0] ='-' ;
	//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
	#if 0
	if(DialMenuMode ==1)
	{
		API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x,GetIconXY(ICON_007_kb_call).y,SPRITE_KeypadCall);
		API_SpriteLayerDisplay_XY(0,GetIconXY(ICON_002_name_call).x,GetIconXY(ICON_002_name_call).y,DialMenuActSpriteid);
	}
	#endif
	#endif
	API_EnableOsdUpdate();
	
}

void MENU_002_DIAL_Exit(void)
{
		if(GetKeypadMenu_Type() == 1)
		{
			MENU_003_CODEUNLOCK_Exit();
			return;
		}
}


void MENU_002_DIAL_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i;
	int room_code;
	Global_Addr_Stru    target_addr;		//lyx_20171218
	char disp[MAX_DAILINPUT_LEN+2];
	int redisp;
	long timeval_difference;
	struct timeval now;

	if(GetKeypadMenu_Type() == 1)
	{
		MENU_003_CODEUNLOCK_Process(arg);
		return;
	}

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}

	else if( pglobal_win_msg->type == MSG_2_TKEY )	 
	{
		switch(GetCurIcon())
		{
			//case ICON_019_kb_backspace:
			case ICON_004_kb_return:			
				popDisplayLastMenu();				
				break;
			#if 1
			#if 0
			case ICON_007_kb_call:
				if(Dail_Input_Run.input_len == 0)		//czn_20190116
					break;
				Dail_Input_Run.input_buff[Dail_Input_Run.input_len]=0;
				if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0&&strstr(Dail_Input_Run.input_buff,"#")==NULL)
				{
					if(Dail_Input_Run.input_len < Dail_Input_Run.DialKbMaxInput)
					{
						
						Dail_Input_Run.input_buff[Dail_Input_Run.input_len++] = '#';
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
						API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);

						InputCtrl_InputAdd();	//czn_20190516
					}

					API_OsdStringClearExt2(0,PROMPT_DISP_H,OSD_LAYER_STRING,480,40);
					API_SpriteDisplay_XY(300, PROMPT_DISP_H, SPRITE_SmallKeypadCall);
					API_OsdUnicodeStringDisplayWithIdAndAsc2(0,PROMPT_DISP_H,2,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtRmNo,0,NULL,NULL,298,40);
					API_OsdUnicodeStringDisplayWithIdAndAsc2(342,PROMPT_DISP_H,0,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtTail,0,NULL,NULL,138,40);	
					Update_KeypadIconBackspace(Dail_Input_Run.input_len);
					break;
				}
				InputCtrl_HaveDail();		//czn_20190516
				// lzh_20180903_s
				if( Dail_Input_Run.input_buff[0] == '0' && Dail_Input_Run.input_len == 1 )
				{
					Dail_Input_Run_dump(&Dail_Input_Run,&Dail_Input_Run_last);	
					get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);					
					API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
				}				
				//close_monitor_server();				
				// lzh_20180903_e
				
				if(DialMenu_StartCall() !=  0)
				{
					//BEEP_ERROR();
				}
				// lzh_20180903_s
				Dail_Input_Run_dump(&Dail_Input_Run_last,&Dail_Input_Run);
				// lzh_20180903_e
				break;
			#endif
			case ICON_019_kb_backspace:
				if(pglobal_win_msg->status == TOUCHCLICK)
				{
					if(Dail_Input_Run.input_len > 0)		//czn_20190506
					{
						if(Dail_Input_Run.DialKbType == DIALKB_CHAR)
							Dail_Input_Run.input_char_state = 0;

						if(KPBackspaceMode == 1)
						{
							Dail_Input_Run.input_len = 0;
							Dail_Input_Run.input_buff[0] = 0;
						}
						else
						{
							#if 0
							if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0&&Dail_Input_Run.input_buff[Dail_Input_Run.input_len-1]=='#')
							{
								API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadSharp);
								API_OsdStringClearExt2(0,PROMPT_DISP_H,OSD_LAYER_STRING,480,40);
								API_SpriteDisplay_XY(300, PROMPT_DISP_H, SPRITE_SmallKeypadSharp);
								API_OsdUnicodeStringDisplayWithIdAndAsc2(0,PROMPT_DISP_H,2,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtBdNo,0,NULL,NULL,298,40);
								API_OsdUnicodeStringDisplayWithIdAndAsc2(342,PROMPT_DISP_H,0,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtTail,0,NULL,NULL,138,40);	
							}
							#endif
							Dail_Input_Run.input_buff[--Dail_Input_Run.input_len] = 0;
						}
						//API_DisableOsdUpdate();
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						//if(Dail_Input_Run.input_len > 0)
						{
							get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
							API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
						}
						//API_EnableOsdUpdate();
						if(Dail_Input_Run.input_len == 0||(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0))
							Update_KeypadIconBackspace(Dail_Input_Run.input_len);

						InputCtrl_InputBackSpace();	//czn_20190516
					}
					else
					{
						popDisplayLastMenu();
						return;
					}
					SetKeyBroadMode();//czn_20190111
				}
				#if 0
				else if(pglobal_win_msg->status == TOUCHLONGPRESS)
				{
					if(Dail_Input_Run.input_len > 0)
					{
						if(Dail_Input_Run.DialKbType == DIALKB_CHAR)
							Dail_Input_Run.input_char_state = 0;
						
						Dail_Input_Run.input_len = 0;
						Dail_Input_Run.input_buff[0] = 0;
						//API_DisableOsdUpdate();
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						//if(Dail_Input_Run.input_len > 0)
						{
							get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
							API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
						}
						//API_EnableOsdUpdate();
						//if(Dail_Input_Run.input_len == 0)
						Update_KeypadIconBackspace(Dail_Input_Run.input_len);

						InputCtrl_InputBackSpace();	//czn_20190516

						if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0)
						{
							API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadSharp);
							API_OsdStringClearExt2(0,PROMPT_DISP_H,OSD_LAYER_STRING,480,40);
							API_SpriteDisplay_XY(300, PROMPT_DISP_H, SPRITE_SmallKeypadSharp);
							API_OsdUnicodeStringDisplayWithIdAndAsc2(0,PROMPT_DISP_H,2,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtBdNo,0,NULL,NULL,298,40);
							API_OsdUnicodeStringDisplayWithIdAndAsc2(342,PROMPT_DISP_H,0,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtTail,0,NULL,NULL,138,40);	
						}
					}
				}
				#endif
				break;
			#if 0
			case ICON_005_kb_search:	
				if(Dail_Input_Run.input_len > 0)
				{
					if(Dail_Input_Run.callback_input!=NULL)
					{
						memcpy(Dail_Input_Run.callback_input,Dail_Input_Run.input_buff,Dail_Input_Run.input_len);
						Dail_Input_Run.callback_input[Dail_Input_Run.input_len] = 0;
						DisableOnce_extbuf2vram();
						DisableOnce_vram2extbuf();
						StartNamelistSearch();
					}
				}
				else
				{
					if(Dail_Input_Run.callback_input!=NULL)
					{
						Dail_Input_Run.callback_input[0] = 0;
						StartNamelistSearch();
					}
				}
				//my_keyboard_result = 1;
				//EnterKeypadMenu(KEYPAD_CHAR, "sip test", test_str, 20, COLOR_PURE_WHITE, "sip test...", 1, check_data_test);
				break;
			#endif
			case ICON_040_kb_char:
				//StartInitOneMenu(MENU_002_DIAL,MENU_029_DIAL_CHAR,0);
				//DisableOnce_extbuf2vram();
				//DisableOnce_vram2extbuf();
				SwitchDialKbType(DIALKB_CHAR);
				InputCtrl_InputSwitchKbType();		//czn_20190516
				break;

			case ICON_041_kb_num:
				//StartInitOneMenu(MENU_002_DIAL,MENU_028_DIAL_NUM,0);
				//DisableOnce_extbuf2vram();
				//DisableOnce_vram2extbuf();
				SwitchDialKbType(DIALKB_NUM);
				InputCtrl_InputSwitchKbType();		//czn_20190516
				break;
			#if 0
			case ICON_002_name_call:
				if(DialMenuMode ==1)
				{
					if(Dail_Input_Run.input_len > 0)
					{
						if(Dail_Input_Run.callback_input!=NULL)
						{
							memcpy(Dail_Input_Run.callback_input,Dail_Input_Run.input_buff,Dail_Input_Run.input_len);
							Dail_Input_Run.callback_input[Dail_Input_Run.input_len] = 0;
							DisableOnce_extbuf2vram();
							DisableOnce_vram2extbuf();
							StartNamelistSearch();
						}
					}
					else
					{
						if(Dail_Input_Run.callback_input!=NULL)
						{
							Dail_Input_Run.callback_input[0] = 0;
							StartNamelistSearch();
						}
					}
				}
				break;
			#endif	
			default:	
				SetKeyBroadMode();//czn_20190111
				if(Dail_Input_Run.DialKbType == DIALKB_NUM)
				{
					for( i = 0;i < Menu2_Input_Symbol_Tab_Size; i++)
					{
						if(GetCurIcon() == Menu2_Input_Symbol_Tab[i].icon)
						{
							if(Dail_Input_Run.input_len < Dail_Input_Run.DialKbMaxInput)
							{
								//printf("GetCurIcon() = %d,i = %d char = %c\n",GetCurIcon(),i,Menu2_Input_Symbol_Tab[i].iCon
								Dail_Input_Run.input_buff[Dail_Input_Run.input_len++] = Menu2_Input_Symbol_Tab[i].key_value;
								//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
								get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
								API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);

								InputCtrl_InputAdd();	//czn_20190516
							}
							if(Dail_Input_Run.input_len == 1)		//czn_20190506
								Update_KeypadIconBackspace(Dail_Input_Run.input_len);
							
							return;
						}
					}
				}
				if(Dail_Input_Run.DialKbType == DIALKB_CHAR)
				{
					for( i = 0;i < Menu2_Input_Char_Tab_Size; i++)
					{
						if(GetCurIcon() == Menu2_Input_Char_Tab[i].icon)
						{
							if(Dail_Input_Run.input_len <= Dail_Input_Run.DialKbMaxInput)
							{
								timeval_difference = calc_timeval_difference(pglobal_win_msg->happen_time,Dail_Input_Run.input_char_time);
								//printf("GetCurIcon() = %d,i = %d char = %c\n",GetCurIcon(),i,Menu2_Input_Symbol_Tab[i].iCon
								//if(Dail_Input_Run.input_len < Dail_Input_Run.DialKbMaxInput && (Dail_Input_Run.input_char_state == 0||
								//Dail_Input_Run.input_char_keepicon!=Menu2_Input_Char_Tab[i].icon||
								//timeval_difference > 1500000))
								if( (Dail_Input_Run.input_char_state == 0||
								Dail_Input_Run.input_char_keepicon!=Menu2_Input_Char_Tab[i].icon||
								timeval_difference > 1500000))
								{
									if(Dail_Input_Run.input_len < Dail_Input_Run.DialKbMaxInput)
									{
										Dail_Input_Run.input_char_state = 1;
										Dail_Input_Run.input_char_keepicon = Menu2_Input_Char_Tab[i].icon;
										Dail_Input_Run.input_char_time = pglobal_win_msg->happen_time;
										Dail_Input_Run.input_char_index = 0;
										Dail_Input_Run.input_buff[Dail_Input_Run.input_len++] = Menu2_Input_Char_Tab[i].ch[Dail_Input_Run.input_char_index++];

										if(Menu2_Input_Char_Tab[i].ch[Dail_Input_Run.input_char_index] == 0)
										{
											Dail_Input_Run.input_char_index = 0;
											Dail_Input_Run.input_char_state = 0;
										}
										OS_RetriggerTimer(&Timer_DialKbKeep);

										if(Dail_Input_Run.input_len == 1)
											Update_KeypadIconBackspace(Dail_Input_Run.input_len);

										InputCtrl_InputChar();		//czn_20190516
									}
									else 
									{
										if(Dail_Input_Run.input_char_state == 1)
										{
											Dail_Input_Run.input_char_state = 0;
										}
										else
										{
											return;
										}
									}
										
								}
								else if(Dail_Input_Run.input_char_state == 1 && timeval_difference < 1500000)
								{
									Dail_Input_Run.input_char_state = 1;
									Dail_Input_Run.input_char_keepicon = Menu2_Input_Char_Tab[i].icon;
									Dail_Input_Run.input_char_time = pglobal_win_msg->happen_time;

									Dail_Input_Run.input_buff[Dail_Input_Run.input_len-1] = Menu2_Input_Char_Tab[i].ch[Dail_Input_Run.input_char_index++];

									if(Menu2_Input_Char_Tab[i].ch[Dail_Input_Run.input_char_index] == 0)
										Dail_Input_Run.input_char_index = 0;

									OS_RetriggerTimer(&Timer_DialKbKeep);

									InputCtrl_InputChar();		//czn_20190516
								}
								else 
								{
									Dail_Input_Run.input_char_state = 0;
									return;
								}
								//API_DisableOsdUpdate();
								//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
								get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
								if(Dail_Input_Run.input_char_state == 1)
								{
									redisp = Dail_Input_Run.input_len-1;
									API_OsdStringCenterReDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40,redisp,redisp);
								}
								else
								{
									API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
								}
								//API_EnableOsdUpdate();
							}

							return;
						}
					}
				}
				//DefaultPublicIconProcessing();
				break;
				#endif
		}	
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_DialKbKeepTimeout:
				if(Dail_Input_Run.input_char_state && Dail_Input_Run.DialKbType == DIALKB_CHAR)
				{
					gettimeofday(&now,NULL);
					timeval_difference = calc_timeval_difference(now,Dail_Input_Run.input_char_time);
					if(timeval_difference > 500000)
					{
						Dail_Input_Run.input_char_state = 0;

						API_DisableOsdUpdate();
						//API_OsdStringClearExt(0, INPUT_DISP_H, 480, 40);
						get_calldail_disp(Dail_Input_Run.input_len,Dail_Input_Run.input_buff,disp);
						
						API_OsdStringCenterDisplayExt(0, INPUT_DISP_H, DISPLAY_KEYBOARD_COLOR, disp, strlen(disp),0,STR_UTF8,480,40);
						
						API_EnableOsdUpdate();

						InputCtrl_InputAdd();	//czn_20190516
					}
					else
					{
						OS_RetriggerTimer(&Timer_DialKbKeep);
					}
				}
				break;
			#if 0	
			case MSG_7_BRD_SUB_NoInputTimeout:	//czn_20190516
				InputCtrl_NoInputTimeout_Process();
				break;
			#endif	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
		
	}
	
}


void EnterDialKbMenu(unsigned char type, char* pInput, uint8 inputMaxLen, char* pInitInputString,int pushstack)
{
	char* pDisplay;
	
	SetKeypadMenu_Type(0);
	
	Dail_Input_Run.callback_input = pInput;
	Dail_Input_Run.DialKbType = type;
	Dail_Input_Run.DialKbMaxInput = inputMaxLen;
	Dail_Input_Run.enterDialKbFlag = 1;
	
	if(pInitInputString != NULL)
	{
		Dail_Input_Run.input_len = strlen(pInitInputString);
		strcpy(Dail_Input_Run.input_buff,pInitInputString);
	}
	else
	{
		Dail_Input_Run.input_len = 0;
		Dail_Input_Run.input_buff[0] = 0;
	}
	
	//API_DisableOsdUpdate();
	if(Dail_Input_Run.DialKbType == DIALKB_NUM)
	{
		StartInitOneMenu(MENU_002_DIAL, MENU_031_DIAL_NUM, pushstack);
	}
	else if(Dail_Input_Run.DialKbType == DIALKB_CHAR)
	{
		Dail_Input_Run.input_char_state = 0;
		Dail_Input_Run.input_char_index = 0;
		StartInitOneMenu(MENU_002_DIAL, MENU_032_DIAL_CHAR, pushstack);
	}

	InputCtrl_ParameterInit();		//czn_20190516
	//API_EnableOsdUpdate();
}

static void SwitchDialKbType(unsigned char type)
{	
	if(Dail_Input_Run.DialKbType != type)
	{
		Dail_Input_Run.DialKbType = type;
		Dail_Input_Run.enterDialKbFlag = 0;
		
		API_DisableOsdUpdate();
		if(Dail_Input_Run.DialKbType == DIALKB_NUM)
		{
			StartInitOneMenu(MENU_002_DIAL, MENU_031_DIAL_NUM, 0);
		}
		else if(Dail_Input_Run.DialKbType == DIALKB_CHAR)
		{
			Dail_Input_Run.input_char_state = 0;
			Dail_Input_Run.input_char_index = 0;
			StartInitOneMenu(MENU_002_DIAL, MENU_032_DIAL_CHAR, 0);
		}
		API_EnableOsdUpdate();
	}
}

long calc_timeval_difference(struct timeval t1,struct timeval t2)
{
	long rev = 0;
	
	rev = (t1.tv_sec-t2.tv_sec)*1000000 + (t1.tv_usec-t2.tv_usec);
	
	return rev;
}

static void Callback_Timer_DialKbKeep(void)
{
	if(Dail_Input_Run.input_char_state && Dail_Input_Run.DialKbType == DIALKB_CHAR)
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DialKbKeepTimeout);
		return;
	}
	InputCtrl_Timer_CallBack();
}

void Update_KeypadIconBackspace(int input_len)		//czn_20190506
{
	if(DialMenuMode == 1)
	{
		char buff[50];
		int len;
		if(input_len==0)
		{
			API_SpriteDisplay_XY(GetIconXY(ICON_019_kb_backspace).x, GetIconXY(ICON_019_kb_backspace).y, (Get_CurScreenStyle()?SPRITE_KeypadExit2:SPRITE_KeypadExit));
			if(GetCurMenuCnt() == MENU_002_DIAL)
			{
				//API_OsdStringCenterDisplayExt(0, GetIconXY(ICON_002_name_call).y+9, COLOR_WHITE, "UNIT LIST",strlen("UNIT LIST"),1,STR_UTF8,480,66 );
				API_GetOSD_StringWithID(MESG_TEXT_CusNamelist, NULL, 0, NULL, 0,buff, &len);
				if(len > 2)
				{
					//API_OsdStringCenterDisplayExt(0, GetIconXY(ICON_002_name_call).y, COLOR_WHITE, buff,len,1,STR_UNICODE,480,66 );
				}
			}
		}
		else
		{
			API_SpriteDisplay_XY(GetIconXY(ICON_019_kb_backspace).x, GetIconXY(ICON_019_kb_backspace).y, SPRITE_KeypadBackspace);
			if(GetCurMenuCnt() == MENU_002_DIAL)
			{
				//API_OsdStringCenterDisplayExt(0, GetIconXY(ICON_002_name_call).y+9, COLOR_WHITE, "SEARCHING",strlen("SEARCHING"),1,STR_UTF8,480,66 );
				API_GetOSD_StringWithID(MESG_TEXT_CusSearch, NULL, 0, NULL, 0,buff, &len);
				if(len > 2)
				{
					//API_OsdStringCenterDisplayExt(0, GetIconXY(ICON_002_name_call).y, COLOR_WHITE, buff,len,1,STR_UNICODE,480,66 );
				}
			}
		}
	}
	else
	{
		if(input_len==0)
		{
			API_SpriteDisplay_XY(GetIconXY(ICON_019_kb_backspace).x, GetIconXY(ICON_019_kb_backspace).y, (Get_CurScreenStyle()?SPRITE_KeypadExit2:SPRITE_KeypadExit));
		}
		else
			API_SpriteDisplay_XY(GetIconXY(ICON_019_kb_backspace).x, GetIconXY(ICON_019_kb_backspace).y, SPRITE_KeypadBackspace);

		if(GetCurMenuCnt() == MENU_002_DIAL)
		{
			if(input_len==0)
			{
				API_SpriteClose(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadCall);
			}
			else
			{
				if(GetKeypadMenu_Type() == 0)
				{
					Dail_Input_Run.input_buff[input_len]=0;
					if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0&&strstr(Dail_Input_Run.input_buff,"#")==NULL)
					{
						API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadSharp);
						//API_OsdStringClearExt2(0,PROMPT_DISP_H,OSD_LAYER_STRING,480,40);
						//API_SpriteDisplay_XY(300, PROMPT_DISP_H, SPRITE_SmallKeypadSharp);
						//API_OsdUnicodeStringDisplayWithIdAndAsc2(0,PROMPT_DISP_H,2,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtBdNo,0,NULL,NULL,298,40);
						//API_OsdUnicodeStringDisplayWithIdAndAsc2(338,PROMPT_DISP_H,0,OSD_LAYER_STRING,DISPLAY_KEYBOARD_COLOR,COLOR_KEY,MESG_TEXT_KpInputPromtTail,0,NULL,NULL,120,40);	
					}
					else
					{
						API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadCall);
					}
				}
				else
				{
					API_SpriteDisplay_XY(GetIconXY(ICON_007_kb_call).x, GetIconXY(ICON_007_kb_call).y, SPRITE_KeypadUnlock);
				}
			}
		}
	}
}

//czn_20190516_s
static int InputCtrl_NoInputStartCallTime = 0;
static int InputCtrl_NoInputStartCallLeastLen = 0;
static int InputCtrl_ImmediatelyStartCallLen = 0;
static int NoInputTime = 0;


void InputCtrl_ParameterInit(void)
{
	char buff[5];
	
	InputCtrl_NoInputStartCallTime = 0;
	InputCtrl_NoInputStartCallLeastLen = 0;
	InputCtrl_ImmediatelyStartCallLen = 0;

	if(API_Event_IoServer_InnerRead_All(NoInputStartCallTime, buff) == 0)
	{
		InputCtrl_NoInputStartCallTime = atoi(buff);
	}
	if(API_Event_IoServer_InnerRead_All(NoInputStartCallLeastLen, buff) == 0)
	{
		InputCtrl_NoInputStartCallLeastLen = atoi(buff);
	}
	if(API_Event_IoServer_InnerRead_All(InputImmediatelyStartCallLen, buff) == 0)
	{
		InputCtrl_ImmediatelyStartCallLen = atoi(buff);
	}
}

void InputCtrl_HaveDail(void)
{
	NoInputTime = 0;
	OS_StopTimer(&Timer_DialKbKeep);
}

void InputCtrl_InputAdd(void)
{
	if(InputCtrl_ImmediatelyStartCallLen>0 && Dail_Input_Run.input_len == InputCtrl_ImmediatelyStartCallLen)
	{
		if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0)
			return;
		OS_StopTimer(&Timer_DialKbKeep);
		DialMenu_StartCall();
		return;
	}
	if(InputCtrl_NoInputStartCallTime>0&&InputCtrl_NoInputStartCallLeastLen>0&& Dail_Input_Run.input_len >= InputCtrl_NoInputStartCallLeastLen)
	{
		//OS_StopTimer(&Timer_DialKbKeep);
		NoInputTime = 0;
		OS_RetriggerTimer(&Timer_DialKbKeep);
	}	
}
void InputCtrl_InputBackSpace(void)
{
	if(InputCtrl_NoInputStartCallTime>0&&InputCtrl_NoInputStartCallLeastLen>0&& Dail_Input_Run.input_len >= InputCtrl_NoInputStartCallLeastLen)
	{
		NoInputTime = 0;
		OS_RetriggerTimer(&Timer_DialKbKeep);
	}	
	else
	{
		OS_StopTimer(&Timer_DialKbKeep);
	}
}

void InputCtrl_InputChar(void)
{
	NoInputTime = 0;
}

void InputCtrl_InputSwitchKbType(void)
{
	NoInputTime = 0;
}
#if 0
void InputCtrl_InputHaveChange(void)
{
	
	if(Dail_Input_Run.input_char_state && Dail_Input_Run.DialKbType == DIALKB_CHAR)
	{
		NoInputTime = 0;
		return;
	}
	OS_StopTimer(&Timer_DialKbKeep);
	NoInputTime = 0;
	
	
	if(InputCtrl_ImmediatelyStartCallLen>0 && Dail_Input_Run.input_len == InputCtrl_ImmediatelyStartCallLen)
	{
		DialMenu_StartCall();
		return;
	}
	if(InputCtrl_NoInputStartCallLeastLen>0&& Dail_Input_Run.input_len >= InputCtrl_NoInputStartCallLeastLen)
	{
		OS_RetriggerTimer(&Timer_DialKbKeep);
	}	
}
#endif
void InputCtrl_Timer_CallBack(void)
{
	//printf("111111111InputCtrl_Timer_CallBack menu%d,input_len=%d,leastlen=%d,\n"
	if(DialMenuMode ==2&&strcmp(GetSysVerInfo_bd(),"0000")==0)
		return;
	if(GetCurMenuCnt() == MENU_002_DIAL&&InputCtrl_NoInputStartCallTime>0&&InputCtrl_NoInputStartCallLeastLen>0&& Dail_Input_Run.input_len >= InputCtrl_NoInputStartCallLeastLen)
	{
		if(++NoInputTime >= InputCtrl_NoInputStartCallTime)
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_NoInputTimeout);
			return;
		}
		OS_RetriggerTimer(&Timer_DialKbKeep);
	}
}

void InputCtrl_NoInputTimeout_Process(void)
{
	if(Dail_Input_Run.input_char_state && Dail_Input_Run.DialKbType == DIALKB_CHAR)
	{
		return;
	}
	
	if(InputCtrl_NoInputStartCallLeastLen>0&& Dail_Input_Run.input_len >= InputCtrl_NoInputStartCallLeastLen)
	{
		if(NoInputTime >= InputCtrl_NoInputStartCallTime)
		{
			DialMenu_StartCall();
		}
		else
		{
			OS_RetriggerTimer(&Timer_DialKbKeep);
		}
	}
}
//czn_20190516_e

static int KeypadMenu_Type = 0;	//0,dail,1 unlock;		//czn_20190613

void SetKeypadMenu_Type(int type)
{
	KeypadMenu_Type = type;
}

int GetKeypadMenu_Type(void)
{
	return KeypadMenu_Type;
}

