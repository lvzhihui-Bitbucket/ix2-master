/**
  ******************************************************************************
  * @file    obj_Survey_Keyboard.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the functions of the obj_Survey_Keyboard
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/

#include "MENU_Keyboard.h"
#include "obj_menu_data.h"
#include "task_VideoMenu.h"

KbdProperty kbd_property;
extern OS_TIMER timer_sprite_ctrl;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
const KeyProperty ASCII_CHAR_NUB_TAB[]=
{
	{ICON_101_Key00,	  'q'},
	{ICON_102_Key01,	  'w'},
	{ICON_103_Key02,	  'e'},
	{ICON_104_Key03,	  'r'},
	{ICON_105_Key04,	  't'},
	{ICON_106_Key05,	  'y'},
	{ICON_107_Key06,	  'u'},
	{ICON_108_Key07,	  'i'},
	{ICON_109_Key08,	  'o'},
	{ICON_110_Key09,	  'p'},
	{ICON_111_Key10,	  'a'},
	{ICON_112_Key11,	  's'},
	{ICON_113_Key12,	  'd'},
	{ICON_114_Key13,	  'f'},
	{ICON_115_Key14,	  'g'},
	{ICON_116_Key15,	  'h'},
	{ICON_117_Key16,	  'j'},
	{ICON_118_Key17,	  'k'},
	{ICON_119_Key18,	  'l'},
	{ICON_120_Key19,	  CAPSLOCK_KEY_VALUE},
	{ICON_121_Key20,	  'z'},
	{ICON_122_Key21,	  'x'},
	{ICON_123_Key22,	  'c'},
	{ICON_124_Key23,	  'v'},
	{ICON_125_Key24,	  'b'},
	{ICON_126_Key25,	  'n'},
	{ICON_127_Key26,	  'm'},
	{ICON_128_Key27,	  BACKSPACE_KEY_VALUE},
	{ICON_129_Key28,	  TURN_TO_NUM_KEY_VALUE},
	{ICON_130_Key29,	  ','},
	{ICON_131_Key30,	  '.'},
	{ICON_132_Key31,	  ' '},
	{ICON_133_Key32,	  '\r'},
	{ICON_134_Key33,	  RETURN_KEY_VALUE},
	{ICON_135_Key34,	  OK_KEY_VALUE},

	{ICON_136_Key35,	  '1'},
	{ICON_137_Key36,	  '2'},
	{ICON_138_Key37,	  '3'},
	{ICON_139_Key38,	  '4'},
	{ICON_140_Key39,	  '5'},
	{ICON_141_Key40,	  '6'},
	{ICON_142_Key41,	  '7'},
	{ICON_143_Key42,	  '8'},
	{ICON_144_Key43,	  '9'},
	{ICON_145_Key44,	  '0'},
	{ICON_146_Key45,	  '_'},
	{ICON_147_Key46,	  '\\'},
	{ICON_148_Key47,	  '|'},
	{ICON_149_Key48,	  '~'},
	{ICON_150_Key49,	  '<'},
	{ICON_151_Key50,	  '>'},
	{ICON_152_Key51,	  '$'},
	{ICON_153_Key52,	  '.'},
	{ICON_154_Key53,	  '/'},
	{ICON_155_Key54,	  '@'},
	{ICON_156_Key55,	  '%'},
	{ICON_157_Key56,	  ':'},
	{ICON_158_Key57,	  '"'},
	{ICON_159_Key58,	  '\''},
	{ICON_160_Key59,	  '?'},
	{ICON_161_Key60,	  '!'},
	{ICON_162_Key61,	  '['},
	{ICON_163_Key62,	  ']'},
	{ICON_164_Key63,	  '&'},
	{ICON_165_Key64,	  '#'},
	{ICON_166_Key65,	  TURN_TO_CHAR_KEY_VALUE},
	{ICON_167_Key66,	  '+'},
	{ICON_168_Key67,	  '-'},
	{ICON_169_Key68,	  '='},
	{ICON_170_Key69,	  '*'},
	{ICON_171_Key70,	  ' '},
	{ICON_172_Key71,	  BACKSPACE_KEY_VALUE},
	{ICON_173_Key72,	  RETURN_KEY_VALUE},
	{ICON_174_Key73,	  OK_KEY_VALUE},

};

const uint8 ASCII_TAB_NUMBERS = sizeof( ASCII_CHAR_NUB_TAB ) / sizeof( ASCII_CHAR_NUB_TAB[0] );

/******************************************************************************
 * @fn      OneKeyValue()
 *
 * @brief   输入一个按键
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char OneKeyValue(unsigned char Keypad_type,int icon)   
{
    uint8 i;
    uint8 key_value = 0;
    if(Keypad_type == KEYPAD_CHAR ||Keypad_type == KEYPAD_NUM ) 
    {
        for(i=0;i<ASCII_TAB_NUMBERS;i++)                                            //查表找出哪个按键按下
        {
 			if(icon == ASCII_CHAR_NUB_TAB[i].icon)
			{
	            key_value = ASCII_CHAR_NUB_TAB[i].key_value;                                 //找到键值
	            if((kbd_property.capsLock != 0) && (key_value>96) && (key_value < 123))//如果大写锁定并且按下的是26个字母，转换成大写字母
	            {
	                if(kbd_property.capsLock == 1)                                     //判断大写锁定只用一次
	                {
	                    kbd_property.capsLock = 0;
						API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "abc   ", strlen("abc   "),0,STR_UTF8,0);
	                }
	                key_value = key_value - 32;
	            }
	            break;                                                                 //结束循环
			}
        }
    }
    else if(Keypad_type == KEYPAD_EDIT)
    {
        key_value = SET_HIGHLIGHT_KEY_VALUE;                                           //高亮
    }
    return key_value;
}


/******************************************************************************
 * @fn      clearscrean()
 *
 * @brief   清屏
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void ClearScrean(void)
{
    uint8 i;
    for(i=0;i < kbd_property.dispMaxRow;i++)
    {
		API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*KP_DISP_CHAR_W, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + i*KP_DISP_CHAR_H, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*KP_DISP_CHAR_W, KP_DISP_CHAR_H);
    }
}

/******************************************************************************
 * @fn      DisplayAllScreen()
 *
 * @brief   更新整个编辑框的显示
 *
 * @param   iColor       显示颜色       范围0~15
 * @param   highLight    高亮属性       0--不高亮/ 1--高亮
 * @param   cursor       是否显示光标   0--不显示/1--显示
 *
 * @return  none
 *****************************************************************************/
void DisplayAllScreen( void )       
{
    unsigned char Display[MAX_DISPLAY_COL+10 + 1] = {0};
    unsigned char i,j,k=0;
    unsigned char end_flag = 0;
	int 		backColor;

    if( kbd_property.highLight )                                                   //背景颜色设置
    {
        backColor = COLOR_GREEN;
    }
	else
	{
		backColor = COLOR_KEY;
	}

    for(i=0;i<kbd_property.dispMaxRow;i++)                          //更新显示
    {
        for(j=0, memset(Display, ' ', MAX_DISPLAY_COL+10); j<kbd_property.dispMaxCol;j++)
        {
            if(kbd_property.inputbuf[k]=='\0')
            {
                if(kbd_property.cursor && (kbd_property.char_num < kbd_property.inputSize) && (!kbd_property.highLight))
                {
                    Display[j] = '_';
                }
				end_flag = 1;
                break;
            }
            else if(kbd_property.inputbuf[k]=='\r')
            {
                k++;
                break;
            }
            else 
            {
                Display[j]=kbd_property.inputbuf[k];
                k++; 
            } 
        }

		if( kbd_property.highLight )
		{
			Display[j] = '\0';
		}

		API_OsdStringDisplayExtWithBackColor(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*KP_DISP_CHAR_W, KEYBOARD_DISPLAY_Y + (kbd_property.dispStartRow + i)*38, kbd_property.dispColor, backColor, Display, strlen(Display),0,STR_UTF8 );

		if(end_flag)
		{
			API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*KP_DISP_CHAR_W, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + (i+1)*KP_DISP_CHAR_H, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*KP_DISP_CHAR_W, KP_DISP_CHAR_H);
			break;
		}
	}
	
    if(i == kbd_property.dispMaxRow - 1)           //显示屏只剩下一行
    {
        kbd_property.dispFullFlag = 1;
    }
    else if(i == kbd_property.dispMaxRow)           //显示屏已经满了
    {
        kbd_property.dispFullFlag = 2;                              
    }
    else                                                            //显示屏空行还有两行或以上
    {
        kbd_property.dispFullFlag = 0;
    }
}

/******************************************************************************
 * @fn      API_KeyboardInit()
 *
 * @brief   初始化键盘属性
 *
 * @param   startRow        显示起始行  范围0~MAX_DISPLAY_ROW-1
 * @param   StartCol        显示起始列  范围0~MAX_DISPLAY_COL-1
 * @param   dispMaxRow      显示最大行  范围1~MAX_DISPLAY_ROW
 * @param   dispMaxCol      显示最大列  范围1~MAX_DISPLAY_COL
 * @param   color           显示颜色    范围0~15
 *
 * @return  1--成功/ 0--失败
 *****************************************************************************/
unsigned char API_KeyboardInit(unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
                               unsigned short dispMaxCol, int color, unsigned char* pString, unsigned char highLight, available_check_func call_back_ptr )
{

	int i = 0; 

    //编辑框大小是:MAX_DISPLAY_ROW * MAX_DISPLAY_COL
    if((startRow >= MAX_DISPLAY_ROW) || (StartCol >= MAX_DISPLAY_COL) || \
       ((startRow + dispMaxRow) > MAX_DISPLAY_ROW) || ((StartCol + dispMaxCol) > MAX_DISPLAY_COL)/* || (color > 15)*/)
    {
        return 0;
    }
    else
    {
        kbd_property.dispStartCol = StartCol;
        kbd_property.dispStartRow = startRow;
        kbd_property.dispMaxCol = dispMaxCol;
        kbd_property.dispMaxRow = dispMaxRow;        
        kbd_property.dispColor = color;
        kbd_property.capsLock = 0;                                  //默认小写
        kbd_property.dispFullFlag =0;                               //显示屏未满
        kbd_property.cursor = 1;                                    //显示光标
        kbd_property.char_num = 0;                                  //inputbuf中的数据是0

		kbd_property.highLight		= highLight;
		kbd_property.call_back_ptr 	= call_back_ptr;
		
        if(dispMaxRow * dispMaxCol <= MAX_INPUTBUF_SIZE)
        {
            kbd_property.inputSize = dispMaxRow * dispMaxCol;       //输入数据限度
        }
        else
        {
            kbd_property.inputSize = MAX_INPUTBUF_SIZE;             //输入数据限度
        }
		
		if(pString != NULL)
		{
			for(i=0;(*pString != '\0') && (i < kbd_property.inputSize);i++,pString++)
			{
				kbd_property.inputbuf[i] = *pString;												 //把数据放到inputbuf中
			}
		}
		
		kbd_property.char_num = i;																 //buffer中数据的字节数
		kbd_property.inputbuf[kbd_property.char_num] = '\0';									 //结尾添加结束符
		
        return 1;
    }
}

/******************************************************************************
 * @fn      API_KeyboardInputStart()
 *
 * @brief   开始输入
 *
 * @param   pString     开始输入字符指针，字符大小不能超过MAX_INPUTBUF_SIZE
 * @param   highLight   开始是否高亮  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_KeyboardInputStart( void )
{
    DisplayAllScreen();
}

/******************************************************************************
 * @fn      API_KeyboardChangeHighLight()
 *
 * @brief   设置高亮或取消高亮
 *
 * @param   highLight  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_KeyboardInputModeChange(void)
{
	kbd_property.highLight = !kbd_property.highLight;
	ClearScrean();
    DisplayAllScreen();
}


/******************************************************************************
 * @fn      API_KeyboardInputOneKey()
 *
 * @brief   触摸输入
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char API_KeyboardInputOneKey(unsigned char Keypad_type,int icon )
{
    unsigned char key_value;
    key_value = OneKeyValue(Keypad_type, icon);
    if((key_value != RETURN_KEY_VALUE) && (key_value != 0) && (key_value != OK_KEY_VALUE) &&
       (key_value != TURN_TO_NUM_KEY_VALUE) && (key_value != TURN_TO_CHAR_KEY_VALUE))  
    {
		if(key_value == BACKSPACE_KEY_VALUE)														//输入退格键按下
		{
			if(kbd_property.highLight)																//高亮选中，全部删除buffer
			{
				kbd_property.inputbuf[0] = '\0';
				kbd_property.highLight = 0;
				kbd_property.char_num = 0;
			}
			else if(kbd_property.char_num>0)														//高亮没有选中，buffer中数据不为0
			{
				kbd_property.char_num--;
				kbd_property.inputbuf[kbd_property.char_num] = '\0';
			}
		}
		else if(key_value == SET_HIGHLIGHT_KEY_VALUE)												//如果点一下编辑框，高亮属性反选
		{
			API_KeyboardInputModeChange();
			return key_value;
		}
		else if(key_value == CAPSLOCK_KEY_VALUE)													//大小写切换，改变大小写锁定属性
		{
			kbd_property.capsLock++;
			if(kbd_property.capsLock > 2)
			{
				kbd_property.capsLock = 0;
			}	 
		}
		else																							 //如果输入其他，把输入值放到buffer中
		{ 
			if(kbd_property.highLight)
			{
				if((kbd_property.dispMaxRow != 1) || (key_value != '\r'))
				{
					kbd_property.highLight = 0;
					kbd_property.char_num = 0;
					kbd_property.inputbuf[kbd_property.char_num] = key_value;
					kbd_property.char_num++;
					kbd_property.inputbuf[kbd_property.char_num] = '\0';								 //数据后面加结束符
				}
			}
			else
			{
				if((kbd_property.char_num < kbd_property.inputSize) && (kbd_property.dispFullFlag != 2)) //输入字符不超过限制且屏幕未满允许输入
				{
					if(key_value == '\r')																 //输入回车键
					{	
						if(kbd_property.dispFullFlag == 0)
						{	
							kbd_property.inputbuf[kbd_property.char_num] = key_value;
							kbd_property.char_num++;
							kbd_property.inputbuf[kbd_property.char_num] = '\0';						 //数据后面加结束符
						}
					}
					else																				 //输入其他字符
					{
						kbd_property.inputbuf[kbd_property.char_num] = key_value;
						kbd_property.char_num++;
						kbd_property.inputbuf[kbd_property.char_num] = '\0';							 //数据后面加结束符
					} 
				}
			}
		}
		DisplayAllScreen();//显示
    }
    return key_value;
}

int API_KeyboardInputGetcapsLock( void )
{
	return kbd_property.capsLock;
}


//	返回 -1，不切换界面
//  返回 0， BEEP_ERROR();
//  返回 1， popDisplayLastMenu();
int API_KeyboardInputDataDump( char* pbuffer )
{
	int ret;
	
	if( pbuffer != NULL )
	{
		if( kbd_property.call_back_ptr == NULL )
		{
			strcpy(pbuffer, kbd_property.inputbuf);
			return 1;
		}
		else
		{
			strcpy(pbuffer, kbd_property.inputbuf);
			ret = (*kbd_property.call_back_ptr)(kbd_property.inputbuf);
			return ret;
		}
	}
	else
		return 1;
}

