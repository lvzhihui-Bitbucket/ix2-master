
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "obj_menu_data.h"
#include "obj_MenuList.h"

/*******************************************************************
 ___________________________________________________________________
|																	|
|																	|
4																	|
|																	|
|________________________________2__________________________________|
|1																|	|
|																|	|
|																|	|
|																|	|
|																|	|
3																|	|
|																|	|
|																|	|
|																|	|
|																|	|
|_______________________________________________________________|___|

1-坐标 textBoxPositionX， textBoxPositionY
2-textBoxWidth
3-textBoxHeight
4-textBoxTitleHeight

********************************************************************/
const ICON_TYPE_TO_PIC typeToIconPic[] = 
{
	{ICON_PIC_NONE, NONE_ICON_PIC, NONE_ICON_PIC, 	NONE_ICON_PIC, 				NONE_ICON_PIC, 				NONE_ICON_PIC, NONE_ICON_PIC},
	{ICON_PIC_UP, 	NONE_ICON_PIC, NONE_ICON_PIC, 	MENU_LIST_ICON_PIC_UP, 		MENU_LIST_ICON_PIC_UPS, 	NONE_ICON_PIC, NONE_ICON_PIC},
	{ICON_PIC_DOWN, NONE_ICON_PIC, NONE_ICON_PIC, 	MENU_LIST_ICON_PIC_DOWN, 	MENU_LIST_ICON_PIC_DOWNS, 	NONE_ICON_PIC, NONE_ICON_PIC},
	//{ICON_PIC_IPC, 	NONE_ICON_PIC, NONE_ICON_PIC, 	MENU_LIST_ICON_PIC_LIST, 	SPRITE_VideoBindingIX, 		ICON_PIC_NONE, NONE_ICON_PIC},
};

const ICON_TYPE_TO_PIC typeToListPic[] = 
{
	{ICON_PIC_NONE, NONE_ICON_PIC, NONE_ICON_PIC, 	NONE_ICON_PIC, 				NONE_ICON_PIC, 				NONE_ICON_PIC, NONE_ICON_PIC},
	{ICON_PIC_UP, 	NONE_ICON_PIC, NONE_ICON_PIC, 	NONE_ICON_PIC, 				NONE_ICON_PIC, 				NONE_ICON_PIC, NONE_ICON_PIC},
	{ICON_PIC_DOWN, NONE_ICON_PIC, NONE_ICON_PIC, 	NONE_ICON_PIC, 				NONE_ICON_PIC, 				NONE_ICON_PIC, NONE_ICON_PIC},
};

const int typeToIconPicNum = sizeof(typeToIconPic)/sizeof(typeToIconPic[0]);
const int typeToListPicNum = sizeof(typeToListPic)/sizeof(typeToListPic[0]);


static LIST_PROPERTY listMenu;


//初始化对象，带进需要随时改变的参数
int InitMenuList(LIST_INIT_T listInit)
{
	int i;
	
	listMenu.titleHeight = MENU_LIST_titleHeight;
	listMenu.titleStrLen = listInit.titleStrLen;
	memcpy(listMenu.titleStr, listInit.titleStr, listMenu.titleStrLen);

	listMenu.boxX =	MENU_LIST_boxX;
	listMenu.boxY =	MENU_LIST_boxY;
	listMenu.boxWidth =	MENU_LIST_boxWidth;
	listMenu.boxHeight = MENU_LIST_boxHeight;

	listMenu.textBoxPositionX =	MENU_LIST_textBoxX;
	listMenu.textBoxPositionY =	MENU_LIST_textBoxY;
	listMenu.textBoxWidth =	MENU_LIST_textBoxWidth;
	listMenu.textBoxHeight = MENU_LIST_textBoxHeight;
	
	listMenu.up.icon = MENU_LIST_ICON_UP;
	listMenu.up.x = MENU_LIST_upX;
	listMenu.up.y = MENU_LIST_upY;
	listMenu.up.w = MENU_LIST_upWidth;
	listMenu.up.h = MENU_LIST_upHeight;
	listMenu.up.iconType = ICON_PIC_UP;
	listMenu.up.revColor = COLOR_LIST_MENU_ICON;
	

	
	listMenu.valAlign = listInit.valAlign;
	listMenu.textAlign = listInit.textAlign;
	
	listMenu.up.fun.x = listMenu.up.x;
	listMenu.up.fun.y = listMenu.up.y;
	listMenu.up.fun.xsize = listMenu.up.w;
	listMenu.up.fun.ysize = listMenu.up.h;
	listMenu.up.fun.spiAlign = ALIGN_LEFT_UP;

	listMenu.down.icon = MENU_LIST_ICON_DOWN;
	listMenu.down.x = MENU_LIST_downX;
	listMenu.down.y = MENU_LIST_downY;
	listMenu.down.w = MENU_LIST_downWidth;
	listMenu.down.h = MENU_LIST_downHeight;
	listMenu.down.iconType = ICON_PIC_DOWN;
	listMenu.down.revColor = COLOR_LIST_MENU_ICON;
	
	
	listMenu.down.fun.x = listMenu.down.x;
	listMenu.down.fun.y = listMenu.down.y;
	listMenu.down.fun.xsize = listMenu.down.w;
	listMenu.down.fun.ysize = listMenu.down.h;
	listMenu.down.fun.spiAlign = ALIGN_LEFT_UP;

	listMenu.scheduleBotW = MENU_LIST_scheduleBotW;
	listMenu.scheduleBotH = MENU_LIST_scheduleBotH;
	listMenu.scheduleTopW = MENU_LIST_scheduleTopW;
	listMenu.scheduleTopH = MENU_LIST_scheduleTopH;
	listMenu.scheduleBotSpriteId = MENU_LIST_ScheduleBottom;
	listMenu.scheduleTopSpriteId = MENU_LIST_ScheduleTop;
	
	listMenu.listType = listInit.listType;

	listMenu.getDisplay = listInit.fun;

    listMenu.textColor = listInit.textColor;											
    listMenu.valColor = listInit.valColor;											
	listMenu.listIconEn = listInit.listIconEn;
	listMenu.valEn = listInit.valEn;
    listMenu.textListOffsetX = listInit.textOffset;
	listMenu.textValOffset = listInit.textValOffset;
	listMenu.selEn = listInit.selEn;
	listMenu.ediEn = listInit.ediEn;

	SetRowAndColumnAndTextSize(listMenu.listType);

	listMenu.onePageListMax = listMenu.lineNum * listMenu.columnNum;
	

	listMenu.listNum = listInit.listCnt;
	listMenu.maxPage = listInit.listCnt/listMenu.onePageListMax + (listInit.listCnt%listMenu.onePageListMax ? 1 : 0);
	listMenu.textBoxWidth = ((listMenu.maxPage > 1) ? (listMenu.textBoxWidth - listMenu.scheduleBotW) : listMenu.textBoxWidth);

	listMenu.lineHeight	= listMenu.textBoxHeight/listMenu.lineNum;
	listMenu.lineWidth = listMenu.textBoxWidth/listMenu.columnNum;

	listMenu.currentPage = listInit.currentPage >= listMenu.maxPage ? 0 : listInit.currentPage;
	
	CreateIcons(listMenu.onePageListMax);

	DisplayOnePage();
}

//删除一条list
int AddOrDeleteMenuListItem(int num)
{
	int i;

	listMenu.listNum += num;
	listMenu.maxPage = listMenu.listNum/listMenu.onePageListMax + (listMenu.listNum%listMenu.onePageListMax ? 1 : 0);

	while((listMenu.currentPage >= listMenu.maxPage) && (listMenu.currentPage > 0))
	{
		listMenu.currentPage--;
	}
	
	CreateIcons(listMenu.onePageListMax);

	DisplayOnePage();
}

void MenuListDiplayPage(void)
{
	DisplayOnePage();
}


LIST_INIT_T ListPropertyDefault(void)
{
	LIST_INIT_T listInit;
	
	listInit.listType = TEXT_5X1;
	listInit.listIconEn = 1;
	listInit.selEn = 0;
	listInit.ediEn = 0;
	listInit.valEn = 0;
	listInit.textValOffset = 0;
	listInit.textOffset = MENU_LIST_YTPE_LIST_STR_OFFSET_X;
	listInit.valColor = MENU_LIST_VAL_COLOR;
	listInit.textColor = MENU_LIST_STR_COLOR;
	listInit.listCnt = 0;
	listInit.fun = NULL;
	listInit.titleStr = NULL;
	listInit.titleStrLen = 0;

	listInit.valAlign = ALIGN_LEFT;
	listInit.textAlign = ALIGN_LEFT;
	listInit.currentPage = 0;

	return listInit;
}


//当大ICON按下的处理
LIST_ICON MenuListIconProcess(int x, int y)
{
	LIST_ICON icon;

	int i;
	
	icon = MenuListGetIcon(x, y);
	
	if(icon.mainIcon != MENU_LIST_ICON_NONE)
	{
		BEEP_KEY();
		SelectMenuListIcon(1, icon);
	}

	return icon;
}

//当大ICON释放的处理
LIST_ICON MenuListIconClick(int x, int y)
{
	LIST_ICON icon;
	int i;

	icon = MenuListGetIcon(x, y);
	
	SelectMenuListIcon(0, icon);

	if(icon.mainIcon == MENU_LIST_ICON_UP)
	{
		MenuListPageUpOrDown(1);
	}
	else if(icon.mainIcon == MENU_LIST_ICON_DOWN)
	{
		MenuListPageUpOrDown(0);
	}

	return icon;
}

static void SetRowAndColumnAndTextSize(LIST_TYPE listType)
{
	switch(listType)
	{
		case ICON_1X2:
			listMenu.lineNum = 1;
			listMenu.columnNum = 2;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = ICON_TYPE;		
			break;
		case ICON_1X3:
			listMenu.lineNum = 1;
			listMenu.columnNum = 3;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = ICON_TYPE;		
			break;
		case ICON_2X2:
			listMenu.lineNum = 2;
			listMenu.columnNum = 2;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = ICON_TYPE;		
			break;
		case ICON_2X3:
			listMenu.lineNum = 2;
			listMenu.columnNum = 3;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = ICON_TYPE;		
			break;
	    case ICON_2X4:
			listMenu.lineNum = 2;
			listMenu.columnNum = 4;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = ICON_TYPE;		
			break;
	    
		case TEXT_5X1:
			listMenu.lineNum = 5;
			listMenu.columnNum = 1;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = TEXT_TYPE;		
			break;
		case TEXT_5X2:
			listMenu.lineNum = 5;
			listMenu.columnNum = 2;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = TEXT_TYPE;		
			break;
		case TEXT_6X1:
			listMenu.lineNum = 6;
			listMenu.columnNum = 1;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = TEXT_TYPE;		
			break;
		case TEXT_10X1:
			listMenu.lineNum = 10;
			listMenu.columnNum = 1;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = TEXT_TYPE;		
			break;
		default:
			listMenu.lineNum = 5;
			listMenu.columnNum = 1;
			listMenu.textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listMenu.dispType = TEXT_TYPE;		
			break;
	}
}

static void CreateIcons(int onePageListMax)
{
	int i;
	
	for(i = 0, listMenu.iconListCnt = 0; i < onePageListMax; i++)
	{
		listMenu.iconList[listMenu.iconListCnt++] = i;
		
		memset(&listMenu.list[i], 0, sizeof(listMenu.list[i]));

		listMenu.list[i].iconType = ICON_PIC_NONE;
		listMenu.list[i].revColor = MENU_LIST_REV_COLOR;
		listMenu.list[i].icon = i;
		listMenu.list[i].x = listMenu.textBoxPositionX + listMenu.lineWidth*(i%listMenu.columnNum);
		listMenu.list[i].y = listMenu.textBoxPositionY + listMenu.lineHeight*(i/listMenu.columnNum);
		listMenu.list[i].w = listMenu.lineWidth;
		listMenu.list[i].h = listMenu.lineHeight;

		if(listMenu.dispType == ICON_TYPE)
		{
			listMenu.list[i].sel.x = listMenu.list[i].x;
			listMenu.list[i].sel.y = listMenu.list[i].y;
			listMenu.list[i].sel.xsize = (listMenu.selEn ? GetIconTypeSelH(listMenu.list[i].h) : 0);
			listMenu.list[i].sel.ysize = listMenu.list[i].sel.xsize;
			
			listMenu.list[i].sel.spiAlign = ALIGN_MIDDLE;
			
			listMenu.list[i].edi.xsize = (listMenu.selEn ? GetIconTypeSelH(listMenu.list[i].h) : 0);
			listMenu.list[i].edi.ysize = listMenu.list[i].edi.xsize;
			listMenu.list[i].edi.x = listMenu.list[i].x + listMenu.list[i].w -listMenu.list[i].edi.xsize;
			listMenu.list[i].edi.y = listMenu.list[i].y;
			
			listMenu.list[i].edi.spiAlign = ALIGN_MIDDLE;
			
			listMenu.list[i].fun.x = listMenu.list[i].x;
			listMenu.list[i].fun.y = listMenu.list[i].y;
			listMenu.list[i].fun.xsize = listMenu.list[i].w;
			listMenu.list[i].fun.ysize = listMenu.list[i].h;
			
			listMenu.list[i].fun.spiAlign = ALIGN_MIDDLE;

			listMenu.list[i].fun.strx = listMenu.list[i].fun.x;
			listMenu.list[i].fun.stry = listMenu.list[i].fun.y + listMenu.textListOffsetX;
			listMenu.list[i].fun.strAlign = listMenu.textAlign;
			listMenu.list[i].fun.strw = listMenu.list[i].fun.xsize;
			listMenu.list[i].fun.strh = listMenu.list[i].fun.ysize - listMenu.textListOffsetX;
		}
		else if(listMenu.dispType == TEXT_TYPE)
		{
			listMenu.list[i].sel.xsize = (listMenu.selEn ? listMenu.list[i].h : 0);
			listMenu.list[i].sel.ysize = listMenu.list[i].sel.xsize;
			listMenu.list[i].sel.x = listMenu.list[i].x;
			listMenu.list[i].sel.y = listMenu.list[i].y;
			listMenu.list[i].sel.spiAlign = ALIGN_MIDDLE;
			
			listMenu.list[i].edi.xsize = (listMenu.ediEn ? listMenu.list[i].h : 0);
			listMenu.list[i].edi.ysize = listMenu.list[i].edi.xsize;
			listMenu.list[i].edi.x = listMenu.list[i].x + listMenu.list[i].w - listMenu.list[i].edi.xsize;
			listMenu.list[i].edi.y = listMenu.list[i].y;
			listMenu.list[i].edi.spiAlign = ALIGN_MIDDLE;


			listMenu.list[i].fun.x = listMenu.list[i].x + listMenu.list[i].sel.xsize;
			listMenu.list[i].fun.y = listMenu.list[i].y;
			listMenu.list[i].fun.xsize = listMenu.list[i].w - listMenu.list[i].sel.xsize - listMenu.list[i].edi.xsize;
			listMenu.list[i].fun.ysize = listMenu.list[i].h;
			

			listMenu.list[i].fun.spiAlign = ALIGN_LEFT;

			listMenu.textValOffset = (listMenu.valEn ? listMenu.textValOffset : listMenu.list[i].fun.xsize);

			listMenu.list[i].fun.strx = listMenu.list[i].fun.x + listMenu.textListOffsetX;
			listMenu.list[i].fun.stry = listMenu.list[i].fun.y;
			listMenu.list[i].fun.strAlign = listMenu.textAlign;
			listMenu.list[i].fun.strw = listMenu.list[i].fun.x + listMenu.textValOffset - listMenu.list[i].fun.strx;
			listMenu.list[i].fun.strh = listMenu.list[i].fun.ysize;

			listMenu.list[i].fun.vlax = listMenu.list[i].fun.x + listMenu.textValOffset;
			listMenu.list[i].fun.vlay = listMenu.list[i].fun.y;
			listMenu.list[i].fun.vlaw = listMenu.list[i].edi.x - listMenu.list[i].fun.vlax;
			listMenu.list[i].fun.vlah = listMenu.list[i].fun.ysize;
			listMenu.list[i].fun.vlaAlign = listMenu.valAlign;
		}
	}
	
	if(listMenu.maxPage > 1)
	{
		listMenu.iconList[listMenu.iconListCnt++] = MENU_LIST_ICON_UP;
		listMenu.iconList[listMenu.iconListCnt++] = MENU_LIST_ICON_DOWN;
	}
}

//获取Icon号
static LIST_ICON MenuListGetIcon(int x, int y)
{
	LIST_ICON_PROPERTY_T iconProperty;
	LIST_ICON icon;
	int i;

	x = TouchToDisplayX(x);
	y = TouchToDisplayY(y);
	

	for(i = 0; i < listMenu.iconListCnt; i++)
	{
		icon.mainIcon = listMenu.iconList[i];
		
		iconProperty = GetOneIconProperty(icon.mainIcon);
		
		if(iconProperty.x < x && x < iconProperty.x + iconProperty.w && 
			iconProperty.y < y && y < iconProperty.y + iconProperty.h)
		{
			if(iconProperty.sel.x < x && x < iconProperty.sel.x + iconProperty.sel.xsize && 
					iconProperty.sel.y < y && y < iconProperty.sel.y + iconProperty.sel.ysize)
			{
				icon.subIcon = MENU_LIST_SUB_ICON_SEL;
			}
			else if(iconProperty.edi.x < x && x < iconProperty.edi.x + iconProperty.edi.xsize && 
						iconProperty.edi.y < y && y < iconProperty.edi.y + iconProperty.edi.ysize)
			{
				icon.subIcon = MENU_LIST_SUB_ICON_EDI;
			}
			else
			{
				icon.subIcon = MENU_LIST_SUB_ICON_FUN;
			}

			if(icon.mainIcon >= 0)
			{
				icon.mainIcon = listMenu.currentPage * listMenu.onePageListMax + icon.mainIcon;
				if((icon.mainIcon >= listMenu.listNum) || (!listMenu.listIconEn))
				{
					icon.mainIcon = MENU_LIST_ICON_NONE;
					break;
				}
			}
			break;
		}
	}

	if(i == listMenu.iconListCnt)
	{
		icon.mainIcon = MENU_LIST_ICON_NONE;
	}
	
	dprintf("x=%d, y=%d, icon.mainIcon=%d, icon.subIcon=%d\n", x, y, icon.mainIcon, icon.subIcon);
	
	return icon;
}

//跳转到指定页数
int MenuListSetCurrentPage(int page)
{
	if(page >= 0 && page*listMenu.onePageListMax <= listMenu.listNum-1)
	{
		listMenu.currentPage = page;
		DisplayOnePage();
	}
	
	return listMenu.currentPage;
}

//获取当前页
int MenuListGetCurrentPage(void)
{
	return listMenu.currentPage;
}

//翻页option 0-下翻， 1-上翻
void MenuListPageUpOrDown(int option)
{
	//上翻
	if(option)
	{
		if(listMenu.currentPage)
		{
			listMenu.currentPage--;
		}
		else
		{
			listMenu.currentPage = (listMenu.listNum ? (listMenu.listNum-1)/listMenu.onePageListMax : 0);
		}
	}
	else
	{
		if((++listMenu.currentPage)*listMenu.onePageListMax > listMenu.listNum-1)
		{
			listMenu.currentPage = 0;
		}
	}
	
	MenuListSetCurrentPage(listMenu.currentPage);
}

//显示菜单列表
static void DisplayOnePage(void)
{
	LIST_DISP_T display = {0};

	if(listMenu.getDisplay != NULL)
	{
		(listMenu.getDisplay)(listMenu.currentPage, listMenu.onePageListMax, &display);
	}

	API_OsdUpdateDisplay(0, 0, 0, 0, 0);
	
	//清除string层
	API_OsdStringClearExt2(listMenu.boxX, listMenu.boxY, OSD_LAYER_STRING, listMenu.boxWidth,  listMenu.boxHeight);

	//清除ICON层
	API_OsdStringClearExt2(listMenu.boxX, listMenu.boxY, OSD_LAYER_CURSOR, listMenu.boxWidth,  listMenu.boxHeight);

	//显示标题
	DisplayTitle();
	DisplayPageNum();
	DisplayTurnPageIcon();
	DisplayListIcon(display);

	API_OsdUpdateDisplay(1, listMenu.boxX, listMenu.boxY, listMenu.boxWidth, listMenu.boxHeight);
}

static int IconIfExist(int mainIcon)
{
	int i, ret;

	for(i = 0, ret = 0; i < listMenu.iconListCnt; i++)
	{
		if(mainIcon == listMenu.iconList[i])
		{
			ret = 1;
			break;
		}
	}

	return ret;
}

static void DisplayTitle(void)
{
	MenuStrMulti_T list;
	
	if(listMenu.titleStrLen > 0)
	{
		list.dataCnt = 0;
		list.data[list.dataCnt].x = listMenu.boxX;
		list.data[list.dataCnt].y = listMenu.boxY;
		list.data[list.dataCnt].fg = listMenu.textColor;
		list.data[list.dataCnt].uBgColor = COLOR_KEY;
		list.data[list.dataCnt].format = STR_UNICODE;
		list.data[list.dataCnt].width = listMenu.up.x - listMenu.boxX;
		list.data[list.dataCnt].height = listMenu.titleHeight;
		list.data[list.dataCnt].fnt_type = listMenu.textSize;
		list.data[list.dataCnt].align = ALIGN_LEFT;
		list.data[list.dataCnt].str_len = listMenu.titleStrLen;
		memcpy(list.data[list.dataCnt].str_dat, listMenu.titleStr, list.data[list.dataCnt].str_len);
		list.dataCnt++;
	}
	
	if(list.dataCnt)
	{
		API_OsdStringDisplayExtMultiLine(list);
	}
}

static void DisplayTurnPageIcon(void)
{
	int i;
	ICON_TYPE_TO_PIC iconToPic;
	LIST_ICON icon;
	LIST_ICON_PROPERTY_T iconProperty;
	MenuSpriteMulti_T sprite;

	for(i = 0, sprite.dataCnt = 0; i < 2; i++)
	{
		icon.mainIcon = ((i == 0) ? MENU_LIST_ICON_UP : MENU_LIST_ICON_DOWN);
		
		if(IconIfExist(icon.mainIcon))
		{
			iconProperty = GetOneIconProperty(icon.mainIcon);
			
			iconToPic = GetPicByIconType(iconProperty.iconType);
			
			if(iconToPic.funFpic != NONE_ICON_PIC)
			{
				sprite.data[sprite.dataCnt].x = iconProperty.fun.x;
				sprite.data[sprite.dataCnt].y = iconProperty.fun.y;
				sprite.data[sprite.dataCnt].width = iconProperty.fun.xsize;
				sprite.data[sprite.dataCnt].height = iconProperty.fun.ysize;
				sprite.data[sprite.dataCnt].spriteId = iconToPic.funFpic;
				sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
				sprite.data[sprite.dataCnt].align = iconProperty.fun.spiAlign;
				sprite.data[sprite.dataCnt].enable = 1;
				sprite.dataCnt++;
			}
		}
	}
	if(sprite.dataCnt)
	{
		API_OsdSpriteDisplayMulti(sprite);
	}
}

static void SetListIconType(int mainIcon, ICON_PIC_TYPE iconType)
{
	int i;

	for(i = 0; i < listMenu.onePageListMax; i++)
	{
		if(listMenu.list[i].icon == mainIcon)
		{
			listMenu.list[i].iconType = iconType;
			break;
		}
	}
}


static void DisplayListIcon(LIST_DISP_T display)
{
	int i;
	ICON_TYPE_TO_PIC iconToPic;
	LIST_ICON icon;
	LIST_ICON_PROPERTY_T iconProperty;
	MenuStrMulti_T list, listValue;
	MenuSpriteMulti_T preSpr, sufSpr, selIconSpr, ediIconSpr, funIconSpr;
	
	list.dataCnt = 0;
	listValue.dataCnt = 0;
	
	preSpr.dataCnt = 0;
	sufSpr.dataCnt = 0;
	selIconSpr.dataCnt = 0;
	ediIconSpr.dataCnt = 0;
	funIconSpr.dataCnt = 0;

	for(i = 0; i < display.dispCnt; i++)
	{
		icon.mainIcon = i;
		
		if(!IconIfExist(icon.mainIcon))
		{
			continue;
		}
		
		SetListIconType(icon.mainIcon, display.disp[i].iconType);
		
		iconProperty = GetOneIconProperty(icon.mainIcon);
		
		iconToPic = GetPicByIconType(iconProperty.iconType);
		
		if(iconToPic.funFpic != NONE_ICON_PIC)
		{
			funIconSpr.data[funIconSpr.dataCnt].x = iconProperty.fun.x;
			funIconSpr.data[funIconSpr.dataCnt].y = iconProperty.fun.y;
			funIconSpr.data[funIconSpr.dataCnt].width = iconProperty.fun.xsize;
			funIconSpr.data[funIconSpr.dataCnt].height = iconProperty.fun.ysize;
			funIconSpr.data[funIconSpr.dataCnt].spriteId = iconToPic.funFpic;
			funIconSpr.data[funIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			funIconSpr.data[funIconSpr.dataCnt].align = iconProperty.fun.spiAlign;
			funIconSpr.data[funIconSpr.dataCnt].enable = 1;
			funIconSpr.dataCnt++;
		}
		
		if(iconToPic.ediFpic != NONE_ICON_PIC)
		{
			ediIconSpr.data[ediIconSpr.dataCnt].x = iconProperty.edi.x;
			ediIconSpr.data[ediIconSpr.dataCnt].y = iconProperty.edi.y;
			ediIconSpr.data[ediIconSpr.dataCnt].width = iconProperty.edi.xsize;
			ediIconSpr.data[ediIconSpr.dataCnt].height = iconProperty.edi.ysize;
			ediIconSpr.data[ediIconSpr.dataCnt].spriteId = iconToPic.ediFpic;
			ediIconSpr.data[ediIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			ediIconSpr.data[ediIconSpr.dataCnt].align = iconProperty.edi.spiAlign;
			ediIconSpr.data[ediIconSpr.dataCnt].enable = 1;
			ediIconSpr.dataCnt++;
		}
		
		if(iconToPic.selFpic != NONE_ICON_PIC)
		{
			selIconSpr.data[selIconSpr.dataCnt].x = iconProperty.sel.x;
			selIconSpr.data[selIconSpr.dataCnt].y = iconProperty.sel.y;
			selIconSpr.data[selIconSpr.dataCnt].width = iconProperty.sel.xsize;
			selIconSpr.data[selIconSpr.dataCnt].height = iconProperty.sel.ysize;
			selIconSpr.data[selIconSpr.dataCnt].spriteId = iconToPic.selFpic;
			selIconSpr.data[selIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			selIconSpr.data[selIconSpr.dataCnt].align = iconProperty.sel.spiAlign;
			selIconSpr.data[selIconSpr.dataCnt].enable = 1;
			selIconSpr.dataCnt++;
		}
			
		if(display.disp[i].strLen != 0)
		{
			list.data[list.dataCnt].x = iconProperty.fun.strx;
			list.data[list.dataCnt].y = iconProperty.fun.stry;
			list.data[list.dataCnt].fg = listMenu.textColor;
			list.data[list.dataCnt].uBgColor = COLOR_KEY;
			list.data[list.dataCnt].format = STR_UNICODE;
			list.data[list.dataCnt].width = iconProperty.fun.strw;
			list.data[list.dataCnt].height = iconProperty.fun.strh;
			list.data[list.dataCnt].fnt_type = listMenu.textSize;
			list.data[list.dataCnt].align = iconProperty.fun.strAlign;
			list.data[list.dataCnt].str_len = display.disp[i].strLen;
			memcpy(list.data[list.dataCnt].str_dat, display.disp[i].str, display.disp[i].strLen);
			list.dataCnt++;
		}

		if(listMenu.valEn)
		{
			if(display.disp[i].valLen != 0)
			{
				listValue.data[listValue.dataCnt].x = iconProperty.fun.vlax;
				listValue.data[listValue.dataCnt].y = iconProperty.fun.vlay;
				listValue.data[listValue.dataCnt].fg = listMenu.valColor;
				listValue.data[listValue.dataCnt].uBgColor = COLOR_KEY;
				listValue.data[listValue.dataCnt].format = STR_UNICODE;
				listValue.data[listValue.dataCnt].width = iconProperty.fun.vlaw;
				listValue.data[listValue.dataCnt].height = iconProperty.fun.vlah;
				listValue.data[listValue.dataCnt].fnt_type = listMenu.textSize;
				listValue.data[listValue.dataCnt].align = iconProperty.fun.vlaAlign;

				listValue.data[listValue.dataCnt].str_len = display.disp[i].valLen;
				memcpy(listValue.data[listValue.dataCnt].str_dat, display.disp[i].val, display.disp[i].valLen);
				listValue.dataCnt++;
			}
		}
		
		if(listMenu.selEn)
		{
			if(display.disp[i].preSprId != 0)
			{
				preSpr.data[preSpr.dataCnt].x = iconProperty.sel.x;
				preSpr.data[preSpr.dataCnt].y = iconProperty.sel.y;
				preSpr.data[preSpr.dataCnt].width = iconProperty.sel.xsize;
				preSpr.data[preSpr.dataCnt].height = iconProperty.sel.ysize;
				preSpr.data[preSpr.dataCnt].spriteId = display.disp[i].preSprId;
				preSpr.data[preSpr.dataCnt].layerId = OSD_LAYER_STRING;
				preSpr.data[preSpr.dataCnt].align = iconProperty.sel.spiAlign;
				preSpr.data[preSpr.dataCnt].enable = 1;
				preSpr.dataCnt++;
			}
		}
		
		if(listMenu.ediEn)
		{
			if(display.disp[i].sufSprId != 0)
			{
				sufSpr.data[sufSpr.dataCnt].x = iconProperty.edi.x;
				sufSpr.data[sufSpr.dataCnt].y = iconProperty.edi.y;
				sufSpr.data[sufSpr.dataCnt].width = iconProperty.edi.xsize;
				sufSpr.data[sufSpr.dataCnt].height = iconProperty.edi.ysize;
				sufSpr.data[sufSpr.dataCnt].spriteId = display.disp[i].sufSprId;
				sufSpr.data[sufSpr.dataCnt].layerId = OSD_LAYER_STRING;
				sufSpr.data[sufSpr.dataCnt].align = iconProperty.edi.spiAlign;
				sufSpr.data[sufSpr.dataCnt].enable = 1;
				sufSpr.dataCnt++;
			}
		}
	}
	
	if(selIconSpr.dataCnt)
	{
		dprintf("selIconSpr.dataCnt = %d, \n", selIconSpr.dataCnt);
		API_OsdSpriteDisplayMulti(selIconSpr);
	}
	
	if(ediIconSpr.dataCnt)
	{
		dprintf("ediIconSpr.dataCnt = %d, \n", ediIconSpr.dataCnt);
		API_OsdSpriteDisplayMulti(ediIconSpr);
	}
	
	if(funIconSpr.dataCnt)
	{
		dprintf("funIconSpr.dataCnt = %d, \n", funIconSpr.dataCnt);
		API_OsdSpriteDisplayMulti(funIconSpr);
	}
		
	if(preSpr.dataCnt)
	{
		dprintf("preSpr.dataCnt = %d, \n", preSpr.dataCnt);
		API_OsdSpriteDisplayMulti(preSpr);
	}
	
	if(sufSpr.dataCnt)
	{
		dprintf("sufSpr.dataCnt = %d, \n", sufSpr.dataCnt);
		API_OsdSpriteDisplayMulti(sufSpr);
	}



	if(list.dataCnt)
	{
		dprintf("list.dataCnt = %d, \n", list.dataCnt);
		API_OsdStringDisplayExtMultiLine(list);
	}
	

	if(listValue.dataCnt)
	{
		dprintf("listValue.dataCnt = %d, \n", listValue.dataCnt);
		API_OsdStringDisplayExtMultiLine(listValue);
	}
}


//取消/选中菜单列表
int SelectMenuListIcon(int enable, LIST_ICON icon)
{
	LIST_ICON_PROPERTY_T iconProperty;
	int color, spriteId, xColor, yColor, xSpr, ySpr, xsize, ysize, align;
	ICON_TYPE_TO_PIC iconToPic;

	iconProperty = GetOneIconProperty(icon.mainIcon);
	iconToPic = GetPicByIconType(iconProperty.iconType);
	color = enable ? iconProperty.revColor : COLOR_KEY;

	if(icon.subIcon == MENU_LIST_SUB_ICON_SEL)
	{
		xColor = iconProperty.sel.x;
		yColor = iconProperty.sel.y;
		xsize = iconProperty.sel.xsize;
		ysize = iconProperty.sel.ysize;
		xSpr = iconProperty.sel.x;
		ySpr = iconProperty.sel.y;
		align = iconProperty.sel.spiAlign;
		
		spriteId = enable ? iconToPic.selBpic : iconToPic.selFpic;
	}
	else if(icon.subIcon == MENU_LIST_SUB_ICON_EDI)
	{
		xColor = iconProperty.edi.x;
		yColor = iconProperty.edi.y;
		xsize = iconProperty.edi.xsize;
		ysize = iconProperty.edi.ysize;
		
		xSpr = iconProperty.edi.x;
		ySpr = iconProperty.edi.y;
		align = iconProperty.edi.spiAlign;

		spriteId = enable ? iconToPic.ediBpic : iconToPic.ediFpic;
	}
	else
	{
		xColor = iconProperty.fun.x;
		yColor = iconProperty.fun.y;
		xsize = iconProperty.fun.xsize;
		ysize = iconProperty.fun.ysize;
		
		xSpr = iconProperty.fun.x;
		ySpr = iconProperty.fun.y;
		align = iconProperty.fun.spiAlign;
		
		spriteId = enable ? iconToPic.funBpic : iconToPic.funFpic;
	}
	
	API_OsdUpdateDisplay(0, 0, 0, 0, 0);

	API_OsdStringClearExt2(xColor, yColor, OSD_LAYER_CURSOR, xsize,  ysize);
	
	if(spriteId != NONE_ICON_PIC)
	{
		API_OsdIconPicDisplay(xSpr, ySpr, xsize, ysize, align, spriteId);
	}
	else
	{
		API_OsdCursorListIconColor(xColor, yColor, xsize, ysize, color, 1);
	}
	
	API_OsdUpdateDisplay(1, xColor, yColor, xsize, ysize);
	
	return 0;
}

static void DisplayPageNum(void)
{
	char display[20];
	int botx, boty, topx, topy;
	int botw, both, topw, toph;
	int topMoveLen;
	MenuSpriteMulti_T sprite;
	MenuStrMulti_T list;
	
		
	if(listMenu.maxPage > 1)
	{
		topMoveLen = listMenu.scheduleBotH - listMenu.scheduleBotW*2 - listMenu.scheduleTopH;

		botx = listMenu.textBoxPositionX + listMenu.textBoxWidth;
		boty = listMenu.textBoxPositionY;
		botw = listMenu.scheduleBotW;
		both = listMenu.textBoxHeight;

		topx = botx;
		topy = boty + topMoveLen * listMenu.currentPage/(listMenu.maxPage - 1);
		topw = listMenu.scheduleBotW;
		toph = listMenu.scheduleTopH;

		sprite.dataCnt = 0;
		sprite.data[sprite.dataCnt].x = botx;
		sprite.data[sprite.dataCnt].y = boty;
		sprite.data[sprite.dataCnt].width = botw;
		sprite.data[sprite.dataCnt].height = both;
		sprite.data[sprite.dataCnt].spriteId = listMenu.scheduleBotSpriteId;
		sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
		sprite.data[sprite.dataCnt].align = ALIGN_MIDDLE;
		sprite.data[sprite.dataCnt].enable = 1;
		sprite.dataCnt++;
		
		sprite.data[sprite.dataCnt].x = topx;
		sprite.data[sprite.dataCnt].y = topy;
		sprite.data[sprite.dataCnt].width = topw;
		sprite.data[sprite.dataCnt].height = toph;
		sprite.data[sprite.dataCnt].spriteId = listMenu.scheduleTopSpriteId;
		sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
		sprite.data[sprite.dataCnt].align = ALIGN_MIDDLE;
		sprite.data[sprite.dataCnt].enable = 1;
		sprite.dataCnt++;

		if(sprite.dataCnt)
		{
			API_OsdSpriteDisplayMulti(sprite);
		}

		
		sprintf(display, "%d/%d", listMenu.currentPage+1, listMenu.maxPage);
		
		list.dataCnt = 0;
		list.data[list.dataCnt].x = listMenu.up.x;
		list.data[list.dataCnt].y = listMenu.boxY;
		list.data[list.dataCnt].fg = COLOR_WHITE;
		list.data[list.dataCnt].uBgColor = COLOR_KEY;
		list.data[list.dataCnt].format = STR_UTF8;
		list.data[list.dataCnt].width = listMenu.up.w + listMenu.down.w;
		list.data[list.dataCnt].height = listMenu.titleHeight;
		list.data[list.dataCnt].fnt_type = listMenu.textSize;
		list.data[list.dataCnt].align = ALIGN_MIDDLE;
		
		list.data[list.dataCnt].str_len = strlen(display);
		memcpy(list.data[list.dataCnt].str_dat, display, list.data[list.dataCnt].str_len);
		list.dataCnt++;
		
		if(list.dataCnt)
		{
			API_OsdStringDisplayExtMultiLine(list);
		}
	}
}


//获取Icon属性
static LIST_ICON_PROPERTY_T GetOneIconProperty(int mainIcon)
{
	LIST_ICON_PROPERTY_T item = {0};

	if(mainIcon == MENU_LIST_ICON_UP)
	{
		item = listMenu.up;
	}
	else if(mainIcon == MENU_LIST_ICON_DOWN)
	{
		item = listMenu.down;
	}
	else if(mainIcon >= 0)
	{
		item = listMenu.list[mainIcon%listMenu.onePageListMax];
	}

	return item;
}

static ICON_TYPE_TO_PIC GetPicByIconType(ICON_PIC_TYPE iconType)
{
	int i, typeToPicNum;
	ICON_TYPE_TO_PIC* typeToPicTable = NULL;
		
	if(listMenu.dispType == ICON_TYPE)
	{
		typeToPicTable = typeToIconPic;
		typeToPicNum = typeToIconPicNum;
	}
	else if(listMenu.dispType == TEXT_TYPE)
	{
		typeToPicTable = typeToListPic;
		typeToPicNum = typeToListPicNum;
	}

	if(typeToPicTable == NULL)
	{
		return typeToIconPic[0];
	}

	for(i = 0; i < typeToPicNum; i++)
	{
		if(typeToPicTable[i].iconType == iconType)
		{
			return typeToPicTable[i];
		}
	}

	return typeToPicTable[0];
}


