
#ifndef _OBJ_VIDEO_H
#define _OBJ_VIDEO_H

/*******************************************************************************
                              Define Msg Type  
*******************************************************************************/
typedef enum
{
   	VIDEO_OFF = 0,
   	VIDEO_ON,
	IMAGE_ADJ,
	IMAGE_SCALE,
	VIDEO_STD_UPGRADE,			//lzh_20140809
} VideoOptType;

typedef enum
{
	CVBS_Y1 = 0,	//cvbs
	CVBS_Y2,		//playback	
	CVBS_Y3,		//-none-
	BT656,
} VIDEO_CHANEL;

typedef enum
{
    AUTO = 0,
	PAL,
	NTSC,	
} VIDEO_STD;

typedef enum
{
	SCALE_4_3 = 0,	
	SCALE_16_9,	
} VIDEO_SCALE;

typedef struct _VIDEO_ATTR
{
	VIDEO_CHANEL 		ch;		//视频的通道
	VIDEO_STD			std;		//视频的制式
	VIDEO_SCALE			scale;	//视频的比例
} VIDEO_ATTR;

// Define Object Property
typedef enum
{
	AUTO_SCALE 	= 0x00,	//缺省类型,自动检测，缩小
	PAL_SCALE		= 0x01,	//PAL，缩小
	NTSC_SCALE		= 0x02,	//NTSC，缩小
	AUTO_FULL		= 0x10,	//自动检测，全屏
	PAL_FULL		= 0x11,	//PAL，全屏
	NTSC_FULL		= 0x12, 	//NTSC，全屏
} VIDEO_BUSINESS_TYPE;

typedef struct
{
	unsigned char 	logBrightCnt;	//用户调节亮度逻辑值
	unsigned char 	logColorCnt;		//用户调节色度逻辑值
	unsigned char 	logContrast;		//用户调节色度逻辑值
} VIDEO_ADJLOG;

typedef struct _VIDEO_STATUS_
{
	unsigned char 			on;		//视频开启标志
	VIDEO_BUSINESS_TYPE	type;	//视频的类型
	VIDEO_ATTR				attr;	//视频属性
	VIDEO_ADJLOG			adjlog;	//视频调节逻辑值
	VIDEO_STD				laststd;	//视频制式的更改
} VIDEO_STATUS;

/**********************************************************************************
**********************************************************************************/
// Define Object Function - Public

/*******************************************************************************************
 * @fn：	Video_TurnOn_Init
 *
 * @brief:	视频开启初始化
 *
 * @param:  	type - 开启模式，std - 开启制式,   logContrast - 对比度逻辑值，logBright - 亮度逻辑值, logColor - 色度逻辑值,
 *
 * @return: 	none
 *******************************************************************************************/
void Video_TurnOn_Init(VIDEO_BUSINESS_TYPE type, VIDEO_STD std, unsigned char logContrast, unsigned char logBright, unsigned char logColor );

/*******************************************************************************************
 * @fn：	Video_TurnOn
 *
 * @brief:	视频开启
 *
 * @param:  	type - 开启模式，std - 开启制式
 *
 * @return: 	none
 *******************************************************************************************/
void Video_TurnOn(int win);

/*******************************************************************************************
 * @fn：	Video_TurnOff
 *
 * @brief:	视频关闭
 *
 * @param:  	none
 *
 * @return: 	none
 *******************************************************************************************/
void Video_TurnOff(void);


extern int CheckAutoVideoType(void);
// 在自动模式下判断制式是否有改变
extern int CheckAutoVideoTypeChange(VIDEO_STD curStd);


#endif


