/**
  ******************************************************************************
  * @file    obj_RM_Client.h
  * @author  DH
  * @version V00.01.00
  * @date    2017.11.2
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_RM_Client_H
#define _obj_RM_Client_H


typedef struct
{
	int len;
	int col;
	int row;
	int color;
	int clear;	
}Display_CMD;



int Rm_Display(unsigned char *disp_buf);




#endif
