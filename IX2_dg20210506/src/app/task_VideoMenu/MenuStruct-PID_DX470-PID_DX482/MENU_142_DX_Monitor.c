#include "MENU_public.h"
#include "MENU_142_DX_Monitor.h"
#include "obj_GetIpByNumber.h"
#include "obj_business_manage.h"		//czn_20190107
#include "task_Ring.h"
#include "task_Power.h"
#include "task_survey.h"
#include "task_monitor.h"		//czn_20170112
#include "obj_memo.h"
#include "task_DXMonitor.h"

#include "task_DXMonitor.h"

static int DxdsWinId, DxDsiconNum;
int brightness,color,contrast;
int adjust_dispx1,adjust_dispx2,adjust_dispx3,adjust_dispy;

void EnterDxDSMonMenu(int win_id, int stack)
{
	DxdsWinId = win_id;
	if(stack == 0)
	{
		VdShowHide();
	}
	StartInitOneMenu(MENU_142_DX_MONITOR,0,stack);
	if(stack == 0)
	{
		usleep(100*1000);
		VdShowToFull(DxdsWinId);
	}
	else
	{
		SetWinMonState(0, 1);
		SetWinDeviceType(0, 2);
	}
}

int GetDxDs_Show_Win(void)
{
	return DxdsWinId;
}
void SetDxDs_Show_Win(int win_id)
{
	DxdsWinId=win_id;
}

const ICON_PIC_TYPE AdjustIconTable[] = 
{
	ICON_PIC_bright,
	ICON_PIC_color,
	ICON_PIC_contrast,
	ICON_PIC_FishEye,
};

void display_adjust_para(int x, int y, int value)
{
	char display[20];
	sprintf(display, "%d", value);
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 0, STR_UTF8, 0);	
}

static void AdjustIconPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	char unicode_buf[200];
	char display[20];

	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < 4)
		{
			pDisp->disp[pDisp->dispCnt].iconType = AdjustIconTable[index];
			pDisp->dispCnt++;
		}
	}
}

void VdAdjustIconShow(void)
{
	LIST_INIT_T listInit;
	int base;
	listInit = ListPropertyDefault();
	
	listInit.listType = ICON_1X4;
	listInit.listCnt = 4;
	listInit.fun = AdjustIconPage;
	if(get_pane_type() == 7)	//dx470v 150*96
	{
		//InitMenuListXY(listInit, 75, 380, 450, 75, 1, 1, ICON_IconView1);
		InitMenuListXY(listInit, 0, 380, 600, 75, 1, 1, ICON_IconView1);
		base=0+70;
		adjust_dispx1 = base;//145;
		adjust_dispx2 = base+150;//295;
		adjust_dispx3 = base+150*2;//445;
		adjust_dispy = 460;
	}
	else
	{
		//InitMenuListXY(listInit, 320, 436, 384, 64, 1, 1, ICON_IconView1);
		InitMenuListXY(listInit, 256, 436, 512, 64, 1, 1, ICON_IconView1);
		base=256+60;
		adjust_dispx1 = base;//380;
		adjust_dispx2 = base+128;//508;
		adjust_dispx3 = base+128*2;//636;
		adjust_dispy = 506;
	} 

	get_sensor_cur_para(&brightness,&color,&contrast);
	display_adjust_para(adjust_dispx1, adjust_dispy, brightness);
	display_adjust_para(adjust_dispx2, adjust_dispy, color);
	display_adjust_para(adjust_dispx3, adjust_dispy, contrast);
}
void VdAdjustIconClose(void)
{
	if(get_pane_type() == 7)	//dx470v
	{
		MenuListClear(1, 1, 75, 380, 450, 75);
		API_OsdStringClearExt(adjust_dispx1, adjust_dispy, 320, 30);
	}
	else 
	{
		MenuListClear(1, 1, 320, 436, 384, 64);
		API_OsdStringClearExt(adjust_dispx1, adjust_dispy, 300, 30);
	}
	 
	MenuListDisable(1);

}



const ICON_PIC_TYPE DxMonIconTable1[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
	ICON_PIC_UNLOCK2,
    ICON_PIC_TALK,
    ICON_PIC_QUAD,
};
const ICON_PIC_TYPE DxMonIconTable2[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
    ICON_PIC_TALK,
    ICON_PIC_QUAD,
};

static void IconListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < DxDsiconNum)
		{
			if(DxDsiconNum == 6)
			{
				pDisp->disp[pDisp->dispCnt].iconType = DxMonIconTable1[index];
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = DxMonIconTable2[index];
			}
			
			pDisp->dispCnt++;
		}
	}
}

int GetDxMonIconNum()
{
	return DxDsiconNum;
}
void IconListPageGates(int currentPage, int onePageListMax, LIST_DISP_T* pDisp);
void DxMonIconListShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	//if(lockProperty.lock1Enable && lockProperty.lock2Enable)
	{
		DxDsiconNum = 6;
		listInit.listType = ICON_1X6;
		listInit.listCnt = 6;
		if(strcmp(GetCustomerizedName(),"GATES")==0||strcmp(GetCustomerizedName(),"AAD")==0)
		{
			listInit.listCnt = 7;
			listInit.fun = IconListPageGates;
		}
		else
			listInit.fun = IconListPage;
		#if 0
		if(get_pane_type() == 7)	//dx470v 100*64
			InitMenuListXY(listInit, 0, 960, 600, 64, 1, 0, ICON_888_ListView);
		else 
			InitMenuListXY(listInit, 128, 536, 768, 64, 1, 0, ICON_888_ListView);
		 #endif
		if(get_pane_type() == 7)	//dx470v 150*96
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}
		else 
		{
			if(strcmp(GetCustomerizedName(),"GATES")==0||strcmp(GetCustomerizedName(),"AAD")==0)
			{
				listInit.listType = ICON_1X7;
				InitMenuListXY(listInit, 64, 536, 768+128, 64, 1, 0, ICON_888_ListView);
			}
			else
				InitMenuListXY(listInit, 128, 536, 768, 64, 1, 0, ICON_888_ListView);
		}
	}
	
}
void DxMonIconListClose(void)
{
	#if 0
	if(get_pane_type() == 7)	//dx470v
		API_ListAlpha_Close(1, 0, 960, 600, 64, OSD_LAYER_CURSOR);
	else 
		API_ListAlpha_Close(1, 128, 536, 768, 64, OSD_LAYER_CURSOR);
	#endif
	if(get_pane_type() == 7)	//dx470v
		API_ListAlpha_Close(1, 0, 832, 600, 192, OSD_LAYER_CURSOR);
	else 
	{
		if(strcmp(GetCustomerizedName(),"GATES")==0||strcmp(GetCustomerizedName(),"AAD")==0)
			API_ListAlpha_Close(1, 64, 536, 768+128, 64, OSD_LAYER_CURSOR);
		else
			API_ListAlpha_Close(1, 128, 536, 768, 64, OSD_LAYER_CURSOR);
	}
	 
	MenuListDisable(0);

}

void DxMonTalkProcess(void)
{
	if(Get_DxMonTalkState())
	{
		//SetDsTalkState(dsWinId, 0);
		//DsTalkClose(dsWinId);
		ExitQuadMenu();
		popDisplayLastMenu();
	}
	else
	{
		if(CheckTalkOnoff() == 0&&Get_DxMonTalkState()==0)
		{
			API_DXMonitor_Talk_On();
		}
	}
}

//#define SENSOR_ADJUST_TEST

#define DISP_BR_POS_X	200
#define DISP_BR_POS_y	500
#define DISP_CL_POS_X	460
#define DISP_CL_POS_y	DISP_BR_POS_y
#define DISP_CO_POS_X	720
#define DISP_CO_POS_y	DISP_BR_POS_y

void display_sensor_para(int x, int y, char* title, int value)
{
	char strbuf[100];
	memset(strbuf,0,sizeof(strbuf));
	sprintf(strbuf,"%s[%d]",title,value);
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, strbuf, strlen(strbuf), 0, STR_UTF8, 0);	
}

void MENU_142_DxMonitor_Init(int uMenuCnt)		
{
	char name_temp[41];
	#if 0
	GetLockProperty(GetDsIp(dsWinId), &lockProperty);
	MonIconListShow();
	SetLayerAlpha(8);
	API_TimeLapseStart(COUNT_RUN_UP , 0);
	GetDs_MonName(dsWinId, name_temp);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, COLOR_WHITE, name_temp, strlen(name_temp), CallMenuDisp_Name_Font, STR_UTF8, 0);
	//DisplayLockSprite(Ds_Menu_Para[dsWinId].dsIp);
	//Quad_Monitor_Timer_Init();
	API_Business_Request(Business_State_MonitorIpc);
	
	if( GetDsTalkState(dsWinId) == 0 && api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		MonTalkProcess();
	}
	#else
	API_Business_Request(Business_State_MonitorIpc);
	DxMonIconListShow();
	SetLayerAlpha(8);
	API_TimeLapseStart(COUNT_RUN_UP , 0);
	GetWinDeviceName(DxdsWinId, name_temp);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, COLOR_WHITE, name_temp, strlen(name_temp), CallMenuDisp_Name_Font, STR_UTF8, 0);
	#endif
	// lzh_20220411 test
	//API_LocalCaptureOn(0);

	#ifdef SENSOR_ADJUST_TEST
	display_sensor_para(DISP_BR_POS_X,DISP_BR_POS_y,"brightness",brightness);
	display_sensor_para(DISP_CL_POS_X,DISP_CL_POS_y,"color",color);
	display_sensor_para(DISP_CO_POS_X,DISP_CO_POS_y,"contrast",contrast);
	#endif

	DisplayMonitorLightState();
}

void MENU_142_DxMonitor_Exit(void)
{
	DxMonIconListClose();
	VdAdjustIconClose();
	API_Business_Close(Business_State_MonitorIpc);
	// lzh_20220411 test
	//API_LocalCaptureOff();
}

void MENU_142_DxMonitor_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char free[20]={0};
	char cpu[30]={0};
	struct {char onOff; char win;} *data;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					DxMonTalkProcess();
					break;			
				case KEY_UNLOCK:
					API_DXMonitor_Unlock1();
					break;
				
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			if(GetCurIcon() == 0)
			{
				if(GetMenuListEn(1) == 1)
					VdAdjustIconClose();
				else
					VdAdjustIconShow();
			}
			else
			{
				switch(GetCurIcon())
				{
					// lzh_20181108_s
					if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
					{
						API_menu_create_bitmap_file(-1,-1,-1);				
					}
					// lzh_20181108_e
					case ICON_IconView1:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_IconView1);
						if(listIcon.mainIcon == 0)
						{
							brightness = sensor_brightness_adjust(1);
							display_adjust_para(adjust_dispx1, adjust_dispy, brightness);
						}
						else if(listIcon.mainIcon == 1)
						{
							color = sensor_color_adjust(1);
							display_adjust_para(adjust_dispx2, adjust_dispy, color);	
						}
						else if(listIcon.mainIcon == 2)
						{
							contrast = sensor_contrast_adjust(1);
							display_adjust_para(adjust_dispx3, adjust_dispy, contrast);
						}
						else if(listIcon.mainIcon == 3)
						{
							API_FishEye_Switch();
						}
						break;
					case ICON_888_ListView:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
						if(listIcon.mainIcon == 0)
						{
							// lzh_20220506_s
							#ifndef SENSOR_ADJUST_TEST
							ExitQuadMenu();
							popDisplayLastMenu();
							#else
							// bright inc
							brightness = sensor_brightness_adjust(1);
							display_sensor_para(DISP_BR_POS_X,DISP_BR_POS_y,"brightness",brightness);
							#endif
							// lzh_20220506_e
						}
						else if(listIcon.mainIcon == 1)
						{
							// lzh_20220506_s
							#ifndef SENSOR_ADJUST_TEST
							if(api_record_start(DxdsWinId)==0)
								SaveVdRecordProcess(DxdsWinId);
							//display_sensor_para(DISP_BR_POS_X,DISP_BR_POS_y,"mic inc",adj_mic_vol(1));
							#else
							// bright dec
							brightness = sensor_brightness_adjust(0);
							display_sensor_para(DISP_BR_POS_X,DISP_BR_POS_y,"brightness",brightness);
							#endif
							// lzh_20220506_e
						}
						else if(listIcon.mainIcon == 2)
						{
							// lzh_20220506_s
							#ifndef SENSOR_ADJUST_TEST
							//set_filter(1);
							API_DXMonitor_Unlock1();
							//vi_read_fail_deal();
							//display_sensor_para(DISP_CL_POS_X,DISP_CL_POS_y,"mic dec",adj_mic_vol(0));
							#else
							// color inc
							color = sensor_color_adjust(1);
							display_sensor_para(DISP_CL_POS_X,DISP_CL_POS_y,"color",color);
							#endif
							// lzh_20220506_e
						}
						else if(listIcon.mainIcon == 6&&(strcmp(GetCustomerizedName(),"GATES")==0||strcmp(GetCustomerizedName(),"AAD")==0))
						{
							//printf("__%s:%d\n",__func__,__LINE__);
							LightIconProcess();
						}
						else
						{
							if(DxDsiconNum == 6)
							{
								if(listIcon.mainIcon == 3)
								{
									// lzh_20220506_s
									#ifndef SENSOR_ADJUST_TEST
									//set_filter(0);
									API_DXMonitor_Unlock2();
									//display_sensor_para(DISP_CL_POS_X,DISP_CL_POS_y,"spk inc",adj_spk_vol(1));
									#else
									// color dec
									color = sensor_color_adjust(0);
									display_sensor_para(DISP_CL_POS_X,DISP_CL_POS_y,"color",color);
									#endif
									// lzh_20220506_e									
								}
								else if(listIcon.mainIcon == 4)
								{
									// lzh_20220506_s
									#ifndef SENSOR_ADJUST_TEST
									DxMonTalkProcess();
									#else
									// contrast inc
									contrast = sensor_contrast_adjust(1);
									display_sensor_para(DISP_CO_POS_X,DISP_CO_POS_y,"contrast",contrast);
									#endif
									// lzh_20220506_e
								}
								else
								{
									// lzh_20220506_s
									#ifndef SENSOR_ADJUST_TEST
									EnterQuadMenu(DxdsWinId, NULL, 0);
									//display_sensor_para(DISP_CO_POS_X,DISP_CO_POS_y,"spk dec",adj_spk_vol(0));
									#else
									// contrast dec
									contrast = sensor_contrast_adjust(0);
									display_sensor_para(DISP_CO_POS_X,DISP_CO_POS_y,"contrast",contrast);
									#endif
									// lzh_20220506_e
								}
							}
							else
							{
								if(listIcon.mainIcon == 3)
								{
									DxMonTalkProcess();
								}
								else
								{
									EnterQuadMenu(DxdsWinId, NULL, 0);
								}
							}
						}
						break;
						
				}	
								
			}

		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			
			case MSG_7_BRD_SUB_MonitorOff:
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_MonitorTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;	
			case MSG_7_BRD_SUB_MonitorUnlock1:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				usleep(1000000);
				//ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				break;

			case MSG_7_BRD_SUB_MonitorUnlock2:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				usleep(1000000);
				//ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				break;
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN:
				if(Get_DxMonTalkState() == 1)
				{
					DxMonTalkProcess();
				}
				break;
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP:
				// ���ժ��
				if(Get_DxMonTalkState() == 0)
				{
					DxMonTalkProcess();
				}
				else
				{
					// ͨ��״̬�£������ֱ�ͨ��״̬��ˢ����Ƶͨ���л�
					api_plugswitch_control_update(2,1);
				}
				break;
			case MSG_7_BRD_SUB_MonitorStartting:
				if(get_pane_type() != 7)
					API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;
			case MSG_7_BRD_SUB_MonitorOn:
				API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;	
			case MSG_7_BRD_SUB_MonitorError:
				BEEP_ERROR();
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(data->onOff, data->win);
				#if 0
				if(!data->onOff)
					SaveVdRecordProcess(dsWinId);
				#endif
				break;	
			case MSG_7_BRD_LightStateChange:
				DisplayMonitorLightState();
				break;
			#if 0	
			case MSG_7_BRD_SUB_GetCpuMemUse:
				GetCpuMem_Polling(free, cpu);
				API_OsdStringDisplayExt(bkgd_w-100, 10, DISPLAY_STATE_COLOR, free, strlen(free), 0, STR_UTF8, 0);	
				API_OsdStringDisplayExt(bkgd_w-250, 50, DISPLAY_STATE_COLOR, cpu, strlen(cpu), 0, STR_UTF8, 0); 
				break;
			

			
				
			
				
			// lzh_20191009_s
			
			
			#endif
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
	
}






