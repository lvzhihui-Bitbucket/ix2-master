#include "MENU_public.h"

FuncMenuType onlineManageSubMenu;

void MENU_022_OnlineManage_Init(int uMenuCnt)
{
	switch(onlineManageSubMenu)
	{
		case MENU_033_ONLINE_MANAGE_CALL_NBR:
			MENU_033_OnlineManageCallNbr_Init(uMenuCnt);
			break;
					
		case MENU_034_ONLINE_MANAGE_INFO:
			MENU_034_OnlineManageInfo_Init(uMenuCnt);
			break;

		case MENU_038_ONLINE_MANAGE_REBOOT:
			MENU_038_OnlineManageReboot_Init(uMenuCnt);
			break;
			
		case MENU_069_OnlineFwUpgrade:
			MENU_069_OnlineFwUpgrade_Init(uMenuCnt);
			break;
			
		case MENU_076_RemoteParaGroup:
			MENU_076_OnlineManageParameter_Init(uMenuCnt);
			break;
			
			
		default:
			break;
			
	}
}

void MENU_022_OnlineManage_Exit(void)
{
	switch(onlineManageSubMenu)
	{
		case MENU_033_ONLINE_MANAGE_CALL_NBR:
			MENU_033_OnlineManageCallNbr_Exit();
			break;

		case MENU_034_ONLINE_MANAGE_INFO:
			MENU_034_OnlineManageInfo_Exit();
			break;
			
		case MENU_038_ONLINE_MANAGE_REBOOT:
			MENU_038_OnlineManageReboot_Exit();
			break;
			
		case MENU_069_OnlineFwUpgrade:
			MENU_069_OnlineFwUpgrade_Exit();
			break;
			
		case MENU_076_RemoteParaGroup:
			MENU_076_OnlineManageParameter_Exit();
			break;
		
		default:
			break;
			
	}
	
}

void MENU_022_OnlineManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{				
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_048_Device:
					EnterOnlineManageMenu(MENU_034_ONLINE_MANAGE_INFO, 0);
					break;

				case ICON_030_CALL_NUMBER:
					EnterOnlineManageMenu(MENU_033_ONLINE_MANAGE_CALL_NBR, 0);
					break;
				case ICON_050_Reboot:
					EnterOnlineManageMenu(MENU_038_ONLINE_MANAGE_REBOOT, 0);
					break;
				case ICON_045_Upgrade:
					EnterOnlineManageMenu(MENU_069_OnlineFwUpgrade, 0);
					break;		
				case ICON_049_Parameter:
					EnterOnlineManageMenu(MENU_076_RemoteParaGroup, 0);
					break;

				default:
					switch(onlineManageSubMenu)
					{
						case MENU_034_ONLINE_MANAGE_INFO:
							MENU_034_OnlineManageInfo_Process(arg);
							break;

						case MENU_033_ONLINE_MANAGE_CALL_NBR:
							MENU_033_OnlineManageCallNbr_Process(arg);
							break;
							
						case MENU_038_ONLINE_MANAGE_REBOOT:
							MENU_038_OnlineManageReboot_Process(arg);
							break;
						case MENU_069_OnlineFwUpgrade:
							MENU_069_OnlineFwUpgrade_Process(arg);
							break;
						
						case MENU_076_RemoteParaGroup:
							MENU_076_OnlineManageParameter_Process(arg);
							break;

						default:
							break;
							
					}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch(onlineManageSubMenu)
		{
			case MENU_034_ONLINE_MANAGE_INFO:
				MENU_034_OnlineManageInfo_Process(arg);
				break;

			case MENU_033_ONLINE_MANAGE_CALL_NBR:
				MENU_033_OnlineManageCallNbr_Process(arg);
				break;
				
			case MENU_038_ONLINE_MANAGE_REBOOT:
				MENU_038_OnlineManageReboot_Process(arg);
				break;
				
			case MENU_069_OnlineFwUpgrade:
				MENU_069_OnlineFwUpgrade_Process(arg);
				break;
				
			case MENU_076_RemoteParaGroup:
				MENU_076_OnlineManageParameter_Process(arg);
				break;
				
			default:
				break;
					
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		switch(onlineManageSubMenu)
		{
			case MENU_034_ONLINE_MANAGE_INFO:
				MENU_034_OnlineManageInfo_Process(arg);
				break;
				
			case MENU_033_ONLINE_MANAGE_CALL_NBR:
				MENU_033_OnlineManageCallNbr_Process(arg);
				break;
			
			case MENU_038_ONLINE_MANAGE_REBOOT:
				MENU_038_OnlineManageReboot_Process(arg);
				break;
				
			case MENU_069_OnlineFwUpgrade:
				MENU_069_OnlineFwUpgrade_Process(arg);
				break;
				
			case MENU_076_RemoteParaGroup:
				MENU_076_OnlineManageParameter_Process(arg);
				break;
				
			default:
				break;
				
		}
	}
}

void EnterOnlineManageMenu(FuncMenuType subMenu, int pushstack)
{
	if(GetCurMenuCnt() == MENU_022_ONLINE_MANAGE)
	{
		if(onlineManageSubMenu == subMenu)
		{
			return;
		}

		//正在等待重启，不能切换
		if(WaitingForRemoteDeviceRebootFlag())
		{
			return;
		}
		
		onlineManageSubMenu = subMenu;
		StartInitOneMenu(MENU_022_ONLINE_MANAGE,onlineManageSubMenu,pushstack);
	}
	else
	{
		onlineManageSubMenu = subMenu;
		StartInitOneMenu(MENU_022_ONLINE_MANAGE,onlineManageSubMenu,pushstack);
	}
}

