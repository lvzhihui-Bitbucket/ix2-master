#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_MS_DeviceTable.h"
#include "obj_GetIpByNumber.h"

//#define	msListIconMax		5
#define ONE_LIST_MAX_LEN		50
int msListIconMax;
int msListIconSelect;
int msListPageSelect;
int msListIndex;
int msListState;
int msListMaxNum = 0;

MS_ListRecord_T msListRecord;

void QuickMSlistCall(char* rm, char* name)
{
	GetIpRspData data;
	Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

	if(API_GetIpNumberFromNet(rm, NULL, NULL, 2, 8, &data) != 0)
	{
		BEEP_ERROR();
		return;
	}
	
	int i,j;
	
	data.cnt = data.cnt > (MAX_CALL_TARGET_NUM) ? MAX_CALL_TARGET_NUM : data.cnt;	
	for(i = 0,j=0; i < data.cnt; i++)
	{
		if(api_nm_if_judge_include_dev(data.BD_RM_MS[i],data.Ip[i])==0)
		{
			continue;
		}
		memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
		memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
		strcpy(target_dev[j].name, name);
		target_dev[j].ip_addr = data.Ip[i];
		j++;
	}
	data.cnt=j;
	return API_CallServer_Invite(IxCallScene3_Active, data.cnt, target_dev);	
}

void StartMSlistCall(int index)	//czn_20170711
{
	MS_ListRecord_T record;
	GetIpRspData data;
	Global_Addr_Stru target_addr;
	uint8 slaveMaster;
	uint8 intercomen;
	uint8 para_buff[20];
	
	msListMaxNum = GetMSListRecordCnt();

	if(msListMaxNum == 0)
	{
		msListMaxNum = GetMSListTempRecordCnt();
	}
	
	if(index < msListMaxNum)
	{
		if(GetMSListRecordCnt() == 0)
		{
			if(GetMsListTempRecordItems(index, &record) != 0)
				return -1;
		}
		else
		{
			if(GetMsListRecordItems(index, &record) != 0)
				return -1;
		}
	
		Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

		if(API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 2, 8, &data) != 0)
		{
			BEEP_ERROR();
			return;
		}
		
		int i,j;
		
		data.cnt = data.cnt > (MAX_CALL_TARGET_NUM) ? MAX_CALL_TARGET_NUM : data.cnt;	
		for(i = 0,j=0; i < data.cnt; i++)
		{
			if(api_nm_if_judge_include_dev(data.BD_RM_MS[i],data.Ip[i])==0)
			{
				continue;
			}
			memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
			memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
			strcpy(target_dev[j].name, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			target_dev[j].ip_addr = data.Ip[i];
			j++;
		}
		data.cnt=j;

		return API_CallServer_Invite(IxCallScene3_Active, data.cnt, target_dev);		//czn_20190127
		
	}
}

void StartBroadcast(void)	//czn_20170711
{
	MS_ListRecord_T record;
	GetIpRspData data;
	Global_Addr_Stru target_addr;
	uint8 slaveMaster;
	uint8 intercomen;
	uint8 para_buff[20];
	int i,j;
	int done_flag=0;
	
	msListMaxNum = GetMSListRecordCnt();

	if(msListMaxNum == 0)
	{
		msListMaxNum = GetMSListTempRecordCnt();
	}
	
	if(msListMaxNum>0)
	{
		for(i=0;i<msListMaxNum;i++)
		{
			if(GetMSListRecordCnt() == 0)
			{
				if(GetMsListTempRecordItems(i, &record) != 0)
					return -1;
			}
			else
			{
				if(GetMsListRecordItems(i, &record) != 0)
					return -1;
			}
		
			

			if(API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 2, 8, &data) != 0)
			{
				continue;
			}
			
			for(j=0;j<data.cnt;j++)
			{
				if(Send_ListenCtrlForceSubscriber_Req(data.Ip[j],0,0,0,AUDIO_CLIENT_UNICAST_PORT)==0)
					done_flag=1;
			}

		}
		
	}
	if(done_flag)
		StartInitOneMenu(MENU_141_ListenTalk,0,1);
	else
		BEEP_ERROR();
}
void DisplayOneMSlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[ONE_LIST_MAX_LEN] = {0};
	char oneRecord[50];
	MS_ListRecord_T record;
	
	msListMaxNum = GetMSListRecordCnt();

	if(msListMaxNum == 0)
	{
		msListMaxNum = GetMSListTempRecordCnt();
	}

	if(index < msListMaxNum)
	{	
		if(GetMSListRecordCnt() == 0)
		{
			if(GetMsListTempRecordItems(index,	&record) != 0)
				return;
		}
		else
		{
			if(GetMsListRecordItems(index,	&record) != 0)
				return;
		}

		record.BD_RM_MS[10] = 0;
		get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, !strcmp(record.R_Name, "-") ? record.name1 : record.R_Name, recordBuffer);

/*
		memcpy(recordBuffer, record.BD_RM_MS, 8);
		
		if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(record.BD_RM_MS, "0099", 4))
		{
			char tempChar[5] = {0};
			memcpy(tempChar, record.BD_RM_MS+4, 4);
			sprintf(recordBuffer, "IM%d(%s)", atoi(tempChar), record.BD_RM_MS+8);
		}
		else
		{
			strcpy(recordBuffer, "IM        (  )");
			memcpy(recordBuffer+2, record.BD_RM_MS, 8);
			memcpy(recordBuffer+11, record.BD_RM_MS+8, 2);
		}

		strcat(recordBuffer, !strcmp(record.R_Name, "-") ? record.name1 : record.R_Name);
*/						

		API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
	}
}

void DisplayOnePageMSlist(uint8 page)
{
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	// zfz_20190605
	msListMaxNum = GetMSListRecordCnt();

	if(msListMaxNum == 0)
	{
		msListMaxNum = GetMSListTempRecordCnt();
	}
	// end

	msListIconMax = GetListIconNum();
	list_start = page*msListIconMax;
	
	for( i = 0; i < msListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		if(list_start+i < msListMaxNum)
		{
			DisplayOneMSlist(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, hv.h - x);
		}
	}
	
	maxPage = msListMaxNum/msListIconMax + (msListMaxNum%msListIconMax ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

void MENU_037_MSList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	msListIconMax = GetListIconNum();
	if(GetLastNMenu() == MENU_003_INTERCOM)
	{
		msListIconSelect = 0;
		msListPageSelect = 0;

		msListState = ICON_270_MS_List;
		API_MenuIconDisplaySelectOn(msListState);
		
		msListMaxNum = GetMSListRecordCnt(); 
		if(msListMaxNum == 0)
		{
			API_ListUpdate(MSG_TYPE_UPDATE_MSLIST_TABLE);
		}	
	}

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, msListState == ICON_270_MS_List  ? MESG_TEXT_Icon_013_InnerCall : MESG_TEXT_NamelistEdit, 1, 0);
	DisplayOnePageMSlist(msListPageSelect);
}

int ConfirmModifyOneMSlist(const char* name)
{
	if(strlen(name) > 0)
	{
		strcpy(msListRecord.R_Name, name);
		MSlist_Rename(msListIndex, name);
		return 1;
	}
	else
	{
		return 0;
	}
}


void MENU_037_MSList_Exit(void)
{
	
}

void MENU_037_MSList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_270_MS_List:
					if(msListState != ICON_270_MS_List)
					{
						msListState = ICON_270_MS_List;
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_Icon_013_InnerCall, 1, 0);
						API_MenuIconDisplaySelectOff(ICON_256_Edit);
						API_MenuIconDisplaySelectOn(ICON_270_MS_List);
					}
					break;
					
				case ICON_256_Edit:
					//czn_20190221_s
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)
					{
						if(GetMSListRecordCnt() == 0)
						{
							return;
						}
						
						if(msListState != ICON_256_Edit)
						{
							msListState = ICON_256_Edit;
							API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
							API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_NamelistEdit, 1, 0);
							API_MenuIconDisplaySelectOff(ICON_270_MS_List);
							API_MenuIconDisplaySelectOn(ICON_256_Edit);
						}
					}
					else
					{
						BusySpriteDisplay(1);
						updateCnt = API_MsSyncListReq(MsSyncListId_MsNamelist);
						
						//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
						//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
						OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
						x = pos.x;
						y = pos.y+(hv.v - pos.y)/2;
						if(updateCnt >= 0)
						{
							sprintf(display, "MS list sync %d items        ", updateCnt);
						}
						else
						{
							if(updateCnt == -2)
							{
								sprintf(display, "MS list sync fail:master offline");
							}
							else
							{
								sprintf(display, "MS list sync fail:transmit error");
							}
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
						
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						if(updateCnt >= 0)
						{
							if(!GetMSListRecordCnt())
							{
								AddTempMSlist_TableBySearching();
							}
							msListIconSelect = 0;
							msListPageSelect = 0;
							msListState = ICON_270_MS_List;
							DisplayOnePageMSlist(msListPageSelect);
						}
						
						sleep(1);
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
					}
					//czn_20190221_e
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					msListMaxNum = GetMSListRecordCnt();
					if(msListMaxNum == 0)
					{
						msListMaxNum = GetMSListTempRecordCnt();
					}
					
					msListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					msListIndex = msListPageSelect*msListIconMax + msListIconSelect;
					if(msListIndex >= msListMaxNum)
					{
						return;
					}

					if(msListState == ICON_256_Edit)
					{
						GetMsListRecordItems(msListIndex,  &msListRecord);
						EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, msListRecord.R_Name, 20, COLOR_WHITE, msListRecord.R_Name, 1, ConfirmModifyOneMSlist);
					}
					else
					{
						StartMSlistCall(msListIndex);
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&msListPageSelect, msListIconMax, msListMaxNum, (DispListPage)DisplayOnePageMSlist);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&msListPageSelect, msListIconMax, msListMaxNum, (DispListPage)DisplayOnePageMSlist);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_MSListUpdate:
				API_DisableOsdUpdate();
				DisplayOnePageMSlist(msListPageSelect);
				API_EnableOsdUpdate();
				break;
				
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


