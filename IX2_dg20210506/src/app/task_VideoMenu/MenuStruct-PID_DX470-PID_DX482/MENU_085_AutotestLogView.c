//20181017
#include "MENU_public.h"

void Create_AutotestLog_Index_Tb(void);
void free_AutotestLog_Index_Tb(void);
int Get_AutotestLog_Num(void);
int Get_One_AutotestLog(int index,char *log_str);

#define	AutotestLog_LIST_ICON_MAX		5

int AutotestLogIconSelect;
int AutotestLogPageSelect;
//int SipStatIndex;
int logNum;

void DisplayOneAutotestLog(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[100] = {0};
	//char oneRecord[50];


	if(index < logNum)
	{
		
		API_OsdStringClearExt(x, y, width, CLEAR_STATE_H);
		if(GetLastNMenu() == MENU_108_QuickAccess)
		{
			if(Get_One_RebootLog(index,recordBuffer) == 0)
			{
				API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer), fnt_type, STR_UTF8, width);
			}
		}
		else
		{
			if(Get_One_AutotestLog(index,recordBuffer) == 0)
			{
				API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer), fnt_type, STR_UTF8, width);
			}
		}
	}
}

void DisplayOnePageAutotestLog(uint8 page)
{
 	//CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;
	
	if( logNum == 0)
	{
		printf("---siplog is empty----\n");
		return -1;
	}
	
	//API_DisableOsdUpdate();

	list_start = page*AutotestLog_LIST_ICON_MAX;
	
	for( i = 0; i < AutotestLog_LIST_ICON_MAX; i++ )
	{
		if((list_start+i) >= logNum)
			break;
		
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
		DisplayOneAutotestLog( list_start+i, x-25, y, DISPLAY_LIST_COLOR, 1, bkgd_w-x);
	}
	for( ; i < AutotestLog_LIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		
 		API_OsdStringClearExt(x-25, y, bkgd_w-x, CLEAR_STATE_H);
	}
	
	maxPage = logNum/AutotestLog_LIST_ICON_MAX + (logNum%AutotestLog_LIST_ICON_MAX ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}



void MENU_085_AutotestLogView_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	//if(GetLastNMenu() == MENU_061_SipTools)
	{
		AutotestLogIconSelect = 0;
		AutotestLogPageSelect = 0;
		//nameListState = 0;
	}
	if(GetLastNMenu() == MENU_108_QuickAccess)
	{
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_RebootLog, 1, 0);
		Create_RebootLog_Index_Tb();
		logNum = Get_RebootLog_Num();
	}
	else
	{
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_AutotestLog, 1, 0);
		Create_AutotestLog_Index_Tb();
		logNum = Get_AutotestLog_Num();
	}
	
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	
	if(  logNum== 0)
	{
		//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X,MENULIST_PAGENUM_POS_Y, COLOR_WHITE,0,0);
		usleep(1000000);
		popDisplayLastMenu();
		return;
	}
	
	DisplayOnePageAutotestLog(AutotestLogPageSelect);
}


void MENU_085_AutotestLogView_Exit(void)
{
	if(GetLastNMenu() == MENU_108_QuickAccess)
		free_RebootLog_Index_Tb();
	else
		free_AutotestLog_Index_Tb();
}

void MENU_085_AutotestLogView_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	uint8 temp;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					#if 0
					SipToolsIconSelect = GetCurIcon() - ICON_007_PublicList1;
					SipToolsIndex = SipToolsPageSelect*SIP_STAT_LIST_ICON_MAX + SipToolsIconSelect;
					
					if(SipToolsIndex >= SipToolsIconNum)
					{
						return;
					}
					switch(SipToolsIconTable[SipToolsIndex].iCon)
					{
						case ICON_325_SipCallTest:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
						case ICON_326_SipCallStatistics:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
						case ICON_327_SipCallStatisticsClear:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
						case ICON_328_ViewSipCallLog:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
						case ICON_329_SipLogCopy:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
						case ICON_330_ViewRegSipPhone:
							//StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
							break;
					}
					#endif
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&AutotestLogPageSelect, AutotestLog_LIST_ICON_MAX, logNum, (DispListPage)DisplayOnePageAutotestLog);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&AutotestLogPageSelect, AutotestLog_LIST_ICON_MAX, logNum, (DispListPage)DisplayOnePageAutotestLog);
					break;				
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




