
#ifndef _OBJ_MENU_LIST_H
#define _OBJ_MENU_LIST_H


//����������ת��ʾ����
#define TouchToDisplayX(touchX) 				(1280 - (touchX))
#define TouchToDisplayY(touchY) 				(800 - (touchY))

#define GetIconTypeSelH(iconHeight) 			((iconHeight)/3)

#define GetIconTypeFunSpiY(startY, height) 		((startY) + (height)*3/8)

//���ƿ��λ�úʹ�С
#define MENU_LIST_boxX							350
#define MENU_LIST_boxY							0
#define MENU_LIST_boxWidth						930
#define MENU_LIST_boxHeight						682
#define MENU_LIST_titleHeight					116

#define MENU_LIST_textBoxX						390
#define MENU_LIST_textBoxY						140
#define MENU_LIST_textBoxWidth					870
#define MENU_LIST_textBoxHeight					542

//����������
#define MENU_LIST_ITEM_MAX						(10)
#define MENU_LIST_CHAR_MAX						200
#define MENU_LIST_VAL_CHAR_MAX					60

#define MENU_LIST_VAL_COLOR						COLOR_RED
#define MENU_LIST_STR_COLOR						COLOR_SLIVER_GRAY
#define MENU_LIST_REV_COLOR						COLOR_LIST_MENU_ICON

#define MENU_LIST_TEXT_SIZE_BIG					1
#define MENU_LIST_TEXT_SIZE_SMALL				2

#define MENU_LIST_YTPE_LIST_STR_OFFSET_X		32

//������λ�ö���
#define MENU_LIST_scheduleBotW					35
#define MENU_LIST_scheduleBotH					542

#define MENU_LIST_scheduleTopW					33
#define MENU_LIST_scheduleTopH					89

//����ICON���궨��
#define MENU_LIST_upX							875
#define MENU_LIST_upY							0
#define MENU_LIST_upWidth						202
#define MENU_LIST_upHeight						113

#define MENU_LIST_downX							1078
#define MENU_LIST_downY							0
#define MENU_LIST_downWidth						202
#define MENU_LIST_downHeight					113

//����ICON�Ŷ���
#define MENU_LIST_ICON_UP						(-2)
#define MENU_LIST_ICON_DOWN						(-3)
#define MENU_LIST_ICON_NONE						(-1)

#define MENU_LIST_SUB_ICON_FUN					0
#define MENU_LIST_SUB_ICON_SEL					1
#define MENU_LIST_SUB_ICON_EDI					2

#define NONE_ICON_PIC							0

#define MenuListResMaxNum						10


#define MENU_LIST_ICON_PIC_DOWN					SPRITE_800_PageDwon
#define MENU_LIST_ICON_PIC_DOWNS				SPRITE_801_PageDwonS
#define MENU_LIST_ICON_PIC_UP					SPRITE_802_PageUp
#define MENU_LIST_ICON_PIC_UPS					SPRITE_803_PageUpS

#define MENU_LIST_ICON_PIC_LIST					SPRITE_VideoBindingIPCList

#define MENU_LIST_ScheduleBottom				SPRITE_ScheduleBottom
#define MENU_LIST_ScheduleTop					SPRITE_ScheduleTop


typedef enum
{
	ICON_PIC_NONE,
	ICON_PIC_UP,
	ICON_PIC_DOWN,
	ICON_PIC_DS,
	ICON_PIC_IPC,
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
	ICON_PIC_UNLOCK2,
    ICON_PIC_TALK,
    ICON_PIC_QUAD,
	ICON_PIC_Exit2,
	ICON_PIC_IPC2,
	ICON_PIC_NAMECALL,
	ICON_PIC_WIPC,
	ICON_PIC_GUARD,
	ICON_PIC_LIST_DS,
	ICON_PIC_LIST_IPC,
	ICON_PIC_LIST_WIPC,
	ICON_PIC_bright,
	ICON_PIC_color,
	ICON_PIC_contrast,
	ICON_PIC_FishEye,//22
	ICON_PIC_FastUnlock1,
	ICON_PIC_FastUnlock2,
	ICON_PIC_Light,
    
}ICON_PIC_TYPE;

typedef enum
{
	PRE_SPR_NONE,
	PRE_SPR_SELECT,
	PRE_SPR_Collect,
	
}PRE_SPR_TYPE;

typedef enum
{
	SUF_SPR_NONE,
	SUF_SPR_Wifi_QUALITY1,
	SUF_SPR_Wifi_QUALITY2,
	SUF_SPR_Wifi_QUALITY3,
	SUF_SPR_Wifi_QUALITY4,
}SUF_SPR_TYPE;

typedef enum
{
	BACK_GROUND_NONE,
	BACK_GROUND_ICON,
	BACK_GROUND1_List5_1,
	BACK_GROUND2_List6_1,
	BACK_GROUND3_List10_1,
}BG_TYPE;

typedef struct
{
    ICON_PIC_TYPE iconType;

    PRE_SPR_TYPE preSprType;				//�����sprite
    SUF_SPR_TYPE sufSprType;				//���ұ�sprite
    
    int strLen;					//��ʾ�ַ�����
    int valLen;					//��ʾ�ַ�����
    
    char str[MENU_LIST_CHAR_MAX];	//һҳ����ʾ����
    char val[MENU_LIST_VAL_CHAR_MAX];	//һҳ����ʾ����
}ICON_DISP_T;

typedef struct
{
	int dispCnt;
    ICON_DISP_T disp[MENU_LIST_ITEM_MAX];
} LIST_DISP_T;

//�����ȡһҳ��ʾ���ݵĻص�����
typedef  void (*MenuListGetDisplay)(int startIndex, int num, LIST_DISP_T* disp);

typedef struct
{
    int mainIcon;											//��ICON
    int subIcon;											//��ICON
} LIST_ICON;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	int spiAlign;

	//str��ʾ����
    int strx;
	int stry;
	int strw;
	int strh;
	int strAlign;
	
	//vlaue��ʾ����
    int vlax;
	int vlay;
	int vlaw;
	int vlah;
	int vlaAlign;
} ICON_FUN_T;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	//ѡ����ʾ��ʼλ��
	int spiAlign;
} ICON_SEL_T;

typedef struct
{
    int x;
    int y;
    int xsize;
    int ysize;

	//ѡ����ʾ��ʼλ��
	int spiAlign;
} ICON_EDI_T;

typedef struct
{
	int icon;
	
	int	revColor;			//������ɫ
	ICON_PIC_TYPE iconType;

	int x;
	int y;
	int w;
	int h;
	
    ICON_FUN_T fun;
    ICON_SEL_T sel;
    ICON_EDI_T edi;
} LIST_ICON_PROPERTY_T;

typedef struct
{
	ICON_PIC_TYPE iconType;
    int selFpic;
    int selBpic;
    int funFpic;
    int funBpic;
    int ediFpic;
    int ediBpic;
} ICON_TYPE_TO_PIC;


typedef struct
{
	PRE_SPR_TYPE sprType;
    int sprId;
}PRE_SPR_TYPE_TO_PIC;

typedef struct
{
	SUF_SPR_TYPE sprType;
    int sprId;
}SUF_SPR_TYPE_TO_PIC;

typedef struct
{
	BG_TYPE bgType;
    int bgId;
}BG_TYPE_TO_PIC;

typedef struct
{
	ICON_TYPE_TO_PIC iconPic[30];
	int iconPicNum;

	ICON_TYPE_TO_PIC listPic[MenuListResMaxNum];
	int listPicNum;
	
    PRE_SPR_TYPE_TO_PIC preSpr[MenuListResMaxNum];				//�����sprite id
	int preSprNum;
    
    SUF_SPR_TYPE_TO_PIC sufSpr[MenuListResMaxNum];				//���ұ�sprite id
	int sufSprNum;

	BG_TYPE_TO_PIC bg[MenuListResMaxNum];
	int bgNum;
	
}MenuListResTable_T;


typedef enum
{
	ICON_1X1,
	ICON_1X2,
	ICON_1X3,
	ICON_1X4,
	ICON_1X5,
	ICON_1X6,
	ICON_4X1,
	ICON_2X2,
	ICON_2X3,
    ICON_2X4,
	ICON_4X2,
    
	TEXT_5X1,
	TEXT_5X2,
	TEXT_6X1,
	TEXT_10X1,
	ICON_1X7,
} LIST_TYPE;
	
typedef enum
{
	ICON_TYPE,
	TEXT_TYPE,
} LIST_DISP_TYPE;

typedef struct
{
	LIST_TYPE listType;
	int listIconEn;
	int selEn;
	int ediEn;
	int valEn;
	int textValOffset;
	int textValOffsetY;
	int textOffset;
	int valColor;
	int textColor;
	int textSize;	
	int valAlign;
	int textAlign;
	unsigned int listCnt;
	MenuListGetDisplay fun;
	int titleColor;
	int titleSize;
	int titleOffset;
	char* titleStr;
	int titleStrLen;
	int currentPage;
} LIST_INIT_T;

typedef struct
{
	int listViewEnable;
	int listViewAlpha;
	int listViewId;
	
    LIST_TYPE listType;										//list ����  TEXT_LIST�� ICON_LIST
	LIST_DISP_TYPE dispType;								//��ʾ����

	//���ƿ���
	int boxX;
	int boxY;
	int boxWidth;
	int boxHeight;

	//����ͼ
	BG_TYPE bg;

	//�ı�����
    int textBoxPositionX;									//�ı���λ��
    int textBoxPositionY;									//�ı���λ��
	int textBoxWidth;										//�ı����
	int textBoxHeight;										//�ı����

	
	int titleHeight;										//��������
	int lineHeight;											//ÿ�еĸ߶�
	int lineWidth;											//ÿ�еĿ���
	
	int selEn;												//����ѡ��ICON
	int ediEn;												//�����༭ICON
	int listIconEn;											//�Ƿ���ӦICON
	int valEn;												//�Ƿ���ֵ��ʾ

	int titleColor;
	int titleSize;
	int titleOffset;
	int titleStr[MENU_LIST_CHAR_MAX];						//������ʾ�ַ�
	int titleStrLen;										//�����ַ�����
	
	//�Ϸ�ҳICON
	LIST_ICON_PROPERTY_T up;

	//�·�ҳICON
	LIST_ICON_PROPERTY_T down;

	//List ICON
	LIST_ICON_PROPERTY_T list[MENU_LIST_ITEM_MAX];

	int iconList[MENU_LIST_ITEM_MAX+2];
	int iconListCnt;

	//������
	int scheduleBotW;
	int scheduleBotH;
	int scheduleBotSpriteId;
	int scheduleTopW;
	int scheduleTopH;
	int scheduleTopSpriteId;

    int columnNum;											//����
    int lineNum;											//����
    
    int textColor;											//������ɫ
	int valColor;
    int textSize;											//�ֺ�
    
    int textListOffsetX;									//�б���ʾƫ��X
	int textValOffset;
	int textValOffsetY;
	int valAlign;
	int textAlign;
    
    int maxPage;											//���ҳ
    int listNum;											//list����
    int currentPage;										//��ǰҳ
    int onePageListMax;										//ÿҳ���list����

    MenuListGetDisplay getDisplay;
    
} LIST_PROPERTY;

//��ʼ�����󣬲���ʾ��һҳ��������Ҫ��ʱ�ı�Ĳ���
int InitMenuList(LIST_INIT_T listInit);
void MenuListDiplayPage(void);

LIST_INIT_T ListPropertyDefault(void);


//����ICON���µĴ���
LIST_ICON MenuListIconProcess(int x, int y, int id);

//����ICON�ͷŵĴ���
LIST_ICON MenuListIconClick(int x, int y, int id);

//�ص���ҳ
void ResetMenuListCurrentPage(void);

//��ת��ָ��ҳ��
int MenuListSetCurrentPage(int page);

//��ȡ��ǰҳ��
int MenuListGetCurrentPage(void);

//��ҳoption 0-�·��� 1-�Ϸ�
void MenuListPageUpOrDown(int option);

//��ʾ�˵��б�
static void DisplayOnePage(int id);

static void DisplayTitle(void);
static void DisplayTurnPageIcon(void);
static void DisplayListIcon(LIST_DISP_T display, int id);

static ICON_TYPE_TO_PIC GetPicByIconType(ICON_PIC_TYPE iconType, int id);
static int GetPicByPreSprType(PRE_SPR_TYPE preSprType);
static int GetPicBySufSprType(SUF_SPR_TYPE sufSprType);
static int GetPicByBgType(BG_TYPE bgType);

static void SetRowAndColumnAndTextSize(LIST_TYPE listType, int id);
static void CreateIcons(int onePageListMax, int id);
static void DisplayPageNum(void);


//���Ի�ȡ������ICON
static int SelectMenuListIcon(int enable, LIST_ICON icon, int id);

//ͨ�����������ȡICONֵ
static LIST_ICON MenuListGetIcon(int x, int y, int id);

//��ʾ������
static void MenuListDisplaySchedule(int x, int y, int curPage, int pageNum);


//��ȡһ���б��Ĵ�С��λ��
static LIST_ICON_PROPERTY_T GetOneIconProperty(int mainIcon, int id);


#endif



