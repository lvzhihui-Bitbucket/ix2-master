#include "MENU_021_InstallOnsiteTools.h"
#include "obj_SearchIpByFilter.h"

#define MAX_SET_NUM 	5
//static const char tempChar[MAX_SET_NUM][31] = {"ALL", "IM", "OS", "DS", "GL"};
//static const char tempChar[MAX_SET_NUM][31] = {"ALL", "IM", "VM", "OS", "DS", "GL"};
static const int tempCharTextid[MAX_SET_NUM] = 
{
MESG_TEXT_CusOnsiteSearchAll,
MESG_TEXT_CusOnsiteSearchIm,
MESG_TEXT_CusOnsiteSearchOs,
MESG_TEXT_CusOnsiteSearchDs,
MESG_TEXT_CusOnsiteSearchGl,
};
const char * searchMaxnum[4] = {"32", "64", "128", "256"};

#define	OnsiteTools_ICON_MAX	5
int OnsiteToolsIconSelect;
int OnsiteToolsPageSelect;
char searchAndProgBdNbr[10] = {0};
Type_e searchAndProgType;
//int searchTimeout;
int searchNum,searchNumSelect;
char searchAndProgInput[10] = {0};

const IconAndText_t searchAndProgSetMode0[] = 
{
	{ICON_288_BD_NBR,					MESG_TEXT_ICON_288_BD_NBR},
	{ICON_289_Device,					MESG_TEXT_ICON_289_Device},
	{ICON_291_SearchTime,				MESG_TEXT_ICON_291_SearchTime},
	{ICON_290_Search,					MESG_TEXT_ICON_290_Search},
	{ICON_299_SearchTest, 				MESG_TEXT_ICON_SearchTest}
};


const unsigned char searchAndProgSetNumMode0 = sizeof(searchAndProgSetMode0)/sizeof(searchAndProgSetMode0[0]);

const IconAndText_t searchAndProgSetMode1[] = 
{
	{ICON_288_BD_NBR,					MESG_TEXT_ICON_288_BD_NBR},
	{ICON_289_Device,					MESG_TEXT_ICON_289_Device},
	{ICON_291_SearchTime,				MESG_TEXT_ICON_291_SearchTime},
	{ICON_290_Search,					MESG_TEXT_ICON_290_Search},
	//{ICON_299_SearchTest, 				MESG_TEXT_ICON_SearchTest}
};


const unsigned char searchAndProgSetNumMode1 = sizeof(searchAndProgSetMode1)/sizeof(searchAndProgSetMode1[0]);
static IconAndText_t *searchAndProgSet=NULL;
static int searchAndProgSetNum=0;

static void DisplaysearchAndProgPageIcon(int page)
{
	int i;
	uint16 x, y;
	int pageNum;
	char display[5];
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < OnsiteTools_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnsiteTools_ICON_MAX+i < searchAndProgSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, searchAndProgSet[i+page*OnsiteTools_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(searchAndProgSet[i+page*OnsiteTools_ICON_MAX].iCon)
			{
				case ICON_288_BD_NBR:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, searchAndProgBdNbr, strlen(searchAndProgBdNbr), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_289_Device:
					//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, tempChar[searchAndProgType], strlen(tempChar[searchAndProgType]), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,tempCharTextid[searchAndProgType],1,hv.h-x);
					break;
					
				case ICON_291_SearchTime:
					//snprintf(display, 5, "%d", searchTimeout);
					snprintf(display, 5, "%d", searchNum);	//改为Maxinum
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = searchAndProgSetNum/OnsiteTools_ICON_MAX + (searchAndProgSetNum%OnsiteTools_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 

int SetSearchBdNumber(const char* number)
{
	strcpy(searchAndProgBdNbr, number);
	return 1;
}

void SetSearchDeviceType(int select)
{
	searchAndProgType = select;
}
/*
int SetSearchTimeout(const char* number)
{
	searchTimeout = atoi(number);
	return 1;
}*/
void SetSearchMaxnum(int select)
{
	if(select < 4)
	{
		searchNum = atoi(searchMaxnum[select]);
		searchNumSelect = select;
	}
}


void MENU_021_OnsiteTools_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	char temp[5];
	API_Event_IoServer_InnerRead_All(OnsiteToolsMenuMode, (uint8*)temp);	
	if(atoi(temp)==1)
	{
		searchAndProgSet = searchAndProgSetMode1;
		searchAndProgSetNum=searchAndProgSetNumMode1;
	}
	else
	{
		searchAndProgSet = searchAndProgSetMode0;
		searchAndProgSetNum=searchAndProgSetNumMode0;
	}
	if(GetLastNMenu()==MENU_011_SETTING||GetLastNMenu()==MENU_015_INSTALL_SUB||GetLastNMenu()==MENU_008_SET_INSTALLER||GetLastNMenu()==MENU_108_QuickAccess)
	{
		SetSearchMaxnum(0);
	}
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_031_OnsiteTools);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_031_OnsiteTools, 1, 0);
	OnsiteToolsIconSelect = 0;
	OnsiteToolsPageSelect = 0;
	
	DisplaysearchAndProgPageIcon(OnsiteToolsPageSelect);
}

void MENU_021_OnsiteTools_Exit(void)
{

}

void MENU_021_OnsiteTools_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					OnsiteToolsIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(OnsiteToolsPageSelect*OnsiteTools_ICON_MAX+OnsiteToolsIconSelect >= searchAndProgSetNum)
					{
						return;
					}

					switch(searchAndProgSet[OnsiteToolsPageSelect*OnsiteTools_ICON_MAX+OnsiteToolsIconSelect].iCon)
					{
 						case ICON_288_BD_NBR:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_288_BD_NBR, searchAndProgBdNbr, 4, COLOR_WHITE, searchAndProgBdNbr, 1, SetSearchBdNumber);
							break;
						case ICON_289_Device:
							if(1)
							{
								int i, j;
								char temp[5];
								API_Event_IoServer_InnerRead_All(OnsiteToolsMenuMode, (uint8*)temp);
								for(i = 0; (atoi(temp)==0&&i < MAX_SET_NUM)||(atoi(temp)==1&&i < 4); i++)
								{
									API_GetOSD_StringWithID(tempCharTextid[i], NULL, 0, NULL, 0,&publicSettingDisplay[i][1], &j);		//FOR_INDEXA
									publicSettingDisplay[i][0] = j;
								}
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_289_Device, MAX_SET_NUM, searchAndProgType, SetSearchDeviceType);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							}
							break;
						case ICON_291_SearchTime:
							//snprintf(searchAndProgInput, 5, "%d", searchTimeout);
							//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_291_SearchTime, searchAndProgInput, 2, COLOR_WHITE, searchAndProgInput, 1, SetSearchTimeout);
							InitPublicSettingMenuDisplay(4,searchMaxnum);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_291_SearchTime, 4, searchNumSelect, SetSearchMaxnum);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_290_Search:
							StartInitOneMenu(MENU_025_SEARCH_ONLINE,0,1);
							break;
						case ICON_299_SearchTest:
							StartInitOneMenu(MENU_132_SEARCH_TEST,0,1);
							break;
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




