#include "MENU_public.h"

#define	SETTING_ICON_MAX	5
FuncMenuType settingSubMenu;

void MENU_011_Settings_Init(int uMenuCnt)
{
	switch(settingSubMenu)
	{
		case MENU_006_CALL_TUNE:
			MENU_006_SetCallTune_Init(uMenuCnt);
			break;
		case MENU_007_SET_GENERAL:
			MENU_007_SetGeneral_Init(uMenuCnt);
			break;
		case MENU_008_SET_INSTALLER:
			MENU_008_SetInstaller_Init(uMenuCnt);
			break;
		//case MENU_009_SIP_CONFIG:
		//	MENU_009_SipConfig_Init(uMenuCnt);
		//	break;
		case MENU_107_Manager:
			MENU_107_Manager_Init(uMenuCnt);
			break;
		case MENU_079_ExternalUnit:
			MENU_079_ExternalUnit_Init(uMenuCnt);
			break;
	}
}

void MENU_011_Settings_Exit(void)
{
	switch(settingSubMenu)
	{
		case MENU_006_CALL_TUNE:
			MENU_006_SetCallTune_Exit();
			break;
		case MENU_007_SET_GENERAL:
			MENU_007_SetGeneral_Exit();
			break;
		case MENU_008_SET_INSTALLER:
			MENU_008_SetInstaller_Exit();
			break;
		//case MENU_009_SIP_CONFIG:
		//	MENU_009_SipConfig_Exit();
		//	break;
		case MENU_107_Manager:
			MENU_107_Manager_Exit();
			break;
		case MENU_079_ExternalUnit:
			MENU_079_ExternalUnit_Exit();
			break;
	}
	
}

void MENU_011_Settings_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	//czn_20191123_s
	char tempchar[5];
	int PwdPlace;
	int Pwden;
	//czn_20191123_e

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					//popDisplayLastMenu();
					GoHomeMenu();		//czn_20191123
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_024_CallTune:
					EnterSettingMenu(MENU_006_CALL_TUNE, 0);
					break;
				case ICON_025_general:
					API_Event_IoServer_InnerRead_All(GerneralPwdPlace, (uint8*)tempchar);
					PwdPlace = atoi(tempchar);
					API_Event_IoServer_InnerRead_All(GerneralPwdEnable, (uint8*)tempchar);
					Pwden= atoi(tempchar);
					if(PwdPlace == 0||Pwden ==0||(JudgeIsInstallerMode() || API_AskDebugState(100)))
					{
						EnterSettingMenu(MENU_007_SET_GENERAL, 0);
					}
					else
					{
						extern char installerInput[9];
						extern int GeneralSettingVerifyPassword(const char* password);
						//SaveIcon(ICON_033_Manager);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, GeneralSettingVerifyPassword);
					}
					break;
				case ICON_026_InstallerSetup:
					//czn_20191123_s
					API_Event_IoServer_InnerRead_All(InstallerPwdPlace, (uint8*)tempchar);
					PwdPlace = atoi(tempchar);
					//API_Event_IoServer_InnerRead_All(InstallerPwdAlways, (uint8*)tempchar);
					//installerPwdAllway =  atoi(tempchar);
					if(PwdPlace ==0||(JudgeIsInstallerMode() || API_AskDebugState(100)))
					{
						EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
					}
					else
					{
						extern char installerInput[9];
						extern int VerifyPassword(const char* password);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 8, COLOR_WHITE, NULL, 0, VerifyPassword);
					}
					//czn_20191123_e
					break;
				//case ICON_027_SipConfig:
				//	EnterSettingMenu(MENU_009_SIP_CONFIG, 0);
				//	break;
				case ICON_033_Manager:
					//czn_20191123_s
					API_Event_IoServer_InnerRead_All(ManagePwdPlace, (uint8*)tempchar);
					PwdPlace = atoi(tempchar);
					API_Event_IoServer_InnerRead_All(ManagePwdEnable, (uint8*)tempchar);
					Pwden= atoi(tempchar);
					if(Pwden == 0||PwdPlace ==0||(JudgeIsInstallerMode() || API_AskDebugState(100)))
					{
						EnterSettingMenu(MENU_107_Manager, 0);
					}
					else
					{
						extern char installerInput[9];
						extern int MananerSettingVerifyPassword(const char* password);
						SaveIcon(ICON_033_Manager);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, MananerSettingVerifyPassword);
					}
					//czn_20191123_e
					break;
				case ICON_051_ExternalUnit:
					//czn_20191123_s
					API_Event_IoServer_InnerRead_All(ExtUnitPwdPlace, (uint8*)tempchar);
					PwdPlace = atoi(tempchar);
					API_Event_IoServer_InnerRead_All(ExtUnitPwdEnable, (uint8*)tempchar);
					Pwden= atoi(tempchar);
					if(Pwden == 0||PwdPlace ==0||(JudgeIsInstallerMode() || API_AskDebugState(100)))
					{
						EnterSettingMenu(MENU_079_ExternalUnit, 0);
					}
					else
					{
						extern char installerInput[9];
						extern int ExtUnitVerifyPassword(const char* password);
						//SaveIcon(ICON_033_Manager);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, ExtUnitVerifyPassword);
					}
					//czn_20191123_e
					break;

				default:
					switch(settingSubMenu)
					{
						case MENU_006_CALL_TUNE:
							MENU_006_SetCallTune_Process(arg);
							break;
						case MENU_007_SET_GENERAL:
							MENU_007_SetGeneral_Process(arg);
							break;
						case MENU_008_SET_INSTALLER:
							MENU_008_SetInstaller_Process(arg);
							break;
						//case MENU_009_SIP_CONFIG:
						//	MENU_009_SipConfig_Process(arg);
						//	break;
						case MENU_107_Manager:
							MENU_107_Manager_Process(arg);
							break;
						case MENU_079_ExternalUnit:
							MENU_079_ExternalUnit_Process(arg);
							break;
					}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch(settingSubMenu)
		{
			case MENU_006_CALL_TUNE:
				MENU_006_SetCallTune_Process(arg);
				break;
			case MENU_007_SET_GENERAL:
				MENU_007_SetGeneral_Process(arg);
				break;
			case MENU_008_SET_INSTALLER:
				MENU_008_SetInstaller_Process(arg);
				break;
			//case MENU_009_SIP_CONFIG:
			//	MENU_009_SipConfig_Process(arg);
			//	break;
			case MENU_107_Manager:
				MENU_107_Manager_Process(arg);
				break;
			case MENU_079_ExternalUnit:
				MENU_079_ExternalUnit_Process(arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		switch(settingSubMenu)
		{
			case MENU_006_CALL_TUNE:
				MENU_006_SetCallTune_Process(arg);
				break;
			case MENU_007_SET_GENERAL:
				MENU_007_SetGeneral_Process(arg);
				break;
			case MENU_008_SET_INSTALLER:
				MENU_008_SetInstaller_Process(arg);
				break;
			//case MENU_009_SIP_CONFIG:
			//	MENU_009_SipConfig_Process(arg);
			//	break;
			case MENU_107_Manager:
				MENU_107_Manager_Process(arg);
				break;
			case MENU_079_ExternalUnit:
				MENU_079_ExternalUnit_Process(arg);
				break;
		}
	}
}

void EnterSettingMenu(FuncMenuType subMenu, int pushstack)
{
	if(GetCurMenuCnt() == MENU_011_SETTING)
	{
		if(settingSubMenu == subMenu)
		{
			return;
		}
		
		// lzh_20181016_s
#if 0
		//settingSubMenu = subMenu;
		//StartInitOneMenu(MENU_011_SETTING,settingSubMenu,pushstack);
#else
		if( subMenu == MENU_006_CALL_TUNE || subMenu == MENU_007_SET_GENERAL || subMenu == MENU_008_SET_INSTALLER || subMenu == MENU_107_Manager || subMenu == MENU_079_ExternalUnit)
		{
			MENU_011_Settings_Exit();
			settingSubMenu = subMenu;
			MENU_011_Settings_Init(0);
		}
		else
		{
			settingSubMenu = subMenu;
			StartInitOneMenu(MENU_011_SETTING,settingSubMenu,pushstack);
		}
#endif
		// lzh_20181016_e
	}
	else
	{
		settingSubMenu = subMenu;
		StartInitOneMenu(MENU_011_SETTING,settingSubMenu,pushstack);
	}
}

