#include "MENU_public.h"
#include "MENU_045_SDCard.h"


#define	SD_CARD_ICON_MAX	5
int sdCardIconSelect;
int sdCardPageSelect;


const SDCardIcon sdCardIconTable[] = 
{
	{ICON_060_SDInformation,				MESG_TEXT_ICON_060_SDInformation},    
	{ICON_061_CopyImage,					MESG_TEXT_ICON_061_CopyImage},
//	{ICON_062_Format,						MESG_TEXT_ICON_062_Format},
	//{ICON_307_SdFwUpdate,					MESG_TEXT_ICON_307_SdFwUpdate}		//czn_20181219
};

const int sdCardIconNum = sizeof(sdCardIconTable)/sizeof(sdCardIconTable[0]);

static void DisplaySDCardPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[11];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < SD_CARD_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*SD_CARD_ICON_MAX+i < sdCardIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, sdCardIconTable[i+page*SD_CARD_ICON_MAX].iConText, 1, (hv.h - pos.x)/2);
		}
	}
	pageNum = sdCardIconNum/SD_CARD_ICON_MAX + (sdCardIconNum%SD_CARD_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}


void MENU_045_SDCard_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	sdCardIconSelect = 0;
	sdCardPageSelect = 0;

	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_025_general);

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_SDCard,1, 0);
	DisplaySDCardPageIcon(sdCardPageSelect);
}

void MENU_045_SDCard_Exit(void)
{
	
}

void MENU_045_SDCard_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[100];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					switch(GetCurIcon())
					{
						case ICON_060_SDInformation:
							StartInitOneMenu(MENU_040_SD_CARD_INFO,0,1);
							break;
						case ICON_061_CopyImage:
							if(Judge_SdCardLink())
							{
								BusySpriteDisplay(1);
								snprintf(tempChar,100, "cp -R %s %s\n", JPEG_STORE_DIR, DISK_SDCARD);
								system(tempChar);
								sync();
								usleep(500000);
								BusySpriteDisplay(0);
							}
							else
							{
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								sleep(1);
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
							}
							break;
						case ICON_062_Format:
							
							break;
						//case ICON_307_SdFwUpdate:
						//	Api_FwUpdate_UseSdCard();
						//	break;
							
					}
					break;
				case KEY_UP:
					PublicUpProcess(&sdCardIconSelect, SD_CARD_ICON_MAX);
					break;
				case KEY_DOWN:
					PublicDownProcess(&sdCardIconSelect, SD_CARD_ICON_MAX);
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					sdCardIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(sdCardPageSelect*SD_CARD_ICON_MAX+sdCardIconSelect >= sdCardIconNum)
					{
						return;
					}
					switch(sdCardIconTable[sdCardPageSelect*SD_CARD_ICON_MAX+sdCardIconSelect].iCon)
					{
						case ICON_060_SDInformation:
							StartInitOneMenu(MENU_040_SD_CARD_INFO,0,1);
							break;
						case ICON_061_CopyImage:
							if(Judge_SdCardLink())
							{
								BusySpriteDisplay(1);
								MakeDir(VIDEO_STORE_DIR);

								snprintf(tempChar,100, "cp -r %s/* %s\n", JPEG_STORE_DIR, VIDEO_STORE_DIR);
								system(tempChar);
								sync();
								BusySpriteDisplay(0);
								UpdateOkSpriteNotify();
							}
							else
							{
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								sleep(1);
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
							}
							break;
							
						case ICON_062_Format:
							break;	
						case ICON_307_SdFwUpdate:
							Api_FwUpdate_UseSdCard();
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


