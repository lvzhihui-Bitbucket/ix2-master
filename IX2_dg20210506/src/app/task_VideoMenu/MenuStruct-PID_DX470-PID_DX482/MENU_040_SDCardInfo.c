#include "MENU_public.h"

#define	SDCARD_ICON_MAX	3
int sdCardIconSelect;
int sdCardPageSelect;
int sdCardConfirm;
int sdCardConfirmSelect;

void MENU_040_SDCardInfo_Init(int uMenuCnt)
{
	int x, y;
	int freesize, totalsize;
	char display[20];
	POS pos;
	SIZE hv;
	
	sdCardIconSelect = 0;
	sdCardPageSelect = 0;
	sdCardConfirm = 0;
	sdCardConfirmSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_025_general);

	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_SDCardInfo,1, 0);

	if(Judge_SdCardLink())
	{
		API_GetSDCardSize(&freesize, &totalsize);

		snprintf(display, 20, "%d MB", totalsize);
		OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_TotalSize,1, NULL, display, 0);

		snprintf(display, 20, "%d MB", freesize);
		OSD_GetIconInfo(ICON_008_PublicList2, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_FreeSize,1, NULL, display, 0);
		
		OSD_GetIconInfo(ICON_009_PublicList3, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;

		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_Format, 1, 0);
	}
	else
	{
		OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_HaveNoSDCard, 1, 0);
		sleep(1);
		popDisplayLastMenu();
	}
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);

}

void MENU_040_SDCardInfo_Exit(void)
{
	
}

void MENU_040_SDCardInfo_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[100];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
				case KEY_UP:
					PublicUpProcess(&sdCardIconSelect, SDCARD_ICON_MAX);
					break;
				case KEY_DOWN:
					PublicDownProcess(&sdCardIconSelect, SDCARD_ICON_MAX);
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				case ICON_009_PublicList3:
					sdCardIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(ConfirmSelect(&sdCardConfirm, &sdCardConfirmSelect, &sdCardIconSelect, sdCardPageSelect, SDCARD_ICON_MAX))
					{
						return;
					}
					BusySpriteDisplay(1);
					snprintf(tempChar,100, "rm -r %s\n",VIDEO_STORE_DIR);
					system(tempChar);
					sync();
					BusySpriteDisplay(0);
					UpdateOkSpriteNotify();
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


