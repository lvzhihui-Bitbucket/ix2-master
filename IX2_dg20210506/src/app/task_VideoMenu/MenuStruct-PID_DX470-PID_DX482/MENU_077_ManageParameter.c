#include "MENU_077_ManageParameter.h"
#include "obj_ProgInfoByIp.h"
#include "obj_DeviceManage.h"
#include "MENU_public.h"

#define	ManageParameter_ICON_MAX		5
int manageParameterIconSelect;
int manageParameterPageSelect;

const IconAndText_t manageParameterSetMode0[] = 
{
	//{ICON_ParaGroup0,					MESG_TEXT_ICON_ParaGroup0},
	//{ICON_ParaGroup1,					MESG_TEXT_ICON_ParaGroup1},
	//{ICON_ParaGroup2,					MESG_TEXT_ICON_ParaGroup2},
	{ICON_ParaGroup3,					MESG_TEXT_ICON_ParaGroup3},
	{ICON_ParaGroup4,					MESG_TEXT_ICON_ParaGroup4},
	{ICON_ParaGroup5,					MESG_TEXT_ICON_ParaGroup5},
};

const int manageParameterSetNumMode0 = sizeof(manageParameterSetMode0)/sizeof(manageParameterSetMode0[0]);

const IconAndText_t manageParameterSetMode1[] = 
{
	//{ICON_ParaGroup0,					MESG_TEXT_ICON_ParaGroup0},
	//{ICON_ParaGroup1,					MESG_TEXT_ICON_ParaGroup1},
	//{ICON_ParaGroup2,					MESG_TEXT_ICON_ParaGroup2},
	{ICON_ParaGroup3,					MESG_TEXT_ICON_ParaGroup3},
	{ICON_ParaGroup4,					MESG_TEXT_ICON_ParaGroup4},
	{ICON_ParaGroup5,					MESG_TEXT_ICON_ParaGroup5},
};

const int manageParameterSetNumMode1 = sizeof(manageParameterSetMode1)/sizeof(manageParameterSetMode1[0]);

IconAndText_t *manageParameterSet = NULL;
int manageParameterSetNum;

static void DisplayManageParameterPageIcon(int page)
{
	uint8 i, j;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;

	//API_DisableOsdUpdate();
		
	for(i = 0; i < ManageParameter_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*ManageParameter_ICON_MAX+i < manageParameterSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, manageParameterSet[i+page*ManageParameter_ICON_MAX].iConText, 1,  val_x - x);
		}
	}
	pageNum = manageParameterSetNum/ManageParameter_ICON_MAX + (manageParameterSetNum%ManageParameter_ICON_MAX ? 1 : 0);


	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 

void MENU_077_ManageParameter_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	char temp[5];
	API_Event_IoServer_InnerRead_All(ParaManaMenuMode, (uint8*)temp);
	
	if(atoi(temp) == 1)
	{
		manageParameterSet = manageParameterSetMode1;
		manageParameterSetNum = manageParameterSetNumMode1;
	}
	else
	{
		manageParameterSet = manageParameterSetMode0;
		manageParameterSetNum = manageParameterSetNumMode0;
	}
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	//API_OsdStringClearExt(DISPLAY_TITLE_X,DISPLAY_TITLE_Y,DISPLAY_TITLE_WIDTH,CLEAR_STATE_H);
	//API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_044_Parameter, strlen(GetOnlineManageTitle()), 1, STR_UTF8, DISPLAY_TITLE_WIDTH);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_044_Parameter, 1, 0);
	manageParameterIconSelect = 0;
	manageParameterPageSelect = 0;
	DisplayManageParameterPageIcon(manageParameterPageSelect);
}

void MENU_077_ManageParameter_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_049_Parameter);
}

void MENU_077_ManageParameter_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastNMenu(2);
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&manageParameterPageSelect, ManageParameter_ICON_MAX, manageParameterSetNum, (DispListPage)DisplayManageParameterPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&manageParameterPageSelect, ManageParameter_ICON_MAX, manageParameterSetNum, (DispListPage)DisplayManageParameterPageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					manageParameterIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(manageParameterPageSelect*ManageParameter_ICON_MAX+manageParameterIconSelect >= manageParameterSetNum)
					{
						return;
					}
					
					switch(manageParameterSet[manageParameterPageSelect*ManageParameter_ICON_MAX+manageParameterIconSelect].iCon)
					{
						case ICON_ParaGroup0:
							SetParaGroupNumber(0);
							break;
						case ICON_ParaGroup1:
							SetParaGroupNumber(1);
							break;
						case ICON_ParaGroup2:
							SetParaGroupNumber(2);
							break;
						case ICON_ParaGroup3:
							SetParaGroupNumber(3);
							break;
						case ICON_ParaGroup4:
							SetParaGroupNumber(4);
							break;
						case ICON_ParaGroup5:
							SetParaGroupNumber(5);
							break;

					}
					StartInitOneMenu(MENU_071_ParaManage,0,1);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}





