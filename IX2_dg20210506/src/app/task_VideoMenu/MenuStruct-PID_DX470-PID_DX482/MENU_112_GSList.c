#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"
#include "MENU_139_KeySelect.h"

//#define	gsListIconMax		5
#define ONE_LIST_MAX_LEN		50

int gsListIconSelect;
int gsListPageSelect;
int gsListIndex;
int gsListState;
int gsListMaxNum = 0;
int gsListIconMax;



void StartGSlistCall(int index)	//czn_20170711
{
	IM_NameListRecord_T record;
	GetIpRspData data;
	Global_Addr_Stru target_addr;
	uint8 slaveMaster;
	uint8 intercomen;
	uint8 para_buff[20] = {0};
	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	//API_Event_IoServer_InnerRead_All(IntercomEnable, (uint8*)&intercomen);
	//if(intercomen == 0)
	//{
	//	BEEP_ERROR();
	//	return;
	//}
	
	if(index < gsListMaxNum)
	{
		
		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(index,	&record) != 0)
				return -1;
		}
		else
		{
			if(GetImNameListRecordItems(index,	&record) != 0)
				return -1;
		}
		
		extern EXT_MODULE_KEY_RECORD oneExtModuleKeyRecord;
		if(GetKeyConfigListSelectFlag())
		{
			strcpy(oneExtModuleKeyRecord.number,record.BD_RM_MS);
			AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
			SetKeyConfigListSelectFlag(0);
			popDisplayLastNMenu(2);
			return;
		}
		
		memcpy(para_buff, record.BD_RM_MS,10);
		//strcat(para_buff,"00");
		
	
		Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

		if(API_GetIpNumberFromNet(para_buff, NULL, NULL, 2, 8, &data) != 0)
		{
			BEEP_ERROR();
			return;
		}
		
		int i,j;
		
		data.cnt = data.cnt > MAX_CALL_TARGET_NUM ? MAX_CALL_TARGET_NUM : data.cnt;	
		for(i = 0,j=0; i < data.cnt; i++)
		{
			if(api_nm_if_judge_include_dev(data.BD_RM_MS[i], data.Ip[i])==0)
			{
				 continue;
			}
			memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
			memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
			strcpy(target_dev[j].name, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			target_dev[j].ip_addr = data.Ip[i];

			j++;
		}
		data.cnt=j;

		if(API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev) < 0)
		{
			BEEP_ERROR();
		}
		//return API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev);
		
		//return Interface_Caller_Start_Use_GlobalAddr(&target_addr);
	}
}

void DisplayOneGSlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[ONE_LIST_MAX_LEN] = {0};
	char oneRecord[50];
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;
		
	if(index < gsListMaxNum)
	{

		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(index,	&record) != 0)
				return;
		}
		else
		{
			if(GetImNameListRecordItems(index,	&record) != 0)
				return;
		}
		strcpy(oneRecord,  strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
		get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, oneRecord, recordBuffer);
		
		API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
	}
}

void DisplayOnePageGSlist(uint8 page)
{
	//API_EnableOsdUpdate();

	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	gsListMaxNum = GetImNamelistGLNum();
	POS pos;
	SIZE hv;
	
	gsListIconMax = GetListIconNum();
	for( i = 0; i < gsListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		API_SpriteClose(pos.x, y, SPRITE_Collect);

		list_start = page*gsListIconMax+i;
		if(list_start < gsListMaxNum)
		{
			DisplayOneGSlist(list_start, x, y, DISPLAY_LIST_COLOR, 1, 0);
		}
	}
	
	maxPage = gsListMaxNum/gsListIconMax + (gsListMaxNum%gsListIconMax ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

void MENU_112_GSList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	if(GetLastNMenu() == MENU_003_INTERCOM || GetLastNMenu() == MENU_140_KeyConfig)
	{
		gsListIconSelect = 0;
		gsListPageSelect = 0;

		gsListState = ICON_181_MS_List;
		API_MenuIconDisplaySelectOn(gsListState);
		
		gsListMaxNum =  GetImNamelistGLNum(); 
		if(gsListMaxNum == 0)
		{
			API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
		}	
	}
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_Icon_014_GuardStation, 1, 0);
	DisplayOnePageGSlist(gsListPageSelect);
}




void MENU_112_GSList_Exit(void)
{
	SetKeyConfigListSelectFlag(0);
}

void MENU_112_GSList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					#if 0
					gsListMaxNum = GetMSListRecordCnt();
					if(gsListMaxNum == 0)
					{
						gsListMaxNum = GetMSListTempRecordCnt();
					}
					#endif
					gsListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					gsListIndex = gsListPageSelect*gsListIconMax + gsListIconSelect;
					if(gsListIndex >= gsListMaxNum)
					{
						return;
					}

					
					{
						StartGSlistCall(gsListIndex);
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&gsListPageSelect, gsListIconMax, gsListMaxNum, (DispListPage)DisplayOnePageGSlist);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&gsListPageSelect, gsListIconMax, gsListMaxNum, (DispListPage)DisplayOnePageGSlist);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_NameListUpdate:
				API_DisableOsdUpdate();
				DisplayOnePageGSlist(gsListPageSelect);
				API_EnableOsdUpdate();
				break;
				
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


