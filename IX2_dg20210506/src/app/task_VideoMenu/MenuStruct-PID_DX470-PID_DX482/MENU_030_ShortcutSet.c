#include "MENU_public.h"
#include "MENU_030_ShortcutSet.h"
#include "obj_ImNameListTable.h"
#include "obj_MS_DeviceTable.h"

#define	SHORTCUT_SET_STATE_ICON			0
#define	SHORTCUT_SET_STATE_NAMELIST		1
#define	SHORTCUT_SET_STATE_MONITOR		2
#define	SHORTCUT_SET_STATE_INNER_CALL	3
#define	SHORTCUT_SET_STATE_GUARD_STATION	4

#define	SHORTCUT_SET_STATE_UNLOCK1	5
#define	SHORTCUT_SET_STATE_UNLOCK2	6


//#define	shortcutIconMax	5
int shortcutIconMax;

int shortcutSetIconSelect;
int shortcutSetPageSelect;
int shortcutSetListPageSelect;
int shortcutSetSelect;
int shortcutSetState;
int shortcutSetIconNum = 0;
static int ipcNum;

extern one_vtk_table* nameListTable;


const ShortcutSetIcon shortcutSetIconTable0[] = 
{
	{ICON_301_RemoveShortcutSetting, 	MESG_TEXT_ICON_301_RemoveShortcut},
	{ICON_302_MontorList,				MESG_TEXT_ICON_302_MontorList},
	{ICON_012_namelist,					MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,				MESG_TEXT_Icon_013_InnerCall},
	{ICON_014_GuardStation,				MESG_TEXT_Icon_014_GuardStation},
};
const ShortcutSetIcon shortcutSetIconTable1[] = 
{
	{ICON_301_RemoveShortcutSetting, 	MESG_TEXT_ICON_301_RemoveShortcut},
	{ICON_302_MontorList,				MESG_TEXT_ICON_302_MontorList},
	{ICON_204_quad,						MESG_TEXT_QuadMonitor},
};
const ShortcutSetIcon shortcutSetIconTable2[] = 
{
	{ICON_301_RemoveShortcutSetting, 	MESG_TEXT_ICON_301_RemoveShortcut},
	{ICON_302_MontorList,				MESG_TEXT_ICON_302_MontorList},
	{ICON_012_namelist,					MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,				MESG_TEXT_Icon_013_InnerCall},
};

const ShortcutSetIcon shortcutSetIconTable3[] = 
{
	{ICON_301_RemoveShortcutSetting, 	MESG_TEXT_ICON_301_RemoveShortcut},
	{ICON_302_MontorList,				MESG_TEXT_ICON_302_MontorList},
	{ICON_012_namelist,					MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,				MESG_TEXT_Icon_013_InnerCall},
	{ICON_014_GuardStation,				MESG_TEXT_Icon_014_GuardStation},
	{ICON_Unlock1List,				MESG_TEXT_Unlock1List},
	{ICON_Unlock2List,				MESG_TEXT_Unlock2List},
};

static ShortcutSetIcon *shortcutSetIconTable=NULL;

// zfz_20190601
const char* short_cut_io[] = 
{
	MainShortcut1Select,
	MainShortcut2Select,
	MainShortcut3Select,
	MainShortcut4Select,
};

char* get_short_cut_io_id(void)
{
	if( shortcutSetSelect == ICON_379_Shortcut5 )
	{
		return FAST_MON_KEY_SHORTCUT;
	}
	else
	{
		return short_cut_io[shortcutSetSelect - ICON_229_ShortcutSet1];
	}
}

int read_short_cut_io_val(char* id, char* rm, char* name, char* index)
{
	char temp[100]={0};
	char *pos1, *pos2;
	int len, type; 
	API_Event_IoServer_InnerRead_All(id, temp);
	type = atoi(temp);
	printf("read_short_cut_io= %s value= %s ,type=%d\n", id, temp,type);
	if(rm != NULL)
	{
		pos1 = strstr(temp, "RM=");
		if(pos1 != NULL)
		{
			pos2 = strchr(pos1, '>');
			if(pos2 != NULL)
			{
				len = (int)(pos2-pos1) - strlen("RM=");
				memcpy(rm, pos1+strlen("RM="),len);
				rm[len] = 0;
			}
		}
	}
	if(name != NULL)
	{
		pos1 = strstr(temp, "NA=");
		if(pos1 != NULL)
		{
			pos2 = strchr(pos1, '>');
			if(pos2 != NULL)
			{
				len = (int)(pos2-pos1) - strlen("NA=");
				memcpy(name, pos1+strlen("NA="),len);
				name[len] = 0;
			}
		}
	}
	if(index != NULL)
	{
		pos1 = strstr(temp, "IX=");
		if(pos1 != NULL)
		{
			pos2 = strchr(pos1, '>');
			if(pos2 != NULL)
			{
				len = (int)(pos2-pos1) - strlen("IX=");
				memcpy(index, pos1+strlen("IX="),len);
				index[len] = 0;
			}
		}
	}
	printf("read_short_cut_io_val type=%d,name:%s,rm:%s\n",type,name,rm);
	return (type);
}

void write_short_cut_io_val(char* id, int type, char* rm, char* name)
{
	char temp[100];
	if(type == 0 || type == 6 || type == 10 || type == 11)
	{
		sprintf(temp, "%d", type);
	}
	else
	{
		sprintf(temp, "%d<RM=%s><NA=%s>", type, rm, name);
	}
	//printf("write_short_cut_io_val= %s value= %s\n", id, temp);
	API_Event_IoServer_InnerWrite_All(id, temp);
	if(id != FAST_MON_KEY_SHORTCUT)
		SetShortcut(id, (type == 0? 0 : 1));
}

void DisplayShortcutPageMonlist(uint8 page);
void DisplayShortcutPageNamelist(uint8 page);
void DisplayOnePageMSlist(uint8 page);

static void DisplayShortcutSetPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;
	
	char temp[41];
	API_Event_IoServer_InnerRead_All(GLListDis, (uint8*)temp);
	//API_DisableOsdUpdate();
	if( shortcutSetSelect == ICON_379_Shortcut5 )
	{
		shortcutSetIconNum = 3;
		shortcutSetIconTable = shortcutSetIconTable1;
	}
	else
	{
		
		if(atoi(temp) == 1)
		{
			shortcutSetIconNum = sizeof(shortcutSetIconTable2)/sizeof(shortcutSetIconTable2[0]);
			shortcutSetIconTable = shortcutSetIconTable2;
		}
		else
		{
			if(strcmp(GetCustomerizedName(),"GATES")==0)
			{
				shortcutSetIconNum = sizeof(shortcutSetIconTable3)/sizeof(shortcutSetIconTable3[0]);
				shortcutSetIconTable = shortcutSetIconTable3;
			}
			else
			{
				shortcutSetIconNum = sizeof(shortcutSetIconTable0)/sizeof(shortcutSetIconTable0[0]);
				shortcutSetIconTable = shortcutSetIconTable0;
			}
		}
	}
	for(i = 0; i < shortcutIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*shortcutIconMax+i < shortcutSetIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, shortcutSetIconTable[i+page*shortcutIconMax].iConText, 1, hv.h - x);
		}
	}
	pageNum = shortcutSetIconNum/shortcutIconMax + (shortcutSetIconNum%shortcutIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);

	ShortcutSetIconSelect();
	//API_EnableOsdUpdate();
}

void ShortcutSetIconSelect(void)
{
	int shortcutSelect;
	uint8 listSelect = shortcutIconMax;
	uint8 i;
	uint16 x, y;
	char temp[20];

	//API_Event_IoServer_InnerRead_All(get_short_cut_io_id(), temp);
	//shortcutSelect = atoi(temp);
	shortcutSelect =read_short_cut_io_val(get_short_cut_io_id(), NULL, NULL, NULL);

	switch(shortcutSelect)
	{
		case SHORTCUT_TYPE_MONITOR:
		case SHORTCUT_TYPE_MONITOR_IPC:
		case SHORTCUT_TYPE_DXMONITOR:	
			listSelect = 1;
			break;
			
		case SHORTCUT_TYPE_NAMELIST:
		case SHORTCUT_TYPE_DXNAMELIST:	
			listSelect = 2;
			break;
			
		case SHORTCUT_TYPE_INNERCALL:
		case SHORTCUT_TYPE_DXINNERCALL:
			listSelect = 3;
			break;
			
		case SHORTCUT_TYPE_GUARD_STATION:
		case SHORTCUT_TYPE_DXGUARD:
			listSelect = 4;
			break;

		case SHORTCUT_TYPE_MONITOR_QUAD:
			listSelect = 2;
			break;
			
		case SHORTCUT_TYPE_DXUnlock1:
			listSelect = 5;
			break;
		case SHORTCUT_TYPE_DXUnlock2:
			listSelect = 6;
			break;
	}

	for(i = 0; i < shortcutIconMax; i++)
	{
		if(shortcutSetPageSelect*shortcutIconMax+i == listSelect)
		{
			ListSelect(i, 1);
		}
		else
		{
			ListSelect(i, 0);
		}
	}

}

void ShortcutSetListSelect(int listSelect)
{
	uint8 i;
	for(i = 0; i < shortcutIconMax; i++)
	{
		if(i == listSelect)
		{
			ListSelect(i, 1);
		}
		else
		{
			ListSelect(i, 0);
		}
	}
}

void MENU_030_ShortcutSet_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	shortcutIconMax = GetListIconNum();
	shortcutSetState = SHORTCUT_SET_STATE_ICON;
	shortcutSetPageSelect = 0;
	shortcutSetListPageSelect = 0;
	shortcutSetSelect = ICON_229_ShortcutSet1;
	DisplayShortcutSetPageIcon(shortcutSetPageSelect);
	API_MenuIconDisplaySelectOn(shortcutSetSelect);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, shortcutSetSelect, 1, 0);
	//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_ShortcutSet, 1, DISPLAY_TITLE_WIDTH);
	ShortcutSetIconSelect();
	ipcNum = GetIpcNum()+GetWlanIpcNum();
}

void MENU_030_ShortcutSet_Exit(void)
{
	
}

void MENU_030_ShortcutSet_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	//uint16 shortcutSelect;
	//char temp[20];
	int shortcutSelectIndex;
	POS pos;
	SIZE hv;
	int shortcut_type;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(shortcutSetState != SHORTCUT_SET_STATE_ICON)
					{
						shortcutSetState = SHORTCUT_SET_STATE_ICON;
						shortcutSetPageSelect = 0;
						shortcutSetListPageSelect = 0;
						DisplayShortcutSetPageIcon(shortcutSetPageSelect);
						ShortcutSetIconSelect();
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					if(shortcutSetState == SHORTCUT_SET_STATE_NAMELIST)
					{
						PublicPageDownProcess(&shortcutSetListPageSelect, shortcutIconMax, Get_DxNamelist_Num(), (DispListPage)DisplayShortcutPageNamelist);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_MONITOR)
					{
						PublicPageDownProcess(&shortcutSetListPageSelect, shortcutIconMax, Get_DxMonRes_Num()+ ipcNum, (DispListPage)DisplayShortcutPageMonlist);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_INNER_CALL)
					{
						PublicPageDownProcess(&shortcutSetListPageSelect, shortcutIconMax, GetMSListRecordCnt()==0? (GetMSListTempRecordCnt()):(GetMSListRecordCnt()), (DispListPage)DisplayOnePageMSlist);
					}
					else	 if(shortcutSetState == SHORTCUT_SET_STATE_ICON)
					{
						PublicPageDownProcess(&shortcutSetPageSelect,shortcutIconMax,shortcutSetIconNum,DisplayShortcutSetPageIcon);
					}
					break;
					
				case ICON_202_PageUp:
					if(shortcutSetState == SHORTCUT_SET_STATE_NAMELIST)
					{
						PublicPageUpProcess(&shortcutSetListPageSelect, shortcutIconMax, Get_DxNamelist_Num(), (DispListPage)DisplayShortcutPageNamelist);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_MONITOR)
					{
						PublicPageUpProcess(&shortcutSetListPageSelect, shortcutIconMax, Get_DxMonRes_Num()+ ipcNum, (DispListPage)DisplayShortcutPageMonlist);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_INNER_CALL)
					{
						PublicPageUpProcess(&shortcutSetListPageSelect, shortcutIconMax, GetMSListRecordCnt()==0? (GetMSListTempRecordCnt()):(GetMSListRecordCnt()), (DispListPage)DisplayOnePageMSlist);
					}
					else	 if(shortcutSetState == SHORTCUT_SET_STATE_ICON)
					{
						PublicPageUpProcess(&shortcutSetPageSelect,shortcutIconMax,shortcutSetIconNum,DisplayShortcutSetPageIcon);
					}
					break;
					
				case ICON_229_ShortcutSet1:
				case ICON_230_ShortcutSet2:
				case ICON_231_ShortcutSet3:
				case ICON_232_ShortcutSet4:
				case ICON_379_Shortcut5:
					if(shortcutSetSelect != GetCurIcon())
					{
						shortcutSetState = SHORTCUT_SET_STATE_ICON;
						shortcutSetPageSelect = 0;
						shortcutSetListPageSelect = 0;
						API_MenuIconDisplaySelectOff(shortcutSetSelect);
						shortcutSetSelect = GetCurIcon();
						API_MenuIconDisplaySelectOn(shortcutSetSelect);
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, shortcutSetSelect, 1, 0);
						DisplayShortcutSetPageIcon(shortcutSetPageSelect);
						ShortcutSetIconSelect();
					}
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					shortcutSetIconSelect = GetCurIcon() - ICON_007_PublicList1;
					shortcutSelectIndex = shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
					
					if(shortcutSetState == SHORTCUT_SET_STATE_ICON)
					{

						shortcutSelectIndex = shortcutSetPageSelect*shortcutIconMax+shortcutSetIconSelect;
						if(shortcutSelectIndex >= shortcutSetIconNum)
						{
							return;
						}
						
						switch(shortcutSetIconTable[shortcutSelectIndex].iCon)
						{
							case ICON_301_RemoveShortcutSetting:
								write_short_cut_io_val(get_short_cut_io_id(), 0, NULL, NULL);
								ShortcutSetIconSelect();
								break;
								
							case ICON_012_namelist:
								shortcutSetState = SHORTCUT_SET_STATE_NAMELIST;
								shortcutSetListPageSelect = 0;
								if(GetImNameListRecordCnt() == 0)
									//API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
								
								DisplayShortcutPageNamelist(shortcutSetListPageSelect);
								break;
							case ICON_302_MontorList:
								shortcutSetState = SHORTCUT_SET_STATE_MONITOR;
								shortcutSetListPageSelect = 0;
								if(!Get_MonRes_Num() || !Get_MonRes_Temp_Table_Num())
								{
									//API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
								}
								DisplayShortcutPageMonlist(shortcutSetListPageSelect);
								break;
								
							case ICON_013_InnerCall:
								write_short_cut_io_val(get_short_cut_io_id(), 10, NULL, NULL);
								ShortcutSetIconSelect();
								#if 0
								shortcutSetState = SHORTCUT_SET_STATE_INNER_CALL;
								shortcutSetListPageSelect = 0;
								if(!GetMSListRecordCnt())
								{
									API_ListUpdate(MSG_TYPE_UPDATE_MSLIST_TABLE);
								}
								DisplayOnePageMSlist(shortcutSetListPageSelect);
								#endif
								break;
								
							case ICON_014_GuardStation:
								write_short_cut_io_val(get_short_cut_io_id(), 11, NULL, NULL);
								ShortcutSetIconSelect();
								#if 0
								shortcutSetState = SHORTCUT_SET_STATE_GUARD_STATION;
								shortcutSetListPageSelect = 0;
								if(GetImNamelistGLNum() == 0)
								{
									API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
								}																
								DisplayOnePageGSlist(shortcutSetListPageSelect);
								#endif
								break;
								
							case ICON_204_quad:
								write_short_cut_io_val(get_short_cut_io_id(), 6, NULL, NULL);
								ShortcutSetIconSelect();
								break;
							case ICON_Unlock1List:
								shortcutSetState = SHORTCUT_SET_STATE_UNLOCK1;
								shortcutSetListPageSelect = 0;
								if(!Get_MonRes_Num() || !Get_MonRes_Temp_Table_Num())
								{
									//API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
								}
								DisplayShortcutPageUnlocklist(shortcutSetListPageSelect);
								break;

							case ICON_Unlock2List:
								shortcutSetState = SHORTCUT_SET_STATE_UNLOCK2;
								shortcutSetListPageSelect = 0;
								if(!Get_MonRes_Num() || !Get_MonRes_Temp_Table_Num())
								{
									//API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
								}
								DisplayShortcutPageUnlocklist(shortcutSetListPageSelect);
								break;
								
						}
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_NAMELIST)
					{	
						IM_NameListRecord_T record;	
						if(GetImNameListRecordCnt() == 0)
						{
							if(shortcutSelectIndex >= (GetImNamelistTempTableNum()+Get_DxNamelist_Num()))
							{
								break;
							}
							else if(shortcutSelectIndex<GetImNamelistTempTableNum())
							{
								GetImNameListTempRecordItems(shortcutSelectIndex,	&record);
								shortcut_type=SHORTCUT_TYPE_NAMELIST;
							}
							else
							{
								Get_DxNamelist_Record(shortcutSelectIndex-GetImNamelistTempTableNum(), record.R_Name, record.BD_RM_MS);
								shortcut_type=strtoul(record.BD_RM_MS,NULL,0);
								sprintf(record.BD_RM_MS,"%d",shortcut_type);
								shortcut_type=SHORTCUT_TYPE_DXNAMELIST;
							}
						}
						else
						{
							if(shortcutSelectIndex >= (GetImNameListRecordCnt()+Get_DxNamelist_Num()))
							{
								break;
							}
							else if(shortcutSelectIndex<GetImNameListRecordCnt())
							{
								GetImNameListRecordItems(shortcutSelectIndex,	&record);
								shortcut_type=SHORTCUT_TYPE_NAMELIST;
							}
							else
							{
								Get_DxNamelist_Record(shortcutSelectIndex-GetImNameListRecordCnt(), record.R_Name, record.BD_RM_MS);
								shortcut_type=strtoul(record.BD_RM_MS,NULL,0);
								sprintf(record.BD_RM_MS,"%d",shortcut_type);
								shortcut_type=SHORTCUT_TYPE_DXNAMELIST;
							}
						}						
						//shortcutSelect = (SHORTCUT_TYPE_NAMELIST << 8) + shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
						//sprintf(temp, "%d", shortcutSelect);
						//API_Event_IoServer_InnerWrite_All(get_short_cut_io_id(), temp);
						write_short_cut_io_val(get_short_cut_io_id(), shortcut_type, record.BD_RM_MS, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
						ShortcutSetListSelect(shortcutSetIconSelect);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_MONITOR)
					{
						char name[41];
						char name_temp2[41];
						char bdRmMs[11];
						IPC_ONE_DEVICE ipcRecord;
						if(Get_MonRes_Num() == 0)
						{
							if(shortcutSelectIndex >= (Get_MonRes_Temp_Table_Num()+ ipcNum+Get_DxMonRes_Num()))
							{
								break;
							}
							else
							{
								if(shortcutSelectIndex < ipcNum)
								{
									if(shortcutSelectIndex < GetIpcNum())
										GetIpcCacheRecord(shortcutSelectIndex, &ipcRecord);
									else
										GetWlanIpcRecord(shortcutSelectIndex-GetIpcNum(), &ipcRecord);
									strcpy(name, ipcRecord.NAME);	
									if(GetMonFavByAddr(NULL, ipcRecord.NAME, name_temp2) == 1)
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : ipcRecord.NAME);
									}	
									write_short_cut_io_val(get_short_cut_io_id(), 5, ipcRecord.IP, name);
								}
								else if(shortcutSelectIndex<(Get_MonRes_Temp_Table_Num()+ ipcNum))
								{
									Get_MonRes_Temp_Table_Record(shortcutSelectIndex - ipcNum,name,bdRmMs, NULL);
									if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
									}
									write_short_cut_io_val(get_short_cut_io_id(), 1, bdRmMs, name);
								}
								else
								{
									Get_DxMonRes_Record(shortcutSelectIndex-(Get_MonRes_Temp_Table_Num()+ ipcNum), name, bdRmMs);
									if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
									}
									shortcut_type=strtoul(bdRmMs,NULL,0);
									sprintf(bdRmMs,"%d",shortcut_type);
									write_short_cut_io_val(get_short_cut_io_id(), SHORTCUT_TYPE_DXMONITOR, bdRmMs, name);
								}

							}
						}
						else
						{
							if(shortcutSelectIndex >= (Get_MonRes_Num()+ ipcNum+Get_DxMonRes_Num())) 
							{
								break;
							}
							else
							{
								if(shortcutSelectIndex < ipcNum)
								{
									if(shortcutSelectIndex < GetIpcNum())
										GetIpcCacheRecord(shortcutSelectIndex, &ipcRecord);
									else
										GetWlanIpcRecord(shortcutSelectIndex-GetIpcNum(), &ipcRecord);
									strcpy(name, ipcRecord.NAME);	
										
									if(GetMonFavByAddr(NULL, ipcRecord.NAME, name_temp2) == 1)
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : ipcRecord.NAME);
									}		
									write_short_cut_io_val(get_short_cut_io_id(), 5, NULL, name);
								}
								else if(shortcutSelectIndex<(Get_MonRes_Num()+ ipcNum))
								{
									Get_MonRes_Record(shortcutSelectIndex - ipcNum,name,bdRmMs, NULL);
									if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
									}
									write_short_cut_io_val(get_short_cut_io_id(), 1, bdRmMs, name);
								}
								else
								{
									Get_DxMonRes_Record(shortcutSelectIndex-(Get_MonRes_Num()+ ipcNum), name, bdRmMs);
									if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
									{
										strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
									}
									shortcut_type=strtoul(bdRmMs,NULL,0);
									sprintf(bdRmMs,"%d",shortcut_type);
									write_short_cut_io_val(get_short_cut_io_id(), SHORTCUT_TYPE_DXMONITOR, bdRmMs, name);
								}

							}
						}
						//shortcutSelect = (SHORTCUT_TYPE_MONITOR << 8) + shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
						//sprintf(temp, "%d", shortcutSelect);
						//API_Event_IoServer_InnerWrite_All(get_short_cut_io_id(), temp);
						ShortcutSetListSelect(shortcutSetIconSelect);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_INNER_CALL)
					{
						MS_ListRecord_T record;
						if(GetMSListRecordCnt() == 0)
						{
							if(shortcutSelectIndex >= GetMSListTempRecordCnt())
							{
								break;
							}
							else
							{
								GetMsListTempRecordItems(shortcutSelectIndex, &record);
							}
						}
						else
						{
							if(shortcutSelectIndex >= GetMSListRecordCnt())
							{
								break;
							}
							else
							{
								GetMsListRecordItems(shortcutSelectIndex, &record);
							}
						}
						
						//shortcutSelect = (SHORTCUT_TYPE_INNERCALL << 8) + shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
						//sprintf(temp, "%d", shortcutSelect);
						//API_Event_IoServer_InnerWrite_All(get_short_cut_io_id(), temp);
						write_short_cut_io_val(get_short_cut_io_id(), 3, record.BD_RM_MS, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
						ShortcutSetListSelect(shortcutSetIconSelect);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_GUARD_STATION)
					{
						IM_NameListRecord_T record;
						if(shortcutSelectIndex >= GetImNamelistGLNum())
						{
							break;
						}
						if(GetImNameListRecordCnt() == 0)
						{
							GetImNameListTempRecordItems(shortcutSelectIndex,	&record);
						}
						else
						{
							GetImNameListRecordItems(shortcutSelectIndex,	&record);
						}
						write_short_cut_io_val(get_short_cut_io_id(), 4, record.BD_RM_MS, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
						ShortcutSetListSelect(shortcutSetIconSelect);

					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_UNLOCK1||shortcutSetState == SHORTCUT_SET_STATE_UNLOCK2)
					{
						char name[41];
						char name_temp2[41];
						char bdRmMs[11];
						
						
						
						if(shortcutSelectIndex >= Get_DxMonRes_Num()) 
						{
							break;
						}
						else
						{
							
							{
								Get_DxMonRes_Record(shortcutSelectIndex, name, bdRmMs);
								if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
								{
									strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
								}
								shortcut_type=strtoul(bdRmMs,NULL,0);
								sprintf(bdRmMs,"%d",shortcut_type);
								write_short_cut_io_val(get_short_cut_io_id(), shortcutSetState == SHORTCUT_SET_STATE_UNLOCK1?SHORTCUT_TYPE_DXUnlock1:SHORTCUT_TYPE_DXUnlock2, bdRmMs, name);
							}

						}
						
						//shortcutSelect = (SHORTCUT_TYPE_MONITOR << 8) + shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
						//sprintf(temp, "%d", shortcutSelect);
						//API_Event_IoServer_InnerWrite_All(get_short_cut_io_id(), temp);
						ShortcutSetListSelect(shortcutSetIconSelect);
					}
					else if(shortcutSetState == SHORTCUT_SET_STATE_INNER_CALL)
					{
						MS_ListRecord_T record;
						if(GetMSListRecordCnt() == 0)
						{
							if(shortcutSelectIndex >= GetMSListTempRecordCnt())
							{
								break;
							}
							else
							{
								GetMsListTempRecordItems(shortcutSelectIndex, &record);
							}
						}
						else
						{
							if(shortcutSelectIndex >= GetMSListRecordCnt())
							{
								break;
							}
							else
							{
								GetMsListRecordItems(shortcutSelectIndex, &record);
							}
						}
						
						//shortcutSelect = (SHORTCUT_TYPE_INNERCALL << 8) + shortcutSetListPageSelect*shortcutIconMax+shortcutSetIconSelect;
						//sprintf(temp, "%d", shortcutSelect);
						//API_Event_IoServer_InnerWrite_All(get_short_cut_io_id(), temp);
						write_short_cut_io_val(get_short_cut_io_id(), 3, record.BD_RM_MS, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
						ShortcutSetListSelect(shortcutSetIconSelect);
					}
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_MonitorListUpdate:
				API_DisableOsdUpdate();
				SetIpcListUpdateFlag(1);
				DisplayShortcutPageMonlist(shortcutSetListPageSelect);
				API_EnableOsdUpdate();
				break;
			case MSG_7_BRD_SUB_NameListUpdate:	
				API_DisableOsdUpdate();
				if(shortcutSetState == SHORTCUT_SET_STATE_NAMELIST)
				{
					DisplayShortcutPageNamelist(shortcutSetListPageSelect);
				}
				else if	(shortcutSetState == SHORTCUT_SET_STATE_GUARD_STATION)
				{
					DisplayOnePageGSlist(shortcutSetListPageSelect);
				}
				API_EnableOsdUpdate();
				break;
			case MSG_7_BRD_SUB_MSListUpdate:
				API_DisableOsdUpdate();
				DisplayOnePageMSlist(shortcutSetListPageSelect);
				API_EnableOsdUpdate();
				break;	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


