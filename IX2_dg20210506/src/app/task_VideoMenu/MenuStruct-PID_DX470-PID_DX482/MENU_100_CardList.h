
#ifndef _MENU_100_CARD_LIST_H
#define _MENU_100_CARD_LIST_H

#include "MENU_public.h"
typedef enum
{
	ID_CARD_KEY_RM = 0,
	ID_CARD_KEY_ID,
	ID_CARD_KEY_NAME,
	ID_CARD_KEY_DATE,
}Id_card_key_t;

#pragma pack(1)
typedef struct
{
	unsigned char type;
	unsigned char keyType;
	unsigned short num;
	char keyName[4];
	char key[9]; // bd_rm
}IdCardGetReq_s;

typedef struct
{
	unsigned char type;
	unsigned char keyType;
	unsigned short num;
	char keyName[4];
	char key[8]; // bd_rm
	char cnName[4];
	char cn[4];
}IdCardGetCountRsp_s;


typedef struct
{
	char keyRM[4];
	char bd_rm_ms[9]; // bd_rm
	char keyID[4];
	char id[11];
	char keyName[4];
	char name[21];
	char keyDate[4];
	char date[9];
}IdCardInfo_s;
//typedef st
#endif