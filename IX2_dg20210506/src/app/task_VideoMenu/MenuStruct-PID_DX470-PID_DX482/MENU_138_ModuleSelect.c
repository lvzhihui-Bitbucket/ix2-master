#include "MENU_public.h"
#include "obj_UnifiedParameterMenu.h"

#define	ExtModuel_ICON_MAX		5
int extModuleIconSelect;
int extModulePageSelect;

extern ParaMenu_T OneRemotePararecord;
EXT_MODULE_JSON extModuleObj;

const IconAndText_t extModuleIconTable[] = 
{
	{ICON_ExtModuleS4,				MESG_TEXT_ICON_ExtModuleS4},    
	{ICON_ExtModuleMK,				MESG_TEXT_ICON_ExtModuleMK},
	{ICON_ExtModuleDR,				MESG_TEXT_ICON_ExtModuleDR},
};
const int ExtModuleIconNum = sizeof(extModuleIconTable)/sizeof(extModuleIconTable[0]);


static void DisplayExtModulePageIcon(uint8 page)
{
	uint8 i, j;
	uint16 x, y;
	int pageNum;
	char temp[10];
	char display[100];
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < ExtModuel_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*ExtModuel_ICON_MAX+i < ExtModuleIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, extModuleIconTable[i+page*ExtModuel_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 7 )
			{
				x += (hv.h - pos.x)/3;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(extModuleIconTable[i+page*ExtModuel_ICON_MAX].iCon)
			{
				case ICON_ExtModuleS4:
					display[0] = 0;
					strcat(display, "[ ");
					for(j = 0; j < extModuleObj.s4Nbr; j++)
					{
						if(j == extModuleObj.s4Nbr-1)
						{
							snprintf(temp,10, "%d", extModuleObj.s4[j]);
						}
						else
						{
							snprintf(temp,10, "%d, ", extModuleObj.s4[j]);
						}
						strcat(display, temp);
					}
					strcat(display, " ]");
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_ExtModuleMK:
					display[0] = 0;
					strcat(display, "[ ");
					for(j = 0; j < extModuleObj.mkNbr; j++)
					{
						if(j == extModuleObj.mkNbr-1)
						{
							snprintf(temp,10, "%d", extModuleObj.mk[j]);
						}
						else
						{
							snprintf(temp,10, "%d, ", extModuleObj.mk[j]);
						}
						strcat(display, temp);
					}
					strcat(display, " ]");
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_ExtModuleDR:
					display[0] = 0;
					strcat(display, "[ ");
					for(j = 0; j < extModuleObj.drNbr; j++)
					{
						if(j == extModuleObj.drNbr-1)
						{
							snprintf(temp,10, "%d", extModuleObj.dr[j]);
						}
						else
						{
							snprintf(temp,10, "%d, ", extModuleObj.dr[j]);
						}
						strcat(display, temp);
					}
					strcat(display, " ]");
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = ExtModuleIconNum/ExtModuel_ICON_MAX + (ExtModuleIconNum%ExtModuel_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

//cao_20181020_s

void GetExtModuleConfigValue(void)
{
	BusySpriteDisplay(1);
	Api_ResSyncFromServer(8005,GetOnlineManageNbr());
	ParseExtModuleObject(OneRemotePararecord.value, &extModuleObj);
	BusySpriteDisplay(0);
}


void MENU_138_ExtModuleSelect_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	extModulePageSelect = 0;
	extModuleIconSelect = 0;

	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_LIST_COLOR, OneRemotePararecord.name, OneRemotePararecord.nameLen, 1, STR_UNICODE, 0);			
	
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	DisplayExtModulePageIcon(extModulePageSelect);


	
}

void MENU_138_ExtModuleSelect_Exit(void)
{
	
}


void MENU_138_ExtModuleSelect_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	int i;
	char temp[20];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&extModulePageSelect, ExtModuel_ICON_MAX, ExtModuleIconNum, (DispListPage)DisplayExtModulePageIcon);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&extModulePageSelect, ExtModuel_ICON_MAX, ExtModuleIconNum, (DispListPage)DisplayExtModulePageIcon);
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					extModuleIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(extModulePageSelect*ExtModuel_ICON_MAX+extModuleIconSelect >= ExtModuleIconNum)
					{
						return;
					}
					
					switch(extModuleIconTable[extModulePageSelect*ExtModuel_ICON_MAX+extModuleIconSelect].iCon)
					{
						case ICON_ExtModuleS4:
							if(extModuleObj.s4Nbr)
							{
								StartInitOneMenu(MENU_139_KeySelect,0,1);
							}
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


