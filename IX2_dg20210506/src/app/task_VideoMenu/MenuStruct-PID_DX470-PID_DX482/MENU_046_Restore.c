#include "MENU_public.h"
#include "MENU_046_Restore.h"

#define	RESTORE_ICON_MAX	5
int restoreIconSelect;
int restorePageSelect;
int restoreConfirm;
int restoreConfirmSelect;




const RestoreIcon restoreIconTable[] = 
{
	//{ICON_083_RestoreGeneralData,					MESG_TEXT_ICON_083_RestoreGeneralData},    
	//{ICON_084_RestoreUserData,					MESG_TEXT_ICON_084_RestoreUserData},
	//{ICON_085_RestoreWirelessData,				MESG_TEXT_ICON_085_RestoreWirelessData},
	//{ICON_086_RestoreInstallerData,				MESG_TEXT_ICON_086_RestoreInstallerData},
	{ICON_087_RestoreAndBackup,					MESG_TEXT_ICON_087_RestoreAndBackup},
};

const int restoreIconNum = sizeof(restoreIconTable)/sizeof(restoreIconTable[0]);

static void DisplayRestorePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[11];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < RESTORE_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*RESTORE_ICON_MAX+i < restoreIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, restoreIconTable[i+page*RESTORE_ICON_MAX].iConText, 1, (hv.h - pos.x)/2);
		}
	}
	pageNum = restoreIconNum/RESTORE_ICON_MAX + (restoreIconNum%RESTORE_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void UpdateOkAndWaitingForReboot(void)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	BusySpriteDisplay(0);
	API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h), SPRITE_CONFIRM);
	API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, MESG_TEXT_WaitingForReboot,1, 0);
	BEEP_CONFIRM();
	usleep(500000);
	API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h), SPRITE_CONFIRM);
	SoftRestar();	
	usleep(5000000);
	BEEP_ERROR();
	CloseOneMenu();
}
void MENU_046_Restore_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	restoreIconSelect = 0;
	restorePageSelect = 0;
	
	API_MenuIconDisplaySelectOn(ICON_025_general);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_Restore,1, 0);
	DisplayRestorePageIcon(restorePageSelect);
	restoreConfirm = 0;
}

void MENU_046_Restore_Exit(void)
{

}

void MENU_046_Restore_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[2000];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					restoreConfirm = 0;
					API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h), SPRITE_IF_CONFIRM);
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					restoreIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(restorePageSelect*RESTORE_ICON_MAX+restoreIconSelect >= restoreIconNum)
					{
						return;
					}
					if(ConfirmSelect(&restoreConfirm, &restoreConfirmSelect, &restoreIconSelect, restorePageSelect, RESTORE_ICON_MAX))
					{
						return;
					}
					#if 0
					if(!restoreConfirm)
					{
						restoreConfirm = 1;
						restoreConfirmSelect = restorePageSelect*RESTORE_ICON_MAX+restoreIconSelect;
						API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
						return;
					}
					else if(restoreConfirmSelect == restorePageSelect*RESTORE_ICON_MAX+restoreIconSelect)
					{
						restoreConfirm = 0;
						API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
					}
					else
					{
						API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreConfirmSelect%RESTORE_ICON_MAX), SPRITE_IF_CONFIRM);
						restoreConfirm = 1;
						restoreConfirmSelect = restorePageSelect*RESTORE_ICON_MAX+restoreIconSelect;
						API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
						return;
					}
					#endif
					switch(restoreIconTable[restorePageSelect*RESTORE_ICON_MAX+restoreIconSelect].iCon)
					{
						case ICON_083_RestoreGeneralData:
							ClearConfirm(&restoreConfirm, &restoreConfirmSelect, RESTORE_ICON_MAX);
							//restoreConfirm = 0;
							//API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
							break;
						case ICON_084_RestoreUserData:
							ClearConfirm(&restoreConfirm, &restoreConfirmSelect, RESTORE_ICON_MAX);
							//restoreConfirm = 0;
							//API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
							break;
						case ICON_085_RestoreWirelessData:
							ClearConfirm(&restoreConfirm, &restoreConfirmSelect, RESTORE_ICON_MAX);
							//restoreConfirm = 0;
							//API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
							break;
						case ICON_086_RestoreInstallerData:
							ClearConfirm(&restoreConfirm, &restoreConfirmSelect, RESTORE_ICON_MAX);
							//restoreConfirm = 0;
							//API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(restoreIconSelect), SPRITE_IF_CONFIRM);
							break;
						
						case ICON_087_RestoreAndBackup:
							BusySpriteDisplay(1);
							if(1)
							{
								const char* rmFile[] = 
								{
									MS_LIST_TABLE_NAME,
									IM_NAME_LIST_TABLE_NAME,
									DEVICE_REGISTER_TABLE_NAME,
									CALL_CONTROL_MAP_TABLE_NAME,
									DEVICE_MAP_TABLE_NAME,
									MON_LIST_FILE_NAME,
									NAME_LIST_FILE_NAME,
									NAME_LIST_BAK_FILE_NAME,
									IPC_MON_LIST_FILE_NAME,
									CALL_RECORD_FILE_NAME,
									
									NULL,
								};

								const char* rmFileDir[] = 
									{
										JPEG_STORE_DIR,
										VIDEO_STORE_DIR,
										NULL,
									};
								tempChar[0] = 0;
								int i = 0;
								
								strcat(tempChar, "rm ");
								
								for(i = 0; rmFileDir[i]; i++)
								{
									strcat(tempChar, "-R ");
									strcat(tempChar, rmFileDir[i]);
									strcat(tempChar, " ");
								}
								
								for(i = 0; rmFile[i]; i++)
								{
									strcat(tempChar, rmFile[i]);
									strcat(tempChar, " ");
								}

								system(tempChar);
								API_Event_IoServer_Factory_Default();
								
								SipConfigRestoreDefault();

								UpdateOkAndWaitingForReboot();
							}
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


