#include "MENU_070_BackFwUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_RemoteUpgrade.h"
#include "MenuBackFwUpgrade_Business.h"
#include "ota_FileProcess.h"

#define	BackFwUpgrade_ICON_MAX	5
int BackFwUpgradeIconSelect;
int BackFwUpgradePageSelect;

//0,uncheck,1,checking,2 checked 3 tranfering 4tranfered/updating,5canceling

extern char text_file_dir[60];
extern int Fw_file_size;
extern int Fw_file_trans_size;
extern char fw_file_dir[60];
extern char trans_file_name[40];
extern OtaDlTxt_t dlTxtInfo;

const IconAndText_t BackFwUpgradeIconTable[] = 
{
	{ICON_FwUpgrade_Server,			MESG_TEXT_ICON_FwUpgrage_Server},    
	{ICON_FwUpgrade_DLCode,			MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		NULL},
	{ICON_FwUpgrade_Notice,			NULL},
	{ICON_FwUpgrade_Operation,		NULL},
};

const int BackFwUpgradeIconNum = sizeof(BackFwUpgradeIconTable)/sizeof(BackFwUpgradeIconTable[0]);

static void DisplayBackFwUpgradePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];

	for(i = 0; i < BackFwUpgrade_ICON_MAX; i++)
	{
		if(page*BackFwUpgrade_ICON_MAX+i < BackFwUpgradeIconNum)
		{
			switch(BackFwUpgradeIconTable[i+page*BackFwUpgrade_ICON_MAX].iCon)
			{
					
				case ICON_FwUpgrade_Operation:
					if(BackFwUpgrade_State == FwUpgrade_State_Uncheck)
					{
						BackFwUpgradeLine4StrId = MESG_TEXT_FwUpgrage_Check;
						strcpy(BackFwUpgradeLine4ValueStr, "");
					}
					break;

			}
		}
	}
	//pageNum = BackFwUpgradeIconNum/BackFwUpgrade_ICON_MAX + (BackFwUpgradeIconNum%BackFwUpgrade_ICON_MAX ? 1 : 0);

	//DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void BackFwUpgrade_Operation(void)
{
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	if(BackFwUpgrade_State == FwUpgrade_State_Uncheck)
	{
		if(BackFwUpgrade_Ser_Select == 4)
		{	
			sprintf(display,"%s/%s.txt",SdUpgradePath,BackFwUpgrade_Code);
			if(Judge_SdCardLink() == 0) //r
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
			}
			else if(CheckDownloadIsAllowed(display, &dlTxtInfo))
			{
				BackFwUpgrade_State = FwUpgrade_State_CheckOk;
				BackFwUpgrade_CodeInfo_Disp(dlTxtInfo.info);
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
				BackFwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
			}
			else
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrMismatchedType);
			}
		}
		else
		{
			if((error_code=get_upgrade_file_dir(1,0,text_file_dir)) == 0)
			{
				sysinfo = GetSysVerInfo();
				char tempIp[18];
				if(BackFwUpgrade_Ser_Select)
				{
					sscanf(BackFwUpgrade_Ser_Disp[BackFwUpgrade_Ser_Select], "%*[^\[][%[^\]]", tempIp);
				}
				else
				{
					strcpy(tempIp, BackFwUpgrade_Ser_Disp[BackFwUpgrade_Ser_Select]);
				}
				api_ota_ifc_file_download(tempIp,text_file_dir,BackFwUpgrade_Code,1,sysinfo.type,sysinfo.swVer,sysinfo.sn);
				BackFwUpgrade_State = FwUpgrade_State_Checking;
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
			}
			else if(error_code == -1)
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
			}
			else
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
			}
		}
	}
	else if(BackFwUpgrade_State == FwUpgrade_State_CheckOk)
	{
		if(BackFwUpgrade_Ser_Select == 4)
		{
			BackFwUpgrade_State = FwUpgrade_State_Intalling;
			BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
			BackFwUpgrade_Update_Operation(0);
			sprintf(display,"%s/%s",SdUpgradePath,BackFwUpgrade_Code);
			if(start_updatefile_and_reboot_forsdcard(0,display) >= 0)
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
				remove_upgrade_tempfile();
				BackFwUpgrade_RebootTime = 100;
				//HardwareRestar();
			}
			else
			{
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
			}
			BackFwUpgrade_State = FwUpgrade_State_Intalled; 
		}
		else
		{
			if((error_code = get_upgrade_file_dir(0,Fw_file_size,fw_file_dir)) == 0)
			{
				sysinfo = GetSysVerInfo();
				char tempIp[18];
				if(BackFwUpgrade_Ser_Select)
				{
					sscanf(BackFwUpgrade_Ser_Disp[BackFwUpgrade_Ser_Select], "%*[^\[][%[^\]]", tempIp);
				}
				else
				{
					strcpy(tempIp, BackFwUpgrade_Ser_Disp[BackFwUpgrade_Ser_Select]);
				}
				api_ota_ifc_ftp_start(tempIp,fw_file_dir,BackFwUpgrade_Code,0,sysinfo.type,sysinfo.swVer,sysinfo.sn);
				BackFwUpgrade_State = FwUpgrade_State_Downloading;
				BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
				BackFwUpgrade_Update_Operation(0);
			}
			else
			{
				if(error_code == -1)
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
				}
				else
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
				}
			}
		}
	}
}

void BackFwUpgrade_Cancle(void)
{
	if(BackFwUpgrade_State == FwUpgrade_State_Uncheck ||BackFwUpgrade_State == FwUpgrade_State_Intalled||BackFwUpgrade_State == FwUpgrade_State_CheckOk)
	{
		remove_upgrade_tempfile();
		popDisplayLastMenu();
		BackFwUpgrade_State = FwUpgrade_State_Uncheck;
	}
	if(BackFwUpgrade_State == FwUpgrade_State_Checking||BackFwUpgrade_State == FwUpgrade_State_Downloading)
	{
		BackFwUpgrade_State = FwUpgrade_State_Canceling;
		BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Canceling);
		api_ota_ifc_ftp_cancel();
	}
}

void MENU_070_BackFwUpgrade_Init(int uMenuCnt)
{
	BackFwUpgrade_State = FwUpgrade_State_Uncheck;
	BackFwUpgradePageSelect = 0;
	BackFwUpgrade_IoPara_Init();
	DisplayBackFwUpgradePageIcon(BackFwUpgradePageSelect);
}

void MENU_070_BackFwUpgrade_Exit(void)
{
	BackFwUpgrade_State = FwUpgrade_State_Uncheck;
	API_RemoteUpgradeOperation(BackFwUpgrade_ReomteIp, UpgradeCMD_OP_CODE_CANCLE, NULL, 1);
}

void MENU_070_BackFwUpgrade_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					BackFwUpgrade_Cancle();
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					BackFwUpgradeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(BackFwUpgradePageSelect*BackFwUpgrade_ICON_MAX+BackFwUpgradeIconSelect >= BackFwUpgradeIconNum)
					{
						return;
					}
					
					switch(BackFwUpgradeIconTable[BackFwUpgradePageSelect*BackFwUpgrade_ICON_MAX+BackFwUpgradeIconSelect].iCon)
					{							
						case ICON_FwUpgrade_CodeInfo:
							
							break;
							
						case ICON_FwUpgrade_Notice:
							break;	
							
						case ICON_FwUpgrade_Operation:
							BackFwUpgrade_Operation();
							break;
							
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_OtaClientIfc_ActOk:
				if(BackFwUpgrade_State == FwUpgrade_State_Canceling)
				{
					sleep(1);
					error_code = popDisplayLastMenu();
					printf("error_code = %d\n", error_code);
					sleep(1);
					remove_upgrade_tempfile();
					BackFwUpgrade_State = FwUpgrade_State_Uncheck;
				}
				else if(BackFwUpgrade_State == FwUpgrade_State_Checking || BackFwUpgrade_State == FwUpgrade_State_Downloading)
				{	
					Fw_file_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					strncpy(trans_file_name,(char *)(arg+sizeof(SYS_WIN_MSG)+4),40);
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translated:
				if(BackFwUpgrade_State == FwUpgrade_State_Checking)
				{
					BackFwUpgrade_State = FwUpgrade_State_CheckOk;
					sprintf(display,"%s/%s",text_file_dir,trans_file_name);
					if(CheckDownloadIsAllowed(display, &dlTxtInfo))
					{
						BackFwUpgrade_State = FwUpgrade_State_CheckOk;
						BackFwUpgrade_CodeInfo_Disp(dlTxtInfo.info);
						BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
						BackFwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
					}
					else
					{
						BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrMismatchedType);
						BackFwUpgrade_State = FwUpgrade_State_Uncheck;
					}
				}
				else if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
				{
					BackFwUpgrade_State = FwUpgrade_State_Intalling;
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
					sprintf(display,"%s/%s",fw_file_dir,trans_file_name);
					if(FwUprade_Installing(display) >= 0)
					{
						BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
						remove_upgrade_tempfile();
						SoftRestar();
					}
					else
					{
						BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
					}
					BackFwUpgrade_State = FwUpgrade_State_Intalled;
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translating:
				if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
				{
					Fw_file_trans_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
				}
	
				AutoPowerOffReset();
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Error:
				//rsp -1:busy,-2:connect ser error,-3:no file,-4:download error -5:no sdcard
				error_code = *((int*)(arg+sizeof(SYS_WIN_MSG)));
				if(error_code == -1)
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -2)
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -3)
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
				}
				else if(error_code == -4)
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				else
				{
					BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				if(BackFwUpgrade_State == FwUpgrade_State_Checking)
				{
					BackFwUpgrade_State = FwUpgrade_State_Uncheck;
				}
				else if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
				{
					BackFwUpgrade_State = FwUpgrade_State_CheckOk;
					BackFwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
				}
				break;
			case MSG_7_BRD_SUB_OtaOperation:
				BackFwUpgrade_Operation();
				break;
			case MSG_7_BRD_SUB_OtaReturn:
				BackFwUpgrade_Cancle();
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}

void BackFwUpgrade_Notice_Notice(int notice_type)
{
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	int disp_flag = 0;
	
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_010_PublicList4, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+3*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =x+ (hv.h - pos.x)/2;//x1 =x+ DISPLAY_VALUE_X_DEVIATION-210;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		
	switch(notice_type)
	{
		case MESG_TEXT_FwUpgrage_CodeSize:
			get_file_size_disp(Fw_file_size, BackFwUpgradeLine3ValueStr);
			BackFwUpgradeLine3StrId = notice_type;
			break;

		case MESG_TEXT_FwUpgrage_Downloading:
			BackFwUpgradeLine3StrId = notice_type;
			get_file_size_disp(Fw_file_size,disp);
			get_file_size_disp(Fw_file_trans_size,disp1);
			sprintf(BackFwUpgradeLine3ValueStr, "%s(%s)", disp, disp1);
			break;
			
		default:
			BackFwUpgradeLine3StrId = notice_type;
			strcpy(BackFwUpgradeLine3ValueStr, "");
			break;
	}
	
}

void BackFwUpgrade_CodeInfo_Disp(char* fwInfo)
{
	BackFwUpgradeLine2StrId = MESG_TEXT_FwUpgrage_CodeInfo;
	strcpy(BackFwUpgradeLine2ValueStr, fwInfo);
}

void BackFwUpgrade_Update_Operation(int opt)
{
	BackFwUpgradeLine4StrId = opt;
	strcpy(BackFwUpgradeLine4ValueStr, "");
}

void BackFwUpgrade_IoPara_Init(void)
{
	char ch[21];
	
	API_Event_IoServer_InnerRead_All(FWUPGRADE_SER_SELECT, ch);
	BackFwUpgrade_Ser_Select = atoi(ch);
	API_Event_IoServer_InnerRead_All(FWUPGRADE_OTHER_SER, ch);
	if(strcmp(ch,"-") == 0)
	{
		strcpy(BackFwUpgrade_Ser_Disp[0],"Please input ip");
	}
	else
	{
		strcpy(BackFwUpgrade_Ser_Disp[0],ch);
	}
		
	API_Event_IoServer_InnerRead_All(FWUPGRADE_FW_CODE, ch);
	if(strcmp(ch,"-") == 0)
	{
		BackFwUpgrade_Code[0] = 0;
	}
	else
	{
		strcpy(BackFwUpgrade_Code,ch);
	}

	if(BackFwUpgrade_State == FwUpgrade_State_Uncheck)
	{
		BackFwUpgrade_RebootTime = 0;
		BackFwUpgradeLine2StrId = 0;
		BackFwUpgradeLine3StrId = 0;
		BackFwUpgradeLine4StrId = MESG_TEXT_FwUpgrage_Check;
		strcpy(BackFwUpgradeLine2ValueStr, "");
		strcpy(BackFwUpgradeLine3ValueStr, "");
		strcpy(BackFwUpgradeLine4ValueStr, "");
	}
}



