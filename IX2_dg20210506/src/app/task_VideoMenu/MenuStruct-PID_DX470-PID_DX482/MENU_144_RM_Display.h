
#ifndef _MENU_144_RM_Display_H
#define _MENU_144_RM_Display_H

#include "MENU_public.h"
#include "task_StackAI.h"
#include "obj_APCIService.h"

#define STATE_RM_SEL			0
#define STATE_PASSWORD_INPUT	1
#define STATE_RM_CONNECT		2
#define STATE_RM_INPUT			3

extern char rmInput[5];
int Input_RM_Password_Process(void);

#endif


