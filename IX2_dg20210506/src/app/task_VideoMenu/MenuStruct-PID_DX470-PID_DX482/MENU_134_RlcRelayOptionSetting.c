#include "../../cJSON/cJSON.h"
#include "MENU_134_RlcRelayOptionSetting.h"
#include "obj_NoticeIxmini.h"
#include "obj_UnifiedParameterMenu.h"	

unsigned int rlcRelayOptionSettingDataItem = 0;
const IconAndText_t rlcRelayOptionSettingIconTable[] = 
{
	{ICON_RlcRelayMode, 				MESG_TEXT_RlcRelayMode},	 
	//{ICON_RlcRelayActivateTime1, 		MESG_TEXT_RlcRelayActivateTime1},	 
	//{ICON_RlcRelayActivateTime2, 		MESG_TEXT_RlcRelayActivateTime2},	 
	//{ICON_RlcRelayDelayTime, 			MESG_TEXT_RlcRelayDelayTime},	 
};


const int rlcRelayOptionSettingIconTableNum = sizeof(rlcRelayOptionSettingIconTable)/sizeof(rlcRelayOptionSettingIconTable[0]);
static char rlcRelayOptionSettingRelayModeInput[4] = {0};
static char rlcRelayOptionSettingRelayActivateTimeInput[4] = {0};
static char rlcRelayOptionSettingRelayActivateTime2Input[4] = {0};
static char rlcRelayOptionSettingRelayDelayTimeInput[4] = {0};
static IXMiniRelayOption_T rlcRelayOptionSettingData;


static void DisplayRlcRelayOptionSettingIconTablePageIcon(int page)
{
	uint8 i, j;
	uint16 x, y,x1,y1;
	char display[100];
	POS pos;
	SIZE hv;

	API_DisableOsdUpdate();
		
	for(i = 0; i < rlcRelayOptionSettingIconTableNum; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x1 = x + (hv.h - pos.x)/3;
			y1 = y + 40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x1 =x+ (hv.h - pos.x)/2;
			y1 =y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		switch(rlcRelayOptionSettingIconTable[i].iCon)
		{
			case ICON_RlcRelayMode:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayOptionSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 4, "%d", rlcRelayOptionSettingData.RelayMode);
				snprintf(rlcRelayOptionSettingRelayModeInput, 4, "%d", rlcRelayOptionSettingData.RelayMode);
				break;
			
			case ICON_RlcRelayActivateTime1:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayOptionSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime);
				snprintf(rlcRelayOptionSettingRelayActivateTimeInput, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime);
				break;
			case ICON_RlcRelayActivateTime2:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayOptionSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime2);
				snprintf(rlcRelayOptionSettingRelayActivateTime2Input, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime2);
				break;
			case ICON_RlcRelayDelayTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayOptionSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 4,"%d", rlcRelayOptionSettingData.RelayDelayTime);
				snprintf(rlcRelayOptionSettingRelayDelayTimeInput, 4,"%d", rlcRelayOptionSettingData.RelayDelayTime);
				break;
		}
		API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
	}

	API_EnableOsdUpdate();
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);

}


static char* CreateIXMiniRelayOptionObject(IXMiniRelayOption_T obj)
{
    cJSON *root = NULL;
	char* string;
	
    root = cJSON_CreateObject();
	
    cJSON_AddNumberToObject(root, "RelayDeviceQty", obj.RelayDeviceQty);
    cJSON_AddNumberToObject(root, "RelayMode", obj.RelayMode);
    cJSON_AddNumberToObject(root, "RelayActivateTime", obj.RelayActivateTime);
    cJSON_AddNumberToObject(root, "RelayActivateTime2", obj.RelayActivateTime2);
    cJSON_AddNumberToObject(root, "RelayDelayTime", obj.RelayDelayTime);

	
	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static int ParseIXMiniRelayOptionObject(const char* json, IXMiniRelayOption_T* pData)
{
    int status = 0;

	memset(pData, 0, sizeof(IXMiniRelayOption_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
    ParseJsonNumber(root, "RelayDeviceQty", &pData->RelayDeviceQty);
    ParseJsonNumber(root, "RelayMode", &pData->RelayMode);
    ParseJsonNumber(root, "RelayActivateTime", &pData->RelayActivateTime);
    ParseJsonNumber(root, "RelayActivateTime2", &pData->RelayActivateTime2);
    ParseJsonNumber(root, "RelayDelayTime", &pData->RelayDelayTime);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void GetRlcRelayOptionConfigValue(void)
{
	extern ParaMenu_T OneRemotePararecord;
	ParseIXMiniRelayOptionObject(OneRemotePararecord.value, &rlcRelayOptionSettingData);
}

static int SaverlcRelayOptionSet(void)
{
	int num, i, j;
	char* tempChar;
	char temp1, temp2;
	int ret1, ret2;
	

	num = atoi(rlcRelayOptionSettingRelayModeInput);
	if(num >= 0 && num <= 1)
	{
		rlcRelayOptionSettingData.RelayMode = num;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
	
	num = atoi(rlcRelayOptionSettingRelayActivateTimeInput);
	if(num >= 0 && num <= 300)
	{
		rlcRelayOptionSettingData.RelayActivateTime = num;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
	
	num = atoi(rlcRelayOptionSettingRelayActivateTime2Input);
	if(num >= 0 && num <= 300)
	{
		rlcRelayOptionSettingData.RelayActivateTime2 = num;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
	
	num = atoi(rlcRelayOptionSettingRelayDelayTimeInput);
	if(num >= 0 && num <= 300)
	{
		rlcRelayOptionSettingData.RelayDelayTime = num;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}


	tempChar = CreateIXMiniRelayOptionObject(rlcRelayOptionSettingData);
	if(tempChar != NULL)
	{
		SetRemoteParaValue(tempChar);
		free(tempChar);
		return 1;
	}
	
	
	return 0;
}

void MENU_134_RlcRelayOptionSetting_Init(int uMenuCnt)
{
	ParaMenu_T record;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	GetParaMenuRecord(IXRLC_RelayOption, &record);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_LIST_COLOR, record.name, record.nameLen, 1, STR_UNICODE, 0);			

	DisplayRlcRelayOptionSettingIconTablePageIcon(0);
}

void MENU_134_RlcRelayOptionSetting_Exit(void)
{

}

void MENU_134_RlcRelayOptionSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len, i;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					CloseMenu();
					break;
					
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					rlcRelayOptionSettingDataItem = GetCurIcon() - ICON_007_PublicList1;
					switch(rlcRelayOptionSettingIconTable[rlcRelayOptionSettingDataItem].iCon)
					{
						case ICON_RlcRelayMode:
							snprintf(rlcRelayOptionSettingRelayModeInput, 4,"%d", rlcRelayOptionSettingData.RelayMode);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcRelayMode, rlcRelayOptionSettingRelayModeInput, 1, COLOR_WHITE, rlcRelayOptionSettingRelayModeInput, 1, SaverlcRelayOptionSet);
							break;	
							
						case ICON_RlcRelayActivateTime1:
							snprintf(rlcRelayOptionSettingRelayActivateTimeInput, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcRelayActivateTime1, rlcRelayOptionSettingRelayActivateTimeInput, 3, COLOR_WHITE, rlcRelayOptionSettingRelayActivateTimeInput, 1, SaverlcRelayOptionSet);
							break;	
						case ICON_RlcRelayActivateTime2:
							snprintf(rlcRelayOptionSettingRelayActivateTime2Input, 4,"%d", rlcRelayOptionSettingData.RelayActivateTime2);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcRelayActivateTime2, rlcRelayOptionSettingRelayActivateTime2Input, 3, COLOR_WHITE, rlcRelayOptionSettingRelayActivateTime2Input, 1, SaverlcRelayOptionSet);
							break;	
						case ICON_RlcRelayDelayTime:
							snprintf(rlcRelayOptionSettingRelayDelayTimeInput, 4,"%d", rlcRelayOptionSettingData.RelayDelayTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcRelayDelayTime, rlcRelayOptionSettingRelayDelayTimeInput, 3, COLOR_WHITE, rlcRelayOptionSettingRelayDelayTimeInput, 1, SaverlcRelayOptionSet);
							break;
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}



