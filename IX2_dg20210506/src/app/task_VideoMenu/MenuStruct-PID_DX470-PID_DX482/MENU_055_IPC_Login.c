#include "MENU_public.h"
#include "MENU_055_IPC_Login.h"
#include "onvif_discovery.h"
#include "obj_IPCTableSetting.h"


#define	IPC_Login_ICON_MAX	5
#define MAX_STR 			45

int IPC_LoginIconSelect;
int IPC_LoginPageSelect;
int IPC_LoginSetting;
int IPC_DelConfirm;
int IPC_DelConfirmSelect;

IPC_ONE_DEVICE ipcRecord;
extern onvif_login_info_t Login;

static IPC_LoginIcon *IPC_LoginIconTable=NULL;
int IPC_LoginIconNum;

const IPC_LoginIcon IPC_LoginIconTable1[] = 
{
	{ICON_275_IPC_IpAddr,				MESG_TEXT_ICON_275_IPC_IpAddr},
	{ICON_261_IPC_UserName,				MESG_TEXT_IPC_UserName},
	{ICON_262_IPC_UserPwd,				MESG_TEXT_IPC_UserPwd},
	{ICON_270_IPC_Login, 				MESG_TEXT_ICON_270_IPC_Login},
};
const IPC_LoginIcon IPC_LoginIconTable2[] = 
{
	{ICON_275_IPC_IpAddr,				MESG_TEXT_ICON_275_IPC_IpAddr},
	{ICON_261_IPC_UserName,				MESG_TEXT_IPC_UserName},
	{ICON_262_IPC_UserPwd,				MESG_TEXT_IPC_UserPwd},
	{ICON_270_IPC_Login, 				MESG_TEXT_ICON_270_IPC_Login},
	{ICON_267_IPC_Delete, 				MESG_TEXT_Delete},
};


static void DisplayIPC_LoginPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;
	
	for(i = 0; i < IPC_Login_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*IPC_Login_ICON_MAX+i < IPC_LoginIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, IPC_LoginIconTable[i+page*IPC_Login_ICON_MAX].iConText, 1, val_x-x);
			if(get_pane_type() == 7 )
			{
				x += 100;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(IPC_LoginIconTable[i+page*IPC_Login_ICON_MAX].iCon)
			{
				case ICON_275_IPC_IpAddr:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, ipcRecord.IP, strlen(ipcRecord.IP), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_261_IPC_UserName:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, ipcRecord.USER, strlen(ipcRecord.USER), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_262_IPC_UserPwd:
					memset(display,'*',strlen(ipcRecord.PWD));
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(ipcRecord.PWD), 1, STR_UTF8, hv.h - x);
					break;
				#if 0
				case ICON_263_IPC_Channel:
					snprintf(display, 100, "%d", ipcRecord.channel);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - x);
					break;
				#endif
			}
		}
	}
	pageNum = IPC_LoginIconNum/IPC_Login_ICON_MAX + (IPC_LoginIconNum%IPC_Login_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}

int InputIPC_IP_Addr(const char* string)
{
	strcpy(ipcRecord.IP, string);
	//snprintf(ipcRecord.IP, 200, "http://%s/onvif/device_service", IPC_IP);
	return 1;
}

void EnterIpcLoginMenu(char* ip, char* user, char* pwd, int stack)
{
	if(ip != NULL)
	{
		strcpy(ipcRecord.IP, ip);
	}
	else
	{
		strcpy(ipcRecord.IP, "192.168.243.101");
	}
	if(user != NULL)
	{
		strcpy(ipcRecord.USER, user);
	}
	else
	{
		strcpy(ipcRecord.USER, "admin");
	}
	if(pwd != NULL)
	{
		strcpy(ipcRecord.PWD, pwd);
	}
	else
	{
		strcpy(ipcRecord.PWD, "admin");
	}
	StartInitOneMenu(MENU_055_IPC_LOGIN,0,stack);

}

void MENU_055_IPC_Login_Init(int uMenuCnt)
{
	int x, y;
	char *pos1, *pos2;
	int len;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_Login, 1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	
	IPC_LoginIconSelect = 0;
	IPC_LoginPageSelect = 0;
	IPC_DelConfirm = 0;
	IPC_DelConfirmSelect = 0;
	if(ipc_check_exist(ipcRecord.IP, NULL) == 0)
	{
		IPC_LoginIconNum = sizeof(IPC_LoginIconTable2)/sizeof(IPC_LoginIconTable2[0]);
		IPC_LoginIconTable = IPC_LoginIconTable2;
	}
	else
	{
		IPC_LoginIconNum = sizeof(IPC_LoginIconTable1)/sizeof(IPC_LoginIconTable1[0]);
		IPC_LoginIconTable = IPC_LoginIconTable1;
	}
	DisplayIPC_LoginPageIcon(IPC_LoginPageSelect);
}

void MENU_055_IPC_Login_Exit(void)
{
	
}

void MENU_055_IPC_Login_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char rtsp_url[250] = {0};
	char device_url[250] = {0};
	int ret=-1;	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					IPC_LoginIconSelect = GetCurIcon() - ICON_007_PublicList1;
					IPC_LoginSetting = IPC_LoginPageSelect*IPC_Login_ICON_MAX + GetCurIcon() - ICON_007_PublicList1;
					
					if(IPC_LoginSetting >= IPC_LoginIconNum)
					{
						return;
					}
					
					switch(IPC_LoginIconTable[IPC_LoginSetting].iCon)
					{
						case ICON_275_IPC_IpAddr:
							ClearConfirm(&IPC_DelConfirm, &IPC_DelConfirmSelect, IPC_Login_ICON_MAX);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_275_IPC_IpAddr, ipcRecord.IP, 20, COLOR_WHITE, ipcRecord.IP, 1, InputIPC_IP_Addr);
							break;

						case ICON_261_IPC_UserName:
							ClearConfirm(&IPC_DelConfirm, &IPC_DelConfirmSelect, IPC_Login_ICON_MAX);
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_UserName, ipcRecord.USER, MAX_STR-1, COLOR_WHITE, ipcRecord.USER, 1, NULL);
							break;
						case ICON_262_IPC_UserPwd:
							ClearConfirm(&IPC_DelConfirm, &IPC_DelConfirmSelect, IPC_Login_ICON_MAX);
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_UserPwd, ipcRecord.PWD, MAX_STR-1, COLOR_WHITE, ipcRecord.PWD, 1, NULL);
							break;
						case ICON_270_IPC_Login:
							ClearConfirm(&IPC_DelConfirm, &IPC_DelConfirmSelect, IPC_Login_ICON_MAX);
							BusySpriteDisplay(1);
							ret = GetNameByIp(ipcRecord.IP, ipcRecord.NAME);
							if(ret == 0)
							{
								strcpy(ipcRecord.ID, ipcRecord.NAME);
								ipcRecord.NAME[20] = 0;
								if(ipcRecord.NAME[0]==0)
									strcpy(ipcRecord.NAME,"-");
								memset(&Login, 0, sizeof(onvif_login_info_t));
								if(one_ipc_Login(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, &Login) == 0)
								{
									BusySpriteDisplay(0);
									StartInitOneMenu(MENU_058_IPC_MANAGE,0,0);
								}
								else
								{
									BusySpriteDisplay(0);
									BEEP_ERROR();
								}
							}
							else
							{
								#if 0
								strcpy(ipcRecord.NAME,"-");
								strcpy(ipcRecord.ID, ipcRecord.NAME);
								memset(&Login, 0, sizeof(onvif_login_info_t));
								BusySpriteDisplay(1);
								if(one_ipc_Login(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, &Login) == 0)
								{
									BusySpriteDisplay(0);
									StartInitOneMenu(MENU_058_IPC_MANAGE,0,0);
								}
								else
								{
									BEEP_ERROR();
								}
								#else
								ret = PingNetwork(ipcRecord.IP);
								if(ret == 0)
								{
									strcpy(ipcRecord.NAME,"-");
									strcpy(ipcRecord.ID, ipcRecord.NAME);
									memset(&Login, 0, sizeof(onvif_login_info_t));
									if(one_ipc_Login(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, &Login) == 0)
									{
										BusySpriteDisplay(0);
										StartInitOneMenu(MENU_058_IPC_MANAGE,0,0);
									}
									else
									{
										BusySpriteDisplay(0);
										BEEP_ERROR();
									}
								}
								else
								{
									BusySpriteDisplay(0);
									BEEP_ERROR();
								}
								#endif
							}
							break;
						case ICON_267_IPC_Delete:
							if(ConfirmSelect(&IPC_DelConfirm, &IPC_DelConfirmSelect, &IPC_LoginIconSelect, IPC_LoginPageSelect, IPC_Login_ICON_MAX))
							{
								return;
							}
							DeleteOneIpc(ipcRecord.IP, NULL);
							BEEP_CONFIRM();
							popDisplayLastMenu();
							break;
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}



