#include "MENU_public.h"
#include "MENU_005_CallScene.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_CallServer.h"	//czn_20181227
#include "MENU_100_CardList.h"
#include "vtk_udp_stack_io_server.h"
#include "obj_menu_data.h"
#include "obj_GetIpByNumber.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_SearchIpByFilter.h"
#include "define_file.h"




#define	ID_CARD_LIST_ICON_MAX	5
#define	ID_CARD_LENGTH	11

unsigned int getCardSourceIp = 0;
unsigned int idCardListItem = 0;
unsigned int idCardSourceIndex = 0;
int idCardtargetIp;
int card_onLineDsCnt = 0;
extern FuncMenuType idCardSubMenu;


//IdCardList_s id_card_list;
IdCardGetReq_s id_card_get_req;
IdCardGetCountRsp_s id_card_get_conunt_rsp;
//IdCardGetInfoRsp_s id_card_get_info_rsp;
//IdCardInfo_s* id_card_list;
int id_card_cnt;
char* p_id_card_get_info_rsp = NULL;
char** p_id_card_info=NULL;
int card_list_cur_page;

void print_ip_from_uint(int ip)
{
        unsigned char *p = (unsigned char *)&ip; //注意p一定要定义为unsigned char
        
        int i;
        for(i = 4; i >0 ; i--)            //从高地址 还是低地址 开始输出
          printf("%d.", *(p + i - 1));    //取决于你机器是大端还是小端（这里是小端）
                                          //这里隐含一个char到int的转换

        printf("\b \n");
}


void GetIdCardSourceIpAdress(void)
{
	SearchIpRspData onLineDsListData;
	if(getCardSourceIp == 0)
	{
		API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_DS, SearchDeviceRecommendedWaitingTime, 30, &onLineDsListData);		
		if( onLineDsListData.deviceCnt > 0 )
		{
			unsigned int i;
			for( i=0; i<onLineDsListData.deviceCnt; i++ )
			{
				if( strcmp(onLineDsListData.data[i].BD_RM_MS+8, "01") == 0 )
				{
					idCardtargetIp = onLineDsListData.data[i].Ip;
					getCardSourceIp = 2;
					break;
				}
			}
		}
		if( getCardSourceIp < 2 )
		{
			getCardSourceIp = 1;
		}
	}
}


int get_id_card_info(void)
{
	int i;
	p_id_card_info = malloc( id_card_cnt*sizeof(p_id_card_info) );
	for( i = 0; i< id_card_cnt; i++ )
	{
		if( i == 0 )
		{			
			p_id_card_info[i] = strstr( p_id_card_get_info_rsp, "<ID>" );
			if( p_id_card_info[i] == NULL)
			{
				return -1;
			}
			else
			{
				//printf("p_id_card_info[%d]=%#X\n",i,p_id_card_info[i]);
				//p_id_card_info[i] += 4;
			}
		}
		else
		{
			p_id_card_info[i] = strstr( p_id_card_info[i-1]+4, "<ID>" );
			if( p_id_card_info[i] == NULL)
			{
				break;
			}
			else
			{
				//printf("p_id_card_info[%d]=%#X\n",i,p_id_card_info[i]);
				//p_id_card_info[i] += 4;
			}
		}
		//sprintf("%10.10s\n",p_id_card_info[i]);
	}
}


char* get_id_card_key_position(char* pCur,  Id_card_key_t key, int* len)
{
	char s_key[5];
	char* p;
	int i;
	switch(key)
	{
		case ID_CARD_KEY_RM:
		memcpy(s_key,"<RM>",5);
		break;
		case ID_CARD_KEY_ID:
		memcpy(s_key,"<ID>",5);
		break;
		case ID_CARD_KEY_NAME:
		memcpy(s_key,"<NA>",5);
		break;
		case ID_CARD_KEY_DATE:
		memcpy(s_key,"<DA>",5);
		break;
	}
	p = strstr( pCur, s_key );
	if( p == NULL )
	{
		return NULL;
	}
	p += 4;
	for( i = 0; i<100; i++ )
	{
		if( p[i] == 0 || p[i] == '<'  ||p[i] == 0x0A )
		{
			*len = i;
			return p;
		}
	}
	return NULL;
}

#define IDCARD		"CARD_NUM"
#define ENABLED		"ENABLED"
#define ROOM_NUM	"ROOM_NUM"
#define IN_DATE		"DATE"
#define OWNER		"NAME"

typedef struct
{
	unsigned char		card_num[10+1];
	char				property;
	unsigned char		room_num[8+1];
	unsigned char        date[8+1];
	unsigned char		owner[20+1];
	
} id_card_table_t;

one_vtk_table* idCardTable;


int FilterIdCardData( char* file )
{
	int i,len;
	one_vtk_dat* tempRecord;
	id_card_table_t pRecord;
	char data[40];
	char temp[100]={0};
	int idcard_cnt;
	
	idCardTable = load_vtk_table_file( file );

	if(idCardTable == NULL)
	{
		printf("idCardTable =NULL\n");
		return -1;
	}
	idcard_cnt = idCardTable->record_cnt;
	p_id_card_get_info_rsp = malloc(idcard_cnt*(sizeof(IdCardInfo_s)+10));
	p_id_card_get_info_rsp[0]=0;
	for(i=0; i<idcard_cnt; i++)
	{
		memset(&pRecord, 0, sizeof(id_card_table_t));
		tempRecord = get_one_vtk_record_without_keyvalue(idCardTable, i);
		if(tempRecord == NULL)
		{
			continue;
		}
		//取PROPERTY
		len = 1; data[0] = 0;
		get_one_table_record_string(idCardTable,i,ENABLED,data,&len); 
		data[len] = 0;
		pRecord.property = atoi(data);
		//取CARD_NUM
		data[0] = 0; len=10;
		get_one_table_record_string(idCardTable,i,IDCARD,data,&len); 
		data[len] = 0;
		strcpy(pRecord.card_num, data);
		//取ROOM_NUM
		len = 8; data[0] = 0;
		get_one_table_record_string(idCardTable,i,ROOM_NUM,data,&len); 
		data[len] = 0;
		strcpy(pRecord.room_num, data);
		//取date
		len = 8; data[0] = 0;
		get_one_table_record_string(idCardTable,i,IN_DATE,data,&len); 
		data[len] = 0;
		strcpy(pRecord.date, data);
		//取owner
		len = 20; data[0] = 0;
		get_one_table_record_string(idCardTable,i,OWNER,data,&len); 
		data[len] = 0;
		strcpy(pRecord.owner, data);
		
		//printf("Card room = %s name = %s\n",pRecord.room_num, pRecord.owner);
		if(memcmp(pRecord.room_num, GetSysVerInfo_BdRmMs(), 8))
		{
			continue;
		}
		id_card_cnt++;
	
		snprintf(temp,100,"<ID>%s<EN>%d<RM>%s<NA>%s<DA>%s\n",pRecord.card_num,pRecord.property,pRecord.room_num,pRecord.owner,pRecord.date);
		printf("1111111:buff=%d,temp=%d\n",sizeof(IdCardInfo_s),strlen(temp));
		strcat(p_id_card_get_info_rsp,temp);
	}
	printf("Card list cnt = %d  id_card_get_info_rsp= %s \n",id_card_cnt, p_id_card_get_info_rsp);
	return 0;
}

static void MenuListGetCardListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	int i, index;
	int recordLen, unicode_len;
	char unicode_buf[200];
	char* p;
	char DisplayBuffer[50];
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < id_card_cnt)
		{
			p = get_id_card_key_position( p_id_card_info[index], ID_CARD_KEY_ID, &recordLen );
			if( p != NULL )
			{
				memcpy(DisplayBuffer, p, recordLen);
			}
			unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}

	}
}

void CardListShow(int page)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithIcon(ICON_IdCard_List, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	if( get_pane_type() == 7 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = id_card_cnt;
	printf("!!!!!!!CardListShow listCnt %d\n", id_card_cnt);
	listInit.fun = MenuListGetCardListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.currentPage = page;
	InitMenuList(listInit);

}

void MENU_100_CardList_Init(int uMenuCnt)
{
	int i;
	char sync_ds_rm_nrb[11];
	char res_path[80];
	
	API_MenuIconDisplaySelectOn(ICON_IdCard_List);
	
	BusySpriteDisplay(1);

	GetIdCardSourceIpAdress();
	
	if( getCardSourceIp < 2 )
	{
		BusySpriteDisplay(0);
		return;
	}

	id_card_cnt = 0;
	strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
	strcat(sync_ds_rm_nrb,"000001");
	int ret = Api_ResSyncFromServer(8009,sync_ds_rm_nrb);
	if(ret==0)
	{
		sprintf(res_path,"%s/R8009.%s.csv",ResFileDir,GetSysVerInfo_bd());
		//printf("!!!!!!!Api_ResSyncFromServer ok file=%s\n", res_path);
		ret = FilterIdCardData(res_path);
	}
	else
	{
		printf("!!!!!!!Api_ResSyncFromServer fail code %d\n", ret);
	}
	

	if( (ret != 0)  || (id_card_cnt==0) )
	{
		BusySpriteDisplay(0);
		return;
	}
	
	get_id_card_info();

	CardListShow(0);

	BusySpriteDisplay(0);
	

}


void MENU_100_CardList_Exit(void)
{
	MenuListDisable(0);
	API_MenuIconDisplaySelectOff(ICON_IdCard_List);
	if( p_id_card_get_info_rsp != NULL )
	{
		free(p_id_card_get_info_rsp);
		p_id_card_get_info_rsp = NULL;
	}
	if( p_id_card_info != NULL )
	{
		free(p_id_card_info);
		p_id_card_info = NULL;
	}	
}

void MENU_100_CardList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int index;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					index = listIcon.mainIcon;
					if(index >= 0)	
					{
						idCardListItem = index;
						card_list_cur_page = MenuListGetCurrentPage();
						idCardSubMenu = MENU_103_CardInfo;
						MENU_103_CardInfo_Init();
					}
					break;
				#if 0	
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					idCardListItem = GetCurIcon() - ICON_007_PublicList1;
					
					if(idCardListItem >= ID_CARD_LIST_ICON_MAX)
					{
						return;
					}
					if( card_list_cur_page*ID_CARD_LIST_ICON_MAX + idCardListItem >= id_card_cnt )
					{
						return;
					}
					//EnterIdCardMenu(MENU_072_IdCardInfo,0);
					idCardSubMenu = MENU_103_CardInfo;
					MENU_103_CardInfo_Init();
					break;
				case ICON_201_PageDown:
					//PublicPageDownProcess(&card_list_cur_page, ID_CARD_LIST_ICON_MAX, id_card_cnt, (DispListPage)display_one_page_id_card);
					break;
					
				case ICON_202_PageUp:
					//PublicPageUpProcess(&card_list_cur_page, ID_CARD_LIST_ICON_MAX, id_card_cnt, (DispListPage)display_one_page_id_card);
					break;
				#endif	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		
	}
}

#if 0

int display_one_page_id_card( uint8 page )
{
	int i, x, y,length;
	char* p;
	//int list_start;
	//int max_pag = (id_card_cnt-1)/ID_CARD_LIST_ICON_MAX+1;
	POS pos;
	SIZE hv;
	
	for( i = 0; i < ID_CARD_LIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		if( page*ID_CARD_LIST_ICON_MAX + i >= id_card_cnt )
		{
			break;
		}
		else
		{
			//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, p_id_card_info[page*ID_CARD_LIST_ICON_MAX + i], 10,1,STR_UTF8, MENU_SCHEDULE_POS_X-x);
			p = get_id_card_key_position( p_id_card_info[page*ID_CARD_LIST_ICON_MAX + i], ID_CARD_KEY_ID, &length );
			if( p != NULL )
			{
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, p, length, 1, STR_UTF8, hv.h - x);
			}
		}
	}
	for( ; i < ID_CARD_LIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = GetIconXY(ICON_007_PublicList1+i).x+DISPLAY_DEVIATION_X;
		//y = GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	}
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, id_card_cnt>0?(id_card_cnt-1)/ID_CARD_LIST_ICON_MAX+1:0);

}


void MENU_100_CardList_Init(int uMenuCnt)
{
	SYS_VER_INFO_T sysInfo;
	//SearchIpRspData onLineDsListData;

	sysInfo = GetSysVerInfo();

	int len;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	

	API_MenuIconDisplaySelectOn(ICON_IdCard_List);
	
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_IdCard_List, 1, 0);

	BusySpriteDisplay(1);

	GetIdCardSourceIpAdress();
	
	if( getCardSourceIp < 2 )
	{
		BusySpriteDisplay(0);
		return;
	}
	
//	printf("Source room id: [%s]\n",sysInfo.bd_rm_ms);
	id_card_get_req.type = 1; // 统计信息
	id_card_get_req.keyType = 1; // bd_rm_ms
	id_card_get_req.num = 1; // 仅获取一个房号的卡数量
	memcpy( id_card_get_req.keyName, "<RM>",4 );
	memcpy(id_card_get_req.key,sysInfo.bd_rm_ms,8); // 仅取前8位
	id_card_get_req.key[8] = 0;
	len = sizeof(id_card_get_conunt_rsp);
	if( api_udp_io_server_send_req( idCardtargetIp ,CMD_RF_CARD_GET_REQ,(char*)&id_card_get_req,sizeof(id_card_get_req), (char*)&id_card_get_conunt_rsp, (unsigned int*)&len) == -1 )
	{
		printf("No response cmd = %#X\n",CMD_RF_CARD_GET_REQ);
		BusySpriteDisplay(0);
		return;
	}
//	unsigned int i;
//	for(i = 0; i<len; i++)
//	{
//		printf("id_card_get_conunt_rsp[%d]: %#X\n",i,((unsigned char*)&id_card_get_conunt_rsp)[i]);
//	}
	
	id_card_get_req.type = 0; // 具体信息

	unsigned short card_cnt = atoi(id_card_get_conunt_rsp.cn);

	printf("Card list cnt = %d\n",card_cnt);

	if( card_cnt <= 0 )
	{
		BusySpriteDisplay(0);
		return;
	}
	
	if( card_cnt > 100 )
	{
		card_cnt = 100;
	}

	id_card_cnt = card_cnt;
	
	len = card_cnt*(sizeof(IdCardInfo_s)+1);
	len += 4;
	p_id_card_get_info_rsp = malloc(len);
	if( api_udp_io_server_send_req( idCardtargetIp ,CMD_RF_CARD_GET_REQ,(char*)&id_card_get_req,sizeof(id_card_get_req),p_id_card_get_info_rsp, (unsigned int*)&len) == -1 )
	{
		printf("No response cmd = %#X\n",CMD_RF_CARD_GET_REQ);
		BusySpriteDisplay(0);
		return;
	}
	
//	for(i = 0; i<len; i++)
//	{
//		printf("id_card_get_info_rsp[%d]: %#X\n",i,p_id_card_get_info_rsp[i]);
//	}

	get_id_card_info();

	card_list_cur_page = 0;

	display_one_page_id_card(card_list_cur_page);

	BusySpriteDisplay(0);
	
	//id_card_list = (IdCardInfo_s*)p_id_card_get_info_rsp;

}
#endif