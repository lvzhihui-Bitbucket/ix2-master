#include "MENU_public.h"
#include "obj_SYS_VER_INFO.h"
#include "define_Command.h"
#include "MENU_010_About.h"
#include "cJSON.h"

static AboutList abloutList;
static int aboutQRCodeState;
static int aboutPageSelect;

const AboutKeyword_S AboutKeywordMode0[] = 
{
	{About_RMAddress, 				About_KeyRMAddress}, 		
	{About_Name,					About_KeyName},			
	//{About_Global_Nbr,				About_KeyGlobal_Nbr},	
	//{About_Local_Nbr, 				About_KeyLocal_Nbr}, 	

	//{About_IPAddress, 				About_KeyIPAddress},
	//{About_HW_Address,				About_KeyHW_Address},	
	//{About_SubnetMask,				About_KeySubnetMask},		
	//{About_DefaultRoute,			About_KeyDefaultRoute},	
	
	{About_WLANIPAddress,			About_KeyWLANIPAddress},
	{About_WLANHW_Address,			About_KeyWLANHW_Address},
	{About_WLANSubnetMask,			About_KeyWLANSubnetMask},
	{About_WLANDefaultRoute,		About_KeyWLANDefaultRoute}, 

	//{About_IPAddress,				About_KeyIPAddress},
	//{About_HW_Address,				About_KeyHW_Address},	
	//{About_SubnetMask,				About_KeySubnetMask},		
	//{About_DefaultRoute,			About_KeyDefaultRoute}, 

	{About_SW_Ver,					About_KeySW_Ver},	
	{About_HWVersion, 				About_KeyHWVersion},	
	//{About_UpgradeTime,				About_KeyUpgradeTime},		
	//{About_UpgradeCode,				About_KeyUpgradeCode},	
	{About_UpTime,					About_KeyUpTime},	
	{About_SerialNo,				About_KeySerialNo},		
	{About_DeviceType,				About_KeyDeviceType},	
	{About_DeviceModel,				About_KeyDeviceModel},		
	//{About_AreaCode,				About_KeyAreaCode},			
	//{About_TransferState,			About_KeyTransferState}, 	


	{AboutItemMax,			NULL}, 
};

const AboutKeyword_S AboutKeywordMode1[] = 
{
	{About_RMAddress, 				About_KeyRMAddress}, 		
	{About_Name,					About_KeyName},			
	//{About_Global_Nbr,				About_KeyGlobal_Nbr},	
	//{About_Local_Nbr, 				About_KeyLocal_Nbr}, 	

	{About_IPAddress, 				About_KeyIPAddress},
	{About_HW_Address,				About_KeyHW_Address},	
	{About_SubnetMask,				About_KeySubnetMask},		
	{About_DefaultRoute,			About_KeyDefaultRoute},	
	
	{About_WLANIPAddress,			About_KeyWLANIPAddress},
	{About_WLANHW_Address,			About_KeyWLANHW_Address},
	{About_WLANSubnetMask,			About_KeyWLANSubnetMask},
	{About_WLANDefaultRoute,		About_KeyWLANDefaultRoute}, 

	//{About_IPAddress,				About_KeyIPAddress},
	//{About_HW_Address,				About_KeyHW_Address},	
	//{About_SubnetMask,				About_KeySubnetMask},		
	//{About_DefaultRoute,			About_KeyDefaultRoute}, 

	{About_SW_Ver,					About_KeySW_Ver},	
	{About_HWVersion, 				About_KeyHWVersion},	
	//{About_UpgradeTime,				About_KeyUpgradeTime},		
	//{About_UpgradeCode,				About_KeyUpgradeCode},	
	{About_UpTime,					About_KeyUpTime},	
	{About_SerialNo,				About_KeySerialNo},		
	{About_DeviceType,				About_KeyDeviceType},	
	{About_DeviceModel,				About_KeyDeviceModel},		
	{About_AreaCode,				About_KeyAreaCode},			
	//{About_TransferState,			About_KeyTransferState}, 	


	{AboutItemMax,			NULL}, 
};
static AboutKeyword_S *AboutKeyword=NULL;

static void CollectAboutInfo(void)
{
	int i, len;
	char temp[5];
	API_Event_IoServer_InnerRead_All(AboutMenuMode, (uint8*)temp);
	if(atoi(temp)==1)
		AboutKeyword = AboutKeywordMode1;
	else
		AboutKeyword = AboutKeywordMode0;
	for(i = 0, abloutList.cnt = 0; AboutKeyword[i].item != AboutItemMax; i++)
	{
		switch(AboutKeyword[i].item)
		{
			case About_WLANIPAddress:
			case About_WLANHW_Address:
			case About_WLANSubnetMask:
			case About_WLANDefaultRoute:
				if(GetWifiConnected())
				{
					abloutList.itemAndKeyword[abloutList.cnt].item = AboutKeyword[i].item;
					abloutList.itemAndKeyword[abloutList.cnt].keyWord = AboutKeyword[i].keyWord;
					abloutList.cnt++;
				}
				break;
				
			default:
				abloutList.itemAndKeyword[abloutList.cnt].item = AboutKeyword[i].item;
				abloutList.itemAndKeyword[abloutList.cnt].keyWord = AboutKeyword[i].keyWord;
				abloutList.cnt++;
				break;
		}
	}
}


static void MenuListGetAboutPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen;

	char value_buf[200];
	int valueLen;

	int unicode_len;
	char unicode_buf[200];
	
	SYS_VER_INFO_T sysInfo;
	sysInfo = GetSysVerInfo();
	char content[40];
	int addr;

	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < abloutList.cnt)
		{
			switch(abloutList.itemAndKeyword[index].item)
			{
				case About_IPAddress:
					API_GetOSD_StringWithID(MESG_TEXT_IPAddress, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 

					displayLen = strlen(sysInfo.ip);
					valueLen = 2*api_ascii_to_unicode(sysInfo.ip, displayLen, value_buf);
					break;
					
				case About_RMAddress:
					API_GetOSD_StringWithID(MESG_TEXT_ICON_279_RM_ADDR, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					API_Event_IoServer_InnerRead_All(DX_IM_ADDR, content);
					addr = atoi(content);
					snprintf(content, 20, "[%02d]", addr==0? 32 : addr);
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					//displayLen = strlen(sysInfo.bd_rm_ms);
					//valueLen = 2*api_ascii_to_unicode(sysInfo.bd_rm_ms, displayLen, value_buf);
					break;

				case About_Name:
					API_GetOSD_StringWithID(MESG_TEXT_ICON_281_Name, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				
					displayLen = strlen(sysInfo.myName);
					valueLen = 2*utf82unicode(sysInfo.myName, displayLen, value_buf);
					break;
				case About_Global_Nbr:
					API_GetOSD_StringWithID(MESG_TEXT_ICON_278_Global_Nbr, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				
					displayLen = strlen(sysInfo.GLOBAL_NUM);
					valueLen = 2*api_ascii_to_unicode(sysInfo.GLOBAL_NUM, displayLen, value_buf);
					break;
				case About_Local_Nbr:
					API_GetOSD_StringWithID(MESG_TEXT_ICON_282_Local_Nbr, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				
					displayLen = strlen(sysInfo.LOCAL_NUM);
					valueLen = 2*api_ascii_to_unicode(sysInfo.LOCAL_NUM, displayLen, value_buf);
					break;
				case About_SW_Ver:
					API_GetOSD_StringWithID(MESG_TEXT_SW_Ver, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				
					displayLen = strlen(sysInfo.swVer);
					valueLen = 2*api_ascii_to_unicode(sysInfo.swVer, displayLen, value_buf);
					break;
				case About_HWVersion:
					API_GetOSD_StringWithString(HWVersion, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(HWVersion, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_UpgradeTime:
					API_GetOSD_StringWithString(UpgradeTime, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(UpgradeTime, content);
					
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_UpgradeCode:
					API_GetOSD_StringWithString(UpgradeCode, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(UpgradeCode, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_UpTime:
					API_GetOSD_StringWithString(UpTime, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(UpTime, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_SerialNo:
					API_GetOSD_StringWithID(MESG_TEXT_SerialNo, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.sn);
					valueLen = 2*api_ascii_to_unicode(sysInfo.sn, displayLen, value_buf);
					break;
				case About_DeviceType:
					API_GetOSD_StringWithID(MESG_TEXT_DeviceType, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					API_Event_IoServer_InnerRead_All(ParaDeviceType, content);
					//displayLen = strlen(sysInfo.type);
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_DeviceModel:
					API_GetOSD_StringWithString(DeviceModel, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(DeviceModel, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_AreaCode:
					API_GetOSD_StringWithString(AreaCode, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(AreaCode, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_TransferState:
					API_GetOSD_StringWithString(TransferState, unicode_buf, &unicode_len);
					API_Event_IoServer_InnerRead_All(TransferState, content);
				
					displayLen = strlen(content);
					valueLen = 2*api_ascii_to_unicode(content, displayLen, value_buf);
					break;
				case About_HW_Address:
					API_GetOSD_StringWithID(MESG_TEXT_HW_Address, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.mac);
					valueLen = 2*api_ascii_to_unicode(sysInfo.mac, displayLen, value_buf);
					break;
				case About_SubnetMask:
					API_GetOSD_StringWithID(MESG_TEXT_SubnetMask, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.mask);
					valueLen = 2*api_ascii_to_unicode(sysInfo.mask, displayLen, value_buf);
					break;
				case About_DefaultRoute:
					API_GetOSD_StringWithID(MESG_TEXT_DefaultRoute, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.gw);
					valueLen = 2*api_ascii_to_unicode(sysInfo.gw, displayLen, value_buf);
					break;
					
				case About_WLANIPAddress:
					API_GetOSD_StringWithID(MESG_TEXT_WLAN_IPAddress, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				
					displayLen = strlen(sysInfo.wlan_ip);
					valueLen = 2*api_ascii_to_unicode(sysInfo.wlan_ip, displayLen, value_buf);
					break;
				case About_WLANHW_Address:
					API_GetOSD_StringWithID(MESG_TEXT_WLAN_HW_Address, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.wlan_mac);
					valueLen = 2*api_ascii_to_unicode(sysInfo.wlan_mac, displayLen, value_buf);
					break;
				case About_WLANSubnetMask:
					API_GetOSD_StringWithID(MESG_TEXT_WLAN_SubnetMask, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.wlan_mask);
					valueLen = 2*api_ascii_to_unicode(sysInfo.wlan_mask, displayLen, value_buf);
					break;
				case About_WLANDefaultRoute:
					API_GetOSD_StringWithID(MESG_TEXT_WLAN_DefaultRoute, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					
					displayLen = strlen(sysInfo.wlan_gw);
					valueLen = 2*api_ascii_to_unicode(sysInfo.wlan_gw, displayLen, value_buf);
					break;
					
				default :
					continue;
			}

			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;

			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			

			memcpy(pDisp->disp[pDisp->dispCnt].val, value_buf, valueLen);
			pDisp->disp[pDisp->dispCnt].valLen = valueLen;
			pDisp->dispCnt++;
		}
	}

}

static char* GreateAboutJson(void)
{	
	int index;
	SYS_VER_INFO_T sysInfo;
	sysInfo = GetSysVerInfo();
	
	char content[40];
	char *pValue;
	
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	for(index = 0; index < abloutList.cnt; index++)
	{
		content[0] = 0;
		pValue = NULL;
		
		switch(abloutList.itemAndKeyword[index].item)
		{
			case About_IPAddress:
				pValue = sysInfo.ip;
				break;
				
			case About_RMAddress:
				//pValue = sysInfo.bd_rm_ms;
				API_Event_IoServer_InnerRead_All(DX_IM_ADDR, content);
				break;

			case About_Name:
				pValue = sysInfo.myName;
				break;
			case About_Global_Nbr:
				pValue = sysInfo.GLOBAL_NUM;
				break;
			case About_Local_Nbr:
				pValue = sysInfo.LOCAL_NUM;
				break;
			case About_SW_Ver:
				pValue = sysInfo.swVer;
				break;
			case About_HWVersion:
				API_Event_IoServer_InnerRead_All(HWVersion, content);
				break;
			case About_UpgradeTime:
				API_Event_IoServer_InnerRead_All(UpgradeTime, content);
				break;
			case About_UpgradeCode:
				API_Event_IoServer_InnerRead_All(UpgradeCode, content);
				break;
			case About_UpTime:
				API_Event_IoServer_InnerRead_All(UpTime, content);
				break;
			case About_SerialNo:
				pValue = sysInfo.sn;
				break;
			case About_DeviceType:
				pValue = sysInfo.type;
				break;
			case About_DeviceModel:
				API_Event_IoServer_InnerRead_All(DeviceModel, content);
				break;
			case About_AreaCode:
				API_Event_IoServer_InnerRead_All(AreaCode, content);
				break;
			case About_TransferState:
				API_Event_IoServer_InnerRead_All(TransferState, content);
				break;
			case About_HW_Address:
				pValue = sysInfo.mac;
				break;
			case About_SubnetMask:
				pValue = sysInfo.mask;
				break;
			case About_DefaultRoute:
				pValue = sysInfo.gw;
				break;
			
			case About_WLANIPAddress:
				pValue = sysInfo.wlan_ip;
				break;
			case About_WLANHW_Address:
				pValue = sysInfo.wlan_mac;
				break;
			case About_WLANSubnetMask:
				pValue = sysInfo.wlan_mask;
				break;
			case About_WLANDefaultRoute:
				pValue = sysInfo.wlan_gw;
				break;
							
			default :
				continue;
		}
		
		if(pValue != NULL)
		{
			cJSON_AddStringToObject(root, abloutList.itemAndKeyword[index].keyWord, pValue);
		}
		else
		{
			cJSON_AddStringToObject(root, abloutList.itemAndKeyword[index].keyWord, content);
		}
	}
	
	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static ShowAboutQRCode(int enable)
{
	char* jsonString;

	if(enable)
	{
		jsonString = GreateAboutJson();
		if(jsonString != NULL)
		{
			qrenc_draw_bitmap(jsonString, GetMenuListXY(5), GetMenuListXY(6), 6, GetMenuListXY(7), GetMenuListXY(8));
			free(jsonString);
		}
	}
	else
	{
		API_DisableOsdUpdate();
		API_OsdStringClearExt(GetMenuListXY(5), GetMenuListXY(6), GetMenuListXY(7), GetMenuListXY(8));
		API_EnableOsdUpdate();
	}
}
void	Flash_Check_Display(int index,char *path,char *md5,int ret)
{
	printf("Flash_Check_Display %d:%s:\n%s:%s\n",index+1,(ret==0)?"ok":"error",path,md5);
}
#include "cJSON.h"	
int Flash_Check(char *check_file_path)
{
	int i,j;
	cJSON *check_json = GetJsonFromFile(check_file_path);
	cJSON *item;
	char *path,*md5;
	char check_md5[40]={0};
	char calc_md5[40]={0};
	int ret=0;
	if(check_json==NULL||cJSON_GetArraySize(check_json)==0)
		return -1;
	for(i=0;i<cJSON_GetArraySize(check_json);i++)
	{
		item=cJSON_GetArrayItem(check_json,i);
		if(item==NULL)
			break;
		path=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(item,"PATH"));
		md5=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(item,"MD5"));
		if(path==NULL||md5==NULL)
			break;
		FileMd5_CalculateString(path,calc_md5);
		for(j=0;j<32;j++)
		{
			check_md5[j]=toupper(md5[j]);
		}
		
		if(memcmp(calc_md5,check_md5,32)==0)
		{
			Flash_Check_Display(i,path,calc_md5,0);
		}
		else
		{
			
			Flash_Check_Display(i,path,calc_md5,-1);
			ret=-1;
		}
	}
	cJSON_Delete(check_json);
	return ret;
}
void MENU_010_About_Init(int uMenuCnt)
{
	char unicode_buf[200];
	int unicode_len;
	LIST_INIT_T listInit;
	
	POS pos;
	SIZE hv;
	int width;
	
	aboutQRCodeState = 0;

	//显示二维码按键
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	width = ((hv.h - pos.x) < (hv.v - pos.y) ? (hv.h - pos.x) : (hv.v - pos.y));
	qrenc_draw_bitmap("show QR code!", pos.x , pos.y, 3, width, width);

	//显示about信息
	API_GetOSD_StringWithIcon(ICON_028_about, unicode_buf, &unicode_len);

	listInit = ListPropertyDefault();
	
	if( get_pane_type() == 7 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 100;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 300;
	}	
		
	listInit.listIconEn = 0;
	listInit.valEn = 1;
	listInit.fun = MenuListGetAboutPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	
	if(GetLastNMenu() != MENU_001_MAIN && GetLastNMenu() != MENU_108_QuickAccess)
	{
		listInit.currentPage = aboutPageSelect;
	}
	else
	{
		listInit.currentPage = 0;
		CollectAboutInfo();
	}
	listInit.listCnt = abloutList.cnt;

	API_MenuIconDisplaySelectOn(ICON_040_AboutPage1);
	
	InitMenuList(listInit);
}

void MENU_010_About_Exit(void)
{	
	MenuListDisable(0);
}

void MENU_010_About_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					break;
				case KEY_TALK:
					BusySpriteDisplay(1);
					if(Flash_Check("/mnt/nand1-2/check_flash.json")==0)
					{
						BEEP_CONFIRM();
					}
					else
					{
						BEEP_ERROR();
					}
					BusySpriteDisplay(0);
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_176_KeyState:
					if(aboutQRCodeState)
					{
						MenuListSetCurrentPage(aboutPageSelect);
						aboutQRCodeState = 0;
					}
					else
					{
						ShowAboutQRCode(1);
						aboutQRCodeState = 1;
					}
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon < MENU_LIST_ICON_NONE)
					{
						aboutPageSelect = MenuListGetCurrentPage();
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


