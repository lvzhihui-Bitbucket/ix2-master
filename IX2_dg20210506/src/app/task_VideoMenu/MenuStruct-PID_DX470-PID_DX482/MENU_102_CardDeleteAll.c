#include "MENU_public.h"
#include "MENU_005_CallScene.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_CallServer.h"	//czn_20181227
#include "MENU_103_CardInfo.h"


#define	ID_CARD_DEL_ALL_ICON_MAX	5
unsigned int idCardDelAllItem = 0;
extern int idCardtargetIp;
extern int getCardSourceIp;
static int card_del_all_confirm = -1;
static void ClearDeleteConfirmFlag(void)
{
	int confirmx,confirmy;
	if( get_pane_type() == 7 )
	{
		confirmx = 450;
		confirmy = 110;
	}
	else
	{
		confirmx = 620;
		confirmy = 105;
	}
	if(card_del_all_confirm >= 0)
	{
		API_SpriteClose(confirmx, confirmy, SPRITE_IF_CONFIRM);
	}
	card_del_all_confirm = -1;
}

void card_info_del_all_process(void)
{
	int len;
	IdCardDelReq_s id_card_del_req;
	IdCardDelRsp_s id_card_del_rsp;
	SYS_VER_INFO_T sysInfo;
	int confirmx,confirmy;
	if( get_pane_type() == 7 )
	{
		confirmx = 450;
		confirmy = 110;
	}
	else
	{
		confirmx = 620;
		confirmy = 105;
	}
	if( getCardSourceIp < 2 )
	{
		BEEP_ERROR();
		return;
	}
	
	if(card_del_all_confirm < 0)
	{
		card_del_all_confirm = 0;
		API_SpriteDisplay_XY(confirmx, confirmy, SPRITE_IF_CONFIRM);
	}
	else
	{
		card_del_all_confirm = -1;
		API_SpriteClose(confirmx, confirmy, SPRITE_IF_CONFIRM);

		id_card_del_req.keyType = 2; // room
		id_card_del_req.num = 1; // 删除一个房号所有卡
		memcpy( id_card_del_req.keyName, "<RM>",4 );
		sysInfo = GetSysVerInfo();
		memcpy(id_card_del_req.key,sysInfo.bd_rm_ms,10); // <ID>&card_num
		id_card_del_req.key[10] = 0;
		len = sizeof(id_card_del_rsp);
		printf("del card dat:%s\n",id_card_del_req.keyName);
		if( api_udp_io_server_send_req( idCardtargetIp ,CMD_RF_CARD_DEL_REQ,(char*)&id_card_del_req,sizeof(id_card_del_req), (char*)&id_card_del_rsp, &len ) == -1 )
		{
			printf("No response cmd = %#X\n",CMD_RF_CARD_DEL_REQ);
			BEEP_ERROR();
			return;
		}

		if( id_card_del_rsp.num >= 1 )
		{
			BEEP_CONFIRM();
		}
		else
		{
			BEEP_ERROR();
			return;
		}
	}
}

static void MenuListGetCardDelPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	int i, index;
	int unicode_len;
	char unicode_buf[200];
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < 1)
		{
			API_GetOSD_StringWithID(MESG_TEXT_CARD_Delete, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}

	}
}

void MENU_102_CardDelAll_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	API_MenuIconDisplaySelectOn(ICON_IdCard_DelAll);
	API_GetOSD_StringWithIcon(ICON_IdCard_DelAll, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	if( get_pane_type() == 7 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = 1;
	listInit.fun = MenuListGetCardDelPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);	
}

void MENU_102_CardDelAll_Exit(void)
{
	MenuListDisable(0);
	API_MenuIconDisplaySelectOff(ICON_IdCard_DelAll);
	ClearDeleteConfirmFlag();
}

void MENU_102_CardDelAll_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int index;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					index = listIcon.mainIcon;
					if(index >= 0)
					{
						card_info_del_all_process();
					}
					break;		
				#if 0
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					idCardDelAllItem = GetCurIcon() - ICON_007_PublicList1;
					
					if(idCardDelAllItem >= ID_CARD_DEL_ALL_ICON_MAX)
					{
						return;
					}
					
					break;
				case ICON_007_PublicList1:
					card_info_del_all_process();
					break;	
				#endif					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		
	}
}

#if 0
void MENU_102_CardDelAll_Init(int uMenuCnt)
{
	uint16 x,y;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	API_MenuIconDisplaySelectOn(ICON_IdCard_DelAll);
	
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_IdCard_DelAll, 1, 0);

	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+DISPLAY_DEVIATION_Y;

	 // rm
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_CARD_Delete, 1, (DISPLAY_VALUE_X_DEVIATION - 150));

}
#endif