#include "MENU_public.h"
#include "MENU_144_RM_Display.h"



static unsigned char Link_addr;
char rmInput[5];
static int RmState;

int GetRmState(void)
{
	return RmState;
}
void SetRmState(int state)
{
	RmState = state;
}
void SetRmLinkAddr(unsigned char addr)
{
	Link_addr = addr;
}

int Input_RM_Password_Process(void)
{
    uint8 data[20];
	uint8 inputLen;
	int i;

	//printf("Input_RM_Password_Process RmState = %d\n", RmState);
	if(RmState == STATE_PASSWORD_INPUT)
	{
		inputLen = strlen(rmInput);
		data[0] = 0; // all rm item
		data[1] = myAddr;
		data[2] = inputLen;
		memcpy( data+3, rmInput, inputLen);

		API_Stack_New_APT_Data( Link_addr, APCI_RM_REQUEST, inputLen+3, data );
		for(i = 10; i > 0; i--)
		{
			usleep(100*1000);
			if(RmState != STATE_PASSWORD_INPUT)
			{
				break;
			}
		}

		if(RmState == STATE_PASSWORD_INPUT)
		{
			RmState =STATE_RM_SEL;
			BEEP_ERROR();
			popDisplayLastMenu();
		}
		else
		{
			RmState = STATE_RM_CONNECT;
			StartInitOneMenu(MENU_144_RM_Display,0,0);
		}
	}
	else if(RmState == STATE_RM_INPUT)
	{
		inputLen = strlen(rmInput);
		data[0] = myAddr;
		data[1] = inputLen;
		memcpy( data+2, rmInput, inputLen);
		API_Stack_New_APT_Data( Link_addr, APCI_RM_INPUT_RES, inputLen+2, data );
		RmState = STATE_RM_CONNECT;
		popDisplayLastMenu();
	}
	
	return 2;
}


void MENU_144_RM_Display_Init(int uMenuCnt)
{
	if(RmState == STATE_RM_INPUT)
	{
		RmState = STATE_RM_CONNECT;
		API_Stack_New_APT(Link_addr, APCI_RM_EXIT_INPUT);
	}
}


void MENU_144_RM_Display_Exit(void)
{
	if(RmState == STATE_RM_CONNECT)
	{
		TimerRmMenuExit();
	}
}

void MENU_144_RM_Display_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					//DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					TimerRmMenuExit();
					popDisplayLastMenu();
					break;
				case ICON_RM_T_OK:
				case ICON_RM_T_EXIT:
				case ICON_RM_T_MOVE_UP:
				case ICON_RM_T_MOVE_DN:
				case ICON_RM_T_PAGE_LAST:
				case ICON_RM_T_PAGE_NEXT:
					if(RmState == STATE_RM_CONNECT)
					{
						API_Stack_New_APT(Link_addr,APCI_RM_OK+(GetCurIcon()-ICON_RM_T_OK));
					}

					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{

	}
}

void RmCmdProcess( unsigned char cmd, unsigned char len, unsigned char* data )
{
	if(GetLastNMenu() == MENU_144_RM_Display)
    {
        return;
    }
	
    if( RmState == STATE_PASSWORD_INPUT )
	{
        switch( cmd )
        {
            case APCI_RM_REP_REQUEST:
                if( data[0] == 0 )
                {
                    RmState = STATE_RM_CONNECT;
					//printf("RmState = STATE_RM_CONNECT\n");
                }
                break;
        }
    }
    else if( RmState == STATE_RM_CONNECT )
    {
        switch( cmd )
        {
            case APCI_RM_QUIT:
                RmState = STATE_RM_SEL;
				//printf("RmState = STATE_RM_SEL\n");
				popDisplayLastMenu();
                break;
            case APCI_RM_ENTER_INPUT:
                RmState = STATE_RM_INPUT;
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_Keypad_Prompt, rmInput, data[0], COLOR_WHITE, NULL, 0, Input_RM_Password_Process);
                break;
        }
    }
//    else if( RmState == STATE_RM_INPUT )
//	{
//        switch( cmd )
//		{
//			case APCI_RM_EXIT_INPUT:
//                RmState = STATE_RM_CONNECT;
//                break;	
//		}
//	
//	}
}

void TimerRmMenuExit(void)
{
    RmState = STATE_RM_SEL;
    API_Stack_New_APT( Link_addr, APCI_RM_QUIT );
}





