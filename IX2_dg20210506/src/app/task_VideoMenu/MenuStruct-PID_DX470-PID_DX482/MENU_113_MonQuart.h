
#ifndef _MENU_113_H
#define _MENU_113_H

#include "MENU_public.h"

typedef struct 
{
	int vd_posx;
	int vd_posy;
	int vd_width;
	int vd_height;

}QUART_POS_T;

void SetVDPos(int x, int y, int width, int height);
void GetVDPos(QUART_POS_T* show);

#endif


