#include "MENU_007_SetGeneral.h"
#include "MenuSetDateTime_Business.h"

//#define	SetGeneral_ICON_MAX	5
int setGeneralIconMax;
int setGeneralIconSelect;
static int setGeneralPageSelect;
int autoUnlock = 0;
int divert_with_mon;
extern int private_code_mask;
extern int confirm_set_private_code(const char* input);
int SetMenuLanguage(int index);
int SetMyName(const char* name);
extern char installCallNumInput[21];

const SetGeneralIcon setGeneralIconTableMode0[] = 
{
	{ICON_041_DateAndTime,				MESG_TEXT_Icon_041_DateAndTime},    
	{ICON_056_Language,					MESG_TEXT_Icon_056_Language},
	{ICON_057_MonitorTime,				MESG_TEXT_Icon_057_MonitorTime},
	{ICON_300_ShortcutSetting,			MESG_TEXT_ICON_300_ShortcutSetting},
	{ICON_058_SDCard,					MESG_TEXT_Icon_058_SDCard},
	{ICON_AutoCloseAfterUnlock,			MESG_TEXT_ICON_AutoCloseAfterUnlock},
	{ICON_AutoUnlock, 					MESG_TEXT_ICON_AutoUnlock},
	{ICON_281_Name, 					MESG_TEXT_ICON_281_Name},
	{ICON_KeyBeepCtrl,					MESG_TEXT_ICON_KeyBeepCtrl},
	//{ICON_DivertWithMonitor,			MESG_TEXT_ICON_DivertWithMonitor},	
};

const int setGeneralIconNumMode0 = sizeof(setGeneralIconTableMode0)/sizeof(setGeneralIconTableMode0[0]);

const SetGeneralIcon setGeneralIconTableMode1[] = 
{
	{ICON_041_DateAndTime,				MESG_TEXT_Icon_041_DateAndTime},    
	{ICON_056_Language,					MESG_TEXT_Icon_056_Language},
	{ICON_057_MonitorTime,				MESG_TEXT_Icon_057_MonitorTime},
	{ICON_300_ShortcutSetting,			MESG_TEXT_ICON_300_ShortcutSetting},
	{ICON_058_SDCard,					MESG_TEXT_Icon_058_SDCard},
	{ICON_AutoCloseAfterUnlock,			MESG_TEXT_ICON_AutoCloseAfterUnlock},
	{ICON_AutoUnlock, 					MESG_TEXT_ICON_AutoUnlock},
	{ICON_281_Name, 					MESG_TEXT_ICON_281_Name},
	{ICON_KeyBeepCtrl,					MESG_TEXT_ICON_KeyBeepCtrl},
};

const int setGeneralIconNumMode1 = sizeof(setGeneralIconTableMode1)/sizeof(setGeneralIconTableMode1[0]);

const SetGeneralIcon setGeneralIconTableMode2[] = 
{
	{ICON_041_DateAndTime,				MESG_TEXT_Icon_041_DateAndTime},    
	{ICON_056_Language,					MESG_TEXT_Icon_056_Language},
	{ICON_057_MonitorTime,				MESG_TEXT_Icon_057_MonitorTime},
	{ICON_300_ShortcutSetting,			MESG_TEXT_ICON_300_ShortcutSetting},
	{ICON_058_SDCard,					MESG_TEXT_Icon_058_SDCard},
	{ICON_AutoCloseAfterUnlock,			MESG_TEXT_ICON_AutoCloseAfterUnlock},
	{ICON_AutoUnlock, 					MESG_TEXT_ICON_AutoUnlock},
	{ICON_281_Name, 					MESG_TEXT_ICON_281_Name},
	{ICON_027_SipConfig,				MESG_TEXT_SipConfig},
	{ICON_051_PrivateUnlockCode,		MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_KeyBeepCtrl,				MESG_TEXT_ICON_KeyBeepCtrl},
};

const int setGeneralIconNumMode2 = sizeof(setGeneralIconTableMode2)/sizeof(setGeneralIconTableMode2[0]);

static SetGeneralIcon *setGeneralIconTable=NULL;
static int setGeneralIconNum = 0;

static void DisplaySetGeneralPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	uint16 monitorTime;
	char display[200];
	int stringId;
	POS pos;
	SIZE hv;
	// lzh_20181016_s	
	API_DisableOsdUpdate();
	// lzh_20181016_e
	for(i = 0; i < setGeneralIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		//printf("OSD_GetIconInfo x=%d, y=%d, h=%d, v=%d\n",pos.x, pos.y, hv.h, hv.v);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		if(page*setGeneralIconMax+i < setGeneralIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setGeneralIconTable[i+page*setGeneralIconMax].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			
			switch(setGeneralIconTable[i+page*setGeneralIconMax].iCon)
			{
				case ICON_056_Language:
					display[0] = '[';
					GetMenuLanguageDisplayName(display+1);
					strcat(display, "]");
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_057_MonitorTime:
					API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, display);
					monitorTime = atoi(display);
					snprintf(display, 20, "[%03d]", monitorTime);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_AutoCloseAfterUnlock:
					API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, display);
					#if 0
					if(setGeneralIconTable==setGeneralIconTableMode2)
						stringId = (atoi(display) ? MESG_TEXT_Enable2 : MESG_TEXT_Disable2);
					else
					#endif
						stringId = (atoi(display) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplay(x, y, COLOR_RED, stringId, 1, hv.h - x);
					break;
				case ICON_AutoUnlock:
					API_Event_IoServer_InnerRead_All(AutoUnlockEditEn, display);
					autoUnlock=atoi(display);
					if(autoUnlock==0)
					{
						API_Event_IoServer_InnerRead_All(AutoUnlockIo, display);
						autoUnlock = atoi(display);
					}
					else
						autoUnlock=0;
					stringId = (autoUnlock ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplay(x, y, COLOR_RED, stringId, 1, hv.h - x);
					break;
				case ICON_281_Name:
					strcpy(display, GetSysVerInfo_name());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
					
				case ICON_051_PrivateUnlockCode:
					private_code_display(x,y);
					break;
				case ICON_KeyBeepCtrl	:
					API_Event_IoServer_InnerRead_All(KeyBeepDis, display);
					KeyBeepDisable = atoi(display);
					stringId = (KeyBeepDisable ? MESG_TEXT_Disable: MESG_TEXT_Enable);
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, stringId, 1, hv.h - x);
					break;
				case ICON_DivertWithMonitor:		
					API_Event_IoServer_InnerRead_All(DIVERT_WITH_LOCAL, display);
					divert_with_mon = atoi(display);
					stringId = (divert_with_mon ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, stringId, 1, hv.h - x);
					break;
	
				default:
					break;
			}
		}
	}
	pageNum = setGeneralIconNum/setGeneralIconMax + (setGeneralIconNum%setGeneralIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	// lzh_20181016_s	
	API_EnableOsdUpdate();
	// lzh_20181016_e	
}

void UpdateMonitorTimeSet(int select)
{
	uint16 monitorTime;
	char temp[20];

	if(select >= 0 && select < 10)
	{
		monitorTime = (select+1) * 30;
		
		sprintf(temp, "%d", monitorTime);
		API_Event_IoServer_InnerWrite_All(MONITOR_TIME_LIMIT, temp);
	}
}


void AutoUnlockSet(int enable)
{
	char temp[5];
	autoUnlock = enable ? 1 : 0;
	sprintf(temp,"%d",autoUnlock);
	API_Event_IoServer_InnerWrite_All(AutoUnlockIo, temp);
}

int GetAutoUnlock(void)
{
	char temp[5];
	int timeS = -1;
	int timeE = -1;
	API_Event_IoServer_InnerRead_All(AutoUnlockEditEn, temp);
	autoUnlock = atoi(temp);
	if(autoUnlock==0)
	{
		API_Event_IoServer_InnerRead_All(AutoUnlockIo, temp);
		autoUnlock = atoi(temp);
	}
	else
		autoUnlock=0;
	if(API_Event_IoServer_InnerRead_All(AutoUnlockStartTime, temp) == 0)
	{
		timeS = atoi(temp);
	}
	if(API_Event_IoServer_InnerRead_All(AutoUnlockEndTime, temp) == 0)
	{
		timeE = atoi(temp);
		if(timeE < timeS)
		{
			timeS = 0;
			timeE = 23;
		}
	}
	if(timeS == -1 || timeE == -1 || autoUnlock == 0)
	{
		return autoUnlock;
	}
	else
	{
		ReadDateTime(0);
		if(atol(hour) >= timeS && atol(hour) <= timeE)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
}

void AutoCloseAfterUnlockSet(int enable)
{
	char temp[20];
	int setValue;
	
	setValue = enable ? 1 : 0;
	sprintf(temp, "%d", setValue);
	API_Event_IoServer_InnerWrite_All(AutoCloseAfterUnlockEnable, temp);
}

void KeyBeepDisSet(int enable)
{
	char temp[5];
	KeyBeepDisable = enable;
	sprintf(temp,"%d",KeyBeepDisable);
	API_Event_IoServer_InnerWrite_All(KeyBeepDis, temp);
}

void SaveDivertWithMon(int mode)
{
	char temp[5];
	divert_with_mon = mode;
	sprintf(temp,"%d",divert_with_mon);
	API_Event_IoServer_InnerWrite_All(DIVERT_WITH_LOCAL, temp);
}

int GeneralSettingVerifyPassword(const char* password)
 {
 	char tempChar[30];
	int i, j;
	uint16 monitorTime;
	char pwd[8+1];
	int len, setValue;

	API_Event_IoServer_InnerRead_All(InstallerPassword, (uint8*)pwd);

	

	if(!strcmp(password, pwd))
	{
		API_Event_IoServer_InnerRead_All(GerneralPwdPlace, (uint8*)tempChar);
		//EnterInstallerMode();
		if(atoi(tempChar) ==0)
		{
			switch(setGeneralIconTable[setGeneralPageSelect*setGeneralIconMax+setGeneralIconSelect].iCon)
			{
				case ICON_041_DateAndTime:
					StartInitOneMenu(MENU_029_DATE_TIME_SET,0,0);
					break;

				case ICON_056_Language:
					InitPublicSettingMenuDisplay(GetMenuLanguageListCnt(), GetMenuLanguageListBuffer());
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_LanguageSelectTitle, GetMenuLanguageListCnt(), GetMenuLanguageIndex(), SetMenuLanguage);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;

				case ICON_057_MonitorTime:
					for(i = 0; i < 10; i++)
					{
						snprintf(tempChar, 30, "%d(s)", 30 * (i+1));
						for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
						{
							publicSettingDisplay[i][j] = tempChar[j/2];
						}
					}
					API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, tempChar);
					monitorTime = atoi(tempChar);
					monitorTime = monitorTime > 300 ? 300 : monitorTime;
					monitorTime = monitorTime < 30 ? 30 : monitorTime;
					
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_MonitorTimeSet, 10, monitorTime/30 - 1, UpdateMonitorTimeSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;

				case ICON_058_SDCard:
					StartInitOneMenu(MENU_045_SD_CARD,0,0);
					break;
				case ICON_300_ShortcutSetting:
					StartInitOneMenu(MENU_030_SHORTCUT_SET,0,0);
					break;
					
				case ICON_AutoCloseAfterUnlock:
					#if 0
					if(setGeneralIconTable==setGeneralIconTableMode2)
					{
						API_GetOSD_StringWithID(MESG_TEXT_Disable2, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
						publicSettingDisplay[0][0] = len;
						API_GetOSD_StringWithID(MESG_TEXT_Enable2, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
						publicSettingDisplay[1][0] = len;
					}
					else
					#endif
					{
						API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
						publicSettingDisplay[0][0] = len;
						API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
						publicSettingDisplay[1][0] = len;
					}
					
					API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, tempChar);
					setValue = atoi(tempChar);
				
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoCloseAfterUnlock, 2, setValue, AutoCloseAfterUnlockSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_AutoUnlock:
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
				
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoUnlock, 2, autoUnlock, AutoUnlockSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_281_Name:
					EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
					popLastMenu();
					break;
				case ICON_027_SipConfig:
					StartInitOneMenu(MENU_009_SIP_CONFIG,0,0);
					break;	
				case ICON_051_PrivateUnlockCode:
					API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, tempChar);
					//if( (strcmp(tempChar, "0") == 0) || private_code_mask >= 2 )
					{
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_PrivateUnlockCode, tempChar, 4, COLOR_WHITE, tempChar, 1, confirm_set_private_code);
						popLastMenu();
					}
					break;
				case ICON_KeyBeepCtrl	:
					//API_Event_IoServer_InnerRead_All(KeyBeepDis, (uint8*)tempChar);
				
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
				
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_KeyBeepCtrl, 2, KeyBeepDisable, KeyBeepDisSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;	
				case ICON_DivertWithMonitor:	
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_DivertWithMonitor, 2, divert_with_mon, SaveDivertWithMon);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;		
									
			}
		}
		else
		{
			EnterSettingMenu(MENU_007_SET_GENERAL, 0);

		}
		return -1;
	}
	else
	{
		return 0;
	}

 }


static void MenuListGetGeneralPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen, unicode_len;
	char unicode_buf[200];
	char display[80] = {0};
	int stringId;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < setGeneralIconNum)
		{
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			
			API_GetOSD_StringWithID(setGeneralIconTable[index].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

			unicode_len = 0;
			
			switch(setGeneralIconTable[index].iCon)
			{
				case ICON_056_Language:
					display[0] = '[';
					GetMenuLanguageDisplayName(display+1);
					strcat(display, "]");
					displayLen = strlen(display);
					unicode_len = 2*api_ascii_to_unicode(display,displayLen,unicode_buf);
					break;
				case ICON_057_MonitorTime:
					API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, display);
					snprintf(display, 20, "[%03d]", atoi(display));
					displayLen = strlen(display);
					unicode_len = 2*api_ascii_to_unicode(display,displayLen,unicode_buf);
					break;
				case ICON_AutoCloseAfterUnlock:
					API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, display);
					stringId = (atoi(display) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
				API_GetOSD_StringWithID(stringId, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
				case ICON_AutoUnlock:
					stringId = (autoUnlock ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_GetOSD_StringWithID(stringId, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
				case ICON_281_Name:
					strcpy(display, GetSysVerInfo_name());
					displayLen = strlen(display);
					unicode_len = 2*api_ascii_to_unicode(display,displayLen,unicode_buf);
					break;
				default:
					break;
			}
			memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
			pDisp->dispCnt++;
		}
	}

}

void MENU_007_SetGeneral_Init(int uMenuCnt)
{
	#if 0
	{
		int unicode_len;
		char unicode_buf[200];
		LIST_INIT_T listInit;
		char display[20] = {0};

		API_MenuIconDisplaySelectOn(ICON_025_general);
		API_GetOSD_StringWithIcon(ICON_025_general, unicode_buf, &unicode_len);

		API_Event_IoServer_InnerRead_All(GerneralMenuMode, (uint8*)display);

		if(atoi(display) == 1)
		{
			setGeneralIconTable = setGeneralIconTableMode1;
			setGeneralIconNum = setGeneralIconNumMode1;
		}
		else
		{
			setGeneralIconTable = setGeneralIconTableMode0;
			setGeneralIconNum = setGeneralIconNumMode0;
		}
		
		listInit = ListPropertyDefault();
		
		listInit.listType = TEXT_10X1;
		listInit.listCnt = setGeneralIconNum;
		listInit.valEn = 1;
		listInit.textValOffset = 600;
		listInit.fun = MenuListGetGeneralPage;
		listInit.titleStr = unicode_buf;
		listInit.titleStrLen = unicode_len;
		
		if(GetLastNMenu() != MENU_001_MAIN)
		{
			listInit.currentPage = setGeneralPageSelect;
		}
		
		InitMenuList(listInit);
	}
	#else
	{
		int x, y;
		uint16 monitorTime;
		char display[20];
		POS pos;
		SIZE hv;
		OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
		
		API_Event_IoServer_InnerRead_All(GerneralMenuMode, (uint8*)display);
		
		if(atoi(display) == 1)
		{
			setGeneralIconTable = setGeneralIconTableMode1;
			setGeneralIconNum = setGeneralIconNumMode1;
		}
		else if(atoi(display)==2)
		{
			setGeneralIconTable = setGeneralIconTableMode2;
			setGeneralIconNum = setGeneralIconNumMode2;
		}
		else
		{
			setGeneralIconTable = setGeneralIconTableMode0;
			setGeneralIconNum = setGeneralIconNumMode0;
		}
		
		if(GetLastNMenu() == MENU_001_MAIN)
		{
			setGeneralPageSelect = 0;
			private_code_mask=0;
		}
		
		API_MenuIconDisplaySelectOn(ICON_025_general);
		// lzh_20181016_s
		API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
		// lzh_20181016_e		
		API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_025_general, 1, 0);
		//setGeneralPageSelect = 0;
		
		setGeneralIconMax = GetListIconNum();
		//printf("GetListIconNum = %d\n", setGeneralIconMax);
		DisplaySetGeneralPageIcon(setGeneralPageSelect);
	}
	#endif
}

void MENU_007_SetGeneral_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_025_general);	
	MenuListDisable(0);
}


void MENU_007_SetGeneral_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char tempChar[30];
	char pwdplace[5];
	int i, j;
	uint16 monitorTime;
	int len, setValue;
	POS pos;
	SIZE hv;	
	int x,y;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					#if 0
					if(GeneralSet[setGeneralIconSelect+setGeneralPageSelect*setGeneralIconMax].menu != MENU_255_NONE)
					{
						if(GeneralSet[setGeneralIconSelect+setGeneralPageSelect*setGeneralIconMax].menu == MENU_042_PUBLIC_SETTING)
						{
							int i, j;
							for(i = 0; i < get_total_language_num(); i++)
							{
								get_one_language_pack_name_by_index(i, tempChar);
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_LanguageSelectTitle, get_total_language_num(), get_cur_language_index(), UpdateLanguageSet);
							StartInitOneMenu(MENU_042_PUBLIC_SETTING,0,1);
						}
						else
						{
							StartInitOneMenu(GeneralSet[setGeneralIconSelect+setGeneralPageSelect*setGeneralIconMax].menu,0,1);
						}
					}
					#else
					switch(GetCurIcon())
					{
						case ICON_041_DateAndTime:
							StartInitOneMenu(MENU_029_DATE_TIME_SET,0,1);
							break;
						case ICON_056_Language:
							InitPublicSettingMenuDisplay(GetMenuLanguageListCnt(), GetMenuLanguageListBuffer());
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_LanguageSelectTitle, GetMenuLanguageListCnt(), GetMenuLanguageIndex(), SetMenuLanguage);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_057_MonitorTime:
							for(i = 0; i < 10; i++)
							{
								snprintf(tempChar, 30, "%d(s)", 30 * (i+1));
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, tempChar);
							monitorTime = atoi(tempChar);
							monitorTime = monitorTime > 300 ? 300 : monitorTime;
							monitorTime = monitorTime < 30 ? 30 : monitorTime;
							
							EnterPublicSettingMenu(ICON_025_general, MESG_TEXT_LanguageSelectTitle, 10, monitorTime/30 - 1, UpdateMonitorTimeSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_058_SDCard:
							StartInitOneMenu(MENU_045_SD_CARD,0,1);
							break;
					}
					#endif
					break;
					
				case KEY_UP:
					//PublicListUpProcess(&setGeneralIconSelect, &setGeneralPageSelect, setGeneralIconMax, GeneralSettingNum, (DispListPage)DisplayGeneralSettingPage);
					PublicUpProcess(&setGeneralIconSelect, setGeneralIconMax);
					break;
					
				case KEY_DOWN:
					//PublicListDownProcess(&setGeneralIconSelect, &setGeneralPageSelect, setGeneralIconMax, GeneralSettingNum, (DispListPage)DisplayGeneralSettingPage);
					PublicDownProcess(&setGeneralIconSelect, setGeneralIconMax);
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&setGeneralPageSelect, setGeneralIconMax, setGeneralIconNum, (DispListPage)DisplaySetGeneralPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&setGeneralPageSelect, setGeneralIconMax, setGeneralIconNum, (DispListPage)DisplaySetGeneralPageIcon);
					break;	
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						setGeneralPageSelect = MenuListGetCurrentPage();
						
						API_Event_IoServer_InnerRead_All(GerneralPwdEnable, tempChar);
						API_Event_IoServer_InnerRead_All(GerneralPwdPlace, pwdplace);
						if(atoi(pwdplace)==1||atoi(tempChar)==0||JudgeIsInstallerMode() || API_AskDebugState(100))
						{
							switch(setGeneralIconTable[listIcon.mainIcon].iCon)
							{
								case ICON_041_DateAndTime:
									StartInitOneMenu(MENU_029_DATE_TIME_SET,0,1);
									break;
						
								case ICON_056_Language:
									InitPublicSettingMenuDisplay(GetMenuLanguageListCnt(), GetMenuLanguageListBuffer());
									EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_LanguageSelectTitle, GetMenuLanguageListCnt(), GetMenuLanguageIndex(), SetMenuLanguage);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
									break;
						
								case ICON_057_MonitorTime:
									for(i = 0; i < 10; i++)
									{
										snprintf(tempChar, 30, "%d(s)", 30 * (i+1));
										for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
										{
											publicSettingDisplay[i][j] = tempChar[j/2];
										}
									}
									API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, tempChar);
									monitorTime = atoi(tempChar);
									monitorTime = monitorTime > 300 ? 300 : monitorTime;
									monitorTime = monitorTime < 30 ? 30 : monitorTime;
									
									EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_MonitorTimeSet, 10, monitorTime/30 - 1, UpdateMonitorTimeSet);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
									break;
						
								case ICON_058_SDCard:
									StartInitOneMenu(MENU_045_SD_CARD,0,1);
									break;
								case ICON_300_ShortcutSetting:
									StartInitOneMenu(MENU_030_SHORTCUT_SET,0,1);
									break;
								case ICON_AutoCloseAfterUnlock:
									#if 0
									if(setGeneralIconTable==setGeneralIconTableMode2)
									{
										API_GetOSD_StringWithID(MESG_TEXT_Disable2, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
										publicSettingDisplay[0][0] = len;
										API_GetOSD_StringWithID(MESG_TEXT_Enable2, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
										publicSettingDisplay[1][0] = len;
									}
									else
									#endif
									{
										API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
										publicSettingDisplay[0][0] = len;
										API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
										publicSettingDisplay[1][0] = len;
									}
									
									API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, tempChar);
									setValue = atoi(tempChar);
						
									EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoCloseAfterUnlock, 2, setValue, AutoCloseAfterUnlockSet);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
									break;
								case ICON_AutoUnlock:
									API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
									publicSettingDisplay[0][0] = len;
									API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
									publicSettingDisplay[1][0] = len;
								
									EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoUnlock, 2, autoUnlock, AutoUnlockSet);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
									break;
								case ICON_281_Name:
									//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
									if(atoi(tempChar)==1)
										EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									else
										EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									break;
								case ICON_KeyBeepCtrl	:
									//API_Event_IoServer_InnerRead_All(KeyBeepDis, (uint8*)tempChar);
									
									API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
									publicSettingDisplay[0][0] = len;
									API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
									publicSettingDisplay[1][0] = len;
								
									EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_KeyBeepCtrl, 2, KeyBeepDisable, KeyBeepDisSet);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
									break;
									
							}
						}
						else
						{
							extern char installerInput[9];
								
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, GeneralSettingVerifyPassword);
						}
					}
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					
					setGeneralIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(setGeneralPageSelect*setGeneralIconMax+setGeneralIconSelect >= setGeneralIconNum)
					{
						return;
					}
					API_Event_IoServer_InnerRead_All(GerneralPwdEnable, tempChar);
					API_Event_IoServer_InnerRead_All(GerneralPwdPlace, pwdplace);
					if(atoi(pwdplace)==1||atoi(tempChar)==0||JudgeIsInstallerMode() || API_AskDebugState(100))
					{
					switch(setGeneralIconTable[setGeneralPageSelect*setGeneralIconMax+setGeneralIconSelect].iCon)
					{
						case ICON_041_DateAndTime:
							StartInitOneMenu(MENU_029_DATE_TIME_SET,0,1);
							break;

						case ICON_056_Language:
							InitPublicSettingMenuDisplay(GetMenuLanguageListCnt(), GetMenuLanguageListBuffer());
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_LanguageSelectTitle, GetMenuLanguageListCnt(), GetMenuLanguageIndex(), SetMenuLanguage);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;

						case ICON_057_MonitorTime:
							for(i = 0; i < 10; i++)
							{
								snprintf(tempChar, 30, "%d(s)", 30 * (i+1));
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, tempChar);
							monitorTime = atoi(tempChar);
							monitorTime = monitorTime > 300 ? 300 : monitorTime;
							monitorTime = monitorTime < 30 ? 30 : monitorTime;
							
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_MonitorTimeSet, 10, monitorTime/30 - 1, UpdateMonitorTimeSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;

						case ICON_058_SDCard:
							StartInitOneMenu(MENU_045_SD_CARD,0,1);
							break;
						case ICON_300_ShortcutSetting:
							StartInitOneMenu(MENU_030_SHORTCUT_SET,0,1);
							break;
						case ICON_AutoCloseAfterUnlock:
							#if 0
							if(setGeneralIconTable==setGeneralIconTableMode2)
							{
								API_GetOSD_StringWithID(MESG_TEXT_Disable2, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable2, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
							}
							else
							#endif
							{
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
							}
							
							API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, tempChar);
							setValue = atoi(tempChar);

							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoCloseAfterUnlock, 2, setValue, AutoCloseAfterUnlockSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_AutoUnlock:
							API_Event_IoServer_InnerRead_All(AutoUnlockEditEn, (uint8*)tempChar);
							if(atoi(tempChar)==1)
								return;
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
						
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_AutoUnlock, 2, autoUnlock, AutoUnlockSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_281_Name:
							#if 0
							API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
							if(atoi(tempChar)==1)
								EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
							else
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
							#endif
							API_Event_IoServer_InnerRead_All(EditUserNameDis, (uint8*)tempChar);
							if(atoi(tempChar) == 0)
							{
								API_Event_IoServer_InnerRead_All(ManagerEditUserNameDis, (uint8*)tempChar);
								if(atoi(tempChar) == 0)
								{
									//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
									if(atoi(tempChar)==1)
										EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									else
										EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
								}
							}
							else
							{
								;
							}
							break;
						case ICON_027_SipConfig:
							API_Event_IoServer_InnerRead_All(SipCfgMenuMode, (uint8*)tempChar);
							if(atoi(tempChar)==1)
							{
								API_Event_IoServer_InnerRead_All(SIP_ENABLE,tempChar);
								if(atoi(tempChar)==0)
									return;
							}
							StartInitOneMenu(MENU_009_SIP_CONFIG,0,1);
							break;	
								
						case ICON_051_PrivateUnlockCode:
							private_code_mask++;
							OSD_GetIconInfo(ICON_007_PublicList1+setGeneralIconSelect, &pos, &hv);
							x = pos.x+DISPLAY_DEVIATION_X+(hv.h - pos.x)/2;
							y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
							private_code_display(x,y);
							API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, tempChar);
							if( (strcmp(tempChar, "0") == 0) || private_code_mask >= 2 )
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_PrivateUnlockCode, tempChar, 4, COLOR_WHITE, tempChar, 1, confirm_set_private_code);
							}
							break;
						case ICON_KeyBeepCtrl	:
							//API_Event_IoServer_InnerRead_All(KeyBeepDis, (uint8*)tempChar);
							
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
						
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_KeyBeepCtrl, 2, KeyBeepDisable, KeyBeepDisSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_DivertWithMonitor:	
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_DivertWithMonitor, 2, divert_with_mon, SaveDivertWithMon);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;			
					}
				}
				else
				{
					extern char installerInput[9];
						
					EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, GeneralSettingVerifyPassword);
				}
				break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


