#include "MENU_108_QuickAccess.h"
#include "obj_SearchIxProxy.h"
#include "obj_IX_Report.h"

static SearchIxProxyRspData ixProxyData;


void MENU_108_QuickAccess_Init(int uMenuCnt)
{

}

void MENU_108_QuickAccess_Exit(void)
{

}

void MENU_108_QuickAccess_Process(void* arg)
{
	int i;
	int curIcon,CurSelectIndex;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_QuickAccess1:
					if(JudgeIfWlanDevice())
						EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
					else
						EnterInstallSubMenu(MENU_126_NM_LanSetting, 0);
					//EnterInstallSubMenu(MENU_016_INSTALL_IP, 1);
					break;	
				case ICON_QuickAccess2:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 1);
					break;	
				case ICON_QuickAccess3:
					StartInitOneMenu(MENU_105_BackupList,0,1);
					break;	
				case ICON_QuickAccess4:
					StartInitOneMenu(MENU_098_RES_Manger,0,1);
					break;	
				case ICON_QuickAccess5:
					StartInitOneMenu(MENU_087_AutotestTools,0,1);
					break;	
				case ICON_QuickAccess6:
					if(Judge_SdCardLink())
					{
						BusySpriteDisplay(1);
						creat_r8001_summary_report();
						BusySpriteDisplay(0);
						UpdateOkSpriteNotify();
					}
					else
					{
						API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0); 							
						sleep(1);
						API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
					}
					break;	
				case ICON_QuickAccess7:
					StartInitOneMenu(MENU_078_AutoCallbackList,0,1);
					break;	
				case ICON_QuickAccess8:
					StartInitOneMenu(MENU_010_ABOUT,0,1);
					break;	
				case ICON_QuickAccess9:
					StartInitOneMenu(MENU_106_RestoreList,0,1);
					break;	
				case ICON_QuickAccess10:
					EnterInstallSubMenu(MENU_066_FwUpgrade,1);
					break;	
				case ICON_QuickAccess11:
					StartInitOneMenu(MENU_095_AddressHealthCheck,0,1);
					break;	
				case ICON_QuickAccess12:
					if(Judge_SdCardLink())
					{
						BusySpriteDisplay(1);
						int ret = creat_r8001_checklist_report();
						BusySpriteDisplay(0);
						if(ret == 0)
						{
							UpdateOkSpriteNotify();
						}
						else
						{
							BEEP_ERROR();
						}
					}
					else
					{
						API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0); 							
						sleep(1);
						API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
					}
					break;	
				case ICON_QuickAccess13:
					SetSearchBdNumber(GetSysVerInfo_bd());
					SetSearchDeviceType(0);
					SetSearchMaxnum(0);
					EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 1);
					break;	
				case ICON_QuickAccess14:
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
					{
						if(!GetMSListRecordCnt() && !GetMSListTempRecordCnt())
						{
							AddTempMSlist_TableBySearching();
						}
						StartInitOneMenu(MENU_080_IM_Extensions,0,1);
					}
					break;	
				case ICON_QuickAccess15:
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
					{
						if(!GetOS_ListTempTableNum())
						{
							AddOS_ListTempTableBySearching();	
						}
						StartInitOneMenu(MENU_081_OutdoorStations,0,1);
					}
					break;	
				case ICON_QuickAccess16:
					HardwareRestar();	
					BEEP_CONFIRM();
					sleep(1);
					CloseOneMenu();
					break;	
				case ICON_QuickAccess17:
					break;	
				case ICON_QuickAccess18:
					StartInitOneMenu(MENU_097_BatCallNbrSet,0,1);
					break;	
				case ICON_QuickAccess19:
					GoHomeMenu();
					break;	
				case ICON_QuickAccess20:
					StartInitOneMenu(MENU_085_AutotestLogs,0,1);
					break;	
				case ICON_QuickAccess21:
					API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
					for(i = 0; i < ixProxyData.deviceCnt; i++)
					{
						IxReport(inet_addr(ixProxyData.data[i].ip_addr), REQUEST_TIP_REQ);
					}
					break;	
				case ICON_QuickAccess22:
					API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
					for(i = 0; i < ixProxyData.deviceCnt; i++)
					{
						IxReport(inet_addr(ixProxyData.data[i].ip_addr), REQUEST_PROG_CALL_NBR_REQ);
					}
					break;	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


