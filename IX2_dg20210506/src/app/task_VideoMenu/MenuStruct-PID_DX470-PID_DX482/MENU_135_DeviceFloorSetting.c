#include "cJSON.h"
#include "MENU_135_DeviceFloorSetting.h"
#include "obj_NoticeIxmini.h"
#include "obj_UnifiedParameterMenu.h"	

#define		DEVICE_FLOOR_SETTING_ICON_MAX		6

static LiftFloorCfg_T deviceFloorSettingData;
static int deviceFloorSettingDataItem = 0;
static int deviceFloorSettingPage = 0;
static int deviceFloorSettingType = 0;		// 0设置本地参数，1设置远程参数

const IconAndText_t deviceFloorSettingIconTable[] = 
{
	{ICON_PublicTime, 		MESG_TEXT_PublicTime},	 
	{ICON_RlcPublicTime, 	MESG_TEXT_RlcPublicTime},	 
	{ICON_PublicFloor, 		MESG_TEXT_PublicFloor},
	{ICON_PrivateTime,		MESG_TEXT_PrivateTime},
	{ICON_RlcPrivateTime,	MESG_TEXT_RlcPrivateTime},
	{ICON_PrivateFloor, 	MESG_TEXT_PrivateFloor},
//	{ICON_CallliftEn, 		MESG_TEXT_CallliftEn},
//	{ICON_CallliftTime,		MESG_TEXT_CallliftTime},
//	{ICON_RlcCallliftTime,	MESG_TEXT_RlcCallliftTime},
//	{ICON_CallliftFloor, 	MESG_TEXT_CallliftFloor},
};


const int deviceFloorSettingIconTableNum = sizeof(deviceFloorSettingIconTable)/sizeof(deviceFloorSettingIconTable[0]);
static char deviceFloorSettingInput[100] = {0};


static void DisplayDeviceFloorSettingIconTablePageIcon(int page)
{
	uint8 i, j;
	uint16 x, y,x1,y1, maxPage;
	char display[100];
	POS pos;
	SIZE hv;

	API_DisableOsdUpdate();
		
	for(i = 0; i < DEVICE_FLOOR_SETTING_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x1 = x + (hv.h - pos.x)/3;
			y1 = y + 40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x1 =x+ (hv.h - pos.x)/2;
			y1 =y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		switch(deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iCon)
		{
			case ICON_PublicTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.publicTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_RlcPublicTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.publicRlcTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			
			case ICON_PublicFloor:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				for(j = 0, display[0] = 0; j<deviceFloorSettingData.publicCnt; j++)
				{
					snprintf(deviceFloorSettingInput, 3,"%s", deviceFloorSettingData.publicFloor[j]);
					strcat(display, deviceFloorSettingInput);
					if(j != deviceFloorSettingData.publicCnt - 1)
					{
						strcat(display, ",");
					}
				}
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
				
			case ICON_PrivateTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.privateTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_RlcPrivateTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.privateRlcTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_PrivateFloor:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				for(j = 0, display[0] = 0; j<deviceFloorSettingData.privateCnt; j++)
				{
					snprintf(deviceFloorSettingInput, 3,"%s", deviceFloorSettingData.privateFloor[j]);
					strcat(display, deviceFloorSettingInput);
					if(j != deviceFloorSettingData.privateCnt - 1)
					{
						strcat(display, ",");
					}
				}
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_CallliftEn:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.callliften==0?0:1);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_CallliftTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.callliftTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_RlcCallliftTime:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", deviceFloorSettingData.callliftRlcTime);
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			
			case ICON_CallliftFloor:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, deviceFloorSettingIconTable[page*DEVICE_FLOOR_SETTING_ICON_MAX+i].iConText, 1, bkgd_w-x-10);
				for(j = 0, display[0] = 0; j<deviceFloorSettingData.callliftCnt; j++)
				{
					snprintf(deviceFloorSettingInput, 3,"%s", deviceFloorSettingData.callliftFloor[j]);
					strcat(display, deviceFloorSettingInput);
					if(j != deviceFloorSettingData.callliftCnt- 1)
					{
						strcat(display, ",");
					}
				}
				if(deviceFloorSettingData.callliftCnt>0)
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
				break;	
				
		}

	
	}

	maxPage = deviceFloorSettingIconTableNum/DEVICE_FLOOR_SETTING_ICON_MAX + (deviceFloorSettingIconTableNum%DEVICE_FLOOR_SETTING_ICON_MAX ? 1 : 0);				
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);

	API_EnableOsdUpdate();

}

static int SaveDeviceFloorSetting(void)
{
	char* tempChar;

	tempChar = CreateLiftFloorCfgJsonData(deviceFloorSettingData);
	if(tempChar != NULL)
	{
		if(deviceFloorSettingType)
		{
			SetRemoteParaValue(tempChar);
		}
		else
		{
			API_Event_IoServer_InnerWrite_All(DEVICE_FLOOR, tempChar);
		}
		free(tempChar);
	}
}

static int SavePrivateTimeSet(void)
{
	int temp;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.privateTime = temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

static int SavePublicTimeSet(void)
{
	int temp;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.publicTime = temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

static int SaveCallliftTimeSet(void)
{
	int temp;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.callliftTime= temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}
static int SaveCallliftEnSet(void)
{
	int temp;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp == 0 || temp == 1)
	{
		deviceFloorSettingData.callliften= temp;
		SaveDeviceFloorSetting();
		SetCallLiftEnable(temp);
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}
static int SavePrivateRlcTimeSet(void)
{
	int temp;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.privateRlcTime = temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

static int SavePublicRlcTimeSet(void)
{
	int temp;
	char* tempChar;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.publicRlcTime = temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}
static int SaveCallliftRlcTimeSet(void)
{
	int temp;
	char* tempChar;
	
	temp = atoi(deviceFloorSettingInput);

	if(temp >= 0 && temp <= 300)
	{
		deviceFloorSettingData.callliftRlcTime= temp;
		SaveDeviceFloorSetting();
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}
static int SavePublicFloorSet(void)
{
	char* tempChar;
	int i;
	
	tempChar = strtok(deviceFloorSettingInput,",");
	deviceFloorSettingData.publicCnt = 0;
	if(tempChar != NULL && strlen(tempChar) <= 2)
	{
		strcpy(deviceFloorSettingData.publicFloor[deviceFloorSettingData.publicCnt], tempChar); 
		deviceFloorSettingData.publicCnt++;
	}
	
	for(i= 0; (tempChar = strtok(NULL,",")) != NULL && i< NoticeIxminiUnlockMaxNum; i++)
	{
		if(strlen(tempChar) <= 2)
		{
			strcpy(deviceFloorSettingData.publicFloor[deviceFloorSettingData.publicCnt], tempChar); 
			deviceFloorSettingData.publicCnt++;
		}
	}
	
	SaveDeviceFloorSetting();
	return 1;
}

static int SavePrivateFloorSet(void)
{
	char* tempChar;
	int i;
	
	tempChar = strtok(deviceFloorSettingInput,",");
	deviceFloorSettingData.privateCnt = 0;
	if(tempChar != NULL && strlen(tempChar) <= 2)
	{
		strcpy(deviceFloorSettingData.privateFloor[deviceFloorSettingData.privateCnt], tempChar); 
		deviceFloorSettingData.privateCnt++;
	}
	
	for(i= 0; (tempChar = strtok(NULL,",")) != NULL && i< NoticeIxminiUnlockMaxNum; i++)
	{
		if(strlen(tempChar) <= 2)
		{
			strcpy(deviceFloorSettingData.privateFloor[deviceFloorSettingData.privateCnt], tempChar); 
			deviceFloorSettingData.privateCnt++;
		}
	}
	
	SaveDeviceFloorSetting();
	return 1;
}

static int SaveCallliftFloorSet(void)
{
	char* tempChar;
	int i;
	
	tempChar = strtok(deviceFloorSettingInput,",");
	deviceFloorSettingData.callliftCnt= 0;
	if(tempChar != NULL && strlen(tempChar) <= 2)
	{
		strcpy(deviceFloorSettingData.callliftFloor[deviceFloorSettingData.callliftCnt], tempChar); 
		deviceFloorSettingData.callliftCnt++;
	}
	
	for(i= 0; (tempChar = strtok(NULL,",")) != NULL && i< NoticeIxminiUnlockMaxNum; i++)
	{
		if(strlen(tempChar) <= 2)
		{
			strcpy(deviceFloorSettingData.callliftFloor[deviceFloorSettingData.callliftCnt], tempChar); 
			deviceFloorSettingData.callliftCnt++;
		}
	}
	
	SaveDeviceFloorSetting();
	return 1;
}

void InitDeviceFloorSettingData(int setType, char* settingValue)
{
	char json[500];

	deviceFloorSettingType = setType;
	if(deviceFloorSettingType)
	{
		ParseLiftFloorCfgJsonData(settingValue, &deviceFloorSettingData);
	}
	else
	{
		if(API_Event_IoServer_InnerRead_All(DEVICE_FLOOR, json)==0)
		{
			ParseLiftFloorCfgJsonData(json, &deviceFloorSettingData);
		}
		else
		{
			memset(&deviceFloorSettingData,0,sizeof(LiftFloorCfg_T));
		}
	}
}

void MENU_135_DeviceFloorSetting_Init(int uMenuCnt)
{
	ParaMenu_T record;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	GetParaMenuRecord(DEVICE_FLOOR, &record);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_LIST_COLOR, record.name, record.nameLen, 1, STR_UNICODE, 0);			

	if(GetLastNMenu() == MENU_071_ParaManage)
	{
		deviceFloorSettingPage = 0;
	}
	DisplayDeviceFloorSettingIconTablePageIcon(deviceFloorSettingPage);
}

void MENU_135_DeviceFloorSetting_Exit(void)
{

}

void MENU_135_DeviceFloorSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len, i;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					CloseMenu();
					break;
					
					case ICON_201_PageDown:
						PublicPageDownProcess(&deviceFloorSettingPage, DEVICE_FLOOR_SETTING_ICON_MAX, deviceFloorSettingIconTableNum, (DispListPage)DisplayDeviceFloorSettingIconTablePageIcon);
						break;
					case ICON_202_PageUp:
						PublicPageUpProcess(&deviceFloorSettingPage, DEVICE_FLOOR_SETTING_ICON_MAX, deviceFloorSettingIconTableNum, (DispListPage)DisplayDeviceFloorSettingIconTablePageIcon);
						break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					deviceFloorSettingDataItem = GetCurIcon() - ICON_007_PublicList1;
					switch(deviceFloorSettingIconTable[deviceFloorSettingPage * DEVICE_FLOOR_SETTING_ICON_MAX + deviceFloorSettingDataItem].iCon)
					{
						case ICON_PublicTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.publicTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PublicTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SavePublicTimeSet);
							break;	

						case ICON_RlcPublicTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.publicRlcTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcPublicTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SavePublicRlcTimeSet);
							break;	
							
						case ICON_PublicFloor:
							for(i = 0, deviceFloorSettingInput[0] = 0; i<deviceFloorSettingData.publicCnt; i++)
							{
								snprintf(display, 3,"%s", deviceFloorSettingData.publicFloor[i]);
								strcat(deviceFloorSettingInput, display);
								if(i != deviceFloorSettingData.publicCnt - 1)
								{
									strcat(deviceFloorSettingInput, ",");
								}
							}
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PublicFloor, deviceFloorSettingInput, 30, COLOR_WHITE, deviceFloorSettingInput, 1, SavePublicFloorSet);
							break;	
						case ICON_PrivateTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.privateTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PrivateTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SavePrivateTimeSet);
							break;	
						case ICON_RlcPrivateTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.privateRlcTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcPrivateTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SavePrivateRlcTimeSet);
							break;	
						case ICON_PrivateFloor:
							for(i = 0, deviceFloorSettingInput[0] = 0; i<deviceFloorSettingData.privateCnt; i++)
							{
								snprintf(display, 3,"%s", deviceFloorSettingData.privateFloor[i]);
								strcat(deviceFloorSettingInput, display);
								if(i != deviceFloorSettingData.privateCnt - 1)
								{
									strcat(deviceFloorSettingInput, ",");
								}
							}
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PrivateFloor, deviceFloorSettingInput, 30, COLOR_WHITE, deviceFloorSettingInput, 1, SavePrivateFloorSet);
							break;
							
						case ICON_CallliftEn:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.callliften);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_CallliftTime, deviceFloorSettingInput, 1, COLOR_WHITE, deviceFloorSettingInput, 1, SaveCallliftEnSet);
							break;
							
						case ICON_CallliftTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.callliftTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_CallliftTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SaveCallliftTimeSet);
							break;	

						case ICON_RlcCallliftTime:
							snprintf(deviceFloorSettingInput, 100,"%d", deviceFloorSettingData.callliftRlcTime);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RlcCallliftTime, deviceFloorSettingInput, 3, COLOR_WHITE, deviceFloorSettingInput, 1, SaveCallliftRlcTimeSet);
							break;	
							
						case ICON_CallliftFloor:
							for(i = 0, deviceFloorSettingInput[0] = 0; i<deviceFloorSettingData.callliftCnt; i++)
							{
								snprintf(display, 3,"%s", deviceFloorSettingData.callliftFloor[i]);
								strcat(deviceFloorSettingInput, display);
								if(i != deviceFloorSettingData.callliftCnt- 1)
								{
									strcat(deviceFloorSettingInput, ",");
								}
							}
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PublicFloor, deviceFloorSettingInput, 30, COLOR_WHITE, deviceFloorSettingInput, 1, SaveCallliftFloorSet);
							break;		
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}



