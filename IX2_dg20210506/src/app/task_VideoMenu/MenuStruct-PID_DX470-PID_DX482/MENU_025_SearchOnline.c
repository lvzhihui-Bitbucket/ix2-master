#include "MENU_025_SearchOnline.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "obj_DeviceRegisterTable.h"


//#define	searchOnlineIconMax	5
int searchOnlineIconMax;
int searchOnlineIconSelect;
int searchOnlinePageSelect;
SearchIpRspData searchOnlineListData;
int searchOnlineIndex;

extern DeviceInfo onlineManageCallNbrData;

char onlineManageTitle[50];
char onlineManageMFG_SN[20];
int searchOnlineIp;
char onlineManageNbr[11];

void SetOnlineManageMFG_SN(char* MFG_SN)
{
	strcpy(onlineManageMFG_SN, MFG_SN);
}

char* GetOnlineManageMFG_SN(void)
{
	return onlineManageMFG_SN;
}

void SetOnlineManageTitle(char* title)
{
	strcpy(onlineManageTitle, title);
}

char* GetOnlineManageTitle(void)
{
	return onlineManageTitle;
}

void SetSearchOnlineIp(int ip)
{
	searchOnlineIp = ip;
}

int GetSearchOnlineIp(void)
{
	return searchOnlineIp;
}

void SetOnlineManageNbr(char* nbr)
{
	strcpy(onlineManageNbr, nbr);
}

char* GetOnlineManageNbr(void)
{
	return onlineManageNbr;
}
#if 0
void SearchRoomNbrToDisplay(Type_e type, const char* BD_RM_MS, char* display)
{	
	char temp[5] = {0};
	
	if(type == TYPE_IM)
	{
		if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(BD_RM_MS, "0099", 4))
		{
			memcpy(temp, BD_RM_MS+4, 4);
			sprintf(display, "%d(%d)", atoi(temp), atoi(BD_RM_MS+8));
		}
		else
		{
			memcpy(display, BD_RM_MS, 8);
			sprintf(display+8, "(%d)", atoi(BD_RM_MS+8));
		}
	}
	else if(type == TYPE_DS)
	{
		if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(BD_RM_MS, "0099", 4))
		{
			sprintf(display, "%d", atoi(BD_RM_MS+8));
		}
		else
		{
			memcpy(display, BD_RM_MS, 4);
			sprintf(display+4, "(%d)", atoi(BD_RM_MS+8));
		}
	}
	else if(type == TYPE_OS)
	{
		sprintf(display, "%d", atoi(BD_RM_MS+8)-50);
	}
	else
	{
		strcpy(display, BD_RM_MS);
	}
}
#endif
void DisplaySearching(int cnt)
{
	char display[200];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	
	if( GetCurMenuCnt() == MENU_025_SEARCH_ONLINE)
	{
		snprintf(display, 200, "%d", cnt);
		API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
		API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_LIST_COLOR, display, strlen(display), 0, STR_UTF8, 0);			
	}
}

static void DisplaySearchOnlinePageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	char display[200];
	char room[15] = {0};
	POS pos;
	SIZE hv;
	

	if(searchOnlineListData.deviceCnt == 0)
	{
		return;
	}
	
	for(i = 0; i < searchOnlineIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		index = page*searchOnlineIconMax+i;
		if(index < searchOnlineListData.deviceCnt)
		{
			//SearchRoomNbrToDisplay(searchOnlineListData.data[index].deviceType, searchOnlineListData.data[index].BD_RM_MS, room);

			//snprintf(display, 200, "%s%s %s %03d.%03d.%03d.%03d %02d:%02d",
			//	DeviceTypeToString(searchOnlineListData.data[index].deviceType),
			//	room,
			//	searchOnlineListData.data[index].name,
			get_device_addr_and_name_disp_str(0, searchOnlineListData.data[index].BD_RM_MS, NULL, NULL, searchOnlineListData.data[index].name, room);

			snprintf(display, 200, "%s %03d.%03d.%03d.%03d %02d:%02d",
				room,
				searchOnlineListData.data[index].Ip&0xFF, (searchOnlineListData.data[index].Ip>>8)&0xFF, (searchOnlineListData.data[index].Ip>>16)&0xFF, (searchOnlineListData.data[index].Ip>>24)&0xFF,
				searchOnlineListData.data[index].systemBootTime/3600, (searchOnlineListData.data[index].systemBootTime%3600)/60);
			
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), (get_pane_type()==7)? 0:1, STR_UTF8, bkgd_w-x);			
		}
	}
	pageNum = searchOnlineListData.deviceCnt/searchOnlineIconMax + (searchOnlineListData.deviceCnt%searchOnlineIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	DisplaySearching(searchOnlineListData.deviceCnt);
}
 

void MENU_025_SearchOnline_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_031_OnsiteTools);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_SearchOnline, 2, 0);
	searchOnlineIconMax = GetListIconNum();
	
	if( GetLastNMenu() == MENU_015_INSTALL_SUB)
	{
		extern char searchAndProgBdNbr[10];
		extern Type_e searchAndProgType;
		extern int searchNum;

		searchOnlineIconSelect = 0;
		searchOnlinePageSelect = 0;
		
		BusySpriteDisplay(1);
		API_SearchIpByFilter(searchAndProgBdNbr, searchAndProgType, SearchDeviceRecommendedWaitingTime, searchNum, &searchOnlineListData);
		SearchResultSorting(&searchOnlineListData);
		BusySpriteDisplay(0);
	}
	
	DisplaySearchOnlinePageIcon(searchOnlinePageSelect);
}

void MENU_025_SearchOnline_Exit(void)
{

}

void MENU_025_SearchOnline_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&searchOnlinePageSelect, searchOnlineIconMax, searchOnlineListData.deviceCnt, (DispListPage)DisplaySearchOnlinePageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&searchOnlinePageSelect, searchOnlineIconMax, searchOnlineListData.deviceCnt, (DispListPage)DisplaySearchOnlinePageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					searchOnlineIconSelect = GetCurIcon() - ICON_007_PublicList1;
					searchOnlineIndex = searchOnlinePageSelect*searchOnlineIconMax+searchOnlineIconSelect;
					
					if(searchOnlineIndex < searchOnlineListData.deviceCnt)
					{
						//char room[15] = {0};
						char temp[200] = {0};
						//SearchRoomNbrToDisplay(searchOnlineListData.data[searchOnlineIndex].deviceType, searchOnlineListData.data[searchOnlineIndex].BD_RM_MS, room);
					
						//snprintf(temp, 200, "%s%s %s",
						//	DeviceTypeToString(searchOnlineListData.data[searchOnlineIndex].deviceType),
						//	room,
						//	searchOnlineListData.data[searchOnlineIndex].name);
						get_device_addr_and_name_disp_str(0, searchOnlineListData.data[searchOnlineIndex].BD_RM_MS, NULL, NULL, searchOnlineListData.data[searchOnlineIndex].name, temp);
						
						SetOnlineManageTitle(temp);
						SetOnlineManageMFG_SN(searchOnlineListData.data[searchOnlineIndex].MFG_SN);
						SetSearchOnlineIp(searchOnlineListData.data[searchOnlineIndex].Ip);
						SetOnlineManageNbr(searchOnlineListData.data[searchOnlineIndex].BD_RM_MS);
						EnterOnlineManageMenu(MENU_034_ONLINE_MANAGE_INFO, 1);	
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


void ProgOnlineByIp(DeviceInfo* info)
{
	if(API_ProgInfoByIp(searchOnlineIp, 2, info) == 0)
	{
		onlineManageCallNbrData = *info;
	}
	else
	{
		BEEP_ERROR();
	}
}

void EnterProgOnline(void)
{
	if(API_GetInfoByIp(searchOnlineListData.data[searchOnlineIndex].Ip, 2, &onlineManageCallNbrData) == 0)
	{	
		//StartInitOneMenu(MENU_033_PROG_ONLINE,0,1);
	}
	else
	{
		BEEP_ERROR();
	}
}

void deleteRecord(void)
{
	DeleteOneDeviceRegisterRecord(onlineManageCallNbrData.MFG_SN, 0);
}

