#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"
#include "task_ListUpdate.h"
#include "MENU_139_KeySelect.h"

//#define	nameListIconMax		5
#define ONE_LIST_MAX_LEN		50
int nameListIconMax;
int nameListIconSelect;
int nameListPageSelect;
extern one_vtk_table* nameListTable;
int nameListIndex;
int nameListState;
int nameListMaxNum = 0;		
int GlNum = 0;			//czn_20191203
char	nickName[21];			
char collectIndex[100];

IM_NameListRecord_T nameListRecord;

void StartNamelistCall(int index)	//czn_20190127
{
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;
	GetIpRspData data;
	Global_Addr_Stru target_addr;
	uint8 slaveMaster;
	uint8 intercomen;
	uint8 para_buff[20] = {0};
	char dt_addr[10];
	char dt_name[41];
	uint16 dt_addr_int;

	extern EXT_MODULE_KEY_RECORD oneExtModuleKeyRecord;
	if(GetKeyConfigListSelectFlag())
	{
		if((index+1)==nameListMaxNum)
		{
			strcpy(oneExtModuleKeyRecord.number,GetSysVerInfo_BdRmMs());
		}
		else
			GetExtKeyConfigDeviceNbr(index, oneExtModuleKeyRecord.number);
		
		AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
		SetKeyConfigListSelectFlag(0);
		popDisplayLastNMenu(2);
		return;
	}

	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	//API_Event_IoServer_InnerRead_All(IntercomEnable, (uint8*)&intercomen);
	//if(intercomen == 0)
	//{
	//	BEEP_ERROR();
	//	return;
	//}
	
	if(index < nameListMaxNum)
	{
		if(nameListState == ICON_256_Edit)
		{
			GetImCookieRecordItems(collectIndex[index], &cRecord);
			if(strstr(cRecord.BD_RM_MS,"0x")==cRecord.BD_RM_MS)
			{
				dt_addr_int = strtoul(cRecord.BD_RM_MS,NULL,0);
				if(API_CallServer_DXInvite(DxCallScene3, dt_addr_int)<0)
				{
					BEEP_ERROR();
				}
				return;
			}
			else
			{
				memcpy(para_buff, cRecord.BD_RM_MS,8);
				strcat(para_buff,"00");
			}
		}
		else if(index<(nameListMaxNum-Get_DxNamelist_Num()))
		{
			if(GetImNameListRecordCnt() == 0)
			{
				if(GetImNameListTempRecordItems(index+GlNum,	&record) != 0)
					return -1;
			}
			else
			{
				if(GetImNameListRecordItems(index+GlNum,	&record) != 0)
					return -1;
			}
			memcpy(para_buff, record.BD_RM_MS,8);
			strcat(para_buff,"00");
		}
		else
		{
			Get_DxNamelist_Record(index-(nameListMaxNum-Get_DxNamelist_Num()),dt_name,dt_addr);
			dt_addr_int = strtoul(dt_addr,NULL,0);
			if(API_CallServer_DXInvite(DxCallScene3, dt_addr_int)<0)
			{
				BEEP_ERROR();
			}
			return;
		}
	
		Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

		if(API_GetIpNumberFromNet(para_buff, NULL, NULL, 2, 8, &data) != 0)
		{
			BEEP_ERROR();
			return;
		}
		
		int i,j;
		
		data.cnt = data.cnt > MAX_CALL_TARGET_NUM ? MAX_CALL_TARGET_NUM : data.cnt;	
		for(i = 0,j=0; i < data.cnt; i++)
		{
			if(api_nm_if_judge_include_dev(data.BD_RM_MS[i],data.Ip[i])==0)
			{
				continue;
			}
			memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
			memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
			if(nameListState == ICON_256_Edit)
			{
				strcpy(target_dev[j].name, strcmp(cRecord.NickName, "-") ? cRecord.NickName: cRecord.name);
			}
			else
			{
				strcpy(target_dev[j].name, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			}
			target_dev[j].ip_addr = data.Ip[i];
			j++;
		}
		data.cnt=j;
		if(API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev) < 0)
		{
			BEEP_ERROR();
		}
		//return API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev);
		
		//return Interface_Caller_Start_Use_GlobalAddr(&target_addr);
	}
}

void GetExtKeyConfigDeviceNbr(int index, char* nbr)
{
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;

	if(index < nameListMaxNum)
	{
		if(nameListState == ICON_256_Edit)
		{
			GetImCookieRecordItems(collectIndex[index], &cRecord);
			strcpy(nbr, cRecord.BD_RM_MS);
		}
		else 
		{
			if(GetImNameListRecordCnt() == 0)
			{
				if(GetImNameListTempRecordItems(index+GlNum,	&record) != 0)
					return -1;
			}
			else
			{
				if(GetImNameListRecordItems(index+GlNum,	&record) != 0)
					return -1;
			}
			strcpy(nbr, record.BD_RM_MS);
		}
	}
}


void DisplayOneNamelist(one_vtk_table* pNameListTable, int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[ONE_LIST_MAX_LEN] = {0};
	char oneRecord[50];
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;
	POS pos;
	SIZE hv;
	char dt_addr[10];
	char dt_name[41];
	OSD_GetIconInfo(ICON_Left_list1, &pos, &hv);

	if(index < (nameListMaxNum-Get_DxNamelist_Num()))
	{
	#if 0
		char *pchar1, *pchar2;
		int len;
		
		memcpy(oneRecord, pNameListTable->precord[index].pdat, pNameListTable->precord[index].len);
		memset(recordBuffer, ' ', ONE_LIST_MAX_LEN-1);

		pchar1 = strchr(oneRecord, ',');
		recordBuffer[0] = '[';
		memcpy(recordBuffer+1, ++pchar1, 2);
		recordBuffer[3] = ']';
		memcpy(recordBuffer+4, oneRecord, pNameListTable->precord[index].len-3);
		len = 4 + pNameListTable->precord[index].len - 3;
		API_OsdStringClearExt(x, y, width, 40);
		API_OsdStringDisplayExt(x, y, color, recordBuffer, len, fnt_type, STR_UTF8, width);
	#endif

		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(index+GlNum,	&record) != 0)
				return;
		}
		else
		{
			if(GetImNameListRecordItems(index+GlNum,	&record) != 0)
				return;
		}
		strcpy(oneRecord,  strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
		if( !ImGetCookieItemByAddr(record.BD_RM_MS, &cRecord) )
		{
			if(cRecord.faverite)
			{
				API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			}
			if(strcmp(cRecord.NickName, "-"))
			{
				strcpy(oneRecord, cRecord.NickName);
			}
		}
		get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, oneRecord, recordBuffer);
		
		API_OsdStringDisplayExt(hv.h, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
	}
	else

	{
		Get_DxNamelist_Record(index-(nameListMaxNum-Get_DxNamelist_Num()),dt_name,dt_addr);
		if( !ImGetCookieItemByAddr(dt_addr, &cRecord) )
		{
			if(cRecord.faverite)
			{
				API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			}
			if(strcmp(cRecord.NickName, "-"))
			{
				strcpy(dt_name, cRecord.NickName);
			}
		}
		API_OsdStringDisplayExt(hv.h, y, color, dt_name, strlen(dt_name),fnt_type,STR_UTF8,width);
	}
}

void DisplayOnePageNamelist(uint8 page)		//czn_20191203
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;
	POS pos1;
	SIZE hv1;
	#if 0
	GlNum = GetImNamelistGLNum();
	//dh_20190823_s
	if(GetImNameListRecordCnt())//BY RES
	{
		nameListMaxNum = GetImNameListRecordCnt() + 1+Get_DxNamelist_Num(); 
	}
	else
	{
		nameListMaxNum = GetImNamelistTempTableNum()+Get_DxNamelist_Num();
	}
	//dh_20190823_e
	nameListMaxNum -=GlNum;

	if(GetKeyConfigListSelectFlag())
		nameListMaxNum+=1;
	#endif
	nameListMaxNum=Get_DxNamelist_Num(); 	
	
	for( i = 0; i < nameListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_Left_list1+i, &pos, &hv);
		OSD_GetIconInfo(ICON_Right_list1+i, &pos1, &hv1);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		API_SpriteClose(x, y, SPRITE_Collect);
		API_SpriteClose(pos1.x, y, SPRITE_NameEdit);

		list_start = page*nameListIconMax+i;
		if(list_start < (nameListMaxNum-Get_DxNamelist_Num()))
		{
			if(GetKeyConfigListSelectFlag()&&(list_start+1)==nameListMaxNum)
			{
				API_OsdUnicodeStringDisplay(hv.h, y, DISPLAY_LIST_COLOR, MESG_TEXT_CusAutoTestDevSelf, 1, bkgd_w - x);
				continue;
			}
			if(GetImNameListRecordCnt())
			{
				if(list_start == 0)
				{
					API_OsdUnicodeStringDisplay(hv.h, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ListOnline, 1, bkgd_w - x);
				}
				else
				{
					DisplayOneNamelist(NULL, list_start-1, x, y, DISPLAY_LIST_COLOR, 1, bkgd_w - x);
				}
			}
			else
			{
				DisplayOneNamelist(NULL, list_start, x, y, DISPLAY_LIST_COLOR, 1, bkgd_w - x);
			}
			API_SpriteDisplay_XY(pos1.x, y, SPRITE_NameEdit);
		}
		else if(list_start < nameListMaxNum)
		{
			DisplayOneNamelist(NULL, list_start, x, y, DISPLAY_LIST_COLOR, 1, bkgd_w - x);
			API_SpriteDisplay_XY(pos1.x, y, SPRITE_NameEdit);
			
		}
	}
	
	maxPage = nameListMaxNum/nameListIconMax + (nameListMaxNum%nameListIconMax ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

void DisplayOnePageCollect(uint8 page)
{
	int i, x, y;
	int index,maxPage;
	IM_CookieRecord_T cRecord;
	char oneRecord[50];
	POS pos;
	SIZE hv;
	POS pos1;
	SIZE hv1;
	
	nameListMaxNum = GetImCookieFaveriteNum(collectIndex);
	for( i = 0; i < nameListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_Left_list1+i, &pos, &hv);
		OSD_GetIconInfo(ICON_Right_list1+i, &pos1, &hv1);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		API_SpriteClose(x, y, SPRITE_Collect);
		API_SpriteClose(pos1.x, y, SPRITE_NameEdit);
		index = page*nameListIconMax+i;
		if(index < nameListMaxNum)
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			GetImCookieRecordItems(collectIndex[index], &cRecord);
			if(strstr(cRecord.BD_RM_MS,"0x")==cRecord.BD_RM_MS)
				strcpy(oneRecord,strcmp(cRecord.NickName, "-") ? cRecord.NickName : cRecord.name);
			else
				get_device_addr_and_name_disp_str(0, cRecord.BD_RM_MS, NULL, NULL, strcmp(cRecord.NickName, "-") ? cRecord.NickName : cRecord.name, oneRecord);
			
			API_OsdStringDisplayExt(hv.h, y, DISPLAY_LIST_COLOR, oneRecord, strlen(oneRecord),1,STR_UTF8,bkgd_w - x);
			
			API_SpriteDisplay_XY(pos1.x, y, SPRITE_NameEdit);
		}

	}
	maxPage = nameListMaxNum/nameListIconMax + (nameListMaxNum%nameListIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}
#if 0
void ModifyOneNamelist(char* pName, int roomNum, int index)
{
	char recordBuffer[200] = {0};
	one_vtk_dat record;

	if(index < nameListTable->record_cnt)
	{
		record.len = strlen(pName);
		strcpy(recordBuffer, pName);
		recordBuffer[record.len++] = ',';
		snprintf(recordBuffer + record.len, 10, "%02d", roomNum);
		record.len += 2;
		record.pdat = recordBuffer;
		Modify_one_vtk_record(&record, nameListTable, index);
	}
}
#endif
void MENU_014_NameList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_003_INTERCOM || GetLastNMenu() == MENU_140_KeyConfig)
	{
		nameListIconSelect = 0;
		nameListPageSelect = 0;
		nameListState = ICON_206_Namelist;
		API_MenuIconDisplaySelectOn(nameListState);
		//if(GetImNameListRecordCnt() == 0)
			//API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
	}
	
	nameListIconMax = GetListIconNum();
	API_MenuIconDisplaySelectOn(nameListState);

	//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, nameListState == ICON_206_Namelist  ? MESG_TEXT_Namelist : MESG_TEXT_NamelistEdit, 1, DISPLAY_TITLE_WIDTH);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, nameListState, 1, 0);
	if(nameListState == ICON_206_Namelist)
	{
		DisplayOnePageNamelist(nameListPageSelect);
	}
	else
	{
		DisplayOnePageCollect(nameListPageSelect);
	}
}

int ConfirmModifyNickName(const char* name)
{
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;
	char dt_name[41];
	char dt_addr[10];
	if(strlen(name) > 0)
	{
		if(nameListState == ICON_206_Namelist)
		{
			if(nameListIndex<(nameListMaxNum-Get_DxNamelist_Num()))
			{
				if(GetImNameListRecordCnt() == 0)
				{
					GetImNameListTempRecordItems(nameListIndex+GlNum,	&record);
				}
				else
				{
					GetImNameListRecordItems(nameListIndex - 1+GlNum,	&record);
				}
				ImGetCookieItemByAddr(record.BD_RM_MS, &cRecord);
			}
			else
			{
			
				Get_DxNamelist_Record(nameListIndex-(nameListMaxNum-Get_DxNamelist_Num()),dt_name,dt_addr);
				ImGetCookieItemByAddr(dt_addr, &cRecord);
			}
			if(nameListIndex<(nameListMaxNum-Get_DxNamelist_Num()))
				ImCookieModify(record.BD_RM_MS, cRecord.faverite, name, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			else
				ImCookieModify(dt_addr, cRecord.faverite, name, dt_name);	
		}
		else
		{
			GetImCookieRecordItems(collectIndex[nameListIndex], &cRecord);
			ImCookieModify(cRecord.BD_RM_MS, cRecord.faverite, name, cRecord.name);
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

void ModifyFavorite(void)
{
	IM_NameListRecord_T record;
	IM_CookieRecord_T cRecord;
	int x,y;
	POS pos;
	SIZE hv;
	int isCookie;
	char dt_name[41];
	char dt_addr[10];
	OSD_GetIconInfo(ICON_Left_list1+(nameListIndex%nameListIconMax), &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;

	if(nameListState == ICON_206_Namelist)
	{
		if(nameListIndex<(nameListMaxNum-Get_DxNamelist_Num()))
		{
			if(GetImNameListRecordCnt() == 0)
			{
				GetImNameListTempRecordItems(nameListIndex+GlNum, &record);
			}
			else
			{
				GetImNameListRecordItems(nameListIndex - 1+GlNum,	&record);
			}
			isCookie = ImGetCookieItemByAddr(record.BD_RM_MS, &cRecord);
		}
		else
		{
		
			Get_DxNamelist_Record(nameListIndex-(nameListMaxNum-Get_DxNamelist_Num()),dt_name,dt_addr);
			isCookie = ImGetCookieItemByAddr(dt_addr, &cRecord);
		}
		
		if( !isCookie )
		{
			if(cRecord.faverite)
			{
				API_SpriteClose(x, y, SPRITE_Collect);
			}
			else
			{
				API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			}
			if(nameListIndex<(nameListMaxNum-Get_DxNamelist_Num()))
				ImCookieModify(record.BD_RM_MS, (cRecord.faverite == 0? 1:0), NULL, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			else
				ImCookieModify(dt_addr, (cRecord.faverite == 0? 1:0), NULL, dt_name);
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			if(nameListIndex<(nameListMaxNum-Get_DxNamelist_Num()))
			{
				ImCookieModify(record.BD_RM_MS, 1, NULL, strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			}
			else
			{
				ImCookieModify(dt_addr, 1, NULL, dt_name);
			}
		}
	}
	else
	{
		GetImCookieRecordItems(collectIndex[nameListIndex], &cRecord);
		if(cRecord.faverite)
		{
			API_SpriteClose(x, y, SPRITE_Collect);
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
		}
		ImCookieModify(cRecord.BD_RM_MS, cRecord.faverite == 0? 1:0, NULL, cRecord.name);
	}
}

void MENU_014_NameList_Exit(void)
{
	SetKeyConfigListSelectFlag(0);
}

void MENU_014_NameList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					StartNamelistCall(nameListPageSelect*nameListIconMax + nameListIconSelect);
					break;
				case KEY_UP:
					PublicListUpProcess(&nameListIconSelect, &nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageNamelist);
					break;
				case KEY_DOWN:
					PublicListDownProcess(&nameListIconSelect, &nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageNamelist);
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_206_Namelist:
					if(nameListState != ICON_206_Namelist)
					{
						nameListState = ICON_206_Namelist;
						nameListPageSelect = 0;
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_Namelist, 1, DISPLAY_TITLE_WIDTH);
						API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, nameListState, 1, 0);
						API_MenuIconDisplaySelectOff(ICON_256_Edit);
						API_MenuIconDisplaySelectOn(ICON_206_Namelist);
						DisplayOnePageNamelist(nameListPageSelect);
					}
					break;
					
				case ICON_256_Edit:
					//dh_20190823_s
					if(nameListState != ICON_256_Edit)
					{
						nameListState = ICON_256_Edit;
						nameListPageSelect = 0;
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_NamelistEdit, 1, DISPLAY_TITLE_WIDTH);
						API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, nameListState, 1, 0);
						API_MenuIconDisplaySelectOff(ICON_206_Namelist);
						API_MenuIconDisplaySelectOn(ICON_256_Edit);
						DisplayOnePageCollect(nameListPageSelect);
					}
					//dh_20190823_e
					break;
					
				case ICON_short_list1:
				case ICON_short_list2:
				case ICON_short_list3:
				case ICON_short_list4:
				case ICON_short_list5:
				case ICON_short_list6:
				case ICON_short_list7:
				case ICON_short_list8:
				case ICON_short_list9:
				case ICON_short_list10:
					nameListIconSelect = GetCurIcon() - ICON_short_list1;
					nameListIndex = nameListPageSelect*nameListIconMax + nameListIconSelect;
					if(nameListIndex >= nameListMaxNum)
					{
						return;
					}

					if(nameListState == ICON_206_Namelist && GetImNameListRecordCnt())	
					{
						if(GetImNameListRecordCnt()&&nameListIndex == 0)
						{
							ClearImNamelist_Table();
							API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
							DisplayOnePageNamelist(nameListPageSelect);
						}
						else
						{
							StartNamelistCall(nameListIndex - 1);
						}
					}
					else
					{
						StartNamelistCall(nameListIndex);
					}
					break;
				case ICON_Left_list1:
				case ICON_Left_list2:
				case ICON_Left_list3:
				case ICON_Left_list4:
				case ICON_Left_list5:
				case ICON_Left_list6:
				case ICON_Left_list7:
				case ICON_Left_list8:
				case ICON_Left_list9:
				case ICON_Left_list10:
					if(GetKeyConfigListSelectFlag())
						return;
					nameListIconSelect = GetCurIcon() - ICON_Left_list1;
					nameListIndex = nameListPageSelect*nameListIconMax + nameListIconSelect;
					if(nameListIndex >= nameListMaxNum)
					{
						return;
					}
					if(nameListState == ICON_206_Namelist && GetImNameListRecordCnt())
					{
						if(nameListIndex == 0)
							return;
					}
					ModifyFavorite();
					
					break;	
				#if 1	
				case ICON_Right_list1:
				case ICON_Right_list2:
				case ICON_Right_list3:
				case ICON_Right_list4:
				case ICON_Right_list5:
				case ICON_Right_list6:
				case ICON_Right_list7:
				case ICON_Right_list8:
				case ICON_Right_list9:
				case ICON_Right_list10:
					nameListIconSelect = GetCurIcon() - ICON_Right_list1;
					nameListIndex = nameListPageSelect*nameListIconMax + nameListIconSelect;
					if(nameListIndex >= nameListMaxNum)
					{
						return;
					}
					if(nameListState == ICON_206_Namelist && GetImNameListRecordCnt())
					{
						if(nameListIndex == 0)
							return;
					}
					EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, nickName, 20, COLOR_WHITE, "-", 1, ConfirmModifyNickName);

					break;	
					#endif
				case ICON_201_PageDown:
					if(nameListState == ICON_206_Namelist)
					{
						PublicPageDownProcess(&nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageNamelist);
					}
					else
					{
						PublicPageDownProcess(&nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageCollect);
					}
					break;			
				case ICON_202_PageUp:
					if(nameListState == ICON_206_Namelist)
					{
						PublicPageUpProcess(&nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageNamelist);
					}
					else
					{
						PublicPageUpProcess(&nameListPageSelect, nameListIconMax, nameListMaxNum, (DispListPage)DisplayOnePageCollect);
					}
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_NameListUpdate:
				if(nameListState == ICON_206_Namelist)
				{
					//printf("MSG_7_BRD_SUB_NameListUpdate 999999999999\n");
					API_DisableOsdUpdate();
					DisplayOnePageNamelist(nameListPageSelect);
					API_EnableOsdUpdate();
					//printf("MSG_7_BRD_SUB_NameListUpdate aaaaaaaaaaaaaaaa\n");
				}
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


extern one_vtk_table * pvtk_gl_device_table;

int GetNamelistNums(void)
{
	if(pvtk_gl_device_table == NULL)
		return 0;

	return pvtk_gl_device_table->record_cnt;
}

int GetOneNamelistInfo(int index,int *dev_type,char *dev_name,char *dev_input,Global_Addr_Stru *dev_addr)
{
	ip_table_t ip_table_value;
	
	if(pvtk_gl_device_table == NULL)
		return -1;
	
	if(index >= pvtk_gl_device_table->record_cnt)
		return -1;
	
	if(get_ip_table_record_items(pvtk_gl_device_table,index,&ip_table_value) != 0)
		return -1;

	if(dev_type != NULL)
	{
		dev_type = ip_table_value.device_type;
	}

	if(dev_name != NULL)
	{
		strcpy(dev_name,ip_table_value.name);
	}

	if(dev_input != NULL)
	{
		strcpy(dev_input,ip_table_value.ip_input);
	}

	if(dev_addr != NULL)
	{
		dev_addr->gatewayid = ip_table_value.ipnode_id;
		dev_addr->ip = ip_table_value.ip_addr;
		dev_addr->rt = 16;
		dev_addr->code = 0;
	}
	
	return 0;
}


int SearchNamelistByInput(char *dev_input)
{
	if(pvtk_gl_device_table == NULL)
		return -1;
	
	int i;
	ip_table_t ip_table_value;
	
	for(i = 0;i < pvtk_gl_device_table->record_cnt;i ++)
	{
		if(get_ip_table_record_items(pvtk_gl_device_table,i,&ip_table_value) != 0)
			continue;
		
		if(strcmp(dev_input,ip_table_value.ip_input) == 0)
			return i;
	}

	return -1;
}

int SearchNamelistByAddr(Global_Addr_Stru dev_addr)
{
	if(pvtk_gl_device_table == NULL)
		return -1;

	int i;
	ip_table_t ip_table_value;
	
	for(i = 0;i < pvtk_gl_device_table->record_cnt;i ++)
	{
		if(get_ip_table_record_items(pvtk_gl_device_table,i,&ip_table_value) != 0)
			continue;
		
		if(dev_addr.ip == ip_table_value.ip_addr || dev_addr.gatewayid == ip_table_value.ipnode_id)
			return i;
	}

	return -1;
}

void DisplayShortcutNamelist(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[ONE_LIST_MAX_LEN] = {0};
	char oneRecord[50];
	IM_NameListRecord_T record;	
	IM_CookieRecord_T cRecord;
	char dt_name[41];
	char dt_addr[10];
	if(index < nameListMaxNum)
	{	
		if(index<(nameListMaxNum-Get_DxNamelist_Num()))
		{
			if(GetImNameListRecordCnt() == 0)
			{
				if(GetImNameListTempRecordItems(index+GlNum,	&record) != 0)
					return;
			}
			else
			{
				if(GetImNameListRecordItems(index+GlNum,	&record) != 0)
					return;
			}
			//strcpy(oneRecord,  strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
			//if( !ImGetCookieItemByAddr(record.BD_RM_MS, &cRecord) )
			//{
			//	if(strcmp(cRecord.NickName, "-"))
			//	{
			//		strcpy(oneRecord, cRecord.NickName);
			//	}
			//}
			get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, strcmp(record.R_Name, "-") ? record.R_Name : record.name1, recordBuffer);
			
			API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
		}
		else
		{
			Get_DxNamelist_Record(index-(nameListMaxNum-Get_DxNamelist_Num()),dt_name,dt_addr);
			if( !ImGetCookieItemByAddr(dt_addr, &cRecord) )
			{
				if(strcmp(cRecord.NickName, "-"))
				{
					strcpy(dt_name, cRecord.NickName);
				}
			}
			
			API_OsdStringDisplayExt(x, y, color, dt_name, strlen(dt_name),fnt_type,STR_UTF8,width);
		}
	}
}

void DisplayShortcutPageNamelist(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;
	int IconMax = GetListIconNum();
	#if 0
	GlNum = GetImNamelistGLNum();
	nameListMaxNum = GetImNameListRecordCnt(); 

	if(nameListMaxNum == 0)
	{
		nameListMaxNum = GetImNamelistTempTableNum();
	}
	nameListMaxNum -=GlNum;
	nameListMaxNum+=Get_DxNamelist_Num(); 
	#endif
	nameListMaxNum=Get_DxNamelist_Num(); 
	list_start = page*IconMax;
	
	for( i = 0; i < IconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		API_SpriteClose(x, y, SPRITE_WIFI_SELECT);
		if(list_start+i < nameListMaxNum)
		{
			DisplayShortcutNamelist(list_start+i, x+DISPLAY_DEVIATION_X, y, DISPLAY_LIST_COLOR, 1, hv.h - x);
		}
	}
	
	maxPage = nameListMaxNum/IconMax + (nameListMaxNum%IconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
}

void QuickNamelistCall(char* rm, char* name)
{
	GetIpRspData data;
	Global_Addr_Stru target_addr;
	uint8 intercomen;
	uint8 para_buff[20] = {0};
	API_Event_IoServer_InnerRead_All(IntercomEnable, para_buff);
	if(atoi(para_buff) == 0)
	{
		BEEP_ERROR();
		return;
	}
	memset(para_buff,0,20);
	if(atoi(rm+8)>=51)
	{
		memcpy(para_buff, rm,10);
	}
	else
	{
		memcpy(para_buff, rm,8);
		strcat(para_buff,"00");
	}

	Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

	if(API_GetIpNumberFromNet(para_buff, NULL, NULL, 2, 8, &data) != 0)
	{
		BEEP_ERROR();
		return;
	}
	
	int i,j;
	
	data.cnt = data.cnt > MAX_CALL_TARGET_NUM ? MAX_CALL_TARGET_NUM : data.cnt;	
	for(i = 0,j=0; i < data.cnt; i++)
	{
		if(api_nm_if_judge_include_dev(data.BD_RM_MS[i],data.Ip[i])==0)
		{
			continue;
		}
		memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
		memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
		target_dev[j].ip_addr = data.Ip[i];

		//get_device_addr_and_name_disp_str(0, data.BD_RM_MS[i], NULL, NULL, name, target_dev[i].name);
		strcpy(target_dev[j].name, name);
		j++;
	}
	data.cnt=j;
	if(API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev) < 0)
	{
		BEEP_ERROR();
	}
}

void QuickDXNamelistCall(char* rm, char* name)
{
	uint8 para_buff[20] = {0};
	int dt_addr; 
	API_Event_IoServer_InnerRead_All(IntercomEnable, para_buff);
	if(atoi(para_buff) == 0)
	{
		BEEP_ERROR();
		return;
	}
	
	//dt_addr = strtoul(rm,NULL,0);
	dt_addr = atoi(rm);
	if(API_CallServer_DXInvite(DxCallScene3, dt_addr)<0)
	{
		BEEP_ERROR();
	}
}