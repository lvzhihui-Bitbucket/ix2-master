#include "MENU_public.h"
#include "task_Ring.h"
#include "task_Power.h"
#include "task_CallServer.h"	//czn_20170518
#include "task_monitor.h"
#include "cJSON.h"
//#include "../../device_manage/obj_device_register/obj_device_register.h"

static int DoorBell_State = 0;
extern monitor_sbu	one_monitor;


void MENU_048_DoorBell_Init(int uMenuCnt)
{
	if(DoorBell_State == 0)
	{
		popDisplayLastMenu();
		return;
	}
	
	API_TimeLapseStart(COUNT_RUN_UP,0);
}

void MENU_048_DoorBell_Exit(void)
{
	Api_DoorBell_Stop();
}

void MENU_048_DoorBell_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int y;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_CONNECTING:
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
			case MSG_7_BRD_SUB_WIFI_OPEN:
			case MSG_7_BRD_SUB_WIFI_CLOSE:
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
				
				break;
				
			case MSG_7_BRD_SUB_WIFI_SEARCHING:
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCH_OVER:
				break;
			case MSG_7_BRD_SUB_RING_STOP:
				Api_DoorBell_Stop();
				popDisplayLastMenu();
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

void Send_DoorBellCmd_ToDxSlave(void)
{
/*

	int dxslave_num,i;
	DevReg_Info dev_info;
	
	dxslave_num = Api_Get_RegDxSlaveNum();
	
	for(i = 0;i < dxslave_num;i++)
	{
		if(Api_Get_OneRegDxSlaveItem(i,&dev_info) == 0)
		{
			uint8 data = 0;
			api_udp_c5_ipc_send_data_without_ack(dev_info.ip_addr,CMD_DOORBELL_REQ,(char*)&data,1);
		}
	}

*/
	
}
void StartDoorBellVideo(void);
void Api_DoorBell_LocalTouch(void)
{
	if(CallServer_Run.state != CallServer_Wait || one_monitor.state != MONITOR_IDLE )
	{
		DoorBell_State = 0;
		return;
	}
	DoorBell_State = 1;
	API_POWER_EXT_RING_OFF();
	if(RingGetState()==0);
		API_RingPlay(RING_Doorbell);
	API_POWER_EXT_RING_ON();		//czn_20170809
	Send_DoorBellCmd_ToDxSlave();
	StartDoorBellVideo();
/*
	uint8 slaveMaster;
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	if(slaveMaster == 0)
	{	
		if(CallServer_Run.state != CallServer_Wait || Mon_Run.state != MON_STATE_OFF)
		{
			DoorBell_State = 0;
			return;
		}
		
		DoorBell_State = 1;
		API_POWER_EXT_RING_OFF();
		API_RingPlay(RING_Doorbell);
		API_POWER_EXT_RING_ON();		//czn_20170809
		Send_DoorBellCmd_ToDxSlave();
	}
*/
}

void Api_DoorBell_RemoteTouch(void)
{

/*
	if(CallServer_Run.state != CallServer_Wait || Mon_Run.state != MON_STATE_OFF)
	{
		DoorBell_State = 0;
		return;
	}
	
	DoorBell_State = 1;
	API_RingPlay(RING_Doorbell);

*/
}

void Api_DoorBell_Stop(void)
{
	DoorBell_State = 0;
	if(CallServer_Run.state != CallServer_Wait || one_monitor.state != MONITOR_IDLE)
	{
		return;
	}
	API_RingStop();
	API_POWER_EXT_RING_OFF();		//czn_20170809

}
#if 1
int ReadDoorBellVideo(char *ip,char *name)
{
	char buff[500];
	cJSON *root;
	int type=0;
	if(API_Event_IoServer_InnerRead_All(DoorBellVideoSource, buff)==0)
	{
		root=cJSON_Parse(buff);
		if(root!=NULL)
		{
			ParseJsonString(root, "TYPE", buff, 40);
			if(strcmp(buff,"IPC")==0)
			{
				ParseJsonString(root, "NAME", name, 40);
				ParseJsonString(root, "IP", ip, 100);
				type=2;
			}
			else if(strcmp(buff,"IX")==0)
			{
				ParseJsonString(root, "NAME", name, 40);
				ParseJsonString(root, "bdRmMs", ip, 11);
				type=1;
			}
			else if(strcmp(buff,"NONE")==0)
			{
				type=0;
			}
			cJSON_Delete(root);
		}
	}
	return type;
}
void WriteDoorBellVideo(int type,char *ip,char *name)
{
	cJSON *root;
	root =cJSON_CreateObject();
	if(root==NULL)
		return;
	if(type==1)
	{
		cJSON_AddStringToObject(root,"TYPE","IX");
		cJSON_AddStringToObject(root,"bdRmMs",ip);
		cJSON_AddStringToObject(root,"NAME",name);
	}
	else if(type==2)
	{
		cJSON_AddStringToObject(root,"TYPE","IPC");
		cJSON_AddStringToObject(root,"IP",ip);
		cJSON_AddStringToObject(root,"NAME",name);
	}
	else
	{
		cJSON_AddStringToObject(root,"TYPE","NONE");
	}
	char *cstring=cJSON_Print(root);
	cJSON_Delete(root);
	if(cstring!=NULL)
	{
		API_Event_IoServer_InnerWrite_All(DoorBellVideoSource, cstring);
		free(cstring);
	}
	
}
void StartDoorBellVideo(void)
{
	char buff[500];
	char name[41];
	char ip[101];
	//char bdRmMs[11];
	int type;
	//static time_t save_t;
	//save_t=
	type=ReadDoorBellVideo(ip,name);
	
	if(type==2&&GetCurMenuCnt()!=MENU_056_IPC_MONITOR)
	{
		IpcShortcutMon(ip, name);
	}
	else if(type==1&&GetCurMenuCnt()!=MENU_028_MONITOR2)
	{
		if(Api_Ds_Show(0, 0, ip, name) == 0)
		{
			EnterDSMonMenu(0, 1);
		}
	}
	else if(type==0)
	{
		;
	}
}

void DoorBellVideoSet(int sel)
{
	int ipcNum = GetIpcNum()+GetWlanIpcNum();
	int monres_num;
	int index;
	char bdRmMs[11];
	int deviceType;
	char name_temp[41];
	IPC_ONE_DEVICE dat;
	if(sel==0)
	{
		WriteDoorBellVideo(0,NULL,NULL);
		return;
	}
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		monres_num = Get_MonRes_Temp_Table_Num() + ipcNum;
		printf("DisplayShortcutPageMonlist num: %d\n",monres_num);
	}
	else
	{
		monres_num = Get_MonRes_Num() + ipcNum;
	}
	index=sel-1;
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		//monResNum = Get_MonRes_Temp_Table_Num();
		
		if(index < ipcNum)
		{
			if(index < GetIpcNum())
				GetIpcCacheRecord(index, &dat);
			else
				GetWlanIpcRecord(index-GetIpcNum(), &dat);
			WriteDoorBellVideo(2,dat.IP,dat.NAME);
			
		}
		else if(index < monres_num)
		{
			if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
			WriteDoorBellVideo(1,bdRmMs,name_temp);
		}
	}
	else
	{
		//monResNum = Get_MonRes_Num();
		if(index < ipcNum)
		{
			if(index < GetIpcNum())
				GetIpcCacheRecord(index, &dat);
			else
				GetWlanIpcRecord(index-GetIpcNum(), &dat);
			WriteDoorBellVideo(2,dat.IP,dat.NAME);
		}
		else if(index < monres_num)
		{
			if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
			WriteDoorBellVideo(1,bdRmMs,name_temp);
		}
	}
}


int LoadDoorBellVideoPublicSetting(int *select,int *max)
{
	int ipcNum = GetIpcNum()+GetWlanIpcNum();
	int monres_num;
	int index;
	char display[100];
	IPC_ONE_DEVICE dat;
	char name_temp[41];
	char name_temp2[41];
	char bdRmMs[11];
	int deviceType;
	int type;
	char name[41];
	char ip[101];
	short buff16[50];
	int sel=0;
	type=ReadDoorBellVideo(ip,name);
	if(type==0)
		sel=0;
	//if(GetIpcListUpdateFlag())
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		monres_num = Get_MonRes_Temp_Table_Num() + ipcNum;
		printf("DisplayShortcutPageMonlist num: %d\n",monres_num);
	}
	else
	{
		monres_num = Get_MonRes_Num() + ipcNum;
	}

	API_GetOSD_StringWithID(MESG_TEXT_ProxyIpcCancel,NULL,0, NULL,0, publicSettingDisplay[0]+1, &index);
	//list_start = page*IconMax;
	publicSettingDisplay[0][0]=index;
	//memcpy(&publicSettingDisplay[0][1],buff16,publicSettingDisplay[0][0]);
	int i;
	for(i=0;i<16;i++)
	{
		printf("%02x ",publicSettingDisplay[0][i]);
	}
	printf("11111111\n");
	for(index = 0; index < monres_num&&index<29; index++ )
	{
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			//monResNum = Get_MonRes_Temp_Table_Num();
			
			if(index < ipcNum)
			{
				if(index < GetIpcNum())
					GetIpcCacheRecord(index, &dat);
				else
					GetWlanIpcRecord(index-GetIpcNum(), &dat);
				if(type==2&&strcmp(dat.IP,ip)==0)
				{
					sel=index+1;
				}
				
				snprintf(display, 51, "[IPC]%s", dat.NAME);
				if(GetMonFavByAddr(NULL, dat.NAME, name_temp2) == 1)
				{
					snprintf(display, 51, "[IPC]%s", strcmp(name_temp2, "-") ? name_temp2 : dat.NAME);
				}
				publicSettingDisplay[index+1][0]=utf82unicode(display,strlen(display),(short*)buff16)*2;
				memcpy(&publicSettingDisplay[index+1][1],buff16,publicSettingDisplay[index+1][0]);
			}
			else if(index < monres_num)
			{
				if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
					continue;
				if(type==1&&strcmp(bdRmMs,ip)==0)
				{
					sel=index+1;
				}
				if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
				{
					strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
				}
				
				get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
				publicSettingDisplay[index+1][0]=utf82unicode(display,strlen(display),(short*)buff16)*2;
				memcpy(&publicSettingDisplay[index+1][1],buff16,publicSettingDisplay[index+1][0]);
			}
		}
		else
		{
			//monResNum = Get_MonRes_Num();
			if(index < ipcNum)
			{
				if(index < GetIpcNum())
					GetIpcCacheRecord(index, &dat);
				else
					GetWlanIpcRecord(index-GetIpcNum(), &dat);
				if(type==2&&strcmp(dat.IP,ip)==0)
				{
					sel=index+1;
				}
				snprintf(display, 51, "[IPC]%s", dat.NAME);
				
				publicSettingDisplay[index+1][0]=utf82unicode(display,strlen(display),(short*)buff16)*2;
				memcpy(&publicSettingDisplay[index+1][1],buff16,publicSettingDisplay[index+1][0]);
			}
			else if(index < monres_num)
			{
				if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
					continue;
				if(type==1&&strcmp(bdRmMs,ip)==0)
				{
					sel=index+1;
				}
				
				get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
				publicSettingDisplay[index+1][0]=utf82unicode(display,strlen(display),(short*)buff16)*2;
				memcpy(&publicSettingDisplay[index+1][1],buff16,publicSettingDisplay[index+1][0]);
			}
		}
		for(i=0;i<16;i++)
		{
			printf("%02x ",publicSettingDisplay[index+1][i]);
		}
		printf("2222222==%d\n",index+1);
	}
	*select=sel;
	*max=index+1;
	return 0;
	//EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_KeyBeepCtrl, index+1, sel, DoorBellVideoSet);
}
#endif
