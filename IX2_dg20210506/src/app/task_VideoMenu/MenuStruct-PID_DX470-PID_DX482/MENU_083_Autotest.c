#include "MENU_public.h"
#include "MENU_083_Autotest.h"
#include "obj_R8001_Table.h"

#define	SetAutotest_ICON_MAX	5
int setAutotestIconSelect;
int setAutotestPageSelect;
int setAutotestPassword = 0;
char AutotestInput[11];
char AutotestSourceNum[11]={"0099990101"};
char AutotestSourceDisp[20]={"DS1"};
char AutotestTargetNum[11]={0};
char AutotestTargetDisp[20]={"Self"};
int test_count = 1;
int test_interval = 3;

int Autotest_test_count_Verify(char *buff);
int Autotest_test_interval_Verify(char *buff);
int Autotest_test_target_Verify(char *buff);
void AutotestSource_Select(void);
void AutotestTarget_Select(void);

const SetAutotestIcon setAutotestIconTable[] = 
{
	{ICON_AutotestSource,			MESG_TEXT_ICON_AutotestSource},    
	{ICON_AutotestTarget,			MESG_TEXT_ICON_AutotestTarget},
	{ICON_AutotestCount,			MESG_TEXT_ICON_AutotestCount},
	{ICON_AutotestInterval,		MESG_TEXT_ICON_AutotestInterval},	
	{ICON_AutotestStart,			MESG_TEXT_ICON_AutotestStart},
	{ICON_AutotestLogClear, 		MESG_TEXT_ICON_AutotestLogClear},
	{ICON_ViewAutotestLog, 	MESG_TEXT_ICON_ViewAutotestLog},
	{ICON_AutotestLogCopy, 		MESG_TEXT_ICON_AutotestLogCopy},
};

const int setAutotestIconNum = sizeof(setAutotestIconTable)/sizeof(setAutotestIconTable[0]);

static void DisplayAutotestPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	// lzh_20181016_s	
	API_DisableOsdUpdate();
	// lzh_20181016_e
	
	for(i = 0; i < SetAutotest_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*SetAutotest_ICON_MAX+i < setAutotestIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setAutotestIconTable[i+page*SetAutotest_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(setAutotestIconTable[i+page*SetAutotest_ICON_MAX].iCon)
			{
				case ICON_AutotestSource:
					if(strcmp(AutotestSourceDisp,"Self")==0)		//FOR_INDEXA
					{
						API_OsdUnicodeStringDisplay(x,y,DISPLAY_STATE_COLOR,MESG_TEXT_CusAutoTestDevSelf,0,hv.h-x);
					}
					else
					{
						strcpy(display, AutotestSourceDisp);
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 0, STR_UTF8, hv.h-x);
					}
					break;
				
				case ICON_AutotestTarget:
					strcpy(display, AutotestTargetDisp);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;

				case ICON_AutotestCount:
					sprintf(display,"[%d]",test_count);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;

				case ICON_AutotestInterval:
					sprintf(display,"[%dmin]",test_interval);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;	
					
			}
		}
	}
	pageNum = setAutotestIconNum/SetAutotest_ICON_MAX + (setAutotestIconNum%SetAutotest_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	// lzh_20181016_s	
	API_EnableOsdUpdate();
	// lzh_20181016_e	
}


void MENU_083_Autotest_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(AutotestTargetNum[0] == 0)
	{
		strcpy(AutotestSourceNum,"0099990101");
		strcpy(AutotestSourceDisp,"DS1");
		strcpy(AutotestTargetNum,GetSysVerInfo_BdRmMs());
		strcpy(AutotestTargetDisp,"Self");
	}
	setAutotestIconSelect = 0;
	setAutotestPageSelect = 0;
	//API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	// lzh_20181016_s
	//API_OsdStringClearExt(DISPLAY_TITLE_X,DISPLAY_TITLE_Y,DISPLAY_TITLE_WIDTH,CLEAR_STATE_H);
	// lzh_20181016_e		
	//API_OsdUnicodeStringDisplayWithIcon(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, ICON_026_InstallerSetup, 1, DISPLAY_TITLE_WIDTH);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_Autotest,1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	DisplayAutotestPageIcon(setAutotestPageSelect);
}

void MENU_083_Autotest_Exit(void)
{
	//API_MenuIconDisplaySelectOff(ICON_026_InstallerSetup);
}

void MENU_083_Autotest_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[10];
	
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					setAutotestIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(setAutotestPageSelect*SetAutotest_ICON_MAX+setAutotestIconSelect >= setAutotestIconNum)
					{
						return;
					}
					
					
					if(/*setInstallerPassword*/1)
					{
						switch(setAutotestIconTable[setAutotestPageSelect*SetAutotest_ICON_MAX+setAutotestIconSelect].iCon)
						{
							case ICON_AutotestSource:
								AutotestSource_Select();
								break;

							case ICON_AutotestTarget:
								AutotestTarget_Select();
								//sprintf(AutotestInput,"%d",test_count);
								//strcpy(AutotestInput,AutotestTargetNum);
								//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_AutotestTarget, AutotestInput, 10, COLOR_WHITE, AutotestInput, 1, Autotest_test_target_Verify);
								break;
								
							case ICON_AutotestCount:
								sprintf(AutotestInput,"%d",test_count);
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_AutotestCount, AutotestInput, 4, COLOR_WHITE, AutotestInput, 1, Autotest_test_count_Verify);
								break;
								
							case ICON_AutotestInterval:
								sprintf(AutotestInput,"%d",test_interval);
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_AutotestCount, AutotestInput, 2, COLOR_WHITE, AutotestInput, 1, Autotest_test_interval_Verify);
								break;
								
							case ICON_AutotestStart:
								if(!strcmp(AutotestTargetNum,"By R8001"))
								{
									if(get_r8001_record_cnt())
									{
										API_Start_AutoTestByTable(0,test_count,test_interval*60,AutotestSourceNum,get_r8001_record_cnt());
										StartInitOneMenu(MENU_086_CallTestStat,0,1);
									}
								}
								else
								{
									API_Start_AutoTestByRmNum(0,test_count,test_interval*60,AutotestSourceNum,AutotestTargetNum);
									StartInitOneMenu(MENU_086_CallTestStat,0,1);
								}
								break;

						}
					}
					else
					{
						//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 4, COLOR_WHITE, NULL, 0, VerifyPassword);
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//setInstallerPageSelect = 0;
		//DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}

int Autotest_test_count_Verify(char *buff)
{
	int i;
	
	int len = strlen(buff);
	
	for(i = 0;i < len;i++)
	{
		if(buff[i] < '0' ||buff[i] > '9')
			return 0;
	}
	test_count = atoi(buff);
	return 1;
}

int Autotest_test_interval_Verify(char *buff)
{
	int i;
	
	int len = strlen(buff);
	
	for(i = 0;i < len;i++)
	{
		if(buff[i] < '0' ||buff[i]  > '9')
			return 0;
	}
	test_interval = atoi(buff);
	return 1;
}

int Autotest_test_target_Verify(char *buff)
{
	int i;
	
	int len = strlen(buff);
	
	if(len!=10)
		return 0;
	
	for(i = 0;i < len;i++)
	{
		if(buff[i] < '0' ||buff[i] > '9')
			return 0;
	}
	strcpy(AutotestTargetNum,buff);
	if(strcmp(AutotestTargetNum,GetSysVerInfo_BdRmMs())==0)
	{
		strcpy(AutotestTargetDisp,"Self");
	}
	else
	{
		buff[8] = 0;
		i = atoi(buff+4);
		sprintf(AutotestTargetDisp,"IM%d",i);
	}
	return 1;
}

int AutotestDevSelect_type = 0;

void AutotestTarget_Select(void)
{
	AutotestDevSelect_type = 1;
	StartInitOneMenu(MENU_084_AutotestSource,0,1);
}

void AutotestSource_Select(void)
{
	AutotestDevSelect_type = 0;
	StartInitOneMenu(MENU_084_AutotestSource,0,1);
}
int get_AutotestDevSelect_type(void)
{
	return AutotestDevSelect_type;
}



void AutotestDev_Select_Update(char *dev_rm_num,char *dev_disp)
{
	if(get_AutotestDevSelect_type() == 0)
	{
		strcpy(AutotestSourceNum,dev_rm_num);
		strcpy(AutotestSourceDisp,dev_disp);
	}
	else
	{
		strcpy(AutotestTargetNum,dev_rm_num);
		strcpy(AutotestTargetDisp,dev_disp);
	}
}
