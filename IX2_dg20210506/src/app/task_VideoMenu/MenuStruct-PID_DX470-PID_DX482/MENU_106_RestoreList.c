#include "MENU_public.h"
#include "MENU_106_RestoreList.h"
#include "obj_BackupAndRestore.h"
#include "obj_GetIpByNumber.h"

#define	RESTORE_LIST_ICON_MAX		6


static int restoreListIconSelect;
static int restoreListPageSelect;
static int restoreListConfirm;
static int restoreListConfirmSelect;
static int restoreListIconNum;
static char restorefromRoomNumber[11]={0};

static void MenuListGetRestoreListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int unicode_len;
	char unicode_buf[200];
	int stringId;
	
	pDisp->dispCnt = 0;
	
	restoreListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < restoreListIconNum)
		{
			switch(index)
			{
				case 0:
					API_GetOSD_StringWithID(MESG_TEXT_RestoreFromOtherDevice, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					pDisp->disp[pDisp->dispCnt].valLen = 2*api_ascii_to_unicode(restorefromRoomNumber, strlen(restorefromRoomNumber), pDisp->disp[pDisp->dispCnt].val);
					break;
				
				case 1:
					API_GetOSD_StringWithID(MESG_TEXT_InstallerBak1, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
			}
			
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;			
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->dispCnt++;
		}
	}
}


void MENU_106_RestoreList_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;

	restoreListIconSelect = 0;
	restoreListPageSelect = 0;
	restoreListConfirm = 0;
	restoreListConfirmSelect = 0;

	if(GetLastNMenu() == MENU_104_BackupAndRestore2 || GetLastNMenu() == MENU_108_QuickAccess)
	{
		strcpy(restorefromRoomNumber, GetSysVerInfo_BdRmMs());
	}

	restoreListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);
	API_GetOSD_StringWithID(MESG_TEXT_ICON_RestoreFromBackup, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
	
	listInit = ListPropertyDefault();
	
	listInit.ediEn = 1;
	listInit.valEn = 1;
	if( get_pane_type() == 7 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 120;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 300;
	}	
	listInit.listCnt = restoreListIconNum;
	listInit.fun = MenuListGetRestoreListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.currentPage = 0;

	InitMenuList(listInit);
}

void MENU_106_RestoreList_Exit(void)
{
	MenuListDisable(0);
}

void MENU_106_RestoreList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					ClearConfirm(&restoreListConfirm, &restoreListConfirmSelect, RESTORE_LIST_ICON_MAX);
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon > MENU_LIST_ICON_NONE)
					{
						restoreListIconSelect = listIcon.mainIcon % RESTORE_LIST_ICON_MAX;
						
						switch(listIcon.mainIcon)
						{
							case 0:
								if(listIcon.subIcon == 2)
								{
									ClearConfirm(&restoreListConfirm, &restoreListConfirmSelect, RESTORE_LIST_ICON_MAX);
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RestoreFromOtherDevice, restorefromRoomNumber, 10, COLOR_WHITE, restorefromRoomNumber, 1, NULL);
								}
								else
								{
									if(ConfirmSelect(&restoreListConfirm, &restoreListConfirmSelect, &restoreListIconSelect, 0, RESTORE_LIST_ICON_MAX))
									{
										return;
									}
									ClearConfirm(&restoreListConfirm, &restoreListConfirmSelect, RESTORE_LIST_ICON_MAX);

									BusySpriteDisplay(1);
									if(!strcmp(restorefromRoomNumber, GetSysVerInfo_BdRmMs()))
									{
										if(RestoreBak(1) == 0)
										{
											BEEP_CONFIRM();
											UpdateOkAndWaitingForReboot();
										}
										else
										{
											BEEP_ERROR();
										}
									}
									else
									{
										GetIpRspData data = {0};
										API_GetIpNumberFromNet(restorefromRoomNumber, NULL, NULL, 2, 1, &data);
										if(data.cnt)
										{
											if(RestoreByOtherDeviceBak1(data.Ip[0]))
											{
												BEEP_CONFIRM();
												UpdateOkAndWaitingForReboot();
											}
											else
											{
												BEEP_ERROR();
											}
										}
										else
										{
											BEEP_ERROR();
										}
									}
									BusySpriteDisplay(0);
								}
								break;
								
							case 1:
								if(ConfirmSelect(&restoreListConfirm, &restoreListConfirmSelect, &restoreListIconSelect, 0, RESTORE_LIST_ICON_MAX))
								{
									return;
								}
								ClearConfirm(&restoreListConfirm, &restoreListConfirmSelect, RESTORE_LIST_ICON_MAX);
								BusySpriteDisplay(1);
								if(RestoreBak(1) == 0)
								{
									BEEP_CONFIRM();
									UpdateOkAndWaitingForReboot();
								}
								else
								{
									BEEP_ERROR();
								}
								BusySpriteDisplay(0);
								break;
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




