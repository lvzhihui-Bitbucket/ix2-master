
#ifndef _MENU_125_H
#define _MENU_125_H

#include "MENU_public.h"

#define LIFT_CONTROLER_MAX_NUM				8
#define ONE_LIFT_CONTROLER_RELAY_NUM		14
#define IXMINI_FLOOR_MAX_NUM				(ONE_LIFT_CONTROLER_RELAY_NUM * LIFT_CONTROLER_MAX_NUM)

typedef struct 
{
	int 		floorCnt;
	char 		floorNbr[IXMINI_FLOOR_MAX_NUM][3];
}IXMiniRelayMap_T;


#endif


