#include "MENU_public.h"
#include "obj_IPCTableSetting.h"

//#define	ipcListIconMax			5
int ipcListIconMax;

int ipcListIconSelect;
int ipcListPageSelect;
int ipcListState;
int ipcListIndex;
char ipcListName[41];
IPC_RECORD saveIPCMonList; 
#if 0
one_vtk_table* ipcMonListTable = NULL;
#endif
int deleteConfirm;
int ipcListLastIconSelect;
int ipcSelectForCh;
extern int recIpcIndex[4];

void StartIPC_MonlistMonitor(int index)
{
#if 0	
	char rtsp_url[240];
	int	 main_ch,width,height,rate;

	onvif_discovery_result_list_set_avtive(index);
	
	if( onvif_discovery_result_list_get_active_url( rtsp_url, &main_ch, &width, &height, &rate ) < 0 )
		return;

	printf("onvif discovery list result cur = %d, chanel=%s, rtsp_url=%s ......\n",onvif_discovery_result_list_get_active(),main_ch?"main":"sub",rtsp_url);
	
	api_onvif_start_mointor_one_stream(0, rtsp_url,main_ch, width, height, rate );	
#endif	
}

#if 0
void IPC_MonRes_Table_Init(void)
{
	char temp[20];
	
	API_Event_IoServer_InnerRead_All(IPC_DHCP_Enable, temp);
	ipcDhcpEnable = atoi(temp) ? 1 : 0;
	
	ipcMonListTable = load_vtk_table_file(IPC_MON_LIST_FILE_NAME);
	if(ipcMonListTable == NULL)
	{
		ipcMonListTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &ipcMonListTable->lock, 0);		//czn_20190327
		ipcMonListTable->keyname_cnt = 7;
		ipcMonListTable->pkeyname_len = malloc(ipcMonListTable->keyname_cnt*sizeof(int));
		ipcMonListTable->pkeyname = malloc(ipcMonListTable->keyname_cnt*sizeof(one_vtk_dat));
		ipcMonListTable->pkeyname_len[0] = 40;
		ipcMonListTable->pkeyname_len[1] = 40;
		ipcMonListTable->pkeyname_len[2] = 40;
		ipcMonListTable->pkeyname_len[3] = 1;
		ipcMonListTable->pkeyname_len[4] = 100;
		ipcMonListTable->pkeyname_len[5] = 40;
		ipcMonListTable->pkeyname_len[6] = 1;
		ipcMonListTable->pkeyname[0].len = strlen("IPC_NAME");
		ipcMonListTable->pkeyname[0].pdat = malloc(ipcMonListTable->pkeyname[0].len);
		memcpy(ipcMonListTable->pkeyname[0].pdat, "IPC_NAME", ipcMonListTable->pkeyname[0].len);
		
		ipcMonListTable->pkeyname[1].len = strlen("USER_NAME");
		ipcMonListTable->pkeyname[1].pdat = malloc(ipcMonListTable->pkeyname[1].len);
		memcpy(ipcMonListTable->pkeyname[1].pdat, "USER_NAME", ipcMonListTable->pkeyname[1].len);

		ipcMonListTable->pkeyname[2].len = strlen("USER_PWD");
		ipcMonListTable->pkeyname[2].pdat = malloc(ipcMonListTable->pkeyname[2].len);
		memcpy(ipcMonListTable->pkeyname[2].pdat, "USER_PWD", ipcMonListTable->pkeyname[2].len);

		ipcMonListTable->pkeyname[3].len = strlen("CHANNEL");
		ipcMonListTable->pkeyname[3].pdat = malloc(ipcMonListTable->pkeyname[3].len);
		memcpy(ipcMonListTable->pkeyname[3].pdat, "CHANNEL", ipcMonListTable->pkeyname[3].len);

		ipcMonListTable->pkeyname[4].len = strlen("DEVICE_URL");
		ipcMonListTable->pkeyname[4].pdat = malloc(ipcMonListTable->pkeyname[4].len);
		memcpy(ipcMonListTable->pkeyname[4].pdat, "DEVICE_URL", ipcMonListTable->pkeyname[4].len);

		ipcMonListTable->pkeyname[5].len = strlen("IPC_ID");
		ipcMonListTable->pkeyname[5].pdat = malloc(ipcMonListTable->pkeyname[5].len);
		memcpy(ipcMonListTable->pkeyname[5].pdat, "IPC_ID", ipcMonListTable->pkeyname[5].len);

		ipcMonListTable->pkeyname[6].len = strlen("FAVERITE");
		ipcMonListTable->pkeyname[6].pdat = malloc(ipcMonListTable->pkeyname[6].len);
		memcpy(ipcMonListTable->pkeyname[6].pdat, "FAVERITE", ipcMonListTable->pkeyname[6].len);

		ipcMonListTable->record_cnt = 0;
		
		save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);

		//printf_one_table(ipcMonListTable);
	}
}

int Get_IPC_MonRes_Num(void)
{
	if(ipcMonListTable == NULL)
	{
		return 0;
	}

	return ipcMonListTable->record_cnt;
}

//dh_20191204_s
int Get_IPC_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_IPC_MonRes_Num(); i++)
	{
		Get_IPC_MonRes_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

//czn_20190221_s
void IPC_MonRes_Reload(void)
{
	free_vtk_table_file_buffer(ipcMonListTable);
	IPC_MonRes_Table_Init();
}
//czn_20190221_e
int Get_IPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(ipcMonListTable == NULL || ipcMonListTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(ipcMonListTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	//printf("Get_IPC_MonRes_Record index=%d\n", index);
	//printf_one_table(ipcMonListTable);
	
	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);	

	#if 0
	memcpy(recordBuffer, pRecord->pdat, pRecord->len);

	//printf("Get_IPC_MonRes_Record = %s\n", recordBuffer);
	
	strcpy(strTable[i++], strtok(recordBuffer,","));
	while(pos1 = strtok(NULL,",")) strcpy(strTable[i++], pos1);
	
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#endif
	return 0;
}

int Modify_IPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	onvif_dis_dat_t dat;
	
	//snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id);
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	//printf("Modify_IPC_MonRes_Record = %s\n", data);
	Modify_one_vtk_record(&record, ipcMonListTable, index);
	
	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Delete_IPC_MonRes_Record(int index)
{
	delete_one_vtk_record(ipcMonListTable, index);

	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Add_IPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	onvif_dis_dat_t dat;
	
	//snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id);
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(ipcMonListTable, &record);

	remove(IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);
	SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
	return 0;
}

int Add_Or_Modify_IPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	int i;
	IPC_RECORD record;

	for(i = 0; i < Get_IPC_MonRes_Num(); i++)
	{
		Get_IPC_MonRes_Record(i, &record);

		if(ipcDhcpEnable)
		{
			if(!strcmp(record.id, ipcRecord->id))
			{
				Modify_IPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
		else
		{
			if(!strcmp(record.devUrl, ipcRecord->devUrl))
			{
				Modify_IPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
	}
	
	Add_IPC_MonRes_Record(ipcRecord);
	
	return 0;
}

int Clear_IPC_MonRes_Record(void)
{
	int i;
	
	for(i=0; i<ipcMonListTable->record_cnt; i++)
	{
		if(ipcMonListTable->precord[i].pdat != NULL)
		{
			free(ipcMonListTable->precord[i].pdat);
			ipcMonListTable->precord[i].pdat = NULL;
		}
	}
	
	release_one_vtk_tabl(ipcMonListTable);
	remove(IPC_MON_LIST_FILE_NAME);
	
	IPC_MonRes_Table_Init();
	
	save_vtk_table_file_buffer(ipcMonListTable, 0, Get_IPC_MonRes_Num(), IPC_MON_LIST_FILE_NAME);

	return 0;
}
#endif

void DisplayOneIPCList(int index, int x, int y)
{
	//IPC_RECORD dat;
	IPC_ONE_DEVICE dat;
	char *pos1, *pos2;
	char deviceIp[100] = {0};
	int len;
	char display[100];
	
	//if(index < Get_IPC_MonRes_Num())
	if(index < GetIpcNum())
	{
		printf("DisplayOneIPCList index:%d\n", index);
		GetIpcCacheRecord(index, &dat);
		#if 0
		pos1 = strstr(dat.IP, "http://");
		pos1 += strlen("http://");
		pos2 = strstr(pos1, "/onvif/device_service");
		if(pos1 && pos2)
		{
			memcpy(deviceIp, pos1, strlen(pos1) - strlen(pos2));
		}
		#endif
		snprintf(display, 100, "[%s] %s", dat.IP, dat.NAME);
		API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, 0);
	}
}

void DisplayOnePageIPCMonitor(uint8 page)
{
	int i, x, y, maxPage, listStart, maxList;
	POS pos;
	SIZE hv;
	
	ipcListIconMax = GetListIconNum();
	//maxList = Get_IPC_MonRes_Num();
	maxList = GetIpcNum();
	printf("DisplayOnePageIPCMonitor ipc_num:%d\n", maxList);
	
	listStart = page*ipcListIconMax;
	
	for( i = 0; i < ipcListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(GetLastNMenu() == MENU_116_MultiRec)
		{
			if( (listStart+i) == recIpcIndex[ipcSelectForCh] )
				ListSelect(listStart+i, 1);
		}
		DisplayOneIPCList(listStart+i, x, y);
	}

	maxPage = maxList/ipcListIconMax + (maxList%ipcListIconMax ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
}

int AddIPC_TableBySearching(char *defualt_user,char *default_pwd,int default_ch)
{
#if 0
	int dis_num,i,add_num = 0;
	char *pos1, *pos2;
	int len;
	
	//char IPC_IP[45];
	//IPC_RECORD ipcRecord;
	IPC_ONE_DEVICE ipcRecord;
	
	onvif_device_discovery();
	
	usleep(100000);
	
	//DisplayOnePageSearchingIPC(0);
	dis_num = onvif_discovery_device_list_total();
	printf("AddIPC_TableBySearching num=%d\n", dis_num);
	for(i = 0;i < dis_num;i ++)
	{
		onvif_device_get_addr(i, ipcRecord.IP, ipcRecord.ID);
		#if 1
		if(pos1 = strstr(ipcRecord.IP, "http://"))
		{
			pos1 += strlen("http://");
			pos2 = strstr(pos1, "/onvif");
			len = (int)(pos2-pos1);
			memcpy(IPC_IP, pos1, len);
			IPC_IP[len] = 0;
		}
		//#endif
		strcpy(ipcRecord.USER, defualt_user);
		strcpy(ipcRecord.PWD, default_pwd);
		
		if(!strcmp(GetNetDeviceNameByTargetIp(inet_addr(ipcRecord.IP)), NET_ETH0))
		{
			if(GetNameByIp(ipcRecord.IP, ipcRecord.NAME) == 0)
			{
				strcpy(ipcRecord.ID, ipcRecord.NAME);
				ipcRecord.NAME[20] = 0;
				//if(onvif_Login(ipcRecord.devUrl, ipcRecord.userName, ipcRecord.userPwd, ipcRecord.channel, &info) == 0)
				memset(&Login, 0, sizeof(onvif_one_device_info_t));
				if(one_ipc_Login(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, &Login) == 0)
				{
					sprintf(ipcRecord.NAME,"IP-CAM%d",++add_num);
					ipcRecord.CH_ALL = Login.ch_cnt;
					if(ipcRecord.CH_ALL < 3)
					{
						ipcRecord.CH_REC = 0;
						ipcRecord.CH_FULL = 0;
						ipcRecord.CH_QUAD = 1;
						ipcRecord.CH_APP = 1;
					}
					else
					{
						ipcRecord.CH_REC = 0;
						ipcRecord.CH_FULL = 0;
						ipcRecord.CH_QUAD = 1;
						ipcRecord.CH_APP = 2;
					}
					for(i=0; i<ipcRecord.CH_ALL; i++ )
					{
						ipcRecord.CH_DAT[i] = Login.dat[i];
					}
					AddOrModifyOneIpcRecord(&ipcRecord);
					
					//strcpy(ipcRecord.devUrl, Login.device_url);
					//Add_Or_Modify_IPC_MonRes_Record(&ipcRecord);
					//printf("!!!!!!!!!!add tb ok %d,%s\n",i,ipcRecord.name);
				}
				else
				{
					//BEEP_ERROR();
					printf("!!!!!!!!!!fail onvif_Login %d\n",i);
				}
			}
			else
			{
				printf("!!!!!!!!!!fail GetNameByIp %d\n",i);
			}
		}
		#endif
	}

	return add_num;
#endif
}

int ConfirmModifyOneIPCMonlist(const char* string)
{
	strcpy(ipcListName, string);
	if(ipcListName[0] != 0)
	{
		strcpy(saveIPCMonList.name, ipcListName);
		Modify_IPC_MonRes_Record(ipcListIndex, &saveIPCMonList);
	}
	return 1;
}

void Enter_IPC_ListSelect(int ch)
{
	ipcSelectForCh = ch;
	StartInitOneMenu(MENU_057_IPC_LIST,0,1);
}

int VdListNum, ipcNum, vdSelect;
int GetVdListNum(void)
{
	ipcNum = GetIpcNum()+GetWlanIpcNum();
	VdListNum = ipcNum + (Get_MonRes_Num() == 0? Get_MonRes_Temp_Table_Num() : Get_MonRes_Num());
	VdListNum+=Get_DxMonRes_Num();
	printf("GetVdListNum = %d\n", VdListNum);	
	
	return VdListNum;
}
#if 0
void GetVdListNameBuffer(void)
{
	int i;
	IPC_ONE_DEVICE ipcRecord;
	char name_temp[41];
	char bdRmMs[11];
	
	for(i = 0; i < VdListNum; i++)
	{
		if(i < GetIpcNum())
		{
			GetIpcCacheRecord(i, &ipcRecord);
			strcpy(getVdListDisplayBuffer[i], ipcRecord.NAME);
		}
		else
		{
			if(Get_MonRes_Num() == 0)
			{
				Get_MonRes_Temp_Table_Record(i - GetIpcNum(),name_temp,bdRmMs, NULL);
			}
			else
			{
				Get_MonRes_Record(i - GetIpcNum(),name_temp,bdRmMs, NULL);
			}
			
			get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, getVdListDisplayBuffer[i]);
		}
	}
	
}
#endif
static void MenuListGetVdListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	IPC_ONE_DEVICE ipcRecord;
	char name_temp[41];
	char bdRmMs[11];
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < VdListNum)
		{
			if(index < ipcNum)
			{
				if(index < GetIpcNum())
				{
					pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_IPC;
					GetIpcCacheRecord(index, &ipcRecord);
				}
				else
				{
					pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_WIPC;
					GetWlanIpcRecord(index-GetIpcNum(), &ipcRecord);
				}				
				strcpy(DisplayBuffer, ipcRecord.NAME);
			}
			else if(index<(VdListNum-Get_DxMonRes_Num()))
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				if(Get_MonRes_Num() == 0)
				{
					if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, NULL) != 0)
						return;
				}
				else
				{
					if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, NULL) != 0)
						return;
				}
				get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, DisplayBuffer);
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				Get_DxMonRes_Record(index-(VdListNum-Get_DxMonRes_Num()), name_temp,bdRmMs);
				strcpy(DisplayBuffer,name_temp);
			}

			recordLen = strlen(DisplayBuffer);
			//unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			unicode_len = 2*utf82unicode(DisplayBuffer,recordLen,unicode_buf);
			//memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			if(index == vdSelect)
			{
				pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
			}
			
			pDisp->dispCnt++;
		}
	}
}




void MENU_057_IPC_List_Init(int uMenuCnt)
{
#if 0
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorList,1, 0);
	ipcListIconSelect = 0;
	ipcListPageSelect = 0;
	DisplayOnePageIPCMonitor(ipcListPageSelect);
#endif
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID2(MESG_TEXT_Monitor, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	
	listInit.listCnt = GetVdListNum();
	listInit.selEn = 1;
	if(get_pane_type() == 7)//dx470v
	{
		listInit.listType = ICON_4X2;
		listInit.textOffset = 110;
	}	
	else 
	{
		listInit.listType = ICON_2X4;
		listInit.textOffset = 110;
	}
	 
	listInit.textSize = 0;
	listInit.textAlign = 8;
	//GetVdListNameBuffer();
	listInit.fun = MenuListGetVdListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);
}




void MENU_057_IPC_List_Exit(void)
{
	MenuListDisable(0);
}

void MENU_057_IPC_List_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						vdSelect = listIcon.mainIcon;
						MenuListDiplayPage();
						if(SetOneVdShow(listIcon.mainIcon)==0)
						{
							usleep(100000);
							popDisplayLastMenu();
						}
						else
						{
							BEEP_ERROR();
						}
					}
				break;
				#if 0
				case ICON_266_IPC_Edit:
					if(ipcListState != ICON_266_IPC_Edit)
					{
						API_MenuIconDisplaySelectOff(ipcListState);
						ipcListState = ICON_266_IPC_Edit;
						API_MenuIconDisplaySelectOn(ipcListState);
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorListEdit,1, 0);
						ipcListPageSelect = 0;
						DisplayOnePageIPCMonitor(ipcListPageSelect);
						if(deleteConfirm)
						{
							deleteConfirm = 0;
							API_SpriteClose(640, 85+65*ipcListLastIconSelect, SPRITE_IF_CONFIRM);
						}
					}
					break;
				case ICON_267_IPC_Delete:
					if(ipcListState != ICON_267_IPC_Delete)
					{
						API_MenuIconDisplaySelectOff(ipcListState);
						ipcListState = ICON_267_IPC_Delete;
						API_MenuIconDisplaySelectOn(ipcListState);
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorListDelete,1, 0);
						ipcListPageSelect = 0;
						DisplayOnePageIPCMonitor(ipcListPageSelect);
					}
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					ipcListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					ipcListIndex = ipcListPageSelect*ipcListIconMax + ipcListIconSelect;
					if(ipcListIndex >= GetIpcNum())
						return;
					if( recIpcIndex[ipcSelectForCh] == ipcListIndex )
					{
						ListSelect(ipcListIndex, 0);
						recIpcIndex[ipcSelectForCh] = -1;
					}
					else
					{
						ListSelect(ipcListIndex, 1);
						recIpcIndex[ipcSelectForCh] = ipcListIndex;
					}
					popDisplayLastMenu();
					
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&ipcListPageSelect, ipcListIconMax, GetIpcNum(), (DispListPage)DisplayOnePageIPCMonitor);
					break;
				case ICON_202_PageUp:
					//PublicPageUpProcess(&ipcListPageSelect, ipcListIconMax, GetIpcNum(), (DispListPage)DisplayOnePageIPCMonitor);
					break;
				
				#endif	
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}



#if 0
one_vtk_table* ipcTempTable = NULL;


void IPC_Temp_Table_Delete(void)
{
	free_vtk_table_file_buffer(ipcTempTable);
	ipcTempTable = NULL;
}

void IPC_Temp_Table_Create(void)
{
	char temp[20];
	
	if(ipcTempTable == NULL)
	{
		ipcTempTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &ipcTempTable->lock, 0);
		ipcTempTable->keyname_cnt = 7;
		ipcTempTable->pkeyname_len = malloc(ipcTempTable->keyname_cnt*sizeof(int));
		ipcTempTable->pkeyname = malloc(ipcTempTable->keyname_cnt*sizeof(one_vtk_dat));
		ipcTempTable->pkeyname_len[0] = 40;
		ipcTempTable->pkeyname_len[1] = 40;
		ipcTempTable->pkeyname_len[2] = 40;
		ipcTempTable->pkeyname_len[3] = 1;
		ipcTempTable->pkeyname_len[4] = 100;
		ipcTempTable->pkeyname_len[5] = 40;
		ipcTempTable->pkeyname_len[6] = 1;
		ipcTempTable->pkeyname[0].len = strlen("IPC_NAME");
		ipcTempTable->pkeyname[0].pdat = malloc(ipcTempTable->pkeyname[0].len);
		memcpy(ipcTempTable->pkeyname[0].pdat, "IPC_NAME", ipcTempTable->pkeyname[0].len);
		
		ipcTempTable->pkeyname[1].len = strlen("USER_NAME");
		ipcTempTable->pkeyname[1].pdat = malloc(ipcTempTable->pkeyname[1].len);
		memcpy(ipcTempTable->pkeyname[1].pdat, "USER_NAME", ipcTempTable->pkeyname[1].len);

		ipcTempTable->pkeyname[2].len = strlen("USER_PWD");
		ipcTempTable->pkeyname[2].pdat = malloc(ipcTempTable->pkeyname[2].len);
		memcpy(ipcTempTable->pkeyname[2].pdat, "USER_PWD", ipcTempTable->pkeyname[2].len);

		ipcTempTable->pkeyname[3].len = strlen("CHANNEL");
		ipcTempTable->pkeyname[3].pdat = malloc(ipcTempTable->pkeyname[3].len);
		memcpy(ipcTempTable->pkeyname[3].pdat, "CHANNEL", ipcTempTable->pkeyname[3].len);

		ipcTempTable->pkeyname[4].len = strlen("DEVICE_URL");
		ipcTempTable->pkeyname[4].pdat = malloc(ipcTempTable->pkeyname[4].len);
		memcpy(ipcTempTable->pkeyname[4].pdat, "DEVICE_URL", ipcTempTable->pkeyname[4].len);

		ipcTempTable->pkeyname[5].len = strlen("IPC_ID");
		ipcTempTable->pkeyname[5].pdat = malloc(ipcTempTable->pkeyname[5].len);
		memcpy(ipcTempTable->pkeyname[5].pdat, "IPC_ID", ipcTempTable->pkeyname[5].len);
		
		ipcTempTable->pkeyname[6].len = strlen("FAVERITE");
		ipcTempTable->pkeyname[6].pdat = malloc(ipcTempTable->pkeyname[6].len);
		memcpy(ipcTempTable->pkeyname[6].pdat, "FAVERITE", ipcTempTable->pkeyname[6].len);

		ipcTempTable->record_cnt = 0;
	}
}

int Get_IPC_Temp_Num(void)
{
	if(ipcTempTable == NULL)
	{
		return 0;
	}

	return ipcTempTable->record_cnt;
}

int Get_IPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(ipcTempTable == NULL || ipcTempTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(ipcTempTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#if 0
	memcpy(recordBuffer, pRecord->pdat, pRecord->len);

	//printf("Get_IPC_MonRes_Record = %s\n", recordBuffer);
	
	strcpy(strTable[i++], strtok(recordBuffer,","));
	while(pos1 = strtok(NULL,",")) strcpy(strTable[i++], pos1);
	
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	#endif
	return 0;
}

int Add_IPC_Temp_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	onvif_dis_dat_t dat;
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(ipcTempTable, &record);
	return 0;
}

//dh_20191204_s
int Get_IPC_Temp_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_IPC_Temp_Num(); i++)
	{
		Get_IPC_Temp_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

int Modify_IPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	onvif_dis_dat_t dat;
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	//printf("Modify_IPC_MonRes_Record = %s\n", data);
	Modify_one_vtk_record(&record, ipcTempTable, index);
	
	return 0;
}
#endif


