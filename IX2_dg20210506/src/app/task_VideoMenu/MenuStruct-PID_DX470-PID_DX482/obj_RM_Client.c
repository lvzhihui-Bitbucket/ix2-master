/**
  ******************************************************************************
  * @file    obj_RM_Client.h
  * @author  DH
  * @version V00.01.00
  * @date    2017.11.2
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_RM_Client.h"	
#include "obj_OsdString.h"
#include "obj_Menu.h"
#include "task_VideoMenu.h"

#define INNER_FONT_W		   16  //12 //16
#define INNER_FONT_H		   24  //16 //24


Display_CMD cmd_buf;

int Rm_Display(unsigned char *disp_buf)
{
	unsigned char pString[65];
	cmd_buf.len = disp_buf[0];
	int i = 0, x, y;
	int block_head,block_tail;
	if(cmd_buf.len > MAX_RM_CMD_LEN || GetCurMenuCnt() != MENU_144_RM_Display)
	{
		return -1;
	}
#if 1	
block_start:
	block_head = ++i;
	cmd_buf.col = (disp_buf[block_head]&0x07)*4;
	cmd_buf.row = (disp_buf[block_head]>>3)&0x07;
	if((disp_buf[block_head]&0xc0)>>6 == 3)
	{
		cmd_buf.clear = 1;
	}
	else
	{
		cmd_buf.clear = 0;
		cmd_buf.color = (disp_buf[block_head]&0xc0)>>6;
		//cmd_buf.color = (cmd_buf.color == 0? COLOR_BLACK: (cmd_buf.color == 1? COLOR_YELLOW : COLOR_RED));
		cmd_buf.color = (cmd_buf.color == 0? COLOR_BLACK: COLOR_RED);
	}
	while( i < cmd_buf.len )
	{
		if( get_pane_type() == 5 || get_pane_type() == 7)//ix470V
		{
			x = 200+INNER_FONT_W*cmd_buf.col;
			y = 60+(INNER_FONT_H+20)*cmd_buf.row;
		}
		else
		{
			x = 320+INNER_FONT_W*cmd_buf.col;
			y = 60+(INNER_FONT_H+20)*cmd_buf.row;
		}
		i++;
		
		if(disp_buf[i] == 0)
		{
			block_tail = i;
			memset(pString,0,65);
			memcpy(pString, disp_buf+block_head+1, block_tail-block_head-1);
			if(cmd_buf.clear)
			{
				if(*(pString) == 0xff)
				{
					OSD_StringClearExt(x, y, 800-x, 50*8);
				}
				else
				{
					memset(pString+1, ' ', *(pString));
					OSD_StringClear(x, y, pString+1);
				}
			}
			else
			{
				OSD_StringDisplay(x, y, cmd_buf.color, COLOR_KEY, pString);
				//OSD_StringDisplayExt(x, y,cmd_buf.color, COLOR_KEY, pString, strlen(pString), 0, STR_UTF8, 0);

			}
			
			if( i == cmd_buf.len-1 )
			{
				break;
			}
			else
			{
				goto block_start;
			}
		}
	}
#endif
	return 0;
}
