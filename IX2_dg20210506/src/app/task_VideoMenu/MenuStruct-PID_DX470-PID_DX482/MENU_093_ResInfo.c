#include "MENU_093_ResInfo.h"

const IconAndText_t ResInfoIconTable[] = 
{
	{ICON_ViewCustomizer,				MESG_TEXT_ICON_ViewCustomizer},
	{ICON_ViewRes,						MESG_TEXT_ICON_ViewRes},
	{ICON_ViewUserData,					MESG_TEXT_ICON_ViewUserData},
	{ICON_ViewBackup,					MESG_TEXT_ICON_ViewBackup},
};

const int ResInfoIconNum = sizeof(ResInfoIconTable)/sizeof(ResInfoIconTable[0]);

static void MenuListGetResInfoPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen, unicode_len;
	char unicode_buf[200];
	char display[80] = {0};
	int stringId;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < ResInfoIconNum)
		{
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			
			API_GetOSD_StringWithID(ResInfoIconTable[index].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->dispCnt++;
		}
	}

}


void MENU_093_ResInfo_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID(MESG_TEXT_ICON_RES_INFO, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
	
	listInit = ListPropertyDefault();
	
	if( get_pane_type() == 7 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = ResInfoIconNum;
	listInit.fun = MenuListGetResInfoPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	
	InitMenuList(listInit);

}

void MENU_093_ResInfo_Exit(void)
{
	MenuListDisable(0);
}

void MENU_093_ResInfo_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon > MENU_LIST_ICON_NONE)
					{
						switch(ResInfoIconTable[listIcon.mainIcon].iCon)
						{
							case ICON_ViewCustomizer:
								EnterFileListMenu(CustomerFileDir, ResInfoIconTable[listIcon.mainIcon].iConText);
								break;
							case ICON_ViewRes:
								EnterFileListMenu(UserResFolder, ResInfoIconTable[listIcon.mainIcon].iConText);
								break;
							case ICON_ViewUserData:
								EnterFileListMenu(UserDataFolder, ResInfoIconTable[listIcon.mainIcon].iConText);
								break;
							case ICON_ViewBackup:
								EnterFileListMenu(INSTALLER_BAK_PATH, ResInfoIconTable[listIcon.mainIcon].iConText);
								break;
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}


