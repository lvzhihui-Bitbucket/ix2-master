#include "MENU_012_PublicSetting.h"


//#define	publicSettingIconMax	5
int publicSettingIconMax;

char publicSettingDisplay[30][101];

int publicSettingTitle;
int publicSettingMaxNum;
int publicSettingDispDataSize;
int publicSettingValue;
int publicSettingSave;

void (*PublicSettingUpdateValue)(int) = NULL;


int publicSettingSelect;
int publicSettingIconSelect;
int publicSettingPage;

void InitPublicSettingMenuDisplay(int maxNum, const char** display)
{
	int i, j;
	for(i = 0; i < maxNum; i++)
	{
		for(publicSettingDisplay[i][0] = strlen(display[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
		{
			publicSettingDisplay[i][j] = display[i][j/2];
			publicSettingDisplay[i][j+1]=0;
		}
	}
}

void EnterPublicSettingMenu(int settingSelect, int title, int maxNum, int settingValue, void (*UpdateValue)(int))
{
	publicSettingTitle = title;
	publicSettingMaxNum = maxNum;
	publicSettingSave = settingValue;
	publicSettingValue = settingValue;
	PublicSettingUpdateValue = UpdateValue;
	publicSettingSelect = settingSelect;
}

static void DisplayOnePagepublicSetting(int page);

static void MenuListGetPublicPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	char* disp;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < publicSettingMaxNum)
		{
			disp = publicSettingDisplay[index];

			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;

			memcpy(pDisp->disp[pDisp->dispCnt].str, disp+1, disp[0]);
			pDisp->disp[pDisp->dispCnt].strLen = disp[0];
			
			if(index == publicSettingValue)
			{
				pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
			}
			
			pDisp->dispCnt++;
		}
	}
}

void MENU_012_PublicSetting_Init(int uMenuCnt)
{
	#if 0
	{
		int unicode_len;
		char unicode_buf[200];
		LIST_INIT_T listInit;
		uint16 x, y;
		POS pos;
		SIZE hv;

		x = GetIconXY(ICON_218_PublicSetTitle).x;
		y = GetIconXY(ICON_218_PublicSetTitle).y;
		
		API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);	
		GetIconTextInfo(ICON_218_PublicSetTitle, &pos, &hv);
		API_OsdUnicodeStringDisplay(pos.x, pos.y, DISPLAY_TITLE_COLOR,publicSettingSelect,1, 0);

		switch(publicSettingSelect)
		{
			case MESG_TEXT_ICON_024_CallTune:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetCalltune);		
				break;
			case MESG_TEXT_ICON_025_general:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetGeneral);		
				break;
			case MESG_TEXT_ICON_026_InstallerSetup:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetInstaller);		
				break;
			case MESG_TEXT_ICON_027_wireless:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetWireless);		
				break;
		}
		
		API_GetOSD_StringWithID2(publicSettingTitle, unicode_buf, &unicode_len);
		
		listInit = ListPropertyDefault();
		
		listInit.listType = TEXT_5X1;
		listInit.selEn = 1;
		listInit.listCnt = publicSettingMaxNum;
		listInit.fun = MenuListGetPublicPage;
		listInit.titleStr = unicode_buf;
		listInit.titleStrLen = unicode_len;
		
		InitMenuList(listInit);
	}
	#else
	{
		uint16 x, y;
		POS pos;
		SIZE hv;
		
		x = GetIconXY(ICON_218_PublicSetTitle).x;
		y = GetIconXY(ICON_218_PublicSetTitle).y;
		publicSettingIconMax = GetListIconNum();
			
		publicSettingPage = publicSettingValue/publicSettingIconMax;
		publicSettingIconSelect = publicSettingValue%publicSettingIconMax;
		
		switch(publicSettingSelect)
		{
			case MESG_TEXT_ICON_024_CallTune:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetCalltune);		
				break;
			case MESG_TEXT_ICON_025_general:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetGeneral);		
				break;
			case MESG_TEXT_ICON_026_InstallerSetup:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetInstaller);		
				break;
			case MESG_TEXT_ICON_027_wireless:
				API_SpriteDisplay_XY(x, y, SPRITE_PublicSetWireless);		
				break;
		}
		
		API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);	
		GetIconTextInfo(ICON_218_PublicSetTitle, &pos, &hv);
		//printf("GetIconTextInfo x=%d, y=%d, h=%d, v=%d\n",pos.x, pos.y, hv.h, hv.v);
		//API_OsdUnicodeStringDisplay2(pos.x, pos.y, DISPLAY_TITLE_COLOR,publicSettingSelect,1, hv.h-pos.x, hv.v - pos.y);
		API_OsdUnicodeStringDisplay(pos.x, pos.y, DISPLAY_TITLE_COLOR,publicSettingSelect,1, 0);
		
		OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,publicSettingTitle,2, 0);
		DisplayOnePagepublicSetting(publicSettingPage);
	}
	#endif
}

void MENU_012_PublicSetting_Exit(void)
{
	API_RingStop();
	MenuListDisable(0);
	memset(publicSettingDisplay,0,sizeof(publicSettingDisplay));
}

void MENU_012_PublicSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	uint8 setValue;
	int i, x, y;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&publicSettingPage, publicSettingIconMax, publicSettingMaxNum, (DispListPage)DisplayOnePagepublicSetting);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&publicSettingPage, publicSettingIconMax, publicSettingMaxNum, (DispListPage)DisplayOnePagepublicSetting);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					publicSettingIconSelect = GetCurIcon() - ICON_007_PublicList1;
					publicSettingValue = publicSettingPage*publicSettingIconMax+publicSettingIconSelect;
					
					if(publicSettingValue >= publicSettingMaxNum)
					{
						return;
					}
					
					if(publicSettingSelect == MESG_TEXT_ICON_024_CallTune)
					{
						publicSettingSelectFlagDisplay();
						BusySpriteDisplay(1);
						usleep(200000);
						PublicSettingUpdateValue(publicSettingValue);
					}
					else if(publicSettingTitle == MESG_TEXT_ICON_337_TimeServer)
					{
						if(publicSettingValue == 0)
						{
							extern char timeServer[];
							int InputTimeServerSet(char* input);
							
							popLastMenu();
							SetCurMenuCnt(GetLastNMenu());
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_337_TimeServer, timeServer, 32, COLOR_WHITE, timeServer, 1, InputTimeServerSet);
						}
						else
						{
							publicSettingSelectFlagDisplay();
							BusySpriteDisplay(1);
							usleep(200000);
							PublicSettingUpdateValue(publicSettingValue);
							popDisplayLastMenu();
						}
					}
					else if(publicSettingTitle == MESG_TEXT_ICON_FwUpgrage_Server)
					{
						if(publicSettingValue == 0)
						{
							RemoteUpgradeInputServerProcess();
						}
						else
						{
							publicSettingSelectFlagDisplay();
							BusySpriteDisplay(1);
							usleep(200000);
							PublicSettingUpdateValue(publicSettingValue);
							popDisplayLastMenu();
						}
					}
					else if(publicSettingTitle == MESG_TEXT_ICON_100_SipEnable)
					{
						if(publicSettingValue == 0)
						{
							publicSettingSelectFlagDisplay();
							BusySpriteDisplay(1);
							usleep(200000);
							PublicSettingUpdateValue(publicSettingValue);
						}
						else
						{
							PublicSettingUpdateValue(publicSettingValue);
						}
					}
					else
					{
						if(publicSettingSave != publicSettingValue)
						{
							publicSettingSelectFlagDisplay();
							BusySpriteDisplay(1);
							usleep(200000);
							PublicSettingUpdateValue(publicSettingValue);
						}
						popDisplayLastMenu();
					}
					break;
					
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						if(publicSettingValue >= publicSettingMaxNum)
						{
							return;
						}

						publicSettingValue = listIcon.mainIcon;
						MenuListDiplayPage();
						
						if(publicSettingSelect == MESG_TEXT_ICON_024_CallTune)
						{
							BusySpriteDisplay(1);
							usleep(200000);
							PublicSettingUpdateValue(publicSettingValue);
						}
						else if(publicSettingTitle == MESG_TEXT_ICON_337_TimeServer)
						{
							if(publicSettingValue == 0)
							{
								extern char timeServer[];
								int InputTimeServerSet(char* input);
								
								popLastMenu();
								SetCurMenuCnt(GetLastNMenu());
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_337_TimeServer, timeServer, 32, COLOR_WHITE, timeServer, 1, InputTimeServerSet);
							}
							else
							{
								BusySpriteDisplay(1);
								usleep(200000);
								PublicSettingUpdateValue(publicSettingValue);
								popDisplayLastMenu();
							}
						}
						else if(publicSettingTitle == MESG_TEXT_ICON_FwUpgrage_Server)
						{
							if(publicSettingValue == 0)
							{
								RemoteUpgradeInputServerProcess();
							}
							else
							{
								BusySpriteDisplay(1);
								usleep(200000);
								PublicSettingUpdateValue(publicSettingValue);
								popDisplayLastMenu();
							}
						}
						else if(publicSettingTitle == MESG_TEXT_ICON_100_SipEnable)
						{
							if(publicSettingValue == 0)
							{
								BusySpriteDisplay(1);
								usleep(200000);
								PublicSettingUpdateValue(publicSettingValue);
							}
							else
							{
								PublicSettingUpdateValue(publicSettingValue);
							}
						}
						else
						{
							if(publicSettingSave != publicSettingValue)
							{
								BusySpriteDisplay(1);
								usleep(200000);
								PublicSettingUpdateValue(publicSettingValue);
							}
							popDisplayLastMenu();
						}
					}
					break;
			}
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}
static void DisplayOnePagepublicSetting(int page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	char* disp;
	POS pos;
	SIZE hv;
	//API_DisableOsdUpdate();

	for(i = 0; i < publicSettingIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		//printf("OSD_GetIconInfo x=%d, y=%d, h=%d, v=%d\n",pos.x, pos.y, hv.h, hv.v);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		ListSelect(i, 0);
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		
		if(page*publicSettingIconMax+i < publicSettingMaxNum)
		{
			disp = publicSettingDisplay[publicSettingPage*publicSettingIconMax+i];
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, disp+1, disp[0],1,STR_UNICODE, bkgd_w - x);
			if(page*publicSettingIconMax+i == publicSettingValue)
			{
				ListSelect(i, 1);
			}
		}
	}
	
	//API_EnableOsdUpdate();

	pageNum = publicSettingMaxNum/publicSettingIconMax + (publicSettingMaxNum%publicSettingIconMax ? 1 : 0);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
}



void publicSettingSelectFlagDisplay(void)
{
	int x, y, i;
	
	for(i = 0; i < publicSettingIconMax; i++)
	{
		ListSelect(i, 0);		
		if(publicSettingPage*publicSettingIconMax+i < publicSettingMaxNum)
		{
			if(publicSettingPage*publicSettingIconMax+i == publicSettingValue)
			{
				ListSelect(i, 1);
			}
		}
	}

}

void PublicListUpProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage)
{
	if(*iconSelect)
	{
		(*iconSelect)--;
	}
	else
	{
		if(*pageSelect)
		{
			(*pageSelect)--;
			*iconSelect = maxIcon-1;
		}
		else
		{
			*pageSelect = (listNum ? (listNum-1)/maxIcon : 0);
			*iconSelect = (listNum ? (listNum-1)%maxIcon : 0);;
		}
		DispPage(*pageSelect);
	}
	API_FixedCurIconCursor(*iconSelect);
}

void PublicListDownProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage)
{
	if(*iconSelect < maxIcon-1)
	{
		//最后一页
		if((*pageSelect+1)*maxIcon > listNum-1)
		{
			if(*iconSelect < (listNum-1)%maxIcon)
			{
				(*iconSelect)++;
			}
			else
			{
				*iconSelect = 0;
				*pageSelect = 0;
				DispPage(*pageSelect);
			}
		}
		//不是最后一页
		else
		{
			(*iconSelect)++;
		}
	}
	//翻页
	else
	{
		*iconSelect = 0;
		if((++(*pageSelect))*maxIcon > listNum-1)
		{
			*pageSelect = 0;
		}
		DispPage(*pageSelect);
	}
	API_FixedCurIconCursor(*iconSelect);
}

void PublicListRefreshProcess(int *iconSelect, int *pageSelect, int maxIcon, int listNum, DispListPage DispPage)
{
	int pageNum;


	if(listNum == 0)
	{
		*pageSelect = 0;
		*iconSelect = 0;
	}
	else
	{
		pageNum = listNum/maxIcon + (listNum%maxIcon ? 1 : 0);

		//页数超过，光标去到最后一条
		if(*pageSelect >= pageNum)
		{
			*pageSelect = pageNum - 1;
			(*iconSelect) = (listNum-1)%maxIcon;
		}
		//页数是最后一页
		else if(*pageSelect == pageNum - 1)
		{
			//行数超过最后一行
			if(*iconSelect > (listNum-1)%maxIcon)
			{
				(*iconSelect) = (listNum-1)%maxIcon;
			}
		}
	}
	API_FixedCurIconCursor(*iconSelect);
	DispPage(*pageSelect);
}

void ListSelect(uint8 line, uint8 onOff)
{
	uint16 x, y;
	POS pos;
	SIZE hv;

	OSD_GetIconInfo(ICON_007_PublicList1+line, &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	if(onOff)
	{
		API_SpriteDisplay_XY(x, y, SPRITE_WIFI_SELECT);
	}
	else
	{
		API_SpriteClose(x, y, SPRITE_WIFI_SELECT);
	}
}

//#define BAR_MOVE_START		174
//#define BAR_MOVE_END		559
//#define BAR_MOVE_LENGTH 	(BAR_MOVE_END - BAR_MOVE_START)

void DisplaySchedule(int x, int y, int curPage, int pageNum)
{
	int posY;
	int overPage;
	char display[20];
	POS pos;
	SIZE hv;
	int barStart,barEnd,barLen;
	uint16 xsize, ysize1, ysize2;
	OSD_GetIconInfo(ICON_177_KeyBar1, &pos, &hv);
	Get_SpriteSize(SPRITE_ScheduleBottom, &xsize, &ysize1);
	Get_SpriteSize(SPRITE_ScheduleTop, &xsize, &ysize2);
	x = pos.x;
	y = pos.y;
	barStart = hv.v;
	barEnd = pos.y + ysize1 - ysize2 - (hv.v - pos.y);
	barLen = barEnd - barStart;
	
	if(pageNum > 1)
	{
		API_SpriteClose(GetIconXY(ICON_202_PageUp).x, GetIconXY(ICON_202_PageUp).y, SPRITE_HidePageTurning);
		if( get_pane_type() != 7 )
			API_SpriteDisplay_XY(x, y, SPRITE_ScheduleBottom);
		if(pageNum-1 > barLen)
		{
			overPage = pageNum - 1 - barLen;
			if(curPage < barLen/2)
			{
				posY = barStart+curPage;
			}
			else if(curPage < barLen/2+overPage)
			{
				posY = barStart + barLen/2;
			}
			else
			{
				posY = barStart + curPage - overPage;
			}
		}
		else
		{
			if(barLen%(pageNum-1) && curPage <= barLen%(pageNum-1))
			{
				posY = barStart + curPage*(barLen/(pageNum-1)) +  curPage;
			}
			else
			{
				posY = barStart + curPage*(barLen/(pageNum-1)) + barLen%(pageNum-1);
			}
		}
		if( get_pane_type() != 7 )
			API_SpriteDisplay_XY(x+1, posY, SPRITE_ScheduleTop);

		OSD_GetIconInfo(ICON_201_PageDown, &pos, &hv);
		
		API_OsdStringClearExt(pos.x-24, pos.y-10+(hv.v - pos.y)/2, bkgd_w-pos.x+24, CLEAR_STATE_H);		
		sprintf(display, "%02d/%02d", curPage+1, pageNum);
		API_OsdStringDisplayExt(pos.x-24, pos.y-10+(hv.v - pos.y)/2, COLOR_WHITE, display, strlen(display), 0, STR_UTF8, 0);
		//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, curPage+1, pageNum);
	}
	else
	{
		if( get_pane_type() != 7 )
			API_SpriteClose(x, y, SPRITE_ScheduleBottom);
		API_SpriteDisplay_XY(GetIconXY(ICON_202_PageUp).x, GetIconXY(ICON_202_PageUp).y, SPRITE_HidePageTurning);
	}

}
//menu-BAR底图更改为另一种颜色
void DisplaySchedule2(int x, int y, int curPage, int pageNum)
{
	int posY;
	int overPage;
	char display[20];
	POS pos;
	SIZE hv;
	int barStart,barEnd,barLen;
	uint16 xsize, ysize1, ysize2;
	OSD_GetIconInfo(ICON_177_KeyBar1, &pos, &hv);
	Get_SpriteSize(SPRITE_ScheduleBottom, &xsize, &ysize1);
	Get_SpriteSize(SPRITE_ScheduleTop, &xsize, &ysize2);
	x = pos.x;
	y = pos.y;
	barStart = hv.v;
	barEnd = pos.y + ysize1 - ysize2 - (hv.v - pos.y);
	barLen = barEnd - barStart;

	if(pageNum > 1)
	{
		API_SpriteClose(GetIconXY(ICON_202_PageUp).x, GetIconXY(ICON_202_PageUp).y, SPRITE_HidePageTurning2);
		if( get_pane_type() != 7 )
			API_SpriteDisplay_XY(x, y, SPRITE_ScheduleBottom);
		if(pageNum-1 > barLen)
		{
			overPage = pageNum - 1 - barLen;
			if(curPage < barLen/2)
			{
				posY = barStart+curPage;
			}
			else if(curPage < barLen/2+overPage)
			{
				posY = barStart + barLen/2;
			}
			else
			{
				posY = barStart + curPage - overPage;
			}
		}
		else
		{
			if(barLen%(pageNum-1) && curPage <= barLen%(pageNum-1))
			{
				posY = barStart + curPage*(barLen/(pageNum-1)) +  curPage;
			}
			else
			{
				posY = barStart + curPage*(barLen/(pageNum-1)) + barLen%(pageNum-1);
			}
		}
		if( get_pane_type() != 7 )
			API_SpriteDisplay_XY(x+1, posY, SPRITE_ScheduleTop);

		OSD_GetIconInfo(ICON_201_PageDown, &pos, &hv);
		API_OsdStringClearExt(pos.x-24, pos.y-10+(hv.v - pos.y)/2, bkgd_w-pos.x+24, CLEAR_STATE_H);		
		sprintf(display, "%02d/%02d", curPage+1, pageNum);
		API_OsdStringDisplayExt(pos.x-24, pos.y-10+(hv.v - pos.y)/2, COLOR_WHITE, display, strlen(display), 0, STR_UTF8, 0);
		//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, curPage+1, pageNum);
	}
	else
	{
		if( get_pane_type() != 7 )
			API_SpriteClose(x, y, SPRITE_ScheduleBottom);
		API_SpriteDisplay_XY(GetIconXY(ICON_202_PageUp).x, GetIconXY(ICON_202_PageUp).y, SPRITE_HidePageTurning2);
	}

}


void UpdateOkSpriteNotify(void)
{
	API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h), SPRITE_CONFIRM);
	usleep(500000);
	API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h), SPRITE_CONFIRM);
}


