#include "MENU_public.h"
#include "MENU_005_CallScene.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_CallServer.h"	//czn_20181227
#include "obj_GetIpByNumber.h"
#include "obj_ip_mon_vres_map_table.h"
#include "MENU_103_CardInfo.h"
#include "MENU_100_CardList.h"


#define	ID_CARD_INFO_ICON_MAX	5
unsigned int idCardInfoItem = 0;
extern char** p_id_card_info;
extern unsigned int idCardListItem;
extern int card_list_cur_page;
extern int idCardtargetIp;
extern FuncMenuType idCardSubMenu;


static int card_info_del_confirm = -1;
static void ClearDeleteConfirmFlag(void)
{
	int confirmx,confirmy;
	if( get_pane_type() == 7 )
	{
		confirmx = 450;
		confirmy = 110+80*4;
	}
	else
	{
		confirmx = 620;
		confirmy = 105+68*4;
	}
	if(card_info_del_confirm >= 0)
	{
		API_SpriteClose(confirmx, confirmy, SPRITE_IF_CONFIRM);
	}
	card_info_del_confirm = -1;
}
void clear_card_info_display_zone(void)
{
	#if 0
	int i,x,y;
	for( i = 0; i<5; i++ )
	{
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
	}	
	#endif
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	InitMenuList(listInit);
}
void card_info_del_process(void)
{
	int len;
	IdCardDelReq_s id_card_del_req;
	IdCardDelRsp_s id_card_del_rsp;
	int confirmx,confirmy;
	if( get_pane_type() == 7 )
	{
		confirmx = 450;
		confirmy = 110+80*4;
	}
	else
	{
		confirmx = 620;
		confirmy = 105+68*4;
	}
	if(card_info_del_confirm < 0)
	{
		card_info_del_confirm = 0;
		API_SpriteDisplay_XY(confirmx, confirmy, SPRITE_IF_CONFIRM);
	}
	else
	{
		card_info_del_confirm = -1;
		API_SpriteClose(confirmx, confirmy, SPRITE_IF_CONFIRM);

		id_card_del_req.keyType = 1; // id
		id_card_del_req.num = 1; // 删除一张卡
		//memcpy( id_card_del_req.keyName, "<ID>",4 );
		memcpy(id_card_del_req.keyName,p_id_card_info[idCardListItem],4+10); // <ID>&card_num
		id_card_del_req.key[10] = 0;
		len = sizeof(id_card_del_rsp);
		printf("del card dat:%s\n",id_card_del_req.keyName);
		if( api_udp_io_server_send_req( idCardtargetIp ,CMD_RF_CARD_DEL_REQ,(char*)&id_card_del_req,sizeof(id_card_del_req), (char*)&id_card_del_rsp, &len ) == -1 )
		{
			printf("No response cmd = %#X\n",CMD_RF_CARD_DEL_REQ);
			BEEP_ERROR();
			return;
		}

		if( id_card_del_rsp.num == 1 )
		{
			BEEP_CONFIRM();
			EnterIdCardMenu(MENU_100_CardList,0);
		}
		else
		{
			BEEP_ERROR();
			return;
		}
	}
}

static void MenuListGetCardInfoPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	int i, index;
	int recordLen, unicode_len;
	char unicode_buf[200];
	char DisplayBuffer[50];
	char* p;
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < 5)
		{
			if(index == 0)
			{
				API_GetOSD_StringWithID(MESG_TEXT_ICON_279_RM_ADDR, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				memcpy(DisplayBuffer,GetSysVerInfo_BdRmMs(),8);
				unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,8,unicode_buf);
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
			}
			else if(index == 1) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_CARD_ID, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				p = get_id_card_key_position( p_id_card_info[idCardListItem], ID_CARD_KEY_ID, &recordLen );
				if( p != NULL )
				{
					unicode_len = 2*api_ascii_to_unicode(p,recordLen,unicode_buf);
					memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
				}
			}
			else if(index == 2) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_IPC_UserName, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				p = get_id_card_key_position( p_id_card_info[idCardListItem], ID_CARD_KEY_NAME, &recordLen );
				if( p != NULL )
				{
					unicode_len = 2*api_ascii_to_unicode(p,recordLen,unicode_buf);
					memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
				}
			}
			else if(index == 3) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_ICON_054_Date, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				p = get_id_card_key_position( p_id_card_info[idCardListItem], ID_CARD_KEY_DATE, &recordLen );
				if( p != NULL )
				{
					unicode_len = 2*api_ascii_to_unicode(p,recordLen,unicode_buf);
					memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
				}
			}
			else
			{
				API_GetOSD_StringWithID(MESG_TEXT_CARD_Delete, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

			}
			pDisp->dispCnt++;
		}

	}
}
void MENU_103_CardInfo_Init(int uMenuCnt)
{
	LIST_INIT_T listInit;
	API_MenuIconDisplaySelectOn(ICON_IdCard_List);
	listInit = ListPropertyDefault();
	if( get_pane_type() == 7 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 120;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 300;
	}	
	listInit.valEn = 1;
	listInit.listCnt = 5;
	listInit.fun = MenuListGetCardInfoPage;
	InitMenuList(listInit);
}


void MENU_103_CardInfo_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_IdCard_List);
	ClearDeleteConfirmFlag();
}

void MENU_103_CardInfo_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int index;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					//popDisplayLastMenu();
					ClearDeleteConfirmFlag();
					//clear_card_info_display_zone();
					idCardSubMenu = MENU_100_CardList;
					CardListShow(card_list_cur_page);
					//display_one_page_id_card(card_list_cur_page);
					//API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_IdCard_List, 1, 0);
					break;
				
				case ICON_047_Home:
					ClearDeleteConfirmFlag();
					GoHomeMenu();
					break;

				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					index = listIcon.mainIcon;
					if(index == 4)
					{
						card_info_del_process();
					}
					break;	
				#if 0
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
					idCardInfoItem = GetCurIcon() - ICON_007_PublicList1;
					
					if(idCardInfoItem >= ID_CARD_INFO_ICON_MAX)
					{
						return;
					}
					
					break;
				case ICON_011_PublicList5:
					card_info_del_process();
					break;
				#endif	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		
	}
}

#if 0
void MENU_103_CardInfo_Init(int uMenuCnt)
{
	int i,length;
	uint16 x,x1,y;
	char* p;
	SYS_VER_INFO_T sysInfo;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	API_MenuIconDisplaySelectOn(ICON_IdCard_List);
	
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	
	//API_OsdUnicodeStringDisplayWithIcon(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, ICON_IdCard_List, 1, DISPLAY_TITLE_WIDTH);
	
	clear_card_info_display_zone();

	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//x1 = x+ (DISPLAY_VALUE_X_DEVIATION - 150);
	//y = DISPLAY_LIST_Y+DISPLAY_DEVIATION_Y;

	 // rm
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_279_RM_ADDR, 1, (DISPLAY_VALUE_X_DEVIATION - 150));
	sysInfo = GetSysVerInfo();
	API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, sysInfo.bd_rm_ms, 8, 1, STR_UTF8, bkgd_w - x1);

	 // id
	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_CARD_ID, 1, (DISPLAY_VALUE_X_DEVIATION - 150));
	//API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, p_id_card_info[card_list_cur_page*ID_CARD_INFO_ICON_MAX + idCardListItem], 10, 1, STR_UTF8, MENU_SCHEDULE_POS_X - x1);
	p = get_id_card_key_position( p_id_card_info[card_list_cur_page*ID_CARD_INFO_ICON_MAX + idCardListItem], ID_CARD_KEY_ID, &length );
	if( p != NULL )
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, p, length, 1, STR_UTF8, bkgd_w - x1);
	}

	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_IPC_UserName, 1, (DISPLAY_VALUE_X_DEVIATION - 150)); // NAME
	p = get_id_card_key_position( p_id_card_info[card_list_cur_page*ID_CARD_INFO_ICON_MAX + idCardListItem], ID_CARD_KEY_NAME, &length );
	if( p != NULL )
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, p, length, 1, STR_UTF8, bkgd_w - x1);
	}

	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_054_Date, 1, (DISPLAY_VALUE_X_DEVIATION - 150)); // DATE
	p = get_id_card_key_position( p_id_card_info[card_list_cur_page*ID_CARD_INFO_ICON_MAX + idCardListItem], ID_CARD_KEY_DATE, &length );
	if( p != NULL )
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, p, length, 1, STR_UTF8, bkgd_w - x1);
	}

	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_CARD_Delete, 1, (DISPLAY_VALUE_X_DEVIATION - 150)); // DATE
	
}

void MENU_103_CardInfo_Exit(void)
{
	//printf("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz8\n");
	//API_MenuIconDisplaySelectOff(ICON_IdCard_Source);
	API_MenuIconDisplaySelectOff(ICON_IdCard_List);
	clear_card_info_display_zone();
	ClearDeleteConfirmFlag();
}
#endif