#include "MENU_095_AddressHealthCheck.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "MenuAddressHealthCheck_Business.h"

#define	AddrCheck_ICON_MAX	5
int addrCheckIconSelect;
int addrCheckPageSelect;
int addrCheckIndex;

extern SearchIpRspData searchOnlineListData;
#if 0
SearchIpRspData unhealthyDevice;
static int unhealthyFlag;
static int waitCheckFlag;
#endif
#if 0
void SingleOutUnhealthyDevice(void)
{
	int i, j;
	int addFlag;
	
	unhealthyDevice.deviceCnt = 0;

		
	for(i = 0; i < searchOnlineListData.deviceCnt; i++)
	{
		for(j = 0, addFlag = 0; j < searchOnlineListData.deviceCnt; j++)
		{
			if(i != j)
			{
				if( searchOnlineListData.data[i].Ip == searchOnlineListData.data[j].Ip || 
					!strcmp(searchOnlineListData.data[i].BD_RM_MS, searchOnlineListData.data[j].BD_RM_MS))
				{
					unhealthyDevice.data[unhealthyDevice.deviceCnt++] = searchOnlineListData.data[i];
					addFlag = 1;
					break;
				}
			}
		}

		if(addFlag == 0)
		{
			if( searchOnlineListData.data[i].Ip == inet_addr(GetSysVerInfo_IP()) || 
				!strcmp(searchOnlineListData.data[i].BD_RM_MS, GetSysVerInfo_BdRmMs()))
			{
				unhealthyDevice.data[unhealthyDevice.deviceCnt++] = searchOnlineListData.data[i];
			}		
		}
	}

	for (i = 0; i < unhealthyDevice.deviceCnt - 1; i++)     //外层循环控制趟数，总趟数为deviceCnt-1  
	{
		for (j = 0; j < unhealthyDevice.deviceCnt - 1 - i; j++)  	//内层循环为当前j趟数 所需要比较的次数  
		{
			if (atoi(unhealthyDevice.data[j].BD_RM_MS) > atoi(unhealthyDevice.data[j+1].BD_RM_MS))
			{
				SearchOneDeviceData tempData;

				tempData = unhealthyDevice.data[j+1];
				unhealthyDevice.data[j+1] = unhealthyDevice.data[j];
				unhealthyDevice.data[j] = tempData;
			}
		}
	}
}
#endif

static void DisplaySearchOnlinePageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;
	
	for(i = 0; i < AddrCheck_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		index = page*AddrCheck_ICON_MAX+i;
		if(index < unhealthyDevice.deviceCnt+1)
		{
			
			char display[200];
			char room[15] = {0};

			if(index == 0)
			{
				if(CheckIPExist())				//FOR_INDEXA
				{
					//snprintf(display, 200, "Error:IP exist!");
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_CheckIPRepeat, (get_pane_type()==7)? 0:1, hv.h-x);
				}
				else
				{
					//snprintf(display, 200, "Self IP address checking passed.");
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_CheckIPNoRepeat, (get_pane_type()==7)? 0:1, hv.h-x);
				}
				//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 2, STR_UTF8, MENU_SCHEDULE_POS_X-x);			
			}
			else
			{
				index--;
				
				get_device_addr_and_name_disp_str(0, unhealthyDevice.data[index].BD_RM_MS, NULL, NULL, unhealthyDevice.data[index].name, room);

				snprintf(display, 200, "%s %03d.%03d.%03d.%03d %02d:%02d",
					room,
					unhealthyDevice.data[index].Ip&0xFF, (unhealthyDevice.data[index].Ip>>8)&0xFF, (unhealthyDevice.data[index].Ip>>16)&0xFF, (unhealthyDevice.data[index].Ip>>24)&0xFF,
					unhealthyDevice.data[index].systemBootTime/3600, (unhealthyDevice.data[index].systemBootTime%3600)/60);
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), (get_pane_type()==7)? 0:1, STR_UTF8, hv.h-x);			
			}
		}
	}
	pageNum = (unhealthyDevice.deviceCnt+1)/AddrCheck_ICON_MAX + ((unhealthyDevice.deviceCnt+1)%AddrCheck_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}
 #if 0
void SetMenuWaitCheckFlag(int flag)
{
	waitCheckFlag = flag;
}

int GetMenuWaitCheckFlag(void)
{
	return waitCheckFlag;
}

void SetUnhealthyFlag(int flag)
{
	unhealthyFlag = flag;
}

int CheckIPExist(void)
{
	Dhcp_Autoip_CheckIP();
	SetMenuWaitCheckFlag(1);
	while(GetMenuWaitCheckFlag())
	{
		int i = 20;
		
		usleep(100*1000);
		if(i <= 0)
		{
			SetMenuWaitCheckFlag(0);
			SetUnhealthyFlag(0);
			break;
		}
	}

	return unhealthyFlag;
}
#endif

void MENU_095_AddressHealthCheck_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_098_AddressHealthCheck, 1, 0);
	
	if( GetLastNMenu() == MENU_015_INSTALL_SUB || GetLastNMenu() == MENU_108_QuickAccess||GetLastNMenu() ==MENU_NM_LanSetting)
	{

		addrCheckIconSelect = 0;
		addrCheckPageSelect = 0;
		
		BusySpriteDisplay(1);
		
		API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);
		SingleOutUnhealthyDevice();
		DisplaySearchOnlinePageIcon(addrCheckPageSelect);
		BusySpriteDisplay(0);
	}

	else
	{
		DisplaySearchOnlinePageIcon(addrCheckPageSelect);
	}
	
}

void MENU_095_AddressHealthCheck_Exit(void)
{

}

void MENU_095_AddressHealthCheck_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&addrCheckPageSelect, AddrCheck_ICON_MAX, unhealthyDevice.deviceCnt+1, (DispListPage)DisplaySearchOnlinePageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&addrCheckPageSelect, AddrCheck_ICON_MAX, unhealthyDevice.deviceCnt+1, (DispListPage)DisplaySearchOnlinePageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					addrCheckIconSelect = GetCurIcon() - ICON_007_PublicList1;
					addrCheckIndex = addrCheckPageSelect*AddrCheck_ICON_MAX + addrCheckIconSelect;
					
					if(addrCheckIndex < unhealthyDevice.deviceCnt+1 && addrCheckIndex != 0)
					{
						char temp[200] = {0};
						addrCheckIndex--;
						get_device_addr_and_name_disp_str(0, unhealthyDevice.data[addrCheckIndex].BD_RM_MS, NULL, NULL, unhealthyDevice.data[addrCheckIndex].name, temp);
					
						
						SetOnlineManageTitle(temp);
						SetOnlineManageMFG_SN(unhealthyDevice.data[addrCheckIndex].MFG_SN);
						SetSearchOnlineIp(unhealthyDevice.data[addrCheckIndex].Ip);
						SetOnlineManageNbr(unhealthyDevice.data[addrCheckIndex].BD_RM_MS);
						EnterOnlineManageMenu(MENU_034_ONLINE_MANAGE_INFO, 1);	
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


