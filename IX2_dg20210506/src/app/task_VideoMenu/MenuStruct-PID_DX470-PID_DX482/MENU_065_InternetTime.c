#include "MENU_public.h"
#include "MENU_065_InternetTime.h"
#include "task_RTC.h"

#define MAX_SET_NUM 	2

#define	InternetTime_ICON_MAX	5
int internetTimeIconSelect;
int internetTimePageSelect;

unsigned char timeZone;
unsigned char timeAutoUpdate;
char timeServer[50];


const InternetTimeIcon internetTimeIconTable[] = 
{
	{ICON_337_TimeServer, 				MESG_TEXT_ICON_337_TimeServer},
	{ICON_308_TimeZone, 				MESG_TEXT_ICON_308_TimeZone},
	{ICON_309_TimeAutoUpdate, 			MESG_TEXT_ICON_309_TimeAutoUpdate},
	{ICON_338_TimeUpdate,				MESG_TEXT_ICON_338_TimeUpdate},
};

const int internetTimeIconNum = sizeof(internetTimeIconTable)/sizeof(internetTimeIconTable[0]);

const char TimeServer[MAX_SET_NUM][33] = {"Input server", "131.188.3.220"};

static void DisplayInternetTimePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[50];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < InternetTime_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*InternetTime_ICON_MAX+i < internetTimeIconNum)
			{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, internetTimeIconTable[i+page*InternetTime_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(internetTimeIconTable[i+page*InternetTime_ICON_MAX].iCon)
			{
				case ICON_337_TimeServer:
					memset(timeServer, 0, 50);
					API_Event_IoServer_InnerRead_All(TIME_SERVER, (uint8*)&timeServer);
					API_OsdStringDisplayExt(x, y, COLOR_RED, timeServer, strlen(timeServer),1,STR_UTF8, hv.h - x);
					break;	

				case ICON_308_TimeZone:
					if(timeZone < 12)
						snprintf(display,11,"UTC-%02d:00",12 - timeZone);
					else if(timeZone > 12)
						snprintf(display,11,"UTC+%02d:00",timeZone - 12);
					else
						strcpy(display, "UTC");
					
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
				case ICON_309_TimeAutoUpdate:
					stringId = (timeAutoUpdate ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplay(x, y, COLOR_RED, stringId, 1, hv.h - x);
					break;	
					
			}
		}
	}
	pageNum = internetTimeIconNum/InternetTime_ICON_MAX + (internetTimeIconNum%InternetTime_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

//cao_20181020_s

void TimeZoneSet(int mode)
{
	char temp[20];
	
	timeZone = mode;
	sprintf(temp, "%d", timeZone);
	
	API_Event_IoServer_InnerWrite_All(TimeZone, temp);
	set_timezone(timeZone - 12);
}

void TimeAutoUpdateEnableSet(int mode)
{
	
	char temp[20];
	
	timeAutoUpdate = mode ? 1 : 0;
	sprintf(temp, "%d", timeAutoUpdate);

	API_Event_IoServer_InnerWrite_All(TimeAutoUpdateEnable, temp);
}

void TimeServerSet(int select)
{
	if(select == 0)
	{
		API_Event_IoServer_InnerWrite_All(TIME_SERVER, (uint8*)&timeServer);
	}
	else if(select < MAX_SET_NUM)
	{
		strcpy(timeServer, TimeServer[select]);
		API_Event_IoServer_InnerWrite_All(TIME_SERVER, (uint8*)&timeServer);
	}
}

int InputTimeServerSet(char* input)
{
	strcpy(timeServer, input);
	TimeServerSet(0);
	return 1;
}


void MENU_065_InternetTime_Init(int uMenuCnt)
{
	internetTimePageSelect = 0;
	internetTimeIconSelect = 0;
	char temp[20];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	API_Event_IoServer_InnerRead_All(TimeZone, temp);
	timeZone = atoi(temp);
	
	API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, temp);
	timeAutoUpdate = atoi(temp);

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_DateTimeSet, 1, 0);	
	
	API_MenuIconDisplaySelectOn(ICON_025_general);
	DisplayInternetTimePageIcon(internetTimePageSelect);
}

void MENU_065_InternetTime_Exit(void)
{
	
}

void MENU_065_InternetTime_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	int i, j, setValue;
	char temp[20];
	int ret1, ret2;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					internetTimeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(internetTimePageSelect*InternetTime_ICON_MAX+internetTimeIconSelect >= internetTimeIconNum)
					{
						return;
					}
					switch(internetTimeIconTable[internetTimePageSelect*InternetTime_ICON_MAX+internetTimeIconSelect].iCon)
					{
						case ICON_308_TimeZone:
							for(i = 0; i < 25; i++)
							{
								memset(publicSettingDisplay[i], 0, 61);
								if(i < 12)
								{
									publicSettingDisplay[i][0] = 18;
									publicSettingDisplay[i][1] = 'U';
									publicSettingDisplay[i][3] = 'T';
									publicSettingDisplay[i][5] = 'C';
									publicSettingDisplay[i][7] = '-';
									publicSettingDisplay[i][9] = '0'+(12-i)/10;
									publicSettingDisplay[i][11] = '0'+(12-i)%10;
									publicSettingDisplay[i][13] = ':';
									publicSettingDisplay[i][15] = '0';
									publicSettingDisplay[i][17] = '0';
								}
								else if(i == 12)
								{
									publicSettingDisplay[i][0] = 6;
									publicSettingDisplay[i][1] = 'U';
									publicSettingDisplay[i][3] = 'T';
									publicSettingDisplay[i][5] = 'C';
								}
								else
								{
									publicSettingDisplay[i][0] = 18;
									publicSettingDisplay[i][1] = 'U';
									publicSettingDisplay[i][3] = 'T';
									publicSettingDisplay[i][5] = 'C';
									publicSettingDisplay[i][7] = '+';
									publicSettingDisplay[i][9] = '0'+(i-12)/10;
									publicSettingDisplay[i][11] = '0'+(i-12)%10;
									publicSettingDisplay[i][13] = ':';
									publicSettingDisplay[i][15] = '0';
									publicSettingDisplay[i][17] = '0';
								}
							}
								
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_308_TimeZone, 25, timeZone, TimeZoneSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						
						case ICON_309_TimeAutoUpdate:
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							
							API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, temp);
							timeAutoUpdate = atoi(temp) ? 1 : 0;
							
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_338_TimeUpdate, 2, timeAutoUpdate, TimeAutoUpdateEnableSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						
						case ICON_337_TimeServer:
							for(setValue = 0, i = 0; i < MAX_SET_NUM; i++)
							{
								if(!strcmp(timeServer, TimeServer[i]))
								{
									setValue = i;
								}
								if(i==0)
								{
									API_GetOSD_StringWithID(MESG_TEXT_InputIPaddress, NULL, 0, NULL, 0, publicSettingDisplay[i]+1, &len);
										publicSettingDisplay[i][0] = len;
								}
								else
								{
									for(publicSettingDisplay[i][0] = strlen(TimeServer[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
									{
										publicSettingDisplay[i][j] = TimeServer[i][j/2];
									}
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_ICON_337_TimeServer, MAX_SET_NUM, setValue, TimeServerSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;	
						case ICON_338_TimeUpdate:
							BusySpriteDisplay(1);
							
							ret1 = -1;
							ret2 = -1;

							if(IfOnLan(inet_addr(timeServer)))
							{
								ret2 = SetTimeServerRequest(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), 1, timeServer);
								if(ret2 != -1)
								{
									API_Event_IoServer_InnerRead_All(TimeZone, temp);
									timeZone = atoi(temp);
									API_DisableOsdUpdate();
									DisplayInternetTimePageIcon(internetTimePageSelect);
									API_EnableOsdUpdate();
								}
							}

							if(inet_addr(GetSysVerInfo_IP()) != inet_addr(timeServer))
							{
								ret1 = sync_time_from_sntp_server(inet_addr(timeServer), timeZone-12);
							}
							
							if(ret1 != -1 || ret2 != -1)
							{
								BEEP_CONFIRM();
							}
							else
							{
								BEEP_ERROR();
							}
							
							BusySpriteDisplay(0);
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


