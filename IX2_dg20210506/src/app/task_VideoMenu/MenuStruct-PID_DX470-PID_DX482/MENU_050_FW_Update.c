#include "MENU_050_FW_Update.h"

#define	FW_UPDATE_ICON_MAX		5
int fwUpdateIconSelect;
int fwUpdatePageSelect;
char fwUpdateCode[MAX_STR];
char fwUpdateServer[MAX_STR];

const FW_UpdateIcon fwUpdateIconTable[] = 
{
	{ICON_310_FW_UpdateCode,					MESG_TEXT_ICON_310_FW_UpdateCode},    
	{ICON_311_FW_UpdateServer,					MESG_TEXT_ICON_311_FW_UpdateServer},
	{ICON_312_FW_UpdateStart,					MESG_TEXT_ICON_312_FW_UpdateStart},
};

const int fwUpdateIconNum = sizeof(fwUpdateIconTable)/sizeof(fwUpdateIconTable[0]);

static void DisplayFWUpdatePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[11];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < FW_UPDATE_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*FW_UPDATE_ICON_MAX+i < fwUpdateIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, fwUpdateIconTable[i+page*FW_UPDATE_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(fwUpdateIconTable[i+page*FW_UPDATE_ICON_MAX].iCon)
			{
				case ICON_310_FW_UpdateCode:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, fwUpdateCode, strlen(fwUpdateCode), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_311_FW_UpdateServer:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, fwUpdateServer, strlen(fwUpdateServer), 1, STR_UTF8, hv.h - x);
					break;
			}
		}
	}
	pageNum = fwUpdateIconNum/FW_UPDATE_ICON_MAX + (fwUpdateIconNum%FW_UPDATE_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void MENU_050_FW_Update_Init(int uMenuCnt)
{
	int x, y, i, j;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	fwUpdateIconSelect = 0;
	fwUpdatePageSelect = 0;

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_FW_Update,1, 0);
	DisplayFWUpdatePageIcon(fwUpdatePageSelect);
	
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
}

void MENU_050_FW_Update_Exit(void)
{
	
}

void MENU_050_FW_Update_Process(void* arg)
{

/*

	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 setValue;
	int i, x, y;
	DevReg_Info data;
	int ret;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&fwUpdatePageSelect, FW_UPDATE_ICON_MAX, fwUpdateIconNum, (DispListPage)DisplayFWUpdatePageIcon);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&fwUpdatePageSelect, FW_UPDATE_ICON_MAX, fwUpdateIconNum, (DispListPage)DisplayFWUpdatePageIcon);
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					fwUpdateIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(fwUpdatePageSelect*FW_UPDATE_ICON_MAX+fwUpdateIconSelect >= fwUpdateIconNum)
					{
						return;
					}
					
					switch(fwUpdateIconTable[fwUpdatePageSelect*FW_UPDATE_ICON_MAX+fwUpdateIconSelect].iCon)
					{
						case ICON_310_FW_UpdateCode:
							EnterKeypadMenu(KEYPAD_NUM, INPUT_FW_UPDATE_CODE, fwUpdateCode, MAX_STR-1, COLOR_WHITE, fwUpdateCode, 1);
							break;
						case ICON_311_FW_UpdateServer:
							EnterKeypadMenu(KEYPAD_NUM, INPUT_FW_UPDATE_SERVER, fwUpdateServer, MAX_STR-1, COLOR_WHITE, fwUpdateServer, 1);
							break;
						case ICON_312_FW_UpdateStart:
							FwUpdateProcess(fwUpdateCode);
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}

*/
	
}

void FwUpdatePercentageDisplay(char percentage)
{
	int x, y;
	char display[11];
	POS pos;
	SIZE hv;

	if(GetCurMenuCnt() == MENU_050_FW_UPDATE)
	{
		OSD_GetIconInfo(ICON_009_PublicList3, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X+(hv.h - pos.x)/2;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X+250;
		//y = DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		snprintf(display, 11, "%d%%    ", percentage);
		API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
	}
}

int FwUpdateProcess(char* updateCode)
{
	#define sd_fw_update_path		"/mnt/sdcard/Ix471_FwUpdate"

	char sd_fw_update_file[200];
	char fileName[200];

	struct dirent *entry;
    DIR *dir;
		
	if(Judge_SdCardLink() == 0)
	{
		BEEP_ERROR();
		return -2;
	}	
	
    dir = opendir(sd_fw_update_path);
	if(dir == NULL)
	{
		BEEP_ERROR();
		return -2;
	}
	
	snprintf(fileName,200,"IX471_%s.zip", updateCode);
    while ((entry=readdir(dir))!=NULL)
	{
		if(strstr(entry->d_name, fileName)  != NULL)
		{
			snprintf(sd_fw_update_file,200,"%s/%s",sd_fw_update_path,entry->d_name);
			break;
		}
	}
		
	closedir(dir);

	if(entry == NULL)
	{
		BEEP_ERROR();
		return -2;
	}	
	
	BEEP_CONFIRM();
	//CloseOneMenu();
	API_LedDisplay_FwUpdate();		//czn_20170926		//API_LED_POWER_FLASH_IRREGULAR();//API_LED_MENU_FLASH_SLOW();
	
	int update_result = start_updatefile_and_reboot_forsdcard(sd_fw_update_file);
	
	API_LedDisplay_FwUpdateFinish();		//czn_20170926			API_LED_POWER_ON();
	if(update_result == 0)
	{
		BEEP_CONFIRM();
		usleep(1000000);
		system("reboot");
		return 0;
	}
	else if(update_result == -1)
	{
		BEEP_ERROR();
		return -1;
	}
	
	BEEP_CONFIRM();
	
	return 0;
}


