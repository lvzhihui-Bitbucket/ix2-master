#include "MENU_public.h"
#include "MENU_023_CallRecordList.h"
#include "obj_call_record.h"

#define	CALL_RECORD_ICON_MAX	4
int callRecordIconSelect;
int callRecordSubIcon;

void MENU_004_CallRecord_Init(int uMenuCnt)
{
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		callRecordSubIcon = ICON_017_MissedCalls;
	}
	
	switch(callRecordSubIcon)
	{
		case ICON_018_IncomingCalls:
			set_call_record_page_type(CALL_INCOMING);
			MENU_023_CallRecordList_Init(uMenuCnt);
			break;
		case ICON_019_OutgoingCalls:
			set_call_record_page_type(CALL_OUTGOING);
			MENU_023_CallRecordList_Init(uMenuCnt);
			break;
		case ICON_020_playback:
			SetVideoRecordPageType(VIDEO_PLAYLIST);
			MENU_024_VideoRecordList_Init(uMenuCnt);
			break;
		// zfz_20190601
		case ICON_CallRecordDelAll:
			set_call_record_page_type(CALL_REC_DEL_ALL);
			MENU_023_CallRecordList_Init(uMenuCnt);
			break;
		default :
			callRecordSubIcon = ICON_017_MissedCalls;
			call_record_allmiss_mark_read();
			API_MenuIconDisplaySelectOn(ICON_017_MissedCalls);
			set_call_record_page_type(CALL_MISSED);
			MENU_023_CallRecordList_Init(uMenuCnt);
			API_MsSyncCallRecordCheckedMissCall();	//czn_20190221
			break;
			
	}
}

void MENU_004_CallRecord_Exit(void)
{
}

void MENU_004_CallRecord_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint16 missedCall;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					switch(GetCurIcon())
					{
						case ICON_020_playback:
							SetVideoRecordPageType(VIDEO_PLAYLIST);
							StartInitOneMenu(MENU_004_CALL_RECORD,MENU_024_VIDEO_RECORD_LIST,0);
							break;
						case ICON_017_MissedCalls:
							call_record_allmiss_mark_read();
							set_call_record_page_type(CALL_MISSED);
							StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
							API_MsSyncCallRecordCheckedMissCall();	//czn_20190221
							break;
						case ICON_018_IncomingCalls:
							set_call_record_page_type(CALL_INCOMING);
							StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
							break;
						case ICON_019_OutgoingCalls:
							set_call_record_page_type(CALL_OUTGOING);
							StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
							break;
					}
					break;
					
				case KEY_UP:
					PublicUpProcess(&callRecordIconSelect, CALL_RECORD_ICON_MAX);					
					break;
				case KEY_DOWN:
					PublicDownProcess(&callRecordIconSelect, CALL_RECORD_ICON_MAX);					
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_020_playback:
					if(callRecordSubIcon != ICON_020_playback)
					{
						callRecordSubIcon = ICON_020_playback;
						SetVideoRecordPageType(VIDEO_PLAYLIST);
						StartInitOneMenu(MENU_004_CALL_RECORD,MENU_024_VIDEO_RECORD_LIST,0);
					}
					break;
				case ICON_017_MissedCalls:
					if(callRecordSubIcon != ICON_017_MissedCalls)
					{
						callRecordSubIcon = ICON_017_MissedCalls;
						call_record_allmiss_mark_read();
						set_call_record_page_type(CALL_MISSED);
						StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
						API_MsSyncCallRecordCheckedMissCall();	//czn_20190221
					}
					break;
				case ICON_018_IncomingCalls:
					if(callRecordSubIcon != ICON_018_IncomingCalls)
					{
						callRecordSubIcon = ICON_018_IncomingCalls;
						set_call_record_page_type(CALL_INCOMING);
						StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
					}
					break;
				case ICON_019_OutgoingCalls:
					if(callRecordSubIcon != ICON_019_OutgoingCalls)
					{
						callRecordSubIcon = ICON_019_OutgoingCalls;
						set_call_record_page_type(CALL_OUTGOING);
						StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
					}
					break;
				// zfz_20190601
				case ICON_CallRecordDelAll:
					if(callRecordSubIcon != ICON_CallRecordDelAll)
					{
						callRecordSubIcon = ICON_CallRecordDelAll;
						set_call_record_page_type(CALL_REC_DEL_ALL);
						StartInitOneMenu(MENU_004_CALL_RECORD, MENU_023_CALL_RECORD_LIST,0);
					}
					break;

				default:
					switch(callRecordSubIcon)
					{
						case ICON_020_playback:
							MENU_024_VideoRecordList_Process(arg);
							break;
						case ICON_017_MissedCalls:
						case ICON_018_IncomingCalls:
						case ICON_019_OutgoingCalls:
						case ICON_CallRecordDelAll: // zfz_20190601
							MENU_023_CallRecordList_Process(arg);
							break;
					}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


