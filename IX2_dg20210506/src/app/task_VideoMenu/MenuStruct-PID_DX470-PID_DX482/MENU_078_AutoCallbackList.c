#include "MENU_public.h"
#include "task_monitor.h"	
#include "onvif_discovery.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_business_manage.h"		//czn_20190107
#include "obj_GetIpByNumber.h"
#include "task_CallServer.h"

//extern one_vtk_table* videoResourceTable;

//#define AutoCallback_LIST_FILE_NAME					"/mnt/nand1-2/rid1014_VideoResource.csv"
//#define MON_LIST_BAK_FILE_NAME				"/mnt/nand1-2/rid1014_VideoResource.csv.bak"

#define	AutoCallbackList_ICON_MAX	5
int AutoCallbackListIconSelect;
int AutoCallbackListPageSelect;
int AutoCallbackListState;
int AutoCallbackListIndex;
char AutoCallbackListName[41];
//extern IPC_RECORD saveIPCMonList; 





void DisplayOneAutoCallbackList(int index, int x, int y, int color, int fnt_type, int width)
{
	char display[51];
	int monResNum;
		
	
	monResNum = Get_MonRes_Temp_Table_Num();

	if(index < monResNum)
	{
		char name_temp[41];
		char bdRmMs[11];
		char displayRoomNumber[11];

	
		if(Get_MonRes_Temp_Table_Record(index,name_temp,bdRmMs, NULL) == 0)
		{
			//SearchRoomNbrToDisplay(TYPE_DS, bdRmMs, displayRoomNumber);
			//snprintf(display, 51, "DS%s %s", displayRoomNumber, name_temp);
			get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
		}
	}


}

void DisplayOnePageAutoCallbackList(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;

	int monres_num;
	POS pos;
	SIZE hv;

	
	monres_num = Get_MonRes_Temp_Table_Num();
	

	//API_DisableOsdUpdate();

	list_start = page*AutoCallbackList_ICON_MAX;
	
	for( i = 0; i < AutoCallbackList_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if((list_start+i) < monres_num)
		{
			//DisplayOneMonlist(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, DISPLAY_ICON_X_WIDTH);
			DisplayOneAutoCallbackList(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, val_x - x);
		}
		
	}
	
	
	maxPage = (monres_num%AutoCallbackList_ICON_MAX) ? (monres_num/AutoCallbackList_ICON_MAX+1) : (monres_num/AutoCallbackList_ICON_MAX);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

//const char* create_mon_qrcode(char* qrcode, int maxLen);

void MENU_078_AutoCallbackList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	AutoCallbackListIconSelect = 0;
	AutoCallbackListPageSelect = 0;
	//AutoCallbackListState = ICON_203_Monitor;
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_AutoCallbackList,1, 0);

	if(Get_MonRes_Num() == 0 && Get_MonRes_Temp_Table_Num() == 0)
	{
		//popDisplayLastMenu();
		//return;
		BusySpriteDisplay(1);
		AddMonRes_Temp_TableBySearching();
		BusySpriteDisplay(0);
		
	}

	//qrenc_draw_bitmap(create_mon_qrcode(qrcode, 1000),0,215,3,218-0,480-215);

	
	DisplayOnePageAutoCallbackList(AutoCallbackListPageSelect);

}


void MENU_078_AutoCallbackList_Exit(void)
{
	
}

void MENU_078_AutoCallbackList_Process(void* arg)
{

#if 1
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					AutoCallbackListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					AutoCallbackListIndex = AutoCallbackListPageSelect*AutoCallbackList_ICON_MAX + AutoCallbackListIconSelect;

					if(AutoCallbackListIndex >= Get_MonRes_Temp_Table_Num())
					{
						return;
					}
					
					if(ReqSelectDs_Callback(AutoCallbackListIndex)!= 0)
					{
						BEEP_ERROR();
						return;
					}
					
					BEEP_CONFIRM();
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&AutoCallbackListPageSelect, AutoCallbackList_ICON_MAX, Get_MonRes_Temp_Table_Num(), (DispListPage)DisplayOnePageAutoCallbackList);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&AutoCallbackListPageSelect, AutoCallbackList_ICON_MAX, Get_MonRes_Temp_Table_Num(), (DispListPage)DisplayOnePageAutoCallbackList);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}


	#endif
}


int ReqSelectDs_Callback(int index)
{
	char bdRmMs[11];
	GetIpRspData data;
	Call_Dev_Info target_dev;
	int i;
	if(Get_MonRes_Temp_Table_Record(index,NULL,bdRmMs, NULL) != 0)
	{
		return -1;
	}	
	
	if(API_GetIpNumberFromNet(bdRmMs, NULL, NULL, 2, 1, &data) != 0)
	{
		
		return -1;
	}	
	if(api_nm_if_judge_include_dev(data.BD_RM_MS[0],data.Ip[0])==0)
	{
		for(i=0;i<data.cnt;i++)
		{
			if(api_nm_if_judge_include_dev(data.BD_RM_MS[i], data.Ip[i])==1)
			{
				 data.Ip[0]= data.Ip[i];
				 memcpy(data.BD_RM_MS[0], data.BD_RM_MS[i],11);
				 break;
			}
		}
		if(i>=data.cnt)
			return -1;
	}
	memset(&target_dev,0,sizeof(Call_Dev_Info));
	strcpy(target_dev.bd_rm_ms,GetSysVerInfo_BdRmMs());
	target_dev.ip_addr = inet_addr(GetSysVerInfo_IP());
	
	if(Send_CmdCallbackReq(data.Ip[0],target_dev)!=0)
	{
		return -1;
	}

	return 0;
}
void Start_AutoCallback_Process(void)
{
	int ds_nums;
	
	
	AddMonRes_Temp_TableBySearching();
	ds_nums = Get_MonRes_Temp_Table_Num();
	
	printf("!!!!!!Start_AutoCallback_Process ds_nums = %d\n",ds_nums);
	
	if(ds_nums == 0)
	{
		BEEP_ERROR();
		return;
	}
	if(ds_nums == 1)
	{
		
		if(ReqSelectDs_Callback(0)!= 0)
		{
			BEEP_ERROR();
			return;
		}
		
		BEEP_CONFIRM();
		return;
	}

	if(ds_nums > 1)
	{
		StartInitOneMenu(MENU_078_AutoCallbackList,0,1);
	}
}


