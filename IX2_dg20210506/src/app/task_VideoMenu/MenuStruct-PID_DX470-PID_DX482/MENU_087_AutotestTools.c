//20181017
#include "MENU_public.h"

#define	AutoTestTools_LIST_ICON_MAX		5

int AutoTestToolsIconSelect;
int AutoTestToolsPageSelect;
int AutoTestToolsIndex;
int AutoTestLogClearConfirm = 0;
int confirm_index;

const IconAndText_t AutoTestToolsIconTable[] = 
{
	{ICON_Autotest,			MESG_TEXT_ICON_Autotest},    
	{ICON_AutotestStatistics,			MESG_TEXT_ICON_AutotestStatistics},
	{ICON_AutotestLogClear, 				MESG_TEXT_ICON_AutotestLogClear},
	{ICON_ViewAutotestLog, 				MESG_TEXT_ICON_ViewAutotestLog},
	{ICON_AutotestLogCopy, 				MESG_TEXT_ICON_AutotestLogCopy},
	//{ICON_330_ViewRegSipPhone, 				MESG_TEXT_ICON_330_ViewRegSipPhone},
};
const int AutoTestToolsIconNum = sizeof(AutoTestToolsIconTable)/sizeof(AutoTestToolsIconTable[0]);


static void DisplayAutoTestToolsPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	uint16 monitorTime;
	char display[20];
	int stringId;
	uint8 temp;
	POS pos;
	SIZE hv;
	
	for(i = 0; i < AutoTestTools_LIST_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*AutoTestTools_LIST_ICON_MAX+i < AutoTestToolsIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, AutoTestToolsIconTable[i+page*AutoTestTools_LIST_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(AutoTestToolsIconTable[i+page*AutoTestTools_LIST_ICON_MAX].iCon)
			{
				default:
					break;
			}
		}
	}
	pageNum = AutoTestToolsIconNum/AutoTestTools_LIST_ICON_MAX + (AutoTestToolsIconNum%AutoTestTools_LIST_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}




void MENU_087_AutoTestTools_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_AutotestTools,1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);	//czn_20181027
	//if(GetLastNMenu() == MENU_009_WIRELESS)		//czn_20181027
	{
		AutoTestToolsIconSelect = 0;
		AutoTestToolsPageSelect= 0;
	}
	DisplayAutoTestToolsPageIcon(AutoTestToolsPageSelect);
	AutoTestLogClearConfirm = 0;
}


void MENU_087_AutoTestTools_Exit(void)
{
	
}

void MENU_087_AutoTestTools_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	uint8 temp;
	time_t t;
	struct tm *tblock; 
	char tempChar[100];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					AutoTestToolsIconSelect = GetCurIcon() - ICON_007_PublicList1;
					AutoTestToolsIndex = AutoTestToolsPageSelect*AutoTestTools_LIST_ICON_MAX + AutoTestToolsIconSelect;
					
					if(AutoTestToolsIndex >= AutoTestToolsIconNum)
					{
						return;
					}
					
					if(AutoTestToolsIconTable[AutoTestToolsIndex].iCon != ICON_AutotestLogClear)
					{
						if(AutoTestLogClearConfirm)
						{
							AutoTestLogClearConfirm = 0;
							API_SpriteClose(640, 85+65*confirm_index, SPRITE_IF_CONFIRM);
						}
					}
					
					switch(AutoTestToolsIconTable[AutoTestToolsIndex].iCon)
					{
						case ICON_Autotest:
							StartInitOneMenu(MENU_083_Autotest,0,1);
							break;
						case ICON_AutotestStatistics:
							StartInitOneMenu(MENU_086_CallTestStat,0,1);
							break;
						case ICON_AutotestLogClear:
							if(!AutoTestLogClearConfirm)
							{
								AutoTestLogClearConfirm = 1;
								API_SpriteDisplay_XY(640, 85+65*AutoTestToolsIconSelect, SPRITE_IF_CONFIRM);
								confirm_index = AutoTestToolsIconSelect;
							}
							else
							{
								AutoTestLogClearConfirm = 0;
								API_SpriteClose(640, 85+65*AutoTestToolsIconSelect, SPRITE_IF_CONFIRM);
								
								BusySpriteDisplay(1);
								AutotestLog_Stat_Clear();
								BusySpriteDisplay(0);
								UpdateOkSpriteNotify();
							}
							break;
						case ICON_ViewAutotestLog:
							StartInitOneMenu(MENU_085_AutotestLogs,0,1);
							break;
						case ICON_AutotestLogCopy:
							if(Judge_SdCardLink())
							{
								BusySpriteDisplay(1);
								//snprintf(tempChar,100, "cp -R %s %s\n", JPEG_STORE_DIR, DISK_SDCARD);
								//system(tempChar);
									
								t = time(NULL); 
								tblock=localtime(&t);

								snprintf( tempChar,100,"/mnt/sdcard/Autotest_%02d%02d%02d%02d%02d%02d.log",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
								CopyLogFile(tempChar);
								sync();
								BusySpriteDisplay(0);
								UpdateOkSpriteNotify();
							}
							else
							{
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								sleep(1);
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
							}
							break;
					}
					
					break;
				case ICON_201_PageDown:
					if(AutoTestLogClearConfirm)
					{
						AutoTestLogClearConfirm = 0;
						API_SpriteClose(640, 85+65*confirm_index, SPRITE_IF_CONFIRM);
					}
					PublicPageDownProcess(&AutoTestToolsPageSelect, AutoTestTools_LIST_ICON_MAX, AutoTestToolsIconNum, (DispListPage)DisplayAutoTestToolsPageIcon);
					break;			
				case ICON_202_PageUp:
					if(AutoTestLogClearConfirm)
					{
						AutoTestLogClearConfirm = 0;
						API_SpriteClose(640, 85+65*confirm_index, SPRITE_IF_CONFIRM);
					}
					PublicPageUpProcess(&AutoTestToolsPageSelect, AutoTestTools_LIST_ICON_MAX, AutoTestToolsIconNum, (DispListPage)DisplayAutoTestToolsPageIcon);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




