/**
  ******************************************************************************
  * @file    obj_Survey_Keyboard.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the functions of the obj_Survey_Keyboard
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/

#include "MENU_Keyboard.h"
#include "task_VideoMenu.h"
#include "MENU_public.h"

KbdProperty kbd_property;
extern OS_TIMER timer_sprite_ctrl;

extern const KeyProperty ASC_Char_Tab[];
extern const uint8 ASC_CHAR_TAB_NUMBERS;
extern const KeyProperty ASC_NUM_Tab[];
extern const uint8 ASC_NUM_TAB_NUMBERS;

extern int KeypadTitleX, KeypadTitleY, KeypadAscX;

/******************************************************************************
 * @fn      OneKeyValue()
 *
 * @brief   输入一个按键
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char OneKeyValue(unsigned char Keypad_type,int icon)   
{
    uint8 i;
    uint8 key_value = 0;
    if(Keypad_type == KEYPAD_CHAR)                                                    //字母输入
    {
        for(i=0;i<ASC_CHAR_TAB_NUMBERS;i++)                                            //查表找出哪个按键按下
        {
 			if(icon == ASC_Char_Tab[i].icon)
			{
	            key_value = ASC_Char_Tab[i].key_value;                                 //找到键值
	            if((kbd_property.capsLock != 0) && (key_value>96) && (key_value < 123))//如果大写锁定并且按下的是26个字母，转换成大写字母
	            {
	                if(kbd_property.capsLock == 1)                                     //判断大写锁定只用一次
	                {
	                    kbd_property.capsLock = 0;
						API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, "abc   ", strlen("abc   "),1,STR_UTF8, 0);
	                }
	                key_value = key_value - 32;
	            }
	            break;                                                                 //结束循环
			}
        }
    }
    else if(Keypad_type == KEYPAD_NUM)                                                //数字输入
    {
        for(i=0;i<ASC_NUM_TAB_NUMBERS;i++)                   
        {
            if(icon == ASC_NUM_Tab[i].icon)
            {
	            key_value = ASC_NUM_Tab[i].key_value;
	            break;
        	}
		}
    }
    else if(Keypad_type == KEYPAD_EDIT)
    {
        key_value = SET_HIGHLIGHT_KEY_VALUE;                                           //高亮
    }
	else
	{
		for(i=0;i<ASC_CHAR_TAB_NUMBERS;i++) 										   //查表找出哪个按键按下
		{
			if(icon == ASC_Char_Tab[i].icon)
			{
				key_value = ASC_Char_Tab[i].key_value;								   //找到键值
				if((kbd_property.capsLock != 0) && (key_value>96) && (key_value < 123))//如果大写锁定并且按下的是26个字母，转换成大写字母
				{
					if(kbd_property.capsLock == 1)									   //判断大写锁定只用一次
					{
						kbd_property.capsLock = 0;
						API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, "abc   ", strlen("abc   "),1,STR_UTF8, 0);
					}
					key_value = key_value - 32;
				}
				break;																   //结束循环
			}
		}

		for(i=0;i<ASC_NUM_TAB_NUMBERS;i++)					 
		{
			if(icon == ASC_NUM_Tab[i].icon)
			{
				key_value = ASC_NUM_Tab[i].key_value;
				break;
			}
		}

	}
    return key_value;
}




/******************************************************************************
 * @fn      API_KeyboardInit()
 *
 * @brief   初始化键盘属性
 *
 * @param   startRow        显示起始行  范围0~MAX_DISPLAY_ROW-1
 * @param   StartCol        显示起始列  范围0~MAX_DISPLAY_COL-1
 * @param   dispMaxRow      显示最大行  范围1~MAX_DISPLAY_ROW
 * @param   dispMaxCol      显示最大列  范围1~MAX_DISPLAY_COL
 * @param   color           显示颜色    范围0~15
 *
 * @return  1--成功/ 0--失败
 *****************************************************************************/
unsigned char API_KeyboardInit(unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
                               unsigned short dispMaxCol, int color)
{
    //编辑框大小是:MAX_DISPLAY_ROW * MAX_DISPLAY_COL
    if((startRow >= MAX_DISPLAY_ROW) || (StartCol >= MAX_DISPLAY_COL) || \
       ((startRow + dispMaxRow) > MAX_DISPLAY_ROW) || ((StartCol + dispMaxCol) > MAX_DISPLAY_COL)/* || (color > 15)*/)
    {
        return 0;
    }
    else
    {
        kbd_property.dispStartCol = StartCol;
        kbd_property.dispStartRow = startRow;
        kbd_property.dispMaxCol = dispMaxCol;
        kbd_property.dispMaxRow = dispMaxRow;        
        kbd_property.dispColor = color;
        kbd_property.capsLock = 0;                                  //默认小写
        kbd_property.dispFullFlag =0;                               //显示屏未满
        kbd_property.cursor = 1;                                    //显示光标
        kbd_property.char_num = 0;                                  //inputbuf中的数据是0
        if(dispMaxRow * dispMaxCol <= MAX_INPUTBUF_SIZE)
        {
            kbd_property.inputSize = dispMaxRow * dispMaxCol;       //输入数据限度
        }
        else
        {
            kbd_property.inputSize = MAX_INPUTBUF_SIZE;             //输入数据限度
        }
        return 1;
    }
}

/******************************************************************************
 * @fn      API_KeyboardInputStart()
 *
 * @brief   开始输入
 *
 * @param   pString     开始输入字符指针，字符大小不能超过MAX_INPUTBUF_SIZE
 * @param   highLight   开始是否高亮  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_KeyboardInputStart(unsigned char* pString, unsigned char highLight)
{
    uint8 i = 0; 
	
	if(pString != NULL)
	{
		for(i=0;(*pString != '\0') && (i < kbd_property.inputSize);i++,pString++)
	    {
	        kbd_property.inputbuf[i] = *pString;                                                 //把数据放到inputbuf中
	    }
	}

    kbd_property.char_num = i;                                                               //buffer中数据的字节数
    kbd_property.inputbuf[kbd_property.char_num] = '\0';                                     //结尾添加结束符
    DisplayAllScreen(kbd_property.dispColor, highLight,!kbd_property.highLight); //显示
}

/******************************************************************************
 * @fn      API_KeyboardSetHighLight()
 *
 * @brief   设置高亮或取消高亮
 *
 * @param   highLight  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_KeyboardSetHighLight(unsigned char highLight)
{                                                                         //清屏
    DisplayAllScreen(kbd_property.dispColor, highLight, kbd_property.cursor);      //显示
}

/******************************************************************************
 * @fn      API_KeyboardInputOneKey()
 *
 * @brief   触摸输入
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char API_KeyboardInputOneKey(unsigned char Keypad_type,int icon )
{
    unsigned char key_value;
    key_value = OneKeyValue(Keypad_type, icon);
    if((key_value != RETURN_KEY_VALUE) && (key_value != 0) && (key_value != OK_KEY_VALUE) &&
       (key_value != TURN_TO_NUM_KEY_VALUE) && (key_value != TURN_TO_CHAR_KEY_VALUE))  
    {
        KeyboardInputProcess(key_value);
    }
    return key_value;
}

/******************************************************************************
 * @fn      KeyboardInputProcess()
 *
 * @brief   键盘输入处理
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  none
 *****************************************************************************/
void KeyboardInputProcess(unsigned char key_value)
{
    if(key_value == BACKSPACE_KEY_VALUE)                                                        //输入退格键按下
    {
        if(kbd_property.highLight)                                                              //高亮选中，全部删除buffer
        {
            kbd_property.inputbuf[0] = '\0';
            kbd_property.highLight = 0;
            kbd_property.char_num = 0;
        }
        else if(kbd_property.char_num>0)                                                        //高亮没有选中，buffer中数据不为0
        {
            kbd_property.char_num--;
            kbd_property.inputbuf[kbd_property.char_num] = '\0';
        }
    }
    else if(key_value == SET_HIGHLIGHT_KEY_VALUE)                                               //如果点一下编辑框，高亮属性反选
    {
		DisplayAllScreen(kbd_property.dispColor, !kbd_property.highLight, kbd_property.cursor);	//显示
		return;
    }
    else if(key_value == CAPSLOCK_KEY_VALUE)                                                    //大小写切换，改变大小写锁定属性
    {
        kbd_property.capsLock++;
        if(kbd_property.capsLock > 2)
        {
            kbd_property.capsLock = 0;
        }    
    }
    else                                                                                             //如果输入其他，把输入值放到buffer中
    { 
        if(kbd_property.highLight)
        {
            if((kbd_property.dispMaxRow != 1) || (key_value != '\r'))
            {
                kbd_property.highLight = 0;
                kbd_property.char_num = 0;
                kbd_property.inputbuf[kbd_property.char_num] = key_value;
                kbd_property.char_num++;
                kbd_property.inputbuf[kbd_property.char_num] = '\0';                                 //数据后面加结束符
            }
        }
        else
        {
            if((kbd_property.char_num < kbd_property.inputSize) && (kbd_property.dispFullFlag != 2)) //输入字符不超过限制且屏幕未满允许输入
            {
                if(key_value == '\r')                                                                //输入回车键
                {   
                    if(kbd_property.dispFullFlag == 0)
                    {   
                        kbd_property.inputbuf[kbd_property.char_num] = key_value;
                        kbd_property.char_num++;
                        kbd_property.inputbuf[kbd_property.char_num] = '\0';                         //数据后面加结束符
                    }
                }
                else                                                                                 //输入其他字符
                {
                    kbd_property.inputbuf[kbd_property.char_num] = key_value;
                    kbd_property.char_num++;
                    kbd_property.inputbuf[kbd_property.char_num] = '\0';                             //数据后面加结束符
                } 
            }
        }
    }
	DisplayAllScreen(kbd_property.dispColor,kbd_property.highLight,kbd_property.cursor);//显示
}
/******************************************************************************
 * @fn      API_KeyboardInputEnd()
 *
 * @brief   输入结束，返回字符串指针
 *
 * @param   none
 *
 * @return  kbd_property.inputbuf 字符串指针
 *****************************************************************************/
unsigned char* API_KeyboardInputEnd(void)
{
    DisplayAllScreen(kbd_property.dispColor,kbd_property.highLight,0);                 //更新显示
    return kbd_property.inputbuf;
}



/******************************************************************************
 * @fn      clearscrean()
 *
 * @brief   清屏
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void ClearScrean(void)
{
    uint8 i;
    for(i=0;i < kbd_property.dispMaxRow;i++)
    {
		API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*20, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + i*30, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*20, 30);
    }
}

/******************************************************************************
 * @fn      DisplayAllScreen()
 *
 * @brief   更新整个编辑框的显示
 *
 * @param   iColor       显示颜色       范围0~15
 * @param   highLight    高亮属性       0--不高亮/ 1--高亮
 * @param   cursor       是否显示光标   0--不显示/1--显示
 *
 * @return  none
 *****************************************************************************/
void DisplayAllScreen(int iColor, unsigned char highLight,unsigned char cursor)       
{
    unsigned char Display[MAX_DISPLAY_COL+10 + 1] = {0};
    unsigned char i,j,k=0;
    unsigned char end_flag = 0;
	int 		backColor;

    if(highLight)                                                   //背景颜色设置
    {
        backColor = COLOR_GREEN;//
    }
	else
	{
		backColor = COLOR_KEY;
	}

	if(kbd_property.highLight != highLight)
	{
		ClearScrean();
	}
	
    for(i=0;i<kbd_property.dispMaxRow;i++)                          //更新显示
    {
        for(j=0, memset(Display, ' ', MAX_DISPLAY_COL+10); j<kbd_property.dispMaxCol;j++)
        {
            if(kbd_property.inputbuf[k]=='\0')
            {
                if(cursor && (kbd_property.char_num < kbd_property.inputSize) && (!highLight))
                {
                    Display[j] = '_';
                }
				end_flag = 1;
                break;
            }
            else if(kbd_property.inputbuf[k]=='\r')
            {
                k++;
                break;
            }
            else 
            {
                Display[j]=kbd_property.inputbuf[k];
                k++; 
            } 
        }

		if(highLight)
		{
			Display[j] = '\0';
		}

		API_OsdStringDisplayExtWithBackColor(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*20, KEYBOARD_DISPLAY_Y + (kbd_property.dispStartRow + i)*30, iColor, backColor, Display, strlen(Display),1,STR_UTF8 );

		if(end_flag)
		{
			API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*20, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + (i+1)*30, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*20, 30);
			break;
		}
	}
	
	kbd_property.highLight = highLight;
	
    if(i == kbd_property.dispMaxRow - 1)           //显示屏只剩下一行
    {
        kbd_property.dispFullFlag = 1;
    }
    else if(i == kbd_property.dispMaxRow)           //显示屏已经满了
    {
        kbd_property.dispFullFlag = 2;                              
    }
    else                                                            //显示屏空行还有两行或以上
    {
        kbd_property.dispFullFlag = 0;
    }
}

/******************************************************************************
 * @fn      GetKeyboardcapsLock()
 *
 * @brief   取大小写状态
 *
 * @param   none
 *
 * @return  capsLock 大小写状态
 *****************************************************************************/
unsigned char GetKeyboardcapsLock(void)
{
    return kbd_property.capsLock;
}

