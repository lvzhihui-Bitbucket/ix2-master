
#include "MENU_public.h"
#include "MENU_136_MulLangKeypad.h"
#include "MENU_MLKeyboard.h"

unsigned char 	mullang_keypadType;
char* 		mullang_keypadPInput;
int 			mullang_keyboard_title;

static void display_mullang_kp_input_type(void)
{
	int caps_lock;

	if(mullang_keypadType == KEYPAD_CHAR)
	{
		
		caps_lock = GetKeyboardcapsLock();
		
		if(caps_lock == 0)
		{
			API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "abc   ", strlen("abc   "),0,STR_UTF8,0);
		}		
		else if(caps_lock == 1)
		{
			API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "Abc   ", strlen("Abc   "),0,STR_UTF8,0);
		}
		else
		{
			API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "ABC   ", strlen("ABC   "),0,STR_UTF8,0);
		}	
	}
	else if(mullang_keypadType == KEYPAD_NUM)
	{
		API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "123", strlen("123"),0,STR_UTF8,0);
	}
}


void EnterMulLangKeypadMenu(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitInputString, unsigned char highLight, InputOK callback)
{
	mullang_keypadType 				= type;
	mullang_keypadPInput 			= pInput;
	mullang_keyboard_title			= title;

	API_MLKeyboardInit( 0,0, 1,inputMaxLen, Color,pInitInputString,highLight,callback );			//��ʼ������
	
	if(mullang_keypadType == KEYPAD_CHAR)
	{
		StartInitOneMenu(MENU_136_MulLangKeyPad, 0, 1);
	}
	else if(mullang_keypadType == KEYPAD_NUM)
	{
		StartInitOneMenu(MENU_137_MulLangKeyPad_Num, 0, 1);
		//API_BG_Display(MENU_BACK_GROUND_007_KeyPadNum);
	}
	display_mullang_kp_input_type();
}
void EnterMulLangKeypadNoStack(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitInputString, unsigned char highLight, InputOK callback)
{
#if 0
	keypadType 				= type;
	keypadPInput 			= pInput;
	keyboard_title			= title;

	API_KeyboardInit( 0,0, 1,inputMaxLen, Color,pInitInputString,highLight,callback );			//��ʼ������
	
	if(keypadType == KEYPAD_CHAR)
	{
		StartInitOneMenu(MENU_019_KEYPAD, MENU_017_KEYPAD_CHAR, 0);
	}
	else if(keypadType == KEYPAD_NUM)
	{
		StartInitOneMenu(MENU_019_KEYPAD, MENU_018_KEYPAD_NUM, 0);
	}
	display_kp_input_type();
#endif
}

static void SwitchMulLangKeypadType(unsigned char type)
{
	char* pDisplay;
	
	if(mullang_keypadType != type)
	{
		mullang_keypadType = type;
		
		if(mullang_keypadType == KEYPAD_CHAR)
		{
			StartInitOneMenu(MENU_136_MulLangKeyPad, 0, 0);
		}
		else if(mullang_keypadType == KEYPAD_NUM)
		{
			StartInitOneMenu( MENU_137_MulLangKeyPad_Num,0, 0);
			//API_BG_Display(MENU_BACK_GROUND_007_KeyPadNum);
		}
		display_mullang_kp_input_type();		
	}
}

void MENU_136_MulLangKeypad_Init(int uMenuCnt)
{
	//printf("----GetIconXY(ICON_401_MLKPResSelect) %d:%d\n",GetIconXY(ICON_401_MLKPResSelect).x,GetIconXY(ICON_401_MLKPResSelect).y);
	if(mullang_keypadType == KEYPAD_CHAR)
	{
		API_DisableOsdUpdate();
		DisplayMLKPAllKey();
		API_EnableOsdUpdate();
	}
	API_MLKeyboardInputStart();
	API_OsdUnicodeStringDisplay(15, 5, COLOR_WHITE, mullang_keyboard_title, 1, 0);
}

void MENU_136_MulLangKeypad_Exit(void)
{
}

void MENU_136_MulLangKeypad_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int ret;
	char* pDisplay;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					//DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			//SetKeyBroadMode();//czn_20190111
			switch(GetCurIcon())
			{
				case ICON_180_KeyEdit:
					API_MLKeyboardInputModeChange();
					//InitPublicSettingMenuDisplay(GetMLKPResListCnt(), GetMLKPResListBuffer());
					//EnterPublicSettingMenu(0, GetMLKPResListCnt(), GetMLKPResIndex(), SetMLKPRes);
					//StartInitOneMenu(MENU_014_PUBLIC_SETTING,0,1);
					break;
				case ICON_130_Key29:
				case ICON_448_MLKPResSelect:
					InitPublicSettingMenuDisplay(GetMLKPResListCnt(), GetMLKPResListBuffer());
					EnterPublicSettingMenu(0,MESG_TEXT_KeyboardLanguage, GetMLKPResListCnt(), GetMLKPResIndex(), SetMLKPRes);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
					break;
				case ICON_131_Key30:
				case ICON_133_Key32:
					break;

				default:
					switch(API_MLKeyboardInputOneKey(mullang_keypadType, GetCurIcon()))
					{
						case OK_KEY_VALUE:
							ret = API_MLKeyboardInputDataDump(mullang_keypadPInput);
							if(ret  == 0 )
							{
								BEEP_ERROR();
							}
							else if(ret  == 1 )
							{
								popDisplayLastMenu();
							}
							else
							{
								;
							}
							break;
						case RETURN_KEY_VALUE:
							popDisplayLastMenu();
							break;
							
						case TURN_TO_NUM_KEY_VALUE:
							SwitchMulLangKeypadType(KEYPAD_NUM);
							break;
						case TURN_TO_CHAR_KEY_VALUE:
							SwitchMulLangKeypadType(KEYPAD_CHAR);
							break;
						case CAPSLOCK_KEY_VALUE:
							//display_kp_input_type();	
							display_mullang_kp_input_type();
							break;

					}
					break;
					
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
}


