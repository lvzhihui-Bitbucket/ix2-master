
#ifndef _MENU_ShortcutTable_H
#define _MENU_ShortcutTable_H

typedef struct
{
	int iConText;
	void (*iConProcess)(void);
}MainShortcutIcon;

extern MainShortcutIcon mainShortcutIconTable[];
extern const int mainShortcutIconNum;


#endif


