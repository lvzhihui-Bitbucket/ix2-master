#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "MENU_public.h"
#include "task_monitor.h"	
#include "onvif_discovery.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_business_manage.h"		//czn_20190107
#include "obj_IPCTableSetting.h"
#include "obj_ipc_manager.h"
#include "task_DXMonitor.h"

int monListMaxNum = 0;
int monListState;
int monListIndex;
int monListIconSelect;
int monListPageSelect;
static int monListOnlineUpdate;
static int ipcNum;


#define	MAX_FAV_DEVICE	20
typedef struct 
{
	int 	type;
	char	dev_name[41];
	char	dev_addr[21];
	char	dev_name2[41];
}MON_FAVORITE_DAT;
typedef struct
{
	int						deviceCnt;				//�豸����
	MON_FAVORITE_DAT 		device[MAX_FAV_DEVICE];
} MON_FAVORITE_Table;
static MON_FAVORITE_Table MonFavTable = {0};

int GetMonFavNum(void)
{
	return MonFavTable.deviceCnt;
}
int GetMonFavRecord(int index, MON_FAVORITE_DAT* Record)
{
	if(MonFavTable.deviceCnt <= index)
	{
		return -1;
	}
	memset(Record, 0, sizeof(MON_FAVORITE_DAT));

	*Record = MonFavTable.device[index];
	return 0;
}
int GetMonFavByAddr(char* addr, char* name, char* name2)
{
	int i;
	for(i = 0; i < MonFavTable.deviceCnt; i++)
	{
		if(addr != NULL)
		{
			if(!strcmp(addr, MonFavTable.device[i].dev_addr))
			{
				strcpy(name2, MonFavTable.device[i].dev_name2);
				return 1;
			}
		}
		else if(name != NULL)
		{
			if(!strcmp(name, MonFavTable.device[i].dev_name))
			{
				strcpy(name2, MonFavTable.device[i].dev_name2);
				return 1;
			}
		}
	}
	return 0;
}
int DeleteMonFavRecord(int index)
{
	int i;
	if(MonFavTable.deviceCnt <= index)
	{
		return -1;
	}
	for(i=index; i+1 < MonFavTable.deviceCnt; i++)
	{
		MonFavTable.device[i] = MonFavTable.device[i+1];
	}
	MonFavTable.deviceCnt--;
	SaveMonFavToFile();	
	return 0;	
}
int SetMonFav(int type, char* addr, char* name, char* name2)
{
	int i;

	if( MonFavTable.deviceCnt >= MAX_FAV_DEVICE )
		return -1;
	
	for(i = 0; i < MonFavTable.deviceCnt; i++)
	{
		if(!strcmp(addr, MonFavTable.device[i].dev_addr))
		{
			if(name2 != NULL)
			{
				MonFavTable.device[i].type = type;
				strcpy(MonFavTable.device[i].dev_addr, addr);
				strcpy(MonFavTable.device[i].dev_name, name);
				strcpy(MonFavTable.device[i].dev_name2, name2);
				SaveMonFavToFile();
				return 1;	
			}
			else
			{
				DeleteMonFavRecord(i);
				return 1;	
			}
		}
	}
	MonFavTable.device[MonFavTable.deviceCnt].type = type;
	strcpy(MonFavTable.device[MonFavTable.deviceCnt].dev_addr, addr);
	strcpy(MonFavTable.device[MonFavTable.deviceCnt].dev_name, name);
	strcpy(MonFavTable.device[MonFavTable.deviceCnt].dev_name2, "-");
	//printf("AddMonFav addr=%s\n", MonFavTable.device[MonFavTable.deviceCnt].dev_addr);
	MonFavTable.deviceCnt++;
	SaveMonFavToFile();
	return 0;	
}

char* CreateMonFavoriteObject(void)
{
    cJSON *root = NULL;
    cJSON *devices = NULL;
    cJSON *pSub = NULL;
	char *string = NULL;
	int i;
    root = cJSON_CreateObject();
	cJSON_AddItemToObject(root, "MonFavorite", devices = cJSON_CreateArray());	
    for (i = 0; i < MonFavTable.deviceCnt; i++)
    {
		pSub = cJSON_CreateObject();
		cJSON_AddNumberToObject(pSub, "Type", MonFavTable.device[i].type);
		cJSON_AddStringToObject(pSub, "DeviceName", MonFavTable.device[i].dev_name);
		cJSON_AddStringToObject(pSub, "DeviceAddr", MonFavTable.device[i].dev_addr);
		cJSON_AddStringToObject(pSub, "NickName", MonFavTable.device[i].dev_name2);

		cJSON_AddItemToArray(devices, pSub);
	}
	string = cJSON_Print(root);
	
	//if(string != NULL)
	//{	
	//	printf("MonFavorite_JSON=%s\n", string);
	//}
	cJSON_Delete(root);

	return string;
}
int ParseMonFavoriteObject(const char* json)
{
	int status = 0;
	int i,j;
    cJSON *devices = NULL;
    cJSON *pSub = NULL;
	/* ����һ�����ڽ����� cJSON �ṹ */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	devices = cJSON_GetObjectItem( root, "MonFavorite");
	if(cJSON_IsArray(devices))
	{
		MonFavTable.deviceCnt = cJSON_GetArraySize ( devices );
		for(i=0; i<MonFavTable.deviceCnt; i++)
		{
			pSub = cJSON_GetArrayItem(devices, i);
			if(NULL == pSub ){ continue ; }
			ParseJsonNumber(pSub, "Type", &(MonFavTable.device[i].type));
			ParseJsonString(pSub, "DeviceName", MonFavTable.device[i].dev_name, 40);
			ParseJsonString(pSub, "DeviceAddr", MonFavTable.device[i].dev_addr, 20);
			ParseJsonString(pSub, "NickName", MonFavTable.device[i].dev_name2, 40);
		}
	}
	status = 1;
end:
	cJSON_Delete(root);
	return status;
}

void SaveMonFavToFile(void)
{
	char* jsonString;
	jsonString = CreateMonFavoriteObject();
	if(jsonString != NULL)
	{
		SetJsonStringToFile(MON_COOKIE_TABLE_NAME, jsonString);
		free(jsonString);
	}

}
void GetMonFavFromFile(void)
{
	ParseMonFavoriteObject(GetJsonStringFromFile(MON_COOKIE_TABLE_NAME));
}


int GetIpcListUpdateFlag(void)
{
	return monListOnlineUpdate;
}

void SetIpcListUpdateFlag(int flag)
{
	monListOnlineUpdate = flag;
}

int SwitchIPCMonitor(onvif_login_info_t info)
{
	return -1;
}

int IpcShortcutMon(char* device_ip, char* device_name)
{
	API_OneIpc_Show_start(0, device_ip, device_name, 0, 0);
	EnterIPCMonMenu(0, NULL, 1);
	return 0;
}

int StarFastKeyMon(void)
{
	int shortcutSelect;
	char name[41];
	char bdRmMs[21];	
	char index;
	shortcutSelect = read_short_cut_io_val(FAST_MON_KEY_SHORTCUT, bdRmMs, name, &index);
	if( shortcutSelect == SHORTCUT_TYPE_MONITOR )
	{
		if(Api_Ds_Show(0, 0, bdRmMs, name) == 0)
		{
			EnterDSMonMenu(0, 1);
		}
		return 0;
	}
	else if( shortcutSelect == SHORTCUT_TYPE_MONITOR_IPC )
	{
		IpcShortcutMon(bdRmMs, name);
		return 0;
	}
	else if( shortcutSelect == SHORTCUT_TYPE_MONITOR_QUAD )
	{
		StartInitOneMenu(MENU_113_MonQuart,0,1);
		return 0;
	}
	else if( shortcutSelect == SHORTCUT_TYPE_DXMONITOR )
	{
		API_Event_DXMonitor_On(atoi(bdRmMs),0);
		SetWinDeviceName(0,name);
		EnterDxDSMonMenu(0, 1);
		return 0;
	}
	return -1;
}

int StartIPCMonitor(int index)
{
 	int ret;
	//API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	if(index<GetIpcNum())
		ret = API_IpcIndex_Show(0, 0,index, 0, 0 );
	else
		ret = API_IpcIndex_Show(0, 1,index-GetIpcNum(), 0, 0 );
	if(ret == 0 || ret == 1)
	{
		//API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		EnterIPCMonMenu(0, NULL, 1);
		return 0;
	}
	else
	{
		//API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		return -1;
	}
	return -1;
}
void StartMonlistMonitor(int index)	//czn_20170711
{
	char name[41];
	char name_temp2[41];
	char para_buff[20];
	int monResNum;
	MON_FAVORITE_DAT dat;
	uint16 ds_addr;
	if(monListState == ICON_256_Edit)
	{
		if(GetMonFavRecord(index, &dat) == 0)
		{
			if(dat.type == 0)
			{
				if(Api_Ds_Show(0, 0, dat.dev_addr, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name) == 0)
				{
					EnterDSMonMenu(0, 1);
				}
			}
			else if(dat.type == 3)
			{
				ds_addr = strtoul(dat.dev_addr,NULL,0);
			
				API_Event_DXMonitor_On(ds_addr,0);
				SetWinDeviceName(0,strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name);
				EnterDxDSMonMenu(0, 1);
			}
			else
			{
				IpcShortcutMon(dat.dev_addr, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name);
			}
		}
	}
	else
	{
		if(Get_MonRes_Num() == 0)
		{
			monResNum = Get_MonRes_Temp_Table_Num();
		}
		else
		{
			monResNum = Get_MonRes_Num();
		}
		
		if(index < ipcNum)
		{
			if(StartIPCMonitor(index) == -1)
			{
				BEEP_ERROR();
			}
		}
		else if(index < monResNum + ipcNum)
		{
			if(Get_MonRes_Num() == 0)
			{
				if(Get_MonRes_Temp_Table_Record(index - ipcNum, name,para_buff, NULL) == 0)
				{
					if( GetMonFavByAddr(para_buff, NULL, name_temp2) == 1 )
					{
						strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
					}
					if(Api_Ds_Show(0, 0, para_buff, name) == 0)
					{
						EnterDSMonMenu(0, 1);
					}
				}
			}
			else
			{
				if(Get_MonRes_Record(index - ipcNum, name,para_buff, NULL) == 0)
				{
					if( GetMonFavByAddr(para_buff, NULL, name_temp2) == 1 )
					{
						strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
					}
					if(Api_Ds_Show(0, 0, para_buff, name) == 0)
					{
						EnterDSMonMenu(0, 1);
					}
				}
			}
			
		}	
		else
		{
			
			Get_DxMonRes_Record(index - ipcNum-monResNum, name,para_buff);
			ds_addr = strtoul(para_buff,NULL,0);
			//InstructionCheck(ds_addr, ST_LINK_MR);
			//DXMonitor_Camera_Link(ds_addr);
			if( GetMonFavByAddr(para_buff, NULL, name_temp2) == 1 )
			{
				strcpy(name, strcmp(name_temp2, "-") ? name_temp2 : name);
			}
			#if 1
			API_Event_DXMonitor_On(ds_addr,0);
			SetWinDeviceName(0,name);
			EnterDxDSMonMenu(0, 1);
			#endif
		}
	}

}

int ModifyMonNickName(const char* name)
{
	char name_temp[41];
	char bdRmMs[11];
	IPC_ONE_DEVICE dat;
	MON_FAVORITE_DAT favdat;
	if(strlen(name) > 0)
	{
		if(monListState == ICON_256_Edit)
		{
			if(GetMonFavRecord(monListIndex, &favdat) == 0)
				SetMonFav(favdat.type, favdat.dev_addr, favdat.dev_name, name);
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

void ModifyMonFavorite(int index)
{
	char name_temp[41];
	char bdRmMs[11];
	IPC_ONE_DEVICE dat;
	MON_FAVORITE_DAT favdat;
	int favAdd;
	if(monListState == ICON_203_Monitor)
	{
		if(index < ipcNum)
		{
			if(index < GetIpcNum())
			{
				GetIpcCacheRecord(index, &dat);
				favAdd = SetMonFav(1, dat.IP, dat.NAME, NULL);
			}
			else
			{
				GetWlanIpcRecord(index-GetIpcNum(), &dat);
				favAdd = SetMonFav(2, dat.IP, dat.NAME, NULL);
			}
		}
		else if(index<(monListMaxNum-Get_DxMonRes_Num()))
		{
			if(Get_MonRes_Num()==0)
			{
				Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, NULL);
			}
			else
			{
				Get_MonRes_Record(index - ipcNum - 1,name_temp,bdRmMs, NULL);
			}
			
			favAdd = SetMonFav(0, bdRmMs, name_temp, NULL);
		}
		else
		{
			Get_DxMonRes_Record(index-(monListMaxNum-Get_DxMonRes_Num()),name_temp,bdRmMs);
			favAdd = SetMonFav(3, bdRmMs, name_temp, NULL);
		}
		
	}
	else
	{
		if(GetMonFavRecord(index, &favdat) == 0)
		{
			favAdd = SetMonFav(favdat.type, favdat.dev_addr, favdat.dev_name, NULL);
		}
		//monListMaxNum = GetMonFavNum();
	}
	#if 0
	if( favAdd == 1 )
	{
		API_SpriteClose(x, y, SPRITE_Collect);
	}
	else if( favAdd == 0 )
	{
		API_SpriteDisplay_XY(x, y, SPRITE_Collect);
	}
	#endif
}

static void MenuListGetCollectPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	MON_FAVORITE_DAT dat;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < monListMaxNum)
		{
			GetMonFavRecord(index, &dat);
			if(dat.type == 1)
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_IPC;
				strcpy(DisplayBuffer, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name);
			}
			else if(dat.type == 2)
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_WIPC;
				strcpy(DisplayBuffer, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name);
			}
			else if(dat.type == 3)
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				strcpy(DisplayBuffer, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name);
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				get_device_addr_and_name_disp_str(0, dat.dev_addr, NULL, NULL, strcmp(dat.dev_name2, "-") ? dat.dev_name2 : dat.dev_name, DisplayBuffer);
			}
			recordLen = strlen(DisplayBuffer);
			//unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			unicode_len = 2*utf82unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_Collect;
			pDisp->dispCnt++;
		}
	}
}

void MonCollectShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID2(MESG_TEXT_Icon256_Edit, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	monListMaxNum = GetMonFavNum();
	//printf("MonitorListShow monListMaxNum=%d\n", monListMaxNum);
	listInit.selEn = 1;
	listInit.ediEn = 1;
	listInit.listCnt = monListMaxNum;
	if(get_pane_type() == 7)//dx470v
	{
		listInit.listType = ICON_4X2;
		listInit.textOffset = 110;
	}
	else 
	{
		listInit.listType = ICON_2X4;
		listInit.textOffset = 110;
	}
	
	listInit.textSize = 0;
	listInit.textAlign = 8;
	listInit.fun = MenuListGetCollectPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);

}


static void MenuListGetMonitorPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	IPC_ONE_DEVICE ipcRecord;
	char name_temp[41];
	char name_temp2[41];
	char bdRmMs[11];
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < monListMaxNum)
		{
			if(index < ipcNum)
			{
				if(index < GetIpcNum())
				{
					pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_IPC;
					GetIpcCacheRecord(index, &ipcRecord);
				}
				else
				{
					pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_WIPC;
					GetWlanIpcRecord(index-GetIpcNum(), &ipcRecord);
				}
				strcpy(DisplayBuffer, ipcRecord.NAME);
				
				if(GetMonFavByAddr(NULL, ipcRecord.NAME, name_temp2) == 1)
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_Collect;
					strcpy(DisplayBuffer, strcmp(name_temp2, "-") ? name_temp2 : ipcRecord.NAME);
				}
			}
			else if(index<(monListMaxNum-Get_DxMonRes_Num()))
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				if(Get_MonRes_Num() == 0)
				{
					if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, NULL) != 0)
						return;
				}
				else
				{
					if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, NULL) != 0)
						return;
				}
				if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_Collect;
					strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
				}
				get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, DisplayBuffer);
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_DS;
				Get_DxMonRes_Record(index-(monListMaxNum-Get_DxMonRes_Num()),name_temp,bdRmMs);
				strcpy(DisplayBuffer, name_temp);
				if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_Collect;
					strcpy(DisplayBuffer, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
				}
			}
			recordLen = strlen(DisplayBuffer);
			//unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			unicode_len = 2*utf82unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}
	}
}

void MonitorListShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID2(MESG_TEXT_Monitor, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	ipcNum = GetIpcNum()+GetWlanIpcNum();
	#if 0
	if(Get_MonRes_Num())
	{
		monListMaxNum = Get_MonRes_Num() + ipcNum+Get_DxMonRes_Num();
	}
	else
	{
		monListMaxNum = Get_MonRes_Temp_Table_Num() + ipcNum+Get_DxMonRes_Num();;
	}
	#endif
	monListMaxNum = ipcNum+Get_DxMonRes_Num();
	printf("MonitorListShow monListMaxNum=%d\n", monListMaxNum);
	listInit.selEn = 1;
	listInit.ediEn = 1;
	listInit.listCnt = monListMaxNum;
	if(get_pane_type() == 7)//dx470v
	{
		listInit.listType = ICON_4X2;
		listInit.textOffset = 110;
	}
	else 
	{
		listInit.listType = ICON_2X4;
		listInit.textOffset = 110;
	}
	
	listInit.textSize = 0;
	listInit.textAlign = 8;
	listInit.fun = MenuListGetMonitorPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);

}


void MENU_002_Monitor_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		monListIconSelect = 0;
		monListPageSelect = 0;
		monListState = ICON_203_Monitor;
		//if(Get_MonRes_Num() == 0)
		#if 0
		if(!Get_MonRes_Num() || !Get_MonRes_Temp_Table_Num())	
		{
			if(Get_MonRes_Temp_Table_Num() == 0)
				monListOnlineUpdate = 0;
			API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
		}
		#endif
	}
	API_MenuIconDisplaySelectOn(monListState);
	if(monListState == ICON_203_Monitor)
	{
		MonitorListShow();
	}
	else
	{
		MonCollectShow();
	}
		
	#if 0
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, monListState, 1, 0);

	monitorIconMax = GetListIconNum();

	API_DisableOsdUpdate();
	if(monListState == ICON_203_Monitor)
	{
		DisplayOnePageMonlist(monListPageSelect);
	}
	else
	{
		DisplayOnePageMonCollect(monListPageSelect);
	}
	API_EnableOsdUpdate();
	#endif
}


void MENU_002_Monitor_Exit(void)
{
	MenuListDisable(0);
}

void MENU_002_Monitor_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int updateCnt,IpcupdateCnt,x,y;	//czn_20190221
	char display[100];
	//POS pos;
	//SIZE hv;
	//OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	LIST_ICON listIcon;
	char	nickName[21];			
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					monListIndex = listIcon.mainIcon;
					if(listIcon.subIcon == 0)
					{
						if(monListIndex >= 0)
						{
							// lzh_20230710_s 	// testing cvbs direct into t527
							#if !defined(CVBS_DIRECT_INTO_T527)
							StartMonlistMonitor(monListIndex);
							#else
							API_LocalCaptureOn(0);		// open video
							EnterDxDSMonMenu(0, 1);		// config display
							#endif
							// lzh_20230710_e 	// testing cvbs direct into t527
						}
					}
					else if(listIcon.subIcon == 1)
					{
						if(monListIndex >= 0)
						{
							ModifyMonFavorite(monListIndex);
							if(monListState == ICON_203_Monitor)
							{
								MonitorListShow();
							}
							else
							{
								MonCollectShow();
							}
							//MenuListDiplayPage();
						}
					}
					else if(listIcon.subIcon == 2)
					{
						if(monListIndex >= 0 && (monListState == ICON_256_Edit))
						{
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, nickName, 20, COLOR_WHITE, "-", 1, ModifyMonNickName);
						}
					}

					break;
					
				case ICON_204_quad:
					StartInitOneMenu(MENU_113_MonQuart,0,1);
					break;
				case ICON_203_Monitor:
					if(monListState != ICON_203_Monitor)
					{
						API_MenuIconDisplaySelectOff(monListState);
						monListState = ICON_203_Monitor;
						API_MenuIconDisplaySelectOn(monListState);
						MonitorListShow();
						#if 0
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, monListState, 1, 0);
						monListPageSelect = 0;
						DisplayOnePageMonlist(monListPageSelect);
						#endif
					}
					break;
				case ICON_256_Edit:
					if(monListState != ICON_256_Edit)
					{
						API_MenuIconDisplaySelectOff(monListState);
						monListState = ICON_256_Edit;
						API_MenuIconDisplaySelectOn(monListState);
						MonCollectShow();
						#if 0
						monListPageSelect = 0;
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, monListState, 1, 0);
						DisplayOnePageMonCollect(monListPageSelect);
						#endif
					}
					break;
				#if 0
				case ICON_short_list1:
				case ICON_short_list2:
				case ICON_short_list3:
				case ICON_short_list4:
				case ICON_short_list5:
				case ICON_short_list6:
				case ICON_short_list7:
				case ICON_short_list8:
				case ICON_short_list9:
				case ICON_short_list10:
					monListIconSelect = GetCurIcon() - ICON_short_list1;
					monListIndex = monListPageSelect*monitorIconMax + monListIconSelect;

					if(monListIndex >= monListMaxNum)
					{
						return;
					}
					
					if(monListState == ICON_203_Monitor && Get_MonRes_Num())
					{
						if(monListIndex == 0)	//List Online device
						{
							//ClearMonRes_Table();
							monListOnlineUpdate = 0;
							API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
							DisplayOnePageMonlist(monListPageSelect);
						}
						else
						{
							StartMonlistMonitor(monListIndex - 1);
						}
					}
					else
					{
						StartMonlistMonitor(monListIndex);
					}
					break;
				case ICON_Left_list1:
				case ICON_Left_list2:
				case ICON_Left_list3:
				case ICON_Left_list4:
				case ICON_Left_list5:
				case ICON_Left_list6:
				case ICON_Left_list7:
				case ICON_Left_list8:
				case ICON_Left_list9:
				case ICON_Left_list10:
					monListIconSelect = GetCurIcon() - ICON_Left_list1;
					monListIndex = monListPageSelect*monitorIconMax + monListIconSelect;

					if(monListIndex >= monListMaxNum)
					{
						return;
					}
					
					if(monListState == ICON_203_Monitor)
					{
						if(Get_MonRes_Num())
						{
							if(monListIndex == 0)
								return;
						}
						ModifyMonFavorite();
					}
					else
					{
						ModifyMonFavorite();
					}					
					break;
				#if 0
				case ICON_Right_list1:
				case ICON_Right_list2:
				case ICON_Right_list3:
				case ICON_Right_list4:
				case ICON_Right_list5:
					monListIconSelect = GetCurIcon() - ICON_Right_list1;
					monListIndex = monListPageSelect*monitorIconMax + monListIconSelect;

					if(monListIndex >= monListMaxNum)
					{
						return;
					}
					
					if(monListState == ICON_203_Monitor)
					{
						if(Get_MonRes_Num())
						{
							if(monListIndex == 0)
								return;
						}
					}
					EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, monNickName, 20, COLOR_WHITE, "-", 1, ConfirmModifyOneMonlist);
					break;
					#endif
				case ICON_201_PageDown:
					if(monListState == ICON_203_Monitor)
					{
						PublicPageDownProcess(&monListPageSelect, monitorIconMax, monListMaxNum, (DispListPage)DisplayOnePageMonlist);
					}
					else
					{
						PublicPageDownProcess(&monListPageSelect, monitorIconMax, monListMaxNum, (DispListPage)DisplayOnePageMonCollect);
					}
					break;
				case ICON_202_PageUp:
					if(monListState == ICON_203_Monitor)
					{
						PublicPageUpProcess(&monListPageSelect, monitorIconMax, monListMaxNum, (DispListPage)DisplayOnePageMonlist);
					}
					else
					{
						PublicPageUpProcess(&monListPageSelect, monitorIconMax, monListMaxNum, (DispListPage)DisplayOnePageMonCollect);
					}
					break;

				#endif	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_MonitorListUpdate:
				monListOnlineUpdate = 1;
				printf("MSG_7_BRD_SUB_MonitorListUpdate!!!\n");
				
				if(monListState == ICON_203_Monitor)
				{
					MonitorListShow();
				}
				else
				{
					MonCollectShow();
				}
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}


	
}


void PublicPageUpProcess(int *pageSelect, int maxIcon, int listNum, DispListPage DispPage)
{
	if(*pageSelect)
	{
		(*pageSelect)--;
	}
	else
	{
		*pageSelect = (listNum ? (listNum-1)/maxIcon : 0);
	}
	
	API_DisableOsdUpdate();
	DispPage(*pageSelect);
	API_EnableOsdUpdate();
}

void PublicPageDownProcess(int *pageSelect, int maxIcon, int listNum, DispListPage DispPage)
{
	if((++(*pageSelect))*maxIcon > listNum-1)
	{
		*pageSelect = 0;
	}
	
	API_DisableOsdUpdate();
	DispPage(*pageSelect);
	API_EnableOsdUpdate();
}

void DisplayShortcutPageMonlist(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	int monres_num;
	int IconMax = GetListIconNum();
	
	ipcNum = GetIpcNum()+GetWlanIpcNum();
	//if(GetIpcListUpdateFlag())
	#if 0
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		monres_num = Get_MonRes_Temp_Table_Num() + ipcNum;
		printf("DisplayShortcutPageMonlist num: %d\n",monres_num);
	}
	else
	{
		monres_num = Get_MonRes_Num() + ipcNum;
	}
	monres_num+=Get_DxMonRes_Num();
	#endif
	monres_num = ipcNum + Get_DxMonRes_Num();
	list_start = page*IconMax;
	
	for( i = 0; i < IconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_SpriteClose(x, y, SPRITE_WIFI_SELECT);
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if((list_start+i) < monres_num)
		{
			DisplayShortcutMonlist(list_start+i, x+DISPLAY_DEVIATION_X, y, DISPLAY_LIST_COLOR, 1, hv.h - x);
		}
		
	}
	
	
	maxPage = (monres_num%IconMax) ? (monres_num/IconMax+1) : (monres_num/IconMax);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}
void DisplayShortcutPageUnlocklist(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	int monres_num;
	int IconMax = GetListIconNum();
	
	ipcNum = GetIpcNum()+GetWlanIpcNum();
	//if(GetIpcListUpdateFlag())
	#if 0
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		monres_num = Get_MonRes_Temp_Table_Num() + ipcNum;
		printf("DisplayShortcutPageMonlist num: %d\n",monres_num);
	}
	else
	{
		monres_num = Get_MonRes_Num() + ipcNum;
	}
	monres_num+=Get_DxMonRes_Num();
	#endif
	monres_num = Get_DxMonRes_Num();
	list_start = page*IconMax;
	
	for( i = 0; i < IconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_SpriteClose(x, y, SPRITE_WIFI_SELECT);
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if((list_start+i) < monres_num)
		{
			DisplayShortcutMonlist(list_start+i+ipcNum, x+DISPLAY_DEVIATION_X, y, DISPLAY_LIST_COLOR, 1, hv.h - x);
		}
		
	}
	
	
	maxPage = (monres_num%IconMax) ? (monres_num/IconMax+1) : (monres_num/IconMax);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}
void DisplayShortcutMonlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char display[51];
	int monResNum;
	IPC_ONE_DEVICE dat;
	char name_temp[41];
	char name_temp2[41];
	char bdRmMs[11];
	int deviceType;

		
	//if(GetIpcListUpdateFlag())
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		monResNum = Get_MonRes_Temp_Table_Num();
		
		if(index < ipcNum)
		{
			if(index < GetIpcNum())
				GetIpcCacheRecord(index, &dat);
			else
				GetWlanIpcRecord(index-GetIpcNum(), &dat);
			
			snprintf(display, 51, "[IPC]%s", dat.NAME);
			if(GetMonFavByAddr(NULL, dat.NAME, name_temp2) == 1)
			{
				snprintf(display, 51, "[IPC]%s", strcmp(name_temp2, "-") ? name_temp2 : dat.NAME);
			}
			
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type, STR_UTF8, width);
		}
		else if(index < (monResNum + ipcNum))
		{
			if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
			if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
			{
				strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
			}
			
			get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
		}
		else
		{
			Get_DxMonRes_Record(index-monResNum-ipcNum, name_temp, bdRmMs);
			if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
				strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
			API_OsdStringDisplayExt(x, y, color, name_temp, strlen(name_temp), fnt_type,STR_UTF8, width);
		}
	}
	else
	{
		monResNum = Get_MonRes_Num();
		if(index < ipcNum)
		{
			if(index < GetIpcNum())
				GetIpcCacheRecord(index, &dat);
			else
				GetWlanIpcRecord(index-GetIpcNum(), &dat);
			snprintf(display, 51, "[IPC]%s", dat.NAME);
			if(GetMonFavByAddr(NULL, dat.NAME, name_temp2) == 1)
			{
				snprintf(display, 51, "[IPC]%s", strcmp(name_temp2, "-") ? name_temp2 : dat.NAME);
			}
			
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type, STR_UTF8, width);
		}
		else if(index < (monResNum + ipcNum))
		{
			if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
			if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
			{
				strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
			}
			get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
		}
		else
		{
			Get_DxMonRes_Record(index-monResNum-ipcNum, name_temp, bdRmMs);
			if( GetMonFavByAddr(bdRmMs, NULL, name_temp2) == 1 )
				strcpy(name_temp, strcmp(name_temp2, "-") ? name_temp2 : name_temp);
			API_OsdStringDisplayExt(x, y, color, name_temp, strlen(name_temp), fnt_type,STR_UTF8, width);
		}
	}
}

#if 0

int GetMonitorName(char pName)
{
	int ret, monResNum;
	
	if(pName == NULL)
	{
		return -1;
	}

	if(GetIpcListUpdateFlag())
	{
		if(monListIndex < ipcNum)
		{
			ret = Get_IPC_Temp_Record(monListIndex,&saveIPCMonList);
			strcpy(pName, saveIPCMonList.name);
		}
		else
		{
			ret = Get_MonRes_Temp_Table_Record(monListIndex - ipcNum,pName,NULL, NULL);
		}

	}
	else
	{
		if(Get_MonRes_Num() == 0)
		{
			monResNum = Get_MonRes_Temp_Table_Num();
		}
		else
		{
			monResNum = Get_MonRes_Num();
		}
		if(monListIndex < ipcNum)
		{
			ret = Get_IPC_Temp_Record(monListIndex,&saveIPCMonList);
			strcpy(pName, saveIPCMonList.name);
		}
		else if(monListIndex < monResNum + ipcNum)
		{
			if(Get_MonRes_Num() == 0)
			{
				ret = Get_MonRes_Temp_Table_Record(monListIndex - ipcNum,pName,NULL, NULL);
			}
			else
			{
				ret = Get_MonRes_Record(monListIndex - ipcNum,pName,NULL, NULL);
			}
		}
	}
	
	return ret;
}

void DisplayOneMonlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char display[51];
	int monResNum;
	char name_temp[41];
	char bdRmMs[11];
	int deviceType;
	IPC_ONE_DEVICE dat;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_Left_list1, &pos, &hv);
	
	if(Get_MonRes_Num() == 0)
	{
		monResNum = Get_MonRes_Temp_Table_Num();
	}
	else
	{
		monResNum = Get_MonRes_Num();
	}

	if(index < ipcNum)
	{
		if(index < GetIpcNum())
			GetIpcCacheRecord(index, &dat);
		else
			GetWlanIpcRecord(index-GetIpcNum(), &dat);
		snprintf(display, 51, "[IPC]%s", dat.NAME);

		
		if(GetMonFavByAddr(NULL, dat.NAME) == 1)
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
		}
		
		API_OsdStringDisplayExt(hv.h, y, color, display, strlen(display), fnt_type, STR_UTF8, width);
	}
	else if(index < monResNum + ipcNum)
	{
		
		if(Get_MonRes_Num() == 0)
		{
			if(Get_MonRes_Temp_Table_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
		}
		else
		{
			if(Get_MonRes_Record(index - ipcNum,name_temp,bdRmMs, &deviceType) != 0)
				return;
		}
		
		if( GetMonFavByAddr(bdRmMs, NULL) == 1 )
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
		}
		
		get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
		API_OsdStringDisplayExt(hv.h, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
	}
}

void DisplayOnePageMonlist(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	ipcNum = GetIpcNum()+GetWlanIpcNum();
	if(Get_MonRes_Num())
	{
		monListMaxNum = Get_MonRes_Num() + ipcNum + 1;
	}
	else
	{
		monListMaxNum = Get_MonRes_Temp_Table_Num() + ipcNum;
	}
	
	//API_DisableOsdUpdate();
	
	for( i = 0; i < monitorIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_Left_list1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		API_SpriteClose(x, y, SPRITE_Collect);
		list_start = page*monitorIconMax+i;
		if(list_start< monListMaxNum)
		{
			if(Get_MonRes_Num())
			{
				if(list_start == 0)
				{
					API_OsdUnicodeStringDisplay(hv.h, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ListOnline, 1, bkgd_w - x);
				}
				else
				{
					DisplayOneMonlist(list_start-1, x, y, DISPLAY_LIST_COLOR, 1, bkgd_w - x);
				}
			}
			else
			{
				DisplayOneMonlist(list_start, x, y, DISPLAY_LIST_COLOR, 1, bkgd_w - x);
			}
		}
	}
	
	maxPage = (monListMaxNum%monitorIconMax) ? (monListMaxNum/monitorIconMax+1) : (monListMaxNum/monitorIconMax);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}


void DisplayOnePageMonCollect(uint8 page)
{
	int i, x, y;
	int index,maxPage;
	MON_FAVORITE_DAT dat;
	char oneRecord[50];
	POS pos;
	SIZE hv;
		
	monListMaxNum = GetMonFavNum();
	for( i = 0; i < monitorIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_Left_list1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		API_SpriteClose(x, y, SPRITE_Collect);
		index = page*monitorIconMax+i;
		if(index < monListMaxNum)
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			GetMonFavRecord(index, &dat);
			
			if(dat.type == 1)
			{
				snprintf(oneRecord, 51, "[IPC]%s", dat.dev_name);
				API_OsdStringDisplayExt(hv.h, y, DISPLAY_LIST_COLOR, oneRecord, strlen(oneRecord),1,STR_UTF8,bkgd_w - x);
				continue;
			}
			else
			{
				get_device_addr_and_name_disp_str(0, dat.dev_addr, NULL, NULL, dat.dev_name, oneRecord);
			}
			API_OsdStringDisplayExt(hv.h, y, DISPLAY_LIST_COLOR, oneRecord, strlen(oneRecord),1,STR_UTF8,bkgd_w - x);
		}

	}
	maxPage = monListMaxNum/monitorIconMax + (monListMaxNum%monitorIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}


int ConfirmModifyOneMonlist(const char* name)
{
	char name_temp[41];
	char bdRmMs[11];
	MON_CookieRecord_T cRecord;
	IPC_RECORD dat;
	if(strlen(name) > 0)
	{
		if(monListState == ICON_203_Monitor)
		{
			if(monListIndex < ipcNum)
			{
				//if(GetIpcListUpdateFlag())
				if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
				{
					Get_IPC_Temp_Record(monListIndex, &dat);
					strcpy(dat.name, name);
					Modify_IPC_Temp_Record(monListIndex, &dat);
					Add_Or_Modify_IPC_MonRes_Record(&dat);
				}
				else
				{
					Get_IPC_MonRes_Record(monListIndex, &dat);
					Modify_IPC_MonRes_Record(monListIndex, &dat);
				}
			}
			else
			{
				if(Get_MonRes_Num() == 0)
				{
					Get_MonRes_Temp_Table_Record(monListIndex - ipcNum,name_temp,bdRmMs, NULL);
				}
				else
				{
					Get_MonRes_Record(monListIndex - ipcNum - 1,name_temp,bdRmMs, NULL);
				}
				MonCookieModify(bdRmMs, 0, name, name_temp);
			}
		}
		else
		{
			if(monListIndex < ipcFavNum)
			{
				//if(GetIpcListUpdateFlag())
				if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
				{
					Get_IPC_Temp_Record(ipcCollectIndex[monListIndex], &dat);
					strcpy(dat.name, name);
					Modify_IPC_Temp_Record(ipcCollectIndex[monListIndex], &dat);
					Add_Or_Modify_IPC_MonRes_Record(&dat);
				}
				else
				{
					Get_IPC_MonRes_Record(ipcCollectIndex[monListIndex], &dat);
					Modify_IPC_MonRes_Record(ipcCollectIndex[monListIndex], &dat);
				}
			}
			else
			{
				GetMonCookieRecordItems(monCollectIndex[monListIndex-ipcFavNum], &cRecord);
				MonCookieModify(cRecord.BD_RM_MS, cRecord.faverite, name, cRecord.name);
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

void ModifyMonFavorite(void)
{
	char name_temp[41];
	char bdRmMs[11];
	MON_CookieRecord_T cRecord;
	POS pos;
	SIZE hv;
	int x, y;
	OSD_GetIconInfo(ICON_Left_list1+(monListIndex%monitorIconMax), &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;

	if(monListState == ICON_203_Monitor)
	{
		//if(GetIpcListUpdateFlag())
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			Get_MonRes_Temp_Table_Record(monListIndex - ipcNum,name_temp,bdRmMs, NULL);
		}
		else
		{
			Get_MonRes_Record(monListIndex - ipcNum - 1,name_temp,bdRmMs, NULL);
		}
		
		if( !MonGetCookieItemByAddr(bdRmMs, &cRecord) )
		{
			if(cRecord.faverite)
			{
				API_SpriteClose(x, y, SPRITE_Collect);
			}
			else
			{
				API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			}
			MonCookieModify(bdRmMs, (cRecord.faverite == 0? 1:0), NULL, name_temp);
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			MonCookieModify(bdRmMs, 1, NULL, name_temp);
		}
	}
	else
	{
		GetMonCookieRecordItems(monCollectIndex[monListIndex], &cRecord);
		if(cRecord.faverite)
		{
			API_SpriteClose(x, y, SPRITE_Collect);
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
		}
		MonCookieModify(cRecord.BD_RM_MS, cRecord.faverite == 0? 1:0, NULL, cRecord.name);
	}
}
void ModifyIPCFavorite(void)
{
	IPC_RECORD dat;
	POS pos;
	SIZE hv;
	int x,y;
	OSD_GetIconInfo(ICON_Left_list1+(monListIndex%monitorIconMax), &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;

	if(monListState == ICON_203_Monitor)
	{
		//if(GetIpcListUpdateFlag())
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			Get_IPC_Temp_Record(monListIndex, &dat);
		}
		else
		{
			Get_IPC_MonRes_Record(monListIndex, &dat);
		}
		
		if(dat.faverite)	
		{
			API_SpriteClose(x, y, SPRITE_Collect);
			dat.faverite = 0;
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			dat.faverite = 1;
		}
		
		//if(GetIpcListUpdateFlag())
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			Modify_IPC_Temp_Record(monListIndex, &dat);
			Add_Or_Modify_IPC_MonRes_Record(&dat);
		}
		else
		{
			Modify_IPC_MonRes_Record(monListIndex, &dat);
		}
	}
	else
	{
		//if(GetIpcListUpdateFlag())
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			Get_IPC_Temp_Record(ipcCollectIndex[monListIndex], &dat);
		}
		else
		{
			Get_IPC_MonRes_Record(ipcCollectIndex[monListIndex], &dat);
		}
		
		if(dat.faverite)	
		{
			API_SpriteClose(x, y, SPRITE_Collect);
			dat.faverite = 0;
		}
		else
		{
			API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			dat.faverite = 1;
		}
		
		//if(GetIpcListUpdateFlag())
		if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
		{
			Modify_IPC_Temp_Record(ipcCollectIndex[monListIndex], &dat);
			Add_Or_Modify_IPC_MonRes_Record(&dat);
		}
		else
		{
			Modify_IPC_MonRes_Record(ipcCollectIndex[monListIndex], &dat);
		}
	}
}
#endif

