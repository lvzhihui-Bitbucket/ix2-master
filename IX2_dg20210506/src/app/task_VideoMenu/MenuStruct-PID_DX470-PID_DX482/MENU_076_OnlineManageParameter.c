#include "MENU_076_OnlineManageParameter.h"
#include "obj_ProgInfoByIp.h"
#include "obj_DeviceManage.h"
#include "MENU_public.h"

//#define	OnlineManageParameterIconMax		5
int OnlineManageParameterIconMax;
int onlineManageParameterIconSelect;
int onlineManageParameterPageSelect;

const IconAndText_t onlineManageParameterSet[] = 
{
	//{ICON_ParaGroup0,					MESG_TEXT_ICON_ParaGroup0},
	{ICON_ParaGroup1,					MESG_TEXT_ICON_ParaGroup1},
	{ICON_ParaGroup2,					MESG_TEXT_ICON_ParaGroup2},
	{ICON_ParaGroup3,					MESG_TEXT_ICON_ParaGroup3},
	{ICON_ParaGroup4,					MESG_TEXT_ICON_ParaGroup4},
	{ICON_ParaGroup5,					MESG_TEXT_ICON_ParaGroup5},
};

const int onlineManageParameterSetNum = sizeof(onlineManageParameterSet)/sizeof(onlineManageParameterSet[0]);



static void DisplayOnlineManageParameterPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
		
	//API_DisableOsdUpdate();
	for(i = 0; i < OnlineManageParameterIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnlineManageParameterIconMax+i < onlineManageParameterSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineManageParameterSet[i+page*OnlineManageParameterIconMax].iConText, 1, val_x - x);
		}
	}
	pageNum = onlineManageParameterSetNum/OnlineManageParameterIconMax + (onlineManageParameterSetNum%OnlineManageParameterIconMax ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 

void MENU_076_OnlineManageParameter_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	OnlineManageParameterIconMax = GetListIconNum();
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	onlineManageParameterIconSelect = 0;
	onlineManageParameterPageSelect = 0;
	DisplayOnlineManageParameterPageIcon(onlineManageParameterPageSelect);
}

void MENU_076_OnlineManageParameter_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_049_Parameter);
}

void MENU_076_OnlineManageParameter_Process(void* arg)
{	
	int i, j;
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&onlineManageParameterPageSelect, OnlineManageParameterIconMax, onlineManageParameterSetNum, (DispListPage)DisplayOnlineManageParameterPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&onlineManageParameterPageSelect, OnlineManageParameterIconMax, onlineManageParameterSetNum, (DispListPage)DisplayOnlineManageParameterPageIcon);
					break;		
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					onlineManageParameterIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(onlineManageParameterPageSelect*OnlineManageParameterIconMax+onlineManageParameterIconSelect >= onlineManageParameterSetNum)
					{
						return;
					}

					
					switch(onlineManageParameterSet[onlineManageParameterPageSelect*OnlineManageParameterIconMax+onlineManageParameterIconSelect].iCon)
					{
						case ICON_ParaGroup0:
							SetParaGroupNumber(0);
							break;
						case ICON_ParaGroup1:
							SetParaGroupNumber(1);
							break;
						case ICON_ParaGroup2:
							SetParaGroupNumber(2);
							break;
						case ICON_ParaGroup3:
							SetParaGroupNumber(3);
							break;
						case ICON_ParaGroup4:
							SetParaGroupNumber(4);
							break;
						case ICON_ParaGroup5:
							SetParaGroupNumber(5);
							break;

					}
					StartInitOneMenu(MENU_074_RemoteParaManage,0,1);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}





