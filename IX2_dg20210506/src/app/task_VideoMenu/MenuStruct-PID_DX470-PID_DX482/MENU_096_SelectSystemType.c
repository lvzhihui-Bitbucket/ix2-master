#include "MENU_public.h"
#include "MENU_096_SelectSystemType.h"
#include "obj_R8001_Table.h"

#define	SelectSystemType_ICON_MAX	5
int selectSystemTypeIconSelect;
int selectSystemTypePageSelect;
char selectSystemTypeInput[21];
int roomNumberRestoreConfirm,selectSystemConfirm;
int selectSystemType;

const IconAndText_t selectSystemTypeSet[] = 
{
	{ICON_SelectSystemTypeVS,			MESG_TEXT_ICON_SelectSystemTypeVS},
	{ICON_SelectSystemTypeSS,			MESG_TEXT_ICON_SelectSystemTypeSS},
	{ICON_SelectSystemTypeNS,			MESG_TEXT_ICON_SelectSystemTypeNS},
	{ICON_SelectSystemTypeRestore,		MESG_TEXT_ICON_SelectSystemTypeRestore},
	{ICON_UpdateCallNbrByRes,			MESG_TEXT_ICON_UpdateCallNbrByRes},
};


const unsigned char selectSystemTypeSetNum = sizeof(selectSystemTypeSet)/sizeof(selectSystemTypeSet[0]);

static void DisplaySelectSystemTypePageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	int ms;
	POS pos;
	SIZE hv;
	
	API_DisableOsdUpdate();
	for(i = 0; i < SelectSystemType_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		ListSelect(i, 0);
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*SelectSystemType_ICON_MAX+i < selectSystemTypeSetNum)
		{
			if(page*SelectSystemType_ICON_MAX+i == selectSystemType)
			{
				ListSelect(i, 1);
			}
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, selectSystemTypeSet[i+page*SelectSystemType_ICON_MAX].iConText, 1,  hv.h - x);
		}
	}
	pageNum = selectSystemTypeSetNum/SelectSystemType_ICON_MAX + (selectSystemTypeSetNum%SelectSystemType_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	API_EnableOsdUpdate();
}
 

static int SetRM_Nbr(const char* number)
{
	int i;
	char rm[5];

	for(i = 0; number[i] != 0; i++)
	{
		if(number[i] > '9' || number[i] < '0')
		{
			return 0;
		}
	}

	i = atoi(number);
	if(i < 1 || i > 8999)
	{
		return 0;
	}
	
	sprintf(rm, "%04d", i);
	SetSysVerInfo_rm(rm);

	//popDisplayLastNMenu(2);
	return 1;
}

static int SetBD_RM_Nbr(const char* number)
{
	int i;
	char BD_RM_MS[11];

	for(i = 0; number[i] != 0; i++)
	{
		if(number[i] > '9' || number[i] < '0')
		{
			return 0;
		}
	}

	if(i != 8)
	{
		return 0;
	}
	
	strcpy(BD_RM_MS, number);
	strcat(BD_RM_MS,GetSysVerInfo_ms());
	
	SetSysVerInfo_BdRmMs(BD_RM_MS);
	//popDisplayLastNMenu(2);
	return 1;
}

void MENU_096_SelectSystemType_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_030_CALL_NUMBER);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_030_CALL_NUMBER, 1, 0);
	selectSystemTypeIconSelect = 0;
	selectSystemTypePageSelect = 0;
	roomNumberRestoreConfirm = 0;
	//dh_20190916_s
	#if 0
	if(memcmp(GetSysVerInfo_BdRmMs(), "00990001", 8))
	{
		if(!strcmp(GetSysVerInfo_bd(), "0099"))
		{
			selectSystemType = 1;
		}
		else
		{
			selectSystemType = 2;
		}
	}
	else
	{
		selectSystemType = 0;
	}
	#endif
	selectSystemType = GetSysVerInfo_SystemType();
	//dh_20190916_e
	DisplaySelectSystemTypePageIcon(selectSystemTypePageSelect);
}

void MENU_096_SelectSystemType_Exit(void)
{

}

void MENU_096_SelectSystemType_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	CallNbr_T imCallNbr;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:

					selectSystemTypeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(selectSystemTypePageSelect*SelectSystemType_ICON_MAX+selectSystemTypeIconSelect >= selectSystemTypeSetNum)
					{
						return;
					}
					

					switch(selectSystemTypeSet[selectSystemTypePageSelect*SelectSystemType_ICON_MAX+selectSystemTypeIconSelect].iCon)
					{
 						case ICON_SelectSystemTypeVS:
							ClearConfirm(&roomNumberRestoreConfirm, &selectSystemConfirm, SelectSystemType_ICON_MAX);
							#if 0
							if(roomNumberRestoreConfirm)
							{
								roomNumberRestoreConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(3), SPRITE_IF_CONFIRM);
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
								return;
							}
							#endif
							SetSysVerInfo_bd("0099");
							SetSysVerInfo_rm("0001");
							selectSystemType = 0;
							SetSysVerInfo_SystemType(0);	//dh_20190916
							DisplaySelectSystemTypePageIcon(0);
							//popDisplayLastMenu();
							break;
						case ICON_SelectSystemTypeSS:
							ClearConfirm(&roomNumberRestoreConfirm, &selectSystemConfirm, SelectSystemType_ICON_MAX);
							#if 0
							if(roomNumberRestoreConfirm)
							{
								roomNumberRestoreConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(3), SPRITE_IF_CONFIRM);
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
								return;
							}
							#endif			
							selectSystemType = 1;
							SetSysVerInfo_SystemType(1);//dh_20190916
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputRM_Nbr, selectSystemTypeInput, 4, COLOR_WHITE, GetSysVerInfo_rm(), 1, SetRM_Nbr);
							break;
						case ICON_SelectSystemTypeNS:
							ClearConfirm(&roomNumberRestoreConfirm, &selectSystemConfirm, SelectSystemType_ICON_MAX);
							#if 0
							if(roomNumberRestoreConfirm)
							{
								roomNumberRestoreConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(3), SPRITE_IF_CONFIRM);
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
								return;
							}
							#endif			
							selectSystemType = 2;
							SetSysVerInfo_SystemType(2);//dh_20190916
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputBD_RM_Nbr, selectSystemTypeInput, 8, COLOR_WHITE, GetSysVerInfo_BdRmMs(), 1, SetBD_RM_Nbr);
							break;
							
						case ICON_SelectSystemTypeRestore:
						case ICON_UpdateCallNbrByRes:
							if(ConfirmSelect(&roomNumberRestoreConfirm, &selectSystemConfirm, &selectSystemTypeIconSelect, selectSystemTypePageSelect, SelectSystemType_ICON_MAX))
							{
								return;
							}
							#if 0
							if(!roomNumberRestoreConfirm)
							{
								roomNumberRestoreConfirm = 1;
								selectSystemConfirm = selectSystemTypeIconSelect;
								API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(selectSystemConfirm), SPRITE_IF_CONFIRM);
								return;
							}
							else if(selectSystemConfirm == selectSystemTypeIconSelect)
							{
								roomNumberRestoreConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(selectSystemConfirm), SPRITE_IF_CONFIRM);
							}
							else
							{
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(selectSystemConfirm), SPRITE_IF_CONFIRM);
								roomNumberRestoreConfirm = 1;
								selectSystemConfirm = selectSystemTypeIconSelect;
								API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(selectSystemConfirm), SPRITE_IF_CONFIRM);
								return;
							}
							#endif
							if(GetCurIcon() == ICON_010_PublicList4)
							{
								//dh_20190919_s
								#if 0
								SetSysVerInfo_bd("0099");
								SetSysVerInfo_rm("0001");
								BEEP_CONFIRM();
								selectSystemType = 0;
								SetSysVerInfo_SystemType(0);//dh_20190916
								DisplaySelectSystemTypePageIcon(0);
								#endif
								//SetSysVerInfo_BdRmMs("0099000101");
								SetSysDefault_BdRmMs();
								SetSysDefault_GlobalNum();
								SetSysDefault_LocalNum();
								SetSysDefault_name();
								BEEP_CONFIRM();
								selectSystemType = 0;
								SetSysVerInfo_SystemType(0);
								DisplaySelectSystemTypePageIcon(0);
								//dh_20190919_e
							}
							else
							{
								if(!get_r8001_record_cnt())
								{
									char temp[11];
									sprintf(temp,"%s000001",GetSysVerInfo_bd());
									if(Api_ResSyncFromServer(8001,temp) !=0)
									{
										API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, MESG_TEXT_UpdateCallNbr_Tips1, 1, 0);
										BEEP_ERROR();
										sleep(1);
										API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
									}
								}
								else
								{
									if(get_r8001_record_by_brm(GetSysVerInfo_BdRmMs(),&imCallNbr))
									{
										API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, MESG_TEXT_UpdateCallNbr_Tips3, 1, 0);
										SetSysVerInfo_name((strcmp(imCallNbr.R_Name, "-")&&imCallNbr.R_Name[0]) ? imCallNbr.R_Name : imCallNbr.Name);
										SetSysVerInfo_LocalNum(imCallNbr.Local);
										SetSysVerInfo_GlobalNum(imCallNbr.Global);
										//set ip
										char temp[20];
										char dhcpState;
										if(!strcmp(imCallNbr.ip_static,"DHCP"))//DHCP
										{
											// zfz_20190617
											API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
											if(atoi(temp) == 0)
											{
												dhcpState = 1;
												sprintf(temp, "%d", dhcpState);
												API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
												API_DHCP_SaveEnable(1);
											}
										}
										else		 //��̬
										{
											dhcpState = 0;
											sprintf(temp, "%d", dhcpState);
											API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
											API_DHCP_SaveEnable(0);
											SetSysVerInfo_IP(imCallNbr.ip);
											SetSysVerInfo_mask(imCallNbr.mask);
											SetSysVerInfo_gateway(imCallNbr.gw);
											SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
											ResetNetWork();
											Dhcp_Autoip_CheckIP();		//czn_20190604
										}
										BEEP_CONFIRM();
										sleep(1);
										API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
									}
									else
									{
										API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, MESG_TEXT_UpdateCallNbr_Tips2, 1, 0);
										BEEP_ERROR();
										sleep(1);
										API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
									}

								}

							}
							break;
					}
					break;
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




