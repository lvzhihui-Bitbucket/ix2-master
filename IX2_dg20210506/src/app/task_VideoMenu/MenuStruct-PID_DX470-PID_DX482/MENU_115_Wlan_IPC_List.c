#include "MENU_public.h"
#include "../../onvif_service/onvif_discovery.h"
#include "MENU_115_Wlan_IPC_List.h"
#include "obj_business_manage.h"		//czn_20190107
#include "obj_IPCTableSetting.h"
#include "obj_APP_IPC_List.h"
	


#define	WLAN_ICP_LIST_ICON_MAX			5

int wlanIpcListIconSelect;
int wlanIpcListPageSelect;
int wlanIpcListLastIconSelect;
int wlanIpcListState;
int wlanIpcListIndex;
char wlanIpcListName[41];
IPC_RECORD saveWlanIPCMonList; 
one_vtk_table* wlanIpcMonListTable = NULL;
static int deleteConfirm;

void StartWlanIPC_MonlistMonitor(int index)
{
#if 0	
	char rtsp_url[240];
	int	 main_ch,width,height,rate;

	onvif_discovery_result_list_set_avtive(index);
	
	if( onvif_discovery_result_list_get_active_url( rtsp_url, &main_ch, &width, &height, &rate ) < 0 )
		return;

	printf("onvif discovery list result cur = %d, chanel=%s, rtsp_url=%s ......\n",onvif_discovery_result_list_get_active(),main_ch?"main":"sub",rtsp_url);
	
	api_onvif_start_mointor_one_stream(0, rtsp_url,main_ch, width, height, rate );	
#endif	
}


extern int ipcDhcpEnable;
void WlanIPC_MonRes_Table_Init(void)
{
	char temp[20];
	
	API_Event_IoServer_InnerRead_All(IPC_DHCP_Enable, temp);
	ipcDhcpEnable = atoi(temp) ? 1 : 0;
	
	wlanIpcMonListTable = load_vtk_table_file(WLAN_IPC_MON_LIST_FILE_NAME);
	if(wlanIpcMonListTable == NULL)
	{
		wlanIpcMonListTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &wlanIpcMonListTable->lock, 0);		//czn_20190327
		wlanIpcMonListTable->keyname_cnt = 7;
		wlanIpcMonListTable->pkeyname_len = malloc(wlanIpcMonListTable->keyname_cnt*sizeof(int));
		wlanIpcMonListTable->pkeyname = malloc(wlanIpcMonListTable->keyname_cnt*sizeof(one_vtk_dat));
		wlanIpcMonListTable->pkeyname_len[0] = 40;
		wlanIpcMonListTable->pkeyname_len[1] = 40;
		wlanIpcMonListTable->pkeyname_len[2] = 40;
		wlanIpcMonListTable->pkeyname_len[3] = 1;
		wlanIpcMonListTable->pkeyname_len[4] = 100;
		wlanIpcMonListTable->pkeyname_len[5] = 40;
		wlanIpcMonListTable->pkeyname_len[6] = 1;
		wlanIpcMonListTable->pkeyname[0].len = strlen("IPC_NAME");
		wlanIpcMonListTable->pkeyname[0].pdat = malloc(wlanIpcMonListTable->pkeyname[0].len);
		memcpy(wlanIpcMonListTable->pkeyname[0].pdat, "IPC_NAME", wlanIpcMonListTable->pkeyname[0].len);
		
		wlanIpcMonListTable->pkeyname[1].len = strlen("USER_NAME");
		wlanIpcMonListTable->pkeyname[1].pdat = malloc(wlanIpcMonListTable->pkeyname[1].len);
		memcpy(wlanIpcMonListTable->pkeyname[1].pdat, "USER_NAME", wlanIpcMonListTable->pkeyname[1].len);

		wlanIpcMonListTable->pkeyname[2].len = strlen("USER_PWD");
		wlanIpcMonListTable->pkeyname[2].pdat = malloc(wlanIpcMonListTable->pkeyname[2].len);
		memcpy(wlanIpcMonListTable->pkeyname[2].pdat, "USER_PWD", wlanIpcMonListTable->pkeyname[2].len);

		wlanIpcMonListTable->pkeyname[3].len = strlen("CHANNEL");
		wlanIpcMonListTable->pkeyname[3].pdat = malloc(wlanIpcMonListTable->pkeyname[3].len);
		memcpy(wlanIpcMonListTable->pkeyname[3].pdat, "CHANNEL", wlanIpcMonListTable->pkeyname[3].len);

		wlanIpcMonListTable->pkeyname[4].len = strlen("DEVICE_URL");
		wlanIpcMonListTable->pkeyname[4].pdat = malloc(wlanIpcMonListTable->pkeyname[4].len);
		memcpy(wlanIpcMonListTable->pkeyname[4].pdat, "DEVICE_URL", wlanIpcMonListTable->pkeyname[4].len);

		wlanIpcMonListTable->pkeyname[5].len = strlen("IPC_ID");
		wlanIpcMonListTable->pkeyname[5].pdat = malloc(wlanIpcMonListTable->pkeyname[5].len);
		memcpy(wlanIpcMonListTable->pkeyname[5].pdat, "IPC_ID", wlanIpcMonListTable->pkeyname[5].len);

		wlanIpcMonListTable->pkeyname[6].len = strlen("FAVERITE");
		wlanIpcMonListTable->pkeyname[6].pdat = malloc(wlanIpcMonListTable->pkeyname[6].len);
		memcpy(wlanIpcMonListTable->pkeyname[6].pdat, "FAVERITE", wlanIpcMonListTable->pkeyname[6].len);

		wlanIpcMonListTable->record_cnt = 0;
		
		save_vtk_table_file_buffer( wlanIpcMonListTable, 0, Get_WlanIPC_MonRes_Num(), WLAN_IPC_MON_LIST_FILE_NAME);

		//printf_one_table(wlanIpcMonListTable);
	}
}

int Get_WlanIPC_MonRes_Num(void)
{
	if(wlanIpcMonListTable == NULL)
	{
		return 0;
	}

	return wlanIpcMonListTable->record_cnt;
}

//dh_20191204_s
int Get_WlanIPC_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_WlanIPC_MonRes_Num(); i++)
	{
		Get_WlanIPC_MonRes_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

//czn_20190221_s
void WlanIPC_MonRes_Reload(void)
{
	free_vtk_table_file_buffer(wlanIpcMonListTable);
	WlanIPC_MonRes_Table_Init();
}
//czn_20190221_e
int Get_WlanIPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(wlanIpcMonListTable == NULL || wlanIpcMonListTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(wlanIpcMonListTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	//printf("Get_IPC_MonRes_Record index=%d\n", index);
	//printf_one_table(wlanIpcMonListTable);
	
	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);	

	return 0;
}

int Modify_WlanIPC_MonRes_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	Modify_one_vtk_record(&record, wlanIpcMonListTable, index);
	
	remove(WLAN_IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( wlanIpcMonListTable, 0, Get_WlanIPC_MonRes_Num(), WLAN_IPC_MON_LIST_FILE_NAME);
	return 0;
}

int Delete_WlanIPC_MonRes_Record(int index)
{
	delete_one_vtk_record(wlanIpcMonListTable, index);

	remove(WLAN_IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( wlanIpcMonListTable, 0, Get_WlanIPC_MonRes_Num(), WLAN_IPC_MON_LIST_FILE_NAME);
	return 0;
}

int Add_WlanIPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	//snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id);
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(wlanIpcMonListTable, &record);

	remove(WLAN_IPC_MON_LIST_FILE_NAME);
	save_vtk_table_file_buffer( wlanIpcMonListTable, 0, Get_WlanIPC_MonRes_Num(), WLAN_IPC_MON_LIST_FILE_NAME);
	return 0;
}

int Add_Or_Modify_WlanIPC_MonRes_Record(IPC_RECORD* ipcRecord)
{
	int i;
	IPC_RECORD record;

	for(i = 0; i < Get_WlanIPC_MonRes_Num(); i++)
	{
		Get_WlanIPC_MonRes_Record(i, &record);

		if(ipcDhcpEnable)
		{
			if(!strcmp(record.id, ipcRecord->id))
			{
				Modify_WlanIPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
		else
		{
			if(!strcmp(record.devUrl, ipcRecord->devUrl))
			{
				Modify_WlanIPC_MonRes_Record(i, ipcRecord);
				return 0;
			}
		}
	}
	
	Add_WlanIPC_MonRes_Record(ipcRecord);
	
	return 0;
}

int Update_WlanIPC_MonRes_Record(char* device_id, char* device_url, char* ip)
{
	IPC_RECORD dat;
	int i;

	if(strcmp(GetNetDeviceNameByTargetIp(inet_addr(ip)), NET_WLAN0))
	{
		return -1;
	}
	
	for(i=0; i < Get_WlanIPC_MonRes_Num(); i++)
	{
		Get_WlanIPC_MonRes_Record(i,&dat);
		if(!strcmp(device_id, dat.id))
		{
			if(!strcmp(device_url, dat.devUrl))
			{
				break;
			}
			else
			{
				if(ipcDhcpEnable)
				{
					strcpy(dat.devUrl, device_url);
					Modify_WlanIPC_MonRes_Record(i, &dat);
				}
			}
		}
	}
	
	return 0;
}

int Clear_WlanIPC_MonRes_Record(void)
{
	int i;
	
	for(i=0; i<wlanIpcMonListTable->record_cnt; i++)
	{
		if(wlanIpcMonListTable->precord[i].pdat != NULL)
		{
			free(wlanIpcMonListTable->precord[i].pdat);
			wlanIpcMonListTable->precord[i].pdat = NULL;
		}
	}
	
	release_one_vtk_tabl(wlanIpcMonListTable);
	remove(WLAN_IPC_MON_LIST_FILE_NAME);
	
	WlanIPC_MonRes_Table_Init();
	
	save_vtk_table_file_buffer(wlanIpcMonListTable, 0, Get_WlanIPC_MonRes_Num(), WLAN_IPC_MON_LIST_FILE_NAME);

	return 0;
}


void DisplayOneWlanIPCList(int index, int x, int y)
{
	//IPC_RECORD dat;
	IPC_ONE_DEVICE dat;
	char *pos1, *pos2;
	char deviceIp[100] = {0};
	int len;
	char display[100];
	
	//if(index < Get_IPC_MonRes_Num())
	if(index < GetIpcNum())
	{
		printf("DisplayOneIPCList index:%d\n", index);
		GetWlanIpcRecord(index, &dat);
		#if 0
		pos1 = strstr(dat.IP, "http://");
		pos1 += strlen("http://");
		pos2 = strstr(pos1, "/onvif/device_service");
		if(pos1 && pos2)
		{
			memcpy(deviceIp, pos1, strlen(pos1) - strlen(pos2));
		}
		#endif
		snprintf(display, 100, "[%s] %s", dat.IP, dat.NAME);
		API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, 0);
	}

}

void DisplayOnePageWlanIPCMonitor(uint8 page)
{
	#if 0
	int i, x, y, maxPage, listStart, maxList;
	POS pos;
	SIZE hv;
	
	maxList = Get_WlanIPC_MonRes_Num();
	printf("DisplayOnePageWlanIPCMonitor ipc_num:%d\n", maxList);
	
	listStart = page*WLAN_ICP_LIST_ICON_MAX;
	
	for( i = 0; i < WLAN_ICP_LIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		DisplayOneWlanIPCList(listStart+i, x, y);
	}

	maxPage = maxList/WLAN_ICP_LIST_ICON_MAX + (maxList%WLAN_ICP_LIST_ICON_MAX ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	#endif
	int i, x, y, maxPage, listStart, maxList;
	POS pos;
	SIZE hv;
	
	//ipcListIconMax = GetListIconNum();
	//maxList = Get_IPC_MonRes_Num();
	maxList = GetWlanIpcNum();
	printf("DisplayOnePageIPCMonitor ipc_num:%d\n", maxList);
	
	listStart = page*GetListIconNum();
	
	for( i = 0; i < GetListIconNum(); i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		#if 0
		if(GetLastNMenu() == MENU_116_MultiRec)
		{
			if( (listStart+i) == recIpcIndex[ipcSelectForCh] )
				ListSelect(listStart+i, 1);
		}
		#endif
		DisplayOneWlanIPCList(listStart+i, x, y);
	}

	maxPage = maxList/GetListIconNum() + (maxList%GetListIconNum() ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	
}

int AddWlanIPC_TableBySearching(char *defualt_user,char *default_pwd,int default_ch)
{
#if 1
	int dis_num,i,add_num = 0;
	char *pos1, *pos2;
	int len;
	
	char IPC_IP[45];
	IPC_RECORD ipcRecord;
	onvif_login_info_t ipcLogin;
	
	ChangeDefaultGateway(NET_WLAN0, NULL);
	
	onvif_device_discovery();
	
	usleep(100000);

	ChangeDefaultGateway(NULL, NULL);
	
	//DisplayOnePageSearchingIPC(0);
	//dis_num = onvif_discovery_device_list_total();
	dis_num =get_ipc_device_total();
	for(i = 0;i < dis_num;i ++)
	{
		//onvif_device_get_addr(i, ipcRecord.devUrl, ipcRecord.id);
		ipc_discovery_device_get_addr(i, ipcRecord.devUrl, ipcRecord.id);
		#if 0
		if(pos1 = strstr(ipcRecord.devUrl, "http://"))
		{
			pos1 += strlen("http://");
			pos2 = strstr(pos1, "/onvif");
			len = (int)(pos2-pos1);
			memcpy(IPC_IP, pos1, len);
			IPC_IP[len] = 0;
		}
		#endif
		strcpy(IPC_IP,ipcRecord.devUrl);
		strcpy(ipcRecord.userName, defualt_user);
		strcpy(ipcRecord.userPwd, default_pwd);
		ipcRecord.channel = default_ch;
		ipcRecord.faverite = 0;
		
		if(!strcmp(GetNetDeviceNameByTargetIp(inet_addr(IPC_IP)), NET_WLAN0))
		{
			if(GetNameByIp(IPC_IP, ipcRecord.name) == 0)
			{
				strcpy(ipcRecord.id, ipcRecord.name);
				ipcRecord.name[20] = 0;
				//if(onvif_Login(ipcRecord.devUrl, ipcRecord.userName, ipcRecord.userPwd, ipcRecord.channel, &info) == 0)
				if(one_ipc_Login(ipcRecord.devUrl, ipcRecord.userName, ipcRecord.userPwd, &ipcLogin) == 0)
				{
					//sprintf(ipcRecord.name,"IP-CAM%d",++add_num);
					//strcpy(ipcRecord.devUrl, ipcLogin.device_url);
					//Add_Or_Modify_WlanIPC_MonRes_Record(&ipcRecord);
					printf("!!!!!!!!!!add tb ok %d,%s\n",i,ipcRecord.name);
				}
				else
				{
					//BEEP_ERROR();
					printf("!!!!!!!!!!fail onvif_Login %d\n",i);
				}
			}
			else
			{
				printf("!!!!!!!!!!fail GetNameByIp %d\n",i);
			}
		}
	}

	return add_num;
#endif	
}

int ConfirmModifyOneWlanIPCMonlist(const char* string)
{
	strcpy(wlanIpcListName, string);
	if(wlanIpcListName[0] != 0)
	{
		strcpy(saveWlanIPCMonList.name, wlanIpcListName);
		Modify_WlanIPC_MonRes_Record(wlanIpcListIndex, &saveWlanIPCMonList);
	}
	return 1;
}

int IPCMonitor(IPC_RECORD record)
{
#if 0
	onvif_login_info_t info;

	if(onvif_check_online(record.devUrl, record.id) != 0)
	{
		return -1;
	}
	//czn_20190107_s
	if(API_Business_Request(Business_State_MonitorIpc) == 0)
	{
		return -1;
	}
	API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	//czn_20190107_e
	if(onvif_get_rtsp_rul_by_deviceUrl(record.devUrl, record.userName, record.userPwd, record.channel, &info) != -1)
	{
		api_onvif_start_mointor_one_stream(0, info.rtsp_url, record.channel, info.width, info.height, 0 );
		sleep(1);
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		StartInitOneMenu(MENU_056_IPC_MONITOR,0,1);
		return 0;
	}
	else
	{
		//czn_20190107_s
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		API_Business_Close(Business_State_MonitorIpc);
		//czn_20190107_e
		return -1;
	}
#endif
	return -1;
}

//cao_20200907
static pthread_mutex_t 	LinphoneIPCMonitorLock = PTHREAD_MUTEX_INITIALIZER;
static int    			LinphoneIPCMonitorState = 0;
static char				LinphoneIPCMonitorRtsp_url[250];

int LinphoneIPCMonitorStart(IPC_ONE_DEVICE dat)
{
	int ipc_vd_type;
	
	dprintf("LinphoneIPCMonitorStart dat.IP = %s, dat.ID=%s\n", dat.IP, dat.ID);
	
	if(ipc_check_online(dat.IP) != 0)
	{
		return -1;
	}

	pthread_mutex_lock(&LinphoneIPCMonitorLock);

	if(LinphoneIPCMonitorState)
	{
		if(!strcmp(dat.CH_DAT[dat.CH_APP].rtsp_url, LinphoneIPCMonitorRtsp_url))
		{
			pthread_mutex_unlock(&LinphoneIPCMonitorLock);
			return 0;
		}
		else
		{
			api_stop_ffmpeg_for_rtp();
			//usleep(100*1000);
		}
	}
	else
	{
		if(API_Business_Request(Business_State_MonitorIpc) == 0)
		{
			pthread_mutex_unlock(&LinphoneIPCMonitorLock);
			return -1;
		}
	}
	dprintf("11111111 LinphoneIPCMonitorStart\n");
	ipc_vd_type = api_start_ffmpeg_for_rtp(dat.CH_DAT[dat.CH_APP].rtsp_url,dat.CH_DAT[dat.CH_APP].vd_type);
	dprintf("22222222222222 LinphoneIPCMonitorStart\n");
	if(ipc_vd_type == 0 || ipc_vd_type == 1)
	{
		//SetRtpVedioEncodeH265(ipc_vd_type);
		LinphoneIPCMonitorState = 1;
		strcpy(LinphoneIPCMonitorRtsp_url, dat.CH_DAT[dat.CH_APP].rtsp_url);
		pthread_mutex_unlock(&LinphoneIPCMonitorLock);

		APP_IPC_ONE_DEVICE appIpcRecord;
		strcpy(appIpcRecord.ID, dat.ID);
		strcpy(appIpcRecord.NAME, dat.NAME);
		
		if(ipc_vd_type == 1)
		{
			strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H265);
		}
		else
		{
			strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H264);
		}
		
		AddOrModifyOneAppIpcListRecord(appIpcRecord);
		
		return 0;
	}

	API_Business_Close(Business_State_MonitorIpc);
	LinphoneIPCMonitorState = 0;
	pthread_mutex_unlock(&LinphoneIPCMonitorLock);
	return -1;
}

int LinphoneIPCMonitorStop(void)
{
	pthread_mutex_lock(&LinphoneIPCMonitorLock);
	api_stop_ffmpeg_for_rtp();
	API_Business_Close(Business_State_MonitorIpc);
	LinphoneIPCMonitorState = 0;
	LinphoneIPCMonitorRtsp_url[0] = 0;
	
	pthread_mutex_unlock(&LinphoneIPCMonitorLock);
	return 0;
}

int StartWlanIPCMonitor(int index)
{
	onvif_login_info_t info;

	if(index >= Get_WlanIPC_MonRes_Num())
	{
		return -1;
	}
	
	if(Get_WlanIPC_MonRes_Record(index, &saveWlanIPCMonList) == -1)
	{
		return -1;
	}

	return IPCMonitor(saveWlanIPCMonList);
}

int StartIPCListMonitor(int index)
{
	onvif_login_info_t info;

	if(index >= Get_IPC_MonRes_Num())
	{
		return -1;
	}
	
	if(Get_IPC_MonRes_Record(index, &saveWlanIPCMonList) == -1)
	{
		return -1;
	}

	return IPCMonitor(saveWlanIPCMonList);
}


void MENU_115_Wlan_IPC_List_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_054_IPC_SETTING)
	{
		wlanIpcListIconSelect = 0;
		wlanIpcListPageSelect = 0;
		wlanIpcListState = ICON_266_IPC_Edit;
	}
	deleteConfirm = 0;

	API_MenuIconDisplaySelectOn(wlanIpcListState);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorList,1, 0);

	DisplayOnePageWlanIPCMonitor(wlanIpcListPageSelect);
	
}




void MENU_115_Wlan_IPC_List_Exit(void)
{
	
}

void MENU_115_Wlan_IPC_List_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_266_IPC_Edit:
					if(wlanIpcListState != ICON_266_IPC_Edit)
					{
						API_MenuIconDisplaySelectOff(wlanIpcListState);
						wlanIpcListState = ICON_266_IPC_Edit;
						API_MenuIconDisplaySelectOn(wlanIpcListState);
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorListEdit,1, 0);
						wlanIpcListPageSelect = 0;
						DisplayOnePageWlanIPCMonitor(wlanIpcListPageSelect);
						if(deleteConfirm)
						{
							deleteConfirm = 0;
							API_SpriteClose(640, 85+65*wlanIpcListLastIconSelect, SPRITE_IF_CONFIRM);
						}
					}
					break;
				case ICON_267_IPC_Delete:
					if(wlanIpcListState != ICON_267_IPC_Delete)
					{
						API_MenuIconDisplaySelectOff(wlanIpcListState);
						wlanIpcListState = ICON_267_IPC_Delete;
						API_MenuIconDisplaySelectOn(wlanIpcListState);
						API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
						API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_MonitorListDelete,1, 0);
						wlanIpcListPageSelect = 0;
						DisplayOnePageWlanIPCMonitor(wlanIpcListPageSelect);
					}
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					wlanIpcListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					wlanIpcListIndex = wlanIpcListPageSelect*WLAN_ICP_LIST_ICON_MAX + wlanIpcListIconSelect;

					if(wlanIpcListIndex >= Get_WlanIPC_MonRes_Num())
					{
						if(deleteConfirm)
						{
							deleteConfirm = 0;
							API_SpriteClose(640, 85+65*wlanIpcListLastIconSelect, SPRITE_IF_CONFIRM);
						}
						return;
					}

					if(wlanIpcListState == ICON_266_IPC_Edit)
					{
						Get_IPC_MonRes_Record(wlanIpcListIndex, &saveWlanIPCMonList);
						strcpy(wlanIpcListName, saveWlanIPCMonList.name);
						EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_Name, wlanIpcListName, 20, COLOR_WHITE, wlanIpcListName, 1, ConfirmModifyOneWlanIPCMonlist);
					}
					else if(wlanIpcListState == ICON_267_IPC_Delete)
					{
						if(!deleteConfirm)
						{
							deleteConfirm = 1;
							wlanIpcListLastIconSelect = wlanIpcListIconSelect;
							API_SpriteDisplay_XY(640, 85+65*wlanIpcListIconSelect, SPRITE_IF_CONFIRM);
						}
						else if(deleteConfirm && wlanIpcListLastIconSelect == wlanIpcListIconSelect)
						{
							deleteConfirm = 0;
							API_SpriteClose(640, 85+65*wlanIpcListIconSelect, SPRITE_IF_CONFIRM);
							Delete_WlanIPC_MonRes_Record(wlanIpcListIndex);
							DisplayOnePageWlanIPCMonitor(wlanIpcListPageSelect);
							BEEP_CONFIRM();
						}
						else if(deleteConfirm && wlanIpcListLastIconSelect != wlanIpcListIconSelect)
						{
							API_SpriteClose(640, 85+65*wlanIpcListLastIconSelect, SPRITE_IF_CONFIRM);
							API_SpriteDisplay_XY(640, 85+65*wlanIpcListIconSelect, SPRITE_IF_CONFIRM);
							wlanIpcListLastIconSelect = wlanIpcListIconSelect;
						}
						
					}
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&wlanIpcListPageSelect, WLAN_ICP_LIST_ICON_MAX, Get_WlanIPC_MonRes_Num(), (DispListPage)DisplayOnePageWlanIPCMonitor);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&wlanIpcListPageSelect, WLAN_ICP_LIST_ICON_MAX, Get_WlanIPC_MonRes_Num(), (DispListPage)DisplayOnePageWlanIPCMonitor);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




one_vtk_table* wlanIpcTempTable = NULL;


void WlanIPC_Temp_Table_Delete(void)
{
	free_vtk_table_file_buffer(wlanIpcTempTable);
	wlanIpcTempTable = NULL;
}

void WlanIPC_Temp_Table_Create(void)
{
	char temp[20];
	
	if(wlanIpcTempTable == NULL)
	{
		wlanIpcTempTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &wlanIpcTempTable->lock, 0);
		wlanIpcTempTable->keyname_cnt = 7;
		wlanIpcTempTable->pkeyname_len = malloc(wlanIpcTempTable->keyname_cnt*sizeof(int));
		wlanIpcTempTable->pkeyname = malloc(wlanIpcTempTable->keyname_cnt*sizeof(one_vtk_dat));
		wlanIpcTempTable->pkeyname_len[0] = 40;
		wlanIpcTempTable->pkeyname_len[1] = 40;
		wlanIpcTempTable->pkeyname_len[2] = 40;
		wlanIpcTempTable->pkeyname_len[3] = 1;
		wlanIpcTempTable->pkeyname_len[4] = 100;
		wlanIpcTempTable->pkeyname_len[5] = 40;
		wlanIpcTempTable->pkeyname_len[6] = 1;
		wlanIpcTempTable->pkeyname[0].len = strlen("IPC_NAME");
		wlanIpcTempTable->pkeyname[0].pdat = malloc(wlanIpcTempTable->pkeyname[0].len);
		memcpy(wlanIpcTempTable->pkeyname[0].pdat, "IPC_NAME", wlanIpcTempTable->pkeyname[0].len);
		
		wlanIpcTempTable->pkeyname[1].len = strlen("USER_NAME");
		wlanIpcTempTable->pkeyname[1].pdat = malloc(wlanIpcTempTable->pkeyname[1].len);
		memcpy(wlanIpcTempTable->pkeyname[1].pdat, "USER_NAME", wlanIpcTempTable->pkeyname[1].len);

		wlanIpcTempTable->pkeyname[2].len = strlen("USER_PWD");
		wlanIpcTempTable->pkeyname[2].pdat = malloc(wlanIpcTempTable->pkeyname[2].len);
		memcpy(wlanIpcTempTable->pkeyname[2].pdat, "USER_PWD", wlanIpcTempTable->pkeyname[2].len);

		wlanIpcTempTable->pkeyname[3].len = strlen("CHANNEL");
		wlanIpcTempTable->pkeyname[3].pdat = malloc(wlanIpcTempTable->pkeyname[3].len);
		memcpy(wlanIpcTempTable->pkeyname[3].pdat, "CHANNEL", wlanIpcTempTable->pkeyname[3].len);

		wlanIpcTempTable->pkeyname[4].len = strlen("DEVICE_URL");
		wlanIpcTempTable->pkeyname[4].pdat = malloc(wlanIpcTempTable->pkeyname[4].len);
		memcpy(wlanIpcTempTable->pkeyname[4].pdat, "DEVICE_URL", wlanIpcTempTable->pkeyname[4].len);

		wlanIpcTempTable->pkeyname[5].len = strlen("IPC_ID");
		wlanIpcTempTable->pkeyname[5].pdat = malloc(wlanIpcTempTable->pkeyname[5].len);
		memcpy(wlanIpcTempTable->pkeyname[5].pdat, "IPC_ID", wlanIpcTempTable->pkeyname[5].len);
		
		wlanIpcTempTable->pkeyname[6].len = strlen("FAVERITE");
		wlanIpcTempTable->pkeyname[6].pdat = malloc(wlanIpcTempTable->pkeyname[6].len);
		memcpy(wlanIpcTempTable->pkeyname[6].pdat, "FAVERITE", wlanIpcTempTable->pkeyname[6].len);

		wlanIpcTempTable->record_cnt = 0;
	}
}

int Get_WlanIPC_Temp_Num(void)
{
	if(wlanIpcTempTable == NULL)
	{
		return 0;
	}

	return wlanIpcTempTable->record_cnt;
}

int Get_WlanIPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	one_vtk_dat *pRecord;
	char ch[5] = {0};
	char fav[5] = {0};
	char recordBuffer[200] = {0};
	int i = 0;
	char* pos1;
	
	if(wlanIpcTempTable == NULL || wlanIpcTempTable->record_cnt <= index)
	{
		return -1;
	}


	pRecord = get_one_vtk_record_without_keyvalue(wlanIpcTempTable, index);
	if(pRecord == NULL)
	{
		return -1;
	}

	char *strTable[7] = {ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ch, ipcRecord->devUrl, ipcRecord->id, fav};
	int strlen[7]={40,40,40,4,100,40,4};
	for(i=0;i<7;i++)
		get_one_record_string(pRecord,i,strTable[i],&strlen[i]);
	ipcRecord->channel = atoi(ch);
	ipcRecord->faverite = atoi(fav);
	return 0;
}

int Add_WlanIPC_Temp_Record(IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	//printf("Add_IPC_MonRes_Record = %s\n", data);
	add_one_vtk_record(wlanIpcTempTable, &record);
	return 0;
}

//dh_20191204_s
int Get_WlanIPC_Temp_Faverite_Num(char* index)
{
	int i;
	IPC_RECORD record;
	int FaveriteNum = 0;
	for(i = 0; i < Get_WlanIPC_Temp_Num(); i++)
	{
		Get_WlanIPC_Temp_Record(i, &record);
		if(record.faverite)
		{
			index[FaveriteNum] = i;
			FaveriteNum++;
		}
	}
	return FaveriteNum;
}
//dh_20191204_e

int Modify_WlanIPC_Temp_Record(int index, IPC_RECORD* ipcRecord)
{
	#define RECORD_MAX_LEN	352
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%s,%s,%s,%d,%s,%s,%d", ipcRecord->name, ipcRecord->userName, ipcRecord->userPwd, ipcRecord->channel, ipcRecord->devUrl, ipcRecord->id, ipcRecord->faverite);
	record.pdat = data;
	record.len = strlen(data);
	//printf("Modify_IPC_MonRes_Record = %s\n", data);
	Modify_one_vtk_record(&record, wlanIpcTempTable, index);
	
	return 0;
}



