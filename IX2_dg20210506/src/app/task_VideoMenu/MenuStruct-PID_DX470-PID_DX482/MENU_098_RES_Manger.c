#include "MENU_098_RES_Manger.h"
//#include "obj_R8001_Table.h"

#define	RES_Manger_ICON_MAX		5
int RES_MangerPageSelect;
int ResSetSelect;

const IconAndText_t RES_MangerIconTable[] = 
{
	//{ICON_RES_TO_SD,			MESG_TEXT_ICON_RES_TO_SD},    
	{ICON_RES_FROM_SD,			MESG_TEXT_ICON_RES_FROM_SD},    
	//{ICON_RES_TO_SYNC,			MESG_TEXT_ICON_RES_TO_SYNC},  
	//{ICON_RES_FROM_SYNC,		MESG_TEXT_ICON_RES_FROM_SYNC}, 
	//{ICON_RES_INFO,				MESG_TEXT_ICON_RES_INFO},
};

const int RES_MangerIconNum = sizeof(RES_MangerIconTable)/sizeof(RES_MangerIconTable[0]);

int Backup_Res_ToSd(void)
{
	typedef struct
	{
		char* backupDir;
		char* tarName;
	}Backup_T;	

	const Backup_T backupTable[] = 
	{
		{CustomerFileDir,			"R8100.tar"},    
		{PublicResFileDir,			"R8000.tar"},    
	};
	const int backupTableNum = sizeof(backupTable)/sizeof(backupTable[0]);

	char tempChar[200];
	char dir1[200],dir2[200];
	int i;
	 
	if(MakeDir(SDCARD_RES_PATH) != 0)
	{
		return -1;
	}

	for(i = 0; i < backupTableNum; i++)
	{
		if( access(backupTable[i].backupDir, F_OK ) == 0 )
		{
			strcpy(dir1, backupTable[i].backupDir);
			while(dir1[strlen(dir1)-1] == '/')
			{
				dir1[strlen(dir1)-1] = 0;
			}
			strcpy(dir2, dir1);

			snprintf(tempChar,200, "tar -cvf %s/%s -C %s %s", SDCARD_RES_PATH, backupTable[i].tarName, dirname(dir1), basename(dir2));
			dprintf("%s\n", tempChar);
			system(tempChar);
			sync();
		}
	}

	UpdateOkSpriteNotify();

	return 0;
}


static void DisplayRES_MangerPageIcon(int page)
{
	int i;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;
	
		
	for(i = 0; i < RES_Manger_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		//API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(i), SPRITE_IF_CONFIRM);
		if(page*RES_Manger_ICON_MAX+i < RES_MangerIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RES_MangerIconTable[i+page*RES_Manger_ICON_MAX].iConText, 1, hv.h - x);
		}
		
	}
	pageNum = RES_MangerIconNum/RES_Manger_ICON_MAX + (RES_MangerIconNum%RES_Manger_ICON_MAX ? 1 : 0);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	
}
 


void MENU_098_RES_Manger_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_RES_Manger,1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);	
	API_DisableOsdUpdate();
	RES_MangerPageSelect = 0;
	DisplayRES_MangerPageIcon(RES_MangerPageSelect);
	API_EnableOsdUpdate();
}

void MENU_098_RES_Manger_Exit(void)
{
	
}

void MENU_098_RES_Manger_Process(void* arg)
{
	int i;
	int RES_MangerIconSelect,RES_MangerIndex;
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&RES_MangerPageSelect, RES_Manger_ICON_MAX, RES_MangerIconNum, (DispListPage)DisplayRES_MangerPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&RES_MangerPageSelect, RES_Manger_ICON_MAX, RES_MangerIconNum, (DispListPage)DisplayRES_MangerPageIcon);
					break;		
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:

					RES_MangerIconSelect = GetCurIcon() - ICON_007_PublicList1;		
					RES_MangerIndex = RES_MangerPageSelect*RES_Manger_ICON_MAX+RES_MangerIconSelect;
					if( RES_MangerIndex >= RES_MangerIconNum)
					{
						return;
					}
					switch(RES_MangerIconTable[RES_MangerIndex].iCon)
					{
						case ICON_RES_TO_SYNC:
							SaveIcon(ICON_RES_TO_SYNC);
							StartInitOneMenu(MENU_092_ResSyncSelect, 0, 1); 	
							break;	
							
						case ICON_RES_FROM_SYNC:
							SaveIcon(ICON_RES_FROM_SYNC);
							StartInitOneMenu(MENU_092_ResSyncSelect, 0, 1); 	
							break;	
						case ICON_RES_INFO:
							StartInitOneMenu(MENU_093_ResInfo, 0, 1); 	
							break;	
						case ICON_RES_TO_SD:
							if(!Judge_SdCardLink())
							{
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								sleep(1);
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
							}
							else
							{
								BusySpriteDisplay(1);
								Backup_Res_ToSd();
								BusySpriteDisplay(0);
							}
							break;	
						case ICON_RES_FROM_SD:
							if(!Judge_SdCardLink())
							{
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								sleep(1);
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
							}
							else
							{
								StartInitOneMenu(MENU_091_RES_Select, 0, 1); 	
							}
							break;	
					}
					break;	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


