
#include "MENU_026_Playback.h"
#include "MENU_023_CallRecordList.h"
#include "obj_memo.h"


CALL_RECORD_LIST_PAGE*	recordPage;

CALL_RECORD_DAT_T one_call_record_temp;
int have_video_index,have_video_total;
int playback_table_index = -1;
int deletVideoConfirm;
int delSpriteX,delSpriteY;

#define PLAYBACK_TIME_POS_X		(10)
#define PLAYBACK_TIME_POS_Y		(5)

	
void DisplayPlaybackTiming( int sec_cnt )
{
	unsigned char str[10];
	int m,s;
	if(sec_cnt%20 == 0)
	{
		AutoPowerOffReset();
	}
	m = sec_cnt/60;
	s = sec_cnt%60;
	memset( str, 0, 10);
	snprintf( str, 10, "%02d:%02d  ", m, s);
	API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, str, strlen(str),1,STR_UTF8, 0);
	printf("playback time:%s\n",str);
}

void DisplayPlaybackInformation( char* filename, int number, int total )
{
	char info[50];
	API_OsdStringClearExt(bkgd_w/2-100, 5, bkgd_w/2, 40);
	API_OsdStringClearExt(bkgd_w/2-10, 40, bkgd_w/2, 40);
	snprintf(info,50,"%s",filename);
	API_OsdStringDisplayExt(bkgd_w/2-100, 5, COLOR_BLUE, info, strlen(info),1,STR_UTF8, 0);
	snprintf(info,50,"%d/%d",number,total);
	API_OsdStringDisplayExt(bkgd_w/2-10, 40, COLOR_BLUE, info, strlen(info),1,STR_UTF8, 0);
}

void Playback_Act(void)
{
	char full_file_name[100];
	if(Judge_SdCardLink() == 1) 
			snprintf(full_file_name,100,"%s/%s",VIDEO_STORE_DIR,one_call_record_temp.relation);
		else
			snprintf(full_file_name,100,"%s/%s",JPEG_STORE_DIR,one_call_record_temp.relation);
	if(strstr(one_call_record_temp.relation, ".avi")&&access(full_file_name, F_OK) == 0)
	{
		API_OsdStringClearExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, 100,40);
		LoadSpriteDisplay(1, 0);
		memo_vd_playback_stop();
		memo_video_playback_start( one_call_record_temp.relation, NULL );
		LoadSpriteDisplay(0, 0);
		DisplayPlaybackInformation(one_call_record_temp.relation,have_video_index,have_video_total);
	}
	else
	{
		//memo_vd_playback_stop();
		//clear_one_layer(0, bkgd_w, bkgd_h);
		//API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, "No file", strlen("No file"),1,STR_UTF8, 0);
		DisplayPlaybackInformation(" ",have_video_index,have_video_total);
		API_OsdUnicodeStringDisplay(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, MESG_TEXT_NoFile, 1, 0);
	}
}

#if 0
void Playback_Act(void)
{
	char disp_info[80];
	
	
	
	memo_vd_playback_stop();
	
	if(strstr(one_call_record_temp.relation, ".avi"))
	{
		DisplayPlaybackTiming(0);
		API_SpriteClose(GetIconXY(ICON_046_Replay).x,GetIconXY(ICON_046_Replay).y,SPRITE_Lock1Disable);
	}
	else
	{
		API_SpriteDisplay_XY(GetIconXY(ICON_046_Replay).x,GetIconXY(ICON_046_Replay).y,SPRITE_Lock1Disable);
		API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, "Image     ", strlen("Image     "),1,STR_UTF8, 0);
	}
	
	if(!memo_video_playback_start( one_call_record_temp.relation, NULL ) && strstr(one_call_record_temp.relation, ".avi"))
	{
		soundCtrl = 0;
	}
	else
	{
		soundCtrl = 1;
	}
	
	if(strcmp(one_call_record_temp.name,"---") != 0)
	{
		snprintf( disp_info, ONE_LIST_MAX_LEN, "20%s %s", one_call_record_temp.time, one_call_record_temp.name );
	}
	else
	{
		char node[40];
		char time[40];
		int recordLen;

		snprintf(time,40,"%d/%d          ",have_video_index,have_video_total);
		API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X+500, PLAYBACK_TIME_POS_Y, COLOR_BLUE, time, strlen(time),1,STR_UTF8, 0);
		
		snprintf( time, 40, "20%s ", one_call_record_temp.time);
		snprintf( node, 40, "%d                        ", one_call_record_temp.target_node);
		API_GetOSD_StringWithID(MESG_TEXT_Node, time, strlen(time), node, strlen(node), disp_info, &recordLen);	
		API_OsdStringDisplayExt(PLAYBACK_INFO_POS_X, PLAYBACK_INFO_POS_Y, COLOR_BLUE, disp_info, recordLen,1,STR_UNICODE, 0);
		return;
	}
	
	DisplayPlaybackInformation(disp_info,have_video_index,have_video_total);
}
#endif
void playback_turn(int act)
{
	int i,video_total_temp;
	CALL_RECORD_DAT_T record_temp;
	char disp_info[40];
	char time_buf[20];
	
	if(act ==0)
	{
		if(have_video_index > 1)
		{
			have_video_index--;
		}
		else
		{
			have_video_index = have_video_total;
		}
	}
	else if(act ==1)
	{
		if(have_video_index < have_video_total)
		{
			have_video_index++;
		}
		else
		{
			have_video_index = 1;
		}
	}
		
	for(i = recordPage->pcall_record->record_cnt - 1, video_total_temp = 0; i >= 0;i --)
	{
		if(get_callrecord_table_record_items( recordPage->pcall_record, i, &record_temp) != 0)
			continue;
		video_total_temp ++;
		if(have_video_index == video_total_temp)
		{
			one_call_record_temp = record_temp;
			playback_table_index = i;
			break;
		}
	}
	char full_file_name[100];
	if(Judge_SdCardLink() == 1) 
		snprintf(full_file_name,100,"%s/%s",VIDEO_STORE_DIR,one_call_record_temp.relation);
		else
			snprintf(full_file_name,100,"%s/%s",JPEG_STORE_DIR,one_call_record_temp.relation);
	if(strstr(one_call_record_temp.relation, ".avi")&&access(full_file_name, F_OK) == 0)
		Playback_Act();
	else
	{
		memo_vd_playback_stop();
		//clearscreen(1);
		StartInitOneMenu(MENU_026_PLAYBACK,0,0);
	}
}

void Playback_UpdateListFocus(void)
{
	int temp;
	
	temp = recordPage->pcall_record->record_cnt - playback_table_index -1;
	
	recordPage->page_num = temp/recordPage->onePageMaxNum;
	recordPage->list_focus = temp%recordPage->onePageMaxNum;
}

void playback_turn_next(void)
{
	
}
/*************************************************************************************************
06. playback
*************************************************************************************************/
void MENU_026_PLAYBACK_Init(int uMenuCnt)	//cao_20161227
{
	int i;
	CALL_RECORD_DAT_T record_temp;
	if(GetLastNMenu()!=MENU_026_PLAYBACK)
	{
		deletVideoConfirm = 0;
		
		for(i =  recordPage->pcall_record->record_cnt - 1,have_video_index = -1,have_video_total = 0; i >= 0;i --)
		{
			if(get_callrecord_table_record_items( recordPage->pcall_record, i, &record_temp) != 0)
				continue;
			have_video_total ++;
			if(i ==  recordPage->pcall_record->record_cnt - recordPage->page_num*recordPage->onePageMaxNum - 1 -  recordPage->list_focus)
			{
				have_video_index = have_video_total;
				one_call_record_temp = record_temp;
				playback_table_index = i;
			}
		}
	}
	Playback_Act();			
	
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	OSD_GetIconInfo(ICON_226_PlaybackDel, &pos, &hv);
	Get_SpriteSize(SPRITE_DelConfirm, &xsize, &ysize);
	delSpriteX = pos.x + (hv.h - pos.x)/2 - xsize/2;
	delSpriteY = pos.y + (hv.v - pos.y)/2 - ysize/2;
	delSpriteX = pos.x;
	delSpriteY = pos.y;
}

void MENU_026_PLAYBACK_Exit(void)
{
	//memo_vd_playback_stop();
}

void MENU_026_PLAYBACK_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int curnLen; int maxLen;} *pData;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
				
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_046_Replay:
					memo_video_replay();
					break;
					
				case ICON_225_PlaybackClose:
					memo_vd_playback_stop();
					Playback_UpdateListFocus();
					popDisplayLastMenu();
					break;
				case ICON_226_PlaybackDel:
					if(deletVideoConfirm == 0)
					{
						deletVideoConfirm = 1;
						API_SpriteDisplay_XY(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						
					}
					else
					{
						memo_vd_playback_stop();
						
						call_record_mark_delete_video(&recordPage->pcall_record->precord[playback_table_index]);
						strcpy(one_call_record_temp.relation, "-");
						Playback_UpdateListFocus();
						UpdateOkSpriteNotify();
						popDisplayLastMenu();
					}
					break;
				case ICON_227_PlaybackNext:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turn(1);
					break;
				case ICON_228_PlaybackLast:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turn(0);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_PLYBAK_STEP:
				pData = (arg + sizeof(SYS_WIN_MSG));
				DisplayPlaybackTiming(pData->curnLen);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
		
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}



