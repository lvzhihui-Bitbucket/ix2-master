
#include "MENU_ShortcutTable.h"
#include "MENU_public.h"

void ShortcutNamelistProcess(void)
{
	StartInitOneMenu(MENU_014_NAMELIST,0,1);
}

void ShortcutEmptyProcess(void)
{

}


MainShortcutIcon mainShortcutIconTable[] = 
{
	{MESG_TEXT_Icon_012_Namelist,			ShortcutNamelistProcess},    
	{MESG_TEXT_Icon_013_InnerCall,			ShortcutEmptyProcess},
	{MESG_TEXT_Icon_014_GuardStation,		ShortcutEmptyProcess},
	{MESG_TEXT_Icon_015_InputNumbers,		ShortcutEmptyProcess},
};

const int mainShortcutIconNum = sizeof(mainShortcutIconTable)/sizeof(mainShortcutIconTable[0]);



