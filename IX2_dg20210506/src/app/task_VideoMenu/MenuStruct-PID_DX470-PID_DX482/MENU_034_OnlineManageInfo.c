#include "MENU_034_OnlineManageInfo.h"
#include "obj_ProgInfoByIp.h"
#include "obj_GetAboutByIp.h"

typedef int (*Device_Info_Disp_Fun_t)(int);

#if 0
#define	INFO_ID_X				273
#define	INFO_VALUE_X			430
#define	INFO_ID_LIMIT			(INFO_VALUE_X-INFO_ID_X)
#define	INFO_VALUE_LIMIT		(770-INFO_VALUE_X)
#define	INFO_Y(i)				(100+65*(i))
#endif

//#define	OnlineManageIconMax	5
int OnlineManageIconMax;
int onlineManageIconSelect;
int onlineManagePageSelect;
char infoData[DATA_LEN];
int info_x, info_y,info_val_x,info_val_y;

int Device_Info_Disp_TitleAndValue(int i, int tetle, int infoId)
{
	char display[40] = {0};
	char temp[30];
	char *pos1, *pos2;
	
	API_OsdUnicodeStringDisplay(info_x, info_y, COLOR_RED, tetle, 1, bkgd_w - info_x);

	snprintf(temp, 30, "<ID=%03d,Value=", infoId);
	pos1 = strstr(infoData, temp);
	
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			memcpy(display, pos1 + strlen("<ID=001,Value="), pos2 - pos1 - strlen("<ID=001,Value="));
			API_OsdStringDisplayExt(info_val_x, info_val_y, DISPLAY_LIST_COLOR, display, strlen(display),1,STR_UTF8, bkgd_w - info_val_x);
		}
	}

	return 0;
}

int Device_Info_Disp_TitleStringAndValue(int i, char* tetleString, int len, int infoId)
{
	char display[40] = {0};
	char temp[30];
	char *pos1, *pos2;
	
	API_OsdStringDisplayExt(info_x, info_y, COLOR_RED, tetleString, len, 1, STR_UNICODE, bkgd_w - info_x);

	snprintf(temp, 30, "<ID=%03d,Value=", infoId);
	pos1 = strstr(infoData, temp);
	
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			memcpy(display, pos1 + strlen("<ID=001,Value="), pos2 - pos1 - strlen("<ID=001,Value="));
			API_OsdStringDisplayExt(info_val_x, info_val_y, DISPLAY_LIST_COLOR, display, strlen(display),1,STR_UTF8, bkgd_w - info_val_x);
		}
	}

	return 0;
}

int Device_Info_Disp_IpAndState(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_IPAddress, ABOUT_ID_IP_Address);
	return 0;
}

int Device_Info_Disp_RM_ADDR(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_ICON_279_RM_ADDR, ABOUT_ID_BD_RM_MS_Nbr);
	return 0;
}

int Device_Info_Disp_NAME(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_ICON_281_Name, ABOUT_ID_Name);
	return 0;
}

int Device_Info_Disp_GlobalNum(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_ICON_278_Global_Nbr, ABOUT_ID_Global_Nbr);
	return 0;
}

int Device_Info_Disp_LocalNum(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_ICON_282_Local_Nbr, ABOUT_ID_Local_Nbr);
	return 0;
}

int Device_Info_Disp_FWVersion(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_SW_Ver, ABOUT_ID_SW_Version);
	return 0;
}

int Device_Info_Disp_HWVersion(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(HWVersion, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_HW_Version);
	return 0;
}

int Device_Info_Disp_UpgradeTime(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(UpgradeTime, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_UpgradeTime);
	return 0;
}

int Device_Info_Disp_UpgradeCode(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(UpgradeCode, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_UpgradeCode);
	return 0;
}


int Device_Info_Disp_UpTime(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(UpTime, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_UpTime);
	return 0;
}

int Device_Info_Disp_Sn(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_SerialNo, ABOUT_ID_SerialNo);
	return 0;
}

int Device_Info_Disp_DeviceType(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_DeviceType, ABOUT_ID_DeviceType);
	return 0;
}

int Device_Info_Disp_DeviceModel(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(DeviceModel, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_DeviceModel);
	return 0;
}

int Device_Info_Disp_AreaCode(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(AreaCode, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_AreaCode);
	return 0;
}

int Device_Info_Disp_TransferState(int i)
{
	char displayName[100];
	int displayNameLen;
	
	API_GetOSD_StringWithString(TransferState, displayName, &displayNameLen);
	
	Device_Info_Disp_TitleStringAndValue(i, displayName, displayNameLen, ABOUT_ID_TransferState);
	return 0;
}

int Device_Info_Disp_Ip(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_IPAddress, ABOUT_ID_IP_Address);
	return 0;
}

int Device_Info_Disp_Mac(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_HW_Address, ABOUT_ID_HW_Address);
	return 0;
}

int Device_Info_Disp_Mask(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_SubnetMask, ABOUT_ID_SubnetMask);
	return 0;
}

int Device_Info_Disp_Route(int i)
{
	Device_Info_Disp_TitleAndValue(i, MESG_TEXT_DefaultRoute, ABOUT_ID_DefaultRoute);
	return 0;
}



Device_Info_Disp_Fun_t Device_Info_Disp_Fun_tab[] = 
{
	Device_Info_Disp_IpAndState,
	Device_Info_Disp_RM_ADDR,
	Device_Info_Disp_NAME,
	Device_Info_Disp_GlobalNum,
	Device_Info_Disp_LocalNum,

	Device_Info_Disp_FWVersion,
	Device_Info_Disp_HWVersion,
	Device_Info_Disp_UpgradeTime,
	Device_Info_Disp_UpgradeCode,
	Device_Info_Disp_UpTime,

	Device_Info_Disp_Sn,
	Device_Info_Disp_DeviceType,
	Device_Info_Disp_DeviceModel,
	Device_Info_Disp_AreaCode,
	Device_Info_Disp_TransferState,

	//Device_Info_Disp_Ip,
	Device_Info_Disp_Mac,
	Device_Info_Disp_Mask,
	Device_Info_Disp_Route,
};

const unsigned char onlineManageSetNum = sizeof(Device_Info_Disp_Fun_tab)/sizeof(Device_Info_Disp_Fun_tab[0]);


static void DisplayOnlineManagePageIcon(int page)
{
	uint8 i;
	int pageNum;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < OnlineManageIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			info_x = pos.x+DISPLAY_DEVIATION_X;
			info_y = pos.y+5;
			info_val_x = info_x+100;
			info_val_y = info_y+40;
			API_OsdStringClearExt(info_x, info_y, bkgd_w-info_x, 80);
		}
		else
		{
			info_x = pos.x+DISPLAY_DEVIATION_X;
			info_y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			info_val_x = info_x+(hv.h - pos.x)/2;
			info_val_y = info_y;
			API_OsdStringClearExt(info_x, info_y, bkgd_w-info_x, 40);
		}
		//info_x = pos.x+DISPLAY_DEVIATION_X;
		//info_y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//info_val_x =pos.x + (hv.h - pos.x)/2;
		//API_OsdStringClearExt(info_x, info_y, bkgd_w-info_x, 40);
		if(page*OnlineManageIconMax+i < onlineManageSetNum)
		{
			(*Device_Info_Disp_Fun_tab[page * OnlineManageIconMax + i])(i);
		}
	}
	pageNum = onlineManageSetNum/OnlineManageIconMax + (onlineManageSetNum%OnlineManageIconMax ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 
 void Device_Info_PageTurn(int act)
 {
	 API_DisableOsdUpdate();
	 if(act == 1)
	 {
		 if(OnlineManageIconMax*(onlineManagePageSelect+1) < onlineManageSetNum)
		 {
			 onlineManagePageSelect ++;
			 DisplayOnlineManagePageIcon(onlineManagePageSelect);
		 }
	 }
	 else
	 {
		 if(onlineManagePageSelect > 0)
		 {
			 onlineManagePageSelect --;
			 DisplayOnlineManagePageIcon(onlineManagePageSelect);
		 }
	 }
	 API_EnableOsdUpdate();
 }

void MENU_034_OnlineManageInfo_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	OnlineManageIconMax = GetListIconNum();
	API_MenuIconDisplaySelectOn(ICON_048_Device);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	onlineManageIconSelect = 0;
	onlineManagePageSelect = 0;

	infoData[0] = 0;
	API_GetAboutByIp(GetSearchOnlineIp(), 2, infoData);
	printf("infoData = %s\n", infoData);
	
	DisplayOnlineManagePageIcon(onlineManagePageSelect);
}

void MENU_034_OnlineManageInfo_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_048_Device);
}

void MENU_034_OnlineManageInfo_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					Device_Info_PageTurn(1);
					break;			
				case ICON_202_PageUp:
					Device_Info_PageTurn(0);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					onlineManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(onlineManagePageSelect*OnlineManageIconMax+onlineManageIconSelect >= onlineManageSetNum)
					{
						return;
					}
											
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




