#include "MENU_public.h"
extern void EmptyFunction(void);
const void* MENU_FUNCTION_TAB[][3] = 
{
	NULL,							NULL,								NULL,
	MENU_001_MAIN_Init, 			MENU_001_MAIN_Process,				MENU_001_MAIN_Exit,
	MENU_002_Monitor_Init, 			MENU_002_Monitor_Process,			MENU_002_Monitor_Exit,
	MENU_003_Intercom_Init, 		MENU_003_Intercom_Process,			MENU_003_Intercom_Exit,
	MENU_004_CallRecord_Init, 		MENU_004_CallRecord_Process,		MENU_004_CallRecord_Exit,
	MENU_005_CallScene_Init, 		MENU_005_CallScene_Process,			MENU_005_CallScene_Exit,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	MENU_009_SipConfig_Init, 		MENU_009_SipConfig_Process,			MENU_009_SipConfig_Exit,
	MENU_010_About_Init, 			MENU_010_About_Process,				MENU_010_About_Exit,
	MENU_011_Settings_Init, 		MENU_011_Settings_Process,			MENU_011_Settings_Exit,
	MENU_012_PublicSetting_Init, 	MENU_012_PublicSetting_Process,		MENU_012_PublicSetting_Exit,
	MENU_013_Calling_Init,			MENU_013_Calling_Process,			MENU_013_Calling_Exit,
	MENU_014_NameList_Init,			MENU_014_NameList_Process,			MENU_014_NameList_Exit,
	MENU_015_InstallSub_Init, 		MENU_015_InstallSub_Process,		MENU_015_InstallSub_Exit,
	NULL,							NULL,								NULL,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_022_OnlineManage_Init,		MENU_022_OnlineManage_Process,		MENU_022_OnlineManage_Exit,
	MENU_023_CallRecordList_Init,	MENU_023_CallRecordList_Process,	MENU_023_CallRecordList_Exit,
	MENU_024_VideoRecordList_Init,	MENU_024_VideoRecordList_Process,	MENU_024_VideoRecordList_Exit,
	MENU_025_SearchOnline_Init,		MENU_025_SearchOnline_Process,		MENU_025_SearchOnline_Exit,
	MENU_026_PLAYBACK_Init, 		MENU_026_PLAYBACK_Process,			MENU_026_PLAYBACK_Exit,
	MENU_027_Calling2_Init,			MENU_027_Calling2_Process,			MENU_027_Calling2_Exit,
	MENU_028_Monitor2_Init,			MENU_028_Monitor2_Process,			MENU_028_Monitor2_Exit,
	MENU_029_SetDateTime_Init,		MENU_029_SetDateTime_Process,		MENU_029_SetDateTime_Exit,
	MENU_030_ShortcutSet_Init,		MENU_030_ShortcutSet_Process,		MENU_030_ShortcutSet_Exit,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	NULL,							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_036_ListEdit_Init,			MENU_036_ListEdit_Process, 			MENU_036_ListEdit_Exit,
	MENU_037_MSList_Init,			MENU_037_MSList_Process, 			MENU_037_MSList_Exit,
	NULL,							NULL,								NULL,
	MENU_039_DIP_Help_Init,			MENU_039_DIP_Help_Process,			MENU_039_DIP_Help_Exit,	
	MENU_040_SDCardInfo_Init,		MENU_040_SDCardInfo_Process,		MENU_040_SDCardInfo_Exit,	
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_044_VideoAdjust_Init,		MENU_044_VideoAdjust_Process, 		MENU_044_VideoAdjust_Exit,
	MENU_045_SDCard_Init,			MENU_045_SDCard_Process, 			MENU_045_SDCard_Exit,
	MENU_046_Restore_Init,			MENU_046_Restore_Process, 			MENU_046_Restore_Exit,
	MENU_047_PhoneManage_Init,		MENU_047_PhoneManage_Process, 		MENU_047_PhoneManage_Exit,
	MENU_048_DoorBell_Init,			MENU_048_DoorBell_Process, 			MENU_048_DoorBell_Exit,	//czn_20170808
	MENU_049_ProductionTest_Init,	MENU_049_ProductionTest_Process,	MENU_049_ProductionTest_Exit,
	MENU_050_FW_Update_Init,		MENU_050_FW_Update_Process, 		MENU_050_FW_Update_Exit,
	MENU_051_MovementTest_Init,		MENU_051_MovementTest_Process, 		MENU_051_MovementTest_Exit,
	MENU_052_SystemSetting_Init,	MENU_052_SystemSetting_Process, 	MENU_052_SystemSetting_Exit,
	MENU_053_Divert_Init,			MENU_053_Divert_Process,			MENU_053_Divert_Exit,

	MENU_054_IPC_Setting_Init,		MENU_054_IPC_Setting_Process,		MENU_054_IPC_Setting_Exit,
	MENU_055_IPC_Login_Init,		MENU_055_IPC_Login_Process, 		MENU_055_IPC_Login_Exit,
	MENU_056_IPC_Monitor_Init,		MENU_056_IPC_Monitor_Process,		MENU_056_IPC_Monitor_Exit,
	MENU_057_IPC_List_Init, 		MENU_057_IPC_List_Process,			MENU_057_IPC_List_Exit,
	MENU_058_IPC_Manage_Init,		MENU_058_IPC_Manage_Process,		MENU_058_IPC_Manage_Exit,
	MENU_059_monListManage_Init,	MENU_059_monListManage_Process, 	MENU_059_monListManage_Exit,
	MENU_060_IPC_Searching_Init,	MENU_060_IPC_Searching_Process, 	MENU_060_IPC_Searching_Exit,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
	MENU_065_InternetTime_Init, 	MENU_065_InternetTime_Process,		MENU_065_InternetTime_Exit,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,	
	EmptyFunction,					EmptyFunction,						EmptyFunction,	
		NULL,							NULL,								NULL,	
	MENU_070_BackFwUpgrade_Init,	MENU_070_BackFwUpgrade_Process, 	MENU_070_BackFwUpgrade_Exit,
	MENU_071_ParaManage_Init,		MENU_071_ParaManage_Process, 		MENU_071_ParaManage_Exit,
	MENU_072_OnePara_Init,			MENU_072_OnePara_Process, 			MENU_072_OnePara_Exit,
	MENU_073_ParaPublic_Init,		MENU_073_ParaPublic_Process, 		MENU_073_ParaPublic_Exit,
	MENU_074_RemoteParaManage_Init, MENU_074_RemoteParaManage_Process,	MENU_074_RemoteParaManage_Exit,
	MENU_075_OneRemotePara_Init,	MENU_075_OneRemotePara_Process, 	MENU_075_OneRemotePara_Exit,
		NULL,							NULL,								NULL,	
		NULL,							NULL,								NULL,	
	MENU_078_AutoCallbackList_Init, MENU_078_AutoCallbackList_Process,	MENU_078_AutoCallbackList_Exit,		//czn_20190216
		NULL,							NULL,								NULL,	
	MENU_080_IM_Extension_Init, 	MENU_080_IM_Extension_Process,		MENU_080_IM_Extension_Exit,		//czn_20190216
	MENU_081_OutdoorStation_Init, 	MENU_081_OutdoorStation_Process,	MENU_081_OutdoorStation_Exit,		//czn_20190216
	MENU_082_AutoSetupWizard_Init, 	MENU_082_AutoSetupWizard_Process,	MENU_082_AutoSetupWizard_Exit,		//czn_20190216
	MENU_083_Autotest_Init, 		MENU_083_Autotest_Process,			MENU_083_Autotest_Exit,		//czn_20190412
	MENU_084_AutotestDevSelect_Init, 	MENU_084_AutotestDevSelect_Process,		MENU_084_AutotestDevSelect_Exit,		//czn_20190412
	MENU_085_AutotestLogView_Init, 	MENU_085_AutotestLogView_Process,		MENU_085_AutotestLogView_Exit,
	MENU_086_CallTestStat_Init, 	MENU_086_CallTestStat_Process,		MENU_086_CallTestStat_Exit,	
	MENU_087_AutoTestTools_Init, 	MENU_087_AutoTestTools_Process,		MENU_087_AutoTestTools_Exit,
	MENU_088_PublicUnlockCode_Init, MENU_088_PublicUnlockCode_Process,	MENU_088_PublicUnlockCode_Exit,
	MENU_089_SwUpgrade_Init, 		MENU_089_SwUpgrade_Process,		MENU_089_SwUpgrade_Exit,
	MENU_090_CardManage_Init,		MENU_090_CardManage_Process,		MENU_090_CardManage_Exit, // 20190521
	MENU_091_ResSelect_Init,			MENU_091_ResSelect_Process,		MENU_091_ResSelect_Exit,
	MENU_092_ResSync_Init,				MENU_092_ResSync_Process,		MENU_092_ResSync_Exit,
	MENU_093_ResInfo_Init,				MENU_093_ResInfo_Process,		MENU_093_ResInfo_Exit,
	MENU_094_ResView_Init,				MENU_094_ResView_Process,		MENU_094_ResView_Exit,
	MENU_095_AddressHealthCheck_Init,	MENU_095_AddressHealthCheck_Process,MENU_095_AddressHealthCheck_Exit,
	MENU_096_SelectSystemType_Init,		MENU_096_SelectSystemType_Process,	MENU_096_SelectSystemType_Exit,
	MENU_097_IM_Call_Nbr_Init,			MENU_097_IM_Call_Nbr_Process,		MENU_097_IM_Call_Nbr_Exit,
	MENU_098_RES_Manger_Init,			MENU_098_RES_Manger_Process,		MENU_098_RES_Manger_Exit,
	MENU_099_BackupAndRestore_Init, 	MENU_099_BackupAndRestore_Process,	MENU_099_BackupAndRestore_Exit,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
	MENU_104_BackupAndRestore2_Init, 	MENU_104_BackupAndRestore2_Process,	MENU_104_BackupAndRestore2_Exit,
	MENU_105_backupList_Init, 			MENU_105_backupList_Process,		MENU_105_backupList_Exit,
	MENU_106_RestoreList_Init, 			MENU_106_RestoreList_Process,		MENU_106_RestoreList_Exit,
		NULL,								NULL,								NULL,
	MENU_108_QuickAccess_Init,			MENU_108_QuickAccess_Process,		MENU_108_QuickAccess_Exit,
	MENU_109_PLAYBACK_INFO_Init, 		MENU_109_PLAYBACK_INFO_Process,		MENU_109_PLAYBACK_INFO_Exit,
	MENU_110_CallIPC_Init, 				MENU_110_CallIPC_Process,			MENU_110_CallIPC_Exit,
	MENU_111_VideoProxySetting_Init,	MENU_111_VideoProxySetting_Process,	MENU_111_VideoProxySetting_Exit,
	MENU_112_GSList_Init,				MENU_112_GSList_Process,			MENU_112_GSList_Exit,
	MENU_113_MonQuart_Init,				MENU_113_MonQuart_Process,			MENU_113_MonQuart_Exit,
	//MENU_114_WlanSetting_Init,			MENU_114_WlanSetting_Process,		MENU_114_WlanSetting_Exit,
	NULL,								NULL,							NULL,
	MENU_115_Wlan_IPC_List_Init,		MENU_115_Wlan_IPC_List_Process, 	MENU_115_Wlan_IPC_List_Exit,
	MENU_116_MultiRec_Init, 			MENU_116_MultiRec_Process,			MENU_116_MultiRec_Exit,
	MENU_117_RecQuad_Init, 				MENU_117_RecQuad_Process,			MENU_117_RecQuad_Exit,
	MENU_118_IpcScenario_Init, 			MENU_118_IpcScenario_Process,		MENU_118_IpcScenario_Exit,
	MENU_119_IpcRecord_Init,			MENU_119_IpcRecord_Process,			MENU_119_IpcRecord_Exit,
	MENU_120_BackResDownload_Init,		MENU_120_BackResDownload_Process,	MENU_120_BackResDownload_Exit,
	MENU_121_ResUdpDownload_Init,		MENU_121_ResUdpDownload_Process,	MENU_121_ResUdpDownload_Exit,
	MENU_122_DvrPlay_Init,				MENU_122_DvrPlay_Process,			MENU_122_DvrPlay_Exit,
	MENU_123_RecSchedule_Init,			MENU_123_RecSchedule_Process,		MENU_123_RecSchedule_Exit,
	MENU_124_WeekSelect_Init,			MENU_124_WeekSelect_Process,		MENU_124_WeekSelect_Exit,
	MENU_NM_Main_Init,					MENU_NM_Main_Process,				MENU_NM_Main_Exit,
	MENU_NM_LanSetting_Init,			MENU_NM_LanSetting_Process,			MENU_NM_LanSetting_Exit,
	MENU_NM_WlanSetting_Init,			MENU_NM_WlanSetting_Process,		MENU_NM_WlanSetting_Exit,		//127
	MENU_NM_WifiInformation_Init,		MENU_NM_WifiInformation_Process,	MENU_NM_WifiInformation_Exit,		//128
	MENU_129_RestoreSelective_Init,		MENU_129_RestoreSelective_Process,	MENU_129_RestoreSelective_Exit,
	MENU_130_FileList_Init, 			MENU_130_FileList_Process,			MENU_130_FileList_Exit,
	MENU_131_ViewFile_Init, 			MENU_131_ViewFile_Process,			MENU_131_ViewFile_Exit,
	MENU_132_SearchTest_Init, 			MENU_132_SearchTest_Process,		MENU_132_SearchTest_Exit,

	MENU_133_RlcRelayMapSetting_Init, 	MENU_133_RlcRelayMapSetting_Process,MENU_133_RlcRelayMapSetting_Exit,
	MENU_134_RlcRelayOptionSetting_Init,MENU_134_RlcRelayOptionSetting_Process,		MENU_134_RlcRelayOptionSetting_Exit,
	MENU_135_DeviceFloorSetting_Init,	MENU_135_DeviceFloorSetting_Process,		MENU_135_DeviceFloorSetting_Exit,

	MENU_136_MulLangKeypad_Init, 		MENU_136_MulLangKeypad_Process,		MENU_136_MulLangKeypad_Exit,
	MENU_136_MulLangKeypad_Init, 		MENU_136_MulLangKeypad_Process,		MENU_136_MulLangKeypad_Exit,
	MENU_138_ExtModuleSelect_Init,		MENU_138_ExtModuleSelect_Process,	MENU_138_ExtModuleSelect_Exit,
	MENU_139_KeySelect_Init,			MENU_139_KeySelect_Process, 		MENU_139_KeySelect_Exit,
	MENU_140_KeyConfig_Init,			MENU_140_KeyConfig_Process, 		MENU_140_KeyConfig_Exit,
	MENU_141_ListenTalk_Init,			MENU_141_ListenTalk_Process, 		MENU_141_ListenTalk_Exit,
	MENU_142_DxMonitor_Init,			MENU_142_DxMonitor_Process, 		MENU_142_DxMonitor_Exit,
	MENU_143_RM_List_Init,				MENU_143_RM_List_Process, 			MENU_143_RM_List_Exit,
	MENU_144_RM_Display_Init,			MENU_144_RM_Display_Process, 		MENU_144_RM_Display_Exit,

};
const uint16 NotCtrlPressReleaseDispIcon[] = {

	ICON_203_Monitor,
	ICON_204_quad,
	ICON_205_CallScene,
	ICON_206_Namelist,
	ICON_017_MissedCalls,
	ICON_018_IncomingCalls,
	ICON_019_OutgoingCalls,
	ICON_020_playback,
	ICON_024_CallTune,
	ICON_025_general,
	ICON_026_InstallerSetup,
	ICON_027_SipConfig,
	ICON_029_IP_ADDR,
	ICON_030_CALL_NUMBER,
	ICON_031_OnsiteTools,
	ICON_032_System,
	ICON_033_Manager,
	ICON_034_ListView,
	ICON_035_ListAdd,
	ICON_036_ListDelete,
	ICON_037_ListEdit,
	ICON_038_ListCheck,
	//ICON_400_ListSync,	//temp //czn_20190525
	ICON_051_ExternalUnit,
	ICON_052_IM_Process,
	ICON_053_OS_Process,
	ICON_054_IPC_Process,
	ICON_218_PublicSetTitle,
	ICON_229_ShortcutSet1,
	ICON_230_ShortcutSet2,
	ICON_231_ShortcutSet3,
	ICON_232_ShortcutSet4,
	ICON_256_Edit,
	ICON_265_IPC_Monitor,
	ICON_266_IPC_Edit,
	ICON_267_IPC_Delete,
	ICON_269_MonListSelect,
	ICON_270_MS_List,
	ICON_040_AboutPage1,
	ICON_041_AboutPage2,
	ICON_042_AboutPage3,
	ICON_043_AboutPage4,
	ICON_044_AboutPage5,
	ICON_045_Upgrade,
	ICON_048_Device,
	ICON_049_Parameter,
	ICON_050_Reboot,
	ICON_IdCard_List, // 20190521
	ICON_IdCard_Add,
	ICON_IdCard_DelAll,
	ICON_CallRecordDelAll, // zfz_20190601
	ICON_379_Shortcut5, // zfz_20190601
};

const uint16 NotCtrlPressReleaseDispIconNum = sizeof(NotCtrlPressReleaseDispIcon)/sizeof(uint16);

#define MESSAGE_SPRITE_INFORM_X_SIZE	280
#define MESSAGE_SPRITE_INFORM_Y_SIZE	130

#define MESSAGE_INFORM_TIME				5		//��λ��

static int messageDisplayState;

void MessageReportDisplay(const char* preStr, int str_id, const char* lstStr, int fgcolor, int fnt_type, int time)
{
	int x, y;

	x = (bkgd_w - MESSAGE_SPRITE_INFORM_X_SIZE)/2;
	y = (bkgd_h - MESSAGE_SPRITE_INFORM_Y_SIZE)/2;
	
	SetMessageDisplayState(1);
	MessageDisplay_Timer_Set(time < 0 ? MESSAGE_INFORM_TIME : time);
	API_DisableOsdUpdate();
	API_SpriteDisplay_XY(x, y, SPRITE_Message);
	API_OsdUnicodeStringDisplayWithIdAndAsc2(x+10, y+10, OSD_LAYER_SPRITE, fgcolor, COLOR_MESSAGE_BACK, str_id, fnt_type, preStr, lstStr, MESSAGE_SPRITE_INFORM_X_SIZE-20, MESSAGE_SPRITE_INFORM_Y_SIZE-20);
	API_EnableOsdUpdate();
}

void MessageClose(void)
{
	if(GetMessageDisplayState())
	{
		int x, y;
		
		x = (bkgd_w - MESSAGE_SPRITE_INFORM_X_SIZE)/2;
		y = (bkgd_h - MESSAGE_SPRITE_INFORM_Y_SIZE)/2;
		
		API_SpriteClose(x, y, SPRITE_Message);
	}
	SetMessageDisplayState(0);
	MessageDisplay_Timer_Set(0);
}

int GetMessageDisplayState(void)
{
	return messageDisplayState;
}

void SetMessageDisplayState(int state)
{
	messageDisplayState = state;
}

void UnlockReport(int reprotType)
{
	if(reprotType == 0)
	{
		char tempChar[5];
		 API_Event_IoServer_InnerRead_All(DS_LIFT_CTRL_ENABLE, tempChar);
	    if(atoi(tempChar))
	    {
	      ImReportIxminiUnlock();
	    }
		MessageReportDisplay(NULL, MESG_TEXT_DoorIsOpen, NULL, COLOR_RED, 1, 10);
		//BEEP_CONFIRM();
	}
	else
	{
		MessageReportDisplay(NULL,MESG_TEXT_DoorOpenError,NULL, COLOR_RED, 1, 10);
		//BEEP_ERROR();
	}
}

void* GetMenuInformData(void* arg)
{
	return (void*)( ((char*)arg) + sizeof(SYS_WIN_MSG) );
}

void RingReqReport(const char* message, int time)
{
	printf("message=%s, time = %d\n", message, time);
	MessageReportDisplay(NULL, 0, message, COLOR_RED, 1, time);
	BEEP_CONFIRM();
}



void ProgCallNbrReqReport(void)
{
	MessageReportDisplay("Prog Call_Nbr:", 0, GetSysVerInfo_BdRmMs(), COLOR_RED, 1, 5);
	BEEP_CONFIRM();
}
