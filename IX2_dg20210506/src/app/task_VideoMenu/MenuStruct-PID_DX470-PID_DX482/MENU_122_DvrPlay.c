#include "MENU_public.h"
#include "MENU_119_IpcRecord.h"
#include "ffmpeg_play.h"

int dvrplay_index;
char playname[50];
int maxTime, ffFlag;
int timeX,timeY,maxTimeX,barX,barY,barW,barH;
OS_TIMER playffTiming;
int lastpos;
int VdDelConfirm;
//pthread_mutex_t 	playlock = PTHREAD_MUTEX_INITIALIZER;

void DisplayPlayTiming( int sec_cnt, int x, int y )
{
	unsigned char str[20];
	int h,m,s;
	if(sec_cnt%20 == 0)
	{
		AutoPowerOffReset();
	}
	h = sec_cnt/3600;
	m = (sec_cnt%3600)/60;
	s = (sec_cnt%3600)%60;
	snprintf( str, 20, "%02d:%02d:%02d", h, m, s);
	API_OsdStringDisplayExt(x, y, COLOR_BLUE, str, strlen(str),1,STR_UTF8, 0);
}

void DisplayPlayBar( int time )
{
	int x;
	API_ProgBarClose(barX, barY, barW, 2);
	//printf("DisplayPlayBar lastpos=%d\n",lastpos);
	API_SpriteClose(lastpos, barY-8, SPRITE_BarSelect);
	x = barX+barW*time/maxTime ;
	API_SpriteDisplay_XY(x, barY-8, SPRITE_BarSelect);
	//printf("DisplayPlayBar curpos=%d\n",x);
	lastpos = x;
	API_ProgBarDisplay(barX, barY, barW*time/maxTime, 2, COLOR_BLUE);
}

void vd_play_second_step(int sec, int max)
{
	if(GetCurMenuCnt() != MENU_122_DvrPlay)
		return;
	if(max)
	{
		maxTime = max;
		DisplayPlayTiming(maxTime, maxTimeX, timeY);
	}
		
	struct {int curnLen; int maxLen;} data;
	if(maxTime)
	{
		data.curnLen = sec;
		data.maxLen = max;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_PLYBAK_STEP, (char*) &data, 8);
	}
	
	if(sec == maxTime)
	{
		popDisplayLastMenu();
	}
}

void vd_replay(void)
{
	if( api_getPlayState() == FFMPEG_PLAYBACK_RUN )
	{
		api_ffmpegPlayPause();
		API_SpriteDisplay_XY(GetIconXY(ICON_046_Replay).x+20,GetIconXY(ICON_046_Replay).y,SPRITE_Pause);	
	}
	else if( api_getPlayState() == FFMPEG_PLAYBACK_PAUSE )
	{
		api_ffmpegPlayContinue();
		API_SpriteClose(GetIconXY(ICON_046_Replay).x+20,GetIconXY(ICON_046_Replay).y,SPRITE_Pause);	
	}
	else if( api_getPlayState() == FFMPEG_PLAYBACK_END )
	{
		DisplayPlayTiming(0, timeX, timeY);
		API_ProgBarClose(barX, barY, barW, 2);
		api_ffmpegPlayReplay();
	}

}

void DisplayPlayInformation( char* filename, int number )
{
	char info[50];
	API_OsdStringClearExt(bkgd_w/2-100, 5, bkgd_w/2, 40);
	API_OsdStringClearExt(bkgd_w/2-10, 40, bkgd_w/2, 40);
	snprintf(info,50,"%s",filename);
	API_OsdStringDisplayExt(bkgd_w/2-100, 5, COLOR_BLUE, info, strlen(info),1,STR_UTF8, 0);
	snprintf(info,50,"%d/%d",number,GetIpcRecTableNum());
	API_OsdStringDisplayExt(bkgd_w/2-10, 40, COLOR_BLUE, info, strlen(info),1,STR_UTF8, 0);
}

void vd_play_turn(int act)
{
	char full_file_name[100];
	api_stop_ffmpegPlay();
	if(act == -1)//�Ϸ�
	{
		if(dvrplay_index > 0)
			dvrplay_index--;
		else
			dvrplay_index = GetIpcRecTableNum() - 1;
	}
	else
	{
		if(dvrplay_index < GetIpcRecTableNum() - 1)
			dvrplay_index++;
		else
			dvrplay_index = 0;
	}

	API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	usleep(500*1000);
	GetIpcRecRecord(dvrplay_index, playname);
	snprintf(full_file_name,100,"%s/%s",VIDEO_STORE_DIR,playname);
	api_start_ffmpegPlay(full_file_name, vd_play_second_step);
	API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	DisplayPlayTiming(0, timeX, timeY);
	API_ProgBarClose(barX, barY, barW, 2);
	DisplayPlayInformation(playname, dvrplay_index+1);
	API_SpriteClose(GetIconXY(ICON_046_Replay).x+20,GetIconXY(ICON_046_Replay).y,SPRITE_Pause);	
}

void Enter_DvrPlay(int index)
{
	dvrplay_index = index;
	char full_file_name[100];
	GetIpcRecRecord(index, playname);
	snprintf(full_file_name,100,"%s/%s",VIDEO_STORE_DIR,playname);
	if(api_start_ffmpegPlay(full_file_name, vd_play_second_step) ==0)
	{
		StartInitOneMenu(MENU_122_DvrPlay,0,1);
	}
}

void playffTimerCallback(void)
{
	if(ffFlag)
		api_ffmpegPlayFf(0, 1);
	else
		api_ffmpegPlayFf(0, -1);
	OS_RetriggerTimer(&playffTiming);
}

void MENU_122_DvrPlay_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_223_PlaybackFB, &pos, &hv);
	//timeX = pos.x-100;
	timeX = pos.x-120;
	timeY = pos.y;
	OSD_GetIconInfo(ICON_224_PlaybackFF, &pos, &hv);
	maxTimeX = hv.h;

	OSD_GetIconInfo(ICON_222_BarPlay, &pos, &hv);
	barX = pos.x;
	barY = pos.y + (hv.v - pos.y)/2;
	barW = hv.h - pos.x;
	//barH = hv.v - pos.y;
	DisplayPlayInformation(playname, dvrplay_index+1);
	static int playfftimer_init = 0;
	if(playfftimer_init == 0)
	{
		playfftimer_init = 1;
		OS_CreateTimer(&playffTiming, playffTimerCallback, 300/25);	//300ms
	}
	VdDelConfirm = 0;
}




void MENU_122_DvrPlay_Exit(void)
{
	OS_StopTimer(&playffTiming);
	AutoPowerOffReset();
	api_stop_ffmpegPlay();
	//api_h264_show_stop(0);
	//memo_vd_playback_stop();
}

void MENU_122_DvrPlay_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int curnLen; int maxLen;} *pData;
	int barNext;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{

				case ICON_046_Replay:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					vd_replay();
				break;
				case ICON_225_PlaybackClose:
					popDisplayLastMenu();
				break;
				case ICON_226_PlaybackDel:
					if(VdDelConfirm == 0)
					{
						VdDelConfirm = 1;
						API_SpriteDisplay_XY(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						
					}
					else
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						DeleteOneRecRecord(dvrplay_index);
						popDisplayLastMenu();
					}
				break;
				case ICON_227_PlaybackNext:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					vd_play_turn(1);
				break;
				case ICON_228_PlaybackLast:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					vd_play_turn(-1);
				break;
				case ICON_223_PlaybackFB:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					api_ffmpegPlayFf(0, -1);
					OS_StopTimer(&playffTiming);
				break;
				case ICON_224_PlaybackFF:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					api_ffmpegPlayFf(0, 1);
					OS_StopTimer(&playffTiming);
				break;
				case ICON_222_BarPlay:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					
					if( get_pane_type() == 0 )
						barNext = maxTime*(bkgd_w-pglobal_win_msg->wparam - barX)/barW;
					else if( get_pane_type() == 1 )
						barNext = maxTime*(bkgd_w-pglobal_win_msg->lparam - barX)/barW;
					else
						barNext = maxTime*(pglobal_win_msg->lparam - barX)/barW;
					DisplayPlayBar(barNext);
					api_ffmpegPlayFf(barNext, 1);
					break;
				
						
			}
		}
		else if( pglobal_win_msg->status == TOUCHMOVE )
		{
			switch(GetCurIcon())
			{
				case ICON_222_BarPlay:
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					if( get_pane_type() == 0 )
						barNext = maxTime*(bkgd_w-pglobal_win_msg->wparam - barX)/barW;
					else if( get_pane_type() == 1 )
						barNext = maxTime*(bkgd_w-pglobal_win_msg->lparam - barX)/barW;
					else
						barNext = maxTime*(pglobal_win_msg->lparam - barX)/barW;
					DisplayPlayBar(barNext);
					api_ffmpegPlayFf(barNext, 1);
				break;
			}
		}
		else if( pglobal_win_msg->status == TOUCHPRESS )
		{
			switch(GetCurIcon())
			{
				case ICON_223_PlaybackFB:
				case ICON_224_PlaybackFF:	
					if(VdDelConfirm)
					{
						API_SpriteClose(GetIconXY(ICON_226_PlaybackDel).x, GetIconXY(ICON_226_PlaybackDel).y, SPRITE_DelConfirm);
						VdDelConfirm = 0;
					}
					ffFlag = GetCurIcon() - ICON_223_PlaybackFB;
					OS_RetriggerTimer(&playffTiming);
				break;
			}

		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_PLYBAK_STEP:
				pData = (arg + sizeof(SYS_WIN_MSG));
				DisplayPlayTiming(pData->curnLen, timeX, timeY);
				DisplayPlayBar(pData->curnLen);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


