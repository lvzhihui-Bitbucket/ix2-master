#include "MENU_public.h"
#include "MENU_019_Keypad.h"

extern KbdProperty kbd_property;
KeyPad keypad;
//keypad 页显示位置
int KeypadTitleX, KeypadTitleY, KeypadAscX;

void EnterKeypadMenu(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitString, unsigned char highLight, InputOK process)
{
	keypad.type = type;						//键盘类型123 or abd
	keypad.maxLen = inputMaxLen;			//最长输入
	keypad.color = Color;					//显示字体颜色
	keypad.dispTitle = title;				//显示输入标题
	keypad.input = pInput;					//输入数据指针
	keypad.initString = pInitString;		//初始输入数据指针
	keypad.highLight = highLight;			//是否全选
	keypad.enterKeypadFlag = 1;				//是否第一次进入键盘
	keypad.inputProcess = process;			//输入结束处理

	kbd_property.inputSize = inputMaxLen;
	kbd_property.dispColor = Color;
	kbd_property.highLight = highLight;

	if( get_pane_type() == 7 )
	{
		keypad.type = KEYPAD_ALL;
		StartInitOneMenu(MENU_017_KEYPAD_CHAR, 0, 1);
	}
	else
	{
		if(keypad.type == KEYPAD_CHAR)
		{
			StartInitOneMenu(MENU_017_KEYPAD_CHAR, 0, 1);
		}
		else if(keypad.type == KEYPAD_NUM)
		{
			StartInitOneMenu(MENU_018_KEYPAD_NUM, 0, 1);
		}
	}

}

static void SwitchKeypadType(unsigned char type)
{
	char* pDisplay;
	
	if(keypad.type != type)
	{
		keypad.type = type;
		keypad.enterKeypadFlag = 0;
		
		if(keypad.type == KEYPAD_CHAR)
		{
			StartInitOneMenu(MENU_017_KEYPAD_CHAR, 0, 0);
			//API_BG_Display(MENU_BACK_GROUND_006_KeyPadChar);
			if(kbd_property.capsLock == 0)
			{
				pDisplay = "abc   ";
			}
			else if(kbd_property.capsLock == 1)
			{
				pDisplay = "Abc   ";
			}
			else
			{
				pDisplay = "ABC   ";
			}
			API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, pDisplay, strlen(pDisplay),1,STR_UTF8, 0);
		}
		else if(keypad.type == KEYPAD_NUM)
		{
			StartInitOneMenu(MENU_018_KEYPAD_NUM, 0, 0);
			//API_BG_Display(MENU_BACK_GROUND_007_KeyPadNum);
			API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, "123", strlen("123"),1,STR_UTF8, 0);
		}
	}
}
void MENU_019_Keypad_Init(int uMenuCnt)
{
	char* pDisplay;
	if(keypad.enterKeypadFlag)
	{
		API_KeyboardInit( 0,0,1 ,kbd_property.inputSize, kbd_property.dispColor);					//初始化键盘
		API_KeyboardInputStart(keypad.initString, kbd_property.highLight); 							//把原来的字符显示出来
	}
	else
	{
		DisplayAllScreen(kbd_property.dispColor, kbd_property.highLight, 1);
	}

	KeypadTitleX = GetIconXY(ICON_177_KeyBar1).x;
	KeypadTitleY = GetIconXY(ICON_177_KeyBar1).y;
	KeypadAscX = GetIconXY(ICON_178_KeyBar2).x;
	API_OsdUnicodeStringDisplay(KeypadTitleX, KeypadTitleY, DISPLAY_TITLE_COLOR, keypad.dispTitle, 1, 0);
	
	if(get_pane_type() != 7)
	{
		if(keypad.type == KEYPAD_NUM)
			API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, "123", strlen("123"),1,STR_UTF8, 0);
		else if(keypad.type == KEYPAD_CHAR)
		{
			if(kbd_property.capsLock == 0)
			{
				pDisplay = "abc   ";
			}
			else if(kbd_property.capsLock == 1)
			{
				pDisplay = "Abc   ";
			}
			else
			{
				pDisplay = "ABC   ";
			}
			API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, pDisplay, strlen(pDisplay),1,STR_UTF8, 0);
		}
	}
}

void MENU_019_Keypad_Exit(void)
{
	//strcpy(keypadPInput, API_KeyboardInputEnd());
}

void MENU_019_Keypad_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int y;
	char* pDisplay;
	extern uint8 wifiInputFinish;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					//DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_180_KeyEdit:
					API_KeyboardSetHighLight(!kbd_property.highLight);
					break;

				default:
					switch(API_KeyboardInputOneKey(keypad.type, GetCurIcon()))
					{
						case OK_KEY_VALUE:
							if(keypad.input != NULL)
							{
								strcpy(keypad.input, kbd_property.inputbuf);
							}
							
							if(keypad.inputProcess != NULL)
							{
								int ret;

								ret = (*keypad.inputProcess)(kbd_property.inputbuf);

								if(ret == 0)
								{
									BEEP_ERROR();
								}
								else if(ret == 1)
								{
									popDisplayLastMenu();
								}
								else
								{
									;
								}
							}
							else
							{
								popDisplayLastMenu();
							}
							break;
							
						case RETURN_KEY_VALUE:
							popDisplayLastMenu();
							break;
						case TURN_TO_NUM_KEY_VALUE:
							SwitchKeypadType(KEYPAD_NUM);
							break;
						case TURN_TO_CHAR_KEY_VALUE:
							SwitchKeypadType(KEYPAD_CHAR);
							break;
						case CAPSLOCK_KEY_VALUE:
							if(kbd_property.capsLock == 0)
							{
								pDisplay = "abc   ";
							}
							else if(kbd_property.capsLock == 1)
							{
								pDisplay = "Abc   ";
							}
							else
							{
								pDisplay = "ABC   ";
							}
							API_OsdStringDisplayExt(KeypadAscX, KeypadTitleY, COLOR_GREEN, pDisplay, strlen(pDisplay),1,STR_UTF8, 0);
							break;

					}
					break;
					
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


const KeyProperty ASC_Char_Tab[]=
{
	{ICON_101_Key00,	  'q'},
	{ICON_102_Key01,	  'w'},
	{ICON_103_Key02,	  'e'},
	{ICON_104_Key03,	  'r'},
	{ICON_105_Key04,	  't'},
	{ICON_106_Key05,	  'y'},
	{ICON_107_Key06,	  'u'},
	{ICON_108_Key07,	  'i'},
	{ICON_109_Key08,	  'o'},
	{ICON_110_Key09,	  'p'},
	{ICON_111_Key10,	  'a'},
	{ICON_112_Key11,	  's'},
	{ICON_113_Key12,	  'd'},
	{ICON_114_Key13,	  'f'},
	{ICON_115_Key14,	  'g'},
	{ICON_116_Key15,	  'h'},
	{ICON_117_Key16,	  'j'},
	{ICON_118_Key17,	  'k'},
	{ICON_119_Key18,	  'l'},
	{ICON_120_Key19,	  CAPSLOCK_KEY_VALUE},
	{ICON_121_Key20,	  'z'},
	{ICON_122_Key21,	  'x'},
	{ICON_123_Key22,	  'c'},
	{ICON_124_Key23,	  'v'},
	{ICON_125_Key24,	  'b'},
	{ICON_126_Key25,	  'n'},
	{ICON_127_Key26,	  'm'},
	{ICON_128_Key27,	  BACKSPACE_KEY_VALUE},
	{ICON_129_Key28,	  TURN_TO_NUM_KEY_VALUE},
	{ICON_130_Key29,	  ','},
	{ICON_131_Key30,	  '.'},
	{ICON_132_Key31,	  ' '},
	{ICON_133_Key32,	  '\r'},
	{ICON_134_Key33,	  RETURN_KEY_VALUE},
	{ICON_135_Key34,	  OK_KEY_VALUE},
};      
    
const uint8 ASC_CHAR_TAB_NUMBERS = sizeof( ASC_Char_Tab ) / sizeof( ASC_Char_Tab[0] );
    
const KeyProperty ASC_NUM_Tab[]= 
{
	{ICON_136_Key35,	  '1'},
	{ICON_137_Key36,	  '2'},
	{ICON_138_Key37,	  '3'},
	{ICON_139_Key38,	  '4'},
	{ICON_140_Key39,	  '5'},
	{ICON_141_Key40,	  '6'},
	{ICON_142_Key41,	  '7'},
	{ICON_143_Key42,	  '8'},
	{ICON_144_Key43,	  '9'},
	{ICON_145_Key44,	  '0'},
	{ICON_146_Key45,	  '_'},
	{ICON_147_Key46,	  '\\'},
	{ICON_148_Key47,	  '|'},
	{ICON_149_Key48,	  '~'},
	{ICON_150_Key49,	  '<'},
	{ICON_151_Key50,	  '>'},
	{ICON_152_Key51,	  '$'},
	{ICON_153_Key52,	  '.'},
	{ICON_154_Key53,	  '/'},
	{ICON_155_Key54,	  '@'},
	{ICON_156_Key55,	  '%'},
	{ICON_157_Key56,	  ':'},
	{ICON_158_Key57,	  '"'},
	{ICON_159_Key58,	  '\''},
	{ICON_160_Key59,	  '?'},
	{ICON_161_Key60,	  '!'},
	{ICON_162_Key61,	  '['},
	{ICON_163_Key62,	  ']'},
	{ICON_164_Key63,	  '&'},
	{ICON_165_Key64,	  '#'},
	{ICON_166_Key65,	  TURN_TO_CHAR_KEY_VALUE},
	{ICON_167_Key66,	  '+'},
	{ICON_168_Key67,	  '-'},
	{ICON_169_Key68,	  '='},
	{ICON_170_Key69,	  '*'},
	{ICON_171_Key70,	  ' '},
	{ICON_172_Key71,	  BACKSPACE_KEY_VALUE},
	{ICON_173_Key72,	  RETURN_KEY_VALUE},
	{ICON_174_Key73,	  OK_KEY_VALUE},

};

const uint8 ASC_NUM_TAB_NUMBERS = sizeof( ASC_NUM_Tab ) / sizeof( ASC_NUM_Tab[0] );


