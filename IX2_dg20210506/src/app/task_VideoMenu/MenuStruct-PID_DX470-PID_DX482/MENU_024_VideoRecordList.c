
#include "MENU_public.h"
#include "MENU_023_CallRecordList.h"
#include "MENU_024_VideoRecordList.h"

//#define	VideoRecordIconMax	5
int VideoRecordIconMax;

CALL_RECORD_LIST_PAGE	videoRecordPage;
extern CALL_RECORD_LIST_PAGE*	recordPage;


void SetVideoRecordPageType( CALL_RECORD_LIST_TYPE type )
{
	videoRecordPage.type 			= type;
	videoRecordPage.act_flag		= 0;
	videoRecordPage.page_num		= 0;	
	videoRecordPage.list_focus 		= 0;
}

void DisplayOneVideoRecord( CALL_RECORD_DAT_T* pcall_record, int x, int y, int color )
{
	char recordBuffer[80] = {0};
	int	recordLen;
	char time_buf[20];
	struct in_addr temp;
	
	API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	
	if(strcmp(pcall_record->name,"---") != 0)
	{
		snprintf( recordBuffer, ONE_LIST_MAX_LEN, "[20%s]%s", pcall_record->time, pcall_record->name );
		recordLen = strlen(recordBuffer);
		API_OsdStringDisplayExt(x, y, color, recordBuffer, recordLen,(get_pane_type()==7)? 0:1,STR_UTF8, bkgd_w-x);
	}
	else
	{
		char node[40];
		char time[40];
		snprintf( time, 40, "[20%s]", pcall_record->time);
		snprintf( node, 40, "%d", pcall_record->target_node);
		API_GetOSD_StringWithID(MESG_TEXT_Node, time, strlen(time), node, strlen(node), recordBuffer, &recordLen);	
		API_OsdStringDisplayExt(x, y, color, recordBuffer, recordLen,(get_pane_type()==7)? 0:1,STR_UNICODE, bkgd_w-x);
	}
}

void MenuListGetVideoRecordPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, stringIndex;
	int list_start;
	char recordBuffer[80] = {0};
	int recordLen, unicode_len;
	char unicode_buf[200];
	char time[40];
	char node[40];
	
	pDisp->dispCnt = 0;
	
	if( videoRecordPage.pcall_record == NULL )
	{
		printf("---videoRecordPage.pcall_record is empty----\n");
		return -1;
	}
	
	//API_DisableOsdUpdate();

	list_start = videoRecordPage.pcall_record->record_cnt - currentPage*onePageListMax - 1;
	
	for( i = 0; i < onePageListMax; i++ )
	{
		if( (list_start-i) < 0 )
			break;
		if( get_callrecord_table_record_items( videoRecordPage.pcall_record, list_start-i, &call_record_value ) != -1 )
		{
			if(strcmp(call_record_value.name,"---") != 0)
			{
				snprintf( recordBuffer, ONE_LIST_MAX_LEN, "[20%s]%s", call_record_value.time, call_record_value.name );
				recordLen = strlen(recordBuffer);
				unicode_len = 2*api_ascii_to_unicode(recordBuffer,recordLen,unicode_buf);
			}
			else
			{
				snprintf( time, 40, "[20%s]", call_record_value.time);
				snprintf( node, 40, "%d", call_record_value.target_node);
				API_GetOSD_StringWithID(MESG_TEXT_Node, time, strlen(time), node, strlen(node), unicode_buf, &unicode_len);	
			}
			
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_IPC;
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->dispCnt++; 
		}
		else
		{
			break;
		}
	}
}

int DisplayOnePageVideoRecord( int page )
{
	char recordBuffer[ONE_LIST_MAX_LEN];
	CALL_RECORD_DAT_T call_record_value;
	int i,x,y;	
	int list_start;
	POS pos;
	SIZE hv;
	
	if( videoRecordPage.pcall_record == NULL )
	{
		printf("---videoRecordPage.pcall_record is empty----\n");
		return -1;
	}
	
	//API_DisableOsdUpdate();

	list_start = videoRecordPage.pcall_record->record_cnt - page*VideoRecordIconMax - 1;
	
	for( i = 0; i < VideoRecordIconMax; i++ )
	{
		if( (list_start-i) < 0 )
			break;
		
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		if( get_callrecord_table_record_items( videoRecordPage.pcall_record, list_start-i, &call_record_value ) != -1 )
		{
 			DisplayOneVideoRecord(&call_record_value,x,y,DISPLAY_LIST_COLOR);
		}
		else
		{
			break;
		}
	}
	for( ; i < VideoRecordIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	}

	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X,MENULIST_PAGENUM_POS_Y, COLOR_WHITE, page+1,videoRecordPage.page_max?videoRecordPage.page_max:1);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, videoRecordPage.page_max?videoRecordPage.page_max:1);

	
	//API_EnableOsdUpdate();
}

/*************************************************************************************************
010.
*************************************************************************************************/
void MENU_024_VideoRecordList_Init(int uMenuCnt)
{
	if(OSD_GetIconInfo(ICON_888_ListView, NULL, NULL))
	{
		int unicode_len;
		char unicode_buf[200];
		
		LIST_INIT_T listInit = ListPropertyDefault();

		API_MenuIconDisplaySelectOn(ICON_020_playback);
		call_record_filter(CallRecord_FilterType_VideoRecord);
		videoRecordPage.pcall_record = pvideo_record;
		listInit.listCnt = videoRecordPage.pcall_record->record_cnt;
		API_GetOSD_StringWithID(MESG_TEXT_PlaybackList, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
		
		listInit.fun = MenuListGetVideoRecordPage;
		listInit.titleStr = unicode_buf;
		listInit.titleStrLen = unicode_len;

		InitMenuList(listInit);
	}
	else
	{
		char tempDisplay[ONE_LIST_MAX_LEN];
		
		int list_cnt_save;
		int temp;
		POS pos;
		SIZE hv;
		OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
		
		VideoRecordIconMax = GetListIconNum();
		videoRecordPage.onePageMaxNum	= VideoRecordIconMax;
		API_MenuIconDisplaySelectOn(ICON_020_playback);
		
		if(videoRecordPage.act_flag == 1)
		{
			list_cnt_save = videoRecordPage.pcall_record->record_cnt;
		}
		
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_PlaybackList,2, 0);
		
		if(call_record_filter(CallRecord_FilterType_VideoRecord) != 0)
		{
			//popDisplayLastMenu();
			DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
			return;
		}
		
		videoRecordPage.pcall_record	= pvideo_record;
		
		if(videoRecordPage.pcall_record->record_cnt == 0)
		{
			//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X,MENULIST_PAGENUM_POS_Y, COLOR_WHITE,0,0);
			//usleep(1000000);
			//popDisplayLastMenu();
			DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
			return;
		}
		if(videoRecordPage.act_flag == 1)
		{
			if(list_cnt_save < videoRecordPage.pcall_record->record_cnt)
			{
				temp = videoRecordPage.pcall_record->record_cnt - list_cnt_save;
				videoRecordPage.page_num += (videoRecordPage.list_focus + temp) / VideoRecordIconMax;
				videoRecordPage.list_focus = (videoRecordPage.list_focus + temp) % VideoRecordIconMax;	
			}
			if(list_cnt_save > videoRecordPage.pcall_record->record_cnt)
			{
				if(videoRecordPage.pcall_record->record_cnt <=(videoRecordPage.page_num*VideoRecordIconMax + videoRecordPage.list_focus))
				{
					videoRecordPage.page_num = (videoRecordPage.pcall_record->record_cnt -1)/VideoRecordIconMax;
					videoRecordPage.list_focus = (videoRecordPage.pcall_record->record_cnt -1)%VideoRecordIconMax;
				}
			}
		}
		videoRecordPage.page_max		= (videoRecordPage.pcall_record->record_cnt+VideoRecordIconMax-1)/VideoRecordIconMax;
		
		time_t t = time(NULL); 
		videoRecordPage.tblock = localtime(&t);
		
		if(videoRecordPage.tblock != NULL)
		snprintf(videoRecordPage.time_dispbuf,20, "%02d/%02d/%02d ",videoRecordPage.tblock->tm_year-100,videoRecordPage.tblock->tm_mon+1,videoRecordPage.tblock->tm_mday);
		
		DisplayOnePageVideoRecord(videoRecordPage.page_num);
		videoRecordPage.act_flag = 1;
		
	}
}

void MENU_024_VideoRecordList_Exit(void)
{
}

void MENU_024_VideoRecordList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					if(videoRecordPage.page_num*VideoRecordIconMax+videoRecordPage.list_focus < videoRecordPage.pcall_record->record_cnt)
					{
						recordPage = (CALL_RECORD_LIST_PAGE*)&videoRecordPage;
						StartInitOneMenu(MENU_026_PLAYBACK,0,1);
					}
					break;
					
				case KEY_UP:
					PublicListUpProcess(&videoRecordPage.list_focus, &videoRecordPage.page_num, VideoRecordIconMax, videoRecordPage.pcall_record->record_cnt, (DispListPage)DisplayOnePageVideoRecord);					
					break;
				case KEY_DOWN:	
					PublicListDownProcess(&videoRecordPage.list_focus, &videoRecordPage.page_num, VideoRecordIconMax, videoRecordPage.pcall_record->record_cnt, (DispListPage)DisplayOnePageVideoRecord);					
					break;
					
				default:
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					videoRecordPage.list_focus = GetCurIcon() - ICON_007_PublicList1;

					if(videoRecordPage.page_num*VideoRecordIconMax+videoRecordPage.list_focus < videoRecordPage.pcall_record->record_cnt)
					{
						recordPage = (CALL_RECORD_LIST_PAGE*)&videoRecordPage;
						StartInitOneMenu(MENU_026_PLAYBACK,0,1);
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&videoRecordPage.page_num, VideoRecordIconMax, videoRecordPage.pcall_record->record_cnt, (DispListPage)DisplayOnePageVideoRecord);
					break;
					
				case ICON_202_PageUp:
					PublicPageUpProcess(&videoRecordPage.page_num, VideoRecordIconMax, videoRecordPage.pcall_record->record_cnt, (DispListPage)DisplayOnePageVideoRecord);
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


