#include "MENU_public.h"
#include "MENU_123_RecSchedule.h"
#include "obj_ipc_rec.h"

int weekSelectIconSelect;
int weekSelectPageSelect;
int weekSelectIconMax;
#define weekIconNum 7
const char *weekDisplay[7] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
extern int inputWeek[7];
extern int weekCnt;
extern int ScheduleCH;

void DisplayOnePageweekSelect(uint8 page)
{
	//API_EnableOsdUpdate();

	uint8 i,j;
	uint16 x, y;
	int pageNum;
	int listIndex;
	POS pos;
	SIZE hv;
	char display[10];
	
	for( i = 0; i < weekSelectIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		ListSelect(i, 0);
		listIndex = i + page*weekSelectIconMax;
		if(listIndex < weekIconNum)
		{
			for(j=0; j<weekCnt; j++)
			{
				if(inputWeek[j] == (listIndex))
				{
					ListSelect(i, 1);
					break;
				}
			}
			
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, weekDisplay[listIndex], strlen(weekDisplay[listIndex]),1,STR_UTF8, 0);
		}
		
		
	}
	
	pageNum = weekIconNum/weekSelectIconMax + (weekIconNum%weekSelectIconMax ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void MENU_124_WeekSelect_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	weekSelectIconSelect = 0;
	weekSelectPageSelect = 0;
	weekSelectIconMax = GetListIconNum();
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_Week, 2, 0);
	DisplayOnePageweekSelect(weekSelectPageSelect);
}

void WeekSelectSet(int sel)
{
	int i,j;
	REC_SCHEDULE_DAT_T dat;
	for(i=0; i<weekCnt; i++)
	{
		if(inputWeek[i] == (sel))
		{
			ListSelect(sel%weekSelectIconMax, 0);
			break;
		}
	}
	if(i<weekCnt)
	{
		for(; i+1 < weekCnt; i++)
		{
			inputWeek[i] = inputWeek[i+1];
		}
		weekCnt--;
	}
	else
	{
		ListSelect(sel%weekSelectIconMax, 1);
		//inputWeek[weekCnt++] = sel+1;
		
		for(i=weekCnt;i>=0;i--)
		{
			 if(inputWeek[i]>sel) 
			 {
			  	inputWeek[i+1]=inputWeek[i];
			 }
			 else
			 {
			    inputWeek[i+1]=sel;	
				break;
			 }
	  	}
		if(i==-1)
			inputWeek[i+1]=sel;	
		weekCnt++;
	}
	if( GetOneRecSchedule(ScheduleCH - ICON_RecordCH1, &dat) == 0 )
	{
		for(i=0; i<weekCnt; i++)
		{
			dat.week[i] = inputWeek[i];
		}
		dat.weekNum = weekCnt;
		SetOneRecSchedule(&dat);
	}
}


void MENU_124_WeekSelect_Exit(void)
{
	
}

void MENU_124_WeekSelect_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int iconSel;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					iconSel = weekSelectPageSelect*weekSelectIconMax + GetCurIcon() - ICON_007_PublicList1;
					if(iconSel >= weekIconNum)
					{
						return;
					}
					WeekSelectSet(iconSel);
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&weekSelectPageSelect, weekSelectIconMax, weekIconNum, (DispListPage)DisplayOnePageweekSelect);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&weekSelectPageSelect, weekSelectIconMax, weekIconNum, (DispListPage)DisplayOnePageweekSelect);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


