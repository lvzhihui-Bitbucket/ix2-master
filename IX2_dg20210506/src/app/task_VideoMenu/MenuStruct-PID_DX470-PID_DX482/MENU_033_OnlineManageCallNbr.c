#include "MENU_033_OnlineManageCallNbr.h"
#include "obj_ProgInfoByIp.h"

#define	OnlineManageCallNbr_ICON_MAX	5
int onlineManageCallNbrIconSelect;
int onlineManageCallNbrPageSelect;
char progOnlineInput[21];
IconAndText_t* onlineManageCallNbrSet;
unsigned char onlineManageCallNbrSetNum;


DeviceInfo onlineManageCallNbrData;

const IconAndText_t onlineManageCallNbrSet1[] = 
{
	{ICON_279_RM_ADDR,					MESG_TEXT_ICON_279_RM_ADDR},
	{ICON_280_MS_Number,				MESG_TEXT_ICON_280_MS_Number},
	{ICON_281_Name,						MESG_TEXT_ICON_281_Name},
	{ICON_278_Global_Nbr,				MESG_TEXT_ICON_278_Global_Nbr},
	{ICON_282_Local_Nbr,				MESG_TEXT_ICON_282_Local_Nbr},
};


const unsigned char onlineManageCallNbrSetNum1 = sizeof(onlineManageCallNbrSet1)/sizeof(onlineManageCallNbrSet1[0]);

const IconAndText_t onlineManageCallNbrSet2[] = 
{
	{ICON_288_BD_NBR,					MESG_TEXT_ICON_288_BD_NBR},
	{ICON_298_DS_NBR,					MESG_TEXT_ICON_280_DS_Number},
	{ICON_281_Name,						MESG_TEXT_ICON_281_Name},
	{ICON_278_Global_Nbr,				MESG_TEXT_ICON_278_Global_Nbr},
	{ICON_282_Local_Nbr,				MESG_TEXT_ICON_282_Local_Nbr},
};


const unsigned char onlineManageCallNbrSetNum2 = sizeof(onlineManageCallNbrSet2)/sizeof(onlineManageCallNbrSet2[0]);

static void DisplayProgOnlineIMPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	int ms;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < OnlineManageCallNbr_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnlineManageCallNbr_ICON_MAX+i < onlineManageCallNbrSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineManageCallNbrSet[i+page*OnlineManageCallNbr_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(onlineManageCallNbrSet[i+page*OnlineManageCallNbr_ICON_MAX].iCon)
			{
				case ICON_279_RM_ADDR:
					memcpy(display, onlineManageCallNbrData.BD_RM_MS, 8);
					display[8] = 0;
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_280_MS_Number:
					ms = atoi(onlineManageCallNbrData.BD_RM_MS+8);
					if(ms == 1)
					{
						sprintf(display, "(%d)%s", ms, "Master");
					}
					else
					{
						sprintf(display, "(%d)%s", ms, "Slave");
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_281_Name:
					strcpy(display, onlineManageCallNbrData.name1);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_278_Global_Nbr:
					strcpy(display, onlineManageCallNbrData.Global);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;

				case ICON_282_Local_Nbr:
					strcpy(display, onlineManageCallNbrData.Local);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = onlineManageCallNbrSetNum/OnlineManageCallNbr_ICON_MAX + (onlineManageCallNbrSetNum%OnlineManageCallNbr_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

static void DisplayProgOnlineDSPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	int dsNumber;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < OnlineManageCallNbr_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnlineManageCallNbr_ICON_MAX+i < onlineManageCallNbrSetNum2)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineManageCallNbrSet2[i+page*OnlineManageCallNbr_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 7 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(onlineManageCallNbrSet2[i+page*OnlineManageCallNbr_ICON_MAX].iCon)
			{
				case ICON_288_BD_NBR:
					if(!memcmp(onlineManageCallNbrData.BD_RM_MS, "0099", 4))
					{
						memcpy(display, "-", 1);
						display[1] = 0;
					}
					else
					{
						memcpy(display, onlineManageCallNbrData.BD_RM_MS, 4);
						display[4] = 0;
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_298_DS_NBR:
					dsNumber = atoi(onlineManageCallNbrData.BD_RM_MS+8);
					dsNumber = dsNumber > 50 ? dsNumber-50 : dsNumber;
					sprintf(display, "%d", dsNumber);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_281_Name:
					strcpy(display, onlineManageCallNbrData.name1);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_278_Global_Nbr:
					strcpy(display, onlineManageCallNbrData.Global);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;

				case ICON_282_Local_Nbr:
					strcpy(display, onlineManageCallNbrData.Local);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = onlineManageCallNbrSetNum2/OnlineManageCallNbr_ICON_MAX + (onlineManageCallNbrSetNum2%OnlineManageCallNbr_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

static void DisplayProgOnlinePageIcon(int page)
{
	if(onlineManageCallNbrData.deviceType == TYPE_IM)
	{
		onlineManageCallNbrSet = onlineManageCallNbrSet1;
		onlineManageCallNbrSetNum = onlineManageCallNbrSetNum1;
		DisplayProgOnlineIMPageIcon(page);
	}
	else if(onlineManageCallNbrData.deviceType == TYPE_DS)
	{
		onlineManageCallNbrSet = onlineManageCallNbrSet2;
		onlineManageCallNbrSetNum = onlineManageCallNbrSetNum2;
		DisplayProgOnlineDSPageIcon(page);
	}
	else if(onlineManageCallNbrData.deviceType == TYPE_OS)
	{
		onlineManageCallNbrSet = onlineManageCallNbrSet2;
		onlineManageCallNbrSetNum = onlineManageCallNbrSetNum2;
		DisplayProgOnlineDSPageIcon(page);
	}
	else
	{
		onlineManageCallNbrSet = onlineManageCallNbrSet1;
		onlineManageCallNbrSetNum = onlineManageCallNbrSetNum1;
		DisplayProgOnlineIMPageIcon(page);
	}
}

 
int ProgOnlineIMRoomNumber(const char* number)
{
	int numLen;
	char tempBdRm[11];
	int num;
	DeviceInfo tempData = onlineManageCallNbrData;
	
	numLen = strlen(number);

	if(numLen == 0 || numLen > 8)
	{
		return 0;
	}
	else if(numLen <= 3)
	{
		num = atoi(number);
		if(num <= 128 && num >= 1)
		{
			sprintf(tempBdRm, "0099%04d", num);
			memcpy(tempData.BD_RM_MS, tempBdRm, 8);
			ProgOnlineByIp(&tempData);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if(numLen == 8)
	{
		for( ; numLen > 0; numLen--)
		{
			if(number[numLen-1] < '0' || number[numLen-1] > '9')
			{
				return 0;
			}
		}
		memcpy(tempData.BD_RM_MS, number, 8);
		ProgOnlineByIp(&tempData);
		return 1;
	}

}

int ProgOnlineDSBDNumber(const char* number)
{
	int numLen;
	char tempBdRm[11];
	int num;
	DeviceInfo tempData = onlineManageCallNbrData;
	
	if(strlen(number) > 4)
	{
		return 0;
	}

	num = atoi(number);
	if(!strcmp(number, "-") || num < 0)
	{
		memcpy(tempData.BD_RM_MS, "0099", 4);
		ProgOnlineByIp(&tempData);
		return 1;
	}
	else
	{
		sprintf(tempBdRm, "%04d", num);
		memcpy(tempData.BD_RM_MS, tempBdRm, 4);
		ProgOnlineByIp(&tempData);
		return 1;
	}
}


int ProgOnlineMasterSlaveNumber(const char* number)
{
	int num = atoi(number);

	DeviceInfo tempData = onlineManageCallNbrData;

	if(num <= 0)
		return 0;
	
	sprintf(tempData.BD_RM_MS+8, "%02d", num);
	
	ProgOnlineByIp(&tempData);
	return 1;
}

int ProgOnlineName(const char* name)
{
	DeviceInfo tempData = onlineManageCallNbrData;
	
	int i, j;

	for(i = 0, j = 0; name[i] != 0; i++)
	{
		if(name[i] != ' ')
		{
			j++;
		}
	}

	strcpy(tempData.name1, (j == 0) ? "-" : name);
	
	ProgOnlineByIp(&tempData);
	return 1;
}

int ProgOnlineGlobalNumber(const char* number)
{
	DeviceInfo tempData = onlineManageCallNbrData;

	int i, j;

	for(i = 0, j = 0; number[i] != 0; i++)
	{
		if(number[i] != ' ')
		{
			j++;
		}
	}
	
	strcpy(tempData.Global, (j == 0) ? "-" : number);
	
	ProgOnlineByIp(&tempData);
	return 1;
}

int ProgOnlineLocalNumber(const char* number)
{
	DeviceInfo tempData = onlineManageCallNbrData;

	int i, j;

	for(i = 0, j = 0; number[i] != 0; i++)
	{
		if(number[i] != ' ')
		{
			j++;
		}
	}
	
	strcpy(tempData.Local, (j == 0) ? "-" : number);
	
	ProgOnlineByIp(&tempData);
	
	return 1;
}


void MENU_033_OnlineManageCallNbr_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_030_CALL_NUMBER);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	//API_OsdUnicodeStringDisplayWithIcon(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, ICON_030_CALL_NUMBER, 1, DISPLAY_TITLE_WIDTH);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	onlineManageCallNbrIconSelect = 0;
	onlineManageCallNbrPageSelect = 0;
	
	
	if(API_GetInfoByIp(GetSearchOnlineIp(), 2, &onlineManageCallNbrData) == 0)
	{	
		DisplayProgOnlinePageIcon(onlineManageCallNbrPageSelect);
	}
	else
	{
		BEEP_ERROR();
		popDisplayLastMenu();
	}
}

void MENU_033_OnlineManageCallNbr_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_030_CALL_NUMBER);
}

void MENU_033_OnlineManageCallNbr_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					onlineManageCallNbrIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(onlineManageCallNbrPageSelect*OnlineManageCallNbr_ICON_MAX+onlineManageCallNbrIconSelect >= onlineManageCallNbrSetNum)
					{
						return;
					}

					switch(onlineManageCallNbrSet[onlineManageCallNbrPageSelect*OnlineManageCallNbr_ICON_MAX+onlineManageCallNbrIconSelect].iCon)
					{
 						case ICON_279_RM_ADDR:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_279_RM_ADDR, progOnlineInput, 8, COLOR_WHITE, onlineManageCallNbrData.BD_RM_MS, 1, ProgOnlineIMRoomNumber);
							break;
						case ICON_280_MS_Number:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_280_MS_Number, progOnlineInput, 2, COLOR_WHITE, onlineManageCallNbrData.BD_RM_MS+8, 1, ProgOnlineMasterSlaveNumber);
							break;
						case ICON_288_BD_NBR:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_288_BD_NBR, progOnlineInput, 4, COLOR_WHITE, memcmp(onlineManageCallNbrData.BD_RM_MS, "0099", 4) ? onlineManageCallNbrData.BD_RM_MS : "-", 1, ProgOnlineDSBDNumber);
							break;
						case ICON_298_DS_NBR:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_280_DS_Number, progOnlineInput, 2, COLOR_WHITE, onlineManageCallNbrData.BD_RM_MS+8, 1, ProgOnlineMasterSlaveNumber);
							break;
						case ICON_281_Name:
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, progOnlineInput, 20, COLOR_WHITE, onlineManageCallNbrData.name1, 1, ProgOnlineName);
							break;
							
						case ICON_278_Global_Nbr:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_278_Global_Nbr, progOnlineInput, 10, COLOR_WHITE, onlineManageCallNbrData.Global, 1, ProgOnlineGlobalNumber);
							break;
							
						case ICON_282_Local_Nbr:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_282_Local_Nbr, progOnlineInput, 6, COLOR_WHITE, onlineManageCallNbrData.Local, 1, ProgOnlineLocalNumber);
							break;
					}
					break;
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




