#include "MENU_public.h"
#include "MENU_143_RM_List.h"
#include "MENU_144_RM_Display.h"

#define	RM_LIST_ICON_MAX			5

//#define	RM_LIST_MAX				5
#define	RM_LIST_MAX				4


int rmListIconSelect;
int rmListPageSelect;
int rmListIndex;

//const char* RM_LIST[RM_LIST_MAX] = {"DS-1", "DS-2", "DS-3", "DS-4", "IPG"};
const char* RM_LIST[RM_LIST_MAX] = {"DS-1", "DS-2", "DS-3", "DS-4"};


void DisplayOnePageRM_List(uint8 page)
{
	int i, x, y, maxPage, listStart, maxList;
	char display[100];
	POS pos;
	SIZE hv;
	maxList = RM_LIST_MAX;

	listStart = page*RM_LIST_ICON_MAX;
	
	for( i = 0; i < RM_LIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(listStart+i < maxList)
		{
			if(listStart+i<4)
				Get_DxMonRes_Name_ByAddr(DS1_ADDRESS+listStart+i,display);
			else
				snprintf(display, 100, "%s", RM_LIST[i]);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - x);
		}
	}

	maxPage = maxList/RM_LIST_ICON_MAX + (maxList%RM_LIST_ICON_MAX ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
}

void MENU_143_RM_List_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_008_SET_INSTALLER)
	{
		rmListIconSelect = 0;
		rmListPageSelect = 0;
	}
	else
	{
		//AI_AppointFastLinkWithEventAck(0);
	}
	
	SetRmState(STATE_RM_SEL);
	//API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_RM_List,1, 0);

	DisplayOnePageRM_List(rmListPageSelect);
	
}


void MENU_143_RM_List_Exit(void)
{
	
}

void MENU_143_RM_List_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;	
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					rmListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					rmListIndex = rmListPageSelect*RM_LIST_ICON_MAX + rmListIconSelect;
					if(rmListIndex >= RM_LIST_MAX)
					{
						return;
					}
					
					if(rmListIndex<4)
					{
						SetRmLinkAddr(DS1_ADDRESS + rmListIndex);
					}
					else if(rmListIndex == 4)
					{
						SetRmLinkAddr(IPG_ADDRESS);
					}
					
					//if(AI_AppointFastLinkWithEventAck(Link_addr))
					{
						rmInput[0] = 0;
						SetRmState(STATE_PASSWORD_INPUT);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_RmPassword, rmInput, 4, COLOR_WHITE, NULL, 0, Input_RM_Password_Process);
					}		
					//else
					{
					//	BEEP_ERROR();
					}
					
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&rmListPageSelect, RM_LIST_ICON_MAX, Get_IPC_MonRes_Num(), (DispListPage)DisplayOnePageRM_List);
					break;
				case ICON_202_PageUp:
					//PublicPageUpProcess(&rmListPageSelect, RM_LIST_ICON_MAX, Get_IPC_MonRes_Num(), (DispListPage)DisplayOnePageRM_List);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


