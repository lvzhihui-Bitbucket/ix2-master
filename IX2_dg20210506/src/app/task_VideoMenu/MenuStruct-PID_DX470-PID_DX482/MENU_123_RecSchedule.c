#include "MENU_public.h"
#include "MENU_123_RecSchedule.h"
#include "obj_ipc_rec.h"


int RecScheduleIconMax;
int RecScheduleIconSelect;
int RecSchedulePageSelect;
int ScheduleCH;
char inputTimeStart[5] = {"0000"};
char inputTimeEnd[5] = {"2400"};
int inputWeek[7]={0,1,2,3,4,5,6};
int weekCnt=7;


const IconAndText_t RecScheduleIconTable[] = 
{
	//{ICON_DVR_IPC_CH, 			MESG_TEXT_IPC_Name},	 
	{ICON_DVR_TIME_S, 			MESG_TEXT_TimeStart},	 
	{ICON_DVR_TIME_E, 			MESG_TEXT_TimeEnd},
	{ICON_DVR_DATE,				MESG_TEXT_Week},
	{ICON_SCHEDULE_SET,			MESG_TEXT_Select},
	{ICON_SCHEDULE_Cancel,		MESG_TEXT_Delete},
};
const int RecScheduleIconTableNum = sizeof(RecScheduleIconTable)/sizeof(RecScheduleIconTable[0]);


static void DisplayRecSchedulePageIcon(uint8 page)
{
	uint8 i,j;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;
	unsigned char hour[3] = {0};
	unsigned char min[3] = {0};
	REC_SCHEDULE_DAT_T dat;
	char temp[10];

	for(i = 0; i < 4; i++)
	{
		OSD_GetIconInfo(ICON_RecordCH1+i, &pos, &hv);
		x = pos.x;
		y = pos.y;
		if( GetOneRecSchedule(i, &dat) == 0 )
		{
			API_SpriteDisplay_XY(x,y,SPRITE_RecTiming);	
			if(i == ScheduleCH-ICON_RecordCH1)
			{
				strcpy(inputTimeStart, dat.timeS);
				strcpy(inputTimeEnd, dat.timeE);
				weekCnt = dat.weekNum;
				for(j=0; j<weekCnt; j++)
				{
					inputWeek[j] = dat.week[j];
				}
			}
		}
		else
		{
			API_SpriteClose(x,y,SPRITE_RecTiming);	
		}
	}
	
	for(i = 0; i < RecScheduleIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = pos.x+(hv.h - pos.x)/2;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*RecScheduleIconMax+i < RecScheduleIconTableNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RecScheduleIconTable[i+page*RecScheduleIconMax].iConText, 1, val_x-x);
			x += (hv.h - pos.x)/2;
			switch(RecScheduleIconTable[i+page*RecScheduleIconMax].iCon)
			{
				case ICON_DVR_TIME_S:
					memcpy(hour, inputTimeStart, 2);
					memcpy(min, inputTimeStart+2, 2);
					sprintf(display,"%s:%s",hour, min);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8,0);
					break;
				case ICON_DVR_TIME_E:
					memcpy(hour, inputTimeEnd, 2);
					memcpy(min, inputTimeEnd+2, 2);
					sprintf(display,"%s:%s",hour, min);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8,0);
					break;
				case ICON_DVR_DATE:
					display[0] = 0;
					strcat(display, "[ ");
					for(j = 0; j < weekCnt; j++)
					{
						if(j == weekCnt-1)
						{
							snprintf(temp,10, "%d", inputWeek[j]);
						}
						else
						{
							snprintf(temp,10, "%d, ", inputWeek[j]);
						}
						strcat(display, temp);
					}
					strcat(display, " ]");
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8,0);
					break;

			}
			
		}
	}
	pageNum = RecScheduleIconTableNum/RecScheduleIconMax + (RecScheduleIconTableNum%RecScheduleIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void MENU_123_RecSchedule_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	RecScheduleIconMax = GetListIconNum();
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_DVR_SCHEDULE, 1, 0);
	RecScheduleIconSelect = 0;
	RecSchedulePageSelect = 0;
	if( GetLastNMenu() == MENU_116_MultiRec)
	{
		ScheduleCH = ICON_RecordCH1;
	}
	API_MenuIconDisplaySelectOn(ScheduleCH);
	DisplayRecSchedulePageIcon(RecSchedulePageSelect);

}

int SaveStartTimeSet(void)
{
	unsigned char hour[3] = {0};
	unsigned char min[3] = {0};
	REC_SCHEDULE_DAT_T dat;
	memcpy(hour, inputTimeStart, 2);
	memcpy(min, inputTimeStart+2, 2);
	if(strlen(inputTimeStart) != 4)
		return 0;
	if(strcmp(inputTimeEnd, inputTimeStart) <= 0)
		return 0;
	if(((atol(hour) < 24) && (atol(min) < 60)) || ((atol(hour) == 24) && (atol(min) == 0)))
	{
		if( GetOneRecSchedule(ScheduleCH - ICON_RecordCH1, &dat) == 0 )
		{
			//dat.id = ScheduleCH - ICON_RecordCH1;
			strcpy(dat.timeS, inputTimeStart);
			//strcpy(dat.timeE, inputTimeEnd);
			SetOneRecSchedule(&dat);
		}
		return 1;
	}
	return 0;
}
int SaveEndTimeSet(void)
{
	unsigned char hour[3] = {0};
	unsigned char min[3] = {0};
	REC_SCHEDULE_DAT_T dat;
	memcpy(hour, inputTimeEnd, 2);
	memcpy(min, inputTimeEnd+2, 2);
	if(strlen(inputTimeEnd) != 4)
		return 0;
	if(strcmp(inputTimeEnd, inputTimeStart) <= 0)
		return 0;
	if(((atol(hour) < 24) && (atol(min) < 60)) || ((atol(hour) == 24) && (atol(min) == 0)))
	{
		if( GetOneRecSchedule(ScheduleCH - ICON_RecordCH1, &dat) == 0 )
		{
			//dat.id = ScheduleCH - ICON_RecordCH1;
			//strcpy(dat.timeS, inputTimeStart);
			strcpy(dat.timeE, inputTimeEnd);
			SetOneRecSchedule(&dat);
		}
		return 1;
	}
	return 0;
}

void MENU_123_RecSchedule_Exit(void)
{
}

void MENU_123_RecSchedule_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i;
	int iconSel;
	REC_SCHEDULE_DAT_T dat;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_RecordCH1:
				case ICON_RecordCH2:
				case ICON_RecordCH3:
				case ICON_RecordCH4:
					API_MenuIconDisplaySelectOff(ScheduleCH);
					ScheduleCH = GetCurIcon();
					API_MenuIconDisplaySelectOn(ScheduleCH);
					DisplayRecSchedulePageIcon(RecSchedulePageSelect);
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					iconSel = RecSchedulePageSelect*RecScheduleIconMax + GetCurIcon() - ICON_007_PublicList1;
					if(iconSel >= RecScheduleIconTableNum)
					{
						return;
					}
					switch(RecScheduleIconTable[iconSel].iCon)
					{
						case ICON_DVR_TIME_S:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_TimeStart, inputTimeStart, 4, COLOR_WHITE, inputTimeStart, 1, SaveStartTimeSet);
							break;
						case ICON_DVR_TIME_E:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_TimeEnd, inputTimeEnd, 4, COLOR_WHITE, inputTimeEnd, 1, SaveEndTimeSet);
							break;
						case ICON_DVR_DATE:
							StartInitOneMenu(MENU_124_WeekSelect,0,1);
							break;
						case ICON_SCHEDULE_SET:
							if(strcmp(inputTimeEnd, inputTimeStart) <= 0)
								return ;
							dat.ch = ScheduleCH - ICON_RecordCH1;
							strcpy(dat.timeS, inputTimeStart);
							strcpy(dat.timeE, inputTimeEnd);
							for(i=0; i<weekCnt; i++)
							{
								dat.week[i] = inputWeek[i];
							}
							dat.weekNum = weekCnt;
							SetOneRecSchedule(&dat);
							//OS_RetriggerTimer(&timer_dvr_schedule);
							API_SpriteDisplay_XY(GetIconXY(ScheduleCH).x,GetIconXY(ScheduleCH).y,SPRITE_RecTiming);	
							break;
						case ICON_SCHEDULE_Cancel:
							DeleteOneRecSchedule(ScheduleCH - ICON_RecordCH1);
							API_SpriteClose(GetIconXY(ScheduleCH).x,GetIconXY(ScheduleCH).y,SPRITE_RecTiming);
							break;
					}
					break;
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}

