#include "MENU_public.h"

#define	OneRemotePara_ICON_MAX	5
int OneRemoteParaIconSelect;
int OneRemoteParaPageSelect;
int OneRemoteParaIndex;

ParaProperty_t oneRemoteParaproperty;
ParaMenu_T OneRemotePararecord;
extern ComboBoxSelect_T OneParaComboBox;

const IconAndText_t OneRemoteParaTable[] = 
{
	{ICON_ParaId,					MESG_TEXT_ICON_ParaId},    
	{ICON_ParaName,					MESG_TEXT_ICON_ParaName},
	{ICON_ParaDesc, 				MESG_TEXT_ICON_ParaDesc},
	{0, 							0},
	{ICON_ParaValue, 				MESG_TEXT_ICON_ParaValue},
};

const int OneRemoteParaIconNum = sizeof(OneRemoteParaTable)/sizeof(OneRemoteParaTable[0]);

int SetRemoteParaValue(const char* pData)
{
	if(API_Event_IoServer_Inner_judgeRange(OneRemotePararecord.paraId, pData) != 0)
	{
		return 0;
	}
	
	sprintf(OneRemotePararecord.value, "<ID=%s,Value=%s>", OneRemotePararecord.paraId, pData);
	API_io_server_UDP_to_write_remote(GetSearchOnlineIp(), 0xFFFFFFFF, OneRemotePararecord.value);
	return 1;	
}

int ComboxSetRemoteParaValue(int value)
{
	
	if(API_Event_IoServer_Inner_judgeRange(OneRemotePararecord.paraId, OneParaComboBox.data[value].value) != 0)
	{
		return 0;
	}
	
	sprintf(OneRemotePararecord.value, "<ID=%s,Value=%s>", OneRemotePararecord.paraId, OneParaComboBox.data[value].value);
	API_io_server_UDP_to_write_remote(GetSearchOnlineIp(), 0xFFFFFFFF, OneRemotePararecord.value);
	return 1;	
}

static void DisplayOneRemoteParaPageIcon(int page)
{
	int i, index;
	uint16 x, y, val_x;
	int pageNum;
	ParaMenu_T record;
	char display[100];
	char *disp, *pos1, *pos2;
	POS pos;
	SIZE hv;
	
	for(i = 0; i < OneRemotePara_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 7 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		index = page*OneRemotePara_ICON_MAX+i;
		
		if(OneRemoteParaTable[index].iCon != 0)
		{
			//API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		
		if(index < OneRemoteParaIconNum)
		{
			if(OneRemoteParaTable[index].iConText != 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, OneRemoteParaTable[index].iConText, 1, val_x-x);
			}
			
			if(get_pane_type() == 7 )
			{
				x += 100;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(OneRemoteParaTable[index].iCon)
			{
				case ICON_ParaId:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OneRemotePararecord.paraId, strlen(OneRemotePararecord.paraId), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaName:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OneRemotePararecord.name, strlen(OneRemotePararecord.name), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaDesc:
					pos1 = strchr(OneRemotePararecord.describe, '\'');
					if(pos1 != NULL)
					{
						*pos1 = 0;
						//API_OsdStringClearExt(x, y+DISPLAY_LIST_SPACE, bkgd_w-x, 40);
						//API_OsdStringDisplayExt(x, y+DISPLAY_LIST_SPACE, DISPLAY_STATE_COLOR, pos1+1, strlen(pos1+1), 1, STR_UTF8, MENU_SCHEDULE_POS_X - x);
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OneRemotePararecord.describe, strlen(OneRemotePararecord.describe), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaValue:
					sprintf(display, "<ID=%s>", OneRemotePararecord.paraId);
					API_io_server_UDP_to_read_remote(GetSearchOnlineIp(), 0xFFFFFFFF, display);

					pos1 = strstr(display, "Value=");
					if(pos1 != NULL)
					{
						pos2 = strchr(pos1, '>');
						if(pos2 != NULL)
						{
							int len;
							len = ((int)(pos2-pos1))-strlen("Value=");
							memcpy(OneRemotePararecord.value, pos1+strlen("Value="), len);
							OneRemotePararecord.value[len] = 0;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
					if(oneRemoteParaproperty.editType)
					{
						int i;
						for(i=0, OneParaComboBox.value = 0; i<OneParaComboBox.num; i++)
						{
							if(!strcmp(OneParaComboBox.data[i].value, OneRemotePararecord.value))
							{
								OneParaComboBox.value = i;
								break;
							}
						}
						disp = OneParaComboBox.data[OneParaComboBox.value].comboBox;
					}
					else
					{
						disp = OneRemotePararecord.value;
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp),1, STR_UTF8, hv.h - x);
					break;
			}
		}
	}
	//pageNum = OneRemoteParaNum/OneRemotePara_ICON_MAX + (OneRemoteParaNum%OneRemotePara_ICON_MAX ? 1 : 0);

	//DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}
 


void MENU_075_OneRemotePara_Init(int uMenuCnt)
{
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	API_Event_IoServer_InnerRead_Property(OneRemotePararecord.paraId, &oneRemoteParaproperty);
	OneRemoteParaPageSelect = 0;
	DisplayOneRemoteParaPageIcon(OneRemoteParaPageSelect);
}

void MENU_075_OneRemotePara_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_049_Parameter);
}

void MENU_075_OneRemotePara_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					//PublicPageDownProcess(&OneRemoteParaPageSelect, OneRemotePara_ICON_MAX, OneRemoteParaNum, (DispListPage)DisplayOneRemoteParaPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&OneRemoteParaPageSelect, OneRemotePara_ICON_MAX, OneRemoteParaNum, (DispListPage)DisplayOneRemoteParaPageIcon);
					break;			
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				case ICON_011_PublicList5:

					OneRemoteParaIconSelect = GetCurIcon() - ICON_007_PublicList1;
					OneRemoteParaIndex = OneRemoteParaPageSelect*OneRemotePara_ICON_MAX+OneRemoteParaIconSelect;
					
					//if(OneRemoteParaIndex < OneRemoteParaNum)
					{
						if(oneRemoteParaproperty.readWrite & 0x02)
						{
							if(!oneRemoteParaproperty.editType)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ParaValue, OneRemotePararecord.value, 30, COLOR_WHITE, OneRemotePararecord.value, 1, SetRemoteParaValue);
							}
							else
							{
								OneParaComboBox.editType = oneRemoteParaproperty.editType;
								EnterParaPublicMenu(0, OneParaComboBox.value, ComboxSetRemoteParaValue);
							}
						}
						else
						{
							BEEP_ERROR();
						}
						//StartInitOneMenu(MENU_072_OneRemotePara,0,1);
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


