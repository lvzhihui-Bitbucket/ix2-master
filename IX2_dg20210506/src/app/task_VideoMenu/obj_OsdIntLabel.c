/**
  ******************************************************************************
  * @file    Obj_OsdExtLable.c
  * @author  Lv
  * @version V1.0.0
  * @date    2013.10.26
  * @brief   This file contains the function of Lable Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2013 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include "obj_OsdExtLabel.h"
#include "obj_OsdIntLabel.h"
#include "obj_OsdLabel.h"


const unsigned char IntlabelDS1[]          = { "DS-1" };
const unsigned char IntlabelDS2[]          = { "DS-2" };
const unsigned char IntlabelDS3[]          = { "DS-3" };
const unsigned char IntlabelDS4[]          = { "DS-4" };
const unsigned char IntlabelCM1[]          = { "CM-1" };
const unsigned char IntlabelCM2[]          = { "CM-2" };
const unsigned char IntlabelCM3[]          = { "CM-3" };
const unsigned char IntlabelCM4[]          = { "CM-4" };
const unsigned char IntlabelFontBlank1[]   = { "  " };
const unsigned char IntlabelFontBlank2[]   = { "    " };

const unsigned char*  const IntlabelPointerTab[] =
{
    IntlabelDS1,
    IntlabelDS2,
    IntlabelDS3,
    IntlabelDS4,
    IntlabelCM1,
    IntlabelCM2,
    IntlabelCM3,
    IntlabelCM4,
};

/*******************************************************************************************
 * @fn:		Show_Int_Label
 *
 * @brief:	显示内部lable
 *
 * @param:  row - 起始行，col - 起始列，color - 显示颜色，intlabelnum - 内部lable索引
 *
 * @return: none
 *******************************************************************************************/
void Show_Int_Label(int row, int col, int color, IntLableType intlabelnum)
{
	//T47m_DisplayRomString( col, row, color, (unsigned char*)IntlabelPointerTab[intlabelnum] );
	display_string( col, row, color, COLOR_KEY, (unsigned char*)IntlabelPointerTab[intlabelnum] );	
}

/*******************************************************************************************
 * @fn:		Hide_Int_Lable
 *
 * @brief:	清除内部lable
 *
 * @param:  row - 起始行，col - 起始列，intlabelnum - 内部lable索引
 *
 * @return: none
 *******************************************************************************************/
void Hide_Int_Label(int row, int col, IntLableType intlabelnum)
{
	if( intlabelnum > INT_LABEL_DS1	)
    {
        //T47m_DisplayRomString( col, row, 0, (unsigned char*)IntlabelFontBlank2 );
		display_string( col, row, 0, COLOR_KEY, (unsigned char*)IntlabelFontBlank2 );	
    }
    else
    {
        //T47m_DisplayRomString( col, row, 0, (unsigned char*)IntlabelFontBlank1 );
		display_string( col, row, 0, COLOR_KEY, (unsigned char*)IntlabelFontBlank1 );	
    }
}
