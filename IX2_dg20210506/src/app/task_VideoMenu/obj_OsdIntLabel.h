/**
  ******************************************************************************
  * @file    Obj_OsdIntLable.h
  * @author  lv
  * @version V1.0.0
  * @date    2013.10.30
  * @brief   This file contains the headers of the Obj_OsdIntLable.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2013 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef _OBJ_OSDINTLABLE_H
#define _OBJ_OSDINTLABLE_H

typedef enum
{
    INT_LABEL_DS1,
    INT_LABEL_DS2,
    INT_LABEL_DS3,
    INT_LABEL_DS4,
    INT_LABEL_CM1,
    INT_LABEL_CM2,
    INT_LABEL_CM3,
    INT_LABEL_CM4,		
} IntLableType;

void Show_Int_Label(int row, int col, int color, IntLableType intlabelnum);
void Hide_Int_Label(int row, int col, IntLableType intlabelnum);


#endif
