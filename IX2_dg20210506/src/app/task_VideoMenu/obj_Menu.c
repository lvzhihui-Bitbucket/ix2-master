
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "task_Hal.h"
#include "obj_menu_data.h"


/*******************************************************************************************
 * @fn��	Menu_Display
 *
 * @brief:	��ʾOSD�˵�,  �˵����ѹ�뵽���?
 *
 * @param:  menu_cnt - �˵����?
 *
 * @return:  0/ok, 1/err
 *******************************************************************************************/
unsigned char Menu_Display(int menu_cnt)
{	
	//BackLightPowerCtrl(1);
	//API_POWER_TFT_ON();
	
	printf("==menu[%03d]==Menu_Display2 start\n\r",menu_cnt); 
	
	long long cur_time;

	cur_time = time_since_last_call(-1);
	
	if(menu_cnt == 27 || menu_cnt == 28 || menu_cnt == 56 || menu_cnt == 113 || menu_cnt == 26||menu_cnt == 142||menu_cnt == 141||menu_cnt == 156)
	{
		if(menu_cnt != 27 ||GetCurMenuCnt()!=28)
			clearscreen(1);
	}
	else
		clearscreen(0);
	printf("==menu[%03d]==clear screen:[%d]\n\r",menu_cnt,time_since_last_call(cur_time));		

	cur_time = time_since_last_call(-1);
	int buf_disp = 0;
	buf_disp = UpdateOsdLayerBuf_extbuf2vram(menu_cnt);
	if(buf_disp ) 
		printf("==menu[%03d]==load cache over:total[%d]\n\r",menu_cnt,time_since_last_call(cur_time)); 
	else
		printf("==menu[%03d]==have no cache:[%d]\n\r",menu_cnt,time_since_last_call(cur_time));

	cur_time = time_since_last_call(-1);	
	if( DisplayOneMenu(menu_cnt,0) )
	{
		printf("==menu[%03d]==DisplayOneMenu:[%d]\n\r",menu_cnt,time_since_last_call(cur_time));
		if( !buf_disp )
		{
			UpdateOsdLayerBuf(0);
			return 0;
		}
		else
			return 2;	// cache delay to refresh display
	}
	else
	{
		printf("have no Menu Display: %d\n\r",menu_cnt);
		return 1;
	}
}

// lzh_20161230_s
/*******************************************************************************************
 * @fn��		Menu_Display2
 *
 * @brief:	׷���Ӳ˵���icon�����˵�������
 *
 * @param:  	menu_cnt - �˵����?
 *
 * @return:  	0/ok, 1/err
 *******************************************************************************************/
unsigned char Menu_Display2(int menu_cnt, int sub_menu_cnt, int clr_scr )
{
	printf("==menu[%03d]==Menu_Display2 start\n\r",menu_cnt); 

	long long cur_time;

	cur_time = time_since_last_call(-1);	
	clearscreen(0);
	printf("==menu[%03d]==clear screen:[%d]\n\r",menu_cnt,time_since_last_call(cur_time));	
	
	cur_time = time_since_last_call(-1);
	int buf_disp = 0;
	#if defined(PID_IX850)
	#else
	buf_disp = UpdateOsdLayerBuf_extbuf2vram(menu_cnt);
	if(buf_disp ) 
		printf("==menu[%03d]==load cache over:total[%d]\n\r",menu_cnt,time_since_last_call(cur_time)); 
	else
		printf("==menu[%03d]==have no cache:[%d]\n\r",menu_cnt,time_since_last_call(cur_time));
	#endif
	cur_time = time_since_last_call(-1);	
	if( DisplayOneMenu(menu_cnt,sub_menu_cnt) )
	{
		printf("==menu[%03d]==DisplayOneMenu:[%d]\n\r",menu_cnt,time_since_last_call(cur_time)); 
		if( !buf_disp )
		{
			UpdateOsdLayerBuf(0);
			return 0;
		}
		else
			return 2;	// cache delay to refresh display
	}
	else
	{
		printf("have no Menu Display2: %d\n\r",menu_cnt);
		return 1;
	}
}
// lzh_20161230_e

/*******************************************************************************************
 * @fn��	Menu_Close
 *
 * @brief:	�رղ˵�,�ص�
 *
 * @param:  menu_cnt - �˵����?
 *
 * @return: none
 *******************************************************************************************/
void Menu_Close(void)
{
		// lzh_20181016_s
#if 1
		DisableOneMenu();
		clearscreen(1);
		//LCD_PWM_RESET();
#else
		//Menu_Display(MENU_028_MONITOR2);
		Menu_Display(MENU_001_MAIN);
		//MENU_001_MAIN_Init(0);
#endif	
		// lzh_20181016_e
	//dprintf("Menu Close and power down...\n");
}



int Menu_Sprite_Display( int x, int y, int Sprite_cnt )
{
	DisplayOneSprite(Sprite_cnt, x, y );
	return 0;
}

//SPRITE��ʾ�ر�
int Menu_Sprite_Close( int x, int y, int Sprite_cnt, int mode )
{
	//dprintf("Menu_Sprite_Close mode=%d...Sprite=%d\n", mode, Sprite_cnt);
	ClearOneSprite(Sprite_cnt, x, y, mode );
	return 0;
}

int Menu_BmpDisplay(int layer,char *bmp_file,int x,int y,int sx,int sy)
{
	DisplayOneBmp(layer,bmp_file,x,y,sx,sy);
	return 0;
}

int Menu_Sprite_Display_Multi(MenuSpriteMulti_T sprite)
{
	return DisplayMultiSprite(sprite);
}

int Menu_ListView_Close(   int         id, int x, int y, int sx, int sy,  int layer)
{
	ClearListViewAlpha(id,x,y,sx,sy,layer);
	return 0;
}


int Menu_Sprite_Display_Alpha( int x, int y, int Sprite_cnt )
{
	DisplayOneSpriteAlpha(Sprite_cnt, x, y );
	return 0;
}

//SPRITE��ʾ�ر�
int Menu_Sprite_Close_Alpha( int x, int y, int Sprite_cnt )
{
	ClearOneSpriteAlpha(Sprite_cnt, x, y );
	return 0;
}


