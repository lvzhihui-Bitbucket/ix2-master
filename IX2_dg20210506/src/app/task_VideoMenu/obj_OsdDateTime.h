/**
  ******************************************************************************
  * @file    ObjDateTime.h
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the headers of the ObjDateTime.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef _OBJDATETIME_H
#define _OBJDATETIME_H


//#include "stm32l1xx_rtc.h"

extern OS_TIMER timerDateTime;

/*******************************************************************************
                         Define object property
*******************************************************************************/
// date format type define
typedef enum
{
    MM_DD_YYYY,
    DD_MM_YYYY,
	YYYY_MM_DD,
} RtcDateFormat;

typedef enum
{
    HOUR_12,
    HOUR_24,
} RtcTimeFormat;

typedef enum
{
    DATE_TIME_OFF = 0,
    DATE_TIME_ON,
    DATE_TIME_UPDATE,
} MsgDateTimeType;

#pragma pack(1)
// hour, date format variable
typedef struct
{
    RtcTimeFormat timeMode;
    RtcDateFormat dateMode;
    unsigned char osdRtcState;
} RtcMode;
#pragma pack()

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
void DateTimeInit(void);
void DateTimeDisplay(void);
void DateTimeClose(void);
unsigned char RtcGetState(void);
RtcDateFormat RtcGetDateMode(void);
void OsdDateTimeSetState(unsigned char onOff);
/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/
void TimerDateTimeCallBack(void);


#endif
