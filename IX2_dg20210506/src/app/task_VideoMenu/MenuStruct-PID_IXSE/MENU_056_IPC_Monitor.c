#include "MENU_public.h"
#include "obj_business_manage.h"		


static int ipcWinId = 0;
	
void EnterIPCMonMenu(int win_id, char* name, int stack)
{
	ipcWinId = win_id;
	if(stack == 0)
	{
		VdShowHide();
	}
	StartInitOneMenu(MENU_056_IPC_MONITOR,0,stack);
	if(stack == 0)
	{
		VdShowToFull(ipcWinId);
	}
	else
	{
		SetWinDeviceType(ipcWinId, 1);
	}
}

static void IconListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < 3)
		{
			if(index == 0)
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_Exit;
			else if(index == 1)
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_REC;
			else
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_QUAD;
			
			pDisp->dispCnt++;
		}
	}
}

void IpcIconListShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	listInit.listType = ICON_1X2;
	listInit.listCnt = 2;
	listInit.fun = IconListPage;
	InitMenuListXY(listInit, 384, 536, 256, 64, 1, 0, ICON_888_ListView);
}
void IpcIconListClose(void)
{
	API_ListAlpha_Close(1, 384, 536, 256, 64, OSD_LAYER_CURSOR);
	MenuListDisable(0);
}
static int ipc_mon_start_time=0;
void MENU_056_IPC_Monitor_Init(int uMenuCnt)
{
	char name_temp[41];
	IpcIconListShow();
	SetLayerAlpha(8);
	GetWinDeviceName(ipcWinId, name_temp);
	API_OsdStringDisplayExt(bkgd_w/2-50, 5, COLOR_WHITE, name_temp, strlen(name_temp),1,STR_UTF8, 0);
	//Quad_Monitor_Timer_Init();	
	API_Business_Request(Business_State_MonitorIpc);
	ipc_mon_start_time=time(NULL);
}


void MENU_056_IPC_Monitor_Exit(void)
{
	IpcIconListClose();
	API_Business_Close(Business_State_MonitorIpc);
}

void MENU_056_IPC_Monitor_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	struct {char onOff; char win;} *data;
	char free[20]={0};
	char cpu[30]={0};
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				case KEY_TALK:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			if(GetCurIcon() == 0)
			{
				if(GetMenuListEn(0) == 1)
					IpcIconListClose();
				else
					IpcIconListShow();
			}
			else
			{
				switch(GetCurIcon())
				{
					case ICON_888_ListView:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
						if(listIcon.mainIcon == 0)
						{
							int cur_time=time(NULL);
							if((cur_time-ipc_mon_start_time)<3)
								usleep(((3-(cur_time-ipc_mon_start_time))*1000+500)*1000);
							Clear_ipc_show_layer(0);
							ExitQuadMenu();
							API_OneIpc_Show_stop(0);
							popDisplayLastMenu();
							usleep(500*1000);
						}
						else if(listIcon.mainIcon == 1)
						{
							if(api_record_start(ipcWinId)==0)
								SaveVdRecordProcess(ipcWinId);
						}
						else
						{
							EnterQuadMenu(ipcWinId, NULL, 1);
						}
						break;
				}
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(data->onOff, data->win);
				#if 0
				if(!data->onOff)
					SaveVdRecordProcess(ipcWinId);
				#endif
				#if 0
				rec_sprite_flag = *(char*)(arg+sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(rec_sprite_flag, 0);
				if(!rec_sprite_flag)
					SaveIpcRecordProcess();
				#endif
				break;
			case MSG_7_BRD_SUB_MonitorOff:
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_GetCpuMemUse:
				GetCpuMem_Polling(free, cpu);
				API_OsdStringDisplayExt(bkgd_w-100, 10, DISPLAY_STATE_COLOR, free, strlen(free), 0, STR_UTF8, 0);	
				API_OsdStringDisplayExt(bkgd_w-250, 50, DISPLAY_STATE_COLOR, cpu, strlen(cpu), 0, STR_UTF8, 0);	
				
				break;
			case MSG_7_BRD_SUB_IpcOpenRtspErr:
				LoadSpriteDisplay(0, 0);
			break;	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

#if 0
void ipc_record_stop(int index)
{
	int state = GetWinRecState(ipcWinId);
	printf("+++++++++ipc_record_stop, state[%d]\n", state); 
	if( state )
	{
		state = 0;
		SetWinRecState(ipcWinId, state);
		vd_record_inform(state, 0);	
	}
}

int ipc_record_start( void )		
{
	int recTime;
	int result = -1;
	char recfile[100];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	
	if(GetWinRecState(ipcWinId) == 1)
	{
		API_RecordEnd_mux(ipcWinId); 
		return 0;
	}

	SetWinRecFile(ipcWinId, recfile);
	if(Judge_SdCardLink() == 1) 
	{
		recTime = 30;
	}
	else
	{
		recTime = 3;
	}
	
	result = API_RecordStart_mux(ipcWinId, 0, 15, recfile, recTime, 0, ipc_record_stop);
	if( result == 0 )
	{
		SetWinRecState(ipcWinId, 1);	//开始录像
		vd_record_inform(1, 0);	
	}
	return 0;
}

void SaveIpcRecordProcess(void)
{
	CALL_RECORD_DAT_T record_temp;
	
	record_temp.type		= VIDEO_RECORD; 
	record_temp.subType 	= LOCAL;
	record_temp.property	= NORMAL;
	record_temp.target_node = 0;		
	record_temp.target_id	= 0;

	strcpy( record_temp.name, lastIpcName);
	strcpy( record_temp.input, "---");
	GetWinRecFile(ipcWinId, record_temp.relation);
	api_register_one_call_record( &record_temp );
	
}
#endif
