#include "MENU_public.h"

//#define	ParaManageIconMax	5
int ParaManageIconMax;
char ParaManage[10][5];
int ParaManageNum;
static char ParaValues[500];

int ParaManageIconSelect;
int ParaManagePageSelect;

static void DisplayParaManagePageIcon(int page)
{
	int i, j, index;
	uint16 x, y;
	int pageNum;
	char oneParaValue[500];
	int pos1, pos2;
	char *pIndex1, *pIndex2;
	ParaMenu_T record;
	ParaProperty_t property;
	int dataIndex, flag;
	char tempValue[500+1];
	POS pos;
	SIZE hv;

	sprintf(ParaValues, "<Group=%d, Index=%d, Number=%d>", GetParaGroupNumber(), page*ParaManageIconMax, ParaManageIconMax);
	API_Event_IoServer_InnerRead_Group(ParaValues);
	dprintf("ParaValues =%s\n", ParaValues);


	for(dataIndex = 0, i = 0; ParaValues[dataIndex] != 0; dataIndex++)
	{
		if(ParaValues[dataIndex] == '<')
		{
			flag = 1;
			pos1 = dataIndex+1;
		}
		else if(ParaValues[dataIndex] == '>')
		{
			if(flag == 1)
			{
				pos2 = dataIndex;
				
				memcpy(oneParaValue, ParaValues + pos1, pos2 - pos1);
				oneParaValue[pos2 - pos1] = 0;
				
				if((pIndex1 = strstr(oneParaValue, "maxNum=")) != NULL)
				{
					
					ParaManageNum = atoi(pIndex1+strlen("maxNum="));
				}
				else if((pIndex1 = strstr(oneParaValue, "ID=")) != NULL)
				{
					pIndex1 += strlen("ID=");

					pIndex2 = strchr(pIndex1, ',');
					if(pIndex2 == NULL)
					{
						return;
					}

					if((int)(pIndex2-pIndex1) != 4)
					{
						return;
					}
					
					memcpy(ParaManage[i], pIndex1, (int)(pIndex2-pIndex1));
					ParaManage[i][(int)(pIndex2-pIndex1)] = 0;
					pIndex2++;
					
					if((pIndex1 = strstr(pIndex2, "Value=")) != NULL)
					{
						pIndex1 += strlen("Value=");

						strcpy(tempValue, pIndex1);
					}

					char *disp;
					ComboBoxSelect_T tempParaComboBox;
					
					if(!GetParaMenuRecord(ParaManage[i], &tempParaComboBox))
					{
					
						API_Event_IoServer_InnerRead_Property(ParaManage[i], &property);
						
						if(property.editType)
						{
							for(j=0, tempParaComboBox.value = 0; j<tempParaComboBox.num; j++)
							{
								if(!strcmp(tempParaComboBox.data[j].value, tempValue))
								{
									disp = tempParaComboBox.data[j].comboBox;
									break;
								}
							}
						}
						else
						{
							disp = tempValue;
						}

						
						OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
						if(get_pane_type() == 5 )
						{
							x = pos.x+DISPLAY_DEVIATION_X;
							y = pos.y+5;
							API_OsdStringClearExt(x, y, bkgd_w-x, 80);
						}
						else
						{
							x = pos.x+DISPLAY_DEVIATION_X;
							y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
							API_OsdStringClearExt(x, y, bkgd_w-x, 40);
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, tempParaComboBox.name, tempParaComboBox.nameLen, 1, STR_UNICODE, hv.h-x);			

						if(!strcmp(ParaManage[i], DEVICE_FLOOR))
						{

						}
						else if(strcmp(ParaManage[i], Alarming_Setting)==0)
						{
							;
						}
						else
						{
							if(get_pane_type() == 5 )
							{
								x += 100;
								y += 40;
							}
							else
							{
								x += (hv.h - pos.x)/2;
							}
							API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);
						}
					}
					i++;
				}
				
				flag = 0;
			}
					
		}

	}
	
	for(; i<ParaManageIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		//x = pos.x+DISPLAY_DEVIATION_X;
		//y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;		
		//API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
	}

	pageNum = ParaManageNum/ParaManageIconMax + (ParaManageNum%ParaManageIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}



void MENU_071_ParaManage_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	ParaManageIconMax = GetListIconNum();
	if(GetLastNMenu() == MENU_015_INSTALL_SUB)
	{
		ParaManagePageSelect = 0;
	}
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetParaGroupNumber()-1+MESG_TEXT_ICON_ParaGroup1, 1, 0);
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	DisplayParaManagePageIcon(ParaManagePageSelect);
}

void MENU_071_ParaManage_Exit(void)
{

}

void MENU_071_ParaManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&ParaManagePageSelect, ParaManageIconMax, ParaManageNum, (DispListPage)DisplayParaManagePageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ParaManagePageSelect, ParaManageIconMax, ParaManageNum, (DispListPage)DisplayParaManagePageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					if(GetParaGroupNumber()==1 || GetParaGroupNumber()==2)
						return;
					ParaManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(ParaManagePageSelect  * ParaManageIconMax + ParaManageIconSelect < ParaManageNum)
					{
						ParaProperty_t property;
						extern ParaMenu_T OnePararecord;
						extern ComboBoxSelect_T OneParaComboBox;
						int ComboxSetParaValue(int value);
						int SetParaValue(const char* pData);
						int i;
						
						
						GetParaMenuRecord(ParaManage[ParaManageIconSelect], &OneParaComboBox);
						//StartInitOneMenu(MENU_072_OnePara,0,1);
						if(strcmp(ParaManage[ParaManageIconSelect], Alarming_Setting)==0)
						{
							Enter_AlarmingParaSettingRootMenu(0);
							return;
						}
						strcpy(OnePararecord.paraId, ParaManage[ParaManageIconSelect]);
						
						API_Event_IoServer_InnerRead_Property(OnePararecord.paraId, &property);
						API_Event_IoServer_InnerRead_All(OnePararecord.paraId, OnePararecord.value);
						if(property.readWrite & 0x02)
						{
							if(!strcmp(ParaManage[ParaManageIconSelect], DEVICE_FLOOR))
							{
								InitDeviceFloorSettingData(0);
								StartInitOneMenu(MENU_135_DeviceFloorSetting,0,1);
								return;
							}	

							if(strcmp(OnePararecord.paraId,IP_Address)==0||strcmp(OnePararecord.paraId,SubnetMask)==0||strcmp(OnePararecord.paraId,DefaultRoute)==0)
							{
								char dhcpbuff[5];
								API_Event_IoServer_InnerRead_All(DHCP_ENABLE, dhcpbuff);
								if(atoi(dhcpbuff)==1)
									return;	
							}
							if(!property.editType)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ParaValue, OnePararecord.value, 30, COLOR_WHITE, OnePararecord.value, 1, SetParaValue);
							}
							else
							{
								OneParaComboBox.editType = property.editType;
								
								for(i=0, OneParaComboBox.value = 0; i<OneParaComboBox.num; i++)
								{
									if(!strcmp(OneParaComboBox.data[i].value, OnePararecord.value))
									{
										OneParaComboBox.value = i;
										break;
									}
								}
								EnterParaPublicMenu(0, OneParaComboBox.value, ComboxSetParaValue);
							}
						}
						else
						{
							BEEP_ERROR();
						}
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}

