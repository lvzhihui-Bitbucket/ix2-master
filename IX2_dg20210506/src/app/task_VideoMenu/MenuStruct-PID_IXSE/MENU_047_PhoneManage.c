#include "MENU_public.h"


#define	PHONE_MANAGE_ICON_MAX	5

int phoneManageIconSelect;
int phoneManagePage;
int phoneManageConfirm;

static void DisplayOnePagePhone(int page);


void MENU_047_PhoneManage_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_011_SETTING)
	{
		phoneManageIconSelect = 0;
		phoneManagePage = 0;
	}
	API_MenuIconDisplaySelectOn(ICON_027_wireless);
	
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_PhoneManage,1, 0);
	//PublicListRefreshProcess(&phoneManageIconSelect, &phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
	DisplayOnePagePhone(phoneManagePage);
}

void MENU_047_PhoneManage_Exit(void)
{
}

void MENU_047_PhoneManage_Process(void* arg)
{

/*

	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 setValue;
	int i, x, y;
	DevReg_Info data;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					if(phoneManageIconSelect+phoneManagePage*PHONE_MANAGE_ICON_MAX == 0)
					{
						StartInitOneMenu(MENU_036_ADD_PHONE,0,1);
					}
					else
					{
						if(!phoneManageConfirm)
						{
							phoneManageConfirm = 1;
							API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y, SPRITE_IF_CONFIRM);
						}
						else
						{
							phoneManageConfirm = 0;
							Api_Get_OneRegLocalPhoneItem(phoneManageIconSelect+phoneManagePage*PHONE_MANAGE_ICON_MAX-1, &data);
							Api_Del_OneLocalPhone(data.ip_addr);
							UpdateOkSpriteNotify();
							PublicListRefreshProcess(&phoneManageIconSelect, &phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
						}
					}
					break;
					
				case KEY_UP:
					if(phoneManageConfirm)
					{
						phoneManageConfirm = 0;
						API_SpriteClose(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y, SPRITE_IF_CONFIRM);
					}
					PublicListUpProcess(&phoneManageIconSelect, &phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
					break;
				case KEY_DOWN:
					if(phoneManageConfirm)
					{
						phoneManageConfirm = 0;
						API_SpriteClose(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y, SPRITE_IF_CONFIRM);
					}
					PublicListDownProcess(&phoneManageIconSelect, &phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
					break;
				default:		//czn_20170120
					if(phoneManageConfirm)
					{
						phoneManageConfirm = 0;
						API_SpriteClose(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y, SPRITE_IF_CONFIRM);
					}
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					phoneManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(phoneManageIconSelect+phoneManagePage*PHONE_MANAGE_ICON_MAX == 0)
					{
						StartInitOneMenu(MENU_036_ADD_PHONE,0,1);
					}
					else if(phoneManageIconSelect+phoneManagePage*PHONE_MANAGE_ICON_MAX <= Api_Get_RegLocalPhoneNum())
					{
						void EnterDeletePhoneMenu(EnterDeletePhoneMenu);
						EnterDeletePhoneMenu(phoneManageIconSelect+phoneManagePage*PHONE_MANAGE_ICON_MAX - 1);
					}
					break;
					
				case ICON_202_PageUp:
					PublicPageUpProcess(&phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_SLAVE_REGISTER_APPLY:
				break;
			case MSG_7_BRD_SUB_SLAVE_DISCOVERY_ONE:
				phoneManageIconSelect = 0;
				Api_Reg_OneDxSlave((DevReg_Info*)(arg+sizeof(SYS_WIN_MSG)));
				PublicListRefreshProcess(&phoneManageIconSelect, &phoneManagePage, PHONE_MANAGE_ICON_MAX, Api_Get_RegLocalPhoneNum()+1, (DispListPage)DisplayOnePagePhone);
				break;	
				
			case MSG_7_BRD_SUB_SLAVE_DISCOVERY_OVER:
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}

*/

	
}
static void DisplayOnePagePhone(int page)
{

/*

	int i;
	uint16 x, y;
	uint8 pageNum;
	char display[80];
	int len;

	//API_DisableOsdUpdate();

	for(i = 0; i < PHONE_MANAGE_ICON_MAX; i++)
	{
		//x = GetIconXY(ICON_007_PublicList1+i).x+DISPLAY_DEVIATION_X;
		//y = GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;
		x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		
		if(page*PHONE_MANAGE_ICON_MAX+i < Api_Get_RegLocalPhoneNum()+1)
		{
			DevReg_Info data;
			char addr[20] = {0};
			if(page || i)
			{
				Api_Get_OneRegLocalPhoneItem(page*PHONE_MANAGE_ICON_MAX+i-1, &data);
				snprintf(addr, 20, "%03d.%03d.%03d.%03d", data.ip_addr&0xFF, (data.ip_addr>>8)&0xFF, (data.ip_addr>>16)&0xFF, (data.ip_addr>>24)&0xFF);
				API_GetOSD_StringWithID(MESG_TEXT_PhoneIP, NULL, 0, addr, strlen(addr), display, &len);
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, len,1,STR_UNICODE, DISPLAY_ICON_X_WIDTH);
			}
			else
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_RegisterPhone,1, MENU_SCHEDULE_POS_X - x);
			}
		}
	}
	
	//API_EnableOsdUpdate();

	pageNum = (Api_Get_RegLocalPhoneNum()+1)/PHONE_MANAGE_ICON_MAX + ((Api_Get_RegLocalPhoneNum()+1)%PHONE_MANAGE_ICON_MAX ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);

*/
	
}



