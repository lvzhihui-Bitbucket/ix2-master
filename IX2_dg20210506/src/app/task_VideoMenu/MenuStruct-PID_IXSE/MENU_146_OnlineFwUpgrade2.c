#include "MENU_public.h"
#include "cJSON.h"
#include "obj_Ftp.h"

typedef enum
{
	FwUpgrade_State_Uncheck = 0,
	FwUpgrade_State_Checking,
	FwUpgrade_State_CheckOk,
	FwUpgrade_State_Downloading,
	FwUpgrade_State_Intalling,
	FwUpgrade_State_Canceling,
	FwUpgrade_State_Intalled,
}Update_State_e;

#define	OnlineFwUpgrade_ICON_MAX	5
char onlineFwUpgrade_Code[9] = {0};
char onlineFwUpgrade_Server[40] = {0};
char onlineFwUpgrade_Server_Disp[3][40] = {"Input ip", "47.91.88.33", "47.106.104.38"};
Update_State_e updateState=FwUpgrade_State_Uncheck;
char update_file_dir[100];
int networkInfo=0;
char serverinput[20];
static int updateTimeCnt = 0;
OS_TIMER updatetimer;
static int sdupate_flag=0;
void UpdateTimerProcess(void)
{
	if(updateTimeCnt>0)
	{
		updateTimeCnt --;
		OS_RetriggerTimer(&updatetimer);
	}
}

const IconAndText_t onlineFwUpgradeIconTable[] = 
{
	{ICON_FwUpgrade_Server,			MESG_TEXT_ICON_FwUpgrage_Server},    
	{ICON_FwUpgrade_DLCode,			MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		NULL},
	{ICON_FwUpgrade_Notice,			NULL},
	{ICON_FwUpgrade_Operation,		MESG_TEXT_FwUpgrage_Check},
};

const int onlineFwUpgradeIconNum = sizeof(onlineFwUpgradeIconTable)/sizeof(onlineFwUpgradeIconTable[0]);

const char *updatestr[2] =
{
	"UpdateServer",
	"UpdateCode",
};


static void DisplayOnlineFwUpgradePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	for(i = 0; i < OnlineFwUpgrade_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x1 = x + 100;
			y1 = y + 40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x1 =pos.x+ (hv.h - pos.x)/2;
			y1 =y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnlineFwUpgrade_ICON_MAX+i < onlineFwUpgradeIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineFwUpgradeIconTable[i+page*OnlineFwUpgrade_ICON_MAX].iConText, 1, bkgd_w-x);
			switch(onlineFwUpgradeIconTable[i+page*OnlineFwUpgrade_ICON_MAX].iCon)
			{
				case ICON_FwUpgrade_Server:
					if(sdupate_flag==1)
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, "SD", strlen("SD"), 1, STR_UTF8, bkgd_w-x1);
					else
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, onlineFwUpgrade_Server, strlen(onlineFwUpgrade_Server), 1, STR_UTF8, bkgd_w-x1);
					break;
				
				case ICON_FwUpgrade_DLCode:
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, onlineFwUpgrade_Code, strlen(onlineFwUpgrade_Code), 1, STR_UTF8, bkgd_w-x1);
					break;
					
				case ICON_FwUpgrade_CodeInfo:
					
					break;
					
				case ICON_FwUpgrade_Notice:
					
					break;
					
				case ICON_FwUpgrade_Operation:
					
					break;

			}
		}
	}
	pageNum = onlineFwUpgradeIconNum/OnlineFwUpgrade_ICON_MAX + (onlineFwUpgradeIconNum%OnlineFwUpgrade_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
}
void MENU_146_OnlineFwUpgrade2_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_045_Upgrade);
	API_OsdStringClearExt(pos.x, hv.v/2, 300, CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);

	cJSON * ret = API_ReadRemoteIo(GetSearchOnlineIp(), cJSON_CreateStringArray(updatestr, 2));
	sdupate_flag=0;
	if(ret)
	{
		cJSON* item = cJSON_GetObjectItemCaseSensitive(ret, "READ");
		GetJsonDataPro(item, "UpdateServer", onlineFwUpgrade_Server);
		GetJsonDataPro(item, "UpdateCode", onlineFwUpgrade_Code);
		sdupate_flag=IfSdUpdateValid(onlineFwUpgrade_Code);
	}
	cJSON *view = cJSON_CreateObject();
	cJSON_AddStringToObject(view, "SIP_CONNECT_NETWORK", "PB");
	cJSON * netInfo = API_GetPbIo(GetSearchOnlineIp(), NULL, view);
	if(netInfo)
	{
		if(!strcmp(cJSON_GetObjectItemCaseSensitive(netInfo, "SIP_CONNECT_NETWORK")->valuestring, "LAN"))
		{
			networkInfo = 1;
		}
		else
		{
			networkInfo = 0;
		}
	}
	cJSON_Delete(view);
	DisplayOnlineFwUpgradePageIcon(0);
	updateState = FwUpgrade_State_Uncheck;
	static int updatetimer_init = 0;
	if(updatetimer_init == 0)
	{
		updatetimer_init = 1;
		OS_CreateTimer(&updatetimer, UpdateTimerProcess, 1000/25);	
	}
}


void MENU_146_OnlineFwUpgrade2_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_045_Upgrade);
	if(!networkInfo)
	{
		remove_update_tempfile();
	}
}

void Send_UpdateEvent(int opt, int ftp, char* file)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventUpdateRequest");
	cJSON_AddStringToObject(send_event, "OPERATE", opt==0? "CHECK":"INSTALL");
	cJSON_AddStringToObject(send_event, "DOWNLOAD_TYPE", ftp==0? "TFTP":"FTP");
	cJSON_AddStringToObject(send_event, "REPORT_IP",GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "SERVER", ftp==1? onlineFwUpgrade_Server : GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "DOWNLOAD_FILE",file);
	API_XD_EventJson(GetSearchOnlineIp(),send_event);
	cJSON_Delete(send_event);
}

static int FTP_Down_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			break;
		case FTP_STATE_DOWN_OK:
			if(sdupate_flag)
				snprintf(update_file_dir, 100, "%s/%s.txt", SdUpgradePath, onlineFwUpgrade_Code);
			else
				snprintf(update_file_dir, 100, "%s%s.txt", IX611_Update_Folder, onlineFwUpgrade_Code);
			Send_UpdateEvent(0, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			RemoteUpdate_Report("CHECK_ERROR");
			break;			
		default:
			break;
	}
	return 0;
}

static int FTP_Down2_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	char disp[40];
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			snprintf(disp, 40, "FTP_DOWNLOAD_ING %u%%", (int)(statistics.now*100.0/statistics.total));
			RemoteUpdate_Report(disp);
			break;
		case FTP_STATE_DOWN_OK:
			RemoteUpdate_Report("FTP_DOWNLOAD_OK");
			if(sdupate_flag)
				snprintf(update_file_dir, 100, "%s/%s", SdUpgradePath, onlineFwUpgrade_Code);
			else	
				snprintf(update_file_dir, 100, "%s%s", IX611_Update_Folder, onlineFwUpgrade_Code);
			Send_UpdateEvent(1, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			RemoteUpdate_Report("FTP_DOWNLOAD_ERROR");
			updateState = FwUpgrade_State_Canceling;
			break;			
		default:
			break;
	}
	return 0;
}
int IfSdUpdateValid(char *dlcode)
{
	char buff[100];
	snprintf(buff, 100, "%s/%s.txt", SdUpgradePath, dlcode);
	if( access( buff, F_OK ) != 0 )
		return 0;
	
	snprintf(buff, 100, "%s/%s", SdUpgradePath, dlcode);
	if( access( buff, F_OK ) != 0 )
		return 0;

	return 1;
	
}
void remove_update_tempfile(void)
{
	char cmd_buff[200];
	if( access( IX611_Update_Folder, F_OK ) == 0 )
	{
		snprintf(cmd_buff, 200, "rm -r %s", IX611_Update_Folder);
		system(cmd_buff);
	}
}

int confirm_set_servercode(const char* input)
{
	if(strlen(input) != 8)
	{
		return 0;
	}
	
	strcpy(onlineFwUpgrade_Code, input);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "UpdateCode", onlineFwUpgrade_Code);
	API_WriteRemoteIo(GetSearchOnlineIp(), newitem);
	cJSON_Delete(newitem);
	return 1;
}
void confirm_set_server(int select)
{
	if(select < 3 && select > 0)
	{
		strcpy(onlineFwUpgrade_Server, onlineFwUpgrade_Server_Disp[select]);
		cJSON* newitem = cJSON_CreateObject();
		cJSON_AddStringToObject(newitem, "UpdateServer", onlineFwUpgrade_Server);
		API_WriteRemoteIo(GetSearchOnlineIp(), newitem);
		cJSON_Delete(newitem);
	}
}
int InputServerSet(char* input)
{
	int temp[4];
	if(sscanf(input, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
	{
		return 0;
	}
	if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
	{
		return 0;
	}
	sprintf(onlineFwUpgrade_Server, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "UpdateServer", onlineFwUpgrade_Server);
	API_WriteRemoteIo(GetSearchOnlineIp(), newitem);
	cJSON_Delete(newitem);
	popDisplayLastNMenu(2);
	return -1;
}


void FwUpgrade2InputServerProcess(void)
{
	EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_Server2, serverinput, 15, COLOR_WHITE, onlineFwUpgrade_Server_Disp[0], 1, InputServerSet);
}

void MENU_146_OnlineFwUpgrade2_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int iconSelect;
	int i, j, setValue;
	printf("updateState=%d,sdupate_flag=%d\n",updateState,sdupate_flag);
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:					
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					iconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(iconSelect < onlineFwUpgradeIconNum)
					{
						switch(onlineFwUpgradeIconTable[iconSelect].iCon)
						{
							case ICON_FwUpgrade_Server:
								if(updateState == FwUpgrade_State_Uncheck)
								{
									for(i = 0; i < 3; i++)
									{
										for(publicSettingDisplay[i][0] = strlen(onlineFwUpgrade_Server_Disp[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
										{
											publicSettingDisplay[i][j] = onlineFwUpgrade_Server_Disp[i][j/2];
										}
									}
									EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_FwUpgrage_Server2, 3, 0, confirm_set_server);
									StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								}
								break;
							case ICON_FwUpgrade_DLCode:
								if(updateState == FwUpgrade_State_Uncheck)
								{
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_DLCode, serverinput, 8, COLOR_WHITE, onlineFwUpgrade_Code, 1, confirm_set_servercode);
								}
								break;
							case ICON_FwUpgrade_Operation:
								if(updateState == FwUpgrade_State_Uncheck)
								{
									updateState = FwUpgrade_State_Checking;
									if(sdupate_flag==1)
									{
										FTP_FILE_STATISTICS dumy;
										FTP_Down_CB(FTP_STATE_DOWN_OK,dumy,NULL);
									}
									else
									{
										if(networkInfo == 0)
										{
											snprintf(update_file_dir, 100, "%s.txt", onlineFwUpgrade_Code);
											RemoteUpdate_Report("CHECK_START");
											MakeDir(IX611_Update_Folder);
											FTP_StartDownload(onlineFwUpgrade_Server, update_file_dir, IX611_Update_Folder, NULL,  FTP_Down_CB);										
										}
										else
										{
											Send_UpdateEvent(0, 1, onlineFwUpgrade_Code);
										}	
									}
								}
								else if(updateState == FwUpgrade_State_CheckOk)
								{
									RemoteUpdate_Operation(MESG_TEXT_FwUpgrage_Install);
									updateState = FwUpgrade_State_Downloading;
									updateTimeCnt = 30;
									OS_RetriggerTimer(&updatetimer);
									AutoPowerOffReset();
									if(sdupate_flag==1)
									{
										FTP_FILE_STATISTICS dumy;
										FTP_Down2_CB(FTP_STATE_DOWN_OK,dumy,NULL);
									}
									else
									{
										if(networkInfo == 0)
										{
											RemoteUpdate_Report("FTP_DOWNLOAD_START");
											FTP_StartDownload(onlineFwUpgrade_Server, onlineFwUpgrade_Code, IX611_Update_Folder, NULL,  FTP_Down2_CB);
										}
										else
										{
											Send_UpdateEvent(1, 1, onlineFwUpgrade_Code);
										}

									}
								}
							break;
						}
					}
					break;
					
				case ICON_201_PageDown:
					break;
				case ICON_202_PageUp:
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

void RemoteUpdate_Operation(int opt)
{
	uint16 x, y;
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_011_PublicList5, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	API_OsdStringClearExt(x, y, hv.h - x, CLEAR_STATE_H);
	if(opt != 0)
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, opt, 1, bkgd_w - x);
}

void RemoteUpdate_Report(char* msg)
{
	uint16 x, y;
	POS pos;
	SIZE hv;
	printf("RemoteUpdate_Report =%s \n",msg);
	if(getOnlineManageSubMenu() != MENU_146_OnlineFwUpgrade2)
	{
		return;
	}
	OSD_GetIconInfo(ICON_010_PublicList4, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;

	API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
	
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, msg, strlen(msg), 1, STR_UTF8, bkgd_w-x);
	if(!strcmp(msg, "CHECK_PASSED"))	
	{
		updateState = FwUpgrade_State_CheckOk;
		RemoteUpdate_Operation(MESG_TEXT_FwUpgrage_InstallOrNot);
	}
	else if(!strcmp(msg, "CHECK_ERROR"))
	{
		updateState = FwUpgrade_State_Uncheck;
	}	
	else if(!strcmp(msg, "DOWNLOAD_OK") || !strcmp(msg, "INSTALLING"))
	{
		updateState = FwUpgrade_State_Intalling;
	}
	else if(!strcmp(msg, "DOWNLOAD_ERROR") || !strcmp(msg, "DOWNLOAD_STOP"))
	{
		updateState = FwUpgrade_State_Canceling;
	}
	else if(!strcmp(msg, "INSTALL_OK") || !strcmp(msg, "WAITING_REBOOT"))
	{
		updateState = FwUpgrade_State_Intalled;
	}
}

int CheckUpdateDownload(void)
{
	if(updateState == FwUpgrade_State_Downloading)
	{
		if(updateTimeCnt>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}	
	}
	else
	{
		return 0;
	}
}
