/**
  ******************************************************************************
  * @file    MENU_Keyboard.h
  * @author  caozhenbin
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the headers of the obj_Survey_Keyboard.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef OBJ_MLKBD_H
#define OBJ_MLKBD_H
#include "MENU_Keyboard.h"

#define MLKPRES_SDDIR			"/mnt/sdcard/Customerized/MulKp/"
#define MLKPRES_CusDIR		"/mnt/nand1-2/Customerized/MulKp/"
#define MLKPRES_ResDIR		"/mnt/nand1-2/MulKp/"

typedef struct
{
	int 	icon;
    	char input_ch[8];
	char cap_input_ch[8];
} MulLangKeyProperty;

#define MLKPRES_MAX_CNT					20

typedef struct
{
	int		cnt;	
	char	name[MLKPRES_MAX_CNT][100];
	char	displayName[MLKPRES_MAX_CNT][30];
	char invert_flag[MLKPRES_MAX_CNT];	//FOR_HEBREW
} MLKPRES_LIST_T;


#define Max_MulLangKPKey		26

int SetMLKPRes(int index);
int GetMLKPResIndex(void);
char** GetMLKPResListBuffer(void);
int GetMLKPResListRecord(int index, char* record);
int GetMLKPResListCnt(void);
int GetMLKPResFileName(char* fileName);
int LoadMLKPResListName(const char * dir, MLKPRES_LIST_T *pMLKPRes);
void MLKPResInit(void);

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
#if 0
unsigned char API_KeyboardInit(unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
							   unsigned short dispMaxCol, int color, unsigned char* pString, unsigned char highLight, available_check_func call_back_ptr );

void API_KeyboardInputStart(void);
void API_KeyboardInputModeChange(void);
unsigned char API_KeyboardInputOneKey(unsigned char Keypad_type,int icon);

int API_KeyboardInputGetcapsLock( void );
int API_KeyboardInputDataDump( char* pbuffer );
#endif

#endif



