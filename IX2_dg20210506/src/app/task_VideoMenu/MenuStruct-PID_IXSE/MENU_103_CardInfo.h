
#ifndef _MENU_094_CARD_INFO_H
#define _MENU_094_CARD_INFO_H

#include "MENU_public.h"

#pragma pack(1)
typedef struct
{
	unsigned char keyType;
	unsigned short num;
	char keyName[4];
	char key[11];
}IdCardDelReq_s;

typedef struct
{
	unsigned char keyType;
	unsigned short num;
}IdCardDelRsp_s;

#pragma pack()



#endif


