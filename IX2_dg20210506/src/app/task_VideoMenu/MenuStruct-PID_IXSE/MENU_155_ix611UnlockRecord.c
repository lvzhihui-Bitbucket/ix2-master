#include "MENU_public.h"
#include "cJSON.h"

int UnlockRecordIconMax;
int UnlockRecordIconSelect;
int UnlockRecordPageSelect;
int UnlockRecordIndex;
cJSON *unlockRecord=NULL;
int unlockRecordCnt;

void DisplayOnePageRecordList(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start, list_max;
	POS pos;
	SIZE hv;
	cJSON *item = NULL;
    char name[50] = {0};
    char type[50] = {0};
    char time[50] = {0};
	char display[100];

	for( i = 0; i < UnlockRecordIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		list_start = unlockRecordCnt - page*UnlockRecordIconMax - 1;
		if( (list_start-i) >= 0 )
		{
			item=cJSON_GetArrayItem(unlockRecord,list_start-i);
			GetJsonDataPro(item, "Time", time);    
			GetJsonDataPro(item, "Type", type); 
			GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(item, "Recorder"), "NAME", name);   
			snprintf(display, 100, "[%s] %s %s", time, name, type);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-pos.x);
		}
	}
	
	
	maxPage = unlockRecordCnt/UnlockRecordIconMax + (unlockRecordCnt%UnlockRecordIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_155_UnlockRecordList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	UnlockRecordIconMax = GetListIconNum();
	UnlockRecordIconSelect = 0;
	UnlockRecordPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_EventRecord);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_UnlockRecord,2, 0);
	unlockRecordCnt = 0;
	if(unlockRecord)
	{
		cJSON_Delete(unlockRecord);
		unlockRecord = NULL;
	}
	cJSON* view = cJSON_CreateArray();
	if(API_TB_SelectBySortByName("EventRecord", view, NULL, 0) > 0)
    {        
        unlockRecord = cJSON_Duplicate(view,1);
		unlockRecordCnt = cJSON_GetArraySize(unlockRecord);
		
    }
    cJSON_Delete(view);
	printf("DisplayOnePageunlockRecord cnt=%d\n",unlockRecordCnt);
	DisplayOnePageRecordList(UnlockRecordPageSelect);
}


void MENU_155_UnlockRecordList_Exit(void)
{
	if(unlockRecord)
	{
		cJSON_Delete(unlockRecord);
		unlockRecord = NULL;
	}
}

void MENU_155_UnlockRecordList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					UnlockRecordIconSelect = GetCurIcon() - ICON_007_PublicList1;
					UnlockRecordIndex = UnlockRecordPageSelect*UnlockRecordIconMax + UnlockRecordIconSelect;
					if(UnlockRecordIndex >= unlockRecordCnt)
						return;
					api_playback_event("Play_Start", cJSON_GetArrayItem(unlockRecord,unlockRecordCnt - UnlockRecordIndex - 1));
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&UnlockRecordPageSelect, UnlockRecordIconMax, unlockRecordCnt, (DispListPage)DisplayOnePageRecordList);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&UnlockRecordPageSelect, UnlockRecordIconMax, unlockRecordCnt, (DispListPage)DisplayOnePageRecordList);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


