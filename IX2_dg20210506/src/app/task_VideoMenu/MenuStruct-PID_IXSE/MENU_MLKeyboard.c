/**
  ******************************************************************************
  * @file    obj_Survey_Keyboard.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the functions of the obj_Survey_Keyboard
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
//#include "MENU_Keyboard.h"
#include "MENU_MLKeyboard.h"
#include "MENU_public.h"
#include "obj_menu_data.h"
#include "task_VideoMenu.h"
#include "obj_TableProcess.h"


one_vtk_table* MLKPRes_Table = NULL;
MLKPRES_LIST_T MLKPResList;
void LoadMLKPRes_Table(void);
int GetMLKPResFileName(char* fileName);

extern KbdProperty kbd_property;
extern OS_TIMER timer_sprite_ctrl;
int utf82unicode( char* pin, int in_len, short* pout );
int unicode2utf8(short *pin,int in_len,char *pout);
//////////////////////////////////////////////////////////////////////////////////////////////////////////
const KeyProperty MulLang_funcKey_TAB[]=
{	
	{ICON_127_Key26,	  CAPSLOCK_KEY_VALUE},
	//{ICON_128_Key27,	  BACKSPACE_KEY_VALUE},
	{ICON_129_Key28,	  TURN_TO_NUM_KEY_VALUE},
	//{ICON_134_Key33,	  RETURN_KEY_VALUE},
	//{ICON_135_Key34,	  OK_KEY_VALUE},
	//{ICON_166_Key65,	  TURN_TO_CHAR_KEY_VALUE},
	{ICON_128_Key27,	  BACKSPACE_KEY_VALUE},
	{ICON_134_Key33,	  RETURN_KEY_VALUE},
	{ICON_135_Key34,	  OK_KEY_VALUE},
	{ICON_132_Key31,		' '},
	{ICON_133_Key32,		'\r'},

};
const int MulLang_funcKey_Num = sizeof(MulLang_funcKey_TAB)/sizeof(MulLang_funcKey_TAB[0]);

MulLangKeyProperty MulLang_Char_TAB[Max_MulLangKPKey*2]=
{
	#if 0
	{ICON_136_Key35,	  "a","A"},
	{ICON_450_MLKPKey02,	  "b","B"},
	{ICON_451_MLKPKey03,	  "c","C"},
	{ICON_452_MLKPKey04,	  "d","D"},
	{ICON_453_MLKPKey05,	  "e","E"},
	{ICON_454_MLKPKey06,	 "f","F"},
	{ICON_455_MLKPKey07,	  "g","G"},
	{ICON_456_MLKPKey08,	  "h","H"},
	{ICON_457_MLKPKey09,	  "i","I"},
	{ICON_458_MLKPKey10,	  "j","J"},
	{ICON_459_MLKPKey11,	  "k","K"},
	{ICON_460_MLKPKey12,	  "l","L"},
	{ICON_461_MLKPKey13,	  "m","M"},
	{ICON_462_MLKPKey14,	  "n","N"},
	{ICON_463_MLKPKey15,	  "o","O"},
	{ICON_464_MLKPKey16,	  "p","P"},
	{ICON_465_MLKPKey17,	  "q","Q"},
	{ICON_466_MLKPKey18,	  "r","R"},
	{ICON_467_MLKPKey19,	  "s","S"},
	{ICON_468_MLKPKey20,	  "t","T"},
	{ICON_469_MLKPKey21,	  "u","U"},
	{ICON_470_MLKPKey22,	  "v","V"},
	{ICON_471_MLKPKey23,	  "w","W"},
	{ICON_472_MLKPKey24,	  "x","X"},
	{ICON_473_MLKPKey25,	  "y","Y"},
	{ICON_474_MLKPKey26,	  "z","Z"},
	#endif
	
};

int MulLangKP_validKey_Max=26;
int MulLangKP_CurPage=0;
POS GetIconXY(uint16 iconNum);
//char bufftest[3]={0xce,0xb2,0};
void DisplayMLKPAllKey(void)
{
	int i;
	POS pos;
	SIZE hv;
	int display_key_num;
	int start_index;
	if(MLKPRes_Table==NULL)
	{
		MLKPResInit();
	}
	if(MulLangKP_validKey_Max>Max_MulLangKPKey)
	{
		display_key_num=(Max_MulLangKPKey-1);
		start_index =MulLangKP_CurPage*(Max_MulLangKPKey-1);
		OSD_GetIconInfo(ICON_101_Key00, &pos, &hv);
		API_OsdStringClearExt(pos.x, pos.y, 90, 70);
		#if 0
		if(MulLangKP_CurPage == 0)
			API_SpriteDisplay_XY(GetIconXY(ICON_101_Key00).x, GetIconXY(ICON_101_Key00).y, SPRITE_MLKPPage1);
		else
			API_SpriteDisplay_XY(GetIconXY(ICON_101_Key00).x, GetIconXY(ICON_101_Key00).y, SPRITE_MLKPPage2);
		#endif
		if(MulLangKP_CurPage == 0)
			API_OsdStringDisplayExt(pos.x+(hv.h - pos.x-50)/2, pos.y+18, COLOR_RED, "1/2",strlen("1/2"),1,STR_UTF8,0);
		else
			API_OsdStringDisplayExt(pos.x+(hv.h - pos.x-50)/2, pos.y+18, COLOR_RED, "2/2",strlen("2/2"),1,STR_UTF8,0);
	}
	else
	{
		display_key_num = MulLangKP_validKey_Max;
		start_index = 0;
	}
	//MulLang_Char_TAB[5].input_ch=bufftest;
	for(i=0;i<display_key_num;i++)
	{
		//pos = GetIconXY(MulLang_Char_TAB[start_index+i].icon);
		OSD_GetIconInfo(MulLang_Char_TAB[start_index+i].icon, &pos, &hv);
		API_OsdStringClearExt(pos.x, pos.y, 90, 70);
		//x = pos.x;
		//y = pos.y + (hv.v - pos.y - ysize)/2;
		if(MulLang_Char_TAB[start_index+i].input_ch[0]==0)
			continue;
		if(kbd_property.capsLock == 0)
		{
			API_OsdStringDisplayExt(pos.x+(hv.h - pos.x-18)/2, pos.y+18, COLOR_WHITE, MulLang_Char_TAB[start_index+i].input_ch,strlen(MulLang_Char_TAB[start_index+i].input_ch),1,STR_UTF8,0);
		}
		else
		{
			if(strcmp(MulLang_Char_TAB[start_index+i].cap_input_ch,"NONE")==0)
			{
				API_OsdStringDisplayExt(pos.x+(hv.h - pos.x-18)/2, pos.y+18, COLOR_WHITE, MulLang_Char_TAB[start_index+i].input_ch,strlen(MulLang_Char_TAB[start_index+i].input_ch),1,STR_UTF8,0);
			}
			else
			{
				API_OsdStringDisplayExt(pos.x+(hv.h - pos.x-18)/2, pos.y+18, COLOR_WHITE, MulLang_Char_TAB[start_index+i].cap_input_ch,strlen(MulLang_Char_TAB[start_index+i].cap_input_ch),1,STR_UTF8,0);
			}	
		}
	}
}


/******************************************************************************
 * @fn      OneKeyValue()
 *
 * @brief   输入一个按键
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char MLKP_OneKeyValue(unsigned char Keypad_type,int icon,char *utf8)   
{
    uint8 i;
    uint8 key_value = 0;
	int display_key_num;
	int start_index;
	if(MulLangKP_validKey_Max>Max_MulLangKPKey)
	{
		if(icon==ICON_101_Key00)
		{
			MulLangKP_CurPage = (MulLangKP_CurPage==0)?1:0;
			API_DisableOsdUpdate();
			DisplayMLKPAllKey();
			API_EnableOsdUpdate();
			return 0;
		}
		display_key_num=(MulLangKP_CurPage==0)?(Max_MulLangKPKey-1):(MulLangKP_validKey_Max%(Max_MulLangKPKey-1));
		start_index =MulLangKP_CurPage*(Max_MulLangKPKey-1);
	}
	else
	{
		display_key_num = MulLangKP_validKey_Max;
		start_index = 0;
	}	
    if(Keypad_type == KEYPAD_CHAR ||Keypad_type == KEYPAD_NUM ) 
    {
    	for(i = 0;i<MulLang_funcKey_Num;i++)
    	{
		if(icon == MulLang_funcKey_TAB[i].icon)
		{
	            key_value = MulLang_funcKey_TAB[i].key_value;                                 //找到键值
			utf8[0] = key_value;
			utf8[1] = 0;
			return key_value;
		}
	}
        for(i=0;i<display_key_num;i++)                                            //查表找出哪个按键按下
        {
        	if(MulLang_Char_TAB[start_index+i].input_ch[0]==0)
			continue;
		if(icon == MulLang_Char_TAB[start_index+i].icon)
		{
			key_value ='a';
			if(kbd_property.capsLock == 0)
			{
	            		strcpy(utf8,MulLang_Char_TAB[start_index+i].input_ch);                           //找到键值
			}
			else
			{
				if(strcmp(utf8,MulLang_Char_TAB[start_index+i].cap_input_ch)==0)
				{
            				strcpy(utf8,MulLang_Char_TAB[start_index+i].input_ch);  
				}
				else
				{
            				strcpy(utf8,MulLang_Char_TAB[start_index+i].cap_input_ch);  
				}
			}
			 if(kbd_property.capsLock == 1)                                     //判断大写锁定只用一次
	                {
	                    kbd_property.capsLock = 0;
				API_DisableOsdUpdate();
				DisplayMLKPAllKey();
				API_EnableOsdUpdate();		
			 }
			#if 0
	            if((kbd_property.capsLock != 0) && (key_value>96) && (key_value < 123))//如果大写锁定并且按下的是26个字母，转换成大写字母
	            {
	                if(kbd_property.capsLock == 1)                                     //判断大写锁定只用一次
	                {
	                    kbd_property.capsLock = 0;
						API_OsdStringDisplayExt(380, 5, COLOR_GREEN, "abc   ", strlen("abc   "),0,STR_UTF8,0);
	                }
	                key_value = key_value - 32;
	            }
			#endif
	            break;                                                                 //结束循环
		}
        }
    }
    else if(Keypad_type == KEYPAD_EDIT)
    {
        key_value = SET_HIGHLIGHT_KEY_VALUE;                                           //高亮
    }
    return key_value;
}


/******************************************************************************
 * @fn      clearscrean()
 *
 * @brief   清屏
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
#if 0
void ClearScrean(void)
{
    uint8 i;
    for(i=0;i < kbd_property.dispMaxRow;i++)
    {
		API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*KP_DISP_CHAR_W, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + i*KP_DISP_CHAR_H, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*KP_DISP_CHAR_W, KP_DISP_CHAR_H);
    }
}
#endif
/******************************************************************************
 * @fn      DisplayAllScreen()
 *
 * @brief   更新整个编辑框的显示
 *
 * @param   iColor       显示颜色       范围0~15
 * @param   highLight    高亮属性       0--不高亮/ 1--高亮
 * @param   cursor       是否显示光标   0--不显示/1--显示
 *
 * @return  none
 *****************************************************************************/
void MLKP_DisplayAllScreen( void )       
{
    unsigned char Display[(MAX_DISPLAY_COL+10)*2] = {0};
    unsigned char i,j,k=0;
    unsigned char end_flag = 0;
	int 		backColor;

    if( kbd_property.highLight )                                                   //背景颜色设置
    {
        backColor = COLOR_GREEN;
    }
	else
	{
		backColor = COLOR_KEY;
	}
	#if 0
	printf("11111MLKP_DisplayAllScreen:%d\n",kbd_property.char_num);
	for(i=0;i<kbd_property.char_num*2;i++)
	{
		printf("%02x ",kbd_property.inputbuf[i]);
	}
	printf("\n");
	#endif
    for(i=0;i<kbd_property.dispMaxRow;i++)                          //更新显示
    {
    	
        for(j=0; j<kbd_property.dispMaxCol;j++)
        {
            if(k==kbd_property.char_num)
            {
                if(kbd_property.cursor && (kbd_property.char_num < kbd_property.inputSize) && (!kbd_property.highLight))
                {
                    Display[j*2] = '_';
			Display[j*2+1] =0;		
                }
				end_flag = 1;
                break;
            }
            else if(kbd_property.inputbuf[k*2]=='\r'&&kbd_property.inputbuf[k*2+1]==0)
            {
                k++;
                break;
            }
            else 
            {
                Display[j*2]=kbd_property.inputbuf[k*2];
		Display[j*2+1]=kbd_property.inputbuf[k*2+1];		
                k++; 
            } 
        }

		if( kbd_property.highLight )
		{
			//Display[j] = '\0';
		}
		else
		{
			for(;j<(kbd_property.dispMaxCol+10);j++)
			{
				 Display[j*2] = ' ';
				Display[j*2+1] =0;
			}
		}

		API_OsdStringDisplayExtWithBackColor(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*20, KEYBOARD_DISPLAY_Y + (kbd_property.dispStartRow + i)*30, kbd_property.dispColor, backColor, Display, j*2,1,STR_UNICODE);

		if(end_flag)
		{
			API_OsdStringClearExt(KEYBOARD_DISPLAY_X + kbd_property.dispStartCol*20, KEYBOARD_DISPLAY_Y + kbd_property.dispStartRow + (i+1)*30, bkgd_w-KEYBOARD_DISPLAY_X-kbd_property.dispStartCol*20, 30);
			break;
		}
	}
	
    if(i == kbd_property.dispMaxRow - 1)           //显示屏只剩下一行
    {
        kbd_property.dispFullFlag = 1;
    }
    else if(i == kbd_property.dispMaxRow)           //显示屏已经满了
    {
        kbd_property.dispFullFlag = 2;                              
    }
    else                                                            //显示屏空行还有两行或以上
    {
        kbd_property.dispFullFlag = 0;
    }
}

/******************************************************************************
 * @fn      API_KeyboardInit()
 *
 * @brief   初始化键盘属性
 *
 * @param   startRow        显示起始行  范围0~MAX_DISPLAY_ROW-1
 * @param   StartCol        显示起始列  范围0~MAX_DISPLAY_COL-1
 * @param   dispMaxRow      显示最大行  范围1~MAX_DISPLAY_ROW
 * @param   dispMaxCol      显示最大列  范围1~MAX_DISPLAY_COL
 * @param   color           显示颜色    范围0~15
 *
 * @return  1--成功/ 0--失败
 *****************************************************************************/
unsigned char API_MLKeyboardInit(unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
                               unsigned short dispMaxCol, int color, unsigned char* pString, unsigned char highLight, InputOK call_back_ptr )
{

	int i = 0; 
	
    //编辑框大小是:MAX_DISPLAY_ROW * MAX_DISPLAY_COL
    if((startRow >= MAX_DISPLAY_ROW) || (StartCol >= MAX_DISPLAY_COL) || \
       ((startRow + dispMaxRow) > MAX_DISPLAY_ROW) || ((StartCol + dispMaxCol) > MAX_DISPLAY_COL)/* || (color > 15)*/)
    {
        return 0;
    }
    else
    {
        kbd_property.dispStartCol = StartCol;
        kbd_property.dispStartRow = startRow;
        kbd_property.dispMaxCol = dispMaxCol;
        kbd_property.dispMaxRow = dispMaxRow;        
        kbd_property.dispColor = color;
        kbd_property.capsLock = 0;                                  //默认小写
        kbd_property.dispFullFlag =0;                               //显示屏未满
        kbd_property.cursor = 1;                                    //显示光标
        kbd_property.char_num = 0;                                  //inputbuf中的数据是0

		kbd_property.highLight		= highLight;
		kbd_property.call_back_ptr 	= call_back_ptr;
	MulLangKP_CurPage = 0;	
		
        if(dispMaxRow * dispMaxCol <= MAX_INPUTBUF_SIZE)
        {
            kbd_property.inputSize = dispMaxRow * dispMaxCol;       //输入数据限度
        }
        else
        {
            kbd_property.inputSize = MAX_INPUTBUF_SIZE;             //输入数据限度
        }
		
		if(pString != NULL)
		{
			short unicode[100];
			kbd_property.char_num = utf82unicode(pString,strlen(pString), (short*)unicode);
			memcpy(kbd_property.inputbuf,unicode,kbd_property.char_num*2);
			if(kbd_property.char_num>kbd_property.inputSize)
				kbd_property.char_num = kbd_property.inputSize;
		}
		
		//kbd_property.char_num = utf82unicode(kbd_property.inputbuf,i, (short*)kbd_property.unicode_buf);																 //buffer中数据的字节数
		//kbd_property.inputbuf[kbd_property.char_num] = '\0';									 //结尾添加结束符
		
        return 1;
    }
}

/******************************************************************************
 * @fn      API_KeyboardInputStart()
 *
 * @brief   开始输入
 *
 * @param   pString     开始输入字符指针，字符大小不能超过MAX_INPUTBUF_SIZE
 * @param   highLight   开始是否高亮  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_MLKeyboardInputStart( void )
{
    MLKP_DisplayAllScreen();
}

/******************************************************************************
 * @fn      API_KeyboardChangeHighLight()
 *
 * @brief   设置高亮或取消高亮
 *
 * @param   highLight  0--不高亮/ 1--高亮
 *
 * @return  none
 *****************************************************************************/
void API_MLKeyboardInputModeChange(void)
{
	kbd_property.highLight = !kbd_property.highLight;
	ClearScrean();
    MLKP_DisplayAllScreen();
}


/******************************************************************************
 * @fn      API_KeyboardInputOneKey()
 *
 * @brief   触摸输入
 *
 * @param   icon  icon值 KEYPAD_CHAR KEYPAD_NUM KEYPAD_CHAR KEYPAD_EDIT
 * @param   x 坐标值
 * @param   y 坐标值
 *
 * @return  key_value  0--没有输入 / 其他--按键值
 *****************************************************************************/
unsigned char API_MLKeyboardInputOneKey(unsigned char Keypad_type,int icon )
{
	char utf8_buf[5];
    unsigned char key_value;
	short unicode[10];
	if(Keypad_type == KEYPAD_NUM)
	{
    		key_value = OneKeyValue(Keypad_type, icon);
		utf8_buf[0]=key_value;
		utf8_buf[1]=0;
	}
	else
		key_value = MLKP_OneKeyValue(Keypad_type, icon,utf8_buf);
    if((key_value != RETURN_KEY_VALUE) && (key_value != 0) && (key_value != OK_KEY_VALUE) &&
       (key_value != TURN_TO_NUM_KEY_VALUE) && (key_value != TURN_TO_CHAR_KEY_VALUE))  
    {
		if(key_value == BACKSPACE_KEY_VALUE)														//输入退格键按下
		{
			if(kbd_property.highLight)																//高亮选中，全部删除buffer
			{
				kbd_property.inputbuf[0] = '\0';
				kbd_property.highLight = 0;
				kbd_property.char_num = 0;
			}
			else if(kbd_property.char_num>0)														//高亮没有选中，buffer中数据不为0
			{
				kbd_property.char_num--;
				//kbd_property.inputbuf[kbd_property.char_num] = '\0';
			}
		}
		else if(key_value == SET_HIGHLIGHT_KEY_VALUE)												//如果点一下编辑框，高亮属性反选
		{
			API_MLKeyboardInputModeChange();
			return key_value;
		}
		else if(key_value == CAPSLOCK_KEY_VALUE)													//大小写切换，改变大小写锁定属性
		{
			kbd_property.capsLock++;
			if(kbd_property.capsLock > 2)
			{
				kbd_property.capsLock = 0;
			}
			if(Keypad_type == KEYPAD_CHAR)
			{
				if(kbd_property.capsLock == 0||kbd_property.capsLock==1)
				{
					API_DisableOsdUpdate();
					DisplayMLKPAllKey();
					API_EnableOsdUpdate();
				}
			}
		}
		else																							 //如果输入其他，把输入值放到buffer中
		{ 
			if(kbd_property.highLight)
			{
				if((kbd_property.dispMaxRow != 1) || (key_value != '\r'))
				{
					kbd_property.highLight = 0;
					kbd_property.char_num = 0;

					utf82unicode(utf8_buf, strlen(utf8_buf), unicode);
					memcpy(kbd_property.inputbuf+kbd_property.char_num*2,unicode,2);
					kbd_property.char_num++;							
				}
			}
			else
			{
				if((kbd_property.char_num < kbd_property.inputSize) && (kbd_property.dispFullFlag != 2)) //输入字符不超过限制且屏幕未满允许输入
				{
					if(key_value == '\r')																 //输入回车键
					{	
						if(kbd_property.dispFullFlag == 0)
						{	
							utf82unicode(utf8_buf, strlen(utf8_buf), unicode);
							memcpy(kbd_property.inputbuf+kbd_property.char_num*2,unicode,2);
							kbd_property.char_num++;						 //数据后面加结束符
						}
					}
					else																				 //输入其他字符
					{
						utf82unicode(utf8_buf, strlen(utf8_buf), unicode);
						memcpy(kbd_property.inputbuf+kbd_property.char_num*2,unicode,2);
						kbd_property.char_num++;							 //数据后面加结束符
					} 
				}
			}
		}
		MLKP_DisplayAllScreen();//显示
    }
    return key_value;
}
#if 0
int API_KeyboardInputGetcapsLock( void )
{
	return kbd_property.capsLock;
}
#endif

//	返回 -1，不切换界面
//  返回 0， BEEP_ERROR();
//  返回 1， popDisplayLastMenu();
int API_MLKeyboardInputDataDump( char* pbuffer )
{
	int ret;
	char unicode[100];
	char utf8buff[100];
	if( pbuffer != NULL )
	{
		memcpy(unicode,kbd_property.inputbuf,kbd_property.char_num*2);
		if( kbd_property.call_back_ptr == NULL )
		{
			//strcpy(pbuffer, kbd_property.inputbuf);
			unicode2utf8(unicode, kbd_property.char_num,pbuffer);
			return 1;
		}
		else
		{
			unicode2utf8(unicode, kbd_property.char_num,pbuffer);
			printf("222222222pbuffer:%s\n",pbuffer);
			strcpy(utf8buff,pbuffer);
			ret = (*kbd_property.call_back_ptr)(utf8buff);
			return ret;
		}
	}
	else
		return 1;
}
//////////////////////////////////////////////////






int LoadMLKPResListName(const char * dir, MLKPRES_LIST_T *pMLKPRes)
{
	char cmd[100];
	char linestr[100];
	int ret = 0;
	char *backupDir;
	char *p;
	int i;
	
	snprintf(cmd, 100, "ls -l %s | grep .csv | awk '{print $9}'", dir);
	
	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}
		
		if(strlen(linestr) == 0)
		{
			continue;
		}
		
		snprintf(pMLKPRes->name[pMLKPRes->cnt++], 100, "%s%s", dir, linestr);

		p = strstr(linestr, ".csv");
		if(p != NULL)
		{
			p[0] = 0;
		}

		if(memcmp(linestr,"REV_",4)==0)		//FOR_HEBREW
		{
			snprintf(pMLKPRes->displayName[pMLKPRes->cnt-1], 30, "%s", linestr+4);
			pMLKPRes->invert_flag[pMLKPRes->cnt-1] = 1;
		}
		else
		{
			snprintf(pMLKPRes->displayName[pMLKPRes->cnt-1], 30, "%s", linestr);
			pMLKPRes->invert_flag[pMLKPRes->cnt-1] = 0;
		}
		//snprintf(pMenuLanguageList->displayName[pMenuLanguageList->cnt-1], 30, "%s", linestr);
		
		if(pMLKPRes->cnt >= MLKPRES_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}
#define MLKPRes_KEY_ID					"KEY_ID"
#define MLKPRes_KEY_SMALL_LETTER		"SMALL_LETTER"
#define MLKPRes_KEY_CAPITAL_LETTER		"CAPITAL_LETTER"

//#include "../../task_io_server/obj_TableProcess.h"


//one_vtk_table* MLKPRes_Table = NULL;
void LoadMLKPRes_Table(void)
{
	//char data[200];
	int i;
	int key_id;
	one_vtk_dat *precord;
	char input_str[20];
	int input_str_len;	
	int id_keyname_index;
	int small_keyname_index;
	int cap_keyname_index;
	char file_name[100];
	
	
	GetMLKPResFileName(file_name);
	if(MLKPRes_Table!=NULL)
	{
		free_vtk_table_file_buffer(MLKPRes_Table);	
		MLKPRes_Table = NULL;
	}
	
	MLKPRes_Table = load_vtk_table_file(file_name);

	
	
	
	if(MLKPRes_Table != NULL)
	{
		MulLangKP_validKey_Max = 0;
		for(i= 0;i<(Max_MulLangKPKey*2-2);i++)
		{
			memset(&MulLang_Char_TAB[i],0,sizeof(MulLangKeyProperty));
		}
		id_keyname_index = get_keyname_index( MLKPRes_Table, MLKPRes_KEY_ID);
		small_keyname_index = get_keyname_index( MLKPRes_Table, MLKPRes_KEY_SMALL_LETTER);
		cap_keyname_index= get_keyname_index( MLKPRes_Table, MLKPRes_KEY_CAPITAL_LETTER);
		for(i=0;i<MLKPRes_Table->record_cnt;i++)
		{
			precord = get_one_vtk_record_without_keyvalue( MLKPRes_Table, i);
			input_str_len = 10;
			get_one_record_string( precord, id_keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			key_id = atoi(input_str);
			if(key_id<1||key_id>(Max_MulLangKPKey*2-2))
				continue;
			//MulLang_Char_TAB[MulLangKP_validKey].icon=ICON_402_MLKPKey01+key_id-1;
			input_str_len = 8;
			get_one_record_string( precord, small_keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			strcpy(MulLang_Char_TAB[key_id-1].input_ch,input_str);

			input_str_len = 8;
			get_one_record_string( precord, cap_keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			strcpy(MulLang_Char_TAB[key_id-1].cap_input_ch,input_str);

			if(MulLangKP_validKey_Max<key_id)
				MulLangKP_validKey_Max = key_id;
				
		}
		if(MulLangKP_validKey_Max<=Max_MulLangKPKey)
		{
			for(i=0;i<Max_MulLangKPKey;i++)
			{
				MulLang_Char_TAB[i].icon=ICON_101_Key00+i;
			}
		}
		else
		{
			for(i=0;i<(Max_MulLangKPKey-1);i++)
			{
				MulLang_Char_TAB[i].icon=ICON_101_Key00+1+i;
				MulLang_Char_TAB[Max_MulLangKPKey-1+i].icon=ICON_101_Key00+1+i;
			}
		}
	}

}

int GetMLKPResListCnt(void)
{
	return MLKPResList.cnt;
}

int GetMLKPResListRecord(int index, char* record)
{
	if(index >= MLKPResList.cnt)
	{
		record[0] = 0;
		return -1;
	}

	strcpy(record, MLKPResList.name[index]);
	return 0;
}
char* MLKPResListDisplayBuffer[MLKPRES_MAX_CNT]={NULL};
char** GetMLKPResListBuffer(void)
{
	int i;

	for(i = 0; i < MLKPResList.cnt; i++)
	{
		MLKPResListDisplayBuffer[i] = MLKPResList.displayName[i];
	}
	
	return MLKPResListDisplayBuffer;
}
//char cur_mlkp_file[100]="/mnt/nand1-2/Customerized/MulKp/English.csv";
int GetMLKPResIndex(void)
{
	char record[100] = {0};
	int i;
	//strcpy(record,cur_mlkp_file);
    API_Event_IoServer_InnerRead_All(MLKP_LANGUAGE_SELECT, record);

	for(i = 0; i < MLKPResList.cnt; i++)
	{
		if(!strcmp(record, MLKPResList.name[i]))
		{
			break;
		}
	}
	
	return i;
}

int GetMLKPResFileName(char* fileName)
{
    API_Event_IoServer_InnerRead_All(MLKP_LANGUAGE_SELECT, fileName);
	FILE *fp=fopen(fileName,"r");
	//strcpy(fileName,cur_mlkp_file);
	if(fp == NULL )
	{
		if(GetMLKPResListCnt())
		{
			GetMLKPResListRecord(0, fileName);
			API_Event_IoServer_InnerWrite_All(MLKP_LANGUAGE_SELECT, fileName);

			
		}	
		else
		{
			
			return -1;
		}
	}
	else
	{
		fclose(fp);
	}
	return 0;
}

int SetMLKPRes(int index)
{
	if(index >= MLKPResList.cnt)
	{
		return -1;
	}

   API_Event_IoServer_InnerWrite_All(MLKP_LANGUAGE_SELECT, MLKPResList.name[index]);
	//strcpy(cur_mlkp_file,MLKPResList.name[index]);
	//load_menu_unicode_resource();
	LoadMLKPRes_Table();
	
	return 0;
}

void MLKPResInit(void)
{
	MLKPResList.cnt=0;
	LoadMLKPResListName(MLKPRES_SDDIR,&MLKPResList);
	if(MLKPResList.cnt == 0)
	{
		LoadMLKPResListName(MLKPRES_CusDIR,&MLKPResList);
		if(MLKPResList.cnt == 0)
		{
			LoadMLKPResListName(MLKPRES_ResDIR,&MLKPResList);
		}
	}
	LoadMLKPRes_Table();
}
