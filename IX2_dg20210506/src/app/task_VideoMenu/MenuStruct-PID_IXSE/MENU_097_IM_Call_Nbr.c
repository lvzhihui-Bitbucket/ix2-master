#include "MENU_097_IM_Call_Nbr.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "obj_ProgInfoByIp.h"
#include "obj_R8001_Table.h"
#include "vtk_udp_stack_device_update.h"

const IconAndText_t Call_NbrIconTable[] = 
{
	{0,					MESG_TEXT_Online_Device},    
	{0,					MESG_TEXT_R8001_Device},    
	{0,					MESG_TEXT_Programed},  
};

SearchIpRspData searchOnlineDevList;
int ProgramedNum;

void DisplayCallNbrSetup(void)
{
	int i;
	uint16 x, y;
	char display[20];
	POS pos;
	SIZE hv;
	for(i=0;i<3;i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, Call_NbrIconTable[i].iConText, 1, hv.h-x);
		x += (hv.h - pos.x)/2;

		switch(Call_NbrIconTable[i].iConText)
		{
			case MESG_TEXT_Online_Device:
				sprintf(display, "%d",searchOnlineDevList.deviceCnt+1);
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);	
				break;
			case MESG_TEXT_R8001_Device:
				sprintf(display, "%d",get_r8001_record_cnt());
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);	
				break;
			case MESG_TEXT_Programed:
				sprintf(display, "%d",ProgramedNum);
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);	
				break;
		}

	}
	
}
#if 0
void UpdateCallNbrByRes(CallNbr_T imCallNbr)
{
	SetSysVerInfo_name((strcmp(imCallNbr.R_Name, "-")&&imCallNbr.R_Name[0]) ? imCallNbr.R_Name : imCallNbr.Name);
	SetSysVerInfo_LocalNum(imCallNbr.Local);
	SetSysVerInfo_GlobalNum(imCallNbr.Global);
	//set ip
	char temp[20];
	char dhcpState;
	if(!strcmp(imCallNbr.ip_static,"DHCP"))//DHCP
	{
		API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
		if(atoi(temp) == 0)
		{
			dhcpState = 1;
			sprintf(temp, "%d", dhcpState);
			API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
			API_DHCP_SaveEnable(1);
		}
	}
	else		 //��̬
	{
		dhcpState = 0;
		sprintf(temp, "%d", dhcpState);
		API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
		API_DHCP_SaveEnable(0);
		SetSysVerInfo_IP(imCallNbr.ip);
		SetSysVerInfo_mask(imCallNbr.mask);
		SetSysVerInfo_gateway(imCallNbr.gw);
		SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		
	}
	
}
#endif
void MENU_097_IM_Call_Nbr_Init(int uMenuCnt)
{
	char disp[100];
	int disp_len;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IM_Call_Nbr_SET, 1, 0);
	//API_OsdUnicodeStringDisplay(400, 105+4*DISPLAY_LIST_SPACE, DISPLAY_LIST_COLOR, MESG_TEXT_Program_TIP1, 1, MENU_SCHEDULE_POS_X-400);

	BusySpriteDisplay(1);
	API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineDevList);
	ProgramedNum = 0;
	BusySpriteDisplay(0);

	API_DisableOsdUpdate();
	DisplayCallNbrSetup();
	API_EnableOsdUpdate();
}

void MENU_097_IM_Call_Nbr_Exit(void)
{
	
}

void MENU_097_IM_Call_Nbr_Process(void* arg)
{
	DeviceInfo devInfo;
	CallNbr_T imCallNbr;
	int i;
	uint16 x, y;
	char disp[100];
	int disp_len;
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;

				case ICON_011_PublicList5:
					if( !get_r8001_record_cnt())
					{
						return;
					}
					//API_OsdStringClearExt(400, 105+4*DISPLAY_LIST_SPACE, bkgd_w-400, 40);
					//API_OsdUnicodeStringDisplay(400, 105+4*DISPLAY_LIST_SPACE, DISPLAY_LIST_COLOR, MESG_TEXT_Program_TIP2, 1, MENU_SCHEDULE_POS_X-400);
					//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
					//y = DISPLAY_LIST_Y+3*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
					if(get_r8001_record_by_brm(GetSysVerInfo_BdRmMs(),&imCallNbr))
					{
						sprintf(disp, "%s",GetSysVerInfo_BdRmMs());
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x);
						UpdateCallNbrByRes(imCallNbr);
						ProgramedNum++;
						sprintf(disp, "%d",ProgramedNum);
						//API_OsdStringDisplayExt(x+250, DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					}
					for(i = 0; i < searchOnlineDevList.deviceCnt; i++)
					{
						API_GetInfoByIp(searchOnlineDevList.data[i].Ip, 2, &devInfo);

						if(get_r8001_record_by_brm(devInfo.BD_RM_MS,&imCallNbr))
						{
							API_OsdStringClearExt(x, y, bkgd_w-x, 40);
							//get_r8001_record_items(i,&imCallNbr);
							sprintf(disp, "%s",devInfo.BD_RM_MS);
							API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x);						
							//strcpy(devInfo.BD_RM_MS,imCallNbr.BD_RM_MS);
							strcpy(devInfo.Global,imCallNbr.Global);
							strcpy(devInfo.Local,imCallNbr.Local);
							strcpy(devInfo.name1,imCallNbr.Name);
							strcpy(devInfo.name2_utf8,imCallNbr.R_Name);
							strcpy(devInfo.IP_STATIC,imCallNbr.ip_static);
							strcpy(devInfo.IP_ADDR,imCallNbr.ip);
							strcpy(devInfo.IP_MASK,imCallNbr.mask);
							strcpy(devInfo.IP_GATEWAY,imCallNbr.gw);
							//if(!API_ProgInfoByIp(searchOnlineDevList.data[i].Ip, 5, &devInfo))
							API_ProgInfoByIp(searchOnlineDevList.data[i].Ip, 5, &devInfo);
							{
								ProgramedNum++;
							}
							//x += (DISPLAY_VALUE_X_DEVIATION-150);
							//y = DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
							sprintf(disp, "%d",ProgramedNum);
							API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x);	
						}
					}
					//API_OsdStringClearExt(400, 105+4*DISPLAY_LIST_SPACE, bkgd_w-400, 40);
					//API_OsdUnicodeStringDisplay(400, 105+4*DISPLAY_LIST_SPACE, DISPLAY_LIST_COLOR, MESG_TEXT_Program_TIP3, 1, MENU_SCHEDULE_POS_X-400);

					break;	
					

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}


