#if 0
#include "MENU_public.h"
#include "cJSON.h"

int CallRecordListIconMax;
int CallRecordIconSelect;
int CallRecordPageSelect;
int CallRecordIndex;
cJSON *CallRecordList=NULL;
int CallRecordListCnt;
int deletAllConfirm;
int deletAllConfirmX,deletAllConfirmY;

void DisplayOnePageCallRecordlist(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;
	POS pos;
	SIZE hv;
	cJSON *item = NULL;
    char name[50] = {0};
    char time[50] = {0};
	char display[100];

	for( i = 0; i < CallRecordListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		list_start = CallRecordListCnt - page*CallRecordListIconMax - 1;
		if( (list_start-i) >= 0 )
		{
			item=cJSON_GetArrayItem(CallRecordList,list_start-i);
			GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(item, "Call_Source"), "NAME", name);        
			GetJsonDataPro(item, "Time", time);    
			snprintf(display, 100, "[%s] %s", time, name);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-pos.x);
		}
	}
	
	
	maxPage = CallRecordListCnt/CallRecordListIconMax + (CallRecordListCnt%CallRecordListIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}
void DisplayCallRecordlist(void)
{
	CallRecordListCnt = 0;
	if(CallRecordList)
	{
		cJSON_Delete(CallRecordList);
		CallRecordList = NULL;
	}
	cJSON* view = cJSON_CreateArray();
	if(API_TB_SelectBySortByName("IMCallRecord", view, NULL, 0) > 0)
	{
		CallRecordList = cJSON_Duplicate(view,1);
		CallRecordListCnt = cJSON_GetArraySize(CallRecordList);
		//char *str=cJSON_Print(CallRecordList);
		//printf("Get IMCallRecord=%s\n",str);
		//free(str);
	}
	cJSON_Delete(view);
	DisplayOnePageCallRecordlist(0);
}
void MENU_155_CallRecord_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	CallRecordListIconMax = GetListIconNum();
	API_MenuIconDisplaySelectOn(ICON_018_IncomingCalls);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_IncomingCallList,2, 0);
	CallRecordPageSelect = 0;

	OSD_GetIconInfo(ICON_CallRecordDelAll, &pos, &hv);
	deletAllConfirmX = hv.h - xsize;
	deletAllConfirmY = hv.v - ysize;
	deletAllConfirm = 0;
	DisplayCallRecordlist();
}

void MENU_155_CallRecord_Exit(void)
{
}

void MENU_155_CallRecord_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
									
					break;
				case KEY_DOWN:
								
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_018_IncomingCalls:
					if(deletAllConfirm)
					{
						API_SpriteClose(deletAllConfirmX, deletAllConfirmY, SPRITE_IF_CONFIRM);
						deletAllConfirm = 0;
					}
					API_MenuIconDisplaySelectOn(ICON_018_IncomingCalls);
					break;
				case ICON_CallRecordDelAll:
					if(deletAllConfirm == 0)
					{
						deletAllConfirm = 1;
						API_SpriteDisplay_XY(deletAllConfirmX, deletAllConfirmY, SPRITE_IF_CONFIRM);
						
					}
					else
					{
						API_SpriteClose(deletAllConfirmX, deletAllConfirmY, SPRITE_IF_CONFIRM);
						deletAllConfirm = 0;
						DelAllRecRecord();
						usleep(500*1000);
						DisplayCallRecordlist();
					}
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					CallRecordIconSelect = GetCurIcon() - ICON_007_PublicList1;
					CallRecordIndex = CallRecordPageSelect*CallRecordListIconMax + CallRecordIconSelect;
					if(CallRecordIndex >= CallRecordListCnt)
						return;
					api_playback_event("Play_Start", cJSON_GetArrayItem(CallRecordList,CallRecordListCnt - CallRecordIndex - 1));
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&CallRecordPageSelect, CallRecordListIconMax, CallRecordListCnt, (DispListPage)DisplayOnePageCallRecordlist);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&CallRecordPageSelect, CallRecordListIconMax, CallRecordListCnt, (DispListPage)DisplayOnePageCallRecordlist);
					break;
				default:
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

#endif
