#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "MENU_119_IpcRecord.h"

int IpcRecordIconSelect;
int IpcRecordPageSelect;
int IpcRecordIconMax;
IPC_RecTable ipcRecTable;
char IpcRecSearch[21];

int GetIpcRecTableNum(void)
{
	return ipcRecTable.recordCnt;
}
int DeleteOneRecRecord(int index)
{
	int i;
	char tempChar[100];
	
	if(ipcRecTable.recordCnt <= index)
	{
		return -1;
	}
	snprintf(tempChar,100, "rm %s/%s\n",VIDEO_STORE_DIR, ipcRecTable.recordName[index]);
	system(tempChar);
	sync();
	for(i=index; i+1 < ipcRecTable.recordCnt; i++)
	{
		strcpy(ipcRecTable.recordName[i], ipcRecTable.recordName[i+1]);
	}
	ipcRecTable.recordCnt--;
	return 0;
}
int GetIpcRecRecord(int index, char* record)
{
	if(index >= ipcRecTable.recordCnt)
	{
		record[0] = 0;
		return -1;
	}

	strcpy(record, ipcRecTable.recordName[index]);
	return 0;
}

int GetIpcRecListName(const char * dir, const char* keyWord, IPC_RecTable *pIpcRecList)
{
	char linestr[100];
	int ret = 0;
	//char *p;
	int i;
	
	//snprintf(cmd, 100, "ls -l %s | egrep '.mkv|.mp4' | awk '{print $9}'", dir);
	snprintf(linestr, 100, "ls %s|grep \"%s\"\n", dir, keyWord);
	
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}
		
		if(strlen(linestr) == 0)
		{
			continue;
		}
		
		snprintf(pIpcRecList->recordName[pIpcRecList->recordCnt++], 100, "%s", linestr);
		#if 0
		p = strstr(linestr, ".mp4");
		if(p != NULL)
		{
			p[0] = 0;
		}
		snprintf(pIpcRecList->recordName[pIpcRecList->recordCnt-1], 50, "%s", linestr);
		#endif
		if(pIpcRecList->recordCnt >= IPC_REC_MAX)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}

void GetIpcRecListByKey(char* keyword)
{
	ipcRecTable.recordCnt = 0;
	
	GetIpcRecListName("/mnt/sdcard/video/", keyword, &ipcRecTable);//20201113.*CH3 ".mp4"

}

void DisplayOnePageIpcRecord(uint8 page)
{
	int i, x, y, maxPage;
	int list_start, list_max;
	POS pos;
	SIZE hv;
	char display[100];

	list_max = ipcRecTable.recordCnt+1;
	for( i = 0; i < IpcRecordIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		list_start = page*IpcRecordIconMax+i;
		if(list_start == 0)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_290_Search, 1, hv.h - x);
		}
		else if(list_start < list_max)
		{
			snprintf(display, 100, "%s", ipcRecTable.recordName[list_start-1]);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-pos.x);
		}

	}
	maxPage = list_max/IpcRecordIconMax + (list_max%IpcRecordIconMax ? 1 : 0);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_119_IpcRecord_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	IpcRecordIconSelect = 0;
	IpcRecordPageSelect = 0;
	IpcRecordIconMax = GetListIconNum();
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_DVR_REC, 1, 0);
	API_MenuIconDisplaySelectOn(ICON_DVR_REC);
	if(GetLastNMenu() == MENU_116_MultiRec)
	{
		GetIpcRecListByKey(".mp4");
	}
	DisplayOnePageIpcRecord(IpcRecordPageSelect);
}

int SearchResult(char* keyword)
{
	int len = strlen(keyword);
	if(len > 0 &&  len <= 20)
	{
		strcpy(IpcRecSearch, keyword);
		GetIpcRecListByKey(IpcRecSearch);
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
		
}


void MENU_119_IpcRecord_Exit(void)
{
}

void MENU_119_IpcRecord_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int iconSel;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					iconSel = IpcRecordPageSelect*IpcRecordIconMax + GetCurIcon() - ICON_007_PublicList1;
					if(iconSel >= ipcRecTable.recordCnt+1)
						return;
					if(iconSel == 0)
					{
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_KeypadHelpEnter, IpcRecSearch, 20, COLOR_WHITE, IpcRecSearch, 1, SearchResult);
					}
					else
					{
						Enter_DvrPlay(iconSel-1);
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&IpcRecordPageSelect, IpcRecordIconMax, ipcRecTable.recordCnt+1, (DispListPage)DisplayOnePageIpcRecord);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&IpcRecordPageSelect, IpcRecordIconMax, ipcRecTable.recordCnt+1, (DispListPage)DisplayOnePageIpcRecord);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


