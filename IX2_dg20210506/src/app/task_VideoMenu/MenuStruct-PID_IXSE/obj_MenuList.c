
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "cJSON.h"

#include "task_VideoMenu.h"
#include "obj_menu_data.h"
#include "obj_MenuList.h"
#include "define_file.h"

/*******************************************************************
 ___________________________________________________________________
|																	|
|																	|
4																	|
|																	|
|________________________________2__________________________________|
|1																|	|
|																|	|
|																|	|
|																|	|
|																|	|
3																|	|
|																|	|
|																|	|
|																|	|
|																|	|
|_______________________________________________________________|___|

1-坐标 textBoxPositionX， textBoxPositionY
2-textBoxWidth
3-textBoxHeight
4-textBoxTitleHeight

********************************************************************/
#define MENULIST_TAB_MAX		21

static MenuListResTable_T listTable = {0};
static LIST_PROPERTY listView[3];

const int MENULIST_TABXY[][MENULIST_TAB_MAX]=
{
	//boxX,boxY,boxW,boxH, titH,textX,textY,textW,textH,upX, upY,upW, upH, dnX, dnY, dnW, dnH,schBW,schBH,schTW,schTH
	280,   0,	744, 528, 86,  310,  105,  700,  423,  702, 0,  161, 90, 863, 0, 161, 90, 26, 406, 24, 66,// IX47

};

int InitMenuListXY(LIST_INIT_T listInit, int x, int y, int w, int h, int alpha, int id, int iconid)
{
	int i;

	listView[id].listViewEnable = 1;
	listView[id].listViewAlpha = alpha;
	listView[id].listViewId = iconid;
	listView[id].titleStrLen = 0;

	listView[id].boxX =	x;
	listView[id].boxY =	y;
	listView[id].boxWidth =	w;
	listView[id].boxHeight = h;

	listView[id].textBoxPositionX = x;
	listView[id].textBoxPositionY = y;
	listView[id].textBoxWidth = w;
	listView[id].textBoxHeight = h;
	
	listView[id].up.icon = MENU_LIST_ICON_UP;
	listView[id].up.x = MENU_LIST_upX;
	listView[id].up.y = MENU_LIST_upY;
	listView[id].up.w = MENU_LIST_upWidth;
	listView[id].up.h = MENU_LIST_upHeight;
	listView[id].up.iconType = ICON_PIC_UP;
	listView[id].up.revColor = COLOR_LIST_MENU_ICON;
	

	
	listView[id].valAlign = listInit.valAlign;
	listView[id].textAlign = listInit.textAlign;
	
	listView[id].up.fun.x = listView[id].up.x;
	listView[id].up.fun.y = listView[id].up.y;
	listView[id].up.fun.xsize = listView[id].up.w;
	listView[id].up.fun.ysize = listView[id].up.h;
	listView[id].up.fun.spiAlign = ALIGN_LEFT_UP;

	listView[id].down.icon = MENU_LIST_ICON_DOWN;
	listView[id].down.x = MENU_LIST_downX;
	listView[id].down.y = MENU_LIST_downY;
	listView[id].down.w = MENU_LIST_downWidth;
	listView[id].down.h = MENU_LIST_downHeight;
	listView[id].down.iconType = ICON_PIC_DOWN;
	listView[id].down.revColor = COLOR_LIST_MENU_ICON;
	
	
	listView[id].down.fun.x = listView[id].down.x;
	listView[id].down.fun.y = listView[id].down.y;
	listView[id].down.fun.xsize = listView[id].down.w;
	listView[id].down.fun.ysize = listView[id].down.h;
	listView[id].down.fun.spiAlign = ALIGN_LEFT_UP;

	listView[id].scheduleBotW = MENU_LIST_scheduleBotW;
	listView[id].scheduleBotH = MENU_LIST_scheduleBotH;
	listView[id].scheduleTopW = MENU_LIST_scheduleTopW;
	listView[id].scheduleTopH = MENU_LIST_scheduleTopH;
	listView[id].scheduleBotSpriteId = MENU_LIST_ScheduleBottom;
	listView[id].scheduleTopSpriteId = MENU_LIST_ScheduleTop;
	
	listView[id].listType = listInit.listType;

	listView[id].getDisplay = listInit.fun;

    listView[id].textSize = listInit.textSize;											
    listView[id].textColor = listInit.textColor;											
    listView[id].valColor = listInit.valColor;											
	listView[id].listIconEn = listInit.listIconEn;
	listView[id].valEn = listInit.valEn;
    listView[id].textListOffsetX = listInit.textOffset;
	listView[id].textValOffset = listInit.textValOffset;
	listView[id].textValOffsetY = listInit.textValOffsetY;
	listView[id].selEn = listInit.selEn;
	listView[id].ediEn = listInit.ediEn;

	SetRowAndColumnAndTextSize(listView[id].listType, id);

	listView[id].onePageListMax = listView[id].lineNum * listView[id].columnNum;
	

	listView[id].listNum = listInit.listCnt;
	listView[id].maxPage = listInit.listCnt/listView[id].onePageListMax + (listInit.listCnt%listView[id].onePageListMax ? 1 : 0);
	listView[id].textBoxWidth = ((listView[id].maxPage > 1) ? (listView[id].textBoxWidth - listView[id].scheduleBotW) : listView[id].textBoxWidth);

	listView[id].lineHeight	= listView[id].textBoxHeight/listView[id].lineNum;
	listView[id].lineWidth = listView[id].textBoxWidth/listView[id].columnNum;

	listView[id].currentPage = listInit.currentPage >= listView[id].maxPage ? 0 : listInit.currentPage;
	
	CreateIcons(listView[id].onePageListMax, id);

	DisplayOnePage(id);
}


int GetMenuListXY(int index)
{
	int panel = get_pane_type()-9;

	if(index < MENULIST_TAB_MAX)
	{
		return MENULIST_TABXY[panel][index];
	}
	else
	{
		return 0;
	}
}

//初始化对象，带进需要随时改变的参数
int InitMenuList(LIST_INIT_T listInit)
{
	int i;
	int panel = get_pane_type()-9;

	listView[0].listViewEnable = 1;
	listView[0].listViewAlpha = 0;
	listView[0].listViewId = ICON_888_ListView;

	
	listView[0].titleColor = listInit.titleColor;
	listView[0].titleSize = listInit.titleSize;
	listView[0].titleOffset = listInit.titleOffset;
	
	listView[0].titleHeight = MENULIST_TABXY[panel][4];
	listView[0].titleStrLen = listInit.titleStrLen;
	memcpy(listView[0].titleStr, listInit.titleStr, listView[0].titleStrLen);

	listView[0].boxX =	MENULIST_TABXY[panel][0];
	listView[0].boxY =	MENULIST_TABXY[panel][1];
	listView[0].boxWidth =	MENULIST_TABXY[panel][2];
	listView[0].boxHeight = MENULIST_TABXY[panel][3];

	listView[0].textBoxPositionX =	MENULIST_TABXY[panel][5];
	listView[0].textBoxPositionY =	MENULIST_TABXY[panel][6];
	listView[0].textBoxWidth =	MENULIST_TABXY[panel][7];
	listView[0].textBoxHeight = MENULIST_TABXY[panel][8];
	
	listView[0].up.icon = MENU_LIST_ICON_UP;
	listView[0].up.x = MENULIST_TABXY[panel][9];
	listView[0].up.y = MENULIST_TABXY[panel][10];
	listView[0].up.w = MENULIST_TABXY[panel][11];
	listView[0].up.h = MENULIST_TABXY[panel][12];
	listView[0].up.iconType = ICON_PIC_UP;
	listView[0].up.revColor = COLOR_LIST_MENU_ICON;
	

	
	listView[0].valAlign = listInit.valAlign;
	listView[0].textAlign = listInit.textAlign;
	
	listView[0].up.fun.x = listView[0].up.x;
	listView[0].up.fun.y = listView[0].up.y;
	listView[0].up.fun.xsize = listView[0].up.w;
	listView[0].up.fun.ysize = listView[0].up.h;
	listView[0].up.fun.spiAlign = ALIGN_LEFT_UP;

	listView[0].down.icon = MENU_LIST_ICON_DOWN;
	listView[0].down.x = MENULIST_TABXY[panel][13];
	listView[0].down.y = MENULIST_TABXY[panel][14];
	listView[0].down.w = MENULIST_TABXY[panel][15];
	listView[0].down.h = MENULIST_TABXY[panel][16];
	listView[0].down.iconType = ICON_PIC_DOWN;
	listView[0].down.revColor = COLOR_LIST_MENU_ICON;
	
	
	listView[0].down.fun.x = listView[0].down.x;
	listView[0].down.fun.y = listView[0].down.y;
	listView[0].down.fun.xsize = listView[0].down.w;
	listView[0].down.fun.ysize = listView[0].down.h;
	listView[0].down.fun.spiAlign = ALIGN_LEFT_UP;

	listView[0].scheduleBotW = MENULIST_TABXY[panel][17];
	listView[0].scheduleBotH = MENULIST_TABXY[panel][18];
	listView[0].scheduleTopW = MENULIST_TABXY[panel][19];
	listView[0].scheduleTopH = MENULIST_TABXY[panel][20];
	listView[0].scheduleBotSpriteId = MENU_LIST_ScheduleBottom;
	listView[0].scheduleTopSpriteId = MENU_LIST_ScheduleTop;
	
	listView[0].listType = listInit.listType;

	listView[0].getDisplay = listInit.fun;

    listView[0].textSize = listInit.textSize;											
    listView[0].textColor = listInit.textColor;											
    listView[0].valColor = listInit.valColor;											
	listView[0].listIconEn = listInit.listIconEn;
	listView[0].valEn = listInit.valEn;
    listView[0].textListOffsetX = listInit.textOffset;
	listView[0].textValOffset = listInit.textValOffset;
	listView[0].textValOffsetY = listInit.textValOffsetY;
	listView[0].selEn = listInit.selEn;
	listView[0].ediEn = listInit.ediEn;

	SetRowAndColumnAndTextSize(listView[0].listType, 0);

	listView[0].onePageListMax = listView[0].lineNum * listView[0].columnNum;
	

	listView[0].listNum = listInit.listCnt;
	listView[0].maxPage = listInit.listCnt/listView[0].onePageListMax + (listInit.listCnt%listView[0].onePageListMax ? 1 : 0);
	listView[0].textBoxWidth = ((listView[0].maxPage > 1) ? (listView[0].textBoxWidth - listView[0].scheduleBotW) : listView[0].textBoxWidth);

	listView[0].lineHeight	= listView[0].textBoxHeight/listView[0].lineNum;
	listView[0].lineWidth = listView[0].textBoxWidth/listView[0].columnNum;

	listView[0].currentPage = listInit.currentPage >= listView[0].maxPage ? 0 : listInit.currentPage;
	
	CreateIcons(listView[0].onePageListMax, 0);

	DisplayOnePage(0);

#if 0	
	listView[0].listViewEnable = 1;
	
	listView[0].titleHeight = MENU_LIST_titleHeight;
	listView[0].titleStrLen = listInit.titleStrLen;
	memcpy(listView[0].titleStr, listInit.titleStr, listView[0].titleStrLen);

	listView[0].boxX =	MENU_LIST_boxX;
	listView[0].boxY =	MENU_LIST_boxY;
	listView[0].boxWidth =	MENU_LIST_boxWidth;
	listView[0].boxHeight = MENU_LIST_boxHeight;

	listView[0].textBoxPositionX =	MENU_LIST_textBoxX;
	listView[0].textBoxPositionY =	MENU_LIST_textBoxY;
	listView[0].textBoxWidth =	MENU_LIST_textBoxWidth;
	listView[0].textBoxHeight = MENU_LIST_textBoxHeight;
	
	listView[0].up.icon = MENU_LIST_ICON_UP;
	listView[0].up.x = MENU_LIST_upX;
	listView[0].up.y = MENU_LIST_upY;
	listView[0].up.w = MENU_LIST_upWidth;
	listView[0].up.h = MENU_LIST_upHeight;
	listView[0].up.iconType = ICON_PIC_UP;
	listView[0].up.revColor = COLOR_LIST_MENU_ICON;
	

	
	listView[0].valAlign = listInit.valAlign;
	listView[0].textAlign = listInit.textAlign;
	
	listView[0].up.fun.x = listView[0].up.x;
	listView[0].up.fun.y = listView[0].up.y;
	listView[0].up.fun.xsize = listView[0].up.w;
	listView[0].up.fun.ysize = listView[0].up.h;
	listView[0].up.fun.spiAlign = ALIGN_LEFT_UP;

	listView[0].down.icon = MENU_LIST_ICON_DOWN;
	listView[0].down.x = MENU_LIST_downX;
	listView[0].down.y = MENU_LIST_downY;
	listView[0].down.w = MENU_LIST_downWidth;
	listView[0].down.h = MENU_LIST_downHeight;
	listView[0].down.iconType = ICON_PIC_DOWN;
	listView[0].down.revColor = COLOR_LIST_MENU_ICON;
	
	
	listView[0].down.fun.x = listView[0].down.x;
	listView[0].down.fun.y = listView[0].down.y;
	listView[0].down.fun.xsize = listView[0].down.w;
	listView[0].down.fun.ysize = listView[0].down.h;
	listView[0].down.fun.spiAlign = ALIGN_LEFT_UP;

	listView[0].scheduleBotW = MENU_LIST_scheduleBotW;
	listView[0].scheduleBotH = MENU_LIST_scheduleBotH;
	listView[0].scheduleTopW = MENU_LIST_scheduleTopW;
	listView[0].scheduleTopH = MENU_LIST_scheduleTopH;
	listView[0].scheduleBotSpriteId = MENU_LIST_ScheduleBottom;
	listView[0].scheduleTopSpriteId = MENU_LIST_ScheduleTop;
	
	listView[0].listType = listInit.listType;

	listView[0].getDisplay = listInit.fun;

    listView[0].textColor = listInit.textColor;											
    listView[0].valColor = listInit.valColor;											
	listView[0].listIconEn = listInit.listIconEn;
	listView[0].valEn = listInit.valEn;
    listView[0].textListOffsetX = listInit.textOffset;
	listView[0].textValOffset = listInit.textValOffset;
	listView[0].selEn = listInit.selEn;
	listView[0].ediEn = listInit.ediEn;

	SetRowAndColumnAndTextSize(listView[0].listType);

	listView[0].onePageListMax = listView[0].lineNum * listView[0].columnNum;
	

	listView[0].listNum = listInit.listCnt;
	listView[0].maxPage = listInit.listCnt/listView[0].onePageListMax + (listInit.listCnt%listView[0].onePageListMax ? 1 : 0);
	listView[0].textBoxWidth = ((listView[0].maxPage > 1) ? (listView[0].textBoxWidth - listView[0].scheduleBotW) : listView[0].textBoxWidth);

	listView[0].lineHeight	= listView[0].textBoxHeight/listView[0].lineNum;
	listView[0].lineWidth = listView[0].textBoxWidth/listView[0].columnNum;

	listView[0].currentPage = listInit.currentPage >= listView[0].maxPage ? 0 : listInit.currentPage;
	
	CreateIcons(listView[0].onePageListMax);

	DisplayOnePage();
#endif	
}

//删除或者增加记录
int AddOrDeleteMenuListItem(int num)
{
	int i;

	listView[0].listNum += num;
	listView[0].maxPage = listView[0].listNum/listView[0].onePageListMax + (listView[0].listNum%listView[0].onePageListMax ? 1 : 0);

	while((listView[0].currentPage >= listView[0].maxPage) && (listView[0].currentPage > 0))
	{
		listView[0].currentPage--;
	}
	
	CreateIcons(listView[0].onePageListMax, 0);

	DisplayOnePage(0);
}

//修改list记录数
int SetMenuListItemNum(int num)
{
	int i;

	listView[0].listNum = num;
	listView[0].maxPage = listView[0].listNum/listView[0].onePageListMax + (listView[0].listNum%listView[0].onePageListMax ? 1 : 0);

	while((listView[0].currentPage >= listView[0].maxPage) && (listView[0].currentPage > 0))
	{
		listView[0].currentPage--;
	}
	
	CreateIcons(listView[0].onePageListMax, 0);

	DisplayOnePage(0);
}

void MenuListDiplayPage(void)
{
	DisplayOnePage(0);
}


int IfMenuListPress(int x, int y, int* id)
{
	int i;
	for(i=0; i<3; i++)
	{
		if(listView[i].listViewEnable)
		{
			if( listView[i].boxX < x && x < listView[i].boxX + listView[i].boxWidth && 
				listView[i].boxY < y && y < listView[i].boxY + listView[i].boxHeight)
			{
				*id = listView[i].listViewId;
				return 1;
			}
		}
	}

	return 0;
}

void MenuListDisable(int id)
{
	listView[id].listViewEnable = 0;
}

int GetMenuListEn(int id)
{
	return listView[id].listViewEnable;
}


void MenuListClear(int id, int alpha, int x, int y, int w, int h)
{
	printf("MenuListClear x[%d] y[%d] w[%d] h[%d]\n", x, y, w, h);	
#if 0
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	InitMenuListXY(listInit, x, y, w, h, 0, 0, ICON_888_ListView);

#else
	API_OsdUpdateDisplay(0, 0, 0, 0, 0, alpha);
	
	//清除string层
	API_ListAlpha_Close(alpha, listView[id].boxX, listView[id].boxY, listView[id].boxWidth,  listView[id].boxHeight, OSD_LAYER_STRING);

	//清除ICON层
	API_ListAlpha_Close(alpha, listView[id].boxX, listView[id].boxY, listView[id].boxWidth,  listView[id].boxHeight, OSD_LAYER_CURSOR);


	API_OsdUpdateDisplay(1, listView[id].boxX, listView[id].boxY, listView[id].boxWidth, listView[id].boxHeight, alpha);
#endif
}



LIST_INIT_T ListPropertyDefault(void)
{
	LIST_INIT_T listInit;
	
	listInit.listType = TEXT_5X1;
	listInit.listIconEn = 1;
	listInit.selEn = 0;
	listInit.ediEn = 0;
	listInit.valEn = 0;
	listInit.textValOffset = 0;
	listInit.textValOffsetY = 0;
	listInit.textOffset = MENU_LIST_YTPE_LIST_STR_OFFSET_X;
	listInit.valColor = MENU_LIST_VAL_COLOR;
	listInit.textColor = MENU_LIST_STR_COLOR;
	listInit.textSize = 1;
	listInit.listCnt = 0;
	listInit.fun = NULL;
	
	listInit.titleColor = COLOR_WHITE;
	listInit.titleSize = 2;
	listInit.titleOffset = MENU_LIST_YTPE_LIST_STR_OFFSET_X;
	
	listInit.titleStr = NULL;
	listInit.titleStrLen = 0;

	listInit.valAlign = ALIGN_LEFT;
	listInit.textAlign = ALIGN_LEFT;
	listInit.currentPage = 0;

	return listInit;
}


//当大ICON按下的处理
LIST_ICON MenuListIconProcess(int x, int y, int iconid)
{
	LIST_ICON icon;

	int i,id;
	for(i=0; i<3; i++)
	{
		if(iconid == listView[i].listViewId)
		{
			id = i;
			break;
		}
	}
	
	icon = MenuListGetIcon(x, y, id);
	
	if(icon.mainIcon != MENU_LIST_ICON_NONE)
	{
		if(KeyBeepDisable==0)
			BEEP_KEY();
		SelectMenuListIcon(1, icon, id);
	}

	return icon;
}

//当大ICON释放的处理
LIST_ICON MenuListIconClick(int x, int y, int iconid)
{
	LIST_ICON icon;
	int i,id;
	for(i=0; i<3; i++)
	{
		if(iconid == listView[i].listViewId)
		{
			id = i;
			break;
		}
	}

	icon = MenuListGetIcon(x, y, id);
	
	SelectMenuListIcon(0, icon, id);

	if(icon.mainIcon == MENU_LIST_ICON_UP)
	{
		MenuListPageUpOrDown(1);
	}
	else if(icon.mainIcon == MENU_LIST_ICON_DOWN)
	{
		MenuListPageUpOrDown(0);
	}

	return icon;
}

void LoadMenuListData(void)
{
    int status = 0;
	int iCnt, deviceNum;
	FILE *file = NULL;
	int len;
	char* json;
	MenuListResTable_T* pListTable;
	char* fileName;

    const cJSON *iconPic = NULL;
    const cJSON *listPic = NULL;
    const cJSON *preSprPic = NULL;
    const cJSON *sufSprPic = NULL;
    const cJSON *bg = NULL;
    const cJSON *pSub = NULL;
	
	pListTable = &listTable;
	memset(pListTable, 0, sizeof(listTable));
	
	fileName = MENU_LIST_TABLE_FILE_NAME;

	if( (file=fopen(fileName,"r")) == NULL )
	{
        goto end;
	}

	fseek(file, 0, SEEK_END);
	len = ftell(file);

	json = malloc(len);
	if(json == NULL)
	{
        goto end;
	}

	fseek(file, 0, SEEK_SET);
	
	fread(json, 1, len, file);	
	

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        dprintf("---------------------------------json Error.\n%s\n", json);
        status = 0;
        goto end;
    }

	iconPic = cJSON_GetObjectItemCaseSensitive(root, "ICON PIC");
	
	if ((iconPic != NULL))
	{
		pListTable->iconPicNum = cJSON_GetArraySize (iconPic);
		
		for( iCnt = 0; iCnt < pListTable->iconPicNum; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(iconPic, iCnt);
			
			if(NULL == pSub ){ continue ; }
		
			ParseJsonNumber(pSub, "TYPE", &pListTable->iconPic[iCnt].iconType);
			ParseJsonNumber(pSub, "SEL_F", &pListTable->iconPic[iCnt].selFpic);
			ParseJsonNumber(pSub, "SEL_B", &pListTable->iconPic[iCnt].selBpic);
			ParseJsonNumber(pSub, "FUN_F", &pListTable->iconPic[iCnt].funFpic);
			ParseJsonNumber(pSub, "FUN_B", &pListTable->iconPic[iCnt].funBpic);
			ParseJsonNumber(pSub, "EDI_F", &pListTable->iconPic[iCnt].ediFpic);
			ParseJsonNumber(pSub, "EDI_B", &pListTable->iconPic[iCnt].ediBpic);

			dprintf("iconPic iCnt=%d, type=%d, %d, %d, %d, %d, %d, %d\n", iCnt, 
				pListTable->iconPic[iCnt].iconType, 
				pListTable->iconPic[iCnt].selFpic, 
				pListTable->iconPic[iCnt].selBpic,
				pListTable->iconPic[iCnt].funFpic,
				pListTable->iconPic[iCnt].funBpic,
				pListTable->iconPic[iCnt].ediFpic,
				pListTable->iconPic[iCnt].ediBpic);
		}
	}
	
	listPic = cJSON_GetObjectItemCaseSensitive(root, "LIST PIC");
	
	if ((listPic != NULL))
	{
		pListTable->listPicNum = cJSON_GetArraySize (listPic);
		
		for( iCnt = 0; iCnt < pListTable->listPicNum; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(listPic, iCnt);
			
			if(NULL == pSub ){ continue ; }
		
			ParseJsonNumber(pSub, "TYPE", &pListTable->listPic[iCnt].iconType);
			ParseJsonNumber(pSub, "SEL_F", &pListTable->listPic[iCnt].selFpic);
			ParseJsonNumber(pSub, "SEL_B", &pListTable->listPic[iCnt].selBpic);
			ParseJsonNumber(pSub, "FUN_F", &pListTable->listPic[iCnt].funFpic);
			ParseJsonNumber(pSub, "FUN_B", &pListTable->listPic[iCnt].funBpic);
			ParseJsonNumber(pSub, "EDI_F", &pListTable->listPic[iCnt].ediFpic);
			ParseJsonNumber(pSub, "EDI_B", &pListTable->listPic[iCnt].ediBpic);

			dprintf("listPic iCnt=%d, type=%d, %d, %d, %d, %d, %d, %d\n", iCnt, 
				pListTable->listPic[iCnt].iconType, 
				pListTable->listPic[iCnt].selFpic, 
				pListTable->listPic[iCnt].selBpic,
				pListTable->listPic[iCnt].funFpic,
				pListTable->listPic[iCnt].funBpic,
				pListTable->listPic[iCnt].ediFpic,
				pListTable->listPic[iCnt].ediBpic);
		}
	}

	preSprPic = cJSON_GetObjectItemCaseSensitive(root, "PRE SPRITE");
	
	if ((preSprPic != NULL))
	{
		pListTable->preSprNum = cJSON_GetArraySize (preSprPic);
		
		for( iCnt = 0; iCnt < pListTable->preSprNum; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(preSprPic, iCnt);
			
			if(NULL == pSub ){ continue ; }
		
			ParseJsonNumber(pSub, "TYPE", &pListTable->preSpr[iCnt].sprType);
			ParseJsonNumber(pSub, "SPRITE", &pListTable->preSpr[iCnt].sprId);

			dprintf("preSprPic iCnt=%d, type=%d, %d\n", iCnt, 
				pListTable->preSpr[iCnt].sprType, 
				pListTable->preSpr[iCnt].sprId);
		}
	}
	
	sufSprPic = cJSON_GetObjectItemCaseSensitive(root, "SUF SPRITE");
	
	if ((sufSprPic != NULL))
	{
		pListTable->sufSprNum = cJSON_GetArraySize (sufSprPic);
		
		for( iCnt = 0; iCnt < pListTable->sufSprNum; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(sufSprPic, iCnt);
			
			if(NULL == pSub ){ continue ; }
		
			ParseJsonNumber(pSub, "TYPE", &pListTable->sufSpr[iCnt].sprType);
			ParseJsonNumber(pSub, "SPRITE", &pListTable->sufSpr[iCnt].sprId);
			
			dprintf("sufSprPic iCnt=%d, type=%d, %d\n", iCnt, 
				pListTable->sufSpr[iCnt].sprType, 
				pListTable->sufSpr[iCnt].sprId);
		}
	}
	
	bg = cJSON_GetObjectItemCaseSensitive(root, "BACK BROUND");
	
	if ((bg != NULL))
	{
		pListTable->bgNum = cJSON_GetArraySize (bg);
		
		for( iCnt = 0; iCnt < pListTable->bgNum; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(bg, iCnt);
			
			if(NULL == pSub ){ continue ; }
		
			ParseJsonNumber(pSub, "TYPE", &pListTable->bg[iCnt].bgType);
			ParseJsonNumber(pSub, "BG", &pListTable->bg[iCnt].bgId);

			dprintf("bg iCnt=%d, type=%d, %d\n", iCnt, 
				pListTable->bg[iCnt].bgType, 
				pListTable->bg[iCnt].bgId);
		}
	}
	
    status = 1;
	
	
end:

	if(file)
	{
		fclose(file);
	}

	if(json)
	{
		free(json);
	}

	if(root)
	{
		cJSON_Delete(root);
	}
	
    return status;
}

static void SetRowAndColumnAndTextSize(LIST_TYPE listType, int id)
{
	switch(listType)
	{
		case ICON_1X1:
			listView[id].lineNum = 1;
			listView[id].columnNum = 1;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_1X2:
			listView[id].lineNum = 1;
			listView[id].columnNum = 2;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_1X3:
			listView[id].lineNum = 1;
			listView[id].columnNum = 3;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_1X4:
			listView[id].lineNum = 1;
			listView[id].columnNum = 4;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_1X5:
			listView[id].lineNum = 1;
			listView[id].columnNum = 5;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_1X6:
			listView[id].lineNum = 1;
			listView[id].columnNum = 6;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_4X1:
			listView[id].lineNum = 4;
			listView[id].columnNum = 1;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_2X2:
			listView[id].lineNum = 2;
			listView[id].columnNum = 2;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case ICON_2X3:
			listView[id].lineNum = 2;
			listView[id].columnNum = 3;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
	    case ICON_2X4:
			listView[id].lineNum = 2;
			listView[id].columnNum = 4;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_ICON;		
			break;
	    case ICON_4X2:
			listView[id].lineNum = 4;
			listView[id].columnNum = 2;
			listView[id].dispType = ICON_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case TEXT_5X1:
			listView[id].lineNum = 5;
			listView[id].columnNum = 1;
			listView[id].textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listView[id].dispType = TEXT_TYPE;		
			listView[id].bg = BACK_GROUND1_List5_1;		
			break;
		case TEXT_5X2:
			listView[id].lineNum = 5;
			listView[id].columnNum = 2;
			listView[id].textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listView[id].dispType = TEXT_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
		case TEXT_6X1:
			listView[id].lineNum = 6;
			listView[id].columnNum = 1;
			listView[id].textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listView[id].dispType = TEXT_TYPE;		
			listView[id].bg = BACK_GROUND2_List6_1;		
			break;
		case TEXT_10X1:
			listView[id].lineNum = 10;
			listView[id].columnNum = 1;
			listView[id].textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listView[id].dispType = TEXT_TYPE;		
			listView[id].bg = BACK_GROUND3_List10_1;		
			break;
		default:
			listView[id].lineNum = 5;
			listView[id].columnNum = 1;
			listView[id].textSize = MENU_LIST_TEXT_SIZE_BIG;		
			listView[id].dispType = TEXT_TYPE;		
			listView[id].bg = BACK_GROUND_NONE;		
			break;
	}
}


static void CreateIcons(int onePageListMax, int id)
{
	int i;
	
	for(i = 0, listView[id].iconListCnt = 0; i < onePageListMax; i++)
	{
		listView[id].iconList[listView[id].iconListCnt++] = i;
		
		memset(&listView[id].list[i], 0, sizeof(listView[id].list[i]));

		listView[id].list[i].iconType = ICON_PIC_NONE;
		listView[id].list[i].revColor = MENU_LIST_REV_COLOR;
		listView[id].list[i].icon = i;
		listView[id].list[i].x = listView[id].textBoxPositionX + listView[id].lineWidth*(i%listView[id].columnNum);
		listView[id].list[i].y = listView[id].textBoxPositionY + listView[id].lineHeight*(i/listView[id].columnNum);
		listView[id].list[i].w = listView[id].lineWidth;
		listView[id].list[i].h = listView[id].lineHeight;

		if(listView[id].dispType == ICON_TYPE)
		{
			listView[id].list[i].sel.x = listView[id].list[i].x;
			listView[id].list[i].sel.y = listView[id].list[i].y;
			listView[id].list[i].sel.xsize = (listView[id].selEn ? GetIconTypeSelH(listView[id].list[i].h) : 0);
			listView[id].list[i].sel.ysize = listView[id].list[i].sel.xsize;
			
			listView[id].list[i].sel.spiAlign = ALIGN_MIDDLE;
			
			//listView[id].list[i].edi.xsize = (listView[id].selEn ? GetIconTypeSelH(listView[id].list[i].h) : 0);
			//listView[id].list[i].edi.ysize = listView[id].list[i].edi.xsize;
			//listView[id].list[i].edi.x = listView[id].list[i].x + listView[id].list[i].w -listView[id].list[i].edi.xsize;
			//listView[id].list[i].edi.y = listView[id].list[i].y;
			listView[id].list[i].edi.xsize = (listView[id].selEn ? listView[id].list[i].w : 0);
			listView[id].list[i].edi.ysize = listView[id].list[i].h - (listView[id].list[i].h*7/10);
			listView[id].list[i].edi.x = listView[id].list[i].x;
			listView[id].list[i].edi.y = listView[id].list[i].y + (listView[id].list[i].h*7/10);
			
			listView[id].list[i].edi.spiAlign = ALIGN_MIDDLE;
			
			listView[id].list[i].fun.x = listView[id].list[i].x;
			listView[id].list[i].fun.y = listView[id].list[i].y;
			listView[id].list[i].fun.xsize = listView[id].list[i].w;
			listView[id].list[i].fun.ysize = listView[id].list[i].h;
			
			listView[id].list[i].fun.spiAlign = ALIGN_MIDDLE;

			listView[id].list[i].fun.strx = listView[id].list[i].fun.x;
			listView[id].list[i].fun.stry = listView[id].list[i].fun.y + listView[id].textListOffsetX;
			listView[id].list[i].fun.strAlign = listView[id].textAlign;
			listView[id].list[i].fun.strw = listView[id].list[i].fun.xsize;
			listView[id].list[i].fun.strh = listView[id].list[i].fun.ysize - listView[id].textListOffsetX;
		}
		else if(listView[id].dispType == TEXT_TYPE)
		{
			listView[id].list[i].sel.xsize = (listView[id].selEn ? listView[id].list[i].h : 0);
			listView[id].list[i].sel.ysize = listView[id].list[i].sel.xsize;
			listView[id].list[i].sel.x = listView[id].list[i].x;
			listView[id].list[i].sel.y = listView[id].list[i].y;
			listView[id].list[i].sel.spiAlign = ALIGN_MIDDLE;
			
			listView[id].list[i].edi.xsize = (listView[id].ediEn ? listView[id].list[i].h : 0);
			listView[id].list[i].edi.ysize = listView[id].list[i].edi.xsize;
			listView[id].list[i].edi.x = listView[id].list[i].x + listView[id].list[i].w - listView[id].list[i].edi.xsize;
			listView[id].list[i].edi.y = listView[id].list[i].y;
			listView[id].list[i].edi.spiAlign = ALIGN_MIDDLE;


			listView[id].list[i].fun.x = listView[id].list[i].x + listView[id].list[i].sel.xsize;
			listView[id].list[i].fun.y = listView[id].list[i].y;
			listView[id].list[i].fun.xsize = listView[id].list[i].w - listView[id].list[i].sel.xsize - listView[id].list[i].edi.xsize;
			listView[id].list[i].fun.ysize = listView[id].list[i].h;
			

			listView[id].list[i].fun.spiAlign = ALIGN_LEFT;

			listView[id].textValOffset = (listView[id].valEn ? listView[id].textValOffset : listView[id].list[i].fun.xsize);

			listView[id].list[i].fun.strx = listView[id].list[i].fun.x + listView[id].textListOffsetX;
			listView[id].list[i].fun.stry = listView[id].list[i].fun.y;
			listView[id].list[i].fun.strAlign = listView[id].textAlign;
			//listView[id].list[i].fun.strw = listView[id].list[i].fun.x + listView[id].textValOffset - listView[id].list[i].fun.strx;
			listView[id].list[i].fun.strw = listView[id].list[i].fun.xsize;
			listView[id].list[i].fun.strh = listView[id].list[i].fun.ysize;

			listView[id].list[i].fun.vlax = listView[id].list[i].fun.x + listView[id].textValOffset;
			listView[id].list[i].fun.vlay = listView[id].list[i].fun.y + listView[id].textValOffsetY;
			listView[id].list[i].fun.vlaw = listView[id].list[i].edi.x - listView[id].list[i].fun.vlax;
			listView[id].list[i].fun.vlah = listView[id].list[i].fun.ysize;
			listView[id].list[i].fun.vlaAlign = listView[id].valAlign;
		}
	}
	
	if(listView[id].maxPage > 1)
	{
		listView[id].iconList[listView[id].iconListCnt++] = MENU_LIST_ICON_UP;
		listView[id].iconList[listView[id].iconListCnt++] = MENU_LIST_ICON_DOWN;
	}
}


//获取Icon号
static LIST_ICON MenuListGetIcon(int x, int y, int id)
{
	LIST_ICON_PROPERTY_T iconProperty;
	LIST_ICON icon;
	int i;
	int posx,posy;
	posx = y;
	posy = x;
	
	for(i = 0; i < listView[id].iconListCnt; i++)
	{
		icon.mainIcon = listView[id].iconList[i];
		
		iconProperty = GetOneIconProperty(icon.mainIcon, id);
		
		if(iconProperty.x < posx && posx < iconProperty.x + iconProperty.w && 
			iconProperty.y < posy && posy < iconProperty.y + iconProperty.h)
		{
			if(iconProperty.sel.x < posx && posx < iconProperty.sel.x + iconProperty.sel.xsize && 
					iconProperty.sel.y < posy && posy < iconProperty.sel.y + iconProperty.sel.ysize)
			{
				icon.subIcon = MENU_LIST_SUB_ICON_SEL;
			}
			else if(iconProperty.edi.x < posx && posx < iconProperty.edi.x + iconProperty.edi.xsize && 
						iconProperty.edi.y < posy && posy < iconProperty.edi.y + iconProperty.edi.ysize)
			{
				icon.subIcon = MENU_LIST_SUB_ICON_EDI;
			}
			else
			{
				icon.subIcon = MENU_LIST_SUB_ICON_FUN;
			}

			if(icon.mainIcon >= 0)
			{
				icon.mainIcon = listView[id].currentPage * listView[id].onePageListMax + icon.mainIcon;
				if((icon.mainIcon >= listView[id].listNum) || (!listView[id].listIconEn))
				{
					icon.mainIcon = MENU_LIST_ICON_NONE;
					break;
				}
			}
			break;
		}
	}

	if(i == listView[id].iconListCnt)
	{
		icon.mainIcon = MENU_LIST_ICON_NONE;
	}
	
	dprintf("x=%d, y=%d, icon.mainIcon=%d, icon.subIcon=%d\n", posx, posy, icon.mainIcon, icon.subIcon);
	
	return icon;
}

//跳转到指定页数
int MenuListSetCurrentPage(int page)
{
	if(page >= 0 && page*listView[0].onePageListMax <= listView[0].listNum-1)
	{
		listView[0].currentPage = page;
		DisplayOnePage(0);
	}
	
	return listView[0].currentPage;
}

//获取当前页
int MenuListGetCurrentPage(void)
{
	return listView[0].currentPage;
}

//翻页option 0-下翻， 1-上翻
void MenuListPageUpOrDown(int option)
{
	//上翻
	if(option)
	{
		if(listView[0].currentPage)
		{
			listView[0].currentPage--;
		}
		else
		{
			listView[0].currentPage = (listView[0].listNum ? (listView[0].listNum-1)/listView[0].onePageListMax : 0);
		}
	}
	else
	{
		if((++listView[0].currentPage)*listView[0].onePageListMax > listView[0].listNum-1)
		{
			listView[0].currentPage = 0;
		}
	}
	
	MenuListSetCurrentPage(listView[0].currentPage);
}

//显示菜单列表
static void DisplayOnePage(int id)
{
	LIST_DISP_T display = {0};

	if(listView[id].getDisplay != NULL)
	{
		(listView[id].getDisplay)(listView[id].currentPage, listView[id].onePageListMax, &display);
		if(display.dispCnt > listView[0].listNum - listView[0].currentPage*listView[0].onePageListMax)
		{
			display.dispCnt = listView[0].listNum - listView[0].currentPage*listView[0].onePageListMax;
		}
	}

	if(listView[id].listViewAlpha)
	{
		API_OsdUpdateDisplay(0, 0, 0, 0, 0, listView[id].listViewAlpha);
		DisplayListIcon(display, id);
		API_OsdUpdateDisplay(1, listView[id].boxX, listView[id].boxY, listView[id].boxWidth, listView[id].boxHeight, listView[id].listViewAlpha);
	}
	else
	{
		API_OsdUpdateDisplay(0, 0, 0, 0, 0, 0);
		
		//清除背景层
		if(listView[id].bg != BACK_GROUND_NONE && GetPicByBgType(listView[id].bg) != NONE_ICON_PIC)
		{
			API_OsdBackGroundDisplay(listView[id].textBoxPositionX, listView[id].textBoxPositionY, listView[id].textBoxWidth, listView[id].textBoxHeight, ALIGN_MIDDLE, GetPicByBgType(listView[id].bg));
		}
	
	
		//清除string层
		API_OsdStringClearExt2(listView[id].boxX, listView[id].boxY, OSD_LAYER_STRING, listView[id].boxWidth,  listView[id].boxHeight);
	
		//清除ICON层
		API_OsdStringClearExt2(listView[id].boxX, listView[id].boxY, OSD_LAYER_CURSOR, listView[id].boxWidth,  listView[id].boxHeight);
	
		//显示标题
		DisplayTitle();
		DisplayPageNum();
		DisplayTurnPageIcon();
		DisplayListIcon(display, id);
	
		API_OsdUpdateDisplay(1, listView[id].boxX, listView[id].boxY, listView[id].boxWidth, listView[id].boxHeight, 0);
	}
	
}


static int IconIfExist(int mainIcon, int id)
{
	int i, ret;

	for(i = 0, ret = 0; i < listView[id].iconListCnt; i++)
	{
		if(mainIcon == listView[id].iconList[i])
		{
			ret = 1;
			break;
		}
	}

	return ret;
}

static void DisplayTitle(void)
{
	MenuStrMulti_T list;
	
	list.dataCnt = 0;
	if(listView[0].titleStrLen > 0)
	{
		list.data[list.dataCnt].x = listView[0].boxX + listView[0].titleOffset;
		list.data[list.dataCnt].y = listView[0].boxY;
		list.data[list.dataCnt].fg = listView[0].titleColor;
		list.data[list.dataCnt].uBgColor = COLOR_KEY;
		list.data[list.dataCnt].format = STR_UNICODE;
		list.data[list.dataCnt].width = listView[0].up.x - listView[0].boxX;
		list.data[list.dataCnt].height = listView[0].titleHeight;
		list.data[list.dataCnt].fnt_type = listView[0].titleSize;
		list.data[list.dataCnt].align = ALIGN_LEFT;
		list.data[list.dataCnt].str_len = listView[0].titleStrLen;
		memcpy(list.data[list.dataCnt].str_dat, listView[0].titleStr, list.data[list.dataCnt].str_len);
		list.dataCnt++;
	}
	
	if(list.dataCnt)
	{
		API_OsdStringDisplayExtMultiLine(list);
	}
}

static void DisplayTurnPageIcon(void)
{
	int i;
	ICON_TYPE_TO_PIC iconToPic;
	LIST_ICON icon;
	LIST_ICON_PROPERTY_T iconProperty;
	MenuSpriteMulti_T sprite;

	for(i = 0, sprite.dataCnt = 0; i < 2; i++)
	{
		icon.mainIcon = ((i == 0) ? MENU_LIST_ICON_UP : MENU_LIST_ICON_DOWN);
		
		if(IconIfExist(icon.mainIcon, 0))
		{
			iconProperty = GetOneIconProperty(icon.mainIcon, 0);
			
			iconToPic = GetPicByIconType(iconProperty.iconType, 0);
			dprintf("mainIcon=%d, funFpic=%d, iconType=%d\n", icon.mainIcon, iconToPic.funFpic, iconProperty.iconType);
			if(iconToPic.funFpic != NONE_ICON_PIC)
			{
				sprite.data[sprite.dataCnt].x = iconProperty.fun.x;
				sprite.data[sprite.dataCnt].y = iconProperty.fun.y;
				sprite.data[sprite.dataCnt].width = iconProperty.fun.xsize;
				sprite.data[sprite.dataCnt].height = iconProperty.fun.ysize;
				sprite.data[sprite.dataCnt].spriteId = iconToPic.funFpic;
				sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
				sprite.data[sprite.dataCnt].align = iconProperty.fun.spiAlign;
				sprite.data[sprite.dataCnt].enable = 1;
				sprite.dataCnt++;
			}
		}
	}
	if(sprite.dataCnt)
	{
		API_OsdSpriteDisplayMulti(sprite);
	}
}

static void SetListIconType(int mainIcon, ICON_PIC_TYPE iconType, int id)
{
	int i;

	for(i = 0; i < listView[id].onePageListMax; i++)
	{
		if(listView[id].list[i].icon == mainIcon)
		{
			listView[id].list[i].iconType = iconType;
			break;
		}
	}
}


static void DisplayListIcon(LIST_DISP_T display, int id)
{
	int i;
	ICON_TYPE_TO_PIC iconToPic;
	LIST_ICON icon;
	LIST_ICON_PROPERTY_T iconProperty;
	MenuStrMulti_T list, listValue;
	MenuSpriteMulti_T preSpr, sufSpr, selIconSpr, ediIconSpr, funIconSpr;
	
	list.dataCnt = 0;
	listValue.dataCnt = 0;
	
	preSpr.dataCnt = 0;
	sufSpr.dataCnt = 0;
	selIconSpr.dataCnt = 0;
	ediIconSpr.dataCnt = 0;
	funIconSpr.dataCnt = 0;

	for(i = 0; i < display.dispCnt; i++)
	{
		icon.mainIcon = i;
		
		if(!IconIfExist(icon.mainIcon, id))
		{
			continue;
		}
		
		SetListIconType(icon.mainIcon, display.disp[i].iconType, id);
		
		iconProperty = GetOneIconProperty(icon.mainIcon, id);
		
		iconToPic = GetPicByIconType(iconProperty.iconType, id);
		
		if(iconToPic.funFpic != NONE_ICON_PIC)
		{
			funIconSpr.data[funIconSpr.dataCnt].x = iconProperty.fun.x;
			funIconSpr.data[funIconSpr.dataCnt].y = iconProperty.fun.y;
			funIconSpr.data[funIconSpr.dataCnt].width = iconProperty.fun.xsize;
			funIconSpr.data[funIconSpr.dataCnt].height = iconProperty.fun.ysize;
			funIconSpr.data[funIconSpr.dataCnt].spriteId = iconToPic.funFpic;
			funIconSpr.data[funIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			funIconSpr.data[funIconSpr.dataCnt].align = iconProperty.fun.spiAlign;
			funIconSpr.data[funIconSpr.dataCnt].enable = 1;
			funIconSpr.data[funIconSpr.dataCnt].alpha = listView[id].listViewAlpha;
			funIconSpr.dataCnt++;
		}
		
		if(iconToPic.ediFpic != NONE_ICON_PIC)
		{
			ediIconSpr.data[ediIconSpr.dataCnt].x = iconProperty.edi.x;
			ediIconSpr.data[ediIconSpr.dataCnt].y = iconProperty.edi.y;
			ediIconSpr.data[ediIconSpr.dataCnt].width = iconProperty.edi.xsize;
			ediIconSpr.data[ediIconSpr.dataCnt].height = iconProperty.edi.ysize;
			ediIconSpr.data[ediIconSpr.dataCnt].spriteId = iconToPic.ediFpic;
			ediIconSpr.data[ediIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			ediIconSpr.data[ediIconSpr.dataCnt].align = iconProperty.edi.spiAlign;
			ediIconSpr.data[ediIconSpr.dataCnt].enable = 1;
			ediIconSpr.dataCnt++;
		}
		
		if(iconToPic.selFpic != NONE_ICON_PIC)
		{
			selIconSpr.data[selIconSpr.dataCnt].x = iconProperty.sel.x;
			selIconSpr.data[selIconSpr.dataCnt].y = iconProperty.sel.y;
			selIconSpr.data[selIconSpr.dataCnt].width = iconProperty.sel.xsize;
			selIconSpr.data[selIconSpr.dataCnt].height = iconProperty.sel.ysize;
			selIconSpr.data[selIconSpr.dataCnt].spriteId = iconToPic.selFpic;
			selIconSpr.data[selIconSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
			selIconSpr.data[selIconSpr.dataCnt].align = iconProperty.sel.spiAlign;
			selIconSpr.data[selIconSpr.dataCnt].enable = 1;
			selIconSpr.dataCnt++;
		}
			
		if(display.disp[i].strLen != 0)
		{
			list.data[list.dataCnt].x = iconProperty.fun.strx;
			list.data[list.dataCnt].y = iconProperty.fun.stry;
			list.data[list.dataCnt].fg = listView[id].textColor;
			list.data[list.dataCnt].uBgColor = COLOR_KEY;
			list.data[list.dataCnt].format = STR_UNICODE;
			list.data[list.dataCnt].width = iconProperty.fun.strw;
			list.data[list.dataCnt].height = iconProperty.fun.strh;
			list.data[list.dataCnt].fnt_type = listView[id].textSize;
			list.data[list.dataCnt].align = iconProperty.fun.strAlign;
			list.data[list.dataCnt].str_len = display.disp[i].strLen;
			memcpy(list.data[list.dataCnt].str_dat, display.disp[i].str, display.disp[i].strLen);
			list.dataCnt++;
		}

		if(listView[id].valEn)
		{
			if(display.disp[i].valLen != 0)
			{
				listValue.data[listValue.dataCnt].x = iconProperty.fun.vlax;
				listValue.data[listValue.dataCnt].y = iconProperty.fun.vlay;
				listValue.data[listValue.dataCnt].fg = listView[id].valColor;
				listValue.data[listValue.dataCnt].uBgColor = COLOR_KEY;
				listValue.data[listValue.dataCnt].format = STR_UNICODE;
				listValue.data[listValue.dataCnt].width = iconProperty.fun.vlaw;
				listValue.data[listValue.dataCnt].height = iconProperty.fun.vlah;
				listValue.data[listValue.dataCnt].fnt_type = listView[id].textSize;
				listValue.data[listValue.dataCnt].align = iconProperty.fun.vlaAlign;

				listValue.data[listValue.dataCnt].str_len = display.disp[i].valLen;
				memcpy(listValue.data[listValue.dataCnt].str_dat, display.disp[i].val, display.disp[i].valLen);
				listValue.dataCnt++;
			}
		}
		
		if(listView[id].selEn)
		{
			if(display.disp[i].preSprType != PRE_SPR_NONE)
			{
				preSpr.data[preSpr.dataCnt].x = iconProperty.sel.x;
				preSpr.data[preSpr.dataCnt].y = iconProperty.sel.y;
				preSpr.data[preSpr.dataCnt].width = iconProperty.sel.xsize;
				preSpr.data[preSpr.dataCnt].height = iconProperty.sel.ysize;
				preSpr.data[preSpr.dataCnt].spriteId = GetPicByPreSprType(display.disp[i].preSprType);
				preSpr.data[preSpr.dataCnt].layerId = OSD_LAYER_CURSOR;
				preSpr.data[preSpr.dataCnt].align = iconProperty.sel.spiAlign;
				preSpr.data[preSpr.dataCnt].enable = 1;
				preSpr.dataCnt++;
			}
		}
		
		if(listView[id].ediEn)
		{
			if(display.disp[i].sufSprType != SUF_SPR_NONE)
			{
				sufSpr.data[sufSpr.dataCnt].x = iconProperty.edi.x;
				sufSpr.data[sufSpr.dataCnt].y = iconProperty.edi.y;
				sufSpr.data[sufSpr.dataCnt].width = iconProperty.edi.xsize;
				sufSpr.data[sufSpr.dataCnt].height = iconProperty.edi.ysize;
				sufSpr.data[sufSpr.dataCnt].spriteId = GetPicBySufSprType(display.disp[i].sufSprType);
				sufSpr.data[sufSpr.dataCnt].layerId = OSD_LAYER_STRING;
				sufSpr.data[sufSpr.dataCnt].align = iconProperty.edi.spiAlign;
				sufSpr.data[sufSpr.dataCnt].enable = 1;
				sufSpr.dataCnt++;
			}
		}
	}
	
	if(selIconSpr.dataCnt)
	{
		dprintf("selIconSpr.dataCnt = %d, \n", selIconSpr.dataCnt);
		API_OsdSpriteDisplayMulti(selIconSpr);
	}
	
	if(ediIconSpr.dataCnt)
	{
		dprintf("ediIconSpr.dataCnt = %d, \n", ediIconSpr.dataCnt);
		API_OsdSpriteDisplayMulti(ediIconSpr);
	}
	
	if(funIconSpr.dataCnt)
	{
		dprintf("funIconSpr.dataCnt = %d, Alpha=%d\n", funIconSpr.dataCnt, listView[id].listViewAlpha);
		API_OsdSpriteDisplayMulti(funIconSpr);
	}
		
	if(preSpr.dataCnt)
	{
		dprintf("preSpr.dataCnt = %d, spriteId = %d\n", preSpr.dataCnt, preSpr.data[0].spriteId);
		API_OsdSpriteDisplayMulti(preSpr);
	}
	
	if(sufSpr.dataCnt)
	{
		dprintf("sufSpr.dataCnt = %d, \n", sufSpr.dataCnt);
		API_OsdSpriteDisplayMulti(sufSpr);
	}



	if(list.dataCnt)
	{
		dprintf("list.dataCnt = %d, \n", list.dataCnt);
		API_OsdStringDisplayExtMultiLine(list);
	}
	

	if(listValue.dataCnt)
	{
		dprintf("listValue.dataCnt = %d, \n", listValue.dataCnt);
		API_OsdStringDisplayExtMultiLine(listValue);
	}
}



//取消/选中菜单列表
int SelectMenuListIcon(int enable, LIST_ICON icon, int id)
{
	LIST_ICON_PROPERTY_T iconProperty;
	int color, spriteId, xColor, yColor, xSpr, ySpr, xsize, ysize, align;
	ICON_TYPE_TO_PIC iconToPic;

	iconProperty = GetOneIconProperty(icon.mainIcon, id);
	iconToPic = GetPicByIconType(iconProperty.iconType, id);
	color = enable ? iconProperty.revColor : COLOR_KEY;

	if(icon.subIcon == MENU_LIST_SUB_ICON_SEL)
	{
		xColor = iconProperty.sel.x;
		yColor = iconProperty.sel.y;
		xsize = iconProperty.sel.xsize;
		ysize = iconProperty.sel.ysize;
		xSpr = iconProperty.sel.x;
		ySpr = iconProperty.sel.y;
		align = iconProperty.sel.spiAlign;
		
		spriteId = enable ? iconToPic.selBpic : iconToPic.selFpic;
	}
	else if(icon.subIcon == MENU_LIST_SUB_ICON_EDI)
	{
		xColor = iconProperty.edi.x;
		yColor = iconProperty.edi.y;
		xsize = iconProperty.edi.xsize;
		ysize = iconProperty.edi.ysize;
		
		xSpr = iconProperty.edi.x;
		ySpr = iconProperty.edi.y;
		align = iconProperty.edi.spiAlign;

		spriteId = enable ? iconToPic.ediBpic : iconToPic.ediFpic;
	}
	else
	{
		xColor = iconProperty.fun.x;
		yColor = iconProperty.fun.y;
		xsize = iconProperty.fun.xsize;
		ysize = iconProperty.fun.ysize;
		
		xSpr = iconProperty.fun.x;
		ySpr = iconProperty.fun.y;
		align = iconProperty.fun.spiAlign;
		
		spriteId = enable ? iconToPic.funBpic : iconToPic.funFpic;
	}
	if(listView[id].listViewAlpha)
	{
		if( get_pane_type() != 5 )
		{
			API_OsdUpdateDisplay(0, 0, 0, 0, 0, listView[id].listViewAlpha);
			//dprintf("SelectMenuListIcon  sel=%d, spriteId=%d\n", enable, spriteId);
			if(enable == 0)
				API_ListAlpha_Close(listView[id].listViewAlpha, xSpr, ySpr, xsize, ysize, OSD_LAYER_CURSOR);	
			if(spriteId != NONE_ICON_PIC)
			{
				API_OsdIconPicDisplay(xSpr, ySpr, xsize, ysize, align, spriteId, listView[id].listViewAlpha);
			}
			else
			{
				API_OsdCursorListIconColor(xColor, yColor, xsize, ysize, color, 1, listView[id].listViewAlpha);
			}
			API_OsdUpdateDisplay(1, xColor, yColor, xsize, ysize, listView[id].listViewAlpha);
		}
		
	}
	else
	{
		API_OsdUpdateDisplay(0, 0, 0, 0, 0, 0);
		
		API_OsdStringClearExt2(xColor, yColor, OSD_LAYER_CURSOR, xsize,  ysize);
		
		if(spriteId != NONE_ICON_PIC)
		{
			API_OsdIconPicDisplay(xSpr, ySpr, xsize, ysize, align, spriteId, 0);
		}
		else
		{
			API_OsdCursorListIconColor(xColor, yColor, xsize, ysize, color, 1, 0);
		}
		
		API_OsdUpdateDisplay(1, xColor, yColor, xsize, ysize, 0);
	}
	
	return 0;
}

static void DisplayPageNum(void)
{
	char display[20];
	int botx, boty, topx, topy;
	int topMoveLen;
	MenuSpriteMulti_T sprite;
	MenuStrMulti_T list;
	
		
	if(listView[0].maxPage > 1)
	{
		topMoveLen = listView[0].scheduleBotH - listView[0].scheduleTopW*2 - listView[0].scheduleTopH;

		//botx = listView[0].textBoxPositionX + listView[0].textBoxWidth + listView[0].scheduleBotW/2;
		//boty = listView[0].textBoxPositionY + listView[0].textBoxHeight/2;
		//topx = botx;
		//topy = (boty - topMoveLen/2) + topMoveLen * listView[0].currentPage/(listView[0].maxPage - 1);

		botx = listView[0].textBoxPositionX + listView[0].textBoxWidth;
		boty = listView[0].textBoxPositionY + (listView[0].textBoxHeight - listView[0].scheduleBotH)/2;
		topx = botx + (listView[0].scheduleBotW - listView[0].scheduleTopW)/2;
		topy = (boty + listView[0].scheduleTopW) + topMoveLen * listView[0].currentPage/(listView[0].maxPage - 1);

		//dprintf("DisplayPageNum topMoveLen=%d  botx=%d boty=%d topx=%d topy=%d\n", topMoveLen, botx, boty, topx, topy);
		sprite.dataCnt = 0;
		if( get_pane_type() != 5 &&Get_ScheduleDisable()==0 )
		{
			sprite.data[sprite.dataCnt].x = botx;
			sprite.data[sprite.dataCnt].y = boty;
			sprite.data[sprite.dataCnt].width = listView[0].scheduleBotW;
			sprite.data[sprite.dataCnt].height = listView[0].scheduleBotH;
			sprite.data[sprite.dataCnt].spriteId = listView[0].scheduleBotSpriteId;
			sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
			sprite.data[sprite.dataCnt].align = ALIGN_LEFT_UP;
			sprite.data[sprite.dataCnt].enable = 1;
			sprite.dataCnt++;
			
			sprite.data[sprite.dataCnt].x = topx;
			sprite.data[sprite.dataCnt].y = topy;
			sprite.data[sprite.dataCnt].width = listView[0].scheduleTopW;
			sprite.data[sprite.dataCnt].height = listView[0].scheduleTopH;
			sprite.data[sprite.dataCnt].spriteId = listView[0].scheduleTopSpriteId;
			sprite.data[sprite.dataCnt].layerId = OSD_LAYER_CURSOR;
			sprite.data[sprite.dataCnt].align = ALIGN_LEFT_UP;
			sprite.data[sprite.dataCnt].enable = 1;
			sprite.dataCnt++;
		}
		

		if(sprite.dataCnt)
		{
			API_OsdSpriteDisplayMulti(sprite);
		}

		
		sprintf(display, "%d/%d", listView[0].currentPage+1, listView[0].maxPage);
		
		list.dataCnt = 0;
		list.data[list.dataCnt].x = listView[0].up.x;
		list.data[list.dataCnt].y = listView[0].boxY;
		list.data[list.dataCnt].fg = COLOR_WHITE;
		list.data[list.dataCnt].uBgColor = COLOR_KEY;
		list.data[list.dataCnt].format = STR_UTF8;
		list.data[list.dataCnt].width = listView[0].up.w + listView[0].down.w;
		list.data[list.dataCnt].height = listView[0].titleHeight;
		list.data[list.dataCnt].fnt_type = listView[0].textSize;
		list.data[list.dataCnt].align = ALIGN_MIDDLE;
		
		list.data[list.dataCnt].str_len = strlen(display);
		memcpy(list.data[list.dataCnt].str_dat, display, list.data[list.dataCnt].str_len);
		list.dataCnt++;
		
		if(list.dataCnt)
		{
			API_OsdStringDisplayExtMultiLine(list);
		}
	}
}


//获取Icon属性
static LIST_ICON_PROPERTY_T GetOneIconProperty(int mainIcon, int id)
{
	LIST_ICON_PROPERTY_T item = {0};

	if(mainIcon == MENU_LIST_ICON_UP)
	{
		item = listView[id].up;
	}
	else if(mainIcon == MENU_LIST_ICON_DOWN)
	{
		item = listView[id].down;
	}
	else if(mainIcon >= 0)
	{
		item = listView[id].list[mainIcon%listView[id].onePageListMax];
	}

	return item;
}

static ICON_TYPE_TO_PIC GetPicByIconType(ICON_PIC_TYPE iconType, int id)
{
	int i, typeToPicNum;
	ICON_TYPE_TO_PIC* typeToPicTable = NULL;
		
	if(listView[id].dispType == ICON_TYPE)
	{
		typeToPicTable = listTable.iconPic;
		typeToPicNum = listTable.iconPicNum;
	}
	else if(listView[id].dispType == TEXT_TYPE)
	{
		typeToPicTable = listTable.listPic;
		typeToPicNum = listTable.listPicNum;
	}

	for(i = 0; i < typeToPicNum; i++)
	{
		if(typeToPicTable[i].iconType == iconType)
		{
			return typeToPicTable[i];
		}
	}

	return typeToPicTable[0];
}

static int GetPicByPreSprType(PRE_SPR_TYPE preSprType)
{
	int i;

	for(i = 0; i < listTable.preSprNum; i++)
	{
		if(listTable.preSpr[i].sprType == preSprType)
		{
			return listTable.preSpr[i].sprId;
		}
	}

	return listTable.preSpr[0].sprId;
}

static int GetPicBySufSprType(SUF_SPR_TYPE sufSprType)
{
	int i;

	for(i = 0; i < listTable.sufSprNum; i++)
	{
		if(listTable.sufSpr[i].sprType == sufSprType)
		{
			return listTable.sufSpr[i].sprId;
		}
	}

	return listTable.sufSpr[0].sprId;
}

static int GetPicByBgType(BG_TYPE bgType)
{
	int i;
	
	for(i = 0; i < listTable.bgNum; i++)
	{
		if(listTable.bg[i].bgType == bgType)
		{
			return listTable.bg[i].bgId;
		}
	}

	return listTable.bg[0].bgId;
}

