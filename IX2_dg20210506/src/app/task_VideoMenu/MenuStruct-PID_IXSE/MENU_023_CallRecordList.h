
#ifndef _MENU_023_H
#define _MENU_023_H

#include "MENU_public.h"
#include "../../task_io_server/obj_call_record.h"
#include "../../task_io_server/obj_TableProcess.h"

typedef enum
{
	CALL_INCOMING = 0,
	CALL_OUTGOING,
	CALL_MISSED,
	CALL_SEARCH,
	VIDEO_PLAYLIST,
	VIDEO_SEARCH,
	CALL_REC_DEL_ALL, // zfz_20190601
}CALL_RECORD_LIST_TYPE;

#define ONE_LIST_MAX_LEN		40

typedef struct
{
	CALL_RECORD_LIST_TYPE	saveType;		//cao_20160708
	CALL_RECORD_LIST_TYPE	type;
	one_vtk_table*			pcall_record;
	int						page_num;
	int						page_max;
	int						list_focus;
	int						onePageMaxNum;
	int						act_flag;	    //czn_20170306
	struct tm *				tblock;
	char 					time_dispbuf[20];
}CALL_RECORD_LIST_PAGE;

extern CALL_RECORD_LIST_PAGE	call_record_page;	//cao_20160708

void set_call_record_page_type( CALL_RECORD_LIST_TYPE type );

#endif

