#include "MENU_066_FwUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "MenuBackFwUpgrade_Business.h"
#include "ota_FileProcess.h"
#include "ota_client_ifc.h"
#include "MENU_public.h"
#include "cJSON.h"


int FwUpgrade_Code_Verify(char *code);
void FwUpgrade_Notice_Notice(int notice_type);
void FwUpgrade_Update_Operation(int opt);
int FwUprade_Installing(char *file_path);
void Update_FwUpgradeServer_Select(int select);
int get_upgrade_file_dir(int flie_type,int file_length,char *file_dir);
void remove_upgrade_tempfile(void);
void FwUpgrade_IoPara_Init(void);

#define	FwUpgrade_ICON_MAX	5
int FwUpgradeIconSelect;
int FwUpgradePageSelect;
//0,uncheck,1,checking,2 checked 3 tranfering 4tranfered/updating,5canceling

static int FwUpgrade_State = FwUpgrade_State_Uncheck;

//czn_20190506_s
int FwUpgrade_Ser_Select = 1;
char FwUpgrade_Ser_Disp[40]={"Server2[47.106.104.38]"};
char FwUpgrade_Code[9] = {0};//{"123456"};
char text_file_dir[60];
char fw_file_dir[60];
char FwUpgrade_server_ip[16] = {"47.106.104.38"};
char FwUpgrade_other_server[16] = {0};
int Fw_file_trans_size =0;
int Fw_file_size;
char trans_file_name[40];
//czn_20190506_e

OtaDlTxt_t dlTxtInfo;


typedef struct
{
	int iCon;
	int iConText; 
}FwUpgradeIcon;

const FwUpgradeIcon FwUpgradeIconTable[] = 
{
	{ICON_FwUpgrade_Server,			MESG_TEXT_ICON_FwUpgrage_Server},    
	{ICON_FwUpgrade_DLCode,			MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		NULL},
	{ICON_FwUpgrade_Notice,			NULL},
	{ICON_FwUpgrade_Operation,		NULL},
};

const int FwUpgradeIconNum = sizeof(FwUpgradeIconTable)/sizeof(FwUpgradeIconTable[0]);

static void DisplayFwUpgradePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	//API_DisableOsdUpdate();
	//API_OsdStringClearExt(x, y, bkgd_w-x, 40);//r
		
	for(i = 0; i < FwUpgrade_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*FwUpgrade_ICON_MAX+i < FwUpgradeIconNum)
		{
			if(get_pane_type() == 5 )
			{
				x1 = x + 100;
				y1 = y + 40;
			}
			else
			{
				x1 =pos.x+ (hv.h - pos.x)/2;
				y1 =y;//+ VALUE_DEVIATION_Y;
			}
			
			switch(FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iCon)
			{
				case ICON_FwUpgrade_Server:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iConText, 1, bkgd_w-x);
					if(FwUpgrade_Ser_Disp[0]  != 0)
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, FwUpgrade_Ser_Disp, strlen(FwUpgrade_Ser_Disp), 1, STR_UTF8, bkgd_w-x1);
					break;
				
				case ICON_FwUpgrade_DLCode:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iConText, 1, bkgd_w-x);
					if(FwUpgrade_Code[0]  != 0)
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, FwUpgrade_Code, strlen(FwUpgrade_Code), 1, STR_UTF8, bkgd_w-x1);
					break;
					
				case ICON_FwUpgrade_CodeInfo:
					break;
					
				case ICON_FwUpgrade_Notice:
					break;
					
				case ICON_FwUpgrade_Operation:
					if(FwUpgrade_State == FwUpgrade_State_Uncheck)
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_FwUpgrage_Check,1,bkgd_w-x);
					}
					break;

			}
		}
	}
	pageNum = FwUpgradeIconNum/FwUpgrade_ICON_MAX + (FwUpgradeIconNum%FwUpgrade_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}





void MENU_066_FwUpgrade_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_045_Upgrade);
	
	FwUpgrade_IoPara_Init();
	Update_FwUpgradeServer_Select(FwUpgrade_Ser_Select);
	FwUpgrade_State = FwUpgrade_State_Uncheck;
	API_OsdStringClearExt(pos.x, hv.v/2, 300, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_FwUpgrage,1, 0);
	FwUpgradePageSelect = 0;
	API_DisableOsdUpdate();
	DisplayFwUpgradePageIcon(FwUpgradePageSelect);
	API_EnableOsdUpdate();
}

void MENU_066_FwUpgrade_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_045_Upgrade);
}

void MENU_066_FwUpgrade_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	char app_code[9];
	char app_ver[40];		
	int rebootFlag;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(FwUpgrade_State == FwUpgrade_State_Uncheck ||FwUpgrade_State == FwUpgrade_State_Intalled||FwUpgrade_State == FwUpgrade_State_CheckOk)
					{
						remove_upgrade_tempfile();
					}
					if(FwUpgrade_State == FwUpgrade_State_Checking||FwUpgrade_State == FwUpgrade_State_Downloading)
					{
						FwUpgrade_State = FwUpgrade_State_Canceling;
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Canceling);
						API_OtaCancel();
					}					
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					FwUpgradeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(FwUpgradePageSelect*FwUpgrade_ICON_MAX+FwUpgradeIconSelect >= FwUpgradeIconNum)
					{
						return;
					}
					
					switch(FwUpgradeIconTable[FwUpgradePageSelect*FwUpgrade_ICON_MAX+FwUpgradeIconSelect].iCon)
					{
						case ICON_FwUpgrade_Server:
							if(FwUpgrade_State == FwUpgrade_State_Uncheck ||FwUpgrade_State == FwUpgrade_State_CheckOk)
							{
								EnterInstallSubMenu(MENU_067_FwUpgradeServer, 0);
							}
							break;

						case ICON_FwUpgrade_DLCode:
							if(FwUpgrade_State == FwUpgrade_State_Uncheck ||FwUpgrade_State == FwUpgrade_State_CheckOk)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_DLCode, FwUpgrade_Code, 8, COLOR_WHITE, FwUpgrade_Code, 1, FwUpgrade_Code_Verify);
							}
							break;
							
						case ICON_FwUpgrade_CodeInfo:
							
							break;
							
						case ICON_FwUpgrade_Notice:
							break;	
							
						case ICON_FwUpgrade_Operation:
							if(FwUpgrade_State == FwUpgrade_State_Uncheck)
							{
								if(FwUpgrade_Ser_Select == 4)
								{	
									sprintf(display,"%s/%s.txt",SdUpgradePath,FwUpgrade_Code);
									if(Judge_SdCardLink() == 0)	//r
									{
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
									}
									else if(CheckDownloadIsAllowed(display, &dlTxtInfo))
									{
										FwUpgrade_State = FwUpgrade_State_CheckOk;
										API_DisableOsdUpdate();
										Fw_file_size = dlTxtInfo.fileLength;
										FwUpgrade_CodeInfo_Disp(dlTxtInfo.info);
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
										FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
										API_EnableOsdUpdate();
									}
									else
									{
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrMismatchedType);
									}
								}
								else
								{
									if((error_code=get_upgrade_file_dir(1,0,text_file_dir)) == 0)
									{
										OtaClientPara para;
										int serverIpAddr;
										int server1IpAddr;
										int server2IpAddr;
										
										serverIpAddr = inet_addr(FwUpgrade_server_ip);
										server1IpAddr = inet_addr("47.106.104.38");
										server2IpAddr = inet_addr("47.91.88.33");
										
										strcpy(para.server_ip, FwUpgrade_server_ip);
										strcpy(para.file_code, FwUpgrade_Code);
										para.file_type = 1;
										strcpy(para.dev_type, GetSysVerInfo_type());
										strcpy(para.dev_ver, GetSysVerInfo_swVer());
										strcpy(para.dev_id, GetSysVerInfo_Sn());
										para.reportIpCnt = 0;
										para.space = 0;
										
										// lzh_20240603_s
										//if(serverIpAddr == server1IpAddr || serverIpAddr == server2IpAddr)
										if(1)
										// lzh_20240603_e
										{
											para.downloadType = OtaDlTypeFtp;
										}
										else
										{
											para.downloadType = OtaDlTypeTcp;
										}
										
										API_OtaDownload(0, para);
										
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
										FwUpgrade_State = FwUpgrade_State_Checking;
									}
									else if(error_code == -1)
									{
										BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
									}
									else
									{
										BackFwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
									}
								}
							}
							else if(FwUpgrade_State == FwUpgrade_State_CheckOk)
							{
								if(FwUpgrade_Ser_Select == 4)
								{
									FwUpgrade_State = FwUpgrade_State_Intalling;
									FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
									FwUpgrade_Update_Operation(0);
									sprintf(display,"%s/%s",SdUpgradePath,FwUpgrade_Code);
									BusySpriteDisplay(1);		
									
									if(InstallDownloadZip(dlTxtInfo.dlType, display, &rebootFlag) >= 0)
									{
										BusySpriteDisplay(0);
										BEEP_CONFIRM();
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
										if(rebootFlag)
										{
											SoftRestar();
										}
									}
									else
									{
										BusySpriteDisplay(0);
										BEEP_ERROR();
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
									}
									FwUpgrade_State = FwUpgrade_State_Intalled;
								}
								else
								{
									if((error_code = get_upgrade_file_dir(0,Fw_file_size,fw_file_dir)) == 0)
									{
										OtaClientPara para;
										int serverIpAddr;
										int server1IpAddr;
										int server2IpAddr;
										
										serverIpAddr = inet_addr(FwUpgrade_server_ip);
										server1IpAddr = inet_addr("47.106.104.38");
										server2IpAddr = inet_addr("47.91.88.33");
										
										strcpy(para.server_ip, FwUpgrade_server_ip);
										strcpy(para.file_code, FwUpgrade_Code);
										para.file_type = 0;
										strcpy(para.dev_type, GetSysVerInfo_type());
										strcpy(para.dev_ver, GetSysVerInfo_swVer());
										strcpy(para.dev_id, GetSysVerInfo_Sn());
										para.reportIpCnt = 0;
										para.space = Fw_file_size;
										
										// lzh_20240603_s
										//if(serverIpAddr == server1IpAddr || serverIpAddr == server2IpAddr)
										if(1)
										// lzh_20240603_e
										{
											para.downloadType = OtaDlTypeFtp;
										}
										else
										{
											para.downloadType = OtaDlTypeTcp;
										}
										
										API_OtaDownload(0, para);
										FwUpgrade_State = FwUpgrade_State_Downloading;
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
										FwUpgrade_Update_Operation(0);
									}
									else
									{
										BEEP_ERROR();
										if(error_code == -1)
										{
											FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
										}
										else
										{
											FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
										}
									}
								}
							}
							break;
							
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{				
			case MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_ok:
				ParseFtpDownloadInfoJsonData(arg+sizeof(SYS_WIN_MSG), NULL, fw_file_dir, trans_file_name, NULL, NULL);
				if(FwUpgrade_State == FwUpgrade_State_Checking)
				{
					sprintf(display,"%s/%s",text_file_dir,trans_file_name);
					if(CheckDownloadIsAllowed(display, &dlTxtInfo))
					{
						FwUpgrade_State = FwUpgrade_State_CheckOk;
						API_DisableOsdUpdate();
						Fw_file_size = dlTxtInfo.fileLength;
						FwUpgrade_CodeInfo_Disp(dlTxtInfo.info);
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
						FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
						API_EnableOsdUpdate();
					}
					else
					{
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
						FwUpgrade_State = FwUpgrade_State_Uncheck;
					}
				}
				else if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					BusySpriteDisplay(1);
					BEEP_CONFIRM();
					FwUpgrade_State = FwUpgrade_State_Intalling;
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
					sprintf(display,"%s/%s",fw_file_dir,trans_file_name);
					if(InstallDownloadZip(dlTxtInfo.dlType, display, &rebootFlag) >= 0)
					{
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
						if(rebootFlag)
						{
							SoftRestar();
						}
					}
					else
					{
						BusySpriteDisplay(0);
						BEEP_ERROR();
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
					}
					FwUpgrade_State = FwUpgrade_State_Intalled;
				}
				break;
			
			case MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_run:
				if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					ParseFtpDownloadInfoJsonData(arg+sizeof(SYS_WIN_MSG), NULL, NULL, NULL, &Fw_file_size, &Fw_file_trans_size);
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
					AutoPowerOffReset();
				}
				break;
			
			case MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_ok:
				if(FwUpgrade_State == FwUpgrade_State_Uncheck || FwUpgrade_State == FwUpgrade_State_Checking)
				{
					FwUpgrade_State = FwUpgrade_State_Checking;
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Check);
				}
				else if(FwUpgrade_State == FwUpgrade_State_CheckOk)
				{
					FwUpgrade_State = FwUpgrade_State_Downloading;
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
				}
				break;
				
			case MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_er:
				if(FwUpgrade_State == FwUpgrade_State_Checking)
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
					FwUpgrade_State = FwUpgrade_State_Uncheck;
				}
				else if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					FwUpgrade_State = FwUpgrade_State_CheckOk;
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				break;

			case MSG_7_BRD_SUB_OtaClientIfc_ActOk:
				if(FwUpgrade_State == FwUpgrade_State_Canceling)
				{
					sleep(1);
					popDisplayLastMenu();
					remove_upgrade_tempfile();
				}
				else if(FwUpgrade_State == FwUpgrade_State_Checking || FwUpgrade_State == FwUpgrade_State_Downloading)
				{	
					Fw_file_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					strncpy(trans_file_name,(char *)(arg+sizeof(SYS_WIN_MSG)+4),40);
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translated:
				if(FwUpgrade_State == FwUpgrade_State_Checking)
				{
					FwUpgrade_State = FwUpgrade_State_CheckOk;
					sprintf(display,"%s/%s",text_file_dir,trans_file_name);
					if(CheckDownloadIsAllowed(display, &dlTxtInfo))
					{
						FwUpgrade_State = FwUpgrade_State_CheckOk;
						API_DisableOsdUpdate();
						Fw_file_size = dlTxtInfo.fileLength;
						FwUpgrade_CodeInfo_Disp(dlTxtInfo.info);
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
						FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
						API_EnableOsdUpdate();
					}
					else
					{
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
						FwUpgrade_State = FwUpgrade_State_Uncheck;
					}
				}
				else if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					BusySpriteDisplay(1);
					BEEP_CONFIRM();
					FwUpgrade_State = FwUpgrade_State_Intalling;
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
					sprintf(display,"%s/%s",fw_file_dir,trans_file_name);
					if(InstallDownloadZip(dlTxtInfo.dlType, display, &rebootFlag) >= 0)
					{
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
						if(rebootFlag)
						{
							SoftRestar();
						}
					}
					else
					{
						BusySpriteDisplay(0);
						BEEP_ERROR();
						FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
					}
					FwUpgrade_State = FwUpgrade_State_Intalled;
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translating:
				if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					Fw_file_trans_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					API_DisableOsdUpdate();
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
					API_EnableOsdUpdate();
					AutoPowerOffReset();
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Error:
				//rsp -1:busy,-2:connect ser error,-3:no file,-4:download error -5:no sdcard
				BEEP_ERROR();
				error_code = *((int*)(arg+sizeof(SYS_WIN_MSG)));
				if(error_code == -1)
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -2)
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -3)
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
				}
				else if(error_code == -4)
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				else
				{
					FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				if(FwUpgrade_State == FwUpgrade_State_Checking)
				{
					FwUpgrade_State = FwUpgrade_State_Uncheck;
				}
				else if(FwUpgrade_State == FwUpgrade_State_Downloading)
				{
					FwUpgrade_State = FwUpgrade_State_CheckOk;
					FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
				}
				break;
				
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//setInstallerPageSelect = 0;
		//DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}


void get_file_size_disp(int file_byte_num,char *disp)
{	
	if(file_byte_num < 10)
	{
		strcpy(disp,"unkown");
		dprintf("file_byte_num = %d\n", file_byte_num);
	}
	else if(file_byte_num < 1000)
	{
		sprintf(disp,"%d B",file_byte_num);
	}
	else if(file_byte_num < 1000000)
	{
		sprintf(disp,"%d kB",file_byte_num/1000);
	}
	else
	{
		sprintf(disp,"%d.%d MB",file_byte_num/1000000,(file_byte_num/100000)%10);
	}
}

void MCU_InstallDisplay(int msgId, int blk_all, int blk_cur)
{
	static int saveMsgId, saveBlk_all, saveBlk_cur;
	
	const char* FW_UART_CB_DISPLAY_STRING[] = 
	{
		"Read version...",
		"Read version ok!",
		"Read version err!",
		"Read version timeout!",
		"Upgrade start...",
		"Upgrade start ok!",
		"Upgrade start err!",
		"Upgrade start timeout!",
		"Upgrade data ...",
		"Upgrade data ok!",
		"Upgrade data err!",
		"Upgrade data timeout!",
		"Upgrade stop ...",
		"Upgrade stop ok!",
		"Upgrade stop err!",
		"Upgrade stop timeout!",
		"Upgrade successful!",
	};

	char disp[100];
	int x, y, x1, y1;
	POS pos;
	SIZE hv;

	if(saveMsgId != msgId || saveBlk_all != blk_all || saveBlk_cur != blk_cur)
	{
		saveMsgId = msgId;
		saveBlk_all = blk_all;
		saveBlk_cur = blk_cur;
		
		OSD_GetIconInfo(ICON_010_PublicList4, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		
		x1 =pos.x+ (hv.h - pos.x)/2;
		y1 =y;
		
		if( blk_all )
		{
			snprintf(disp, 100, "%s[%d%%]            ", FW_UART_CB_DISPLAY_STRING[msgId],(blk_cur+1)*100/blk_all);
		}
		else
		{
			snprintf(disp, 100, "%s                  ", FW_UART_CB_DISPLAY_STRING[msgId]);
		}
		API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, 0);
	}
}

void FwUpgrade_Notice_Notice(int notice_type)
{
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	int disp_flag = 0;
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_010_PublicList4, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+3*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =pos.x+ (hv.h - pos.x)/2;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
	
	switch(notice_type)
	{
		case MESG_TEXT_FwUpgrage_CodeSize:
			get_file_size_disp(Fw_file_size,disp);
			disp_flag = 1;
			API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x1-20);
			break;

		case MESG_TEXT_FwUpgrage_Downloading:
			get_file_size_disp(Fw_file_size,disp);
			get_file_size_disp(Fw_file_trans_size,disp1);
			strcat(disp,"(");
			strcat(disp,disp1);
			strcat(disp,")");
			disp_flag = 1;
			API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x1-20);
			break;

		case MESG_TEXT_FwUpgrage_Cennecting:
			break;

		case MESG_TEXT_FwUpgrage_Installing:
			break;

		case MESG_TEXT_FwUpgrage_ErrConnecting:
			break;

		case MESG_TEXT_FwUpgrage_ErrNoSpace:
			break;
	}

	if(!disp_flag)
	API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, notice_type, 1,bkgd_w-x);
	
}

void FwUpgrade_CodeInfo_Disp(char* fwInfo)
{
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_009_PublicList3, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =pos.x+ (hv.h - pos.x)/2;//x1 =x+ DISPLAY_VALUE_X_DEVIATION-210;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, hv.h - x, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_FwUpgrage_CodeInfo, 1, x1-x-10);

	API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, fwInfo, strlen(fwInfo), 1, STR_UTF8, bkgd_w-x1-20);
}

void FwUpgrade_Update_Operation(int opt)
{
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_011_PublicList5, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =x+ (hv.h - pos.x)/2;//x1 =x+ DISPLAY_VALUE_X_DEVIATION-220;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, hv.h - x, CLEAR_STATE_H);
	if(opt != 0)
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, opt, 1, x1 - x);
}

int Get_FwUpgradeServer_Select(void)
{
	return FwUpgrade_Ser_Select; 
}

void Update_FwUpgradeServer_Select(int select)
{
	FwUpgrade_Ser_Select=select;
	
	if(FwUpgrade_Ser_Select == 0)
	{
		sprintf(FwUpgrade_Ser_Disp,"[%s]",FwUpgrade_other_server);
		strcpy(FwUpgrade_server_ip,FwUpgrade_other_server);
	}
	else if(FwUpgrade_Ser_Select == 1)
	{
		strcpy(FwUpgrade_Ser_Disp,"Server1[47.91.88.33]");
		strcpy(FwUpgrade_server_ip,"47.91.88.33");
	}
	else if(FwUpgrade_Ser_Select == 2)
	{
		strcpy(FwUpgrade_Ser_Disp,"Server2[47.106.104.38]");
		strcpy(FwUpgrade_server_ip,"47.106.104.38");
	}
	else if(FwUpgrade_Ser_Select == 3)
	{
		//sprintf(FwUpgrade_Ser_Disp,"Server3[%s]",FwUpgrade_other_server);
		strcpy(FwUpgrade_Ser_Disp,"Server3[192.168.2.60]");
		strcpy(FwUpgrade_server_ip,"192.168.2.60");
		//strcpy(FwUpgrade_other_server,other_server);
	}
	else if(FwUpgrade_Ser_Select == 4)
	{
		strcpy(FwUpgrade_Ser_Disp,"SD card");
	}
	char temp[20];
	sprintf(temp, "%d", FwUpgrade_Ser_Select);
	API_Event_IoServer_InnerWrite_All(FWUPGRADE_SER_SELECT, temp);
}

int Get_FwUpgradeServer_Disp(int select,char *disp)
{
	if(select == 0)
	{
		if(FwUpgrade_other_server[0] == 0)
			disp[0] = 0;
		else
			sprintf(disp,"[%s]",FwUpgrade_other_server);
	}
	if(select == 1)
	{
		strcpy(disp,"Server1[47.91.88.33]");
	}
	if(select == 2)
	{
		strcpy(disp,"Server2[47.106.104.38]");
	}
	if(select == 3)
	{
		sprintf(disp,"Server3[192.168.2.60]");
	}
	if(select == 4)
	{
		strcpy(disp,"SD card");
	}
	
	return strlen(disp);
}

void Get_FwUpgrade_OtherServer(char *server)
{
	strcpy(server,FwUpgrade_other_server);
}

int Update_FwUpgrade_OtherServer(char *server)
{
	strcpy(FwUpgrade_other_server,server);

	return 1;
}

void FwUpgrade_IoPara_Init(void)
{
	char ch[21];
	
	API_Event_IoServer_InnerRead_All(FWUPGRADE_SER_SELECT, ch);
	FwUpgrade_Ser_Select = atoi(ch);
	API_Event_IoServer_InnerRead_All(FWUPGRADE_OTHER_SER, ch);
	if(strcmp(ch,"-") == 0)
	{
		FwUpgrade_other_server[0] = 0;
	}
	else
	{
		strcpy(FwUpgrade_other_server,ch);
	}
		
	API_Event_IoServer_InnerRead_All(FWUPGRADE_FW_CODE, ch);
	if(strcmp(ch,"-") == 0)
	{
		FwUpgrade_Code[0] = 0;
	}
	else
	{
		strcpy(FwUpgrade_Code,ch);
	}
}

