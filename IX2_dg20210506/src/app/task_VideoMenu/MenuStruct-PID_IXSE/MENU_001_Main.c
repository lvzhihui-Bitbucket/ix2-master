
#include "MENU_public.h"
#include "MENU_001_Main.h"
#include "obj_SearchIpByFilter.h"
#include "obj_ProgInfoByIp.h"
#include "obj_GetIpByNumber.h"
#include "task_WiFiConnect.h"
#include "bmplib.h"
#define CHAR_DEVIATION_X			50
#define CHAR_DEVIATION_Y			15

#if 0
#define MAIN_CALL_SCENE_X			167
#define MAIN_CALL_SCENE_Y			419
#define MAIN_MISS_CALL_X			295
#define MAIN_MISS_CALL_Y			419
#define MAIN_SIP_CONFIG_X			457
#define MAIN_SIP_CONFIG_Y			419
#define MAIN_WIFI_X					548
#define MAIN_WIFI_Y					419
#define MAIN_WIFI_WIDTH				70
#endif

uint8 gLightState = 0;
uint8 TransferSetting;
uint16 missedCall;

static int CallLiftEn=0;

extern OS_TIMER t_rtc;

void IfCallLiftEnable(void)
{
	return CallLiftEn;
}
void SetCallLiftEnable(int value)
{
	CallLiftEn = value;
}
void Rtc_Timer_Process(void)
{
	//OS_StopTimer(&t_rtc);
}

int Rtc_Timer_Set( int sec )
{
	OS_SetTimerPeriod( &t_rtc, sec*100 );
	OS_RetriggerTimer( &t_rtc );	
}

void DisplayMainMenuLogo(void)
{	
	char *logo_file;
	if(get_pane_type()==5)
	{
		logo_file=LOGO_V_FILENAME;
	}
	else
	{
		logo_file=LOGO_FILENAME;
	}

	if(access(logo_file, F_OK) == 0)
	{
		BMP_FILE *bmpfile=NULL;
		bmpfile = OpenBMPFile(logo_file);
		if(bmpfile!=NULL)
		{
			API_Menu_BmpDisplay(OSD_LAYER_SPRITE, logo_file,0,bkgd_h-bmpfile->cur_line-1,bmpfile->bmih.biWidth,bmpfile->cur_line+1);
			CloseBMPFile(bmpfile);
		}
	}
	else if(access(SOS_PRESS_FILENAME, F_OK) == 0&&access(SOS_RELEASE_FILENAME, F_OK) == 0)
	{
		BMP_FILE *bmpfile=NULL;
		if(Get_AlarmingSOS_State()==1)
		{
			logo_file=SOS_PRESS_FILENAME;
			bmpfile = OpenBMPFile(SOS_PRESS_FILENAME);
		}
		else
		{
			logo_file=SOS_RELEASE_FILENAME;
			bmpfile = OpenBMPFile(SOS_RELEASE_FILENAME);
		}

		if(bmpfile!=NULL)
		{
			API_Menu_BmpDisplay(OSD_LAYER_SPRITE, logo_file,0,bkgd_h-bmpfile->cur_line-1,bmpfile->bmih.biWidth,bmpfile->cur_line+1);
			CloseBMPFile(bmpfile);
		}
	}
}

void DisplayMainMenuWifi(void)
{
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	int x,y;
	if(OSD_GetIconInfo(ICON_223_MainWifi, &pos, &hv) == 0)
		return;
	Get_SpriteSize(SPRITE_MainWifi_Disconnect, &xsize, &ysize);
	x = pos.x;
	y = pos.y + (hv.v - pos.y - ysize)/2;

	if(GetSysVerInfo_WlanEn())
	{
		//if(wifiRun.wifiConnect == 2)
		if(GetSysVerInfo_WlanConnect()==WLAN_ACT_CONNECTED)
		{
			int level=0;
			level|=GetSysVerInfo_WifiSignalLev();
			if(GetSysVerInfo_WifiSignalLev()&0x80)
				level|=0xffffff00;
			printf("11111111DisplayMainMenuWifiQuality:%d:%d\n",wifiRun.curWifi.LEVEL,level);
			if(level >= -55)
			{
				//API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY4);
				Api_StateIcon_UpdateOneIcon(ICON_223_MainWifi,4,GetSysVerInfo_WlanCurSsid());
			}
			else if(level >= -70)
			{
				//API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY3);
				Api_StateIcon_UpdateOneIcon(ICON_223_MainWifi,3,GetSysVerInfo_WlanCurSsid());
			}
			else if(level >= -85)
			{
				//API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY2);
				Api_StateIcon_UpdateOneIcon(ICON_223_MainWifi,2,GetSysVerInfo_WlanCurSsid());
			}
			else 
			{
				//API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY1);
				Api_StateIcon_UpdateOneIcon(ICON_223_MainWifi,1,GetSysVerInfo_WlanCurSsid());
			}
			//API_OsdStringDisplayExt(x+CHAR_DEVIATION_X, y+CHAR_DEVIATION_Y, COLOR_RED, GetSysVerInfo_WlanCurSsid(), strlen(GetSysVerInfo_WlanCurSsid()), 0,STR_UTF8, 0);
		}
		else
		{
			//API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_Disconnect);
			Api_StateIcon_UpdateOneIcon(ICON_223_MainWifi,0,NULL);
		}
	}
	else
	{
		//API_SpriteClose( x, y, SPRITE_MainWifi_Disconnect);
	}
	
}

void DisplayMainMenuSip(void)
{
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	int x,y;
	char sip_net_sel[5];
	if(OSD_GetIconInfo(ICON_222_MainSip, &pos, &hv) == 0)
		return;
	Get_SpriteSize(SPRITE_SipSerOk, &xsize, &ysize);
	x = pos.x;
	y = pos.y + (hv.v - pos.y - ysize)/2;
	API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
	
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0)
	{
		//��һ��:IP�������
		char temp_buff[5];		//czn_20191123
		API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp_buff);
		int sip_en =atoi(temp_buff);
		//IP��ַ�ظ�
		if(GetIpRepeatFlag()==1)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_NetNotExit);
		}
		//����������Internet
		else if((atoi(sip_net_sel)==0&&GetInternetState() == 0)||(atoi(sip_net_sel)==1&&GetWifiConnected() == 0))
		{
			API_SpriteDisplay_XY( x, y, SPRITE_NetOK);
		}
		else if(sip_en == 0)		//czn_20191123
		{
			API_SpriteClose( x, y, SPRITE_NetOK);
		}
		//����������sipû�д�
		else if(((atoi(sip_net_sel)==0&&GetInternetState() == 1)||(atoi(sip_net_sel)==1&&GetWifiConnected() == 1) )
			&& Get_SipAccount_State() == 2)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_SipSerFail);
		}
		//sipû�д�
		else if(Get_SipAccount_State() == 1)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_SipSerOk);
		}
		else
		{	
			if(Get_SipReg_ErrCode() == 401)
			{
				API_SpriteDisplay_XY( x, y, SPRITE_SipSerCaution);
			}
			else
			{
				API_SpriteDisplay_XY( x, y, SPRITE_SipSerCaution);
			}
		}

	}
	else
	{
		API_MsSyncSipConfig(0);

		
		//��һ��:IP�������
	
		//IP��ַ�ظ�
		if(GetIpRepeatFlag()==1)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_NetNotExit);
		}
		//����������Internet
		else if(GetMsSyncInternetState() == 0)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_NetOK);
		}
		//����������sipû�д�
		else if(GetMsSyncInternetState() == 1 && Get_MsSync_SipReg_State() == 2)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_SipSerFail);
		}
		//sipû�д�
		else if(Get_MsSync_SipReg_State() == 1)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_SipSerOk);
		}
		else
		{	
			if(Get_MsSync_SipReg_ErrCode() == 401)
			{
				API_SpriteDisplay_XY( x, y, SPRITE_SipSerCaution);
			}
			else
			{
				API_SpriteDisplay_XY( x, y, SPRITE_SipSerCaution);
			}
		}
	}
}
void DisplayMainCallScene(void)
{
	char temp[20];
	char tempDisplay[10] = {0};
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	int x,y;
	if(OSD_GetIconInfo(ICON_220_MainScene, &pos, &hv) == 0)
		return;
	Get_SpriteSize(SPRITE_NoDisturb, &xsize, &ysize);
	x = pos.x;
	y = pos.y + (hv.v - pos.y - ysize)/2;
	
	if((TransferSetting = Get_NoDisturbSetting()) == 0) 	
	{
		API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
		TransferSetting = atoi(temp);
	}

	if(TransferSetting == 0)
	{
		API_SpriteClose( x, y, SPRITE_NoDisturb);
	}
	else if(TransferSetting <= 2)
	{
		API_SpriteDisplay_XY( x, y, SPRITE_NoDisturb);
		if(TransferSetting == 1)
		{
			int remain_hour,remain_min;
			Get_NoDisturb8H_RemainTime(&remain_hour,&remain_min);
			snprintf(tempDisplay, 10, "(%02d:%02d)", remain_hour,remain_min);
			API_OsdStringDisplayExt(x+CHAR_DEVIATION_X, y+CHAR_DEVIATION_Y, COLOR_RED, tempDisplay, strlen(tempDisplay), 0,STR_UTF8, 0);
		}
	}
	else
	{
		API_SpriteDisplay_XY( x, y, SPRITE_Transfer);
	}
}
void DisplayMainMissCall(void)
{
	char tempDisplay[10] = {0};
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	int x,y;
	if(OSD_GetIconInfo(ICON_224_MainMissed, &pos, &hv) == 0)
		return;
	Get_SpriteSize(SPRITE_MissCall, &xsize, &ysize);
	x = pos.x;
	y = pos.y + (hv.v - pos.y - ysize)/2;

	missedCall = get_miss_call_nums();	
	if(missedCall)
	{
		snprintf(tempDisplay, 10, "(%d)", missedCall);
		API_SpriteDisplay_XY( x, y, SPRITE_MissCall);
		API_OsdStringDisplayExt(x+CHAR_DEVIATION_X, y+CHAR_DEVIATION_Y, COLOR_RED, tempDisplay, strlen(tempDisplay), 0,STR_UTF8, 0 );
	}
	else
	{
		API_SpriteClose( x, y, SPRITE_MissCall);
	}
}
#if 0
void setMissCallState(int state)
{
	missedCallState = state;
	if(missedCallState)
	{
		API_LedDisplay_MissEvent();
		DisplayMainMissCall();
	}
	else
	{
		API_LedDisplay_CheckEvent();
	}
}
void DisplayMainNodisturb(void)
{
	if( GetCurMenuCnt() != MENU_001_MAIN)
	{
	    return;
	}
	POS pos;
	SIZE hv;
	if(OSD_GetIconInfo(ICON_005_NoDisturb, &pos, &hv) == 0)
		return;
	if(Get_NoDisturbSetting())
	{
		API_SpriteDisplay_XY( pos.x, pos.y, SPRITE_NoDisturbOn);
	}
	else
	{
		API_SpriteDisplay_XY( pos.x, pos.y, SPRITE_NoDisturbOff);
	}	
}

void DisplayMainDivert(void)
{
	if( GetCurMenuCnt() != MENU_001_MAIN)
	{
	    return;
	}
	POS pos;
	SIZE hv;
	if(OSD_GetIconInfo(ICON_220_MainScene, &pos, &hv) == 0)
		return;
	if(divertSetting)
	{
		API_SpriteDisplay_XY( pos.x, pos.y, SPRITE_DivertOn);
	}
	else
	{
		API_SpriteClose( pos.x, pos.y, SPRITE_DivertOn);
	}	
}
void SetDivertState(int state)
{
	divertSetting = state;
	DisplayMainDivert();
}
int getDiverReqRetTimer(int time)
{
    if(time >= 3)
    {
		SetDivertState(0);
		CmdRingReq("Not available!");
        return 2;
    }
    return 0;
}
int DivertSetReq(int state)
{
	char ds_addr[11];
	char state_set[20]={0};
	printf("DivertSetReq state= %d\n", state);
	sprintf(ds_addr,"%s000001",GetSysVerInfo_bd());
	if(state==0)
	{
		strcpy(state_set,"OFF");
	}
	else if(state==1)
	{
		strcpy(state_set,"ON");
	}
	else
	{
		strcpy(state_set,"CHECK");
	}
    int ret = API_SetDivertState(GetSysVerInfo_BdRmMs(), ds_addr, GetSysVerInfo_BdRmMs(), state_set, NULL);  
	if(state==1)
	{
		if(ret != 0)
		{
			SetDivertState(0);
			CmdRingReq("Not available!");
		}
		else
		{
			API_Add_TimingCheck(getDiverReqRetTimer, 1);
		}
	}
	return ret;
}
int DivertStateCallback(cJSON *cmd)
{
	char* state = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "STATE"));
    char* message = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "MSG")); 

	if(state)
	{
		printf("get divert state %s msg=%s!\n", state, message);
		API_Del_TimingCheck(getDiverReqRetTimer);
		if(!strcmp(state, "ON"))
		{
			SetDivertState(1);
		}
		else if(!strcmp(state, "OFF"))
		{
			SetDivertState(0);
			if(message && message[0])
			{
				CmdRingReq(message);
				//MessageReportDisplay(NULL, 0, message, COLOR_RED, 1, 5);
			}
		}
	}
}
#endif
int ShortcutNum;
char* ShortcutIo[4];
extern const char* short_cut_io[];
const ICON_PIC_TYPE ShortcutIconTab[] = 
{
	ICON_PIC_DS,
	ICON_PIC_NAMECALL,
	ICON_PIC_NAMECALL,
	ICON_PIC_GUARD,
    ICON_PIC_IPC,
};
int ReadShortcut(void)
{
	int i,type;
	ShortcutNum = 0;
	for(i = 0; i < 4; i++)
		ShortcutIo[i]=NULL;
	for(i = 0; i < 4; i++)
	{
		type =read_short_cut_io_val(short_cut_io[i], NULL, NULL, NULL);
		if(type||(CallLiftEn&&strcmp(MainShortcut4Select,short_cut_io[i])==0))
		{
			ShortcutIo[ShortcutNum++] = short_cut_io[i];
		}
	}
	printf("ReadShortcut= %d\n", ShortcutNum);
	return ShortcutNum;
}
void SetShortcut(char* io, int sel)
{
	printf("SetShortcut= %s sel= %d\n", io, sel);
	int i;
	if(sel)
	{
		for(i = 0; i < 4; i++)
		{
			if(io == ShortcutIo[i])
			{
				return;
			}
		}
		ShortcutIo[ShortcutNum++] = io;
	}
	else
	{
		for(i = 0; i < 4; i++)
		{
			if(io == ShortcutIo[i])
			{
				break;
			}
		}
		if(i == 4)
			return;
		for(; i+1 < 4; i++)
		{
			ShortcutIo[i] = ShortcutIo[i+1];
		}
		ShortcutNum--;
	}
}


static void MenuListShortcutPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	int shortcutSelect;
	char name[41];
	char bdRmMs[21];	
	char ShortcutBuffer[50] = {0};
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < ShortcutNum)
		{
			if(CallLiftEn&&strcmp(MainShortcut4Select,ShortcutIo[index])==0)
				shortcutSelect=SHORTCUT_TYPE_CALL_LIFT;
			else
				shortcutSelect = read_short_cut_io_val(ShortcutIo[index], bdRmMs, name, NULL);
			switch(shortcutSelect)
			{
				case SHORTCUT_TYPE_MONITOR:
				case SHORTCUT_TYPE_NAMELIST:
				case SHORTCUT_TYPE_INNERCALL:
				case SHORTCUT_TYPE_GUARD_STATION:
					get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name, ShortcutBuffer);
					recordLen = strlen(ShortcutBuffer);
					//unicode_len = 2*api_ascii_to_unicode(ShortcutBuffer,recordLen,unicode_buf);
					unicode_len=2*utf82unicode(ShortcutBuffer,recordLen,unicode_buf);
					break;
				case SHORTCUT_TYPE_MONITOR_IPC:
					strcpy(ShortcutBuffer, name);
					recordLen = strlen(ShortcutBuffer);
					//unicode_len = 2*api_ascii_to_unicode(ShortcutBuffer,recordLen,unicode_buf);
					unicode_len=2*utf82unicode(ShortcutBuffer,recordLen,unicode_buf);
					break;
				//case SHORTCUT_TYPE_GUARD_STATION:
				//	API_GetOSD_StringWithID(MESG_TEXT_Icon_014_GuardStation, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
				//	break;

				case SHORTCUT_TYPE_CALL_LIFT:
					API_GetOSD_StringWithID(MESG_TEXT_Calllift, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
					
				case SHORTCUT_TYPE_IXInnerBroadcast:
					API_GetOSD_StringWithID(MESG_TEXT_InnerBroadcast, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
			}
			if(shortcutSelect==SHORTCUT_TYPE_IXInnerBroadcast)
				pDisp->disp[pDisp->dispCnt].iconType=ICON_PIC_NAMECALL;
			else if(shortcutSelect==SHORTCUT_TYPE_CALL_LIFT)
				pDisp->disp[pDisp->dispCnt].iconType=ICON_PIC_CALL_LIST;
			else
				pDisp->disp[pDisp->dispCnt].iconType = ShortcutIconTab[shortcutSelect-1];

			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}
	}
}

void ShortcutIconShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	listInit.listCnt = ReadShortcut();
	listInit.textColor = COLOR_WHITE;
	listInit.textSize = 0;
	listInit.textAlign = 8;
	listInit.fun = MenuListShortcutPage;
	listInit.listType = ICON_4X1;
	listInit.textOffset = 65;
	InitMenuListXY(listInit, 10, 100, 180, 400, 0, 0, ICON_IconView1);

}
void ShortcutIconClear(void)
{
	MenuListClear(0, 0, 10, 100, 180, 400);
	MenuListDisable(1);
}


/*************************************************************************************************
01.���˵���ʾ����Ϳ�������
*************************************************************************************************/
void MENU_001_MAIN_Init(int uMenuCnt)
{
	CallLiftEn=GetCallliftEnable();

	PlaybackInfoMenuInit("/mnt/nand1-2/Customerized/UI/", "logoInfo.*.jpg", NULL);
	DisplayMainMenuLogo();
	
	API_DateTimeShow();
	
	ShortcutIconShow();	
	DisplayMainMissCall();
	DisplayMainCallScene();
	DisplayMainMenuSip();
	//DisplayMainMenuWifi();
	#if 0
	if(IsMyProxyLinked())
	{
		API_OsdStringDisplayExt(0, 0, COLOR_RED, GetMyProxyLinked(), strlen(GetMyProxyLinked()), 0,STR_UTF8, 0 );		
	}

	API_Event_IoServer_InnerRead_All(DAY_CALL_VOLUME, tempDisplay);
	tempDisplay[0] = atoi(tempDisplay);
	if(tempDisplay[0] == 0)
	{
		API_SpriteDisplay_XY( GetIconXY(ICON_006_settings).x+76,GetIconXY(ICON_006_settings).y, SPRITE_RingMute);
	}
	#endif
}

void MENU_001_MAIN_Exit(void)
{
	API_DateTimeHide();
	MenuListDisable(0);
	MenuListDisable(1);
}

static int tone_test = 0;
void MENU_001_MAIN_Process(void* arg)
{
	int i;
	uint8 group_value[3];
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 slaveMaster;
	int shortcutSelect;
	uint8 len;
	Global_Addr_Stru gaddr;
	extern int callRecordSubIcon;
	extern one_vtk_table* nameListTable;
	char temp[20];
	char tempDisplay[100];
	char name[41];
	char bdRmMs[21];	
	char para_buff[20];
	int index;
	LIST_ICON listIcon;
	char *logo_file;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					if(JudgeIsInstallerMode() || API_AskDebugState(100))
					{
						StartInitOneMenu(MENU_108_QuickAccess,0,1);
					}
				break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
		//czn_20190216_s
		else if( pglobal_win_msg->status == TOUCH_3SECOND )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
				//Start_AutoCallback_Process();
				StartInitOneMenu(MENU_078_AutoCallbackList,0,1);
				break;
			}
		}
		//czn_20190216_e
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon == 0)
					{
						StartInitOneMenu(MENU_002_MONITOR,0,1);
					}
					else if(listIcon.mainIcon == 1)
					{
						StartInitOneMenu(MENU_003_INTERCOM,0,1);
					}
					else if(listIcon.mainIcon == 2)
					{
						StartInitOneMenu(MENU_004_CALL_RECORD,0,1);
					}
					else if(listIcon.mainIcon == 3)
					{

					}
					else if(listIcon.mainIcon == 4)
					{
						StartInitOneMenu(MENU_005_CALL_SCENE,0,1);
					}
					else if(listIcon.mainIcon == 5)
					{
						EnterSettingMenu(MENU_006_CALL_TUNE, 1);
					}
					break;
				case ICON_IconView1:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_IconView1);
					index = listIcon.mainIcon;
					if(index >= 0)
					{	
						if(CallLiftEn&&strcmp(MainShortcut4Select,ShortcutIo[index])==0)
							shortcutSelect=SHORTCUT_TYPE_CALL_LIFT;
						else
							shortcutSelect = read_short_cut_io_val(ShortcutIo[index], bdRmMs, name, NULL);
						switch(shortcutSelect)
						{
							case SHORTCUT_TYPE_MONITOR:
								if(Api_Ds_Show(0, 0, bdRmMs, name) == 0)
								{
									EnterDSMonMenu(0, 1);
								}
								break;
								
							case SHORTCUT_TYPE_NAMELIST:
								QuickNamelistCall(bdRmMs, name);
								break;
								
							case SHORTCUT_TYPE_INNERCALL:
								QuickMSlistCall(bdRmMs, name);
								break;
								
							case SHORTCUT_TYPE_GUARD_STATION:
								QuickNamelistCall(bdRmMs, name);
								//StartInitOneMenu(MENU_112_GSList,0,1);
								break;
							case SHORTCUT_TYPE_MONITOR_IPC:
								IpcShortcutMon(bdRmMs, name);
								break;
							case SHORTCUT_TYPE_IXInnerBroadcast:	
								StartBroadcast();
								break;
							case SHORTCUT_TYPE_NONE:
								StartInitOneMenu(MENU_030_SHORTCUT_SET,0,1);
								break;
							case SHORTCUT_TYPE_CALL_LIFT:
								if(CallLiftProcess()==0)
									BEEP_CONFIRM();
								break;
						}
					}
					else
					{
						StartInitOneMenu(MENU_030_SHORTCUT_SET,0,1);
					}
					break;
				case ICON_001_monitor:
					// lzh_20220411_s	test preview
					StartInitOneMenu(MENU_002_MONITOR,0,1);
					//EnterDSMonMenu(0, 0);
					// lzh_20220411_e
					//create_mesg_text_csv();
					break;
				case ICON_002_intercom:
					StartInitOneMenu(MENU_003_INTERCOM,0,1);
					break;

				case ICON_003_call_record:
					StartInitOneMenu(MENU_004_CALL_RECORD,0,1);
					break;

				case ICON_004_light:
					break;

				case ICON_005_NoDisturb:
					StartInitOneMenu(MENU_005_CALL_SCENE,0,1);
					//create_mesg_text_csv();
					break;
				
				case ICON_006_settings:
					EnterSettingMenu(MENU_006_CALL_TUNE, 1);
					break;

				case ICON_219_MainTime:
					StartInitOneMenu(MENU_029_DATE_TIME_SET,0,1);
					break;
				case ICON_220_MainScene:
					if(TransferSetting)
					{
						StartInitOneMenu(MENU_005_CALL_SCENE,0,1);
					}
					break;
				case ICON_221_MainLogo:
					if(get_pane_type()==5)
					{
						logo_file=LOGO_V_FILENAME;
					}
					else
					{
						logo_file=LOGO_FILENAME;
					}
					if(access(logo_file, F_OK) == 0)
					{
						if(GetPlaybackInfoJpgNum())
						{
							StartInitOneMenu(MENU_109_Info,0,1);
						}
					}
					else if(access(SOS_PRESS_FILENAME, F_OK) == 0&&access(SOS_RELEASE_FILENAME, F_OK) == 0)
					{
						if(Get_AlarmingSOS_State()==1)
						{
							API_SosAlarming_Release();
						}
						else
						{
							API_SosAlarming_Press();
						}
						
					}
					break;
				case ICON_222_MainSip:
					API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
					if(atoi(temp)&&memcmp(GetSysVerInfo_ms(),"01",2)==0)
					//if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		//czn_20190221
					{
						API_Event_IoServer_InnerRead_All(ManagePwdEnable, temp);
						API_Event_IoServer_InnerRead_All(SipChangePwdEnable, tempDisplay);
						//if(access(SIPHELP_FILENAME, F_OK) == 0||access(SIPHELP_QR_FILENAME, F_OK)==0)
						{
							if(PlaybackInfoMenuInit("/mnt/nand1-2/Customerized/UI/", "SipHelp.*.jpg", NULL)>0)
							{
								SaveIcon(ICON_222_MainSip);
								StartInitOneMenu(MENU_109_Info,0,1);
								return;
							}
						}
						//char glmenu_mode[5];
						//API_Event_IoServer_InnerRead_All(GerneralMenuMode, (uint8*)glmenu_mode);
						if(atoi(temp)==0||atoi(tempDisplay)==1||JudgeIsInstallerMode()|| API_AskDebugState(100))
						{
							StartInitOneMenu(MENU_009_SIP_CONFIG,0,1);
						}
						else
						{
							extern char installerInput[9];
							extern int MananerSettingVerifyPassword(const char* password);
							SaveIcon(ICON_222_MainSip);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, MananerSettingVerifyPassword);
						}
					}
					//	StartInitOneMenu(MENU_009_SIP_CONFIG,0,1);
					break;
				case ICON_223_MainWifi:
					if(JudgeIfWlanDevice()&&GetSysVerInfo_WlanEn())
						EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
					
					break;
				case ICON_224_MainMissed:
					if(missedCall)
					{
						callRecordSubIcon = ICON_017_MissedCalls;
						StartInitOneMenu(MENU_004_CALL_RECORD,MENU_023_CALL_RECORD_LIST,1);
					}
					break;	
				case ICON_028_about:
					StartInitOneMenu(MENU_010_ABOUT,0,1);
					break;
				case ICON_213_Close:
					CloseOneMenu();
					break;
				case ICON_ExitProg:
					if(JudgeIsInstallerMode())
					{
						ExitInstallerMode();
						StartInitOneMenu(MENU_001_MAIN,0,0);
					}
					break;
				case ICON_Help:
					if(PlaybackInfoMenuInit("/mnt/nand1-2/Customerized/UI/", "HelpInfo.*.jpg", NULL))
					{
						SaveIcon(ICON_Help);
						StartInitOneMenu(MENU_109_Info,0,1);
						return;
					}
					break;
				case ICON_ArmingSetting:
				case ICON_DisarmingSetting:
					if(GetAlarmingState()==0)
					{
						StartInitOneMenu(MENU_149_Arming,0,1);
					}
					else
					{
						SetDisarmMenuSub(0);
						StartInitOneMenu(MENU_153_DisarmingSubKeyPad,0,1);
					}
					break;	
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		char tempDisplay[10] = {0};
		
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
				DisplayMainMenuWifi();
				break;	
			case MSG_7_BRD_SUB_WIFI_SEARCHING:
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCH_OVER:
				break;

			case MSG_7_BRD_SUB_SIP_ONLINE:
				break;
			//czn_20190221_s
			case MSG_7_BRD_SUB_MsSyncSipConfig:

				break;
			case MSG_7_BRD_SUB_MsSyncCallScene:	
				//will_add
				break;
			//czn_20190221_e	
			case MSG_7_BRD_SUB_MASTERSTATE_CHANGE:

				break;
			case MSG_7_BRD_IconSosChange:
				
				if(access(SOS_PRESS_FILENAME, F_OK) == 0&&access(SOS_RELEASE_FILENAME, F_OK) == 0)
				{
					DisplayMainMenuLogo();
				}
				break;	

			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

void PublicUpProcess(int *iconSelect, int maxIcon)
{
	if(*iconSelect)
	{
		(*iconSelect)--;
	}
	else
	{
		*iconSelect = maxIcon-1;
	}
	API_FixedCurIconCursor(*iconSelect);
}

void PublicDownProcess(int *iconSelect, int maxIcon)
{
	if(*iconSelect < maxIcon-1)
	{
		(*iconSelect)++;
	}
	else
	{
		*iconSelect = 0;
	}
	API_FixedCurIconCursor(*iconSelect);
}


void BusySpriteDisplay(uint8 enable)
{
	//POS pos;
	//SIZE hv;
	//OSD_GetIconInfo(ICON_201_PageDown, &pos, &hv);
	if(enable)
	{
		API_SpriteDisplay_XY(bkgd_w-32, 32, SPRITE_BusyState);
	}
	else
	{
		API_SpriteClose(bkgd_w-32, 32, SPRITE_BusyState);
	}
}

POS GetIconXY(uint16 iconNum)
{
	POS OSD_GetIconXY(uint16 iconNum);
	return OSD_GetIconXY(iconNum);
}



