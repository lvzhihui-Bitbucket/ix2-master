#include "MENU_092_ResSyncSelect.h"
#include "obj_SearchIpByFilter.h"
#include "obj_SYS_VER_INFO.h"

//#define	ResSyncIconMax	4
int ResSyncIconMax;
int ResSyncIconSelect;
int ResSyncPageSelect;
int ResSyncIndex;
SearchIpRspData OnlineListData,OnlineListDataTemp;
char ResSyncToSel[128];
char ResSyncFromSel;


static void DisplayResSyncPageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;

	API_DisableOsdUpdate();
	for(i = 0; i < ResSyncIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		//API_SpriteClose(x, y, SPRITE_SELECT);
		ListSelect(i, 0);		
		index = page*ResSyncIconMax+i;
		if(index < OnlineListData.deviceCnt)
		{
			char display[200];
			//char temp[20] = {0};
			//SearchRoomNbrToDisplay(OnlineListData.data[index].deviceType, OnlineListData.data[index].BD_RM_MS, temp);
			//snprintf(display, 200, "%s%s %s", 
			//	DeviceTypeToString(OnlineListData.data[index].deviceType),
			//	temp, 
			//	OnlineListData.data[index].name);
			get_device_addr_and_name_disp_str(0, OnlineListData.data[index].BD_RM_MS, NULL, NULL, OnlineListData.data[index].name, display);
			
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);			
			/*sprintf(temp,"%s",DeviceTypeToString(OnlineListData.data[index].deviceType));
			printf("ResSync DeviceType:%s\n",temp);
			if(!strcmp(temp, "DS"))
			{
				sprintf(display, "DS%d",atoi(OnlineListData.data[index].BD_RM_MS+8));
			}
			else
			{
				memcpy(temp,OnlineListData.data[index].BD_RM_MS+4,4);
				temp[4] = 0;
				sprintf(display, "IM%d", atoi(temp));
			}*/
			if(ResSyncToSel[index])
			{
				//API_SpriteDisplay_XY(x, y, SPRITE_SELECT);
				ListSelect(i, 1);		
			}

		}
	}
	pageNum = OnlineListData.deviceCnt/ResSyncIconMax + (OnlineListData.deviceCnt%ResSyncIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	API_EnableOsdUpdate();
	
}
 
void OnlineListFilter(void)
{
	int i;
	char temp[5];
	OnlineListData.deviceCnt = 0;
	for(i = 0; i < OnlineListDataTemp.deviceCnt; i++)
	{
		memcpy(temp,OnlineListDataTemp.data[i].BD_RM_MS,4);
		temp[4] = 0;
		if(!strcmp(temp, GetSysVerInfo_bd()) ||  !strcmp(temp, "0000"))
		{
			OnlineListData.data[OnlineListData.deviceCnt].deviceType = OnlineListDataTemp.data[i].deviceType;
			strcpy(OnlineListData.data[OnlineListData.deviceCnt].BD_RM_MS,OnlineListDataTemp.data[i].BD_RM_MS);
			strcpy(OnlineListData.data[OnlineListData.deviceCnt].name,OnlineListDataTemp.data[i].name);
			OnlineListData.deviceCnt ++;
		}
	}
}


void MENU_092_ResSync_Init(int uMenuCnt)
{
	char disp[100];
	int disp_len;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetSavedIcon() == ICON_RES_TO_SYNC)
	{
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_RES_TO_SYNC,1, 0);
	}
	else
	{
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_RES_FROM_SYNC,1, 0);
	}

	ResSyncIconMax = GetListIconNum() - 1;
	OSD_GetIconInfo(ICON_007_PublicList1+ResSyncIconMax, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x+(hv.h - pos.x)/2, pos.y+(hv.v - pos.y)/2 - ext_font2_h/2, DISPLAY_LIST_COLOR, MESG_TEXT_Program_TIP1, 1, 0);
	memset(ResSyncToSel,0,128);
	ResSyncFromSel = 0;
	ResSyncPageSelect = 0;

	
	BusySpriteDisplay(1);
	/*if(!strcmp(GetSysVerInfo_bd(), "0000"))
	{
		API_SearchIpByFilter("0", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 128, &OnlineListDataTemp);
		OnlineListFilter();
	}
	else*/
	{
		API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_ALL, SearchDeviceRecommendedWaitingTime, 128, &OnlineListData);
	}
	if(GetSavedIcon() == ICON_RES_TO_SYNC)
	{
		memset(ResSyncToSel,1,OnlineListData.deviceCnt);
	}
	else
	{
		ResSyncToSel[0] = 1;
	}
	
	DisplayResSyncPageIcon(0);
	BusySpriteDisplay(0);
}

void MENU_092_ResSync_Exit(void)
{
}

void MENU_092_ResSync_Process(void* arg)
{
	int i;
	int ret;
	char display[200];
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&ResSyncPageSelect, ResSyncIconMax, OnlineListData.deviceCnt, (DispListPage)DisplayResSyncPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ResSyncPageSelect, ResSyncIconMax, OnlineListData.deviceCnt, (DispListPage)DisplayResSyncPageIcon);
					break;		
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					if(!OnlineListData.deviceCnt)
						return;
					ResSyncIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(ResSyncIconSelect == ResSyncIconMax)
					{
						if(GetSavedIcon() == ICON_RES_TO_SYNC)
						{
							//SaveResFileFromActFile(atoi(inputResID),GetSysVerInfo_bd(),0);
							for(i = 0;i < OnlineListData.deviceCnt;i++)
							{
								if(ResSyncToSel[i])
								{
									API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
									sprintf(display, "Sync To %s", OnlineListData.data[i].BD_RM_MS);
									API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
									ret = Api_ResSyncToDevice(8000,OnlineListData.data[i].BD_RM_MS);
									if(ret == 0)
									{
										sprintf(display, "OK");
									}
									else if(ret == -1)
									{
										sprintf(display, "NO FILE");
									}
									else
									{
										sprintf(display, "ERR");
									}
									API_OsdStringDisplayExt(pos.x+360, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
									usleep(500*1000);
								}
							}
						}
						else
						{
							API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
							sprintf(display, "Sync From %s", OnlineListData.data[ResSyncFromSel].BD_RM_MS);
							API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
							ret = Api_ResSyncFromServer(8000,OnlineListData.data[ResSyncFromSel].BD_RM_MS);
							if(ret == 0)
							{
								sprintf(display, "OK");
							}
							else if(ret == -1)
							{
								sprintf(display, "NO FILE");
							}
							else
							{
								sprintf(display, "ERR");
							}
							API_OsdStringDisplayExt(pos.x+360, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
							usleep(500*1000);
						}
						API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
						return;
					}
					
					ResSyncIndex = ResSyncPageSelect*ResSyncIconMax+ResSyncIconSelect ;
					if(ResSyncIndex>= OnlineListData.deviceCnt)
					{
						return;
					}
					if(GetSavedIcon() == ICON_RES_TO_SYNC)
					{
						if(ResSyncToSel[ResSyncIndex])
						{
							ResSyncToSel[ResSyncIndex] = 0;
						}
						else
						{
							ResSyncToSel[ResSyncIndex] = 1;
						}
					}
					else
					{
						ResSyncFromSel = ResSyncIndex;
						memset(ResSyncToSel,0,128);
						ResSyncToSel[ResSyncFromSel] = 1;
					}
					DisplayResSyncPageIcon(ResSyncPageSelect);
					break;	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}


