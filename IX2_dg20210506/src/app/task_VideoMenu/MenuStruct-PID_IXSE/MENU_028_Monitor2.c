#include "MENU_public.h"
#include "MENU_028_Monitor2.h"
#include "obj_GetIpByNumber.h"
#include "obj_business_manage.h"		//czn_20190107
#include "task_Ring.h"
#include "task_Power.h"
#include "task_survey.h"
#include "task_monitor.h"		//czn_20170112
#include "obj_memo.h"

static int dsWinId, iconNum;
	
void EnterDSMonMenu(int win_id, int stack)
{
	dsWinId = win_id;
	if(stack == 0)
	{
		VdShowHide();
	}
	StartInitOneMenu(MENU_028_MONITOR2,0,stack);
	if(stack == 0)
	{
		VdShowToFull(dsWinId);
	}
	else
	{
		SetWinMonState(dsWinId, 1);
		SetWinDeviceType(dsWinId, 0);
	}
}

int GetDs_Show_Win(void)
{
	return dsWinId;
}

int CheckTalkOnoff(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(GetDsTalkState(i))
			return -1;
	}
	return 0;
}

void MonTalkProcess(void)
{
	if(GetDsTalkState(dsWinId))
	{
		SetDsTalkState(dsWinId, 0);
		DsTalkClose(dsWinId);
		ExitQuadMenu();
		popDisplayLastMenu();
	}
	else
	{
		if(CheckTalkOnoff() == 0)
		{
			if(DsTalkDeal(dsWinId) == 0)
			{
				SetDsTalkState(dsWinId, 1);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
			}
			else		
			{
				BEEP_ERROR();
			}
		}
	}
}




const ICON_PIC_TYPE MonIconTable1[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
	ICON_PIC_UNLOCK2,
    ICON_PIC_TALK,
    //ICON_PIC_QUAD,
};
const ICON_PIC_TYPE MonIconTable2[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
    ICON_PIC_TALK,
    //ICON_PIC_QUAD,
};

static void IconListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < iconNum)
		{
			if(iconNum == 5)
			{
				pDisp->disp[pDisp->dispCnt].iconType = MonIconTable1[index];
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = MonIconTable2[index];
			}
			
			pDisp->dispCnt++;
		}
	}
}

int GetMonIconNum()
{
	return iconNum;
}

void MonIconListShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	if(lockProperty.lock1Enable && lockProperty.lock2Enable)
	{
		iconNum = 5;
		listInit.listType = ICON_1X5;
		listInit.listCnt = 5;
		listInit.fun = IconListPage;
		InitMenuListXY(listInit, 192, 536, 640, 64, 1, 0, ICON_888_ListView);

	}
	else
	{
		iconNum = 4;
		listInit.listType = ICON_1X4;
		listInit.listCnt = 4;
		listInit.fun = IconListPage;
		InitMenuListXY(listInit, 256, 536, 512, 64, 1, 0, ICON_888_ListView);
	}
}
void MonIconListClose(void)
{
	API_ListAlpha_Close(1, 192, 536, 640, 64, OSD_LAYER_CURSOR);
	MenuListDisable(0);

}

static int ds_mon_start_time=0;
void MENU_028_Monitor2_Init(int uMenuCnt)		
{
	char name_temp[41];
	GetLockProperty(GetDsIp(dsWinId), &lockProperty);
	MonIconListShow();
	SetLayerAlpha(8);
	API_TimeLapseStart(COUNT_RUN_UP , 0);
	GetDs_MonName(dsWinId, name_temp);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, COLOR_WHITE, name_temp, strlen(name_temp), CallMenuDisp_Name_Font, STR_UTF8, 0);
	//DisplayLockSprite(Ds_Menu_Para[dsWinId].dsIp);
	//Quad_Monitor_Timer_Init();
	linphone_becall_cancel();
	API_Business_Request(Business_State_MonitorIpc);
	SetDsTalkState(dsWinId, 0);
	if( GetDsTalkState(dsWinId) == 0 && api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		MonTalkProcess();
	}
	// lzh_20220411 test
	//API_LocalCaptureOn();
	ds_mon_start_time=time(NULL);
}

void MENU_028_Monitor2_Exit(void)
{
	MonIconListClose();
	API_Business_Close(Business_State_MonitorIpc);
	// lzh_20220411 test
	//API_LocalCaptureOff();
}

void MENU_028_Monitor2_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char free[20]={0};
	char cpu[30]={0};
	struct {char onOff; char win;} *data;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					MonTalkProcess();
					break;			
				case KEY_UNLOCK:
					if(lockProperty.lock1Enable)		//czn_20191123
					{
						if(DsUnlock1Deal(dsWinId) == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
						}
					}
					break;
				case KEY_UNLOCK2:
					if(lockProperty.lock2Enable)		//czn_20191123
					{
						if(DsUnlock2Deal(dsWinId) == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
						}
					}
					break;
				case KEY_POWER:
					//Monitor_DtTalkCloseReq_Deal();	
					//CloseOneMenu();
					//close_monitor_client(); 
					break;

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			if(GetCurIcon() == 0)
			{
				if(GetMenuListEn(0) == 1)
					MonIconListClose();
				else
					MonIconListShow();
			}
			else
			{
				switch(GetCurIcon())
				{
					// lzh_20181108_s
					if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
					{
						API_menu_create_bitmap_file(-1,-1,-1);				
					}
					// lzh_20181108_e
					case ICON_888_ListView:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
						if(listIcon.mainIcon == 0)
						{
							int cur_time=time(NULL);
							if((cur_time-ds_mon_start_time)<3)
								usleep(((3-(cur_time-ds_mon_start_time))*1000+500)*1000);
							if(GetDsTalkState(dsWinId))
							{
								SetDsTalkState(dsWinId, 0);
								DsTalkClose(dsWinId);
							}
							Clear_ds_show_layer(0);
							ExitQuadMenu();
							Api_Ds_Show_Stop2(0);
							popDisplayLastMenu();
							usleep(500*1000);
						}
						else if(listIcon.mainIcon == 1)
						{
							if(api_record_start(dsWinId)==0)
								SaveVdRecordProcess(dsWinId);
						}
						else if(listIcon.mainIcon == 2)
						{
							if(DsUnlock1Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
							}
						}
						else
						{
							if(iconNum == 5)
							{
								if(listIcon.mainIcon == 3)
								{
									if(DsUnlock2Deal(dsWinId) == 0) 	//czn_20161227
									{
										API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
									}
								}
								else if(listIcon.mainIcon == 4)
								{
									MonTalkProcess();
								}
							}
							else
							{
								if(listIcon.mainIcon == 3)
								{
									MonTalkProcess();
								}
							}
						}
						break;
					#if 0
					case ICON_207_MonitorPageDown:
						EnterQuadMenu(dsWinId, NULL, 0);
						break;
					case ICON_233_MonitorPageUp:
						break;
					case ICON_208_MonitorCapture:
						break;
				
					case ICON_209_MonitorUnlock1:
						if(lockProperty.lock1Enable)		//czn_20191123
						{
							if(DsUnlock1Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
							}
						}
						break;			
					case ICON_210_MonitorUnlock2:
						if(lockProperty.lock2Enable)		//czn_20191123
						{
							if(DsUnlock2Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
							}
						}
						break;
					case ICON_211_MonitorTalk:
						if(Ds_Menu_Para[dsWinId].talk_flag)
						{
							Ds_Menu_Para[dsWinId].talk_flag = 0;
							DsTalkClose(dsWinId);
							ExitQuadMenu();
							popDisplayLastMenu();
						}
						else
						{
							if(CheckTalkOnoff() == 0)
							{
								Ds_Menu_Para[dsWinId].talk_flag = 1;
								DsTalkDeal(dsWinId);
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
							}
						}
						break;
						
					case ICON_212_MonitorReturn:
						//Monitor_DtTalkCloseReq_Deal();	//czn_20190529
						ExitQuadMenu();
						popDisplayLastMenu();
						break;
					#endif	
				}	
								
			}

		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{

			case MSG_7_BRD_SUB_MonitorOff:
				if(GetDsTalkState(dsWinId))
				{
					SetDsTalkState(dsWinId, 0);
					DsTalkClose(dsWinId);
				}
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_GetCpuMemUse:
				GetCpuMem_Polling(free, cpu);
				API_OsdStringDisplayExt(bkgd_w-100, 10, DISPLAY_STATE_COLOR, free, strlen(free), 0, STR_UTF8, 0);	
				API_OsdStringDisplayExt(bkgd_w-250, 50, DISPLAY_STATE_COLOR, cpu, strlen(cpu), 0, STR_UTF8, 0); 
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(data->onOff, data->win);
				#if 0
				if(!data->onOff)
					SaveVdRecordProcess(dsWinId);
				#endif
				break;

			case MSG_7_BRD_SUB_MonitorStartting:
				//API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
				break;
				
			
			case MSG_7_BRD_SUB_MonitorTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;
			
			case MSG_7_BRD_SUB_MonitorUnlock1:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				break;

			case MSG_7_BRD_SUB_MonitorUnlock2:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				break;
				
			// lzh_20191009_s
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP:
				// 插簧摘机
				if(GetDsTalkState(dsWinId) == 0)
				{
					MonTalkProcess();
				}
				else
				{
					// 通话状态下，进入手柄通话状态并刷新音频通道切换
					api_plugswitch_control_update(2,1);
				}
				break;
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN:
				if(GetDsTalkState(dsWinId) == 1)
				{
					MonTalkProcess();
				}
				break;

			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
	
}

