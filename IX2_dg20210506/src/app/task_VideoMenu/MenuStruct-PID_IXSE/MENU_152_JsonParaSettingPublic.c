#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "MENU_152_JsonParaSettingPublic.h"

JsonParaSettingMenuDispFunc_T JsonParaSettingMenuItemDispFunc = NULL;
JsonParaSettingMenuDispFunc_T JsonParaSettingMenuValueDispFunc = NULL;


JsonParaSettingSelectFunc_T JsonParaSettingSelectFunc = NULL;

JsonParaSettingReturnFunc_T JsonParaSettingReturnFunc = NULL;

JsonParaSettingSaveFunc_T JsonParaSettingSaveFunc = NULL;

cJSON *JsonParaSettingCurNode = NULL;
int JsonParaSettingCurNodeIndex = 0;
int JsonParaSettingCurItemNums = 0;
int JsonParaSettingCurPage = 0;
int JsonParaSettingCurLine = 0;
int JsonParaSettingToKeypad = 0;
int JsonParaSettingHaveChange = 0;
int JsonParaSettingDispMode = 0;

char JsonParaSettingTitleStr[40];
int JsonParaSettingTitleUnicodeLen = 0;



void Set_JsonParaSettingMenuItemDispFunc(JsonParaSettingMenuDispFunc_T func)
{
	JsonParaSettingMenuItemDispFunc = func;
}

void Set_JsonParaSettingMenuValueDispFunc(JsonParaSettingMenuDispFunc_T func)
{
	JsonParaSettingMenuValueDispFunc = func;
}

void Set_JsonParaSettingMenuSelectFunc(JsonParaSettingSelectFunc_T func)
{
	JsonParaSettingSelectFunc = func;
}

void Set_JsonParaSettingMenuReturnFunc(JsonParaSettingReturnFunc_T func)
{
	JsonParaSettingReturnFunc = func;
}
void Set_JsonParaSettingMenuSaveFunc(JsonParaSettingSaveFunc_T func)
{
	JsonParaSettingSaveFunc = func;
}

void Set_JsonParaSettingDispMode(int mode)
{
	JsonParaSettingDispMode = mode;
}

void Set_JsonParaSettingHaveChange(int val)
{
	JsonParaSettingHaveChange = val;
}

void Set_JsonParaSettingToKeypad(int val)
{
	JsonParaSettingToKeypad = val;
}

void Set_JsonParaSettingCurItemNums(int val)
{
	JsonParaSettingCurItemNums = val;
}

void Set_JsonParaSettingCurPage(int val)
{
	JsonParaSettingCurPage = val;
}

void Set_JsonParaSettingCurLine(int val)
{
	JsonParaSettingCurLine = val;
}

void Set_JsonParaSettingCurNodeIndex(int val)
{
	JsonParaSettingCurNodeIndex= val;
}

int Get_JsonParaSettingCurNodeIndex(void)
{
	return JsonParaSettingCurNodeIndex;
}

void Set_JsonParaSettingCurNode(cJSON *one_node)
{
	JsonParaSettingCurNode = one_node;
}

cJSON* Get_JsonParaSettingCurNode(void)
{
	return JsonParaSettingCurNode;
}

void Set_JsonParaSettingTitle(int len,char *title)
{
	JsonParaSettingTitleUnicodeLen = len;
	
	if(title!=NULL)
	{
		if(len>0)
			memcpy(JsonParaSettingTitleStr,title,len);
		else
			strcpy(JsonParaSettingTitleStr,title);
	}
	else
	{
		JsonParaSettingTitleStr[0]=0;
	}
}

static void DisplayJsonParaSettingPage(int page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char item_disp[100];
	int item_disp_unicode_len=0;
	char value_disp[100];
	int value_disp_unicode_len=0;
	int select_flag;

	
	// lzh_20181016_s	
	//API_DisableOsdUpdate();
	// lzh_20181016_e
	//printf("11111%s:%d\n",__func__,__LINE__);
	API_OsdStringClearExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_WIDTH, 40);
	//printf("11111%s:%d\n",__func__,__LINE__);
	if(JsonParaSettingTitleUnicodeLen>0||strlen(JsonParaSettingTitleStr)>0)
		API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleStr, (JsonParaSettingTitleUnicodeLen>0)?JsonParaSettingTitleUnicodeLen:strlen(JsonParaSettingTitleStr), 1, (JsonParaSettingTitleUnicodeLen>0)?STR_UNICODE:STR_UTF8, 0);
	#if 0
	if(JsonParaSettingTitleText!=0)
	{
		API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleText, 1, DISPLAY_TITLE_WIDTH);
	}
	else if(strlen(JsonParaSettingTitleStr)>0)
	{
		API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleStr, strlen(JsonParaSettingTitleStr), 1, STR_UTF8, DISPLAY_TITLE_WIDTH);
	}
	#endif
	//printf("11111%s:%d\n",__func__,__LINE__);
	for(i = 0; i < JsonParaSetting_ICON_MAX; i++)
	{
		printf("11111%s:%d:%d\n",__func__,__LINE__,i);
		
		//x = GetIconXY(ICON_007_PublicList1+i).x+DISPLAY_DEVIATION_X;
		//y = GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;
		x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		ListSelect(i,0);
		if(page*JsonParaSetting_ICON_MAX+i < JsonParaSettingCurItemNums)
		{
			if(JsonParaSettingDispMode == 0)
			{
				select_flag = 0;
				item_disp[0]= 0;
				value_disp[0]=0;
				item_disp_unicode_len = 0;
				value_disp_unicode_len = 0;
				if(JsonParaSettingMenuItemDispFunc!= NULL)
					JsonParaSettingMenuItemDispFunc(page*JsonParaSetting_ICON_MAX+i,item_disp,&item_disp_unicode_len);
				if(JsonParaSettingMenuValueDispFunc!= NULL)
					JsonParaSettingMenuValueDispFunc(page*JsonParaSetting_ICON_MAX+i,value_disp,&value_disp_unicode_len);
				
			}
			else
			{
				select_flag = 0;
				item_disp[0]= 0;
				value_disp[0]=0;
				item_disp_unicode_len = 0;
				value_disp_unicode_len = 0;
				if(JsonParaSettingMenuItemDispFunc!= NULL)
					select_flag = JsonParaSettingMenuItemDispFunc(page*JsonParaSetting_ICON_MAX+i,item_disp,&item_disp_unicode_len);
				//printf("!!!!!ListSelect %d :%d\n",i,select_flag);
				ListSelect(i,select_flag);
			}
			//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setGeneralIconTable[i+page*SetGeneral_ICON_MAX].iConText, 1, DISPLAY_ICON_X_WIDTH);
			if(item_disp_unicode_len>0||strlen(item_disp)>0)
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, item_disp, (item_disp_unicode_len>0)?item_disp_unicode_len:strlen(item_disp), 1, (item_disp_unicode_len>0)?STR_UNICODE:STR_UTF8, 0);
			x += 300;
			if(value_disp_unicode_len>0||strlen(value_disp)>0)
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, value_disp, (value_disp_unicode_len>0)?value_disp_unicode_len:strlen(value_disp), 1, (value_disp_unicode_len>0)?STR_UNICODE:STR_UTF8, 0);
			
		}
	}
	pageNum = JsonParaSettingCurItemNums/JsonParaSetting_ICON_MAX + (JsonParaSettingCurItemNums%JsonParaSetting_ICON_MAX ? 1 : 0);
	//printf("11111%s:%d\n",__func__,__LINE__);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	//printf("11111%s:%d\n",__func__,__LINE__);
	// lzh_20181016_s	
	//API_EnableOsdUpdate();
	// lzh_20181016_e	
}
void JsonParaSettingDispReflush(void)
{
	API_DisableOsdUpdate();
	//printf("222222222%s:%d\n",__func__,__LINE__);
	DisplayJsonParaSettingPage(JsonParaSettingCurPage);
	API_EnableOsdUpdate();
}
void MENU_152_JsonParaSettingPublic_Init(int uMenuCnt)
{
	JsonParaSettingToKeypad = 0;
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	API_DisableOsdUpdate();
	DisplayJsonParaSettingPage(JsonParaSettingCurPage);
	API_EnableOsdUpdate();
}




void MENU_152_JsonParaSettingPublic_Exit(void)
{
	if(JsonParaSettingToKeypad == 0&&JsonParaSettingHaveChange)
	{
		if(JsonParaSettingSaveFunc!= NULL)
		{
			JsonParaSettingSaveFunc();
		}
	}
}

void MENU_152_JsonParaSettingPublic_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	int rev;
	int i;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(JsonParaSettingReturnFunc!= NULL)
					{
						JsonParaSettingReturnFunc();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				
				case ICON_201_PageDown:
					PublicPageDownProcess(&JsonParaSettingCurPage, JsonParaSetting_ICON_MAX, JsonParaSettingCurItemNums, (DispListPage)DisplayJsonParaSettingPage);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&JsonParaSettingCurPage, JsonParaSetting_ICON_MAX, JsonParaSettingCurItemNums, (DispListPage)DisplayJsonParaSettingPage);
					break;		
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					
					JsonParaSettingCurLine = GetCurIcon() - ICON_007_PublicList1;
					i = JsonParaSettingCurPage*JsonParaSetting_ICON_MAX + JsonParaSettingCurLine;
					if(i >= JsonParaSettingCurItemNums)
					{
						return;
					}

					if(JsonParaSettingSelectFunc!= NULL)
					{
						if(JsonParaSettingDispMode == 1)
						{
							rev = JsonParaSettingSelectFunc(i);
							ListSelect(JsonParaSettingCurLine,rev);
						}
						else
						{
							JsonParaSettingSelectFunc(i);
						}
					}
					break;
				

			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


