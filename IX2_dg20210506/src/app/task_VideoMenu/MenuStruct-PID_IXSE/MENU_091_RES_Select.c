#include "MENU_098_RES_Manger.h"
#include "MENU_091_RES_Select.h"

//#define	ResSelIconMax		5
#define   RES_FILE_MAX			20
int ResSelIconMax;
int RES_IdPageSelect;
int ResIdSelect;
int ResIdNum;
typedef struct
{
	int ResFileId;
	char ResFileName[100];
}ResFileTable_T;
ResFileTable_T ResFileTable[RES_FILE_MAX];

int Get_Res_FromSd(void)
{
	struct dirent *entry;
    	DIR *dir;
	char cmd_buff[200];
	char temp[5];
	
	ResIdNum = 0;
	if( access( SDCARD_RES_PATH, F_OK ) < 0 )
	{
		return 0;
	}
	
    	dir = opendir(SDCARD_RES_PATH);
	while ((entry=readdir(dir))!=NULL)
	{
		if(strstr(entry->d_name,"R8") != NULL)
		{
			memcpy(temp,entry->d_name+1,4);
			temp[4] = 0;
			ResFileTable[ResIdNum].ResFileId = atoi(temp);
			strcpy(ResFileTable[ResIdNum].ResFileName, entry->d_name);
			ResIdNum++;
			if(ResIdNum>= RES_FILE_MAX)
				break;
		}
	}
	closedir(dir);	
	return ResIdNum;
}

void Restore_Res_FromSd(char* fileName, int resId)
{
	char cmd_buff[400];
	char dir1[200], dir2[200];
	int ret;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	if(resId==8100)	//Customer PACK  dh_20190910
	{
		if(strstr(fileName, ".zip") != NULL)
		{
			strcpy(dir1, CustomerFileDir);
			snprintf(cmd_buff, 400, "unzip -o %s/%s -d %s", SDCARD_RES_PATH, fileName, dirname(dir1));
		}
		else if(strstr(fileName, ".tar") != NULL)
		{
			strcpy(dir1, CustomerFileDir);
			while(dir1[strlen(dir1)-1] == '/')
			{
				dir1[strlen(dir1)-1] = 0;
			}
			strcpy(dir2, dir1);

			snprintf(cmd_buff, 400, "tar -xvf %s%s -C %s %s", SDCARD_RES_PATH, fileName, dirname(dir1), basename(dir2));
		}
		dprintf("%s\n", cmd_buff);
		system(cmd_buff);
		ReloadCustomerFile();
	}
	else	//RES FILE
	{
		CheckResFileDir();
		CheckResIdFile(resId);
		snprintf(cmd_buff,400, "cp %s/%s %s", SDCARD_RES_PATH,fileName, ResFileDir);
		system(cmd_buff);
		if(resId==8000)	//RES PACK
		{
			CheckPublicResFileDir();
			ResUnzipToActFile();
		}
		else
		{
			MakeDir(PublicResFileDir);

			snprintf(cmd_buff,400, "cp %s/%s %s", SDCARD_RES_PATH,fileName, PublicResFileDir);
			system(cmd_buff);
			CheckResIdFile(8000);
			sprintf(fileName,"R8000.tar");

			strcpy(dir1, PublicResFileDir);
			while(dir1[strlen(dir1)-1] == '/')
			{
				dir1[strlen(dir1)-1] = 0;
			}
			strcpy(dir2, dir1);

			snprintf(cmd_buff,400, "tar -cvf %s/%s -C %s %s", ResFileDir, fileName, dirname(dir1), basename(dir2));
			system(cmd_buff);
			sync();

			ret = ResReloadById(resId,GetSysVerInfo_BdRmMs());
			if(ret != 0)
			{
				API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,(ret == -1 ? MESG_TEXT_UNUSED_RES : MESG_TEXT_UNMATCHED_RES),1, 0);								
				sleep(1);
				API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
				return;
			}
		}
	}
	UpdateOkSpriteNotify();
}

char* GetOnlineManageNbr(void);

void Trans_Res_FromSd(char* fileName, int resId)
{
	char cmd_buff[200];
	int ret;
	if(resId==8100)	//Customer PACK  dh_20190910
	{
	#if 0
		if(strstr(fileName, ".zip") != NULL)
		{
			snprintf(cmd_buff, 200, "unzip -o %s%s -d /mnt/nand1-2/", SDCARD_RES_PATH, fileName);
		}
		else if(strstr(fileName, ".tar.gz") != NULL)
		{
			snprintf(cmd_buff, 200, "tar -zxvf %s%s -C /mnt/nand1-2/ Customerized/", SDCARD_RES_PATH, fileName);
		}
		system(cmd_buff);
		ReloadCustomerFile();
	#endif
		BusySpriteDisplay(1);
		if(Api_ResSyncToDeviceAppointDirFile(8100,GetOnlineManageNbr(),GetSearchOnlineIp(),SDCARD_RES_PATH,fileName) == 0)
		{
			BEEP_CONFIRM();
		}
		else
		{
			BEEP_ERROR();
		}
		BusySpriteDisplay(0);
	}
}
static void DisplayResSelectPageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	API_DisableOsdUpdate();
	for(i = 0; i < ResSelIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		ListSelect(i,0);
		API_OsdStringClearExt(x, y, hv.h - x, CLEAR_STATE_H);
		index = page*ResSelIconMax+i;
		if(index < ResIdNum)
		{
			sprintf(display,"%s",ResFileTable[index].ResFileName);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
			if(index == ResIdSelect)
			{
				ListSelect(i,1);
			}
		}
	}

	pageNum = ResIdNum/ResSelIconMax + (ResIdNum%ResSelIconMax ? 1 : 0);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	//MenuListNum_Display(MENULIST_NUM_POS_X, MENULIST_NUM_POS_Y,COLOR_RED,ResIdNum);
	API_EnableOsdUpdate();
	
}

void MENU_091_ResSelect_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_SelectRES_ID,1, 0);
	ResIdSelect = 0;
	RES_IdPageSelect = 0;
	ResIdNum = 0;
	Get_Res_FromSd();
	ResSelIconMax = GetListIconNum();
	DisplayResSelectPageIcon(RES_IdPageSelect);
}

void MENU_091_ResSelect_Exit(void)
{

}

void MENU_091_ResSelect_Process(void* arg)
{
	int i;
	int curIcon,CurSelectIndex;
	char idInput[10] = {0};
	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					
					PublicPageDownProcess(&RES_IdPageSelect, ResSelIconMax, ResIdNum, (DispListPage)DisplayResSelectPageIcon);
					break;			
				
				case ICON_202_PageUp:
					
					PublicPageUpProcess(&RES_IdPageSelect, ResSelIconMax, ResIdNum, (DispListPage)DisplayResSelectPageIcon);
					break;		
				
				case ICON_007_PublicList1:
			
				case ICON_008_PublicList2:
				
				case ICON_009_PublicList3:
				
				case ICON_010_PublicList4:
				
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					curIcon = GetCurIcon() - ICON_007_PublicList1;
					ResIdSelect = RES_IdPageSelect*ResSelIconMax+curIcon;
					if( ResIdSelect >= ResIdNum)
					{
						return;
					}
					for(i = 0; i < ResSelIconMax; i++)
					{
						ListSelect(i, 0);	
						if(RES_IdPageSelect*ResSelIconMax+i == ResIdSelect)
						{
							ListSelect(i, 1);
						}
					}
					if(GetLastNMenu() == MENU_022_ONLINE_MANAGE)
					{
						Trans_Res_FromSd(ResFileTable[ResIdSelect].ResFileName, ResFileTable[ResIdSelect].ResFileId);
					}
					else
					{
						Restore_Res_FromSd(ResFileTable[ResIdSelect].ResFileName, ResFileTable[ResIdSelect].ResFileId);
						usleep(500*1000);
					}
					popDisplayLastMenu();
					break;		
					
			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


