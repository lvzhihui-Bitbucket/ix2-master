#include "MENU_public.h"
#include "../../task_monitor/task_monitor.h"	
#include "../../onvif_service/onvif_discovery.h"
#include "../../task_monitor/obj_ip_mon_vres_map_table.h"
#include "../../task_survey/obj_business_manage.h"		//czn_20190107
#include "../../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"


#define	CALL_IPC_ICON_MAX	5
int callIpcIconSelect;
int callIpcPageSelect;
int callIpcIndex;
extern IPC_RECORD saveIPCMonList; 
static int callIpcMaxNum = 0;
extern onvif_login_info_t info;

void DisplayOnePageCallIpclist(uint8 page)
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	callIpcMaxNum = Get_IPC_MonRes_Num();
	
	for( i = 0; i < CALL_IPC_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		list_start = page*CALL_IPC_ICON_MAX+i;
		if(list_start< callIpcMaxNum)
		{
			IPC_RECORD dat;
			char display[51];
			
			Get_IPC_MonRes_Record(list_start, &dat);
			snprintf(display, 51, "[IPC]%s", dat.name);
			
			API_OsdStringDisplayExt(x+32, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, 0);
		}
		
	}
	
	
	maxPage = (callIpcMaxNum%CALL_IPC_ICON_MAX) ? (callIpcMaxNum/CALL_IPC_ICON_MAX+1) : (callIpcMaxNum/CALL_IPC_ICON_MAX);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

int CallIpcListStartMonitor(int index)
{
#if 0
	IPC_RECORD dat;
	int ret = -1;

	if(index < Get_IPC_MonRes_Num())
	{
		Get_IPC_MonRes_Record(index, &dat);
		if(onvif_check_online(dat.devUrl, dat.id) != 0)
		{
			return ret;
		}
		//czn_20190107_s
		
		API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

		
		if(CallServer_Run.videoSource == VIDEO_SOURCE_IPC)
		{
			api_onvif_stop_mointor_one_stream(0);
		}
		else if(CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)
		{
			//close_monitor_client1();
		}
		
		strncpy(CallServer_Run.ipcVideoName, dat.name, 42);
		
		//czn_20190107_e
		if(onvif_get_rtsp_rul_by_deviceUrl(dat.devUrl, dat.userName, dat.userPwd, dat.channel, &CallServer_Run.ipcInfo) != -1)
		{
			if(api_onvif_start_mointor_one_stream(0, CallServer_Run.ipcInfo.rtsp_url, CallServer_Run.ipcInfo.channel, CallServer_Run.ipcInfo.width, CallServer_Run.ipcInfo.height, 0 ) == 0)
			{
				CallServer_Run.videoSource = VIDEO_SOURCE_IPC;
				ret = 0;
			}
		}
		
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	}

	return ret;
#endif
}


void MENU_110_CallIPC_Init(int uMenuCnt)
{
	if(GetLastNMenu() == MENU_027_CALLING2)
	{
		callIpcIconSelect = 0;
		callIpcPageSelect = 0;
	}
	API_DisableOsdUpdate();
	DisplayOnePageCallIpclist(callIpcPageSelect);
	API_EnableOsdUpdate();

}

void MENU_110_CallIPC_Exit(void)
{
	
}

void MENU_110_CallIPC_Process(void* arg)
{


	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int updateCnt,IpcupdateCnt,x,y;	//czn_20190221
	char display[101];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					callIpcIconSelect = GetCurIcon() - ICON_007_PublicList1;
					callIpcIndex = callIpcPageSelect*CALL_IPC_ICON_MAX + callIpcIconSelect;
				
					if(callIpcIndex >= callIpcMaxNum)
					{
						return;
					}
					if(CallIpcListStartMonitor(callIpcIndex) == 0)
					{
						popDisplayLastMenu();
					}
					else
					{
						API_Beep(BEEP_TYPE_DI3);
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&callIpcPageSelect, CALL_IPC_ICON_MAX, callIpcMaxNum, (DispListPage)DisplayOnePageCallIpclist);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&callIpcPageSelect, CALL_IPC_ICON_MAX, callIpcMaxNum, (DispListPage)DisplayOnePageCallIpclist);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_BecalledOff:						
				if(CallServer_Run.state != CallServer_Wait && CallServer_Run.rule_act == 0&&CallServer_Run.call_type == IxCallScene1_Passive)
				{
					API_CallServer_LocalBye(CallServer_Run.call_type);
				}
				
				API_TimeLapseHide();
				API_TimeLapseStop();
				
				if(judge_autotest_targetIsself()!=0&&GetLastNMenu() ==MENU_086_CallTestStat)
					popDisplayLastMenu();
				else
					CloseOneMenu();
				break;

			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}


	
}



