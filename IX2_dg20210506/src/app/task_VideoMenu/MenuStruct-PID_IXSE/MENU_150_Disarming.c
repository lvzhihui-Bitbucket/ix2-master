#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"
#include "cJSON.h"
#include"obj_AlarmingServey.h"

const KeyProperty DisarmKbKey[]= 
{
	{ICON_ArmingKb1,	  '1'},
	{ICON_ArmingKb2,	  '2'},
	{ICON_ArmingKb3,	  '3'},
	{ICON_ArmingKb4,	  '4'},
	{ICON_ArmingKb5,	  '5'},
	{ICON_ArmingKb6,	  '6'},
	{ICON_ArmingKb7,	  '7'},
	{ICON_ArmingKb8,	  '8'},
	{ICON_ArmingKb9,	  '9'},
	{ICON_ArmingKb0,	  '0'},
	{ICON_ArmingKbDel,	  '<'},
};
const int DisarmKbKeyNums = sizeof(DisarmKbKey)/sizeof(DisarmKbKey[0]);
#define MaxDisarmPwdLength	4
#define InputPwdDispX		20
#define InputPwdDispY			20
char DisarmPwdInput[MaxDisarmPwdLength+1];
int DisarmPwdInputLength = 0;
int Disarmed_Delay_Confirm = 0;
int DisarmMenuSub = 0;

void DisarmPwdDisplay(void)
{
	char disp[80];
	int disp_len;
	int i;
	
	API_OsdStringClearExt(472, 25,328,40);
	if(DisarmPwdInputLength>0)
	{
		memset(disp,'*',DisarmPwdInputLength);
		disp[DisarmPwdInputLength] = 0;
		API_OsdStringCenterDisplayExt(606, 25, COLOR_WHITE, disp, strlen(disp),1,STR_UTF8,418,40);
	}
	else
	{
		
		//strcpy(disp,"Input Password");
		API_GetOSD_StringWithID(MESG_TEXT_InputManagerPassword,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(606, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,418,40);
	}
	//API_OsdStringDisplayExt(InputPwdDispX, InputPwdDispY, DISPLAY_LIST_COLOR, disp, strlen(disp),1,STR_UTF8,0);
	//API_OsdStringCenterDisplayExt(472, 25, COLOR_WHITE, disp, strlen(disp),1,STR_UTF8,328,40);
	if(DisarmPwdInputLength>0)
	{
		OSD_GetStringWithIcon(ICON_DisarmingSetting,disp,&disp_len);
		API_OsdStringCenterDisplayExt(GetIconXY(ICON_Disarming).x, GetIconXY(ICON_Disarming).y+32, COLOR_WHITE, disp,disp_len,2,STR_UNICODE,220,40);
	}
	else
	{
		API_GetOSD_StringWithID(MESG_TEXT_AlarmingZone,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(GetIconXY(ICON_Disarming).x, GetIconXY(ICON_Disarming).y+32, COLOR_WHITE, disp,disp_len,2,STR_UNICODE,220,40);
	}
}

int DisarmPwdVerify(void)
{
	if(DisarmPwdInputLength==0)
		return 0;
	if(strcmp(DisarmPwdInput,"1234")==0)
		return 1;

	return 0;
}
cJSON *AlarmRecordDispBuff = NULL;
#define MaxAlarmRecordDisp	8
void AlarmingHappenMenuDisp(char* disp)
{
	if(AlarmRecordDispBuff == NULL)
		AlarmRecordDispBuff = cJSON_CreateArray();
	cJSON *one_str = cJSON_CreateString(disp);
	cJSON_InsertItemInArray(AlarmRecordDispBuff,0,one_str);
	while(cJSON_GetArraySize(AlarmRecordDispBuff)>MaxAlarmRecordDisp)
	{
		cJSON_DeleteItemFromArray(AlarmRecordDispBuff,cJSON_GetArraySize(AlarmRecordDispBuff)-1);
	}
}
void AlarmingHappenMenuDispClean(void)
{
	if(AlarmRecordDispBuff != NULL)
	{
		cJSON_Delete(AlarmRecordDispBuff);
		AlarmRecordDispBuff = NULL;
	}
}
void LastArmedRecordDispRecover(void)
{
	char disp[200];
	int last_index;
	AlarmingHappenMenuDispClean();
	last_index=MaxAlarmRecordDisp;
	while(last_index-->0)
	{
		if(GetLastAlarmRecordDisp(last_index,disp)==0)
		{
			AlarmingHappenMenuDisp(disp);
		}
	}
}
void AlarmingHappenMenuDispUpdate(void)
{
	int i;
	char *disp;
	char armed_info[100];
	char disp_buff[80];
	int disp_len;
	char *pr;
	#if 0
	if(cJSON_GetArraySize(AlarmRecordDispBuff)==0)
	{
		API_OsdStringDisplayExt(20, 90, DISPLAY_LIST_COLOR, "Have no Alarming Event!", strlen("Have no Alarming Event!"),0,STR_UTF8,0);
	}
	else
	#endif
	{
		GetCurArmInfoDisp(armed_info);
		API_OsdStringClearExt(20, 90, 380,35);
		API_OsdStringDisplayExt(20, 90, DISPLAY_LIST_COLOR, armed_info, strlen(armed_info),2,STR_UTF8,472);
		for(i=0;i<MaxAlarmRecordDisp;i++)
		{
			API_OsdStringClearExt(20, 90+35*(i+1), 380,35);
			if(i<cJSON_GetArraySize(AlarmRecordDispBuff))
			{
				disp = cJSON_GetStringValue(cJSON_GetArrayItem(AlarmRecordDispBuff,i));
				
				if((pr = strstr(disp,"happen a alarming"))!=NULL)
				{
					API_GetOSD_StringWithID(MESG_TEXT_HappenAAlarming,disp,pr-disp,NULL,0,disp_buff,&disp_len);
					API_OsdStringDisplayExt(20, 90+35*(i+1), DISPLAY_LIST_COLOR, disp_buff, disp_len,2,STR_UNICODE,472);
				}
				else
					API_OsdStringDisplayExt(20, 90+35*(i+1), DISPLAY_LIST_COLOR, disp, strlen(disp),2,STR_UTF8,472);
			}
		}
	}
}
void SetDisarmMenuSub(int val)
{
	DisarmMenuSub = val;
}

void MENU_150_Disarming_Init(int uMenuCnt)
{
	char disp[80];
	int disp_len;
	
	if(GetLastNMenu() != MENU_150_Disarming&&GetLastNMenu() != MENU_153_DisarmingSubKeyPad&&GetLastNMenu() != MENU_154_DisarmingSubZoneDisp)
	{
		Disarmed_Delay_Confirm = 0;
	}
	DisarmPwdInputLength=0;
	memset(DisarmPwdInput,0,MaxDisarmPwdLength+1);
	if(DisarmMenuSub == 0)
	{
		DisarmPwdDisplay();
		API_DisableOsdUpdate();
		API_GetOSD_StringWithID(MESG_TEXT_AlarmTile,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(0, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,606,40);
		//API_OsdStringCenterDisplayExt(472, 25, COLOR_WHITE, "Disarmed", strlen("Disarmed"),1,STR_UTF8,328,40);
		//API_OsdStringDisplayExt(20, 90, DISPLAY_LIST_COLOR, "Have no Alarming Event!", strlen("Have no Alarming Event!"),0,STR_UTF8,0);
		API_GetOSD_StringWithID(MESG_TEXT_AlarmingZone,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(GetIconXY(ICON_Disarming).x, GetIconXY(ICON_Disarming).y+32, COLOR_WHITE, disp,disp_len,2,STR_UNICODE,220,40);
		AlarmingHappenMenuDispUpdate();
		API_EnableOsdUpdate();
	}
	else
	{
		API_DisableOsdUpdate();
		API_GetOSD_StringWithID(MESG_TEXT_AlarmTile,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(0, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,606,40);
		API_GetOSD_StringWithID(MESG_TEXT_Armed,NULL,0,NULL,0,disp,&disp_len);
		API_OsdStringCenterDisplayExt(606, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,418,40);
		//DisplayAlarmingZoneState();
		if(GetAlarmingState()==AlarmingSurvey_State_24hAlarming)
		{
			DisplayAlarmingZoneState(-1);
		}
		else
		{
			DisplayAlarmingZoneState(GetAlarmingArmMode());
		}
		OSD_GetStringWithIcon(ICON_DisarmingSetting,disp,&disp_len);
		API_OsdStringCenterDisplayExt(GetIconXY(ICON_Disarming).x, GetIconXY(ICON_Disarming).y+32, COLOR_WHITE, disp, disp_len,2,STR_UNICODE,220,40);
		AlarmingHappenMenuDispUpdate();
		API_EnableOsdUpdate();
	}
	
	#if 0
	DisplayAlarmingZoneState();
	if(GetAlarmingState()==0)
	{
		API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Arming", strlen("Arming"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
	}
	else 
	{
		API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Disarming", strlen("Disarming"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
	}
	//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_Icon_014_GuardStation, 1, DISPLAY_TITLE_WIDTH);
	#endif
}




void MENU_150_Disarming_Exit(void)
{
	
}

void MENU_150_Disarming_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	int armed_delay;
	int i;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				case ICON_Disarming:
					if(DisarmMenuSub == 0)
					{
						if(DisarmPwdInputLength == 0)
						{
							SetDisarmMenuSub(1);
							StartInitOneMenu(MENU_154_DisarmingSubZoneDisp,0,0);
							return;
						}
						if(DisarmPwdVerify() == 1)
						{
							if(GetAlarmingState()==AlarmingSurvey_State_24hAlarming)
							{
								API_24hZoneAlarming_Confirm();
							}
							else
							{
								API_Alarming_Disarming_NormalZone();
							}
							BEEP_CONFIRM();
							AlarmingHappenMenuDispClean();
							usleep(100*1000);
							popDisplayLastMenu();
						}
						else
						{
							BEEP_ERROR();
							MessageReportDisplay("Please enter the correct password!", 0, NULL, COLOR_RED, 1, 10);
							sleep(1);
							MessageClose();
							DisarmPwdInputLength=0;
							memset(DisarmPwdInput,0,MaxDisarmPwdLength+1);
							API_DisableOsdUpdate();
							DisarmPwdDisplay();
							API_EnableOsdUpdate();
						}
					}
					else
					{
						SetDisarmMenuSub(0);
						StartInitOneMenu(MENU_153_DisarmingSubKeyPad,0,0);
					}
					break;
				case ICON_201_PageDown:
					
					break;			
				case ICON_202_PageUp:
					
					break;	
				case ICON_AlarmSettingExit:
					popDisplayLastMenu();
					break;
				default:
					for(i = 0;i<DisarmKbKeyNums;i++)
					{
						if(DisarmKbKey[i].icon == GetCurIcon())
						{
							if(DisarmKbKey[i].key_value=='<')
							{
								if(DisarmPwdInputLength>0)
								{
									DisarmPwdInput[--DisarmPwdInputLength] = 0;
									API_DisableOsdUpdate();
									DisarmPwdDisplay();
									API_EnableOsdUpdate();
								}
							}
							else if(DisarmPwdInputLength<MaxDisarmPwdLength)
							{
								DisarmPwdInput[DisarmPwdInputLength++] = DisarmKbKey[i].key_value;
								API_DisableOsdUpdate();
								DisarmPwdDisplay();
								API_EnableOsdUpdate();
							}
						}
					}
					break;

			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_AlarmingHappen:
				Disarmed_Delay_Confirm =1;
				MessageClose();
				AlarmingHappenMenuDisp((char*)(arg+sizeof(SYS_WIN_MSG)));
				API_DisableOsdUpdate();
				AlarmingHappenMenuDispUpdate();
				API_EnableOsdUpdate();
				break;
			case MSG_7_BRD_Disarmed_Delay:
				if(GetAlarmingState()==1&&Disarmed_Delay_Confirm==0)
				{
						
					armed_delay = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					if(armed_delay>0)
					{
						sprintf(display," %ds",armed_delay);
						MessageReportDisplay(NULL, MESG_TEXT_EffectiveAfter, display, COLOR_RED, 1, 10);
						BEEP_KEY();
						//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,display,strlen(display),1,STR_UTF8,0);		
					}
					else
					{
						MessageClose();
						//API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y,480-DISPLAY_STATE_X,30);
						//BEEP_CONFIRM();
						//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Disarming", strlen("Disarming"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
						//popDisplayLastMenu();
					}
				}
				else if(GetAlarmingState()==1&&Disarmed_Delay_Confirm==1)
				{
					armed_delay = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					if(armed_delay>0)
					{
						//sprintf(display," %ds",armed_delay);
						//MessageReportDisplay(NULL, MESG_TEXT_EffectiveAfter, display, COLOR_RED, 1, 10);
						BEEP_KEY();
						//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,display,strlen(display),1,STR_UTF8,0);		
					}
					else
					{
						MessageClose();
						//API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y,480-DISPLAY_STATE_X,30);
						//BEEP_CONFIRM();
						//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Disarming", strlen("Disarming"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
						//popDisplayLastMenu();
					}
				}
				else if(GetAlarmingState()!=1)
				{
					Disarmed_Delay_Confirm=0;
				}
				break;
			case MSG_7_BRD_MsgClose_TouchClick:
				Disarmed_Delay_Confirm = 1;
				MessageClose();
				if(DisarmMenuSub == 1)
				{
					SetDisarmMenuSub(0);
					StartInitOneMenu(MENU_153_DisarmingSubKeyPad,0,0);
				}
				break;
			case MSG_7_BRD_AlarmingSenserUpdate:
				if(DisarmMenuSub == 1)
				{
					if(GetAlarmingState()==AlarmingSurvey_State_24hAlarming)
					{
						DisplayAlarmingZoneState(-1);
					}
					else
					{
						DisplayAlarmingZoneState(GetAlarmingArmMode());
					}
				}
				break;	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


