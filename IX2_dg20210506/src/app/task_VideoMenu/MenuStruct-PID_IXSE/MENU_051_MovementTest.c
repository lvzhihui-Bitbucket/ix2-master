#include "MENU_051_MovementTest.h"
#include "task_Power.h"
#include "task_Ring.h"
#include "obj_GetIpByNumber.h"
#include "task_WiFiConnect.h"

#define TEST_RESULT_DISPLAY_X		800
#define TEST_DISPLAY_X				350
#define TEST_DISPLAY_Y				125
#define TEST_DISPLAY_LINE			40

#define PRESS_TALK_BUTTON_TIMES_EXIT_AGING			2

extern unsigned char myAddr;

typedef enum
{
	MovementTestStart = 0,
	MovementTestLED,
	MovementTestLCD,
	MovementTestRING,
	MovementTestDOORBELL_EXTERNALRING,
	MovementTestZ1_Z4,
//	MovementTestVDD,
	MovementTestMON,
	MovementTestTALK,
	MovementTestEnd,
	MovementTestStateMax,
} MovementStateType;

MovementStateType movementTestState;
int movementTestEnable = 0;
int movementTestTimeCnt;
int movementTestResult[MovementTestStateMax];
int movementTestLcdBG 	= 0;
int movementTestLedState = 0;
OS_TIMER timer_movementTest;
int movementTestRetest = 0;

// 480 x 30 = 4 HOURS
#define MAX_AUTO_AGING_TIME     480

int autoAgingState 	= 0;
int autoAgingEnable = 0;
int autoAgingTime 	= 0;
int autoAgingExit 	= PRESS_TALK_BUTTON_TIMES_EXIT_AGING;

OS_TIMER timer_AutoAging;
OS_TIMER timer_AutoAgingtTalk;

void Callback_AutoAging(void)
{
    API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_INFORM_AUTO_AGING);
}

void Callback_AutoAgingTalk(void)
{
    if( autoAgingEnable && autoAgingState )
    {
        API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_INFORM_AUTO_AGING_TALK);        
    }
}

// must call this funtion after OS start
void AutoAgingInit(void)
{
	LoadAgingConfigFile();
	if( autoAgingEnable )
	{
		if( autoAgingTime < 240 )
		{
			API_LedDisplay_CallRing();
		    
		}
		else if( autoAgingTime < MAX_AUTO_AGING_TIME )
		{
		   API_LedDisplay_MissEvent();
		}
		else
		{
			API_LedDisplay_NotDisturb();
		}
	       API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_INFORM_ENTER_AUTO_AGING);
	}
	#if 0
	else
	{
		if(Judge_ProductionTestSdCardLink())
		{
			if(!IfUpdateProductionTestFile())
			{
				StartInitOneMenu(MENU_049_PRODUCTION_TEST,0,0);
			}
		}
	}
	#endif
}

char wifiTestSsid[WIFI_SSID_LENGTH+1];
char wifiTestPwd[WIFI_PWD_LENGTH+1];
void LoadTestWIFISsid(void)
{
	FILE	*file = NULL;
	int 	i;
	char	buff[200+1];
	char	*pos1, *pos2, *pos3;

		
	if( (file=fopen(WIFI_TEST_FILE,"r")) == NULL )
	{
		return;
	}

	for(i = 0; fgets(buff,200,file) != NULL && i < 1; )
	{
		pos1 = strstr(buff, "[SSID]=");
		pos2 = strstr(buff, "[PWD]=");
		pos3 = strstr(buff, "[END]");
		
		if(pos1 != NULL && pos2 != NULL && pos3 != NULL)
		{
			memcpy(wifiTestSsid, pos1+strlen("[SSID]="), strlen(pos1)-strlen(pos2)-strlen("[SSID]="));
			memcpy(wifiTestPwd, pos2+strlen("[PWD]="), strlen(pos2)-strlen(pos3)-strlen("[PWD]="));
			
			printf("Read wifi [%s][%s]\n", wifiTestSsid, wifiTestPwd);
			
			i++;
		}
	}

	fclose(file);

}
void wifiTestDisplay(void)
{
	int level=0;
	int x,y;
	x = TEST_RESULT_DISPLAY_X;
	y = TEST_DISPLAY_Y;
	if(GetSysVerInfo_WlanConnect()==WLAN_ACT_CONNECTED)
	{
		level|=GetSysVerInfo_WifiSignalLev();
		if(GetSysVerInfo_WifiSignalLev()&0x80)
			level|=0xffffff00;
		//printf("11111111DisplayMainMenuWifiQuality:%d:%d\n",wifiRun.curWifi.LEVEL,level);
		if(level >= -55)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY4);
		}
		else if(level >= -70)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY3);
		}
		else if(level >= -85)
		{
			API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY2);
		}
		else 
		{
			API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_QUALITY1);
		}
		API_OsdStringDisplayExt(x+50, y+15, COLOR_RED, GetSysVerInfo_WlanCurSsid(), strlen(GetSysVerInfo_WlanCurSsid()), 0,STR_UTF8, 0);
	}
	else
	{
		API_SpriteDisplay_XY( x, y, SPRITE_MainWifi_Disconnect);
	}
}

void MovementTestInfoDisplay(void)
{
	char display[100];

	strcpy(display, "LED test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestLED, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestLED]?(movementTestResult[MovementTestLED]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestLED, (movementTestResult[MovementTestLED]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "LCD test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestLCD, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestLCD]?(movementTestResult[MovementTestLCD]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestLCD, (movementTestResult[MovementTestLCD]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Ring test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestRING, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestRING]?(movementTestResult[MovementTestRING]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestRING, (movementTestResult[MovementTestRING]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	wifiTestDisplay();
	//strcpy(display, "VCOM test:");
	//API_OsdStringDisplayExt(340, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestRING, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	//snprintf(display, 100, "%s", "OK   ");
	//API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestRING, COLOR_GREEN, display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Door bell and external ring test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestDOORBELL_EXTERNALRING, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestDOORBELL_EXTERNALRING]?(movementTestResult[MovementTestDOORBELL_EXTERNALRING]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestDOORBELL_EXTERNALRING, (movementTestResult[MovementTestDOORBELL_EXTERNALRING]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Z1 test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestZ1_Z4, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestZ1_Z4]?((movementTestResult[MovementTestZ1_Z4]&0x01)?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(500, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestZ1_Z4, ((movementTestResult[MovementTestZ1_Z4]&0x01)? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Z2 test:");
	API_OsdStringDisplayExt(650, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestZ1_Z4, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestZ1_Z4]?((movementTestResult[MovementTestZ1_Z4]&0x02)?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestZ1_Z4, ((movementTestResult[MovementTestZ1_Z4]&0x02)? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Mon test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestMON, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestMON]?(movementTestResult[MovementTestMON]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(500, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestMON, (movementTestResult[MovementTestMON]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Z3 test:");
	API_OsdStringDisplayExt(650, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestMON, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestZ1_Z4]?((movementTestResult[MovementTestZ1_Z4]&0x04)?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestMON, ((movementTestResult[MovementTestZ1_Z4]&0x04)? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Talk test:");
	API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestTALK, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestTALK]?(movementTestResult[MovementTestTALK]==1?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(500, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestTALK, (movementTestResult[MovementTestTALK]==1? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);

	strcpy(display, "Z4 test:");
	API_OsdStringDisplayExt(650, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestTALK, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	snprintf(display, 100, "%s", movementTestResult[MovementTestZ1_Z4]?((movementTestResult[MovementTestZ1_Z4]&0x08)?"OK   ":"ERR  "): "     ");
	API_OsdStringDisplayExt(TEST_RESULT_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestTALK, ((movementTestResult[MovementTestZ1_Z4]&0x08)? COLOR_GREEN : COLOR_RED), display, strlen(display), 1, STR_UTF8, 0);
	
}

void MovementTestState_Init(void)
{
	int i;
	char display[100];
	
	for(i = 0; i < MovementTestStateMax; i++)
	{
		movementTestResult[i] = 0;
	}
	
	for(i = MovementTestStart; i<= MovementTestStateMax; i++)
	{
		API_OsdStringClearExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+i*TEST_DISPLAY_LINE, bkgd_w-TEST_DISPLAY_X, 40);
	}
	
	movementTestState = MovementTestStart;
	movementTestResult[MovementTestStart] = 1;
	movementTestLcdBG = 0;
	//API_BG_Display(MENU_BACK_GROUND_011_MovementTest);
	Power_Down_Timer_Stop();
	if(!movementTestRetest)
	{
		strcpy(display, "Press unlock button to start testing!");
		API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestStart, COLOR_GREEN, display, strlen(display), 1, STR_UTF8, 0);
	}
	//MovementTestProcess();
	//MovementTestInfoDisplay();
}

void Callback_MovementTest(void)
{
	if(movementTestEnable)
	{
		
		if(++movementTestTimeCnt > 6000)
		{
			movementTestTimeCnt = 0;
		}
		//if(movementTestState < MovementTestMON)
		//	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MovementTestLED);
		OS_RetriggerTimer( &timer_movementTest );

		if(movementTestState<MovementTestEnd-1)
		{
			if(movementTestTimeCnt%4 == 0)
			{
				movementTestResult[++movementTestState] = 0xf0;
    			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MovementTestNext);
			}
		}
		else if(movementTestState == MovementTestEnd)
		{
			OS_StopTimer( &timer_movementTest);
		}
	}
}

void MovementTestStartProcess(void)
{
	char display[100];
	
	//strcpy(display, "Press unlock button to test LCD!           ");
	//API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestStart, COLOR_GREEN, display, strlen(display), 1, STR_UTF8, 0);
	movementTestResult[++movementTestState] = 2;
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MovementTestNext);
	OS_RetriggerTimer( &timer_movementTest );
}


void MENU_051_MovementTest_Init(int uMenuCnt)
{
	char display[100];
	
	if(movementTestEnable)
	{
		movementTestRetest = 0;
		MovementTestState_Init();
	}
	else if(autoAgingEnable)
	{
		//API_BG_Display(MENU_BACK_GROUND_015_MovementTestAging);
		snprintf(display, 100, "Aging time = %dmin            ", autoAgingTime/2);
		API_OsdStringDisplayExt(15, 80, COLOR_RED, display, strlen(display), 1, STR_UTF8, 0);
	}
}

void MENU_051_MovementTest_Exit(void)
{
	AutoPowerOffReset();
}

void MENU_051_MovementTest_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char display[100];
	char* dispString;
	int len;

	Power_Down_Timer_Stop();
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					if(movementTestEnable)
					{
						if(movementTestState == MovementTestEnd - 1)
						{
							movementTestState = MovementTestEnd;
							MovementTestProcess();
						}
					}
					else if(autoAgingEnable)
					{
						if(autoAgingExit)
						{
							autoAgingExit--;
						}
						else
						{
							API_AutoAgingExit();
						}
					}
					break;
				
				case KEY_UNLOCK:	//按<开锁键>启动测试
					if(movementTestEnable)
					{
						if(movementTestState == MovementTestStart)
						{
							//API_WifiAddSSID(wifiTestSsid, wifiTestPwd);
							//API_WifiCNCT(wifiTestSsid, wifiTestPwd);
							MovementTestStartProcess();
						}
					}
					break;
			}
		}
		else if( pglobal_win_msg->status == TOUCH_3SECOND )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					if(movementTestEnable)
					{
						OS_StopTimer( &timer_movementTest );
						TestMonitor_Off();
						movementTestRetest = 1;
						MovementTestState_Init();
						MovementTestStartProcess();
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			if(GetCurIcon() == ICON_200_Return)
			{
				TestMonitor_Off();
				movementTestEnable = 0;
				API_AutoAgingExit();
				popDisplayLastMenu();
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
				if(movementTestState > MovementTestRING)
					wifiTestDisplay();
				break;
			case MSG_7_BRD_SUB_BecalledOn:
				API_AutoAgingExit();
				StartInitOneMenu(MENU_027_CALLING2,MENU_031_CALLING3,1);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
				
			case MSG_7_BRD_SUB_MonitorOn:
				API_TimeLapseStart(TIME_LPSE_UPDATE,0);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteClose(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
				if(movementTestEnable)
				{
					movementTestResult[MovementTestMON] = 1;
					MovementTestInfoDisplay();
				}
				break;
				
			case MSG_7_BRD_SUB_MonitorTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				if(movementTestEnable)
				{
					movementTestResult[MovementTestTALK] = 1;
					MovementTestInfoDisplay();
				}
				break;
				
			case MSG_7_BRD_SUB_MonitorOff:
				API_TimeLapseHide();
				API_TimeLapseStop();
				break;
				
			case MSG_7_BRD_SUB_INFORM_AUTO_AGING:
				AutoAgingProcess();
				break;
			
			case MSG_7_BRD_SUB_INFORM_AUTO_AGING_TALK:
				API_TalkOn();
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				//API_LocalMonitor_Talk_On();
				break;
			case MSG_7_BRD_SUB_MovementTestNext:
				MovementTestProcess();
				if(movementTestState > MovementTestRING)
					MovementTestInfoDisplay();
				break;
			case MSG_7_BRD_SUB_MovementTestLED:
				if(movementTestState < MovementTestMON)
				{
					if(++movementTestLedState >= 3)
					{
						movementTestLedState = 0;
					}
					switch(movementTestLedState)
					{
						case 0:
							hdLedRedCtrl(0);
							hdLedBlueCtrl(1);
							hdLedGreenCtrl(1);
							break;

						case 1:
							hdLedRedCtrl(1);
							hdLedBlueCtrl(1);
							hdLedGreenCtrl(0);
							break;

						case 2:
							hdLedRedCtrl(1);
							hdLedBlueCtrl(0);
							hdLedGreenCtrl(1);
							break;
					}
				}
				break;
	
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
	else if(pglobal_win_msg->type == MSG_9_DB)
	{
		if(movementTestEnable)
		{
			movementTestResult[MovementTestDOORBELL_EXTERNALRING] = 1;
			API_RingPlay(RING_Doorbell);
			API_POWER_EXT_RING_ON();	
			MovementTestInfoDisplay();
		}
	}
	else if(pglobal_win_msg->type == MSG_10_ALARM)
	{
		if(movementTestEnable)
		{
			movementTestResult[MovementTestZ1_Z4] = ((pglobal_win_msg->status) & 0x0f) ^ 0x0f;
			if(movementTestResult[MovementTestZ1_Z4])
				MovementTestInfoDisplay();
		}
	}


	
}

void MovementTestProcess(void)
{
	char display[100];
	
	switch(movementTestState)
	{
		case MovementTestLED:
			hdLedRedCtrl(0);
			hdLedBlueCtrl(1);
			hdLedGreenCtrl(1);
			API_ProgBarDisplay(0, 0, bkgd_w, bkgd_h, COLOR_RED);
			API_OsdStringClearExt(TEST_DISPLAY_X, TEST_DISPLAY_Y, bkgd_w-TEST_DISPLAY_X, 40);
			movementTestResult[MovementTestLED] = 1;
			break;
			
		case MovementTestLCD:
			hdLedRedCtrl(1);
			hdLedBlueCtrl(1);
			hdLedGreenCtrl(0);
			API_ProgBarDisplay(0, 0, bkgd_w, bkgd_h, COLOR_GREEN);
			movementTestResult[MovementTestLCD] = 1;
			break;

		case MovementTestRING:
			hdLedRedCtrl(1);
			hdLedBlueCtrl(0);
			hdLedGreenCtrl(1);
			API_ProgBarDisplay(0, 0, bkgd_w, bkgd_h, COLOR_BLUE);
			API_RingTuneSelect( 1 );
			movementTestResult[MovementTestRING] = 1;
			break;

		case MovementTestDOORBELL_EXTERNALRING:
			API_ProgBarClose(0, 0, bkgd_w, bkgd_h);
			API_POWER_EXT_RING_ON();
			break;			
			
		case MovementTestMON:
			set_menu_with_video_on();
			clearscreen(1);
			API_ProgBarDisplay(0, 0, bkgd_w, bkgd_h, COLOR_KEY);
			TestMonitor_On();
			break;
		case MovementTestTALK:
			TestMonitor_Talk_On();
			strcpy(display, "Press talk button to exit testing and enter Aging!");
			API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*MovementTestEnd, COLOR_GREEN, display, strlen(display), 1, STR_UTF8, 0);
			strcpy(display, "Press unlock button 3s to restart!");
			API_OsdStringDisplayExt(TEST_DISPLAY_X, TEST_DISPLAY_Y+TEST_DISPLAY_LINE*(MovementTestEnd+1), COLOR_GREEN, display, strlen(display), 1, STR_UTF8, 0);
			break;
		case MovementTestEnd:
			TestMonitor_Off();
			MovementTestExit();
			//EditTestLogRecord(0,2);
			API_AutoAgingEnter();
			break;
	}
}

void MovementTestExit(void)
{	
	/*char	buff[100];
		
	snprintf(buff, 100, "rm %s\n", MovementTestConfigFile);
	system(buff);
	
	sync();*/
	movementTestEnable = 0;
	CloseOneMenu();
}

void MovementTestStateEnd(void)
{
	if(movementTestEnable)
	{
		movementTestState = MovementTestEnd;
	}
}

void InitMovementTest(void)
{
	FILE	*file = NULL;

	file = fopen(MovementTestConfigFile,"r");
	if(file == NULL )
	{
		movementTestEnable = 0;
		AutoAgingInit();
		return;
	}
	else
	{
		fclose(file);
	}
	movementTestEnable = 1;
	LoadTestWIFISsid();
	API_AutoAgingExit();
	sleep(10);	
	API_WifiDisCNCT();
	API_WifiCNCT(wifiTestSsid, wifiTestPwd);
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MovementTestStart);
    OS_CreateTimer(&timer_movementTest, Callback_MovementTest, 500/25);
}

void TestMonitor_On(void)
{
	GetIpRspData data;
	if(API_GetIpNumberFromNet("0099000001", NULL, NULL, 2, 1, &data) == 0)
	{
		int target_ip = data.Ip[0];
		if(open_dsmonitor_client(0,target_ip,0x40,150,Resolution_720P,0) == 0)
		{
			if( get_pane_type() == 1 )
			{
				Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h/2);
			}
			else
			{
				Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h);
			}
			API_talk_off();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOn);
		}
	}
}
void TestMonitor_Talk_On(void)
{
	if(DsTalkDeal(0) == 0)	
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
	}
}
void TestMonitor_Off(void)
{
	DsTalkClose(0);
	close_dsmonitor_client(0);
	API_TimeLapseHide();
	API_TimeLapseStop();
	API_SpriteClose(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
}

void API_AutoAgingEnter(void)
{
	static int creatAgingTimerFlag = 0;
	
	if(!creatAgingTimerFlag)
	{
		creatAgingTimerFlag = 1;
	    OS_CreateTimer(&timer_AutoAging, Callback_AutoAging, 30000/25);
	    OS_CreateTimer(&timer_AutoAgingtTalk, Callback_AutoAgingTalk, 5000/25);
	}
	
	autoAgingEnable = 1;
	autoAgingState = 0;
	SaveAgingConfigFile();
	AutoAgingProcess();
}

void AutoAgingOption(void)
{
	autoAgingExit = PRESS_TALK_BUTTON_TIMES_EXIT_AGING;
	
    if(autoAgingState )
    {
        autoAgingState = 0;
		//API_LocalMonitor_Off();
		API_TalkOff();
		CloseOneMenu();
    }
    else
    {
        autoAgingState = 1;        
	 StartInitOneMenu(MENU_051_MOVEMENT_TEST,0,1);
        //API_Event_LocalMonitor_On(DS1_ADDRESS);
        API_RingTuneSelect( 1 );
        OS_RetriggerTimer( &timer_AutoAgingtTalk );
    }
}

void AutoAgingProcess(void)
{
	LoadAgingConfigFile();

    if( autoAgingEnable ) // auto aging enable
    {
        if( autoAgingTime < MAX_AUTO_AGING_TIME ) // aging timer not enough
        {
            autoAgingTime++;
		SaveAgingConfigFile();
	        if( autoAgingTime < 240 )
	        {
	            API_LedDisplay_CallRing();
	        }
	        else
	        {
				API_LedDisplay_CallClose();
	        	API_LedDisplay_MissEvent();
	        }
            if( autoAgingTime == MAX_AUTO_AGING_TIME ) // aging time enough
            {
			API_AutoAgingExit();
			API_LedDisplay_CheckEvent();
			SaveAgingConfigFile();
			//EditTestLogRecord(1,2);
	              return;
            }
        }
        else
        {
			autoAgingEnable = 0;
			SaveAgingConfigFile();
        }
        AutoAgingOption();
        OS_RetriggerTimer( &timer_AutoAging );
    }
}

void API_AutoAgingExit(void)
{
	char	buff[100];

    if( autoAgingEnable )
    {
		autoAgingState = 1;
        AutoAgingOption();
        OS_StopTimer( &timer_AutoAging );
        OS_StopTimer( &timer_AutoAgingtTalk );
        API_LedDisplay_CallClose();
		#if 0
		snprintf(buff, 100, "rm %s\n", AUTO_AGING_CONFIG_FILE_NAME);
		system(buff);
		sync();
		#endif
        autoAgingEnable = 0;
    }
}

int SaveAgingConfigFile(void)
{
	FILE	*file = NULL;
	char	buff[READ_BUFF_LEN+1] = {0};
		
	if( (file=fopen(AUTO_AGING_CONFIG_FILE_NAME,"w")) == NULL)
	{
		return -1;
	}
	
	snprintf( buff, READ_BUFF_LEN, "# Aging config file\r\n\r\n");
	fputs(buff,file);
	
	snprintf( buff, READ_BUFF_LEN, "AgingEnable		= %d\r\n", autoAgingEnable);
	fputs(buff,file);

	snprintf( buff, READ_BUFF_LEN, "AgingTime		= %d\r\n", autoAgingTime);
	fputs(buff,file);

	fclose(file);
	sync();
	
	return 0;


}

int LoadAgingConfigFile(void)
{
	#define PROCESS_END		100
	
	char 	buff[READ_BUFF_LEN+1] = {0};
	char 	*pos1,*pos2;
	int		processState;	//处理状态
	int 	strLen;
	FILE 	*file = NULL;

	autoAgingEnable = 0;
	autoAgingTime = 0;
	
	if( (file=fopen(AUTO_AGING_CONFIG_FILE_NAME,"r")) == NULL )
	{
		return -1;
	}
	
	for(processState = 0, memset(buff, 0, READ_BUFF_LEN+1); fgets(buff,READ_BUFF_LEN,file) != NULL && processState != PROCESS_END; memset(buff, 0, READ_BUFF_LEN+1))
	{
		//去掉前面空格
		for(pos1 = buff; isspace(*pos1); pos1++);
		strcpy(buff, pos1);
		
		//去掉注释
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 == 0;
			}
		}

		//去掉等号后面空格
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']' || *pos1 == ']')
			{
				for(pos2 = pos1+1; isspace(*pos2); pos2++);
				strcpy(pos1+1, pos2);
			}
		}

		//去掉等号前面空格
		for(pos1 = buff; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; isspace(*pos2); pos2--);
				strcpy(pos2+1, pos1);
			}
		}

		//去掉最后的空格
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;

		switch(processState)
		{
			case 0:
				pos1 = strstr( buff, "AgingEnable" );
				if( pos1 != NULL )
				{
					pos2 = strchr( pos1, '=' );
					if( pos2 != NULL )
					{
						autoAgingEnable = atoi(pos2+1);
						strLen = strlen(pos2+1);
						processState = 1;
					}
					else
					{
						processState = PROCESS_END;
					}
				}
				break;
			case 1:
				pos1 = strstr( buff, "AgingTime" );
				if( pos1 != NULL )
				{
					pos2 = strchr( pos1, '=' );
					if( pos2 != NULL )
					{
						autoAgingTime = atoi(pos2+1);
					}
					processState = PROCESS_END;
				}
				break;	
		}
	}
	
	fclose(file);
	return 0;
}

int GetMovementTestState(void)
{
	return movementTestEnable || autoAgingEnable;
}

void EnterQC0Test(void)
{
	movementTestEnable = 1;
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MovementTestStart);
    	OS_CreateTimer(&timer_movementTest, Callback_MovementTest, 500/25);
}

