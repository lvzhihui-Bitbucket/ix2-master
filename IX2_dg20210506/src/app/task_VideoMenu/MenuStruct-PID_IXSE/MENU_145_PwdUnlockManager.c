#include "MENU_public.h"
#include "cJSON.h"

#define	PwdManage_ICON_MAX	5

static int devIp;
static char devName[50];
char Code1[9]={0};
char Code2[9]={0};
int CodeLen;
char Codeinput[9];
const char *iostr[3] =
{
	"Unlock1Pwd1",
	"Unlock1Pwd2",
	"AutoUnlockPwdLen"
};

const IconAndText_t PwdManageIconTable[] = 
{
	{ICON_PublicUnlockCodeLen,		MESG_TEXT_UnlockCodeLen},    
	{ICON_PublicUnlockCode1,		MESG_TEXT_UnlockCode1},    
	{ICON_PublicUnlockCode2,		MESG_TEXT_UnlockCode2},
	{ICON_UnlockCodeMore,			MESG_TEXT_UnlockCodeMore},
};

const int PwdManageIconNum = sizeof(PwdManageIconTable)/sizeof(PwdManageIconTable[0]);

void EnterPwdManageMenu(int ip, char* name)
{
	devIp = ip;
	strcpy(devName, name);
	StartInitOneMenu(MENU_145_PwdUnlockManager,0,1);
}
static void DisplayPwdManagePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
	char display[20] = {0};	
	for(i = 0; i < PwdManage_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*PwdManage_ICON_MAX+i < PwdManageIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, PwdManageIconTable[i+page*PwdManage_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(PwdManageIconTable[i+page*PwdManage_ICON_MAX].iCon)
			{
				case ICON_PublicUnlockCodeLen:
					snprintf(display, 20, "%d", CodeLen);					
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;	
				case ICON_PublicUnlockCode1:					
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, Code1, strlen(Code1), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_PublicUnlockCode2:					
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, Code2, strlen(Code2), 1, STR_UTF8, hv.h-x);
					break;
			}
		}
	}
	pageNum = PwdManageIconNum/PwdManage_ICON_MAX + (PwdManageIconNum%PwdManage_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
}
void MENU_145_PwdUnlockManager_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, devName, strlen(devName), 1, STR_UTF8, 0);

	#if 0
	if(GetLastNMenu() == MENU_081_OutdoorStations)
	{
		
	}
	#endif
	cJSON * ret = API_ReadRemoteIo(devIp, cJSON_CreateStringArray(iostr, 3));
	#if 0
	char *str=cJSON_Print(ret);
	printf("API_ReadRemoteIo =%s\n",str);
	free(str);
	#endif
	if(ret)
	{
		cJSON* item = cJSON_GetObjectItemCaseSensitive(ret, "READ");
		GetJsonDataPro(item, "Unlock1Pwd1", Code1);
		GetJsonDataPro(item, "Unlock1Pwd2", Code2);
		GetJsonDataPro(item, "AutoUnlockPwdLen", &CodeLen);
	}
	DisplayPwdManagePageIcon(0);
}


void MENU_145_PwdUnlockManager_Exit(void)
{

}
int confirm_set_codelen(const char* input)
{
	int len = atoi(input);
	if(len < 4 || len > 8)
	{
		return 0;
	}
	CodeLen = atoi(input);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddNumberToObject(newitem, "AutoUnlockPwdLen", CodeLen);
	API_WriteRemoteIo(devIp, newitem);
	cJSON_Delete(newitem);
	return 1;
}
int confirm_set_code1(const char* input)
{
	int i;
	int len = strlen(input);
	if(len != CodeLen)
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	strcpy(Code1, input);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "Unlock1Pwd1", Code1);
	API_WriteRemoteIo(devIp, newitem);
	cJSON_Delete(newitem);
	return 1;
}
int confirm_set_code2(const char* input)
{
	int i;
	int len = strlen(input);
	if(len != CodeLen)
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	strcpy(Code2, input);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "Unlock1Pwd2", Code2);
	API_WriteRemoteIo(devIp, newitem);
	cJSON_Delete(newitem);
	return 1;
}
int PwdManageVerifyPassword(const char* password)
{
	char display[20];
	char *pos1, *pos2;
	char code[11]={0};
	sprintf(display, "<ID=%s>", ManagePassword);//??611????
	API_io_server_UDP_to_read_remote(devIp, 0xFFFFFFFF, display);
	pos1 = strstr(display, "Value=");
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			int len;
			len = ((int)(pos2-pos1))-strlen("Value=");
			memcpy(code, pos1+strlen("Value="), len);
			code[len] = 0;
			printf("ix611 ManagePassword: %s\n",code);			
		}
	}
	if(!strcmp(password, code))
	{
		EnterPwdListMenu(devIp, devName);
		return -1;
	}
	else
	{
		return 0;
	}
}

void MENU_145_PwdUnlockManager_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int iconSelect;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					iconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(iconSelect < PwdManageIconNum)
					{
						switch(PwdManageIconTable[iconSelect].iCon)
						{
							case ICON_PublicUnlockCodeLen:
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_UnlockCodeLen, Codeinput, 1, COLOR_WHITE, NULL, 0, confirm_set_codelen);
								break;	
							case ICON_PublicUnlockCode1:					
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_UnlockCode1, Codeinput, CodeLen, COLOR_WHITE, Code1, 1, confirm_set_code1);
								break;
							case ICON_PublicUnlockCode2:					
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_UnlockCode2, Codeinput, CodeLen, COLOR_WHITE, Code2, 1, confirm_set_code2);
								break;
							case ICON_UnlockCodeMore:
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, Codeinput, 8, COLOR_WHITE, NULL, 0, PwdManageVerifyPassword);
								break;							
						}
					}
					break;
					
				case ICON_201_PageDown:
					break;
				case ICON_202_PageUp:
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

