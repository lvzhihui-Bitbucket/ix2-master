#include "MENU_public.h"
#include "MENU_029_SetDateTime.h"
#include "task_RTC.h"
#include "MenuSetDateTime_Business.h"

#define	SetDateTime_ICON_MAX	5
int setDateTimeIconSelect;
int setDateTimePageSelect;

int dateMode;
int timeMode;
#if 0
unsigned char year[5] = {0};
unsigned char mon[3] = {0};
unsigned char day[3] = {0};
unsigned char hour[3] = {0};
unsigned char min[3] = {0};
unsigned char second[3] = {0};
unsigned char inputSet[9] = {0};
#endif

const SetDateIcon setDateIconTable[] = 
{
	{ICON_052_DateMode,					MESG_TEXT_ICON_052_DateMode},    
	{ICON_053_TimeMode,					MESG_TEXT_ICON_053_TimeMode},
	{ICON_054_Date,						MESG_TEXT_ICON_054_Date},
	{ICON_055_Time, 					MESG_TEXT_ICON_055_Time},
	{ICON_336_InternetTime, 			MESG_TEXT_ICON_336_InternetTime},
};

const int setDateIconNum = sizeof(setDateIconTable)/sizeof(setDateIconTable[0]);

static void DisplaySetDatePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[11];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < SetDateTime_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		if(page*SetDateTime_ICON_MAX+i < setDateIconNum)
			{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setDateIconTable[i+page*SetDateTime_ICON_MAX].iConText, 1, val_x - x);
			x += (hv.h - pos.x)/2;
			switch(setDateIconTable[i+page*SetDateTime_ICON_MAX].iCon)
			{
				case ICON_052_DateMode:
					if(dateMode == 0)
					{
						stringId = MESG_TEXT_m_d_y;
					}
					else if(dateMode == 1)
					{
						stringId = MESG_TEXT_d_m_y;
					}
					else if(dateMode == 2)
					{
						stringId = MESG_TEXT_y_m_d;
					}
					API_OsdUnicodeStringDisplay(x, y, COLOR_RED, stringId, 1, hv.h - x);
					break;
					
				case ICON_053_TimeMode:
					stringId = (timeMode ? MESG_TEXT_24h : MESG_TEXT_12h);
					API_OsdUnicodeStringDisplay(x, y, COLOR_RED, stringId, 1, hv.h - x);
					break;
				case ICON_054_Date:
					snprintf(display,11,"%s-%s-%s",year, mon, day);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_055_Time:
					snprintf(display,11,"%s:%s",hour, min);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					break;
					
			}
		}
	}
	pageNum = setDateIconNum/SetDateTime_ICON_MAX + (setDateIconNum%SetDateTime_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

//cao_20181020_s

#if 0
//cao_20181025
int	ReadDateTime(uint8 dateOrTime)	
{
//#define RTC_TIME

	unsigned char h;
	unsigned char mi;
	unsigned char s;
	unsigned char y;
	unsigned char mo;
	unsigned char d;

	// Get the current date time
	if(dateOrTime)
	{
#if 0
		RTC_DateTypeDef date;
		date	= RtcGetDate();
		y 	= BcdToBin(date.RTC_Year);
		mo 	= BcdToBin(date.RTC_Month);
		d 	= BcdToBin(date.RTC_Date);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);

		//printf("get localtime %d-%d-%d \n", tblock->tm_year, tblock->tm_mon, tblock->tm_mday);
		
		y = ((tblock->tm_year > 100) ? tblock->tm_year -100 : 0);
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init year
		snprintf(year,5,"20%02d",y);	
		// init month
		snprintf(mon,3,"%02d",mo);	
		// init day
		snprintf(day,3,"%02d",d);	
	}
	else
	{
#if 0
		RTC_TimeTypeDef time;
		time	= RtcGetTime();
		h	= BcdToBin(time.RTC_Hours);
		mi	= BcdToBin(time.RTC_Minutes);
		s	= BcdToBin(time.RTC_Seconds);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);
		
		y = tblock->tm_year-100;
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init hour
		snprintf(hour,3,"%02d",h);	
		// init minute
		snprintf(min,3,"%02d",mi);	
		// init minute
		snprintf(second,3,"%02d",s);	
	}
}


pthread_mutex_t 	receiveLocalTimeLock = PTHREAD_MUTEX_INITIALIZER;
time_t receiveLocalTime;

time_t GetLocalTimeFromStm8l(void)
{
	int i;
	time_t ret = -1;
	
	receiveLocalTime = -1;
	api_uart_send_pack(UART_TYPE_LOCAL_TIME_GET, NULL, 0);
	
	for(i = 10; i > 0; i--)
	{
		usleep(100*1000);
		pthread_mutex_lock(&receiveLocalTimeLock);
		ret = receiveLocalTime;
		pthread_mutex_unlock(&receiveLocalTimeLock);
		if(ret >= 0)
		{
			break;
		}
	}
	printf("receiveLocalTime = %d, i = %d\n", ret, i);
	return ret;	
}

void ReceiveLocalTimeFromStm8l(unsigned char* buff)
{
	pthread_mutex_lock(&receiveLocalTimeLock);
	receiveLocalTime = buff[3]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[2]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[1]; 
	receiveLocalTime = (receiveLocalTime<<8)+buff[0];
	pthread_mutex_unlock(&receiveLocalTimeLock);
	printf("receiveLocalTime = %d, receiveLocalTime = %d\n", receiveLocalTime, receiveLocalTime);
}


void UpdateLocalTimeToStm8l(void)
{
#if 0
	unsigned char buff[4];

	time_t t;
	t = time(NULL); 

	buff[0] = t&0xFF;
	buff[1] = (t>>8)&0xFF;
	buff[2] = (t>>16)&0xFF;
	buff[3] = (t>>24)&0xFF;

	printf("UpdateLocalTimeToStm8l buff[0] = 0x%x, buff[1] = 0x%x, buff[2] = 0x%x,buff[3] = 0x%x\n", buff[0], buff[1],buff[2],buff[3]);
	
	api_uart_send_pack(UART_TYPE_LOCAL_TIME_UPDATE, buff, 4);
#endif	
}

void DateAndTimeInit(void)
{
	FILE 			*fd = NULL;
	char buff[200+1] = {0};
	char buff2[200+1] = {0};
	unsigned char tempYear[5] = {0};
	unsigned char tempMon[3] = {0};
	unsigned char tempDay[3] = {0};
	unsigned char tempHour[3] = {0};
	unsigned char tempMin[3] = {0};
	unsigned char tempSecond[3] = {0};
	int curtime;
	int saveTime;
	int temp;
	int updateFlag;

	ReadDateTime(0);
	ReadDateTime(1);
	
	if( (fd=fopen(TIME_TEMP_FILE,"r")) == NULL )
	{
		return;
	}
	
	usleep(100*1000);
	
	if(fgets(buff,200,fd) != NULL)
	{
		sscanf(buff,"%*[^\"]\"%[0-9]-%[0-9]-%[0-9] %[0-9]:%[0-9]:%[0-9]\"", tempYear,tempMon,tempDay,tempHour,tempMin,tempSecond);
	}
	printf("%s\n", buff);

	if(fgets(buff2,200,fd) != NULL)
	{
		sscanf(buff2,"%*[^=]=%d", &updateFlag);
	}
	printf("updateFlag=%d\n", updateFlag);

	fclose(fd);
	
	printf("curtime %s-%s-%s %s-%s-%s\n", year, mon, day, hour, min, second);
	printf("saveTime %s-%s-%s %s-%s-%s\n", tempYear, tempMon, tempDay, tempHour, tempMin, tempSecond);
	SetTimeUpdateFlag(updateFlag);
	#if 0
	curtime = atoi(min) + atoi(hour)*60 + atoi(day)*24*60 + atoi(mon)*30*24*60 + atoi(year)*12*30*24*60;
	saveTime = atoi(tempMin) + atoi(tempHour)*60 + atoi(tempDay)*24*60 + atoi(tempMon)*30*24*60 + atoi(tempYear)*12*30*24*60;
	
	temp = curtime - saveTime;
	
	printf("temp = curtime - saveTime -> %d = %d - %d\n", temp, curtime, saveTime);

	time_t t_stm8 = GetLocalTimeFromStm8l();
	time_t t_now = time(NULL);

	printf("t_stm8 = %d, t_now = %d\n", t_stm8, t_now);

	//如果stm8时间为0，则表明刚上电
	if(t_stm8 == 0 || t_stm8 == -1)
	{
		//如果RTC时间比文件时间快一个小时以内，则RTC的电还没掉完，此时使用RTC时间，并更新到文件
		if(temp >= 0 && temp <= 60)
		{
			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
		//如果RTC的电已经掉完，此时使用文件时间，但是不显示出来
		else
		{
			if( (fd = popen( buff, "r" )) == NULL)
			{
				return;
			}		
			pclose( fd );
			
			if( (fd = popen( "hwclock -w", "r" )) == NULL)
			{
				return;
			}
			pclose( fd );
			
			SaveDateAndTimeToFile();
		}

	}
	//如果stm8时间不为0，则表明机器刚重启
	else
	{
		//本地RTC时间比stm8时间快一分钟以内，说明RTC时间正确, 使用RTC时间
		if(t_now - t_stm8 >= 0 && t_now - t_stm8 <= 60)
		{
			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
		//RTC时间错误，使用STM8时间
		else
		{
			unsigned char h;
			unsigned char mi;
			unsigned char s;
			unsigned char y;
			unsigned char mo;
			unsigned char d;

			struct tm *tblock;	
		
			tblock=localtime(&t_stm8);
			
			y = tblock->tm_year-100;
			mo = tblock->tm_mon+1;
			d = tblock->tm_mday;
			h = tblock->tm_hour;
			mi = tblock->tm_min;
			s = tblock->tm_sec;

			
			// init year
			snprintf(year,5,"20%02d",y);	
			// init month
			snprintf(mon,3,"%02d",mo);	
			// init day
			snprintf(day,3,"%02d",d);	
			// init hour
			snprintf(hour,3,"%02d",h);	
			// init minute
			snprintf(min,3,"%02d",mi);	
			// init minute
			snprintf(second,3,"%02d",s);	
			
			snprintf( buff, 200,  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

			if( (fd = popen( buff, "r" )) == NULL )
			{
				return;
			}		
			pclose( fd );
			
			if( (fd = popen( "hwclock -w", "r" )) == NULL )
			{
				return;
			}
			pclose( fd );

			SetTimeUpdateFlag(updateFlag);
			SaveDateAndTimeToFile();
		}
	}
	#endif
}

void SaveDateAndTimeToFile(void)
{
	FILE 			*file = NULL;
	char buff[200+1];
	if( (file=fopen(TIME_TEMP_FILE,"w+")) == NULL )
	{
		return NULL;
	}
	
	ReadDateTime(1);
	ReadDateTime(0);

	snprintf( buff, 200,  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	fputs(buff, file);

	snprintf( buff, 200,  "update falg=%d\n", GetTimeUpdateFlag());

	fputs(buff, file);

	fclose(file);

	sync();
}

//此函数每一分钟调用一次
void OneHourSaveDateAndTime(void)
{
	static int timeCnt = 0;
	uint8 enable;
	uint8 timeZone;
	char timeServer[50] = {0};
	
	timeCnt++;

	//60分钟保存到文件
	if(!(timeCnt%60))
	{
		SaveDateAndTimeToFile();
		UpdateLocalTimeToStm8l();
	}

	//隔8小时同步时间
	if(timeCnt >= 8*60)
	{
		timeCnt = 0;
		char temp[20];
		API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, temp);
		enable = atoi(temp);
		if(enable)
		{
			API_Event_IoServer_InnerRead_All(TimeZone, temp);
			timeZone = atoi(temp);
			API_Event_IoServer_InnerRead_All(TIME_SERVER, (uint8*)&timeServer);
			sync_time_from_sntp_server(inet_addr(timeServer), timeZone-12);
		}		
	}
}


//cao_20181020_e

int set_system_date_time(uint8 dateOrTime)
{
	FILE* fd   = NULL;
	char buf[100] = {0};
	
	if(dateOrTime)
	{
		memcpy(year, inputSet, 4);
		memcpy(mon, inputSet+4, 2);
		memcpy(day, inputSet+6, 2);
		ReadDateTime(0);
	}
	else
	{
		memcpy(hour, inputSet, 2);
		memcpy(min, inputSet+2, 2);
		memcpy(second, "00", 2);
		ReadDateTime(1);
	}
	snprintf( buf, sizeof(buf),  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	

	dprintf( "%s\n",buf);
	
	if( (fd = popen( buf, "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}		
	pclose( fd );

	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	
	dprintf( "set_system_date_time ok!\n" );

	//cao_201081020_s
	SetTimeUpdateFlag(1);
	SaveDateAndTimeToFile();
	//cao_201081020_e
	
	return 0;
}

int RtcSet_ParaCheckDate(void)
{
	int para_len,para_value;
	
	memcpy(year, inputSet, 4);
	memcpy(mon, inputSet+4, 2);
	memcpy(day, inputSet+6, 2);

	if(strlen(inputSet) != 8)
	{
		return -1;
	}
	
	para_len = strlen(year);
	if(para_len != 4)
	{
		return -1;
	}
	para_value = atol(year);
	//if(para_value > 2037 || para_value < 2005)
	if(para_value < 2000)
	{
		return -1;
	}

	para_len = strlen(mon);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(mon);
	if(para_value > 12)
	{
		return -1;
	}

	para_len = strlen(day);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(day);
	if(para_value > 31)
	{
		return -1;
	}
	return 0;
}

int RtcSet_ParaCheckTime(void)
{
	int para_len,para_value;
	
	memcpy(hour, inputSet, 2);
	memcpy(min, inputSet+2, 2);
	
	if(strlen(inputSet) != 4)
	{
		return -1;
	}

	para_len = strlen(hour);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(hour);
	if(para_value > 23)
	{
		return -1;
	}

	para_len = strlen(min);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(min);
	if(para_value > 59)
	{
		return -1;
	}

	para_len = strlen(second);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(second);
	if(para_value > 59)
	{
		return -1;
	}

	return 0;
}
#endif
void DateModeSet(int mode)
{
	char temp[20];
	dateMode = mode;
	sprintf(temp, "%d", dateMode);
	API_Event_IoServer_InnerWrite_All(DATE_FORMAT, temp);
}

void TimeModeSet(int mode)
{
	char temp[20];
	timeMode = mode ? 1 : 0;
	sprintf(temp, "%d", timeMode);
	API_Event_IoServer_InnerWrite_All(TIME_FORMAT, temp);
}

int DateSave(const char* date)
{
	strcpy(inputSet, date);
	if(!RtcSet_ParaCheckDate())
	{
		set_system_date_time(1);
		return 1;
	}
	else
	{
		return 0;
	}
}

int TimeSave(const char* time)
{
	strcpy(inputSet, time);
	if(!RtcSet_ParaCheckTime())
	{
		set_system_date_time(0);
		return 1;
	}
	else
	{
		return 0;
	}
}

void MENU_029_SetDateTime_Init(int uMenuCnt)
{
	char temp[20];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	setDateTimePageSelect = 0;
	setDateTimeIconSelect = 0;
	
	API_Event_IoServer_InnerRead_All(DATE_FORMAT, temp);
	dateMode = atoi(temp);
	API_Event_IoServer_InnerRead_All(TIME_FORMAT, temp);
	timeMode = atoi(temp);

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_DateAndTime, 2, 0);	
	
	ReadDateTime(1);
	ReadDateTime(0);
	
	API_MenuIconDisplaySelectOn(ICON_025_general);
	DisplaySetDatePageIcon(setDateTimePageSelect);
}

void MENU_029_SetDateTime_Exit(void)
{
	
}

void MENU_029_SetDateTime_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	int i;
	char temp[20];

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&setDateTimePageSelect, SetDateTime_ICON_MAX, setDateIconNum, (DispListPage)DisplaySetDatePageIcon);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&setDateTimePageSelect, SetDateTime_ICON_MAX, setDateIconNum, (DispListPage)DisplaySetDatePageIcon);
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					setDateTimeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(setDateTimePageSelect*SetDateTime_ICON_MAX+setDateTimeIconSelect >= setDateIconNum)
					{
						return;
					}
					switch(setDateIconTable[setDateTimePageSelect*SetDateTime_ICON_MAX+setDateTimeIconSelect].iCon)
					{
						case ICON_052_DateMode:
							API_GetOSD_StringWithID(MESG_TEXT_m_d_y, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_d_m_y, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_y_m_d, NULL, 0, NULL, 0, publicSettingDisplay[2]+1, &len);
							publicSettingDisplay[2][0] = len;
							
							API_Event_IoServer_InnerRead_All(DATE_FORMAT, temp);
							dateMode = atoi(temp);
							
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_DateTimeSet, 3, dateMode, DateModeSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_053_TimeMode:
							API_GetOSD_StringWithID(MESG_TEXT_12h, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_24h, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							
							API_Event_IoServer_InnerRead_All(TIME_FORMAT, temp);
							timeMode = atoi(temp);

							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_DateTimeSet, 2, timeMode, TimeModeSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_054_Date:
							snprintf(inputSet,9,"%s%s%s",year, mon, day);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_054_Date, inputSet, 8, COLOR_WHITE, inputSet, 1, DateSave);
							break;
						case ICON_055_Time:
							snprintf(inputSet,9,"%s%s",hour, min);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_055_Time, inputSet, 4, COLOR_WHITE, inputSet, 1, TimeSave);
							break;
						case ICON_336_InternetTime:
							StartInitOneMenu(MENU_065_InternetTime,0,1);
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

void AutoSetDateInit(void)
{
	ReadDateTime(1);
	ReadDateTime(0);
	//#if defined(INDEXA_VER)
	char buf[100];
	FILE *fd;
	static uint8  first_flag=0;
	fd = popen( "date", "r" );
	fgets(buf,100,fd);
	pclose(fd);
	
	if(strstr(buf,year)==NULL||atoi(year)<2021)
	{
		
		snprintf( buf, sizeof(buf),  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", "2022","01","01","12","00","00");

		dprintf( "%s\n",buf);
		
		if( (fd = popen( buf, "r" )) == NULL )
		{
			dprintf( "set_system_date_time error:%s\n",strerror(errno) );
			return -1;
		}		
		pclose( fd );
		usleep(500*1000);
		//ReadDateTime(1);
		//ReadDateTime(0);
		
		
	}
}

