#include "MENU_public.h"
#include "MENU_086_CallTestStat.h"
#include "../../obj_AutoTest/obj_autotest_console.h"

static int autotest_self_flag = 0;
#if 0
#define	SetInstaller_ICON_MAX	5
int setInstallerIconSelect;
int setInstallerPageSelect;
int setInstallerPassword = 0;
char installerInput[9];
#endif
#define	CallTestStat_ICON_MAX	5

const IconAndText_t SipStatIconTable[] = 
{
	{0,			MESG_TEXT_TestStatCallNum},    
	{0,			MESG_TEXT_TestStatCallPer},
	{0, 			MESG_TEXT_TestStatDivertNum},
	{0, 			MESG_TEXT_TestStatDivertPer},
	{0, 			MESG_TEXT_TestStatDivertVideoNum},
};

static void Update_CallTestStat_Disp(void)
{
	char disp[41];
	int i;
	uint16 x, y, val_x;
	float per1,per2;
	POS pos;
	SIZE hv;

	for(i = 0; i < CallTestStat_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		//if(page*SIP_STAT_LIST_ICON_MAX+i < SipStatIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, SipStatIconTable[i].iConText, 1, val_x - x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(SipStatIconTable[i].iConText)
			{
				case MESG_TEXT_TestStatCallNum:
					snprintf(disp,20,"[%d/%d]",Get_Calltest_SuccNum(),Get_Calltest_TotalNum());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);

					//sprintf(SipStatLogStr,"(%d/%d,",Get_Divert_SuccNum(),Get_Divert_TotalNum());
					break;
				case MESG_TEXT_TestStatCallPer:
					per1 = Get_Calltest_SuccPercent();
					if(per1 == 100.0)
					{
						strcpy(disp,"[100%]");
					}
					else
					{
						sprintf(disp,"[%0.1f%%]",per1);
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);
					break;
				case MESG_TEXT_TestStatDivertNum:
					snprintf(disp,20,"[%d/%d]",Get_Divert_SuccNum(),Get_Divert_TotalNum());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);

					//sprintf(SipStatLogStr,"(%d/%d,",Get_Divert_SuccNum(),Get_Divert_TotalNum());
					break;
				case MESG_TEXT_TestStatDivertPer:
					per1 = Get_Divert_SuccPercent();
					per2 = Get_Divert_OnceSuccPercent();
					if(per1 == 100.0)
					{
						strcpy(disp,"[100%,");
					}
					else
					{
						sprintf(disp,"[%0.1f%%,",per1);
					}
					if(per2 == 100.0)
					{
						strcat(disp,"100%]");
					}
					else
					{
						sprintf(disp+strlen(disp),"%0.1f%%]",per2);
					}
					//snprintf(display,20,"[%0.1f%%,%0.1f%%]",Get_Divert_SuccPercent(),Get_Divert_OnceSuccPercent());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);

					//sprintf(display,"%0.1f,%0.1f)",per1,per2);
					//strcat(SipStatLogStr,display);
					break;
				case MESG_TEXT_TestStatDivertVideoNum:
					snprintf(disp,20,"[%d/%d]",Get_Divert_VideoConnectNum(),Get_Divert_HookNum());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);

					//sprintf(display,"(%d/%d,",Get_Divert_VideoConnectNum(),Get_Divert_HookNum());
					//strcat(SipStatLogStr,display);
					break;
				
				default:
					break;
			}
		}
	}
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
	if(GetLastNMenu()!=MENU_087_AutotestTools)
	{
		OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
		API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
		if(get_autotest_state() == Autotest_Console_Testing)
		{
			snprintf(disp,40,"In testing(%d remaining,calling)",get_autotest_remain());
		}
		else if(get_autotest_state() == Autotest_Console_TestInterval)
		{
			snprintf(disp,40,"In testing(%d remaining,idle)",get_autotest_remain());
		}
		else
		{
			//strcpy(disp,"Test finished");
			API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_CusAutoTestFinish,0,0);	//FOR_INDEXA
			return;
		}
		API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, 0);
	}
}

void MENU_086_CallTestStat_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_AutotestStatistics,1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	autotest_self_flag = 0;
	Update_CallTestStat_Disp();
}

void MENU_086_CallTestStat_Exit(void)
{
	API_SetOsdLogViewState(0);
	if(autotest_self_flag  == 0)
	{
		API_AutoTest_Console_Close();
	}
	else
	{
		autotest_self_flag = 0;
	}
}

void MENU_086_CallTestStat_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[10];
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				case KEY_UNLOCK:
					//API_SetOsdLogViewState(1);
					//if(GetLastNMenu() != MENU_061_SipTools)
					if(GetLastNMenu()!=MENU_087_AutotestTools)
						API_SwitchOsdLogViewState();
					break;			
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
		
				case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_047_Home:
					GoHomeMenu();
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					#if 0
					setInstallerIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(setInstallerPageSelect*SetInstaller_ICON_MAX+setInstallerIconSelect >= setInstallerIconNum)
					{
						return;
					}

					if(/*setInstallerPassword*/1)
					{
						switch(setInstallerIconTable[setInstallerPageSelect*SetInstaller_ICON_MAX+setInstallerIconSelect].iCon)
						{
							case ICON_047_IpAddress:
								EnterInstallSubMenu(MENU_016_INSTALL_IP, 1);
								break;

							case ICON_048_CallNumber:
								EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 1);
								break;
								
							case ICON_049_OnsiteTools:
								EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 1);
								break;
								
							case ICON_044_Parameter:
								EnterInstallSubMenu(MENU_077_ParaGroup, 1);
								break;
								
							case ICON_FwUpgrade:
								EnterInstallSubMenu(MENU_066_FwUpgrade,1);	//czn_20181219
								break;	
						}
					}
					else
					{
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 4, COLOR_WHITE, NULL, 0, VerifyPassword);
					}
					#endif
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_AutotestStateUpate:
				Update_CallTestStat_Disp();
				break;
			//case MSG_7_BRD_SUB_BecalledOff:
			//	popDisplayLastMenu();
			//	break;	
			case MSG_7_BRD_SUB_BecalledOn:
				if(judge_autotest_targetIsself())
					autotest_self_flag = 1;
				
				//API_AutoAgingExit();
				StartInitOneMenu(MENU_027_CALLING2,MENU_031_CALLING3,1);
				break;
			case MSG_7_BRD_SUB_BeCalledDivert:		
				if(judge_autotest_targetIsself())
					autotest_self_flag = 1;
				
				StartInitOneMenu(MENU_053_DIVERT,0,1);
				break;	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//setInstallerPageSelect = 0;
		//DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}



