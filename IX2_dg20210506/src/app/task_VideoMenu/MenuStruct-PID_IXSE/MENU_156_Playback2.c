
#include "MENU_public.h"
#include "cJSON.h"
#include "obj_PublicInformation.h"


int deletVideoConfirm;
int delSpriteX,delSpriteY;

#define PLAYBACK_TIME_POS_X		(10)
#define PLAYBACK_TIME_POS_Y		(5)
char targFile[20]= {0};
char sourcename[100];

#define MaxRecNum		10
int checkRecDirSize(void)
{
    int i,cnt;
    char recfile[100] = {0};
	char tempfile[20]= {0};
	cJSON* view = cJSON_CreateArray();
    cJSON *item = NULL;
    API_TB_SelectBySortByName("EventRecord", view, NULL, 0);
    cnt = cJSON_GetArraySize(view);
	dprintf("checkRecDirSize cnt=%d\n", cnt);
    if(cnt > MaxRecNum)
    {
        for(i=0; i<(cnt-MaxRecNum); i++)
        {
            cJSON* where = cJSON_CreateObject();
            item=cJSON_GetArrayItem(view,i);
            GetJsonDataPro(item, "Rec_File", recfile);
            cJSON_AddStringToObject(where, "Rec_File", recfile);
            API_TB_DeleteByName("EventRecord", where);
			memcpy(tempfile, recfile+strlen(recfile)-16, 16);	
            delRecCacheFile(tempfile);
            cJSON_Delete(where);
        }
    }
    cJSON_Delete(view);
    return 0;
}
int delRecCacheFile(char *filename)
{
	char full_file_name[200];
	snprintf(full_file_name,200,"%s%s",JPEG_STORE_DIR2,filename);
	remove(full_file_name);
	return 0;
}
void DelAllRecRecord(void)
{
	cJSON* where = cJSON_CreateObject();
	API_TB_DeleteByName("EventRecord", where);
	DeleteFileProcess(JPEG_STORE_DIR2, "*");
	cJSON_Delete(where);
}
void DelOneRecRecord(char *filename)
{
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "Rec_File", filename);
	API_TB_DeleteByName("EventRecord", where);
	delRecCacheFile(filename);
	cJSON_Delete(where);
}

int getRecFileProcess(void)
{
	int i;
	char recfile[100] = {0};
    char fileTemp[100]= {0};
	char ip[100] = {0};
	dprintf("getRecFileProcess!!!\n");
	sleep(3);
	//checkRecDirSize();
	cJSON* view = cJSON_CreateArray();
	cJSON *item = NULL;
	if(API_TB_SelectBySortByName("EventRecord", view, NULL, 0) > 0)
	{
		//for(i = cJSON_GetArraySize(view)-1; i >= 0; i--)
		{
			item=cJSON_GetArrayItem(view,cJSON_GetArraySize(view)-1);
			GetJsonDataPro(item, "Rec_File", recfile);
			GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(item, "Recorder"), "IP_ADDR", ip);
			//playback_get_file(inet_addr(ip), recfile, fileTemp);
			api_get_file_to_local(inet_addr(ip), recfile, fileTemp);
		}
	}
	cJSON_Delete(view);
}

int playback_get_file(int ip, char* filescr, char* filetar)
{
	char targDir[100]= {0};
	if(MakeDir(JPEG_STORE_DIR2) != 0)
	{
		return -1;
	}
	if(filescr && strstr(filescr, ".mp4")!= NULL)
	{
		memcpy(targDir, filescr, strlen(filescr)-16);
		memcpy(targFile, filescr+strlen(filescr)-16, 16);
		snprintf(filetar, 200, "%s%s", JPEG_STORE_DIR2, targFile);	
		if( access( filetar, F_OK ) < 0 )//?????????fftp??
		{
			dprintf("playback_get_file %s from tftp\n", filetar);
			//MessageReportDisplay(NULL, 0, "Please wait...", COLOR_RED, 1, 5);
			Api_TftpReadFileFromDevice(ip, targDir, targFile, JPEG_STORE_DIR2, targFile);	
			//MessageClose();
		}
	}
	return (access( filetar, F_OK ));
}

void DisplayPlaybackTime( int sec_cnt )
{
	unsigned char str[10];
	int m,s;
	if(sec_cnt%20 == 0)
	{
		AutoPowerOffReset();
	}
	m = sec_cnt/60;
	s = sec_cnt%60;
	memset( str, 0, 10);
	snprintf( str, 10, "%02d:%02d  ", m, s);
	API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, str, strlen(str),1,STR_UTF8, 0);
	printf("playback time:%s\n",str);
}

void DisplayPlaybackInfo(void)
{
	API_OsdStringClearExt(bkgd_w/2-100, 5, bkgd_w/2, 40);
	API_OsdStringClearExt(bkgd_w/2-40, 40, bkgd_w/2, 40);
	API_OsdStringDisplayExt(bkgd_w/2-200, 5, COLOR_BLUE, sourcename, strlen(sourcename),1,STR_UTF8, 0);
	API_OsdStringDisplayExt(bkgd_w/2-100, 40, COLOR_BLUE, targFile, strlen(targFile),1,STR_UTF8, 0);
}

void playback_show_cb(int sec, int max)
{
	if(GetCurMenuCnt() != MENU_156_PLAYBACK2)
		return;
	DisplayPlaybackTime(sec);
}
void api_playback_event(char* event, cJSON* record)
{
	char recfile[200] = {0};
	char ip[100] = {0};
	char fileTemp[200]= {0};
	char name[50] = {0};
    char time[50] = {0};
	GetJsonDataPro(record, "Rec_File", recfile);
	GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(record, "Recorder"), "IP_ADDR", ip);
	GetJsonDataPro(record, "Type", name);        
	GetJsonDataPro(record, "Time", time);    
	if(!strcmp(event, "Play_Start"))
	{
		//if( playback_get_file(inet_addr(ip), recfile, fileTemp) == 0)
		if(api_get_file_to_local(inet_addr(ip), recfile, fileTemp)== 0)
		{
			snprintf(sourcename, 100, "[%s] %s", time, name);
			StartInitOneMenu(MENU_156_PLAYBACK2,0,1);
			api_start_ffmpegPlay(fileTemp, playback_show_cb);
		}
		else
		{
			MessageReportDisplay(NULL, 0, "get file error!", COLOR_RED, 1, 5);
		}
	}
	else if(!strcmp(event, "Play_Next"))
	{
		//if( playback_get_file(inet_addr(ip), recfile, fileTemp) == 0)
		if(api_get_file_to_local(inet_addr(ip), recfile, fileTemp)== 0)
		{
			snprintf(sourcename, 100, "[%s] %s", time, name);
			DisplayPlaybackInfo();
			api_change_file_ffmpegPlay(fileTemp, playback_show_cb);
		}
	}
}

void playback_turning(int act)
{
	int i,cnt=0;
	char recfile[200] = {0};
	cJSON *item;
	cJSON* view = cJSON_CreateArray();
	if(API_TB_SelectBySortByName("EventRecord", view, NULL, 0) > 0)
	{
		cnt = cJSON_GetArraySize(view);
	}

	for(i=0;i<cnt;i++)
	{
		item=cJSON_GetArrayItem(view,i);
		GetJsonDataPro(item, "Rec_File", recfile);
		if(strstr(recfile, targFile) !=NULL)
		{
			break;
		}
	}

	if(i ==cnt)
	{
		return;
	}
	if(act ==1)
	{
		i++;
		if(i>=cnt)
		{
			i = 0;
		}			
	}
	else
	{
		i--;
		if(i<0)
		{
			i = cnt - 1;
		}			
	}
	item=cJSON_GetArrayItem(view,i);
	api_playback_event("Play_Next", item);
	cJSON_Delete(view);
}

/*************************************************************************************************
06. playback
*************************************************************************************************/
void MENU_156_PLAYBACK2_Init(int uMenuCnt)	
{
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	OSD_GetIconInfo(ICON_226_PlaybackDel, &pos, &hv);
	Get_SpriteSize(SPRITE_DelConfirm, &xsize, &ysize);
	delSpriteX = pos.x + (hv.h - pos.x)/2 - xsize/2;
	delSpriteY = pos.y + (hv.v - pos.y)/2 - ysize/2;
	deletVideoConfirm = 0;
	DisplayPlaybackInfo();
}

void MENU_156_PLAYBACK2_Exit(void)
{
	api_stop_ffmpegPlay();
}

void MENU_156_PLAYBACK2_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int curnLen; int maxLen;} *pData;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
				
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_046_Replay:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					api_ffmpegPlayPause();
					break;
					
				case ICON_225_PlaybackClose:					
					popDisplayLastMenu();
					break;
				case ICON_226_PlaybackDel:
					if(deletVideoConfirm == 0)
					{
						deletVideoConfirm = 1;
						API_SpriteDisplay_XY(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						
					}
					else
					{
						DelOneRecRecord(targFile);
						popDisplayLastMenu();
					}
					break;
				case ICON_227_PlaybackNext:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turning(0);
					break;
				case ICON_228_PlaybackLast:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turning(1);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_PLYBAK_STEP:
				//pData = (arg + sizeof(SYS_WIN_MSG));
				//DisplayPlaybackTime(pData->curnLen);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
		
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

#define MaxLocalRecFileTime		3600*24*30

int get_onedir_oldest_file(char *path,char *file)
{
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	int ret=0;
	int oldest_time=0;
	
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}

	while(entry = readdir(dir))
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		{
			continue;
		}
		snprintf(fullname,100,"%s/%s",path,entry->d_name);

		struct stat st; 
		
		if ( stat( fullname, &st ) == -1 )
		{
			printf(" get file stat error\n");     
		}

		if( S_ISDIR(st.st_mode) )      
		{
			continue;				
		}     
		else 
		{	
			if(oldest_time==0||st.st_mtime<oldest_time)		
			{
				strcpy(file,fullname);
				oldest_time=st.st_mtime;
				ret=1;
			}
		}
	}	
	closedir(dir);
	
	return ret;
}
int DelRecFileBeforeTime(char *path, int deltime)
{
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	struct tm *tblock; 	
	time_t now_time = time(NULL); 
	tblock=localtime(&now_time);
	
	
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	while(entry = readdir(dir))
	{
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		
		snprintf(fullname,100,"%s/%s",path,entry->d_name);

		struct stat st; 
		
		if ( stat( fullname, &st ) == -1 )
		{
			printf(" get file stat error\n");     
		}

		if( S_ISDIR(st.st_mode) )      
		{
				continue;				
		}     
		else 
		{	
			if(abs(now_time - st.st_mtime) > deltime)
			{
				remove(fullname);				
			}
		}

	}
	
	closedir(dir);
	
	return 0;
}

int getDiscFreeSize( char *path )
{
	int freeSize = 0;
	int totalSize = 0;

	if( getDiscSize(path, &freeSize, &totalSize ) != 0 )
		return -1;

		
	return freeSize/1024;
}

int getDiscUseSize(char *path)
{
	DIR *dir = NULL;
	char fullname[100];
	struct dirent *entry;
	struct stat st;
	int sizeuse = 0;

	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	while(entry = readdir(dir))
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		{
			continue;
		}
		snprintf(fullname,100,"%s/%s",path,entry->d_name);
		if ( stat( fullname, &st ) == 0 )
		{
			if( S_ISDIR(st.st_mode) )      
			{
				continue;				
			}     
			else 
			{
				sizeuse += st.st_size;
			}
		}
	}
	closedir(dir);
	return sizeuse/(1024*1024);
}


int getDiscFileNum(char *path, char* keyWord)
{
	char linestr[100];
	int ret = 0;
	
	snprintf(linestr, 100, "ls -l %s|grep \"%s\"|wc -l\n", path, keyWord);
	
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	if(fgets(linestr,100,pf) != NULL)
	{
		ret = atoi(linestr);
	}
	else
	{
		ret = -2;
	}

	printf(" ---------------getDiscFileNum : %d \n", ret);
	
	pclose(pf);
	
	return ret;
}

int localMemSpaceManager(int filesize)
{
	DIR *dir;
	int sizeLimit,freesize,usesize,sizeNeed; 
	char filename[200];
	char filepath[100];
	char cmd[100];
	snprintf(filepath,100,"%s",JPEG_STORE_DIR2);
	if( access( filepath, F_OK ) < 0 )
	{
		mkdir( filepath, 0777 );
	}
	freesize = getDiscFreeSize(filepath);
	usesize = getDiscUseSize(filepath);
	sizeLimit = freesize + usesize - 10;
	sizeNeed = (10+sizeLimit/2)>(filesize + 10)? (10+sizeLimit/2) : (filesize + 10);  
	dprintf("limitsize=%d freesize=%d usesize=%d\n", sizeLimit, freesize, usesize);
	API_PublicInfo_Write_Int(PB_MEM_LIMIT_SIZE, sizeLimit);
	
	if(freesize < filesize + 10 )
	{
		while(getDiscFreeSize(filepath) < sizeNeed && getDiscFileNum(filepath, ".mp4"))
		{
			if(get_onedir_oldest_file(filepath,filename))
			{
				remove(filename);
			}
		}
		if(getDiscFreeSize(filepath) < filesize + 10)
		{
			snprintf(cmd,100,"rm -r %s",JPEG_STORE_DIR2);
			system(cmd);
			return -1;//空间维护失败
		}
	}
	else
	{
		DelRecFileBeforeTime(filepath, MaxLocalRecFileTime);
	}

	return 0;
}

int checkMemSpaceTimer(int time)
{
	if(time == 10)
	{
		localMemSpaceManager(2);
		return 2;
	}
	return 0;
}
int api_get_file_to_local(int ip, char* filescr, char* filetar)
{
	int ret = -1;
	if(MakeDir(JPEG_STORE_DIR2) != 0)
	{
		return -1;
	}
	if(filescr && strstr(filescr, ".mp4")!= NULL)
	{
		strcpy(targFile, basename(filescr));
		snprintf(filetar, 200, "%s%s", JPEG_STORE_DIR2, targFile);	
		if( access( filetar, F_OK ) == 0 )
		{	
			ret = 0;		
		}
		else
		{
			ret = Api_TftpReadFileFromDevice(ip, dirname(filescr), targFile, JPEG_STORE_DIR2, targFile);
			API_Add_TimingCheck(checkMemSpaceTimer, 10);
		}
	}
	return ret;
}

int ShellCmd_MemSpace(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	int ret = localMemSpaceManager(atoi(jsonstr));

	ShellCmdPrintf("memSpaceManager ret =%d \n", ret);
}