#include "MENU_public.h"
#include "onvif_discovery.h"
#include "MENU_116_MultiRec.h"
#include "obj_GetIpByNumber.h"
#include "obj_business_manage.h"		
#include "obj_IPCTableSetting.h"
#include "MENU_119_IpcRecord.h"

//#define	MultiRecIconMax		5
#define recIpcMax		4

int MultiRecIconMax;
int MultiRecIconSelect;
int MultiRecPageSelect;
int dvrState;

const char * dvrSetDisplay[2] = {"By time", "By size"};
int dvrSetSelect;
int recSize=100;
int recTime=10;
unsigned char inputRecSize[5] = {0};
unsigned char inputRecTime[5] = {0};
 int recIpcIndex[4]={-1,-1,-1,-1};


const IconAndText_t MultiRecIconTable[] = 
{
	{ICON_RecordCH1, 			MESG_TEXT_RecordCH1},	 
	{ICON_RecordCH2, 			MESG_TEXT_RecordCH2},
	{ICON_RecordCH3,			MESG_TEXT_RecordCH3},
	{ICON_RecordCH4, 			MESG_TEXT_RecordCH4},
	{ICON_RecordSetting, 		MESG_TEXT_Setting},
	{ICON_RecordStart,			MESG_TEXT_ICON_Start},
};
const int MultiRecIconTableNum = sizeof(MultiRecIconTable)/sizeof(MultiRecIconTable[0]);

void multi_vd_record_notice_disp(int on, int index)
{
	int x,y;
	POS pos;
	SIZE hv;
	char display[20];
	OSD_GetIconInfo(ICON_Left_list1+index, &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2;
	
	if(on)
	{
		sprintf(display, "[%d]", GetRecCurSec(index));
		API_OsdStringDisplayExt(x-60, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
		API_SpriteDisplay_XY(x,y,SPRITE_RECORDING);	
	}
	else
	{
		API_SpriteClose(x,y,SPRITE_RECORDING);	
	}
}


static void DisplayMultiRecPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;
	IPC_ONE_DEVICE dat;
		
	for(i = 0; i < MultiRecIconMax; i++)
	{
		OSD_GetIconInfo(ICON_short_list1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = pos.x+(hv.h - pos.x)/2;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*MultiRecIconMax+i < MultiRecIconTableNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MultiRecIconTable[i+page*MultiRecIconMax].iConText, 1, val_x-x);
			x += (hv.h - pos.x)/2;
			if(i < recIpcMax)
			{
				if(recIpcIndex[i] != -1)
				{
					GetIpcCacheRecord(recIpcIndex[i], &dat);
					snprintf(display, 100, "%s", dat.NAME);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
					//multi_vd_record_notice_disp(multi_vd_record[i].state, i); 
				}
			}
			if(MultiRecIconTable[i+page*MultiRecIconMax].iCon == ICON_RecordSetting)
			{
				if(!dvrSetSelect)
					sprintf(display, "%d min", recTime);
				else
					sprintf(display, "%d MB", recSize);
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
			}
		}
	}
	pageNum = MultiRecIconTableNum/MultiRecIconMax + (MultiRecIconTableNum%MultiRecIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

int API_MultiRec_start( void )
{
#if 0
	int i;
	IPC_ONE_DEVICE dat;
	int vdtype;
	if(!Judge_SdCardLink()) 
	{
		return -1;
	}
	//if( getSdcardSpace() != 0 )
	//{
	//	return -1;
	//}
	
	for( i = 0; i < recIpcMax; i++ )
	{

		if(multi_vd_record[i].state == 1)
		{
			multi_vd_record[i].state = 0;
			api_stop_ffmpeg_rec(i);
			//API_RecordEnd_mux(i); 
			continue;
		}
		if(recIpcIndex[i] != -1)
		{
			GetIpcCacheRecord(recIpcIndex[i], &dat);
			if(Check_OneRec_Space(dat.CH_DAT[dat.CH_REC].bitrate, recTime) != 0)
			{
				continue;
			}
			if(ipc_check_online(dat.IP, dat.ID) != 0)
			{
				continue;
			}
			//printf("API_MultiRec_start index:%d CH_REC:%d w:%d h:%d\n", i, dat.CH_REC, dat.CH_DAT[dat.CH_REC].width, dat.CH_DAT[dat.CH_REC].height);
			//(ipc_type[i] == -2)
			//pi_stop_ffmpeg_rec(i);
			//multi_vd_record[i].fps = dat.CH_DAT[dat.CH_REC].framerate;
			//multi_vd_record[i].vd_type = api_start_ffmpeg_rec(i, dat.CH_DAT[dat.CH_REC].rtsp_url);
			if(start_ffmpeg_rec(i, dat.CH_DAT[dat.CH_REC].rtsp_url, dat.CH_DAT[dat.CH_REC].framerate, recTime) == 0)
			{
				multi_vd_record[i].state = 1;
				multi_vd_record_notice_disp(1, i); 
				//multi_video_record_start(ch);
			}
		}
	}
#endif
}
int API_MultiRec_stop(void)
{
#if 0
	int i;
	for( i = 0; i < recIpcMax; i++ )
	{
		if(multi_vd_record[i].state == 1)
		{
			printf("API_MultiRec_stop ch=%d \n", i);
			multi_vd_record_notice_disp(0, i); 
			multi_vd_record[i].state = 0;
			api_stop_ffmpeg_rec(i);
			//API_RecordEnd_mux(i); 
		}
	}
#endif
}

int API_OneRec_stop(int ch)
{
#if 0
	if(multi_vd_record[ch].state == 1)
	{
		printf("API_OneRec_stop ch=%d \n", ch);
		multi_vd_record_notice_disp(0, ch); 
		multi_vd_record[ch].state = 0;
		api_stop_ffmpeg_rec(ch);
	}
#endif
}

int API_OneRec_start( int ch, int min )
{
#if 0
	IPC_ONE_DEVICE dat;
	//printf("API_OneRec_start min=%d\n", min);
 	if(!Judge_SdCardLink()) 
	{
		return -1;
	}
		
	if(multi_vd_record[ch].state == 1)
	{
		multi_vd_record[ch].state = 0;
		api_stop_ffmpeg_rec(ch);
		//API_RecordEnd_mux(ch); 
		return 0;
	}

	if(recIpcIndex[ch] != -1)
	{
		GetIpcCacheRecord(recIpcIndex[ch], &dat);
		if(Check_OneRec_Space(dat.CH_DAT[dat.CH_REC].bitrate, min) != 0)
		{
			return -1;
		}
		if(ipc_check_online(dat.IP, dat.ID) != 0)
		{
			return -1;
		}
		//(ipc_type[ch] == -2)
		//pi_stop_ffmpeg_rec(ch);
 		if(start_ffmpeg_rec(ch, dat.CH_DAT[dat.CH_REC].rtsp_url,dat.CH_DAT[dat.CH_REC].framerate, min) == 0)
		{
			multi_vd_record[ch].state = 1;
			multi_vd_record_notice_disp(1, ch); 
			//multi_video_record_start(ch);
		}
	}
	return 0;
#endif
}

void MENU_116_MultiRec_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	MultiRecIconMax = GetListIconNum();
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_DVR, 1, 0);
	MultiRecIconSelect = 0;
	MultiRecPageSelect = 0;
	dvrState = ICON_DVR;
	API_MenuIconDisplaySelectOn(dvrState);
	DisplayMultiRecPageIcon(MultiRecPageSelect);

}

int SaveRecTimeSet(char* input)
{
	int time = atoi(input);

	if(time >= 1 && time <= 60)//*24
	{
		strcpy(inputRecTime, input);
		recTime = atoi(inputRecTime);
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

int SaveRecSizeSet(void)
{
	recSize = atoi(inputRecSize);

	if(recSize >= 10 && recSize <= 1024)
	{
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

void SetDvrSelect(int select)
{
	dvrSetSelect = select;
}

void MENU_116_MultiRec_Exit(void)
{
	#if 0
	int i;
	for( i = 0; i < recIpcMax; i++ )
	{
		if(ipc_type[i] == -2)
		{
			api_stop_ffmpeg_rec(i);
		}
	}
	int i;
	for( i = 0; i < recIpcMax; i++ )
	{
		api_stop_ffmpeg_rec(i);
		if(multi_vd_record[i].state == 1)
		{
			API_RecordEnd_mux(i); 
			multi_vd_record[i].state = 0;
			SaveMultiRecordProcess(i);
		}
	}
	#endif
}

void MENU_116_MultiRec_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int ret;
	int iconSel;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					//API_MultiRec_stop();
					break;
				case KEY_UNLOCK:
				break;
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_DVR_QUAD:
					if(dvrState != ICON_DVR_QUAD)
					{
						API_MenuIconDisplaySelectOff(dvrState);
						dvrState = ICON_DVR_QUAD;
						API_MenuIconDisplaySelectOn(dvrState);
						//API_DvrQuart_start();
					}
					break;
				case ICON_DVR_REC:
					StartInitOneMenu(MENU_119_IpcRecord,0,1);
					break;
				case ICON_DVR_SD:
					StartInitOneMenu(MENU_040_SD_CARD_INFO,0,1);
					break;
				case ICON_DVR_SCHEDULE:
					StartInitOneMenu(MENU_123_DvrSchedule,0,1);
					break;
				case ICON_short_list1:
				case ICON_short_list2:
				case ICON_short_list3:
				case ICON_short_list4:
				case ICON_short_list5:
				case ICON_short_list6:
				case ICON_short_list7:
				case ICON_short_list8:
				case ICON_short_list9:
				case ICON_short_list10:
					iconSel = MultiRecPageSelect*MultiRecIconMax + GetCurIcon() - ICON_short_list1;
					if(iconSel >= MultiRecIconTableNum)
					{
						return;
					}
					switch(MultiRecIconTable[iconSel].iCon)
					{
						case ICON_RecordCH1:
						case ICON_RecordCH2:
						case ICON_RecordCH3:
						case ICON_RecordCH4:
							Enter_IPC_ListSelect(iconSel);
							break;
						case ICON_RecordSetting:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_Time, inputRecTime, 4, COLOR_WHITE, inputRecTime, 1, SaveRecTimeSet);
							#if 0
							InitPublicSettingMenuDisplay(2, dvrSetDisplay);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_RecordSet, 2, dvrSetSelect, SetDvrSelect);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							#endif
							break;
						
						case ICON_RecordStart:
							//API_MultiRec_start();
							break;
						
					}
					break;
				case ICON_Left_list1:
				case ICON_Left_list2:
				case ICON_Left_list3:
				case ICON_Left_list4:
					iconSel = GetCurIcon() - ICON_Left_list1;
					if(iconSel < recIpcMax)
						//API_OneRec_start(iconSel, recTime);

					break;
				#if 0	
				case ICON_Left_list5:
					if(!dvrSetSelect)
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_Time, inputRecTime, 2, COLOR_WHITE, inputRecTime, 1, SaveRecTimeSet);
					else
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_FwUpgrage_CodeSize, inputRecSize, 4, COLOR_WHITE, inputRecSize, 1, SaveRecSizeSet);
					break;
				#endif	
						
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}

#if 0
extern REC_INSTANCE_t rec_ins_array[4];
void multi_vd_record_notice_disp(int on, int index)
{
	int x,y;
	POS pos;
	SIZE hv;
	char display[20];
	OSD_GetIconInfo(ICON_Left_list1+index, &pos, &hv);
	x = pos.x;
	y = pos.y+(hv.v - pos.y)/2;
	
	if(on)
	{
		sprintf(display, "[%d]", rec_ins_array[index].rec_mux_ins.CurrSec/15);
		API_OsdStringDisplayExt(x-60, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
		API_SpriteDisplay_XY(x,y,SPRITE_RECORDING);	
	}
	else
	{
		API_SpriteClose(x,y,SPRITE_RECORDING);	
	}
}

void multi_vd_record_stop(int index)
{
	char data[1];
	printf("+++++++++multi_vd_record_stop, index[%d] state[%d]\n",index, multi_vd_record[index].state); 
	if( multi_vd_record[index].state )
	{
		multi_vd_record[index].state = 0;
 		api_stop_ffmpeg_rec(index);
		multi_vd_record_notice_disp(multi_vd_record[index].state, index);
		//SaveMultiRecordProcess(index);
	}
}

int multi_video_record_start( int index )		
{
	int result = -1;
	char temp[20];
	char recfile[VIDEO_REC_FILENAME_LEN+1];
	time_t t;
	struct tm *tblock; 	
	
	//if(multi_vd_record[index].state == 1)
	//{
	//	API_RecordEnd_mux(index); 
	//	return 0;
	//}

	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"20%02d%02d%02d_%02d%02d%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	sprintf( multi_vd_record[index].filename,"%s-CH%d.avi", recfile, index+1 ); 

	if(!dvrSetSelect)
		result = API_RecordStart_mux( multi_vd_record[index].filename, recTime, multi_vd_record_stop, multi_vd_record[index].vd_type,index, 0, multi_vd_record[index].fps);
	else
		result = API_RecordStart_mux( multi_vd_record[index].filename, 0, multi_vd_record_stop, multi_vd_record[index].vd_type, index, recSize, multi_vd_record[index].fps);
	if( result == 0 )
	{
		multi_vd_record[index].state = 1;	//开始录像
 		multi_vd_record_notice_disp(multi_vd_record[index].state, index);	
	}
	else
	{
		multi_vd_record[index].state = 0;
		strcpy(multi_vd_record[index].filename, "-");
	}
	return 0;
}


void SaveMultiRecordProcess(int index)
{
	#if 0
	IPC_RECORD_DAT_T record;
	IPC_ONE_DEVICE dat;
	printf("+++++++++SaveMultiRecordProcess, index[%d]\n",index); 
	GetIpcCacheRecord(recIpcIndex[index], &dat);
	strcpy( record.recDevice, dat.NAME);
	record.recLength = (rec_ins_array[index].rec_mux_ins.CurrSec/15)/60;
	record.recSize = (rec_ins_array[index].rec_mux_ins.MoviEnd/1024)/1024;
	strcpy(record.recFile, multi_vd_record[index].filename);
	printf("+++++++++SaveMultiRecordProcess, recDevice[%s] recFile[%s] recSize[%d]\n",record.recDevice, record.recFile, record.recSize); 
	AddIpcRecRecord(&record);
	#endif
}

#endif
