#include "MENU_public.h"
#include "MENU_052_SystemSetting.h"

#define	SystemSetting_ICON_MAX	5
int SystemSettingIconSelect;
int SystemSettingPageSelect;
char divertTime[4];
char broadcastTime[4];
int speakVolume;
int micVolume;
char inputBuffer[4];
int resolution_type;

const SystemSettingIcon SystemSettingIconTable[] = 
{
//	{ICON_313_DivertTimeSet,	MESG_TEXT_Icon_313_DivertTimeSet}, 
//	{ICON_315_BroadcastTimeSet,	MESG_TEXT_ICON_315_BroadcastTimeSet}, 
	{ICON_316_MicVolumeSet,		MESG_TEXT_ICON_316_MicVolumeSet}, 
	{ICON_317_SpeakerVolumeSet, MESG_TEXT_ICON_317_SpeakerVolumeSet}, 
//	{ICON_318_VideoResolution,	MESG_TEXT_ICON_318_VideoResolution}, 
};

const int SystemSettingIconNum = sizeof(SystemSettingIconTable)/sizeof(SystemSettingIconTable[0]);

static void DisplaySystemSettingPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	uint16 monitorTime;
	char display[20];
	unsigned char value;
	POS pos;
	SIZE hv;
		
	//API_DisableOsdUpdate();
	for(i = 0; i < SystemSetting_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*SystemSetting_ICON_MAX+i < SystemSettingIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, SystemSettingIconTable[i+page*SystemSetting_ICON_MAX].iConText, 1, val_x - x);
			x += (hv.h - pos.x)/2;
			switch(SystemSettingIconTable[i+page*SystemSetting_ICON_MAX].iCon)
			{
				case ICON_313_DivertTimeSet:
					API_Event_IoServer_InnerRead_All(DivertTime, display);
					value = atoi(display);
					
					snprintf(divertTime, 4, "%d", value);
					snprintf(display, 20, "[%d]", value);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h-x);
					break;

				case ICON_315_BroadcastTimeSet:
					API_Event_IoServer_InnerRead_All(MasterBroadcastTime,display);
					value = atoi(display);
					snprintf(broadcastTime, 4, "%d", value);
					snprintf(display, 20, "[%d]", value);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h-x);
					break;
				case ICON_316_MicVolumeSet:
					snprintf(display, 20, "[%d]", micVolume);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h-x);
					break;
				case ICON_317_SpeakerVolumeSet:
					snprintf(display, 20, "[%d]", speakVolume);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h-x);
					break;
				case ICON_318_VideoResolution:
					API_Event_IoServer_InnerRead_All(VedioResolutionSet, display);
					value = atoi(display);
					snprintf(display, 20, "[%d]", value);
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, 0);
					break;
				default:
					break;
			}
		}
	}
	pageNum = SystemSettingIconNum/SystemSetting_ICON_MAX + (SystemSettingIconNum%SystemSetting_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

int SaveDivertTime(const char* string)
{
	unsigned char value;
	char temp[20];

	strcpy(divertTime, string);
	value = atoi(divertTime);
	
	if(value <= 60 && value > 0)
	{
		sprintf(temp, "%d", value);
		API_Event_IoServer_InnerWrite_All(DivertTime, temp);
		return 1;
	}
	else
	{
		return 0;
	}
}

int SaveBroadcastTime(const char* string)
{
	unsigned char value;
	
	strcpy(broadcastTime, string);
	value = atoi(broadcastTime);
	
	if(value <= 240 && value > 0)
	{
		char temp[20];
		sprintf(temp, "%d", value);
		API_Event_IoServer_InnerWrite_All(MasterBroadcastTime, temp);
		return 1;
	}
	else
	{
		return 0;
	}
}

int SPKVolumeSave(int vol)
{
	if(vol <= 9 && vol >= 0)
	{
		speakVolume = vol+1;
		
		char temp[20];
		sprintf(temp, "%d", vol+1);
		API_Event_IoServer_InnerWrite_All(SpeakVolumeSet, temp);
	}

	return 1;
}

int MICVolumeSave(int vol)
{
	if(vol <= 4 && vol >= 0)
	{
		micVolume = vol+1;
		char temp[20];
		sprintf(temp, "%d", vol+1);

		API_Event_IoServer_InnerWrite_All(MicVolumeSet, temp);
	}
	return 1;
}

int SaveVedioResolution(const char* string)
{
	unsigned char value;

	strcpy(inputBuffer, string);
	value = atoi(inputBuffer);

	if(value <= 2)
	{
		char temp[20];
		sprintf(temp, "%d", value);
		API_Event_IoServer_InnerWrite_All(VedioResolutionSet, temp);
		resolution_type = value;
		return 1;
	}
	else
	{
		return 0;
	}
}


void LoadAudioVolume(void)
{
	char temp[20];
	
	API_Event_IoServer_InnerRead_All(MicVolumeSet, temp);
	micVolume = atoi(temp);
	API_Event_IoServer_InnerRead_All(SpeakVolumeSet, temp);
	speakVolume = atoi(temp);
	API_Event_IoServer_InnerRead_All(VedioResolutionSet, temp);
	resolution_type = atoi(temp);	
}


void MENU_052_SystemSetting_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	SystemSettingPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_032_System);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_SystemSettings, 1, 0);	
	LoadAudioVolume();
	DisplaySystemSettingPageIcon(SystemSettingPageSelect);

}


void MENU_052_SystemSetting_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_032_System);
}

void MENU_052_SystemSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	int i, j;
	unsigned char value;
	char tempChar[30];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					//popDisplayLastMenu();
					popDisplayLastNMenu(2);
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&SystemSettingPageSelect, SystemSetting_ICON_MAX, SystemSettingIconNum, (DispListPage)DisplaySystemSettingPageIcon);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&SystemSettingPageSelect, SystemSetting_ICON_MAX, SystemSettingIconNum, (DispListPage)DisplaySystemSettingPageIcon);
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					SystemSettingIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(SystemSettingPageSelect*SystemSetting_ICON_MAX+SystemSettingIconSelect >= SystemSettingIconNum)
					{
						return;
					}
					
					switch(SystemSettingIconTable[SystemSettingPageSelect*SystemSetting_ICON_MAX+SystemSettingIconSelect].iCon)
					{
						case ICON_313_DivertTimeSet:
							API_Event_IoServer_InnerRead_All(DivertTime, tempChar);
							value = atoi(tempChar);
							
							snprintf(divertTime, 4, "%d", value);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputDivertTime, divertTime, 2, COLOR_WHITE, divertTime, 1, SaveDivertTime);
							break;		
						case ICON_315_BroadcastTimeSet:
							API_Event_IoServer_InnerRead_All(MasterBroadcastTime, tempChar);
							value = atoi(tempChar);
							
							snprintf(broadcastTime, 4, "%d", value);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputBroadcastTime, broadcastTime, 3, COLOR_WHITE, broadcastTime, 1, SaveBroadcastTime);
							break;		
						case ICON_316_MicVolumeSet:
							for(i = 0; i < 5; i++)
							{
								snprintf(tempChar, 30, "%d", (i+1));
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							
							EnterPublicSettingMenu(ICON_026_InstallerSetup, MESG_TEXT_InputMicVolume, 5, micVolume-1, MICVolumeSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;		
						case ICON_317_SpeakerVolumeSet:
							for(i = 0; i < 10; i++)
							{
								snprintf(tempChar, 30, "%d", (i+1));
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}

							EnterPublicSettingMenu(ICON_026_InstallerSetup, MESG_TEXT_InputSpeakerVolume, 10, speakVolume-1, SPKVolumeSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;	
							
						case ICON_318_VideoResolution:
							API_Event_IoServer_InnerRead_All(VedioResolutionSet, tempChar);
							value = atoi(tempChar);
							snprintf(inputBuffer, 4, "%d", value);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_318_VideoResolution, inputBuffer, 1, COLOR_WHITE, inputBuffer, 1, SaveVedioResolution);
							break;		
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}

}


