#include "MENU_038_OnlineManageReboot.h"
#include "obj_ProgInfoByIp.h"
#include "obj_DeviceManage.h"
#include "MENU_public.h"

#include "iperf_udp_server.h"
static int iperf_disp_x = 0;
static int iperf_disp_y = 0;
void menu38_display_iperf_bandwidth(int type, void* pbuf,int len )
{
	printf("%s\n",pbuf);
	API_OsdStringDisplayExt(iperf_disp_x, iperf_disp_y, COLOR_RED, pbuf, strlen(pbuf), 1,STR_UTF8, 0 );		
}

#define	OnlineManageReboot_ICON_MAX		5
int onlineManageRebootIconSelect;
int onlineManageRebootPageSelect;
int onlineManageRebootTimeCnt = 0;
int rebootConfirm;
int rebootConfirmSelect;
extern int ResIdNum;
const IconAndText_t onlineManageRebootSet[] = 
{
	{ICON_292_Reboot,					MESG_TEXT_ICON_292_Reboot},
	{ICON_BACKUP_RESTORE,				MESG_TEXT_ICON_BACKUP_RESTORE},
	{ICON_RES_Manger,				MESG_TEXT_ICON_RES_Manger},		//czn_20191219
	{ICON_IPERF_TEST,				MESG_TEXT_ICON_IPERF_TEST},		//lzh_20210922
};

const unsigned char onlineManageRebootSetNum = sizeof(onlineManageRebootSet)/sizeof(onlineManageRebootSet[0]);

int WaitingForRemoteDeviceRebootFlag(void)
{
	return onlineManageRebootTimeCnt;
}
static void DisplayOnlineManageRebootPageIcon(int page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[100];
	int ms;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < OnlineManageReboot_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		if(ICON_RES_Manger == onlineManageRebootSet[i+page*OnlineManageReboot_ICON_MAX].iCon)
		{
			if(Get_Res_FromSd()<=0)
			{
				continue;
			}
		}
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*OnlineManageReboot_ICON_MAX+i < onlineManageRebootSetNum)
		{
			// test
			if( onlineManageRebootSet[i+page*OnlineManageReboot_ICON_MAX].iConText == 0 )	
			{
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, "Network test", strlen("Network test"), 1,STR_UTF8, 0 );
				iperf_disp_x = x + strlen("Intranet bandwidth")*18;
				iperf_disp_y = y;
			}
			else
			// test
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineManageRebootSet[i+page*OnlineManageReboot_ICON_MAX].iConText, 1, (hv.h - pos.x)/2);
		}
	}
	pageNum = onlineManageRebootSetNum/OnlineManageReboot_ICON_MAX + (onlineManageRebootSetNum%OnlineManageReboot_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 

void MENU_038_OnlineManageReboot_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	onlineManageRebootTimeCnt = 0;
	API_MenuIconDisplaySelectOn(ICON_050_Reboot);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	onlineManageRebootIconSelect = 0;
	onlineManageRebootPageSelect = 0;
	DisplayOnlineManageRebootPageIcon(onlineManageRebootPageSelect);
}

void MENU_038_OnlineManageReboot_Exit(void)
{
	onlineManageRebootTimeCnt = 0;
	rebootConfirm = 0;
	API_MenuIconDisplaySelectOff(ICON_050_Reboot);
	AutoPowerOffReset();
	// lzh_20210922_s
	api_iperf_udp_server_stop();
	// lzh_20210922_e
}

void MENU_038_OnlineManageReboot_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					onlineManageRebootIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect >= onlineManageRebootSetNum)
					{
						return;
					}

					if(onlineManageRebootTimeCnt)
					{
						return;
					}

					if(onlineManageRebootSet[onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect].iCon == ICON_292_Reboot)
					{
						if(ConfirmSelect(&rebootConfirm, &rebootConfirmSelect, &onlineManageRebootIconSelect, onlineManageRebootPageSelect, OnlineManageReboot_ICON_MAX))
						{
							return;
						}
						#if 0
						if(!rebootConfirm)
						{
							rebootConfirm = 1;
							onlineManageRebootIconSelect = onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect;
							API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(onlineManageRebootIconSelect), SPRITE_IF_CONFIRM);
							return;
						}
						else if(onlineManageRebootIconSelect == onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect)
						{
							rebootConfirm = 0;
							API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(onlineManageRebootIconSelect), SPRITE_IF_CONFIRM);
						}
						else
						{
							API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(rebootConfirmSelect&OnlineManageReboot_ICON_MAX), SPRITE_IF_CONFIRM);
							rebootConfirm = 1;
							rebootConfirmSelect = onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect;
							API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(onlineManageRebootIconSelect), SPRITE_IF_CONFIRM);
							return;
						}
						#endif
					}
					
					switch(onlineManageRebootSet[onlineManageRebootPageSelect*OnlineManageReboot_ICON_MAX+onlineManageRebootIconSelect].iCon)
					{
						case ICON_292_Reboot:
							ClearConfirm(&rebootConfirm, &rebootConfirmSelect, OnlineManageReboot_ICON_MAX);
							#if 0
							rebootConfirm = 0;
							API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(onlineManageRebootIconSelect), SPRITE_IF_CONFIRM);
							#endif
							if(onlineManageRebootTimeCnt == 0) 
							{
								DEVICE_REBOOT_REQ_T req;
								DEVICE_REBOOT_RSP_T result;
								
								req.IpAddr = GetSearchOnlineIp();
								req.subAddr = 0;

								if(DeviceManageRebootRequest(req.IpAddr, req, &result) == 0)
								{
									Power_Down_Timer_Stop();
								
									onlineManageRebootTimeCnt = ntohs(result.time); 
									BEEP_CONFIRM();
								}
								else
								{
									BEEP_ERROR();
								}
							}
							break;
						case ICON_BACKUP_RESTORE:
							if(onlineManageRebootTimeCnt == 0) 
							{
								ClearConfirm(&rebootConfirm, &rebootConfirmSelect, OnlineManageReboot_ICON_MAX);
								#if 0
								rebootConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(onlineManageRebootIconSelect), SPRITE_IF_CONFIRM);
								#endif
								//SetLocationSelect(1);
								//StartInitOneMenu(MENU_099_BACKUP_AND_RESTORE,0,1);
							}
							break;
						case ICON_RES_Manger:
							if(ResIdNum<=0)
							{
								#if 0
								API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_HaveNoSDCard,1, 0);								
								
								sleep(1);
								
								API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, CLEAR_STATE_W-DISPLAY_STATE_X, CLEAR_STATE_H);
							
								#endif
							}
							else
							
							{
								
								StartInitOneMenu(MENU_091_RES_Select, 0, 1); 	
							
							}
							break;		
						case ICON_IPERF_TEST:
							printf("onlineip is[%08x]\n",GetSearchOnlineIp());
							api_iperf_udp_server_stop();
							menu38_display_iperf_bandwidth(0,"please wait...           ",strlen("please wait...           "));
							if( api_iperf_udp_server_start( GetSearchOnlineIp(), 6789, 10, 1, 0, menu38_display_iperf_bandwidth ) != 0 )
							{
								menu38_display_iperf_bandwidth(0,"device connect failed     ",strlen("device connect failed     "));
							}
							break;
					}
						
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WaitingRemoteDeviceRebootTimeCnt:
				if(1)
				{
					int upTime, ip; 
					char display[50];
					
					snprintf(display, 50, "Rebooting  %ds!", onlineManageRebootTimeCnt);
					
					if(onlineManageRebootTimeCnt <= 30 && !(onlineManageRebootTimeCnt%3))
					{
						if(API_GetIpByMFG_SN(GetOnlineManageMFG_SN(), 1, &ip, &upTime) == 0)
						{
							onlineManageRebootTimeCnt = 0;
							snprintf(display, 50, "IP after restart:%03d.%03d.%03d.%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
							//API_OsdStringClearExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, 527, 40);
							//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+3*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);		
							snprintf(display, 50, "Reboot time %ds!", upTime+4);
						}
						else
						{
							if(onlineManageRebootTimeCnt == 0)
							{
								strcpy(display, "Reboot error!");
							}
						}
					}
					
					API_DisableOsdUpdate();
					//API_OsdStringClearExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, 527, 40);
					//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);		
					API_EnableOsdUpdate();
					
					if(onlineManageRebootTimeCnt == 0)
					{
						AutoPowerOffReset();
					}
				}
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}



void OnlineManageRebootTimerProcess(void)
{
	if(onlineManageRebootTimeCnt > 0)
	{
		onlineManageRebootTimeCnt--;
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WaitingRemoteDeviceRebootTimeCnt);
	}
}


