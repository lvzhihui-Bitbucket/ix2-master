#include "MENU_public.h"
#include "MENU_003_Intercom.h"
#include "../../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"
#include "obj_GetIpByInput.h"
#include "obj_GetIpByNumber.h"
#include "obj_ImNameListTable.h"

#define	INTERCOM_ICON_MAX	5
int intercomIconSelect;
int intercomPageSelect;
char inputCall[11];



const IntercomIcon intercomIconTableMode0[] = 
{
	{ICON_012_namelist,			MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,		MESG_TEXT_Icon_013_InnerCall},
	{ICON_014_GuardStation,		MESG_TEXT_Icon_014_GuardStation},
	//{ICON_015_InputNumbers,		MESG_TEXT_Icon_015_InputNumbers},
	{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
};

const int intercomIconNumMode0 = sizeof(intercomIconTableMode0)/sizeof(intercomIconTableMode0[0]);

const IntercomIcon intercomIconTableMode1[] = 
{
	{ICON_012_namelist,			MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,		MESG_TEXT_Icon_013_InnerCall},
	//{ICON_014_GuardStation,		MESG_TEXT_Icon_014_GuardStation},
	//{ICON_015_InputNumbers,		MESG_TEXT_Icon_015_InputNumbers},
	{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
};

const int intercomIconNumMode1 = sizeof(intercomIconTableMode1)/sizeof(intercomIconTableMode1[0]);

const IntercomIcon intercomIconTableMode2[] = 
{
	{ICON_012_namelist,			MESG_TEXT_Icon_012_Namelist},    
	{ICON_013_InnerCall,		MESG_TEXT_Icon_013_InnerCall},
	{ICON_014_GuardStation,		MESG_TEXT_Icon_014_GuardStation},
	//{ICON_015_InputNumbers,		MESG_TEXT_Icon_015_InputNumbers},
	//{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
};

const int intercomIconNumMode2 = sizeof(intercomIconTableMode2)/sizeof(intercomIconTableMode2[0]);

IntercomIcon *intercomIconTable;
int intercomIconNum;

static void DisplayIntercomPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char temp[10];
	int stringId;
	POS pos;
	SIZE hv;
	
	API_DisableOsdUpdate();
	
	for(i = 0; i < INTERCOM_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*INTERCOM_ICON_MAX+i < intercomIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, intercomIconTable[i+page*INTERCOM_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(intercomIconTable[i+page*INTERCOM_ICON_MAX].iCon)
			{
				case ICON_045_IntercomEnable:
					API_Event_IoServer_InnerRead_All(IntercomEnable,temp);
					stringId = (atoi(temp) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", hv.h - x);
					break;
			}
		}
	}
	pageNum = intercomIconNum/INTERCOM_ICON_MAX + (intercomIconNum%INTERCOM_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	API_EnableOsdUpdate();
}


Global_Addr_Stru DtAddr_Trs2_IpGlobalAddr(unsigned short dtaddr);

void IntercomEnableSet(int value)
{
	char temp[20];

	sprintf(temp, "%d", value ? 1 : 0);
	API_Event_IoServer_InnerWrite_All(IntercomEnable, temp);
}


int InputNumsCall_Start(char *input)	//czn_20190127
{
#if 1
	Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];
	IpCacheRecord_T record[MAX_CALL_TARGET_NUM];
	int i, getNum;
	
	memset(target_dev, 0, MAX_CALL_TARGET_NUM * sizeof(Call_Dev_Info));
	memset(record, 0, MAX_CALL_TARGET_NUM * sizeof(IpCacheRecord_T));
	getNum = API_GetIpByInput(input, record);

	if(getNum == 0)
	{
		BEEP_ERROR();
		return -1;
	}

	getNum = getNum > MAX_CALL_TARGET_NUM ? MAX_CALL_TARGET_NUM : getNum;
	IM_NameListRecord_T nameRecord;
	char nametemp[21] ={0};
		for(i = 0;i < GetImNameListRecordCnt();i++)
		{
			if(GetImNameListRecordItems(i, &nameRecord) != 0)
			{
				continue;
			}
		
			

			if(!memcmp(record[0].BD_RM_MS,nameRecord.BD_RM_MS,8))
			{
				if(strcmp(nameRecord.R_Name, "-"))
				{
					strcat(nametemp,nameRecord.R_Name);
				}
				else if(strlen(nameRecord.name1)>=1&&strcmp(nameRecord.name1, "-"))
				{
					strcat(nametemp,nameRecord.name1);
				}
				break;
			}
		}
	for(i = 0; i < getNum; i++)
	{
		memcpy(target_dev[i].bd_rm_ms,record[i].BD_RM_MS, 10);
		target_dev[i].ip_addr = record[i].ip;
		
		sprintf(target_dev[i].name,"(%s)",input);
		
		record[i].BD_RM_MS[8] = 0;
		//if(memcmp(record[i].BD_RM_MS,GetSysVerInfo_bd(),4) == 0)
		if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(record[i].BD_RM_MS, "0099", 4))
		{
			sprintf(target_dev[i].name+strlen(target_dev[i].name),"IM%d",atol(record[i].BD_RM_MS+4));
			//strcpy(target_dev[i].name,record[i].BD_RM_MS+4);
		}
		else
		{
			//strcpy(target_dev[i].name,record[i].BD_RM_MS);
			strcat(target_dev[i].name,"IM(");
			memcpy(target_dev[i].name+strlen(target_dev[i].name),record[i].BD_RM_MS,4);
			strcat(target_dev[i].name,")");
			strcat(target_dev[i].name,record[i].BD_RM_MS+4);
		}
		if(nametemp[0]!=0)
		{
			strcat(target_dev[i].name," ");
			strcat(target_dev[i].name,nametemp);
		}
		//memcpy(target_dev[i].bd_rm_ms,record[i].BD_RM_MS, 10);
		//target_dev[i].ip_addr = record[i].ip;
	}

	API_CallServer_Invite(IxCallScene2_Active, getNum, target_dev);
	
	return 0;
#else
	
	int input_len = strlen(input);
	if(!(input_len == 8 || input_len == 10))
		return -1;
	
	for(i = 0;i < input_len;i ++)
	{
		if(!((input[i] >= '0') && (input[i] <= '9')))
		{
			return -1;
		}
	}
	if(input_len == 8)
	{
		input[8] = 0;
		strcat(input,"00");
	}

	GetIpRspData data;
	Call_Dev_Info target_dev[8];
	
	if(API_GetIpNumberFromNet(input, NULL, NULL, 2, 8, &data) != 0)
	{
		return;
	}

	data.cnt = data.cnt > 8 ? 8 : data.cnt;
	for(i = 0; i < data.cnt; i++)
	{
		memset(&target_dev[i],0,sizeof(Call_Dev_Info));
		memcpy(target_dev[i].bd_rm_ms, data.BD_RM_MS[i], 10);
		target_dev[i].ip_addr = data.Ip[i];
	}

	API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev);
	
	return 0;
#endif
}


void MENU_003_Intercom_Init(int uMenuCnt)
{
	Global_Addr_Stru gaddr;
	POS pos;
	SIZE hv;
	char temp[5];
	int intercomMenu_mode;
	API_Event_IoServer_InnerRead_All(GLListDis, (uint8*)temp);
	intercomMenu_mode =atoi(temp);
	if(intercomMenu_mode == 1)
	{
		intercomIconTable = intercomIconTableMode1;
		intercomIconNum= intercomIconNumMode1;
	}
	else if(intercomMenu_mode == 2)
	{
		intercomIconTable = intercomIconTableMode2;
		intercomIconNum= intercomIconNumMode2;
	}
	else
	{
		intercomIconTable = intercomIconTableMode0;
		intercomIconNum= intercomIconNumMode0;
	}
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		memset(inputCall, 0, 10);
		intercomPageSelect = 0;
	}
	else if(GetLastNMenu() == MENU_019_KEYPAD)
	{
		if(inputCall[0] != 0)
		{
			//��������
			//gaddr = DtAddr_Trs2_IpGlobalAddr(myAddr);
			//API_CallServer_Invite(DxCallScene5_Master,&gaddr);
			if(InputNumsCall_Start(inputCall) == 0)
			{
				;
			}
			else
			{
				BEEP_ERROR();
				memset(inputCall,0,sizeof(inputCall));
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputNumCall, inputCall, 10, COLOR_WHITE, NULL, 0, NULL);
				return;
			}
			//return;
		}
	}

	DisplayIntercomPageIcon(intercomPageSelect);
	//API_MenuIconDisplaySelectOn(ICON_204_Intercom);
	API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);	
	GetIconTextInfo(ICON_218_PublicSetTitle, &pos, &hv);
	//API_OsdUnicodeStringDisplay2(pos.x, pos.y, DISPLAY_TITLE_COLOR,MESG_TEXT_Intercom,1, hv.h-pos.x, hv.v - pos.y);
	API_OsdUnicodeStringDisplay(pos.x, pos.y, DISPLAY_TITLE_COLOR,MESG_TEXT_Intercom,1, 0);
	API_SpriteDisplay_XY(GetIconXY(ICON_218_PublicSetTitle).x, GetIconXY(ICON_218_PublicSetTitle).y, SPRITE_Intercom);		

	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_Intercom, 2, 0);
}

void MENU_003_Intercom_Exit(void)
{
	
}

void MENU_003_Intercom_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	Global_Addr_Stru gaddr;
	uint8 slaveMaster;
	char temp[20];
	int len;
	

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					intercomIconSelect = GetCurIcon() - ICON_007_PublicList1;

					if(intercomPageSelect*INTERCOM_ICON_MAX+intercomIconSelect >= intercomIconNum)
					{
						return;
					}
					
					switch(intercomIconTable[intercomPageSelect*INTERCOM_ICON_MAX+intercomIconSelect].iCon)
					{
						case ICON_012_namelist:
							API_Event_IoServer_InnerRead_All(IntercomEnable,temp);
							if(atoi(temp))
							{
								StartInitOneMenu(MENU_014_NAMELIST,0,1);
							}
							else
							{
								BEEP_ERROR();
							}
							break;
						case ICON_013_InnerCall:
							StartInitOneMenu(MENU_037_MS_LIST,0,1);
							break;
						case ICON_014_GuardStation:
							StartInitOneMenu(MENU_112_GSList,0,1);
							break;
						case ICON_015_InputNumbers:
							memset(inputCall,0,sizeof(inputCall));
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputNumCall, inputCall, 10, COLOR_WHITE, NULL, 0, NULL);
							break;
						case ICON_045_IntercomEnable:
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							API_Event_IoServer_InnerRead_All(IntercomEnable,temp);
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_Icon_045_IntercomEnable, 2, atoi(temp), IntercomEnableSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
					}
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


