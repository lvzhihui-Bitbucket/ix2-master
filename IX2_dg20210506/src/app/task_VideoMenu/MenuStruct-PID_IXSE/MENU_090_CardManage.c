#include "MENU_public.h"
#include "MENU_101_CardAdd.h"

#define	ID_CARD_ICON_MAX	5

FuncMenuType idCardSubMenu = MENU_100_CardList;

extern IdCardAdd_s add_one_id_card;
extern int getCardSourceIp;

void EnterIdCardMenu(FuncMenuType subMenu, int pushstack);



void MENU_090_CardManage_Init(int uMenuCnt)
{
	switch(idCardSubMenu)
	{
		case MENU_100_CardList:
			MENU_100_CardList_Init(uMenuCnt);
			break;
		case MENU_101_CardAdd:
			MENU_101_CardAdd_Init(uMenuCnt);
			break;
		case MENU_102_CardDelAll:
			MENU_102_CardDelAll_Init(uMenuCnt);
			break;
		case MENU_103_CardInfo:
			MENU_103_CardInfo_Init(uMenuCnt);
			break;
	}
}

void MENU_090_CardManage_Exit(void)
{
	switch(idCardSubMenu)
	{
		case MENU_100_CardList:
			MENU_100_CardList_Exit();
			break;
		case MENU_101_CardAdd:
			MENU_101_CardAdd_Exit();
			break;
		case MENU_102_CardDelAll:
			MENU_102_CardDelAll_Exit();
			break;
		case MENU_103_CardInfo:
			MENU_103_CardInfo_Exit();
			break;
	}
	
}

void MENU_090_CardManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
//				case ICON_200_Return:
//					popDisplayLastMenu();
//					break;
				
//				case ICON_047_Home:
//					GoHomeMenu();
//					break;

//				case ICON_IdCard_Source:
//					EnterIdCardMenu(MENU_072_IdCardSource, 0);
//					break;
//				case ICON_IdCard_Target:
//					EnterIdCardMenu(MENU_073_IdCardTarget, 0);
//					break;
				case ICON_IdCard_List:
					EnterIdCardMenu(MENU_100_CardList, 0);
					break;
				case ICON_IdCard_Add:
					if( idCardSubMenu != MENU_101_CardAdd )
					{
						memset(&add_one_id_card,0,sizeof(add_one_id_card));
					}
					EnterIdCardMenu(MENU_101_CardAdd, 0);
					break;
				case ICON_IdCard_DelAll:
					EnterIdCardMenu(MENU_102_CardDelAll, 0);
					break;

				default:
					switch(idCardSubMenu)
					{
						case MENU_100_CardList:
							MENU_100_CardList_Process(arg);
							break;
						case MENU_101_CardAdd:
							MENU_101_CardAdd_Process(arg);
							break;
						case MENU_102_CardDelAll:
							MENU_102_CardDelAll_Process(arg);
							break;
						case MENU_103_CardInfo:
							MENU_103_CardInfo_Process(arg);
							break;
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch(idCardSubMenu)
		{
			case MENU_100_CardList:
				MENU_100_CardList_Process(arg);
				break;
			case MENU_101_CardAdd:
				MENU_101_CardAdd_Process(arg);
				break;
			case MENU_102_CardDelAll:
				MENU_102_CardDelAll_Process(arg);
				break;
			case MENU_103_CardInfo:
				MENU_103_CardInfo_Process(arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		switch(idCardSubMenu)
		{
			case MENU_100_CardList:
				MENU_100_CardList_Process(arg);
				break;
			case MENU_101_CardAdd:
				MENU_101_CardAdd_Process(arg);
				break;
			case MENU_102_CardDelAll:
				MENU_102_CardDelAll_Process(arg);
				break;
			case MENU_103_CardInfo:
				MENU_103_CardInfo_Process(arg);
				break;
		}
	}
}

void EnterIdCardMenu(FuncMenuType subMenu, int pushstack)
{
	if(GetCurMenuCnt() == MENU_090_CardManage)
	{
		if(idCardSubMenu == subMenu)
		{
			return;
		}
		
		if( subMenu == MENU_100_CardList || subMenu == MENU_101_CardAdd || subMenu == MENU_102_CardDelAll )
		{
			clear_card_info_display_zone();
			MENU_090_CardManage_Exit();
			idCardSubMenu = subMenu;
			MENU_090_CardManage_Init(0);
		}
		else
		{
			idCardSubMenu = subMenu;
			StartInitOneMenu(MENU_090_CardManage,idCardSubMenu,pushstack);
		}
	}
	else
	{
		idCardSubMenu = subMenu;
		getCardSourceIp = 0;
		StartInitOneMenu(MENU_090_CardManage,idCardSubMenu,pushstack);
	}
}
