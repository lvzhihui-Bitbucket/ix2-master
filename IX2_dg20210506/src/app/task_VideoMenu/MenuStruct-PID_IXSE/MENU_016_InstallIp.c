#include "MENU_public.h"
#include "MENU_016_InstallIp.h"
#include "MenuInstallIp_Business.h"
#define	InstallIp_ICON_MAX	5
int installIpIconSelect;
int installIpPageSelect;
uint8 dhcpState;
char installIpInput[21];


const InstallIpIcon installIpSet[] = 
{
	{ICON_094_IPAssigned,					MESG_TEXT_ICON_094_IPAssigned},
	{ICON_095_IPAddress,					MESG_TEXT_ICON_095_IPAddress},
	{ICON_096_IPSubnetMask,					MESG_TEXT_ICON_096_IPSubnetMask},
	{ICON_097_IPGateway,					MESG_TEXT_ICON_097_IPGateway},
	{ICON_098_AddressHealthCheck,			MESG_TEXT_ICON_098_AddressHealthCheck},
};

const unsigned char installIpSetNum = sizeof(installIpSet)/sizeof(installIpSet[0]);

static void DisplayInstallIpPageIcon(int page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, display);
	dhcpState = atoi(display);
	//API_DisableOsdUpdate();
	for(i = 0; i < InstallIp_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*InstallIp_ICON_MAX+i < installIpSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, installIpSet[i+page*InstallIp_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(installIpSet[i+page*InstallIp_ICON_MAX].iCon)
			{
				case ICON_094_IPAssigned:
						//sprintf(display, "%s", dhcpState ? "DHCP&AUTO" : "STATIC");
					//API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					if(dhcpState==0)			//FOR_INDEXA
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR,MESG_TEXT_CusIpStatic,1,hv.h-x);
					else
					{
						sprintf(display, "%s","DHCP&AUTO");
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					}
					break;
				case ICON_095_IPAddress:
					strcpy(display, GetSysVerInfo_IP());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_096_IPSubnetMask:
					strcpy(display, GetSysVerInfo_mask());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_097_IPGateway:
					strcpy(display, GetSysVerInfo_gateway());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_098_AddressHealthCheck:
					break;
				default:
					break;
			}
		}
	}
	pageNum = installIpSetNum/InstallIp_ICON_MAX + (installIpSetNum%InstallIp_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void IPAssignedSet(int select)
{
	if(select >= 0 && select <= 1)
	{
		char temp[20];
		
		dhcpState = select;
		sprintf(temp, "%d", dhcpState);
		API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
		
		if(dhcpState)//DHCP
		{
			API_DHCP_SaveEnable(1);
		}
		else		 //��̬
		{
			API_DHCP_SaveEnable(0);
			SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
			ResetNetWork();
			Dhcp_Autoip_CheckIP();		//czn_20190604
			SysVerInfoUpdateIp(0);
		}
	}
}
#if 0
int SetIpAddress(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	dhcpState = atoi(temp);
	if(dhcpState == 0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_IP(tmepIp);
		SetNetWork(NULL, GetSysVerInfo_IP(), NULL, NULL);
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}

int SetSubNetMask(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	dhcpState = atoi(temp);
	if(dhcpState == 0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_mask(tmepIp);
		SetNetWork(NULL, NULL, GetSysVerInfo_mask(), NULL);
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}

int SetGateway(const char* ip)
{
	int temp[4];
	char tmepIp[16] = {0};
	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	dhcpState = atoi(temp);
	if(dhcpState == 0)
	{
		
		if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}

		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
		
		sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
		SetSysVerInfo_gateway(tmepIp);
		SetNetWork(NULL, NULL, NULL, GetSysVerInfo_gateway());
		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		return 1;
	}

	return 0;
}
#endif
void ProgIpAddr(int IPAssigned, const char* ip, const char* Mask, const char* Gateway)
{
	if(IPAssigned >= 0 && IPAssigned <= 1)
	{
		char temp[20];
		
		dhcpState = IPAssigned;
		sprintf(temp, "%d", dhcpState);
		API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
		
		if(dhcpState)//DHCP
		{
			API_DHCP_SaveEnable(1);
		}
		else		 //��̬
		{

			int temp[4];
			char tmepIp[16] = {0};

			if(ip != NULL)
			{
				if(sscanf(ip, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
				{
					return 0;
				}
			
				if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
				{
					return 0;
				}
				
				sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
				
				SetSysVerInfo_IP(tmepIp);
			}

			if(Gateway != NULL)
			{
				if(sscanf(Gateway, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
				{
					return 0;
				}
			
				if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
				{
					return 0;
				}
				
				sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
				
				SetSysVerInfo_gateway(tmepIp);
			}

			if(Mask != NULL)
			{
				if(sscanf(Mask, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
				{
					return 0;
				}
			
				if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
				{
					return 0;
				}
				
				sprintf(tmepIp, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
				
				SetSysVerInfo_mask(tmepIp);
			}
			
			API_DHCP_SaveEnable(0);
			SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
			ResetNetWork();
			Dhcp_Autoip_CheckIP();		//czn_20190604
			SysVerInfoUpdateIp(0);
		}
	}
}

void MENU_016_InstallIp_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	API_MenuIconDisplaySelectOn(ICON_029_IP_ADDR);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_029_IP_ADDR, 1, 0);
	DisplayInstallIpPageIcon(installIpPageSelect);
}

void MENU_016_InstallIp_Exit(void)
{

}

void MENU_016_InstallIp_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					installIpIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(installIpPageSelect*InstallIp_ICON_MAX+installIpIconSelect >= installIpSetNum)
					{
						return;
					}

					switch(installIpSet[installIpPageSelect*InstallIp_ICON_MAX+installIpIconSelect].iCon)
					{
						case ICON_094_IPAssigned:
							if(1)
							{
								#define MAX_SET_NUM 	2		//FOR_INDEXA
								int i, j;
								char tempChar[MAX_SET_NUM][31] = {"STATIC", "DHCP&AUTO"};
								API_GetOSD_StringWithID(MESG_TEXT_CusIpStatic, NULL, 0, NULL, 0,&publicSettingDisplay[0][1], &i);
								publicSettingDisplay[0][0]=i;
								for(i = 1; i < MAX_SET_NUM; i++)
								{
									for(publicSettingDisplay[i][0] = strlen(tempChar[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
									{
										publicSettingDisplay[i][j] = tempChar[i][j/2];
									}
								}
								
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_094_IPAssigned, MAX_SET_NUM, dhcpState, IPAssignedSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							}
							break;
						case ICON_095_IPAddress:
							if(dhcpState == 0)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_095_IPAddress, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_IP(), 1, SetIpAddress);
							}
							break;
						case ICON_096_IPSubnetMask:
							if(dhcpState == 0)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_096_IPSubnetMask, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_mask(), 1, SetSubNetMask);
							}
							break;
						case ICON_097_IPGateway:
							if(dhcpState == 0)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_097_IPGateway, installIpInput, 15, COLOR_WHITE, GetSysVerInfo_gateway(), 1, SetGateway);
							}
							break;
						case ICON_098_AddressHealthCheck:
							StartInitOneMenu(MENU_095_AddressHealthCheck,0,1);
							break;
					}
					break;

					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}



