#include "MENU_public.h"
#include "MENU_113_MonQuart.h"
#include "obj_GetIpByNumber.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_business_manage.h"		
#include "obj_IPCTableSetting.h"

OS_TIMER timer_Quad_monitor_stop;
int Quad_monitor_time;


void Quad_Monitor_Timer_Stop(void)
{
	OS_RetriggerTimer(&timer_Quad_monitor_stop);
	printf("Quad_monitor_time: = %d\n", Quad_monitor_time);	
	if(Quad_monitor_time%20 == 0)
	{
		AutoPowerOffReset();
	}
	//if(Quad_monitor_time%5 == 0)
	//{
	//	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_GetCpuMemUse);
	//}
	
	Quad_monitor_time --;
	if(Quad_monitor_time == 0)
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
		OS_StopTimer(&timer_Quad_monitor_stop);	
	}
}

void ICP_MonitorInit(void)
{
	OS_CreateTimer(&timer_Quad_monitor_stop, Quad_Monitor_Timer_Stop, 1000/25);
}

void Quad_Monitor_Timer_Init(void)
{
	uint16 monitorTime;
	char temp[20];

	//if(Quad_monitor_time == 0)
	{
		API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, temp);
		monitorTime = atoi(temp);
		
		Quad_monitor_time = (monitorTime < 10 ? 10 : (monitorTime > 600 ? 600 : monitorTime));
		OS_RetriggerTimer(&timer_Quad_monitor_stop);
		Power_Down_Timer_Stop();
	}
}


typedef struct
{
	int  monIndex;
	int  monState;		//open/close
	int  deviceType;	//ipc/ds
	int  vdType;		//264/265
	int  recState;
	char recFile[41];
	char deviceName[41];
}Mon_Quad_Menu_Para_t;

Mon_Quad_Menu_Para_t MonMenuWin[4];
static int winIdSaved, iconViewSel;
static int quadNum,quadIpcNum;
static int quadSelect[4]={-1,-1,-1,-1};

int GetQuadNum(void)
{
	quadIpcNum = GetIpcNum()+GetWlanIpcNum();
	
	if(Get_MonRes_Num()==0||GetIpcListUpdateFlag())
	{
		quadNum = Get_MonRes_Temp_Table_Num() + quadIpcNum;
	}
	else
	{
		quadNum = Get_MonRes_Num() + quadIpcNum;
	}
	if(quadNum > 4)
		quadNum = 4;
	return quadNum;
}

int GetQuadSelct(void)
{
	int num = 0;
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(quadSelect[i] != -1)
		{
			num++;
		}
	}
	return num;
}
void SetQuadSelct(int win_id, int index)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(quadSelect[i] == index)
		{
			return;
		}
	}
	quadSelect[win_id] = index;
}
void ClearQuadSelct(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		quadSelect[i] = -1;
	}
}

int GetWinMonState(int win_id)
{
	return MonMenuWin[win_id].monState;
}
void SetWinMonState(int win_id, int state)
{
	MonMenuWin[win_id].monState = state;
}
int GetWinRecState(int win_id)
{
	return MonMenuWin[win_id].recState;
}
void SetWinRecState(int win_id, int state)
{
	MonMenuWin[win_id].recState = state;
}
void GetWinRecFile(int win_id, char* name)
{
	strcpy(name, MonMenuWin[win_id].recFile);
}
void SetWinRecFile(int win_id, char* name)
{
	strcpy(MonMenuWin[win_id].recFile, name);
}
void GetWinDeviceName(int win_id, char* name)
{
	//strncpy(name, MonMenuWin[win_id].deviceName,20);
	snprintf(name,40,"%s",MonMenuWin[win_id].deviceName);
}
void SetWinDeviceName(int win_id, char* name)
{
	strcpy(MonMenuWin[win_id].deviceName, name);
}
int GetWinDeviceType(int win_id)
{
	return MonMenuWin[win_id].deviceType;
}
void SetWinDeviceType(int win_id, int type)
{
	MonMenuWin[win_id].deviceType = type;
}
int GetWinVdType(int win_id)
{
	return MonMenuWin[win_id].vdType;
}
void SetWinVdType(int win_id, int type)
{
	MonMenuWin[win_id].vdType = type;
}

int API_MonQuad_start( void )
{
	int i,select;
	char name_temp[41];
	char bdRmMs[11];
	//LoadSpriteDisplay(1);
	if(GetQuadSelct())
	{
		for( i = 0; i < 4; i++ )
		{
			select = quadSelect[i];
			printf("GetQuadSelct = %d\n", select);	
			if(select == -1)
				continue;
			if(select < quadIpcNum)
			{
				if(select < GetIpcNum())
					API_IpcIndex_Show(i, 0, select, 1, i+1);
				else
					API_IpcIndex_Show(i, 1, select-GetIpcNum(), 1, i+1);
				SetWinDeviceType(i, 1);
			}
			else
			{
				if(Get_MonRes_Num() == 0)
				{
					Get_MonRes_Temp_Table_Record(select - quadIpcNum,name_temp,bdRmMs, NULL);
				}
				else
				{
					Get_MonRes_Record(select - quadIpcNum,name_temp,bdRmMs, NULL);
				}
				if(Api_Ds_Show(i, i+1, bdRmMs, name_temp) == 0)
				{
					//SetWinMonState(i, 1);
					//SetWinDeviceType(i, 0);
				}
			}	
		}
	}
	else
	{
		for( i = 0; i < quadNum; i++ )
		{
			if(i < quadIpcNum)
			{
				if(i < GetIpcNum())
					API_IpcIndex_Show(i, 0, i, 1, i+1);
				else
					API_IpcIndex_Show(i, 1, i-GetIpcNum(), 1, i+1);
				SetWinDeviceType(i, 1);
				SetQuadSelct(i, i);
			}
			else
			{
				if(Get_MonRes_Num() == 0)
				{
					Get_MonRes_Temp_Table_Record(i - quadIpcNum,name_temp,bdRmMs, NULL);
				}
				else
				{
					Get_MonRes_Record(i - quadIpcNum,name_temp,bdRmMs, NULL);
				}
				if(Api_Ds_Show(i, i+1, bdRmMs, name_temp) == 0)
				{
					//SetWinMonState(i, 1);
					//SetWinDeviceType(i, 0);
					SetQuadSelct(i, i);
				}
			}
		}
	}
	//LoadSpriteDisplay(0);
}

void VdShowClear(void)
{

	int i;
#if 0	
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			if(GetWinDeviceType(i) == 1)
				clear_one_layer(i+4, bkgd_w/2, bkgd_h/2);
			else
				clear_one_layer(i, bkgd_w/2, bkgd_h/2);
		}
	}
#endif	
	for( i = 0; i < 4; i++ )
		vd_select_disp(1, i+1);
}

void VdShowHide(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			if(GetWinDeviceType(i) == 1)
				Clear_ipc_show_layer(i);
			else
				Clear_ds_show_layer(i);
		}
	}
}
void VdShowToFull(int win_id)
{
	//LoadSpriteDisplay(1);
	if(GetWinDeviceType(win_id) == 1)
	{
		if( get_pane_type() == 1 || get_pane_type() == 5 )
		{
			Set_ipc_show_pos(win_id, 0, 0, bkgd_w, (bkgd_h-64)/2);
		}
		else
		{
			Set_ipc_show_pos(win_id, 0, 0, bkgd_w, bkgd_h);
		}
	}
		
	else
	{
		if( get_pane_type() == 1 || get_pane_type() == 5 )
		{
			Set_ds_show_pos(win_id,0,0,bkgd_w,(bkgd_h-64)/2);
		}
		else
		{
			Set_ds_show_pos(win_id,0,0,bkgd_w,bkgd_h);
		}
	}	
		
	//LoadSpriteDisplay(0);
}
void VdShowToQuad(void)
{
	int i,x,y,w,h;
	//LoadSpriteDisplay(1);
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			Get_OneIpc_ShowPos(i+1,&x,&y,&w,&h);
			if(GetWinDeviceType(i) == 1)
				Set_ipc_show_pos(i, x, y, w, h);
			else
				Set_ds_show_pos(i, x, y, w, h);
		}
	}
	//LoadSpriteDisplay(0);
}

void EnterQuadMenu(int win_id, char* deviceName, int deviceType)
{
	printf("VdShowToQuad win= %d\n", win_id);	
	if(GetWinDeviceType(win_id) == 1)
		Clear_ipc_show_layer(win_id);
	else
		Clear_ds_show_layer(win_id);
	StartInitOneMenu(MENU_113_MonQuart,0,0);
	VdShowToQuad();
}

void ExitQuadMenu(void)
{
	int i;
	printf("ExitQuadMenu state=%d Type=%d \n", GetWinMonState(0), GetWinDeviceType(0));	
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			SetWinMonState(i, 0);
			if(GetWinDeviceType(i) == 1)
				API_OneIpc_Show_stop(i);
			else
				Api_Ds_Show_Stop(i);
		}
		if(GetWinRecState(i) == 1)
			api_record_stop(i);
	}
	Quad_monitor_time = 0;
	OS_StopTimer(&timer_Quad_monitor_stop);
	AutoPowerOffReset();
	
	//API_Business_Close(Business_State_MonitorIpc);

}

void vd_record_inform(int on, int win)
{
	struct {char onOff; char win;} data;
	data.onOff = on;
	data.win = win;
		
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_RECORD_NOTICE, (char*)&data, 2);
}

void vd_record_notice_disp(int on, int win)
{
	int x,y,w,h;

	Get_OneIpc_ShowPos(win,&x,&y,&w,&h);
	x += w - 32;
	y += 10;
	if(on)
	{
		API_SpriteDisplay_XY(x,y,SPRITE_RECORDING); 
	}
	else
	{
		API_SpriteClose(x,y,SPRITE_RECORDING);	
	}

}
	
void SetOneVdShow(int select)
{
	char name_temp[41];
	char bdRmMs[11];
	if(select < quadIpcNum)
	{
		if(select < GetIpcNum())
			API_IpcIndex_Show(winIdSaved, 0, select, 1, winIdSaved+1);
		else
			API_IpcIndex_Show(winIdSaved, 1, select-GetIpcNum(), 1, winIdSaved+1);
		SetWinDeviceType(winIdSaved, 1);
		SetQuadSelct(winIdSaved, select);
	}
	else
	{
		if(Get_MonRes_Num() == 0)
		{
			Get_MonRes_Temp_Table_Record(select - quadIpcNum,name_temp,bdRmMs, NULL);
		}
		else
		{
			Get_MonRes_Record(select - quadIpcNum,name_temp,bdRmMs, NULL);
		}
		if(Api_Ds_Show(winIdSaved, winIdSaved+1, bdRmMs, name_temp) == 0)
		{
			//SetWinMonState(winIdSaved, 1);
			//SetWinDeviceType(winIdSaved, 0);
			SetQuadSelct(winIdSaved, select);
		}
	}
}

void SelectOneVd(int win_id)
{
	winIdSaved = win_id;
	StartInitOneMenu(MENU_057_IPC_LIST,0,1);
}

void MENU_113_MonQuart_Init(int uMenuCnt)
{
	int i;
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		API_ProgBarDisplay(0, (bkgd_h-64)/2, bkgd_w, 1, COLOR_WHITE);
		API_ProgBarDisplay(bkgd_w/2, (bkgd_h-64)/2, 1, (bkgd_h-64)/2, COLOR_WHITE);
		API_ProgBarDisplay(0, (bkgd_h-64)*3/4, bkgd_w, 1, COLOR_WHITE);
		API_ProgBarDisplay(0, bkgd_h-64, bkgd_w, 1, COLOR_WHITE);
	}
	else
	{
		API_ProgBarDisplay(0, bkgd_h/2, bkgd_w, 1, COLOR_WHITE);
		API_ProgBarDisplay(bkgd_w/2, 0, 1, bkgd_h, COLOR_WHITE);
	}
	
	GetQuadNum();
	if(GetLastNMenu() != MENU_028_MONITOR2 && GetLastNMenu() != MENU_056_IPC_MONITOR && GetLastNMenu() != MENU_057_IPC_LIST)
	{
		API_MonQuad_start();
	}
	for( i = 0; i < 4; i++ )
	{
		//vd_select_disp(GetWinMonState(i), i+1);
		vd_record_notice_disp(GetWinRecState(i), i+1);
	}
	linphone_becall_cancel();
	API_Business_Request(Business_State_MonitorIpc);
	printf("Get_Business_State = %d\n", Get_Business_State());

	
	//API_BG_Display(10);
	#if 0
	API_OsdStringDisplayExt(10, 200, COLOR_RED, "monitor quad win 1", strlen("monitor quad win 1"),0,STR_UTF8,400);						
	API_OsdStringDisplayExt(650, 200, COLOR_RED, "monitor quad win 2", strlen("monitor quad win 1"),0,STR_UTF8,400);						
	API_OsdStringDisplayExt(10, 600, COLOR_RED, "monitor quad win 3", strlen("monitor quad win 1"),0,STR_UTF8,400);						
	API_OsdStringDisplayExt(650, 600, COLOR_RED, "monitor quad win 4", strlen("monitor quad win 1"),0,STR_UTF8,400);						
	sleep(2);
	API_OsdStringClearExt(10, 200, 400, 40);
	API_OsdStringClearExt(650, 200, 400, 40);
	API_OsdStringClearExt(10, 600, 400, 40);
	API_OsdStringClearExt(650, 600, 400, 40);
	#endif
	
}


void MENU_113_MonQuart_Exit(void)
{
	//IconListClear();
	API_Business_Close(Business_State_MonitorIpc);
	#if 0
	API_Stack_APT_Without_ACK(DS2_ADDRESS , MON_OFF);
	API_VideoTurnOff();
	#endif
}

void MonQuadProcess(int x, int y)
{
	int winId;
	winId = Get_WinId(x, y) - 1;
	printf("Get_WinId = %d\n", winId);
	if(winId == 4)
		return;
	if(GetWinMonState(winId) == 0)
	{
		SelectOneVd(winId);
	}
	else
	{
		if(GetWinDeviceType(winId) == 1)
			EnterIPCMonMenu(winId, NULL, 0);
		else
			EnterDSMonMenu(winId, 0);
	}
}

void MENU_113_MonQuart_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i;
	struct {char onOff; char win;} *data;
	char free[20]={0};
	char cpu[30]={0};
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
				case KEY_UNLOCK:
					ExitQuadMenu();
					popDisplayLastMenu();
				break;
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			printf("MENU_113_MonQuart GetCurIcon()=%d\n", GetCurIcon()); 		
			switch(GetCurIcon())
			{
				case ICON_212_MonitorReturn:
					ExitQuadMenu();
					popDisplayLastMenu();
					break;
				case ICON_207_MonitorPageDown:
					//VdShowClear();
					ClearQuadSelct();
					ExitQuadMenu();
					break;
				case ICON_208_MonitorCapture:
					//clearscreen(1);
					#if 1
					for( i = 0; i < 4; i++ )
					{
						if(GetWinMonState(i) == 1)
						{
							if(api_record_start(i)==0)
								SaveVdRecordProcess(i);
							//usleep(1000000);
						}
					}
					#endif
					break;
				default:
					
					MonQuadProcess(pglobal_win_msg->wparam, pglobal_win_msg->lparam);
				break;
			}
			
			
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_MonitorOff:
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_IPC_MonitorOff:
				//ExitQuadMenu();
				//popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_GetCpuMemUse:
				GetCpuMem_Polling(free, cpu);
				API_OsdStringDisplayExt(bkgd_w-100, 10, DISPLAY_STATE_COLOR, free, strlen(free), 0, STR_UTF8, 0);	
				API_OsdStringDisplayExt(bkgd_w-250, 50, DISPLAY_STATE_COLOR, cpu, strlen(cpu), 0, STR_UTF8, 0);	
				
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(data->onOff, data->win);
				#if 0
				if(!data->onOff)
					SaveVdRecordProcess(data->win - 1);
				#endif
			break;
			case MSG_7_BRD_SUB_IpcOpenRtspErr:
				LoadSpriteDisplay(0, *(char*)(arg+sizeof(SYS_WIN_MSG)) + 1);
			break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}

int api_record_stop( int index )		
{
	printf("+++++++++api_record_stop, index[%d]\n", index); 
	SetWinRecState(index, 0);
	API_RecordEnd_mux(index); 
	//SaveVdRecordProcess(index);
}

void vd_record_stop(int index)
{
	int state = GetWinRecState(index);
	printf("+++++++++vd_record_stop, state[%d]\n", state); 
	if( state )
	{
		state = 0;
		SetWinRecState(index, state);
		vd_record_inform(state, index+1);	
	}
}

int api_record_start( int index )		
{
	char temp[20];
	int recTime;
	int result = -1;
	char recfile[100];
	time_t t;
	struct tm *tblock; 	
	
	API_Event_IoServer_InnerRead_All(AutoRecordEnable, temp);

	if(!atoi(temp))
	{
		return -1;
	}
	
	if(GetWinRecState(index) == 1)
	{
		API_RecordEnd_mux(index); 
		return -2;
	}
	
	if(Judge_SdCardLink() == 1) 
	{
		recTime = atoi(temp);
	}
	else
	{
		recTime = atoi(temp) < 3? atoi(temp) : 3;
	}
	if(CheckRecFreeSpace()!=0)
		return -1;
	printf("+++++++++api_record_start, index[%d] recTime[%d]\n", index, recTime); 
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"%02d%02d%02d_%02d%02d%02d_CH%d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec,index+1 );
	SetWinRecFile(index, recfile);
	
	result = API_RecordStart_mux(index, GetWinVdType(index), 15, recfile, recTime, 0, vd_record_stop);
	if( result == 0 )
	{
		SetWinRecState(index, 1);	//开始录像
		vd_record_inform(1, index+1);	
	}
	return 0;
}

void SaveVdRecordProcess(int index)
{
	char name_temp[41];
	CALL_RECORD_DAT_T record_temp;
	
	record_temp.type		= VIDEO_RECORD; 
	record_temp.subType 	= LOCAL;
	record_temp.property	= NORMAL;
	record_temp.target_node = 0;		
	record_temp.target_id	= 0;

	strcpy( record_temp.input, "---");
	GetWinDeviceName(index, name_temp);
	snprintf(record_temp.name,20,"%s",name_temp);
	GetWinRecFile(index, record_temp.relation);
	api_register_one_call_record( &record_temp );
	
}

int CheckRecFreeSpace(void)
{
	int freesize, totalsize;
	int i;
	
	if(Judge_SdCardLink() == 1) 
	{
		API_GetSDCardSize(&freesize, &totalsize);
		
		if( freesize >  100)
		{
			return 0;
		}
		else
		{
			call_record_delete_video(5);
		}
	}
	else
	{	
		i=0;
		while(GetVideoNum() >= 10&&i++<10)
		{
			call_record_delete_video(1);
		}
		if(i>=10)
		{
			char cmd[100];
			snprintf(cmd,100,"rm -r %s",JPEG_STORE_DIR);
			system(cmd);
			return -1;
		}
	}
	return 0;
}

void LoadSpriteDisplay(int mode, int win )
{
	uint16 xsize, ysize;
	int x,y,w,h;
	Get_SpriteSize(199, &xsize, &ysize);
	Get_OneIpc_ShowPos(win,&x,&y,&w,&h);
	x += (w - xsize)/2;
	y += (h - ysize)/2;
	if(mode == 1)
	{
		API_SpriteDisplay_XY(x, y, 199);
	}
	else if (mode == 2)
	{
		API_SpriteCloseNoBG(x, y, 199);
	}
	else
	{
		API_SpriteClose(x, y, 199);
	}
}

void vd_select_disp(int on, int win)
{
	int x,y,w,h;
	uint16 xsize, ysize;
	Get_SpriteSize(207, &xsize, &ysize);
	Get_OneIpc_ShowPos(win,&x,&y,&w,&h);
	x += (w - xsize)/2;
	y += (h - ysize)/2;
	if(on)
	{
		API_SpriteDisplay_XY(x,y,207); 	
	}
	else
	{
		API_SpriteClose(x,y,207);
	}
	
}
