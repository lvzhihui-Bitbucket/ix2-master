
#include "MENU_public.h"
#include "task_WiFiConnect.h"

#if 0
#define	WLAN_SETTING_ICON_MAX	5
#define	SSID_LEN_MAX			50

int wlanSettingIconSelect;
int wlanSettingPage;
int wifiSelectNum;
static char  wifiSaveSsid[ESSID_LEN];


void DisplayWifiQuality(int x, int y, uint8 quality)
{
	if(quality > 80)
	{
		API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY4);
	}
	else if(quality > 50)
	{
		API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY3);
	}
	else if(quality > 30)
	{
		API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY2);
	}
	else if(quality > 0)
	{
		API_SpriteDisplay_XY( x, y, SPRITE_WIFI_QUALITY1);
	}
	else
	{
		API_SpriteClose( x, y, SPRITE_WIFI_QUALITY4);
	}
}

static void DisplayWiFiList(uint8 page)
{
	uint8 i, j;
	uint16 x, y, val_x;
	uint8 pageNum;
	WIFI_RUN wifiRun = GetWiFiState();
	POS pos;
	SIZE hv;
	int listNum;
	
	API_DisableOsdUpdate();

	listNum = (wifiRun.wifiSwitch) ? (wifiRun.wifiData.DataCnt+2) : 1;

	for(i = 0; i < WLAN_SETTING_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2;
		val_x = x+(hv.h - pos.x)/2;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		API_SpriteClose( x+450, y-5, SPRITE_WIFI_QUALITY4);
		
		ListSelect(i, 0);
		
		if(page*WLAN_SETTING_ICON_MAX+i < listNum)
		{
		
			if(i == 0 && page == 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_LanSetting, 1, 0);
				API_OsdUnicodeStringDisplay(val_x, y, DISPLAY_STATE_COLOR, wifiRun.wifiSwitch ? MESG_TEXT_WIFI_ON : MESG_TEXT_WIFI_OFF, 1, 0);
			}
			else if(i == 1 && page == 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_WIFI_Input, 1, 0);
			}
			else
			{
				char* pwifistr;
				int len;
				int color;
				
				pwifistr = wifiRun.wifiData.WifiInfo[i+page*WLAN_SETTING_ICON_MAX-2].ESSID;
				
				if((page== 0) && (i == 2) && (wifiRun.wifiConnect == 2) && (!strcmp(wifiRun.curWifi.ESSID, pwifistr)))
				{
					ListSelect(i, 1);
				}

				for(color = DISPLAY_LIST_COLOR, j = 0; j < 3; j++)
				{
					if(!strcmp(pwifistr, wifiRun.saveSsid[j]))
					{
						color = DISPLAY_STATE_COLOR;
						break;
					}
				}
			
				len = (strlen(pwifistr) > SSID_LEN_MAX) ? SSID_LEN_MAX : strlen(pwifistr);
				
				API_OsdStringDisplayExt(x, y, color, pwifistr, len,1,STR_UTF8, 0);
				DisplayWifiQuality(x+450, y-5, wifiRun.wifiData.WifiInfo[i+page*WLAN_SETTING_ICON_MAX-1].LEVEL);
			}
		}
	}
	pageNum = (listNum)/WLAN_SETTING_ICON_MAX + ((listNum)%WLAN_SETTING_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	API_EnableOsdUpdate();
}

int WiFiInputPwdConnect(char * code)
{
	WIFI_RUN wifiRun = GetWiFiState();
	
	strcpy(wifiSaveSsid, wifiRun.wifiData.WifiInfo[wifiSelectNum].ESSID);
	
	API_WifiAddSSID(wifiSaveSsid, code);
	API_WifiCNCT(wifiSaveSsid, code);
	return 1;
}

static void ConnectWiFi(int index)
{
	int j;
	WIFI_RUN wifiRun = GetWiFiState();
	char *pwifistr;

	if(index >= wifiRun.wifiData.DataCnt)
	{
		return;
	}
	
	pwifistr = wifiRun.wifiData.WifiInfo[index].ESSID;

	//已经连接
	if(!strcmp(pwifistr, wifiRun.curWifi.ESSID))
	{
		return;
	}
	//未连接
	else
	{
		for(j = 0; j < 3; j++)
		{
			if(!strcmp(pwifistr, wifiRun.saveSsid[j]))
			{
				break;
			}
		}

		//未连接, 存在保存的列表当中
		if(j < 3)
		{
			API_WifiAddSSID(wifiRun.saveSsid[j], wifiRun.savePwd[j]);
			API_WifiCNCT(wifiRun.saveSsid[j], wifiRun.savePwd[j]);
		}
		//未连接, 需要输入密码
		else
		{
			EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, NULL, 20, COLOR_WHITE, NULL, 1, WiFiInputPwdConnect);
		}
	}
}

void WiFiConnectFailedProcess(const char* data)
{
	char wifiPwd[WIFI_PWD_LENGTH];
	
	ParseWifiJsonData(data, wifiSaveSsid, wifiPwd);
	
	EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_WifiPWD, NULL, 20, COLOR_WHITE, NULL, 1, WiFiInputPwdConnect);
}


void MENU_114_WlanSetting_Init(int uMenuCnt)
{
	WIFI_RUN wifiRun = GetWiFiState();
	int x, y, i, j;
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_WirelessSetting,1, 0);
	DisplayWiFiList(wlanSettingPage);

	if(GetLastNMenu() == MENU_011_SETTING || GetLastNMenu() == MENU_001_MAIN)
	{
		wlanSettingIconSelect = 0;
		wlanSettingPage = 0;
		
		if(wifiRun.wifiSwitch)
		{
			API_WifiSearch();
		}
	}
}

void MENU_114_WlanSetting_Exit(void)
{
	
}

void MENU_114_WlanSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	WIFI_RUN wifiRun = GetWiFiState();
	POS pos;
	SIZE hv;
	int x, y;
	char display[100];
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					
					//搜索完成并且不在连接状态才能进去
					wlanSettingIconSelect = GetCurIcon() - ICON_007_PublicList1;
					wifiSelectNum = wlanSettingPage*WLAN_SETTING_ICON_MAX+wlanSettingIconSelect;

					if(wifiSelectNum == 0)
					{
						if(wifiRun.wifiSwitch)
						{
							API_WifiClose();
						}
						else
						{
							API_WifiOpen();
							API_WifiSearch();
						}
					}
					else if(wifiSelectNum == 1)
					{
						if(wifiRun.wifiSwitch)
						{
							//StartInitOneMenu(MENU_016_WIFI_INFO, 0, 1);
						}
					}
					else if(wifiSelectNum < wifiRun.wifiData.DataCnt+2)
					{
						if(wifiRun.wifiSwitch)
						{
							wifiSelectNum -= 2;
							ConnectWiFi(wifiSelectNum);
						}
					}

					
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&wlanSettingPage, WLAN_SETTING_ICON_MAX, wifiRun.wifiData.DataCnt+2, (DispListPage)DisplayWiFiList);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&wlanSettingPage, WLAN_SETTING_ICON_MAX, wifiRun.wifiData.DataCnt+2, (DispListPage)DisplayWiFiList);
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2;
		API_OsdStringClearExt(x, y,300,CLEAR_STATE_H);

		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_CLOSE:
			case MSG_7_BRD_SUB_WIFI_OPEN:
				DisplayWiFiList(wlanSettingPage);
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCHING:
				BusySpriteDisplay(1);
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Searching,1, 0);
				DisplayWiFiList(wlanSettingPage);
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCH_OVER:
				BusySpriteDisplay(0);
				DisplayWiFiList(wlanSettingPage);
				break;
			case MSG_7_BRD_SUB_WIFI_CONNECTING:
				BusySpriteDisplay(1);
				snprintf(display, 100, "%s ", wifiRun.connectingSsid);
				API_OsdStringClearExt(x, y,300,CLEAR_STATE_H);
				API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Connecting,1, display, NULL, 0);
				DisplayWiFiList(wlanSettingPage);
				break;
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
				BusySpriteDisplay(0);
				break;
				
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
				BusySpriteDisplay(0);
				ReorderWiFiList();
				snprintf(display, 100, "%s ", wifiRun.curWifi.ESSID);
				API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_WIFI_Connected,1, display, NULL, 0);
				wlanSettingPage = 0;
				DisplayWiFiList(wlanSettingPage);
				break;
			case MSG_7_BRD_SUB_WIFI_CONNECT_FAILED:
				WiFiConnectFailedProcess(GetMenuInformData(arg));
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}
#endif
