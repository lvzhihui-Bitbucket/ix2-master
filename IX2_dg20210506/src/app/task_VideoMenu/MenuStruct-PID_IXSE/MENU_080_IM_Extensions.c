#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_MS_DeviceTable.h"
#include "obj_GetIpByNumber.h"

#define	IM_ExtensionLIST_ICON_MAX		5
#define ONE_LIST_MAX_LEN				50

int IM_ExtensionListIconSelect;
int IM_ExtensionListPageSelect;
int IM_ExtensionListMaxNum = 0;
int IM_ExtensionListIndex;

MS_ListRecord_T msListRecord;


void DisplayOneIM_Extension(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[ONE_LIST_MAX_LEN] = {0};
	char oneRecord[50];
	MS_ListRecord_T record;
	
	IM_ExtensionListMaxNum = GetMSListTempRecordCnt();

	if(index < IM_ExtensionListMaxNum)
	{	
		if(GetMsListTempRecordItems(index,	&record) != 0)
			return;

		record.BD_RM_MS[10] = 0;
		get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, !strcmp(record.R_Name, "-") ? record.name1 : record.R_Name, recordBuffer);
		API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
	}
}

void DisplayOnePageIM_Extensionlist(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;
	POS pos;
	SIZE hv;

	IM_ExtensionListMaxNum = GetMSListTempRecordCnt();

	list_start = page*IM_ExtensionLIST_ICON_MAX;
	
	for( i = 0; i < IM_ExtensionLIST_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		if(list_start+i < IM_ExtensionListMaxNum)
		{
			DisplayOneIM_Extension(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, val_x - x);
		}
	}
	
	maxPage = IM_ExtensionListMaxNum/IM_ExtensionLIST_ICON_MAX + (IM_ExtensionListMaxNum%IM_ExtensionLIST_ICON_MAX ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

void MENU_080_IM_Extension_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	IM_ExtensionListIconSelect = 0;
	IM_ExtensionListPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_051_ExternalUnit);

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_IM_Extensions, 1, 0);
	
	DisplayOnePageIM_Extensionlist(IM_ExtensionListPageSelect);
}

void MENU_080_IM_Extension_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_051_ExternalUnit);
}

void MENU_080_IM_Extension_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					IM_ExtensionListMaxNum = GetMSListTempRecordCnt();
					
					IM_ExtensionListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					IM_ExtensionListIndex = IM_ExtensionListPageSelect*IM_ExtensionLIST_ICON_MAX + IM_ExtensionListIconSelect;
					if(IM_ExtensionListIndex < GetMSListTempRecordCnt())
					{
						GetIpRspData data;
						MS_ListRecord_T record;
						
						if(GetMsListTempRecordItems(IM_ExtensionListIndex,	&record) != 0)
						{
							return;
						}
						
						if(API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 1, 1, &data) != 0)
						{
							return;
						}

						get_device_addr_and_name_disp_str(0, data.BD_RM_MS[0], NULL, NULL, record.name1, temp);
						
						SetOnlineManageTitle(temp);
						SetOnlineManageMFG_SN(record.MFG_SN);
						SetSearchOnlineIp(data.Ip[0]);
						SetOnlineManageNbr(data.BD_RM_MS[0]);
						EnterOnlineManageMenu(MENU_034_ONLINE_MANAGE_INFO, 1);	
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&IM_ExtensionListPageSelect, IM_ExtensionLIST_ICON_MAX, IM_ExtensionListMaxNum, (DispListPage)DisplayOnePageIM_Extensionlist);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&IM_ExtensionListPageSelect, IM_ExtensionLIST_ICON_MAX, IM_ExtensionListMaxNum, (DispListPage)DisplayOnePageIM_Extensionlist);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


