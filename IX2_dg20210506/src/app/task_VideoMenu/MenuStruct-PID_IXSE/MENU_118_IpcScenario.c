#include "MENU_public.h"
#include "obj_IPCTableSetting.h"

int ScenarioIconSelect;
int ScenarioPageSelect;
int ScenarioIconMax;
extern IPC_ONE_DEVICE ipcRecord;

const IconAndText_t ScenarioIconTable[] = 
{
	{ICON_IpcScenario1,					MESG_TEXT_IpcScenario1},
	{ICON_IpcScenario2,					MESG_TEXT_IpcScenario2},
	{ICON_IpcScenario3,					MESG_TEXT_IpcScenario3},
	{ICON_IpcScenario4,					MESG_TEXT_IpcScenario4},
	//{ICON_IpcScenarioDefault,			MESG_TEXT_Restore},
};
const int ScenarioIconNum = sizeof(ScenarioIconTable)/sizeof(ScenarioIconTable[0]);

void DisplayOnePageScenario(uint8 page)
{
	//API_EnableOsdUpdate();

	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
	char display[10];
	
	for( i = 0; i < ScenarioIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*ScenarioIconMax+i < ScenarioIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, ScenarioIconTable[i+page*ScenarioIconMax].iConText, 1, val_x-x);
			switch(ScenarioIconTable[i+page*ScenarioIconMax].iCon)
			{
				case ICON_IpcScenario1:
					sprintf(display, "%d", ipcRecord.CH_REC);
					API_OsdStringDisplayExt(val_x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
					break;
				case ICON_IpcScenario2:
					sprintf(display, "%d", ipcRecord.CH_FULL);
					API_OsdStringDisplayExt(val_x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
					break;
				case ICON_IpcScenario3:
					sprintf(display, "%d", ipcRecord.CH_QUAD);
					API_OsdStringDisplayExt(val_x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
					break;
				case ICON_IpcScenario4:
					sprintf(display, "%d", ipcRecord.CH_APP);
					API_OsdStringDisplayExt(val_x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
					break;
			}
		}
		
	}
	
	pageNum = ScenarioIconNum/ScenarioIconMax + (ScenarioIconNum%ScenarioIconMax ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void MENU_118_IpcScenario_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	ScenarioIconSelect = 0;
	ScenarioPageSelect = 0;
	ScenarioIconMax = GetListIconNum();
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ChannelApply, 2, 0);
	DisplayOnePageScenario(ScenarioPageSelect);
}




void MENU_118_IpcScenario_Exit(void)
{
	
}

void ipcREC_ChannelSave(int value)
{
	ipcRecord.CH_REC = value;
}
void ipcFULL_ChannelSave(int value)
{
	ipcRecord.CH_FULL = value;
}
void ipcQuad_ChannelSave(int value)
{
	ipcRecord.CH_QUAD = value+1;
}
void ipcApp_ChannelSave(int value)
{
	ipcRecord.CH_APP = value+1;
}

void MENU_118_IpcScenario_Process(void* arg)
{
	int i, j;
	char tempChar[10];
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					ScenarioIconSelect = ScenarioPageSelect*ScenarioIconMax + GetCurIcon() - ICON_007_PublicList1;
					
					if(ScenarioIconSelect >= ScenarioIconNum)
					{
						return;
					}
					switch(ScenarioIconTable[ScenarioIconSelect].iCon)
					{
						case ICON_IpcScenario1:
							for(i = 0; i < ipcRecord.CH_ALL; i++)
							{
								snprintf(tempChar, 10, "%d", i);
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_IPC_Channel, ipcRecord.CH_ALL, ipcRecord.CH_REC, ipcREC_ChannelSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
						break;
						case ICON_IpcScenario2:
							for(i = 0; i < ipcRecord.CH_ALL; i++)
							{
								snprintf(tempChar, 10, "%d", i);
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_IPC_Channel, ipcRecord.CH_ALL, ipcRecord.CH_FULL, ipcFULL_ChannelSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
						break;
						case ICON_IpcScenario3:
							for(i = 0; i < ipcRecord.CH_ALL-1; i++)
							{
								snprintf(tempChar, 10, "%d", i+1);
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_IPC_Channel, ipcRecord.CH_ALL-1, ipcRecord.CH_QUAD-1, ipcQuad_ChannelSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
						break;
						case ICON_IpcScenario4:
							for(i = 0; i < ipcRecord.CH_ALL-1; i++)
							{
								snprintf(tempChar, 10, "%d", i+1);
								for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
								{
									publicSettingDisplay[i][j] = tempChar[j/2];
								}
							}
							EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_IPC_Channel, ipcRecord.CH_ALL-1, ipcRecord.CH_APP-1, ipcApp_ChannelSave);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
						break;
					}

					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&ScenarioPageSelect, ScenarioIconMax, ScenarioIconNum, (DispListPage)DisplayOnePageScenario);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ScenarioPageSelect, ScenarioIconMax, ScenarioIconNum, (DispListPage)DisplayOnePageScenario);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


