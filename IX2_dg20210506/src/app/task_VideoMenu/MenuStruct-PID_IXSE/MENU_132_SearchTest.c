#include "MENU_132_SearchTest.h"
#include "obj_SearchIpByFilter.h"
#include "elog_forcall.h"

static int searchTestPageSelect;
static SearchResult result;

static int GetSearchResultNameAndValue(int index, char* name, char* value)
{
	if(name == NULL || value == NULL)
	{
		return 0;
	}
	
	switch(index)
	{
		case 0:
			strcpy(name, "Search result");
			snprintf(value, 100, "%s", result.searchResult == 0 ? "Success" : "Error");
			break;
		case 1:
			strcpy(name, "Search net mode");
			snprintf(value, 100, "%s", result.searchNetMode == 0 ? "Lan" : "Wlan");
			break;
		case 2:
			strcpy(name, "Search1 timeout");
			snprintf(value, 100, "%d ms", result.search1Timeout);
			break;
		case 3:
			strcpy(name, "Search1 interval time");
			snprintf(value, 100, "%d ms", result.search1IntervalTime);
			break;
		case 4:
			strcpy(name, "Search2 enable");
			snprintf(value, 100, "%s", result.search2Enable ? "Enable" : "Disable");
			break;
		case 5:
			strcpy(name, "Search2 timeout");
			snprintf(value, 100, "%d ms", result.search2Timeout);
			break;
		case 6:
			strcpy(name, "Search2 interval time");
			snprintf(value, 100, "%d ms", result.search2IntervalTime);
			break;
		case 7:
			strcpy(name, "Search all time");
			snprintf(value, 100, "%d ms", result.searchAllTime);
			break;
		case 8:
			strcpy(name, "Search1 time");
			snprintf(value, 100, "%d ms", result.search1Time);
			break;
		case 9:
			strcpy(name, "Search2 time");
			snprintf(value, 100, "%d ms", result.search2Time);
			break;
		case 10:
			strcpy(name, "Search1 device count");
			snprintf(value, 100, "%d", result.search1DevCnt);
			break;
		case 11:
			strcpy(name, "Search2 device count");
			snprintf(value, 100, "%d", result.search2DevCnt);
			break;
		case 12:
			strcpy(name, "Search1 first device time");
			snprintf(value, 100, "%d ms", result.search1FirstDevTime);
			break;
		case 13:
			strcpy(name, "Search2 first device time");
			snprintf(value, 100, "%d ms", result.search2FirstDevTime);
			break;
		case 14:
			strcpy(name, "Search2 send command count");
			snprintf(value, 100, "%d", result.search2SendCnt);
			break;
		default:
			return 0;
	}
	
	return 1;
}
static void MenuListGetSearchTestPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	char value[100];
	char name[100];
	
	int itemCnt = sizeof(SearchResult)/sizeof(int);

	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < itemCnt)
		{
			GetSearchResultNameAndValue(index, name, value);

			pDisp->disp[pDisp->dispCnt].strLen = 2*api_ascii_to_unicode(name, strlen(name), pDisp->disp[pDisp->dispCnt].str);		
			pDisp->disp[pDisp->dispCnt].valLen = 2*api_ascii_to_unicode(value, strlen(value), pDisp->disp[pDisp->dispCnt].val);		
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;

			pDisp->dispCnt++;
		}
	}
}
 

void MENU_132_SearchTest_Init(int uMenuCnt)
{
	char elogTemp[600];
	int eloglen, index;
	char value[100];
	char name[100];
	
	#define LOG_TAG 		 "Search"

	SearchIpRspData searchData;
	extern char searchAndProgBdNbr[10];
	extern Type_e searchAndProgType;
	extern int searchNum;

	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID(MESG_TEXT_ICON_SearchTest, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 

	listInit = ListPropertyDefault();
	listInit.listType = TEXT_10X1;
	listInit.fun = MenuListGetSearchTestPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.listIconEn = 0;
	listInit.valEn = 1;
	
	if(get_pane_type() == 5 )
	{
		listInit.textValOffset = 150;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.textValOffset = 450;
	}	
	if(GetLastNMenu() == MENU_015_INSTALL_SUB)
	{
		searchTestPageSelect = 0;		
		InitMenuList(listInit);

		BusySpriteDisplay(1);
		API_SearchIpByFilterTest(searchAndProgBdNbr, searchAndProgType, SearchDeviceRecommendedWaitingTime, searchNum, &searchData, &result);
		BusySpriteDisplay(0);
		
		for(eloglen = 0, index = 0; index < 15 && eloglen < 600; index++, eloglen = strlen(elogTemp))
		{
			GetSearchResultNameAndValue(index, name, value);
			if(index == 14)
			{
				snprintf(elogTemp + eloglen, 600-eloglen, "%s:%s", name, value);
			}
			else
			{
				snprintf(elogTemp + eloglen, 600-eloglen, "%s:%s, ", name, value);
			}
		}
		log_i(elogTemp);

		listInit.listCnt = sizeof(SearchResult)/sizeof(int);
		listInit.currentPage = searchTestPageSelect;
		InitMenuList(listInit);
	}
	else
	{
		listInit.listCnt = sizeof(SearchResult)/sizeof(int);
		listInit.currentPage = searchTestPageSelect;
		InitMenuList(listInit);
	}
		
}

void MENU_132_SearchTest_Exit(void)
{
	MenuListDisable(0);
}

void MENU_132_SearchTest_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon < MENU_LIST_ICON_NONE)
					{
						searchTestPageSelect = MenuListGetCurrentPage();
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}

