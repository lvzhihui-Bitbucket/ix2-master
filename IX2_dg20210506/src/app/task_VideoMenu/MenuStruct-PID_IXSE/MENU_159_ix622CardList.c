#include "MENU_public.h"
#include "cJSON.h"

int UserCardListIconMax;
int UserCardListIconSelect;
int UserCardListPageSelect;
int UserCardListIndex;
cJSON *UserCardList=NULL;
int UserCardListCnt;
int UserCardDelConfirm;
int UserCardDelConfirmSelect;

void DisplayOnePageCardList(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start, list_max;
	POS pos;
	SIZE hv;
	cJSON *item = NULL;
    char card[50] = {0};
    char time[50] = {0};
	char display[100];

	for( i = 0; i < UserCardListIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		list_start = UserCardListCnt - page*UserCardListIconMax - 1;
		if( (list_start-i) >= 0 )
		{
			item=cJSON_GetArrayItem(UserCardList,list_start-i);
			GetJsonDataPro(item, "DATE", time);    
			GetJsonDataPro(item, "CARD_NUM", card); 
			snprintf(display, 100, "[%s] %s", time, card);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-pos.x);
		}
	}
	
	
	maxPage = UserCardListCnt/UserCardListIconMax + (UserCardListCnt%UserCardListIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void UserCardListShow(void)
{
	UserCardListIconSelect = 0;
	UserCardListPageSelect = 0;
	UserCardDelConfirm = 0;
	UserCardDelConfirmSelect = 0;
	UserCardListCnt = 0;
	if(UserCardList)
	{
		cJSON_Delete(UserCardList);
		UserCardList = NULL;
	}
	cJSON* view = cJSON_CreateArray();
	cJSON* Where=cJSON_CreateObject();
	cJSON_AddStringToObject(Where,"PROPERTY","-");
	if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", view, Where, 0) > 0)
    {        
        UserCardList = cJSON_Duplicate(view,1);
		UserCardListCnt = cJSON_GetArraySize(UserCardList);
		
    }
    cJSON_Delete(view);
	cJSON_Delete(Where);
	printf("DisplayOnePageUserCardList cnt=%d\n",UserCardListCnt);
	DisplayOnePageCardList(UserCardListPageSelect);
}
void MENU_159_UserCardList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	UserCardListIconMax = GetListIconNum();
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_UserCardList,2, 0);
	UserCardListShow();
}


void MENU_159_UserCardList_Exit(void)
{
	if(UserCardList)
	{
		cJSON_Delete(UserCardList);
		UserCardList = NULL;
	}
}

void MENU_159_UserCardList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					UserCardListIconSelect = GetCurIcon() - ICON_007_PublicList1;
					UserCardListIndex = UserCardListPageSelect*UserCardListIconMax + UserCardListIconSelect;
					if(UserCardListIndex >= UserCardListCnt)
						return;
					if(ConfirmSelect(&UserCardDelConfirm, &UserCardDelConfirmSelect, &UserCardListIconSelect, UserCardListPageSelect, UserCardListIconMax))
					{
						return;
					}
					API_RemoteTableDelete(GetSearchOnlineIp(), "CardTable", cJSON_GetArrayItem(UserCardList,UserCardListCnt-UserCardListIndex-1));
					UserCardListShow();
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&UserCardListPageSelect, UserCardListIconMax, UserCardListCnt, (DispListPage)DisplayOnePageCardList);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&UserCardListPageSelect, UserCardListIconMax, UserCardListCnt, (DispListPage)DisplayOnePageCardList);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


