#include "MENU_public.h"

#define	ParaPublic_ICON_MAX			5

int ParaPublicIconSelect;
int ParaPublicPage;


extern ComboBoxSelect_T OneParaComboBox;

void EnterParaPublicMenu(int title, int settingValue, int (*UpdateValue)(int))
{
	ParaPublicPage = (settingValue+1)/ParaPublic_ICON_MAX - ((settingValue+1)%ParaPublic_ICON_MAX ? 0 : 1);
	OneParaComboBox.title = title;
	OneParaComboBox.saveValue = settingValue;
	OneParaComboBox.value = settingValue;
	OneParaComboBox.ParaComboBoxUpdateValueFunction = UpdateValue;
	
	StartInitOneMenu(MENU_073_ParaPublicComboBox, 0, 1);
}

static void DisplayOnePageParaPublicSetting(int page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	char* disp;
	int index;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();

	for(i = 0; i < ParaPublic_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		ListSelect(i, 0);
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		index = page*ParaPublic_ICON_MAX+i;
		if(index < OneParaComboBox.num)
		{
			disp = OneParaComboBox.data[index].comboBox;
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, disp, strlen(disp),1, STR_UTF8, hv.h - x);
			if(index == OneParaComboBox.value)
			{
				ListSelect(i, 1);
			}
		}
	}
	
	//API_EnableOsdUpdate();

	pageNum = OneParaComboBox.num/ParaPublic_ICON_MAX + (OneParaComboBox.num%ParaPublic_ICON_MAX ? 1 : 0);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
}

void MENU_073_ParaPublic_Init(int uMenuCnt)
{
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	
	//API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, OneParaComboBox.title,1, DISPLAY_TITLE_WIDTH);
	DisplayOnePageParaPublicSetting(ParaPublicPage);
}

void MENU_073_ParaPublic_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_049_Parameter);
}

void MENU_073_ParaPublic_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 setValue;
	int i, x, y;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&ParaPublicPage, ParaPublic_ICON_MAX, OneParaComboBox.num, (DispListPage)DisplayOnePageParaPublicSetting);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ParaPublicPage, ParaPublic_ICON_MAX, OneParaComboBox.num, (DispListPage)DisplayOnePageParaPublicSetting);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					ParaPublicIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(OneParaComboBox.value >= OneParaComboBox.num)
					{
						return;
					}

					OneParaComboBox.value = ParaPublicPage*ParaPublic_ICON_MAX+ParaPublicIconSelect;
					if(OneParaComboBox.value != OneParaComboBox.saveValue)
					{
						ParaPublicSelectFlagDisplay();
						BusySpriteDisplay(1);
						usleep(200000);
						OneParaComboBox.ParaComboBoxUpdateValueFunction(OneParaComboBox.value);
						BusySpriteDisplay(0);
					}
					
					popDisplayLastMenu();
					break;
			}
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

void ParaPublicSelectFlagDisplay(void)
{
	int x, y, i;
	
	for(i = 0; i < ParaPublic_ICON_MAX; i++)
	{
		ListSelect(i, 0);		
		if(ParaPublicPage*ParaPublic_ICON_MAX+i < OneParaComboBox.num)
		{
			if(ParaPublicPage*ParaPublic_ICON_MAX+i == OneParaComboBox.value)
			{
				ListSelect(i, 1);
			}
		}
	}

}

