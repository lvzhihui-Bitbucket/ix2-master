#include "MENU_public.h"
#include "MENU_058_IPC_Manage.h"
#include "onvif_discovery.h"
#include "obj_IPCTableSetting.h"
#include "MENU_036_ListEdit.h"


//#define	IPC_ManageIconMax	5
#define MAX_STR 			45

int IPC_ManageIconMax;
int IPC_ManageIconSelect;
int IPC_ManagePageSelect;
int IPC_ManageSetting;


onvif_login_info_t Login;
extern IPC_ONE_DEVICE ipcRecord;

const IPC_ManageIcon IPC_ManageIconTable[] = 
{
	{ICON_260_IPC_Name,					MESG_TEXT_IPC_Name},    
	{ICON_IpcScenario, 					MESG_TEXT_ChannelApply},
	{ICON_264_IPC_Save, 				MESG_TEXT_IPC_Save},
};

const int IPC_ManageIconNum = sizeof(IPC_ManageIconTable)/sizeof(IPC_ManageIconTable[0]);

static void DisplayIPC_ManagePageIcon(uint8 page)
{
	uint8 i,ch;
	uint16 x, y, val_x, val_y;
	int pageNum;
	char display[100];
	int color;
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	
	for(i = 0; i < IPC_ManageIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+120;
			val_y = y+40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			val_y = y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*IPC_ManageIconMax+i < IPC_ManageIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, IPC_ManageIconTable[i+page*IPC_ManageIconMax].iConText, 1, hv.h-x);
			switch(IPC_ManageIconTable[i+page*IPC_ManageIconMax].iCon)
			{
				case ICON_260_IPC_Name:
					snprintf(display, 100, "%s", ipcRecord.NAME);
					API_OsdStringDisplayExt(val_x, val_y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
					break;
			}
		}
		else
		{
			ch = i-3;
			if(ch < ipcRecord.CH_ALL)
			{
				snprintf(display, 100, "Channel%d", ch);
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - x);
				snprintf(display, 100, "%d*%d-%s", ipcRecord.CH_DAT[ch].width, ipcRecord.CH_DAT[ch].height, (ipcRecord.CH_DAT[ch].vd_type == 1? "H265" : "H264"));
				API_OsdStringDisplayExt(val_x, val_y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h - val_x);
			}
			#if 0
			if(Login.dat[ipcRecord.channel].width > 1920 || Login.dat[ipcRecord.channel].height > 1080)
			{
				OSD_GetIconInfo(ICON_009_PublicList3, &pos, &hv);
				Get_SpriteSize(SPRITE_NotSupport, &xsize, &ysize);
				API_SpriteDisplay_XY(hv.h - xsize, hv.v - ysize, SPRITE_NotSupport);
			}
			#endif
		}

		
	}
	pageNum = IPC_ManageIconNum/IPC_ManageIconMax + (IPC_ManageIconNum%IPC_ManageIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}

int InputIPC_NameSave(const char* string)
{
#if 0
	strcpy(ipcRecord.NAME, string);
	onvif_discovery_device_list_set_name(ipcSettingIndex, ipcRecord.NAME);
	return 1;
#endif
	strcpy(ipcRecord.NAME, string);
	SetIpcDeviceInfo(ipcRecord.IP, ipcRecord.NAME);
	return 1;
}


void MENU_058_IPC_Manage_Init(int uMenuCnt)
{
	int i,w,h;
	POS pos;
	SIZE hv;
	char device_type[50];
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	BusySpriteDisplay(1);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_IPC_Manage, 1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	if(GetLastNMenu() == MENU_055_IPC_LOGIN)
	{
		IPC_ManageIconSelect = 0;
		IPC_ManagePageSelect = 0;
		ipcRecord.CH_ALL = Login.ch_cnt;
		if(ipc_check_exist(ipcRecord.IP, NULL) != 0)
		{
			API_Event_IoServer_InnerRead_All(ParaDeviceType, device_type);
			if(strcmp(device_type,"B-M100")==0)
			{

				ipcRecord.CH_REC = 1;
				ipcRecord.CH_FULL = 1;
				ipcRecord.CH_QUAD = 1;
				ipcRecord.CH_APP = 1;
			}
			else
			{
				if(ipcRecord.CH_ALL < 3)
				{
					ipcRecord.CH_REC = 0;
					ipcRecord.CH_FULL = 0;
					ipcRecord.CH_QUAD = 1;
					ipcRecord.CH_APP = 1;
				}
				else
				{
					ipcRecord.CH_REC = 0;
					ipcRecord.CH_FULL = 0;
					ipcRecord.CH_QUAD = 1;
					ipcRecord.CH_APP = 2;
				}
			}
		}
		else
		{
			GetIpcChannelInfo(ipcRecord.IP, NULL, &ipcRecord.CH_REC, &ipcRecord.CH_FULL, &ipcRecord.CH_QUAD, &ipcRecord.CH_APP);
		}
		
		for(i=0; i<ipcRecord.CH_ALL; i++ )
		{			
			ipcRecord.CH_DAT[i] = Login.dat[i];
			if(ipcRecord.CH_DAT[i].vd_type == -1)
				ipcRecord.CH_DAT[i].vd_type = 0;
			if(!ipcRecord.CH_DAT[i].width)
				ipcRecord.CH_DAT[i].width = 1280;
			if(!ipcRecord.CH_DAT[i].height)
				ipcRecord.CH_DAT[i].height = 720;	
			#if 0
			w = 0;
			h = 0;
			ipcRecord.CH_DAT[i].vd_type = Get_ffmpeg4ipc_videoType(ipcRecord.CH_DAT[i].rtsp_url, &w, &h);
			if(ipcRecord.CH_DAT[i].vd_type == -1)
				ipcRecord.CH_DAT[i].vd_type = 0;
			if(!ipcRecord.CH_DAT[i].width)
				ipcRecord.CH_DAT[i].width = w;
			if(!ipcRecord.CH_DAT[i].height)
				ipcRecord.CH_DAT[i].height = h;
			usleep(200*1000);
			#endif
		}
	}
	IPC_ManageIconMax = GetListIconNum();
	BusySpriteDisplay(0);
	DisplayIPC_ManagePageIcon(IPC_ManagePageSelect);
	
}

void MENU_058_IPC_Manage_Exit(void)
{
	
}

void MENU_058_IPC_Manage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[10];
	//char rtsp_url[250] = {0};
	//char device_url[250] = {0};
	//IPC_ONE_DEVICE ipcDat;
	int ch;
	int x,y,w,h;	
		
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					IPC_ManageSetting = IPC_ManagePageSelect*IPC_ManageIconMax + GetCurIcon() - ICON_007_PublicList1;
					
					if(IPC_ManageSetting < IPC_ManageIconNum)
					{
						switch(IPC_ManageIconTable[IPC_ManageSetting].iCon)
						{
							case ICON_260_IPC_Name:
								API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
								if(atoi(tempChar)==1)
									EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_Name, ipcRecord.NAME, 20, COLOR_WHITE, ipcRecord.NAME, 1, InputIPC_NameSave);
								else
									EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_Name, ipcRecord.NAME, 20, COLOR_WHITE, ipcRecord.NAME, 1, InputIPC_NameSave);
								//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_Name, ipcRecord.NAME, 20, COLOR_WHITE, ipcRecord.NAME, 1, InputIPC_NameSave);
								break;
							case ICON_264_IPC_Save:
								#if 0
								memset(&ipcDat, 0, sizeof(IPC_ONE_DEVICE));
								strcpy(ipcDat.ID, ipcRecord.ID);  
								strcpy(ipcDat.NAME, ipcRecord.NAME);  
								strcpy(ipcDat.IP, ipcRecord.IP);  
								strcpy(ipcDat.USER, ipcRecord.USER);  
								strcpy(ipcDat.PWD, ipcRecord.PWD);	
								ipcDat.CH_ALL = Login.ch_cnt;
								if(ipcDat.CH_ALL < 3)
								{
									ipcDat.CH_REC = 0;
									ipcDat.CH_FULL = 0;
									ipcDat.CH_QUAD = 1;
									ipcDat.CH_APP = 1;
								}
								else
								{
									ipcDat.CH_REC = 0;
									ipcDat.CH_FULL = 0;
									ipcDat.CH_QUAD = 1;
									ipcDat.CH_APP = 2;
								}
								for(i=0; i<ipcDat.CH_ALL; i++ )
								{
									ipcDat.CH_DAT[i] = Login.dat[i];
								}
								#endif
								if(GetIpcSetSelect()==0)
									AddOrModifyOneIpc(&ipcRecord);
								else
									AddOrModifyWlanIpc(&ipcRecord);
								//ipc_discovery_device_list_delete(ipcRecord.IP);
								BEEP_CONFIRM();
								popDisplayLastMenu();
								break;
							case ICON_IpcScenario:
								StartInitOneMenu(MENU_118_IpcScenario,0,1);
								break;
						}
					}
					else
					{
						ch = IPC_ManageSetting-3;
						if(ch < Login.ch_cnt)
						{
							API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
							API_OneIpc_Rtsp_Show(0, ipcRecord.CH_DAT[ch].rtsp_url, ipcRecord.CH_DAT[ch].width, ipcRecord.CH_DAT[ch].height, ipcRecord.CH_DAT[ch].vd_type,0,0,bkgd_w,bkgd_h);
							API_SpriteClose(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
							//EnterIPCMonMenu(0, NULL, 1);
							SetWinDeviceType(0, 1);
							SetWinDeviceName(0, ipcRecord.NAME);
							StartInitOneMenu(MENU_056_IPC_MONITOR,0,1);
							
						}
							
					}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}



