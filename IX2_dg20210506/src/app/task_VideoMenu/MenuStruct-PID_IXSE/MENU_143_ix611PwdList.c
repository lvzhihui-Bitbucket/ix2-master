#include "MENU_public.h"
#include "cJSON.h"

int ix611PwdIconMax;
int ix611PwdIconSelect;
int ix611PwdPageSelect;
int ix611PwdIndex;
cJSON *pwdList=NULL;
int pwdListCnt;
int devIpManage;
char devName[50];

void EnterPwdListMenu(int ip, char* name)
{
	devIpManage = ip;
	strcpy(devName, name);
	StartInitOneMenu(MENU_143_ix611PwdList,0,0);
}

void DisplayOnePagePwdlist(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start, list_max;
	POS pos;
	SIZE hv;
	cJSON *item = NULL;
	char display[100];

	list_max = pwdListCnt + 1 ;
	for( i = 0; i < ix611PwdIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		list_start = page*ix611PwdIconMax+i;
		if(list_start == 0)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_PwdManagerAdd, 1, hv.h - x);
		}
		else if(list_start < list_max)
		{
			item=cJSON_GetArrayItem(pwdList,list_start-1);
			int id = GetEventItemInt(item, "ID");
			char* name = GetEventItemString(item, "NOTE");
			snprintf(display, 100, "[%d]%s", id, name);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-pos.x);
		}
	}
	
	
	maxPage = list_max/ix611PwdIconMax + (list_max%ix611PwdIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_143_ix611PwdList_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	ix611PwdIconMax = GetListIconNum();
	ix611PwdIconSelect = 0;
	ix611PwdPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_051_ExternalUnit);
	//API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_ix611Pwds, 1, 0);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, devName, strlen(devName), 1, STR_UTF8, 0);
	pwdListCnt = 0;
	if(pwdList)
	{
		cJSON_Delete(pwdList);
		pwdList = NULL;
	}
	cJSON* view = cJSON_CreateArray();
	if(API_RemoteTableSelect(devIpManage, "PRIVATE_PWD", view, NULL, 1) > 0)
    {        
        pwdList = cJSON_Duplicate(view,1);
		pwdListCnt = cJSON_GetArraySize(pwdList);
		char *str=cJSON_Print(pwdList);
		printf("API_RemoteTableSelect PRIVATE_PWD=%s\n",str);
		free(str);
    }
    cJSON_Delete(view);
	printf("DisplayOnePagePwdlist cnt=%d\n",pwdListCnt);
	DisplayOnePagePwdlist(ix611PwdPageSelect);
}


void MENU_143_ix611PwdList_Exit(void)
{

}

void MENU_143_ix611PwdList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					ix611PwdIconSelect = GetCurIcon() - ICON_007_PublicList1;
					ix611PwdIndex = ix611PwdPageSelect*ix611PwdIconMax + ix611PwdIconSelect;
					if(ix611PwdIndex >= pwdListCnt+1)
						return;
					if(ix611PwdIndex == 0)
					{
						EnterPwdAddMenu();
					}
					else
					{
						EnterPwdEditMenu(ix611PwdIndex-1);
					}
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&ix611PwdPageSelect, ix611PwdIconMax, pwdListCnt+1, (DispListPage)DisplayOnePagePwdlist);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&ix611PwdPageSelect, ix611PwdIconMax, pwdListCnt+1, (DispListPage)DisplayOnePagePwdlist);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


