#include "MENU_public.h"

FuncMenuType installSubMenu;

void MENU_015_InstallSub_Init(int uMenuCnt)
{
	switch(installSubMenu)
	{
		case MENU_016_INSTALL_IP:
			MENU_016_InstallIp_Init(uMenuCnt);
			break;
			
		case MENU_020_INSTALL_CALL_NUM:
			MENU_020_InstallCallNum_Init(uMenuCnt);
			break;
			
		case MENU_021_ONSITE_TOOLS:
			MENU_021_OnsiteTools_Init(uMenuCnt);
			break;

		case MENU_077_ParaGroup:
			MENU_077_ManageParameter_Init(uMenuCnt);
			break;
			
		case MENU_066_FwUpgrade:
			MENU_066_FwUpgrade_Init(uMenuCnt);
			break;

		case MENU_067_FwUpgradeServer:
			MENU_067_FwUpgradeServer_Init(uMenuCnt);
			break;
		
		default:
			break;
			
	}
}

void MENU_015_InstallSub_Exit(void)
{
	switch(installSubMenu)
	{
		case MENU_016_INSTALL_IP:
			MENU_016_InstallIp_Exit();
			break;

		case MENU_020_INSTALL_CALL_NUM:
			MENU_020_InstallCallNum_Exit();
			break;

		case MENU_021_ONSITE_TOOLS:
			MENU_021_OnsiteTools_Exit();
			break;
			
		case MENU_077_ParaGroup:
			MENU_077_ManageParameter_Exit();
			break;
			
		case MENU_066_FwUpgrade:
			MENU_066_FwUpgrade_Exit();
			break;
			
		case MENU_067_FwUpgradeServer:
			MENU_067_FwUpgradeServer_Exit();
			break;
		
		default:
			break;
			
	}
	
}

void MENU_015_InstallSub_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_029_IP_ADDR:
					if(JudgeIfWlanDevice())
						EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
					else
						EnterInstallSubMenu(MENU_126_NM_LanSetting, 0);
					//EnterInstallSubMenu(MENU_016_INSTALL_IP, 0);
					break;
				case ICON_030_CALL_NUMBER:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 0);
					break;
				case ICON_049_Parameter:
					EnterInstallSubMenu(MENU_077_ParaGroup, 0);
					break;
					
				case ICON_045_Upgrade:
					EnterInstallSubMenu(MENU_066_FwUpgrade, 0);
					break;
					
				case ICON_031_OnsiteTools:
					SetSearchBdNumber(GetSysVerInfo_bd());
					SetSearchDeviceType(0);
					SetSearchMaxnum(0);
					EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
					break;

				default:
					switch(installSubMenu)
					{
						case MENU_016_INSTALL_IP:
							MENU_016_InstallIp_Process(arg);
							break;
							
						case MENU_020_INSTALL_CALL_NUM:
							MENU_020_InstallCallNum_Process(arg);
							break;
							
						case MENU_021_ONSITE_TOOLS:
							MENU_021_OnsiteTools_Process(arg);
							break;
							
						case MENU_077_ParaGroup:
							MENU_077_ManageParameter_Process(arg);
							break;
							
						case MENU_066_FwUpgrade:
							MENU_066_FwUpgrade_Process(arg);
							break;
						
						case MENU_067_FwUpgradeServer:
							MENU_067_FwUpgradeServer_Process(arg);
							break;
						
						default:
							break;
							
					}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch(installSubMenu)
		{
			case MENU_016_INSTALL_IP:
				MENU_016_InstallIp_Process(arg);
				break;
				
			case MENU_020_INSTALL_CALL_NUM:
				MENU_020_InstallCallNum_Process(arg);
				break;	
				
			case MENU_021_ONSITE_TOOLS:
				MENU_021_OnsiteTools_Process(arg);
				break;
				
			case MENU_077_ParaGroup:
				MENU_077_ManageParameter_Process(arg);
				break;
				
			case MENU_066_FwUpgrade:
				MENU_066_FwUpgrade_Process(arg);
				break;
				
			case MENU_067_FwUpgradeServer:
				MENU_067_FwUpgradeServer_Process(arg);
				break;
			default:
				break;
					
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		switch(installSubMenu)
		{
			case MENU_016_INSTALL_IP:
				MENU_016_InstallIp_Process(arg);
				break;
				
			case MENU_020_INSTALL_CALL_NUM:
				MENU_020_InstallCallNum_Process(arg);
				break;	
				
			case MENU_021_ONSITE_TOOLS:
				MENU_021_OnsiteTools_Process(arg);
				break;
			
			case MENU_077_ParaGroup:
				MENU_077_ManageParameter_Process(arg);
				break;
				
			case MENU_066_FwUpgrade:
				MENU_066_FwUpgrade_Process(arg);
				break;
			
			case MENU_067_FwUpgradeServer:
				MENU_067_FwUpgradeServer_Process(arg);
				break;
			default:
				break;
				
		}
	}
}

void EnterInstallSubMenu(FuncMenuType subMenu, int pushstack)
{
	if(GetCurMenuCnt() == MENU_015_INSTALL_SUB)
	{
		if(installSubMenu == subMenu)
		{
			return;
		}
		if(installSubMenu == MENU_066_FwUpgrade && subMenu == MENU_067_FwUpgradeServer)
		{
			installSubMenu = subMenu;
			MENU_066_FwUpgrade_Exit();
			MENU_067_FwUpgradeServer_Init(0);
		}
		else if(installSubMenu == MENU_067_FwUpgradeServer && subMenu == MENU_066_FwUpgrade)
		{
			installSubMenu = subMenu;
			MENU_067_FwUpgradeServer_Exit();
			MENU_066_FwUpgrade_Init(0);
		}
		else if(subMenu==MENU_125_NM_MAIN||subMenu==MENU_126_NM_LanSetting||subMenu==MENU_127_NM_WlanSetting||subMenu==MENU_128_NM_WifiInfo)
		{
			installSubMenu = subMenu;
			StartInitOneMenu(subMenu,0,pushstack);
		}
		else
		{
			installSubMenu = subMenu;
			StartInitOneMenu(MENU_015_INSTALL_SUB,installSubMenu,pushstack);
		}
		
	}
	else
	{
		if(subMenu==MENU_125_NM_MAIN||subMenu==MENU_126_NM_LanSetting||subMenu==MENU_127_NM_WlanSetting||subMenu==MENU_128_NM_WifiInfo)
		{
			installSubMenu = subMenu;
			StartInitOneMenu(subMenu,0,pushstack);
		}
		else
		{
			installSubMenu = subMenu;
			StartInitOneMenu(MENU_015_INSTALL_SUB,installSubMenu,pushstack);
		}
	}
}


