
#ifndef _MENU_038_H
#define _MENU_038_H

#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <list.h>
#include <stdlib.h>
#include <string.h>

#include "MENU_public.h"

#define READ_BUFF_LEN			300
#define MAX_STR 				32

typedef struct
{
	int iCon;
	int iConText; 
}SipCfgIcon;
#if 0
typedef struct
{
	char serverIp[MAX_STR];
	char server[MAX_STR];
	char port[MAX_STR];
	char divert[MAX_STR];
	char account[MAX_STR];
	char password[MAX_STR];
	char monCode[MAX_STR];
	char callCode[MAX_STR];
	char divPwd[MAX_STR];
	char saveCode[MAX_STR];
	// lzh_20180727_s
	char divPwd_change[MAX_STR];
	// lzh_20180727_e
	// lzh_20180804_s
	char qrcode[2000+1];
	// lzh_20180804_e	
	int serverToIpFlag;
}SipCfg_T;
#endif
void SipConfig_Init(void);
int Get_SipConfig_Account(char *paccount);
int Get_SipConfig_DirvertAccount(char *paccount);
int Get_SipConfig_CallCode(char *paccount);
int Get_SipConfig_MonCode(char *paccount);
// lzh_20180727_s
int change_password_of_divert_account(void);
// lzh_20180727_e
// lzh_20180804_s
char* create_sipcfg_qrcode(void);
// lzh_20180804_e
int SaveSipServer(void);
int SaveSipData(char * code);
int SaveCallCodeOrMonCode(char * code);

#endif


