
#include "MENU_public.h"
#include "MENU_139_KeySelect.h"


#define	KEY_SELECT_ICON_MAX		5
extern EXT_MODULE_JSON extModuleObj;
EXT_MODULE_KEY_RECORD oneExtModuleKeyRecord;

int keySelectIconSelect;
int keySelectPageSelect;
static int restoreToDefaultConfirm,keySelectConfirm;

const IconAndText_t keySelectIconTable[] = 
{
	{ICON_ExtModuleS4K4,						MESG_TEXT_ICON_ExtModuleS4K4},
	{ICON_ExtModuleS4K3,						MESG_TEXT_ICON_ExtModuleS4K3},
	{ICON_ExtModuleS4K2,						MESG_TEXT_ICON_ExtModuleS4K2},
	{ICON_ExtModuleS4K1,						MESG_TEXT_ICON_ExtModuleS4K1},
	{ICON_RestoreFactorySettings,				MESG_TEXT_ICON_RestoreFactorySettings},
};

int DisplayOnePageKeySelect( int page )
{
	int i, x, y, len;
	EXT_MODULE_KEY_RECORD record;
	char display[20];
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	snprintf(display, 20, " Module %d", extModuleObj.s4[page]);
	API_OsdStringClearExt(pos.x, hv.v/2, bkgd_w-pos.x, 40);
	API_OsdUnicodeStringDisplayWithIdAndAsc(pos.x, hv.v/2, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ExtModuleS4, 1, NULL, display, bkgd_w-pos.x);
	
	for( i = 0; i < KEY_SELECT_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		//len = (i == 4 ? DISPLAY_VALUE_X_DEVIATION : (DISPLAY_VALUE_X_DEVIATION - 150));
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, keySelectIconTable[i].iConText, 1, hv.h - x);
		if(get_pane_type() == 5 )
		{
			x += (hv.h - pos.x)/3;
			y += 40;
		}
		else
		{
			x += (hv.h - pos.x)/2;
		}
		switch(keySelectIconTable[i].iCon)
		{
			case ICON_ExtModuleS4K4:
				if(GetExtModuleKeyRecord(extModuleObj.s4[page], 4, &record) >= 0)
				{
					strcpy(display, record.number);
				}
				else
				{
					strcpy(display, "-");
				}
				API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
				break;
			case ICON_ExtModuleS4K3:
				if(GetExtModuleKeyRecord(extModuleObj.s4[page], 3, &record) >= 0)
				{
					strcpy(display, record.number);
				}
				else
				{
					strcpy(display, "-");
				}
				API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
				break;
			case ICON_ExtModuleS4K2:
				if(GetExtModuleKeyRecord(extModuleObj.s4[page], 2, &record) >= 0)
				{
					strcpy(display, record.number);
				}
				else
				{
					strcpy(display, "-");
				}
				API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
				break;
				
			case ICON_ExtModuleS4K1:
				if(GetExtModuleKeyRecord(extModuleObj.s4[page], 1, &record) >= 0)
				{
					strcpy(display, record.number);
				}
				else
				{
					strcpy(display, "-");
				}
				API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
				break;
				
				
			default:
				break;
		}
	}
	
	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, extModuleObj.s4Nbr);

	
	//API_EnableOsdUpdate();
}

/*************************************************************************************************
010.
*************************************************************************************************/
void MENU_139_KeySelect_Init(int uMenuCnt)
{
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);

	if(GetLastNMenu() == MENU_138_ModuleSelect)
	{
		ExtModuleKeyTableInit();
		keySelectIconSelect = 0;
		keySelectPageSelect = 0;
	}

	DisplayOnePageKeySelect(keySelectPageSelect);
}

void MENU_139_KeySelect_Exit(void)
{
}

void MENU_139_KeySelect_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	EXT_MODULE_KEY_RECORD keyRecord;
	int i;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);	

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_047_Home:
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_SAVE_ExtModuleS4Table, 1, bkgd_w-pos.x);
					SaveExtModuleKeyTable();
					sleep(1);
					API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
					GoHomeMenu();
					break;
				case ICON_200_Return:
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_SAVE_ExtModuleS4Table, 1, bkgd_w-pos.x);
					SaveExtModuleKeyTable();
					sleep(1);
					API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					keySelectIconSelect = GetCurIcon() - ICON_007_PublicList1;

					switch(keySelectIconTable[keySelectIconSelect].iCon)
					{
						case ICON_ExtModuleS4K4:
							ClearConfirm(&restoreToDefaultConfirm, &keySelectConfirm, KEY_SELECT_ICON_MAX);
							#if 0
							if(restoreToDefaultConfirm)
							{
								restoreToDefaultConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
							}
							#endif

							if(GetExtModuleKeyRecord(extModuleObj.s4[keySelectPageSelect], 4, &oneExtModuleKeyRecord) < 0)
							{
								oneExtModuleKeyRecord.module = extModuleObj.s4[keySelectPageSelect];
								oneExtModuleKeyRecord.key = 4;
								strcpy(oneExtModuleKeyRecord.number, "-");
							}
							StartInitOneMenu(MENU_140_KeyConfig,0,1);
							break;
						case ICON_ExtModuleS4K3:
							ClearConfirm(&restoreToDefaultConfirm, &keySelectConfirm, KEY_SELECT_ICON_MAX);
							#if 0
							if(restoreToDefaultConfirm)
							{
								restoreToDefaultConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
							}
							#endif
							if(GetExtModuleKeyRecord(extModuleObj.s4[keySelectPageSelect], 3, &oneExtModuleKeyRecord) < 0)
							{
								oneExtModuleKeyRecord.module = extModuleObj.s4[keySelectPageSelect];
								oneExtModuleKeyRecord.key = 3;
								strcpy(oneExtModuleKeyRecord.number, "-");
							}
							StartInitOneMenu(MENU_140_KeyConfig,0,1);
							break;
						case ICON_ExtModuleS4K2:
							ClearConfirm(&restoreToDefaultConfirm, &keySelectConfirm, KEY_SELECT_ICON_MAX);
							#if 0
							if(restoreToDefaultConfirm)
							{
								restoreToDefaultConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
							}
							#endif
							if(GetExtModuleKeyRecord(extModuleObj.s4[keySelectPageSelect], 2, &oneExtModuleKeyRecord) < 0)
							{
								oneExtModuleKeyRecord.module = extModuleObj.s4[keySelectPageSelect];
								oneExtModuleKeyRecord.key = 2;
								strcpy(oneExtModuleKeyRecord.number, "-");
							}
							StartInitOneMenu(MENU_140_KeyConfig,0,1);
							break;
						case ICON_ExtModuleS4K1:
							ClearConfirm(&restoreToDefaultConfirm, &keySelectConfirm, KEY_SELECT_ICON_MAX);
							#if 0
							if(restoreToDefaultConfirm)
							{
								restoreToDefaultConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
							}
							#endif
							if(GetExtModuleKeyRecord(extModuleObj.s4[keySelectPageSelect], 1, &oneExtModuleKeyRecord) < 0)
							{
								oneExtModuleKeyRecord.module = extModuleObj.s4[keySelectPageSelect];
								oneExtModuleKeyRecord.key = 1;
								strcpy(oneExtModuleKeyRecord.number, "-");
							}
							StartInitOneMenu(MENU_140_KeyConfig,0,1);
							break;
						case ICON_RestoreFactorySettings:
							if(ConfirmSelect(&restoreToDefaultConfirm, &keySelectConfirm, &keySelectIconSelect, keySelectPageSelect, KEY_SELECT_ICON_MAX))
							{
								return;
							}
							#if 0
							if(restoreToDefaultConfirm)
							{
								restoreToDefaultConfirm = 0;
								API_SpriteClose(ComfirmText_Disp_x, ComfirmText_Disp_y(4), SPRITE_IF_CONFIRM);
							}
							else
							{
								restoreToDefaultConfirm = 1;
								API_SpriteDisplay_XY(ComfirmText_Disp_x, ComfirmText_Disp_y(keySelectIconSelect), SPRITE_IF_CONFIRM);
								return;
							}
							#endif
							for(i = 0; i < 4; i++)
							{
								keyRecord.module = extModuleObj.s4[keySelectPageSelect];
								keyRecord.key= i+1;
								snprintf(keyRecord.number, 11, "%s%04d01", GetSysVerInfo_bd(), extModuleObj.s4[keySelectPageSelect]*4+keyRecord.key);
								AddOrModifyExtModuleKeyRecord(&keyRecord);
							}
							DisplayOnePageKeySelect(keySelectPageSelect);
							break;
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&keySelectPageSelect, KEY_SELECT_ICON_MAX, extModuleObj.s4Nbr*KEY_SELECT_ICON_MAX, (DispListPage)DisplayOnePageKeySelect);
					break;
					
				case ICON_202_PageUp:
					PublicPageUpProcess(&keySelectPageSelect, KEY_SELECT_ICON_MAX, extModuleObj.s4Nbr*KEY_SELECT_ICON_MAX, (DispListPage)DisplayOnePageKeySelect);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

static one_vtk_table* extModuleKeyTable = NULL;

void ExtModuleKeyTableInit(void)
{
	free_vtk_table_file_buffer(extModuleKeyTable);
	
	extModuleKeyTable = load_vtk_table_file(EXT_MODULE_KEY_TABLE_FILE_NAME);
	if(extModuleKeyTable == NULL)
	{
		extModuleKeyTable = malloc(sizeof(one_vtk_table));
		pthread_mutex_init( &extModuleKeyTable->lock, 0);		//czn_20190327
		extModuleKeyTable->keyname_cnt = 3;
		extModuleKeyTable->pkeyname_len = malloc(extModuleKeyTable->keyname_cnt*sizeof(int));
		extModuleKeyTable->pkeyname = malloc(extModuleKeyTable->keyname_cnt*sizeof(one_vtk_dat));
		extModuleKeyTable->pkeyname_len[0] = 1;
		extModuleKeyTable->pkeyname_len[1] = 1;
		extModuleKeyTable->pkeyname_len[2] = 10;
		extModuleKeyTable->pkeyname[0].len = strlen("S4_ID");
		extModuleKeyTable->pkeyname[0].pdat = malloc(extModuleKeyTable->pkeyname[0].len);
		memcpy(extModuleKeyTable->pkeyname[0].pdat, "S4_ID", extModuleKeyTable->pkeyname[0].len);
		
		extModuleKeyTable->pkeyname[1].len = strlen("KEY");
		extModuleKeyTable->pkeyname[1].pdat = malloc(extModuleKeyTable->pkeyname[1].len);
		memcpy(extModuleKeyTable->pkeyname[1].pdat, "KEY", extModuleKeyTable->pkeyname[1].len);

		extModuleKeyTable->pkeyname[2].len = strlen("NUMBER");
		extModuleKeyTable->pkeyname[2].pdat = malloc(extModuleKeyTable->pkeyname[2].len);
		memcpy(extModuleKeyTable->pkeyname[2].pdat, "NUMBER", extModuleKeyTable->pkeyname[2].len);

		extModuleKeyTable->record_cnt = 0;
	}
}

int SaveExtModuleKeyTable(void)
{
	remove(EXT_MODULE_KEY_TABLE_FILE_NAME);
	save_vtk_table_file_buffer( extModuleKeyTable, 0, extModuleKeyTable->record_cnt, EXT_MODULE_KEY_TABLE_FILE_NAME);
	sync();
	SaveResFileFromActFile(8005,GetSysVerInfo_BdRmMs(),0);
	Api_ResSyncToDevice(8005, GetOnlineManageNbr());
}
int DeleteExtModuleKeyTable(void)
{
	free_vtk_table_file_buffer(extModuleKeyTable);
	remove(EXT_MODULE_KEY_TABLE_FILE_NAME);
}

int GetExtModuleKeyNum(void)
{
	if(extModuleKeyTable == NULL)
	{
		return 0;
	}

	return extModuleKeyTable->record_cnt;
}

int GetExtModuleKeyRecord(int module, int key, EXT_MODULE_KEY_RECORD* record)
{
	one_vtk_dat *pRecord;
	char charModule[11] = {0};
	char charkey[11] = {0};
	char number[11] = {0};
	int i, index;
	char* pos1;
	char recordBuffer[100];
	
	char *strTable[3] = {charModule, charkey, number};
	
	if(extModuleKeyTable == NULL || extModuleKeyTable->record_cnt == 0)
	{
		return -1;
	}

	for(index = 0; index < extModuleKeyTable->record_cnt; index++)

	{
		pRecord = get_one_vtk_record_without_keyvalue(extModuleKeyTable, index);
		if(pRecord == NULL)
		{
			continue;
		}

		memcpy(recordBuffer, pRecord->pdat, pRecord->len);
		recordBuffer[pRecord->len] = 0;
		
		for(pos1 = strtok(recordBuffer,","), i = 0; i < 3 && strlen(pos1) < 11 && pos1 != NULL; pos1 = strtok(NULL,","))
		{
			strcpy(strTable[i++], pos1);
		}
		
		
		record->module = atoi(charModule);
		record->key = atoi(charkey);
		strcpy(record->number, number);
		if(record->module == module && record->key == key)
		{
			return index;
		}
	}

	
	return -1;
}

int ModifyExtModuleKeyRecord(int index, EXT_MODULE_KEY_RECORD* keyRecord)
{
	#define RECORD_MAX_LEN	20
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%d,%d,%s", keyRecord->module, keyRecord->key, keyRecord->number);
	record.pdat = data;
	record.len = strlen(data);
	Modify_one_vtk_record(&record, extModuleKeyTable, index);

	return 0;
}

int AddExtModuleKeyRecord(EXT_MODULE_KEY_RECORD* keyRecord)
{
	#define RECORD_MAX_LEN	20
	one_vtk_dat record;
	unsigned char data[RECORD_MAX_LEN] = {0};
	
	snprintf(data, RECORD_MAX_LEN, "%d,%d,%s", keyRecord->module, keyRecord->key, keyRecord->number);
	record.len = strlen(data);
	record.pdat = malloc(record.len);
	memcpy(record.pdat, data, record.len);
	add_one_vtk_record(extModuleKeyTable, &record);
	return 0;
}

int AddOrModifyExtModuleKeyRecord(EXT_MODULE_KEY_RECORD* keyRecord)
{
	EXT_MODULE_KEY_RECORD record;
	int index;
	index = GetExtModuleKeyRecord(keyRecord->module, keyRecord->key, &record);
	if(index >= 0)
	{
		ModifyExtModuleKeyRecord(index, keyRecord);
	}
	else
	{
		AddExtModuleKeyRecord(keyRecord);
	}

	return 0;
}



