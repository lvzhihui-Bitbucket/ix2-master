#include "MENU_036_ListEdit.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"
#include "obj_MS_DeviceTable.h"
#include "task_CallServer.h"
#include "obj_IPCTableSetting.h"

//#define	listEditIconMax	5
int listEditIconMax;
int listEditIconSelect;
int listEditPageSelect;
int listEditMaxNum;
int listEditIndex;
int listDeleteConfirm;

char listEditInput[21];

extern IM_NameListRecord_T nameListRecord;

LIST listSelect;
static int actionSelect;

void ListEditSelect(int index)
{
	listSelect = index;
}
int GetListEditSelect(void)
{
	return listSelect;
}
static void ListMaxNumUpdate(void)
{

	if(actionSelect == ICON_035_ListAdd)
	{
		switch(listSelect)
		{
			case IM_LIST:
			case DS_LIST:
			case MS_LIST:
				listEditMaxNum = 2;
				break;
			case IPC_LIST:
				//listEditMaxNum = Get_IPC_MonRes_Num() + 2;
				listEditMaxNum = GetIpcNum() + 2;
				break;
			case WlanIPC_LIST:
				listEditMaxNum = GetWlanIpcNum() + 2;
				break;
		}
	}
	else if(actionSelect == ICON_038_ListCheck&&listSelect==IPC_LIST)		//czn_20190525
	{
		listEditMaxNum = 2;
	}
	else if(actionSelect == ICON_038_ListCheck&&listSelect==WlanIPC_LIST)
	{
		listEditMaxNum = 2;
	}
	else
	{
		switch(listSelect)
		{
			case IM_LIST:
				listEditMaxNum = GetImNameListRecordCnt();
				break;
			case DS_LIST:
				listEditMaxNum = Get_MonRes_Num();
				break;
			case IPC_LIST:
				//listEditMaxNum = Get_IPC_MonRes_Num();
				listEditMaxNum = GetIpcNum();
				break;
			case MS_LIST:
				listEditMaxNum = GetMSListRecordCnt();
				break;
			case WlanIPC_LIST:
				listEditMaxNum = GetWlanIpcNum();
				break;
		}
	}

	if(listEditMaxNum <= listEditPageSelect*listEditIconMax)
	{
		listEditPageSelect = (listEditMaxNum-1)/listEditIconMax;
	}
}

static void DisplayOnePageImlist(uint8 page)
{
	IM_NameListRecord_T record;
	int i, x, y, maxPage;
	int list_start;
	char recordBuffer[40];
	int monres_num = GetImNameListRecordCnt(); 
	POS pos;
	SIZE hv;

	list_start = page*listEditIconMax;
	
	for( i = 0; i < listEditIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		if(list_start+i < monres_num)
		{

			if(GetImNameListRecordItems(list_start+i,	&record) != 0)
				return;
			
			record.BD_RM_MS[8] = 0;
			
			if(strcmp(record.R_Name, "-"))
			{
				snprintf(recordBuffer,40,"[%s]%s",record.BD_RM_MS,record.R_Name);
			}
			else
			{
				snprintf(recordBuffer,40,"[%s]%s",record.BD_RM_MS,record.name1);
			}
				
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, recordBuffer, strlen(recordBuffer),1,STR_UTF8,hv.h - x);
		}
	}
	
	maxPage = monres_num/listEditIconMax + (monres_num%listEditIconMax ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

static void DisplayOnePageDSlist(uint8 page)
{
	int i, x, y, maxPage;
	int list_start;
	char display[51];
	char displayRoomNumber[11];
	POS pos;
	SIZE hv;

	int monres_num = Get_MonRes_Num();
	
	//API_DisableOsdUpdate();

	list_start = page*listEditIconMax;
	
	for( i = 0; i < listEditIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if((list_start+i) < monres_num)
		{
			char name_temp[41];
			char bdRmMs[11];
			
			if(Get_MonRes_Record(list_start+i, name_temp, bdRmMs, NULL) == 0)
			{
				get_device_addr_and_name_disp_str(0, bdRmMs, NULL, NULL, name_temp, display);
				
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1,STR_UTF8, hv.h - x);
			}
		}
		
	}
	
	
	maxPage = (monres_num%listEditIconMax) ? (monres_num/listEditIconMax+1) : (monres_num/listEditIconMax);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

static void DisplayOnePageMSEditlist(uint8 page)
{
	int i, x, y, maxPage;
	int list_start;
	char display[51];
	POS pos;
	SIZE hv;

	int monres_num = GetMSListRecordCnt();
	
	//API_DisableOsdUpdate();

	list_start = page*listEditIconMax;
	
	for( i = 0; i < listEditIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if((list_start+i) < monres_num)
		{
			char name_temp[41];
			char bdRmMs[11];
			
			if(Get_MS_Record(list_start+i, name_temp, bdRmMs) == 0)
			{
				memcpy(display, bdRmMs, 8);
				snprintf(display+8, 51-8, "(%s)%s", bdRmMs+8, name_temp);
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1,STR_UTF8, hv.h - x);
			}
		}
		
	}
	
	
	maxPage = (monres_num%listEditIconMax) ? (monres_num/listEditIconMax+1) : (monres_num/listEditIconMax);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}

static void DisplayOnePageAddList(uint8 page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	int index;
	POS pos;
	SIZE hv;

	//API_DisableOsdUpdate();

	for(i = 0; i < listEditIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		index = page*listEditIconMax+i;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);

		if(index < listEditMaxNum)
		{
			if(index == 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_AddBySearch, 1, hv.h-x);
			}
			else if(index == 1)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_AddByManual, 1, hv.h-x);
			}
			else
			{
				switch(listSelect)
				{
					case IPC_LIST:
						DisplayOneIPCList(index-2, x, y);
						break;
					case WlanIPC_LIST:
						DisplayOneWlanIPCList(index-2, x, y);
						break;
					case IM_LIST:
					case DS_LIST:
					case MS_LIST:
						break;
				}
			}
		}
	}
	pageNum = listEditMaxNum/listEditIconMax + (listEditMaxNum%listEditIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

static void DisplayOnePageListSync(uint8 page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	int index;
	POS pos;
	SIZE hv;

	//API_DisableOsdUpdate();

	for(i = 0; i < listEditIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		index = page*listEditIconMax+i;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);

		if(index < listEditMaxNum)
		{
			if(index == 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_ListSyncToServer, 1, hv.h-x);
			}
			else if(index == 1)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR,MESG_TEXT_ListSyncFromServer, 1, hv.h-x);
			}
		}
	}
	pageNum = (listEditMaxNum+1)/listEditIconMax + ((listEditMaxNum+1)%listEditIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
static void DisplayOnePageOnlineList(uint8 page)
{
	int i;

	//API_DisableOsdUpdate();
	
	switch(listSelect)
	{
		case IM_LIST:
			DisplayOnePageImlist(page);
			//BusySpriteDisplay(1);
			for(i = 0; i < listEditIconMax; i++)
			{
				GetIpRspData getIpdata;
				IM_NameListRecord_T record;
				
				ListSelect(i, 0);
				getIpdata.cnt = 0;
				if(GetImNameListRecordItems(i + page*listEditIconMax,  &record) == 0)
				{
					API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 1, 1, &getIpdata);
				}

				if(getIpdata.cnt)
				{
					ListSelect(i, 1);
				}
			}
			//BusySpriteDisplay(0);
			break;
		case DS_LIST:
			DisplayOnePageDSlist(page);
			//BusySpriteDisplay(1);
			for(i = 0; i < listEditIconMax; i++)
			{
				GetIpRspData getIpdata;
				MonRes_Entry_Stru record;
				
				ListSelect(i, 0);
				getIpdata.cnt = 0;
				if(get_monres_table_record_items(i + page*listEditIconMax,  &record) == 0)
				{
					API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 1, 1, &getIpdata);
				}
				if(getIpdata.cnt)
				{
					ListSelect(i, 1);
				}
			}
			//BusySpriteDisplay(0);
			break;
		case MS_LIST:
			DisplayOnePageMSEditlist(page);
			for(i = 0; i < listEditIconMax; i++)
			{
				GetIpRspData getIpdata;
				MS_ListRecord_T record;
				
				ListSelect(i, 0);
				getIpdata.cnt = 0;
				if(GetMsListRecordItems(i + page*listEditIconMax,  &record) == 0)
				{
					API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 1, 1, &getIpdata);
				}

				if(getIpdata.cnt)
				{
					ListSelect(i, 1);
				}
			}
			break;
			
		case IPC_LIST:
			DisplayOnePageIPCMonitor(page);
			//BusySpriteDisplay(1);
			for(i = 0; i < listEditIconMax; i++)
			{
				//IPC_RECORD record;
				IPC_ONE_DEVICE record;
				ListSelect(i, 0);
				//if(Get_IPC_MonRes_Record(i + page*listEditIconMax,  &record) == 0)
				if(GetIpcCacheRecord(i + page*listEditIconMax,  &record) == 0)
				{
					if(ipc_check_online(record.IP) == 0)
					{
						ListSelect(i, 1);
					}
				}
			}
			//BusySpriteDisplay(0);
			break;
			
		case WlanIPC_LIST:
			DisplayOnePageWlanIPCMonitor(page);
			//BusySpriteDisplay(1);
			for(i = 0; i < listEditIconMax; i++)
			{
				IPC_ONE_DEVICE record;
				
				ListSelect(i, 0);
				if(GetWlanIpcRecord(i + page*listEditIconMax,	&record) == 0)
				{
					if(ipc_check_online(record.IP) == 0)
					{
						ListSelect(i, 1);
					}
				}
			}
			//BusySpriteDisplay(0);
			break;
	}

	//API_EnableOsdUpdate();
}


void StartDSMonitor(int index)
{
	char name[21] = "NAME TEST";
	char para_buff[20];

	if(index < Get_MonRes_Num())
	{
		if(Get_MonRes_Record(index, name,para_buff, NULL) == 0)
		{
			//Start_Monitor_Menu_ByPara(0,1,name,para_buff);
		}
	}
}

static int ConfirmModifyOneMonResName(const char* name)
{
	if(strlen(name) > 0)
	{
		MonRes_Rename(listEditIndex, name);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmModifyOneIPCName(const char* name)
{
	extern IPC_RECORD saveIPCMonList;
	if(strlen(name) > 0)
	{
		strcpy(saveIPCMonList.name, name);
		Modify_IPC_MonRes_Record(listEditIndex, &saveIPCMonList);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmModifyOneWlanIPCName(const char* name)
{
	extern IPC_RECORD saveWlanIPCMonList;
	if(strlen(name) > 0)
	{
		strcpy(saveWlanIPCMonList.name, name);
		Modify_WlanIPC_MonRes_Record(listEditIndex, &saveWlanIPCMonList);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmAddOneNamelist(const char* BD_RM)
{
	if(strlen(BD_RM) == 8)
	{
		IM_NameListRecord_T record;
		
		memset(&record, 0, sizeof(IM_NameListRecord_T));

		
		//czn_20190221_s
		strcpy(record.BD_RM_MS, BD_RM);
		strcpy(record.BD_RM_MS+8, "00");
		memset(record.MFG_SN, '0',12);
		strcpy(record.name1, "IM");
		strcpy(record.R_Name, "-");
		strcpy(record.Local, "-");
		strcpy(record.Global, "-");
		//czn_20190221_e

		AddOneImNamelist_Record(&record);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmAddOneMSlist(const char* MS)
{
	if(strlen(MS) <= 2)
	{
		MS_ListRecord_T record;
		
		memset(&record, 0, sizeof(MS_ListRecord_T));

		
		sprintf(record.BD_RM_MS, "%s%s%02d", GetSysVerInfo_bd(), GetSysVerInfo_rm(), atoi(MS));
		strcpy(record.R_Name, "MS");

		AddOneMSlist_Record(&record);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmAddOneDslist(const char* BD_RM)
{
	if(strlen(BD_RM) == 10)
	{
		MonRes_Entry_Stru record;
		
		memset(&record, 0, sizeof(MonRes_Entry_Stru));

		
		strcpy(record.BD_RM_MS, BD_RM);
		strcpy(record.R_Name, "DS");

		AddOneMonRes_Record(&record);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmRenameNamelist(const char* name)
{
	if(strlen(name) > 0)
	{
		strcpy(nameListRecord.R_Name, name);
		ImNamelist_Rename(listEditIndex, name);
		return 1;
	}
	else
	{
		return 0;
	}
}

static int ConfirmRenameMSlist(const char* name)
{
	if(strlen(name) > 0)
	{
		MSlist_Rename(listEditIndex, name);
		return 1;
	}
	else
	{
		return 0;
	}
}

static void DisplayOnePageList(int page)
{
	switch(listSelect)
	{
		case IM_LIST:
			DisplayOnePageImlist(page);
			break;
		case DS_LIST:
			DisplayOnePageDSlist(page);
			break;
		case MS_LIST:
			DisplayOnePageMSEditlist(page);
			break;
		case IPC_LIST:
			DisplayOnePageIPCMonitor(page);
			break;
		case WlanIPC_LIST:
			DisplayOnePageWlanIPCMonitor(page);
			break;
	}
}

static void ClearDeleteConfirmFlag(void)
{
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	if(actionSelect == ICON_036_ListDelete)
	{
		if(listDeleteConfirm >= 0)
		{
			OSD_GetIconInfo(ICON_007_PublicList1+listDeleteConfirm, &pos, &hv);
			Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
			API_SpriteClose(hv.h - xsize, hv.v - ysize, SPRITE_IF_CONFIRM);
			//API_SpriteClose(640, 85+65*listDeleteConfirm, SPRITE_IF_CONFIRM);
		}
		listDeleteConfirm = -1;
	}
}

static void ActionSelect(int action)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	listEditPageSelect = 0;
	API_MenuIconDisplaySelectOff(actionSelect);

	if(actionSelect == ICON_038_ListCheck&&listSelect != IPC_LIST)	//czn_20190525
	{
		int i;  
		for(i = 0; i < listEditIconMax; i++)
		{
			ListSelect(i, 0);		 
		}
	}
	
	ClearDeleteConfirmFlag();

	actionSelect = action;
	ListMaxNumUpdate();
	API_MenuIconDisplaySelectOn(actionSelect);
	
	API_OsdStringClearExt(pos.x, hv.v/2, 300, 40);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, actionSelect, 1, 0);

	if(actionSelect == ICON_038_ListCheck)			//czn_20190525
	{
		if(listSelect == IPC_LIST)
		{
			API_DisableOsdUpdate();
			DisplayOnePageListSync(listEditPageSelect);
			API_EnableOsdUpdate();
		}
		else
		{
			BusySpriteDisplay(1);
			API_DisableOsdUpdate();
			DisplayOnePageOnlineList(listEditPageSelect);
			API_EnableOsdUpdate();
			BusySpriteDisplay(0);
		}
	}
	else if(actionSelect == ICON_035_ListAdd)
	{
		API_DisableOsdUpdate();
		DisplayOnePageAddList(listEditPageSelect);
		API_EnableOsdUpdate();
	}
	#if 0
	else if(actionSelect == ICON_400_ListSync)
	{
		API_DisableOsdUpdate();
		DisplayOnePageListSync(listEditPageSelect);
		API_EnableOsdUpdate();
	}
	#endif
	else
	{
		API_DisableOsdUpdate();
		DisplayOnePageList(listEditPageSelect);
		API_EnableOsdUpdate();
	}
	

}

typedef int (*DeleteOneRecord)(int);
int DeleteOneImNamelist_Record(int index);
int Delete_IPC_MonRes_Record(int index);
int DeleteOneMonRes_Record(int index);
int DeleteOneMSlist_Record(int index);
int Delete_WlanIPC_MonRes_Record(int index);

static int DeleteOneRecordProcess(DeleteOneRecord deleteRecordProcess, int index)
{
	int ret;
	uint16 x, y;
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_007_PublicList1+listDeleteConfirm, &pos, &hv);
	Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
	x = hv.h - xsize;
	y = hv.v - ysize;
	
	if(listDeleteConfirm < 0)
	{
		listDeleteConfirm = listEditIconSelect;
		OSD_GetIconInfo(ICON_007_PublicList1+listEditIconSelect, &pos, &hv);
		x = hv.h - xsize;
		y = hv.v - ysize;
		API_SpriteDisplay_XY(x, y, SPRITE_IF_CONFIRM);
		//API_SpriteDisplay_XY(640, 85+65*listEditIconSelect, SPRITE_IF_CONFIRM);
		ret = 0;
	}
	else if(listDeleteConfirm == listEditIconSelect)
	{
		listDeleteConfirm = -1;
		OSD_GetIconInfo(ICON_007_PublicList1+listEditIconSelect, &pos, &hv);
		x = hv.h - xsize;
		y = hv.v - ysize;
		API_SpriteClose(x, y, SPRITE_IF_CONFIRM);
		//API_SpriteClose(640, 85+65*listEditIconSelect, SPRITE_IF_CONFIRM);
		deleteRecordProcess(index);
		BEEP_CONFIRM();
		
		ListMaxNumUpdate();
		API_DisableOsdUpdate();
		DisplayOnePageList(listEditPageSelect);
		API_EnableOsdUpdate();
		ret = 1;
	}
	else
	{
		API_SpriteClose(x, y, SPRITE_IF_CONFIRM);
		OSD_GetIconInfo(ICON_007_PublicList1+listEditIconSelect, &pos, &hv);
		x = hv.h - xsize;
		y = hv.v - ysize;
		API_SpriteDisplay_XY(x, y, SPRITE_IF_CONFIRM);
		//API_SpriteClose(640, 85+65*listDeleteConfirm, SPRITE_IF_CONFIRM);
		//API_SpriteDisplay_XY(640, 85+65*listEditIconSelect, SPRITE_IF_CONFIRM);
		listDeleteConfirm = listEditIconSelect;
		ret = 0;
	}

	return ret;
}

static void StartSlaveCall(int index)
{
	MS_ListRecord_T record;
	GetIpRspData data;
	Global_Addr_Stru target_addr;

	if(GetMsListRecordItems(index,  &record) != 0)
		return;
	
	Call_Dev_Info target_dev[MAX_CALL_TARGET_NUM];

	if(API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 2, 8, &data) != 0)
	{
		BEEP_ERROR();
		return;
	}
	
	int i,j;
	
	data.cnt = data.cnt > (MAX_CALL_TARGET_NUM) ? (MAX_CALL_TARGET_NUM) : data.cnt;	
	for(i = 0,j=0; i < data.cnt; i++)
	{
		if(api_nm_if_judge_include_dev(data.BD_RM_MS[i],data.Ip[i])==0)
		{
			continue;
		}
		memset(&target_dev[j], 0, sizeof(Call_Dev_Info));
		memcpy(target_dev[j].bd_rm_ms, data.BD_RM_MS[i], 10);
		strcpy(target_dev[j].name, record.R_Name);
		target_dev[j].ip_addr = data.Ip[i];
		j++;
	}
	data.cnt=j;
	return API_CallServer_Invite(IxCallScene2_Active, data.cnt, target_dev);
		
}

void MENU_036_ListEdit_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	listEditIconMax = GetListIconNum();
	if( GetLastNMenu() == MENU_011_SETTING)
	{
		listEditIconSelect = 0;
		listEditPageSelect = 0;
		actionSelect = ICON_034_ListView;
		
	}
	//czn_20190525_s
	if(listSelect == IPC_LIST||listSelect == WlanIPC_LIST)
	{
		//API_OsdUnicodeStringDisplay(GetIconXY(ICON_038_ListCheck).x+64, GetIconXY(ICON_038_ListCheck).y+14, DISPLAY_LIST_COLOR,MESG_TEXT_ICON_ListSync, 1, 220-(GetIconXY(ICON_038_ListCheck).x+64));
	}
	else
	{
		API_OsdUnicodeStringDisplay(GetIconXY(ICON_038_ListCheck).x+64, GetIconXY(ICON_038_ListCheck).y+14, DISPLAY_LIST_COLOR,MESG_TEXT_ICON_ListCheck, 1, 220-(GetIconXY(ICON_038_ListCheck).x+64));
	}
	//czn_20190525_e
	listDeleteConfirm = -1;

	ListMaxNumUpdate();

	API_MenuIconDisplaySelectOn(actionSelect);
	
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, actionSelect, 1, 0);

	if(actionSelect == ICON_038_ListCheck)
	{
		DisplayOnePageOnlineList(listEditPageSelect);
	}
	else if(actionSelect == ICON_035_ListAdd)
	{
		DisplayOnePageAddList(listEditPageSelect);
	}
	else
	{
		DisplayOnePageList(listEditPageSelect);
	}
}

void MENU_036_ListEdit_Exit(void)
{
}

void MENU_036_ListEdit_Process(void* arg)
{
	extern IPC_RECORD saveIPCMonList;
	extern IPC_RECORD saveWlanIPCMonList;
	GetIpRspData getIpdata;
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char sync_ds_rm_nrb[11];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					if(actionSelect == ICON_038_ListCheck)
					{
						if(listSelect == IPC_LIST)		//czn_20190525
						{
							PublicPageDownProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageListSync);
		
						}
						else
						{
							BusySpriteDisplay(1);
							PublicPageDownProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageOnlineList);
							BusySpriteDisplay(0);
						}
					}
					else if(actionSelect == ICON_035_ListAdd)
					{
						PublicPageDownProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageAddList);
					}
					#if 0
					else if(actionSelect == ICON_400_ListSync)
					{
						PublicPageDownProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageListSync);
					}
					#endif
					else
					{
						ClearDeleteConfirmFlag();
						PublicPageDownProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageList);
					}
					break;			
				case ICON_202_PageUp:
					if(actionSelect == ICON_038_ListCheck)
					{
						if(listSelect == IPC_LIST)		//czn_20190525
						{
							PublicPageUpProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageListSync);
		
						}
						else
						{
							BusySpriteDisplay(1);
							PublicPageUpProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageOnlineList);
							BusySpriteDisplay(0);
						}
					}
					else if(actionSelect == ICON_035_ListAdd)
					{
						PublicPageUpProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageAddList);
					}
					#if 0
					else if(actionSelect == ICON_400_ListSync)
					{
						PublicPageUpProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageListSync);
					}
					#endif
					else
					{
						ClearDeleteConfirmFlag();
						PublicPageUpProcess(&listEditPageSelect, listEditIconMax, listEditMaxNum, (DispListPage)DisplayOnePageList);
					}
					break;		
					
				case ICON_034_ListView:
					if(actionSelect != ICON_034_ListView)
					{
						ActionSelect(ICON_034_ListView);
					}
					break;

				case ICON_035_ListAdd:
					if(actionSelect != ICON_035_ListAdd)
					{
						ActionSelect(ICON_035_ListAdd);
					}
					break;

				case ICON_036_ListDelete:
					if(actionSelect != ICON_036_ListDelete)
					{
						ActionSelect(ICON_036_ListDelete);
					}
					break;

				case ICON_037_ListEdit:
					if(actionSelect != ICON_037_ListEdit)
					{
						ActionSelect(ICON_037_ListEdit);
					}
					break;
					
				case ICON_038_ListCheck:
					if(actionSelect != ICON_038_ListCheck)
					{
						ActionSelect(ICON_038_ListCheck);
					}
					break;
					#if 0	//czn_20190525
				case ICON_400_ListSync:
					if(actionSelect != ICON_400_ListSync)
					{
						ActionSelect(ICON_400_ListSync);
					}
					break;
					#endif

					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					listEditIconSelect = GetCurIcon() - ICON_007_PublicList1;

					listEditIndex = listEditPageSelect*listEditIconMax+listEditIconSelect;
					
					if(listEditIndex >= listEditMaxNum)
					{
						return;
					}

					
					switch(listSelect)
					{
						case IM_LIST:
							switch(actionSelect)
							{
								case ICON_034_ListView:
									StartNamelistCall(listEditIndex);
									break;
								case ICON_035_ListAdd:
									if(listEditIndex == 0)
									{
										int updateCnt, x, y;
										char display[100];
										
										BusySpriteDisplay(1);
										updateCnt = AddImNamelist_TableBySearching();
										
										//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
										//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
										x = pos.x;
										y = pos.y+(hv.v - pos.y)/2;
										sprintf(display, "IM list update %d items    ", updateCnt);
										API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
										BEEP_CONFIRM();
										BusySpriteDisplay(0);
										ListMaxNumUpdate();
									}
									else if(listEditIndex == 1)
									{
										char initChar[11];
										sprintf(initChar, "%s%s", GetSysVerInfo_bd(), GetSysVerInfo_rm());
										EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_279_RM_ADDR, nameListRecord.BD_RM_MS, 8, COLOR_WHITE, initChar, 1, ConfirmAddOneNamelist);
									}
									break;
								
								case ICON_036_ListDelete:
									DeleteOneRecordProcess((DeleteOneRecord)DeleteOneImNamelist_Record, listEditIndex);
									break;
									
								case ICON_037_ListEdit:
									GetImNameListRecordItems(listEditIndex,  &nameListRecord);
									EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, nameListRecord.R_Name, 20, COLOR_WHITE, strcmp(nameListRecord.R_Name, "-") ? nameListRecord.R_Name : nameListRecord.name1, 1, ConfirmRenameNamelist);
									break;
								case ICON_038_ListCheck:
									
									break;
							}
							break;
						case DS_LIST:
							switch(actionSelect)
							{
								case ICON_034_ListView:
									StartDSMonitor(listEditIndex);
									break;
									
								case ICON_035_ListAdd:
									if(listEditIndex == 0)
									{
										int updateCnt, x, y;
										char display[100];
										
								
										//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
										//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
										x = pos.x;
										y = pos.y+(hv.v - pos.y)/2;

										BusySpriteDisplay(1);
										
										updateCnt = AddMonRes_TableBySearching();
										sprintf(display, "DS list update %d items    ", updateCnt);
										API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
										ListMaxNumUpdate();
										BEEP_CONFIRM();
										BusySpriteDisplay(0);
								
									}
									else if(listEditIndex == 1)
									{
										char initChar[11];
										sprintf(initChar, "%s%s51", GetSysVerInfo_bd(), GetSysVerInfo_rm());
										EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_279_RM_ADDR, listEditInput, 10, COLOR_WHITE, initChar, 1, ConfirmAddOneDslist);
									}
									break;
								
								case ICON_036_ListDelete:
									DeleteOneRecordProcess((DeleteOneRecord)DeleteOneMonRes_Record, listEditIndex);
									break;
								
								case ICON_037_ListEdit:
									Get_MonRes_Record(listEditIndex, listEditInput,NULL, NULL);
									EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, listEditInput, 20, COLOR_WHITE, listEditInput, 1, ConfirmModifyOneMonResName);
									break;
							}
							break;
							
						case MS_LIST:
							switch(actionSelect)
							{
								case ICON_034_ListView:
									StartSlaveCall(listEditIndex);
									break;
								case ICON_035_ListAdd:
									if(listEditIndex == 0)
									{
										int updateCnt, x, y;
										char display[100];
										
										BusySpriteDisplay(1);
										updateCnt = AddMSlist_TableBySearching();
										
										//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
										//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
										x = pos.x;
										y = pos.y+(hv.v - pos.y)/2;
										sprintf(display, "MS list update %d items    ", updateCnt);
										API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
										BEEP_CONFIRM();
										BusySpriteDisplay(0);
										ListMaxNumUpdate();
									}
									else if(listEditIndex == 1)
									{
										char initChar[3] = {"02"};
										EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_280_MS_Number, listEditInput, 2, COLOR_WHITE, initChar, 1, ConfirmAddOneMSlist);
									}
									break;
								
								case ICON_036_ListDelete:
									DeleteOneRecordProcess((DeleteOneRecord)DeleteOneMSlist_Record, listEditIndex);
									break;
									
								case ICON_037_ListEdit:
									Get_MS_Record(listEditIndex, listEditInput, NULL);
									EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, listEditInput, 20, COLOR_WHITE, listEditInput, 1, ConfirmRenameMSlist);
									break;
							}
							break;
						
						case IPC_LIST:
							switch(actionSelect)
							{
								case ICON_034_ListView:
									//StartWlanIPCMonitor(listEditIndex);
									break;
									
								case ICON_035_ListAdd:
									if(listEditIndex == 0)
									{
										#if 0
										int updateCnt, x, y;
										char display[100];
										char update_num[10];
										int len1,len2;
								
										//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
										//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
										x = pos.x;
										y = pos.y+(hv.v - pos.y)/2;

										BusySpriteDisplay(1);
										
										updateCnt = AddIPC_TableBySearching("admin", "admin", 1);
											//sprintf(display, "IPC list update %d items    ", updateCnt);
										sprintf(update_num, " %d ", updateCnt);		//FOR_INDEXA
								
										API_GetOSD_StringWithID(MESG_TEXT_CusIPCListUpdate, NULL, 0, update_num, strlen(update_num),display, &len1);
										API_GetOSD_StringWithID(MESG_TEXT_CusIPCListItems, NULL, 0,"        ", strlen("        "),display+len1, &len2);
								
	
										API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, len1+len2, 1,STR_UNICODE, bkgd_w-x);
										
										BEEP_CONFIRM();
										BusySpriteDisplay(0);
										
										ListMaxNumUpdate();
										API_DisableOsdUpdate();
										DisplayOnePageAddList(listEditPageSelect);
										API_EnableOsdUpdate();
										#endif
										StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
									}
									else if(listEditIndex == 1)
									{
										extern IPC_ONE_DEVICE ipcRecord;

										//strcpy(ipcRecord.IP, "http://192.168.243.101/onvif/device_service");
										strcpy(ipcRecord.IP, "192.168.243.101");
										strcpy(ipcRecord.USER, "admin");
										strcpy(ipcRecord.PWD, "admin");
										StartInitOneMenu(MENU_055_IPC_LOGIN,0,1);
									}
									else
									{
										extern IPC_ONE_DEVICE ipcRecord;
										GetIpcCacheRecord(listEditIndex-2,&ipcRecord);
										StartInitOneMenu(MENU_055_IPC_LOGIN,0,1);
									}
									break;
								
								case ICON_036_ListDelete:
									DeleteOneRecordProcess((DeleteOneRecord)DeleteOneIpcRecord, listEditIndex);
									break;
								
								case ICON_037_ListEdit:
									//Get_IPC_MonRes_Record(listEditIndex,&saveIPCMonList);
									//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, listEditInput, 20, COLOR_WHITE, saveIPCMonList.name, 1, ConfirmModifyOneIPCName);
									break;
								//case ICON_400_ListSync:
								case ICON_038_ListCheck:		//czn_20190525
									if(listEditIndex == 0)
									{
										BusySpriteDisplay(1);
										//SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
										if(atoi(GetSysVerInfo_ms()) == 1)	//主分机目标为主机
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,"000001");
										}
										else	//从分机目标为Master
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,GetSysVerInfo_rm());
											strcat(sync_ds_rm_nrb,"01");
										}
										int ret = Api_ResSyncToDevice(8004,sync_ds_rm_nrb);
										if(ret==0)
										{
											printf("!!!!!!!Api_ResSyncToDevice ok\n");
											BEEP_CONFIRM();
										}
										else
										{
											BEEP_ERROR();
											printf("!!!!!!!Api_ResSyncToDevice fail code %d\n", ret);
										}
										BusySpriteDisplay(0);
									}
									else if(listEditIndex == 1)
									{
										BusySpriteDisplay(1);
										if(atoi(GetSysVerInfo_ms()) == 1)	//主分机目标为主机
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,"000001");
										}
										else	//从分机目标为Master
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,GetSysVerInfo_rm());
											strcat(sync_ds_rm_nrb,"01");
										}
										printf("!!!!!!!Api_ResSyncFromServer %s\n", sync_ds_rm_nrb);
										int ret = Api_ResSyncFromServer(8004,sync_ds_rm_nrb);
										if(ret==0)
										{
											printf("!!!!!!!Api_ResSyncFromServer ok\n");
											BEEP_CONFIRM();
										}
										else
										{
											BEEP_ERROR();
											printf("!!!!!!!Api_ResSyncFromServer fail code %d\n", ret);
										}
										BusySpriteDisplay(0);
									}
									break;
							}
							break;
						case WlanIPC_LIST:
							switch(actionSelect)
							{
								case ICON_034_ListView:
									//StartWlanIPCMonitor(listEditIndex);
									break;
									
								case ICON_035_ListAdd:
									if(listEditIndex == 0)
									{
										#if 0
										int updateCnt, x, y;
										char display[100];
										char update_num[10];
										int len1,len2;
								
										//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
										//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
										x = pos.x;
										y = pos.y+(hv.v - pos.y)/2;
						
										BusySpriteDisplay(1);
										
										updateCnt = AddWlanIPC_TableBySearching("admin", "admin", 1);
											//sprintf(display, "IPC list update %d items	", updateCnt);
										sprintf(update_num, " %d ", updateCnt); 	//FOR_INDEXA
								
										API_GetOSD_StringWithID(MESG_TEXT_CusIPCListUpdate, NULL, 0, update_num, strlen(update_num),display, &len1);
										API_GetOSD_StringWithID(MESG_TEXT_CusIPCListItems, NULL, 0,"        ", strlen("        "),display+len1, &len2);
								
						
										API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, len1+len2, 1,STR_UNICODE, bkgd_w-x);
										
										BEEP_CONFIRM();
										BusySpriteDisplay(0);
										
										ListMaxNumUpdate();
										API_DisableOsdUpdate();
										DisplayOnePageAddList(listEditPageSelect);
										API_EnableOsdUpdate();
										#endif
										StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
									}
									else if(listEditIndex == 1)
									{
										extern IPC_ONE_DEVICE ipcRecord;

										//strcpy(ipcRecord.IP, "http://192.168.243.101/onvif/device_service");
										strcpy(ipcRecord.IP, "192.168.243.101");
										strcpy(ipcRecord.USER, "admin");
										strcpy(ipcRecord.PWD, "admin");
										StartInitOneMenu(MENU_055_IPC_LOGIN,0,1);
									}
									else
									{
										extern IPC_ONE_DEVICE ipcRecord;
										GetWlanIpcRecord(listEditIndex-2,&ipcRecord);
										StartInitOneMenu(MENU_055_IPC_LOGIN,0,1);
									}
									break;
								
								case ICON_036_ListDelete:
									DeleteOneRecordProcess((DeleteOneRecord)DeleteOneWlanIpcRecord, listEditIndex);
									break;
								
								case ICON_037_ListEdit:
									//Get_WlanIPC_MonRes_Record(listEditIndex,&saveWlanIPCMonList);
									//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_InputName, listEditInput, 20, COLOR_WHITE, saveWlanIPCMonList.name, 1, ConfirmModifyOneWlanIPCName);
									break;
								//case ICON_400_ListSync:
								case ICON_038_ListCheck:		//czn_20190525
									if(listEditIndex == 0)
									{
										BusySpriteDisplay(1);
										//SaveResFileFromActFile(8004,GetSysVerInfo_BdRmMs(),0);
										if(atoi(GetSysVerInfo_ms()) == 1)	//主分机目标为主机
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,"000001");
										}
										else	//从分机目标为Master
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,GetSysVerInfo_rm());
											strcat(sync_ds_rm_nrb,"01");
										}
										int ret = Api_ResSyncToDevice(8004,sync_ds_rm_nrb);
										if(ret==0)
										{
											printf("!!!!!!!Api_ResSyncToDevice ok\n");
											BEEP_CONFIRM();
										}
										else
										{
											BEEP_ERROR();
											printf("!!!!!!!Api_ResSyncToDevice fail code %d\n", ret);
										}
										BusySpriteDisplay(0);
									}
									else if(listEditIndex == 1)
									{
										BusySpriteDisplay(1);
										if(atoi(GetSysVerInfo_ms()) == 1)	//主分机目标为主机
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,"000001");
										}
										else	//从分机目标为Master
										{
											strcpy(sync_ds_rm_nrb,GetSysVerInfo_bd());
											strcat(sync_ds_rm_nrb,GetSysVerInfo_rm());
											strcat(sync_ds_rm_nrb,"01");
										}
										//printf("!!!!!!!Api_ResSyncFromServer %s\n", sync_ds_rm_nrb);
										int ret = Api_ResSyncFromServer(8004,sync_ds_rm_nrb);
										if(ret==0)
										{
											printf("!!!!!!!Api_ResSyncFromServer ok\n");
											BEEP_CONFIRM();
										}
										else
										{
											BEEP_ERROR();
											printf("!!!!!!!Api_ResSyncFromServer fail code %d\n", ret);
										}
										BusySpriteDisplay(0);
									}
									break;
							}
							break;
					}

					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




