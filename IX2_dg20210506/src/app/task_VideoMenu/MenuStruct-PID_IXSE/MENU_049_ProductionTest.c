#include "MENU_public.h"

#define PRODUCTION_TEST_DIR			"/mnt/sdcard/IX471_ProductionTest/"
#define PRODUCTION_TEST_FILE		"/mnt/sdcard/IX471_ProductionTest/temp.txt"

void MENU_049_ProductionTest_Init(int uMenuCnt)
{
	Power_Down_Timer_Stop();
}

void MENU_049_ProductionTest_Exit(void)
{
	AutoPowerOffReset();
}

void MENU_049_ProductionTest_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int y;
	
	Power_Down_Timer_Stop();
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_254_ProductionTestCancel:
					StartInitOneMenu(MENU_001_MAIN,0,1);
					break;
				case ICON_255_ProductionTestConfirm:
					API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
					UpdateProductionTestFile();
					StartInitOneMenu(MENU_001_MAIN,0,1);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{

		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		
	}
}

int Judge_ProductionTestSdCardLink(void)
{
	DIR *dir;
	
    	dir = opendir(PRODUCTION_TEST_DIR);

	if(dir != NULL)
	{
		closedir(dir);
		return 1;
	}
	
	return 0;
}

int IfUpdateProductionTestFile(void)
{	
	FILE 	*file = NULL;
	char 	buff[READ_BUFF_LEN+1] = {0};
	SYS_VER_INFO_T sysInfo;
	int ret = 0;
	
	if( (file=fopen(PRODUCTION_TEST_FILE,"r")) == NULL )
	{
		return ret;
	}

	sysInfo = GetSysVerInfo();
	
	for(memset(buff, 0, READ_BUFF_LEN+1); fgets(buff,READ_BUFF_LEN,file) != NULL; memset(buff, 0, READ_BUFF_LEN+1))
	{
		if(strstr( buff, sysInfo.sn) != NULL)
		{
			ret = 1;
			break;
		}
	}
	
	fclose(file);
	
	snprintf(buff, READ_BUFF_LEN, "rm %s", PRODUCTION_TEST_FILE);
	system(buff);
	sync();

	return ret;
}

int UpdateProductionTestFile(void)
{
	int reboot;
	reboot = ProductionTestUpdate();
	
	ReloadIoDataTable();
	
	//ReloadDeviceRegister();
	load_menu_unicode_resource(0);
	ReloadSipConfigFile();
	
	if(reboot)
	{
		system("reboot");
	}
	
	return 1;
}


