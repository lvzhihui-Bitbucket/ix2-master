#include "MENU_094_ResView.h"

//#define	ResViewIconMax	5
int ResViewIconMax;
one_vtk_table* resViewList = NULL;
int ResViewPageSelect;
int ResViewId;

void EnterResViewMenu(int id)
{
	resViewList = load_vtk_table_file(GetResPathById(id));
	if(resViewList == NULL)
		return;
	ResViewId = id;
	StartInitOneMenu(MENU_094_ResView, 0, 1); 	
}

static void DisplayResViewPageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	one_vtk_dat *pRecord;
	char buf[200];
	POS pos;
	SIZE hv;

	//API_DisableOsdUpdate();
	for(i = 0; i < ResViewIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;


		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		index = page*ResViewIconMax+i;
		if(index < resViewList->record_cnt)
		{
			pRecord = get_one_vtk_record_without_keyvalue(resViewList, index);
			if(pRecord != NULL)
			{
				memcpy(buf, pRecord->pdat, pRecord->len);
				buf[pRecord->len] = 0;
				//printf("record[%d].dat=%s,len=%d\n", index, buf,pRecord->len);
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, buf, pRecord->len, 1, STR_UTF8, hv.h-x);			
			}
			/*
			memcpy(buf,resViewList->precord[index].pdat,resViewList->precord[index].len);
			buf[resViewList->precord[index].len] = 0;
			printf("record[%d].dat=%s,len=%d\n", index, buf,resViewList->precord[index].len);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, buf, resViewList->precord[index].len, 0, STR_UTF8, bkgd_w-x);			
			*/
		}
	}
	pageNum = resViewList->record_cnt/ResViewIconMax + (resViewList->record_cnt%ResViewIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	//API_EnableOsdUpdate();
	
}
void MENU_094_ResView_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	char display[30];
	sprintf(display,"R%04d",ResViewId);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, display, strlen(display),1, STR_UTF8, 0);
	ResViewPageSelect = 0;
	ResViewIconMax = GetListIconNum();
	API_DisableOsdUpdate();
	DisplayResViewPageIcon(ResViewPageSelect);
	API_EnableOsdUpdate();
}

void MENU_094_ResView_Exit(void)
{
	release_one_vtk_tabl(resViewList);
}

void MENU_094_ResView_Process(void* arg)
{
	int i;
	int curIcon,CurSelectIndex;
	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&ResViewPageSelect, ResViewIconMax, resViewList->record_cnt, (DispListPage)DisplayResViewPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ResViewPageSelect, ResViewIconMax, resViewList->record_cnt, (DispListPage)DisplayResViewPageIcon);
					break;		
					
			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


