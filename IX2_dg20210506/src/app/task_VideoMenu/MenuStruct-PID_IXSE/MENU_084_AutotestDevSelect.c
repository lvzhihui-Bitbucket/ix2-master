#include "MENU_084_AutotestDevSelect.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "obj_DeviceRegisterTable.h"


#define	AutotestDevSelect_ICON_MAX		5
int AutotestDevSelectIconSelect;
int AutotestDevSelectPageSelect;
int AutotestDevSelectIndex;

SearchIpRspData searchAutotestDevData;
int AutotestDevNum;

// zfz_20190617
void AutotestDev_Search(void)
{
	SearchIpRspData searchAutotestDevDataTemp;
	//BusySpriteDisplay(1);
	if(get_AutotestDevSelect_type() == 0)
	{
		API_SearchIpByFilter("9999", TYPE_OS, SearchDeviceRecommendedWaitingTime, 30, &searchAutotestDevDataTemp);
		API_SearchIpByFilter("9999", TYPE_DS, SearchDeviceRecommendedWaitingTime, 30, &searchAutotestDevData);
		memcpy(&searchAutotestDevData.data[searchAutotestDevData.deviceCnt],searchAutotestDevDataTemp.data,searchAutotestDevDataTemp.deviceCnt*sizeof(SearchOneDeviceData));
		AutotestDevNum = searchAutotestDevData.deviceCnt+searchAutotestDevDataTemp.deviceCnt;
	}
	else
	{
		API_SearchIpByFilter("9999", TYPE_IM, SearchDeviceRecommendedWaitingTime, 30, &searchAutotestDevData);
		AutotestDevNum = searchAutotestDevData.deviceCnt+2;
	}
	//BusySpriteDisplay(0);
}


int get_AutotestDevSelect_MenuTitleText(void)
{
	if(get_AutotestDevSelect_type() == 0)
	{
		return MESG_TEXT_ICON_AutotestSource;
	}
	else
	{
		return MESG_TEXT_ICON_AutotestTarget;
	}
}

static void DisplayAutotestDevSelectPageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;

	if(AutotestDevNum == 0)
	{
		return;
	}
	
	for(i = 0; i < AutotestDevSelect_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		index = page*AutotestDevSelect_ICON_MAX+i;
		
		if(index < AutotestDevNum)
		{
			char display[200];
			char room[15] = {0};
			
			if(get_AutotestDevSelect_type() == 1)
			{
				if(index == 0)
				{
					//strcpy(display,"Self");
					//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);	
					API_OsdUnicodeStringDisplay(x,y,DISPLAY_LIST_COLOR,MESG_TEXT_CusAutoTestDevSelf,0,hv.h-x);		//FOR_INDEXA
					continue;
				}
				else if(index == 1)
				{
					strcpy(display,"By R8001");
					API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);	
					continue;
				}
				else
				{
					index-=2;
				}
			}
			
			get_device_addr_and_name_disp_str(0, searchAutotestDevData.data[index].BD_RM_MS, NULL, NULL, searchAutotestDevData.data[index].name, display);
			
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);			
		}
	}
	pageNum = AutotestDevNum/AutotestDevSelect_ICON_MAX + (AutotestDevNum%AutotestDevSelect_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}
 



void MENU_084_AutotestDevSelect_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	//API_MenuIconDisplaySelectOn(ICON_031_OnsiteTools);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, get_AutotestDevSelect_MenuTitleText(), 1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	

	AutotestDevSelectIconSelect = 0;
	AutotestDevSelectPageSelect = 0;
	
	BusySpriteDisplay(1);
	AutotestDev_Search();
	BusySpriteDisplay(0);
	
	
	DisplayAutotestDevSelectPageIcon(AutotestDevSelectPageSelect);
}

void MENU_084_AutotestDevSelect_Exit(void)
{

}

void MENU_084_AutotestDevSelect_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&AutotestDevSelectPageSelect, AutotestDevSelect_ICON_MAX, AutotestDevNum, (DispListPage)DisplayAutotestDevSelectPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&AutotestDevSelectPageSelect, AutotestDevSelect_ICON_MAX, AutotestDevNum, (DispListPage)DisplayAutotestDevSelectPageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:

					AutotestDevSelectIconSelect = GetCurIcon() - ICON_007_PublicList1;
					AutotestDevSelectIndex = AutotestDevSelectPageSelect*AutotestDevSelect_ICON_MAX+AutotestDevSelectIconSelect;
					
					if(AutotestDevSelectIndex < AutotestDevNum)
					{
						for(i = 0; i < AutotestDevSelect_ICON_MAX; i++)
						{
							ListSelect(i, 0);		
							//if(publicSettingPage*PublicSetting_ICON_MAX+i < publicSettingMaxNum)
							{
								if(AutotestDevSelectPageSelect*AutotestDevSelect_ICON_MAX+i == AutotestDevSelectIndex)
								{
									ListSelect(i, 1);
								}
							}
						}
						char room[15] = {0};
						char temp[200] = {0};
						if(get_AutotestDevSelect_type() == 1)
						{
							if(AutotestDevSelectIndex==0)
							{
								strcpy(temp,"Self");
								strcpy(room,GetSysVerInfo_BdRmMs());
							}
							else if(AutotestDevSelectIndex==1)
							{
								strcpy(temp,"By R8001");
								strcpy(room,"By R8001");
							}
							else
							{
								get_device_addr_and_name_disp_str(0, searchAutotestDevData.data[AutotestDevSelectIndex-2].BD_RM_MS, NULL, NULL, searchAutotestDevData.data[AutotestDevSelectIndex-2].name, temp);
								strcpy(room,searchAutotestDevData.data[AutotestDevSelectIndex-2].BD_RM_MS);
							}
						}
						else
						{
							get_device_addr_and_name_disp_str(0, searchAutotestDevData.data[AutotestDevSelectIndex].BD_RM_MS, NULL, NULL, searchAutotestDevData.data[AutotestDevSelectIndex].name, temp);
							strcpy(room,searchAutotestDevData.data[AutotestDevSelectIndex].BD_RM_MS);
						}
						AutotestDev_Select_Update(room,temp);
						usleep(200*1000);
						popDisplayLastMenu();
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




