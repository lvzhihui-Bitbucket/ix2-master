#include "MENU_public.h"
#include "MENU_105_BackupList.h"
#include "obj_BackupAndRestore.h"

#define	BACKUP_LIST_ICON_MAX		6

static int backupListIconSelect;
static int backupListConfirm;
static int backupListConfirmSelect;
static int backupListIconNum;


static void MenuListGetBackupListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen, unicode_len;
	char unicode_buf[200];
	char display[80] = {0};
	int stringId;
	
	pDisp->dispCnt = 0;
	
	backupListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < backupListIconNum)
		{

			switch(index)
			{
				case 0:
					API_GetOSD_StringWithID(MESG_TEXT_BackupInstallerBak1, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
				
				case 1:
					API_GetOSD_StringWithID(MESG_TEXT_DeleteInstallerBak1, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
					break;
			}
			
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;			
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->dispCnt++;
		}
	}

}


void MENU_105_backupList_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	backupListIconSelect = 0;
	backupListConfirm = 0;
	backupListConfirmSelect = 0;
	backupListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);
	API_GetOSD_StringWithID(MESG_TEXT_ICON_Backup, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
	
	listInit = ListPropertyDefault();
	
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = backupListIconNum;
	listInit.fun = MenuListGetBackupListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.currentPage = 0;

	InitMenuList(listInit);
}

void MENU_105_backupList_Exit(void)
{
	MenuListDisable(0);
}

void MENU_105_backupList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					ClearConfirm(&backupListConfirm, &backupListConfirmSelect, BACKUP_LIST_ICON_MAX);
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon > MENU_LIST_ICON_NONE)
					{
						backupListIconSelect = listIcon.mainIcon % BACKUP_LIST_ICON_MAX;
						
						if(ConfirmSelect(&backupListConfirm, &backupListConfirmSelect, &backupListIconSelect, 0, BACKUP_LIST_ICON_MAX))
						{
							return;
						}
						ClearConfirm(&backupListConfirm, &backupListConfirmSelect, BACKUP_LIST_ICON_MAX);
						switch(listIcon.mainIcon)
						{
							case 0:
								BusySpriteDisplay(1);
								if(BackupInstallerBak(1) == 0)
								{
									backupListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);
									SetMenuListItemNum(backupListIconNum);
									BEEP_CONFIRM();
								}
								else
								{
									BEEP_ERROR();
								}
								BusySpriteDisplay(0);
								break;
							case 1:
								DeleteInstallerBak(1);
								backupListIconNum = 1 + (IsInstallerBakExist(1) ? 1 : 0);
								SetMenuListItemNum(backupListIconNum);
								BEEP_CONFIRM();
								break;
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


