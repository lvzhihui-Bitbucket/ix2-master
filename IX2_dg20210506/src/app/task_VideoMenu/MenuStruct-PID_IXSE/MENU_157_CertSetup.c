#include "MENU_public.h"
#include "cJSON.h"
#include "obj_PublicInformation.h"

#define	CertSetup_ICON_MAX		5
int CertSetupIconSelect;
int CertSetupPageSelect;
int CertSetupConfirm;
int CertSetupConfirmSelect;

const IconAndText_t CertSetupSet1[] = 
{
	{ICON_CertCreate,					MESG_TEXT_CertCreate},
	//{ICON_CertView,						MESG_TEXT_CertView},
	//{ICON_CertLog,						MESG_TEXT_CertLog},

};
const unsigned char CertSetupSetNum1 = sizeof(CertSetupSet1)/sizeof(CertSetupSet1[0]);

const IconAndText_t CertSetupSet2[] = 
{
	{ICON_CertDelete,					MESG_TEXT_CertDelete},
	{ICON_CertCheck,					MESG_TEXT_CertCheck},
	{ICON_CertView,						MESG_TEXT_CertView},
	{ICON_CertLog,						MESG_TEXT_CertLog},

};
const unsigned char CertSetupSetNum2 = sizeof(CertSetupSet2)/sizeof(CertSetupSet2[0]);

IconAndText_t *CertSetupSet = NULL;
int CertSetupSetNum;

static void DisplayCertSetupPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
	char display[20] = {0};	
	for(i = 0; i < CertSetup_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*CertSetup_ICON_MAX+i < CertSetupSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, CertSetupSet[i+page*CertSetup_ICON_MAX].iConText, 1, val_x - x);
			
		}
	}
	pageNum = CertSetupSetNum/CertSetup_ICON_MAX + (CertSetupSetNum%CertSetup_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
} 

void MENU_157_CertSetup_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	//API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_CertSetup,2, 0);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_CertSetup, 1, 0);
	CertSetupIconSelect = 0;
	CertSetupPageSelect = 0;
	CertSetupConfirm = 0;
	CertSetupConfirmSelect = 0;
	char* pbCertState = API_PublicInfo_Read_String(PB_CERT_STATE);
	if(pbCertState && !strcmp(pbCertState, "Delivered"))
	{
		CertSetupSet = CertSetupSet2;
		CertSetupSetNum = CertSetupSetNum2;
	}
	else
	{
		CertSetupSet = CertSetupSet1;
		CertSetupSetNum = CertSetupSetNum1;
	}
	DisplayCertSetupPageIcon(0);
}

void MENU_157_CertSetup_Exit(void)
{

}


void MENU_157_CertSetup_Process(void* arg)
{	
	POS pos;
	SIZE hv;
	char disp[100];
	char* pbCertState;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					CertSetupIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(CertSetupPageSelect*CertSetup_ICON_MAX+CertSetupIconSelect >= CertSetupSetNum)
					{
						return;
					}

					switch(CertSetupSet[CertSetupPageSelect*CertSetup_ICON_MAX+CertSetupIconSelect].iCon)
					{
						case ICON_CertCreate:
							pbCertState = API_PublicInfo_Read_String(PB_CERT_STATE);
							if(pbCertState && !strcmp(pbCertState, "Delivered"))
							{
								sprintf(disp, "CERT Exist!");
								API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, 0);
								return;
							}
							printf("1.API_Create_DCFG_SELF!!!\n");
							if(API_Create_DCFG_SELF("CERT_ETH0", NULL))
							{
								printf("2.API_Create_CERT!!!\n");
								if(API_Create_CERT(CERT_DCFG_SELF_NAME))
								{
									printf("3.API_Go_Live!!!\n");
									if(API_Go_Live()==1)
									{
										sprintf(disp, "API_Go_Live ok");
									}
									else
									{
										sprintf(disp, "API_Go_Live err");
									}
								}
								else
								{
									sprintf(disp, "API_Create_CERT err");
								}
							}
							else
							{
								sprintf(disp, "API_Create_DCFG_SELF err");
							}
							API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, 0);
							break;
						case ICON_CertView:
							ClearConfirm(&CertSetupConfirm, &CertSetupConfirmSelect, CertSetup_ICON_MAX);
							EnterViewFileMenu(UserDataFolder, "cert.json");
							break;
						case ICON_CertLog:
							ClearConfirm(&CertSetupConfirm, &CertSetupConfirmSelect, CertSetup_ICON_MAX);
							EnterViewFileMenu(LOG_DIR, CERT_LOG_FileName);
							break;
						case ICON_CertCheck:
							ClearConfirm(&CertSetupConfirm, &CertSetupConfirmSelect, CertSetup_ICON_MAX);
							if(API_Check_CERT()==1)
							{
								sprintf(disp, "API_Check_CERT ok");
							}
							else
							{
								sprintf(disp, "API_Check_CERT err");
							}
							API_OsdStringDisplayExt(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, 0);
							break;
						case ICON_CertDelete:	
							if(ConfirmSelect(&CertSetupConfirm, &CertSetupConfirmSelect, &CertSetupIconSelect, CertSetupPageSelect, CertSetup_ICON_MAX))
							{
								return;
							}
							API_Delete_CERT();
							popDisplayLastMenu();
							break;
					}
						
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


