#include "MENU_public.h"

#define	OnePara_ICON_MAX	5
int OneParaIconSelect;
int OneParaPageSelect;
int OneParaIndex;

ParaMenu_T OnePararecord;
ComboBoxSelect_T OneParaComboBox;

const IconAndText_t OneParaTable[] = 
{
	{ICON_ParaId,					MESG_TEXT_ICON_ParaId},    
	{ICON_ParaName,					MESG_TEXT_ICON_ParaName},
	{ICON_ParaDesc, 				MESG_TEXT_ICON_ParaDesc},
	{0, 							0},
	{ICON_ParaValue, 				MESG_TEXT_ICON_ParaValue},
};

const int OneParaIconNum = sizeof(OneParaTable)/sizeof(OneParaTable[0]);

int SetParaValue(const char* pData)
{
	dprintf("SetParaValue =%s\n", pData);
	int temp[4];
	if(API_Event_IoServer_Inner_judgeRange(OnePararecord.paraId, pData) != 0)
	{
		return 0;
	}
	if(strstr(pData,"<")!=NULL||strstr(pData,">")!=NULL||strstr(pData,",")!=NULL)
	{
		return 0;
	}
	if(strcmp(OnePararecord.paraId,IP_Address)==0||strcmp(OnePararecord.paraId,SubnetMask)==0||strcmp(OnePararecord.paraId,DefaultRoute)==0||strcmp(OnePararecord.paraId,Autoip_IPSegment)==0||strcmp(OnePararecord.paraId,Autoip_IPMask)==0||strcmp(OnePararecord.paraId,Autoip_IPGateway)==0)
	{
		if(sscanf(pData, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
		{
			return 0;
		}
		if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
		{
			return 0;
		}
	}
	strcpy(OnePararecord.value, pData);
	
	API_Event_IoServer_InnerWrite_All(OnePararecord.paraId, OnePararecord.value);

	#if 0
	if(!strcmp(OnePararecord.paraId,RING_VOLUME_R_CHANNEL))
	{
		RingVolumeRSet(atoi(OnePararecord.value));
	}
	if(!strcmp(OnePararecord.paraId,RING_VOLUME_L_CHANNEL))
	{
		RingVolumeLSet(atoi(OnePararecord.value));
	}
	#endif
	if(strcmp(OnePararecord.paraId,IP_Address)==0||strcmp(OnePararecord.paraId,SubnetMask)==0||strcmp(OnePararecord.paraId,DefaultRoute)==0)
	{
		NM_LAN_IPAssignedSet(0);	
	}
	return 1;	
}

int ComboxSetParaValue(int value)
{
	
	if(API_Event_IoServer_Inner_judgeRange(OnePararecord.paraId, OneParaComboBox.data[value].value) != 0)
	{
		return 0;
	}
	
	strcpy(OnePararecord.value, OneParaComboBox.data[value].value);
	
	if(strcmp(OnePararecord.paraId,DHCP_ENABLE)==0)
		NM_LAN_IPAssignedSet(atoi(OneParaComboBox.data[value].value));
	else
		API_Event_IoServer_InnerWrite_All(OnePararecord.paraId, OneParaComboBox.data[value].value);

	if(!strcmp(OnePararecord.paraId,ZportMode))
	{
		api_set_stm8_mode();
	}
	if(!strcmp(OnePararecord.paraId,RebootIfNetErrorTimeout))
	{
		NetworkCardCheckInit();
		NetworkWLANCheckInit();
	}
	return 1;	
}

static void DisplayOneParaPageIcon(int page)
{
	int i, index;
	uint16 x, y, val_x;
	int pageNum;
	ParaMenu_T record;
	char display[100];
	char *disp, *pos1;
	ParaProperty_t property;
	extern ParaMenu_T OneRemotePararecord;
	POS pos;
	SIZE hv;
	
	for(i = 0; i < OnePara_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		index = page*OnePara_ICON_MAX+i;
		if(index < OneParaIconNum)
		{
			if(OneParaTable[index].iConText != 0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, OneParaTable[index].iConText, 1, val_x-x);
			}
			
			if(get_pane_type() == 5 )
			{
				x += 100;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(OneParaTable[index].iCon)
			{
				case ICON_ParaId:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OnePararecord.paraId, strlen(OnePararecord.paraId), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaName:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OnePararecord.name, strlen(OnePararecord.name), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaDesc:
					pos1 = strchr(OneRemotePararecord.describe, '\'');
					if(pos1 != NULL)
					{
						*pos1 = 0;
						//API_OsdStringClearExt(x, y+DISPLAY_LIST_SPACE, bkgd_w-x, 40);
						//API_OsdStringDisplayExt(x, y+DISPLAY_LIST_SPACE, DISPLAY_STATE_COLOR, pos1+1, strlen(pos1+1), 1, STR_UTF8, MENU_SCHEDULE_POS_X - x);
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, OnePararecord.describe, strlen(OnePararecord.describe), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_ParaValue:
					API_Event_IoServer_InnerRead_Property(OnePararecord.paraId, &property);
					API_Event_IoServer_InnerRead_All(OnePararecord.paraId, OnePararecord.value);
					if(property.editType)
					{
						int i;
						for(i=0, OneParaComboBox.value = 0; i<OneParaComboBox.num; i++)
						{
							if(!strcmp(OneParaComboBox.data[i].value, OnePararecord.value))
							{
								OneParaComboBox.value = i;
								break;
							}
						}
						disp = OneParaComboBox.data[OneParaComboBox.value].comboBox;
					}
					else
					{
						disp = OnePararecord.value;
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp),1, STR_UTF8, hv.h - x);
					break;
			}
		}
	}
	//pageNum = OneParaNum/OnePara_ICON_MAX + (OneParaNum%OnePara_ICON_MAX ? 1 : 0);

	//DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}
 


void MENU_072_OnePara_Init(int uMenuCnt)
{
	OneParaPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	DisplayOneParaPageIcon(OneParaPageSelect);
}

void MENU_072_OnePara_Exit(void)
{

}

void MENU_072_OnePara_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					//PublicPageDownProcess(&OneParaPageSelect, OnePara_ICON_MAX, OneParaNum, (DispListPage)DisplayOneParaPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&OneParaPageSelect, OnePara_ICON_MAX, OneParaNum, (DispListPage)DisplayOneParaPageIcon);
					break;			
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				case ICON_011_PublicList5:

					OneParaIconSelect = GetCurIcon() - ICON_007_PublicList1;
					OneParaIndex = OneParaPageSelect*OnePara_ICON_MAX+OneParaIconSelect;
					
					//if(OneParaIndex < OneParaNum)
					{
						ParaProperty_t property;
						
						API_Event_IoServer_InnerRead_Property(OnePararecord.paraId, &property);
						if(property.readWrite & 0x02)
						{
							if(!property.editType)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ParaValue, OnePararecord.value, 30, COLOR_WHITE, OnePararecord.value, 1, SetParaValue);
							}
							else
							{
								OneParaComboBox.editType = property.editType;
								EnterParaPublicMenu(0, OneParaComboBox.value, ComboxSetParaValue);
							}
						}
						else
						{
							BEEP_ERROR();
						}
						//StartInitOneMenu(MENU_072_OnePara,0,1);
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}

