#if 0
#include "MENU_public.h"
#include "task_Event.h"
#include "utility.h"
#include "obj_TlogCfg.h"

#define	TLOG_CONFIG_ICON_MAX	6

static int tlogConfigMaxNum;
static int tlogConfigPageSelect;
static int tlogConfigIconSelect;
static TLOG_LIST_S tlogList;

void DisplayOnePageTlogConfig(uint8 page)
{
	int i;
	int tlogConfigIndex;
	int maxPage;
	POS pos;
	SIZE hv;
	int x,y;

	API_DisableOsdUpdate();
	for( i = 0; i < TLOG_CONFIG_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		tlogConfigIndex = page*TLOG_CONFIG_ICON_MAX+i;
		if(tlogConfigIndex < tlogConfigMaxNum)
		{
			if(tlogList.cfg[tlogConfigIndex].enable)
			{
				API_SpriteDisplay_XY(x, y, SPRITE_Collect);
			}
			else
			{
				API_SpriteClose(x, y, SPRITE_Collect);
			}
			API_OsdStringDisplayExt(x + DISPLAY_DEVIATION_X, y, DISPLAY_LIST_COLOR, tlogList.cfg[tlogConfigIndex].name, strlen(tlogList.cfg[tlogConfigIndex].name), 1, STR_UTF8, bkgd_w - x);
		}
		else
		{
			API_SpriteClose(x, y, SPRITE_Collect);
		}
	}
	API_EnableOsdUpdate();
	
	maxPage = tlogConfigMaxNum/TLOG_CONFIG_ICON_MAX + (tlogConfigMaxNum%TLOG_CONFIG_ICON_MAX ? 1 : 0);	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_149_TlogConfig_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	tlogList = GetTlogList();

	tlogConfigMaxNum = tlogList.cnt;

	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_107_Manager || GetLastNMenu() == MENU_140_KeyConfig)
	{
		tlogConfigIconSelect = 0;
		tlogConfigPageSelect = 0;
	}

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_TlogRecordConfig,1, 0);
	DisplayOnePageTlogConfig(tlogConfigPageSelect);
}


void MENU_149_TlogConfig_Exit(void)
{

}

void MENU_149_TlogConfig_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int tlogConfigIndex;

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UP:
					PublicListUpProcess(&tlogConfigIconSelect, &tlogConfigPageSelect, TLOG_CONFIG_ICON_MAX, tlogConfigMaxNum, (DispListPage)DisplayOnePageTlogConfig);
					break;
				case KEY_DOWN:
					PublicListDownProcess(&tlogConfigIconSelect, &tlogConfigPageSelect, TLOG_CONFIG_ICON_MAX, tlogConfigMaxNum, (DispListPage)DisplayOnePageTlogConfig);
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					tlogConfigIconSelect = GetCurIcon() - ICON_007_PublicList1;
					tlogConfigIndex = tlogConfigPageSelect*TLOG_CONFIG_ICON_MAX + tlogConfigIconSelect;
					if(tlogConfigIndex < tlogConfigMaxNum)
					{
						if(tlogList.cfg[tlogConfigIndex].enable)
						{
							if(SetTlogEventEnable(tlogList.cfg[tlogConfigIndex].name, 0))
							{
								tlogList.cfg[tlogConfigIndex].enable = 0;
								DisplayOnePageTlogConfig(tlogConfigPageSelect);
							}
						}
						else
						{
							if(SetTlogEventEnable(tlogList.cfg[tlogConfigIndex].name, 1))
							{
								tlogList.cfg[tlogConfigIndex].enable = 1;
								DisplayOnePageTlogConfig(tlogConfigPageSelect);
							}
						}
					}
					break;	
				case ICON_201_PageDown:
					PublicPageDownProcess(&tlogConfigPageSelect, TLOG_CONFIG_ICON_MAX, tlogConfigMaxNum, (DispListPage)DisplayOnePageTlogConfig);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&tlogConfigPageSelect, TLOG_CONFIG_ICON_MAX, tlogConfigMaxNum, (DispListPage)DisplayOnePageTlogConfig);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

#endif
