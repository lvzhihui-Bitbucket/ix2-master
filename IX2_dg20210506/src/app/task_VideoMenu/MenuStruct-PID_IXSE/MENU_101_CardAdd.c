#include "MENU_public.h"
#include "MENU_005_CallScene.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_CallServer.h"	//czn_20181227
#include "MENU_101_CardAdd.h"
#include "MENU_100_CardList.h"


#define	ID_CARD_ADD_ICON_MAX	5
unsigned int idCardAddItem = 0;
extern int idCardtargetIp;
extern int getCardSourceIp;


IdCardInfo_s add_one_id_card;

static void get_date(char* p)
{
	char year[5] = {0};
	char mon[3] = {0};
	char day[3] = {0};
	unsigned char y;
	unsigned char mo;
	unsigned char d;
	time_t t;
	struct tm *tblock;	
	t = time(NULL); 
	tblock=localtime(&t);
	
	y = tblock->tm_year-100;
	mo = tblock->tm_mon+1;
	d = tblock->tm_mday;

	sprintf(p,"20%02d%02d%02d",y,mo,d);
}

static int ConfirmAddOneCardId(const char* input)
{
	int len;
	len = strlen(input);
	if( len == 0 || len > 10 )
	{
		return 0;
	}
	if( len < 10 )
	{
		memset(add_one_id_card.id,0,11);
		memset(add_one_id_card.id,'0',10-len);
		strcat(add_one_id_card.id,input);
	}
	else
	{
		sprintf(add_one_id_card.id,"%s",input);
	}
	return 1;
}

static int ConfirmAddOneCardName(const char* input)
{
	int len;
	len = strlen(input);
	if( len == 0 || len > 20 )
	{
		return 0;
	}
	else
	{
		sprintf(add_one_id_card.name,"%s",input);
		printf("%s\n",add_one_id_card.name);
	}
	return 1;
}

void add_one_id_card_save_process(void)
{
	unsigned short addCntRsp;
	IdCardAdd_s send_data;
	int len;
	int i;
	if( getCardSourceIp < 2 )
	{
		printf("getcardsourceip=%d\n",getCardSourceIp);
		BEEP_ERROR();
		return;
	}
	if( strlen(add_one_id_card.id) != 10 )
	{
		BEEP_ERROR();
		return;
	}

	send_data.num = 1; // add一张卡
	strcat(send_data.addCardData,"<RM>");
	strcat(send_data.addCardData,add_one_id_card.bd_rm_ms);
	strcat(send_data.addCardData,"<ID>");
	strcat(send_data.addCardData,add_one_id_card.id);
	if( strlen(add_one_id_card.name) != 0 )
	{
		strcat(send_data.addCardData,"<NA>");
		strcat(send_data.addCardData,add_one_id_card.name);
	}
	strcat(send_data.addCardData,"<DA>");
	strcat(send_data.addCardData,add_one_id_card.date);
	
	printf("add card dat:%s\n",send_data.addCardData);
	len = 2;
	if( api_udp_io_server_send_req( idCardtargetIp ,CMD_RF_CARD_ADD_REQ,(char*)&send_data,2+strlen(send_data.addCardData)+1, (char*)&addCntRsp, &len ) == -1 )
	{
		printf("No response cmd = %#X\n",CMD_RF_CARD_ADD_REQ);
		BEEP_ERROR();
		return;
	}
	printf("add card cnt:%d\n",addCntRsp);
	//printf("132211111111111111111111111!111!!!!!\n");
	if( addCntRsp == 1 )
	{
		BEEP_CONFIRM();
	}
	else
	{
		BEEP_ERROR();
		return;
	}
}

static void MenuListGetCardAddPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	int i, index;
	int recordLen, unicode_len;
	char unicode_buf[200];
	char DisplayBuffer[50];
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < 5)
		{
			if(index == 0)
			{
				API_GetOSD_StringWithID(MESG_TEXT_ICON_279_RM_ADDR, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				memcpy(add_one_id_card.bd_rm_ms,GetSysVerInfo_BdRmMs(),8);
				add_one_id_card.bd_rm_ms[8] = 0;
				unicode_len = 2*api_ascii_to_unicode(add_one_id_card.bd_rm_ms,8,unicode_buf);
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
			}
			else if(index == 1) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_CARD_ID, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				if( strlen(add_one_id_card.id) != 0 )
				{
					unicode_len = 2*api_ascii_to_unicode(add_one_id_card.id,10,unicode_buf);
					memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
				}
			}
			else if(index == 2) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_IPC_UserName, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				if( strlen(add_one_id_card.name) != 0 )
				{
					recordLen = strlen(add_one_id_card.name);
					unicode_len = 2*api_ascii_to_unicode(add_one_id_card.name,recordLen,unicode_buf);
					memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
				}
			}
			else if(index == 3) 
			{
				API_GetOSD_StringWithID(MESG_TEXT_ICON_054_Date, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

				get_date(add_one_id_card.date);
				unicode_len = 2*api_ascii_to_unicode(add_one_id_card.date,8,unicode_buf);
				memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
			}
			else
			{
				API_GetOSD_StringWithID(MESG_TEXT_IPC_Save, NULL, 0, NULL, 0, unicode_buf, &unicode_len);				
				memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
				pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

			}
			pDisp->dispCnt++;
		}

	}
}

void MENU_101_CardAdd_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	API_MenuIconDisplaySelectOn(ICON_IdCard_Add);
	API_GetOSD_StringWithIcon(ICON_IdCard_Add, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textValOffset = 120;
		listInit.textValOffsetY = 30;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textValOffset = 300;
	}	
	listInit.valEn = 1;
	listInit.listCnt = 5;
	listInit.fun = MenuListGetCardAddPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.currentPage = 0;
	InitMenuList(listInit);
}

void MENU_101_CardAdd_Exit(void)
{
	MenuListDisable(0);
	API_MenuIconDisplaySelectOff(ICON_IdCard_Add);
}

void MENU_101_CardAdd_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int index;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			char initChar[11] = {0};
			char input[11];
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					index = listIcon.mainIcon;
					if(index >= 1)	
					{
						if(index == 1)	
						{
							if( strlen(add_one_id_card.id) == 0 )
							{
								strcpy(initChar,"0001234567");
							}
							else
							{
								strcpy(initChar,add_one_id_card.id);
							}
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_279_RM_ADDR, input, 10, COLOR_WHITE, initChar, 1, ConfirmAddOneCardId);					
						}
						else if(index == 2)	
						{
							if( strlen(add_one_id_card.name) == 0 )
							{
								strcpy(initChar,"Jack");
							}
							else
							{
								strcpy(initChar,add_one_id_card.name);
							}
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_279_RM_ADDR, input, 20, COLOR_WHITE, initChar, 1, ConfirmAddOneCardName);					
						}
						else
						{
							add_one_id_card_save_process();		
						}
					}
					break;
				#if 0	
				case ICON_007_PublicList1:
				case ICON_010_PublicList4:
					idCardAddItem = GetCurIcon() - ICON_007_PublicList1;
					
					if(idCardAddItem >= ID_CARD_ADD_ICON_MAX)
					{
						return;
					}
					break;
				case ICON_008_PublicList2:
					if( strlen(add_one_id_card.id) == 0 )
					{
						strcpy(initChar,"0001234567");
					}
					else
					{
						strcpy(initChar,add_one_id_card.id);
					}
					EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_279_RM_ADDR, input, 10, COLOR_WHITE, initChar, 1, ConfirmAddOneCardId);
					break;
				case ICON_009_PublicList3:
					if( strlen(add_one_id_card.name) == 0 )
					{
						strcpy(initChar,"Jack");
					}
					else
					{
						strcpy(initChar,add_one_id_card.name);
					}
					EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_279_RM_ADDR, input, 20, COLOR_WHITE, initChar, 1, ConfirmAddOneCardName);
					break;
				case ICON_011_PublicList5:
					add_one_id_card_save_process();				
					break;
				#endif	
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		
	}
}

#if 0
void MENU_101_CardAdd_Init(int uMenuCnt)
{
	int i,length;
	uint16 x,x1,y;
	char* p;	
	SYS_VER_INFO_T sysInfo;

	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	//clear_card_info_display_zone();
	API_MenuIconDisplaySelectOn(ICON_IdCard_Add);
	
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_IdCard_Add, 1, 0);

	//memset(&add_one_id_card,0,sizeof(add_one_id_card));
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//x1 = x+ (DISPLAY_VALUE_X_DEVIATION - 150);
	//y = DISPLAY_LIST_Y+DISPLAY_DEVIATION_Y;

	 // rm
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_279_RM_ADDR, 1, (DISPLAY_VALUE_X_DEVIATION - 150));
	sysInfo = GetSysVerInfo();
	memcpy(add_one_id_card.bd_rm_ms,sysInfo.bd_rm_ms,8);
	add_one_id_card.bd_rm_ms[8] = 0;
	API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, add_one_id_card.bd_rm_ms, 8, 1, STR_UTF8, bkgd_w - x1);

	 // id
	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_CARD_ID, 1, (DISPLAY_VALUE_X_DEVIATION - 150));
	if( strlen(add_one_id_card.id) == 0 )
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, "[]", 2, 1, STR_UTF8, bkgd_w - x1);
	}
	else
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, add_one_id_card.id, 10, 1, STR_UTF8, bkgd_w - x1);
	}

	// NAME
	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_IPC_UserName, 1, (DISPLAY_VALUE_X_DEVIATION - 150)); 
	if( strlen(add_one_id_card.name) == 0 )
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, "[]", 2, 1, STR_UTF8, bkgd_w - x1);
	}
	else
	{
		API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, add_one_id_card.name, strlen(add_one_id_card.name), 1, STR_UTF8, bkgd_w - x1);
	}

	// DATE
	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_054_Date, 1, (DISPLAY_VALUE_X_DEVIATION - 150)); 
	get_date(add_one_id_card.date);
	API_OsdStringDisplayExt(x1, y, DISPLAY_STATE_COLOR, add_one_id_card.date, 8, 1, STR_UTF8, bkgd_w - x1);

	// save
	//y += DISPLAY_LIST_SPACE;
	//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_IPC_Save , 1, (DISPLAY_VALUE_X_DEVIATION - 150)); 

}
#endif
