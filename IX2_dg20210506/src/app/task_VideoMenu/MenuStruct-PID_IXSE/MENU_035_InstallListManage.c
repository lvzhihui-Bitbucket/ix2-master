#include "MENU_035_InstallListManage.h"

#define	ListManage_ICON_MAX	5
int listManageIconSelect;
int listManagePageSelect;
char listManageInput[21];

const IconAndText_t listManageSet[] = 
{
	{ICON_293_UpdateAllListBySearch,	MESG_TEXT_ICON_293_UpdateAllListBySearch},
	{ICON_294_ManageImList,				MESG_TEXT_ICON_294_ManageImList},
	{ICON_295_ManageDsList,				MESG_TEXT_ICON_295_ManageDsList},
	{ICON_296_ManageMsList,				MESG_TEXT_ICON_296_ManageMsList},
	{ICON_297_ManageIPCList,			MESG_TEXT_ICON_297_ManageIPCList},
};


const unsigned char listManageSetNum = sizeof(listManageSet)/sizeof(listManageSet[0]);

static void DisplayListManagePageIcon(int page)	//czn_20190221
{
	int i;
	uint16 x, y;
	int pageNum;
	POS pos;
	SIZE hv;
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0)
	{
		//API_DisableOsdUpdate();
		for(i = 0; i < ListManage_ICON_MAX; i++)
		{
			OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
			//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
			if(page*ListManage_ICON_MAX+i < listManageSetNum)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, listManageSet[i+page*ListManage_ICON_MAX].iConText, 1, hv.h - x);
			}
		}
		pageNum = listManageSetNum/ListManage_ICON_MAX + (listManageSetNum%ListManage_ICON_MAX ? 1 : 0);

		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
		
		//API_EnableOsdUpdate();
	}
	else
	{
		i = 0;
		
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		//if(page*ListManage_ICON_MAX+i < listManageSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_293_SyncAllListFromMaster, 1, hv.h - x);
		}
		for(i = 1; i < ListManage_ICON_MAX; i++)
		{
			OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
			//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		pageNum = 1/ListManage_ICON_MAX + (1%ListManage_ICON_MAX ? 1 : 0);

		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	}
}
 


void MENU_035_ListManage_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	API_MenuIconDisplaySelectOn(ICON_033_ListManage);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_033_ListManage, 1, 0);
	listManageIconSelect = 0;
	listManagePageSelect = 0;
	DisplayListManagePageIcon(listManagePageSelect);
}

void MENU_035_ListManage_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_033_ListManage);
}

void MENU_035_ListManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int x, y;
	int updateCnt;
	char display[100];
	char update_num[10];
	int len1,len2;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					//czn_20190221_s
					listManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
					{
						if(listManagePageSelect*ListManage_ICON_MAX+listManageIconSelect >= listManageSetNum)
						{
							return;
						}

						switch(listManageSet[listManagePageSelect*ListManage_ICON_MAX+listManageIconSelect].iCon)
						{
	 						case ICON_293_UpdateAllListBySearch:
								BusySpriteDisplay(1);
								updateCnt = AddImNamelist_TableBySearching();
								
								//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
								//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
								x = pos.x;
								y = pos.y+(hv.v - pos.y)/2;
								sprintf(display, "IM list update %d items        ", updateCnt);
								API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
								sleep(1);
								
								updateCnt = AddMonRes_TableBySearching();

								sprintf(display, "DS list update %d items        ", updateCnt);
								API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
								sleep(1);

								updateCnt = AddMSlist_TableBySearching();
								sprintf(display, "MS list update %d items        ", updateCnt);
								API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
								sleep(1);

								updateCnt = AddIPC_TableBySearching("admin", "admin", 1);
								sprintf(update_num, " %d ", updateCnt);		//FOR_INDEXA
								
								API_GetOSD_StringWithID(MESG_TEXT_CusIPCListUpdate, NULL, 0, update_num, strlen(update_num),display, &len1);
								API_GetOSD_StringWithID(MESG_TEXT_CusIPCListItems, NULL, 0,"        ", strlen("        "),display+len1, &len2);
								
								API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display,len1+len2, 1,STR_UNICODE, bkgd_w-x);
								
								BEEP_CONFIRM();
								BusySpriteDisplay(0);
								
								break;
								
							case ICON_294_ManageImList:
								ListEditSelect(0);
								StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								break;
								
							case ICON_295_ManageDsList:
								ListEditSelect(1);
								StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								break;
								
							case ICON_296_ManageMsList:
								ListEditSelect(2);
								StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								break;
								
							case ICON_297_ManageIPCList:
								ListEditSelect(3);
								StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								break;
						}
					}
					else if(listManageIconSelect == 0)
					{
						BusySpriteDisplay(1);
						
						
						//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
						//y = DISPLAY_LIST_Y+5*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
						x = pos.x;
						y = pos.y+(hv.v - pos.y)/2;

						updateCnt = API_MsSyncListReq(MsSyncListId_Namelist);
						if(updateCnt >= 0)
						{
							sprintf(display, "IM list sync %d items        ", updateCnt);
						}
						else
						{
							if(updateCnt == -2)
							{
								sprintf(display, "IM list sync fail:master offline");
							}
							else
							{
								sprintf(display, "IM list sync fail:transmit error");
							}
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
						if(updateCnt>=0)
							sleep(1);
						
						updateCnt = API_MsSyncListReq(MsSyncListId_MonitorList);
						if(updateCnt >= 0)
						{
							sprintf(display, "DS list sync %d items        ", updateCnt);
						}
						else
						{
							if(updateCnt == -2)
							{
								sprintf(display, "DS list sync fail:master offline");
							}
							else
							{
								sprintf(display, "DS list sync fail:transmit error");
							}
						}
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
						if(updateCnt>=0)
							sleep(1);
						
						updateCnt = API_MsSyncListReq(MsSyncListId_MsNamelist);
						if(updateCnt >= 0)
						{
							sprintf(display, "MS list sync %d items        ", updateCnt);
						}
						else
						{
							if(updateCnt == -2)
							{
								sprintf(display, "MS list sync fail:master offline");
							}
							else
							{
								sprintf(display, "MS list sync fail:transmit error");
							}
						}
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
						if(updateCnt>=0)
							sleep(1);

						updateCnt = API_MsSyncListReq(MsSyncListId_IPCList);
						if(updateCnt >= 0)
						{
							sprintf(display, "IPC list sync %d items        ", updateCnt);
						}
						else
						{
							if(updateCnt == -2)
							{
								sprintf(display, "IPC list sync fail:master offline");
							}
							else
							{
								sprintf(display, "IPC list sync fail:transmit error");
							}
						}
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
			
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						sleep(1);
						
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
					}
					//czn_20190221_e
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}




