#if 0
#include "MENU_public.h"
#include "cJSON.h"


int deletVideoConfirm;
int delSpriteX,delSpriteY;

#define PLAYBACK_TIME_POS_X		(10)
#define PLAYBACK_TIME_POS_Y		(5)
char targFile[11]= {0};
char sourcename[100];

#define MaxRecNum		10
int checkRecDirSize(void)
{
    int i,cnt;
    char recfile[100] = {0};
	char tempfile[20]= {0};
	cJSON* view = cJSON_CreateArray();
    cJSON *item = NULL;
    API_TB_SelectBySortByName("IMCallRecord", view, NULL, 0);
    cnt = cJSON_GetArraySize(view);
	dprintf("checkRecDirSize cnt=%d\n", cnt);
    if(cnt > MaxRecNum)
    {
        for(i=0; i<(cnt-MaxRecNum); i++)
        {
            cJSON* where = cJSON_CreateObject();
            item=cJSON_GetArrayItem(view,i);
            GetJsonDataPro(item, "Rec_File", recfile);
            cJSON_AddStringToObject(where, "Rec_File", recfile);
            API_TB_DeleteByName("IMCallRecord", where);
			memcpy(tempfile, recfile+strlen(recfile)-10, 10);	
            delRecCacheFile(recfile);
            cJSON_Delete(where);
        }
    }
    cJSON_Delete(view);
    return 0;
}
int delRecCacheFile(char *filename)
{
	char full_file_name[200];
	snprintf(full_file_name,200,"%s%s",JPEG_STORE_DIR2,filename);
	remove(full_file_name);
	return 0;
}
void DelAllRecRecord(void)
{
	cJSON* where = cJSON_CreateObject();
	API_TB_DeleteByName("IMCallRecord", where);
	DeleteFileProcess(JPEG_STORE_DIR2, "*");
	cJSON_Delete(where);
}
void DelOneRecRecord(char *filename)
{
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "Rec_File", filename);
	API_TB_DeleteByName("IMCallRecord", where);
	delRecCacheFile(filename);
	cJSON_Delete(where);
}

int getRecFileProcess(void)
{
	int i;
	char recfile[100] = {0};
    char fileTemp[100]= {0};
	char ip[100] = {0};
	dprintf("getRecFileProcess!!!\n");
	sleep(3);
	checkRecDirSize();
	cJSON* view = cJSON_CreateArray();
	cJSON *item = NULL;
	if(API_TB_SelectBySortByName("IMCallRecord", view, NULL, 0) > 0)
	{
		for(i = cJSON_GetArraySize(view)-1; i >= 0; i--)
		{
			item=cJSON_GetArrayItem(view,i);
			GetJsonDataPro(item, "Rec_File", recfile);
			GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(item, "Call_Source"), "IP_ADDR", ip);
			playback_get_file(inet_addr(ip), recfile, fileTemp);
		}
	}
	cJSON_Delete(view);
}

int playback_get_file(int ip, char* filescr, char* filetar)
{
	char targDir[100]= {0};
	if(MakeDir(JPEG_STORE_DIR2) != 0)
	{
		return -1;
	}
	
	if(filescr && strstr(filescr, ".mp4")!= NULL)
	{
		memcpy(targDir, filescr, strlen(filescr)-10);
		memcpy(targFile, filescr+strlen(filescr)-10, 10);
		snprintf(filetar, 200, "%s%s", JPEG_STORE_DIR2, targFile);	
		if( access( filetar, F_OK ) < 0 )//?????????fftp??
		{
			dprintf("playback_get_file %s from tftp\n", filetar);
			//MessageReportDisplay(NULL, 0, "Please wait...", COLOR_RED, 1, 5);
			Api_TftpReadFileFromDevice(ip, targDir, targFile, JPEG_STORE_DIR2, targFile);	
			//MessageClose();
		}
	}
	return (access( filetar, F_OK ));
}

void DisplayPlaybackTiming( int sec_cnt )
{
	unsigned char str[10];
	int m,s;
	if(sec_cnt%20 == 0)
	{
		AutoPowerOffReset();
	}
	m = sec_cnt/60;
	s = sec_cnt%60;
	memset( str, 0, 10);
	snprintf( str, 10, "%02d:%02d  ", m, s);
	API_OsdStringDisplayExt(PLAYBACK_TIME_POS_X, PLAYBACK_TIME_POS_Y, COLOR_BLUE, str, strlen(str),1,STR_UTF8, 0);
	printf("playback time:%s\n",str);
}

void DisplayPlaybackInformation(void)
{
	API_OsdStringClearExt(bkgd_w/2-100, 5, bkgd_w/2, 40);
	API_OsdStringClearExt(bkgd_w/2-40, 40, bkgd_w/2, 40);
	API_OsdStringDisplayExt(bkgd_w/2-100, 5, COLOR_BLUE, sourcename, strlen(sourcename),1,STR_UTF8, 0);
	API_OsdStringDisplayExt(bkgd_w/2-40, 40, COLOR_BLUE, targFile, strlen(targFile),1,STR_UTF8, 0);
}

void playback_show_cb(int sec, int max)
{
	if(GetCurMenuCnt() != MENU_156_PLAYBACK)
		return;
	DisplayPlaybackTiming(sec);
}
void api_playback_event(char* event, cJSON* record)
{
	char recfile[200] = {0};
	char ip[100] = {0};
	char fileTemp[200]= {0};
	char name[50] = {0};
    char time[50] = {0};
	GetJsonDataPro(record, "Rec_File", recfile);
	GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(record, "Call_Source"), "IP_ADDR", ip);
	GetJsonDataPro(cJSON_GetObjectItemCaseSensitive(record, "Call_Source"), "NAME", name);        
	GetJsonDataPro(record, "Time", time);    
	if(!strcmp(event, "Play_Start"))
	{
		if( playback_get_file(inet_addr(ip), recfile, fileTemp) == 0)
		{
			snprintf(sourcename, 100, "[%s] %s", time, name);
			StartInitOneMenu(MENU_156_PLAYBACK,0,1);
			api_start_ffmpegPlay(fileTemp, playback_show_cb);
		}
		else
		{
			MessageReportDisplay(NULL, 0, "get file error!", COLOR_RED, 1, 5);
		}
	}
	else if(!strcmp(event, "Play_Next"))
	{
		if( playback_get_file(inet_addr(ip), recfile, fileTemp) == 0)
		{
			snprintf(sourcename, 100, "[%s] %s", time, name);
			DisplayPlaybackInformation();
			api_change_file_ffmpegPlay(fileTemp, playback_show_cb);
		}
	}
}

void playback_turn(int act)
{
	int i,cnt;
	char recfile[200] = {0};
	cJSON *item;
	cJSON* view = cJSON_CreateArray();
	if(API_TB_SelectBySortByName("IMCallRecord", view, NULL, 0) > 0)
	{
		cnt = cJSON_GetArraySize(view);
	}

	for(i=0;i<cnt;i++)
	{
		item=cJSON_GetArrayItem(view,i);
		GetJsonDataPro(item, "Rec_File", recfile);
		if(strstr(recfile, targFile) !=NULL)
		{
			break;
		}
	}

	if(i ==cnt)
	{
		return;
	}
	if(act ==1)
	{
		i++;
		if(i>=cnt)
		{
			i = 0;
		}			
	}
	else
	{
		i--;
		if(i<0)
		{
			i = cnt - 1;
		}			
	}
	item=cJSON_GetArrayItem(view,i);
	api_playback_event("Play_Next", item);
	cJSON_Delete(view);
}

/*************************************************************************************************
06. playback
*************************************************************************************************/
void MENU_156_PLAYBACK_Init(int uMenuCnt)	//cao_20161227
{
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	OSD_GetIconInfo(ICON_226_PlaybackDel, &pos, &hv);
	Get_SpriteSize(SPRITE_DelConfirm, &xsize, &ysize);
	delSpriteX = pos.x + (hv.h - pos.x)/2 - xsize/2;
	delSpriteY = pos.y + (hv.v - pos.y)/2 - ysize/2;
	deletVideoConfirm = 0;
	DisplayPlaybackInformation();
}

void MENU_156_PLAYBACK_Exit(void)
{
	api_stop_ffmpegPlay();
}

void MENU_156_PLAYBACK_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int curnLen; int maxLen;} *pData;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
				
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_046_Replay:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					api_ffmpegPlayPause();
					break;
					
				case ICON_225_PlaybackClose:					
					popDisplayLastMenu();
					break;
				case ICON_226_PlaybackDel:
					if(deletVideoConfirm == 0)
					{
						deletVideoConfirm = 1;
						API_SpriteDisplay_XY(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						
					}
					else
					{
						DelOneRecRecord(targFile);
						popDisplayLastMenu();
					}
					break;
				case ICON_227_PlaybackNext:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turn(1);
					break;
				case ICON_228_PlaybackLast:
					if(deletVideoConfirm)
					{
						API_SpriteClose(delSpriteX, delSpriteY, SPRITE_DelConfirm);
						deletVideoConfirm = 0;
					}
					playback_turn(0);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_PLYBAK_STEP:
				pData = (arg + sizeof(SYS_WIN_MSG));
				DisplayPlaybackTiming(pData->curnLen);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
		
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

#endif

