#include "MENU_public.h"
#include "obj_SearchIpByFilter.h"
#include "obj_ResSync.h"

#define	IPC_SETTING_LIST_ICON_MAX		5

int ipcSettingIconSelect;
int ipcSettingPageSelect;
int ipcSettingIndex;
int ipcDhcpEnable;
#if 0

const IconAndText_t IPC_SettingIconTable[] = 
{
	{ICON_273_IPC_AddBySearch,			MESG_TEXT_Add_IPC_BySearching},    
	{ICON_274_IPC_AddByManual,			MESG_TEXT_Add_IPC_ByManual},
	{ICON_275_IPC_List, 				MESG_TEXT_ICON_275_IPC_List},
	{ICON_277_IPC_DHCP, 				MESG_TEXT_ICON_277_IPC_DHCP},
};
const int IPC_SettingIconNum = sizeof(IPC_SettingIconTable)/sizeof(IPC_SettingIconTable[0]);


static void DisplayIPCSettingPageIcon(int page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	uint16 monitorTime;
	char display[20];
	int stringId;
	uint8 temp;
	POS pos;
	SIZE hv;
	
	for(i = 0; i < IPC_SETTING_LIST_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*IPC_SETTING_LIST_ICON_MAX+i < IPC_SettingIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, IPC_SettingIconTable[i+page*IPC_SETTING_LIST_ICON_MAX].iConText, 1, val_x - x);
			x += (hv.h - pos.x)/2;
			switch(IPC_SettingIconTable[i+page*IPC_SETTING_LIST_ICON_MAX].iCon)
			{
				case ICON_277_IPC_DHCP:
					stringId = (ipcDhcpEnable ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", hv.h-x);
					break;
				default:
					break;
			}
		}
	}
	pageNum = IPC_SettingIconNum/IPC_SETTING_LIST_ICON_MAX + (IPC_SettingIconNum%IPC_SETTING_LIST_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void IPC_DHCP_EnableSet(int value)
{
	char temp[20];

	sprintf(temp, "%d", value ? 1 : 0);
	API_Event_IoServer_InnerWrite_All(IPC_DHCP_Enable, temp);
	API_Event_IoServer_InnerRead_All(IPC_DHCP_Enable, temp);
	ipcDhcpEnable = atoi(temp) ? 1 : 0;
}
#endif


int ipcListNum, typeSelect;
int ipcSetupState, ipcSyncListNum;
SearchIpRspData *ipcSyncSearchList=NULL;
char *ipcSyncToSel=NULL;
char ipcSyncFromSel;

int GetSyncListCnt(void)
{
	if(api_nm_if_get_net_mode() == 0 && GetIpcSetSelect() ==1)
	{
		ipcSyncListNum = 0;
		return ipcSyncListNum;
	}
	if(ipcSyncSearchList == NULL)
	{
		ipcSyncSearchList = (SearchIpRspData*)malloc(sizeof(SearchIpRspData));
	}
	if(ipcSyncSearchList == NULL)
	{
		return;
	}
	
	ipcSyncSearchList->deviceCnt = 0;
	BusySpriteDisplay(1);
	API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_IM, SearchDeviceRecommendedWaitingTime, 0, ipcSyncSearchList);
	BusySpriteDisplay(0);
	ipcSyncListNum = ipcSyncSearchList->deviceCnt;
	printf("GetSyncListCnt =%d\n", ipcSyncListNum);

	if(ipcSetupState == ICON_037_ListEdit)
	{
		if(ipcSyncToSel == NULL)
		{
			ipcSyncToSel = (char*)malloc(sizeof(char)*(ipcSyncSearchList->deviceCnt>0?ipcSyncSearchList->deviceCnt:1));
		}
		memset(ipcSyncToSel,1,ipcSyncListNum);
	}
	else
	{
		ipcSyncFromSel = 0;
	}

	return ipcSyncListNum;
}

static void MenuListGetSyncListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < ipcSyncListNum)
		{
			get_device_addr_and_name_disp_str(0,ipcSyncSearchList->data[index].BD_RM_MS, NULL, NULL, ipcSyncSearchList->data[index].name,DisplayBuffer);
			recordLen = strlen(DisplayBuffer);
			//unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			unicode_len = 2*utf82unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			if(ipcSetupState == ICON_037_ListEdit)
			{
				if(ipcSyncToSel[index] == 1)
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
				}
			}
			else
			{
				if(ipcSyncFromSel == index)
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
				}
			}			
			pDisp->dispCnt++;
		}

	}
}

void SyncListShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	if(ipcSetupState == ICON_037_ListEdit)
		API_GetOSD_StringWithID2(MESG_TEXT_ICON_RES_TO_SYNC, unicode_buf, &unicode_len);
	else
		API_GetOSD_StringWithID2(MESG_TEXT_ICON_RES_FROM_SYNC, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.selEn = 1;
	listInit.listCnt = GetSyncListCnt();
	listInit.fun = MenuListGetSyncListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	
	InitMenuList(listInit);

}

static void MenuListGetIPCListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	IPC_ONE_DEVICE ipcRecord;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < ipcListNum)
		{
			if(typeSelect == 0)
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_IPC;
				GetIpcCacheRecord(index, &ipcRecord);
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_LIST_WIPC;
				GetWlanIpcRecord(index, &ipcRecord);
			}
			strcpy(DisplayBuffer, ipcRecord.NAME);
			recordLen = strlen(DisplayBuffer);
			unicode_len = 2*utf82unicode(DisplayBuffer,recordLen,unicode_buf);
			//unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}
	}
}

void IPCListIconShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	
	API_GetOSD_StringWithID2(MESG_TEXT_IPC_MonitorList, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	ipcListNum = (typeSelect == 0)? GetIpcNum() : GetWlanIpcNum();
	listInit.listCnt = ipcListNum;
	listInit.listType = ICON_2X4;
	listInit.textOffset = 110;

	listInit.textSize = 0;
	listInit.textAlign = 8;
	listInit.fun = MenuListGetIPCListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);

}

void EnterIpcSetMenu(int type)
{
	typeSelect = type;
	StartInitOneMenu(MENU_054_IPC_SETTING,0,1);
}
int GetIpcSetSelect(void)
{
	return typeSelect;
}

void MENU_054_IPC_Setting_Init(int uMenuCnt)
{
	API_MenuIconDisplaySelectOn(ICON_034_ListView);	
	ipcSetupState = ICON_034_ListView;
	IPCListIconShow();
}


void MENU_054_IPC_Setting_Exit(void)
{
	if(ipcSyncSearchList != NULL)
	{
		free(ipcSyncSearchList);
		ipcSyncSearchList=NULL;	
	}
	if(ipcSyncToSel!=NULL)
	{
		free(ipcSyncToSel);
		ipcSyncToSel=NULL;
	}
	MenuListDisable(0);
}

void MENU_054_IPC_Setting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int index, i;
	LIST_ICON listIcon;
	IPC_ONE_DEVICE ipcRecord;
	char display[200];
	int ret;
	POS pos;
	SIZE hv;
	int resId;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					index = listIcon.mainIcon;
					if(index >= 0)
					{
						if(ipcSetupState == ICON_034_ListView)
						{
							if(typeSelect == 0)
							{
								GetIpcCacheRecord(index, &ipcRecord);
							}
							else
							{
								GetWlanIpcRecord(index, &ipcRecord);
							}
							EnterIpcLoginMenu(ipcRecord.IP, ipcRecord.USER, ipcRecord.PWD, 1);
						}
						else if(ipcSetupState == ICON_037_ListEdit)
						{
							if(ipcSyncToSel[index] == 1)
							{
								ipcSyncToSel[index] = 0;
							}
							else
							{
								ipcSyncToSel[index] = 1;
							}
							MenuListSetCurrentPage(MenuListGetCurrentPage());
						}
						else
						{
							ipcSyncFromSel = index;
							MenuListSetCurrentPage(MenuListGetCurrentPage());
						}
					}
				break;
				case ICON_035_ListAdd:
					StartInitOneMenu(MENU_060_IPC_SEARCHING,0,1);
				break;
				case ICON_036_ListDelete:
					EnterIpcLoginMenu(NULL,NULL,NULL,1);
				break;
				case ICON_034_ListView:
					OSD_GetIconInfo(ipcSetupState, &pos, &hv);
					API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 0);
					if(ipcSetupState != ICON_034_ListView)
					{
						API_MenuIconDisplaySelectOff(ipcSetupState);
						ipcSetupState = ICON_034_ListView;
						API_MenuIconDisplaySelectOn(ipcSetupState); 
						IPCListIconShow();
					}
				break;
				case ICON_037_ListEdit:
					OSD_GetIconInfo(ipcSetupState, &pos, &hv);
					API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 0);
					if(ipcSetupState != ICON_037_ListEdit)
					{
						API_MenuIconDisplaySelectOff(ipcSetupState);
						ipcSetupState = ICON_037_ListEdit;
						API_MenuIconDisplaySelectOn(ipcSetupState); 
						SyncListShow();
						OSD_GetIconInfo(ipcSetupState, &pos, &hv);
						API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 1);
					}
					else
					{
						resId = GetIpcSetSelect() ? RESID_8022_WlanIPCList : RESID_8021_LanIPCList;
						SaveResFileFromActFile(resId,GetSysVerInfo_BdRmMs(),0);
						for(i = 0;i < ipcSyncListNum;i++)
						{
							if(ipcSyncToSel[i] == 1)
							{
								API_OsdStringClearExt(332, 550, bkgd_w-332, 40);
								sprintf(display, "Sync To %s", ipcSyncSearchList->data[i].BD_RM_MS);
								API_OsdStringDisplayExt(332, 550, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
								ret = Api_ResSyncToDevice(resId,ipcSyncSearchList->data[i].BD_RM_MS);
								if(ret == 0)
								{
									sprintf(display, "OK");
								}
								else if(ret == -1)
								{
									sprintf(display, "NO FILE");
								}
								else
								{
									sprintf(display, "ERR");
								}
								API_OsdStringDisplayExt(332+360, 550, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);								
							}
						}
					}
				break;
				case ICON_038_ListCheck:
					OSD_GetIconInfo(ipcSetupState, &pos, &hv);
					API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 0);
					if(ipcSetupState != ICON_038_ListCheck)
					{
						API_MenuIconDisplaySelectOff(ipcSetupState);
						ipcSetupState = ICON_038_ListCheck;
						API_MenuIconDisplaySelectOn(ipcSetupState); 
						SyncListShow();
						OSD_GetIconInfo(ipcSetupState, &pos, &hv);
						API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 1);
					}
					else
					{
						if(ipcSyncListNum == 0)
						{
							return;
						}
						API_OsdStringClearExt(332, 550, bkgd_w-332, 40);
						sprintf(display, "Sync From %s", ipcSyncSearchList->data[ipcSyncFromSel].BD_RM_MS);
						API_OsdStringDisplayExt(332, 550, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);	
						resId = GetIpcSetSelect() ? RESID_8022_WlanIPCList : RESID_8021_LanIPCList;
						ret = Api_ResSyncFromServer(resId,ipcSyncSearchList->data[ipcSyncFromSel].BD_RM_MS);
						if(ret == 0)
						{
							sprintf(display, "OK");
						}
						else if(ret == -1)
						{
							sprintf(display, "NO FILE");
						}
						else
						{
							sprintf(display, "ERR");
						}
						API_OsdStringDisplayExt(332+360, 550, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
					}
				break;
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




