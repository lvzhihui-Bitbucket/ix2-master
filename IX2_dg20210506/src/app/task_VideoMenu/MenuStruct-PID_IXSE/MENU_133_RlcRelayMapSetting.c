#include "cJSON.h"
#include "MENU_133_RlcRelayMapSetting.h"
#include "obj_NoticeIxmini.h"
#include "obj_UnifiedParameterMenu.h"	

static IXMiniRelayMap_T rlcRelayMap;
unsigned int rlcRelayMapSettingDataItem = 0;

const IconAndText_t rlcRelayMapSettingIconTable[] = 
{
	{ICON_RlcRelayMapNum, 				MESG_TEXT_RlcRelayMapNum},	 
	{ICON_RlcRelayMapInitialFloor, 		MESG_TEXT_RlcRelayMapInitialFloor},
};


const int rlcRelayMapSettingIconTableNum = sizeof(rlcRelayMapSettingIconTable)/sizeof(rlcRelayMapSettingIconTable[0]);
static char rlcRelayMapSettingNumInput[4] = {0};
static char rlcRelayMapSettingInitFloorInput[4] = {0};


static void DisplayRlcRelayMapSettingIconTablePageIcon(int page)
{
	uint8 i, j;
	uint16 x, y,x1,y1;
	char display[100];
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
			
	for(i = 0; i < rlcRelayMapSettingIconTableNum; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x1 = x + (hv.h - pos.x)/3;
			y1 = y + 40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x1 =x+ (hv.h - pos.x)/2;
			y1 =y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		switch(rlcRelayMapSettingIconTable[i].iCon)
		{
			case ICON_RlcRelayMapNum:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayMapSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 100, "%d", rlcRelayMap.floorCnt);
				snprintf(rlcRelayMapSettingNumInput, 100, "%d", rlcRelayMap.floorCnt);
				break;
			
			case ICON_RlcRelayMapInitialFloor:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, rlcRelayMapSettingIconTable[i].iConText, 1, bkgd_w-x-10);
				snprintf(display, 3,"%s", rlcRelayMap.floorNbr[0]);
				snprintf(rlcRelayMapSettingInitFloorInput, 3,"%s", rlcRelayMap.floorNbr[0]);
				break;
		}
		API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x1-20);
	}

	API_EnableOsdUpdate();
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);

}


static char* CreateIXMiniRelayMapObject(IXMiniRelayMap_T obj)
{
    cJSON *root = NULL;
	char* string;
	char* pChar[IXMINI_FLOOR_MAX_NUM];
	int i;
	
    for(i = 0; i < obj.floorCnt; i++)
    {
    	pChar[i] = obj.floorNbr[i];
	}
	
	root = cJSON_CreateStringArray(pChar, obj.floorCnt);
	
	string = cJSON_Print(root);
	cJSON_Delete(root);

	return string;
}

static int ParseIXMiniRelayMapObject(const char* json, IXMiniRelayMap_T* pData)
{
    int status = 0;
	int iCnt;
	cJSON *pSub = NULL;

	memset(pData, 0, sizeof(IXMiniRelayMap_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	
	pData->floorCnt = cJSON_GetArraySize ( root );
	
	for( iCnt = 0 ; iCnt < pData->floorCnt && iCnt < IXMINI_FLOOR_MAX_NUM; iCnt ++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }
		
		if (cJSON_IsString(pSub) && (pSub->valuestring != NULL))
		{
			strncpy(pData->floorNbr[iCnt], pSub->valuestring, 2);
			pData->floorNbr[iCnt][2] = 0;
		}
		else
		{
			pData->floorNbr[iCnt][0] = 0;
		}
	}
		
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}
static int CharAdd(char* tempChar, int addr)
{
	int ret;
	
	if(*tempChar >= '0' && *tempChar <= '9')
	{
		if(*tempChar + addr > '9')
		{
			*tempChar = '0' + addr%10 - 1;
			ret = 1;
		}
		else
		{
			*tempChar = *tempChar + addr;
			ret = 0;
		}
	}
	else if(*tempChar >= 'a' && *tempChar <= 'z')
	{
		if(*tempChar + addr > 'z')
		{
			*tempChar = 'a' + addr%26 - 1;
			ret = 1;
		}
		else
		{
			*tempChar = *tempChar + addr;
			ret = 0;
		}
	}
	else if(*tempChar >= 'A' && *tempChar <= 'Z')
	{
		if(*tempChar + addr > 'Z')
		{
			*tempChar = 'A' + addr%26 - 1;
			ret = 1;
		}
		else
		{
			*tempChar = *tempChar + addr;
			ret = 0;
		}
	}
	else
	{
		ret = -1;
	}
	
	return ret;
}
static int SaverlcRelayMapSet(void)
{
	int num, i, j;
	char* tempChar;
	char temp1, temp2;
	int ret1, ret2;
	
	num = atoi(rlcRelayMapSettingNumInput);

	if(num >= 1 && num <= IXMINI_FLOOR_MAX_NUM)
	{
		snprintf(rlcRelayMap.floorNbr[0], 3,"%s", rlcRelayMapSettingInitFloorInput);

		for(i = 0; i < num; i++)
		{
			temp1 = rlcRelayMapSettingInitFloorInput[1];
			temp2 = rlcRelayMapSettingInitFloorInput[0];
			ret1 = CharAdd(&temp1, i);
			if(ret1 == 1)
			{
				ret2 = CharAdd(&temp2, i);
				if(ret2 == -1)
				{
					BEEP_ERROR();
					return 0;
				}
			}
			else if(ret1 == -1)
			{
				BEEP_ERROR();
				return 0;
			}
			
			rlcRelayMap.floorNbr[i][1] = temp1;
			rlcRelayMap.floorNbr[i][0] = temp2;
		}
		
		rlcRelayMap.floorCnt = num;
		tempChar = CreateIXMiniRelayMapObject(rlcRelayMap);
		if(tempChar != NULL)
		{
			SetRemoteParaValue(tempChar);
			free(tempChar);
		}
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}

void GetRlcRelayMapConfigValue(void)
{
	extern ParaMenu_T OneRemotePararecord;
	ParseIXMiniRelayMapObject(OneRemotePararecord.value, &rlcRelayMap);
}

void MENU_133_RlcRelayMapSetting_Init(int uMenuCnt)
{
	ParaMenu_T record;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	GetParaMenuRecord(IXRLC_RelayMap, &record);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_LIST_COLOR, record.name, record.nameLen, 1, STR_UNICODE, 0);			

	DisplayRlcRelayMapSettingIconTablePageIcon(0);
}

void MENU_133_RlcRelayMapSetting_Exit(void)
{

}

void MENU_133_RlcRelayMapSetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len, i;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					CloseMenu();
					break;
					
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					rlcRelayMapSettingDataItem = GetCurIcon() - ICON_007_PublicList1;
					switch(rlcRelayMapSettingIconTable[rlcRelayMapSettingDataItem].iCon)
					{
						case ICON_RlcRelayMapNum:
							snprintf(rlcRelayMapSettingNumInput, 4,"%d", rlcRelayMap.floorCnt);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PublicTime, rlcRelayMapSettingNumInput, 3, COLOR_WHITE, rlcRelayMapSettingNumInput, 1, SaverlcRelayMapSet);
							break;	
							
						case ICON_RlcRelayMapInitialFloor:
							snprintf(rlcRelayMapSettingInitFloorInput, 3,"%d", rlcRelayMap.floorNbr[0]);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_PublicTime, rlcRelayMapSettingInitFloorInput, 2, COLOR_WHITE, rlcRelayMapSettingInitFloorInput, 1, SaverlcRelayMapSet);
							break;	
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}



