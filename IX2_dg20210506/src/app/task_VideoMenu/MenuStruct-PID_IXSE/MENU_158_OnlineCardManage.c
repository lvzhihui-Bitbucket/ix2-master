#include "MENU_public.h"
#include "cJSON.h"
#include "task_Event.h"

int CardManageIconMax;
int CardManageIconSelect;
int CardManagePageSelect;
int CardManageConfirm;
int CardManageConfirmSelect;
int CardReportState=0;

const IconAndText_t CardManageIconTable[] = 
{
	{ICON_MasterCardAddSet,		MESG_TEXT_MasterAddSet},    
	{ICON_MasterCardDelSet,		MESG_TEXT_MasterDelSet},    
	{ICON_UserCardAdd,			MESG_TEXT_UserCardAdd},
	{ICON_UserCardDel,			MESG_TEXT_UserCardDel},
	{ICON_UserCardDelAll,		MESG_TEXT_UserCardDelAll},
	{ICON_UserCardList,			MESG_TEXT_UserCardList},
};

const int CardManageIconNum = sizeof(CardManageIconTable)/sizeof(CardManageIconTable[0]);


void DisplayOnePageCardManage(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;
	POS pos;
	SIZE hv;

	for( i = 0; i < CardManageIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*CardManageIconMax+i < CardManageIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, CardManageIconTable[i+page*CardManageIconMax].iConText, 1, val_x - x);			
		}
		
	}
	
	
	maxPage = CardManageIconNum/CardManageIconMax + (CardManageIconNum%CardManageIconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_158_OnlineCardManage_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	CardManageIconMax = GetListIconNum();
	CardManageIconSelect = 0;
	CardManagePageSelect = 0;
	CardManageConfirm = 0;
	CardManageConfirmSelect = 0;
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_CardManagement,2, 0);
		
	DisplayOnePageCardManage(0);
}


static int ConfirmAddOneCard(const char* input)
{
	int i;
	int len;
	char carddata[11];
	len = strlen(input);
	if( len == 0 || len > 10 )
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	if( len < 10 )
	{
		memset(carddata,0,11);
		memset(carddata,'0',10-len);
		strcat(carddata,input);
	}
	else
	{
		sprintf(carddata,"%s",input);
	}
	cJSON* Where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	cJSON* newitem=cJSON_CreateObject();
	cJSON_AddStringToObject(Where,"CARD_NUM",carddata);
	if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", View, Where, 0) > 0)
	{
		MessageReportDisplay(NULL, 0, "card exist", COLOR_RED, 1, 5);
	}
	else
	{
		time_t t;
		struct tm *tblock; 	
		t = time(NULL); 
		tblock=localtime(&t);
		char buff[50];
		sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
		cJSON_AddStringToObject(newitem,"CARD_NUM",carddata);
		cJSON_AddNumberToObject(newitem,"ROOM",1);
		cJSON_AddStringToObject(newitem,"DATE",buff);
		cJSON_AddStringToObject(newitem,"NAME","-");
		cJSON_AddStringToObject(newitem,"PROPERTY","-");
		int ret = API_RemoteTableAdd(GetSearchOnlineIp(), "CardTable", newitem);
		MessageReportDisplay(NULL, 0, ret? "card add sucess" : "card add error", COLOR_RED, 1, 5);
	}
	cJSON_Delete(Where);
	cJSON_Delete(View);
	cJSON_Delete(newitem);
	//StartInitOneMenu(MENU_159_ix622CardList,0,0);
	return 2;
}
static int ConfirmDelOneCard(const char* input)
{
	int i;
	int len;
	char carddata[11];
	len = strlen(input);
	if( len == 0 || len > 10 )
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	if( len < 10 )
	{
		memset(carddata,0,11);
		memset(carddata,'0',10-len);
		strcat(carddata,input);
	}
	else
	{
		sprintf(carddata,"%s",input);
	}
	cJSON* Where=cJSON_CreateObject();
	cJSON_AddStringToObject(Where,"CARD_NUM",carddata);
	cJSON_AddStringToObject(Where,"PROPERTY","-");
	int ret = API_RemoteTableDelete(GetSearchOnlineIp(), "CardTable", Where);
	MessageReportDisplay(NULL, 0, ret? "card del sucess" : "card del error", COLOR_RED, 1, 5);
	cJSON_Delete(Where);
	//StartInitOneMenu(MENU_159_ix622CardList,0,0);
	return 2;
}
static int ConfirmMasterAddCard(const char* input)
{
	int i;
	int len;
	char carddata[11];
	len = strlen(input);
	if( len == 0 || len > 10 )
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	if( len < 10 )
	{
		memset(carddata,0,11);
		memset(carddata,'0',10-len);
		strcat(carddata,input);
	}
	else
	{
		sprintf(carddata,"%s",input);
	}
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	char buff[50];
	int ret;
	sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
	cJSON* Where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	cJSON* newitem=cJSON_CreateObject();
	cJSON_AddStringToObject(Where,"PROPERTY","add");
	cJSON_AddStringToObject(newitem,"CARD_NUM",carddata);
	cJSON_AddNumberToObject(newitem,"ROOM",1);
	cJSON_AddStringToObject(newitem,"DATE",buff);
	cJSON_AddStringToObject(newitem,"NAME","-");
	cJSON_AddStringToObject(newitem,"PROPERTY","add");
	if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", View, Where, 0) > 0)
	{
		ret = API_RemoteTableUpdate(GetSearchOnlineIp(), "CardTable", Where, newitem);
	}
	else
	{		
		ret = API_RemoteTableAdd(GetSearchOnlineIp(), "CardTable", newitem);
	}
	MessageReportDisplay(NULL, 0, ret? "Master-Add set sucess" : "Master-Add set error", COLOR_RED, 1, 5);
	sleep(1);
	cJSON_Delete(Where);
	cJSON_Delete(View);
	cJSON_Delete(newitem);
	return 1;
}
static int ConfirmMasterDelCard(const char* input)
{
	int i;
	int len;
	char carddata[11];
	len = strlen(input);
	if( len == 0 || len > 10 )
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	if( len < 10 )
	{
		memset(carddata,0,11);
		memset(carddata,'0',10-len);
		strcat(carddata,input);
	}
	else
	{
		sprintf(carddata,"%s",input);
	}
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	char buff[50];
	int ret;
	sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
	cJSON* Where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	cJSON* newitem=cJSON_CreateObject();
	cJSON_AddStringToObject(Where,"PROPERTY","del");
	cJSON_AddStringToObject(newitem,"CARD_NUM",carddata);
	cJSON_AddNumberToObject(newitem,"ROOM",1);
	cJSON_AddStringToObject(newitem,"DATE",buff);
	cJSON_AddStringToObject(newitem,"NAME","-");
	cJSON_AddStringToObject(newitem,"PROPERTY","del");
	if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", View, Where, 0) > 0)
	{
		ret = API_RemoteTableUpdate(GetSearchOnlineIp(), "CardTable", Where, newitem);
	}
	else
	{		
		ret = API_RemoteTableAdd(GetSearchOnlineIp(), "CardTable", newitem);
	}
	MessageReportDisplay(NULL, 0, ret? "Master-Del set sucess" : "Master-Del set error", COLOR_RED, 1, 5);
	sleep(1);
	cJSON_Delete(Where);
	cJSON_Delete(View);
	cJSON_Delete(newitem);
	return 1;
}

void Send_CardReportEvent(int state)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventCardIdReport");
	cJSON_AddStringToObject(send_event, "SOURCE",GetSysVerInfo_name());
	cJSON_AddStringToObject(send_event, "IP_ADDR",GetSysVerInfo_IP());
	cJSON_AddNumberToObject(send_event,"STATE",state);
	API_XD_EventJson(GetSearchOnlineIp(),send_event);
	cJSON_Delete(send_event);
}
int EventCardIdReportCallback(cJSON *cmd)
{
	if(GetCurMenuCnt() != MENU_018_KEYPAD_NUM)
		return;
	char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName && !strcmp(eventName, EventCardIdReport))
	{
		API_KeyboardSetInput(GetEventItemString(cmd, "CARD_NUM"));
	}
}

void MENU_158_OnlineCardManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char input[11];
	char initChar[11] = {0};
	cJSON* Where;
	cJSON* View;
	int ret;
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					CardManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(CardManagePageSelect*CardManageIconMax + CardManageIconSelect >= CardManageIconNum)
					{
						return;
					}
					switch(CardManageIconTable[CardManagePageSelect*CardManageIconMax + CardManageIconSelect].iCon)
					{
						case ICON_MasterCardAddSet:
							Where=cJSON_CreateObject();
							View=cJSON_CreateArray();
							cJSON_AddStringToObject(Where,"PROPERTY","add");
							if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", View, Where, 0) > 0)
							{
								GetJsonDataPro(cJSON_GetArrayItem(View,0), "CARD_NUM", initChar); 
							}
							else
							{
								memset(initChar,0,11);
							}
							cJSON_Delete(Where);
							cJSON_Delete(View);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_MasterAddSet, input, 10, COLOR_WHITE, initChar, 1, ConfirmMasterAddCard);	
							CardReportState = 1;
							Send_CardReportEvent(1);
						break;
						case ICON_MasterCardDelSet:
							Where=cJSON_CreateObject();
							View=cJSON_CreateArray();
							cJSON_AddStringToObject(Where,"PROPERTY","del");
							if(API_RemoteTableSelect(GetSearchOnlineIp(), "CardTable", View, Where, 0) > 0)
							{
								GetJsonDataPro(cJSON_GetArrayItem(View,0), "CARD_NUM", initChar); 
							}
							else
							{
								memset(initChar,0,11);
							}
							cJSON_Delete(Where);
							cJSON_Delete(View);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_MasterDelSet, input, 10, COLOR_WHITE, initChar, 1, ConfirmMasterDelCard);	
							CardReportState = 1;
							Send_CardReportEvent(1);
						break;
						case ICON_UserCardAdd:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_UserCardAdd, input, 10, COLOR_WHITE, NULL, 0, ConfirmAddOneCard);	
							CardReportState = 1;
							Send_CardReportEvent(1);
						break;
						case ICON_UserCardDel:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_UserCardDel, input, 10, COLOR_WHITE, NULL, 0, ConfirmDelOneCard);	
							CardReportState = 1;
							Send_CardReportEvent(1);
						break;
						case ICON_UserCardDelAll:
							if(ConfirmSelect(&CardManageConfirm, &CardManageConfirmSelect, &CardManageIconSelect, CardManagePageSelect, CardManageIconNum))
							{
								return;
							}
							cJSON* Where=cJSON_CreateObject();
							cJSON_AddStringToObject(Where,"PROPERTY","-");
							ret = API_RemoteTableDelete(GetSearchOnlineIp(), "CardTable", Where);
							MessageReportDisplay(NULL, 0, ret? "card delete all" : "card delete none", COLOR_RED, 1, 5);
							cJSON_Delete(Where);
						break;	
						case ICON_UserCardList:
							StartInitOneMenu(MENU_159_ix622CardList,0,1);
						break;
					}
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&CardManagePageSelect, CardManageIconMax, CardManageIconNum, (DispListPage)DisplayOnePageCardManage);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&CardManagePageSelect, CardManageIconMax, CardManageIconNum, (DispListPage)DisplayOnePageCardManage);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


void MENU_158_OnlineCardManage_Exit(void)
{
	if(CardReportState)
	{
		CardReportState = 0;
		Send_CardReportEvent(0);
	}
}