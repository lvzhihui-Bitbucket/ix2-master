/**
  ******************************************************************************
  * @file    obj_String.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the function of String Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
	  
#include "task_VideoMenu.h"
#include "obj_menu_data.h"

void OSD_StringDisplay(int uCol,int uRow,int uColor,int uBgColor, const unsigned char *ptrStr)
{
	display_string( uCol,uRow,uColor, uBgColor, (char*)ptrStr );
}

void OSD_StringClear(int uCol,int uRow,const unsigned char *ptrStr)
{
	//clear_string( uCol,uRow,uLen );
	display_string( uCol,uRow,COLOR_KEY, COLOR_KEY, (char*)ptrStr );
}

void OSD_StringDisplayExt(int x,int y,int uColor, int uBgColor, const unsigned char *ptrStr, int str_len, int fnt_type, int format, int width)
{
	display_ext_string( x,y,uColor, uBgColor, (char*)ptrStr, str_len, fnt_type, format, width);
}
void OSD_StringDisplayExt2(int x,int y,int layer_id, int uColor, int uBgColor, const unsigned char *ptrStr, int str_len, int fnt_type, int format, int xsize, int ysize)
{
	display_ext_string2( x, y, layer_id, uColor, uBgColor, (char*)ptrStr, str_len, fnt_type, format, xsize, ysize);
}

void OSD_StringDisplayExtOnePageList(MenuStrMulti_T list)
{
	display_ext_string_one_page_list(list);
}

void OSD_UpdateDisplay(int enable, int x, int y, int width, int height, int alpha)
{
	if(enable)
	{
		EnableUpdate();
		if(alpha)
		{
			UpdateOsdLayerAlpha(alpha, x, y, width, height, 0);
		}
		else
		{
			UpdateOsdLayerBuf2(0, x, y, width, height, 0);
		}
	}
	else
	{
		DisableUpdate();
	}
}

void OSD_StringDisplayWithID_Str(int x,int y,int uColor, int uBgColor, int str_id, int fnt_type, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, int width)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;
	char unicodeDisplay[400];
	int unicodeDisplayLen;
	
	char* org_ptr;
	
	unicodeDisplayLen = (2*api_ascii_to_unicode( asc_pre, asc_pre_len, unicodeDisplay));

	org_ptr = get_global_mesg_org_with_id(str_id);
	
	if( org_ptr != NULL )
	{
		unicode_org_len = api_ascii_to_unicode( org_ptr, strlen(org_ptr), unicode_org ); 

		if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
		{
			if( unicode_tra_len > 0 )
			{
				if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				{
					Arbic_Position_Process(unicode_tra,unicode_tra_len,unicode_tra,&unicode_tra_len);
					Unicode_Invert(unicode_tra,unicode_tra_len);
				}
				memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, unicode_tra_len);
				unicodeDisplayLen += unicode_tra_len;
			}
		}
		else
		{
			char* tra_ptr;
			tra_ptr = get_global_mesg_tra_with_id( str_id );
			if( tra_ptr != NULL )
			{
				unicode_tra_len = api_ascii_to_unicode( tra_ptr, strlen(tra_ptr), unicode_tra ); 
				if( unicode_tra_len > 0 )
				{
					memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, 2*unicode_tra_len);
					unicodeDisplayLen += (2*unicode_tra_len);
				}				
			}
		}
	}

	unicodeDisplayLen += (2*api_ascii_to_unicode( asc_lst, asc_lst_len, unicodeDisplay+unicodeDisplayLen));
	
	display_unicode_str( x, y, uColor, uBgColor, unicodeDisplay, unicodeDisplayLen, fnt_type, width);
}

void OSD_StringDisplayWithID_Str2(int x,int y,int layer_id, int uColor, int uBgColor, int str_id, int fnt_type, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, int xsize, int ysize)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;
	char unicodeDisplay[400];
	int unicodeDisplayLen;
	
	char* org_ptr;
	
	unicodeDisplayLen = (2*api_ascii_to_unicode( asc_pre, asc_pre_len, unicodeDisplay));

	org_ptr = get_global_mesg_org_with_id(str_id);
	
	if( org_ptr != NULL )
	{
		unicode_org_len = api_ascii_to_unicode( org_ptr, strlen(org_ptr), unicode_org ); 

		if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
		{
			if( unicode_tra_len > 0 )
			{
				if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				{
					Arbic_Position_Process(unicode_tra,unicode_tra_len,unicode_tra,&unicode_tra_len);
					Unicode_Invert(unicode_tra,unicode_tra_len);
				}
				memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, unicode_tra_len);
				unicodeDisplayLen += unicode_tra_len;
			}
		}
		else
		{
			char* tra_ptr;
			tra_ptr = get_global_mesg_tra_with_id( str_id );
			if( tra_ptr != NULL )
			{
				unicode_tra_len = api_ascii_to_unicode( tra_ptr, strlen(tra_ptr), unicode_tra ); 
				if( unicode_tra_len > 0 )
				{
					memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, 2*unicode_tra_len);
					unicodeDisplayLen += (2*unicode_tra_len);
				}				
			}
		}
	}

	unicodeDisplayLen += (2*api_ascii_to_unicode( asc_lst, asc_lst_len, unicodeDisplay+unicodeDisplayLen));
	
	display_unicode_str2( x, y, layer_id, uColor, uBgColor, unicodeDisplay, unicodeDisplayLen, fnt_type, xsize, ysize);
}

void OSD_StringDisplayWithID(int x,int y,int uColor, int uBgColor, int str_id, int fnt_type, int width)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;

	char* org_ptr;
	
	org_ptr = get_global_mesg_org_with_id(str_id);
	
	if( org_ptr != NULL )
	{
		unicode_org_len = api_ascii_to_unicode( org_ptr, strlen(org_ptr), unicode_org ); 

		if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
		{
			if( unicode_tra_len > 0 )
			{
				if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				{
					Arbic_Position_Process(unicode_tra,unicode_tra_len,unicode_tra,&unicode_tra_len);
					Unicode_Invert(unicode_tra,unicode_tra_len);
				}
				display_unicode_str( x, y, uColor, uBgColor, unicode_tra, unicode_tra_len, fnt_type, width);
			}
		}
		else
		{
			char* tra_ptr;
			tra_ptr = get_global_mesg_tra_with_id( str_id );
			if( tra_ptr != NULL )
			{
				unicode_tra_len = api_ascii_to_unicode( tra_ptr, strlen(tra_ptr), unicode_tra ); 
				if( unicode_tra_len > 0 )
				{
					display_unicode_str( x, y, uColor, uBgColor, unicode_tra, unicode_tra_len*2, fnt_type, width);
				}				
			}
		}
	}
}

void OSD_StringDisplayWithIcon(int x,int y,int uColor, int uBgColor, int icon, int fnt_type, int width)
{
	char unicode_buf[200];
	int unicode_len;
	
	if(api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, icon, NULL, NULL, unicode_buf,&unicode_len ))
	{
		if( unicode_len > 0 )
		{
			#if 0
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
			{
				Arbic_Position_Process(unicode_buf,unicode_len,unicode_buf,&unicode_len);
				Unicode_Invert(unicode_buf,unicode_len);
			}
			#endif
			display_ext_string( x,y,uColor, uBgColor, (char*)unicode_buf, unicode_len, fnt_type, STR_UNICODE, width);
		}
	}
}


void API_GetOSD_StringWithID(int str_id, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, char*pStr, int *pLen)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;
	char* org_ptr;
	char unicodeDisplay[400];
	int unicodeDisplayLen;
	
	*pLen = 0;
	if(pStr == NULL)
	{
		return;
	}
	
	unicodeDisplayLen = (2*api_ascii_to_unicode( asc_pre, asc_pre_len, unicodeDisplay));

	org_ptr = get_global_mesg_org_with_id(str_id);
	
	if( org_ptr != NULL )
	{
		unicode_org_len = api_ascii_to_unicode( org_ptr, strlen(org_ptr), unicode_org ); 

		if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
		{
			if( unicode_tra_len > 0 )
			{
				#if 0
				if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				{
					Arbic_Position_Process(unicode_tra,unicode_tra_len,unicode_tra,&unicode_tra_len);
					Unicode_Invert(unicode_tra,unicode_tra_len);
				}
				#endif
				memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, unicode_tra_len);
				unicodeDisplayLen += unicode_tra_len;
			}
		}
		else
		{
			char* tra_ptr;
			tra_ptr = get_global_mesg_tra_with_id( str_id );
			if( tra_ptr != NULL )
			{
				unicode_tra_len = api_ascii_to_unicode( tra_ptr, strlen(tra_ptr), unicode_tra ); 
				if( unicode_tra_len > 0 )
				{
					memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, 2*unicode_tra_len);
					unicodeDisplayLen += (2*unicode_tra_len);
				}				
			}
		}
	}

	unicodeDisplayLen += (2*api_ascii_to_unicode( asc_lst, asc_lst_len, unicodeDisplay+unicodeDisplayLen));

	*pLen = unicodeDisplayLen;
	memcpy(pStr, unicodeDisplay, *pLen);
	
}

void GetOSD_StringWithIcon(int icon, char* asc_pre, int asc_pre_len, char* asc_lst, int asc_lst_len, char*pStr, int *pLen)
{
	char unicode_tra[400];
	int unicode_tra_len;
	char unicodeDisplay[400];
	int unicodeDisplayLen;
	
	*pLen = 0;
	if(pStr == NULL)
	{
		return;
	}
	
	unicodeDisplayLen = 0;

	if(asc_pre != NULL && asc_pre_len != 0)
	{
		unicodeDisplayLen = (2*api_ascii_to_unicode( asc_pre, asc_pre_len, unicodeDisplay));
	}

	if(api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, icon, NULL, NULL, unicode_tra,&unicode_tra_len ))
	{
		if( unicode_tra_len > 0 )
		{
		#if 0
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				Unicode_Invert(unicode_tra,unicode_tra_len);
		#endif
			memcpy(unicodeDisplay+unicodeDisplayLen, unicode_tra, unicode_tra_len);
			unicodeDisplayLen += unicode_tra_len;
		}
	}
	
	if(asc_lst != NULL && asc_lst_len != 0)
	{
		unicodeDisplayLen += (2*api_ascii_to_unicode( asc_lst, asc_lst_len, unicodeDisplay+unicodeDisplayLen));
	}

	*pLen = unicodeDisplayLen;
	memcpy(pStr, unicodeDisplay, *pLen);
}


void OSD_StringClearExt(int x,int y, int width, int height)
{
	clear_ext_string( x,y,width,height,1);
}

void OSD_StringClearExt2(int x,int y, int layerId, int width, int height)
{
	clear_ext_string2( x,y,layerId,width,height,1);
}

void API_GetOSD_StringWithString(char* string, char*pStr, int* pLen)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;

	char unicodeDisplay[400] = {0};
	int unicodeDisplayLen = 0;
	
	
	if(pStr == NULL || string == NULL)
	{
		return;
	}

	unicode_org_len = api_ascii_to_unicode( string, strlen(string), unicode_org ); 
	
	if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
	{
		if( unicode_tra_len > 0 )
		{
			#if 0
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
				Unicode_Invert(unicode_tra,unicode_tra_len);
			#endif
			memcpy(unicodeDisplay, unicode_tra, unicode_tra_len);
			*pLen = unicode_tra_len;
		}
		memcpy(pStr, unicodeDisplay, unicode_tra_len);
	}
	else
	{
		*pLen = 2 * api_ascii_to_unicode( string, strlen(string), pStr );
	}
}

void UnicodeStringToUtf8(char* unicodeStr, int unicodeLen, char* utf8Str)
{
	int i, utf8Len;
	unsigned long unic;
	
	if(unicodeLen == NULL || utf8Str == NULL || unicodeStr == NULL)
	{
		return;
	}

	for(i = 0, utf8Len; i < unicodeLen; i += 2)
	{
		unic = unicodeStr[i] + (unicodeStr[i+1] << 8);
		utf8Len += enc_unicode_to_utf8_one(unic, utf8Str + utf8Len, 6);
	}
	utf8Str[utf8Len] = 0;
}


void API_GetOSD_Utf8StringWithString(char* string, char*pStr)
{
	char unicode_org[200];
	char unicode_tra[400];
	int unicode_org_len;
	int unicode_tra_len;

	int utf8Len = 0;
	int i;
	unsigned long unic;
	
	if(pStr == NULL || string == NULL)
	{
		return;
	}

	unicode_org_len = api_ascii_to_unicode( string, strlen(string), unicode_org ); 
	
	if( api_get_one_unicode_str( (char*)unicode_org, unicode_org_len*2, unicode_tra, &unicode_tra_len ) )
	{
		if( unicode_tra_len > 0 )
		{
			#if 0
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
			{
				Unicode_Invert(unicode_tra,unicode_tra_len);
			}
			#endif
			UnicodeStringToUtf8(unicode_tra, unicode_tra_len, pStr);
		}
	}
	else
	{
		strcpy(pStr, string);
	}
}
//for_alarming
int OSD_GetStringWithIcon(int icon,char *unicode_buf,int *unicode_len)
{
	//char unicode_buf[200];
	//int unicode_len;
	
	if(api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, icon, NULL, NULL, unicode_buf,unicode_len ))
	{
		if( *unicode_len > 0 )
		{
			return *unicode_len;
		}
	}
	return -1;
}