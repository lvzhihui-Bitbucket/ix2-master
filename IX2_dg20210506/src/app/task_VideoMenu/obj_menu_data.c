
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "obj_menu_data.h"
#include "obj_UnitSR.h"
#include "task_Power.h"
#include "MENU_public.h"
#include "vdp_uart.h"    //lyx 20170728

// lzh_20191009_s
//#include "../audio_service/obj_plugswitch.h"
#include "task_CallServer.h"
// lzh_20191009_e

int menu_on_state = 0;		// 0/off, 1/on

extern struOneMenuData 	OneMenuData;

void (*ptrMenuInit)(int uIconCnt) = NULL;
void (*ptrMenuProcess)(void* arg) = NULL;
void (*ptrMenuExit)(void) = NULL;

struMenuTrace MenuTrace={0};

struct timeval MenuValidTime = {0};
/*******************************************************************************************
 * @fn:		ResetMenuStack
 *
 * @brief:	�˵��켣��λ
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void ResetMenuStack(void)
{
	MenuTrace.uTraceCnt = 0;
}

void Empty_Function( void )	//cao_20151118
{
    ;
}

/*******************************************************************************************
 * @fn:		popDisplayLastMenu
 *
 * @brief:	������һ���˵�
 *
 * @param:  none
 *
 * @return: 1/���سɹ���0/����һ���˵�
 *******************************************************************************************/
unsigned char popDisplayLastMenu(void)
{
	unsigned char Menu;
	unsigned char Icon;
		
	if( MenuTrace.uTraceCnt > 0 )
	{
		MenuTrace.uTraceCnt--;		//return current
		Menu = MenuTrace.uMenuCnt[MenuTrace.uTraceCnt];
		Icon = MenuTrace.uIconCnt[MenuTrace.uTraceCnt];	
		if( !Menu && !Icon )
		{
			// power down
			CloseOneMenu();
			dprintf("return power down\n");			
		}
		else
		{
			StartInitOneMenu(Menu,Icon,0);
			dprintf("return menu data = %d \n",Menu);			
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		popDisplayLastMenu
 *
 * @brief:	�����ϼ����˵�
 *
 * @param:  nMenu �ϼ����˵�
 *
 * @return: 1/���سɹ���0/����һ���˵�
 *******************************************************************************************/
unsigned char popDisplayLastNMenu(uint8 nMenu)
{
	unsigned char Menu;
	unsigned char Icon;
		
	if( MenuTrace.uTraceCnt >= nMenu )
	{
		MenuTrace.uTraceCnt -= nMenu;
		Menu = MenuTrace.uMenuCnt[MenuTrace.uTraceCnt];
		Icon = MenuTrace.uIconCnt[MenuTrace.uTraceCnt];	
		if( !Menu && !Icon )
		{
			// power down
			CloseOneMenu();
			dprintf("return power down\n");			
		}
		else
		{
			StartInitOneMenu(Menu,Icon,0);
			dprintf("return menu data = %d \n",Menu);			
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

void GoHomeMenu(void)
{
	ResetMenuStack();
	StartInitOneMenu(MENU_001_MAIN,0,1);
}

/*******************************************************************************************
 * @fn:		pushIntoMenuStack
 *
 * @brief:	������һ���˵�ǰ������ǰ�˵�״̬���浽��ջ�ṹ��
 *
 * @param:  none
 *
 * @return: 1/���سɹ���0/ջ����
 *******************************************************************************************/
unsigned char pushIntoMenuStack(void)
{
	if( MenuTrace.uTraceCnt < MAX_MENU_STACK  )
	{
		if( MenuTrace.uTraceCnt == 0 )
		{
			MenuTrace.uMenuCnt[MenuTrace.uTraceCnt] 	= 0;
			MenuTrace.uIconCnt[MenuTrace.uTraceCnt] 	= 0;
		}
		else
		{
			MenuTrace.uMenuCnt[MenuTrace.uTraceCnt] 	= OneMenuData.cntMenu;
			MenuTrace.uIconCnt[MenuTrace.uTraceCnt] 	= OneMenuData.cntSubMenu;		//czn_20170301
		}
		MenuTrace.uTraceCnt++;			
		dprintf("menu stack data = %d \n",MenuTrace.uTraceCnt);
		return 1;
	}
	else
		return 0;
}

/*******************************************************************************************
 * @fn:		UpdateCurMenuStack
 *
 * @brief:	���µ�ǰ�˵�״̬
 *
 * @param:  	none
 *
 * @return: 	none
 *******************************************************************************************/
void UpdateCurMenuStack(void)
{
	if( MenuTrace.uTraceCnt  )
	{
		MenuTrace.uMenuCnt[MenuTrace.uTraceCnt-1] 	= OneMenuData.cntMenu;
		MenuTrace.uIconCnt[MenuTrace.uTraceCnt-1] 	= OneMenuData.cntSubMenu;		//czn_20170301
	}
}

/*******************************************************************************************
 * @fn��	SimuPushIntoMenuStack
 *
 * @brief:	��ָ���Ĳ˵�ָ�뱣�浽���� 
 *
 * @param:  	uMenu - �˵���ţ�uIcon - icon���
 *
 * @return: 	0/OK 1/ER
 *******************************************************************************************/
unsigned char SimuPushIntoMenuStack( unsigned short uMenu,unsigned short uIcon)
{
	if( MenuTrace.uTraceCnt < MAX_MENU_STACK  )
	{
		MenuTrace.uMenuCnt[MenuTrace.uTraceCnt] 	= uMenu;
		MenuTrace.uIconCnt[MenuTrace.uTraceCnt] 	= uIcon;
		MenuTrace.uTraceCnt++;
		return 1;
	}
	else
		return 0;
}

/*******************************************************************************************
 * @fn:		GetCurMenuLevel
 *
 * @brief:	�õ���ǰ�˵�����
 *
 * @param:  none
 *
 * @return:  ���ز˵�����
 *******************************************************************************************/
unsigned char GetCurMenuLevel(void)
{
	return MenuTrace.uTraceCnt;
}

unsigned char popLastMenu(void)
{
	unsigned char Menu;
	unsigned char Icon;
		
	if( MenuTrace.uTraceCnt > 0 )
	{
		MenuTrace.uTraceCnt--;		//return current
		Menu = MenuTrace.uMenuCnt[MenuTrace.uTraceCnt];
		Icon = MenuTrace.uIconCnt[MenuTrace.uTraceCnt];	
		return 1;
	}
	else
	{
		return 0;
	}
}
int getMenuStackLast(void)
{
	if( MenuTrace.uTraceCnt > 0 )
		return MenuTrace.uMenuCnt[MenuTrace.uTraceCnt-1];
	return -1;
}
void SetCurMenuCnt(unsigned short menu)
{
	OneMenuData.cntMenu = menu;
}


void EmptyFunction(void)
{

}
#if 0
const void* MENU_FUNCTION_TAB[][3] = 
{
	NULL,							NULL,								NULL,
	MENU_001_MAIN_Init, 			MENU_001_MAIN_Process,				MENU_001_MAIN_Exit,
	MENU_002_Monitor_Init, 			MENU_002_Monitor_Process,			MENU_002_Monitor_Exit,
	MENU_003_Intercom_Init, 		MENU_003_Intercom_Process,			MENU_003_Intercom_Exit,
	MENU_004_CallRecord_Init, 		MENU_004_CallRecord_Process,		MENU_004_CallRecord_Exit,
	MENU_005_CallScene_Init, 		MENU_005_CallScene_Process,			MENU_005_CallScene_Exit,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	MENU_009_SipConfig_Init, 		MENU_009_SipConfig_Process,			MENU_009_SipConfig_Exit,
	MENU_010_About_Init, 			MENU_010_About_Process,				MENU_010_About_Exit,
	MENU_011_Settings_Init, 		MENU_011_Settings_Process,			MENU_011_Settings_Exit,
	MENU_012_PublicSetting_Init, 	MENU_012_PublicSetting_Process,		MENU_012_PublicSetting_Exit,
	MENU_013_Calling_Init,			MENU_013_Calling_Process,			MENU_013_Calling_Exit,
	MENU_014_NameList_Init,			MENU_014_NameList_Process,			MENU_014_NameList_Exit,
	MENU_015_InstallSub_Init, 		MENU_015_InstallSub_Process,		MENU_015_InstallSub_Exit,
	NULL,							NULL,								NULL,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	MENU_019_Keypad_Init,			MENU_019_Keypad_Process,			MENU_019_Keypad_Exit,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_022_OnlineManage_Init,		MENU_022_OnlineManage_Process,		MENU_022_OnlineManage_Exit,
	MENU_023_CallRecordList_Init,	MENU_023_CallRecordList_Process,	MENU_023_CallRecordList_Exit,
	MENU_024_VideoRecordList_Init,	MENU_024_VideoRecordList_Process,	MENU_024_VideoRecordList_Exit,
	MENU_025_SearchOnline_Init,		MENU_025_SearchOnline_Process,		MENU_025_SearchOnline_Exit,
	MENU_026_PLAYBACK_Init, 		MENU_026_PLAYBACK_Process,			MENU_026_PLAYBACK_Exit,
	MENU_027_Calling2_Init,			MENU_027_Calling2_Process,			MENU_027_Calling2_Exit,
	MENU_028_Monitor2_Init,			MENU_028_Monitor2_Process,			MENU_028_Monitor2_Exit,
	MENU_029_SetDateTime_Init,		MENU_029_SetDateTime_Process,		MENU_029_SetDateTime_Exit,
	MENU_030_ShortcutSet_Init,		MENU_030_ShortcutSet_Process,		MENU_030_ShortcutSet_Exit,
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	NULL,							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_036_ListEdit_Init,			MENU_036_ListEdit_Process, 			MENU_036_ListEdit_Exit,
	MENU_037_MSList_Init,			MENU_037_MSList_Process, 			MENU_037_MSList_Exit,
	NULL,							NULL,								NULL,
	MENU_039_DIP_Help_Init,			MENU_039_DIP_Help_Process,			MENU_039_DIP_Help_Exit,	
	MENU_040_SDCardInfo_Init,		MENU_040_SDCardInfo_Process,		MENU_040_SDCardInfo_Exit,	
	NULL, 							NULL,								NULL,
	NULL, 							NULL,								NULL,
	NULL,							NULL,								NULL,
	MENU_044_VideoAdjust_Init,		MENU_044_VideoAdjust_Process, 		MENU_044_VideoAdjust_Exit,
	MENU_045_SDCard_Init,			MENU_045_SDCard_Process, 			MENU_045_SDCard_Exit,
	MENU_046_Restore_Init,			MENU_046_Restore_Process, 			MENU_046_Restore_Exit,
	MENU_047_PhoneManage_Init,		MENU_047_PhoneManage_Process, 		MENU_047_PhoneManage_Exit,
	MENU_048_DoorBell_Init,			MENU_048_DoorBell_Process, 			MENU_048_DoorBell_Exit,	//czn_20170808
	MENU_049_ProductionTest_Init,	MENU_049_ProductionTest_Process,	MENU_049_ProductionTest_Exit,
	MENU_050_FW_Update_Init,		MENU_050_FW_Update_Process, 		MENU_050_FW_Update_Exit,
	NULL,							NULL,								NULL,
	MENU_052_SystemSetting_Init,	MENU_052_SystemSetting_Process, 	MENU_052_SystemSetting_Exit,
	MENU_053_Divert_Init,			MENU_053_Divert_Process,			MENU_053_Divert_Exit,

	MENU_054_IPC_Setting_Init,		MENU_054_IPC_Setting_Process,		MENU_054_IPC_Setting_Exit,
	MENU_055_IPC_Login_Init,		MENU_055_IPC_Login_Process, 		MENU_055_IPC_Login_Exit,
	MENU_056_IPC_Monitor_Init,		MENU_056_IPC_Monitor_Process,		MENU_056_IPC_Monitor_Exit,
	MENU_057_IPC_List_Init, 		MENU_057_IPC_List_Process,			MENU_057_IPC_List_Exit,
	MENU_058_IPC_Manage_Init,		MENU_058_IPC_Manage_Process,		MENU_058_IPC_Manage_Exit,
	MENU_059_monListManage_Init,	MENU_059_monListManage_Process, 	MENU_059_monListManage_Exit,
	MENU_060_IPC_Searching_Init,	MENU_060_IPC_Searching_Process, 	MENU_060_IPC_Searching_Exit,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,
	MENU_065_InternetTime_Init, 	MENU_065_InternetTime_Process,		MENU_065_InternetTime_Exit,
		NULL,							NULL,								NULL,
		NULL,							NULL,								NULL,	
	EmptyFunction,					EmptyFunction,						EmptyFunction,	
		NULL,							NULL,								NULL,	
	MENU_070_BackFwUpgrade_Init,	MENU_070_BackFwUpgrade_Process, 	MENU_070_BackFwUpgrade_Exit,
	MENU_071_ParaManage_Init,		MENU_071_ParaManage_Process, 		MENU_071_ParaManage_Exit,
	MENU_072_OnePara_Init,			MENU_072_OnePara_Process, 			MENU_072_OnePara_Exit,
	MENU_073_ParaPublic_Init,		MENU_073_ParaPublic_Process, 		MENU_073_ParaPublic_Exit,
	MENU_074_RemoteParaManage_Init, MENU_074_RemoteParaManage_Process,	MENU_074_RemoteParaManage_Exit,
	MENU_075_OneRemotePara_Init,	MENU_075_OneRemotePara_Process, 	MENU_075_OneRemotePara_Exit,
		NULL,							NULL,								NULL,	
		NULL,							NULL,								NULL,	
	MENU_078_AutoCallbackList_Init, MENU_078_AutoCallbackList_Process,	MENU_078_AutoCallbackList_Exit,		//czn_20190216
		NULL,							NULL,								NULL,	
	MENU_080_IM_Extension_Init, 	MENU_080_IM_Extension_Process,		MENU_080_IM_Extension_Exit,		//czn_20190216
	MENU_081_OutdoorStation_Init, 	MENU_081_OutdoorStation_Process,	MENU_081_OutdoorStation_Exit,		//czn_20190216
	MENU_082_AutoSetupWizard_Init, 	MENU_082_AutoSetupWizard_Process,	MENU_082_AutoSetupWizard_Exit,		//czn_20190216
	MENU_083_Autotest_Init, 		MENU_083_Autotest_Process,			MENU_083_Autotest_Exit,		//czn_20190412
	MENU_084_AutotestDevSelect_Init, 	MENU_084_AutotestDevSelect_Process,		MENU_084_AutotestDevSelect_Exit,		//czn_20190412
	MENU_085_AutotestLogView_Init, 	MENU_085_AutotestLogView_Process,		MENU_085_AutotestLogView_Exit,
	MENU_086_CallTestStat_Init, 	MENU_086_CallTestStat_Process,		MENU_086_CallTestStat_Exit,	
	MENU_087_AutoTestTools_Init, 	MENU_087_AutoTestTools_Process,		MENU_087_AutoTestTools_Exit,
	MENU_088_PublicUnlockCode_Init, MENU_088_PublicUnlockCode_Process,	MENU_088_PublicUnlockCode_Exit,
	MENU_089_SwUpgrade_Init, 		MENU_089_SwUpgrade_Process,		MENU_089_SwUpgrade_Exit,
	MENU_090_CardManage_Init,		MENU_090_CardManage_Process,		MENU_090_CardManage_Exit, // 20190521
	MENU_091_ResSelect_Init,			MENU_091_ResSelect_Process,		MENU_091_ResSelect_Exit,
	MENU_092_ResSync_Init,				MENU_092_ResSync_Process,		MENU_092_ResSync_Exit,
	MENU_093_ResInfo_Init,				MENU_093_ResInfo_Process,		MENU_093_ResInfo_Exit,
	MENU_094_ResView_Init,				MENU_094_ResView_Process,		MENU_094_ResView_Exit,
	MENU_095_AddressHealthCheck_Init,	MENU_095_AddressHealthCheck_Process,MENU_095_AddressHealthCheck_Exit,
	MENU_096_SelectSystemType_Init,		MENU_096_SelectSystemType_Process,	MENU_096_SelectSystemType_Exit,
	MENU_097_IM_Call_Nbr_Init,			MENU_097_IM_Call_Nbr_Process,		MENU_097_IM_Call_Nbr_Exit,
	MENU_098_RES_Manger_Init,			MENU_098_RES_Manger_Process,		MENU_098_RES_Manger_Exit,
	MENU_099_BackupAndRestore_Init, 	MENU_099_BackupAndRestore_Process,	MENU_099_BackupAndRestore_Exit,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
		NULL,								NULL,								NULL,
	MENU_104_BackupAndRestore2_Init, 	MENU_104_BackupAndRestore2_Process,	MENU_104_BackupAndRestore2_Exit,
	MENU_105_backupList_Init, 			MENU_105_backupList_Process,		MENU_105_backupList_Exit,
	MENU_106_RestoreList_Init, 			MENU_106_RestoreList_Process,		MENU_106_RestoreList_Exit,
		NULL,								NULL,								NULL,
	MENU_108_QuickAccess_Init,			MENU_108_QuickAccess_Process,		MENU_108_QuickAccess_Exit,
	MENU_109_PLAYBACK_INFO_Init, 		MENU_109_PLAYBACK_INFO_Process,		MENU_109_PLAYBACK_INFO_Exit,
	MENU_110_CallIPC_Init, 				MENU_110_CallIPC_Process,			MENU_110_CallIPC_Exit,
	MENU_111_VideoProxySetting_Init,	MENU_111_VideoProxySetting_Process,	MENU_111_VideoProxySetting_Exit,
	MENU_112_GSList_Init,				MENU_112_GSList_Process,			MENU_112_GSList_Exit,
	MENU_113_MonQuart_Init,				MENU_113_MonQuart_Process,			MENU_113_MonQuart_Exit,
	MENU_114_WlanSetting_Init,			MENU_114_WlanSetting_Process,		MENU_114_WlanSetting_Exit,
	MENU_115_Wlan_IPC_List_Init,		MENU_115_Wlan_IPC_List_Process, 	MENU_115_Wlan_IPC_List_Exit,
	MENU_116_MultiRec_Init, 			MENU_116_MultiRec_Process,			MENU_116_MultiRec_Exit,
	MENU_117_RecQuad_Init, 				MENU_117_RecQuad_Process,			MENU_117_RecQuad_Exit,
	MENU_118_IpcScenario_Init, 			MENU_118_IpcScenario_Process,		MENU_118_IpcScenario_Exit,
	MENU_119_IpcRecord_Init,			MENU_119_IpcRecord_Process,			MENU_119_IpcRecord_Exit,
	MENU_120_BackResDownload_Init,		MENU_120_BackResDownload_Process,	MENU_120_BackResDownload_Exit,
	MENU_121_ResUdpDownload_Init,		MENU_121_ResUdpDownload_Process,	MENU_121_ResUdpDownload_Exit,
	MENU_122_DvrPlay_Init,				MENU_122_DvrPlay_Process,			MENU_122_DvrPlay_Exit,
	MENU_123_RecSchedule_Init,			MENU_123_RecSchedule_Process,		MENU_123_RecSchedule_Exit,
	MENU_124_WeekSelect_Init,			MENU_124_WeekSelect_Process,		MENU_124_WeekSelect_Exit,


};
#endif
extern const void* MENU_FUNCTION_TAB[][3];
#if 0
const uint16 NotCtrlPressReleaseDispIcon[] = {

	ICON_203_Monitor,
	ICON_204_quad,
	ICON_205_CallScene,
	ICON_206_Namelist,
	ICON_017_MissedCalls,
	ICON_018_IncomingCalls,
	ICON_019_OutgoingCalls,
	ICON_020_playback,
	ICON_024_CallTune,
	ICON_025_general,
	ICON_026_InstallerSetup,
	ICON_027_SipConfig,
	ICON_029_IP_ADDR,
	ICON_030_CALL_NUMBER,
	ICON_031_OnsiteTools,
	ICON_032_System,
	ICON_033_Manager,
	ICON_034_ListView,
	ICON_035_ListAdd,
	ICON_036_ListDelete,
	ICON_037_ListEdit,
	ICON_038_ListCheck,
	//ICON_400_ListSync,	//temp //czn_20190525
	ICON_051_ExternalUnit,
	ICON_052_IM_Process,
	ICON_053_OS_Process,
	ICON_054_IPC_Process,
	ICON_218_PublicSetTitle,
	ICON_229_ShortcutSet1,
	ICON_230_ShortcutSet2,
	ICON_231_ShortcutSet3,
	ICON_232_ShortcutSet4,
	ICON_256_Edit,
	ICON_265_IPC_Monitor,
	ICON_266_IPC_Edit,
	ICON_267_IPC_Delete,
	ICON_269_MonListSelect,
	ICON_270_MS_List,
	ICON_040_AboutPage1,
	ICON_041_AboutPage2,
	ICON_042_AboutPage3,
	ICON_043_AboutPage4,
	ICON_044_AboutPage5,
	ICON_045_Upgrade,
	ICON_048_Device,
	ICON_049_Parameter,
	ICON_050_Reboot,
	ICON_IdCard_List, // 20190521
	ICON_IdCard_Add,
	ICON_IdCard_DelAll,
	ICON_CallRecordDelAll, // zfz_20190601
	ICON_379_Shortcut5, // zfz_20190601
};

const uint16 NotCtrlPressReleaseDispIconNum = sizeof(NotCtrlPressReleaseDispIcon)/sizeof(uint16);
#endif
extern const uint16 NotCtrlPressReleaseDispIcon[];
extern const uint16 NotCtrlPressReleaseDispIconNum;

uint8 IfCtrlPressReleaseDisplay(uint16 icon)
{
	uint16 i;

	for(i = 0; i < NotCtrlPressReleaseDispIconNum; i++)
	{
		if(icon == NotCtrlPressReleaseDispIcon[i])
			return 0;
	}
	return 1;
}
// lzh_20161230_s
// uIcon ��������֮ǰδ�ã��ָ���Ϊ��̬�����Ӳ˵��ı�� (0��ʾ�Ӳ˵���Ч), 
// ��uIcon��Ϊ0�����ʾ�����Ӳ˵�����pushstackΪ1��ʾѹ��˵�ջ��ͬʱ��ʾΪ�˵�first��ʾ����Ҫ�����Ļ
// lzh_20161230_e
int SwitchOneMenu(int uMenu,int subMenu, int pushstack)
{	
	unsigned char menuDisplayResult;
	
	printf("======================SwitchOneMenu[%d-%d]======================================\n",uMenu, subMenu);

	if( uMenu >= MENU_DAT_MAX )
		return 0;

	UpdateMenuValidTime();		//czn_20170111
	#if 0
	if(menu_on_state == 0)
	{
		menu_on_state = 1;
		api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728
		api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728
		
		// lzh_20181016_s	�״ο������⴦��������ʾ�˵�Ϊ���˵���Menu_Display����ΪDisplayOneMenu����ֹ����
		if( uMenu == MENU_001_MAIN )
		{
			// lzh_20181016_s	
			//API_POWER_TFT_ON(); 	// ��ʾ���٣���ǰ�������⣬�ϵ��һ�ο��˵���Ҫ׼�������˵������Ա���ʾ����
			// lzh_20181016_e					
			#if 0
			if( pushstack ) pushIntoMenuStack();
			if( ptrMenuExit != NULL ) (*ptrMenuExit)();
			ptrMenuInit 	= (void(*)(int))MENU_FUNCTION_TAB[uMenu][0];
			ptrMenuProcess	= (void(*)(void*))MENU_FUNCTION_TAB[uMenu][1];
			ptrMenuExit 	= (void(*)(void))MENU_FUNCTION_TAB[uMenu][2];		
			AutoPowerOffReset();			
			DisplayOneMenu(uMenu,0);
			(*ptrMenuInit)(uMenu);		
			return 1;
			#endif
		}
		// lzh_20181016_e		
	}
	#endif
	if( pushstack )		pushIntoMenuStack();

	if( ptrMenuExit != NULL ) (*ptrMenuExit)();
		
	ptrMenuInit 	= (void(*)(int))MENU_FUNCTION_TAB[uMenu][0];
	ptrMenuProcess 	= (void(*)(void*))MENU_FUNCTION_TAB[uMenu][1];
	ptrMenuExit		= (void(*)(void))MENU_FUNCTION_TAB[uMenu][2];

	AutoPowerOffReset();	

	if( subMenu )
	{
		menuDisplayResult = Menu_Display2(uMenu,subMenu,pushstack);
	}
	else
	{
		menuDisplayResult = Menu_Display(uMenu);
	}
	if(menu_on_state == 0)
	{
		#if	defined(PID_DX470)||defined(PID_DX482)||defined(PID_IXSE_V2)
		LCD_POWER_SET();	// set low
		#if defined PID_DX470_V25
		LCD_RESET_RESET();
		usleep(20*1000);	// resest
		LCD_RESET_SET();
		#endif
		usleep(200*1000);	// sleep a while to enable backlight when lcd power on
		#endif	
		menu_on_state = 1;		
		api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728
		api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728
	}

	UpdateOsdLayerBuf_vram2extbuf(uMenu);

	(*ptrMenuInit)(uMenu);

	// cache delay to refresh display
	if( menuDisplayResult == 2 ) UpdateOsdLayerBuf(0);

	//EnableUpdate();
	
#if defined(PID_IXSE_V2)
	LCD_POWER_SET();
#endif
	
#if defined( PID_IXSE ) || defined( PID_IX481 )
	LCD_PWM_SET();
#endif	
}


int CloseMenu(void)
{	
	printf("Menu Close and power off...\n");
	TimeLapseHide();
	menu_on_state = 0;
	//API_POWER_TFT_OFF();
#if defined( PID_IXSE ) || defined( PID_IX481 )
	LCD_PWM_RESET();
#endif	

	api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728
	api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &menu_on_state, 1);    //lyx 20170728

	if( ptrMenuExit != NULL ) (*ptrMenuExit)();
	ptrMenuExit = NULL;

	ResetMenuStack();
	//Menu_Close();
	clearscreen(1);

	//ResetMenuValidTime();		//czn_20170111
	UpdateMenuValidTime();
	// czb_20171127_s
	//if( !IfCallServerBusy())		//lyx_20171218
	//{
	//	capture_turn_on_just_full();		
	//}
	// czb_20171127_e

#if	defined(PID_DX470)||defined(PID_DX482)
	usleep(100*1000);	// first close backlight, then close lcd power
	LCD_POWER_RESET();	// set high
	#if defined PID_DX470_V25
	LCD_RESET_RESET();
	#endif
#elif defined(PID_IXSE_V2)
	LCD_POWER_RESET();
#endif

	return 1;
}

int GetMenuOnState(void)
{
	return menu_on_state;
}

int power_Down_Timer_Cnt_Io = 60;
void AutoPowerOffReset(void)
{
	//Power_Down_Timer_Set(600/10);  // 60 ����Զ�����	
	if(power_Down_Timer_Cnt_Io<60)
		power_Down_Timer_Cnt_Io = 60;
	Power_Down_Timer_Set(power_Down_Timer_Cnt_Io);
	ResetNetworkCardCheck();
}

int IdleWinMsgProcessing(void* arg);
int sys_hook_processing(void* arg);

void WinMsgProcessing(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	if((pglobal_win_msg->type == MSG_7_BRD)||(pglobal_win_msg->happen_time.tv_sec > MenuValidTime.tv_sec)||
		(pglobal_win_msg->happen_time.tv_sec == MenuValidTime.tv_sec && pglobal_win_msg->happen_time.tv_usec > MenuValidTime.tv_usec))		//czn_20170111
	{
		if( !sys_hook_processing(arg) )
		{
			if( menu_on_state )
			{
				if( pglobal_win_msg->type == MSG_2_TKEY )
				{				
					int tp_result = TouchKeyMapToIcon( pglobal_win_msg->wparam, pglobal_win_msg->lparam);
					// ���ҵ�ǰ�˵�icon
					if( tp_result == 0 )
					{
						if(IfCtrlPressReleaseDisplay(GetCurIcon()))
						{
							//API_MenuIconRelease();	// �ͷ�ͼ����ʾ
						}
						
						// lzh_20181108_s
						if(  pglobal_win_msg->status == TOUCHDOUBLECLICK )
						{
							pglobal_win_msg->type 	= MSG_3_VKEY;
							// ����������в˵�
							pglobal_win_msg->status = TOUCHCLICK;
							pglobal_win_msg->wparam	= KEY_TP_DBCLICK;							
							printf("TOUCHDOUBLECLICK processing...\n");			
							(*ptrMenuProcess)(arg);							
						}
						else if( pglobal_win_msg->status == TOUCHCLICK )
						{	
							#if defined(PID_IX850)
							#else
							if(GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_027_CALLING2 || GetCurMenuCnt() == MENU_028_MONITOR2||GetCurMenuCnt() == MENU_142_DX_MONITOR)
								(*ptrMenuProcess)(arg);
							#endif
						}
						// lzh_20181108_e
					}
					else if( tp_result > 0 ) 
					{
						//czn_20190117_s
						if(  pglobal_win_msg->status == TOUCHDOUBLECLICK )
							pglobal_win_msg->status = TOUCHCLICK;
						//czn_20190117_e
						
						if( pglobal_win_msg->status == TOUCHPRESS )
						{
							if(GetCurIcon() >= ICON_888_ListView)
							{
								#if defined(PID_IX850)
								#else
								MenuListIconProcess(pglobal_win_msg->wparam, pglobal_win_msg->lparam, GetCurIcon());
								#endif
							}
							else
							{
								if(KeyBeepDisable==0)
									BEEP_KEY();
								
								if(IfCtrlPressReleaseDisplay(GetCurIcon()))
								{
									API_MenuIconPress();	// ѡ��ͼ����ʾ
								}
								#if defined(PID_IX850)
								#else
								if(GetCurMenuCnt() == MENU_122_DvrPlay)
									(*ptrMenuProcess)(arg);
								#endif
							}
						}
						else if( pglobal_win_msg->status == TOUCHCLICK )
						{
							AutoPowerOffReset();		//czn_20170120
							if(GetCurIcon() != ICON_888_ListView)
							{
								if(IfCtrlPressReleaseDisplay(GetCurIcon()))
								{
									API_MenuIconRelease();	// �ͷ�ͼ����ʾ
								}
							}
							
							(*ptrMenuProcess)(arg);
//							UpdateMenuValidTime();		//czn_20170306
						}
						else if( pglobal_win_msg->status == TOUCHMOVE )
						{
							#if defined(PID_IX850)
							#else
							if(GetCurMenuCnt() == MENU_122_DvrPlay)
								(*ptrMenuProcess)(arg);
							#endif
						}
					}
					else
					{
						printf("TouchKeyMapToIcon err...\n");					
					}
				}
				else if( pglobal_win_msg->type == MSG_3_VKEY )
				{
					if( pglobal_win_msg->status == TOUCHPRESS )
					{
						if(KeyBeepDisable==0)
							BEEP_KEY();
					}
					else if( pglobal_win_msg->status == TOUCHCLICK )
					{	
						AutoPowerOffReset();		//czn_20170120
						(*ptrMenuProcess)(arg);
//						UpdateMenuValidTime();		//czn_20170306
					}
					else if( pglobal_win_msg->status == TOUCH_3SECOND )	//czn_20190216
					{	
						//printf("!!!!!!recv TOUCH_3SECOND\n");
						AutoPowerOffReset();		//czn_20170120
						(*ptrMenuProcess)(arg);
//						UpdateMenuValidTime();		//czn_20170306
					}
				}
				else if( pglobal_win_msg->type == MSG_6_HOOK )
				{
					(*ptrMenuProcess)(arg);
//					UpdateMenuValidTime();		//czn_20170306
				}
				else if( pglobal_win_msg->type == MSG_7_BRD )
				{
					(*ptrMenuProcess)(arg);			
				}
				else if( pglobal_win_msg->type == MSG_8_DIP )
				{	
					AutoPowerOffReset();
					(*ptrMenuProcess)(arg);
//					UpdateMenuValidTime();
				}
			}
			else
			{
				IdleWinMsgProcessing(arg);
			}
		}
	}
	else
	{
		UpdateMenuValidTime();
	}
}

// ϵͳ������Ϣ����
int sys_hook_processing(void* arg)
{
	static int o_temp = 0;
	Ring2Msg_t* pRing2Msg;	

	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	int hook_ok = 0;
	#if defined(PID_IX850)
	#else
	if((pglobal_win_msg->type == MSG_2_TKEY || pglobal_win_msg->type == MSG_3_VKEY) && GetMessageDisplayState())
	{
		if( pglobal_win_msg->status == TOUCHPRESS )
		{
			BEEP_KEY();
		}
		else if(pglobal_win_msg->status == TOUCHCLICK)
		{
			MessageClose();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_MsgClose_TouchClick);
		}

		hook_ok = 1;
		return hook_ok;
	}
	#endif
	if(GetCurMenuCnt()==MENU_109_Info&&GetSavedIcon()==ICON_Help&&GetLastNMenu()==MENU_001_MAIN)
	{
		if((pglobal_win_msg->type == MSG_2_TKEY || pglobal_win_msg->type == MSG_3_VKEY)&&pglobal_win_msg->status == TOUCHCLICK)
		{
			hook_ok = 1;
			SaveIcon(0);
			popDisplayLastMenu();
			return hook_ok;
		}
	}
	if(GetDeviceRemoteManageFlag())
	{
		SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
		
		if(pglobal_win_msg->status == TOUCHCLICK)
		{
			BEEP_KEY();
			#if defined(PID_IX850)
			#else
			if(GetCurMenuCnt() == MENU_068_DeviceRemoteManage)
			{
				popDisplayLastMenu();
			}
			else
			{
				StartInitOneMenu(MENU_068_DeviceRemoteManage,0,1);
			}
			#endif
		}
		
		hook_ok = 1;
		
		return hook_ok;
	}

	if(GetMovementTestState())
	{
		hook_ok = 1;
	}
	else
	{
		return hook_ok;
	}

	
	if( menu_on_state )
	{
		if( pglobal_win_msg->type == MSG_2_TKEY )
		{				
			int tp_result = TouchKeyMapToIcon( pglobal_win_msg->wparam, pglobal_win_msg->lparam);
			// ���ҵ�ǰ�˵�icon
			if( tp_result == 0 )
			{
				if(IfCtrlPressReleaseDisplay(GetCurIcon()))
				{
					API_MenuIconRelease();	// �ͷ�ͼ����ʾ
				}
			}
			else if( tp_result > 0 ) 
			{
				if( pglobal_win_msg->status == TOUCHPRESS )
				{
					BEEP_KEY();
					
					if(IfCtrlPressReleaseDisplay(GetCurIcon()))
					{
						API_MenuIconPress();	// ѡ��ͼ����ʾ
					}
				}
				else if( pglobal_win_msg->status == TOUCHCLICK )
				{
					AutoPowerOffReset();		//czn_20170120
					if(IfCtrlPressReleaseDisplay(GetCurIcon()))
					{
						API_MenuIconRelease();	// �ͷ�ͼ����ʾ
					}
					
					(*ptrMenuProcess)(arg);
					//UpdateMenuValidTime();		//czn_20170306
				}
			}
			else
			{
				printf("TouchKeyMapToIcon err...\n");					
			}
		}
		else if( pglobal_win_msg->type == MSG_3_VKEY )
		{
			if( pglobal_win_msg->status == TOUCHPRESS )
			{
				BEEP_KEY();
			}
			else if( pglobal_win_msg->status == TOUCHCLICK )
			{	
				AutoPowerOffReset();		//czn_20170120
				(*ptrMenuProcess)(arg);
				//UpdateMenuValidTime();		//czn_20170306
			}
			else if( pglobal_win_msg->status == TOUCH_3SECOND )
			{	
				AutoPowerOffReset();		//czn_20170120
				(*ptrMenuProcess)(arg);
			}
			
		}
		else if( pglobal_win_msg->type == MSG_6_HOOK )
		{
			(*ptrMenuProcess)(arg);
			//UpdateMenuValidTime();		//czn_20170306
		}
		else if( pglobal_win_msg->type == MSG_7_BRD )
		{
			(*ptrMenuProcess)(arg); 		
		}
		else if( pglobal_win_msg->type == MSG_8_DIP )
		{	
			AutoPowerOffReset();
			(*ptrMenuProcess)(arg);
			//UpdateMenuValidTime();
		}
		else if( pglobal_win_msg->type == MSG_9_DB)
		{	
			AutoPowerOffReset();
			(*ptrMenuProcess)(arg);
			//UpdateMenuValidTime();
		}
		else if( pglobal_win_msg->type == MSG_10_ALARM )
		{
			(*ptrMenuProcess)(arg); 		
		}
	}
	else
	{
		if( pglobal_win_msg->type == MSG_3_VKEY )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
				case KEY_UNLOCK:
				case KEY_POWER:
				case KEY_MENU:
				case KEY_UP:	  
				case KEY_DOWN:
				case KEY_BACK:
				case KEY_NO_DISTURB:
					
					break;
			}
		}
		else if(pglobal_win_msg->type == MSG_8_DIP)
		{
		}
		else if(pglobal_win_msg->type == MSG_2_TKEY)
		{
			if( pglobal_win_msg->status == TOUCHCLICK)
			{
			
			}
		}
		else if(pglobal_win_msg->type == MSG_7_BRD)
		{
			switch(pglobal_win_msg->status)
			{
				#if defined(PID_IX850)
				default:
				break;
				#else
				case MSG_7_BRD_SUB_BecalledOn:
				case MSG_7_BRD_SUB_GLCallVdOn:	
					API_AutoAgingExit();
					#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
					if(GetCurMenuCnt() == MENU_028_MONITOR2 || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_026_PLAYBACK || GetCurMenuCnt() == MENU_156_PLAYBACK2)
					#else
					if(GetCurMenuCnt() == MENU_028_MONITOR2 || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_026_PLAYBACK )
					#endif
						StartInitOneMenu(MENU_027_CALLING2,MENU_031_CALLING3,0);

					else
						StartInitOneMenu(MENU_027_CALLING2,MENU_031_CALLING3,1);
					API_TimeLapseStart(COUNT_RUN_UP,0);
					break;
					
				case MSG_7_BRD_SUB_MonitorStart:
					StartInitOneMenu(MENU_051_MOVEMENT_TEST,0,1);
					break;
					
				case MSG_7_BRD_SUB_INFORM_ENTER_AUTO_AGING:
					API_AutoAgingEnter();
					break;
					
				case MSG_7_BRD_SUB_INFORM_AUTO_AGING:
					AutoAgingProcess();
					break;
				
				case MSG_7_BRD_SUB_INFORM_AUTO_AGING_TALK:
					//API_LocalMonitor_Talk_On();
					break;
				
				case MSG_7_BRD_SUB_MovementTestStart:
					StartInitOneMenu(MENU_051_MOVEMENT_TEST,0,1);
					break;
				
				case MSG_7_BRD_SUB_MonitorOff:
					API_TimeLapseHide();
					API_TimeLapseStop();
					break;
				case MSG_7_BRD_SUB_UNLOCK_SUCESSFULL:
					UnlockReport(0);
					break;
					
				case MSG_7_BRD_SUB_UNLOCK_UNSUCESSFULL:
					UnlockReport(1);
					break;
				
				case MSG_7_BRD_SUB_RING_REQ:
					RingReqReport(GetMenuInformData(arg), 5);
					break;
				case MSG_7_BRD_SUB_RING2_REQ:
					pRing2Msg = GetMenuInformData(arg);
					RingReqReport(pRing2Msg->msg, pRing2Msg->time);
					break;

				case MSG_7_BRD_SUB_PROG_CALL_NBR:
					ProgCallNbrReqReport();
					SetIP_ADDR_ByDS1();
					break;
				#endif
			}
		}
	}
	
	return hook_ok;	
}

int IdleWinMsgProcessing(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int ch; int min;} *pData;
	Ring2Msg_t* pRing2Msg;	

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		switch( pglobal_win_msg->wparam )
		{
			case KEY_TALK:
			case KEY_UNLOCK:
			case KEY_POWER:
			case KEY_MENU:
			case KEY_UP:      
			case KEY_DOWN:
			case KEY_BACK:
			case KEY_NO_DISTURB:


				if( pglobal_win_msg->status == TOUCHCLICK)		//czn_20170602
				{
					#if defined(PID_DX470)||defined(PID_DX482)
					if(DtSrInUse()==TRUE)
					{
						BEEP_ERROR();
						return;
					}
					#endif
					// zfz_20190601
					if(pglobal_win_msg->wparam == KEY_TALK)
					{
						if(KeyBeepDisable==0)
							BEEP_KEY();
						//API_MonQuart_start();
						#if 1
						if( StarFastKeyMon() == 0 )
						{
							
						}
						else
						{
							StartInitOneMenu(MENU_002_MONITOR,0,1);
						}
						#endif
						break;
					}
					else if(pglobal_win_msg->wparam == KEY_UNLOCK)
					{
						#if (defined(PID_IX850) || defined(PID_DX470)||defined(PID_DX482))
						#else
						BEEP_KEY();
						#if 1
						if(JudgeIsInstallerMode() || API_AskDebugState(100))
						{
							StartInitOneMenu(MENU_108_QuickAccess,0,1);
						}
						#endif
						#endif
					}
					//StartInitOneMenu(MENU_001_MAIN,0,1);
					/*
					if(SR_State.in_use != TRUE)
					{
						if(pglobal_win_msg->wparam == KEY_TALK)
						{
							//API_Event_LocalMonitor_On(DS1_ADDRESS);
							break;
						}
						StartInitOneMenu(MENU_001_MAIN,0,1);
					}
					else
					{
						BEEP_ERROR();
					}
					*/
				}
				else if( pglobal_win_msg->status == TOUCH_3SECOND )
				{
					if(pglobal_win_msg->wparam == KEY_UNLOCK)
					{
						#ifdef PID_IXSE
						HardwareRestar();	
						#endif
					}
				}

				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_6_HOOK)
	{
		
		if( pglobal_win_msg->status == HOOK_STATUS_UP)
		{
			#if 0
			if(GetResDownloadState())
			{
				StartInitOneMenu(MENU_120_BackResDownload,0,1);
			}
			else if(GetResUdpDownloadState())
			{
				StartInitOneMenu(MENU_121_UdpResDownload,0,1);
			}
			else
			#endif	
			#if defined(PID_DX470)||defined(PID_DX482)
			if(DtSrInUse()==TRUE)
			{
				BEEP_ERROR();
				return;
			}
			#endif
			{
				StartInitOneMenu(MENU_001_MAIN,0,1);
			}
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{

/*
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
		if(SR_State.in_use != TRUE)
		{
			EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
		}
		else
		{
			BEEP_ERROR();
		}
*/
	}
	else if(pglobal_win_msg->type == MSG_2_TKEY)
	{

		if( pglobal_win_msg->status == TOUCHCLICK)
		{	
			#if defined(PID_DX470)||defined(PID_DX482)
			if(DtSrInUse()==TRUE)
			{
				BEEP_ERROR();
				return;
			}
			#endif
			StartInitOneMenu(MENU_001_MAIN,0,1);
			/*
			if(SR_State.in_use != TRUE)
			{
				StartInitOneMenu(MENU_001_MAIN,0,1);
			}
			else
			{
				BEEP_ERROR();
			}
			*/
		}
	}
	else if(pglobal_win_msg->type == MSG_7_BRD)
	{
		switch(pglobal_win_msg->status)
		{
			#if defined(PID_IX850)
			default:
			break;
			#else
			case MSG_7_BRD_SUB_MonitorStart:
				//StartInitOneMenu(MENU_028_MONITOR2,0,1);
				break;
			case MSG_7_BRD_SUB_BecalledOn:
			case MSG_7_BRD_SUB_GLCallVdOn:	
				API_AutoAgingExit();
				StartInitOneMenu(MENU_027_CALLING2,0,1);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
				
			case MSG_7_BRD_SUB_IntercomBeCallOn:
			case MSG_7_BRD_SUB_InnerBeCallOn:
			case MSG_7_BRD_SUB_GLCallOn:	
			//case MSG_7_BRD_SUB_InnerBeBrdCallOn:
				StartInitOneMenu(MENU_013_CALLING,0,1);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
				
			//case MSG_7_BRD_SUB_BeCalledDivert:
			case MSG_7_BRD_SUB_BecalledOff:
			case MSG_7_BRD_SUB_GLCallOff:
				popDisplayLastMenu();
				//CloseOneMenu();
				break;

			case MSG_7_BRD_SUB_BeCalledDivert:		//czn_20181219
				StartInitOneMenu(MENU_053_DIVERT,0,1);
				break;

			case MSG_7_BRD_SUB_RecvSoftwareUpdateReq:		//czn_20190506
				StartInitOneMenu(MENU_089_SoftwareUpdate,0,1);
				break;
			
			case MSG_7_BRD_SUB_UNLOCK_SUCESSFULL:
				StartInitOneMenu(MENU_001_MAIN,0,1);
				UnlockReport(0);
				break;
				
			case MSG_7_BRD_SUB_UNLOCK_UNSUCESSFULL:
				StartInitOneMenu(MENU_001_MAIN,0,1);
				UnlockReport(1);
				break;
				
			case MSG_7_BRD_SUB_RING_REQ:
				StartInitOneMenu(MENU_001_MAIN,0,1);
				RingReqReport(GetMenuInformData(arg), 5);
				break;
			case MSG_7_BRD_SUB_RING2_REQ:
				StartInitOneMenu(MENU_001_MAIN,0,1);
				pRing2Msg = GetMenuInformData(arg);
				RingReqReport(pRing2Msg->msg, pRing2Msg->time);
				break;

			case MSG_7_BRD_SUB_PROG_CALL_NBR:
				StartInitOneMenu(MENU_001_MAIN,0,1);
				ProgCallNbrReqReport();
				SetIP_ADDR_ByDS1();
				break;
			
			case MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_START:
			case MSG_7_BRD_SUB_OtaClientIfc_ActOk:
			case MSG_7_BRD_SUB_OtaClientIfc_Translated:
			case MSG_7_BRD_SUB_OtaClientIfc_Translating:
			case MSG_7_BRD_SUB_OtaClientIfc_Error:
				ResUpgradeInform_Operation(pglobal_win_msg->status, arg);
				break;
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_START:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_RATES:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_ERROR:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_CANCEL:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_END:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_ERROR:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_START:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_OK:
			case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_WAITING_REBOOT:
				ResUdpDownloadInformOperation(pglobal_win_msg->status, arg);
				break;
			
			case MSG_7_BRD_SUB_DVR_SCHEDULE:
				//DvrInformOperation();
				break;
			case MSG_7_BRD_SUB_DVR_SPACE_CHECK:
				Check_OneRec_Space(3072, 10);
				break;
			case MSG_7_BRD_SUB_DVR_STOP:
				API_OneRec_stop(*(char*)(arg+sizeof(SYS_WIN_MSG)));
				//pData = (arg + sizeof(SYS_WIN_MSG));
				//API_OneRec_start(pData->ch, pData->min);
				break;
			case MSG_7_BRD_SUB_ListenTalkStart:
				StartInitOneMenu(MENU_141_ListenTalk,0,1);
				break;
			#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			case MSG_7_BRD_Disarmed_Delay:
				if(GetAlarmingState()==1)
				{
					SetDisarmMenuSub(0);
					StartInitOneMenu(MENU_153_DisarmingSubKeyPad,0,1);
				}
				break;
			#endif
			#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			case MSG_7_BRD_UnlcokEvent:
				getRecFileProcess();
				break;	
			#endif
			#endif
			}
	}
	return 0;
}

//czn_20170111_s
//#include <time.h>
void UpdateMenuValidTime(void)
{
	struct timeval timetemp;
	//if(menu_on_state)	//czn_20170316
	{
		if(gettimeofday(&timetemp,NULL) == 0)
		{
			MenuValidTime = timetemp;		//czn_20170111
		}
	}
}
void ResetMenuValidTime(void)
{
	MenuValidTime.tv_sec = 0;
	MenuValidTime.tv_usec = 0;
	bprintf("UpdateMenuValidTime clock = %d:%d\n",MenuValidTime.tv_sec,MenuValidTime.tv_usec);
}
//czn_20170111_e

void DefaultPublicVkeyProcessing(int vkey)
{
	switch( vkey )
	{
		case KEY_POWER:		//czn_20170306
			CloseOneMenu();	
			break;
		case KEY_BACK:		
			popDisplayLastMenu();
			break;	
		case KEY_NO_DISTURB:
			break;
		case KEY_TALK:
			//StartMonlistMonitor(0);
			// zfz_20190601
			#if defined(PID_IX850)
			#else
			StarFastKeyMon();
			#endif
			break;
		case KEY_UNLOCK:		
		case KEY_MENU:      
		case KEY_UP:        
		case KEY_DOWN:
			break;
		// lzh_20181108_s
		case KEY_TP_DBCLICK:
			API_menu_create_bitmap_file(-1,-1,-1);			
			break;
		// lzh_20181108_e
	}
}

void DefaultPublicInformProcessing(int inform,  void* arg)
{
	struct {int ch; int min;} *pData;
	Ring2Msg_t* pRing2Msg;	

	switch(inform)
	{
	#if defined(PID_IX850)
		default:
		break;
	#else
		case MSG_7_BRD_SUB_MonitorStart:
			if(GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_028_MONITOR2||GetCurMenuCnt() == MENU_142_DX_MONITOR)
				Quad_Monitor_Timer_Init();
			//StartInitOneMenu(MENU_028_MONITOR2,0,1);
			break;

		case MSG_7_BRD_SUB_BecalledOn:
		case MSG_7_BRD_SUB_GLCallVdOn:
			API_AutoAgingExit();
			#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			if(GetCurMenuCnt() == MENU_028_MONITOR2 || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_026_PLAYBACK || GetCurMenuCnt() == MENU_156_PLAYBACK2)
			#else
			if(GetCurMenuCnt() == MENU_028_MONITOR2 || GetCurMenuCnt() == MENU_056_IPC_MONITOR || GetCurMenuCnt() == MENU_113_MonQuart || GetCurMenuCnt() == MENU_026_PLAYBACK )
			#endif
				StartInitOneMenu(MENU_027_CALLING2,0,0);

			else
				StartInitOneMenu(MENU_027_CALLING2,0,1);
			API_TimeLapseStart(COUNT_RUN_UP,0);
			break;

		case MSG_7_BRD_SUB_InnerCallStart:
		case MSG_7_BRD_SUB_IntercomCallStart:
			StartInitOneMenu(MENU_013_CALLING,0,1);
			break;

		case MSG_7_BRD_SUB_IntercomBeCallOn:
		case MSG_7_BRD_SUB_InnerBeCallOn:
		case MSG_7_BRD_SUB_InnerBeBrdCallOn:
		case MSG_7_BRD_SUB_GLCallOn:	
			StartInitOneMenu(MENU_013_CALLING,0,1);
			API_TimeLapseStart(COUNT_RUN_UP,0);
			break;
			
		//case MSG_7_BRD_SUB_BeCalledDivert:
		case MSG_7_BRD_SUB_BecalledOff:
		case MSG_7_BRD_SUB_GLCallOff:	
			//CloseOneMenu();
			popDisplayLastMenu();
			break;
			
		case MSG_7_BRD_SUB_BeCalledDivert:		//czn_20181219
			StartInitOneMenu(MENU_053_DIVERT,0,1);
			break;	
			
		case MSG_7_BRD_SUB_FW_UpdateStart:
			CloseOneMenu();
			break;

 		case MSG_7_BRD_SUB_INFORM_ENTER_AUTO_AGING:
			API_AutoAgingEnter();
			break;
			
		case MSG_7_BRD_SUB_INFORM_AUTO_AGING:
			AutoAgingProcess();
			break;

		case MSG_7_BRD_SUB_INFORM_AUTO_AGING_TALK:
			//API_LocalMonitor_Talk_On();
			break;
			
		case MSG_7_BRD_SUB_MovementTestStart:
			StartInitOneMenu(MENU_051_MOVEMENT_TEST,0,1);
			break;

		case MSG_7_BRD_SUB_RecvSoftwareUpdateReq:		//czn_20190506
			StartInitOneMenu(MENU_089_SoftwareUpdate,0,1);
			break;	

		// lzh_20191009_s
		case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP:
			// ���ժ��
			if(CallServer_Run.state == CallServer_Ring)
			{
				API_CallServer_LocalAck(CallServer_Run.call_type);
			}
			else if(CallServer_Run.state == CallServer_Ack)
			{
				// ͨ��״̬�£������ֱ�ͨ��״̬��ˢ����Ƶͨ���л�
				api_plugswitch_control_update(2,1);
			}
			break;
		case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN:
			// ��ɹһ�
			if(CallServer_Run.state == CallServer_Ack)
			{
				API_CallServer_LocalBye(CallServer_Run.call_type);
			}				
			break;
		// lzh_20191009_e
		case MSG_7_BRD_SUB_UNLOCK_SUCESSFULL:
			UnlockReport(0);
			break;
			
		case MSG_7_BRD_SUB_UNLOCK_UNSUCESSFULL:
			UnlockReport(1);
			break;
		
		case MSG_7_BRD_SUB_RING_REQ:
			RingReqReport(GetMenuInformData(arg), 5);
			break;
		case MSG_7_BRD_SUB_RING2_REQ:
			pRing2Msg = GetMenuInformData(arg);
			RingReqReport(pRing2Msg->msg, pRing2Msg->time);
			break;
		case MSG_7_BRD_SUB_PROG_CALL_NBR:
			ProgCallNbrReqReport();
			SetIP_ADDR_ByDS1();
			break;
		
		case MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_START:
			ResUpgradeInform_Operation(inform, arg);
			StartInitOneMenu(MENU_120_BackResDownload,0,1);
			break;
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_START:
			ResUdpDownloadInformOperation(inform, arg);
			StartInitOneMenu(MENU_121_UdpResDownload,0,1);
			break;
		case MSG_7_BRD_SUB_DVR_SCHEDULE:
			//DvrInformOperation();
			break;
		case MSG_7_BRD_SUB_DVR_SPACE_CHECK:
			Check_OneRec_Space(3072, 10);
			break;
		case MSG_7_BRD_SUB_DVR_STOP:
			API_OneRec_stop(*(char*)(arg+sizeof(SYS_WIN_MSG)));
			//pData = (arg + sizeof(SYS_WIN_MSG));
			//API_OneRec_start(pData->ch, pData->min);
			break;

		case MSG_7_BRD_SUB_ListenTalkStart:
			StartInitOneMenu(MENU_141_ListenTalk,0,1);
			break;
		#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			case MSG_7_BRD_Disarmed_Delay:
				if(GetAlarmingState()==1)
				{
					SetDisarmMenuSub(0);
					StartInitOneMenu(MENU_153_DisarmingSubKeyPad,0,1);
				}
				break;
		#endif
		#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			case MSG_7_BRD_UnlcokEvent:
				getRecFileProcess();
				break;	
		#endif
		#endif
			
	}	
}

//czn_20170313_s
#define PromptBox_AxisX			66
#define PromptBox_AxisY			70
#define PromptBoxChar_AxisX		80
#define PromptBoxChar_AxisY		90

int PromptBox_Display(int boxid)
{
	switch(boxid)
	{	
		case PromptBox_Linking:
			API_SpriteDisplay_XY(PromptBox_AxisX,PromptBox_AxisY,SPRITE_LINK_BOX);
			break;
		case PromptBox_Fail:
			API_SpriteDisplay_XY(PromptBox_AxisX,PromptBox_AxisY,SPRITE_FAIL_BOX);
			break;
		case PromptBox_Unlock:
			API_SpriteDisplay_XY(PromptBox_AxisX,PromptBox_AxisY,SPRITE_UNLOCK_BOX);
			break;
		case PromptBox_Fail2:		//czn_20170328
			API_SpriteDisplay_XY(PromptBox_AxisX,PromptBox_AxisY,SPRITE_FAIL2_BOX);
			break;
	}
	//API_OsdStringDisplay(PromptBoxChar_AxisX,PromptBoxChar_AxisY,COLOR_WHITE,disp);
	return 0;
}

int PromptBox_Hide(int boxid)
{
	switch(boxid)
	{	
		case PromptBox_Linking:
			API_SpriteClose(PromptBox_AxisX,PromptBox_AxisY,SPRITE_LINK_BOX);
			break;
		case PromptBox_Fail:
			API_SpriteClose(PromptBox_AxisX,PromptBox_AxisY,SPRITE_FAIL_BOX);
			break;
		case PromptBox_Unlock:
			API_SpriteClose(PromptBox_AxisX,PromptBox_AxisY,SPRITE_UNLOCK_BOX);
			break;
		case PromptBox_Fail2:		//czn_20170328
			API_SpriteClose(PromptBox_AxisX,PromptBox_AxisY,SPRITE_FAIL2_BOX);
			break;
	}
	return 0;
}

int IdleMenuUnlock(unsigned short gw_id,unsigned short res_id,int lockno)	//czn_20170328
{
	Global_Addr_Stru addr;
	unsigned char link_result;
	
	PromptBox_Display(PromptBox_Linking);

	
	
	addr.ip = GatewayId_Trs2_IpAddr(gw_id);		//czn_20170118
	addr.gatewayid = gw_id;

	
	addr.rt = 10;
	addr.code = res_id - 1;
	if((link_result = DeviceManageSingleLinkingReq(addr.ip,320+addr.code)) == 0)
	{
//cao_20170509		if(Send_UnlockCmd(&addr,lockno) != 0)
		{
			PromptBox_Hide(PromptBox_Linking);
			PromptBox_Display(PromptBox_Fail);
			API_Beep(BEEP_TYPE_DI3);
			usleep(2000000);
			PromptBox_Hide(PromptBox_Fail);
			return -1;
		}
//cao_20170509		else
		{
			usleep(500000);
			PromptBox_Hide(PromptBox_Linking);
			PromptBox_Display(PromptBox_Unlock);
			API_Beep(BEEP_TYPE_DL1);
			usleep(2000000);
			PromptBox_Hide(PromptBox_Unlock);
			return 0;
		}
	}
	else if(link_result == 1)
	{
		PromptBox_Hide(PromptBox_Linking);
		PromptBox_Display(PromptBox_Fail2);
		API_Beep(BEEP_TYPE_DI3);
		usleep(2000000);
		PromptBox_Hide(PromptBox_Fail2);
		return -1;
	}
	else
	{
		PromptBox_Hide(PromptBox_Linking);
		PromptBox_Display(PromptBox_Fail);
		API_Beep(BEEP_TYPE_DI3);
		usleep(2000000);
		PromptBox_Hide(PromptBox_Fail);
		return -1;
	}
}
//czn_20170313_e
//czn_20170406_s
#define ListPageNum_MaxCur_WithTotal		99
#define ListPageNum_MaxCurNum				9999
#define ListPageNum_MaxTotalNum			99
#define ListPageNum_MaxDispLength			6
int MenuListPageNum_Display(int disp_x,int disp_y,int disp_color,int cur_page,int total_page)
{
	char disp_buf[15] = {0};
	char cur_num_disp[ListPageNum_MaxDispLength + 1] = {0};
	char total_num_disp[ListPageNum_MaxDispLength + 1] = {0};
	int disp_len;
	
	if(cur_page > ListPageNum_MaxCur_WithTotal)
	{
		total_num_disp[0] = 0;
	}
	else
	{
		if(total_page > ListPageNum_MaxTotalNum)
		{
			snprintf(total_num_disp,ListPageNum_MaxDispLength,"/%d+",ListPageNum_MaxTotalNum);
		}
		else
		{
			snprintf(total_num_disp,ListPageNum_MaxDispLength,"/%d",total_page);
		}
	}

	if(cur_page > ListPageNum_MaxCurNum)
	{
		snprintf(cur_num_disp,ListPageNum_MaxDispLength,"%d+",ListPageNum_MaxCurNum);
	}
	else
	{
		snprintf(cur_num_disp,ListPageNum_MaxDispLength,"%d",cur_page);
	}

	disp_len = strlen(total_num_disp) + strlen(cur_num_disp);

	snprintf(disp_buf,15,"%s%s     ",cur_num_disp,total_num_disp);

	API_OsdStringDisplayExt(disp_x, disp_y, disp_color, disp_buf, strlen(disp_buf),1,STR_UTF8, 0);
	return 0;
}

// lzh_20181108_s
#define KEY_DBCLICK_DEVIATION	20			// pixels
#define KEY_DBCLICK_PERIOD		300*1000	// us
#define KEY_DBCLICK_TIMES		2			// times	
int check_click_continue( HAL_MSG_TYPE *ptr_key_msg )
{
	static HAL_MSG_TYPE 	key_msg_last;
	static int				key_msg_times 	= 0;

	if( ptr_key_msg->key.keyType != KEY_TYPE_TP || ptr_key_msg->key.keyStatus != TOUCHCLICK )
		return;
		
	if( key_msg_times == 0 )
	{
		key_msg_last = *ptr_key_msg;
		key_msg_times++;
	}
	else if( key_msg_times )
	{
		// check the same key message
		if( ( abs(key_msg_last.key.TpKey.x-ptr_key_msg->key.TpKey.x) <= KEY_DBCLICK_DEVIATION ) && ( abs(key_msg_last.key.TpKey.y-ptr_key_msg->key.TpKey.y) <= KEY_DBCLICK_DEVIATION ) )
		{
			int diff;
			diff = (ptr_key_msg->happen_time.tv_sec - key_msg_last.happen_time.tv_sec)*1000000 + ptr_key_msg->happen_time.tv_usec - key_msg_last.happen_time.tv_usec;
			if( diff <= KEY_DBCLICK_PERIOD )		//  100ms
			{
				key_msg_times++;
				if( key_msg_times >= KEY_DBCLICK_TIMES )
				{
					ptr_key_msg->key.keyStatus = TOUCHDOUBLECLICK;
					key_msg_times = 0;
					return 1;
				}
				else
				{
					key_msg_last = *ptr_key_msg;
				}
			}
			else
			{
				key_msg_times = 1;
				key_msg_last = *ptr_key_msg;
			}
		}
		else
		{
			key_msg_times = 1;
			key_msg_last = *ptr_key_msg;
		}
	}
	return 0;
}

#define KEY_ID_EXPIRES		1000*1000	// us
#define KEY_ID_TIMES		2			// times	
int check_key_id_contineous( int stream_id, struct timeval stream_id_t2, int target_id )
{
	static struct timeval t1;
	static int 	key_id_last = 0;
	static int	key_id_times = 0;

	if( key_id_times == 0 )
	{
		t1 = stream_id_t2;
		key_id_last = stream_id;
		key_id_times++;
	}
	else if( key_id_times )
	{
		// check the same key
		if( stream_id == key_id_last )
		{
			int diff;
			diff = (stream_id_t2.tv_sec - t1.tv_sec)*1000000 + stream_id_t2.tv_usec - t1.tv_usec;
			if( diff <= KEY_ID_EXPIRES )
			{
				key_id_times++;
				if( key_id_times >= KEY_DBCLICK_TIMES )
				{
					key_id_times = 0;
					if( stream_id == target_id )
						return 1;
					else
						return 0;
				}
				else
				{
					t1 = stream_id_t2;
					key_id_last = stream_id;
				}
			}
			else
			{
				key_id_times = 1;
				t1 = stream_id_t2;
				key_id_last = stream_id;
			}
		}
		else
		{
			key_id_times = 1;
			t1 = stream_id_t2;
			key_id_last = stream_id;
		}
	}
	return 0;
}

// lzh_20181108_e


unsigned char DipMenuEnable = 0;  
int	MenuHalMessageProcessing( char* pdata, int len )
{
	HAL_MSG_TYPE*	pHalType	= (HAL_MSG_TYPE*)pdata;
	SYS_WIN_MSG 	global_win_msg;
	char msgTemp[500];
	if( pHalType->head.msg_type == HAL_TYPE_TOUCH_KEY )
	{		
		switch(pHalType->key.keyType)
		{
			case KEY_TYPE_TS:
				global_win_msg.type 	= MSG_3_VKEY;	
				global_win_msg.status	= pHalType->key.keyStatus;								
				global_win_msg.wparam	= pHalType->key.keyData;
				global_win_msg.lparam	= 0;
				global_win_msg.happen_time = pHalType->happen_time;
				WinMsgProcessing(&global_win_msg);
				break;
			case KEY_TYPE_TP:
				// lzh_20181108_s
				check_click_continue(pHalType);
				// lzh_20181108_e
				global_win_msg.type 	= MSG_2_TKEY;	
				global_win_msg.status	= pHalType->key.keyStatus;								
				global_win_msg.wparam = pHalType->key.TpKey.x;
				global_win_msg.lparam	= pHalType->key.TpKey.y;
				global_win_msg.happen_time = pHalType->happen_time;
				WinMsgProcessing(&global_win_msg);
				break;
			case KEY_TYPE_DIP:
				if( DipMenuEnable )    //�״��ϵ粻��DIP Menu    lyx_20171116
				{
					global_win_msg.type 	= MSG_8_DIP;	
					global_win_msg.status	= pHalType->key.keyData;								
					global_win_msg.happen_time = pHalType->happen_time;
					WinMsgProcessing(&global_win_msg);
				}
				DipMenuEnable = 1;
				break;
			case KEY_TYPE_DB:
				if(pHalType->key.keyData == 1)
				{
					if(GetMovementTestState())
					{
						global_win_msg.type 	= MSG_9_DB;	
						global_win_msg.status	= pHalType->key.keyData;								
						global_win_msg.happen_time = pHalType->happen_time;
						WinMsgProcessing(&global_win_msg);
					}
					else
					{
						#if defined(PID_IX850)
						#else
						Api_DoorBell_LocalTouch();
						#endif
					}
				}
				break;
			case KEY_TYPE_BOOT:
				#if	defined(PID_DX470)||defined(PID_DX482)||defined(PID_IXSE_V2)
				usleep(100*1000);
				LCD_POWER_SET();
				usleep(100*1000);	// first close backlight, then close lcd power
				LCD_POWER_RESET();	// set high
				usleep(100*1000);
				#endif
				InitMovementTest();
				break;
			case KEY_TYPE_HK:
				global_win_msg.type 	= MSG_6_HOOK;	
				global_win_msg.status	= pHalType->key.keyData;								
				global_win_msg.happen_time = pHalType->happen_time;
				WinMsgProcessing(&global_win_msg);
				break;
			case KEY_TYPE_ALARM:
				global_win_msg.type 	= MSG_10_ALARM; 
				global_win_msg.status	= pHalType->key.keyData;								
				global_win_msg.happen_time = pHalType->happen_time;
				WinMsgProcessing(&global_win_msg);
				break;
		}
	}
	else if( pHalType->head.msg_type == HAL_TYPE_BRD)
	{
		global_win_msg.type 	= MSG_7_BRD;	
		global_win_msg.status	= pHalType->head.msg_sub_type;
		global_win_msg.happen_time = pHalType->happen_time;
		WinMsgProcessing(&global_win_msg);
	}
	else if( pHalType->head.msg_type == HAL_TYPE_BRD_WITH_DATA)
	{
		if(sizeof(SYS_WIN_MSG)+len-4 <= 500)
		{
			global_win_msg.type = MSG_7_BRD;
			global_win_msg.status = pdata[3];
			memcpy(msgTemp, &global_win_msg, sizeof(SYS_WIN_MSG));
			memcpy(msgTemp+sizeof(SYS_WIN_MSG), pdata+4, len-4);
			WinMsgProcessing(msgTemp);		
		}
	}

	
}
#if 0
#define MESSAGE_SPRITE_INFORM_X_SIZE	280
#define MESSAGE_SPRITE_INFORM_Y_SIZE	130

#define MESSAGE_INFORM_TIME				5		//��λ��

static int messageDisplayState;

void MessageReportDisplay(const char* preStr, int str_id, const char* lstStr, int fgcolor, int fnt_type, int time)
{
	int x, y;

	x = (bkgd_w - MESSAGE_SPRITE_INFORM_X_SIZE)/2;
	y = (bkgd_h - MESSAGE_SPRITE_INFORM_Y_SIZE)/2;
	
	SetMessageDisplayState(1);
	MessageDisplay_Timer_Set(time < 0 ? MESSAGE_INFORM_TIME : time);
	API_DisableOsdUpdate();
	API_SpriteDisplay_XY(x, y, SPRITE_Message);
	API_OsdUnicodeStringDisplayWithIdAndAsc2(x+10, y+10, OSD_LAYER_SPRITE, fgcolor, COLOR_MESSAGE_BACK, str_id, fnt_type, preStr, lstStr, MESSAGE_SPRITE_INFORM_X_SIZE-20, MESSAGE_SPRITE_INFORM_Y_SIZE-20);
	API_EnableOsdUpdate();
}

void MessageClose(void)
{
	if(GetMessageDisplayState())
	{
		int x, y;
		
		x = (bkgd_w - MESSAGE_SPRITE_INFORM_X_SIZE)/2;
		y = (bkgd_h - MESSAGE_SPRITE_INFORM_Y_SIZE)/2;
		
		API_SpriteClose(x, y, SPRITE_Message);
	}
	SetMessageDisplayState(0);
	MessageDisplay_Timer_Set(0);
}

int GetMessageDisplayState(void)
{
	return messageDisplayState;
}

void SetMessageDisplayState(int state)
{
	messageDisplayState = state;
}

void UnlockReport(int reprotType)
{
	if(reprotType == 0)
	{
		MessageReportDisplay("Door is", 0, " open", COLOR_RED, 1, 10);
		BEEP_CONFIRM();
	}
	else
	{
		MessageReportDisplay("Door ", 0, "open error", COLOR_RED, 1, 10);
		BEEP_ERROR();
	}
}

void* GetMenuInformData(void* arg)
{
	return (void*)( ((char*)arg) + sizeof(SYS_WIN_MSG) );
}

void RingReqReport(const char* message)
{
	MessageReportDisplay(NULL, 0, message, COLOR_RED, 1, 5);
	BEEP_CONFIRM();
}



void ProgCallNbrReqReport(void)
{
	MessageReportDisplay("Prog Call_Nbr:", 0, GetSysVerInfo_BdRmMs(), COLOR_RED, 1, 5);
	BEEP_CONFIRM();
}
#endif

int CmdRingReq(const char* data)
{
	if(data != NULL)
	{
		int len;

		len = strlen(data) + 1;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_RING_REQ, data, len);
	}
	else
	{
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_RING_REQ, "", 1);
	}
	
	return 0;
}

int CmdProgCallNbrReq(void)
{
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PROG_CALL_NBR);
	return 0;
}


