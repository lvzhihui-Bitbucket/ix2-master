
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "task_Hal.h"
#include "obj_menu_data.h"

#include "obj_ImageAdjust.h"

VIDEO_STATUS objVideoStatus;


const VIDEO_BUSINESS_TYPE VIDEO_OBJ_BUSINESS_TAB_LIST[] = 
{
	AUTO_SCALE,
	PAL_SCALE,
	NTSC_SCALE,
	AUTO_FULL,
	PAL_FULL,
	NTSC_FULL,	
};

//视频对象的属性参数初始表
const VIDEO_ATTR VIDEO_OBJ_BUSINESS_TAB[] = 
{
    	{ CVBS_Y1, AUTO, SCALE_4_3,  	},	//缩小、AUTO
	{ CVBS_Y1, PAL,  	SCALE_4_3,  	},	//缩小、PAL
	{ CVBS_Y1, NTSC, SCALE_4_3,  	},	//缩小、NTSC
    	{ CVBS_Y1, AUTO, SCALE_16_9, 	},	//全屏、AUTO
	{ CVBS_Y1, PAL,  	SCALE_16_9, 	},	//全屏、PAL
	{ CVBS_Y1, NTSC, SCALE_16_9, 	},	//全屏、NTSC
};

/*******************************************************************************************
 * @fn：	Video_TurnOn_Init
 *
 * @brief:	视频开启初始化
 *
 * @param:  	type - 开启模式，std - 开启制式,   logContrast - 对比度逻辑值, logBright - 亮度逻辑值, logColor - 色度逻辑值
 *
 * @return: 	none
 *******************************************************************************************/
void Video_TurnOn_Init(VIDEO_BUSINESS_TYPE type, VIDEO_STD std,  unsigned char logContrast, unsigned char logBright, unsigned char logColor )
{
	int i,len;
	len = sizeof(VIDEO_OBJ_BUSINESS_TAB_LIST)/sizeof(VIDEO_BUSINESS_TYPE);
	for( i = 0; i < len; i++ )
	{
		if( VIDEO_OBJ_BUSINESS_TAB_LIST[i] == type )
			break;
	}
	if( i == len )  i = 0;
	
	//视频对象初始化
    	objVideoStatus.type 			= type;
	objVideoStatus.laststd 				= std;
 	objVideoStatus.attr 				= VIDEO_OBJ_BUSINESS_TAB[i];
	objVideoStatus.adjlog.logContrast	= logContrast;
	objVideoStatus.adjlog.logBrightCnt 	= logBright;
	objVideoStatus.adjlog.logColorCnt 	= logColor;
	//test
	//objVideoStatus.type  		= NTSC_SCALE;
	//objVideoStatus.attr.ch 	= CVBS_Y1;	
}

/*******************************************************************************************
 * @fn：	Video_TurnOn
 *
 * @brief:	视频开启
 *
 * @param:  	none
 *
 * @return: 	none
 *******************************************************************************************/
 
#include "task_Power.h"

extern void API_LocalCaptureOn( int win );

void Video_TurnOn(int win)
{
	objVideoStatus.on 	= 1;	
	//API_LoadImagePara();
	API_POWER_VIDEO_ON();
	API_LocalCaptureOn(win);
/*	
	switch( objVideoStatus.type )
	{
		//lzh_20140806_s
		//自动模式以上一次的制式为准
		case AUTO_SCALE:
			if( objVideoStatus.laststd == PAL )
			{
				printf("last cvbs pal lmod,ch:%d...\n\r",objVideoStatus.attr.ch);
				//Video_Display_Cvbs_Pal_lmod(objVideoStatus.attr.ch);
			}
			else
			{
				printf("last cvbs ntst lmod,ch:%d...\n\r",objVideoStatus.attr.ch);
				//Video_Display_Cvbs_Ntsc_lmod(objVideoStatus.attr.ch);
			}
			break;
			
		//自动模式以上一次的制式为准
		case AUTO_FULL:
			if( objVideoStatus.laststd == PAL )
			{
				printf("last cvbs pal full,ch:%d...\n\r",objVideoStatus.attr.ch);
				//Video_Display_Cvbs_Pal_Full(objVideoStatus.attr.ch);
			}
			else
			{
				printf("last cvbs ntst full,ch:%d...\n\r",objVideoStatus.attr.ch);
				//Video_Display_Cvbs_Ntsc_Full(objVideoStatus.attr.ch);
			}				
			break;
		//lzh_20140806_e
			
		case PAL_SCALE:			
			printf("cvbs pal lmod,ch:%d...\n\r",objVideoStatus.attr.ch);
			//Video_Display_Cvbs_Pal_lmod(objVideoStatus.attr.ch);
			break;
		case PAL_FULL:
			printf("cvbs pal full,ch:%d...\n\r",objVideoStatus.attr.ch);
			//Video_Display_Cvbs_Pal_Full(objVideoStatus.attr.ch);		
			break;
			
		case NTSC_SCALE:
			printf("cvbs ntsc lmod,ch:%d...\n\r",objVideoStatus.attr.ch);			
			//Video_Display_Cvbs_Ntsc_lmod(objVideoStatus.attr.ch);
			break;
		case NTSC_FULL:
			printf("cvbs ntsc full,ch:%d...\n\r",objVideoStatus.attr.ch);			
			//Video_Display_Cvbs_Ntsc_Full(objVideoStatus.attr.ch);
			break;
	}
	//set birght\color\contrast
	Video_ImageParaSet(objVideoStatus.adjlog.logBrightCnt,objVideoStatus.adjlog.logColorCnt,objVideoStatus.adjlog.logContrast);
	*/
}

/*******************************************************************************************
 * @fn：	Video_TurnOff
 *
 * @brief:	视频关闭
 *
 * @param:  	none
 *
 * @return: 	none
 *******************************************************************************************/
extern void API_LocalCaptureOff( void );
 
void Video_TurnOff(void)
{
	objVideoStatus.on = 0;
	API_POWER_VIDEO_OFF();
	API_LocalCaptureOff();	
}

extern void SendVideoAutoStandardUpdate(VIDEO_STD std);

extern unsigned char CurWorkMode;

int CheckAutoVideoType(void)
{
	//  判断当前视频模式是否为自动模式
	//if( (CurWorkMode == WORK_MODE_MONITOR) && ( (objVideoStatus.type==AUTO_SCALE) || (objVideoStatus.type==AUTO_FULL) ) )
	//	return 1;
	//else
		return 0;
}

// 在自动模式下判断制式是否有改变
int CheckAutoVideoTypeChange(VIDEO_STD curStd)
{
	//  判断当前视频模式是否为自动模式
	if( !CheckAutoVideoType() )
		return 0;
	
	if( objVideoStatus.laststd != curStd )
	{
		objVideoStatus.laststd = curStd; 
		// update display
		Video_TurnOn(0);
		// notify MCU update the laststd
		//SendVideoAutoStandardUpdate(curStd);
		printf("Send Current Changed Video Standard: %02d \n\r",curStd);
		return 1;
	}
	else
		return 0;
}

