/**
  ******************************************************************************
  * @file    ObjUnlock.h
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the headers of the ObjUnlock.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef OBJ_OSD_TIME_LAPSE_H
#define OBJ_OSD_TIME_LAPSE_H

//#include "RTOS.h"

typedef enum
{
    COUNT_RUN_UP,
    COUNT_RUN_DN,
    COUNT_SHOW,
    COUNT_HIDE,
    COUNT_STOP,
    TIME_LPSE_UPDATE,
} TimeLapseMode;

typedef struct
{
    TimeLapseMode countMode;
	
    unsigned int timeVal;
    
    unsigned char hide;
	
	OS_TIMER TimeLapseTimer;

	int cntMenu;		//czn_20170313
	
} TimeLapseData;


// Define Object Function - Private

void OSD_UpdateTimeLapseDisp( unsigned char uClear );
// Define Object Function - Public

void TimeLapseInit(void);
void TimeLapseTimerCallBack(void);

void TimeLapseStart(TimeLapseMode iCountMode, unsigned int iVal);
void TimeLapseShow(void);
void TimeLapseHide(void);
void TimeLapseStop(void);

#endif
