/**
  ******************************************************************************
  * @file    Obj_OsdExtLable.c
  * @author  Lv
  * @version V1.0.0
  * @date    2013.10.26
  * @brief   This file contains the function of Lable Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2013 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include "obj_OsdExtLabel.h"
#include "obj_OsdIntLabel.h"
#include "obj_OsdLabel.h"

typedef struct
{
	unsigned char type;
	unsigned char sn;
} LableDatType;


const LableDatType EL_Down				= { EXT_LABEL_TYPE_1,  0};
const LableDatType EL_Up		           		= { EXT_LABEL_TYPE_1,  1};
const LableDatType EL_Missed          			= { EXT_LABEL_TYPE_1,  2};
const LableDatType EL_Paly           			= { EXT_LABEL_TYPE_1,  3};
const LableDatType EL_LEFT           			= { EXT_LABEL_TYPE_1,  4};
const LableDatType EL_RIGHT           			= { EXT_LABEL_TYPE_1,  5};
const LableDatType EL_MIDDLE           			= { EXT_LABEL_TYPE_1,  6};
const LableDatType EL_LEFT_UP           		= { EXT_LABEL_TYPE_1,  7};
const LableDatType EL_RIGHT_UP           		= { EXT_LABEL_TYPE_1,  8};
const LableDatType EL_LEFT_DN           		= { EXT_LABEL_TYPE_1,  9};
const LableDatType EL_RIGHT_DN           		= { EXT_LABEL_TYPE_1,  10};
const LableDatType EL_MID_DOWN           		= { EXT_LABEL_TYPE_1,  11};
const LableDatType EL_MID_UP           			= { EXT_LABEL_TYPE_1,  12};
const LableDatType EL_HOME           			= { EXT_LABEL_TYPE_1,  13};

const LableDatType EL_Unlock1		       	= { EXT_LABEL_TYPE_2,  0};
const LableDatType EL_Unlock2     			= { EXT_LABEL_TYPE_2,  1};
const LableDatType EL_Light       	 		= { EXT_LABEL_TYPE_2,  2};
const LableDatType EL_Bussy	           		= { EXT_LABEL_TYPE_2,  3};
const LableDatType EL_Tick		           		= { EXT_LABEL_TYPE_2,  4};
const LableDatType EL_Menu           			= { EXT_LABEL_TYPE_2,  5};
const LableDatType EL_Transfer       			= { EXT_LABEL_TYPE_2,  6};
const LableDatType EL_Do_Not_Disturb		= { EXT_LABEL_TYPE_2,  7};
const LableDatType EL_Lock           			= { EXT_LABEL_TYPE_2,  8};
const LableDatType EL_Alarm           			= { EXT_LABEL_TYPE_2,  9};
const LableDatType EL_Current        			= { EXT_LABEL_TYPE_2,  10};
const LableDatType EL_Tlak        				= { EXT_LABEL_TYPE_2,  11};
const LableDatType EL_Tip      				= { EXT_LABEL_TYPE_2,  12};
const LableDatType EL_Tip1        				= { EXT_LABEL_TYPE_2,  13};
const LableDatType EL_Voice        				= { EXT_LABEL_TYPE_2,  14};

const LableDatType EL_Big_Light_1 			= { EXT_LABEL_TYPE_3,  0};
const LableDatType EL_Big_Light_2 			= { EXT_LABEL_TYPE_3,  1};
const LableDatType EL_Big_Busy_1 			= { EXT_LABEL_TYPE_3,  2};
const LableDatType EL_Big_Busy_2 			= { EXT_LABEL_TYPE_3,  3};
const LableDatType EL_Big_Disturb_1		= { EXT_LABEL_TYPE_3,  4};
const LableDatType EL_Big_Disturb_2		= { EXT_LABEL_TYPE_3,  5};
const LableDatType EL_Big_Transfer_1 		= { EXT_LABEL_TYPE_3,  6};
const LableDatType EL_Big_Transfer_2 		= { EXT_LABEL_TYPE_3,  7};
const LableDatType EL_Big_Voice_1 		= { EXT_LABEL_TYPE_3,  8};
const LableDatType EL_Big_Voice_2 		= { EXT_LABEL_TYPE_3,  9};

const LableDatType EL_Image 				= { EXT_LABEL_TYPE_6,  0};
const LableDatType EL_Video 				= { EXT_LABEL_TYPE_6,  1};
const LableDatType EL_Pause	 			= { EXT_LABEL_TYPE_6,  2};

const LableDatType EL_Norma            		= { EXT_LABEL_TYPE_7,  0};
const LableDatType EL_Bright				= { EXT_LABEL_TYPE_7,  1};
const LableDatType EL_Soft					= { EXT_LABEL_TYPE_7,  2};
const LableDatType EL_User              		= { EXT_LABEL_TYPE_7,  3};
const LableDatType EL_Delete            		= { EXT_LABEL_TYPE_7,  4};
const LableDatType EL_Record            		= { EXT_LABEL_TYPE_7,  5};
const LableDatType EL_Connated            		= { EXT_LABEL_TYPE_7,  6};

const LableDatType EL_Progress_0            	= { EXT_LABEL_TYPE_10,  0};
const LableDatType EL_Progress_1            	= { EXT_LABEL_TYPE_10,  1};
const LableDatType EL_Progress_2            	= { EXT_LABEL_TYPE_10,  2};
const LableDatType EL_Progress_3            	= { EXT_LABEL_TYPE_10,  3};
const LableDatType EL_Progress_4            	= { EXT_LABEL_TYPE_10,  4};
const LableDatType EL_Progress_5            	= { EXT_LABEL_TYPE_10,  5};
const LableDatType EL_Progress_6            	= { EXT_LABEL_TYPE_10,  6};
const LableDatType EL_Progress_7            	= { EXT_LABEL_TYPE_10,  7};
const LableDatType EL_Progress_8            	= { EXT_LABEL_TYPE_10,  8};
const LableDatType EL_Progress_9            	= { EXT_LABEL_TYPE_10,  9};

const LableDatType EL_Loading_Files  		= { EXT_LABEL_TYPE_11,  0};
const LableDatType EL_Refuse_Calls_In    	= { EXT_LABEL_TYPE_11,  1};
const LableDatType EL_Missed_Calls    		= { EXT_LABEL_TYPE_11,  2};
const LableDatType EL_No_Sim_Card    		= { EXT_LABEL_TYPE_11,  3};
const LableDatType EL_Not_Connect    		= { EXT_LABEL_TYPE_11,  4};
const LableDatType EL_Login_Failed    		= { EXT_LABEL_TYPE_11,  5};
const LableDatType EL_Calling    		= { EXT_LABEL_TYPE_11,  6};
const LableDatType EL_Call_End    		= { EXT_LABEL_TYPE_11,  7};

const LableDatType EL_Update_Display_Driver   		= { EXT_LABEL_TYPE_18,  0};
const LableDatType EL_Failed_To_Send_Sms	   		= { EXT_LABEL_TYPE_18,  1};
const LableDatType EL_Notify_Transfer_Phone	   	= { EXT_LABEL_TYPE_18,  2};
const LableDatType EL_Door_1_Unlock_Error   		= { EXT_LABEL_TYPE_18,  3};
const LableDatType EL_Door_2_Unlock_Error   		= { EXT_LABEL_TYPE_18,  4};
const LableDatType EL_Door_3_Unlock_Error   		= { EXT_LABEL_TYPE_18,  5};
const LableDatType EL_Door_4_Unlock_Error   		= { EXT_LABEL_TYPE_18,  6};
const LableDatType EL_Send_Sos_Sms    			= { EXT_LABEL_TYPE_18,  7};
const LableDatType EL_Sms_Sent_Out    				= { EXT_LABEL_TYPE_18,  8};
const LableDatType EL_Format_SD_Card     			= { EXT_LABEL_TYPE_18,  9};
const LableDatType EL_Update_Software  			= { EXT_LABEL_TYPE_18,  10};
const LableDatType EL_Door_1_Opened    			= { EXT_LABEL_TYPE_18,  11};
const LableDatType EL_Door_2_Opened    			= { EXT_LABEL_TYPE_18,  12};
const LableDatType EL_Door_3_Opened    			= { EXT_LABEL_TYPE_18,  13};
const LableDatType EL_Door_4_Opened    			= { EXT_LABEL_TYPE_18,  14};
const LableDatType EL_Please_Wait    				= { EXT_LABEL_TYPE_18,  15};
const LableDatType EL_Door_Opened    	    			= { EXT_LABEL_TYPE_18,  16};
const LableDatType EL_Restore_To_Default			= { EXT_LABEL_TYPE_18,  17};
const LableDatType EL_Copy_Pictures_To_SD		= { EXT_LABEL_TYPE_18,  18};
const LableDatType EL_Failed						= { EXT_LABEL_TYPE_18,  19};
const LableDatType EL_Succeed						= { EXT_LABEL_TYPE_18,  20};
const LableDatType EL_Have_No_Files				= { EXT_LABEL_TYPE_18,  21};
const LableDatType EL_Have_No_This_File			= { EXT_LABEL_TYPE_18,  22};
const LableDatType EL_MON_SELECT				= { EXT_LABEL_TYPE_18,  23};
const LableDatType EL_Receive_Time				= { EXT_LABEL_TYPE_18,  24};
const LableDatType EL_Send_Time				= { EXT_LABEL_TYPE_18,  25};
const LableDatType EL_NOT_DISTURB				= { EXT_LABEL_TYPE_18,  26};
const LableDatType EL_TRANSFER_1				= { EXT_LABEL_TYPE_18,  27};
const LableDatType EL_VOICE_PROMPT				= { EXT_LABEL_TYPE_18,  28};
const LableDatType EL_NAMELIST				= { EXT_LABEL_TYPE_18,  29};
const LableDatType EL_CALL_RECORD				= { EXT_LABEL_TYPE_18,  30};
const LableDatType EL_Door_Rename				= { EXT_LABEL_TYPE_18,  31};
const LableDatType EL_Camera_Rename				= { EXT_LABEL_TYPE_18,  32};
const LableDatType EL_Monitor_Time_Set				= { EXT_LABEL_TYPE_18,  33};
const LableDatType EL_Ring_Tone				= { EXT_LABEL_TYPE_18,  34};
const LableDatType EL_Volume				= { EXT_LABEL_TYPE_18,  35};
const LableDatType EL_Door_Ring_Mode				= { EXT_LABEL_TYPE_18,  36};
const LableDatType EL_Date_Time				= { EXT_LABEL_TYPE_18,  37};
const LableDatType EL_Date_Sync				= { EXT_LABEL_TYPE_18,  38};
const LableDatType EL_Language				= { EXT_LABEL_TYPE_18,  39};
const LableDatType EL_ABOUT				= { EXT_LABEL_TYPE_18,  40};
const LableDatType EL_Local_Address				= { EXT_LABEL_TYPE_18,  41};
const LableDatType EL_Video_Standard				= { EXT_LABEL_TYPE_18,  42};
const LableDatType EL_System_Verson				= { EXT_LABEL_TYPE_18,  43};
const LableDatType EL_Display_Driver				= { EXT_LABEL_TYPE_18,  44};
const LableDatType EL_FONT				= { EXT_LABEL_TYPE_18,  45};
const LableDatType EL_UI				= { EXT_LABEL_TYPE_18,  46};
const LableDatType EL_SD_INFO				= { EXT_LABEL_TYPE_18,  47};
const LableDatType EL_SD_Card				= { EXT_LABEL_TYPE_18,  48};
const LableDatType EL_Video_Capacity				= { EXT_LABEL_TYPE_18,  49};
const LableDatType EL_Video_Usage				= { EXT_LABEL_TYPE_18,  50};
const LableDatType EL_FLASH				= { EXT_LABEL_TYPE_18,  51};
const LableDatType EL_Image_Capacity				= { EXT_LABEL_TYPE_18,  52};
const LableDatType EL_Image_Usage				= { EXT_LABEL_TYPE_18,  53};
const LableDatType EL_TRANSFER_DEVCE_INFO				= { EXT_LABEL_TYPE_18,  54};
const LableDatType EL_DEVICE				= { EXT_LABEL_TYPE_18,  55};
const LableDatType EL_SIM				= { EXT_LABEL_TYPE_18,  56};
const LableDatType EL_NETWORK				= { EXT_LABEL_TYPE_18,  57};
const LableDatType EL_SIGNAL				= { EXT_LABEL_TYPE_18,  58};
const LableDatType EL_NAME				= { EXT_LABEL_TYPE_18,  59};
const LableDatType EL_DAY				= { EXT_LABEL_TYPE_18,  60};
const LableDatType EL_NIGHT				= { EXT_LABEL_TYPE_18,  61};
const LableDatType EL_SELECT_RECEIVER		= { EXT_LABEL_TYPE_18,  62};
const LableDatType EL_TEL_NUM1		= { EXT_LABEL_TYPE_18,  63};
const LableDatType EL_TEL_NUM2		= { EXT_LABEL_TYPE_18,  64};
const LableDatType EL_TEL_NUM3		= { EXT_LABEL_TYPE_18,  65};
const LableDatType EL_VISITOR_MESSAGE		= { EXT_LABEL_TYPE_18,  66};
const LableDatType EL_Keypad		= { EXT_LABEL_TYPE_18,  67};
const LableDatType EL_Door_1_Motion  = { EXT_LABEL_TYPE_18,  68};
const LableDatType EL_Door_2_Motion  = { EXT_LABEL_TYPE_18,  69};
const LableDatType EL_Door_3_Motion  = { EXT_LABEL_TYPE_18,  70};
const LableDatType EL_Door_4_Motion  = { EXT_LABEL_TYPE_18,  71};

const LableDatType* const EXT_LABEL_INDEX_TAB[] =
{
    // type 1
	&EL_Down,
	&EL_Up,
    	&EL_Missed,
    	&EL_Paly,
        &EL_LEFT,
        &EL_RIGHT,
    	&EL_MIDDLE,
    	&EL_LEFT_UP,
    	&EL_RIGHT_UP,
    	&EL_LEFT_DN,
    	&EL_RIGHT_DN,
    	&EL_MID_DOWN,
    	&EL_MID_UP,
    	&EL_HOME,
    // type 2
	&EL_Unlock1,
	&EL_Unlock2,
	&EL_Light,
	&EL_Bussy,
    	&EL_Tick,
    	&EL_Menu,
    	&EL_Transfer,
    	&EL_Do_Not_Disturb,
    	&EL_Lock,
    	&EL_Alarm,
    	&EL_Current,
    	&EL_Tlak,
    	&EL_Tip,
    	&EL_Tip1,
    	&EL_Voice,
    // type3
	&EL_Big_Light_1,
	&EL_Big_Light_2,
	&EL_Big_Busy_1,
	&EL_Big_Busy_2,
	&EL_Big_Disturb_1,
	&EL_Big_Disturb_2,
	&EL_Big_Transfer_1,
	&EL_Big_Transfer_2,
	&EL_Big_Voice_1,
	&EL_Big_Voice_2,
    // type 5
    	&EL_Image,
    	&EL_Video,
    	&EL_Pause,
    // type 7
	&EL_Norma,
	&EL_Bright,
	&EL_Soft,
	&EL_User,
    	&EL_Delete,
    	&EL_Record,
    	&EL_Connated,
    // type 10
	&EL_Progress_0,
	&EL_Progress_1,
	&EL_Progress_2,
	&EL_Progress_3,
    	&EL_Progress_4,
    	&EL_Progress_5,
    	&EL_Progress_6,
    	&EL_Progress_7,
    	&EL_Progress_8,
    	&EL_Progress_9,    	
    // type 11
	&EL_Loading_Files,
    	&EL_Refuse_Calls_In,
    	&EL_Missed_Calls,
    	&EL_No_Sim_Card,
    	&EL_Not_Connect,
    	&EL_Login_Failed,
    	&EL_Calling,
    	&EL_Call_End,
    // type 18
	&EL_Update_Display_Driver,
	&EL_Failed_To_Send_Sms,
	&EL_Notify_Transfer_Phone,
	&EL_Door_1_Unlock_Error,
	&EL_Door_2_Unlock_Error,
	&EL_Door_3_Unlock_Error,
	&EL_Door_4_Unlock_Error,	
	&EL_Send_Sos_Sms,
    	&EL_Sms_Sent_Out,
    	&EL_Format_SD_Card,
    	&EL_Update_Software,
    	&EL_Door_1_Opened,
    	&EL_Door_2_Opened,
    	&EL_Door_3_Opened,
    	&EL_Door_4_Opened,
    	&EL_Please_Wait,    	
	&EL_Door_Opened,
	&EL_Restore_To_Default,
	&EL_Copy_Pictures_To_SD,
	&EL_Failed,
	&EL_Succeed,
	&EL_Have_No_Files,
	&EL_Have_No_This_File,
    	&EL_MON_SELECT,
    	&EL_Receive_Time,
   	&EL_Send_Time,
   	&EL_NOT_DISTURB,
    	&EL_TRANSFER_1,
    	&EL_VOICE_PROMPT,
    	&EL_NAMELIST,
    	&EL_CALL_RECORD,
    	&EL_Door_Rename,
   	&EL_Camera_Rename,
	&EL_Monitor_Time_Set,
    	&EL_Ring_Tone,
    	&EL_Volume,
    	&EL_Door_Ring_Mode,
    	&EL_Date_Time,
    	&EL_Date_Sync,
    	&EL_Language,
    	&EL_ABOUT,
    	&EL_Local_Address,
    	&EL_Video_Standard,
    	&EL_System_Verson,
    	&EL_Display_Driver,
    	&EL_FONT,
    	&EL_UI,
    	&EL_SD_INFO,
    	&EL_SD_Card,
   	&EL_Video_Capacity,
    	&EL_Video_Usage,
    	&EL_FLASH,
    	&EL_Image_Capacity,
    	&EL_Image_Usage,
    	&EL_TRANSFER_DEVCE_INFO,
    	&EL_DEVICE,
    	&EL_SIM,
    	&EL_NETWORK,
    	&EL_SIGNAL,
    	&EL_NAME,
    	&EL_DAY,
    	&EL_NIGHT,
    	&EL_SELECT_RECEIVER,
    	&EL_TEL_NUM1,
    	&EL_TEL_NUM2,
    	&EL_TEL_NUM3,
    	&EL_VISITOR_MESSAGE,
    	&EL_Keypad,
    	&EL_Door_1_Motion,
    	&EL_Door_2_Motion,
    	&EL_Door_3_Motion,
    	&EL_Door_4_Motion,
};                                                                         

/*******************************************************************************************
 * @fn:		Show_Ext_Label
 *
 * @brief:	显示外部lable（字模赋值显示方式）
 *
 * @param:  row - 起始行，col - 起始列，color - 显示颜色，LableExtType - 外部lable索引
 *
 * @return: none
 *******************************************************************************************/
void Show_Ext_Label(int row, int col, int color, ExtLableType extlabelnum)
{	
	const LableDatType *pLabel;
		
	pLabel = EXT_LABEL_INDEX_TAB[(unsigned char)extlabelnum];

	LoadCurLanguageLabel(pLabel->type,pLabel->sn, col, row, color);	
}

/*******************************************************************************************
 * @fn:		Hide_Ext_Label
 *
 * @brief:	清除外部lable（字模赋值显示方式）
 *
 * @param:  row - 起始行，col - 起始列，LableExtType - 外部lable索引
 *
 * @return: none
 *******************************************************************************************/
void Hide_Ext_Label(int row, int col, ExtLableType extlabelnum)
{
	const LableDatType *pLabel;	
	
	pLabel = EXT_LABEL_INDEX_TAB[(unsigned char)extlabelnum];
	
	LoadCurLanguageLabel(pLabel->type,pLabel->sn, col, row, COLOR_KEY);	
}

