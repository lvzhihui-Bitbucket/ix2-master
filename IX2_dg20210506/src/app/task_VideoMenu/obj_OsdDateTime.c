/**
  ******************************************************************************
  * @file    ObjDateTime.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the function of Date&Time Interface .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "RTOS.h"
#include "OSTIME.h"
#include "obj_OsdDateTime.h"
#include "task_VideoMenu.h"
#include "task_IoServer.h"
#include "task_RTC.h"
#include "task_survey.h"
#include "obj_menu_data.h"


#define SPRITE_DIGIT_BIG_0	0
#define SPRITE_DIGIT_BIG_1	1
#define SPRITE_DIGIT_BIG_2	2
#define SPRITE_DIGIT_BIG_3	3
#define SPRITE_DIGIT_BIG_4	4
#define SPRITE_DIGIT_BIG_5	5
#define SPRITE_DIGIT_BIG_6	6
#define SPRITE_DIGIT_BIG_7	7
#define SPRITE_DIGIT_BIG_8	8
#define SPRITE_DIGIT_BIG_9	9
#define SPRITE_DIGIT_BIG_MH	10
#define SPRITE_AM_SYMBOL	12
#define SPRITE_PM_SYMBOL	13

#define SPRITE_DIGIT_SMA_HX	11
#define SPRITE_DIGIT_SMA_0	14
#define SPRITE_DIGIT_SMA_1	15
#define SPRITE_DIGIT_SMA_2	16
#define SPRITE_DIGIT_SMA_3	17
#define SPRITE_DIGIT_SMA_4	18
#define SPRITE_DIGIT_SMA_5	19
#define SPRITE_DIGIT_SMA_6	20
#define SPRITE_DIGIT_SMA_7	21
#define SPRITE_DIGIT_SMA_8	22
#define SPRITE_DIGIT_SMA_9	23

#define SPRITE_WEEK_1		41
#define SPRITE_WEEK_2		42
#define SPRITE_WEEK_3		43
#define SPRITE_WEEK_4		44
#define SPRITE_WEEK_5		45
#define SPRITE_WEEK_6		46
#define SPRITE_WEEK_7		47
#define SPRITE_SET_RTC_NOTICE	133
#define SPRITE_LOGO			170

#define SPRITE_DIGIT_B_W	25
#define SPRITE_DIGIT_S_W	18

unsigned char BcdToBin(unsigned char iBcd )
{
    unsigned char bin;
    bin = ((iBcd >> 4) & 0x0f)*10;
    bin += ( iBcd & 0x0f );
    return bin;
}

OS_TIMER timerDateTime;

RtcMode rtcMode;

void DateTimeInit(void)
{
    // create timer, not star, for update display
    OS_CreateTimer(&timerDateTime,TimerDateTimeCallBack,600);
}

/*******************************************************************************
 * @fn      OsdDateTimeSetState()
 *
 * @brief   set rtc display state
 *
 * @param   onOff - 
 *
 * @return  none
 ******************************************************************************/
void OsdDateTimeSetState(unsigned char onOff)
{
    rtcMode.osdRtcState = onOff;
}

/*******************************************************************************
 * @fn      OsdDateTimeGetState()
 *
 * @brief   get rtc display state
 *
 * @param   none
 *
 * @return  rtcMode.osdRtcState - 0/off, 1/on
 ******************************************************************************/
unsigned char OsdDateTimeGetState(void)
{
    return rtcMode.osdRtcState;
}

/*******************************************************************************
 * @fn      RtcGetTimeMode()
 *
 * @brief   get time mode
 *
 * @param   none
 *
 * @return  rtcMode.timeMode - 12 or 24 hours mode
 ******************************************************************************/
RtcTimeFormat RtcGetTimeMode(void)
{
	char temp[20];
    API_Event_IoServer_InnerRead_All(TIME_FORMAT, temp);
	rtcMode.timeMode = atoi(temp);
    return rtcMode.timeMode;
}

/*******************************************************************************
 * @fn      RtcGetDateMode()
 *
 * @brief   get date mode
 *
 * @param   none
 *
 * @return  rtcMode.dateMode - MM_DD_YYYY or DD_MM_YYYY mode.
 ******************************************************************************/
RtcDateFormat RtcGetDateMode(void)
{
	char temp[20];
    API_Event_IoServer_InnerRead_All(DATE_FORMAT, temp);
	rtcMode.dateMode = atoi(temp);
    return rtcMode.dateMode;
}

/*******************************************************************************
 * @fn      RtcSetTimeMode()
 *
 * @brief   set date mode, 
 *
 * @param   iTimeMode - 12 or 24 hours mode
 *
 * @return  none
 ******************************************************************************/
void RtcSetTimeMode(RtcTimeFormat iTimeMode)
{   
	char temp[20];
	
	rtcMode.timeMode = iTimeMode;
	
	sprintf(temp, "%d", rtcMode.timeMode);
	
	// save time mode
	API_Event_IoServer_InnerWrite_All(TIME_FORMAT, temp);
}

/*******************************************************************************
 * @fn      RtcSetDateMode()
 *
 * @brief   set date mode
 *
 * @param   iDateMode - MM_DD_YYYY or DD_MM_YYYY mode.
 *
 * @return  none
 ******************************************************************************/
void RtcSetDateMode(RtcDateFormat iDateMode)
{
	char temp[20];
	
	rtcMode.dateMode = iDateMode;
	sprintf(temp, "%d", rtcMode.dateMode);
	// save date mode
	API_Event_IoServer_InnerWrite_All(DATE_FORMAT, temp);
}


/*******************************************************************************
 * @fn      DateTimeDisplay()
 *
 * @brief   display date time 
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/

// lzh_20170929_s
POS ps;
POS OSD_GetIconXY(uint16 iconNum);	
// lzh_20170929_e

// cao_20181025_s
int timeUpdateFlag = 0;

void SetTimeUpdateFlag(int flag)
{
	timeUpdateFlag = flag;
}

int GetTimeUpdateFlag(void)
{
	return timeUpdateFlag;
}
// cao_20181025_e

void DateTimeDisplay(void)
{
	#if defined(PID_IX850)
	#else
	unsigned char hour;
	unsigned char minute;
	unsigned char week;
	unsigned char year;
	unsigned char day;
	unsigned char month;
	unsigned char second;
	char temp[10] = {0};
	POS pos;
	SIZE hv;
	int dateX,dateY,timeX,timeY,timeS;	
	uint16 xsize, ysize;
	
	if( !rtcMode.osdRtcState || GetCurMenuCnt() != MENU_001_MAIN)
	{
	    return;
	}


	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	year = tblock->tm_year-100;
	month = tblock->tm_mon+1;
	day = tblock->tm_mday;
	hour = tblock->tm_hour;
	minute = tblock->tm_min;
	second = tblock->tm_sec;
	week = tblock->tm_wday;
	

	//cao_20181025_s
	if(!GetTimeUpdateFlag())
	{
		return;
	}
	//cao_20181025_e

	API_DisableOsdUpdate();
	char disp[100];
	if( get_pane_type() == 5 || get_pane_type() == 7)//ix470V
	{
		dateX = 90 + OSD_GetIconXY(ICON_219_MainTime).x;
		dateY = 75 + OSD_GetIconXY(ICON_219_MainTime).y;
		timeX = OSD_GetIconXY(ICON_219_MainTime).x;
		timeY = OSD_GetIconXY(ICON_219_MainTime).y;
		timeS = 46;

	}
	else
	{
		dateX = 25 + OSD_GetIconXY(ICON_219_MainTime).x;
		dateY = 55 + OSD_GetIconXY(ICON_219_MainTime).y;
		timeX = 30 + OSD_GetIconXY(ICON_219_MainTime).x;
		timeY = 10 + OSD_GetIconXY(ICON_219_MainTime).y;
		timeS = 25;
	}
	// YYYY_MM_DD mode
	if(RtcGetDateMode() == YYYY_MM_DD)
	{
		sprintf(disp,"%04d-%02d-%02d", year+2000, month, day);
	}
	// DD_MM_YYYY mode
	else if( RtcGetDateMode() == DD_MM_YYYY )
	{
		sprintf(disp,"%02d-%02d-%04d", day, month, year+2000);
	}
	// MM_DD_YYYY mode
	else
	{
		sprintf(disp,"%02d-%02d-%04d", month, day, year+2000);
	}

	API_OsdStringDisplayExt(dateX, dateY, COLOR_WHITE, disp, strlen(disp),1,STR_UTF8,0);
	
    // 12 hour mode
	if( RtcGetTimeMode() == HOUR_12 )
	{
		if( hour >= 12)
		{
			//Menu_Sprite_Display( timeX+7*SPRITE_DIGIT_B_W, 	timeY, SPRITE_PM_SYMBOL);		//czn_20170321
		}
		else
		{
			//Menu_Sprite_Display( timeX+7*SPRITE_DIGIT_B_W, 	timeY, SPRITE_AM_SYMBOL);
		}

		// turn to 12 hours format
		if(hour >= 12)
		{
			hour -= 12;
		}
		if( hour == 0 )
		{
			hour = 12;
		}
	}
	//time
	Menu_Sprite_Display( timeX+0*timeS, 	timeY, SPRITE_DIGIT_BIG_0 +  hour/10);
	Menu_Sprite_Display( timeX+1*timeS, 	timeY, SPRITE_DIGIT_BIG_0 +  hour%10);
	Menu_Sprite_Display( timeX+2*timeS, 	timeY, SPRITE_DIGIT_BIG_MH );
	Menu_Sprite_Display( timeX+3*timeS, 	timeY, SPRITE_DIGIT_BIG_0 +  minute/10);
	Menu_Sprite_Display( timeX+4*timeS, 	timeY, SPRITE_DIGIT_BIG_0 +  minute %10);
	
	API_EnableOsdUpdate();
	#endif

}

/*******************************************************************************
 * @fn      DateTimeClose()
 *
 * @brief   clear date time display
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void DateTimeClose(void)
{    
    rtcMode.osdRtcState = 0;
    OS_StopTimer(&timerDateTime);
	dprintf("DateTimeClose=====================================\n");
    //ClearScreen(1,1);
}

/*******************************************************************************
 * @fn      DateTimeMsgSend()
 *
 * @brief   date time update msg push to task
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void DateTimeMsgSend(void)
{
	DateTime_type 	msg_buf;

	msg_buf.head.msg_source_id 	= MSG_ID_Menu; 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_DATE_TIME;
	msg_buf.control				= DATE_TIME_UPDATE;
	//dprintf("MSG_DATE_TIME=====================================\n");
	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(DateTime_type) );
}

/*******************************************************************************
 * @fn      TimerDateTimeCallBack()
 *
 * @brief   update date time display
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void TimerDateTimeCallBack(void)
{
    DateTimeMsgSend();
    OS_RetriggerTimer(&timerDateTime);
    //dprintf("TimerDateTimeCallBack===================================5s\n");
}



