
#include "task_VideoMenu.h"
#include "task_Hal.h"
#include "obj_ThreadHeartBeatService.h"

#include "onvif_discovery.h"
#include "task_IoServer.h"
#include "MENU_public.h"
#include "cJSON.h"

char IconStateMemTbStr[]=//"\"1000\"";
"1000";
#if 0
"{  
	\"1000\":	
    {
		\"State\":
        {
            \"0\":	100, 
            \"1\":	101,
            \"2\":	102,
            \"3\":	103
        }
	},
	\"1001\":	
    {
		\"State\":
        {
            \"0\":	104, 
            \"1\":	105,
            \"2\":	106
        }
	}
}";
#endif

typedef struct
{
	int icon_id;
	int state_num;
	int state_spri[];
}one_icon_state_t;
const one_icon_state_t main_wifi_state=
{
	ICON_223_MainWifi,
	5,
	{93,94,95,96,97}
};
const one_icon_state_t *icon_state_list[]=
{
	&main_wifi_state,
};
const int icon_state_list_num=sizeof(icon_state_list)/sizeof(icon_state_list[0]);
cJSON* IconState_Json=NULL;
cJSON *StateIcon_CheckRes(char *icon_id)
{
	return NULL;
}
void StateIcon_Table_Load(void)
{
	
	cJSON* value;
	cJSON *one_node;
	char *string;
	char temp[10];
	int i,j;
	one_icon_state_t *one_icon_state;
	if(IconState_Json!=NULL)
		cJSON_Delete(IconState_Json);
	IconState_Json=cJSON_CreateObject();
	if(IconState_Json==NULL)
		return;
	for(i=0;i<icon_state_list_num;i++)
	{
		one_icon_state=icon_state_list[i];
		sprintf(temp,"%04d",one_icon_state->icon_id);
		one_node=cJSON_AddObjectToObject(IconState_Json,temp);
		one_node=cJSON_AddObjectToObject(one_node,"State");
		for(j=0;j<one_icon_state->state_num;j++)
		{
			sprintf(temp,"%d",j);
			cJSON_AddNumberToObject(one_node,temp,one_icon_state->state_spri[j]);
		}
	}
	for(i=0;i<cJSON_GetArraySize(IconState_Json);i++)
	{
		one_node=cJSON_GetArrayItem(IconState_Json,i);
		one_node=StateIcon_CheckRes(one_node->string);
		if(one_node!=NULL)
		{
			cJSON_ReplaceItemInArray(IconState_Json,i,one_node);
		}
	}
	
}

void Api_StateIcon_UpdateOneIcon(int icon,int state,char *text)
{
	char temp[10];
	cJSON *one_icon_state;
	cJSON *one_state;
	if(IconState_Json==NULL)
		return;
	sprintf(temp,"%04d",icon);
	one_icon_state=cJSON_GetObjectItemCaseSensitive(IconState_Json,temp);
	if(one_icon_state==NULL)
		return;
	sprintf(temp,"%d",state);
	one_state=cJSON_GetObjectItemCaseSensitive(one_icon_state,"State");
	one_state=cJSON_GetObjectItemCaseSensitive(one_state,temp);
	if(one_state==NULL)
		return;
	POS pos;
	SIZE hv;
	uint16 xsize, ysize;
	int x,y;
	if(OSD_GetIconInfo(icon, &pos, &hv) == 0)
		return;
	//if((int)one_state->valuedouble==0)
	//	API_SpriteClose(int x, int y, int iSpriteId)
	Get_SpriteSize((int)one_state->valuedouble, &xsize, &ysize);
	x = pos.x;
	y = pos.y + (hv.v - pos.y - ysize)/2;
	API_SpriteDisplay_XY( x, y, (int)one_state->valuedouble);
	if(text!=NULL)
		API_OsdStringDisplayExt(x+50, y+15, COLOR_RED, text, strlen(text), 0,STR_UTF8, 830-x-50);
}

