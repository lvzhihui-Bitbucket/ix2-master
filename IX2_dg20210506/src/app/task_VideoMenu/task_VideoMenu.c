
#include "task_VideoMenu.h"
#include "task_Hal.h"
#include "obj_ThreadHeartBeatService.h"

#include "onvif_discovery.h"
#include "task_IoServer.h"
#include "MENU_public.h"

Loop_vdp_common_buffer	vdp_video_menu_mesg_queue;
Loop_vdp_common_buffer	vdp_video_menu_sync_queue;
vdp_task_t				task_VideoMenu;

void vdp_video_menu_mesg_data_process(char* msg_data, int len);
void* vdp_video_menu_task( void* arg );
int GetIconPosAck_Rsp( unsigned char msg_id, unsigned char msg_type , uint16 icon, POS pos);
POS OSD_GetIconXY(uint16 iconNum);
void Load_Menu_ColorPara(void);
void vtk_TaskInit_video_menu(int priority)
{
	init_vdp_common_queue(&vdp_video_menu_mesg_queue, 5000,(msg_process)vdp_video_menu_mesg_data_process, &task_VideoMenu);
	init_vdp_common_queue(&vdp_video_menu_sync_queue, 500, NULL,								  			&task_VideoMenu);
	init_vdp_common_task(&task_VideoMenu, MSG_ID_Menu, vdp_video_menu_task, &vdp_video_menu_mesg_queue, &vdp_video_menu_sync_queue);

	DateTimeInit();	//cao_20151118
	TimeLapseInit();	//cao_20151118
	#if defined(PID_IX850)
	#else
	//RecSchedule_init();
	#endif
	dprintf("vdp_video_menu_task starting............\n");
}

void exit_vdp_video_menu_task(void)
{
	exit_vdp_common_queue(&vdp_video_menu_mesg_queue);
	exit_vdp_common_queue(&vdp_video_menu_sync_queue);
	exit_vdp_common_task(&task_VideoMenu);	
}

// lzh_20181016_s
void all_display_menu_cache_push_initial(void);
// lzh_20181016_e

void* vdp_video_menu_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	char temp[10];
// lzh_20231115_s
//#ifdef PID_DX470_V25 
//	set_pane_type(DX470_V25);	// 800x480
#if defined(PID_IX482)
//#if defined(PID_IX482)
// lzh_20231115_e
	set_pane_type(2);
#elif defined(PID_IX850)
	set_pane_type(3);
#elif defined(PID_IX47)
	API_Event_IoServer_InnerRead_All(PANEL_TYPE, temp);
	if(atoi(temp))
	{
		set_pane_type(5);
	}
	else
	{
		set_pane_type(4);
	}
#elif defined(PID_DX470)||defined(PID_DX482)
	API_Event_IoServer_InnerRead_All(PANEL_TYPE, temp);
	if(atoi(temp))
	{
		set_pane_type(7);
	}
	else
	{
		set_pane_type(6);
	}	
#elif defined(PID_IXSE)
	set_pane_type(9);
#else
	API_Event_IoServer_InnerRead_All(PANEL_TYPE, temp);
	set_pane_type(atoi(temp));
#endif
	//api_h264_show_initial();
	api_rec_mux_initial();
	
	menu_resource_initial();	
	
	LoadMenuListData();
	StateIcon_Table_Load();
#if 1	
	// lzh_20181016_s
	if( access( "/mnt/sdcard/MovementTest.cfg", F_OK ) < 0 )
	{
		all_display_menu_cache_push_initial();
		ResetMenuStack();
		ClearAllOsdLayers();
		Menu_Close();	// �ϵ���Ҫˢ�����˵�����ʾ���Ա㿪��������ʾ���˵�
	}
	// lzh_20181016_e
	//sleep(2);
	//SwitchOneMenu(MENU_001_MAIN,0,1);
	//SwitchOneMenu(MENU_002_MONITOR,0,1);
	
	//SwitchOneMenu(EXTBUF3_MENU_CNT,0,0);	
	//SwitchOneMenu(EXTBUF2_MENU_CNT,0,0);		
	//SwitchOneMenu(EXTBUF1_MENU_CNT,0,1);		

	//TimeLapseStart(0,0);
	
#endif
	Load_Menu_ColorPara();
	//sleep(3);
	//Menu_Sprite_Display(100,100,98);
	printf("========================osd init ok================================\n");

	#if	defined(PID_DX470)||defined(PID_DX482)||defined(PID_IXSE_V2)
	usleep(100*1000);
	LCD_POWER_SET();
	usleep(100*1000);	// first close backlight, then close lcd power
	LCD_POWER_RESET();	// set high
	#elif defined(PID_IXSE_V2)
	LCD_PWM_RESET();
	usleep(10*1000);
	LCD_POWER_RESET();
	#endif

	while( ptask->task_run_flag )
	{
		#if 0
		int tempcnt = 0;
		if( ++tempcnt >= 10 )
			{
				tempcnt = 0;
				printf("wdt_feed[%d]......\n",tempcnt);
			}
		#endif
		//ak_drv_wdt_feed();
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 1000); //VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			//OS_Q_DisplayInfo(&ptask->p_msg_buf->embosQ);
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
			//OS_Q_DisplayInfo(&ptask->p_msg_buf->embosQ);
		}
	}
	printf("call survey exit\n");
	return 0;
}

void vdp_video_menu_mesg_data_process(char* msg_data,int len)
{
	char*	ptest;
	video_menu_type* 	pvidemenu 	= (video_menu_type*)msg_data;
	string_type*		pstring		= (string_type*)msg_data;
	sprite_type*		psprite		= (sprite_type*)msg_data;
	progbar_type*		bar			= (progbar_type*)msg_data;
	Label_type*			plabel		= (Label_type*)msg_data;
	//cao_20151118
	DateTime_type*	pdatetime	= (DateTime_type*)msg_data;
	TimeLapse_type*	ptimelapse	= (TimeLapse_type*)msg_data;
	//
	LogView_type* pLogView	= (LogView_type*)msg_data;
	
	VDP_MSG_HEAD* pCurcor = (VDP_MSG_HEAD*)msg_data;
	// lzh_20161103_s
	video_type*  pVideoCtrl = (video_type*)msg_data;
	// lzh_20161103_e	
	GetIconPos_type*  pGetIconPos = (GetIconPos_type*)msg_data;

	MenuSwitchType*	pMenuSwitch = (MenuSwitchType*)msg_data;
	
	// lzh_20181016_s
	menu_disp_cache_type*	pMenuDispCache = (menu_disp_cache_type*)msg_data;	
	// lzh_20181016_e
	
	MenuFillColorMsg_t*	pFillColor = (MenuFillColorMsg_t*)msg_data;
	MenuStrMultiMsg_t* pMultiStr = (MenuStrMultiMsg_t*)msg_data;
	MenuUpdateRegion_T* pUpdateDisp = (MenuUpdateRegion_T*)msg_data;
	MenuSpriteMultiMsg_t* pMultiSprite = (MenuSpriteMultiMsg_t*)msg_data;
	MsgRmStruct*	pMenuDispRm = (MsgRmStruct*)msg_data;

	if(pvidemenu->head.msg_source_id == MSG_ID_hal)
	{
		MenuHalMessageProcessing(msg_data, len);
		return;
	}

	switch( pvidemenu->head.msg_type )
	{
		// lzh_20161103_s
		case MSG_VIDEO:
			if( pVideoCtrl->opt == VIDEO_ON )
				Video_TurnOn(pVideoCtrl->win);
			else if( pVideoCtrl->opt == VIDEO_OFF )
				Video_TurnOff();			
			break;
		// lzh_20161103_e
	
		case MSG_MENU:
			if( pvidemenu->head.msg_sub_type )
			{
				printf("menu display id %d\n",pvidemenu->menu_id);
				Menu_Display(pvidemenu->menu_id);
				MenuDisplayAck_Rsp(pvidemenu->head.msg_source_id,pvidemenu->head.msg_type);
			}
			else
			{	
				printf("menu close\n");
				Menu_Close();
			}
			break;

		// lzh_20161230_s
		case MSG_MENU2:
			if( pvidemenu->head.msg_sub_type )
			{
				printf("menu2 display id %d\n",pvidemenu->menu_id);
				Menu_Display2(pvidemenu->menu_id,pvidemenu->sub_menu_id,pvidemenu->state);
				MenuDisplayAck_Rsp(pvidemenu->head.msg_source_id,pvidemenu->head.msg_type);
			}
			else
			{	
				printf("menu2 close\n");
				Menu_Close();
			}
			break;
		// lzh_20161230_e
			
		case MSG_ICON_PRESS:
			SelectCurIcon();
			break;
			
		case MSG_ICON_RELEASE:
			RestoreOldIcon();
			break;

		case MSG_STRING:
			if( pvidemenu->head.msg_sub_type )
				OSD_StringDisplay( pstring->x, pstring->y, pstring->fg, pstring->bg, (const unsigned char*)pstring->str_dat );
			else
				//OSD_StringClear( pstring->x, pstring->y, pstring->str_len);
				OSD_StringClear( pstring->x, pstring->y, (const unsigned char*)pstring->str_dat);				
			break;

		case MSG_STRING_EXT:
			if( pvidemenu->head.msg_sub_type == 1 )
				OSD_StringDisplayExt( pstring->x, pstring->y, pstring->fg, pstring->bg, (const unsigned char*)pstring->str_dat, pstring->str_len, pstring->fnt_type, pstring->format, 0);
			else if( pvidemenu->head.msg_sub_type == 2 )
				OSD_StringDisplayWithID_Str(pstring->x, pstring->y, pstring->fg, pstring->bg, pstring->str_id, pstring->fnt_type, pstring->str_dat, pstring->pre_str_len, pstring->str_dat+pstring->pre_str_len, pstring->lst_str_len, 0);
			else
				OSD_StringClearExt( pstring->x, pstring->y, pstring->width, pstring->height);
			break;
		case MSG_STRING_EXT2:
			if( pvidemenu->head.msg_sub_type == 1 )
				OSD_StringDisplayExt2( pstring->x, pstring->y, pstring->fg, pstring->bg, (const unsigned char*)pstring->str_dat, pstring->str_len, pstring->fnt_type, pstring->format, pstring->width, pstring->height);
			else if( pvidemenu->head.msg_sub_type == 2 )
				OSD_StringDisplayWithID_Str2(pstring->x, pstring->y, pstring->fg, pstring->bg, pstring->str_id, pstring->fnt_type, pstring->str_dat, pstring->pre_str_len, pstring->str_dat+pstring->pre_str_len, pstring->lst_str_len, pstring->width, pstring->height);
			else
				clear_ext_string2( pstring->x, pstring->y, pstring->width, pstring->height,1);
			break;

		//cao_20151118
		case MSG_TIME_LAPSE:
			switch( ptimelapse->control)
			{
				case COUNT_RUN_UP:
				case COUNT_RUN_DN:
					TimeLapseStart(ptimelapse->control, ptimelapse->inittime);
				case COUNT_SHOW:
					OSD_UpdateTimeLapseDisp( 1 );
					break;
				case COUNT_STOP:
					TimeLapseStop();
					//dprintf("time lapse stop================================\n");
				case COUNT_HIDE:
					OSD_UpdateTimeLapseDisp( 0 );
					//dprintf("time lapse hide================================\n");
					break;
				case TIME_LPSE_UPDATE:
					//dprintf("time lapse update================================\n");
					OSD_UpdateTimeLapseDisp( 1 );
					break;
			}
			break;
		//cao_20151118
		case MSG_DATE_TIME:
			switch( pdatetime->control)
			{
				case DATE_TIME_ON:
					OsdDateTimeSetState(1);
					DateTimeDisplay();
					OS_RetriggerTimer(&timerDateTime);
					break;
				case DATE_TIME_OFF:
					DateTimeClose();
					break;
				case DATE_TIME_UPDATE:
					DateTimeDisplay();
					break;
			} 
			break;

		case MSG_SPRITE:
			if( pvidemenu->head.msg_sub_type == 1 )
				Menu_Sprite_Display( psprite->x, psprite->y, psprite->bkgdcnt);
			else if ( pvidemenu->head.msg_sub_type == 2 )	
				Menu_Sprite_Close( psprite->x, psprite->y, psprite->bkgdcnt, 1);
			else
				Menu_Sprite_Close( psprite->x, psprite->y, psprite->bkgdcnt, 0);
			break;
		case MSG_PROG_BAR:
			if( pvidemenu->head.msg_sub_type )
				DisplayOneProgBar( bar->x, bar->y, bar->width, bar->height, bar->bg);
			else
				ClearOneProgBar( bar->x, bar->y, bar->width, bar->height);
			break;

		case MSG_LABEL:
			if( pvidemenu->head.msg_sub_type )
				Lable_Show( plabel->type );
			else
				Lable_Hide( plabel->type );
			break;
			
		case MSG_LABEL_XY:
			if( pvidemenu->head.msg_sub_type )
				Lable_Show_XY( plabel->y, plabel->x, plabel->fg, plabel->type );
			else
				Lable_Hide_XY( plabel->y, plabel->x, plabel->type );
			break;

		case MSG_LOGVIEW:
			if( pvidemenu->head.msg_sub_type == LOG_VIEW_TYPE_TURN_OFF )
			{
				turn_off_log_win();
			}
			else if( pvidemenu->head.msg_sub_type == LOG_VIEW_TYPE_TURN_ON )
			{
				turn_on_log_win();
			}
			else if( pvidemenu->head.msg_sub_type == LOG_VIEW_TYPE_ADD_LOG_DATA )
			{
				add_rec_to_log_view(pLogView->str_dat);
			}
			else if( pvidemenu->head.msg_sub_type == LOG_VIEW_TYPE_CLEAR_WIN)		//czn_20190412
			{
				clear_log_win();
			}
			break;
		case MSG_CURSOR:
			FixedCurIconCursor(pCurcor->msg_sub_type);
			break;
			
		case MSG_ICON_DISP_ON:
			DisplayOneFuncIcon(pCurcor->msg_sub_type,1);
			break;
			
		case MSG_ICON_DISP_OFF:
			DisplayOneFuncIcon(pCurcor->msg_sub_type,0);
			break;
		case MSG_UPDATE_ENABLE:
			EnableUpdate();
			UpdateOsdLayerBuf(0);
			break;
		case MSG_UPDATE_DISABLE:
			DisableUpdate();
			break;
		case MSG_DISP_BKGD:
			LoadMenuBackground( psprite->bkgdcnt, psprite->x, psprite->y);
			UpdateOsdLayerBuf(0);
			break;		
		case MSG_GET_ICON_POS:
			GetIconPosAck_Rsp(pGetIconPos->head.msg_source_id, pGetIconPos->head.msg_type, pGetIconPos->icon, OSD_GetIconXY(pGetIconPos->icon));
			break;
		case MSG_MENU_SWITCH:
			SwitchOneMenu(pMenuSwitch->uMenu, pMenuSwitch->subMenu, pMenuSwitch->pushstack);
			MenuDisplayAck_Rsp(pMenuSwitch->head.msg_source_id, pMenuSwitch->head.msg_type);
			break;
		case MSG_MENU_CLOSE:
			CloseMenu();
			MenuDisplayAck_Rsp(pMenuSwitch->head.msg_source_id, pMenuSwitch->head.msg_type);
			break;
			
		// lzh_20181016_s
		case MSG_MENU_DISP_CACHE:
			if( pMenuDispCache->head.msg_sub_type == 0 )
				menu_display_cache_push(pMenuDispCache->menu_id, pMenuDispCache->layer_id, pMenuDispCache->cache_num, pMenuDispCache->x, pMenuDispCache->y, pMenuDispCache->width, pMenuDispCache->height);
			else if( pMenuDispCache->head.msg_sub_type == 1 )
				menu_display_cache_pop(pMenuDispCache->menu_id, pMenuDispCache->layer_id, pMenuDispCache->cache_num);
			// lzh_20181108_s
			else if( pMenuDispCache->head.msg_sub_type == 2 )
			{			
				int result; 		
				result = API_create_bmpfile_for_menu(pMenuDispCache->menu_id, pMenuDispCache->layer_id, pMenuDispCache->cache_num);
				MenuDisplayAck_Rsp_with_result(pMenuDispCache->head.msg_source_id, pMenuDispCache->head.msg_type, result);	
				break;
			}		
			// lzh_20181108_e
			MenuDisplayAck_Rsp(pMenuDispCache->head.msg_source_id, pMenuDispCache->head.msg_type);			
			break;
		// lzh_20181016_e
		case MSG_MENU_ThreadHeartReq:
			RecvThreadHeartbeatReply(Menu_Heartbeat_Mask);
			break;
		
		case MSG_FILL_COLOR:
			OsdLayerFillColor(pFillColor->layerId, pFillColor->x, pFillColor->y, pFillColor->xsize, pFillColor->ysize, pFillColor->color, pFillColor->option, pFillColor->alpha);
			break;
		case MSG_STRING_EXT_MULTI_LINE:
			OSD_StringDisplayExtOnePageList(pMultiStr->list);
			break;
		case MSG_UPDATE_REGION:
			OSD_UpdateDisplay(pUpdateDisp->enable, pUpdateDisp->x, pUpdateDisp->y, pUpdateDisp->width, pUpdateDisp->height, pUpdateDisp->alpha);
			break;
		case MSG_SPRITE_MULTI:
			Menu_Sprite_Display_Multi(pMultiSprite->sprite);
			break;
		case MSG_MENU_DISP_RM:
			#if defined(PID_DX470)||defined(PID_DX482)
			Rm_Display(pMenuDispRm->disp_buf);
			#endif
			//MenuDisplayAck_Rsp(pMenuDispRm->head.msg_source_id, pMenuDispRm->head.msg_type);
			break;
	
	}
}

int MenuDisplayAck_Rsp( unsigned char msg_id, unsigned char msg_type )
{
	VDP_MSG_HEAD  disp_type;
	vdp_task_t* pTask = GetTaskAccordingMsgID(msg_id);

	if(pTask == NULL)
	{
		return -1;
	}
	
	disp_type.msg_target_id = msg_id;
	disp_type.msg_source_id	= MSG_ID_Menu;			
	disp_type.msg_type		= (msg_type|COMMON_RESPONSE_BIT);
	disp_type.msg_sub_type	= 0;
	push_vdp_common_queue(pTask->p_syc_buf, (char*)&disp_type, sizeof(VDP_MSG_HEAD));
	
	return 0;
}

int GetIconPosAck_Rsp( unsigned char msg_id, unsigned char msg_type , uint16 icon, POS pos)
{
	GetIconPos_type  rspType;
	vdp_task_t* pTask = GetTaskAccordingMsgID(msg_id);
	
	rspType.head.msg_target_id = msg_id;
	rspType.head.msg_source_id	= MSG_ID_Menu;			
	rspType.head.msg_type		= (msg_type|COMMON_RESPONSE_BIT);
	rspType.head.msg_sub_type	= 0;
	rspType.icon				= icon;
	rspType.pos					= pos;
	push_vdp_common_queue(pTask->p_syc_buf, (char*)&rspType, sizeof(GetIconPos_type));
	return 0;
}


/*******************************************************************************
 * @fn      API_Menu_Display()
 *
 * @brief   Provide an application interface to conctrol menu display
 *
 * @param   iMenuId - menu identity
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_MenuDisplay( unsigned char iMenuId )
{
	video_menu_type 	video_menu_buf;
	p_vdp_common_buffer video_menu_syc;
	vdp_task_t*			ptask;	

	video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());	
	video_menu_buf.head.msg_target_id 	= MSG_ID_Menu;
	video_menu_buf.head.msg_type		= MSG_MENU;
	video_menu_buf.head.msg_sub_type	= 1;
	video_menu_buf.menu_id				= iMenuId;

	if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&video_menu_buf, sizeof(video_menu_type)) != 0 )
	{
		printf("-----------API_MenuDisplay:push_vdp_common_queue just full-----------\n");		
		return -1;
	}
	else
	{
		ptask = GetTaskAccordingMsgID(video_menu_buf.head.msg_source_id);
		if( ptask != NULL )
		{
			if( pop_vdp_common_queue( ptask->p_syc_buf, &video_menu_syc, 2000) > 0 )
			{
				purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
				return 1;	
			}
			else
			{
				printf("-----------API_MenuDisplay: wait sync timeout-----------\n");		
				return 0;
			}
		}
		else
			return 0;
	}
}

// lzh_20161230_s
/*******************************************************************************
 * @fn      API_MenuDisplay2()
 *
 * @brief   Provide an application interface to conctrol menu display
 *
 * @param   iMenuId 	- menu identity
 * @param   iSubMenuId 	- sub menu identity
 * @param   first	 	- first display menu, if 0, not to clear screen, only display sub menu
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_MenuDisplay2( unsigned char iMenuId, unsigned char iSubMenuId, unsigned char first )
{
	video_menu_type 	video_menu_buf;
	p_vdp_common_buffer video_menu_syc;
	vdp_task_t*			ptask;	

	video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());	
	video_menu_buf.head.msg_target_id 	= MSG_ID_Menu;
	video_menu_buf.head.msg_type		= MSG_MENU2;
	video_menu_buf.head.msg_sub_type	= 1;
	video_menu_buf.menu_id				= iMenuId;
	video_menu_buf.state				= first;
	video_menu_buf.sub_menu_id			= iSubMenuId;

	if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&video_menu_buf, sizeof(video_menu_type)) != 0 )
	{
		printf("-----------API_MenuDisplay:push_vdp_common_queue just full-----------\n");		
		return -1;
	}
	else
	{
		ptask = GetTaskAccordingMsgID(video_menu_buf.head.msg_source_id);
		if( ptask != NULL )
		{
			if( pop_vdp_common_queue( ptask->p_syc_buf, &video_menu_syc, 2000) > 0 )
			{
				purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
				return 1;	
			}
			else
			{
				printf("-----------API_MenuDisplay: wait sync timeout-----------\n");		
				return 0;
			}
		}
		else
			return 0;
	}
}

// lzh_20161230_e

/*******************************************************************************
 * @fn      API_Menu_Close()
 *
 * @brief   Provide an application interface to conctrol menu close
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_MenuClose( void )
{
	video_menu_type 	video_menu_buf;
	p_vdp_common_buffer video_menu_syc;
	vdp_task_t*			ptask;	

	video_menu_buf.head.msg_source_id 	= MSG_ID_Menu; //GetMsgIDAccordingPid(pthread_self());	
	//video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	video_menu_buf.head.msg_target_id 	= MSG_ID_Menu;
	video_menu_buf.head.msg_type		= MSG_MENU;
	video_menu_buf.head.msg_sub_type	= 0;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&video_menu_buf, sizeof(video_menu_type) );
	return 0;
}

POS API_GetIconPos(uint16 iconId )
{
	GetIconPos_type 	getIconBuf;
	GetIconPos_type* 	getIconSync;
	vdp_task_t*			ptask;
	POS ret = {0, 0};

	getIconBuf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());	
	getIconBuf.head.msg_target_id 	= MSG_ID_Menu;
	getIconBuf.head.msg_type		= MSG_GET_ICON_POS;
	getIconBuf.head.msg_sub_type	= 1;
	getIconBuf.icon					= iconId;

	if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&getIconBuf, sizeof(GetIconPos_type)) != 0 )
	{
		printf("-----------API_GetIconPos:push_vdp_common_queue just full-----------\n");		
		return ret;
	}
	else
	{
		ptask = GetTaskAccordingMsgID(getIconBuf.head.msg_source_id);
		if( ptask != NULL )
		{
			if( pop_vdp_common_queue( ptask->p_syc_buf, &getIconSync, 2000) > 0 )
			{
				purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
				if(getIconBuf.icon == getIconSync->icon)
				{
					return getIconSync->pos;
				}
				else
				{
					return ret;
				}
			}
			else
			{
				printf("-----------API_GetIconPos: wait sync timeout-----------\n");		
				return ret;
			}
		}
		else
		{
			return ret;
		}
	}
}


/*******************************************************************************
 * @fn      API_MenuIconPress()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_MenuIconPress( void )
{
	SelectCurIcon();
}

int API_IconAlphaPress( void )
{
	SelectCurIconAlpha();
}

/*******************************************************************************
 * @fn      API_MenuIconPressedown()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_MenuIconRelease( void )
{
	RestoreOldIcon();
}

int API_IconAlphaRelease( void )
{
	RestoreOldIconAlpha();
}


int API_OsdUnicodeStringDisplayWithIdAndAsc( int x, int y, int fgcolor, int str_id, int fnt_type, const char* preAscStr, const char* lstAscStr, int width)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self()); 
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 2;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width				= width;
		str_buf.fg					= fgcolor;
		str_buf.bg					= COLOR_KEY;
		str_buf.str_id				= str_id;
		str_buf.fnt_type			= fnt_type;
		str_buf.pre_str_len			= (preAscStr == NULL ? 0 : strlen(preAscStr));
		str_buf.lst_str_len			= (lstAscStr == NULL ? 0 : strlen(lstAscStr));
		str_buf.str_len				= str_buf.pre_str_len + str_buf.lst_str_len;
		memcpy(str_buf.str_dat, preAscStr, str_buf.pre_str_len);
		memcpy(str_buf.str_dat+str_buf.pre_str_len, lstAscStr, str_buf.lst_str_len);

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayWithID_Str(str_buf.x, str_buf.y, str_buf.fg, str_buf.bg, str_buf.str_id, str_buf.fnt_type, str_buf.str_dat, str_buf.pre_str_len, str_buf.str_dat+str_buf.pre_str_len, str_buf.lst_str_len, width);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		}
		
		return 0;
		
}

int API_OsdUnicodeStringDisplayWithIdAndAsc2( int x, int y, int layerId, int fgcolor, int bgcolor, int str_id, int fnt_type, const char* preAscStr, const char* lstAscStr, int xsize, int ysize)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self()); 
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT2;
		str_buf.head.msg_sub_type	= 2;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width				= xsize;
		str_buf.height				= ysize;
		str_buf.fg					= fgcolor;
		str_buf.layerId				= layerId;
		str_buf.bg					= bgcolor;
		str_buf.str_id				= str_id;
		str_buf.fnt_type			= fnt_type;
		str_buf.pre_str_len			= (preAscStr == NULL ? 0 : strlen(preAscStr));
		str_buf.lst_str_len			= (lstAscStr == NULL ? 0 : strlen(lstAscStr));
		str_buf.str_len				= str_buf.pre_str_len + str_buf.lst_str_len;
		
		if(preAscStr != NULL)
		{
			memcpy(str_buf.str_dat, preAscStr, str_buf.pre_str_len);
		}
		
		if(lstAscStr != NULL)
		{
			memcpy(str_buf.str_dat+str_buf.pre_str_len, lstAscStr, str_buf.lst_str_len);
		}

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayWithID_Str2(str_buf.x, str_buf.y, str_buf.layerId, str_buf.fg, str_buf.bg, str_buf.str_id, str_buf.fnt_type, str_buf.str_dat, str_buf.pre_str_len, str_buf.str_dat+str_buf.pre_str_len, str_buf.lst_str_len, str_buf.width, str_buf.height);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		}
		
		return 0;
		
}
/*******************************************************************************
 * @fn      API_OsdStringDisplayExt()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_OsdStringDisplayExt( int x, int y, int fgcolor, unsigned char* string, int strlen, int fnt_type,int format, int width)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 1;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width				= width;
		str_buf.fg					= fgcolor;
		str_buf.bg					= COLOR_KEY;
		str_buf.format				= format;
		str_buf.fnt_type			= fnt_type;		
		str_buf.str_len				= strlen;
		if( str_buf.str_len >= MAX_STR_DISP_LEN ) str_buf.str_len = MAX_STR_DISP_LEN;
		memcpy(str_buf.str_dat,string,str_buf.str_len );

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayExt( str_buf.x, str_buf.y, str_buf.fg, str_buf.bg, (const unsigned char*)str_buf.str_dat, str_buf.str_len, str_buf.fnt_type, str_buf.format, width);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		}
		
		return 0;
}

int API_OsdStringDisplayExt2( int x, int y, int layerId, int fgcolor, int bgcolor, unsigned char* string, int strlen, int fnt_type,int format, int xsize, int ysize)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT2;
		str_buf.head.msg_sub_type	= 1;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width				= xsize;
		str_buf.height				= ysize;
		str_buf.layerId				= layerId;
		str_buf.fg					= fgcolor;
		str_buf.bg					= bgcolor;
		str_buf.format				= format;
		str_buf.fnt_type			= fnt_type;		
		str_buf.str_len				= strlen;
		if( str_buf.str_len >= MAX_STR_DISP_LEN ) str_buf.str_len = MAX_STR_DISP_LEN;
		memcpy(str_buf.str_dat,string,str_buf.str_len );

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayExt2( str_buf.x, str_buf.y, str_buf.layerId, str_buf.fg, str_buf.bg, (const unsigned char*)str_buf.str_dat, str_buf.str_len, str_buf.fnt_type, str_buf.format, str_buf.width, str_buf.height);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		}
		
		return 0;
}

/*******************************************************************************
 * @fn      API_OsdStringDisplayExtWithBackColor()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_OsdStringDisplayExtWithBackColor( int x, int y, int fgcolor, int bgcolor, unsigned char* string, int strlen, int fnt_type,int format )
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 1;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.fg					= fgcolor;
		str_buf.bg					= bgcolor;
		str_buf.format				= format;
		str_buf.fnt_type			= fnt_type;		
		str_buf.str_len				= strlen;
		if( str_buf.str_len >= MAX_STR_DISP_LEN ) str_buf.str_len = MAX_STR_DISP_LEN;
		memcpy(str_buf.str_dat,string,str_buf.str_len );

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayExt( str_buf.x, str_buf.y, str_buf.fg, str_buf.bg, (const unsigned char*)str_buf.str_dat, str_buf.str_len, str_buf.fnt_type, str_buf.format, 0);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		}
		return 0;
}

/*******************************************************************************
 * @fn      API_OsdStringCenterDisplayExt()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_OsdStringCenterDisplayExt( int x, int y, int fgcolor, unsigned char* string, int strlen, int fnt_type,int format,int width,int height)	//czn_20181110
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
		
		if((str_buf.head.msg_source_id = GetMsgIDAccordingPid(pthread_self()))  == MSG_ID_Menu)
		{
			DisplayCenterString(x, y, fgcolor, COLOR_KEY, string, strlen, fnt_type,format,width, height);
			return 0;
		}
		//str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 3;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.fg					= fgcolor;
		str_buf.bg					= COLOR_KEY;
		str_buf.format				= format;
		str_buf.fnt_type			= fnt_type;
		str_buf.width				= width;
		str_buf.height				= height;
		str_buf.str_len				= strlen;
		if( str_buf.str_len >= MAX_STR_DISP_LEN ) str_buf.str_len = MAX_STR_DISP_LEN;
		memcpy(str_buf.str_dat,string,str_buf.str_len );
		
		push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		return 0;
}

//czn_20181110
int API_OsdStringCenterReDisplayExt( int x, int y, int fgcolor, unsigned char* string, int strlen, int fnt_type,int format,int width,int height,int redisp_s,int redisp_e)	//czn_20181110
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
		
		if((str_buf.head.msg_source_id = GetMsgIDAccordingPid(pthread_self()))  == MSG_ID_Menu)
		{
			DisplayCenterStringRe(x, y, fgcolor, COLOR_KEY, string, strlen, fnt_type,format,width, height,redisp_s,redisp_e);
			return 0;
		}
		//str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 4;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.fg					= fgcolor;
		str_buf.bg					= COLOR_KEY;
		str_buf.format				= format;
		str_buf.fnt_type			= fnt_type;
		str_buf.width				= width;
		str_buf.height				= height;
		str_buf.str_len				= strlen;
		if( str_buf.str_len >= MAX_STR_DISP_LEN ) str_buf.str_len = MAX_STR_DISP_LEN;
		memcpy(str_buf.str_dat,string,str_buf.str_len );
		
		push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN + str_buf.str_len );
		return 0;
}
/*******************************************************************************
 * @fn      API_OsdStringClearExt()
 *
 * @brief   Provide an application interface to conctrol string clear
 *
 * @param   x 	- clear start col
 * @param   y	 - clear start row
 * @param   iLen - clear length
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_OsdStringClearExt(int x, int y, int width, int height)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
	
		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT;
		str_buf.head.msg_sub_type	= 0;

		str_buf.x					= x>=bkgd_w?bkgd_w-1:x;//x;
		str_buf.y					= y>=bkgd_h?bkgd_h-1:y;//y;
		str_buf.width 				= width>(bkgd_w-str_buf.x)?bkgd_w-str_buf.x:width;
		str_buf.height 				= height>(bkgd_h-str_buf.y)?bkgd_h-str_buf.y:height;
		str_buf.str_len				= 0;
		//x>=bkgd_w?bkgd_w-1:x,bkgd_h>=y?bkgd_h-1:y
		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringClearExt( str_buf.x, str_buf.y, str_buf.width, str_buf.height);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN );
		}
		
		return 0;
}

int API_OsdStringClearExt2(int x, int y, int layerId, int width, int height)
{
		string_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
	
		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT2;
		str_buf.head.msg_sub_type	= 0;
		

		str_buf.layerId				= layerId;
		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width 				= width;
		str_buf.height 				= height;
		str_buf.str_len				= 0;
		
		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringClearExt2( str_buf.x, str_buf.y, str_buf.layerId, str_buf.width, str_buf.height);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(string_type) - MAX_STR_DISP_LEN );
		}
		
		return 0;
}
/*******************************************************************************
 * @fn      API_BG_Display()
 *
 * @brief   Provide an application interface to conctrol back ground,not change menu id
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_BG_Display( unsigned char iBgId )
{
	int LoadMenuBackground(int bkgdcnt, int x, int y);
	void UpdateOsdLayerBuf(int clrscr);

	LoadMenuBackground( iBgId, 160, 720);
	//UpdateOsdLayerBufAlpha(0);
	UpdateOsdLayerBuf(0);
	return 1;
}
int API_SpriteAlpha( int x, int y, int iSpriteId )
{
	Menu_Sprite_Display_Alpha(x, y, iSpriteId);
}
int API_SpriteAlpha_Close( int x, int y, int iSpriteId )
{
	Menu_Sprite_Close_Alpha(x, y, iSpriteId);
}

/*******************************************************************************
 * @fn      API_SpriteDisplay()
 *
 * @brief   Provide an application interface to diplay sprite,not change menu id
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_SpriteDisplay_XY( int x, int y, int iSpriteId )
{
		sprite_type 				sprite_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		sprite_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		sprite_buf.head.msg_target_id	= MSG_ID_Menu;
		sprite_buf.head.msg_type		= MSG_SPRITE;
		sprite_buf.head.msg_sub_type	= 1;

		sprite_buf.bkgdcnt				= iSpriteId;
		sprite_buf.x					= x;
		sprite_buf.y					= y;

		if(sprite_buf.head.msg_source_id == MSG_ID_Menu)
		{
			Menu_Sprite_Display( sprite_buf.x, sprite_buf.y, sprite_buf.bkgdcnt);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&sprite_buf, sizeof(sprite_buf));
		}
		return 1;
}

/*******************************************************************************
 * @fn      API_SpriteClose()
 *
 * @brief   Provide an application interface to close sprite,not change menu id
 *
 * @param   event - 0/no wait event 1/wait event
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/ 
int API_SpriteClose( int x, int y, int iSpriteId )
{
		sprite_type 				sprite_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		sprite_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		sprite_buf.head.msg_target_id	= MSG_ID_Menu;
		sprite_buf.head.msg_type		= MSG_SPRITE;
		sprite_buf.head.msg_sub_type	= 0;

		sprite_buf.x					= x;
		sprite_buf.y					= y;
		sprite_buf.bkgdcnt				= iSpriteId;

		if(sprite_buf.head.msg_source_id == MSG_ID_Menu)
		{
			Menu_Sprite_Close( sprite_buf.x, sprite_buf.y, sprite_buf.bkgdcnt, 0 );
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&sprite_buf, sizeof(sprite_buf) );
		}
		return 1;
}
int API_SpriteCloseNoBG( int x, int y, int iSpriteId )
{
		sprite_type 				sprite_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		sprite_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		sprite_buf.head.msg_target_id	= MSG_ID_Menu;
		sprite_buf.head.msg_type		= MSG_SPRITE;
		sprite_buf.head.msg_sub_type	= 2;

		sprite_buf.x					= x;
		sprite_buf.y					= y;
		sprite_buf.bkgdcnt				= iSpriteId;

		if(sprite_buf.head.msg_source_id == MSG_ID_Menu)
		{
			Menu_Sprite_Close( sprite_buf.x, sprite_buf.y, sprite_buf.bkgdcnt, 1 );
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&sprite_buf, sizeof(sprite_buf) );
		}
		return 1;
}

int API_ProgBarDisplay( int x, int y, int sx, int sy, int color )
{
		progbar_type 				bar_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		bar_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		bar_buf.head.msg_target_id	= MSG_ID_Menu;
		bar_buf.head.msg_type		= MSG_PROG_BAR;
		bar_buf.head.msg_sub_type	= 1;

		bar_buf.x					= x;
		bar_buf.y					= y;
		bar_buf.width				= sx;
		bar_buf.height				= sy;
		bar_buf.bg					= color;
		if(bar_buf.head.msg_source_id == MSG_ID_Menu)
		{
			DisplayOneProgBar( bar_buf.x, bar_buf.y, bar_buf.width, bar_buf.height, bar_buf.bg);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&bar_buf, sizeof(bar_buf));
		}
		return 1;
}
int API_ProgBarClose( int x, int y, int sx, int sy)
{
		progbar_type 				bar_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		bar_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		bar_buf.head.msg_target_id	= MSG_ID_Menu;
		bar_buf.head.msg_type		= MSG_PROG_BAR;
		bar_buf.head.msg_sub_type	= 0;

		bar_buf.x					= x;
		bar_buf.y					= y;
		bar_buf.width				= sx;
		bar_buf.height				= sy;
		if(bar_buf.head.msg_source_id == MSG_ID_Menu)
		{
			ClearOneProgBar( bar_buf.x, bar_buf.y, bar_buf.width, bar_buf.height);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&bar_buf, sizeof(bar_buf));
		}
		return 1;
}

//cao_20151118
/*******************************************************************************
 * @fn      API_TimeLapseStart()
 *
 * @brief   Provide an application interface to start a TimeLapse display
 *
 * @param   iLabelIndex - label index to display
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_TimeLapseStart( TimeLapseMode iCountMode, unsigned int iStartTime )
{
	TimeLapse_type msg_buf;

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_TIME_LAPSE;
	msg_buf.control				= iCountMode;
	msg_buf.inittime				= iStartTime;
	if(msg_buf.head.msg_source_id == MSG_ID_Menu)
	{
		TimeLapseStart(msg_buf.control, msg_buf.inittime);//return SwitchOneMenu(uMenu, subMenu, pushstack);
	}
	else
	{
		push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(TimeLapse_type) );
	}
	return 1;
}
	

/*******************************************************************************
 * @fn      API_TimeLapseShow()
 *
 * @brief   Provide an application interface to conctrol TimeLapse show again
 *
 * @param   
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_TimeLapseShow( void )
{
	TimeLapse_type msg_buf;

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_TIME_LAPSE;
	msg_buf.control				= COUNT_SHOW;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(TimeLapse_type) );
	return 1;
}


/*******************************************************************************
 * @fn      API_TimeLapseHide()
 *
 * @brief   Provide an application interface to conctrol TimeLapse hide
 *
 * @param   
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_TimeLapseHide( void )
{
	TimeLapse_type msg_buf;

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_TIME_LAPSE;
	msg_buf.control				= COUNT_HIDE;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(TimeLapse_type) );
	return 1;
}

/*******************************************************************************
 * @fn      API_TimeLapseStop()
 *
 * @brief   Provide an application interface to conctrol TimeLapse stop
 *
 * @param   
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_TimeLapseStop(void)
{
	TimeLapse_type msg_buf;
	//p_vdp_common_buffer video_menu_syc;
	//vdp_task_t*			ptask;	

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_TIME_LAPSE;
	msg_buf.control				= COUNT_STOP;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(TimeLapse_type) );
	return 1;
}

/*******************************************************************************
 * @fn      API_DateTimeShow()
 *
 * @brief   Provide an application interface to conctrol date time display
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_DateTimeShow(void)
{
	DateTime_type msg_buf;

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_DATE_TIME;
	msg_buf.control				= DATE_TIME_ON;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(DateTime_type) );
	return 1;
}

/*******************************************************************************
 * @fn      API_DateTimeHide()
 *
 * @brief   Provide an application interface to conctrol date time clear
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
unsigned char API_DateTimeHide(void)
{
	DateTime_type msg_buf;

	msg_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg_buf.head.msg_target_id 	= MSG_ID_Menu;
	msg_buf.head.msg_type		= MSG_DATE_TIME;
	msg_buf.control				= DATE_TIME_OFF;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(DateTime_type) );
	return 1;
}

unsigned char API_FixedCurIconCursor(unsigned char curIconCursor)
{
	#if 0
	VDP_MSG_HEAD msg_buf;

	msg_buf.msg_source_id 	= MSG_ID_Menu; 
	msg_buf.msg_target_id 	= MSG_ID_Menu;
	msg_buf.msg_type		= MSG_CURSOR;
	msg_buf.msg_sub_type	= curIconCursor;
	
	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg_buf, sizeof(VDP_MSG_HEAD) );
	#endif
	return 1;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
//log view interface
///////////////////////////////////////////////////////////////////////////////////////////////////
static int logViewState = 0;	// 0: off, 1: on

int API_GetOsdLogViewState( void )
{
	return logViewState;
}

int API_OsdLogViewAddRec( unsigned char* string )
{
		LogView_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		int add_len;

		if( 1 ) //logViewState )
		{
			str_buf.head.msg_source_id	= MSG_ID_Menu;
			str_buf.head.msg_target_id	= MSG_ID_Menu;
			str_buf.head.msg_type		= MSG_LOGVIEW;
			str_buf.head.msg_sub_type	= LOG_VIEW_TYPE_ADD_LOG_DATA;

			str_buf.str_len				= strlen(string)+1; //include 0
			
			if( str_buf.str_len > LOG_VIEW_MAX_STR )	str_buf.str_len = LOG_VIEW_MAX_STR;
			
			memcpy(str_buf.str_dat,string,str_buf.str_len );

			str_buf.str_dat[str_buf.str_len-1] = 0;

			add_len = sizeof(LogView_type) - LOG_VIEW_MAX_STR + str_buf.str_len;
				
			printf("API_OsdLogViewAddRec add_len = %d, dat_len = %d..........\n",add_len,str_buf.str_len);
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, add_len );
			return 0;
		}
		else		
			return -1;
}


int API_SetOsdLogViewState( int onoff )
{
		logViewState = onoff;

		LogView_type 				str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;

		str_buf.head.msg_source_id	= MSG_ID_Menu;
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_LOGVIEW;
		if( onoff )
			str_buf.head.msg_sub_type	= LOG_VIEW_TYPE_TURN_ON;
		else
			str_buf.head.msg_sub_type	= LOG_VIEW_TYPE_TURN_OFF;

		str_buf.str_len 			= 0;
		push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(LogView_type) - LOG_VIEW_MAX_STR + str_buf.str_len );
		return 0;
}

int API_SwitchOsdLogViewState( void )
{
		logViewState = !logViewState;

		return API_SetOsdLogViewState(logViewState);
}
//czn_20190412_s
int API_ClearLogViewWin(void)	//20181017
{
	LogView_type 				str_buf;
	//p_vdp_common_buffer 		video_menu_syc;
	//vdp_task_t* 				ptask;

	str_buf.head.msg_source_id	= MSG_ID_Menu;
	str_buf.head.msg_target_id	= MSG_ID_Menu;
	str_buf.head.msg_type		= MSG_LOGVIEW;
	str_buf.head.msg_sub_type	= LOG_VIEW_TYPE_CLEAR_WIN;

	str_buf.str_len 			= 0;
	
	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(LogView_type) - LOG_VIEW_MAX_STR + str_buf.str_len );
}
//czn_20190412_e
/*******************************************************************************
 * @fn      API_MenuIconDisplaySelectOn()
 *
 * @brief   
 *
 * @param   icon icon��
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
	int DisplayOneFuncIcon(unsigned short icon_func,unsigned char uDispOn);
int API_MenuIconDisplaySelectOn( uint16 icon )
{
	printf("API_MenuIconDisplaySelectOn:icon[%d]\n",icon);	
	DisplayOneFuncIcon(icon,1);
	return 0;
}

/*******************************************************************************
 * @fn      API_MenuIconDisplaySelectOff()
 *
 * @brief   
 *
 * @param   icon icon��
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
	int DisplayOneFuncIcon(unsigned short icon_func,unsigned char uDispOn);
int API_MenuIconDisplaySelectOff( uint16 icon )
{
	printf("API_MenuIconDisplaySelectOff:icon[%d]\n",icon);
	DisplayOneFuncIcon(icon,0);
	return 0;
}


// lzh_20161103_e

int API_EnableOsdUpdate( void )
{
	void EnableUpdate(void);
	void UpdateOsdLayerBuf(int clrscr);
	
	EnableUpdate();
	UpdateOsdLayerBuf(1);
	return 0;	

}

void DisableUpdate(void);

int API_DisableOsdUpdate( void )
{	
	DisableUpdate();
	return 0;	
}

int StartInitOneMenu(int uMenu,int subMenu,int pushstack)
{
	p_vdp_common_buffer menu_syc;
	vdp_task_t*			ptask;	
	MenuSwitchType		msg;	
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.head.msg_target_id 	= MSG_ID_Menu;
	msg.head.msg_type		= MSG_MENU_SWITCH;
	msg.head.msg_sub_type	= 0;
	msg.uMenu 				= uMenu;
	msg.subMenu	 			= subMenu;
	msg.pushstack			= pushstack;

	if(msg.head.msg_source_id == MSG_ID_Menu)
	{
		dprintf("-----------SwitchOneMenu-----------\n");		
		return SwitchOneMenu(uMenu, subMenu, pushstack);
	}
	else
	{
		dprintf("-----------push_vdp_common_queue-----------\n");		
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(MenuSwitchType)) != 0 )
		{
			dprintf("-----------StartInitOneMenu:push_vdp_common_queue just full-----------\n");		
			return -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.head.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
					return 1;	
				}
				else
				{
					dprintf("-----------StartInitOneMenu: wait sync timeout-----------\n");		
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	}
}

int CloseOneMenu(void)
{
	p_vdp_common_buffer menu_syc;
	vdp_task_t*			ptask;	
	VDP_MSG_HEAD		msg;	
	
	msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.msg_target_id 	= MSG_ID_Menu;
	msg.msg_type		= MSG_MENU_CLOSE;
	msg.msg_sub_type	= 0;

	if(msg.msg_source_id == MSG_ID_Menu)
	{
		return CloseMenu();
	}
	else
	{
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(VDP_MSG_HEAD)) != 0 )
		{
			dprintf("-----------CloseOneMenu:push_vdp_common_queue just full-----------\n");		
			return -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
					return 1;	
				}
				else
				{
					dprintf("-----------CloseOneMenu: wait sync timeout-----------\n");		
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	}
}


int	API_add_hal_message_to_VideoMenu_queue( char* pdata, unsigned int len )	//czn_20170807
{
	if(task_VideoMenu.task_run_flag == 0)
	{
		return -1;
	}
	
	return push_vdp_common_queue(task_VideoMenu.p_msg_buf, pdata, len);
}

void API_add_Inform_to_VideoMenu_queue(int inform)
{
	VDP_MSG_HEAD brd_msg;
	
	brd_msg.msg_source_id	= MSG_ID_hal;
	brd_msg.msg_target_id	= MSG_ID_Menu;
	brd_msg.msg_type		= HAL_TYPE_BRD;
	brd_msg.msg_sub_type	= inform;
	API_add_hal_message_to_VideoMenu_queue( (char*) &brd_msg, sizeof(VDP_MSG_HEAD));
}

void API_add_Inform_with_data_to_VideoMenu_queue(int inform, char* data, int len)
{
	VDP_MSG_HEAD brd_msg;
	char msgTemp[500];
	
	if(len + sizeof(VDP_MSG_HEAD) > 500)
	{
		return;
	}
	
	brd_msg.msg_source_id	= MSG_ID_hal;
	brd_msg.msg_target_id	= MSG_ID_survey;
	brd_msg.msg_type		= HAL_TYPE_BRD_WITH_DATA;
	brd_msg.msg_sub_type	= inform;
	
	memcpy(msgTemp, &brd_msg, sizeof(VDP_MSG_HEAD));
	memcpy(msgTemp+sizeof(VDP_MSG_HEAD), data, len);
	
	API_add_hal_message_to_VideoMenu_queue(msgTemp, sizeof(VDP_MSG_HEAD)+len);
}

// lzh_20181016_s
void API_menu_display_cache_push( int menu_id, int layer_id, int cache_num, int x, int y, int width, int height )
{
	p_vdp_common_buffer 	menu_syc;
	vdp_task_t*				ptask;	
	menu_disp_cache_type	msg;	
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.head.msg_target_id 	= MSG_ID_Menu;
	msg.head.msg_type		= MSG_MENU_DISP_CACHE;
	msg.head.msg_sub_type	= 0;
	msg.menu_id				= menu_id;
	msg.layer_id			= layer_id;
	msg.cache_num			= cache_num;
	msg.x					= x;
	msg.y					= y;
	msg.width				= width;
	msg.height				= height;

	if(msg.head.msg_source_id == MSG_ID_Menu)
	{
		return menu_display_cache_push(menu_id, layer_id, cache_num, x, y, width, height);
	}
	else
	{
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(menu_disp_cache_type)) != 0 )
		{
			dprintf("-----------API_menu_display_cache_push:push_vdp_common_queue just full-----------\n");		
			return -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.head.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
					return 1;	
				}
				else
				{
					dprintf("-----------API_menu_display_cache_push: wait sync timeout-----------\n");		
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	}	
}

void API_menu_display_cache_pop( int menu_id, int layer_id, int cache_num )
{
	p_vdp_common_buffer 	menu_syc;
	vdp_task_t*				ptask;	
	menu_disp_cache_type	msg;	
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.head.msg_target_id 	= MSG_ID_Menu;
	msg.head.msg_type		= MSG_MENU_DISP_CACHE;
	msg.head.msg_sub_type	= 1;
	msg.menu_id				= menu_id;
	msg.layer_id			= layer_id;
	msg.cache_num			= cache_num;
	msg.x					= 0;
	msg.y					= 0;
	msg.width				= 0;
	msg.height				= 0;

	if(msg.head.msg_source_id == MSG_ID_Menu)
	{
		return menu_display_cache_pop(menu_id, layer_id, cache_num);
	}
	else
	{
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(menu_disp_cache_type)) != 0 )
		{
			dprintf("-----------API_menu_display_cache_pop:push_vdp_common_queue just full-----------\n");		
			return -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.head.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
					return 1;	
				}
				else
				{
					dprintf("-----------API_menu_display_cache_pop: wait sync timeout-----------\n");		
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	}	
}

void API_menu_display_cache_push_string( int menu_id, int cache_num, int x, int y, int width, int height )
{
	API_menu_display_cache_push( menu_id, OSD_LAYER_STRING, cache_num, x, y, width, height);
}

void API_menu_display_cache_pop_string( int menu_id, int cache_num )
{
	API_menu_display_cache_pop( menu_id, OSD_LAYER_STRING, cache_num);
}

// lzh_20181016_e

// lzh_20181108_s

int MenuDisplayAck_Rsp_with_result( unsigned char msg_id, unsigned char msg_type, unsigned char result )
{
	VDP_MSG_HEAD  disp_type;
	vdp_task_t* pTask = GetTaskAccordingMsgID(msg_id);

	if(pTask == NULL)
	{
		return -1;
	}
	
	disp_type.msg_target_id = msg_id;
	disp_type.msg_source_id	= MSG_ID_Menu;			
	disp_type.msg_type		= (msg_type|COMMON_RESPONSE_BIT);
	disp_type.msg_sub_type	= result;
	push_vdp_common_queue(pTask->p_syc_buf, (char*)&disp_type, sizeof(VDP_MSG_HEAD));
	
	return 0;
}

int API_create_bmpfile_for_menu( int menu_id, int sub_id, int sub2_id )
{
	if( menu_id == -1 ) menu_id = GetCurMenuCnt();
	if( sub_id == -1 ) sub_id = GetCurSubMenuCnt();
	if( sub2_id == -1 ) sub2_id = GetCurSubMenuCursor();	
	return create_bmpfile_for_menu( menu_id, sub_id, sub2_id );	
}

// -1/ err, 0/ok, 1/no sdcard, 2/sdcard err
int API_menu_create_bitmap_file( int menu_index, int sub_index, int sub2_index )
{
	int ret;
	
	p_vdp_common_buffer 	menu_syc;
	vdp_task_t*				ptask;	
	menu_disp_cache_type	msg;	
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.head.msg_target_id 	= MSG_ID_Menu;
	msg.head.msg_type		= MSG_MENU_DISP_CACHE;
	msg.head.msg_sub_type	= 2;
	msg.menu_id				= menu_index;
	msg.layer_id			= sub_index;
	msg.cache_num			= sub2_index;
	msg.x					= 0;
	msg.y					= 0;
	msg.width				= 0;
	msg.height				= 0;

	if(msg.head.msg_source_id == MSG_ID_Menu)
	{
		ret = API_create_bmpfile_for_menu(menu_index, sub_index, sub2_index);
	}
	else
	{
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(menu_disp_cache_type)) != 0 )
		{
			ret = -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.head.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					int result;
					VDP_MSG_HEAD  *ptr_sync_result;
					ptr_sync_result = (VDP_MSG_HEAD*)menu_syc;
					result = ptr_sync_result->msg_sub_type;
					
					purge_vdp_common_queue(ptask->p_syc_buf);
					ret = result;	
				}
				else
				{
					ret = -1;
				}
			}
			else
			{
				ret = -1;
			}
		}
	}	
	// �����Ӧ: ok-���ѣ�sd������-3��
	if( ret == -1 )
		BEEP_ERROR();
	else if( ret == 0 )
		BEEP_CONFIRM();
	else
		BEEP_INFORM();		
	return ret;
}
// lzh_20181108_e

int API_MenuThreadHeartbeatReq( void )	//czn_20190910
{
	video_menu_type 	video_menu_buf;
	p_vdp_common_buffer video_menu_syc;
	vdp_task_t*			ptask;	

	video_menu_buf.head.msg_source_id 	= MSG_ID_Menu; //GetMsgIDAccordingPid(pthread_self());	
	//video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	video_menu_buf.head.msg_target_id 	= MSG_ID_Menu;
	video_menu_buf.head.msg_type		= MSG_MENU_ThreadHeartReq;
	video_menu_buf.head.msg_sub_type	= 0;

	push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&video_menu_buf, sizeof(video_menu_type) );
	return 0;
}

int API_Menu_BmpDisplay(int layer,char *bmp_file,int x,int y,int sx,int sy)
{
	Menu_BmpDisplay(layer,bmp_file,x,y,sx,sy);
	return 0;
}

static int API_OsdFillColor(int layerId, int x, int y, int xsize, int ysize, int color, int option, int alpha)
{
		MenuFillColorMsg_t 			str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
	
		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_FILL_COLOR;
		str_buf.head.msg_sub_type	= 0;

		str_buf.layerId				= layerId;
		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.xsize				= xsize;
		str_buf.ysize 				= ysize;
		str_buf.color				= color;
		str_buf.option				= option;
		str_buf.alpha				= alpha;
		
		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OsdLayerFillColor(layerId, x, y, xsize, ysize, color, option, alpha);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(str_buf));
		}
		
		return 0;
}

int API_OsdCursorListIconColor(int x, int y, int xsize, int ysize, int color, int option, int alpha)
{
	return API_OsdFillColor(OSD_LAYER_CURSOR, x, y, xsize, ysize, color, option, alpha);
}

/*******************************************************************************
 * @fn      API_OsdStringDisplayExtMultiLine()
 *
 * @brief   
 *
 * @param   none
 *
 * @return  0 - false, 1 - ok
 ******************************************************************************/
int API_OsdStringDisplayExtMultiLine(MenuStrMulti_T list)
{
		MenuStrMultiMsg_t 			str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
		int len;

		str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_STRING_EXT_MULTI_LINE;
		str_buf.head.msg_sub_type	= 1;

		str_buf.list = list;

		len = sizeof(VDP_MSG_HEAD) + sizeof(list.dataCnt) + sizeof(MenuOneString_T)*list.dataCnt;

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_StringDisplayExtOnePageList(list);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, len);
		}
		
		return 0;
}


int API_OsdUpdateDisplay(int enable, int x, int y, int width, int height, int alpha)
{
		MenuUpdateRegion_T			str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
	
		str_buf.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_UPDATE_REGION;

		str_buf.x					= x;
		str_buf.y					= y;
		str_buf.width 				= width;
		str_buf.height 				= height;
		str_buf.enable				= enable;
		str_buf.alpha				= alpha;
		
		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			OSD_UpdateDisplay(enable, x, y, width, height, alpha);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, sizeof(str_buf));
		}
		
		return 0;
}

int API_OsdSpriteDisplayMulti(MenuSpriteMulti_T sprite)
{
		MenuSpriteMultiMsg_t 		str_buf;
		p_vdp_common_buffer 		video_menu_syc;
		vdp_task_t* 				ptask;
		int len;

		str_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
		str_buf.head.msg_target_id	= MSG_ID_Menu;
		str_buf.head.msg_type		= MSG_SPRITE_MULTI;
		str_buf.head.msg_sub_type	= 0;

		str_buf.sprite = sprite;

		len = sizeof(VDP_MSG_HEAD) + sizeof(sprite.dataCnt) + sizeof(MenuOneSprite_T)*sprite.dataCnt;

		if(str_buf.head.msg_source_id == MSG_ID_Menu)
		{
			Menu_Sprite_Display_Multi(sprite);
		}
		else
		{
			push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&str_buf, len);
		}
		
		return 0;
}

int API_OsdIconPicDisplayMulti(MenuSpriteMulti_T sprite)
{
	int i;

	for(i = 0; i < sprite.dataCnt; i++)
	{
		sprite.data[i].layerId = OSD_LAYER_CURSOR;
	}
	return API_OsdSpriteDisplayMulti(sprite);
}

int API_OsdIconPicDisplay(int x, int y, int xsize, int ysize, int align, int spriteId, int alpha)
{
	dprintf("API_OsdIconPicDisplay  alpha=%d, spriteId=%d\n", alpha, spriteId);
	MenuSpriteMulti_T sprite;

	sprite.dataCnt = 1;
	sprite.data[0].enable = 1;
	sprite.data[0].x = x;
	sprite.data[0].y = y;
	sprite.data[0].width = xsize;
	sprite.data[0].height = ysize;
	sprite.data[0].spriteId = spriteId;
	sprite.data[0].align = align;
	sprite.data[0].alpha = alpha;
	sprite.data[0].layerId = OSD_LAYER_CURSOR;

	return API_OsdSpriteDisplayMulti(sprite);
}

int API_OsdPicDisplay(int layerId, int x, int y, int xsize, int ysize, int align, int spriteId, int alpha, int enable)
{
	MenuSpriteMulti_T sprite;

	sprite.dataCnt = 1;
	sprite.data[0].enable = enable;
	sprite.data[0].x = x;
	sprite.data[0].y = y;
	sprite.data[0].width = xsize;
	sprite.data[0].height = ysize;
	sprite.data[0].spriteId = spriteId;
	sprite.data[0].align = align;
	sprite.data[0].alpha = alpha;
	sprite.data[0].layerId = layerId;

	return API_OsdSpriteDisplayMulti(sprite);
}

int API_OsdDisplayExtString(int x, int y, int xsize, int ysize, int align, char* unicode, int unicodeLen, int fntSize, int color)
{
	MenuStrMulti_T list;

	list.dataCnt = 0;
	list.data[list.dataCnt].x = x;
	list.data[list.dataCnt].y = y;
	list.data[list.dataCnt].fg = color;
	list.data[list.dataCnt].uBgColor = COLOR_KEY;
	list.data[list.dataCnt].format = STR_UNICODE;
	list.data[list.dataCnt].width = xsize;
	list.data[list.dataCnt].height = ysize;
	list.data[list.dataCnt].fnt_type = fntSize;
	list.data[list.dataCnt].align = align;
	list.data[list.dataCnt].str_len = unicodeLen;
	memcpy(list.data[list.dataCnt].str_dat, unicode, list.data[list.dataCnt].str_len);
	list.dataCnt++;

	return API_OsdStringDisplayExtMultiLine(list);
}

int API_OsdBackGroundDisplay(int x, int y, int xsize, int ysize, int align, int spriteId)
{
	MenuSpriteMulti_T sprite;

	sprite.dataCnt = 1;
	sprite.data[0].enable = 1;
	sprite.data[0].x = x;
	sprite.data[0].y = y;
	sprite.data[0].width = xsize;
	sprite.data[0].height = ysize;
	sprite.data[0].spriteId = spriteId;
	sprite.data[0].align = align;
	sprite.data[0].layerId = OSD_LAYER_BKGD;

	return API_OsdSpriteDisplayMulti(sprite);
}

int API_ListAlpha_Close(int id, int x,int y,int sx,int sy, int layer)
{
	Menu_ListView_Close(id,x,y,sx,sy,layer);
	return 0;
}

// lzh_20210910_s
int api_disp_icon_bind_sprite( uint16 icon, int offset )
{
	DisplayOneFuncIcon(icon,0);
	return 0;
}
// lzh_20210910_e
extern int power_Down_Timer_Cnt_Io;
int KeyBeepDisable = 0;
static int ScheduleDisable=0;
static char CustomerizedNameBuff[40]={0};
const char *GetCustomerizedName(void)
{
	return CustomerizedNameBuff;
}
int Get_ScheduleDisable(void)
{
	return ScheduleDisable;
}
void Load_Menu_ColorPara(void)
{
	char buff[50];
	
	if(API_Event_IoServer_InnerRead_All(AutoCloseScreenTime, buff)==0)
	{
		power_Down_Timer_Cnt_Io = atoi(buff);
		//printf("Load_Menu_ColorPara77:%04x\n",CallMenuDisp_Name_Color);
	}
	
	if(API_Event_IoServer_InnerRead_All(KeyBeepDis, buff)==0)
	{
		KeyBeepDisable = atoi(buff);
	}
	if(API_Event_IoServer_InnerRead_All(ParaDeviceType, buff)==0)
	{
		if(strcmp(buff,"B-M100")==0)
		{
			ScheduleDisable=1;
		}
	}
	if(API_Event_IoServer_InnerRead_All(CustomerizedName, CustomerizedNameBuff)!=0)
	{
		CustomerizedNameBuff[0]=0;
	}
	AutoSetDateInit();

	//#endif
}

unsigned char API_RM_Display(unsigned char *disp_buf, unsigned char len)
{
	p_vdp_common_buffer 	menu_syc;
	vdp_task_t*				ptask;	
    MsgRmStruct msg;

	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self()); 
	msg.head.msg_target_id 	= MSG_ID_Menu;
	msg.head.msg_type		= MSG_MENU_DISP_RM;
	msg.head.msg_sub_type	= 1;
    
    // put queue
	memset(msg.disp_buf,0,MAX_RM_CMD_LEN);
    msg.disp_buf[0] = len;
	memcpy(msg.disp_buf+1,disp_buf,len);

	if(msg.head.msg_source_id == MSG_ID_Menu)
	{
		#if defined(PID_DX470)||defined(PID_DX482)
		return Rm_Display(msg.disp_buf);
		#endif
	}
	else
	{
		push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(msg));
		#if 0
		if( push_vdp_common_queue( &vdp_video_menu_mesg_queue, (char*)&msg, sizeof(msg)) != 0 )
		{
			return -1;
		}
		else
		{
			ptask = GetTaskAccordingMsgID(msg.head.msg_source_id);
			if( ptask != NULL )
			{
				if( pop_vdp_common_queue( ptask->p_syc_buf, &menu_syc, 2000) > 0 )
				{
					purge_vdp_common_queue(ptask->p_syc_buf);		//czn_20160516
					return 1;	
				}
				else
				{
					dprintf("-----------API_RM_Display: wait sync timeout-----------\n");		
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
		#endif
	}	
    return 1;    
}

void API_VideoMenu_Ring2(char* msg, int time)
{
	Ring2Msg_t ring2Msg;
	snprintf(ring2Msg.msg, 450, "%s", msg);
	ring2Msg.time = (time > 0 ? time : 5);
	
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_RING2_REQ, (char*)&ring2Msg, sizeof(ring2Msg));
}
