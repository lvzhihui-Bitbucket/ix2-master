/**
  ******************************************************************************
  * @file    Obj_OsdExtLable.h
  * @author  lv
  * @version V1.0.0
  * @date    2013.10.30
  * @brief   This file contains the headers of the ObjLable.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2013 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef _OBJ_OSDEXTLABLE_H
#define _OBJ_OSDEXTLABLE_H

#define EXT_LABEL_TYPE_1		0
#define EXT_LABEL_TYPE_2		1
#define EXT_LABEL_TYPE_3		2
#define EXT_LABEL_TYPE_4		3
#define EXT_LABEL_TYPE_5		4
#define EXT_LABEL_TYPE_6		5
#define EXT_LABEL_TYPE_7		6
#define EXT_LABEL_TYPE_8		7
#define EXT_LABEL_TYPE_9		8
#define EXT_LABEL_TYPE_10		9
#define EXT_LABEL_TYPE_11		10
#define EXT_LABEL_TYPE_12		11
#define EXT_LABEL_TYPE_13		12
#define EXT_LABEL_TYPE_14		13
#define EXT_LABEL_TYPE_15		14
#define EXT_LABEL_TYPE_16		15
#define EXT_LABEL_TYPE_17		16
#define EXT_LABEL_TYPE_18		17
#define EXT_LABEL_TYPE_19		18
#define EXT_LABEL_TYPE_20		19

	
typedef enum
{   
    // type 1
	EL_Down_Index,
	EL_Up_Index,
    	EL_Missed_Index,
    	EL_Paly_Index,
        EL_LEFT_Index,
        EL_RIGHT_Index,
    	EL_MIDDLE_Index,
    	EL_LEFT_UP_Index,
    	EL_RIGHT_UP_Index,
    	EL_LEFT_DN_Index,
    	EL_RIGHT_DN_Index,
    	EL_MID_DOWN_Index,
    	EL_MID_UP_Index,
    	EL_HOME_Index,
    // type 2
	EL_Unlock1_Index,
	EL_Unlock2_Index,
	EL_Light_Index,
	EL_Bussy_Index,
    	EL_Tick_Index,
    	EL_Menu_Index,
    	EL_Transfer_Index,
    	EL_Do_Not_Disturb_Index,
    	EL_Lock_Index,
    	EL_Alarm_Index,
    	EL_Current_Index, 
    	EL_Tlak_Index,
    	EL_Tip_Index,
    	EL_Tip1_Index,
    	EL_Voice_Index,
    // type3
	EL_Big_Light_1_Index,
	EL_Big_Light_2_Index,
	EL_Big_Busy_1_Index,
	EL_Big_Busy_2_Index,
	EL_Big_Disturb_1_Index,
	EL_Big_Disturb_2_Index,
	EL_Big_Transfer_1_Index,
	EL_Big_Transfer_2_Index, 
	EL_Big_Voice_1_Index,
	EL_Big_Voice_2_Index,
    // type 5
    	EL_Image_Index,
    	EL_Video_Index,
    	EL_Pause_Index,    	
    // type 7
	EL_Norma_Index,
	EL_Bright_Index,
	EL_Soft_Index,
	EL_User_Index,
    	EL_Delete_Index,
    	EL_Record_Index,
    	EL_Connated_Index,
    // type 10
	EL_Progress_0_Index,
	EL_Progress_1_Index,
	EL_Progress_2_Index,
	EL_Progress_3_Index,
    	EL_Progress_4_Index,
    	EL_Progress_5_Index,
    	EL_Progress_6_Index,
    	EL_Progress_7_Index,
    	EL_Progress_8_Index,
    	EL_Progress_9_Index,     	
    // type 11
	EL_Loading_Files_Index,
    	EL_Refuse_Calls_In_Index,
    	EL_Missed_Calls_Index,
    	EL_No_Sim_Card_Index,
    	EL_Not_Connect_Index,
    	EL_Login_Failed_Index,
    	EL_Calling_Index,
    	EL_Call_End_Index,
     // type 18
	EL_Update_Display_Driver_Index,
	EL_Failed_To_Send_Sms_Index,
	EL_Notify_Transfer_Phone_Index,
	EL_Door_1_Unlock_Error_Index,
	EL_Door_2_Unlock_Error_Index,
	EL_Door_3_Unlock_Error_Index,
	EL_Door_4_Unlock_Error_Index,	
	EL_Send_Sos_Sms_Index,
    	EL_Sms_Sent_Out_Index,
    	EL_Format_SD_Card_Index,
    	EL_Update_Software_Index,
    	EL_Door_1_Opened_Index,
    	EL_Door_2_Opened_Index,
    	EL_Door_3_Opened_Index,
    	EL_Door_4_Opened_Index,
    	EL_Please_Wait_Index,    	
	EL_Door_Opened_Index,
	EL_Restore_To_Default_Index,
	EL_Copy_Pictures_To_SD_Index,
	EL_Failed_Index,
	EL_Succeed_Index,
	EL_Have_No_Files_Index,
	EL_Have_No_This_File_Index,	
    	 EL_MON_SELECT_Index,
    	 EL_Receive_Time_Index,
   	 EL_Send_Time_Index,
   	 EL_NOT_DISTURB_Index,
    	 EL_TRANSFER_1_Index,
    	 EL_VOICE_PROMPT_Index,
    	 EL_NAMELIST_Index,
    	 EL_CALL_RECORD_Index,
    	 EL_Door_Rename_Index,
   	 EL_Camera_Rename_Index,
	 EL_Monitor_Time_Set_Index,
    	 EL_Ring_Tone_Index,
    	 EL_Volume_Index,
    	 EL_Door_Ring_Mode_Index,
    	 EL_Date_Time_Index,
    	 EL_Date_Sync_Index,
    	 EL_Language_Index,
    	 EL_ABOUT_Index,
    	 EL_Local_Address_Index,
    	 EL_Video_Standard_Index,
    	 EL_System_Verson_Index,
    	 EL_Display_Driver_Index,
    	 EL_FONT_Index,
    	 EL_UI_Index,
    	 EL_SD_INFO_Index,
    	 EL_SD_Card_Index,
   	 EL_Video_Capacity_Index,
    	 EL_Video_Usage_Index,
    	 EL_FLASH_Index,
    	 EL_Image_Capacity_Index,
    	 EL_Image_Usage_Index,
    	 EL_TRANSFER_DEVCE_INFO_Index,
    	 EL_DEVICE_Index,
    	 EL_SIM_Index,
    	 EL_NETWORK_Index,
    	 EL_SIGNAL_Index,
    	 EL_NAME_Index,
    	 EL_DAY_Index,
    	 EL_NIGHT_Index,
    	 EL_SELECT_RECEIVER_Index,
    	 EL_TEL_NUM1_Index,
    	 EL_TEL_NUM2_Index,
    	 EL_TEL_NUM3_Index,
    	 EL_VISITOR_MESSAGE_Index,
    	 EL_KEYPAD_Index,
    	EL_Door_1_Motion_Index,
    	EL_Door_2_Motion_Index,
    	EL_Door_3_Motion_Index,
    	EL_Door_4_Motion_Index,
} ExtLableType;

void Show_Ext_Label(int row, int col, int color, ExtLableType extlabelnum);
void Hide_Ext_Label(int row, int col, ExtLableType extlabelnum);

#endif
