/**
  ******************************************************************************
  * @file    obj_Language.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.06
  * @brief   This file contains the function of multi language info .
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
	  
#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include "obj_Language.h"

LanguageType 	language;

/*******************************************************************************
 * @fn      LanguageInit()
 *
 * @brief   init language para
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void LanguageInit(void)
{
	unsigned char i;

	//T47m_ReadSpiFlashData( 0, (unsigned char*)&language,  sizeof(LanguageType)  );
	
	//NVMEM_Read_P(EX_SPI_FLASH, 0, (unsigned char*)&language, sizeof(LanguageType) );
	//API_Event_IoServer_InnerRead_All( LANGUAGE_USABLE_NUM, (unsigned char*)&language.UsableNum );
	if( language.UsableNum >= MAX_LANGUAGE_NUMS )
	{
	    language.UsableNum = 1;
	}

	for( i = 0; i < language.UsableNum; i++)
	{
	    //API_Event_IoServer_InnerRead_All( LANGUAGE_INDEX0+i, (unsigned char*)&(language.LanguageIndex[i]) );
	}

}

/*******************************************************************************
 * @fn      GetMaxUsableLanguageNum()
 *
 * @brief   get the max usable language number
 *
 * @param   none
 *
 * @return  language.UsableNum
 ******************************************************************************/
unsigned char GetMaxUsableLanguageNum(void)
{
    //API_Event_IoServer_InnerRead_All( LANGUAGE_USABLE_NUM, (unsigned char*)&language.UsableNum );
    return language.UsableNum;
}

/*******************************************************************************
 * @fn      GetCurLanguage()
 *
 * @brief   get the current language index
 *
 * @param   none
 *
 * @return  current language index
 ******************************************************************************/
unsigned char GetCurLanguage(void)
{
    //API_Event_IoServer_InnerRead_All( LANGUAGE_SELECT, (unsigned char*)&language.CurSelNum );
    //if( language.CurSelNum >= language.UsableNum )
    {
  //      language.CurSelNum = 0;
    }
	
    return 0; //language.CurSelNum;	
}

/*******************************************************************************
 * @fn      GetCurLanguage()
 *
 * @brief   get the current language index, according to flash language package
 *
 * @param   none
 *
 * @return  current language index
 ******************************************************************************/
unsigned char GetCurLanguageIndex(unsigned char cur)
{
    return language.CurSelNum;	 //language.LanguageIndex[cur];
}

/*******************************************************************************
 * @fn      SetCurLanguage()
 *
 * @brief   set the current language code
 *
 * @param   LangIndex - language index, according to LanguageIndex table offset
 *
 * @return  0/err, 1/ok
 ******************************************************************************/
unsigned char SetCurLanguage( unsigned char LangIndex )
{
	language.CurSelNum = LangIndex;	
	return 1;
}

