#include "MENU_107_Manager.h"

extern int SetMyName(const char* name);
extern void SipEnableSet(int value);
extern void IntercomEnableSet(int value);
extern char installCallNumInput[];
//#define	ManagerIconMax	5
int ManagerIconMax;
int ManagerIconSelect;
int ManagerPageSelect;
int private_code_mask = 0;

const IconAndText_t ManagerSetMode0[] = 
{
	{ICON_027_SipConfig,				MESG_TEXT_SipConfig},
	{ICON_051_PrivateUnlockCode,		MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_052_PublicUnlockCode,		MESG_TEXT_ICON_PublicUnlockCode},
	{ICON_053_CardManagement,		MESG_TEXT_ICON_CardManagement},
	{ICON_BACKUP_RESTORE,			MESG_TEXT_ICON_BACKUP_RESTORE},
	//{ICON_CertSetup,				MESG_TEXT_CertSetup},
};
const unsigned char ManagerSetNumMode0 = sizeof(ManagerSetMode0)/sizeof(ManagerSetMode0[0]);

const IconAndText_t ManagerSetMode1[] = 
{
	{ICON_052_PublicUnlockCode,		MESG_TEXT_ICON_PublicUnlockCode},
	{ICON_053_CardManagement,		MESG_TEXT_ICON_CardManagement},
	//{ICON_BACKUP_RESTORE,			MESG_TEXT_ICON_BACKUP_RESTORE},
	{ICON_100_SipEnable,				MESG_TEXT_ICON_100_SipEnable},
	{ICON_EditName_Enable,			MESG_TEXT_EditName_Enable},
	{ICON_281_Name, 					MESG_TEXT_ICON_281_Name},
};
const unsigned char ManagerSetNumMode1 = sizeof(ManagerSetMode1)/sizeof(ManagerSetMode1[0]);

const IconAndText_t ManagerSetMode2[] = 
{
	{ICON_027_SipConfig,				MESG_TEXT_SipConfig},
	{ICON_051_PrivateUnlockCode,		MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_052_PublicUnlockCode,		MESG_TEXT_ICON_PublicUnlockCode},
	{ICON_053_CardManagement,		MESG_TEXT_ICON_CardManagement},
	{ICON_BACKUP_RESTORE,			MESG_TEXT_ICON_BACKUP_RESTORE},
	{ICON_BabyroomEn,			MESG_TEXT_ICON_BabyroomEn},
};
const unsigned char ManagerSetNumMode2 = sizeof(ManagerSetMode2)/sizeof(ManagerSetMode2[0]);

const IconAndText_t ManagerSetMode3[] = 
{
	{ICON_027_SipConfig,				MESG_TEXT_SipConfig},
	{ICON_051_PrivateUnlockCode,		MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_052_PublicUnlockCode,		MESG_TEXT_ICON_PublicUnlockCode},
	{ICON_053_CardManagement,		MESG_TEXT_ICON_CardManagement},
	{ICON_BACKUP_RESTORE,			MESG_TEXT_ICON_BACKUP_RESTORE},
	{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
};
const unsigned char ManagerSetNumMode3 = sizeof(ManagerSetMode3)/sizeof(ManagerSetMode3[0]);

IconAndText_t *ManagerSet = NULL;
int ManagerSetNum;

// 20190521
void private_code_display(int x, int y)
{
	char display[8];
	API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, display);
	if( strcmp(display, "0") == 0 )
	{
		strcpy( display, "-" );
	}
	else
	{
		if( private_code_mask == 0 )
		{
			strcpy( display, "****" );
		}
//		else
//		{
//			strcat( display, "]" );
//		}
	}
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
	
}
int confirm_set_private_code(const char* input)
{
	int len,i;
	len = strlen(input);
	if( len == 0 || len == 4 )
	{		
		if( len == 0 )
		{
			strcpy(input,"0");
		}
		else
		{
			for( i = 0; i < 4; i++ )
			{
				if( !isdigit(input[i]) )
				{
					return 0;
				}
			}
		}
		//sprintf(add_one_id_card.name,"%s",input);
		API_Event_IoServer_InnerWrite_All(PRIVATE_UNLOCK_CODE, input);
		return 1;
	}
	else
	{
		return 0;
	}
}

void EditNameEnSet(int value)
{
	char temp[20];
	
	sprintf(temp, "%d", value ? 0 : 1);
	API_Event_IoServer_InnerWrite_All(ManagerEditUserNameDis, temp);
	
	//popDisplayLastMenu();
}
void BabyroomEnSet(int value)
{
	char temp[20];
	
	sprintf(temp, "%d", value);
	API_Event_IoServer_InnerWrite_All(BabyRoomEn, temp);
	
	//popDisplayLastMenu();
}


static void DisplayManagerPageIcon(int page)	//czn_20190221
{
	int i;
	uint16 x, y, val_x;
	int pageNum;
	char display[50];
	POS pos;
	SIZE hv;
	int iConText;
	int iCon;
	int stringId;
	
	API_DisableOsdUpdate();
	for(i = 0; i < ManagerIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*ManagerIconMax+i < ManagerSetNum)
		{
			iConText = ManagerSet[i+page*ManagerIconMax].iConText;
			iCon = ManagerSet[i+page*ManagerIconMax].iCon;
			if(iConText != NULL)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, iConText, 1, val_x - x);
			}
			else
			{
				if(iCon == ICON_wireless)
				{
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ParaGroup1,1, WLAN, NULL, val_x - x);
				}
			}
			
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(iCon)
			{
				case ICON_051_PrivateUnlockCode:
					private_code_display(x,y);
					break;
				case ICON_100_SipEnable:
					API_Event_IoServer_InnerRead_All(SIP_ENABLE, display);
					stringId = (atoi(display) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", MENU_SCHEDULE_POS_X - x);
					break;
				case ICON_EditName_Enable:
					API_Event_IoServer_InnerRead_All(ManagerEditUserNameDis, display);
					stringId = (atoi(display)==0? MESG_TEXT_EditNameEnable : MESG_TEXT_EditNameDisable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", MENU_SCHEDULE_POS_X - x);
					break;	
				case ICON_BabyroomEn:
					API_Event_IoServer_InnerRead_All(BabyRoomEn, display);
					stringId = (atoi(display)==1? MESG_TEXT_EditNameEnable : MESG_TEXT_EditNameDisable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", MENU_SCHEDULE_POS_X - x);
					break;	
				case ICON_045_IntercomEnable:
					API_Event_IoServer_InnerRead_All(IntercomEnable,display);
					stringId = (atoi(display)==1? MESG_TEXT_Enable: MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", MENU_SCHEDULE_POS_X - x);
					//API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, (atoi(display) ? MESG_TEXT_Enable: MESG_TEXT_Disable), 1, hv.h-x);
					break;
			}
		}
			
			
	}
	pageNum = ManagerSetNum/ManagerIconMax + (ManagerSetNum%ManagerIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	API_EnableOsdUpdate();
}
 
int MananerSettingVerifyPassword(const char* password)
 {
 	extern int playbackInfoResult;
 	extern void (*playbackInfoProcess)(int);
	char pwd[8+1];
	char tempChar[30];
	int len;
	char temp[20];
	API_Event_IoServer_InnerRead_All(InstallerPassword, (uint8*)pwd);

	if(!strcmp(password, pwd))
	{
		//EnterInstallerMode();
		API_Event_IoServer_InnerRead_All(ManagePwdPlace, (uint8*)tempChar);
		if(atoi(tempChar)==0&&GetLastNMenu()!=MENU_001_MAIN)
		{
			switch(ManagerSet[ManagerPageSelect*ManagerIconMax+ManagerIconSelect].iCon)
			{
				case ICON_wireless:
					StartInitOneMenu(MENU_114_WlanSetting,0,0);
					break;	
				
				case ICON_027_SipConfig:
					StartInitOneMenu(MENU_009_SIP_CONFIG,0,0);
					break;	
					
				case ICON_051_PrivateUnlockCode:
					API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, tempChar);
					//if( (strcmp(tempChar, "0") == 0) || private_code_mask >= 2 )
					{
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_PrivateUnlockCode, tempChar, 4, COLOR_WHITE, tempChar, 1, confirm_set_private_code);
						popLastMenu();
					}
					break;
				case ICON_052_PublicUnlockCode:
					StartInitOneMenu(MENU_088_PublicUnlockCode,0,0);
					break;
					
				// zfz_20190601
				case ICON_053_CardManagement:
					EnterIdCardMenu(MENU_100_CardList, 0);
					break;					

				case ICON_BACKUP_RESTORE:
					StartInitOneMenu(MENU_104_BackupAndRestore2,0,0);
					break;	
				case ICON_100_SipEnable:
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					API_Event_IoServer_InnerRead_All(SIP_ENABLE,temp);
					EnterPublicSettingMenu(0, MESG_TEXT_ICON_100_SipEnable, 2, atoi(temp), SipEnableSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_EditName_Enable:
					API_GetOSD_StringWithID(MESG_TEXT_EditNameDisable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_EditNameEnable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					API_Event_IoServer_InnerRead_All(ManagerEditUserNameDis,temp);
					EnterPublicSettingMenu(0, MESG_TEXT_EditName_Enable, 2, atoi(temp)==0?1:0, EditNameEnSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;	
				case ICON_281_Name:
					//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
					
					API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
					if(atoi(tempChar)==1)
						EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
					else
						EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
					
					popLastMenu();
					break;
				case ICON_BabyroomEn:
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					API_Event_IoServer_InnerRead_All(BabyRoomEn,temp);
					EnterPublicSettingMenu(0, MESG_TEXT_ICON_BabyroomEn, 2, atoi(temp), BabyroomEnSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_045_IntercomEnable:
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					API_Event_IoServer_InnerRead_All(IntercomEnable,tempChar);
					EnterPublicSettingMenu(0, MESG_TEXT_Icon_045_IntercomEnable, 2, atoi(tempChar), IntercomEnableSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_CertSetup:
					StartInitOneMenu(MENU_157_CertSetup,0,0);
					break;		
			}
		}
		else
		{
			switch(GetSavedIcon())
			{
				case ICON_033_Manager:
					EnterSettingMenu(MENU_107_Manager, 0);
					break;
			
				case ICON_222_MainSip:
					StartInitOneMenu(MENU_009_SIP_CONFIG,0,0);
					break;
					
				case ICON_421_PlaybackInfoConfirm:
					playbackInfoResult = 1;
					if(playbackInfoProcess != NULL)
					{
						playbackInfoProcess(playbackInfoResult);
					}
					 
					popDisplayLastNMenu(2);
					//artInitOneMenu(MENU_009_SIP_CONFIG,0,0);
					
					break;
					
			}
		}
		return -1;
	}
	else
	{
		return 0;
	}

 }

void MENU_107_Manager_Init(int uMenuCnt)
{
	char display[50];
	POS pos;
	SIZE hv;
	int menu_mode;
	API_Event_IoServer_InnerRead_All(ManagerMenuMode, (uint8*)display);
	menu_mode = atoi(display);
	if(menu_mode == 1)
	{
		ManagerSet = ManagerSetMode1;
		ManagerSetNum = ManagerSetNumMode1;
	}
	else if(menu_mode == 2)
	{
		ManagerSet = ManagerSetMode2;
		ManagerSetNum = ManagerSetNumMode2;
	}
	else if(menu_mode == 3)
	{
		ManagerSet = ManagerSetMode3;
		ManagerSetNum = ManagerSetNumMode3;
	}
	else
	{
		ManagerSet = ManagerSetMode0;
		ManagerSetNum = ManagerSetNumMode0;
	}
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		ManagerPageSelect = 0;
		private_code_mask = 0;
	}
	
	API_MenuIconDisplaySelectOn(ICON_033_Manager);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_033_Manager, 1, 0);
	ManagerIconMax = GetListIconNum();
	DisplayManagerPageIcon(ManagerPageSelect);
}

void MENU_107_Manager_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_033_Manager);	
}

void MENU_107_Manager_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int x, y;
	int updateCnt;
	char display[100];
	char tempChar[30];
	POS pos;
	SIZE hv;	
	int len;
	char temp[20];
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&ManagerPageSelect, ManagerIconMax, ManagerSetNum, (DispListPage)DisplayManagerPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ManagerPageSelect, ManagerIconMax, ManagerSetNum, (DispListPage)DisplayManagerPageIcon);
					break;	
				
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
					//czn_20190221_s
					ManagerIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(ManagerPageSelect*ManagerIconMax+ManagerIconSelect >= ManagerSetNum)
					{
						return;
					}
					API_Event_IoServer_InnerRead_All(ManagePwdEnable, tempChar);
					API_Event_IoServer_InnerRead_All(ManagePwdPlace, display);
					if(atoi(display)==1||atoi(tempChar)==0||JudgeIsInstallerMode() || API_AskDebugState(100))
					{
						switch(ManagerSet[ManagerPageSelect*ManagerIconMax+ManagerIconSelect].iCon)
						{
							case ICON_wireless:
								StartInitOneMenu(MENU_114_WlanSetting,0,1);
								break;	
								
							case ICON_027_SipConfig:
								StartInitOneMenu(MENU_009_SIP_CONFIG,0,1);
								break;	
								
							case ICON_051_PrivateUnlockCode:
								private_code_mask++;
								OSD_GetIconInfo(ICON_007_PublicList1+ManagerIconSelect, &pos, &hv);
								x = pos.x+DISPLAY_DEVIATION_X+(hv.h - pos.x)/2;
								y = pos.y+(hv.v - pos.y)/2;
								//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X+DISPLAY_VALUE_X_DEVIATION;
								//y = DISPLAY_LIST_Y+ManagerIconSelect*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
								private_code_display(x,y);
								API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, tempChar);
								if( (strcmp(tempChar, "0") == 0) || private_code_mask >= 2 )
								{
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_PrivateUnlockCode, tempChar, 4, COLOR_WHITE, tempChar, 1, confirm_set_private_code);
								}
								break;
							case ICON_052_PublicUnlockCode:
								StartInitOneMenu(MENU_088_PublicUnlockCode,0,1);
								break;
								
							// zfz_20190601
							case ICON_053_CardManagement:
								EnterIdCardMenu(MENU_100_CardList, 1);
								break;					

							case ICON_BACKUP_RESTORE:
								StartInitOneMenu(MENU_104_BackupAndRestore2,0,1);
								break;
							case ICON_100_SipEnable:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(SIP_ENABLE,temp);
								EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_ICON_100_SipEnable, 2, atoi(temp), SipEnableSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							case ICON_EditName_Enable:
								API_GetOSD_StringWithID(MESG_TEXT_EditNameDisable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_EditNameEnable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(ManagerEditUserNameDis,temp);
								EnterPublicSettingMenu(0, MESG_TEXT_EditName_Enable, 2, atoi(temp)==0?1:0, EditNameEnSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							case ICON_281_Name:
								//API_Event_IoServer_InnerRead_All(EditUserNameDis, (uint8*)tempChar);
								API_Event_IoServer_InnerRead_All(EditUserNameDis, (uint8*)tempChar);
								if(atoi(tempChar) == 0)
								{
									//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
									if(atoi(tempChar)==1)
										EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
									else
										EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
								}
								break;	
							case ICON_BabyroomEn:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(BabyRoomEn,temp);
								EnterPublicSettingMenu(0, MESG_TEXT_ICON_BabyroomEn, 2, atoi(temp), BabyroomEnSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							case ICON_045_IntercomEnable:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(IntercomEnable,display);
								EnterPublicSettingMenu(0, MESG_TEXT_Icon_045_IntercomEnable, 2, atoi(display), IntercomEnableSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							case ICON_CertSetup:
								StartInitOneMenu(MENU_157_CertSetup,0,1);
								break;	
						}
					}
					else
					{
						switch(ManagerSet[ManagerPageSelect*ManagerIconMax+ManagerIconSelect].iCon)
						{
							case ICON_wireless:
								StartInitOneMenu(MENU_114_WlanSetting,0,1);
								break;	

							case ICON_027_SipConfig:
								API_Event_IoServer_InnerRead_All(SipChangePwdEnable, tempChar);
								if(atoi(tempChar) == 1)
								{
									StartInitOneMenu(MENU_009_SIP_CONFIG,0,1);
									return;
								}
								break;	
							case ICON_051_PrivateUnlockCode:
								private_code_mask++;
								OSD_GetIconInfo(ICON_007_PublicList1+ManagerIconSelect, &pos, &hv);
								x = pos.x+DISPLAY_DEVIATION_X+(hv.h - pos.x)/2;
								y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
								//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X+DISPLAY_VALUE_X_DEVIATION;
								//y = DISPLAY_LIST_Y+ManagerIconSelect*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
								private_code_display(x,y);
								API_Event_IoServer_InnerRead_All(PRIVATE_UNLOCK_CODE, tempChar);	
								if( (strcmp(tempChar, "0") != 0) && private_code_mask < 2 )
								{
									return;
								}
								break;
							case ICON_281_Name:
								//API_Event_IoServer_InnerRead_All(EditUserNameDis, (uint8*)tempChar);
								API_Event_IoServer_InnerRead_All(EditUserNameDis, (uint8*)tempChar);
								if(atoi(tempChar) == 1)
								{
									return;
								}
								break;	
								
						}
						extern char installerInput[9];
							
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, MananerSettingVerifyPassword);
					}
											
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}

