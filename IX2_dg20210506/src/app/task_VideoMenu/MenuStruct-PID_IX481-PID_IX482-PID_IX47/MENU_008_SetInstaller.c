#include "MENU_public.h"
#include "MENU_008_SetInstaller.h"
#include "obj_R8001_Table.h"

//#define	setInstallerIconMax	5
int setInstallerIconMax;
int setInstallerIconSelect;
int setInstallerPageSelect;
char installerInput[9];
#if 0
static int EnterInstaller_Flag = 0;
static int EnterInstaller_time = 0;
#endif
void EnterInstallerMode(void);
void DoorBellVideoSet(int sel);
extern void IntercomEnableSet(int value);


const SetInstallerIcon setInstallerIconTableMode0[] = 
{
	{ICON_047_IpAddress,			MESG_TEXT_ICON_047_IpAddress},    
	{ICON_048_CallNumber,			MESG_TEXT_ICON_048_CallNumber},
	{ICON_044_Parameter,			MESG_TEXT_ICON_044_Parameter},
	{ICON_FwUpgrade,				MESG_TEXT_ICON_FwUpgrage},		//czn_20181219
	{ICON_049_OnsiteTools,			MESG_TEXT_ICON_049_OnsiteTools},
	//{ICON_AutotestTools,			MESG_TEXT_ICON_AutotestTools},		//czn_20190412
	//{ICON_SummaryReport,			MESG_TEXT_ICON_Summary},		//dh_20190603
	//{ICON_InstallerChecklist,		MESG_TEXT_ICON_Checklist},		//dh_20190603
	//{ICON_RES_Manger,				MESG_TEXT_ICON_RES_Manger}, 	//dh_20190603
	//{ICON_BATCH_CALL_NBR,				MESG_TEXT_ICON_BATCH_CALL_NBR},    
	//{ICON_AllDevGotoUpdate,				MESG_TEXT_ICON_BATCH_UPDATE},    
	//{ICON_BATCH_REBOOT,				MESG_TEXT_ICON_BATCH_REBOOT},		
	//{ICON_BATCH_PWD,					MESG_TEXT_ICON_BATCH_PWD},		
};

const int setInstallerIconNumMode0 = sizeof(setInstallerIconTableMode0)/sizeof(setInstallerIconTableMode0[0]);

const SetInstallerIcon setInstallerIconTableMode1[] = 
{
	{ICON_Overview,				MESG_TEXT_ICON_Overview},
	{ICON_047_IpAddress,			MESG_TEXT_ICON_047_IpAddress},    
	{ICON_048_CallNumber,			MESG_TEXT_ICON_048_CallNumber},
	{ICON_044_Parameter,			MESG_TEXT_ICON_044_Parameter},
	{ICON_FwUpgrade,				MESG_TEXT_ICON_FwUpgrage},		//czn_20181219
	{ICON_049_OnsiteTools,			MESG_TEXT_ICON_049_OnsiteTools},
	{ICON_MonitorDoorStation,		MESG_TEXT_ICON_MonitorDoorStation},
	{ICON_BACKUP_RESTORE,		MESG_TEXT_ICON_BACKUP_RESTORE},
	{ICON_DoorbellVideo,			MESG_TEXT_DoorbellVideo},
	//{ICON_AutotestTools,			MESG_TEXT_ICON_AutotestTools},		//czn_20190412
	//{ICON_SummaryReport,			MESG_TEXT_ICON_Summary},		//dh_20190603
	//{ICON_InstallerChecklist,		MESG_TEXT_ICON_Checklist},		//dh_20190603
	//{ICON_RES_Manger,				MESG_TEXT_ICON_RES_Manger}, 	//dh_20190603
	//{ICON_BATCH_CALL_NBR,				MESG_TEXT_ICON_BATCH_CALL_NBR},    
	//{ICON_AllDevGotoUpdate,				MESG_TEXT_ICON_BATCH_UPDATE},    
	//{ICON_BATCH_REBOOT,				MESG_TEXT_ICON_BATCH_REBOOT},		
	//{ICON_BATCH_PWD,					MESG_TEXT_ICON_BATCH_PWD},		
};

const int setInstallerIconNumMode1= sizeof(setInstallerIconTableMode1)/sizeof(setInstallerIconTableMode1[0]);

const SetInstallerIcon setInstallerIconTableMode2[] = 
{
	{ICON_047_IpAddress,			MESG_TEXT_ICON_047_IpAddress},    
	{ICON_048_CallNumber,			MESG_TEXT_ICON_048_CallNumber},
	{ICON_044_Parameter,			MESG_TEXT_ICON_044_Parameter},
	{ICON_FwUpgrade,				MESG_TEXT_ICON_FwUpgrage},		//czn_20181219
	{ICON_049_OnsiteTools,			MESG_TEXT_ICON_049_OnsiteTools},
	{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
	{ICON_MonitorDoorStation,		MESG_TEXT_ICON_MonitorDoorStation},
	//{ICON_AutotestTools,			MESG_TEXT_ICON_AutotestTools},		//czn_20190412
	//{ICON_SummaryReport,			MESG_TEXT_ICON_Summary},		//dh_20190603
	//{ICON_InstallerChecklist,		MESG_TEXT_ICON_Checklist},		//dh_20190603
	//{ICON_RES_Manger,				MESG_TEXT_ICON_RES_Manger}, 	//dh_20190603
	//{ICON_BATCH_CALL_NBR,				MESG_TEXT_ICON_BATCH_CALL_NBR},    
	//{ICON_AllDevGotoUpdate,				MESG_TEXT_ICON_BATCH_UPDATE},    
	//{ICON_BATCH_REBOOT,				MESG_TEXT_ICON_BATCH_REBOOT},		
	//{ICON_BATCH_PWD,					MESG_TEXT_ICON_BATCH_PWD},		
};

const int setInstallerIconNumMode2= sizeof(setInstallerIconTableMode2)/sizeof(setInstallerIconTableMode2[0]);

const SetInstallerIcon setInstallerIconTableMode3[] = 
{
	{ICON_047_IpAddress,			MESG_TEXT_ICON_047_IpAddress},    
	{ICON_048_CallNumber,			MESG_TEXT_ICON_048_CallNumber},
	{ICON_044_Parameter,			MESG_TEXT_ICON_044_Parameter},
	{ICON_FwUpgrade,				MESG_TEXT_ICON_FwUpgrage},		//czn_20181219
	{ICON_049_OnsiteTools,			MESG_TEXT_ICON_049_OnsiteTools},
	{ICON_045_IntercomEnable, 	MESG_TEXT_Icon_045_IntercomEnable},
	//{ICON_BACKUP_RESTORE,			MESG_TEXT_ICON_BACKUP_RESTORE2},
	//{ICON_AutotestTools,			MESG_TEXT_ICON_AutotestTools},		//czn_20190412
	//{ICON_SummaryReport,			MESG_TEXT_ICON_Summary},		//dh_20190603
	//{ICON_InstallerChecklist,		MESG_TEXT_ICON_Checklist},		//dh_20190603
	//{ICON_RES_Manger,				MESG_TEXT_ICON_RES_Manger}, 	//dh_20190603
	//{ICON_BATCH_CALL_NBR,				MESG_TEXT_ICON_BATCH_CALL_NBR},    
	//{ICON_AllDevGotoUpdate,				MESG_TEXT_ICON_BATCH_UPDATE},    
	//{ICON_BATCH_REBOOT,				MESG_TEXT_ICON_BATCH_REBOOT},		
	//{ICON_BATCH_PWD,					MESG_TEXT_ICON_BATCH_PWD},		
};

const int setInstallerIconNumMode3 = sizeof(setInstallerIconTableMode3)/sizeof(setInstallerIconTableMode3[0]);

SetInstallerIcon *setInstallerIconTable=NULL;
int setInstallerIconNum = 0;

void MonitorDsDisSet(int enable)
{
	char temp[5];
	sprintf(temp,"%d",enable);
	API_Event_IoServer_InnerWrite_All(MonitorDsDis, temp);
}

static void DisplaySetInstallerPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[100];
	int type;
	POS pos;
	SIZE hv;
	
	// lzh_20181016_s	
	API_DisableOsdUpdate();
	// lzh_20181016_e
	
	for(i = 0; i < setInstallerIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*setInstallerIconMax+i < setInstallerIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setInstallerIconTable[i+page*setInstallerIconMax].iConText, 1, hv.h-x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(setInstallerIconTable[i+page*setInstallerIconMax].iCon)
			{
				case ICON_047_IpAddress:
					#if 0
					switch(GetIpActionType())
					{
						case 1:
							sprintf(display, "%s%s", GetSysVerInfo_IP(), "-D");
							break;
							
						case 2:
							sprintf(display, "%s%s", GetSysVerInfo_IP(), "-A");
							break;
							
						case 3:
							sprintf(display, "%s%s", GetSysVerInfo_IP(), "-S");
							break;
							
						default:
							sprintf(display, "%s%s", GetSysVerInfo_IP(), "-U");
							break;
					}
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					#endif
					break;
				
				case ICON_048_CallNumber:
					#if 0
					if(memcmp(GetSysVerInfo_BdRmMs(), "00990001", 8))
					{
						if(!strcmp(GetSysVerInfo_bd(), "0099"))
						{
							sprintf(display, "(SS)%d", atoi(GetSysVerInfo_rm()));
						}
						else
						{
							sprintf(display, "(NS)%s %s", GetSysVerInfo_bd(), GetSysVerInfo_rm());
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					}
					else
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_ICON_SelectSystemTypeVS, 1, MENU_SCHEDULE_POS_X-x);
					}
					#endif
					type = GetSysVerInfo_SystemType();
					if(type)
					{
						char systype_buff[100];
						int unicode_len;
						if(type == 1)
						{
							API_GetOSD_StringWithID(MESG_TEXT_ICON_SelectSystemTypeSS, NULL, 0, NULL, 0,systype_buff, &unicode_len);
							if(GetSystemTypeAb(systype_buff, unicode_len, display)==0)
							{
								sprintf(display, "%s%d", display,atoi(GetSysVerInfo_rm()));
							}
							else
							{
								sprintf(display, "(SS)%d", atoi(GetSysVerInfo_rm()));
							}
						}
						else
						{
							API_GetOSD_StringWithID(MESG_TEXT_ICON_SelectSystemTypeNS, NULL, 0, NULL, 0,systype_buff, &unicode_len);
							if(GetSystemTypeAb(systype_buff, unicode_len, display)==0)
							{
								sprintf(display, "%s%s %s",display,GetSysVerInfo_bd(), GetSysVerInfo_rm());
							}
							else
							{
								sprintf(display, "(NS)%s %s", GetSysVerInfo_bd(), GetSysVerInfo_rm());
							}
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					}
					else
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_ICON_SelectSystemTypeVS, 1, hv.h-x);
					}
					break;
				case ICON_MonitorDoorStation:
					API_Event_IoServer_InnerRead_All(MonitorDsDis, display);
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, (atoi(display) ? MESG_TEXT_Disable: MESG_TEXT_Enable), 1, hv.h-x);
					break;
				case ICON_045_IntercomEnable:
					API_Event_IoServer_InnerRead_All(IntercomEnable,display);
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, (atoi(display) ? MESG_TEXT_Enable: MESG_TEXT_Disable), 1, hv.h-x);
					break;	
					
			}
		}
	}
	pageNum = setInstallerIconNum/setInstallerIconMax + (setInstallerIconNum%setInstallerIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	// lzh_20181016_s	
	API_EnableOsdUpdate();
	// lzh_20181016_e	
}


int VerifyPassword(const char* password)
{
	char* pwd[8+1];
	char tempChar[5];
	int len;
	int x,y;

	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)pwd);

	if(!strcmp(password, pwd))
	{
		
		API_Event_IoServer_InnerRead_All(InstallerPwdAlways, (uint8*)tempChar);
		
		if(atoi(tempChar) == 0)
			EnterInstallerMode();
		
		API_Event_IoServer_InnerRead_All(InstallerPwdPlace, (uint8*)tempChar);
		if(atoi(tempChar) == 0)
		{
			switch(setInstallerIconTable[setInstallerPageSelect*setInstallerIconMax+setInstallerIconSelect].iCon)
			{
				case ICON_047_IpAddress:
					if(JudgeIfWlanDevice())
						EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
					else
						EnterInstallSubMenu(MENU_126_NM_LanSetting, 0);
					//EnterInstallSubMenu(MENU_016_INSTALL_IP, 0);
					break;
			
				case ICON_048_CallNumber:
					EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 0);
					break;
					
				case ICON_049_OnsiteTools:
					API_Event_IoServer_InnerRead_All(InstallerMenuMode, (uint8*)tempChar);
					if(atoi(tempChar)==0)
						StartInitOneMenu(MENU_108_QuickAccess,0,0);
					else
					{
						#if 0
						API_Event_IoServer_InnerRead_All(OnsiteToolsDisable, (uint8*)tempChar);
						if(atoi(tempChar) == 1)
						{
							return;
						}
						#endif
						SetSearchMaxnum(0);
						EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
					}
					break;
					
				case ICON_044_Parameter:
					//SetParaGroupNumber(1);
					EnterInstallSubMenu(MENU_077_ParaGroup, 0);
					break;
									
				case ICON_FwUpgrade:
					EnterInstallSubMenu(MENU_066_FwUpgrade,0);	//czn_20181219
					break;
				case ICON_BACKUP_RESTORE:
					StartInitOneMenu(MENU_104_BackupAndRestore2,0,0);
					break;
				case ICON_Overview:
					StartInitOneMenu(MENU_108_QuickAccess,0,0);
					break;	
				case ICON_MonitorDoorStation:
					API_Event_IoServer_InnerRead_All(MonitorDsDis, (uint8*)tempChar);
					
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
				
					EnterPublicSettingMenu(ICON_026_InstallerSetup, MESG_TEXT_ICON_MonitorDoorStation, 2, atoi(tempChar), MonitorDsDisSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_DoorbellVideo:
					LoadDoorBellVideoPublicSetting(&x,&y);
					EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_DoorbellVideo, y, x, DoorBellVideoSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;
				case ICON_045_IntercomEnable:
					API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
					publicSettingDisplay[0][0] = len;
					API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
					publicSettingDisplay[1][0] = len;
					API_Event_IoServer_InnerRead_All(IntercomEnable,tempChar);
					EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_Icon_045_IntercomEnable, 2, atoi(tempChar), IntercomEnableSet);
					StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
					break;	
				#if 0	
				case ICON_AutotestTools:	//czn_20190412
				case ICON_SummaryReport:	
				case ICON_InstallerChecklist:
				case ICON_RES_Manger	:
				case ICON_BATCH_CALL_NBR:
					return 1;
				#endif	
			}
			
		}
		else
		{
			EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
		}
		return -1;
	}
	else
	{
		return 0;
	}
}


void MENU_008_SetInstaller_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	int menumode;
	char temp[5];
	API_Event_IoServer_InnerRead_All(InstallerMenuMode, (uint8*)temp);
	menumode = atoi(temp);
	if(menumode == 1)
	{
		setInstallerIconTable = setInstallerIconTableMode1;
		setInstallerIconNum = setInstallerIconNumMode1;
	}
	else if(menumode == 2)
	{
		setInstallerIconTable = setInstallerIconTableMode2;
		setInstallerIconNum = setInstallerIconNumMode2;
	}
	else if(menumode == 3)
	{
		setInstallerIconTable = setInstallerIconTableMode3;
		setInstallerIconNum = setInstallerIconNumMode3;
	}
	else
	{
		setInstallerIconTable = setInstallerIconTableMode0;
		setInstallerIconNum = setInstallerIconNumMode0;
	}
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		setInstallerPageSelect = 0;
	}
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_026_InstallerSetup, 1, 0);

	setInstallerIconMax = GetListIconNum();
	DisplaySetInstallerPageIcon(setInstallerPageSelect);
}

void MENU_008_SetInstaller_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_026_InstallerSetup);
}

void MENU_008_SetInstaller_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[10];
	char pwdplace[5];
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&setInstallerPageSelect, setInstallerIconMax, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&setInstallerPageSelect, setInstallerIconMax, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					setInstallerIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(setInstallerPageSelect*setInstallerIconMax+setInstallerIconSelect >= setInstallerIconNum)
					{
						return;
					}
					API_Event_IoServer_InnerRead_All(InstallerPwdPlace, pwdplace);
					if(atoi(pwdplace)==1||JudgeIsInstallerMode() || API_AskDebugState(100))
					{
						switch(setInstallerIconTable[setInstallerPageSelect*setInstallerIconMax+setInstallerIconSelect].iCon)
						{
							case ICON_047_IpAddress:
								if(JudgeIfWlanDevice())
									EnterInstallSubMenu(MENU_125_NM_MAIN, 0);
								else
									EnterInstallSubMenu(MENU_126_NM_LanSetting, 0);
								//EnterInstallSubMenu(MENU_016_INSTALL_IP, 1);
								break;

							case ICON_048_CallNumber:
								EnterInstallSubMenu(MENU_020_INSTALL_CALL_NUM, 1);
								break;
								
							case ICON_049_OnsiteTools:
								#if 0
								SetSearchBdNumber(GetSysVerInfo_bd());
								SetSearchDeviceType(0);
								SetSearchMaxnum(0);
								EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 0);
								#endif
								//StartInitOneMenu(MENU_108_QuickAccess,0,0);
								API_Event_IoServer_InnerRead_All(InstallerMenuMode, (uint8*)display);
								if(atoi(display)==0)
									StartInitOneMenu(MENU_108_QuickAccess,0,1);
								else
								{
									#if 0
									API_Event_IoServer_InnerRead_All(OnsiteToolsDisable, (uint8*)display);
									if(atoi(display) == 1)
									{
										return;
									}
									#endif
									EnterInstallSubMenu(MENU_021_ONSITE_TOOLS, 1);
								}
								break;
								
							case ICON_044_Parameter:
								//SetParaGroupNumber(1);
								EnterInstallSubMenu(MENU_077_ParaGroup, 1);
								break;
								
							case ICON_FwUpgrade:
								EnterInstallSubMenu(MENU_066_FwUpgrade,1);	//czn_20181219
								break;
							case ICON_BACKUP_RESTORE:
								StartInitOneMenu(MENU_104_BackupAndRestore2,0,1);
								break;
							case ICON_Overview:
								StartInitOneMenu(MENU_108_QuickAccess,0,1);
								break;
							case ICON_MonitorDoorStation:
								API_Event_IoServer_InnerRead_All(MonitorDsDis, (uint8*)display);
								
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
							
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_MonitorDoorStation, 2, atoi(display), MonitorDsDisSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;	
							case ICON_DoorbellVideo:
								LoadDoorBellVideoPublicSetting(&x,&y);
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_DoorbellVideo, y, x, DoorBellVideoSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							case ICON_045_IntercomEnable:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(IntercomEnable,display);
								EnterPublicSettingMenu(MESG_TEXT_ICON_025_general, MESG_TEXT_Icon_045_IntercomEnable, 2, atoi(display), IntercomEnableSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							
						}
					}
					else
					{
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 8, COLOR_WHITE, NULL, 0, VerifyPassword);
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		setInstallerPageSelect = 0;
		DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}
#if 0
void timer_EixtInstaller_callback(void)
{
	if(!EnterInstaller_Flag)
	{
		return;
	}
	
	if(EnterInstaller_time != 0)
	{
		if(--EnterInstaller_time == 0)
		{
			ExitInstallerMode();
		}
	}
}

void EnterInstallerMode(void)
{
	EnterInstaller_time = 60;
	EnterInstaller_Flag = 1;
}

void ExitInstallerMode(void)
{
	EnterInstaller_Flag  = 0;
	EnterInstaller_time = 0;
}

int JudgeIsInstallerMode(void)
{
	return EnterInstaller_Flag;
}
#endif



