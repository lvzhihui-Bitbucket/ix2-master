#include "MENU_public.h"
#include "cJSON.h"

int Parameter2IconMax;
int Parameter2IconSelect;
int Parameter2PageSelect;
int Parameter2Index;
cJSON *paraList=NULL;
int paraListCnt;
char valueinput[11];
char paraKeyName[100];

const char *parastr[5] =
{
	"ParameterGroup1",
	"ParameterGroup2",
	"ParameterGroup3",
	"ParameterGroup4",
	"ParameterGroup5"
};

void DisplayOnePageParameter2list(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;
	POS pos;
	SIZE hv;
	char paraid[100];
	char value[20]={0};
	cJSON * para = API_ReadRemoteIo(GetSearchOnlineIp(), paraList);
	cJSON* item = cJSON_GetObjectItemCaseSensitive(para, "READ");

	#if 0
	char *str=cJSON_Print(para);
	printf("API_ReadRemoteIo =%s \n",str);
	free(str);
	#endif

	for( i = 0; i < Parameter2IconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		list_start = page*Parameter2IconMax+i;
		if(list_start < paraListCnt)
		{
			snprintf(paraid, 100, "%s", cJSON_GetArrayItem(paraList, list_start)->valuestring);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, paraid, strlen(paraid), 1, STR_UTF8,  val_x - x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			cJSON* valueJson = cJSON_GetObjectItemCaseSensitive(item, paraid);
			if(cJSON_IsNumber(valueJson))
			{
				snprintf(value, 20, "%d", valueJson->valueint);
			}
			else if(cJSON_IsString(valueJson))
			{
				snprintf(value, 20, "%s", valueJson->valuestring);
			}
			API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, value, strlen(value), 1, STR_UTF8, hv.h-x);
		}
		
	}
	
	
	maxPage = paraListCnt/Parameter2IconMax + (paraListCnt%Parameter2IconMax ? 1 : 0);
		
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void MENU_148_OnlineParameter2_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	Parameter2IconMax = GetListIconNum();
	Parameter2IconSelect = 0;
	Parameter2PageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	
	cJSON * ret = API_ReadRemoteIo(GetSearchOnlineIp(), cJSON_CreateStringArray(parastr, 5));
	paraList = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(ret, "READ"), "ParameterGroup1");
	paraListCnt = cJSON_GetArraySize(paraList);
	#if 0
	char *str=cJSON_Print(paraList);
	printf("API_ReadRemoteIo =%s cnt=%d\n",str, paraListCnt);
	free(str);
	#endif
	DisplayOnePageParameter2list(0);
}


void MENU_148_OnlineParameter2_Exit(void)
{

}

int confirmParaValue(const char* input)
{
	int i;
	if(!strcmp(paraKeyName, "RL1_MODE"))
	{
		if(atoi(input) > 1)
		{
			return 0;
		}
	}
	else if(!strcmp(paraKeyName, "RL1_TIMER"))
	{
		if(atoi(input) < 1 || atoi(input) > 99)
		{
			return 0;
		}
	}
	else if(!strcmp(paraKeyName, "VOICE_VOLUME_IOID"))
	{
		if(atoi(input) < 0 || atoi(input) > 9)
		{
			return 0;
		}
	}
	else if(!strcmp(paraKeyName, "PRJ_PWD"))
	{
		for( i = 0; i < strlen(input); i++ )
		{
			if( !isdigit(input[i]) )
			{
				return 0;
			}
		}
	}
	cJSON* newitem = cJSON_CreateObject();
	if(!strcmp(paraKeyName, "RL1_MODE"))
	{
		cJSON_AddStringToObject(newitem, paraKeyName, atoi(input)==0? "0-NO" : "1-NC");
	}
	else if(!strcmp(paraKeyName, "RL1_TIMER"))
	{
		cJSON_AddNumberToObject(newitem, paraKeyName, atoi(input));
	}
	else
	{
		cJSON_AddStringToObject(newitem, paraKeyName, input);
	}
	API_WriteRemoteIo(GetSearchOnlineIp(), newitem);
	cJSON_Delete(newitem);
	return 1;
}
void MENU_148_OnlineParameter2_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					Parameter2IconSelect = GetCurIcon() - ICON_007_PublicList1;
					Parameter2Index = Parameter2PageSelect*Parameter2IconMax + Parameter2IconSelect;
					if(Parameter2Index >= paraListCnt)
						return;
					snprintf(paraKeyName, 100, "%s", cJSON_GetArrayItem(paraList, Parameter2Index)->valuestring);	
					EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ParaValue, valueinput, 10, COLOR_WHITE, NULL, 0, confirmParaValue);
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&Parameter2PageSelect, Parameter2IconMax, paraListCnt, (DispListPage)DisplayOnePageParameter2list);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&Parameter2PageSelect, Parameter2IconMax, paraListCnt, (DispListPage)DisplayOnePageParameter2list);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


