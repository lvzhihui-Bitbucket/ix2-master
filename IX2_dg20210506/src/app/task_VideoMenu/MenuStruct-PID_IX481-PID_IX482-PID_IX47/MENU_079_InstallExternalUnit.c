#include "MENU_079_InstallExternalUnit.h"
#include "obj_OS_ListTable.h"

//#define	externalUnitIconMax	5
int externalUnitIconMax;
int externalUnitIconSelect;
int externalUnitPageSelect;
char externalUnitInput[21];
int externalUnitConfirm;
int externalUnitConfirmSelect;

const IconAndText_t externalUnitSet[] = 
{
	{ICON_AutoSetupWizard,				MESG_TEXT_ICON_AutoSetupWizard},
	{ICON_IM_Extensions,				MESG_TEXT_ICON_IM_Extensions},
	{ICON_OutdoorStations,				MESG_TEXT_ICON_OutdoorStations},
	{ICON_IP_Camera,					MESG_TEXT_ICON_IP_Camera},
	{ICON_EnterCallBtnBinding,			MESG_TEXT_ICON_EnterCallBtnBinding},
	{ICON_Wlan_IP_Camera,				NULL},
	{ICON_PwdManage,					MESG_TEXT_PwdManager},
};


const unsigned char externalUnitSetNum = sizeof(externalUnitSet)/sizeof(externalUnitSet[0]);

static void DisplayExternalUnitPageIcon(int page)	//czn_20190221
{
	int i;
	uint16 x, y, val_x;
	int pageNum;
	int color;
	char display[50];
	POS pos;
	SIZE hv;
	int iCon;
	int iConText;
	
	
	API_DisableOsdUpdate();
	for(i = 0; i < externalUnitIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		
		iCon = externalUnitSet[i+page*externalUnitIconMax].iCon;
		iConText = externalUnitSet[i+page*externalUnitIconMax].iConText;
		
		if(page*externalUnitIconMax+i < externalUnitSetNum)
		{
			if(iCon != ICON_IP_Camera && iCon != ICON_Wlan_IP_Camera)
			{
				if(memcmp(GetSysVerInfo_ms(),"01",2)==0)
				{
					color = DISPLAY_LIST_COLOR;
				}
				else
				{
					color = COLOR_BLUE;
				}
			}
			else
			{
				color = DISPLAY_LIST_COLOR;
			}
			if(iConText != NULL)
			{
				API_OsdUnicodeStringDisplay(x, y, color, iConText, 1, val_x - x);
			}
			else
			{
				switch(iCon)
				{
					case ICON_Wlan_IP_Camera:
						API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, color, MESG_TEXT_ICON_IP_Camera,1, WLAN, NULL, val_x - x);
						break;
				}
			}
			
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(iCon)
			{
				case ICON_IM_Extensions:
					sprintf(display, "%d", GetMSListTempRecordCnt());
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
					break;
				case ICON_OutdoorStations:
					sprintf(display, "%d", GetOS_ListTempTableNum());
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
					break;
				case ICON_IP_Camera:
					//sprintf(display, "%d", Get_IPC_MonRes_Num());
					sprintf(display, "%d", GetIpcNum());
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
					break;
				case ICON_Wlan_IP_Camera:
					sprintf(display, "%d", GetWlanIpcNum());
					API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
					break;
			}
		}
	}
	pageNum = externalUnitSetNum/externalUnitIconMax + (externalUnitSetNum%externalUnitIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	API_EnableOsdUpdate();
}
 
  int ExtUnitVerifyPassword(const char* password)
 {
	char* pwd[8+1];
	POS pos;
	SIZE hv;
	

	API_Event_IoServer_InnerRead_All(InstallerPassword, (uint8*)pwd);

	if(!strcmp(password, pwd))
	{
		//EnterInstallerMode();
		API_Event_IoServer_InnerRead_All(ExtUnitPwdPlace, (uint8*)pwd);
		if(atoi(pwd) == 0)
		{
			switch(externalUnitSet[externalUnitPageSelect*externalUnitIconMax+externalUnitIconSelect].iCon)
			{
			case ICON_AutoSetupWizard:
				if(strcmp(GetSysVerInfo_BdRmMs(),"0099000101")==0)		
				{
					StartInitOneMenu(MENU_082_AutoSetupWizard,0,0);
				}
				break;
											
			case ICON_IM_Extensions:
				//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
				if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
				{
					StartInitOneMenu(MENU_080_IM_Extensions,0,0);
				}
				break;
				
			case ICON_OutdoorStations:
				//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
				if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
				{
					SaveIcon(ICON_OutdoorStations);
					StartInitOneMenu(MENU_081_OutdoorStations,0,0);
				}
				break;
			case ICON_PwdManage:
				if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
				{
					SaveIcon(ICON_PwdManage);
					StartInitOneMenu(MENU_081_OutdoorStations,0,0);
				}
				break;	
			case ICON_IP_Camera:
				//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
				//ListEditSelect(3);
				//StartInitOneMenu(MENU_036_LIST_EDIT,0,0);
				EnterIpcSetMenu(0);
				break;
				
			case ICON_Wlan_IP_Camera:
				//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
				//ListEditSelect(4);
				//StartInitOneMenu(MENU_036_LIST_EDIT,0,0);
				EnterIpcSetMenu(1);
				break;
				
			case ICON_EnterCallBtnBinding:
				//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
				popDisplayLastMenu();
				if(!GetCallBtnBingdingState())
				{
					if(API_GetCallBtnBindingState(1) == 0)
					{
						SetCallBtnBingdingState(1);
					}
				}
				OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
				API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
				if(GetCallBtnBingdingState())
				{
					AddOS_ListTempTableBySearching();
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_OS_BINDING, 1, 0);
					//sprintf(display, "In binding state, please press call button 10s");
					//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-DISPLAY_TITLE_X);
				}
				break;
			}
		}
		else
		{
			EnterSettingMenu(MENU_079_ExternalUnit, 0);
		}
		return -1;
	}
	else
	{
		return 0;
	}

 }


void MENU_079_ExternalUnit_Init(int uMenuCnt)
{
	char display[50];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	API_MenuIconDisplaySelectOn(ICON_051_ExternalUnit);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_051_ExternalUnit, 1, 0);
	externalUnitIconMax = GetListIconNum();
	externalUnitIconSelect = 0;
	externalUnitPageSelect = 0;
	externalUnitConfirm = 0;
	externalUnitConfirmSelect = 0;
	DisplayExternalUnitPageIcon(externalUnitPageSelect);
	if(GetLastNMenu() == MENU_001_MAIN)
	{
		BusySpriteDisplay(1);
		AddTempMSlist_TableBySearching();
		AddOS_ListTempTableBySearching();	
		BusySpriteDisplay(0);
		DisplayExternalUnitPageIcon(externalUnitPageSelect);
	}
}

void MENU_079_ExternalUnit_Exit(void)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	API_MenuIconDisplaySelectOff(ICON_051_ExternalUnit);
	SetCallBtnBingdingState(0);
	API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
}

void MENU_079_ExternalUnit_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int x, y;
	int updateCnt;
	char display[100] = {0};
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&externalUnitPageSelect, externalUnitIconMax, externalUnitSetNum, (DispListPage)DisplayExternalUnitPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&externalUnitPageSelect, externalUnitIconMax, externalUnitSetNum, (DispListPage)DisplayExternalUnitPageIcon);
					break;	
				
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					//czn_20190221_s
					externalUnitIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(externalUnitPageSelect*externalUnitIconMax+externalUnitIconSelect >= externalUnitSetNum)
					{
						return;
					}
					
					API_Event_IoServer_InnerRead_All(ExtUnitPwdEnable, display);
					
					if(atoi(display))
					{
						API_Event_IoServer_InnerRead_All(ExtUnitPwdPlace, display);
						if(atoi(display)==1||JudgeIsInstallerMode() || API_AskDebugState(100))
						{
							switch(externalUnitSet[externalUnitPageSelect*externalUnitIconMax+externalUnitIconSelect].iCon)
							{
								case ICON_AutoSetupWizard:
									if(ConfirmSelect(&externalUnitConfirm, &externalUnitConfirmSelect, &externalUnitIconSelect, externalUnitPageSelect, externalUnitIconMax))
									{
										return;
									}
									if(strcmp(GetSysVerInfo_BdRmMs(),"0099000101")==0)		
									{
										StartInitOneMenu(MENU_082_AutoSetupWizard,0,1);
									}
									break;
																
								case ICON_IM_Extensions:
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
									{
										StartInitOneMenu(MENU_080_IM_Extensions,0,1);
									}
									break;
									
								case ICON_OutdoorStations:
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
									{
										SaveIcon(ICON_OutdoorStations);
										StartInitOneMenu(MENU_081_OutdoorStations,0,1);
									}
									break;
								case ICON_PwdManage:
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
									{
										SaveIcon(ICON_PwdManage);
										StartInitOneMenu(MENU_081_OutdoorStations,0,1);
									}
									break;	
								case ICON_IP_Camera:
									//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									//ListEditSelect(3);
									//StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
									EnterIpcSetMenu(0);
									break;
									
								case ICON_Wlan_IP_Camera:
									//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									//ListEditSelect(4);
									//StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
									EnterIpcSetMenu(1);
									break;
									
								case ICON_EnterCallBtnBinding:
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									if(!GetCallBtnBingdingState())
									{
										if(API_GetCallBtnBindingState(1) == 0)
										{
											SetCallBtnBingdingState(1);
										}
									}
									
									API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
									if(GetCallBtnBingdingState())
									{
										AddOS_ListTempTableBySearching();
										API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_OS_BINDING, 1, 0);
										//sprintf(display, "In binding state, please press call button 10s");
										//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-DISPLAY_TITLE_X);
									}
									break;
							}
						}
						else
						{
							switch(externalUnitSet[externalUnitPageSelect*externalUnitIconMax+externalUnitIconSelect].iCon)
							{
								case ICON_AutoSetupWizard:
									if(strcmp(GetSysVerInfo_BdRmMs(),"0099000101")!=0)
										return;
									if(ConfirmSelect(&externalUnitConfirm, &externalUnitConfirmSelect, &externalUnitIconSelect, externalUnitPageSelect, externalUnitIconMax))
									{
										return;
									}
									break;	
								case ICON_IM_Extensions:
									if(memcmp(GetSysVerInfo_ms(),"01",2)!=0)	
										return;
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									break;
								case ICON_OutdoorStations:
									if(memcmp(GetSysVerInfo_ms(),"01",2)!=0)	
										return;
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									break;
								default:
									ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
									break;
									
							}
							extern char installerInput[9];
							
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, ExtUnitVerifyPassword);
						}
					}
					else
					{
						switch(externalUnitSet[externalUnitPageSelect*externalUnitIconMax+externalUnitIconSelect].iCon)
						{
	 						case ICON_AutoSetupWizard:
								if(ConfirmSelect(&externalUnitConfirm, &externalUnitConfirmSelect, &externalUnitIconSelect, externalUnitPageSelect, externalUnitIconMax))
								{
									return;
								}
								if(strcmp(GetSysVerInfo_BdRmMs(),"0099000101")==0)		
								{
									StartInitOneMenu(MENU_082_AutoSetupWizard,0,1);
								}
								break;
															
							case ICON_IM_Extensions:
								ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
								{
									StartInitOneMenu(MENU_080_IM_Extensions,0,1);
								}
								break;
								
							case ICON_OutdoorStations:
								ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
								{
									SaveIcon(ICON_OutdoorStations);
									StartInitOneMenu(MENU_081_OutdoorStations,0,1);
								}
								break;
							case ICON_PwdManage:
								ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		
								{
									SaveIcon(ICON_PwdManage);
									StartInitOneMenu(MENU_081_OutdoorStations,0,1);
								}
								break;		
							case ICON_IP_Camera:
								//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								//ListEditSelect(3);
								//StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								EnterIpcSetMenu(0);
								break;

							case ICON_Wlan_IP_Camera:
								//ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								//ListEditSelect(4);
								//StartInitOneMenu(MENU_036_LIST_EDIT,0,1);
								EnterIpcSetMenu(1);
								break;
								
							case ICON_EnterCallBtnBinding:
								ClearConfirm(&externalUnitConfirm, &externalUnitConfirmSelect, externalUnitIconMax);
								if(!GetCallBtnBingdingState())
								{
									if(API_GetCallBtnBindingState(1) == 0)
									{
										SetCallBtnBingdingState(1);
									}
								}
								
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
								if(GetCallBtnBingdingState())
								{
									AddOS_ListTempTableBySearching();
									API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_OS_BINDING, 1, 0);
									//sprintf(display, "In binding state, please press call button 10s");
									//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-DISPLAY_TITLE_X);
								}
								break;
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_OSBinding:
				API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 40);
				API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, COLOR_RED, MESG_TEXT_OS_BINDING_OK, 1, 0);
				//sprintf(display, "Binding OK");
				//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, COLOR_RED, display, strlen(display), 1, STR_UTF8, bkgd_w-DISPLAY_TITLE_X);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}
#if 0
int GetFreeOSNumber(void)
{
	int i;
	int number;
	int osCnt;
	int osNumber[100];
	OS_ListRecord_T data;

	osCnt = 1;//GetOS_ListTempTableNum();

	if(osCnt >= 100)
	{
		return 50;
	}
	
	for(i = 0; i < osCnt; i++)
	{
		GetOS_ListTempRecordItems(i, &data);
		osNumber[i] = atoi(data.BD_RM_MS+8);
	}

	for(number = 51; number < 100; number++)
	{
		for(i = 0; i < osCnt; i++)
		{
			if(osNumber[i] == number)
			{
				break;
			}
		}

		if(i == osCnt)
		{
			break;
		}
	}

	return number;
}
#endif


