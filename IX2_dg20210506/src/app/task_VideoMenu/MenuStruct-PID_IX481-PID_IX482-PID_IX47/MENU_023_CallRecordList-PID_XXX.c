
#include "MENU_public.h"
#include "MENU_023_CallRecordList.h"

//#define	CallRecordIconMax	5
int CallRecordIconMax;

CALL_RECORD_LIST_PAGE	call_record_page;
extern CALL_RECORD_LIST_PAGE*	recordPage;

// zfz_20190601 start
int call_record_del_all_confirm = -1;
static void ClearCallRecordDeleteConfirmFlag(void)
{
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	if(call_record_del_all_confirm >= 0)
	{
		OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
		Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
		API_SpriteClose(hv.h - xsize, hv.v - ysize, SPRITE_IF_CONFIRM);
	}
	call_record_del_all_confirm = -1;
};
void call_record_del_all_process(void)
{	
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
	Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
	if(call_record_del_all_confirm < 0)
	{
		call_record_del_all_confirm = 0;
		API_SpriteDisplay_XY(hv.h - xsize, hv.v - ysize, SPRITE_IF_CONFIRM);
	}
	else
	{
		call_record_del_all_confirm = -1;
		API_SpriteClose(hv.h - xsize, hv.v - ysize, SPRITE_IF_CONFIRM);

		call_record_delete_all();
		BEEP_CONFIRM();
	}
}
// end

void set_call_record_page_type( CALL_RECORD_LIST_TYPE type )		 //czn_20170306
{
	call_record_page.type 			= type;
	call_record_page.act_flag		= 0;
	call_record_page.page_num	= 0;
	call_record_page.list_focus = 0;
}

void display_one_call_record( CALL_RECORD_DAT_T* pcall_record, int x, int y, int color )
{
	char recordBuffer[80] = {0};
	int recordLen;
	
	API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	
	if(strcmp(pcall_record->name,"---") != 0)
	{
		snprintf( recordBuffer, ONE_LIST_MAX_LEN, "[20%s]%s", pcall_record->time, pcall_record->name );
		recordLen = strlen(recordBuffer);
		API_OsdStringDisplayExt(x, y, color, recordBuffer, recordLen,1,STR_UTF8, bkgd_w-x);
	}
	else
	{
		char node[40];
		char time[40];
		snprintf( time, 40, "[20%s]", pcall_record->time);
		snprintf( node, 40, "%d", pcall_record->target_node);
		API_GetOSD_StringWithID(MESG_TEXT_Node, time, strlen(time), node, strlen(node), recordBuffer, &recordLen);	
		API_OsdStringDisplayExt(x, y, color, recordBuffer, recordLen,1,STR_UNICODE, bkgd_w-x);
	}
}

int display_one_page_call_record( uint8 page )
{
 	CALL_RECORD_DAT_T call_record_value;
	int i, x, y;
	int list_start;
	POS pos;
	SIZE hv;

	// zfz_20190601
	if( call_record_page.type == CALL_REC_DEL_ALL )
	{
		return -1;
	}
	
	if( call_record_page.pcall_record == NULL )
	{
		printf("---call_record_page.pcall_record is empty----\n");
		return -1;
	}
	
	//API_DisableOsdUpdate();

	list_start = call_record_page.pcall_record->record_cnt - page*CallRecordIconMax - 1;
	
	for( i = 0; i < CallRecordIconMax; i++ )
	{
		if( (list_start-i) < 0 )
			break;
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
		if( get_callrecord_table_record_items( call_record_page.pcall_record, list_start-i, &call_record_value ) != -1 )
		{
 			display_one_call_record(&call_record_value,x,y,DISPLAY_LIST_COLOR);
		}
		else
		{
			break;
		}
	}
	for( ; i < CallRecordIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	}
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1,call_record_page.page_max?call_record_page.page_max:1);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, call_record_page.page_max?call_record_page.page_max:1);

	//API_EnableOsdUpdate();
}

/*************************************************************************************************
010.
*************************************************************************************************/
void MENU_023_CallRecordList_Init(int uMenuCnt)
{
	char tempDisplay[ONE_LIST_MAX_LEN];
	int list_cnt_save;
	int temp;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	CallRecordIconMax = GetListIconNum();
	call_record_page.onePageMaxNum 	= CallRecordIconMax;	
	
	if(call_record_page.act_flag == 1)
	{
		list_cnt_save = call_record_page.pcall_record->record_cnt;
	}
	
	if( call_record_page.type == CALL_INCOMING )
	{
		API_MenuIconDisplaySelectOn(ICON_018_IncomingCalls);
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_IncomingCallList,2, 0);
		if(call_record_filter(CallRecord_FilterType_IncomingCall) != 0)
		{
			//popDisplayLastMenu();
			DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
			return;
		}
		call_record_page.pcall_record 	= pcall_record_incoming;
	}
	else if( call_record_page.type == CALL_OUTGOING )
	{
		API_MenuIconDisplaySelectOn(ICON_019_OutgoingCalls);
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_OutgoingCallList,2, 0);
		if(call_record_filter(CallRecord_FilterType_OutgoingCall) != 0)
		{
			//popDisplayLastMenu();
			DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
			return;
		}
		call_record_page.pcall_record 	= pcall_record_outgoing;
	}
	else if( call_record_page.type == CALL_MISSED )
	{
		API_MenuIconDisplaySelectOn(ICON_017_MissedCalls);
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_MissedCallList,2, 0);
		if(call_record_filter(CallRecord_FilterType_MissCall) != 0)
		{
			//popDisplayLastMenu();
			DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
			return;
		}
		call_record_page.pcall_record 	= pcall_record_missed;
	}
	// zfz_20190601
	else if( call_record_page.type == CALL_REC_DEL_ALL )
	{
		API_MenuIconDisplaySelectOn(ICON_CallRecordDelAll);		
		//API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);			
		API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_CallRecordDelAll, 1, 0);
		OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
		API_OsdUnicodeStringDisplay(pos.x+DISPLAY_DEVIATION_X, pos.y+(hv.v - pos.y)/2 - ext_font2_h/2, DISPLAY_LIST_COLOR, MESG_TEXT_CARD_Delete, 1, 0);
		ClearCallRecordDeleteConfirmFlag();
		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
		return;
	}
	else
	{
		//popDisplayLastMenu();
	}

	if(call_record_page.pcall_record->record_cnt == 0)
	{
		//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X,MENULIST_PAGENUM_POS_Y, COLOR_WHITE,0,0);
		//usleep(1000000);
		//popDisplayLastMenu();
		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
		return;
	}
	
	if(call_record_page.act_flag == 1)
	{
		if(list_cnt_save < call_record_page.pcall_record->record_cnt)
		{
			temp = call_record_page.pcall_record->record_cnt - list_cnt_save;
			call_record_page.page_num += (call_record_page.list_focus + temp) / CallRecordIconMax;
			call_record_page.list_focus = (call_record_page.list_focus + temp) % CallRecordIconMax;	
		}
		if(list_cnt_save > call_record_page.pcall_record->record_cnt)
		{
			if(call_record_page.pcall_record->record_cnt <=(call_record_page.page_num*CallRecordIconMax + call_record_page.list_focus))
			{
				call_record_page.page_num = (call_record_page.pcall_record->record_cnt -1)/CallRecordIconMax;
				call_record_page.list_focus = (call_record_page.pcall_record->record_cnt -1)%CallRecordIconMax;
			}
		}
	}
	
	call_record_page.page_max	= (call_record_page.pcall_record->record_cnt+CallRecordIconMax-1)/CallRecordIconMax;

	//time_t t = time(NULL); 
	//call_record_page.tblock = localtime(&t);
	
	//if(call_record_page.tblock != NULL)
	//snprintf(call_record_page.time_dispbuf,20, "%02d/%02d/%02d ",call_record_page.tblock->tm_year-100,call_record_page.tblock->tm_mon+1,call_record_page.tblock->tm_mday);

	
	display_one_page_call_record(call_record_page.page_num);

	call_record_page.act_flag = 1;
}

void MENU_023_CallRecordList_Exit(void)
{
}

void MENU_023_CallRecordList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					//StartInitOneMenu(MENU_025_CALL_RECORD_DETAIL,0,1);
					recordPage = (CALL_RECORD_LIST_PAGE*)&call_record_page;
					StartInitOneMenu(MENU_026_PLAYBACK,0,1);
					break;
					
				case KEY_UP:
					PublicListUpProcess(&call_record_page.list_focus, &call_record_page.page_num, CallRecordIconMax, call_record_page.pcall_record->record_cnt, (DispListPage)display_one_page_call_record);
					break;
				case KEY_DOWN:
					PublicListDownProcess(&call_record_page.list_focus, &call_record_page.page_num, CallRecordIconMax, call_record_page.pcall_record->record_cnt, (DispListPage)display_one_page_call_record);
					break;
					
				default:
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					call_record_page.list_focus = GetCurIcon() - ICON_007_PublicList1;

					// zfz_20190601
					if( call_record_page.type == CALL_REC_DEL_ALL )
					{
						if( call_record_page.list_focus == 0 )
						{
							call_record_del_all_process();
						}
					}
					else
					{
						if(call_record_page.page_num*CallRecordIconMax+call_record_page.list_focus < call_record_page.pcall_record->record_cnt)
						{
							recordPage = (CALL_RECORD_LIST_PAGE*)&call_record_page;
							StartInitOneMenu(MENU_026_PLAYBACK,0,1);
						}
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&call_record_page.list_focus, CallRecordIconMax, call_record_page.pcall_record->record_cnt, (DispListPage)display_one_page_call_record);
					break;
					
				case ICON_202_PageUp:
					PublicPageUpProcess(&call_record_page.list_focus, CallRecordIconMax, call_record_page.pcall_record->record_cnt, (DispListPage)display_one_page_call_record);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
 }



