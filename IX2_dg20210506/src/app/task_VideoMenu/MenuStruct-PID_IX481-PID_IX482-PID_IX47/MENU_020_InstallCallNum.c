#include "MENU_public.h"
#include "MENU_020_InstallCallNum.h"

#define	InstallCallNum_ICON_MAX	5
int installCallNumIconSelect;
int installCallNumPageSelect;
char installCallNumInput[41];
int GetSystemTypeAb(char *unicode,int len,char *ascii);

const InstallCallNumIcon installCallNumSet0[] = 
{
	{ICON_279_RM_ADDR,					MESG_TEXT_ICON_279_RM_ADDR},
	{ICON_280_MS_Number,				MESG_TEXT_ICON_280_MS_Number},
	{ICON_281_Name,						MESG_TEXT_ICON_281_Name},
	{ICON_278_Global_Nbr,				MESG_TEXT_ICON_278_Global_Nbr},
	{ICON_282_Local_Nbr,				MESG_TEXT_ICON_282_Local_Nbr},
};


const unsigned char installCallNumSetNum0 = sizeof(installCallNumSet0)/sizeof(installCallNumSet0[0]);

const InstallCallNumIcon installCallNumSet1[] = 
{
	{ICON_279_RM_ADDR,					MESG_TEXT_ICON_279_RM_ADDR},
	{ICON_280_MS_Number,				MESG_TEXT_ICON_280_MS_Number},
	{ICON_281_Name,						MESG_TEXT_ICON_281_Name},
	//{ICON_278_Global_Nbr,				MESG_TEXT_ICON_278_Global_Nbr},
	//{ICON_282_Local_Nbr,				MESG_TEXT_ICON_282_Local_Nbr},
};


const unsigned char installCallNumSetNum1 = sizeof(installCallNumSet1)/sizeof(installCallNumSet1[0]);

const InstallCallNumIcon installCallNumSet2[] = 
{
	{ICON_279_RM_ADDR,					MESG_TEXT_ICON_279_RM_ADDR},
	{ICON_280_MS_Number,				MESG_TEXT_ICON_280_MS_Number},
	{ICON_281_Name,						MESG_TEXT_ICON_281_Name},
	{ICON_282_Local_Nbr,				MESG_TEXT_ICON_282_Local_Nbr},
	{ICON_278_Global_Nbr,				MESG_TEXT_ICON_278_Global_Nbr},
};


const unsigned char installCallNumSetNum2 = sizeof(installCallNumSet2)/sizeof(installCallNumSet2[0]);

unsigned char installCallNumSetNum=0;
InstallCallNumIcon *installCallNumSet=NULL;

static void DisplayInstallCallNumPageIcon(int page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char display[100];
	char ms_buf[10];
	int ms;
	int disp_len;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	for(i = 0; i < InstallCallNum_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*InstallCallNum_ICON_MAX+i < installCallNumSetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, installCallNumSet[i+page*InstallCallNum_ICON_MAX].iConText, 1, hv.h-x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(installCallNumSet[i+page*InstallCallNum_ICON_MAX].iCon)
			{
				case ICON_279_RM_ADDR:
					//dh_20190916_s
					#if 0
					if(memcmp(GetSysVerInfo_BdRmMs(), "00990001", 8))
					{
						if(!strcmp(GetSysVerInfo_bd(), "0099"))
						{
							sprintf(display, "(SS)%d", atoi(GetSysVerInfo_rm()));
						}
						else
						{
							sprintf(display, "(NS)%s %s", GetSysVerInfo_bd(), GetSysVerInfo_rm());
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, MENU_SCHEDULE_POS_X-x);
					}
					else
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_ICON_SelectSystemTypeVS, 1, MENU_SCHEDULE_POS_X-x);
					}
					#endif
					ms = GetSysVerInfo_SystemType();
					if(ms)
					{
						char systype_buff[100];
						int unicode_len;
						if(ms == 1)
						{
							API_GetOSD_StringWithID(MESG_TEXT_ICON_SelectSystemTypeSS, NULL, 0, NULL, 0,systype_buff, &unicode_len);
							if(GetSystemTypeAb(systype_buff, unicode_len, display)==0)
							{
								sprintf(display, "%s%d", display,atoi(GetSysVerInfo_rm()));
							}
							else
							{
								sprintf(display, "(SS)%d", atoi(GetSysVerInfo_rm()));
							}
						}
						else
						{
							API_GetOSD_StringWithID(MESG_TEXT_ICON_SelectSystemTypeNS, NULL, 0, NULL, 0,systype_buff, &unicode_len);
							if(GetSystemTypeAb(systype_buff, unicode_len, display)==0)
							{
								sprintf(display, "%s%s %s",display,GetSysVerInfo_bd(), GetSysVerInfo_rm());
							}
							else
							{
								sprintf(display, "(NS)%s %s", GetSysVerInfo_bd(), GetSysVerInfo_rm());
							}
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					}
					else
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_ICON_SelectSystemTypeVS, 1, hv.h-x);
					}
					//dh_20190916_e
					break;
				case ICON_280_MS_Number:
					ms = atoi(GetSysVerInfo_ms());
					#if 0
					if(ms == 1)
					{
						sprintf(display, "(%d)%s", ms, "Master");
					}
					else
					{
						sprintf(display, "(%d)%s", ms, "Slave");
					}
					#endif
					sprintf(ms_buf, "(%d)", ms);		//FOR_INDEXA
					API_GetOSD_StringWithID((ms==1)?MESG_TEXT_CusMsMaster:MESG_TEXT_CusMsSlave,ms_buf,strlen(ms_buf), NULL,0,display,&disp_len);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, disp_len, 1, STR_UNICODE, hv.h-x);
					break;
				case ICON_281_Name:
					strcpy(display, GetSysVerInfo_name());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
				case ICON_278_Global_Nbr:
					strcpy(display, GetSysVerInfo_GlobalNum());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;

				case ICON_282_Local_Nbr:
					strcpy(display, GetSysVerInfo_LocalNum());
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = installCallNumSetNum/InstallCallNum_ICON_MAX + (installCallNumSetNum%InstallCallNum_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}
 
int SetRoomNumber(const char* number)
{
	int numLen;
	char bdRmMs[11];
	int num;
	
	numLen = strlen(number);

	if(numLen == 0 || numLen > 8)
	{
		return 0;
	}
	else if(numLen <= 3)
	{
		num = atoi(number);
		if(num <= 128 && num >= 1)
		{
			sprintf(bdRmMs, "0099%04d%s", num, GetSysVerInfo_ms());
			SetSysVerInfo_BdRmMs(bdRmMs);
			SetIP_ADDR_ByDS1();
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if(numLen == 8)
	{
		for( ; numLen > 0; numLen--)
		{
			if(number[numLen-1] < '0' || number[numLen-1] > '9')
			{
				return 0;
			}
		}
		strcpy(bdRmMs, GetSysVerInfo_BdRmMs());
		memcpy(bdRmMs, number, 8);
		SetSysVerInfo_BdRmMs(bdRmMs);
		SetIP_ADDR_ByDS1();
		return 1;
	}

}

int SetMasterSlaveNumber(const char* number)
{
	int num = atoi(number);
	char ms[3];

	if(num <= 0 || num > 32)
		return 0;
	
	sprintf(ms, "%02d", num);
	SetSysVerInfo_ms(ms);
	SetIP_ADDR_ByDS1();
	return 1;
}

int SetMyName(const char* name)
{
	int i, j;

	for(i = 0, j = 0; name[i] != 0; i++)
	{
		if(name[i] != ' ')
		{
			j++;
		}
	}

	SetSysVerInfo_name((j == 0) ? "-" : name);
	
	return 1;
}

int SetMyGlobalNumber(const char* number)
{
	int i, j;

	for(i = 0, j = 0; number[i] != 0; i++)
	{
		if(number[i] != ' ')
		{
			j++;
		}
	}

	SetSysVerInfo_GlobalNum((j == 0) ? "-" : number);	
	
	return 1;
}

int SetMyLocalNumber(const char* number)
{
	int i, j;

	for(i = 0, j = 0; number[i] != 0; i++)
	{
		if(number[i] != ' ')
		{
			j++;
		}
	}

	SetSysVerInfo_LocalNum((j == 0) ? "-" : number);	

	return 1;
}


void MENU_020_InstallCallNum_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	char buff[5];
	API_Event_IoServer_InnerRead_All(CallNumMenuMode, (uint8*)buff);
	
	if(atoi(buff) == 1)
	{
		installCallNumSetNum = installCallNumSetNum1;
		installCallNumSet = installCallNumSet1;
	}
	else if(atoi(buff) == 2)
	{
		installCallNumSetNum = installCallNumSetNum2;
		installCallNumSet = installCallNumSet2;
	}
	else
	{
		installCallNumSetNum = installCallNumSetNum0;
		installCallNumSet = installCallNumSet0;
	}
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_030_CALL_NUMBER);
	API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_030_CALL_NUMBER, 1, 0);
	installCallNumIconSelect = 0;
	installCallNumPageSelect = 0;
	DisplayInstallCallNumPageIcon(installCallNumPageSelect);
}

void MENU_020_InstallCallNum_Exit(void)
{

}

void MENU_020_InstallCallNum_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[10];
	char* pbCertState = API_PublicInfo_Read_String("CERT_State");
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					installCallNumIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(installCallNumPageSelect*InstallCallNum_ICON_MAX+installCallNumIconSelect >= installCallNumSetNum)
					{
						return;
					}

					switch(installCallNumSet[installCallNumPageSelect*InstallCallNum_ICON_MAX+installCallNumIconSelect].iCon)
					{
 						case ICON_279_RM_ADDR:
							if(pbCertState && !strcmp(pbCertState, "Delivered"))
							{
								CmdRingReq("CERT Delivered, please delete.");
								StartInitOneMenu(MENU_157_CertSetup,0,1);
								return;
							}
							StartInitOneMenu(MENU_096_SelectSystemType,0,1);
							break;
						case ICON_280_MS_Number:
							if(pbCertState && !strcmp(pbCertState, "Delivered"))
							{
								CmdRingReq("CERT Delivered, please delete.");
								StartInitOneMenu(MENU_157_CertSetup,0,1);
								return;
							}
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_280_MS_Number, installCallNumInput, 2, COLOR_WHITE, GetSysVerInfo_ms(), 1, SetMasterSlaveNumber);
							break;
						case ICON_281_Name:
							//EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
							API_Event_IoServer_InnerRead_All(EditNameKeyPad, tempChar);
							if(atoi(tempChar)==1)
								EnterMulLangKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
							else
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_281_Name, installCallNumInput, 20, COLOR_WHITE, GetSysVerInfo_name(), 1, SetMyName);
							break;
							
						case ICON_278_Global_Nbr:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_278_Global_Nbr, installCallNumInput, 10, COLOR_WHITE, GetSysVerInfo_GlobalNum(), 1, SetMyGlobalNumber);
							break;
							
						case ICON_282_Local_Nbr:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_282_Local_Nbr, installCallNumInput, 6, COLOR_WHITE, GetSysVerInfo_LocalNum(), 1, SetMyLocalNumber);
							break;
					}
					break;
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}



int GetSystemTypeAb(char *unicode,int len,char *ascii)
{
	int i;
	if(len<8)
		return -1;
	if(unicode[0]!='('||unicode[1]!=0)
	{
		return -1;
	}
	ascii[0]= '(';
	for(i = 1;i<(len/2);i++)
	{
		if(unicode[i*2+1]!=0)
			return -1;
		ascii[i]=unicode[i*2];
		if(unicode[i*2]==')'&&i>=2)
		{
			ascii[i+1]=0;
			return 0;
		}
	}
	
	return -1;
}
