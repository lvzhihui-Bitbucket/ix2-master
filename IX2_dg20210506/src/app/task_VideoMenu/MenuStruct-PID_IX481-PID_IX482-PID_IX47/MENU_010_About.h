
#ifndef _MENU_010_About_H
#define _MENU_010_About_H

#include "MENU_public.h"

#define About_KeyIPAddress					"IPAddress"
#define About_KeyRMAddress					"RMAddress"
#define About_KeyName						"Name"
#define About_KeyGlobal_Nbr					"Global_Nbr"
#define About_KeyLocal_Nbr					"Local_Nbr"
#define About_KeySW_Ver						"SW_Ver"
#define About_KeyHWVersion					"HWVersion"
#define About_KeyUpgradeTime				"UpgradeTime"
#define About_KeyUpgradeCode				"UpgradeCode"
#define About_KeyUpTime						"UpTime"
#define About_KeySerialNo					"SerialNo"
#define About_KeyDeviceType					"DeviceType"
#define About_KeyDeviceModel				"DeviceModel"
#define About_KeyAreaCode					"AreaCode"
#define About_KeyTransferState				"TransferState"
#define About_KeyHW_Address					"HW_Address"
#define About_KeySubnetMask					"SubnetMask"
#define About_KeyDefaultRoute				"DefaultRoute"

#define About_KeyWLANIPAddress				"W_IPAddress"
#define About_KeyWLANHW_Address				"W_HW_Address"
#define About_KeyWLANSubnetMask				"W_SubnetMask"
#define About_KeyWLANDefaultRoute			"W_DefaultRoute"

typedef enum
{
	About_IPAddress,		
	About_RMAddress,		
	About_Name,			
	About_Global_Nbr,	
	About_Local_Nbr,		
	About_SW_Ver,		
	About_HWVersion,		
	About_UpgradeTime,	
	About_UpgradeCode,	
	About_UpTime,		
	About_SerialNo,		
	About_DeviceType,	
	About_DeviceModel,	
	About_AreaCode,		
	About_TransferState,	
	About_HW_Address,	
	About_SubnetMask,	
	About_DefaultRoute,	
	
	About_WLANIPAddress,
	About_WLANHW_Address,
	About_WLANSubnetMask,
	About_WLANDefaultRoute,	

	
    AboutItemMax,
    
} AboutItem_E;

typedef struct
{
	AboutItem_E 		item;
	char* 				keyWord;
}AboutKeyword_S;

typedef struct
{
	int 				cnt;
	AboutKeyword_S 		itemAndKeyword[50];
}AboutList;


#endif


