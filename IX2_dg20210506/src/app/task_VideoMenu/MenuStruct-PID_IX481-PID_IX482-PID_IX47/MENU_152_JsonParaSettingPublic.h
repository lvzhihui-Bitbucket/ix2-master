
#ifndef _MENU_123_H
#define _MENU_123_H

#include "MENU_public.h"

#define	JsonParaSetting_ICON_MAX	5

typedef int (*JsonParaSettingMenuDispFunc_T)(int,char*,int*);
typedef int (*JsonParaSettingSelectFunc_T)(int);
typedef int (*JsonParaSettingReturnFunc_T)(void);
typedef void (*JsonParaSettingSaveFunc_T)(void);
typedef int (*JsonParaSettingInformFilterFunc_T)(int,void*);
typedef int (*JsonParaSettingListRightDispFunc_T)(int);
typedef int (*JsonParaSettingListRightSelectFunc_T)(int);

void Set_JsonParaSettingMenuItemDispFunc(JsonParaSettingMenuDispFunc_T func);

void Set_JsonParaSettingMenuValueDispFunc(JsonParaSettingMenuDispFunc_T func);

void Set_JsonParaSettingMenuSelectFunc(JsonParaSettingSelectFunc_T func);

void Set_JsonParaSettingMenuReturnFunc(JsonParaSettingReturnFunc_T func);

void Set_JsonParaSettingMenuSaveFunc(JsonParaSettingSaveFunc_T func);

void Set_JsonParaSettingDispMode(int mode);

void Set_JsonParaSettingHaveChange(int val);

void Set_JsonParaSettingToKeypad(int val);

void Set_JsonParaSettingCurItemNums(int val);

void Set_JsonParaSettingCurPage(int val);

void Set_JsonParaSettingCurLine(int val);

void Set_JsonParaSettingCurNodeIndex(int val);

int Get_JsonParaSettingCurNodeIndex(void);

void Set_JsonParaSettingCurNode(cJSON *one_node);

cJSON* Get_JsonParaSettingCurNode(void);

void Set_JsonParaSettingTitle(int text,char *title);

#endif


