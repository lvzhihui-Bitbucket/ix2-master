#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "MENU_public.h"
#include "obj_SYS_VER_INFO.h"
#include "define_Command.h"
#include "MENU_131_ViewFile.h"
#include "cJSON.h"

#define	FILE_MAX_LINE_NUM			1000		//允许浏览的最大文件行数

static int viewFileLineNum;
static int viewFilePageSelect = 0;
static char viewFileDir[100];
static char viewFileName[50];


int EnterViewFileMenu(const char* dir, const char* fileName)
{
	char cmd[200];
	char value[200];
	char* pReadBuf[1];
	int ret = 0;


	snprintf(cmd, 200, "awk 'END{print NR}' %s/%s", dir, fileName);	
	pReadBuf[0] = value;
	if(PopenProcess(cmd, pReadBuf, 200, 1))
	{
		viewFileLineNum = atoi(value);
		if(viewFileLineNum <= FILE_MAX_LINE_NUM)
		{
			snprintf(viewFileDir, 100, "%s", dir); 
			snprintf(viewFileName, 50, "%s", fileName); 

			StartInitOneMenu(MENU_131_ViewFile, 0, 1);
			ret = 1;
		}
	}

	return ret;
}


static void MenuListGetViewFilePage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	#define MAX_LINE_NUM	10
	int i, j, k, strLen;
	char cmd[200];
	char value[MAX_LINE_NUM][100];
	char* pReadBuf[MAX_LINE_NUM];
	int lineStart;
	int lineEnd;
	int lineNum;
	int lineCnt;

	pDisp->dispCnt = 0;

	for(i = 0; i < MAX_LINE_NUM; i++)
	{
		pReadBuf[i] = value[i];
	}

	lineStart = currentPage * onePageListMax + 1;
	lineEnd = (currentPage+1)*onePageListMax;
	if(lineEnd > viewFileLineNum) lineEnd = viewFileLineNum;
	if(lineEnd - lineStart + 1 > MAX_LINE_NUM) lineEnd = lineStart + MAX_LINE_NUM - 1;
	lineNum = lineEnd - lineStart + 1;

	snprintf(cmd, 200, "sed -n '%d,%dp' %s/%s", lineStart, lineEnd, viewFileDir, viewFileName); 
	lineCnt = PopenProcess(cmd, pReadBuf, 100, lineNum);
	for(i = 0; i < lineCnt; i++)
	{
		for(j = 0, k = 0, strLen = strlen(value[i]); j < strLen; j++)
		{
			if(value[i][j] == '\t')
			{
				cmd[k++] = ' ';
				cmd[k++] = ' ';
			}
			else
			{
				cmd[k++] = value[i][j];
			}
		}
		
		pDisp->disp[pDisp->dispCnt].strLen = 2*api_ascii_to_unicode(cmd, k, pDisp->disp[pDisp->dispCnt].str); 	
		pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
		pDisp->dispCnt++;
	}	
}

void MENU_131_ViewFile_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
			
	unicode_len = 2*api_ascii_to_unicode(viewFileName, strlen(viewFileName), unicode_buf); 	
	
	listInit = ListPropertyDefault();
	
	listInit.listType = TEXT_10X1;
	listInit.listIconEn = 0;
	listInit.listCnt = viewFileLineNum;
	listInit.fun = MenuListGetViewFilePage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	
	if(GetLastNMenu() == MENU_130_FileList)
	{
		viewFilePageSelect = 0;
	}

	listInit.currentPage = viewFilePageSelect;
	
	InitMenuList(listInit);
}

void MENU_131_ViewFile_Exit(void)
{	
	MenuListDisable(0);
}

void MENU_131_ViewFile_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon < MENU_LIST_ICON_NONE)
					{
						viewFilePageSelect = MenuListGetCurrentPage();
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


