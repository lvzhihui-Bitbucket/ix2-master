#include "MENU_120_BackResDownload.h"
#include "MENU_066_FwUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_RemoteUpgrade.h"
#include "obj_PublicUdpCmd.h"
#include "obj_SearchIxProxy.h"
#include "MENU_public.h"
#include "MenuBackFwUpgrade_Business.h"
#include "ota_FileProcess.h"
#include "cJSON.h"
//0,uncheck,1,checking,2 checked 3 tranfering 4tranfered/updating,5canceling


static RES_DOWNLOAD_REQ_DATA_T resDownloadData;
static SearchIxProxyRspData ixProxyData;
static int reportIp[10];
static int resDownloadFlag = 0;




extern int  BackFwUpgrade_State;

extern char text_file_dir[60];
extern int Fw_file_size;
extern int Fw_file_trans_size;
extern char fw_file_dir[60];
extern char trans_file_name[40];
extern OtaDlTxt_t dlTxtInfo;

const IconAndText_t resDLIconTable[] = 
{
	{ICON_FwUpgrade_Server, 		MESG_TEXT_ICON_FwUpgrage_Server},	 
	{ICON_FwUpgrade_DLCode, 		MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		MESG_TEXT_FwUpgrage_CodeInfo},
	{ICON_FwUpgrade_Notice, 		NULL},
};

const int resDLIconTableNum = sizeof(resDLIconTable)/sizeof(resDLIconTable[0]);

static void DisplayResDownLoadPageIcon(void)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	

	if(GetCurMenuCnt() != MENU_120_BackResDownload)
	{
		return;
	}
	
	API_DisableOsdUpdate();
		
	for(i = 0; i < resDLIconTableNum; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		
		x = pos.x + DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2;
		

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);

		x1 = x + (hv.h - pos.x)/2;
		y1 = y;

		switch(resDLIconTable[i].iCon)
		{
			case ICON_FwUpgrade_Server:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resDLIconTable[i].iConText, 1, x1-x-10);
				if(resDownloadData.SERVER[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, resDownloadData.SERVER, strlen(resDownloadData.SERVER), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			
			case ICON_FwUpgrade_DLCode:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resDLIconTable[i].iConText, 1, x1-x-10);
				if(resDownloadData.CODE[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, resDownloadData.CODE, strlen(resDownloadData.CODE), 1, STR_UTF8, bkgd_w-x1-20);
				break;
				
			case ICON_FwUpgrade_CodeInfo:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resDLIconTable[i].iConText, 1, x1-x-10);
				if(trans_file_name[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, trans_file_name, strlen(trans_file_name), 1, STR_UTF8, bkgd_w-x1-20);
				break;
		}
	}

	API_EnableOsdUpdate();
}

void DisplayResDownLoadNotice(const char* state , const char* notice)
{
	uint16 x, y,x1,y1;
	char display[200];
	POS pos;
	SIZE hv;
	
	if(GetCurMenuCnt() != MENU_120_BackResDownload)
	{
		return;
	}
	
	OSD_GetIconInfo(ICON_007_PublicList1+3, &pos, &hv);
	
	x = pos.x + DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2;

	x1 = x + (hv.h - pos.x)/2;
	y1 = y;

	API_DisableOsdUpdate();

	API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
	
	snprintf(display, 200, "%s    %s", (state == NULL) ? "" : state, (notice == NULL) ? "" : notice);
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
		
	API_EnableOsdUpdate();
}

static int SD_CardFwUpgradeOperation(char* code)
{
	char display[100];
	char *state;

	if(code == NULL)
	{
		return -1;
	}
	
	if(Judge_SdCardLink() == 0)
	{
		state = RES_DOWNLOAD_STATE_INSTALL_FAIL;
	}
	else if(FwUpgrade_Code_Verify(code) == 1)
	{
		sprintf(display,"%s/%s.txt", SdUpgradePath, code);
		
		if(CheckDownloadIsAllowed(display, &dlTxtInfo))
		{
			BackFwUpgrade_State = FwUpgrade_State_CheckOk;
			IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_CHECK_OK, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
							
			BackFwUpgrade_State = FwUpgrade_State_Intalling;
			
			IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_INSTALLING, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
			
			sprintf(display,"%s/%s.zip",SdUpgradePath, code);
			if(start_updatefile_and_reboot_forsdcard(0,display) >= 0)
			{
				remove_upgrade_tempfile();
				IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_INSTALL_OK, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
				sleep(1);
				IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_WAITING_REBOOT, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
				DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_WAITING_REBOOT, NULL);
				HardwareRestar();
			}
			else
			{
				state = RES_DOWNLOAD_STATE_INSTALL_FAIL;
			}
			BackFwUpgrade_State = FwUpgrade_State_Intalled; 
		}
		else
		{
			BackFwUpgrade_State = FwUpgrade_State_Intalled; 
			state = RES_DOWNLOAD_STATE_ERR_TYPE;
		}
	}
	else
	{
		BackFwUpgrade_State = FwUpgrade_State_Intalled; 
		state = RES_DOWNLOAD_STATE_ERR_CODE;
	}
	
	IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", state, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
}
int ResUpgradeForward_Operation(char* server, char* code)
{
	int serverSelsct;
	
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	char *state;

	resDownloadFlag = 1;
	
	if(!strcmp(server, "SD card"))
	{
		//SD card 更新
		
		SD_CardFwUpgradeOperation(code);
		resDownloadFlag = 0;
		CloseMenu();
		return 0;
	}
	else
	{
		//服务器 更新
		if(BackFwUpgrade_State == FwUpgrade_State_Uncheck || BackFwUpgrade_State == FwUpgrade_State_Intalled)
		{
			if(FwUpgrade_Code_Verify(code) == 1)
			{
				if((error_code = get_upgrade_file_dir(1,0,text_file_dir)) == 0)
				{
					sysinfo = GetSysVerInfo();
					printf("111111111 api_ota_ifc_file_download server=%s, code=%s",server,code);
					api_ota_ifc_ftp_start(server,text_file_dir, code, 1, GetSysVerInfo_type(),GetSysVerInfo_swVer(),GetSysVerInfo_Sn());
					BackFwUpgrade_State = FwUpgrade_State_Checking;
					state = RES_DOWNLOAD_STATE_CONNECTING;
				}
				else
				{
					state = (error_code == -1) ? RES_DOWNLOAD_STATE_NO_SDCARD : RES_DOWNLOAD_STATE_ERR_NO_SPASE;
				}
			}
			else
			{
				state = RES_DOWNLOAD_STATE_ERR_CODE;
			}
		}
		else if(BackFwUpgrade_State == FwUpgrade_State_Checking)
		{
			BackFwUpgrade_State = FwUpgrade_State_CheckOk;
			sprintf(display,"%s/%s",text_file_dir,trans_file_name);
			if(CheckDownloadIsAllowed(display, &dlTxtInfo))
			{
				if((error_code = get_upgrade_file_dir(0,Fw_file_size,fw_file_dir)) == 0)
				{
					printf("0000000 api_ota_ifc_file_download server=%s, code=%s",server,code);
					api_ota_ifc_ftp_start(server, fw_file_dir, code, 0, GetSysVerInfo_type(), GetSysVerInfo_swVer(), GetSysVerInfo_Sn());
					BackFwUpgrade_State = FwUpgrade_State_Downloading;
					state = RES_DOWNLOAD_STATE_DOWNLOADING;
					
					IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", state, "0.00%", NULL, resDownloadData.SERVER, resDownloadData.CODE);
					DisplayResDownLoadNotice(state, "0.00%");
					return 0;
				}
				else
				{
					BackFwUpgrade_State = FwUpgrade_State_Uncheck;
					state = (error_code == -1) ? RES_DOWNLOAD_STATE_NO_SDCARD : RES_DOWNLOAD_STATE_ERR_NO_SPASE;
				}
			}
			else
			{
				state = RES_DOWNLOAD_STATE_ERR_TYPE;
				BackFwUpgrade_State = FwUpgrade_State_Uncheck;
			}
		}
		else if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
		{
			snprintf(display, 21, "%4.2f%%", Fw_file_size*100.0/Fw_file_size);
			IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_DOWNLOADING, display, NULL, resDownloadData.SERVER, resDownloadData.CODE);
			DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_DOWNLOADING, display);

			BackFwUpgrade_State = FwUpgrade_State_Intalling;
			IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_INSTALLING, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
			DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_INSTALLING, NULL);
			sprintf(display,"%s/%s",fw_file_dir,trans_file_name);
			if(FwUprade_Installing(display) >= 0)
			{
				IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_INSTALL_OK, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
				DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_INSTALL_OK, NULL);
				sleep(1);
				IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_WAITING_REBOOT, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
				DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_WAITING_REBOOT, NULL);
				remove_upgrade_tempfile();

				if(IsMyProxyLinked())
				{
					SetIxProxyRebootFlag(1);
				}
				else
				{
					WriteRebootFlag(NULL, 0);
					HardwareRestar();
				}
				BackFwUpgrade_State = FwUpgrade_State_Intalled;
				return 0;
			}
			else
			{
				remove_upgrade_tempfile();
				state = RES_DOWNLOAD_STATE_INSTALL_FAIL;
				BackFwUpgrade_State = FwUpgrade_State_Intalled;
			}
		}
	}

	DisplayResDownLoadNotice(state, NULL);
	IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", state, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
	if(BackFwUpgrade_State == FwUpgrade_State_Intalled || BackFwUpgrade_State == FwUpgrade_State_Uncheck)
	{
		resDownloadFlag = 0;
		CloseMenu();
	}
	
}

void ResUpgrade_Cancle(void)
{
	if(BackFwUpgrade_State == FwUpgrade_State_Checking||BackFwUpgrade_State == FwUpgrade_State_Downloading)
	{
		BackFwUpgrade_State = FwUpgrade_State_Canceling;
		IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_CANCEL, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
		DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_CANCEL, NULL);
		api_ota_ifc_cancel();
		BackFwUpgrade_State = FwUpgrade_State_Uncheck;
	}
}

int ResUpgradeInform_Operation(int inform, void* arg)
{
	int ret = 0;
	int error_code;
	char retes[21];
	char *state;
	int i;
	cJSON *root;
	
	switch(inform)
	{
		case MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_START:
		    root = cJSON_Parse((char*)GetMenuInformData(arg));
			if(strcmp(cJSON_GetStringValue(cJSON_GetObjectItem(root, DM_KEY_operate)), DM_KEY_CANCEL))
			{
				API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
				for(i = 0; i < ixProxyData.deviceCnt; i++)
				{
					reportIp[i] = inet_addr(ixProxyData.data[i].ip_addr);
				}
				
				if(BackFwUpgrade_State == FwUpgrade_State_Uncheck || BackFwUpgrade_State == FwUpgrade_State_Intalled)
				{
					Fw_file_trans_size = 0;
					ResUpgradeForward_Operation(cJSON_GetStringValue(cJSON_GetObjectItem(root, DM_KEY_server)), cJSON_GetStringValue(cJSON_GetObjectItem(root, DM_KEY_code)));
				}
			}
			else
			{
				ResUpgrade_Cancle();
			}
			cJSON_Delete(root);
			break;

		case MSG_7_BRD_SUB_OtaClientIfc_ActOk:
			if(BackFwUpgrade_State == FwUpgrade_State_Checking || BackFwUpgrade_State == FwUpgrade_State_Downloading)
			{	
				Fw_file_size = *((int*)GetMenuInformData(arg));
				strncpy(trans_file_name,(char *)(GetMenuInformData(arg)+sizeof(int)),40);
				DisplayResDownLoadPageIcon();
			}
			break;
		case MSG_7_BRD_SUB_OtaClientIfc_Translated:
			ResUpgradeForward_Operation(resDownloadData.SERVER, resDownloadData.CODE);
			break;
		case MSG_7_BRD_SUB_OtaClientIfc_Translating:
			if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
			{
				Fw_file_trans_size = *((int*)GetMenuInformData(arg));
				snprintf(retes, 21, "%4.2f%%", Fw_file_trans_size*100.0/Fw_file_size);
				IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", RES_DOWNLOAD_STATE_DOWNLOADING, retes, NULL, resDownloadData.SERVER, resDownloadData.CODE);
				DisplayResDownLoadNotice(RES_DOWNLOAD_STATE_DOWNLOADING, retes);
			}
			break;
		case MSG_7_BRD_SUB_OtaClientIfc_Error:
			//rsp -1:busy,-2:connect ser error,-3:no file,-4:download error -5:no sdcard
			error_code = *((int*)GetMenuInformData(arg));
			
			if(error_code == -1)
			{
				state = RES_DOWNLOAD_STATE_ERR_CONNECT;
			}
			else if(error_code == -2)
			{
				state = RES_DOWNLOAD_STATE_ERR_CONNECT;
			}
			else if(error_code == -3)
			{
				state = RES_DOWNLOAD_STATE_ERR_NO_FILE;
			}
			else if(error_code == -4)
			{
				state = RES_DOWNLOAD_STATE_ERR_DOWNLOAD;
			}
			else
			{
				state = RES_DOWNLOAD_STATE_ERR_DOWNLOAD;
			}

			IxReportResDownload(reportIp, ixProxyData.deviceCnt, "TCP", state, NULL, NULL, resDownloadData.SERVER, resDownloadData.CODE);
			DisplayResDownLoadNotice(state, NULL);

			if(BackFwUpgrade_State == FwUpgrade_State_Checking)
			{
				BackFwUpgrade_State = FwUpgrade_State_Uncheck;
			}
			else if(BackFwUpgrade_State == FwUpgrade_State_Downloading)
			{
				remove_upgrade_tempfile();
				BackFwUpgrade_State = FwUpgrade_State_Uncheck;
			}
			resDownloadFlag = 0;
			CloseMenu();
			break;
		default:
			ret = -1;
			break;
	}

	return ret;
}


int GetResDownloadState(void)
{
	if(resDownloadFlag)
	{
		return 1;
	}

	return 0;	
}

void MENU_120_BackResDownload_Init(int uMenuCnt)
{
	DisplayResDownLoadPageIcon();
}

void MENU_120_BackResDownload_Exit(void)
{

}

void MENU_120_BackResDownload_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
				case ICON_047_Home:
					CloseMenu();
					break;
					
				case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		ResUpgradeInform_Operation(pglobal_win_msg->status, arg);
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}



