#include "MENU_public.h"
#include "cJSON.h"

#define	OnlineReboot2_ICON_MAX		5
int onlineReboot2IconSelect;
int onlineReboot2PageSelect;
int reboot2Confirm;
int reboot2ConfirmSelect;
int certState=0;
const IconAndText_t onlineReboot2Set0[] = 
{
	{ICON_292_Reboot,					MESG_TEXT_ICON_292_Reboot},
	{ICON_RestoreFactorySettings,		MESG_TEXT_ICON_RestoreFactorySettings},
	{ICON_053_CardManagement,			MESG_TEXT_ICON_CardManagement},

};
const unsigned char onlineReboot2SetNum0 = sizeof(onlineReboot2Set0)/sizeof(onlineReboot2Set0[0]);

const IconAndText_t onlineReboot2Set1[] = 
{
	{ICON_292_Reboot,					MESG_TEXT_ICON_292_Reboot},
	{ICON_RestoreFactorySettings,		MESG_TEXT_ICON_RestoreFactorySettings},
	{ICON_CertCreate,					MESG_TEXT_CertCreate},
	//{ICON_CertView,						MESG_TEXT_CertView},
	//{ICON_CertLog,						MESG_TEXT_CertLog},

};
const unsigned char onlineReboot2SetNum1 = sizeof(onlineReboot2Set1)/sizeof(onlineReboot2Set1[0]);

const IconAndText_t onlineReboot2Set2[] = 
{
	{ICON_292_Reboot,					MESG_TEXT_ICON_292_Reboot},
	{ICON_RestoreFactorySettings,		MESG_TEXT_ICON_RestoreFactorySettings},
	{ICON_CertDelete,					MESG_TEXT_CertDelete},
	{ICON_CertView,						MESG_TEXT_CertView},
	{ICON_CertLog,						MESG_TEXT_CertLog},

};
const unsigned char onlineReboot2SetNum2 = sizeof(onlineReboot2Set2)/sizeof(onlineReboot2Set2[0]);

IconAndText_t *onlineReboot2Set = NULL;
int onlineReboot2SetNum;

static void DisplayOnlineReboot2PageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
	char display[20] = {0};	
	for(i = 0; i < OnlineReboot2_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*OnlineReboot2_ICON_MAX+i < onlineReboot2SetNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, onlineReboot2Set[i+page*OnlineReboot2_ICON_MAX].iConText, 1, val_x - x);
			
		}
	}
	pageNum = onlineReboot2SetNum/OnlineReboot2_ICON_MAX + (onlineReboot2SetNum%OnlineReboot2_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
} 

void MENU_147_OnlineReboot2_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_050_Reboot);
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, GetOnlineManageTitle(), strlen(GetOnlineManageTitle()), 1, STR_UTF8, 0);
	onlineReboot2IconSelect = 0;
	onlineReboot2PageSelect = 0;
	reboot2Confirm = 0;
	reboot2ConfirmSelect = 0;
	#if 0
	certState = 0;
	cJSON* pbCert = API_GetRemotePb(GetSearchOnlineIp(), "CERT_State");
	if(pbCert)
	{
		if(!strcmp(cJSON_GetObjectItemCaseSensitive(pbCert, "CERT_State")->valuestring, "Delivered"))
		{
			certState = 1;
			onlineReboot2Set = onlineReboot2Set2;
			onlineReboot2SetNum = onlineReboot2SetNum2;
		}
		else
		{
			onlineReboot2Set = onlineReboot2Set1;
			onlineReboot2SetNum = onlineReboot2SetNum1;
		}
	}
	else
	{
		onlineReboot2Set = onlineReboot2Set0;
		onlineReboot2SetNum = onlineReboot2SetNum0;
	}
	cJSON_Delete(pbCert);
	#endif
	onlineReboot2Set = onlineReboot2Set0;
	onlineReboot2SetNum = onlineReboot2SetNum0;
	DisplayOnlineReboot2PageIcon(0);
}

void MENU_147_OnlineReboot2_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_050_Reboot);
	AutoPowerOffReset();
}

void Send_RebootEvent(void)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventReboot");
	cJSON_AddStringToObject(send_event, "SOURCE",GetSysVerInfo_name());
	cJSON_AddStringToObject(send_event, "IX_ADDR",GetSysVerInfo_IP());
	API_XD_EventJson(GetSearchOnlineIp(),send_event);
	cJSON_Delete(send_event);
}
void Send_RestoreEvent(void)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventFactoryDefault");
	cJSON_AddStringToObject(send_event, "SOURCE",GetSysVerInfo_name());
	cJSON_AddStringToObject(send_event, "IX_ADDR",GetSysVerInfo_IP());
	API_XD_EventJson(GetSearchOnlineIp(),send_event);
	cJSON_Delete(send_event);
}

void MENU_147_OnlineReboot2_Process(void* arg)
{	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					break;
					
				case KEY_DOWN:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:

					onlineReboot2IconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(onlineReboot2PageSelect*OnlineReboot2_ICON_MAX+onlineReboot2IconSelect >= onlineReboot2SetNum)
					{
						return;
					}

					switch(onlineReboot2Set[onlineReboot2PageSelect*OnlineReboot2_ICON_MAX+onlineReboot2IconSelect].iCon)
					{
						case ICON_292_Reboot:
							if(ConfirmSelect(&reboot2Confirm, &reboot2ConfirmSelect, &onlineReboot2IconSelect, onlineReboot2PageSelect, OnlineReboot2_ICON_MAX))
							{
								return;
							}
							Send_RebootEvent();
							break;
						case ICON_RestoreFactorySettings:
							if(ConfirmSelect(&reboot2Confirm, &reboot2ConfirmSelect, &onlineReboot2IconSelect, onlineReboot2PageSelect, OnlineReboot2_ICON_MAX))
							{
								return;
							}
							Send_RestoreEvent();
							break;
						case ICON_053_CardManagement:
							StartInitOneMenu(MENU_158_OnlineCardManage,0,1);
							break;	
						case ICON_CertCreate:
							ClearConfirm(&reboot2Confirm, &reboot2ConfirmSelect, OnlineReboot2_ICON_MAX);

							break;
						case ICON_CertView:
							ClearConfirm(&reboot2Confirm, &reboot2ConfirmSelect, OnlineReboot2_ICON_MAX);

							break;		
						case ICON_CertLog:
							ClearConfirm(&reboot2Confirm, &reboot2ConfirmSelect, OnlineReboot2_ICON_MAX);

							break;		
						case ICON_CertDelete:
							if(ConfirmSelect(&reboot2Confirm, &reboot2ConfirmSelect, &onlineReboot2IconSelect, onlineReboot2PageSelect, OnlineReboot2_ICON_MAX))
							{
								return;
							}
							
							break;		
					}
						
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WaitingRemoteDeviceRebootTimeCnt:
				
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


