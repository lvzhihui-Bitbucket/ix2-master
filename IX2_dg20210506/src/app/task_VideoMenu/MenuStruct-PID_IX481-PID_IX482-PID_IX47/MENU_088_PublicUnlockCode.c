#include "MENU_public.h"
#include "obj_SearchIpByFilter.h"


#define DS_PUB_UNLOCK_CODE_IO_ID	"1095"

#define	PUB_UNLOCK_CODE_ICON_MAX		5
int pub_unlock_code_icon_sel;
int pub_unlock_code_page;

static SearchIpRspData onLineDsListData;


static void DisplayPublicUnlokCodePageIcon(int page)
{
	int i, index;
	uint16 x, y;
	int pageNum;
	char *pos1, *pos2;
	POS pos;
	SIZE hv;

	if(onLineDsListData.deviceCnt == 0)
	{
		return;
	}
	
	for(i = 0; i < PUB_UNLOCK_CODE_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		index = page*PUB_UNLOCK_CODE_ICON_MAX+i;
		if(index < onLineDsListData.deviceCnt)
		{
			char display[50];
			char room[15] = {0};
			int len;
			// zfz_20190605 strart
			//SearchRoomNbrToDisplay(onLineDsListData.data[index].deviceType, onLineDsListData.data[index].BD_RM_MS, room);

			//snprintf(display, 50, "%s%s %s",DeviceTypeToString(onLineDsListData.data[index].deviceType),room,onLineDsListData.data[index].name);
			get_device_addr_and_name_disp_str(0,onLineDsListData.data[index].BD_RM_MS,NULL,NULL,onLineDsListData.data[index].name,display);
			// end
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, hv.h-x);

			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			//dh_20190821_s
			char enQuery[5] = {0};
			sprintf(display, "<ID=%s>", USER_QUERY_ENABLE);
			if( API_io_server_UDP_to_read_remote(onLineDsListData.data[index].Ip, 0xFFFFFFFF, display) == 0 )
			{
				pos1 = strstr(display, "Value=");
				if(pos1 != NULL)
				{
					pos2 = strchr(pos1, '>');
					if(pos2 != NULL)
					{
						len = ((int)(pos2-pos1))-strlen("Value=");
						memcpy(enQuery, pos1+strlen("Value="), len);
						enQuery[len] = 0;
					}
				}	

			}
			if(atoi(enQuery))
			{
				sprintf(display, "<ID=%s>", DS_PUB_UNLOCK_CODE_IO_ID);
				if( API_io_server_UDP_to_read_remote(onLineDsListData.data[index].Ip, 0xFFFFFFFF, display) == 0 )
				{
					pos1 = strstr(display, "Value=");
					if(pos1 != NULL)
					{
						pos2 = strchr(pos1, '>');
						if(pos2 != NULL)
						{
							len = ((int)(pos2-pos1))-strlen("Value=");
							memcpy(room, pos1+strlen("Value="), len);
							room[len] = 0;
							API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, room, strlen(room), 1, STR_UTF8, bkgd_w-x);
						}
					}				
				}
			}
			else
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, MESG_TEXT_FwUpgrage_ErrInvalidCode, 1, bkgd_w-x);
			}
			//dh_20190821_e
		}
	}
	pageNum = onLineDsListData.deviceCnt/PUB_UNLOCK_CODE_ICON_MAX + (onLineDsListData.deviceCnt%PUB_UNLOCK_CODE_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}



void MENU_088_PublicUnlockCode_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	pub_unlock_code_icon_sel = 0;
	pub_unlock_code_page = 0;

	API_MenuIconDisplaySelectOn(ICON_033_Manager);

	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_PublicUnlockCode,1, 0);
	//DisplaySDCardPageIcon(pub_unlock_code_page);
	BusySpriteDisplay(1);
	API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_DS, SearchDeviceRecommendedWaitingTime, 30, &onLineDsListData);

	DisplayPublicUnlokCodePageIcon(pub_unlock_code_page);
	BusySpriteDisplay(0);
}

void MENU_088_PublicUnlockCode_Exit(void)
{
	
}

void MENU_088_PublicUnlockCode_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[100];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					switch(GetCurIcon())
					{
						//case ICON_307_SdFwUpdate:
						//	Api_FwUpdate_UseSdCard();
						//	break;
							
					}
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case KEY_UP:
					PublicPageDownProcess(&pub_unlock_code_page, PUB_UNLOCK_CODE_ICON_MAX, onLineDsListData.deviceCnt, (DispListPage)DisplayPublicUnlokCodePageIcon);
					break;
				case KEY_DOWN:
					PublicPageUpProcess(&pub_unlock_code_page, PUB_UNLOCK_CODE_ICON_MAX, onLineDsListData.deviceCnt, (DispListPage)DisplayPublicUnlokCodePageIcon);
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					pub_unlock_code_icon_sel = GetCurIcon() - ICON_007_PublicList1;
//					if(pub_unlock_code_page*PUB_UNLOCK_CODE_ICON_MAX+pub_unlock_code_icon_sel >= sdCardIconNum)
//					{
//						return;
//					}

					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


