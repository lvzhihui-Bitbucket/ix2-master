
#ifndef _MenuStruct_Resource_H
#define _MenuStruct_Resource_H


typedef enum
{
	MENU_000_RETURN = 0, // �����menu��������
    MENU_001_MAIN,
	MENU_002_MONITOR,
	MENU_003_INTERCOM,
	MENU_004_CALL_RECORD,
	MENU_005_CALL_SCENE,
	MENU_006_CALL_TUNE,
	MENU_007_SET_GENERAL,
	MENU_008_SET_INSTALLER,
	MENU_009_SIP_CONFIG,
	MENU_010_ABOUT,
	MENU_011_SETTING,
	MENU_012_PUBLIC_SETTING,
	MENU_013_CALLING,
	MENU_014_NAMELIST,
	MENU_015_INSTALL_SUB,
	MENU_016_INSTALL_IP,
	MENU_017_KEYPAD_CHAR,
	MENU_018_KEYPAD_NUM,
	MENU_019_KEYPAD,
	MENU_020_INSTALL_CALL_NUM,
	MENU_021_ONSITE_TOOLS,
	MENU_022_ONLINE_MANAGE,
	MENU_023_CALL_RECORD_LIST,
	MENU_024_VIDEO_RECORD_LIST,
	MENU_025_SEARCH_ONLINE,
	MENU_026_PLAYBACK,
	MENU_027_CALLING2,	//czn_20170602
	MENU_028_MONITOR2,
	MENU_029_DATE_TIME_SET,
	MENU_030_SHORTCUT_SET,
	MENU_031_CALLING3,
	MENU_032_CALLING4,
	MENU_033_ONLINE_MANAGE_CALL_NBR,
	MENU_034_ONLINE_MANAGE_INFO,
	MENU_035_LIST_MANAGE,	
	MENU_036_LIST_EDIT,
	MENU_037_MS_LIST,
	MENU_038_ONLINE_MANAGE_REBOOT,


	MENU_039_DIP_HELP,
	MENU_040_SD_CARD_INFO,
	MENU_041_SLAVE_INFO,	
	MENU_042_PUBLIC_SETTING,	
	MENU_043_PUBLIC_EMPTY,	
	MENU_044_VIDEO_ADJUST,	
	MENU_045_SD_CARD,
	MENU_046_RESTORE_TO_DEFAULT,
	MENU_047_PHONE_MANAGE,
	MENU_048_DOORBELL,	//czn_20170808
	MENU_049_PRODUCTION_TEST,
	MENU_050_FW_UPDATE,
	MENU_051_MOVEMENT_TEST,
	MENU_052_SYSTEM_SETTING,
	MENU_053_DIVERT,
	MENU_054_IPC_SETTING,
	MENU_055_IPC_LOGIN,
	MENU_056_IPC_MONITOR,
	MENU_057_IPC_LIST,
	MENU_058_IPC_MANAGE,
	MENU_059_MON_LIST_MANAGE,
	MENU_060_IPC_SEARCHING,

	MENU_061_SipTools,
	MENU_062_SipCallTest,
	MENU_063_SipCallStatistics,
	MENU_064_SipCallLogs,
	MENU_065_InternetTime,	//cao_20181025

	MENU_066_FwUpgrade,	//czn_20181219
	MENU_067_FwUpgradeServer,
	MENU_068_DeviceRemoteManage,
	MENU_069_OnlineFwUpgrade,
	MENU_070_BackFwUpgrade,
	
	MENU_071_ParaManage,
	MENU_072_OnePara,
	MENU_073_ParaPublicComboBox,
	MENU_074_RemoteParaManage,
	MENU_075_OneRemotePara,
	MENU_076_RemoteParaGroup,
	MENU_077_ParaGroup,
	MENU_078_AutoCallbackList,		//czn_20190216
	MENU_079_ExternalUnit,	
	MENU_080_IM_Extensions,	
	MENU_081_OutdoorStations,	
	MENU_082_AutoSetupWizard,	
	MENU_083_Autotest,		//czn_20190412
	MENU_084_AutotestSource,
	MENU_085_AutotestLogs,
	MENU_086_CallTestStat,
	MENU_087_AutotestTools,
	MENU_088_PublicUnlockCode,
	MENU_089_SoftwareUpdate,
	MENU_090_CardManage,

	MENU_091_RES_Select,
	MENU_092_ResSyncSelect,
	MENU_093_ResInfo,
	MENU_094_ResView,
	MENU_095_AddressHealthCheck,
	
	MENU_096_SelectSystemType,
	MENU_097_BatCallNbrSet,
	MENU_098_RES_Manger,
	MENU_099_BACKUP_AND_RESTORE,

	
	MENU_100_CardList,
	MENU_101_CardAdd,
	MENU_102_CardDelAll,
	MENU_103_CardInfo,
	MENU_104_BackupAndRestore2,
	MENU_105_BackupList,
	MENU_106_RestoreList,
	MENU_107_Manager,
	MENU_108_QuickAccess,
	MENU_109_Info,
	MENU_110_CALL_IPC,
	MENU_111_VIDEO_PROXY_SET,
	MENU_112_GSList,		
	MENU_113_MonQuart,		
	MENU_114_WlanSetting,		
	MENU_115_Wlan_IPC_List,
	MENU_116_MultiRec,
	MENU_117_RecQuad,
	MENU_118_IpcScenario,		
	MENU_119_IpcRecord,
	MENU_120_BackResDownload = 120,	
	MENU_121_UdpResDownload,
	MENU_122_DvrPlay,
	MENU_123_DvrSchedule,
	MENU_124_WeekSelect,
	MENU_125_NM_MAIN,
	MENU_126_NM_LanSetting,
	MENU_127_NM_WlanSetting,
	MENU_128_NM_WifiInfo,
	MENU_129_RestoreSelective,
	MENU_130_FileList,
	MENU_131_ViewFile,
	MENU_132_SEARCH_TEST,
	MENU_133_RlcRelayMapSetting,
	MENU_134_RlcRelayOptionSetting,
	MENU_135_DeviceFloorSetting,
	MENU_136_MulLangKeyPad,
	MENU_137_MulLangKeyPad_Num,
	MENU_138_ModuleSelect,
	MENU_139_KeySelect,
	MENU_140_KeyConfig,
	MENU_141_ListenTalk,
	MENU_142_DX_MONITOR,
	MENU_143_ix611PwdList,
	MENU_144_ix611PwdEdit,
	MENU_145_PwdUnlockManager,
	MENU_146_OnlineFwUpgrade2,
	MENU_147_OnlineReboot2,
	MENU_148_OnlineParameter2,

	MENU_149_Arming,
	MENU_150_Disarming,
	MENU_151_AlarmInformService,
	MENU_152_JsonParaSettingPublic,
	MENU_153_DisarmingSubKeyPad,
	MENU_154_DisarmingSubZoneDisp,
	MENU_155_ix611UnlockRecord,
	MENU_156_PLAYBACK2,
	MENU_157_CertSetup,
	MENU_158_OnlineCardManage,
	MENU_159_ix622CardList,

    MENU_DAT_MAX,
    MENU_255_NONE = 0xff,
} FuncMenuType;

void MENU_001_MAIN_Init(int uMenuCnt);
void MENU_001_MAIN_Exit(void);
void MENU_001_MAIN_Process(void* arg);

void MENU_002_Monitor_Init(int uMenuCnt);
void MENU_002_Monitor_Exit(void);
void MENU_002_Monitor_Process(void* arg);

void MENU_003_Intercom_Init(int uMenuCnt);
void MENU_003_Intercom_Exit(void);
void MENU_003_Intercom_Process(void* arg);

void MENU_004_CallRecord_Init(int uMenuCnt);
void MENU_004_CallRecord_Exit(void);
void MENU_004_CallRecord_Process(void* arg);

void MENU_005_CallScene_Init(int uMenuCnt);
void MENU_005_CallScene_Exit(void);
void MENU_005_CallScene_Process(void * arg);

void MENU_006_SetCallTune_Init(int uMenuCnt);
void MENU_006_SetCallTune_Exit(void);
void MENU_006_SetCallTune_Process(void* arg);

void MENU_007_SetGeneral_Init(int uMenuCnt);
void MENU_007_SetGeneral_Exit(void);
void MENU_007_SetGeneral_Process(void* arg);

void MENU_008_SetInstaller_Init(int uMenuCnt);
void MENU_008_SetInstaller_Exit(void);
void MENU_008_SetInstaller_Process(void* arg);

void MENU_009_SipConfig_Init(int uMenuCnt);
void MENU_009_SipConfig_Exit(void);
void MENU_009_SipConfig_Process(void* arg);

void MENU_010_About_Init(int uMenuCnt);
void MENU_010_About_Exit(void);
void MENU_010_About_Process(void* arg);


void MENU_011_Settings_Init(int uMenuCnt);
void MENU_011_Settings_Exit(void);
void MENU_011_Settings_Process(void * arg);

void MENU_012_PublicSetting_Init(int uMenuCnt);
void MENU_012_PublicSetting_Exit(void);
void MENU_012_PublicSetting_Process(void * arg);

void MENU_013_Calling_Init(int uMenuCnt);
void MENU_013_Calling_Exit(void);
void MENU_013_Calling_Process(void* arg);

void MENU_014_NameList_Init(int uMenuCnt);
void MENU_014_NameList_Exit(void);
void MENU_014_NameList_Process(void* arg);

void MENU_015_InstallSub_Init(int uMenuCnt);
void MENU_015_InstallSub_Exit(void);
void MENU_015_InstallSub_Process(void* arg);

void MENU_016_WifiInformation_Init(int uMenuCnt);
void MENU_016_WifiInformation_Exit(void);
void MENU_016_WifiInformation_Process(void* arg);

void MENU_019_Keypad_Init(int uMenuCnt);
void MENU_019_Keypad_Exit(void);
void MENU_019_Keypad_Process(void* arg);

void MENU_020_InstallCallNum_Init(int uMenuCnt);
void MENU_020_InstallCallNum_Exit(void);
void MENU_020_InstallCallNum_Process(void* arg);

void MENU_021_OnsiteTools_Init(int uMenuCnt);
void MENU_021_OnsiteTools_Exit(void);
void MENU_021_OnsiteTools_Process(void* arg);

void MENU_022_OnlineManage_Init(int uMenuCnt);
void MENU_022_OnlineManage_Exit(void);
void MENU_022_OnlineManage_Process(void* arg);

void MENU_023_CallRecordList_Init(int uMenuCnt);
void MENU_023_CallRecordList_Exit(void);
void MENU_023_CallRecordList_Process(void* arg);

void MENU_024_VideoRecordList_Init(int uMenuCnt);
void MENU_024_VideoRecordList_Exit(void);
void MENU_024_VideoRecordList_Process(void* arg);

void MENU_025_SearchOnline_Init(int uMenuCnt);
void MENU_025_SearchOnline_Exit(void);
void MENU_025_SearchOnline_Process(void* arg);

void MENU_026_PLAYBACK_Init(int uMenuCnt);
void MENU_026_PLAYBACK_Exit(void);
void MENU_026_PLAYBACK_Process(void* arg);

void MENU_027_Calling2_Init(int uMenuCnt);
void MENU_027_Calling2_Exit(void);
void MENU_027_Calling2_Process(void* arg);

void MENU_028_Monitor2_Init(int uMenuCnt);
void MENU_028_Monitor2_Exit(void);
void MENU_028_Monitor2_Process(void * arg);

void MENU_029_SetDateTime_Init(int uMenuCnt);
void MENU_029_SetDateTime_Exit(void);
void MENU_029_SetDateTime_Process(void * arg);

void MENU_030_ShortcutSet_Init(int uMenuCnt);
void MENU_030_ShortcutSet_Exit(void);
void MENU_030_ShortcutSet_Process(void * arg);



void MENU_033_OnlineManageCallNbr_Init(int uMenuCnt);
void MENU_033_OnlineManageCallNbr_Exit(void);
void MENU_033_OnlineManageCallNbr_Process(void * arg);

void MENU_034_OnlineManage_Init(int uMenuCnt);
void MENU_034_OnlineManage_Exit(void);
void MENU_034_OnlineManage_Process(void * arg);

void MENU_035_ListManage_Init(int uMenuCnt);
void MENU_035_ListManage_Exit(void);
void MENU_035_ListManage_Process(void * arg);

void MENU_036_ListEdit_Init(int uMenuCnt);
void MENU_036_ListEdit_Exit(void);
void MENU_036_ListEdit_Process(void * arg);

void MENU_037_MSList_Init(int uMenuCnt);
void MENU_037_MSList_Exit(void);
void MENU_037_MSList_Process(void * arg);

void MENU_038_OnlineManageReboot_Init(int uMenuCnt);
void MENU_038_OnlineManageReboot_Exit(void);
void MENU_038_OnlineManageReboot_Process(void * arg);

void MENU_039_DIP_Help_Init(int uMenuCnt);
void MENU_039_DIP_Help_Exit(void);
void MENU_039_DIP_Help_Process(void * arg);



void MENU_040_SDCardInfo_Init(int uMenuCnt);
void MENU_040_SDCardInfo_Exit(void);
void MENU_040_SDCardInfo_Process(void * arg);

void MENU_041_SalveInfo_Init(int uMenuCnt);
void MENU_041_SalveInfo_Exit(void);
void MENU_041_SalveInfo_Process(void * arg);

void MENU_042_PublicSetting_Init(int uMenuCnt);
void MENU_042_PublicSetting_Exit(void);
void MENU_042_PublicSetting_Process(void * arg);

void MENU_044_VideoAdjust_Init(int uMenuCnt);
void MENU_044_VideoAdjust_Exit(void);
void MENU_044_VideoAdjust_Process(void * arg);

void MENU_045_SDCard_Init(int uMenuCnt);
void MENU_045_SDCard_Exit(void);
void MENU_045_SDCard_Process(void * arg);

void MENU_046_Restore_Init(int uMenuCnt);
void MENU_046_Restore_Exit(void);
void MENU_046_Restore_Process(void * arg);

void MENU_047_PhoneManage_Init(int uMenuCnt);
void MENU_047_PhoneManage_Exit(void);
void MENU_047_PhoneManage_Process(void * arg);

void MENU_048_DoorBell_Init(int uMenuCnt);
void MENU_048_DoorBell_Exit(void);
void MENU_048_DoorBell_Process(void * arg);

void MENU_049_ProductionTest_Init(int uMenuCnt);
void MENU_049_ProductionTest_Exit(void);
void MENU_049_ProductionTest_Process(void * arg);

void MENU_050_FW_Update_Init(int uMenuCnt);
void MENU_050_FW_Update_Exit(void);
void MENU_050_FW_Update_Process(void * arg);

void MENU_051_MovementTest_Init(int uMenuCnt);
void MENU_051_MovementTest_Exit(void);
void MENU_051_MovementTest_Process(void * arg);

void MENU_052_SystemSetting_Init(int uMenuCnt);
void MENU_052_SystemSetting_Exit(void);
void MENU_052_SystemSetting_Process(void * arg);

void MENU_053_Divert_Init(int uMenuCnt);
void MENU_053_Divert_Exit(void);
void MENU_053_Divert_Process(void * arg);

void MENU_054_IPC_Setting_Init(int uMenuCnt);
void MENU_054_IPC_Setting_Exit(void);
void MENU_054_IPC_Setting_Process(void * arg);

void MENU_055_IPC_Login_Init(int uMenuCnt);
void MENU_055_IPC_Login_Exit(void);
void MENU_055_IPC_Login_Process(void * arg);

void MENU_056_IPC_Monitor_Init(int uMenuCnt);
void MENU_056_IPC_Monitor_Exit(void);
void MENU_056_IPC_Monitor_Process(void * arg);

void MENU_057_IPC_List_Init(int uMenuCnt);
void MENU_057_IPC_List_Exit(void);
void MENU_057_IPC_List_Process(void * arg);

void MENU_058_IPC_Manage_Init(int uMenuCnt);
void MENU_058_IPC_Manage_Exit(void);
void MENU_058_IPC_Manage_Process(void * arg);

void MENU_059_monListManage_Init(int uMenuCnt);
void MENU_059_monListManage_Exit(void);
void MENU_059_monListManage_Process(void * arg);

void MENU_060_IPC_Searching_Init(int uMenuCnt);
void MENU_060_IPC_Searching_Exit(void);
void MENU_060_IPC_Searching_Process(void * arg);

void MENU_065_InternetTime_Init(int uMenuCnt);
void MENU_065_InternetTime_Exit(void);
void MENU_065_InternetTime_Process(void * arg);

//czn_20181219_s
void MENU_066_FwUpgrade_Init(int uMenuCnt);
void MENU_066_FwUpgrade_Exit(void);
void MENU_066_FwUpgrade_Process(void* arg);

void MENU_067_FwUpgradeServer_Init(int uMenuCnt);
void MENU_067_FwUpgradeServer_Exit(void);
void MENU_067_FwUpgradeServer_Process(void* arg);
//czn_20181219_e

void MENU_069_OnlineFwUpgrade_Init(int uMenuCnt);
void MENU_069_OnlineFwUpgrade_Exit(void);
void MENU_069_OnlineFwUpgrade_Process(void* arg);

void MENU_070_BackFwUpgrade_Init(int uMenuCnt);
void MENU_070_BackFwUpgrade_Exit(void);
void MENU_070_BackFwUpgrade_Process(void* arg);

void MENU_071_ParaManage_Init(int uMenuCnt);
void MENU_071_ParaManage_Exit(void);
void MENU_071_ParaManage_Process(void* arg);

void MENU_072_OnePara_Init(int uMenuCnt);
void MENU_072_OnePara_Exit(void);
void MENU_072_OnePara_Process(void* arg);

void MENU_073_ParaPublic_Init(int uMenuCnt);
void MENU_073_ParaPublic_Exit(void);
void MENU_073_ParaPublic_Process(void* arg);

void MENU_074_RemoteParaManage_Init(int uMenuCnt);
void MENU_074_RemoteParaManage_Exit(void);
void MENU_074_RemoteParaManage_Process(void* arg);

void MENU_075_OneRemotePara_Init(int uMenuCnt);
void MENU_075_OneRemotePara_Exit(void);
void MENU_075_OneRemotePara_Process(void* arg);

void MENU_076_OnlineManageParameter_Init(int uMenuCnt);
void MENU_076_OnlineManageParameter_Exit(void);
void MENU_076_OnlineManageParameter_Process(void* arg);

void MENU_077_ManageParameter_Init(int uMenuCnt);
void MENU_077_ManageParameter_Exit(void);
void MENU_077_ManageParameter_Process(void* arg);

//czn_20190216_s
void MENU_078_AutoCallbackList_Init(int uMenuCnt);
void MENU_078_AutoCallbackList_Exit(void);
void MENU_078_AutoCallbackList_Process(void* arg);
//czn_20190216_e
//���е���Ϣ����

void MENU_079_ExternalUnit_Init(int uMenuCnt);
void MENU_079_ExternalUnit_Exit(void);
void MENU_079_ExternalUnit_Process(void* arg);

void MENU_080_IM_Extension_Init(int uMenuCnt);
void MENU_080_IM_Extension_Exit(void);
void MENU_080_IM_Extension_Process(void* arg);

void MENU_081_OutdoorStation_Init(int uMenuCnt);
void MENU_081_OutdoorStation_Exit(void);
void MENU_081_OutdoorStation_Process(void* arg);

void MENU_082_AutoSetupWizard_Init(int uMenuCnt);
void MENU_082_AutoSetupWizard_Exit(void);
void MENU_082_AutoSetupWizard_Process(void* arg);

//czn_20190412_s
void MENU_083_Autotest_Init(int uMenuCnt);
void MENU_083_Autotest_Exit(void);
void MENU_083_Autotest_Process(void* arg);

void MENU_084_AutotestDevSelect_Init(int uMenuCnt);
void MENU_084_AutotestDevSelect_Exit(void);
void MENU_084_AutotestDevSelect_Process(void* arg);

void MENU_085_AutotestLogView_Init(int uMenuCnt);
void MENU_085_AutotestLogView_Exit(void);
void MENU_085_AutotestLogView_Process(void* arg);

void MENU_086_CallTestStat_Init(int uMenuCnt);
void MENU_086_CallTestStat_Exit(void);
void MENU_086_CallTestStat_Process(void* arg);

void MENU_087_AutoTestTools_Init(int uMenuCnt);
void MENU_087_AutoTestTools_Exit(void);
void MENU_087_AutoTestTools_Process(void* arg);

void MENU_088_PublicUnlockCode_Init(int uMenuCnt);
void MENU_088_PublicUnlockCode_Exit(void);
void MENU_088_PublicUnlockCode_Process(void* arg);

void MENU_089_SwUpgrade_Init(int uMenuCnt);			//czn_20190506
void MENU_089_SwUpgrade_Exit(void);
void MENU_089_SwUpgrade_Process(void* arg);

void MENU_090_CardManage_Init(int uMenuCnt);			//czn_20190506
void MENU_090_CardManage_Exit(void);
void MENU_090_CardManage_Process(void* arg);

void MENU_091_ResSelect_Init(int uMenuCnt);
void MENU_091_ResSelect_Exit(void);
void MENU_091_ResSelect_Process(void* arg);

void MENU_092_ResSync_Init(int uMenuCnt);
void MENU_092_ResSync_Exit(void);
void MENU_092_ResSync_Process(void* arg);

void MENU_093_ResInfo_Init(int uMenuCnt);
void MENU_093_ResInfo_Exit(void);
void MENU_093_ResInfo_Process(void* arg);

void MENU_094_ResView_Init(int uMenuCnt);
void MENU_094_ResView_Exit(void);
void MENU_094_ResView_Process(void* arg);

void MENU_095_AddressHealthCheck_Init(int uMenuCnt);			//czn_20190506
void MENU_095_AddressHealthCheck_Exit(void);
void MENU_095_AddressHealthCheck_Process(void* arg);

void MENU_096_SelectSystemType_Init(int uMenuCnt);
void MENU_096_SelectSystemType_Exit(void);
void MENU_096_SelectSystemType_Process(void* arg);

void MENU_097_IM_Call_Nbr_Init(int uMenuCnt);
void MENU_097_IM_Call_Nbr_Exit(void);
void MENU_097_IM_Call_Nbr_Process(void* arg);

void MENU_098_RES_Manger_Init(int uMenuCnt);
void MENU_098_RES_Manger_Exit(void);
void MENU_098_RES_Manger_Process(void* arg);

void MENU_099_BackupAndRestore_Init(int uMenuCnt);
void MENU_099_BackupAndRestore_Exit(void);
void MENU_099_BackupAndRestore_Process(void* arg);
void MENU_104_BackupAndRestore2_Init(int uMenuCnt);
void MENU_104_BackupAndRestore2_Exit(void);
void MENU_104_BackupAndRestore2_Process(void* arg);

void MENU_105_backupList_Init(int uMenuCnt);
void MENU_105_backupList_Exit(void);
void MENU_105_backupList_Process(void* arg);

void MENU_106_RestoreList_Init(int uMenuCnt);
void MENU_106_RestoreList_Exit(void);
void MENU_106_RestoreList_Process(void* arg);

void MENU_108_QuickAccess_Init(int uMenuCnt);
void MENU_108_QuickAccess_Exit(void);
void MENU_108_QuickAccess_Process(void* arg);

void MENU_109_PLAYBACK_INFO_Init(int uMenuCnt);
void MENU_109_PLAYBACK_INFO_Exit(void);
void MENU_109_PLAYBACK_INFO_Process(void* arg);

void MENU_110_CallIPC_Init(int uMenuCnt);
void MENU_110_CallIPC_Exit(void);
void MENU_110_CallIPC_Process(void* arg);

void MENU_111_VideoProxySetting_Init(int uMenuCnt);
void MENU_111_VideoProxySetting_Exit(void);
void MENU_111_VideoProxySetting_Process(void* arg);
void MENU_112_GSList_Init(int uMenuCnt);		
void MENU_112_GSList_Exit(void);
void MENU_112_GSList_Process(void* arg);

void MENU_113_MonQuart_Init(int uMenuCnt);		
void MENU_113_MonQuart_Exit(void);
void MENU_113_MonQuart_Process(void* arg);

void MENU_114_WlanSetting_Init(int uMenuCnt);		
void MENU_114_WlanSetting_Exit(void);
void MENU_114_WlanSetting_Process(void* arg);

void MENU_115_Wlan_IPC_List_Init(int uMenuCnt);		
void MENU_115_Wlan_IPC_List_Exit(void);
void MENU_115_Wlan_IPC_List_Process(void* arg);

void MENU_116_MultiRec_Init(int uMenuCnt);		
void MENU_116_MultiRec_Exit(void);
void MENU_116_MultiRec_Process(void* arg);

void MENU_117_RecQuad_Init(int uMenuCnt);		
void MENU_117_RecQuad_Exit(void);
void MENU_117_RecQuad_Process(void* arg);

void MENU_118_IpcScenario_Init(int uMenuCnt);		
void MENU_118_IpcScenario_Exit(void);
void MENU_118_IpcScenario_Process(void* arg);

void MENU_119_IpcRecord_Init(int uMenuCnt);		
void MENU_119_IpcRecord_Exit(void);
void MENU_119_IpcRecord_Process(void* arg);

void MENU_120_BackResDownload_Init(int uMenuCnt);		
void MENU_120_BackResDownload_Exit(void);
void MENU_120_BackResDownload_Process(void* arg);

void MENU_121_ResUdpDownload_Init(int uMenuCnt);		
void MENU_121_ResUdpDownload_Exit(void);
void MENU_121_ResUdpDownload_Process(void* arg);

void MENU_122_DvrPlay_Init(int uMenuCnt);		
void MENU_122_DvrPlay_Exit(void);
void MENU_122_DvrPlay_Process(void* arg);

void MENU_123_RecSchedule_Init(int uMenuCnt);		
void MENU_123_RecSchedule_Exit(void);
void MENU_123_RecSchedule_Process(void* arg);

void MENU_124_WeekSelect_Init(int uMenuCnt);		
void MENU_124_WeekSelect_Exit(void);
void MENU_124_WeekSelect_Process(void* arg);

void MENU_NM_Main_Init(int uMenuCnt);		
void MENU_NM_Main_Exit(void);
void MENU_NM_Main_Process(void* arg);

void MENU_NM_LanSetting_Init(int uMenuCnt);		
void MENU_NM_LanSetting_Exit(void);
void MENU_NM_LanSetting_Process(void* arg);

void MENU_NM_WlanSetting_Init(int uMenuCnt);		
void MENU_NM_WlanSetting_Exit(void);
void MENU_NM_WlanSetting_Process(void* arg);

void MENU_NM_WifiInformation_Init(int uMenuCnt);		
void MENU_NM_WifiInformation_Exit(void);
void MENU_NM_WifiInformation_Process(void* arg);

void MENU_129_RestoreSelective_Init(int uMenuCnt);		
void MENU_129_RestoreSelective_Exit(void);
void MENU_129_RestoreSelective_Process(void* arg);

void MENU_130_FileList_Init(int uMenuCnt);		
void MENU_130_FileList_Exit(void);
void MENU_130_FileList_Process(void* arg);

void MENU_131_ViewFile_Init(int uMenuCnt);		
void MENU_131_ViewFile_Exit(void);
void MENU_131_ViewFile_Process(void* arg);

void MENU_132_SearchTest_Init(int uMenuCnt);		
void MENU_132_SearchTest_Exit(void);
void MENU_132_SearchTest_Process(void* arg);

void MENU_133_RlcRelayMapSetting_Init(int uMenuCnt);		
void MENU_133_RlcRelayMapSetting_Exit(void);
void MENU_133_RlcRelayMapSetting_Process(void* arg);

void MENU_134_RlcRelayOptionSetting_Init(int uMenuCnt);		
void MENU_134_RlcRelayOptionSetting_Exit(void);
void MENU_134_RlcRelayOptionSetting_Process(void* arg);

void MENU_135_DeviceFloorSetting_Init(int uMenuCnt);		
void MENU_135_DeviceFloorSetting_Exit(void);
void MENU_135_DeviceFloorSetting_Process(void* arg);

void MENU_136_MulLangKeypad_Init(int uMenuCnt);		
void MENU_136_MulLangKeypad_Exit(void);
void MENU_136_MulLangKeypad_Process(void* arg);	

void MENU_138_ExtModuleSelect_Init(int uMenuCnt);		
void MENU_138_ExtModuleSelect_Exit(void);
void MENU_138_ExtModuleSelect_Process(void* arg);

void MENU_139_KeySelect_Init(int uMenuCnt);		
void MENU_139_KeySelect_Exit(void);
void MENU_139_KeySelect_Process(void* arg);

void MENU_140_KeyConfig_Init(int uMenuCnt);		
void MENU_140_KeyConfig_Exit(void);
void MENU_140_KeyConfig_Process(void* arg);

void MENU_141_ListenTalk_Init(int uMenuCnt);		
void MENU_141_ListenTalk_Exit(void);
void MENU_141_ListenTalk_Process(void* arg);

void MENU_142_DxMonitor_Init(int uMenuCnt);		
void MENU_142_DxMonitor_Exit(void);
void MENU_142_DxMonitor_Process(void* arg);

void MENU_143_ix611PwdList_Init(int uMenuCnt);
void MENU_143_ix611PwdList_Exit(void);
void MENU_143_ix611PwdList_Process(void* arg);
void MENU_144_ix611PwdEdit_Init(int uMenuCnt);
void MENU_144_ix611PwdEdit_Exit(void);
void MENU_144_ix611PwdEdit_Process(void* arg);
void MENU_145_PwdUnlockManager_Init(int uMenuCnt);
void MENU_145_PwdUnlockManager_Exit(void);
void MENU_145_PwdUnlockManager_Process(void* arg);
void MENU_146_OnlineFwUpgrade2_Init(int uMenuCnt);
void MENU_146_OnlineFwUpgrade2_Exit(void);
void MENU_146_OnlineFwUpgrade2_Process(void* arg);
void MENU_147_OnlineReboot2_Init(int uMenuCnt);
void MENU_147_OnlineReboot2_Exit(void);
void MENU_147_OnlineReboot2_Process(void* arg);
void MENU_148_OnlineParameter2_Init(int uMenuCnt);
void MENU_148_OnlineParameter2_Exit(void);
void MENU_148_OnlineParameter2_Process(void* arg);

void MENU_149_AlarmingArm_Init(int uMenuCnt);		
void MENU_149_AlarmingArm_Exit(void);
void MENU_149_AlarmingArm_Process(void* arg);

void MENU_150_Disarming_Init(int uMenuCnt);		
void MENU_150_Disarming_Exit(void);
void MENU_150_Disarming_Process(void* arg);

void MENU_151_AlarmInformService_Init(int uMenuCnt);		
void MENU_151_AlarmInformService_Exit(void);
void MENU_151_AlarmInformService_Process(void* arg);

void MENU_152_JsonParaSettingPublic_Init(int uMenuCnt);		
void MENU_152_JsonParaSettingPublic_Exit(void);
void MENU_152_JsonParaSettingPublic_Process(void* arg);

void MENU_155_UnlockRecordList_Init(int uMenuCnt);
void MENU_155_UnlockRecordList_Exit(void);
void MENU_155_UnlockRecordList_Process(void* arg);
void MENU_156_PLAYBACK2_Init(int uMenuCnt);
void MENU_156_PLAYBACK2_Exit(void);
void MENU_156_PLAYBACK2_Process(void* arg);
void MENU_157_CertSetup_Init(int uMenuCnt);
void MENU_157_CertSetup_Exit(void);
void MENU_157_CertSetup_Process(void* arg);
void MENU_158_OnlineCardManage_Init(int uMenuCnt);
void MENU_158_OnlineCardManage_Exit(void);
void MENU_158_OnlineCardManage_Process(void* arg);
void MENU_159_UserCardList_Init(int uMenuCnt);
void MENU_159_UserCardList_Exit(void);
void MENU_159_UserCardList_Process(void* arg);

#define	ICON_001_monitor                1
#define	ICON_002_intercom               2
#define	ICON_003_call_record            3
#define	ICON_004_light                  4
#define	ICON_005_NoDisturb              5
#define	ICON_006_settings               6
#define	ICON_007_PublicList1            7
#define	ICON_008_PublicList2            8
#define	ICON_009_PublicList3            9
#define	ICON_010_PublicList4            10
#define	ICON_011_PublicList5            11
#define	ICON_012_PublicList6           	12
#define	ICON_013_PublicList7            13
#define	ICON_014_PublicList8            14
#define	ICON_015_PublicList9            15
#define	ICON_016_PublicList10           16


#define	ICON_012_namelist               12
#define	ICON_013_InnerCall              13
#define	ICON_014_GuardStation           14
#define	ICON_015_InputNumbers           15
#define	ICON_017_MissedCalls            17
#define	ICON_018_IncomingCalls          18
#define	ICON_019_OutgoingCalls          19
#define	ICON_020_playback               20

#define	ICON_020_NormalUse              20
#define	ICON_021_NoDisbAlways           21
#define	ICON_022_NoDisb1h               22
#define	ICON_023_NoDisb8h               23
#define	ICON_024_CallTune               24
#define	ICON_025_general                25
#define	ICON_026_InstallerSetup         26
#define	ICON_027_wireless				27
#define	ICON_027_SipConfig              27
#define	ICON_028_about                  28
#define	ICON_029_IP_ADDR	            29
#define	ICON_030_CALL_NUMBER            30
#define	ICON_031_OnsiteTools            31
#define	ICON_032_System             	32
#define	ICON_033_Manager             	33	//dh_20190829
#define	ICON_033_ListManage             33
#define	ICON_034_ListView               34
#define	ICON_035_ListAdd				35
#define	ICON_036_ListDelete				36
#define	ICON_037_ListEdit		        37
#define	ICON_038_ListCheck				38			//czn_20190525
#define	ICON_039_RingVolume             39
#define	ICON_039_QRCodeShow             39

#define	ICON_040_AboutPage1          	40
#define	ICON_041_AboutPage2          	41
#define	ICON_042_AboutPage3          	42
#define	ICON_043_AboutPage4          	43
#define	ICON_044_AboutPage5          	44
#define	ICON_044_AboutPage5          	44
#define	ICON_045_Upgrade          		45
#define	ICON_046_Replay          		46
#define	ICON_047_Home          			47
#define	ICON_048_Device          		48
#define	ICON_049_Parameter          	49
#define	ICON_050_Reboot          		50
#define	ICON_051_ExternalUnit        	51
#define	ICON_052_IM_Process        		52
#define	ICON_053_OS_Process        		53
#define	ICON_054_IPC_Process        	54

#define	ICON_040_IntercomCalls          40
#define	ICON_041_DateAndTime            41
#define	ICON_042_CurrentAddress         42
#define	ICON_043_MasterSlaver           43
#define	ICON_044_Parameter         		44
#define	ICON_045_IntercomEnable         45
#define	ICON_046_Reboot         		46
#define	ICON_047_IpAddress		        47
#define	ICON_048_CallNumber           	48
#define	ICON_049_OnsiteTools    	    49
#define	ICON_050_PCUtility         		50
#define	ICON_051_PrivateUnlockCode		51
#define	ICON_052_PublicUnlockCode		52
#define	ICON_053_CardManagement			53

#define	ICON_052_DateMode          		52
#define	ICON_053_TimeMode          		53
#define	ICON_054_Date	          		54
#define	ICON_055_Time	          		55
#define	ICON_056_Language	          	56
#define	ICON_057_MonitorTime          	57
#define	ICON_058_SDCard          		58
#define	ICON_059_RestoreToDefault	    59
#define	ICON_060_SDInformation	        60
#define	ICON_061_CopyImage          	61
#define	ICON_062_Format	          		62

#define	ICON_063_BrightAdjust	          			63		//czn_20170803
#define	ICON_064_ColorAdjust	          			64
#define	ICON_065_VoiceAdjust	          			65
#define	ICON_066_NormalUse							66
#define	ICON_067_NoDisturb8H						67
#define	ICON_068_NoDisturbAlways					68
#define	ICON_069_DivertCallIfNoAnswer				69
#define	ICON_070_DivertCallSimultaneously			70
#define	ICON_071_SipConfig							71
#define	ICON_072_SipServer				72
#define	ICON_073_SipPort				73
#define	ICON_074_SipDivert				74
#define	ICON_075_SipAccount				75
#define	ICON_076_SipPassword			76
#define	ICON_077_RingVolume				77
// zfz_20190601 start
#define	ICON_078_DS_SetTune				78
#define	ICON_079_CDS_SetTune			79
#define	ICON_080_OS_SetTune				80
#define	ICON_081_MSG_SetTune			81
// end

//dh_20190830_s
#define	ICON_QuickAccess1				66
#define	ICON_QuickAccess2				67
#define	ICON_QuickAccess3				68
#define	ICON_QuickAccess4				69
#define	ICON_QuickAccess5				70
#define	ICON_QuickAccess6				71
#define	ICON_QuickAccess7				72
#define	ICON_QuickAccess8				73
#define	ICON_QuickAccess9				74
#define	ICON_QuickAccess10				75
#define	ICON_QuickAccess11				76
#define	ICON_QuickAccess12				77
#define	ICON_QuickAccess13				78
#define	ICON_QuickAccess14				79
#define	ICON_QuickAccess15				80
#define	ICON_QuickAccess16				81
#define	ICON_QuickAccess17				82
#define	ICON_QuickAccess18				83
#define	ICON_QuickAccess19				84
#define	ICON_QuickAccess20				85
#define	ICON_QuickAccess21				86
#define	ICON_QuickAccess22				87
//dh_20190830_e


#define	ICON_082_Recording				82
#define	ICON_083_RestoreGeneralData		83
#define	ICON_084_RestoreUserData		84
#define	ICON_085_RestoreWirelessData	85
#define	ICON_086_RestoreInstallerData	86
#define	ICON_087_RestoreAndBackup		87
#define	ICON_088_Delete					88

#define	ICON_089_DoreBell_SetTune		89
#define	ICON_090_Intercom_SetTune		90
#define	ICON_091_InnerCall_SetTune		91
#define	ICON_092_Alarm_SetTune			92 // zfz_20190601
#define	ICON_093_FW_Upgrade				93


#define	ICON_094_IPAssigned             94
#define	ICON_095_IPAddress              95
#define	ICON_096_IPSubnetMask           96
#define	ICON_097_IPGateway            	97
#define	ICON_098_AddressHealthCheck		98
#define	ICON_099_RingTime               99
#define	ICON_100_SipEnable				100

#define	ICON_101_Key00      101
#define	ICON_102_Key01      102
#define	ICON_103_Key02      103
#define	ICON_104_Key03      104
#define	ICON_105_Key04      105
#define	ICON_106_Key05      106
#define	ICON_107_Key06      107
#define	ICON_108_Key07      108
#define	ICON_109_Key08      109
#define	ICON_110_Key09      110
#define	ICON_111_Key10      111
#define	ICON_112_Key11      112
#define	ICON_113_Key12      113
#define	ICON_114_Key13      114
#define	ICON_115_Key14      115
#define	ICON_116_Key15      116
#define	ICON_117_Key16      117
#define	ICON_118_Key17      118
#define	ICON_119_Key18      119
#define	ICON_120_Key19      120
#define	ICON_121_Key20      121
#define	ICON_122_Key21      122
#define	ICON_123_Key22      123
#define	ICON_124_Key23      124
#define	ICON_125_Key24      125
#define	ICON_126_Key25      126
#define	ICON_127_Key26      127
#define	ICON_128_Key27      128
#define	ICON_129_Key28      129
#define	ICON_130_Key29      130
#define	ICON_131_Key30      131
#define	ICON_132_Key31      132
#define	ICON_133_Key32      133
#define	ICON_134_Key33      134
#define	ICON_135_Key34      135
#define	ICON_136_Key35      136
#define	ICON_137_Key36      137
#define	ICON_138_Key37      138
#define	ICON_139_Key38      139
#define	ICON_140_Key39      140
#define	ICON_141_Key40      141
#define	ICON_142_Key41      142
#define	ICON_143_Key42      143
#define	ICON_144_Key43      144
#define	ICON_145_Key44      145
#define	ICON_146_Key45      146
#define	ICON_147_Key46      147
#define	ICON_148_Key47      148
#define	ICON_149_Key48      149
#define	ICON_150_Key49      150
#define	ICON_151_Key50      151
#define	ICON_152_Key51      152
#define	ICON_153_Key52      153
#define	ICON_154_Key53      154
#define	ICON_155_Key54      155
#define	ICON_156_Key55      156
#define	ICON_157_Key56      157
#define	ICON_158_Key57      158
#define	ICON_159_Key58      159
#define	ICON_160_Key59      160
#define	ICON_161_Key60      161
#define	ICON_162_Key61      162
#define	ICON_163_Key62      163
#define	ICON_164_Key63      164
#define	ICON_165_Key64      165
#define	ICON_166_Key65      166
#define	ICON_167_Key66      167
#define	ICON_168_Key67      168
#define	ICON_169_Key68      169
#define	ICON_170_Key69      170
#define	ICON_171_Key70      171
#define	ICON_172_Key71      172
#define	ICON_173_Key72      173
#define	ICON_174_Key73      174

#define	ICON_175_KeyTitle   175
#define	ICON_176_KeyState   176
#define	ICON_177_KeyBar1    177
#define	ICON_178_KeyBar2    178
#define	ICON_179_Key78      179
#define	ICON_180_KeyEdit	180
#define	ICON_181_MS_List    181		//czn_20191203


#define	ICON_200_Return              	200
#define	ICON_201_PageDown             	201
#define	ICON_202_PageUp             	202
#define	ICON_203_Monitor             	203
#define	ICON_204_quad             		204
#define	ICON_205_CallScene             	205
#define	ICON_206_Namelist              	206
#define	ICON_207_MonitorPageDown        207
#define	ICON_208_MonitorCapture         208
#define	ICON_209_MonitorUnlock1         209
#define	ICON_210_MonitorUnlock2         210
#define	ICON_211_MonitorTalk            211
#define	ICON_212_MonitorReturn          212
#define	ICON_213_Close          		213
#define	ICON_214_Shortcut1          	214
#define	ICON_215_Shortcut2          	215
#define	ICON_216_Shortcut3          	216
#define	ICON_217_Shortcut4          	217
#define	ICON_218_PublicSetTitle         218
#define	ICON_219_MainTime				219
#define	ICON_220_MainScene         		220
#define	ICON_221_MainLogo          		221
#define	ICON_222_MainSip          		222
#define	ICON_223_MainWifi         		223
#define	ICON_224_MainMissed         	224
#define	ICON_224_DipHelpReturn         	224

#define	ICON_222_BarPlay				222
#define	ICON_223_PlaybackFB				223
#define	ICON_224_PlaybackFF				224
#define	ICON_225_PlaybackClose			225
#define	ICON_226_PlaybackDel         	226
#define	ICON_227_PlaybackNext          	227
#define	ICON_228_PlaybackLast          	228
#define	ICON_229_ShortcutSet1         	229
#define	ICON_230_ShortcutSet2         	230
#define	ICON_231_ShortcutSet3         	231
#define	ICON_232_ShortcutSet4         	232
#define	ICON_233_MonitorPageUp        	233
#define	ICON_234_MonitorColor         	234
#define	ICON_235_MonitorBright         	235
#define	ICON_236_MonitorVolume          236
#define	ICON_237_MonitorEmpty			237

#define	ICON_238_MonitorFishEye1		238
#define	ICON_239_MonitorFishEye2		239
#define	ICON_240_MonitorFishEye3		240
#define	ICON_241_MonitorFishEye4		241
#define	ICON_242_MonitorFishEye5		242
#define	ICON_243_MonitorFishEye6		243
#define	ICON_244_MonitorFishEye7		244
#define	ICON_245_MonitorFishEye8		245
#define	ICON_246_MonitorFishEye9		246
#define	ICON_247_MonitorFishEye10		247
#define	ICON_248_MonitorFishEye11		248
#define	ICON_249_MonitorFishEye12		249
#define	ICON_250_MonitorFishEye13		250
#define	ICON_251_MonitorFishEye14		251
#define	ICON_252_MonitorFishEye15		252
#define	ICON_253_MonitorFishEye16		253
#define	ICON_254_ProductionTestCancel	254
#define	ICON_255_ProductionTestConfirm	255
#define	ICON_256_Edit					256
#define	ICON_257_IntercomClose			257
#define	ICON_258_IntercomTalk			258
#define	ICON_259_IPC_Setting			259
#define	ICON_260_IPC_Name				260
#define	ICON_261_IPC_UserName			261
#define	ICON_262_IPC_UserPwd			262
#define	ICON_263_IPC_Channel			263
#define	ICON_264_IPC_Save				264
#define	ICON_265_IPC_Monitor			265
#define	ICON_266_IPC_Edit				266
#define	ICON_267_IPC_Delete				267
#define	ICON_268_IPC_Resolution			268
#define	ICON_269_MonListSelect			269
#define	ICON_270_MS_List              	270

#define	ICON_270_IPC_Login				270
#define	ICON_271_MonListManage			271
#define	ICON_272_IPC_Preview			272
#define	ICON_273_IPC_AddBySearch		273
#define	ICON_274_IPC_AddByManual		274
#define	ICON_275_IPC_List				275
#define	ICON_275_IPC_IpAddr				276
#define	ICON_277_IPC_DHCP				277

#define	ICON_278_Global_Nbr				278
#define	ICON_279_RM_ADDR				279
#define	ICON_280_MS_Number				280
#define	ICON_281_Name					281
#define	ICON_282_Local_Nbr				282

#define	ICON_283_AutoCallBackRequest		283
#define	ICON_284_SearchAndProg				284
#define	ICON_285_ProgCall_NbrByDSCalling	285
#define	ICON_286_AssignCall_NbrToDSButton 	286
#define	ICON_287_RestoreDefaultCall_Nbr 	287

#define	ICON_288_BD_NBR						288
#define	ICON_289_Device						289
#define	ICON_290_Search						290
#define	ICON_291_SearchTime					291

#define	ICON_292_Reboot						292


#define	ICON_293_UpdateAllListBySearch		293
#define	ICON_294_ManageImList				294
#define	ICON_295_ManageDsList				295
#define	ICON_296_ManageMsList				296
#define	ICON_297_ManageIPCList				297
#define	ICON_298_DS_NBR						298
#define	ICON_299_SearchTest					299

#define	ICON_300_ShortcutSetting        300
#define	ICON_301_RemoveShortcutSetting  301
#define	ICON_302_MontorList             302
#define	ICON_303_SipUseDefault			303
#define	ICON_304_RemoteMonCode			304
#define	ICON_305_RemoteCallCode			305
#define	ICON_306_SipDivPwd				306
#define	ICON_307_SdFwUpdate				307
#define	ICON_308_TimeZone				308
#define	ICON_309_TimeAutoUpdate			309
#define	ICON_310_FW_UpdateCode			310
#define	ICON_311_FW_UpdateServer		311
#define	ICON_312_FW_UpdateStart			312
#define	ICON_313_DivertTimeSet			313
#define	ICON_314_ReRegister				314
#define	ICON_315_BroadcastTimeSet		315
#define	ICON_316_MicVolumeSet			316
#define	ICON_317_SpeakerVolumeSet		317
#define	ICON_318_VideoResolution		318
#define	ICON_320_DivertVideoResolution		320
#define	ICON_321_DivertMicVolumeSet			321
#define	ICON_322_DivertSpeakerVolumeSet		322

// 20190521
#define ICON_IdCard_List				332
#define ICON_IdCard_Add					333
#define ICON_IdCard_DelAll				334

#define	ICON_336_InternetTime			336
#define	ICON_337_TimeServer				337
#define	ICON_338_TimeUpdate				338
#define	ICON_340_CallSceneSipTest		340		//czn_20181227
#define	ICON_345_AutoMuteSupport		345		//czn_20181117

//czn_20181219_s
#define	ICON_FwUpgrade_Server				346
#define	ICON_FwUpgrade_DLCode				347
#define	ICON_FwUpgrade_CodeInfo				348
#define	ICON_FwUpgrade_Notice				349
#define	ICON_FwUpgrade_Operation				350
#define	ICON_FwUpgrade				351
//czn_20181219_e
#define	ICON_ParaId					352
#define	ICON_ParaName				353
#define	ICON_ParaDesc				354
#define	ICON_ParaValue				355

#define	ICON_ParaGroup0				356
#define	ICON_ParaGroup1				357
#define	ICON_ParaGroup2				358
#define	ICON_ParaGroup3				359
#define	ICON_ParaGroup4				360
#define	ICON_ParaGroup5				361

#define	ICON_AutoSetupWizard		362
#define	ICON_IM_Extensions			363
#define	ICON_OutdoorStations		364
#define	ICON_IP_Camera				365

//czn_20190412_s
#define	ICON_AutotestSource			366
#define	ICON_AutotestTarget				367
#define	ICON_AutotestCount				368
#define	ICON_AutotestInterval			369
#define	ICON_AutotestStart				370
#define	ICON_Autotest					371
#define 	ICON_AutotestLogClear			372
#define 	ICON_ViewAutotestLog			373
#define 	ICON_AutotestLogCopy			374
#define 	ICON_AutotestTools				375
#define	ICON_AutotestStatistics			376
#define	ICON_EnterCallBtnBinding		377

// zfz_20190601
#define	ICON_CallRecordDelAll			378
#define	ICON_379_Shortcut5				379

//czn_20190412_e
#define	ICON_SelectSystemTypeVS			380
#define	ICON_SelectSystemTypeSS			381
#define	ICON_SelectSystemTypeNS			382
#define	ICON_SelectSystemTypeRestore	383

#define	ICON_UpdateCallNbrByRes			384
#define	ICON_SummaryReport				385
#define ICON_InstallerChecklist			386
#define ICON_RES_TO_SD					387
#define ICON_RES_FROM_SD				388
#define ICON_RES_TO_SYNC				389
#define ICON_RES_FROM_SYNC				390
#define ICON_RES_FORMAT					391
#define ICON_RES_Manger					392
#define ICON_RES_INFO						393
#define ICON_AllDevGotoUpdate				394
#define ICON_BATCH_CALL_NBR				395
#define ICON_BATCH_REBOOT					396
#define ICON_BATCH_PWD					397

#define ICON_BACKUP_RESTORE				398

//dh_20190823_s
#define	ICON_short_list1          	399
#define	ICON_short_list2          	400
#define	ICON_short_list3          	401
#define	ICON_short_list4          	402
#define	ICON_short_list5          	403
#define	ICON_short_list6          	404
#define	ICON_short_list7          	405
#define	ICON_short_list8          	406
#define	ICON_short_list9          	407
#define	ICON_short_list10          	408
#define	ICON_Left_list1          	409
#define	ICON_Left_list2          	410
#define	ICON_Left_list3          	411
#define	ICON_Left_list4          	412
#define	ICON_Left_list5          	413
#define	ICON_Left_list6          	414
#define	ICON_Left_list7          	415
#define	ICON_Left_list8         	416
#define	ICON_Left_list9          	417
#define	ICON_Left_list10          	418
#define	ICON_419_PlaybackInfoNext		419
#define	ICON_420_PlaybackInfoCancel		420
#define	ICON_421_PlaybackInfoConfirm	421

#define	ICON_AutoCloseAfterUnlock		422
#define	ICON_AutoUnlock					423

#define	ICON_CallVideoSelect			424
#define	ICON_IPC_Agent					425
#define	ICON_IX_Agent					426

#define	ICON_ExtModuleS4				427
#define	ICON_ExtModuleMK				428
#define	ICON_ExtModuleDR				429
	
#define	ICON_ExtModuleS4K1				430
#define	ICON_ExtModuleS4K2				431
#define	ICON_ExtModuleS4K3				432
#define	ICON_ExtModuleS4K4				433
	
#define	ICON_ExtKeyCfgNoUse				434
#define	ICON_ExtKeyCfgGL				435
#define	ICON_ExtKeyCfgCancel			436
#define	ICON_ExtKeyCfgInput				437
#define	ICON_ExtKeyCfgImList			438

#define	ICON_TestModeQC0				439
#define	ICON_TestModeQC1				440
#define	ICON_TestModeQC2				441
#define	ICON_TestModeQA1				442
#define	ICON_TestModeQA2				443

#define	ICON_wireless              		444
#define	ICON_SipNetworkSetting			445
#define	ICON_Wlan_IP_Camera				446

#define	ICON_DVR						447
#define	ICON_DVR_QUAD					448
#define	ICON_DVR_REC					449
#define	ICON_DVR_SD						450
#define	ICON_DVR_SCHEDULE				451
#define	ICON_RecordCH1					452
#define	ICON_RecordCH2					453
#define	ICON_RecordCH3					454
#define	ICON_RecordCH4					455
#define	ICON_RecordSetting				456
#define	ICON_RecordStart				457
#define	ICON_IpcScenario				458
#define	ICON_IpcScenario1				459
#define	ICON_IpcScenario2				460
#define	ICON_IpcScenario3				461
#define	ICON_IpcScenario4				462
#define	ICON_IpcScenarioDefault			463
#define	ICON_DVR_TIME_S					464
#define	ICON_DVR_TIME_E					465
#define	ICON_DVR_DATE					466
#define	ICON_SCHEDULE_SET				467
#define	ICON_SCHEDULE_Cancel			468

#define	ICON_RestoreInstallerDefault	469
#define	ICON_RestoreUserDefault			470
#define	ICON_ClearCustomizer			471
#define	ICON_ClearRes					472
#define	ICON_ClearUserData				473
#define	ICON_ClearBackup				474

#define	ICON_ViewCustomizer				475
#define	ICON_ViewRes					476
#define	ICON_ViewUserData				477
#define	ICON_ViewBackup					478

#define ICON_OperationSelect			479
#define ICON_RangeSelect				480
#define ICON_BackupSelect				481
#define ICON_Location					482
#define ICON_Start						483

#define ICON_Backup						484
#define ICON_RestoreFromBackup			485
#define ICON_RestoreFactorySettings		486
#define ICON_RestoreDefaultSelective	487

#define	ICON_RlcRelayMode				488
#define	ICON_RlcRelayActivateTime1		489
#define	ICON_RlcRelayActivateTime2		490
#define	ICON_RlcRelayDelayTime			491

//#define	ICON_RlcRelayMapNum				492
//#define	ICON_RlcRelayMapInitialFloor	493

#define	ICON_PublicTime					494
#define	ICON_PublicFloor				495
#define	ICON_PrivateTime				496
#define	ICON_PrivateFloor				497
#define	ICON_RlcRelayMapNum				498
#define	ICON_RlcRelayMapInitialFloor	499
#define	ICON_RlcPublicTime				500
#define	ICON_RlcPrivateTime				501
#define	ICON_CallliftEn					502
#define	ICON_CallliftTime				503
#define	ICON_CallliftFloor				504
#define	ICON_RlcCallliftTime			505
#define	ICON_Overview					506
#define	ICON_EditName_Enable			507
#define	ICON_KeyBeepCtrl				508
#define	ICON_MonitorDoorStation			509
#define 	ICON_DoorbellVideo				510
#define 	ICON_BabyroomEn				511

#define	ICON_DivertToDev					446

#define	ICON_ExitProg					601		//ͼ����ڰ���

#define	ICON_WifiState					601
#define	ICON_SipState					602
#define	ICON_LanState					603

#define 	ICON_Help						604

#define	ICON_888_ListView				888
#define	ICON_IconView1					889
#define	ICON_IconView2					890

#define	ICON_448_MLKPResSelect			448

#define	ICON_489_ListernDS		489

#define ICON_IPERF_TEST					891		// lzh_20210922

#define	ICON_PwdManage					700
#define	ICON_PwdEditName				701
#define	ICON_PwdEditNew					702
#define	ICON_PwdEditNewAgain			703
#define	ICON_PwdEditSave				704
#define	ICON_PwdEditDelete				705
#define	ICON_PublicUnlockCodeLen		706
#define	ICON_PublicUnlockCode1			707
#define	ICON_PublicUnlockCode2			708
#define	ICON_UnlockCodeMore				709
#define	ICON_CertSetup					710
#define	ICON_CertCreate					711
#define	ICON_CertDelete					712
#define	ICON_CertView					713
#define	ICON_CertLog					714
#define	ICON_CertCheck					715
#define	ICON_EventRecord				500
#define	ICON_MasterCardAddSet			716
#define	ICON_MasterCardDelSet			717
#define	ICON_UserCardAdd				718
#define	ICON_UserCardDel				719
#define	ICON_UserCardDelAll				720
#define	ICON_UserCardList				721

//for_alarming
#define ICON_ArmMode1					449
#define ICON_ArmMode2					450
#define ICON_ArmMode3					451
#define ICON_ArmMode4					452

#define ICON_AlarmInformMode1			453
#define ICON_AlarmInformMode2			454
#define ICON_AlarmInformMode3			455
#define ICON_AlarmInformMode4			456

#define ICON_Arming						457
#define ICON_Disarming					458
#define ICON_AlarmSettingExit				459

#define ICON_ArmingSetting				460
#define ICON_DisarmingSetting				461
#define ICON_ArmingKb0					462
#define ICON_ArmingKb1					463
#define ICON_ArmingKb2					464
#define ICON_ArmingKb3					465
#define ICON_ArmingKb4					466
#define ICON_ArmingKb5					467
#define ICON_ArmingKb6					468
#define ICON_ArmingKb7					469
#define ICON_ArmingKb8					470
#define ICON_ArmingKb9					471
#define ICON_ArmingKbDel				472



	// sprite define
#define SPRITE_SHORTCUT					24
#define SPRITE_INPUT_FOCUS_ENABLE		48
#define SPRITE_INPUT_FOCUS_DISABLE	49
#define SPRITE_INPUT_FOCUS_MONTH		50
#define SPRITE_INPUT_FOCUS_YEAR		51
#define SPRITE_RECORDING				52
#define SPRITE_TALKING					53
#define SPRITE_UNLOCK					54
#define SPRITE_MONITOR					55
#define SPRITE_KEYPAD_HELP				56
#define SPRITE_USER						57
#define SPRITE_DOORSTATION				58
#define SPRITE_VIDEORESOURCE			59
#define SPRITE_IM_LIST					60
#define SPRITE_MANUAL_RECORD			61
#define SPRITE_RECORD_DETAIL			62
#define SPRITE_CALL_RECORD				63
#define SPRITE_ABOUT					64
#define SPRITE_LINK_BOX					65
#define SPRITE_FAIL_BOX					66
#define SPRITE_UNLOCK_BOX				67
#define SPRITE_FAIL2_BOX				68	//czn_20170328


#define SPRITE_WIFI_QUALITY1			80
#define SPRITE_WIFI_QUALITY2			81
#define SPRITE_WIFI_QUALITY3			82
#define SPRITE_WIFI_QUALITY4			83
#define SPRITE_WIFI_SELECT				84
#define SPRITE_NO_SELECT				85

#define SPRITE_BarSelect				86
#define SPRITE_BarPlay					87
#define SPRITE_ScheduleBottom			88
#define SPRITE_ScheduleTop				89
#define SPRITE_MissCall					90
#define SPRITE_NoDisturb				91
#define SPRITE_Transfer					92
#define SPRITE_MainWifi_Disconnect		93
#define SPRITE_MainWifi_QUALITY1		94
#define SPRITE_MainWifi_QUALITY2		95
#define SPRITE_MainWifi_QUALITY3		96
#define SPRITE_MainWifi_QUALITY4		97
#define SPRITE_BUSY						98
#define SPRITE_SWITCH_HELP				99
#define SPRITE_CONFIRM					100
#define SPRITE_IF_CONFIRM				101
#define SPRITE_MainWifi_Close			105
#define SPRITE_BusyState				106
#define SPRITE_WifiState_QUALITY1		107
#define SPRITE_WifiState_QUALITY2		108
#define SPRITE_WifiState_QUALITY3		109
#define SPRITE_WifiState_QUALITY4		110
#define SPRITE_LinkError1				111	//czn_20170803
#define SPRITE_LinkError2				112
#define SPRITE_LinkError3				113
#define SPRITE_SystemBusy				114
#define SPRITE_UNLOCK2					115
#define SPRITE_SipSerOk					116
#define SPRITE_SipSerFail				117
#define SPRITE_SipSerCaution			118
#define SPRITE_DxMasterOk				119
#define SPRITE_DxMasterFail				120
#define SPRITE_DxMasterCaution			121
#define SPRITE_PlayBack					122
#define SPRITE_MissedCall				123
#define SPRITE_IncomingCall				124
#define SPRITE_OutgoingCall				125
#define SPRITE_HidePageTurning			126
#define SPRITE_PublicSetCalltune		127
#define SPRITE_PublicSetGeneral			128
#define SPRITE_PublicSetInstaller		129
#define SPRITE_PublicSetWireless		130
#define sprite_ShortcutNamelist			131
#define SPRITE_ShortcutMonitor			132

#define SPRITE_SET_RTC_NOTICE			133
#define SPRITE_FishEyeMiddle			134
#define SPRITE_FishEyeUp				135
#define SPRITE_FishEyeDown				136
#define SPRITE_FishEyeLeft				137
#define SPRITE_FishEyeRight				138
#define SPRITE_FishEyeLeftUp			139
#define SPRITE_FishEyeLeftDown			140
#define SPRITE_FishEyeRightUp			141
#define SPRITE_FishEyeRightDown			142
#define SPRITE_FishEyeHome				143

#define SPRITE_FishEyeState0			144
#define SPRITE_FishEyeState1			145
#define SPRITE_FishEyeState2			146
#define SPRITE_FishEyeState3			147
#define SPRITE_FishEyeState4			148
#define SPRITE_FishEyeState5			149
#define SPRITE_FishEyeState6			150
#define SPRITE_FishEyeState7			151
#define SPRITE_FishEyeState8			152
#define SPRITE_FishEyeState9			153
#define SPRITE_BlackBG					154
#define SPRITE_WhiteBG					155
#define SPRITE_MainWifi_QUALITY_N1		156
#define SPRITE_MainWifi_QUALITY_N2		157
#define SPRITE_MainWifi_QUALITY_N3		158
#define SPRITE_MainWifi_QUALITY_N4		159
#define SPRITE_NotSupport				160
#define SPRITE_Enlarge					161
#define SPRITE_Narrow					162
#define SPRITE_NetNotExit				163		//czn_20190121
#define SPRITE_ListSync					164		//czn_20190221
#define SPRITE_RingMute					165
#define SPRITE_NetOK					166
#define SPRITE_NetCaution				167
#define SPRITE_Collect					168
#define SPRITE_HidePageTurning2			169

#define SPRITE_Lock1Disable				171
#define SPRITE_Lock2Disable				172

#define SPRITE_lockOpenReport			173
#define SPRITE_lockOpenErrorReport		174

#define SPRITE_VideoNoBinding			175
#define SPRITE_VideoBindingIPC			176
#define SPRITE_VideoBindingIPCList		177
#define SPRITE_VideoBindingIX			178
#define SPRITE_Message					179

#define SPRITE_Intercom					180
#define SPRITE_CallScene				181
#define SPRITE_Namelist					182
#define SPRITE_Pause					183
#define SPRITE_RecTiming				184
#define SPRITE_DelConfirm				185
#define SPRITE_Exit						186
#define SPRITE_Clear					187

#define SPRITE_800_PageDwon				180
#define SPRITE_801_PageDwonS			181
#define SPRITE_802_PageUp				182
#define SPRITE_803_PageUpS				183

#define SPRITE_MLKPPage1							180
#define SPRITE_MLKPPage2							181

//for_alarming
#define SPRITE_ZoneOk					220
#define SPRITE_ZoneCaution				221

#define SPRITE_24hZoneOk				222
#define SPRITE_24hZoneCaution			223

#define SPRITE_ListSetting				225
#define SPRITE_TransferToDev			226

#endif

