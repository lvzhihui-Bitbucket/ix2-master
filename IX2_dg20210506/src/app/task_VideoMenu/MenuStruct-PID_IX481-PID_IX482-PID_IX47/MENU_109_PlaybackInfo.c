
#include "MENU_109_PlaybackInfo.h"

FILE_NAME_T infoFileName;
int playbackInfoIndex;
int playbackInfoResult;
void (*playbackInfoProcess)(int) = NULL;

int GetFileNameList(const char* dir, const char* keyWord, FILE_NAME_T *fileName)
{
	char linestr[100];
	int ret = 0;
	char *p;
	int i;

	fileName->cnt = 0;
	
	snprintf(linestr, 100, "ls %s|grep \"%s\"\n", dir, keyWord);
		
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		//文件名太长
		if(strlen(linestr) + strlen(dir) > FILE_NAME_LEN)
		{
			continue;
		}

		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}

		sprintf(fileName->name[fileName->cnt++], "%s%s", dir, linestr);
		
		if(fileName->cnt > 0)
		{
			//printf("name[%d] = %s\n", fileName->cnt-1, fileName->name[fileName->cnt-1]);
		}

		if(fileName->cnt >= FILE_NAME_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}

int GetPlaybackInfoResult(void)
{
	return playbackInfoResult;
}

int GetPlaybackInfoJpgNum(void)
{
	return infoFileName.cnt;
}

int PlaybackInfoMenuInit(const char* dir, const char* keyWord, void (*process)(int))
{
	playbackInfoProcess = process;
	GetFileNameList(dir, keyWord, &infoFileName);
	return infoFileName.cnt;
}


/*************************************************************************************************
playback info
*************************************************************************************************/
void MENU_109_PLAYBACK_INFO_Init(int uMenuCnt)	//cao_20161227
{
	playbackInfoIndex = 0;
	playbackInfoResult = 0;
	
	API_JpegPlaybackClose();
	if(infoFileName.cnt)
	{
		if( get_pane_type() == 2 || get_pane_type() == 4)//ix482/47 128*64
		{
			Set_ds_show_pos(1,0,0,1024,600);
			API_JpegPlaybackStart(infoFileName.name[playbackInfoIndex]);
		}
		else if(get_pane_type() == 5)	//ix470v 150*96
		{
			Set_ds_show_pos(1,0,0,600,1024);
			API_JpegPlaybackStart(infoFileName.name[playbackInfoIndex]);
		}
	}
}

void MENU_109_PLAYBACK_INFO_Exit(void)
{
	API_JpegPlaybackClose();
	Clear_ds_show_layer(1);
}

void MENU_109_PLAYBACK_INFO_Process(void* arg)
{	
	extern void SipEnableAgree(int value);
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	struct {int curnLen; int maxLen;} *pData;
	char tempchar[5];

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_419_PlaybackInfoNext:
					if(GetSavedIcon() == ICON_222_MainSip)
						return;
					API_JpegPlaybackClose();
					if(playbackInfoIndex + 1 < infoFileName.cnt)
					{
						API_JpegPlaybackStart(infoFileName.name[++playbackInfoIndex]);
					}
					else
					{
						playbackInfoIndex = 0;
						API_JpegPlaybackStart(infoFileName.name[playbackInfoIndex]);
					}
					break;
				case ICON_420_PlaybackInfoCancel:
					if(GetSavedIcon() == ICON_222_MainSip)
					{
						if(playbackInfoIndex + 1 < infoFileName.cnt)
						{
							API_JpegPlaybackStart(infoFileName.name[++playbackInfoIndex]);
							if(strstr(infoFileName.name[playbackInfoIndex],"QR")!=NULL)
							{
								qrenc_draw_bitmap(create_sipcfg_qrcode(),440,0,4,360,420);
							}
							else
							{
								API_OsdStringClearExt(440,0,360,420);
							}
						}
						else
						{
							popDisplayLastMenu();
						}
						return;
					}
					playbackInfoResult = 0;
					if(playbackInfoProcess != NULL)
					{
						playbackInfoProcess(playbackInfoResult);
					}
					popDisplayLastMenu();
					break;
				case ICON_421_PlaybackInfoConfirm:
					API_Event_IoServer_InnerRead_All(ManagePwdPlace, (uint8*)tempchar);
					if(playbackInfoProcess == SipEnableAgree&&atoi(tempchar)==1)
					{
						extern char installerInput[9];
						extern int MananerSettingVerifyPassword(const char* password);
						SaveIcon(ICON_421_PlaybackInfoConfirm);
						EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, MananerSettingVerifyPassword);
						return;
					}
					
					if(GetSavedIcon() == ICON_222_MainSip)
						return;
					
				
					playbackInfoResult = 1;
					if(playbackInfoProcess != NULL)
					{
						playbackInfoProcess(playbackInfoResult);
					}
					popDisplayLastMenu();
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
		
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}



