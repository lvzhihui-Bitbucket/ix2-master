#include "MENU_009_SipConfig.h"
#include	"MenuSipConfig_Business.h"		//20210428
#include "obj_ip_mon_vres_map_table.h"
#include "task_ListUpdate.h"

#include "iperf_tcp_client.h"
static int iperf_disp_x = 0;
static int iperf_disp_y = 0;
void menu009_display_iperf_bandwidth(int type, void* pbuf,int len )
{
	printf("%s\n",pbuf);
	API_OsdStringDisplayExt(iperf_disp_x, iperf_disp_y, COLOR_RED, pbuf, strlen(pbuf), 1,STR_UTF8, 0 );		
}

// lzh_20181101_s
static int timeout_update_registration_state = 0;
// lzh_20181101_e

// lzh_20181031_s
//void manual_register_enable(void);
//void manual_register_disable(void);
//int get_manual_register_count(void);
// lzh_20181031_e
void AutoMuteSupportSet(int value);		//czn_20181117
void SipEnableSet(int value);

//#define	SIP_CONFIG_ICON_MAX					6
int SipConfigIconMax;
int SipConfigIconSelect;
int SipConfigPageSelect;
int SipConfigQRCodeState;
#if 0
char* VTK_AUTO_REG_SERVER_IPADD0 = NULL;
char* VTK_AUTO_REG_SERVER_IPADD1 = NULL;
char* VTK_AUTO_REG_SERVER_IPADD2 = NULL;

char* VTK_AUTO_REG_SERVER_IP0 = NULL;
char* VTK_AUTO_REG_SERVER_IP1 = NULL;
char* VTK_AUTO_REG_SERVER_IP2 = NULL;

SipCfg_T sipCfg = {.serverToIpFlag = -1};
#endif
const char * sipNetworkSelect[2] = {LAN, WLAN};


const SipCfgIcon sipCfgIconTableMode0[] = 
{
	
	{ICON_100_SipEnable,					MESG_TEXT_ICON_100_SipEnable},
	{ICON_SipNetworkSetting,				MESG_TEXT_SipNetworkSetting},
	{ICON_074_SipDivert,					MESG_TEXT_ICON_074_SipDivert},
	{ICON_306_SipDivPwd,					MESG_TEXT_ICON_306_SipDivPwd},
	{ICON_072_SipServer,					MESG_TEXT_ICON_072_SipServer},    
	{ICON_314_ReRegister,					MESG_TEXT_Icon_314_ReRegister},
	{ICON_303_SipUseDefault,				MESG_TEXT_ICON_303_SipUseDefault},
	{ICON_304_RemoteMonCode,				MESG_TEXT_ICON_304_RemoteMonCode},
	{ICON_305_RemoteCallCode,				MESG_TEXT_ICON_305_RemoteCallCode},
	{ICON_075_SipAccount,					MESG_TEXT_ICON_075_SipAccount},
	{ICON_076_SipPassword,					MESG_TEXT_ICON_076_SipPassword},
	{ICON_073_SipPort,						MESG_TEXT_ICON_073_SipPort},
	//{ICON_345_AutoMuteSupport,				MESG_TEXT_AutoMuteSupport},
	// lzh_20210922_s
	{ICON_IPERF_TEST,						MESG_TEXT_Networktest},
};

const int sipCfgIconNumMode0 = sizeof(sipCfgIconTableMode0)/sizeof(sipCfgIconTableMode0[0]);

const SipCfgIcon sipCfgIconTableMode1[] = 
{
	{ICON_SipNetworkSetting,				MESG_TEXT_SipNetworkSetting},
	{ICON_074_SipDivert,					MESG_TEXT_ICON_074_SipDivert},
	{ICON_306_SipDivPwd,					MESG_TEXT_ICON_306_SipDivPwd},
	{ICON_072_SipServer,					MESG_TEXT_ICON_072_SipServer},    
	{ICON_314_ReRegister,					MESG_TEXT_Icon_314_ReRegister},
	{ICON_303_SipUseDefault,				MESG_TEXT_ICON_303_SipUseDefault},
	{ICON_304_RemoteMonCode,				MESG_TEXT_ICON_304_RemoteMonCode},
	{ICON_305_RemoteCallCode,				MESG_TEXT_ICON_305_RemoteCallCode},
	{ICON_075_SipAccount,					MESG_TEXT_ICON_075_SipAccount},
	{ICON_076_SipPassword,					MESG_TEXT_ICON_076_SipPassword},
	{ICON_073_SipPort,						MESG_TEXT_ICON_073_SipPort},
	//{ICON_345_AutoMuteSupport,				MESG_TEXT_AutoMuteSupport},
	// lzh_20210922_s
	{ICON_IPERF_TEST,						MESG_TEXT_Networktest},
};

const int sipCfgIconNumMode1 = sizeof(sipCfgIconTableMode1)/sizeof(sipCfgIconTableMode1[0]);

SipCfgIcon *sipCfgIconTable;
int sipCfgIconNum;

static void DisplaySipConfigPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[11];
	char temp[20];
	int stringId;
	char* stringChar;
	POS pos;
	SIZE hv;
	int iConText;
	int iCon;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < SipConfigIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}

		if(page*SipConfigIconMax+i < sipCfgIconNum)
		{
			iConText = sipCfgIconTable[i+page*SipConfigIconMax].iConText;
			iCon = sipCfgIconTable[i+page*SipConfigIconMax].iCon;
			
			if(iConText != NULL&&iCon != ICON_IPERF_TEST)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, iConText, 1, val_x - x);
			}
			else
			{
				#if 0
				if(iCon == ICON_SipNetworkSetting)
				{
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ParaGroup1,1, "SIP ", NULL, val_x - x);
				}
				// lzh_20210922_s
				else 
				#endif
				if( iCon == ICON_IPERF_TEST )
				{
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, iConText, 1, val_x - x);
					//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, "Network test", strlen("Network test"), 1,STR_UTF8, 0 );
					//API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_ICON_ParaGroup1,1, "iperf test start", NULL, val_x - x);
					if(get_pane_type() == 5 )
					{
						iperf_disp_x = x + 120;
						iperf_disp_y = y+40;
					}
					else
					{
						iperf_disp_x = x + strlen("Extranet bandwidth")*18;
						iperf_disp_y = y;
					}
					
				}
				// lzh_20210922_e

			}

			
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(iCon)
			{
				case ICON_100_SipEnable:
					API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
					stringId = (atoi(temp) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", hv.h - x);
					break;
				case ICON_SipNetworkSetting:
					API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
					stringChar = (atoi(temp) ? WLAN : LAN);
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, stringChar, strlen(stringChar), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_072_SipServer:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.server, strlen(sipCfg.server), 1, STR_UTF8, hv.h - x);
					break;
					
				case ICON_073_SipPort:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.port, strlen(sipCfg.port), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_074_SipDivert:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.divert, strlen(sipCfg.divert), 1, STR_UTF8, hv.h - x);
					break;
					
				case ICON_075_SipAccount:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.account, strlen(sipCfg.account), 1, STR_UTF8, hv.h - x);
					break;
					
				case ICON_076_SipPassword:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, "***************", strlen(sipCfg.password), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_303_SipUseDefault:
					break;
				case ICON_304_RemoteMonCode:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.monCode, strlen(sipCfg.monCode), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_305_RemoteCallCode:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.callCode, strlen(sipCfg.callCode), 1, STR_UTF8, hv.h - x);
					break;
				case ICON_306_SipDivPwd:
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, sipCfg.divPwd, strlen(sipCfg.divPwd), 1, STR_UTF8, hv.h - x);
					break;
				#if 0	
				case ICON_345_AutoMuteSupport:		//czn_20181117
					API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, temp);
					stringId = (atoi(temp) ? MESG_TEXT_Enable : MESG_TEXT_Disable);
					API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, DISPLAY_STATE_COLOR,stringId,1, "[", "]", DISPLAY_VALUE_X_WIDTH);
					break;
				#endif	
			}
		}
	}
	pageNum = sipCfgIconNum/SipConfigIconMax + (sipCfgIconNum%SipConfigIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}


int SaveSipConfigFile(void);
int LoadSipConfigFile(void);

// lzh_20180804_s
int qrenc_draw_bitmap( const char* str, int x, int y, int scaler, int panel_w, int panel_h  );
// lzh_20180804_e
#if 0
// lzh_20201110_s
int SaveSipPort(void)
{
	printf(">>>>>set linphone sip port[%s]<<<<<<\n",sipCfg.port);
	API_linphonec_set_sip_port(atoi(sipCfg.port));
}
// lzh_20201110_e

int SipConfigUseDefault(void)
{
	int ret;
	
	api_get_default_sip_local_username(sipCfg.account);
	api_get_default_sip_local_password(sipCfg.password);
	api_get_default_sip_divert_username(sipCfg.divert);
	api_get_default_sip_divert_password(sipCfg.divPwd);
	
	strcpy(sipCfg.server, VTK_AUTO_REG_SERVER_IP2);
	sipCfg.serverToIpFlag = -1;
	SaveSipServerToServerIp();
	strcpy(sipCfg.monCode, "1000");
	strcpy(sipCfg.callCode, "2000");
	strcpy(sipCfg.port,"5069");

	SaveSipConfigFile();	// lzh_20180112
	
	// lzh_20180802

	clear_locations_to_server0(sipCfg.account, sipCfg.password);
	clear_locations_to_server0(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server0(sipCfg.account, sipCfg.password);
	restore_default_psw_to_server0(sipCfg.divert, sipCfg.divPwd);

	clear_locations_to_server1(sipCfg.account, sipCfg.password);
	clear_locations_to_server1(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server1(sipCfg.account, sipCfg.password);
	restore_default_psw_to_server1(sipCfg.divert, sipCfg.divPwd);

	clear_locations_to_server2(sipCfg.account, sipCfg.password);
	clear_locations_to_server2(sipCfg.divert, sipCfg.divPwd);							
	restore_default_psw_to_server2(sipCfg.account, sipCfg.password);
	ret = restore_default_psw_to_server2(sipCfg.divert, sipCfg.divPwd);
	// 4
	API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);

	
	if( (ret == 1) || (ret == 0) )	// ok :  生成新账号, 恢复了缺省密码
	{
		ret = 0;
	}
	else if(ret == -2)		// 账号密码相同，已存在了
	{
		ret = 0;
	}
	else
	{
		ret = -1;
	}
	//czn_20181227
}
#endif
void SipNetworkSet(int value)
{
	char temp[20];

	sprintf(temp, "%d", value ? 1 : 0);
	API_Event_IoServer_InnerWrite_All(SIP_NetworkSetting, temp);
	ChangeDefaultGateway(NULL, NULL);	
}

#if 0
int SipConfigManualRegistration(void)
{
	int ret;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];
	
	parse_remote_server(VTK_AUTO_REG_SERVER_IP0, NULL, ipStr1);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP1, NULL, ipStr2);
	parse_remote_server(VTK_AUTO_REG_SERVER_IP2, NULL, ipStr3);
	
	dprintf("serverIp=%s, account=%s, password=%s, divert=%s, divPwd=%s\n", sipCfg.serverIp, sipCfg.account, sipCfg.password, sipCfg.divert, sipCfg.divPwd);
	
	if(!strcmp(ipStr1, sipCfg.serverIp) || 
	   !strcmp(ipStr2, sipCfg.serverIp) ||
	   !strcmp(ipStr3, sipCfg.serverIp))
	{
		ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.account, sipCfg.password,NULL );
		ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.divert, sipCfg.divPwd,NULL );
	}
	else
		ret = -1;	// other server apply
	
	API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);

	return ret;
}
#endif
int SipConfigVerifyPassword(const char* password)
 {
	char pwd[8+1];
	uint8 setValue;
	int i, x, y;
	int ret = 0;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];

	// lzh_20181031_s
	uint8 slaveMaster;
	// lzh_20181031_e	
	//czn_20181117_s
	int len;
	char temp[20];
	char display[100];
	//czn_20181117_e
	//char tempChar[30];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);

	API_Event_IoServer_InnerRead_All(InstallerPassword, (uint8*)pwd);

	if(!strcmp(password, pwd))
	{
		//EnterInstallerMode();
		
		switch(sipCfgIconTable[SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect].iCon)
		{
			case ICON_100_SipEnable:
				API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
				publicSettingDisplay[0][0] = len;
				API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
				publicSettingDisplay[1][0] = len;
				API_Event_IoServer_InnerRead_All(SIP_ENABLE,temp);
				EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_ICON_100_SipEnable, 2, atoi(temp), SipEnableSet);
				StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
				break;
			case ICON_SipNetworkSetting:
				InitPublicSettingMenuDisplay(2,sipNetworkSelect);
				API_Event_IoServer_InnerRead_All(SIP_NetworkSetting,temp);
				EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_ICON_ParaGroup1, 2, atoi(temp), SipNetworkSet);
				StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,0);
			case ICON_072_SipServer:
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_072_SipServer, sipCfg.server, MAX_STR-1, COLOR_WHITE, sipCfg.server, 1, SaveSipServer);
				popLastMenu();
				break;
			case ICON_073_SipPort:
				strcpy(sipCfg.saveCode, sipCfg.port);
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_073_SipPort, sipCfg.port, MAX_STR-1, COLOR_WHITE, sipCfg.port, 1, SaveSipData);
				popLastMenu();
				break;
			case ICON_074_SipDivert:
				strcpy(sipCfg.saveCode, sipCfg.divert);
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_074_SipDivert, sipCfg.divert, MAX_STR-1, COLOR_WHITE, sipCfg.divert, 1, SaveSipData);
				popLastMenu();
				break;
			case ICON_075_SipAccount:
				strcpy(sipCfg.saveCode, sipCfg.account);
				EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_075_SipAccount, sipCfg.account, MAX_STR-1, COLOR_WHITE, sipCfg.account, 1, SaveSipData);
				popLastMenu();
				break;
			case ICON_076_SipPassword:
				strcpy(sipCfg.saveCode, sipCfg.password);
				EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_076_SipPassword, sipCfg.password, MAX_STR-1, COLOR_WHITE, sipCfg.password, 1, SaveSipData);
				popLastMenu();
				break;

			// 1、恢复vtk缺省服务器和帐号
			// 2、清除所有vtk服务器的本机和转呼帐号地址信息
			// 3、注册所有vtk服务器本机和转呼帐号
			// 4、向指定vtk服务器申请注册帐号地址信息
			case ICON_303_SipUseDefault:
				popDisplayLastMenu();

				API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

				SipConfigUseDefault();

				DisplaySipConfigPageIcon(SipConfigPageSelect);
				
				
				API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
				if( (ret == 1) || (ret == 0) )  // ok :  生成新账号, 恢复了缺省密码
				{
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterOk,1, 0);
				}
				else if(ret == -2) 		// 账号密码相同，已存在了
				{
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterExist,1, 0);
				}
				else
				{
					API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterError,1, 0);
				}
				
				BEEP_KEY();
				if(OS_SipConfigRemoteCmd(1) == 0)
				{
					BEEP_CONFIRM();
				}
				else
				{
					BEEP_ERROR();
				}
				

				API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);								
				break;
				
			case ICON_304_RemoteMonCode:
				strcpy(sipCfg.saveCode, sipCfg.monCode);
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_304_RemoteMonCode, sipCfg.monCode, 4, COLOR_WHITE, sipCfg.monCode, 1, SaveCallCodeOrMonCode);
				popLastMenu();
				break;
			case ICON_305_RemoteCallCode:
				strcpy(sipCfg.saveCode, sipCfg.callCode);
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_305_RemoteCallCode, sipCfg.callCode, 4, COLOR_WHITE, sipCfg.callCode, 1, SaveCallCodeOrMonCode);
				popLastMenu();
				break;
			case ICON_306_SipDivPwd:
				// lzh_20180727_s
				EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_306_SipDivPwd, sipCfg.divPwd_change, 15, COLOR_WHITE, sipCfg.divPwd, 1, change_password_of_divert_account);
				// lzh_20180727_e							
				popLastMenu();
				break;

			// 若为vtk的服务器  1、清除当前服务器的本机和转呼帐号地址信息，2、向指定服务器申请注册帐号地址信息
			// 若为第三方服务器 1、向指定服务器申请注册帐号地址信息
			case ICON_314_ReRegister:
				
				popDisplayLastMenu();
				// lzh_20181031_s
				manual_register_enable();
				timeout_update_registration_state = 0;
				API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
				API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterInprogress,1, 0);							
				// lzh_20181031_e
				
				API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

				SipConfigManualRegistration();

				
				BEEP_KEY();
				if(OS_SipConfigRemoteCmd(1) == 0)
				{
					BEEP_CONFIRM();
				}
				else
				{
					BEEP_ERROR();
				}
				API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;
		}
		return -1;
	}
	else
	{
		return 0;
	}

}

void SipConfigShowQRCode(int enable)
{
	POS pos, pos2;
	SIZE hv, hv2;
	char* jsonString;

	OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
	OSD_GetIconInfo(ICON_012_PublicList6, &pos2, &hv2);

	if(enable)
	{
		jsonString = create_sipcfg_qrcode();
		if(jsonString != NULL)
		{
			qrenc_draw_bitmap(jsonString, pos.x, pos.y, 6, hv2.h - pos.x, hv2.v - pos.y);
			free(jsonString);
		}
	}
	else
	{
		API_DisableOsdUpdate();
		API_OsdStringClearExt(pos.x, pos.y, hv2.h - pos.x, hv2.v - pos.y);
		API_EnableOsdUpdate();
	}
}


void MENU_009_SipConfig_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	int width;
	char display[50];
	API_Event_IoServer_InnerRead_All(SipCfgMenuMode, (uint8*)display);
	if(atoi(display) == 1)
	{
		sipCfgIconTable = sipCfgIconTableMode1;
		sipCfgIconNum= sipCfgIconNumMode1;
	}
	else
	{
		sipCfgIconTable = sipCfgIconTableMode0;
		sipCfgIconNum= sipCfgIconNumMode0;
	}
	
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);

	//API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);
	// lzh_20181016_s
	API_OsdStringClearExt(pos.x, hv.v/2,300,CLEAR_STATE_H);
	// lzh_20181016_e		
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_SipConfig,1, 0);
	SipConfigIconMax = GetListIconNum();

	if(GetLastNMenu() == MENU_011_SETTING || GetLastNMenu() == MENU_001_MAIN)
	{
		SipConfigIconSelect = 0;
		SipConfigPageSelect = 0;
		BusySpriteDisplay(1);
		LoadSipConfigFile();
		BusySpriteDisplay(0);
	}
	
	// lzh_20180804_s
	SipConfigQRCodeState = 0;
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		//czn_20190221
	{
		OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
		width = ((hv.h - pos.x) < (hv.v - pos.y) ? (hv.h - pos.x) : (hv.v - pos.y));
		qrenc_draw_bitmap("show QR code!", pos.x , pos.y, 3, width, width);
	}
	// lzh_20180804_e	

	DisplaySipConfigPageIcon(SipConfigPageSelect);
	
	// lzh_20181031_s
	manual_register_disable();
	// lzh_20181031_e	
}

void MENU_009_SipConfig_Exit(void)
{
	POS pos, pos2;
	SIZE hv, hv2;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);
	pos.x += (((hv.h - pos.x) < (hv.v - pos.y) ? (hv.h - pos.x) : (hv.v - pos.y)) + 10);	//往右偏移一点，留下左边显示二维码
	API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, 70);

	//API_MenuIconDisplaySelectOff(ICON_033_Manager);
	SaveSipConfigFile();

	if(SipConfigQRCodeState)
	{
		SipConfigShowQRCode(0);
	}
	//API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, 710-DISPLAY_STATE_X, CLEAR_STATE_H);
	//API_linphonec_Register_SipCurAccount(sipCfg.server,sipCfg.account,sipCfg.password);

	// lzh_20210922_s
	api_iperf_tcp_client_stop();
	// lzh_20210922_e
}

void MENU_009_SipConfig_Process(void* arg)
{
	char* ptrstr;
	char iperf_server[100];

	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 setValue;
	int i, x, y;
	int ret = 0;
	char ipStr1[20];
	char ipStr2[20];
	char ipStr3[20];

	// lzh_20181031_s
	uint8 slaveMaster;
	// lzh_20181031_e	
	//czn_20181117_s
	int len;
	char temp[20];
	char display[100];
	//czn_20181117_e
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_176_KeyState, &pos, &hv);

	pos.x += (((hv.h - pos.x) < (hv.v - pos.y) ? (hv.h - pos.x) : (hv.v - pos.y)) + 10);	//往右偏移一点，留下左边显示二维码
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					if(SipConfigQRCodeState)
					{
						return;
					}
					PublicPageDownProcess(&SipConfigPageSelect, SipConfigIconMax, sipCfgIconNum, (DispListPage)DisplaySipConfigPageIcon);
					break;
				case ICON_202_PageUp:
					if(SipConfigQRCodeState)
					{
						return;
					}
					PublicPageUpProcess(&SipConfigPageSelect, SipConfigIconMax, sipCfgIconNum, (DispListPage)DisplaySipConfigPageIcon);
					break;
				case ICON_176_KeyState:
				//case ICON_039_QRCodeShow:
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)		//czn_20190221
					{
						if(SipConfigQRCodeState)
						{
							SipConfigShowQRCode(0);
							DisplaySipConfigPageIcon(SipConfigPageSelect);
						}
						else
						{
							SipConfigShowQRCode(1);
						}
						SipConfigQRCodeState = !SipConfigQRCodeState;
					}
					break;

					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					SipConfigIconSelect = GetCurIcon() - ICON_007_PublicList1;
					#if 0
					if(SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect >= sipCfgIconNum)
					{
						return;
					}

					if(SipConfigQRCodeState)
					{
						return;
					}

					switch(sipCfgIconTable[SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect].iCon)
					{
						case ICON_072_SipServer:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_072_SipServer, sipCfg.server, MAX_STR-1, COLOR_WHITE, sipCfg.server, 1, SaveSipServer);
							break;
						case ICON_073_SipPort:
							strcpy(sipCfg.saveCode, sipCfg.port);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_073_SipPort, sipCfg.port, MAX_STR-1, COLOR_WHITE, sipCfg.port, 1, SaveSipData);
							break;
						case ICON_074_SipDivert:
							strcpy(sipCfg.saveCode, sipCfg.divert);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_074_SipDivert, sipCfg.divert, MAX_STR-1, COLOR_WHITE, sipCfg.divert, 1, SaveSipData);
							break;
						case ICON_075_SipAccount:
							strcpy(sipCfg.saveCode, sipCfg.account);
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_075_SipAccount, sipCfg.account, MAX_STR-1, COLOR_WHITE, sipCfg.account, 1, SaveSipData);
							break;
						case ICON_076_SipPassword:
							strcpy(sipCfg.saveCode, sipCfg.password);
							EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_076_SipPassword, sipCfg.password, MAX_STR-1, COLOR_WHITE, sipCfg.password, 1, SaveSipData);
							break;

						// 1、恢复vtk缺省服务器和帐号
						// 2、清除所有vtk服务器的本机和转呼帐号地址信息
						// 3、注册所有vtk服务器本机和转呼帐号
						// 4、向指定vtk服务器申请注册帐号地址信息
						case ICON_303_SipUseDefault:
							// 1
							API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);

							api_get_default_sip_local_username(sipCfg.account);
							api_get_default_sip_local_password(sipCfg.password);
							api_get_default_sip_divert_username(sipCfg.divert);
							api_get_default_sip_divert_password(sipCfg.divPwd);
							
							strcpy(sipCfg.server, VTK_AUTO_REG_SERVER_IP2);
							sipCfg.serverToIpFlag = -1;
							SaveSipServerToServerIp();
							strcpy(sipCfg.monCode, "1000");
							strcpy(sipCfg.callCode, "2000");
							strcpy(sipCfg.port,"5069");

							SaveSipConfigFile();	// lzh_20180112
							
							// lzh_20180802

							clear_locations_to_server0(sipCfg.account, sipCfg.password);
							clear_locations_to_server0(sipCfg.divert, sipCfg.divPwd);							
							restore_default_psw_to_server0(sipCfg.account, sipCfg.password);
							restore_default_psw_to_server0(sipCfg.divert, sipCfg.divPwd);

							clear_locations_to_server1(sipCfg.account, sipCfg.password);
							clear_locations_to_server1(sipCfg.divert, sipCfg.divPwd);							
							restore_default_psw_to_server1(sipCfg.account, sipCfg.password);
							restore_default_psw_to_server1(sipCfg.divert, sipCfg.divPwd);

							clear_locations_to_server2(sipCfg.account, sipCfg.password);
							clear_locations_to_server2(sipCfg.divert, sipCfg.divPwd);							
							restore_default_psw_to_server2(sipCfg.account, sipCfg.password);
							ret = restore_default_psw_to_server2(sipCfg.divert, sipCfg.divPwd);
							// 4
							API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);		//czn_20181227

							DisplaySipConfigPageIcon(SipConfigPageSelect);
							
							
							API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, 700-DISPLAY_STATE_X, CLEAR_STATE_H);
							if( (ret == 1) || (ret == 0) )  // ok :  生成新账号, 恢复了缺省密码
							{
								API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterOk,1, 0);
							}
							else if(ret == -2) 		// 账号密码相同，已存在了
							{
								API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterExist,1, 0);
							}
							else
							{
								API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterError,1, 0);
							}

							API_SpriteClose(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
								
							break;
							
						case ICON_304_RemoteMonCode:
							strcpy(sipCfg.saveCode, sipCfg.monCode);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_304_RemoteMonCode, sipCfg.monCode, 4, COLOR_WHITE, sipCfg.monCode, 1, SaveCallCodeOrMonCode);
							break;
						case ICON_305_RemoteCallCode:
							strcpy(sipCfg.saveCode, sipCfg.callCode);
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_305_RemoteCallCode, sipCfg.callCode, 4, COLOR_WHITE, sipCfg.callCode, 1, SaveCallCodeOrMonCode);
							break;
						case ICON_306_SipDivPwd:
							// lzh_20180727_s
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_306_SipDivPwd, sipCfg.divPwd_change, 15, COLOR_WHITE, sipCfg.divPwd, 1, change_password_of_divert_account);
							// lzh_20180727_e							
							break;

						// 若为vtk的服务器  1、清除当前服务器的本机和转呼帐号地址信息，2、向指定服务器申请注册帐号地址信息
						// 若为第三方服务器 1、向指定服务器申请注册帐号地址信息
						case ICON_314_ReRegister:	
							// lzh_20181031_s
							manual_register_enable();
							timeout_update_registration_state = 0;
							API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, 710-DISPLAY_STATE_X, CLEAR_STATE_H);
							API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterInprogress,1, 0);							
							// lzh_20181031_e
							
							API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);

							parse_remote_server(VTK_AUTO_REG_SERVER_IP0, NULL, ipStr1);
							parse_remote_server(VTK_AUTO_REG_SERVER_IP1, NULL, ipStr2);
							parse_remote_server(VTK_AUTO_REG_SERVER_IP2, NULL, ipStr3);
							
							if(!strcmp(ipStr1, sipCfg.serverIp) || 
							   !strcmp(ipStr2, sipCfg.serverIp) ||
							   !strcmp(ipStr3, sipCfg.serverIp))
							{
								// lzh_20180802_s
								//ret1 |= auto_clearul_to_server0(sipCfg.account, sipCfg.password);
								//ret1 |= auto_clearul_to_server0(sipCfg.divert, sipCfg.divPwd);							
								ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.account, sipCfg.password,NULL );
								ret |= remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2, sipCfg.serverIp, sipCfg.divert, sipCfg.divPwd,NULL );
								// lzh_20180802_e
							}
							else
								ret = -1;	// other server apply
							API_SpriteClose(SYSTEM_SPRITE_INFORM_X, SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
							
							API_linphonec_Register_SipCurAccount(sipCfg.serverIp,sipCfg.account,sipCfg.password);		//czn_20181227
							
							break;
						//czn_20181117_s
						case ICON_345_AutoMuteSupport:
							API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
							publicSettingDisplay[0][0] = len;
							API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
							publicSettingDisplay[1][0] = len;
							API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE, temp);
							EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_AutoMuteSupport, 2, atoi(temp), AutoMuteSupportSet);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						//czn_20181117_e

					}
					#endif
					//czn_20190221_s
					if(memcmp(GetSysVerInfo_ms(),"01",2)==0)
					{
						if(SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect >= sipCfgIconNum)
						{
							return;
						}

						if(SipConfigQRCodeState)
						{
							return;
						}
						
						API_Event_IoServer_InnerRead_All(SipChangePwdEnable, temp);
						
						if(atoi(temp)==0||JudgeIsInstallerMode() || API_AskDebugState(100))
						{
						switch(sipCfgIconTable[SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect].iCon)
						{
							case ICON_100_SipEnable:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(SIP_ENABLE,temp);
								EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_ICON_100_SipEnable, 2, atoi(temp), SipEnableSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
								
							case ICON_SipNetworkSetting:
								InitPublicSettingMenuDisplay(2,sipNetworkSelect);
								API_Event_IoServer_InnerRead_All(SIP_NetworkSetting,temp);
								EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_ICON_ParaGroup1, 2, atoi(temp), SipNetworkSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
								
							case ICON_072_SipServer:
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_072_SipServer, sipCfg.server, MAX_STR-1, COLOR_WHITE, sipCfg.server, 1, SaveSipServer);
								break;
							case ICON_073_SipPort:
								strcpy(sipCfg.saveCode, sipCfg.port);
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_073_SipPort, sipCfg.port, MAX_STR-1, COLOR_WHITE, sipCfg.port, 1, SaveSipData);
								break;
							case ICON_074_SipDivert:
								strcpy(sipCfg.saveCode, sipCfg.divert);
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_074_SipDivert, sipCfg.divert, MAX_STR-1, COLOR_WHITE, sipCfg.divert, 1, SaveSipData);
								break;
							case ICON_075_SipAccount:
								strcpy(sipCfg.saveCode, sipCfg.account);
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_075_SipAccount, sipCfg.account, MAX_STR-1, COLOR_WHITE, sipCfg.account, 1, SaveSipData);
								break;
							case ICON_076_SipPassword:
								strcpy(sipCfg.saveCode, sipCfg.password);
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_ICON_076_SipPassword, sipCfg.password, MAX_STR-1, COLOR_WHITE, sipCfg.password, 1, SaveSipData);
								break;

							// 1、恢复vtk缺省服务器和帐号
							// 2、清除所有vtk服务器的本机和转呼帐号地址信息
							// 3、注册所有vtk服务器本机和转呼帐号
							// 4、向指定vtk服务器申请注册帐号地址信息
							case ICON_303_SipUseDefault:
								API_Event_IoServer_InnerRead_All(SIP_ENABLE, (uint8*)temp);
								if(atoi(temp) == 0)
								{
									Set_SipAccount_State(2,0);
									BEEP_ERROR();
									break;
								}
	
								API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

								SipConfigUseDefault();

								DisplaySipConfigPageIcon(SipConfigPageSelect);
								
								
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
								if( (ret == 1) || (ret == 0) )  // ok :  生成新账号, 恢复了缺省密码
								{
									API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterOk,1, 0);
								}
								else if(ret == -2) 		// 账号密码相同，已存在了
								{
									API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterExist,1, 0);
								}
								else
								{
									API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterError,1, 0);
								}
								
								BEEP_KEY();
								if(OS_SipConfigRemoteCmd(1) == 0)
								{
									BEEP_CONFIRM();
								}
								else
								{
									BEEP_ERROR();
								}
								

								API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);								
								break;
								
							case ICON_304_RemoteMonCode:
								API_VtkMediaTrans_StartJpgPush(1,sipCfg.serverIp,sipCfg.account,sipCfg.password,sipCfg.divert);
								//strcpy(sipCfg.saveCode, sipCfg.monCode);
								//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_304_RemoteMonCode, sipCfg.monCode, 4, COLOR_WHITE, sipCfg.monCode, 1, SaveCallCodeOrMonCode);
								break;
							case ICON_305_RemoteCallCode:
								API_VtkMediaTrans_StopJpgPush();
								//strcpy(sipCfg.saveCode, sipCfg.callCode);
								//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_305_RemoteCallCode, sipCfg.callCode, 4, COLOR_WHITE, sipCfg.callCode, 1, SaveCallCodeOrMonCode);
								break;
							case ICON_306_SipDivPwd:
								// lzh_20180727_s
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_306_SipDivPwd, sipCfg.divPwd_change, 15, COLOR_WHITE, sipCfg.divPwd, 1, change_password_of_divert_account);
								// lzh_20180727_e							
								break;

							// 若为vtk的服务器  1、清除当前服务器的本机和转呼帐号地址信息，2、向指定服务器申请注册帐号地址信息
							// 若为第三方服务器 1、向指定服务器申请注册帐号地址信息
							case ICON_314_ReRegister:
								API_Event_IoServer_InnerRead_All(SIP_ENABLE, (uint8*)temp);
								if(atoi(temp) == 0)
								{
									BEEP_ERROR();
									Set_SipAccount_State(2,0);
									break;
								}
								// lzh_20181031_s
								manual_register_enable();
								timeout_update_registration_state = 0;
								API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
								API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterInprogress,1, 0);							
								// lzh_20181031_e
								
								API_SpriteDisplay_XY(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

								SipConfigManualRegistration();

								
								BEEP_KEY();
								if(OS_SipConfigRemoteCmd(1) == 0)
								{
									BEEP_CONFIRM();
								}
								else
								{
									BEEP_ERROR();
								}
								API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
								break;
							#if 0	
							//czn_20181117_s
							case ICON_345_AutoMuteSupport:
								API_GetOSD_StringWithID(MESG_TEXT_Disable, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
								publicSettingDisplay[0][0] = len;
								API_GetOSD_StringWithID(MESG_TEXT_Enable, NULL, 0, NULL, 0, publicSettingDisplay[1]+1, &len);
								publicSettingDisplay[1][0] = len;
								API_Event_IoServer_InnerRead_All(DIVERT_AUTO_MUTE,temp);
								//temp[0] = temp[0] ? 1 : 0;
								//EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_AutoMuteSupport, 2, temp, AutoMuteSupportSet);
								EnterPublicSettingMenu(MESG_TEXT_ICON_027_wireless, MESG_TEXT_AutoMuteSupport, 2, atoi(temp), AutoMuteSupportSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
								break;
							//czn_20181117_e
							#endif

							// lzh_20210922_s
							case ICON_IPERF_TEST:				
								api_iperf_tcp_client_stop();
								menu009_display_iperf_bandwidth(0,"please wait...           ",strlen("please wait...           "));
								strcpy(iperf_server,sipCfg.serverIp); 
								ptrstr = strstr(iperf_server,":");
								if( ptrstr != NULL ) ptrstr[0] = '\0';
								api_iperf_tcp_client_start( iperf_server, 6789, 10, 1, 0, menu009_display_iperf_bandwidth );
								break;
							// lzh_20210922_e
						}
						}
						else
						{
							switch(sipCfgIconTable[SipConfigPageSelect*SipConfigIconMax+SipConfigIconSelect].iCon)
							{

								// 1、恢复vtk缺省服务器和帐号
								// 2、清除所有vtk服务器的本机和转呼帐号地址信息
								// 3、注册所有vtk服务器本机和转呼帐号
								// 4、向指定vtk服务器申请注册帐号地址信息
								case ICON_303_SipUseDefault:
									API_Event_IoServer_InnerRead_All(SIP_ENABLE, (uint8*)temp);
									if(atoi(temp) == 0)
									{
										Set_SipAccount_State(2,0);
										BEEP_ERROR();
										return;
									}
									
									break;
								

								// 若为vtk的服务器  1、清除当前服务器的本机和转呼帐号地址信息，2、向指定服务器申请注册帐号地址信息
								// 若为第三方服务器 1、向指定服务器申请注册帐号地址信息
								case ICON_314_ReRegister:
									API_Event_IoServer_InnerRead_All(SIP_ENABLE, (uint8*)temp);
									if(atoi(temp) == 0)
									{
										BEEP_ERROR();
										Set_SipAccount_State(2,0);
										return;
									}
									break;

							}
							extern char installerInput[9];
							
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, SipConfigVerifyPassword);
						}
					}
					else if(SipConfigIconSelect == 0)
					{
						BusySpriteDisplay(1);
						
						x = pos.x;
						y = pos.y+(hv.v - pos.y)/2;

						
						if(API_MsSyncSipConfig(1) == 0)
						{
							sprintf(display, "Sync sip config ok        ");
						}
						else
						{
							sprintf(display, "Sync sip config fail:master offline");
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						sleep(1);
						API_OsdStringClearExt(x,y,bkgd_w-x, 40);
					}
					//czn_20190221_e
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			// lzh_20181101_s
			case MSG_7_BRD_SUB_MANUAL_REGISTER_TIMEOUT:				
				// lzh_20181101_s
				dprintf("------MSG_7_BRD_SUB_MANUAL_REGISTER_TIMEOUT------\n");
				// lzh_20181101_e				
				API_linphonec_apply_registration_state();	
				timeout_update_registration_state = 1;
				// API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterFailNoRespose,1, 0);					
				break;				
			case MSG_7_BRD_SUB_GET_REGISTER_STATE_OK:
				// lzh_20181101_s
				dprintf("------MSG_7_BRD_SUB_GET_REGISTER_STATE_OK------\n");
				// lzh_20181101_e				
				if(get_manual_register_count() || timeout_update_registration_state )//get_manual_register_count() ||
				{				
					timeout_update_registration_state = 0;

					manual_register_disable();
					if(!strcmp(GetSysVerInfo_ms(), "01"))
					{
						API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
						// register sccessful!
						API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterOk,1, 0);					
					}
				}
				API_SpriteClose(SYSTEM_SPRITE_INFORM_X(bkgd_w), SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;
			case MSG_7_BRD_SUB_GET_REGISTER_STATE_FAIL:
				// lzh_20181101_s
				dprintf("------MSG_7_BRD_SUB_GET_REGISTER_STATE_FAIL------\n");
				// lzh_20181101_e				
				if( get_manual_register_count() || timeout_update_registration_state )//get_manual_register_count() || 
				{				
					timeout_update_registration_state = 0;
					if(!strcmp(GetSysVerInfo_ms(), "01"))
					{
						if(Get_SipReg_ErrCode() == 401)
						{
							//API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, bkgd_w-DISPLAY_STATE_X, CLEAR_STATE_H);
							//API_OsdUnicodeStringDisplay(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterFailBadCredentials,1, 0);
							dprintf("------Get_SipReg_ErrCode: 401\n");							
						}
						// register fail, bad credentials!
						else if(Get_SipReg_ErrCode() == LinphoneReasonBadCredentials)
						{					
							manual_register_disable();						
							API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
							API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterFailBadCredentials,1, 0);
							dprintf("------Get_SipReg_ErrCode: LinphoneReasonBadCredentials\n");							
						}
						// register fail, no response!
						else if(Get_SipReg_ErrCode() == LinphoneReasonNoResponse)
						{
							manual_register_disable();						
							API_OsdStringClearExt(pos.x, pos.y+(hv.v - pos.y)/2, bkgd_w-pos.x, CLEAR_STATE_H);
							API_OsdUnicodeStringDisplay(pos.x, pos.y+(hv.v - pos.y)/2, DISPLAY_STATE_COLOR,MESG_TEXT_RegisterFailNoRespose,1, 0);
							dprintf("------Get_SipReg_ErrCode: LinphoneReasonNoResponse\n");							
						}
					}
				}
				break;
			// lzh_20181101_e
		
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}	
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

int SaveCallCodeOrMonCode(char * code)
{
	if(strlen(code) != 4)
	{
		strcpy(code, sipCfg.saveCode);
	}
	return 1;
}

int SaveSipData(char * code)
{
	if(strlen(code) == 0)
	{
		strcpy(code, sipCfg.saveCode);
	}
	return 1;
}

// lzh_20180804_e
//czn_20181117_s
void AutoMuteSupportSet(int value)
{
	char temp[20];

	sprintf(temp, "%d", value ? 1 : 0);
	API_Event_IoServer_InnerWrite_All(DIVERT_AUTO_MUTE, temp);
}
//czn_20181117_e

void SipEnableAgree(int value)
{
	char temp[20];
	
	if(value)
	{
		sprintf(temp, "%d", 1);
		API_Event_IoServer_InnerWrite_All(SIP_ENABLE, temp);
		AutoRegistration();
	}
}

void SipEnableSet(int value)
{
	char temp[20];

	if(value)
	{
		if(PlaybackInfoMenuInit("/mnt/nand1-2/Customerized/UI/", "SipAgreement.*.jpg", SipEnableAgree))
		{
			StartInitOneMenu(MENU_109_Info,0,0);
			return;
		}
	}
	
	sprintf(temp, "%d", value ? 1 : 0);
	API_Event_IoServer_InnerWrite_All(SIP_ENABLE, temp);
	if(value)
	{
		AutoRegistration();
	}
	else
	{
		Set_SipAccount_State(2,0);
		//if((TransferSetting = Get_NoDisturbSetting()) == 0)		
		char temp[20];
		API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
		if(atoi(temp) > 2&&atoi(temp)!=5)
		{
			sprintf(temp, "%d", 0);
			API_Event_IoServer_InnerWrite_All(CallScene_SET, temp);
		}
	}
	
	popDisplayLastMenu();
}
//czn_20181117_e
#if 0
SipCfg_T* GetSipConfig(void)
{
	return &sipCfg;
}
#endif



