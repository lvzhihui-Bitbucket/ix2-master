
#ifndef _MENU_005_CallScene_H
#define _MENU_005_CallScene_H

#include "MENU_public.h"

typedef struct
{
	int iCon;
	int iConText;
}CallSceneIcon;


int Get_NoDisturbSetting(void);
int Set_NoDisturbSetting(int CallScene);
int Get_NoDisturb8H_RemainTime(int *remain_hour,int *remain_min);

#endif


