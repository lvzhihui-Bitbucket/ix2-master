#include "MENU_public.h"
#include "MENU_082_AutoSetupWizard.h"
#include "obj_ip_mon_vres_map_table.h"
#include "obj_GetIpByNumber.h"
#include "obj_ProgInfoByIp.h"
#include "obj_GetInfoByIp.h"




#define	AutoSetupWizard_ICON_MAX							5

int autoSetupWizardIconSelect;
int autoSetupWizardPageSelect;
int autoSetupWizardIndex;


void DisplayOnePageAutoSetupWizardlist(uint8 page)
{
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	int monres_num = 0;
	

	list_start = page*AutoSetupWizard_ICON_MAX;

	for( i = 0; i < AutoSetupWizard_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		if((list_start+i) < monres_num)
		{
			
		}
	}
	
	
	maxPage = (monres_num%AutoSetupWizard_ICON_MAX) ? (monres_num/AutoSetupWizard_ICON_MAX+1) : (monres_num%AutoSetupWizard_ICON_MAX);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}



void MENU_082_AutoSetupWizard_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	autoSetupWizardIconSelect = 0;
	autoSetupWizardPageSelect = 0;
	autoSetupWizardIndex = ICON_052_IM_Process;
	API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_AutoSetupWizard, 1, 0);
	//DisplayOnePageAutoSetupWizardlist(autoSetupWizardPageSelect);
	BusySpriteDisplay(1);
	AutoSetupMS_Device();
	sleep(1);
	API_MenuIconDisplaySelectOff(autoSetupWizardIndex);

	
	autoSetupWizardIndex = ICON_053_OS_Process;
	API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
	AutoSetupOS_Device();
	API_MenuIconDisplaySelectOff(autoSetupWizardIndex);

	#if 0
	autoSetupWizardIndex = ICON_054_IPC_Process;
	API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
	sleep(1);
	AutoSetupIPC_Device();
	sleep(1);
	#endif
	BusySpriteDisplay(0);
}


void MENU_082_AutoSetupWizard_Exit(void)
{
	
}

void MENU_082_AutoSetupWizard_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_052_IM_Process:
					if(autoSetupWizardIndex != ICON_052_IM_Process)
					{
						API_MenuIconDisplaySelectOff(autoSetupWizardIndex);
						autoSetupWizardIndex = ICON_052_IM_Process;
						API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
						BusySpriteDisplay(1);
						AutoSetupMS_Device();
						BusySpriteDisplay(0);
					}
					break;
					
				case ICON_053_OS_Process:
					if(autoSetupWizardIndex != ICON_053_OS_Process)
					{
						API_MenuIconDisplaySelectOff(autoSetupWizardIndex);
						autoSetupWizardIndex = ICON_053_OS_Process;
						API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
						BusySpriteDisplay(1);
						AutoSetupOS_Device();
						BusySpriteDisplay(0);
					}

					break;
					
				case ICON_054_IPC_Process:
					if(autoSetupWizardIndex != ICON_054_IPC_Process)
					{
						API_MenuIconDisplaySelectOff(autoSetupWizardIndex);
						autoSetupWizardIndex = ICON_054_IPC_Process;
						API_MenuIconDisplaySelectOn(autoSetupWizardIndex);
						BusySpriteDisplay(1);
						AutoSetupIPC_Device();
						BusySpriteDisplay(0);
					}
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					//autoSetupWizardIconSelect = GetCurIcon() - ICON_007_PublicList1;
					//autoSetupWizardIndex = autoSetupWizardPageSelect*AutoSetupWizard_ICON_MAX + autoSetupWizardIconSelect;

					break;
					
				case ICON_201_PageDown:
					break;
				case ICON_202_PageUp:
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


void AutoSetupMS_Device(void)
{
	GetIpRspData getIpdata;
	GetIpRspData saveIm;
	DeviceInfo devInfo;
	int i, j;
	int roomNumber;
	DeviceInfo info;
	int x, y;
	char display[100];
	POS pos;
	SIZE hv;
	int len1,len2;
	char name[50];
	char num[5];

	char BD_RM_MS[11];
	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 1, 50, &getIpdata);
	
	for(i = 0, saveIm.cnt = 0; i < getIpdata.cnt; i++)
	{
		if(getIpdata.deviceType[i] == TYPE_IM)
		{
			saveIm.Ip[saveIm.cnt] = getIpdata.Ip[i];
			strcpy(saveIm.BD_RM_MS[saveIm.cnt], getIpdata.BD_RM_MS[i]);
			saveIm.cnt++;
		}
	}
	
	for(i = 0; i < saveIm.cnt; i++)
	{
		roomNumber = atoi(saveIm.BD_RM_MS[i]+8);

		while(1)
		{
			for(j = 0; j < saveIm.cnt; j++)
			{
				if(i == j && roomNumber != 1)
				{
					continue;
				}
				
				if(roomNumber == atoi(saveIm.BD_RM_MS[j]+8) || roomNumber == 1)
				{
					roomNumber++;
					break;
				}
			}
				
			if(j == saveIm.cnt)
			{
				snprintf(saveIm.BD_RM_MS[i]+8, 3, "%02d", roomNumber);
				break;
			}
		}

		API_GetInfoByIp(saveIm.Ip[i], 1, &info);
		strcpy(info.BD_RM_MS, saveIm.BD_RM_MS[i]);
		snprintf(info.name1, 21, "%s_%02d", GetSysVerInfo_name(), roomNumber);
		API_ProgInfoByIp(saveIm.Ip[i], 1, &info);
	}

	#if 0
	if(saveIm.cnt == 0)
	{
		snprintf(display, 100, "Find 0 monitor");
	}
	else if(saveIm.cnt == 1)
	{
		snprintf(display, 100, "Find 1 monitors, Rename to \"%s_02\"", GetSysVerInfo_name());
	}
	else
	{
		snprintf(display, 100, "Find %d monitors, Rename to \"%sXX\"", saveIm.cnt, GetSysVerInfo_name());
	}
	#endif
	
	sprintf(num,"%d",saveIm.cnt);
	if(saveIm.cnt == 0)
	{
		name[0]=0;
	}
	else if(saveIm.cnt == 1)
	{
		snprintf(name, 50, "\"%s_02\"", GetSysVerInfo_name());
	}
	else
	{
		snprintf(name,50, "\"%sXX\"", GetSysVerInfo_name());
	}
	API_GetOSD_StringWithID(MESG_TEXT_AutoSettingFoundMonitor,num,strlen(num), (saveIm.cnt>0)?",":NULL,(saveIm.cnt>0)?1:0,display,&len1);
	if(saveIm.cnt)
		API_GetOSD_StringWithID(MESG_TEXT_AutoSettingRenameTo,NULL,NULL,name,strlen(name),display+len1,&len2);
	else
		len2=0;
	
	OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+0*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
	API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
	API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, len1+len2, 1,STR_UNICODE, bkgd_w-x);
}

void AutoSetupOS_Device(void)
{
	GetIpRspData getIpdata;
	GetIpRspData saveIm;
	DeviceInfo devInfo;
	int i, j;
	int roomNumber;
	DeviceInfo info;
	int x, y;
	char display[100];
	POS pos;
	SIZE hv;
	int len1,len2;
	int num[5];
	int name[50];
	int ospre[20];

	char BD_RM_MS[11];
	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	getIpdata.cnt = 0;
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 2, 50, &getIpdata);
	
	for(i = 0, saveIm.cnt = 0; i < getIpdata.cnt; i++)
	{
		if(getIpdata.deviceType[i] == TYPE_OS)
		{
			saveIm.Ip[saveIm.cnt] = getIpdata.Ip[i];
			memcpy(saveIm.BD_RM_MS[saveIm.cnt], getIpdata.BD_RM_MS[i], 10);
			saveIm.cnt++;
		}
	}
	
	for(i = 0; i < saveIm.cnt; i++)
	{
		memcpy(display, saveIm.BD_RM_MS[i]+8, 2);
		display[2] = 0;
		roomNumber = atoi(display);

		while(1)
		{
			
			for(j = 0; j < saveIm.cnt; j++)
			{
				if(i == j)
				{
					continue;
				}
				
				memcpy(display, saveIm.BD_RM_MS[j]+8, 2);
				display[2] = 0;
				
				if(roomNumber == atoi(display))
				{
					roomNumber++;
					break;
				}
			}
			if(j == saveIm.cnt)
			{
				sprintf(display, "%02d", roomNumber);
				memcpy(saveIm.BD_RM_MS[i]+8, display, 2);
				break;
			}

		}
		

		API_GetInfoByIp(saveIm.Ip[i], 1, &info);
		memcpy(info.BD_RM_MS, saveIm.BD_RM_MS[i], 10);
		info.BD_RM_MS[10] = 0;
		snprintf(info.name1, 21, "%s_OS%02d", GetSysVerInfo_name(), roomNumber > 50 ? roomNumber-50 : roomNumber);
		API_ProgInfoByIp(saveIm.Ip[i], 1, &info);
	}
	#if 0
	if(saveIm.cnt == 0)
	{
		snprintf(display, 100, "Find 0 OS");
	}
	else if(saveIm.cnt == 1)
	{
		snprintf(display, 100, "Find 1 OS, Rename to \"%s_OS01\"", GetSysVerInfo_name());
	}
	else
	{
		snprintf(display, 100, "Find %d OS, Rename to \"%s_OSXX\"", saveIm.cnt, GetSysVerInfo_name());
	}
	#endif
	API_Event_IoServer_InnerRead_All(DEVICE_DISP_PREFIX_OS,ospre);
	sprintf(num,"%d",saveIm.cnt);
	if(saveIm.cnt == 0)
	{
		name[0]=0;
	}
	else if(saveIm.cnt == 1)
	{
		snprintf(name, 50, "\"%s_%s%02d\"", GetSysVerInfo_name(),ospre,roomNumber > 50 ? roomNumber-50 : roomNumber);
	}
	else
	{
		snprintf(name, 50, "\"%s_%sXX\"", GetSysVerInfo_name(),ospre);
	}
	API_GetOSD_StringWithID(MESG_TEXT_AutoSettingFoundOS,num,strlen(num), (saveIm.cnt>0)?",":NULL,(saveIm.cnt>0)?1:0,display,&len1);
	if(saveIm.cnt)
		API_GetOSD_StringWithID(MESG_TEXT_AutoSettingRenameTo,NULL,NULL,name,strlen(name),display+len1,&len2);
	else
		len2=0;
	
	OS_SipConfigRemoteCmd(1);
	
	OSD_GetIconInfo(ICON_008_PublicList2, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+1*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
	API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
	API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, len1+len2, 1,STR_UNICODE, bkgd_w-x);
}

void AutoSetupIPC_Device(void)
{
	int updateCnt;
	char display[100];
	int x, y;
	POS pos;
	SIZE hv;
	char num[5];
	int len1;
	
	updateCnt = AddIPC_TableBySearching("admin", "admin", 1);
	
	//snprintf(display, 100, "Find %d IPC, Add to Monitor list", updateCnt);
	sprintf(num,"%d",updateCnt);
	API_GetOSD_StringWithID(MESG_TEXT_AutoSettingFoundIPC,num,strlen(num), NULL,0,display,&len1);
	
	OSD_GetIconInfo(ICON_009_PublicList3, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	//y = DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
	API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1,STR_UTF8, bkgd_w-x);
	API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, len1, 1,STR_UNICODE, bkgd_w-x);
}


