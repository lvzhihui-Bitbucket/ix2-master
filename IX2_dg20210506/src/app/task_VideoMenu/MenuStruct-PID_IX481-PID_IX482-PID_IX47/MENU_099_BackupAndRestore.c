#include "MENU_public.h"
#include "MENU_099_BackupAndRestore.h"
#include "obj_BackupAndRestoreCmd.h"
#include "obj_BackupAndRestore.h"

#define	BACKUP_AND_RESTORE_ICON_MAX		5

int backupAndRestoreIconSelect;
int backupAndRestorePageSelect;

int operationSelect = 0;
int rangeSelect = 3;
int backupSelect = 0;
int locationSelect = 0;

const char *operationDisplay[2] = {"Restore", "Backup"};
const char *rangeDisplay[4] = {"User data", "User set", "Admin set", "ALL"};
const char *backupDisplay[2] = {"Installer data", "Last data"};
const char *locationDisplay[1] = {"Local"};

Backup_FILE_NAME_T backupFile;

const IconAndText_t backupAndRestoreIconTable[] = 
{
	{ICON_OperationSelect,					MESG_TEXT_ICON_OperationSelect},
	{ICON_RangeSelect,						MESG_TEXT_ICON_RangeSelect},
	{ICON_BackupSelect,						MESG_TEXT_ICON_BackupSelect},
	{ICON_Location,							MESG_TEXT_ICON_Location},
	{ICON_Start,							MESG_TEXT_ICON_Start},
};



const int backupAndRestoreIconNum = sizeof(backupAndRestoreIconTable)/sizeof(backupAndRestoreIconTable[0]);

static void DisplaybackupAndRestorePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[50];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < BACKUP_AND_RESTORE_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*BACKUP_AND_RESTORE_ICON_MAX+i < backupAndRestoreIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, backupAndRestoreIconTable[i+page*BACKUP_AND_RESTORE_ICON_MAX].iConText, 1, val_x-x);
			x += (hv.h - pos.x)/2;
			switch(backupAndRestoreIconTable[i+page*BACKUP_AND_RESTORE_ICON_MAX].iCon)
			{
				case ICON_OperationSelect:
					API_OsdStringDisplayExt(x, y, COLOR_RED, operationDisplay[operationSelect], strlen(operationDisplay[operationSelect]),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_RangeSelect:
					API_OsdStringDisplayExt(x, y, COLOR_RED, rangeDisplay[rangeSelect], strlen(rangeDisplay[rangeSelect]),1,STR_UTF8, hv.h - x);
					break;
				case ICON_BackupSelect:
					API_OsdStringDisplayExt(x, y, COLOR_RED, backupDisplay[backupSelect], strlen(backupDisplay[backupSelect]),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_Location:
					if(locationSelect == 0)
					{
						API_OsdStringDisplayExt(x, y, COLOR_RED, locationDisplay[locationSelect], strlen(locationDisplay[locationSelect]),1,STR_UTF8, hv.h - x);
					}
					else
					{
						int ip = GetSearchOnlineIp();
						snprintf(display, 50, "%03d.%03d.%03d.%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
						API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
					}
					break;
					
			}
		}
	}
	pageNum = backupAndRestoreIconNum/BACKUP_AND_RESTORE_ICON_MAX + (backupAndRestoreIconNum%BACKUP_AND_RESTORE_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void SetLocationSelect(int select)
{
	locationSelect = select;
}

void SetOperationSelect(int select)
{
	operationSelect = select;
}

void SetRangeSelect(int select)
{
	rangeSelect = select;
}

void SetBackupSelectSelect(int select)
{
	backupSelect = select;
}

void MENU_099_BackupAndRestore_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	backupAndRestoreIconSelect = 0;
	backupAndRestorePageSelect = 0;
	
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_BACKUP_RESTORE,1, 0);
	DisplaybackupAndRestorePageIcon(backupAndRestorePageSelect);

	GetBackupFileName(&backupFile);
}

void MENU_099_BackupAndRestore_Exit(void)
{

}

void MENU_099_BackupAndRestore_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[2000];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					backupAndRestoreIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(backupAndRestorePageSelect*BACKUP_AND_RESTORE_ICON_MAX+backupAndRestoreIconSelect >= backupAndRestoreIconNum)
					{
						return;
					}
					
					switch(backupAndRestoreIconTable[backupAndRestorePageSelect*BACKUP_AND_RESTORE_ICON_MAX+backupAndRestoreIconSelect].iCon)
					{
						case ICON_OperationSelect:
							InitPublicSettingMenuDisplay(2, operationDisplay);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_OperationSelect, 2, operationSelect, SetOperationSelect);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
							
						case ICON_RangeSelect:
							InitPublicSettingMenuDisplay(4, rangeDisplay);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_RangeSelect, 4, rangeSelect, SetRangeSelect);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
							
						case ICON_BackupSelect:
							InitPublicSettingMenuDisplay(2, backupDisplay);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_BackupSelect, 2, backupSelect, SetBackupSelectSelect);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
						case ICON_Start:
							if(locationSelect == 0)
							{
								if(operationSelect == 0)
								{
									if(API_RestoreFromBackup(backupSelect, rangeSelect, backupFile.name[0]) == 0)
									{
										BEEP_CONFIRM();
									}
									else
									{
										BEEP_ERROR();
									}
								}
								else
								{
									if(API_Backup(backupSelect, rangeSelect) == 0)
									{
										BEEP_CONFIRM();
									}
									else
									{
										BEEP_ERROR();
									}
								}
							}
							else
							{
								if(API_BackupAndRestoreCmd(GetSearchOnlineIp(), operationSelect, backupSelect, rangeSelect, 5) == 0)
								{
									BEEP_CONFIRM();
								}
								else
								{
									BEEP_ERROR();
								}
							}
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


