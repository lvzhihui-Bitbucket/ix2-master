#include "MENU_public.h"
#include "MENU_028_Monitor2.h"
#include "obj_GetIpByNumber.h"
#include "obj_business_manage.h"		//czn_20190107
#include "task_Ring.h"
#include "task_Power.h"
#include "task_survey.h"
#include "task_monitor.h"		//czn_20170112
#include "obj_memo.h"

static int dsWinId, iconNum;
	
void EnterDSMonMenu(int win_id, int stack)
{
	dsWinId = win_id;
	if(stack == 0)
	{
		VdShowHide();
	}
	StartInitOneMenu(MENU_028_MONITOR2,0,stack);
	if(stack == 0)
	{
		VdShowToFull(dsWinId);
	}
	else
	{
		SetWinMonState(dsWinId, 1);
		SetWinDeviceType(dsWinId, 0);
	}
}

int GetDs_Show_Win(void)
{
	return dsWinId;
}

int CheckTalkOnoff(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(GetDsTalkState(i))
			return -1;
	}
	return 0;
}

void MonTalkProcess(void)
{
	if(GetDsTalkState(dsWinId))
	{
		SetDsTalkState(dsWinId, 0);
		DsTalkClose(dsWinId);
		ExitQuadMenu();
		popDisplayLastMenu();
	}
	else
	{
		if(CheckTalkOnoff() == 0)
		{
			if(DsTalkDeal(dsWinId) == 0)
			{
				SetDsTalkState(dsWinId, 1);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
			}
			else		
			{
				BEEP_ERROR();
			}
		}
	}
}




const ICON_PIC_TYPE MonIconTable1[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
	ICON_PIC_UNLOCK2,
    ICON_PIC_TALK,
    ICON_PIC_QUAD,
};
const ICON_PIC_TYPE MonIconTable2[] = 
{
	ICON_PIC_Exit,
	ICON_PIC_REC,
	ICON_PIC_UNLOCK1,
    ICON_PIC_TALK,
    ICON_PIC_QUAD,
};

static void IconListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < iconNum)
		{
			if(iconNum == 6)
			{
				pDisp->disp[pDisp->dispCnt].iconType = MonIconTable1[index];
			}
			else
			{
				pDisp->disp[pDisp->dispCnt].iconType = MonIconTable2[index];
			}
			
			pDisp->dispCnt++;
		}
	}
}

int GetMonIconNum()
{
	return iconNum;
}

void MonIconListShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	if(lockProperty.lock1Enable && lockProperty.lock2Enable)
	{
		iconNum = 6;
		listInit.listType = ICON_1X6;
		listInit.listCnt = 6;
		listInit.fun = IconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 160, 720, 960, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 128, 536, 768, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 150*96
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}

	}
	else
	{
		iconNum = 5;
		listInit.listType = ICON_1X5;
		listInit.listCnt = 5;
		listInit.fun = IconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 240, 720, 800, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 192, 536, 640, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 100*64
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}
	}
}
void MonIconListClose(void)
{
	if( get_pane_type() == 0 )//ix481
		API_ListAlpha_Close(1, 160, 720, 960, 80, OSD_LAYER_CURSOR);
	else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47
		API_ListAlpha_Close(1, 128, 536, 768, 64, OSD_LAYER_CURSOR);
	else if(get_pane_type() == 5)	//ix470v
		API_ListAlpha_Close(1, 0, 832, 600, 192, OSD_LAYER_CURSOR);
	MenuListDisable(0);

}


void MENU_028_Monitor2_Init(int uMenuCnt)		
{
	char name_temp[41];
	GetLockProperty(GetDsIp(dsWinId), &lockProperty);
	MonIconListShow();
	SetLayerAlpha(8);
	API_TimeLapseStart(COUNT_RUN_UP , 0);
	GetDs_MonName(dsWinId, name_temp);
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, COLOR_WHITE, name_temp, strlen(name_temp), CallMenuDisp_Name_Font, STR_UTF8, 0);
	//DisplayLockSprite(Ds_Menu_Para[dsWinId].dsIp);
	//Quad_Monitor_Timer_Init();
	linphone_becall_cancel();
	API_Business_Request(Business_State_MonitorIpc);
	SetDsTalkState(dsWinId, 0);
	if( GetDsTalkState(dsWinId) == 0 && api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		MonTalkProcess();
	}
	// lzh_20220411 test
	//API_LocalCaptureOn();
}

void MENU_028_Monitor2_Exit(void)
{
	MonIconListClose();
	API_Business_Close(Business_State_MonitorIpc);
	// lzh_20220411 test
	//API_LocalCaptureOff();
}

void MENU_028_Monitor2_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char free[20]={0};
	char cpu[30]={0};
	struct {char onOff; char win;} *data;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					MonTalkProcess();
					break;			
				case KEY_UNLOCK:
					if(lockProperty.lock1Enable)		//czn_20191123
					{
						if(DsUnlock1Deal(dsWinId) == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
						}
					}
					break;
				case KEY_UNLOCK2:
					if(lockProperty.lock2Enable)		//czn_20191123
					{
						if(DsUnlock2Deal(dsWinId) == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
						}
					}
					break;
				case KEY_POWER:
					//Monitor_DtTalkCloseReq_Deal();	
					//CloseOneMenu();
					//close_monitor_client(); 
					break;

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			if(GetCurIcon() == 0)
			{
				if(GetMenuListEn(0) == 1)
					MonIconListClose();
				else
					MonIconListShow();
			}
			else
			{
				switch(GetCurIcon())
				{
					// lzh_20181108_s
					if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
					{
						API_menu_create_bitmap_file(-1,-1,-1);				
					}
					// lzh_20181108_e
					case ICON_888_ListView:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
						if(listIcon.mainIcon == 0)
						{
							if(GetDsTalkState(dsWinId))
							{
								SetDsTalkState(dsWinId, 0);
								DsTalkClose(dsWinId);
							}
							ExitQuadMenu();
							popDisplayLastMenu();
						}
						else if(listIcon.mainIcon == 1)
						{
							if(api_record_start(dsWinId)==0)
								SaveVdRecordProcess(dsWinId);
						}
						else if(listIcon.mainIcon == 2)
						{
							if(DsUnlock1Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
							}
						}
						else
						{
							if(iconNum == 6)
							{
								if(listIcon.mainIcon == 3)
								{
									if(DsUnlock2Deal(dsWinId) == 0) 	//czn_20161227
									{
										API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
									}
								}
								else if(listIcon.mainIcon == 4)
								{
									MonTalkProcess();
								}
								else
								{
									EnterQuadMenu(dsWinId, NULL, 0);
								}
							}
							else
							{
								if(listIcon.mainIcon == 3)
								{
									MonTalkProcess();
								}
								else
								{
									EnterQuadMenu(dsWinId, NULL, 0);
								}
							}
						}
						break;
					#if 0
					case ICON_207_MonitorPageDown:
						EnterQuadMenu(dsWinId, NULL, 0);
						break;
					case ICON_233_MonitorPageUp:
						break;
					case ICON_208_MonitorCapture:
						break;
				
					case ICON_209_MonitorUnlock1:
						if(lockProperty.lock1Enable)		//czn_20191123
						{
							if(DsUnlock1Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
							}
						}
						break;			
					case ICON_210_MonitorUnlock2:
						if(lockProperty.lock2Enable)		//czn_20191123
						{
							if(DsUnlock2Deal(dsWinId) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
							}
						}
						break;
					case ICON_211_MonitorTalk:
						if(Ds_Menu_Para[dsWinId].talk_flag)
						{
							Ds_Menu_Para[dsWinId].talk_flag = 0;
							DsTalkClose(dsWinId);
							ExitQuadMenu();
							popDisplayLastMenu();
						}
						else
						{
							if(CheckTalkOnoff() == 0)
							{
								Ds_Menu_Para[dsWinId].talk_flag = 1;
								DsTalkDeal(dsWinId);
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
							}
						}
						break;
						
					case ICON_212_MonitorReturn:
						//Monitor_DtTalkCloseReq_Deal();	//czn_20190529
						ExitQuadMenu();
						popDisplayLastMenu();
						break;
					#endif	
				}	
								
			}

		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{

			case MSG_7_BRD_SUB_MonitorOff:
				if(GetDsTalkState(dsWinId))
				{
					SetDsTalkState(dsWinId, 0);
					DsTalkClose(dsWinId);
				}
				ExitQuadMenu();
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_GetCpuMemUse:
				GetCpuMem_Polling(free, cpu);
				API_OsdStringDisplayExt(bkgd_w-100, 10, DISPLAY_STATE_COLOR, free, strlen(free), 0, STR_UTF8, 0);	
				API_OsdStringDisplayExt(bkgd_w-250, 50, DISPLAY_STATE_COLOR, cpu, strlen(cpu), 0, STR_UTF8, 0); 
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				vd_record_notice_disp(data->onOff, data->win);
				#if 0
				if(!data->onOff)
					SaveVdRecordProcess(dsWinId);
				#endif
				break;

			case MSG_7_BRD_SUB_MonitorStartting:
				//API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
				break;
				
			
			case MSG_7_BRD_SUB_MonitorTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;
			
			case MSG_7_BRD_SUB_MonitorUnlock1:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				break;

			case MSG_7_BRD_SUB_MonitorUnlock2:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				break;
				
			// lzh_20191009_s
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP:
				// ���ժ��
				if(GetDsTalkState(dsWinId) == 0)
				{
					MonTalkProcess();
				}
				else
				{
					// ͨ��״̬�£������ֱ�ͨ��״̬��ˢ����Ƶͨ���л�
					api_plugswitch_control_update(2,1);
				}
				break;
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN:
				if(GetDsTalkState(dsWinId) == 1)
				{
					MonTalkProcess();
				}
				break;

			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
	
}


#if 0

OS_TIMER timer_monitor = {0};

//czn_20170112_s

int menu28_memo_flag = 0;
char menu28_memofilename[100];

int menu4_memo_flag = 0;
char menu4_memofilename[100];
Monitor_Menu_Para_t Monitor_Menu_Para;


void Monitor_Timer_Callback(void)		//czn_20170413
{
	SYS_BRD_MSG_TYPE brd_msg;
	if(++Monitor_Menu_Para.monitortimecnt >= Monitor_Menu_Para.monitorTime)
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
	#if  0
		brd_msg.head.msg_source_id	= MSG_ID_IOServer;
		brd_msg.head.msg_target_id	= MSG_ID_survey;
		brd_msg.head.msg_type		= MSG_7_BRD;
		brd_msg.head.msg_sub_type	= MSG_7_BRD_SUB_MONITOR_TIMEROVER;
		brd_msg.wparam				= 0;
		brd_msg.lparam				= 0;	
		API_add_message_to_suvey_queue(	(char*)&brd_msg, sizeof(SYS_BRD_MSG_TYPE) ); 
		OS_StopTimer(&timer_monitor);
	#endif
	}
	else
	{
		if(Monitor_Menu_Para.monitortimecnt%20 == 0)
		{
			AutoPowerOffReset();
		}
		OS_RetriggerTimer(&timer_monitor);
	}
	
}
void Start_Monitor_Menu_ByPara(unsigned short gw_id,unsigned short vres_id,char *disp,char *para_buff)	//czn_20190127
{
	unsigned short monitorTime;
	int monitor_result;
	char temp[20];

	API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	
	Monitor_Menu_Para.gw_id = gw_id;
	Monitor_Menu_Para.target_ip = 0;
	Monitor_Menu_Para.vres_id = vres_id;
	Monitor_Menu_Para.talk_flag = 0;

	if(para_buff !=  NULL)
	{
		strcpy(Monitor_Menu_Para.input,para_buff);
	}
	else
	{
		strcpy(Monitor_Menu_Para.input,"0000000100");
	}
	
	get_device_addr_and_name_disp_str(0, Monitor_Menu_Para.input, NULL, NULL, disp, Monitor_Menu_Para.name);
		
	monitorTime = 0;
	API_talk_off(); 	//czn_201704010
	API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, temp);
	monitorTime = atoi(temp);
	
	GetIpRspData data;

	
	if(API_GetIpNumberFromNet(Monitor_Menu_Para.input, NULL, NULL, 2, 1, &data) != 0)
	{
		printf("API_GetIpNumberFromNet fail:%s \n",Monitor_Menu_Para.input);
		API_Beep(BEEP_TYPE_DI3);
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		return;
	}

	Monitor_Menu_Para.target_ip = data.Ip[0];
	//czn_20190107_s
	if(API_Business_Request(Business_State_MonitorDs) == 0)
	{
		API_Beep(BEEP_TYPE_DI3);
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		return;
	}
	//czn_20190107_e
	if( get_pane_type() == 1 )
	{
		Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h/2);
	}
	else
	{
		Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h);
	}
	if((monitor_result = open_monitor_client_remote(Monitor_Menu_Para.target_ip,0,REASON_CODE_MON,Monitor_Menu_Para.vres_id,monitorTime*2,&Monitor_Menu_Para.remote_ip,&Monitor_Menu_Para.remote_vres_id)) != 0)		//czn_20170109
	{
		//PromptBox_Hide(PromptBox_Linking);
		API_Beep(BEEP_TYPE_DI3);
		if(monitor_result == 1 || monitor_result == -1)
		{
			API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError1);
			//PromptBox_Hide(PromptBox_Fail);
		}
		else
		{
			API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError2);
			//PromptBox_Hide(PromptBox_Fail2);
		}
		usleep(2000000);
		API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
		return;
	}
	
	
	Monitor_Menu_Para.monitorTime = monitorTime;
	Monitor_Menu_Para.monitortimecnt = 0;		//czn_20170413
	if(timer_monitor.timer.callback != Monitor_Timer_Callback)
	{
		OS_CreateTimer(&timer_monitor, Monitor_Timer_Callback, 1000/25);
	}
	OS_SetTimerPeriod(&timer_monitor,1000/25);		//czn_20170413
	
	API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStart);
}

//czn_20170112_e

void Monitor_TalkOn(void)
{
	if(Monitor_DtTalkReq_Deal() == 0)	//czn_20170109
	{
		Monitor_Menu_Para.monitortimecnt = 0;	//czn_20170413
		API_TimeLapseStart(COUNT_RUN_UP,0);
		OS_SetTimerPeriod(&timer_monitor,1000/25);	//czn_20170413
		OS_RetriggerTimer(&timer_monitor);
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
		Monitor_Menu_Para.talk_flag = 1;
		usleep(1000000);
		UpdateMenuValidTime();
	}
	else		//czn_20190529
	{
		BEEP_ERROR();
	}
}

void MonitorExit(void)
{



	CALL_RECORD_DAT_T record_temp;

	API_TimeLapseHide();
	API_TimeLapseStop();

	if(API_RecordEnd() != 0)
	{
		menu28_memo_flag = 0;
	}
	
	if(menu28_memo_flag == 1)
	{	
		menu28_memo_flag = 0;
		record_temp.type			= VIDEO_RECORD; 
		record_temp.subType 		= LOCAL;
		record_temp.property		= NORMAL;
		record_temp.target_id		= 0;
		record_temp.target_node = 0;	

		strcpy( record_temp.name, "---");
		GetMonitorName(record_temp.name);

		//strcpy( record_temp.name, Monitor_Menu_Para.name);
		
		strcpy( record_temp.input, "---");
		strcpy(record_temp.relation, menu28_memofilename);
		api_register_one_call_record( &record_temp );
	}


}
/*************************************************************************************************
04.
*************************************************************************************************/
void MENU_028_Monitor2_Init(int uMenuCnt)		//czn_20170328
{
	OS_RetriggerTimer(&timer_monitor);
	API_TimeLapseStart(COUNT_RUN_UP , 0);

	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, COLOR_WHITE, Monitor_Menu_Para.name, strlen(Monitor_Menu_Para.name), CallMenuDisp_Name_Font, STR_UTF8, 0);
	DisplayLockSprite(Monitor_Menu_Para.target_ip);

	#if 0
	if( get_pane_type() == 1 )
		DisplayPipImage();
	// lzh_20191009_s
	if(Monitor_Menu_Para.talk_flag == 0)//&& api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		Monitor_TalkOn();
	}	
	// lzh_20191009_e
	#endif
}

void MENU_028_Monitor2_Exit(void)
{
#if 1
	CALL_RECORD_DAT_T record_temp;
	
	if(API_RecordEnd() != 0)				//cao_20161230
	{
		menu4_memo_flag = 0;
	}
	Monitor_DtTalkCloseReq_Deal();	//czn_20190529
	close_monitor_client();		//czn_20170112		//api_video_c_service_turn_off();	//cao_20161230
	//api_onvif_stop_mointor_one_stream(0);
	API_TimeLapseStop();

	if(menu4_memo_flag == 1)
	{
		menu4_memo_flag = 0;
		record_temp.type			= VIDEO_RECORD; 
		record_temp.subType			= LOCAL;
		record_temp.property		= NORMAL;
		record_temp.target_id		= Monitor_Menu_Para.vres_id;
		record_temp.target_node	= Monitor_Menu_Para.gw_id;		//czn_20170329
		strcpy( record_temp.name, Monitor_Menu_Para.name);
		strcpy( record_temp.input, Monitor_Menu_Para.input);
		strcpy(record_temp.relation, menu4_memofilename);
		api_register_one_call_record( &record_temp );
	}
#endif
	//czn_20190107_s
	API_Business_Close(Business_State_MonitorDs);
	//czn_20190107_e
}

void MENU_028_Monitor2_Process(void* arg)
{



	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	CALL_RECORD_DAT_T record_temp;
	POS pos;
	int val_temp1,val_temp2;
	char disp[2] = {0};
	char* dispString;
	int len;
	//MON_ERR_CODE *pErrorCode;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					if(Monitor_Menu_Para.talk_flag == 0)
					{
						if(Monitor_DtTalkReq_Deal() == 0)	//czn_20170109
						{
							Monitor_Menu_Para.monitortimecnt = 0;	//czn_20170413
							//API_SpriteDisplay_XY(TALKING_SPRITE_POS_X,TALKING_SPRITE_POS_Y,SPRITE_TALKING);
							API_TimeLapseStart(COUNT_RUN_UP,0);
							OS_SetTimerPeriod(&timer_monitor,1000/25);	//czn_20170413
							OS_RetriggerTimer(&timer_monitor);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
							Monitor_Menu_Para.talk_flag = 1;
							usleep(1000000);
							UpdateMenuValidTime();
						}
						else		//czn_20190529
						{
							BEEP_ERROR();
						}
					}
					else
					{
						Monitor_DtTalkCloseReq_Deal();		//czn_20190529
						popDisplayLastMenu();
						#if 0
						if(close_monitor_client() == 0) 	//czn_20170109
						{
							popDisplayLastMenu();
						}
						#endif
						UpdateMenuValidTime();
					}
					
					break;			
				case KEY_UNLOCK:
					if(lockProperty.lock1Enable)		//czn_20191123
					{
						if(Monitor_DtUnlockReq_Deal() == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
						}
					}
					break;
				case KEY_UNLOCK2:
					if(lockProperty.lock2Enable)		//czn_20191123
					{
						if(Monitor_DtUnlock2Req_Deal() == 0)		//czn_20161227
						{
							API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
						}
					}
					break;
				case KEY_POWER:
					Monitor_DtTalkCloseReq_Deal();	
					CloseOneMenu();
					close_monitor_client(); 
					break;

				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				// lzh_20181108_s
				if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
				{
					API_menu_create_bitmap_file(-1,-1,-1);				
				}
				// lzh_20181108_e
				case ICON_207_MonitorPageDown:
					break;
				case ICON_233_MonitorPageUp:
					break;
				case ICON_208_MonitorCapture:
					usleep(500000);
					if(one_vd_record.state)
					{
						if(API_RecordEnd() != 0)
						{
							menu28_memo_flag = 0;
						}

						if(menu28_memo_flag == 1)
						{	
							menu28_memo_flag = 0;
							record_temp.type			= VIDEO_RECORD; 
							record_temp.subType		= LOCAL;
							record_temp.property		= NORMAL;
							record_temp.target_id		= 0;
							record_temp.target_node	= 0;	
							
							//strcpy( record_temp.name, "---");
							GetMonitorName(record_temp.name);

							//strcpy( record_temp.name, Monitor_Menu_Para.name);

							//Get_MonResName_ByAddr(Mon_Run.partner_IA, record_temp.name);
							strcpy( record_temp.input, "---");
							strcpy(record_temp.relation, menu28_memofilename);
							api_register_one_call_record( &record_temp );
						}
						
					}
					else
					{
						if(menu28_memo_flag == 1)
						{	
							menu28_memo_flag = 0;
							record_temp.type			= VIDEO_RECORD; 
							record_temp.subType			= LOCAL;
							record_temp.property		= NORMAL;
							record_temp.target_id		= 0;
							record_temp.target_node	= 0;	

							//strcpy( record_temp.name, "---");
							GetMonitorName(record_temp.name);

							//strcpy( record_temp.name, Monitor_Menu_Para.name);
							
							//Get_MonResName_ByAddr(Mon_Run.partner_IA, record_temp.name);
							strcpy( record_temp.input, "---");
							strcpy(record_temp.relation, menu28_memofilename);
							api_register_one_call_record( &record_temp );
						}
						
						if(memo_video_record_start(NULL) == 0)
						{
							strncpy(menu28_memofilename,one_vd_record.filename,100);
						}
					}
					break;

				case ICON_209_MonitorUnlock1:
					if(lockProperty.lock1Enable)		//czn_20191123
					{
						if(Monitor_DtUnlockReq_Deal() == 0)		//czn_20161227
						{
							//API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
						}
					}
					break;			
				case ICON_210_MonitorUnlock2:
					if(lockProperty.lock2Enable)		//czn_20191123
					{
						if(Monitor_DtUnlock2Req_Deal() == 0)		//czn_20161227
						{
							//API_Beep(BEEP_TYPE_DL1);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
						}
					}
					break;
				case ICON_211_MonitorTalk:
					if(Monitor_Menu_Para.talk_flag == 0)
					{
						if(Monitor_DtTalkReq_Deal() == 0)	//czn_20170109
						{
							Monitor_Menu_Para.monitortimecnt = 0;	//czn_20170413
							//API_SpriteDisplay_XY(TALKING_SPRITE_POS_X,TALKING_SPRITE_POS_Y,SPRITE_TALKING);
							API_TimeLapseStart(COUNT_RUN_UP,0);
							OS_SetTimerPeriod(&timer_monitor,1000/25);	//czn_20170413
							OS_RetriggerTimer(&timer_monitor);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
							Monitor_Menu_Para.talk_flag = 1;
							usleep(1000000);
							UpdateMenuValidTime();
						}
						else		//czn_20190529
						{
							//BEEP_ERROR();
						}
					}
					else
					{
						Monitor_DtTalkCloseReq_Deal();		//czn_20190529
						popDisplayLastMenu();
						#if 0
						if(close_monitor_client() == 0) 	//czn_20170109
						{
							popDisplayLastMenu();
						}
						#endif
						UpdateMenuValidTime();
					}
					break;
					
				case ICON_212_MonitorReturn:
					Monitor_DtTalkCloseReq_Deal();	//czn_20190529
					popDisplayLastMenu();
					#if 0
					if(close_monitor_client() == 0)		//czn_20170109
					{
						popDisplayLastMenu();
					}
					#endif
					UpdateMenuValidTime();
					break;
					
			}
		}
	}
	#if 1
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			#if 0
			case MSG_7_BRD_SUB_INFORM_FISH_EYE:
				API_FishEye_Inform_Process((char*)(arg+sizeof(SYS_WIN_MSG)), Mon_Run.partner_IA);
				break;
			#endif	

			case MSG_7_BRD_SUB_RECORD_NOTICE:
				display_recording_image(*(char*)(arg+sizeof(SYS_WIN_MSG)));
				if(!(*(char*)(arg+sizeof(SYS_WIN_MSG))))
				{
					if(API_RecordEnd() != 0)
					{
						menu28_memo_flag = 0;
					}
					
					if(menu28_memo_flag == 1)
					{	
						menu28_memo_flag = 0;
						record_temp.type			= VIDEO_RECORD; 
						record_temp.subType 		= LOCAL;
						record_temp.property		= NORMAL;
						record_temp.target_id		= 0;
						record_temp.target_node = 0;	
						//strcpy( record_temp.name, "---");
						GetMonitorName(record_temp.name);
						//strcpy( record_temp.name, Monitor_Menu_Para.name);
						strcpy( record_temp.input, "---");
						strcpy(record_temp.relation, menu28_memofilename);
						api_register_one_call_record( &record_temp );
					}
				}
				else
				{
					menu28_memo_flag = 1;
				}
				break;

			case MSG_7_BRD_SUB_MonitorStartting:
				API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
				break;
				
			#if 0	
			case MSG_7_BRD_SUB_RECV_QSW_RESID:
				menu28_QSW_state = 1;
				memcpy(QuardBuf,(arg+sizeof(SYS_WIN_MSG)),4);
				DisplayQswDev();
				break;

			

			case MSG_7_BRD_SUB_MonitorOn:
				API_SpriteClose(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				if(menu28_QSW_state)
				{
					DisplayQswDev();
				}
				break;
			#endif	
			case MSG_7_BRD_SUB_MonitorTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;
			#if 0
			case MSG_7_BRD_SUB_MonitorName:
				dispString = (char*)(arg+sizeof(SYS_WIN_MSG));
				len = *dispString;
				API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, ++dispString, len,CallMenuDisp_Name_Font, STR_UTF8, 0);
				break;
			#endif	
			case MSG_7_BRD_SUB_MonitorUnlock1:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				usleep(1000000);
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				break;

			case MSG_7_BRD_SUB_MonitorUnlock2:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				usleep(1000000);
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				break;
				
			case MSG_7_BRD_SUB_MonitorOff:
				//MonitorExit();
				Monitor_DtTalkCloseReq_Deal();	//czn_20190529
				popDisplayLastMenu();
				#if 0
				if(close_monitor_client() == 0) 	//czn_20170109
				{
					popDisplayLastMenu();
				}
				#endif
				UpdateMenuValidTime();
				break;
			// lzh_20191009_s
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_UP:
				// ���ժ��
				if(Monitor_Menu_Para.talk_flag == 0)
				{
					Monitor_TalkOn();
				}
				else
				{
					// ͨ��״̬�£������ֱ�ͨ��״̬��ˢ����Ƶͨ���л�
					//api_plugswitch_control_update(2,1);
				}
				break;
			case MSG_7_BRD_SUB_PLUGSWITCH_TAKE_DN:
				// ��ɹһ�
				if(Monitor_Menu_Para.talk_flag == 1)
				{
					Monitor_DtTalkCloseReq_Deal();		//czn_20190529
					if(close_monitor_client() == 0)		//czn_20170109
					{
						popDisplayLastMenu();
					}
					UpdateMenuValidTime();
				}				
				break;
			// lzh_20191009_e			
		#if 0
			case MSG_7_BRD_SUB_MonitorError:
				pErrorCode = (MON_ERR_CODE*)(arg+sizeof(SYS_WIN_MSG));
				switch(*pErrorCode)
				{
					case MON_LINK_ERROR:
					case MON_LINK_BUSY:
						MonitorExit();
						CloseOneMenu();
						break;
						
					case MON_LINK_ERROR1:
						API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_LinkError1);
						usleep(1500000);
						MonitorExit();
						popDisplayLastMenu();
						break;
						
					case MON_LINK_ERROR2:
						API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_LinkError2);
						usleep(1500000);
						MonitorExit();
						popDisplayLastMenu();
						break;
						
					case MON_LINK_ERROR3:
						API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_LinkError3);
						usleep(1500000);
						MonitorExit();
						popDisplayLastMenu();
						break;
						
					case MON_LINK_ERROR4:
					case MON_SWITCH_ERROR:
						API_SpriteClose(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
						break;
				}
				break;
			#endif	
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
	#endif


	
}
void Ds_record_stop(int index)
{
	int state = GetWinRecState(dsWinId);
	printf("+++++++++Ds_record_stop, state[%d]\n", state); 
	if( state )
	{
		state = 0;
		SetWinRecState(dsWinId, state);
		vd_record_inform(state, 0);	
	}
}

int Ds_record_start( void )		
{
	int recTime;
	int result = -1;
	char recfile[41];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	
	if(GetWinRecState(dsWinId) == 1)
	{
		API_RecordEnd_mux(dsWinId); 
		return 0;
	}
	
	SetWinRecFile(dsWinId, recfile);
	if(Judge_SdCardLink() == 1) 
	{
		recTime = 30;
	}
	else
	{
		recTime = 3;
	}
	result = API_RecordStart_mux(dsWinId, 0, 15, recfile, recTime, 0, Ds_record_stop);
	if( result == 0 )
	{
		SetWinRecState(dsWinId, 1);	//��ʼ¼��
		vd_record_inform(1, 0);	
	}
	return 0;
}

void SaveDsRecordProcess(void)
{
	CALL_RECORD_DAT_T record_temp;
	
	record_temp.type		= VIDEO_RECORD; 
	record_temp.subType 	= LOCAL;
	record_temp.property	= NORMAL;
	record_temp.target_node = 0;		
	record_temp.target_id	= 0;

	strcpy( record_temp.name, Ds_Menu_Para[dsWinId].dsName);
	strcpy( record_temp.input, "---");
	GetWinRecFile(dsWinId, record_temp.relation);
	api_register_one_call_record( &record_temp );
	
}

#endif



