#include "MENU_089_SoftwareUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "ota_FileProcess.h"
#include "MENU_public.h"

//int SwUpgrade_Code_Verify(char *code);
void SwUpgrade_Notice_Notice(int notice_type);
void SwUpgrade_CodeInfo_Disp(void);
void SwUpgrade_Update_Operation(int opt);
void SwUpgrade_CodeVer_Disp(char *app_ver);
void get_SwUpgradeAppCode(char *code);
int get_SwUpgradeSer(char *server_ip);
int get_upgrade_text_info1(char *file_name,char *fw_info,char *bref_info,int *file_length,char *app_code,char *app_ver,char *bsp_ver,char *res_ver);//czn_20190320
//int SwUprade_Installing(char *file_path);
//void Update_SwUpgradeServer_Select(int select);
//int get_upgrade_file_dir(int flie_type,int file_length,char *file_dir);
//int get_upgrade_text_info(char *file_name,char *fw_info,char *bref_info,int *file_length);
//void remove_upgrade_tempfile(void);
//void FwUpgrade_IoPara_Init(void);

#define	SwUpgrade_ICON_MAX	5
int SwUpgradeIconSelect;
int SwUpgradePageSelect;
//0,uncheck,1,checking,2 checked 3 tranfering 4tranfered/updating,5canceling

typedef enum
{
	SwUpgrade_State_Uncheck = 0,
	SwUpgrade_State_Checking,
	SwUpgrade_State_CheckOk,
	SwUpgrade_State_CheckErr,
	SwUpgrade_State_Downloading,
	SwUpgrade_State_Intalling,
	SwUpgrade_State_Canceling,
	SwUpgrade_State_Intalled,
}SwUpgrade_State_e;

static int SwUpgrade_State = SwUpgrade_State_Uncheck;

extern int FwUpgrade_Ser_Select;
extern char FwUpgrade_Ser_Disp[40];
extern char FwUpgrade_Code[7];//{"123456"};
extern char text_file_dir[60];
extern char fw_file_dir[60];
extern char FwUpgrade_server_ip[16];
extern char FwUpgrade_other_server[16];
extern int Fw_file_size;
extern int Fw_file_trans_size;
extern char trans_file_name[40];
extern OtaDlTxt_t dlTxtInfo;


typedef struct
{
	int iCon;
	int iConText; 
}SwUpgradeIcon;

const SwUpgradeIcon SwUpgradeIconTable[] = 
{
	//{ICON_FwUpgrade_Server,			MESG_TEXT_ICON_FwUpgrage_Server},    
	//{ICON_FwUpgrade_DLCode,			MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		NULL},
	{0,NULL},
	{ICON_FwUpgrade_Notice,			NULL},
	{ICON_FwUpgrade_Operation,		NULL},
};

const int SwUpgradeIconNum = sizeof(SwUpgradeIconTable)/sizeof(SwUpgradeIconTable[0]);
#if 0
static void DisplayFwUpgradePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];

	//API_DisableOsdUpdate();
	//API_OsdStringClearExt(x, y, bkgd_w-x, 40);//r
		
	for(i = 0; i < FwUpgrade_ICON_MAX; i++)
	{
		x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, DISPLAY_ICON_X_WIDTH_WITHOUT_VALUE-x, CLEAR_STATE_H);
		if(page*FwUpgrade_ICON_MAX+i < FwUpgradeIconNum)
		{
			x1 =x+ DISPLAY_VALUE_X_DEVIATION-210;
			y1 =y;//+ VALUE_DEVIATION_Y;
			switch(FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iCon)
			{
				case ICON_FwUpgrade_Server:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iConText, 1, x1-x-10);
					if(FwUpgrade_Ser_Disp[0]  != 0)
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, FwUpgrade_Ser_Disp, strlen(FwUpgrade_Ser_Disp), 1, STR_UTF8, bkgd_w-x1-20);
					break;
				
				case ICON_FwUpgrade_DLCode:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, FwUpgradeIconTable[i+page*FwUpgrade_ICON_MAX].iConText, 1, x1-x-10);
					if(FwUpgrade_Code[0]  != 0)
					API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, FwUpgrade_Code, strlen(FwUpgrade_Code), 1, STR_UTF8, bkgd_w-x1-20);
					break;
					
				case ICON_FwUpgrade_CodeInfo:
					break;
					
				case ICON_FwUpgrade_Notice:
					break;
					
				case ICON_FwUpgrade_Operation:
					if(FwUpgrade_State == FwUpgrade_State_Uncheck)
					{
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_FwUpgrage_Check,1,x1-x-10);
					}
					break;

			}
		}
	}
	pageNum = FwUpgradeIconNum/FwUpgrade_ICON_MAX + (FwUpgradeIconNum%FwUpgrade_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

#endif
//char SwUpgradeCode[7];
//char SwUpgradeSer[16];
int SwUpgrade_Ser_Select = 0;
#define Max_SwUpgrade_Ser_Select	1


void MENU_089_SwUpgrade_Init(int uMenuCnt)
{
	char display[100];
	SYS_VER_INFO_T sysinfo;
	char app_code[7];
	char app_ver[40];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	SwUpgrade_Ser_Select = 0;
	//API_MenuIconDisplaySelectOn(ICON_028_about);
	API_OsdStringClearExt(pos.x, hv.v/2, 300, CLEAR_STATE_H);
	get_SwUpgradeAppCode(FwUpgrade_Code);

	SwUpgrade_State = SwUpgrade_State_Checking;
	printf("111111111111%s:%d\n",__func__,__LINE__);
	if(get_SwUpgradeSer(FwUpgrade_server_ip) == 0)
	{
		printf("111111111111%s:%d:%s:%s\n",__func__,__LINE__,FwUpgrade_server_ip,FwUpgrade_Code);
		SwUpgrade_Ser_Select = 1;
		BusySpriteDisplay(1);
		sysinfo = GetSysVerInfo();
		strcpy(text_file_dir,NAND_DOWNLOAD_PATH);
		printf("111111111111%s:%d:%s:%s\n",__func__,__LINE__,FwUpgrade_server_ip,FwUpgrade_Code);
		api_ota_ifc_file_download(FwUpgrade_server_ip,NAND_DOWNLOAD_PATH,FwUpgrade_Code,1,sysinfo.type,sysinfo.swVer,sysinfo.sn);
		SwUpgrade_State = SwUpgrade_State_Checking;
		SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
	}
	else
	{
		SwUpgrade_Notice_Notice(MESG_TEXT_SwUpgrage_NoUpdatesAvailable);
	}
	printf("111111111111%s:%d:%s:%s\n",__func__,__LINE__,FwUpgrade_server_ip,FwUpgrade_Code);
}

void MENU_089_SwUpgrade_Exit(void)
{
	//API_MenuIconDisplaySelectOff(ICON_025_general);
}

void MENU_089_SwUpgrade_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	char app_code[7];
	char app_ver[40];
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(SwUpgrade_State == SwUpgrade_State_Uncheck ||SwUpgrade_State == SwUpgrade_State_Intalled||SwUpgrade_State == SwUpgrade_State_CheckOk||SwUpgrade_State == SwUpgrade_State_CheckErr)
					{
						popDisplayLastMenu();
						remove_upgrade_tempfile();
					}
					if(SwUpgrade_State == SwUpgrade_State_Checking||SwUpgrade_State == SwUpgrade_State_Downloading)
					{
						SwUpgrade_State = SwUpgrade_State_Canceling;
						SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Canceling);
						api_ota_ifc_ftp_cancel();
					}
					
					
					break;
					
				//case ICON_201_PageDown:
					//PublicPageDownProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
				//	break;			
				//case ICON_202_PageUp:
				//	//PublicPageUpProcess(&setInstallerPageSelect, SetInstaller_ICON_MAX, setInstallerIconNum, (DispListPage)DisplaySetInstallerPageIcon);
				//	break;	
				#if 0	
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					SwUpgradeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					#if 0
					if(SwUpgradePageSelect*FwUpgrade_ICON_MAX+FwUpgradeIconSelect >= FwUpgradeIconNum)
					{
						return;
					}
					#endif
					switch(SwUpgradeIconTable[SwUpgradeIconSelect].iCon)
					{		
						case ICON_FwUpgrade_Operation:
							if(SwUpgrade_State == SwUpgrade_State_Uncheck||SwUpgrade_State == SwUpgrade_State_CheckErr)
							{
								#if 0
								if(FwUpgrade_Ser_Select == 4)
								{	if(Judge_SdCardLink() == 0)	//r
									{
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
									}
									else if(FwUpgrade_Code_Verify(FwUpgrade_Code) == 1)
									{
										sprintf(display,"%s/%s.txt",SdUpgradePath,FwUpgrade_Code);
										if(get_upgrade_text_info(display,fw_verify_info,fw_code_info,&Fw_file_size)==0)
										{
											if(FwDeviceTypeVerify(fw_verify_info)==0)
											{
												FwUpgrade_State = FwUpgrade_State_CheckOk;
												API_DisableOsdUpdate();
												FwUpgrade_CodeInfo_Disp();//API_OsdStringDisplayExt(38+80, 38+111*5, COLOR_BLACK, "check ok", strlen("check ok"),0,STR_UTF8,0);
												FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_CodeSize);
												FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
												API_EnableOsdUpdate();
											}
											else
											{
												FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrMismatchedType);
											}
										}
										else
										{
											FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoFile);
										}
									}
									else
									{
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrInvalidCode);
									}
								}
								else
								{
									if(FwUpgrade_Code_Verify(FwUpgrade_Code) == 1)
									{
										if((error_code=get_upgrade_file_dir(1,0,text_file_dir)) == 0)
										{
											sysinfo = GetSysVerInfo();
											api_ota_ifc_file_download(FwUpgrade_server_ip,text_file_dir,FwUpgrade_Code,1,sysinfo.type,sysinfo.swVer,sysinfo.sn);
											FwUpgrade_State = FwUpgrade_State_Checking;
											FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
										}
										else
										{
											if(error_code == -1)
											{
												FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
											}
											else
											{
												FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
											}
										}
									}
									else
									{
										FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrInvalidCode);
									}
								}
								#endif
							}
							else if(SwUpgrade_State == SwUpgrade_State_CheckOk)
							{
								if(SwUpgrade_Ser_Select == 0)
								{
									SwUpgrade_State = SwUpgrade_State_Intalling;
									SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
									SwUpgrade_Update_Operation(0);
									sprintf(display,"%s/%s.zip",SdUpgradePath,FwUpgrade_Code);
									BusySpriteDisplay(1);
									if(start_updatefile_and_reboot_forsdcard(0,display) >= 0)
									{
										BusySpriteDisplay(0);
										BEEP_CONFIRM();
										SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
										remove_upgrade_tempfile();
										usleep(1000000);
										//system("reboot");
										//HardwareRestar
										HardwareRestar();
									}
									else
									{
										BusySpriteDisplay(0);
										BEEP_ERROR();
										SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
									}
									SwUpgrade_State = SwUpgrade_State_Intalled;	
								}
								else
								{
									if((error_code = get_upgrade_file_dir(0,Fw_file_size,fw_file_dir)) == 0)
									{
										BusySpriteDisplay(1);
										sysinfo = GetSysVerInfo();
										api_ota_ifc_file_download(FwUpgrade_server_ip,fw_file_dir,FwUpgrade_Code,0,sysinfo.type,sysinfo.swVer,sysinfo.sn);
										SwUpgrade_State = SwUpgrade_State_Downloading;
										SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Cennecting);
										SwUpgrade_Update_Operation(0);
									}
									else
									{
										BEEP_ERROR();
										#if 0
										if(error_code == -1)
										{
											FwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_PleaseInsertSDCard);
										}
										else
										#endif
										{
											SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
										}
									}
								}
							}
							break;
							
					}
					break;
					#endif
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_OtaClientIfc_ActOk:
				if(SwUpgrade_State == SwUpgrade_State_Canceling)
				{
					sleep(1);
					popDisplayLastMenu();
					remove_upgrade_tempfile();
				}
				else if(SwUpgrade_State == SwUpgrade_State_Checking || SwUpgrade_State == SwUpgrade_State_Downloading)
				{	
					Fw_file_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					strncpy(trans_file_name,(char *)(arg+sizeof(SYS_WIN_MSG)+4),40);
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translated:
				if(SwUpgrade_State == SwUpgrade_State_Checking)
				{
					BusySpriteDisplay(0);
					sprintf(display,"%s/%s",text_file_dir,trans_file_name);
					if(CheckDownloadIsAllowed(display, &dlTxtInfo))
					{
						sysinfo = GetSysVerInfo();
						SwUpgrade_State = SwUpgrade_State_CheckOk;
						//will change
						if((error_code = get_upgrade_file_dir(0,Fw_file_size,fw_file_dir)) == 0)
						{
							BusySpriteDisplay(1);
							sysinfo = GetSysVerInfo();
							api_ota_ifc_ftp_start(FwUpgrade_server_ip,fw_file_dir,FwUpgrade_Code,0,sysinfo.type,sysinfo.swVer,sysinfo.sn);
							SwUpgrade_State = SwUpgrade_State_Downloading;
						}
						else
						{
							BEEP_ERROR();
							SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrNoSpace);
 						}
 					}
					else
					{
						SwUpgrade_Notice_Notice(MESG_TEXT_SwUpgrage_NoUpdatesAvailable);
						SwUpgrade_State = SwUpgrade_State_CheckErr;
					}
 				}
				else if(SwUpgrade_State == SwUpgrade_State_Downloading)
				{
					BusySpriteDisplay(1);
					BEEP_CONFIRM();
					SwUpgrade_State = SwUpgrade_State_Intalling;
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Installing);
					//sprintf(display,"%s/%s",fw_file_dir,FwUpgrade_Code);
					sprintf(display,"%s/%s",fw_file_dir,trans_file_name);
					if(FwUprade_Installing(display) >= 0)
					{
						BusySpriteDisplay(0);
						BEEP_CONFIRM();
						SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallOk);
						remove_upgrade_tempfile();
						usleep(1000000);
						//system("reboot");
						//HardwareRestar
						//HardwareRestar();
					}
					else
					{
						BusySpriteDisplay(0);
						BEEP_ERROR();
						SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_InstallFail);
						usleep(1000000);
					}
					SwUpgrade_State = SwUpgrade_State_Intalled;
				}
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Translating:
				if(SwUpgrade_State == SwUpgrade_State_Downloading)
				{
					Fw_file_trans_size = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					API_DisableOsdUpdate();
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_Downloading);
					API_EnableOsdUpdate();
				}
				//snprintf(disp,100,"downloading %d",ota_notice_para);
	
				AutoPowerOffReset();
				break;
			case MSG_7_BRD_SUB_OtaClientIfc_Error:
				#if 0
				if(SwUpgrade_State == SwUpgrade_State_Checking&&SwUpgrade_Ser_Select<Max_SwUpgrade_Ser_Select)
				{
					get_SwUpgradeSer(FwUpgrade_server_ip);
					SwUpgrade_Ser_Select ++;
					BusySpriteDisplay(1);
					sysinfo = GetSysVerInfo();
					strcpy(text_file_dir,NAND_DOWNLOAD_PATH);
					api_ota_ifc_file_download(FwUpgrade_server_ip,NAND_DOWNLOAD_PATH,FwUpgrade_Code,1,sysinfo.type,sysinfo.swVer,sysinfo.sn);
					return;
				}
				#endif
				BusySpriteDisplay(0);
				 if(error_code == -3&&SwUpgrade_State == SwUpgrade_State_Checking)
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_SwUpgrage_NoUpdatesAvailable);
					SwUpgrade_State = SwUpgrade_State_Uncheck;
					return;
				}
				//rsp -1:busy,-2:connect ser error,-3:no file,-4:download error -5:no sdcard
				
				//BEEP_ERROR();
				error_code = *((int*)(arg+sizeof(SYS_WIN_MSG)));
				if(error_code == -1)
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -2)
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrConnecting);
				}
				else if(error_code == -3)
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_SwUpgrage_NoUpdatesAvailable);
				}
				else if(error_code == -4)
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				else
				{
					SwUpgrade_Notice_Notice(MESG_TEXT_FwUpgrage_ErrDownload);
				}
				if(SwUpgrade_State == SwUpgrade_State_Checking)
				{
					SwUpgrade_State = SwUpgrade_State_Uncheck;
					//FwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
				}
				else if(SwUpgrade_State == SwUpgrade_State_Downloading)
				{
					SwUpgrade_State = SwUpgrade_State_CheckOk;
					SwUpgrade_Update_Operation(MESG_TEXT_FwUpgrage_Install);
				}
				break;
			case MSG_7_BRD_SUB_RecvSoftwareUpdateReq:
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//setInstallerPageSelect = 0;
		//DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}

void SwUpgrade_Notice_Notice(int notice_type)
{
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	int disp_flag = 0;
	POS pos;
	SIZE hv;

	OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
	if(get_pane_type() == 5 )
	{
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+5;
		x1 = x + 120;
		y1 = y + 40;
		API_OsdStringClearExt(x, y, bkgd_w-x, 80);
	}
	else
	{
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		x1 =x+ (hv.h - pos.x)/2;//DISPLAY_VALUE_X_DEVIATION-210;
		y1 =y;//+ VALUE_DEVIATION_Y;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	}
	
	
	switch(notice_type)
	{
	#if 0
		case MESG_TEXT_FwUpgrage_CodeSize:
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, notice_type, 1,x1-x-10);
			get_file_size_disp(Fw_file_size,disp);
			disp_flag = 1;
			API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x1-20);
			break;
	#endif

		case MESG_TEXT_FwUpgrage_Downloading:
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, notice_type, 1,bkgd_w-x-10);
			get_file_size_disp(Fw_file_size,disp);
			get_file_size_disp(Fw_file_trans_size,disp1);
			strcat(disp,"(");
			strcat(disp,disp1);
			strcat(disp,")");
			disp_flag = 1;
			API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, bkgd_w-x1-20);
			break;

		case MESG_TEXT_FwUpgrage_Cennecting:
			break;

		case MESG_TEXT_FwUpgrage_Installing:
			break;

		case MESG_TEXT_FwUpgrage_ErrConnecting:
			break;

		case MESG_TEXT_FwUpgrage_ErrNoSpace:
			break;
	}
	if(!disp_flag)
	API_OsdUnicodeStringDisplay(x, y, DISPLAY_STATE_COLOR, notice_type, 1,bkgd_w-x);
	
}

void SwUpgrade_CodeInfo_Disp(void)
{
#if 0
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	
	x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	y = DISPLAY_LIST_Y+0*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =x+ DISPLAY_VALUE_X_DEVIATION-210;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, CLEAR_STATE_W-x, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_FwUpgrage_CodeInfo, 1, x1-x-10);

	API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, fw_code_info, strlen(fw_code_info), 1, STR_UTF8, bkgd_w-x1-20);
#endif
}

void SwUpgrade_Update_Operation(int opt)
{
#if 0
	uint16 x, y,x1,y1;
	char disp[40];
	char disp1[40];
	
	x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	y = DISPLAY_LIST_Y+3*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =x+ DISPLAY_VALUE_X_DEVIATION-220;
	y1 =y;//+ VALUE_DEVIATION_Y;
	API_OsdStringClearExt(x, y, CLEAR_STATE_W-x, CLEAR_STATE_H);
	if(opt != 0)
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, opt, 1, DISPLAY_ICON_X_WIDTH);
#endif
}

void SwUpgrade_CodeVer_Disp(char *app_ver)
{
#if 0
	uint16 x, y,x1,y1;
	//char disp[40];
	//char disp1[40];
	
	x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
	y = DISPLAY_LIST_Y+1*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

	x1 =x+ DISPLAY_VALUE_X_DEVIATION-210;
	y1 =y;//+ VALUE_DEVIATION_Y

	API_OsdStringClearExt(x, y, CLEAR_STATE_W-x, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_SW_Ver, 1, x1-x-10);
	API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, app_ver, strlen(app_ver),1,STR_UTF8, bkgd_w-x1-20);
#endif

}
#if 0
#define	SwUpgradeIni	"/mnt/nand1-2/SwUpgrade.ini"
void get_SwUpgradeAppCode(char *code)
{
	char buff[100];
	char 	*pos1,*pos2;
	int		processState;	//处理状态
	int 	i, j;
	char sys_type[7]={0};
	char dev_type[7]={0};
	char cus_type[7]={0};
	
	//strcpy(code,"948100");
	//return;
	FILE *pf = fopen(SwUpgradeIni,"r");
	
	if(pf == NULL)
	{
		return;
	}
	
	for(processState = 0, memset(buff, 0, 100+1); feof(pf) == 0; memset(buff, 0, 100+1))
	{
		fgets(buff,100,pf);
		//去掉前面空格
		for(pos1 = buff; isspace(*pos1); pos1++);
		if(pos1!=buff)
		strcpy(buff, pos1);
		
		//去掉注释
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 = 0;
				break;
			}
		}

		//去掉等号后面空格
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1+1; *pos2!= 0 && isspace(*pos2); pos2++);
				if(pos2 != (pos1+1))
				strcpy(pos1+1, pos2);
			}
		}

		//去掉等号前面空格
		for(pos1 = buff+1; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; pos2 >= buff && isspace(*pos2); pos2--);
				if(pos2 != (pos1-1))
				strcpy(pos2+1, pos1);
			}
		}

		//去掉最后的空格
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;

		//printf("%s\n", buff);
		switch(processState)
		{
			case 0:
				if(strstr( buff, "[App code]" ) != NULL)
				{
					processState = 1;
				}
				#if 0
				if(strstr( buff, "[System type]" ) != NULL)
				{
					processState = 1;
				}
				else if(strstr( buff, "[Device type]")!= NULL)
				{
					processState = 2;
				}
				else if(strstr(buff, "[Customer type]")!= NULL)
				{
					processState = 3;
				}
				#endif
				break;
			case 1:
				if(strlen(buff) > 0)
				{
					strcpy(sys_type,buff);
					processState = 0;
				}
				break;
			case 2:
				if(strlen(buff) > 0)
				{
					strcpy(dev_type,buff);
					processState = 0;
				}
				break;
			case 3:
				//if(strlen(buff) > 0)
				{
					strcpy(cus_type,buff);
					processState = 0;
				}
				break;	
				
		}
	}
	fclose(pf);
	printf("get_SwUpgradeAppCode:%s %s %s\n",sys_type,dev_type,cus_type);
	//sprintf(buff,"%s%s%s",dev_type,cus_type,sys_type);
	
	if(FwUpgrade_Code_Verify(sys_type) == 1)
	{
		strcpy(code,sys_type);
	}
}

#define SwUpgradeSer1	"2easyip.com"
#define SwUpgradeSer2	"2easyip.cn"
char RemoteUpdateServer[16]={0};
void SetRemoteUpdateServer(char *ser)
{
	strcpy(RemoteUpdateServer,ser);
}

void DisableRemoteUpdateServer(void)
{
	memset(RemoteUpdateServer,0,16);
}
int get_SwUpgradeSer(char *server_ip)
{
	//FwUpgrade_Ser_Select=select;
	
	uint8 temp,str_buff[5];
	char ch[21];

	if(RemoteUpdateServer[0]!=0)
	{
		strcpy(server_ip,RemoteUpdateServer);
		return 0;
	}
	
	API_Event_IoServer_InnerRead_All(FWUPGRADE_SER_SELECT, (uint8*)str_buff);
	temp = atoi(str_buff);
	//FwUpgrade_Ser_Select = temp;
	API_Event_IoServer_InnerRead_All(FWUPGRADE_OTHER_SER, (uint8*)ch);
	
	if(temp == 0)
	{
		strcpy(server_ip,ch);
	}
	else if(temp == 1)
	{
		//strcpy(FwUpgrade_Ser_Disp,"Server1[47.91.88.33]");
		//strcpy(FwUpgrade_server_ip,"47.91.88.33");
		strcpy(server_ip,"47.91.88.33");
		#if 0
		if(GetWifiConnected())
		{
			parse_remote_server(SwUpgradeSer1, NULL, server_ip);
		}
		#endif
	}
	else if(temp == 2)
	{
		strcpy(server_ip,"47.106.104.38");
		#if 0
		if(GetWifiConnected())
		{
			parse_remote_server(SwUpgradeSer2, NULL, server_ip);
		}
		#endif
	}
	else if(temp == 3)
	{
		//sprintf(FwUpgrade_Ser_Disp,"Server3[%s]",FwUpgrade_other_server);
		//strcpy(FwUpgrade_Ser_Disp,"Server3[192.168.2.60]");
		strcpy(server_ip,"192.168.2.60");
		//strcpy(FwUpgrade_other_server,other_server);
	}
	else if(FwUpgrade_Ser_Select == 4)
	{
		return -1;
	}

	return 0;
}

int get_upgrade_text_info1(char *file_name,char *fw_info,char *bref_info,int *file_length,char *app_code,char *app_ver,char *bsp_ver,char *res_ver)//czn_20190320
{
	#define PROCESS_END		100
	
	char 	buff[100+1] = {0};
	char 	*pos1,*pos2;
	int		processState;	//处理状态
	int 	i, j;
	

	if(fw_info!=NULL)
		*fw_info=0;
	if(bref_info!=NULL)
		*bref_info = 0;
	if(file_length!=NULL)
		*file_length = 0;
	if(app_code!=NULL)
		*app_code =0;
	if(app_ver!=NULL)
		*app_ver=0;
	if(bsp_ver!=NULL)
		*bsp_ver=0;
	if(res_ver!=NULL)
		*res_ver =0;

	//rcpy(fw_info,"ix50");
	//rcpy(bref_info,"IX50_Firmware");
	//ile_length = 24094572;
	printf("get_upgrade_text_info1: %s\n", file_name);
	FILE *pf = fopen(file_name,"r");

	if(pf == NULL)
	{
		return -1;
	}
	
	for(processState = 0, memset(buff, 0, 100+1); fgets(buff,100,pf) != NULL && processState != PROCESS_END; memset(buff, 0, 100+1))
	{
		//去掉前面空格
		for(pos1 = buff; isspace(*pos1); pos1++);
		if(pos1!=buff)
		strcpy(buff, pos1);
		
		//去掉注释
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '#')
			{
				*pos1 = 0;
				break;
			}
		}

		//去掉等号后面空格
		for(pos1 = buff; *pos1 != 0; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1+1; *pos2!= 0 && isspace(*pos2); pos2++);
				if(pos2 != (pos1+1))
				strcpy(pos1+1, pos2);
			}
		}

		//去掉等号前面空格
		for(pos1 = buff+1; *pos1 != 0 && buff[0] != '='; pos1++)
		{
			if(*pos1 == '=' || *pos1 == '[' || *pos1 == ']')
			{
				for(pos2 = pos1-1; pos2 >= buff && isspace(*pos2); pos2--);
				if(pos2 != (pos1-1))
				strcpy(pos2+1, pos1);
			}
		}

		//去掉最后的空格
		for(pos1 = buff; *pos1 != 0; pos1++);
		for(pos2 = pos1-1; isspace(*pos2); pos2--);
		*(pos2+1) = 0;

		printf("%s\n", buff);
		switch(processState)
		{
			case 0:
				if(strstr( buff, "[Device type]" ) != NULL)
				{
					processState = 1;
				}
				else if(strstr( buff, "[Code info]" )!= NULL)
				{
					processState = 2;
				}
				else if(strstr( buff, "[Code size]" )!= NULL)
				{
					processState = 3;
				}
				else if(strstr( buff, "[App Code]" )!= NULL)
				{
					processState = 4;
				}
				else if(strstr( buff, "[App Ver]" )!= NULL)
				{
					processState = 5;
				}
				else if(strstr( buff, "[Bsp Ver]" )!= NULL)
				{
					processState = 6;
				}
				else if(strstr( buff, "[Res Ver]" )!= NULL)
				{
					processState = 7;
				}
				break;
			case 1:
				if(strlen(buff) > 0)
				{
					if(fw_info!=NULL)
					strcpy(fw_info,buff);
					processState = 0;
				}
				break;
			case 2:
				if(strlen(buff) > 0)
				{
					if(bref_info!= NULL)
					strcpy(bref_info,buff);
					processState = 0;
				}
				break;
			case 3:
				if(strlen(buff) > 0)
				{
					if(file_length!=NULL)
					*file_length = atoi(buff);//strcpy(bref_info,buff);
					processState = 0;
				}
				break;	
			case 4:
				if(strlen(buff) > 0)
				{	
					if(app_code!=NULL)
					strcpy(app_code,buff);
					processState = 0;
				}
				break;
			case 5:
				if(strlen(buff) > 0)
				{
					if(app_ver!=NULL)
					strcpy(app_ver,buff);
					processState = 0;
				}
				break;
			case 6:
				if(strlen(buff) > 0)
				{
					if(bsp_ver!=NULL)
					strcpy(bsp_ver,buff);
					processState = 0;
				}
				break;	
			case 7:
				if(strlen(buff) > 0)
				{
					if(res_ver!=NULL)
					strcpy(res_ver,buff);
					processState = 0;
				}
				break;	
				
		}
	}
	
	fclose(pf);
	//printf("%s %s,%d,%d\n",fw_info,bref_info,file_length,processState);
	//if(processState != PROCESS_END)
	//	return -1;
	
	return 0;
}
#endif
