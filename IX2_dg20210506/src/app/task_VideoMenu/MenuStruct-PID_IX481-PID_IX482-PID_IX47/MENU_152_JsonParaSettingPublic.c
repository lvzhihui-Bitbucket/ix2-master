#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "MENU_152_JsonParaSettingPublic.h"

JsonParaSettingMenuDispFunc_T JsonParaSettingMenuItemDispFunc = NULL;
JsonParaSettingMenuDispFunc_T JsonParaSettingMenuValueDispFunc = NULL;


JsonParaSettingSelectFunc_T JsonParaSettingSelectFunc = NULL;

JsonParaSettingReturnFunc_T JsonParaSettingReturnFunc = NULL;

JsonParaSettingSaveFunc_T JsonParaSettingSaveFunc = NULL;

JsonParaSettingInformFilterFunc_T JsonParaSettingInformFilterFunc = NULL;

JsonParaSettingListRightDispFunc_T JsonParaSettingListRightDispFunc = NULL;

JsonParaSettingListRightSelectFunc_T JsonParaSettingListRightSelectFunc=NULL;

cJSON *JsonParaSettingCurNode = NULL;
int JsonParaSettingCurNodeIndex = 0;
int JsonParaSettingCurItemNums = 0;
int JsonParaSettingCurPage = 0;
int JsonParaSettingCurLine = 0;
int JsonParaSettingToKeypad = 0;
int JsonParaSettingHaveChange = 0;
int JsonParaSettingDispMode = 0;

char JsonParaSettingTitleStr[40];
int JsonParaSettingTitleUnicodeLen = 0;



void Set_JsonParaSettingMenuItemDispFunc(JsonParaSettingMenuDispFunc_T func)
{
	JsonParaSettingMenuItemDispFunc = func;
}

void Set_JsonParaSettingMenuValueDispFunc(JsonParaSettingMenuDispFunc_T func)
{
	JsonParaSettingMenuValueDispFunc = func;
}

void Set_JsonParaSettingMenuSelectFunc(JsonParaSettingSelectFunc_T func)
{
	JsonParaSettingSelectFunc = func;
}

void Set_JsonParaSettingMenuReturnFunc(JsonParaSettingReturnFunc_T func)
{
	JsonParaSettingReturnFunc = func;
}
void Set_JsonParaSettingMenuSaveFunc(JsonParaSettingSaveFunc_T func)
{
	JsonParaSettingSaveFunc = func;
}
void Set_JsonParaSettingInformFilterFunc(JsonParaSettingInformFilterFunc_T func)
{
	JsonParaSettingInformFilterFunc= func;
}
void Set_JsonParaSettingListRightDispFunc(JsonParaSettingListRightDispFunc_T func)
{
	JsonParaSettingListRightDispFunc= func;
}

void Set_JsonParaSettingListRightSelectFunc(JsonParaSettingListRightSelectFunc_T func)
{
	JsonParaSettingListRightSelectFunc= func;
}

void Set_JsonParaSettingDispMode(int mode)
{
	JsonParaSettingDispMode = mode;
}

void Set_JsonParaSettingHaveChange(int val)
{
	JsonParaSettingHaveChange = val;
}

void Set_JsonParaSettingToKeypad(int val)
{
	JsonParaSettingToKeypad = val;
}

void Set_JsonParaSettingCurItemNums(int val)
{
	JsonParaSettingCurItemNums = val;
}

void Set_JsonParaSettingCurPage(int val)
{
	JsonParaSettingCurPage = val;
}

void Set_JsonParaSettingCurLine(int val)
{
	JsonParaSettingCurLine = val;
}

void Set_JsonParaSettingCurNodeIndex(int val)
{
	JsonParaSettingCurNodeIndex= val;
}

int Get_JsonParaSettingCurNodeIndex(void)
{
	return JsonParaSettingCurNodeIndex;
}

void Set_JsonParaSettingCurNode(cJSON *one_node)
{
	JsonParaSettingCurNode = one_node;
}

cJSON* Get_JsonParaSettingCurNode(void)
{
	return JsonParaSettingCurNode;
}

void Set_JsonParaSettingTitle(int len,char *title)
{
	JsonParaSettingTitleUnicodeLen = len;
	
	if(title!=NULL)
	{
		if(len>0)
			memcpy(JsonParaSettingTitleStr,title,len);
		else
			strcpy(JsonParaSettingTitleStr,title);
	}
	else
	{
		JsonParaSettingTitleStr[0]=0;
	}
}

static void DisplayJsonParaSettingPage(int page)
{
	uint8 i;
	uint16 x, y;
	int pageNum;
	char item_disp[100];
	int item_disp_unicode_len=0;
	char value_disp[100];
	int value_disp_unicode_len=0;
	int select_flag;

	
	// lzh_20181016_s	
	//API_DisableOsdUpdate();
	// lzh_20181016_e
	//printf("11111%s:%d\n",__func__,__LINE__);
	API_OsdStringClearExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_WIDTH, 40);
	//printf("11111%s:%d\n",__func__,__LINE__);
	if(JsonParaSettingTitleUnicodeLen>0||strlen(JsonParaSettingTitleStr)>0)
		API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleStr, (JsonParaSettingTitleUnicodeLen>0)?JsonParaSettingTitleUnicodeLen:strlen(JsonParaSettingTitleStr), 1, (JsonParaSettingTitleUnicodeLen>0)?STR_UNICODE:STR_UTF8, 0);
	#if 0
	if(JsonParaSettingTitleText!=0)
	{
		API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleText, 1, DISPLAY_TITLE_WIDTH);
	}
	else if(strlen(JsonParaSettingTitleStr)>0)
	{
		API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleStr, strlen(JsonParaSettingTitleStr), 1, STR_UTF8, DISPLAY_TITLE_WIDTH);
	}
	#endif
	//printf("11111%s:%d\n",__func__,__LINE__);
	for(i = 0; i < JsonParaSetting_ICON_MAX; i++)
	{
		printf("11111%s:%d:%d\n",__func__,__LINE__,i);
		
		//x = GetIconXY(ICON_007_PublicList1+i).x+DISPLAY_DEVIATION_X;
		//y = GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;
		x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		ListSelect(i,0);
		if(page*JsonParaSetting_ICON_MAX+i < JsonParaSettingCurItemNums)
		{
			if(JsonParaSettingDispMode == 0)
			{
				select_flag = 0;
				item_disp[0]= 0;
				value_disp[0]=0;
				item_disp_unicode_len = 0;
				value_disp_unicode_len = 0;
				if(JsonParaSettingMenuItemDispFunc!= NULL)
					JsonParaSettingMenuItemDispFunc(page*JsonParaSetting_ICON_MAX+i,item_disp,&item_disp_unicode_len);
				if(JsonParaSettingMenuValueDispFunc!= NULL)
					JsonParaSettingMenuValueDispFunc(page*JsonParaSetting_ICON_MAX+i,value_disp,&value_disp_unicode_len);
				
			}
			else
			{
				select_flag = 0;
				item_disp[0]= 0;
				value_disp[0]=0;
				item_disp_unicode_len = 0;
				value_disp_unicode_len = 0;
				if(JsonParaSettingMenuItemDispFunc!= NULL)
					select_flag = JsonParaSettingMenuItemDispFunc(page*JsonParaSetting_ICON_MAX+i,item_disp,&item_disp_unicode_len);
				//printf("!!!!!ListSelect %d :%d\n",i,select_flag);
				ListSelect(i,select_flag);
			}
			//API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, setGeneralIconTable[i+page*SetGeneral_ICON_MAX].iConText, 1, DISPLAY_ICON_X_WIDTH);
			if(item_disp_unicode_len>0||strlen(item_disp)>0)
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, item_disp, (item_disp_unicode_len>0)?item_disp_unicode_len:strlen(item_disp), 1, (item_disp_unicode_len>0)?STR_UNICODE:STR_UTF8, 0);
			x += 300;
			if(value_disp_unicode_len>0||strlen(value_disp)>0)
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, value_disp, (value_disp_unicode_len>0)?value_disp_unicode_len:strlen(value_disp), 1, (value_disp_unicode_len>0)?STR_UNICODE:STR_UTF8, 0);
			
		}
	}
	pageNum = JsonParaSettingCurItemNums/JsonParaSetting_ICON_MAX + (JsonParaSettingCurItemNums%JsonParaSetting_ICON_MAX ? 1 : 0);
	//printf("11111%s:%d\n",__func__,__LINE__);
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	//printf("11111%s:%d\n",__func__,__LINE__);
	// lzh_20181016_s	
	//API_EnableOsdUpdate();
	// lzh_20181016_e	
}
static void JsonParaSettingMenuListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	IPC_ONE_DEVICE ipcRecord;
	char name_temp[41];
	char name_temp2[41];
	char bdRmMs[11];
	int select_flag;
	int right_sprite;
	pDisp->dispCnt = 0;
	//bprintf("111111:%d\n",CallSceneIconSelect);
	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < JsonParaSettingCurItemNums)
		{
			if(JsonParaSettingMenuItemDispFunc!= NULL)
			{
				select_flag=JsonParaSettingMenuItemDispFunc(currentPage*onePageListMax+i,unicode_buf,&unicode_len);
				if(unicode_len==0)
				{
					pDisp->disp[pDisp->dispCnt].strLen=2*utf82unicode(unicode_buf,strlen(unicode_buf), pDisp->disp[pDisp->dispCnt].str);
					
				}
				else
				{
					memcpy(pDisp->disp[pDisp->dispCnt].str,unicode_buf,unicode_len);
					pDisp->disp[pDisp->dispCnt].strLen=unicode_len;
				}
				if(JsonParaSettingDispMode&&select_flag)
				{
					pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
				}
				else
					pDisp->disp[pDisp->dispCnt].preSprType=PRE_SPR_NONE;
			}
			if(JsonParaSettingMenuValueDispFunc!= NULL)
			{
				
				JsonParaSettingMenuValueDispFunc(currentPage*onePageListMax+i,unicode_buf,&unicode_len);
				if(unicode_len==0)
				{
					pDisp->disp[pDisp->dispCnt].valLen=2*utf82unicode(unicode_buf,strlen(unicode_buf), pDisp->disp[pDisp->dispCnt].val);
					
				}
				else
				{
					memcpy(pDisp->disp[pDisp->dispCnt].val,unicode_buf,unicode_len);
					pDisp->disp[pDisp->dispCnt].valLen=unicode_len;
				}
			}
			if(JsonParaSettingListRightDispFunc!=NULL)
			{
				right_sprite=JsonParaSettingListRightDispFunc(currentPage*onePageListMax+i);
				pDisp->disp[pDisp->dispCnt].sufSprType=right_sprite;
			}
			else
				pDisp->disp[pDisp->dispCnt].sufSprType==SUF_SPR_NONE;
		}
		else
		{
			pDisp->disp[pDisp->dispCnt].strLen=0;
			pDisp->disp[pDisp->dispCnt].valLen=0;
			pDisp->disp[pDisp->dispCnt].preSprType=PRE_SPR_NONE;
			pDisp->disp[pDisp->dispCnt].sufSprType==SUF_SPR_NONE;
		}
		pDisp->dispCnt++;
	}
		
	
}

// lzh_20181016_e
void JsonParaSettingMenuListShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	char temp[5];
	
	listInit = ListPropertyDefault();
	//if(JsonParaSettingDispMode)
		listInit.selEn = 1;
	//else
	//	listInit.selEn = 0;
	if(JsonParaSettingListRightDispFunc)
		listInit.ediEn = 1;
	else
		listInit.ediEn = 0;
	if(JsonParaSettingMenuValueDispFunc)
		listInit.valEn=1;
	listInit.listCnt = JsonParaSettingCurItemNums;
	
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textOffset = 0;
		listInit.textValOffset = 150;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textOffset = 0;
		listInit.textValOffset = 400;
	}
	if(JsonParaSettingTitleUnicodeLen>0||strlen(JsonParaSettingTitleStr)>0)
	{
		if(JsonParaSettingTitleUnicodeLen>0)
		{
			listInit.titleStr = JsonParaSettingTitleStr;
			listInit.titleStrLen = JsonParaSettingTitleUnicodeLen;
		}
		else
		{
			unicode_len=2*utf82unicode(JsonParaSettingTitleStr, strlen(JsonParaSettingTitleStr), unicode_buf);
			listInit.titleStr = unicode_buf;
			listInit.titleStrLen = unicode_len;
		}
	}
		//API_OsdStringDisplayExt(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, JsonParaSettingTitleStr, (JsonParaSettingTitleUnicodeLen>0)?JsonParaSettingTitleUnicodeLen:strlen(JsonParaSettingTitleStr), 1, (JsonParaSettingTitleUnicodeLen>0)?STR_UNICODE:STR_UTF8, 0);
	listInit.textSize = 0;
	//listInit.textAlign = 8;
	listInit.fun = JsonParaSettingMenuListPage;
	//listInit.titleStr = unicode_buf;
	//listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);

}

void JsonParaSettingDispReflush(void)
{
	API_DisableOsdUpdate();
	//printf("222222222%s:%d\n",__func__,__LINE__);
	//if(JsonParaSettingListRightDispFunc)
		JsonParaSettingMenuListShow();
	//else
	//	DisplayJsonParaSettingPage(JsonParaSettingCurPage);
	API_EnableOsdUpdate();
}
void ParaPublicSettingMenu_Init(void)
{
	MenuListDisable(0);
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	Set_JsonParaSettingCurItemNums(0);
	Set_JsonParaSettingCurPage(0);
	Set_JsonParaSettingCurLine(0);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	Set_JsonParaSettingTitle(0,NULL);
	
	
	Set_JsonParaSettingMenuItemDispFunc(NULL);
	Set_JsonParaSettingMenuValueDispFunc(NULL);
	Set_JsonParaSettingMenuSelectFunc(NULL);
	Set_JsonParaSettingMenuReturnFunc(NULL);
	Set_JsonParaSettingInformFilterFunc(NULL);
	Set_JsonParaSettingListRightDispFunc(NULL);
	Set_JsonParaSettingListRightSelectFunc(NULL);
}

void MENU_152_JsonParaSettingPublic_Init(int uMenuCnt)
{
	JsonParaSettingToKeypad = 0;
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	API_DisableOsdUpdate();
	//DisplayJsonParaSettingPage(JsonParaSettingCurPage);
	JsonParaSettingMenuListShow();
	API_EnableOsdUpdate();
}




void MENU_152_JsonParaSettingPublic_Exit(void)
{
	MenuListDisable(0);
	if(JsonParaSettingToKeypad == 0&&JsonParaSettingHaveChange)
	{
		if(JsonParaSettingSaveFunc!= NULL)
		{
			JsonParaSettingSaveFunc();
		}
	}
}

void MENU_152_JsonParaSettingPublic_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	int rev;
	int i;
	LIST_ICON listIcon;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					if(JsonParaSettingReturnFunc!= NULL)
					{
						JsonParaSettingReturnFunc();
					}
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				
				case ICON_201_PageDown:
					PublicPageDownProcess(&JsonParaSettingCurPage, JsonParaSetting_ICON_MAX, JsonParaSettingCurItemNums, (DispListPage)DisplayJsonParaSettingPage);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&JsonParaSettingCurPage, JsonParaSetting_ICON_MAX, JsonParaSettingCurItemNums, (DispListPage)DisplayJsonParaSettingPage);
					break;		
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					
					JsonParaSettingCurLine = GetCurIcon() - ICON_007_PublicList1;
					i = JsonParaSettingCurPage*JsonParaSetting_ICON_MAX + JsonParaSettingCurLine;
					bprintf("111111 %d:%d\n",i,JsonParaSettingCurItemNums);
					if(i >= JsonParaSettingCurItemNums)
					{
						return;
					}

					if(JsonParaSettingSelectFunc!= NULL)
					{
						bprintf("111111 %d:%d\n",i,JsonParaSettingCurItemNums);
						if(JsonParaSettingDispMode == 1)
						{
							rev = JsonParaSettingSelectFunc(i);
							ListSelect(JsonParaSettingCurLine,rev);
						}
						else
						{
							bprintf("111111 %d:%d\n",i,JsonParaSettingCurItemNums);
							JsonParaSettingSelectFunc(i);
						}
					}
					break;
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					//bprintf("!!!!!%d:%d\n",listIcon.mainIcon,callSceneIconNum);
					
					
					if(listIcon.mainIcon<0)
					{
						return;
					}
					JsonParaSettingCurLine = listIcon.mainIcon;
					if(listIcon.subIcon == 0)
					{
						
						if(JsonParaSettingSelectFunc!= NULL)
						{
							bprintf("111111 %d:%d\n",i,JsonParaSettingCurItemNums);
							if(JsonParaSettingDispMode == 1)
							{
								rev = JsonParaSettingSelectFunc(listIcon.mainIcon);
								JsonParaSettingMenuListShow();
							}
							else
							{
								bprintf("111111 %d:%d\n",i,JsonParaSettingCurItemNums);
								JsonParaSettingSelectFunc(listIcon.mainIcon);
							}
						}
					}
					else if(listIcon.subIcon == 1)
					{
						
						//Enter_CallTransferParaSettingRootMenu(0);
					}
					else if(listIcon.subIcon == 2)
					{
						if(JsonParaSettingListRightSelectFunc!=NULL)
						{
							JsonParaSettingListRightSelectFunc(listIcon.mainIcon);
							
						}
					}

					break;
				

			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


