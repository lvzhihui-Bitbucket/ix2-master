/**
  ******************************************************************************
  * @file    MENU_Keyboard.h
  * @author  caozhenbin
  * @version V1.0.0
  * @date    2014.04.09
  * @brief   This file contains the headers of the obj_Survey_Keyboard.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef OBJ_KBD_H
#define OBJ_KBD_H


/*******************************************************************************
                            Define Object Property 
*******************************************************************************/
#define	KEYBOARD_DISPLAY_X		        64
#define	KEYBOARD_DISPLAY_Y	    	    80 

#define	MAX_DISPLAY_ROW		        6
#define	MAX_DISPLAY_COL	    	    46 
#define	MAX_INPUTBUF_SIZE           100

#define	KEYPAD_CHAR                 1
#define	KEYPAD_NUM                  2
#define	KEYPAD_EDIT                 3
#define	KEYPAD_ALL                  4

#define	BACKSPACE_KEY_VALUE         8
#define	SET_HIGHLIGHT_KEY_VALUE     255
#define	CAPSLOCK_KEY_VALUE          254
#define	TURN_TO_NUM_KEY_VALUE       253
#define	TURN_TO_CHAR_KEY_VALUE      252
#define	RETURN_KEY_VALUE            251
#define OK_KEY_VALUE                250

typedef struct
{
    unsigned char dispStartRow;
    unsigned char dispStartCol;
    unsigned char dispMaxRow;
    unsigned char dispMaxCol;
    int 		  dispColor;
    unsigned char inputSize; 
    unsigned char highLight;
    unsigned char dispFullFlag;
    unsigned char cursor;
    unsigned char capsLock;
    unsigned char inputbuf[MAX_INPUTBUF_SIZE + 1];
    unsigned char char_num;
	int (*call_back_ptr)(char*);
}KbdProperty;

typedef struct
{
	int icon;
    int key_value;
} KeyProperty;

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
unsigned char API_KeyboardInit( unsigned short startRow,unsigned short StartCol,unsigned short dispMaxRow,
                                unsigned short dispMaxCol, int color);             
void API_KeyboardInputStart(unsigned char* pString, unsigned char highLight);
void API_KeyboardSetHighLight(unsigned char highLight);
unsigned char API_KeyboardInputOneKey(unsigned char Keypad_type,int icon);
unsigned char* API_KeyboardInputEnd(void);
unsigned char GetKeyboardcapsLock(void);

/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/
void KeyboardInputProcess(unsigned char key_value);
unsigned char OneKeyValue(unsigned char Keypad_type, int icon);
void ClearScrean(void);
void DisplayAllScreen(int iColor, unsigned char highLight,unsigned char cursor);

#endif



