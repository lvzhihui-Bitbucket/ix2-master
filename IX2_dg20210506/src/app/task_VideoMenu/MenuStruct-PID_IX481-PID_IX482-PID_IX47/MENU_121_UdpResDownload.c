#include "../../cJSON/cJSON.h"
#include "MENU_120_BackResDownload.h"
#include "MENU_121_UdpResDownload.h"
#include "MENU_066_FwUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_RemoteUpgrade.h"
#include "obj_PublicUdpCmd.h"
#include "obj_SearchIxProxy.h"
#include "MENU_public.h"

//0,uncheck,1,checking,2 checked 3 tranfering 4tranfered/updating,5canceling

static RES_UDP_DOWNLOAD_DATA_T resUdpDownloadRun = {0};

const IconAndText_t resUdpDLIconTable[] = 
{
	{ICON_FwUpgrade_Server, 		MESG_TEXT_ICON_FwUpgrage_Server},	 
	{ICON_FwUpgrade_DLCode, 		MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		MESG_TEXT_FwUpgrage_CodeInfo},
	{ICON_FwUpgrade_Notice, 		NULL},
};

const int resUdpDLIconTableNum = sizeof(resUdpDLIconTable)/sizeof(resUdpDLIconTable[0]);

static void DisplayResUdpDownLoadNotice(const char* state , const char* notice)
{
	uint16 x, y;
	char display[200];
	POS pos;
	SIZE hv;
	
	if(GetCurMenuCnt() != MENU_121_UdpResDownload)
	{
		return;
	}

	OSD_GetIconInfo(ICON_007_PublicList1+3, &pos, &hv);
	
	x = pos.x + DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2;

	API_DisableOsdUpdate();

	API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);

	snprintf(display, 200, "%s    %s", (state == NULL) ? "" : state, (notice == NULL) ? "" : notice);
	API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x);

	API_EnableOsdUpdate();
}


static void DisplayResUdpDownLoadPageIcon(const char* state , const char* notice)
{
	uint8 i;
	uint16 x, y,x1,y1;
	char display[100];
	POS pos;
	SIZE hv;

	if(GetCurMenuCnt() != MENU_121_UdpResDownload)
	{
		return;
	}

	API_DisableOsdUpdate();
		
	for(i = 0; i < resUdpDLIconTableNum; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		
		x = pos.x + DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2;

		x1 = x + (hv.h - pos.x)/2;
		y1 = y;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);

		switch(resUdpDLIconTable[i].iCon)
		{
			case ICON_FwUpgrade_Server:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resUdpDLIconTable[i].iConText, 1, x1-x-10);
				if(resUdpDownloadRun.server[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, resUdpDownloadRun.server, strlen(resUdpDownloadRun.server), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			
			case ICON_FwUpgrade_DLCode:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resUdpDLIconTable[i].iConText, 1, x1-x-10);
				if(resUdpDownloadRun.resId[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, resUdpDownloadRun.resId, strlen(resUdpDownloadRun.resId), 1, STR_UTF8, bkgd_w-x1-20);
				break;
				
			case ICON_FwUpgrade_CodeInfo:
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, resUdpDLIconTable[i].iConText, 1, x1-x-10);
				if(resUdpDownloadRun.fileName[0]  != 0)
				API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, resUdpDownloadRun.fileName, strlen(resUdpDownloadRun.fileName), 1, STR_UTF8, bkgd_w-x1-20);
				break;
			case ICON_FwUpgrade_Notice:
				snprintf(display, 200, "%s  %s", (state == NULL) ? "" : state, (notice == NULL) ? "" : notice);
				API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, bkgd_w-x);
				break;
		}
	}

	API_EnableOsdUpdate();

}
#if 0
char* CreateResUdpDownloadJsonData(RES_UDP_DOWNLOAD_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "server", data->server);
	cJSON_AddStringToObject(root, "resId", data->resId);
	cJSON_AddStringToObject(root, "fileName", data->fileName);
	cJSON_AddNumberToObject(root, "fileSize", data->fileSize);
	cJSON_AddNumberToObject(root, "downloadFileSize", data->downloadFileSize);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

int ParseResUdpDownloadReqJsonData(const char* json, RES_UDP_DOWNLOAD_DATA_T* pData)
{
    int status = 0;
	

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "server", pData->server, 40);
	ParseJsonString(root, "resId", pData->resId, 4);
	ParseJsonString(root, "fileName", pData->fileName, 40);
	ParseJsonNumber(root, "fileSize", &(pData->fileSize));
	ParseJsonNumber(root, "downloadFileSize", &(pData->downloadFileSize));

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}
#endif
int ResUdpDownloadInformOperation(int inform, void* arg)
{
	int ret = 0;
	char *state;
	int i;
	float downloadSize;
	SearchIxProxyRspData ixProxyData;
	
	switch(inform)
	{
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_START:
			resUdpDownloadRun.downloadFlag = 1;
			ParseResUdpDownloadReqJsonData((char*)GetMenuInformData(arg), &resUdpDownloadRun);
			
			API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
			resUdpDownloadRun.reportCnt = ixProxyData.deviceCnt;
			for(i = 0; i < ixProxyData.deviceCnt; i++)
			{
				resUdpDownloadRun.reportIp[i] = inet_addr(ixProxyData.data[i].ip_addr);
			}
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_DOWNLOADING;
			snprintf(resUdpDownloadRun.notice, 41, "%4.2f%%", (resUdpDownloadRun.downloadFileSize*1.0)/resUdpDownloadRun.fileSize);
			
			DisplayResUdpDownLoadPageIcon(resUdpDownloadRun.state, resUdpDownloadRun.notice);
			
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
			
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_RATES:
			resUdpDownloadRun.downloadFileSize = *((int*)GetMenuInformData(arg));

			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_DOWNLOADING;
			snprintf(resUdpDownloadRun.notice, 41, "%4.2f%%", (resUdpDownloadRun.downloadFileSize*100.0)/resUdpDownloadRun.fileSize);
			
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, resUdpDownloadRun.notice);
			
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_CANCEL:
			resUdpDownloadRun.downloadFlag = 0;
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_CANCEL;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, resUdpDownloadRun.notice);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			sleep(1);
			CloseMenu();
			break;

		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_ERROR:
			resUdpDownloadRun.downloadFlag = 0;
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_ERR_DOWNLOAD;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, resUdpDownloadRun.notice);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
			
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_END:
			resUdpDownloadRun.downloadFileSize = *((int*)GetMenuInformData(arg));
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_DOWNLOADING;
			snprintf(resUdpDownloadRun.notice, 41, "%4.2f%%", (resUdpDownloadRun.downloadFileSize*100.0)/resUdpDownloadRun.fileSize);
			
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, resUdpDownloadRun.notice);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
			
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_ERROR:
			resUdpDownloadRun.downloadFlag = 0;
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_INSTALL_FAIL;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, NULL);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
			
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_START:
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_INSTALLING;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, NULL);
			resUdpDownloadRun.notice[0] = 0;
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
			
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_UPDATE_OK:
			resUdpDownloadRun.downloadFlag = 0;
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_INSTALL_OK;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, NULL);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
		case MSG_7_BRD_SUB_RES_UDP_DOWNLOAD_WAITING_REBOOT:
			resUdpDownloadRun.downloadFlag = 0;
			resUdpDownloadRun.state = RES_DOWNLOAD_STATE_WAITING_REBOOT;
			DisplayResUdpDownLoadNotice(resUdpDownloadRun.state, NULL);
			IxReportResDownload(resUdpDownloadRun.reportIp, resUdpDownloadRun.reportCnt, "UDP", resUdpDownloadRun.state, resUdpDownloadRun.notice, resUdpDownloadRun.resId, resUdpDownloadRun.server, resUdpDownloadRun.resId);
			break;
		default:
			ret = -1;
			break;
	}

	return ret;
}



int GetResUdpDownloadState(void)
{
	if(resUdpDownloadRun.downloadFlag)
	{
		return 1;
	}

	return 0;	
}

void MENU_121_ResUdpDownload_Init(int uMenuCnt)
{
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, 0, 1);
	DisplayResUdpDownLoadPageIcon(resUdpDownloadRun.state, resUdpDownloadRun.notice);
}

void MENU_121_ResUdpDownload_Exit(void)
{

}

void MENU_121_ResUdpDownload_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
				case ICON_047_Home:
					CloseMenu();
					break;
					
				case ICON_201_PageDown:
					break;			
				case ICON_202_PageUp:
					break;	
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		ResUdpDownloadInformOperation(pglobal_win_msg->status, arg);
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
	}
}



