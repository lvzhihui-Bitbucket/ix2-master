#include "MENU_public.h"
#include "MENU_081_OutdoorStation.h"
#include "obj_GetIpByNumber.h"
#include "obj_OS_SipCMD.h"
#include "obj_OS_ListTable.h"

#define	OUT_DOORSTATION_ICON_MAX							5

int outDoorStationIconSelect;
int outDoorStationPageSelect;
int outDoorStationIndex;


void DisplayOneOutDoorStationlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char display[100];
	
	if(index < GetOS_ListTempTableNum())
	{
		OS_ListRecord_T data;
		if(GetOS_ListTempRecordItems(index, &data) == 0)
		{
			if(data.sipState)
			{
				//ListSelect(index, 1);
			}
			
			data.BD_RM_MS[10] = 0;
			get_device_addr_and_name_disp_str(0, data.BD_RM_MS, NULL, NULL, !strcmp(data.R_Name, "-") ? data.name1: data.R_Name, display);

			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
		}
	}
}


void DisplayOnePageOutDoorStationlist(uint8 page)
{
	int i, maxPage;
	uint16 x, y, val_x;
	int list_start;
	POS pos;
	SIZE hv;

	int monres_num = GetOS_ListTempTableNum();
	

	list_start = page*OUT_DOORSTATION_ICON_MAX;

	for( i = 0; i < OUT_DOORSTATION_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		ListSelect(i, 0);

		if((list_start+i) < monres_num)
		{
			DisplayOneOutDoorStationlist(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, val_x - x);
		}
	}
	
	
	maxPage = (monres_num%OUT_DOORSTATION_ICON_MAX) ? (monres_num/OUT_DOORSTATION_ICON_MAX+1) : (monres_num%OUT_DOORSTATION_ICON_MAX);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}



void MENU_081_OutdoorStation_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	outDoorStationIconSelect = 0;
	outDoorStationPageSelect = 0;
	API_MenuIconDisplaySelectOn(ICON_051_ExternalUnit);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_OutdoorStations, 1, 0);
	DisplayOnePageOutDoorStationlist(outDoorStationPageSelect);
}


void MENU_081_OutdoorStation_Exit(void)
{

}

void MENU_081_OutdoorStation_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					outDoorStationIconSelect = GetCurIcon() - ICON_007_PublicList1;
					outDoorStationIndex = outDoorStationPageSelect*OUT_DOORSTATION_ICON_MAX + outDoorStationIconSelect;

					if(outDoorStationIndex < GetOS_ListTempTableNum())
					{
						char room[11] = {0};
						char temp[200] = {0};
						GetIpRspData data;
						OS_ListRecord_T record;
						
						if(GetOS_ListTempRecordItems(outDoorStationIndex, &record) != 0)
						{
							return;
						}
						
						if(API_GetIpNumberFromNet(record.BD_RM_MS, NULL, NULL, 1, 1, &data) != 0)
						{
							return;
						}
						
						strcpy(room, record.BD_RM_MS+8);
						snprintf(temp, 200, "%s%d %s", "OS", atoi(room) < 50 ? atoi(room) : atoi(room)-50, record.name1);
						
						if(GetSavedIcon() == ICON_OutdoorStations)
						{
							SetOnlineManageTitle(temp);
							SetOnlineManageMFG_SN(record.MFG_SN);
							SetSearchOnlineIp(data.Ip[0]);
							SetOnlineManageNbr(record.BD_RM_MS);
							EnterOnlineManageMenu(MENU_034_ONLINE_MANAGE_INFO, 1);	
						}
						else
						{
							//extern char installerInput[9];
							//devIp = data.Ip[0];
							//strcpy(devName, temp);
							//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputManagerPassword, installerInput, 8, COLOR_WHITE, NULL, 0, PwdManageVerifyPassword);
							EnterPwdManageMenu(data.Ip[0], temp);
						}
						
					}
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&outDoorStationPageSelect, OUT_DOORSTATION_ICON_MAX, GetOS_ListTempTableNum(), (DispListPage)DisplayOnePageOutDoorStationlist);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&outDoorStationPageSelect, OUT_DOORSTATION_ICON_MAX, GetOS_ListTempTableNum(), (DispListPage)DisplayOnePageOutDoorStationlist);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

int OS_SipConfigRemoteCmd(int cmd)
{
	int ret, i;
	SipCfgResult cfgData;
	char BD_RM_MS[11];

	sprintf(BD_RM_MS, "%s%s00", GetSysVerInfo_bd(), GetSysVerInfo_rm());

	if(API_SipConfigCmd(cmd, BD_RM_MS, &cfgData, 15) == 0)
	{
		for(i = 0, ret = 0; i < cfgData.cnt; i++)
		{
			if(cfgData.device[i].result != 0)
			{
				ret = -1;
				break;
			}
		}
	}
	else
	{
		ret = -1;
	}

	return ret;
}



