#include "MENU_public.h"
#include "MENU_059_MonListManage.h"
#include "../../task_monitor/obj_ip_mon_vres_map_table.h"

#define	VIDEO_RESOURCE_ICON_MAX							5
#define MON_LIST_FILE_NAME								"/mnt/nand1-2/rid1014_VideoResource.csv"

int monManageIconSelect;
int monManagePageSelect;
int monManageIndex;
extern one_vtk_table* videoResourceTable;


void DisplayOneVideoResourcelist(int index, int x, int y, int color, int fnt_type, int width)
{
	char display[100];
	
	if(index < videoResourceTable->record_cnt)
	{
		MonRes_Entry_Stru data;
		if(get_monres_table_record_items(videoResourceTable, index, &data) == 0)
		{
			snprintf(display, 100, "[%s]%s", (data.enable ? "X" : "-"), data.res_name);
			API_OsdStringDisplayExt(x, y, color, display, strlen(display), fnt_type,STR_UTF8, width);
		}
	}
}


void DisplayOnePageVideoResourcelist(uint8 page)
{
	int i, x, y, maxPage;
	int list_start;
	POS pos;
	SIZE hv;

	int monres_num = videoResourceTable->record_cnt;
	

	list_start = page*VIDEO_RESOURCE_ICON_MAX;

	for( i = 0; i < VIDEO_RESOURCE_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		if((list_start+i) < monres_num)
		{
			DisplayOneVideoResourcelist(list_start+i, x, y, DISPLAY_LIST_COLOR, 1, (hv.h - pos.x)/2);
		}
	}
	
	
	maxPage = (monres_num%VIDEO_RESOURCE_ICON_MAX) ? (monres_num/VIDEO_RESOURCE_ICON_MAX+1) : (monres_num%VIDEO_RESOURCE_ICON_MAX);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
}

void ModifyOneVideoResourcelist(int index)
{
	
	if(index < videoResourceTable->record_cnt)
	{
		one_vtk_dat*	precord;
		one_vtk_dat 	record;
		char recordBuffer[200] = {0};
		int i = 0;
		char str[8][41] = {0};
		char *pos1;

		precord = get_one_vtk_record_without_keyvalue(videoResourceTable, index);

		memcpy(recordBuffer, precord->pdat, precord->len);
		strcpy(str[i++], strtok(recordBuffer,","));
		while(pos1 = strtok(NULL,",")) strcpy(str[i++], pos1);
		
		snprintf(recordBuffer,200,"%s,%s,%s,%s,%s,%s,%s,%s", str[0],str[1],str[2],str[3],str[4],str[5],str[6],(atoi(str[7]) ? "0" : "1"));
	
		record.len = strlen(recordBuffer);
		record.pdat = recordBuffer;

		Modify_one_vtk_record(&record, videoResourceTable, index);

		remove(MON_LIST_FILE_NAME);
	
		save_vtk_table_file_buffer( videoResourceTable, 0, videoResourceTable->record_cnt, MON_LIST_FILE_NAME);
		
	}
}


void MENU_059_monListManage_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_269_MonListSelect);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_MonitorListSelect,1, 0);
	DisplayOnePageVideoResourcelist(monManagePageSelect);
}


void MENU_059_monListManage_Exit(void)
{

}

void MENU_059_monListManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					monManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					monManageIndex = monManagePageSelect*VIDEO_RESOURCE_ICON_MAX + monManageIconSelect;

					if(monManageIndex >= videoResourceTable->record_cnt)
					{
						return;
					}
					ModifyOneVideoResourcelist(monManageIndex);
					DisplayOnePageVideoResourcelist(monManagePageSelect);
					break;
					
				case ICON_201_PageDown:
					PublicPageDownProcess(&monManagePageSelect, VIDEO_RESOURCE_ICON_MAX, videoResourceTable->record_cnt, (DispListPage)DisplayOnePageVideoResourcelist);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&monManagePageSelect, VIDEO_RESOURCE_ICON_MAX, videoResourceTable->record_cnt, (DispListPage)DisplayOnePageVideoResourcelist);
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}





