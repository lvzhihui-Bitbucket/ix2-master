#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"

cJSON *AlarmInformPara = NULL;
cJSON *AlarmInformGl = NULL;



void AlarmInformPara_Init(void)
{
	if(AlarmInformPara!=NULL)
	{
		cJSON_Delete(AlarmInformPara);
		AlarmInformPara = NULL;
		AlarmInformGl = NULL;
	}
	AlarmInformPara = cJSON_CreateObject();
	if(AlarmInformPara!=NULL)
	{
		AlarmInformGl = cJSON_CreateArray();
		if(AlarmInformGl != NULL)
		{
			cJSON_AddItemToObject(AlarmInformPara,"GL",AlarmInformGl);
		}
		else
		{
			cJSON_Delete(AlarmInformPara);
			AlarmInformPara = NULL;
		}
	}
}

void AlarmInformPara_Release(void)
{
	if(AlarmInformPara!=NULL)
	{
		cJSON_Delete(AlarmInformPara);
		AlarmInformPara = NULL;
		AlarmInformGl = NULL;
	}
}

int JudgeGlExistAlarmInformPara(char *gl_num)
{
	int i;
	char gl_num_temp[11];
	for(i=0;i<GetAlarmInformParaGlNums();i++)
	{
		if(GetAlarmInformParaGlItems(i,gl_num_temp)==0)
		{
			if(strcmp(gl_num,gl_num_temp) == 0)
				return 1;
		}
	}
	return 0;
}

void AddOneGlToAlarmInformPara(char *gl_num)
{
	cJSON *gl_node=NULL;
	
	
	if(AlarmInformGl!=NULL)
	{
		if(JudgeGlExistAlarmInformPara(gl_num) ==1)
			return;
		
		gl_node = cJSON_CreateObject();
		if(gl_node!=NULL)
		{
			cJSON_AddItemToArray(AlarmInformGl,gl_node);
			cJSON_AddStringToObject(gl_node, "ADDR", gl_num);
		}
	}
}

int  GetAlarmInformParaGlNums(void)
{
	if(AlarmInformGl==NULL)
	{
		if(AlarmInformPara==NULL)
			return 0;

		AlarmInformGl= cJSON_GetObjectItemCaseSensitive(AlarmInformPara,"GL");

		if(AlarmInformGl==NULL)
			return 0;
	}
	return cJSON_GetArraySize(AlarmInformGl);
}

int GetAlarmInformParaGlItems(int index,char *gl_num)
{
	if(index>=GetAlarmInformParaGlNums())
		return -1;
	cJSON *gl_node = cJSON_GetArrayItem(AlarmInformGl,index);
	cJSON *addr = cJSON_GetObjectItemCaseSensitive(gl_node, "ADDR");
	
	if (addr!=NULL&&cJSON_IsString(addr) && (addr->valuestring != NULL))
	{
		//printf("Checking ID \"%s\".\n", ID->valuestring);
		strcpy(gl_num, addr->valuestring);
		return 0;
	}
	return -1;
}

void DeleteOneGlFromAlarmInformPara(int index)
{
	if(index<GetAlarmInformParaGlNums())
	{
		cJSON_DeleteItemFromArray(AlarmInformGl,index);	
	}
}

void DeleteOneGlFromAlarmInformByAddr(char *gl_num)
{
	int i;
	char gl_num_temp[11];
	for(i=0;i<GetAlarmInformParaGlNums();i++)
	{
		if(GetAlarmInformParaGlItems(i,gl_num_temp)==0)
		{
			if(strcmp(gl_num,gl_num_temp) == 0)
			{
				cJSON_DeleteItemFromArray(AlarmInformGl,i);
				return;
			}
		}
	}
}

void AddAllGlToAlarmInformPara(void)
{
	int gl_nums=GetImNamelistGLNum();
	int i;
	IM_NameListRecord_T record;
	for(i = 0;i<gl_nums;i++)
	{
		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(i,&record) != 0)
				continue;
		}
		else
		{
			if(GetImNameListRecordItems(i,	&record) != 0)
				continue;
		}
		AddOneGlToAlarmInformPara(record.BD_RM_MS);
	}
	char *string = cJSON_Print(AlarmInformPara);
	if(string != NULL)
	{	
		printf("AlarmInformPara:\n%s\n", string);
		free(string);
	}
}
int AlarmInformIconSelect;
int AlarmInformPageSelect;
int AlarmInformMaxNum = 0;
#define	AlarmInform_ICON_MAX		5

void ModifyAlarmInformSelect(int index)
{
	IM_NameListRecord_T record;
	if(GetImNameListRecordCnt() == 0)
	{
		if(GetImNameListTempRecordItems(index,&record) != 0)
			return;
	}
	else
	{
		if(GetImNameListRecordItems(index,&record) != 0)
			return;
	}
	
	if(JudgeGlExistAlarmInformPara(record.BD_RM_MS)==1)
	{
		ListSelect(index%AlarmInformMaxNum,0);
		DeleteOneGlFromAlarmInformByAddr(record.BD_RM_MS);
	}
	else
	{
		ListSelect(index%AlarmInformMaxNum,1);
		AddOneGlToAlarmInformPara(record.BD_RM_MS);
	}
}

void DisplayOneAlarmInformlist(int index, int x, int y, int color, int fnt_type, int width)
{
	char recordBuffer[50] = {0};
	char oneRecord[50];
	IM_NameListRecord_T record;
	//IM_CookieRecord_T cRecord;
		
	if(index < AlarmInformMaxNum)
	{

		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(index,&record) != 0)
				return;
		}
		else
		{
			if(GetImNameListRecordItems(index,&record) != 0)
				return;
		}
		strcpy(oneRecord,  strcmp(record.R_Name, "-") ? record.R_Name : record.name1);
		get_device_addr_and_name_disp_str(0, record.BD_RM_MS, NULL, NULL, oneRecord, recordBuffer);
		
		API_OsdStringDisplayExt(x, y, color, recordBuffer, strlen(recordBuffer),fnt_type,STR_UTF8,width);
		if(JudgeGlExistAlarmInformPara(record.BD_RM_MS)==1)
		{
			ListSelect(index%AlarmInformMaxNum,1);
		}
	}
}

void DisplayOnePageAlarmInformService(uint8 page)
{
	//API_EnableOsdUpdate();

	CALL_RECORD_DAT_T call_record_value;
	int i, x, y, maxPage;
	int list_start;
	AlarmInformMaxNum = GetImNamelistGLNum();
	POS pos;
	SIZE hv;
	
	for( i = 0; i < AlarmInform_ICON_MAX; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X+4;//DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;//GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;//DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);

		//API_SpriteClose(DISPLAY_LIST_SELECT_X, y, SPRITE_Collect);

		list_start = page*AlarmInform_ICON_MAX+i;
		ListSelect(i,0);
		if(list_start < AlarmInformMaxNum)
		{
			DisplayOneAlarmInformlist(list_start, x, y, DISPLAY_LIST_COLOR, 1, DISPLAY_ICON_X_WIDTH);
		}
	}
	
	maxPage = AlarmInformMaxNum/AlarmInform_ICON_MAX + (AlarmInformMaxNum%AlarmInform_ICON_MAX ? 1 : 0);
	
 	//MenuListPageNum_Display(MENULIST_PAGENUM_POS_X, MENULIST_PAGENUM_POS_Y,COLOR_WHITE, page+1, maxPage);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);
	
	//API_EnableOsdUpdate();
}
void MENU_151_AlarmInformService_Init(int uMenuCnt)
{
	//if(GetLastNMenu() == MENU_003_INTERCOM)
	{
		AlarmInformIconSelect = 0;
		AlarmInformPageSelect = 0;

		
		//API_MenuIconDisplaySelectOn(gsListState);
		
		AlarmInformMaxNum =  GetImNamelistGLNum(); 
		if(AlarmInformMaxNum == 0)
		{
			API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
		}	
	}

	API_OsdUnicodeStringDisplay(DISPLAY_TITLE_X, DISPLAY_TITLE_Y, DISPLAY_TITLE_COLOR, MESG_TEXT_AlarmInformMode1, 1, DISPLAY_TITLE_WIDTH);
	DisplayOnePageAlarmInformService(AlarmInformPageSelect);
}




void MENU_151_AlarmInformService_Exit(void)
{
	
}

void MENU_151_AlarmInformService_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	int armed_delay;
	int i;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				
				case ICON_201_PageDown:
					PublicPageDownProcess(&AlarmInformPageSelect, AlarmInform_ICON_MAX, AlarmInformMaxNum, (DispListPage)DisplayOnePageAlarmInformService);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&AlarmInformPageSelect, AlarmInform_ICON_MAX, AlarmInformMaxNum, (DispListPage)DisplayOnePageAlarmInformService);
					break;		
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					
					AlarmInformIconSelect = GetCurIcon() - ICON_007_PublicList1;
					i = AlarmInformPageSelect*AlarmInform_ICON_MAX + AlarmInformIconSelect;
					if(i >= AlarmInformMaxNum)
					{
						return;
					}

					ModifyAlarmInformSelect(i);
					
					break;
				

			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_NameListUpdate:
				AlarmInformIconSelect = 0;
				AlarmInformPageSelect = 0;
				AddAllGlToAlarmInformPara();
				API_DisableOsdUpdate();
				DisplayOnePageAlarmInformService(AlarmInformPageSelect);
				API_EnableOsdUpdate();
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


