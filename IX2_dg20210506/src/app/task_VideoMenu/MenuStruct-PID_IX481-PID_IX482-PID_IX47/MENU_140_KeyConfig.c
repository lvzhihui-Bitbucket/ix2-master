
#include "MENU_public.h"
#include "MENU_139_KeySelect.h"


#define	KEY_Confing_ICON_MAX		5

int keyConfingIconSelect;
int keyConfingPageSelect;

const IconAndText_t keyConfingIconTable[] = 
{
	{ICON_ExtKeyCfgNoUse,						MESG_TEXT_ICON_ExtKeyCfgNoUse},
	{ICON_ExtKeyCfgGL,							MESG_TEXT_ICON_ExtKeyCfgGL},
	{ICON_ExtKeyCfgCancel,						MESG_TEXT_ICON_ExtKeyCfgCancel},
	{ICON_ExtKeyCfgInput,						MESG_TEXT_ICON_ExtKeyCfgInput},
	{ICON_ExtKeyCfgImList,						MESG_TEXT_ICON_ExtKeyCfgImList},
};

static int keyConfingIconNum = sizeof(keyConfingIconTable)/sizeof(keyConfingIconTable[0]);
extern EXT_MODULE_KEY_RECORD oneExtModuleKeyRecord;
static int keyConfingListSelectFlag;


int GetKeyConfigListSelectFlag(void)
{
	return keyConfingListSelectFlag;
}

void SetKeyConfigListSelectFlag(int flag)
{
	keyConfingListSelectFlag = flag;
}


int DisplayOnePageKeyConfing( int page )
{
	int i, x, y, len;
	EXT_MODULE_KEY_RECORD record;
	char display[20];
	POS pos;
	SIZE hv;
		
	for( i = 0; page*KEY_Confing_ICON_MAX + i < keyConfingIconNum; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
 		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, keyConfingIconTable[page*KEY_Confing_ICON_MAX + i].iConText, 1, hv.h - x);
	}
	
	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, 1);

	
	//API_EnableOsdUpdate();
}

int InputKeyConfigRoomNumber(const char* nbr)
{
	int len = strlen(nbr);
	if(len > 0 &&  len <= 10)
	{
		strcpy(oneExtModuleKeyRecord.number, nbr);
	}
	else
	{
		strcpy(oneExtModuleKeyRecord.number, "-");
	}
	printf("InputKeyConfigRoomNumber %s\n",nbr);
	AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
	popDisplayLastNMenu(2);
	return 2;
}

/*************************************************************************************************
010.
*************************************************************************************************/
void MENU_140_KeyConfig_Init(int uMenuCnt)
{
	keyConfingIconSelect = 0;
	keyConfingPageSelect = 0;
	DisplayOnePageKeyConfing(keyConfingPageSelect);
}

void MENU_140_KeyConfig_Exit(void)
{
}

void MENU_140_KeyConfig_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	EXT_MODULE_KEY_RECORD keyRecord;
	int i;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					keyConfingIconSelect = GetCurIcon() - ICON_007_PublicList1;
					switch(keyConfingIconTable[keyConfingPageSelect*KEY_Confing_ICON_MAX + keyConfingIconSelect].iCon)
					{
						case ICON_ExtKeyCfgNoUse:
							strcpy(oneExtModuleKeyRecord.number, "-");
							AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
							popDisplayLastMenu();
							break;
						case ICON_ExtKeyCfgGL:
							//snprintf(oneExtModuleKeyRecord.number, 11, "%s000051", GetSysVerInfo_bd());
							//AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
							//popDisplayLastMenu();
							SetKeyConfigListSelectFlag(1);
							StartInitOneMenu(MENU_112_GSList,0,1);
							break;
						case ICON_ExtKeyCfgCancel:
							strcpy(oneExtModuleKeyRecord.number, "*");
							AddOrModifyExtModuleKeyRecord(&oneExtModuleKeyRecord);
							popDisplayLastMenu();
							break;
						case ICON_ExtKeyCfgInput:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ExtKeyCfgInput, oneExtModuleKeyRecord.number, 10, COLOR_WHITE, oneExtModuleKeyRecord.number, 1, InputKeyConfigRoomNumber);
							break;
						case ICON_ExtKeyCfgImList:
							SetKeyConfigListSelectFlag(1);
							StartInitOneMenu(MENU_014_NAMELIST,0,1);
							break;
					}
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&keyConfingPageSelect, KEY_Confing_ICON_MAX, keyConfingIconNum, (DispListPage)DisplayOnePageKeyConfing);
					break;
					
				case ICON_202_PageUp:
					PublicPageUpProcess(&keyConfingPageSelect, KEY_Confing_ICON_MAX, keyConfingIconNum, (DispListPage)DisplayOnePageKeyConfing);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

