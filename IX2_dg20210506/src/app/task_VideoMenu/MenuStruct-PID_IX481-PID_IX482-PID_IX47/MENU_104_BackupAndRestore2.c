#include "MENU_public.h"
#include "MENU_104_BackupAndRestore2.h"
#include "obj_BackupAndRestore.h"

#define	BACKUP_AND_RESTORE2_ICON_MAX		5

int backupAndRestore2IconSelect;
int backupAndRestore2PageSelect;
int backupAndRestoreConfirm;
int backupAndRestoreConfirmSelect;

const IconAndText_t backupAndRestore2IconTable[] = 
{
	{ICON_Backup,						MESG_TEXT_ICON_Backup},
	{ICON_RestoreFromBackup,			MESG_TEXT_ICON_RestoreFromBackup},
	{ICON_RestoreFactorySettings,		MESG_TEXT_ICON_RestoreFactorySettings},
	{ICON_RestoreDefaultSelective,		MESG_TEXT_ICON_RestoreDefaultSelective},
};




const int backupAndRestore2IconNum = sizeof(backupAndRestore2IconTable)/sizeof(backupAndRestore2IconTable[0]);

static void DisplaybackupAndRestore2PageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char display[50];
	uint8 temp;
	int stringId;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < BACKUP_AND_RESTORE2_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(page*BACKUP_AND_RESTORE2_ICON_MAX+i < backupAndRestore2IconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, backupAndRestore2IconTable[i+page*BACKUP_AND_RESTORE2_ICON_MAX].iConText, 1, hv.h-x);
		}
	}
	pageNum = backupAndRestore2IconNum/BACKUP_AND_RESTORE2_ICON_MAX + (backupAndRestore2IconNum%BACKUP_AND_RESTORE2_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

int ConfirmSelect(int* confirmFlag, int* confirmSelect, int* iconSelect, int page, int ICON_MAX)
{
	uint16 x, y;
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_007_PublicList1+(*confirmSelect%ICON_MAX), &pos, &hv);
	Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
	x = hv.h - xsize;
	y = hv.v - ysize;
	
	if(*confirmFlag == 0)
	{
		*confirmFlag = 1;
		*confirmSelect = page*ICON_MAX+*iconSelect;
		OSD_GetIconInfo(ICON_007_PublicList1+(*confirmSelect%ICON_MAX), &pos, &hv);
		x = hv.h - xsize;
		y = hv.v - ysize;
		API_SpriteDisplay_XY(x, y, SPRITE_IF_CONFIRM);
	}
	else if(*confirmSelect == page*ICON_MAX+(*iconSelect))
	{
		*confirmFlag = 0;
		API_SpriteClose(x, y, SPRITE_IF_CONFIRM);
	}
	else
	{
		API_SpriteClose(x, y, SPRITE_IF_CONFIRM);
		*confirmFlag = 1;
		*confirmSelect = page*ICON_MAX+(*iconSelect);
		OSD_GetIconInfo(ICON_007_PublicList1+(*confirmSelect%ICON_MAX), &pos, &hv);
		x = hv.h - xsize;
		y = hv.v - ysize;
		API_SpriteDisplay_XY(x, y, SPRITE_IF_CONFIRM);
	}

	return *confirmFlag;
}

void ClearConfirm(int* confirmFlag, int* confirmSelect, int ICON_MAX)
{
	uint16 x, y;
	uint16 xsize, ysize;
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_007_PublicList1+(*confirmSelect%ICON_MAX), &pos, &hv);
	Get_SpriteSize(SPRITE_IF_CONFIRM, &xsize, &ysize);
	x = hv.h - xsize;
	y = hv.v - ysize;
	//x = hv.h - 122;
	//y = hv.v - 64;
	
	API_SpriteClose(x, y, SPRITE_IF_CONFIRM);
	*confirmSelect = 0;
	*confirmFlag = 0;
}
int RestoreFactoryVerifyPassword(const char* password)
 {
 	extern int playbackInfoResult;
 	extern void (*playbackInfoProcess)(int);
	char pwd[8+1];
	char tempChar[30];

	int len;
	char temp[20];
	//int stringId;
	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)pwd);

	if(!strcmp(password, pwd))
	{
		switch(backupAndRestore2IconTable[backupAndRestore2PageSelect*BACKUP_AND_RESTORE2_ICON_MAX+backupAndRestore2IconSelect].iCon)
		{
			case ICON_RestoreDefaultSelective:
				StartInitOneMenu(MENU_129_RestoreSelective,0,0);
				break;
			case ICON_RestoreFactorySettings:
				popDisplayLastMenu();
				BusySpriteDisplay(1);
				API_Event_IoServer_RestoreDefaults(0);
				API_Event_IoServer_RestoreDefaults(1);
				DeleteFileProcess(UserResFolder, "*");
				DeleteFileProcess(UserDataFolder, "*");
				DeleteFileProcess(SettingFolder, "*");
				DeleteFileProcess(INSTALLER_BAK_PATH, "*");
				system("rm /mnt/nand1-2/MyConfig.json");
				SipConfigRestoreDefault();
				BEEP_CONFIRM();
				BusySpriteDisplay(0);

				UpdateOkAndWaitingForReboot();
				break;
		}

		//UpdateOkAndWaitingForReboot();
		return -1;
	}
	else
	{
		return 0;
	}

 }
void MENU_104_BackupAndRestore2_Init(int uMenuCnt)
{
	backupAndRestore2IconSelect = 0;
	backupAndRestore2PageSelect = 0;
	backupAndRestoreConfirm = 0;
	backupAndRestoreConfirmSelect = 0;

	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_BACKUP_RESTORE,1, 0);
	DisplaybackupAndRestore2PageIcon(backupAndRestore2PageSelect);
}

void MENU_104_BackupAndRestore2_Exit(void)
{

}

void MENU_104_BackupAndRestore2_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char tempChar[2000];
	extern char installerInput[9];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					ClearConfirm(&backupAndRestoreConfirm, &backupAndRestoreConfirmSelect, BACKUP_AND_RESTORE2_ICON_MAX);
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					backupAndRestore2IconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(backupAndRestore2PageSelect*BACKUP_AND_RESTORE2_ICON_MAX+backupAndRestore2IconSelect >= backupAndRestore2IconNum)
					{
						return;
					}
					
					switch(backupAndRestore2IconTable[backupAndRestore2PageSelect*BACKUP_AND_RESTORE2_ICON_MAX+backupAndRestore2IconSelect].iCon)
					{
						case ICON_Backup:
							ClearConfirm(&backupAndRestoreConfirm, &backupAndRestoreConfirmSelect, BACKUP_AND_RESTORE2_ICON_MAX);
							StartInitOneMenu(MENU_105_BackupList,0,1);
							break;
							
						case ICON_RestoreFromBackup:
							ClearConfirm(&backupAndRestoreConfirm, &backupAndRestoreConfirmSelect, BACKUP_AND_RESTORE2_ICON_MAX);
							StartInitOneMenu(MENU_106_RestoreList,0,1);
							break;
							
						case ICON_RestoreFactorySettings:
							if(ConfirmSelect(&backupAndRestoreConfirm, &backupAndRestoreConfirmSelect, &backupAndRestore2IconSelect, backupAndRestore2PageSelect, BACKUP_AND_RESTORE2_ICON_MAX))
							{
								return;
							}
							API_Event_IoServer_InnerRead_All(ManagePwdEnable, tempChar);
							if(atoi(tempChar)==1||JudgeIsInstallerMode() || API_AskDebugState(100))
							{
								BusySpriteDisplay(1);
								API_Event_IoServer_RestoreDefaults(0);
								API_Event_IoServer_RestoreDefaults(1);
								DeleteFileProcess(UserResFolder, "*");
								DeleteFileProcess(UserDataFolder, "*");
								DeleteFileProcess(SettingFolder, "*");
								DeleteFileProcess(INSTALLER_BAK_PATH, "*");
								system("rm /mnt/nand1-2/MyConfig.json");
								SipConfigRestoreDefault();
								BEEP_CONFIRM();
								BusySpriteDisplay(0);

								UpdateOkAndWaitingForReboot();
							}
							else
							{
								
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 8, COLOR_WHITE, NULL, 0, RestoreFactoryVerifyPassword);
							}

							
							break;
						case ICON_RestoreDefaultSelective:
							ClearConfirm(&backupAndRestoreConfirm, &backupAndRestoreConfirmSelect, BACKUP_AND_RESTORE2_ICON_MAX);
							API_Event_IoServer_InnerRead_All(ManagePwdEnable, tempChar);
							if(atoi(tempChar)==1||JudgeIsInstallerMode() || API_AskDebugState(100))
							{
								StartInitOneMenu(MENU_129_RestoreSelective,0,1);
							}
							else
							{
								
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_InputSystemPassword, installerInput, 8, COLOR_WHITE, NULL, 0, RestoreFactoryVerifyPassword);
							}
							
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


