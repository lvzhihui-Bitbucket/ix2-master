
#ifndef _MENU_019_Keypad_H
#define _MENU_019_Keypad_H

#include "MENU_public.h"

typedef int (*InputOK)(const char*);

typedef struct
{
	unsigned char type;				//键盘类型123 or abd
	unsigned char maxLen;			//最长输入
	int color;						//显示字体颜色
	int dispTitle;					//显示输入标题
	char* input;					//输入数据指针
	char* initString;				//初始输入数据指针
	unsigned char highLight;		//是否全选
	unsigned char enterKeypadFlag;	//是否第一次进入键盘
	InputOK inputProcess;			//输入结束处理
}KeyPad;

void EnterKeypadMenu(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitString, unsigned char highLight, InputOK process);

#endif


