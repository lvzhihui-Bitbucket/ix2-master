#include "MENU_public.h"
#include "MENU_027_Calling2.h"
#include "task_CallServer.h"
//#include "../../task_BeCalled/task_BeCalled.h"
#include "task_monitor.h"	
#include "obj_IPCTableSetting.h"


int menu27_divert_flag=0;
int menu27_memo_auto = 0;

int PipListNum,listx,listy,listw,listh;
int pipShowx,pipShowy,pipShoww,pipShowh;

const int pipShowPos[][4]=
{
	//x, y,	width, height
	96,  30, 320, 240,
	608, 30, 320, 240,
	96, 240, 320, 240,
	608, 240, 320, 240,
};

int SetPipShowPos(void)
{
	char temp[20];
	int pos;
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		pipShowx = 0;
		pipShowy = (bkgd_h-64)/2;
		pipShoww = bkgd_w;
		pipShowh = (bkgd_h-64)/2;
	}
	else
	{
		API_Event_IoServer_InnerRead_All(PIP_SHOW_POS, temp);
		if(atoi(temp) > 3 || atoi(temp) < 0)
		{
			pos = 0;
		}
		else
		{
			pos = atoi(temp);
		}
		pipShowx = pipShowPos[pos][0];
		pipShowy = pipShowPos[pos][1];
		pipShoww = pipShowPos[pos][2];
		pipShowh = pipShowPos[pos][3];
	}
	
}
int SetGUProxyIPCPos(void)
{
	char temp[20];
	int pos;
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		pipShowx = 0;
		pipShowy = 0;
		pipShoww = bkgd_w;
		pipShowh = (bkgd_h-64)/2;
	}
	else
	{
	
		pipShowx = 0;
		pipShowy = 0;
		pipShoww = bkgd_w;
		pipShowh = bkgd_h;
	}
	
}

void SetVideoProxyShow(char* device_ip, char* device_name, char* user, char* pwd)
{
	char ip[21] = {0};
	char *pos1, *pos2;
	int len;
	
	for(pos1 = device_ip, pos1 = strstr(pos1, "http://"); pos1 != NULL; pos1 = strstr(pos1, "http://"))
	{
		pos1 = pos1+strlen("http://");
		pos2 = strstr(pos1, "/onvif/device_service");
		len = strlen(pos1) - strlen(pos2);
		if(len <= strlen("192.168.243.101"))
		{
			memcpy(ip, pos1, len);
		}
	}
	API_IpcDevice_Show_XY(1, ip, device_name, user, pwd, pipShowx, pipShowy, pipShoww, pipShowh);
}
void SetVideoProxyInfoShow(char* name, char* rtsp, int width ,int height, int vdtype)
{
	API_OneIpc_Rtsp_Show(1, rtsp, width, height, vdtype, pipShowx, pipShowy, pipShoww, pipShowh);
	SetWinDeviceName(1, name);
}

void SetVideoProxyDsShowPos(void)
{
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		Set_ds_show_pos(0,0,0,bkgd_w,(bkgd_h-64)/2);
	}
	else
	{
		Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h);
	}
	if(CallServer_Run.defaultVideoSource == VIDEO_SOURCE_IPC)
	{
		PipShowSwitch();
	}
	
}
	
int GetPipListNum(void)
{
	PipListNum = GetIpcNum()+GetWlanIpcNum();
	PipListNum = PipListNum > 6 ? 6 : PipListNum;
	printf("GetPipListNum = %d\n", PipListNum);	
	
	return PipListNum;
}
static void MenuListGetPipListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	char unicode_buf[200];
	IPC_ONE_DEVICE ipcRecord;
	char DisplayBuffer[50] = {0};
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < PipListNum)
		{
			if(index < GetIpcNum())
			{
				GetIpcCacheRecord(index, &ipcRecord);
			}
			else
			{
				GetWlanIpcRecord(index-GetIpcNum(), &ipcRecord);
			}				
			strcpy(DisplayBuffer, ipcRecord.NAME);
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_IPC2;

			recordLen = strlen(DisplayBuffer);
			unicode_len = 2*api_ascii_to_unicode(DisplayBuffer,recordLen,unicode_buf);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			
			pDisp->dispCnt++;
		}
	}
}

void PipListShow(void)
{
	LIST_INIT_T listInit;
	
	listInit = ListPropertyDefault();
	
	listInit.listCnt = PipListNum;
	listInit.listType = (LIST_TYPE) (listInit.listCnt - 1);
	listInit.textColor = COLOR_RED;
	listInit.textSize = 0;
	listInit.textAlign = 8;
	listInit.fun = MenuListGetPipListPage;
	
	if( get_pane_type() == 0 )//ix481
	{
		listInit.textOffset = 100;
		listx = (1280-160*PipListNum)/2;
		listy = 680;
		listw = 160*PipListNum;
		listh = 120;
	}
	else if( get_pane_type() == 2 || get_pane_type() == 4)//ix482/47
	{
		listInit.textOffset = 80;
		listx = (1024-128*PipListNum)/2;
		listy = 500;
		listw = 128*PipListNum;
		listh = 100;
	}
	else if( get_pane_type() == 5)//ix470v
	{
		listInit.textOffset = 80;
		listx = (600-128*PipListNum)/2;
		listy = 800;
		listw = 128*PipListNum;
		listh = 100;
	}
	InitMenuListXY(listInit, listx, listy, listw, listh, 1, 1, ICON_IconView1);
}
void PipListClear(void)
{
	MenuListClear(1, 1, listx, listy, listw, listh);
	MenuListDisable(1);
}

void PipShowSwitch(void)
{
	int layer1 = get_ds_show_layer(0);
	int layer2 = get_ds_show_layer(5);
	
	printf("---PipShowSwitch layer1=%d layer2=%d\n",layer1, layer2);
	set_ds_show_layer(0, layer2);
	set_ds_show_layer(5, layer1);
	
}

void SetOnePipShow(int select)
{
	if(select<GetIpcNum())
		API_OneIpc_Show_XY(1, 0,select, 1, pipShowx, pipShowy, pipShoww, pipShowh);
	else
		API_OneIpc_Show_XY(1, 1,select-GetIpcNum(), 1, pipShowx, pipShowy, pipShoww, pipShowh);
	
}
void PipShowClear(void)
{
	if(GetWinMonState(1) == 1)
	{
		set_default_show_layer(0);
		SetWinMonState(1, 0);
		API_OneIpc_Show_stop(1);
		MenuListDisable(2);
		API_ProgBarClose(pipShowx, pipShowy, pipShoww, 1);
		API_ProgBarClose(pipShowx, pipShowy, 1, pipShowh);
		API_ProgBarClose(pipShowx, pipShowy+pipShowh, pipShoww, 1);
		API_ProgBarClose(pipShowx+pipShoww, pipShowy, 1, pipShowh);
	}
}

//呼叫自动开锁处理
void AutoUnlockProcess(void)
{
	if(GetAutoUnlock())
	{
		sleep(2);
		if(lockProperty.lock1Enable)
		{
			API_CallServer_LocalUnlock1(CallServer_Run.call_type);
		}

		if(lockProperty.lock2Enable)
		{
			API_CallServer_LocalUnlock2(CallServer_Run.call_type);
		}
		ImReportIxminiUnlock();
	}
}

//呼叫插璜摘机
void PlugswitchTalkOn(void)
{
	if(CallServer_Run.state == CallServer_Ring && api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		API_CallServer_LocalAck(CallServer_Run.call_type);
	}	
}

void MENU_027_Calling2_Init(int uMenuCnt)
{
	menu27_divert_flag = 0;
	menu27_memo_auto = 0;
	if(CallServer_Run.call_type!=IxCallScene10_Passive)
		GetLockProperty(CallServer_Run.source_dev.ip_addr, &lockProperty);
	else
	{
		lockProperty.lock1Enable=0;
		lockProperty.lock2Enable=0;
	}
	MonIconListShow();
	SetLayerAlpha(8);
	if(CallServer_Run.call_type!=IxCallScene10_Passive)
		AutoUnlockProcess();
	PlugswitchTalkOn();
}

static void UnlockReportProcess(int lockSelect)
{
	char temp[10] = {0};
	int spriteSelect;
	
	API_RingStop();

	spriteSelect = (lockSelect == 0 ? SPRITE_UNLOCK : SPRITE_UNLOCK2);
	
	API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,spriteSelect);
	usleep(1000000);
	API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,spriteSelect);
	
	API_Event_IoServer_InnerRead_All(AutoCloseAfterUnlockEnable, temp);

	if(atoi(temp))
	{
		sleep(2);
		if(CallServer_Run.state != CallServer_Wait)
		{
			API_CallServer_LocalBye(CallServer_Run.call_type);
		}
		else
		{
			popDisplayLastMenu();
		}
	}

}


int CallingRecordProcess(char* recname)
{
	char tempData[41]={0};
	int ret;
	get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, tempData);
	SetWinDeviceName(0, tempData);
	SetWinVdType(0, 0);
	ret = api_record_start(0);
	menu27_memo_auto = 1;
	GetWinRecFile(0, recname);
	if(GetWinMonState(1) == 1)
	{
		if(api_record_start(1)==0)
			SaveVdRecordProcess(1);
	}
	return ret;
}

void MENU_027_Calling2_Exit(void)
{
	MonIconListClose();
	PipListClear();
	PipShowClear();
	if(menu27_divert_flag==0)
		Api_Ds_Show_Stop(0);//close_dsmonitor_client(0);
	if(GetWinRecState(0) == 1)
		api_record_stop(0);
	if(GetWinRecState(1) == 1)
		api_record_stop(1);	
}


void MENU_027_Calling2_Process(void* arg)
{

	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	CALL_RECORD_DAT_T record_temp;
	POS pos;
	int val_temp1,val_temp2;
	char disp[2] = {0};
	extern unsigned char FishMode;
	char *dispString;
	int len;
	LIST_ICON listIcon;
	struct {char onOff; char win;} *data;
	LIST_INIT_T listInit;

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					break;
				case KEY_UP:
					//if(menu27_submenu_select == 1)
					{
						//PublicUpProcess(&menu27_subicon_select, MENU27_ICON_MAX);
					}
					break;
				case KEY_DOWN:
					//if(menu27_submenu_select== 1)
					{
						//PublicDownProcess(&menu27_subicon_select, MENU27_ICON_MAX);
					}
					break;
				case KEY_TALK:

					if(CallServer_Run.state == CallServer_Ring)
					{
						API_CallServer_LocalAck(CallServer_Run.call_type);
						//API_FishEye_Enlarged(CallServer_Run.s_addr.code + 0x34);
					}
					else if(CallServer_Run.state == CallServer_Ack)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
					

					break;

				case KEY_BACK:
					break;
					
				case KEY_UNLOCK:
					if(CallServer_Run.call_type==DxCallScene1_Master)
					{
						API_CallServer_LocalUnlock1(CallServer_Run.call_type);
						return;
					}
					if(lockProperty.lock1Enable)		//czn_20191123
					{
						API_CallServer_LocalUnlock1(CallServer_Run.call_type);
						ImReportIxminiUnlock();
					}
					break;
				case KEY_UNLOCK2:
					if(CallServer_Run.call_type==DxCallScene1_Master)
					{
						API_CallServer_LocalUnlock2(CallServer_Run.call_type);
						return;
					}
					if(lockProperty.lock2Enable)		//czn_20191123
					{
						API_CallServer_LocalUnlock2(CallServer_Run.call_type);
						ImReportIxminiUnlock();
					}
					break;
				case KEY_POWER:
					//if(CallServer_Run.state != CallServer_Wait)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
					CloseOneMenu();
					break;

				case KEY_NO_DISTURB:
					//API_CallServer_LocalUnlock2(CallServer_Run.call_type);
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			//printf("MENU_027_Calling GetCurIcon()=%d\n", GetCurIcon()); 		
			switch(GetCurIcon())
			{
				// lzh_20181108_s
				if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
				{
					API_menu_create_bitmap_file(-1,-1,-1);				
				}
				// lzh_20181108_e

				case ICON_IconView1:
					if(CallServer_Run.call_type==IxCallScene10_Passive)
						return;
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_IconView1);
					if(listIcon.mainIcon >= 0)
					{
						//MenuListDiplayPage();
						SetOnePipShow(listIcon.mainIcon);
						usleep(100000);
						PipListClear();
						MonIconListShow();
					}
					break;
				case ICON_IconView2:
					if(CallServer_Run.call_type==IxCallScene10_Passive)
						return;
					//listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_IconView2);
					//if(listIcon.mainIcon >= 0)
					{
						PipShowSwitch();
					}

					break;
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon == 0)
					{
						if(CallServer_Run.state != CallServer_Wait)
						{
							API_CallServer_LocalBye(CallServer_Run.call_type);
						}
						else
						{
							popDisplayLastMenu();
						}
					}
					else if(listIcon.mainIcon == 1)
					{
						if(GetWinRecState(0) == 1)
						{
							API_RecordEnd_mux(0); 
						}
						else
						{
							if(api_record_start(0)==0)
								SaveVdRecordProcess(0);
							menu27_memo_auto = 0;
						}
						if(GetWinMonState(1) == 1)
						{
							if(api_record_start(1)==0)
								SaveVdRecordProcess(1);
						}
					}
					else if(listIcon.mainIcon == 2)
					{
						if(CallServer_Run.call_type==DxCallScene1_Master)
						{
							API_CallServer_LocalUnlock1(CallServer_Run.call_type);
							return;
						}
						if(lockProperty.lock1Enable)
						{
							API_CallServer_LocalUnlock1(CallServer_Run.call_type);
							ImReportIxminiUnlock();
						}
						break;
					}
					else
					{
						if(GetMonIconNum() == 6)
						{
							if(listIcon.mainIcon == 3)
							{
								if(lockProperty.lock2Enable)		
								{
									API_CallServer_LocalUnlock2(CallServer_Run.call_type);
									ImReportIxminiUnlock();
								}
							}
							else if(listIcon.mainIcon == 4)
							{
								if(CallServer_Run.state == CallServer_Ring)
								{
									API_CallServer_LocalAck(CallServer_Run.call_type);
								}
								else if(CallServer_Run.state == CallServer_Ack)
								{
									API_CallServer_LocalBye(CallServer_Run.call_type);
								}
							}
							else
							{
								if(CallServer_Run.call_type==IxCallScene10_Passive)
									return;
								#if 1
								if(GetWinMonState(1) == 1)
								{
									PipShowClear();
								}
								else
								{
									if( GetPipListNum() )
									{
										MonIconListClose();
										PipListShow();
									}
								}
								#endif
							}
						}
						else
						{
							if(listIcon.mainIcon == 3)
							{
								if(CallServer_Run.state == CallServer_Ring)
								{
									API_CallServer_LocalAck(CallServer_Run.call_type);
								}
								else if(CallServer_Run.state == CallServer_Ack)
								{
									API_CallServer_LocalBye(CallServer_Run.call_type);
								}
							}
							else
							{
								if(CallServer_Run.call_type==IxCallScene10_Passive)
									return;
								#if 1
								if(GetWinMonState(1) == 1)
								{
									PipShowClear();
								}
								else
								{
									if( GetPipListNum() )
									{
										MonIconListClose();
										PipListShow();
									}
								}
								#endif
							}
						}
					}
					break;	
				default:
					if(GetMenuListEn(1) == 1)
						PipListClear();
					if(GetMenuListEn(0) == 0)
						MonIconListShow();
					break;	
				#if 0	
				case ICON_207_MonitorPageDown:
					#if 0
					SaveCallingCallRecordProcess();
					
					switch(videoSwitchIconFunction)
					{
						case VIDEO_SWITCH_ICON_FUNCTION_NONE:
							break;
						case VIDEO_SWITCH_ICON_FUNCTION_IX:
							SwitchIPCOrIXMonitor(VIDEO_SOURCE_IX_DEVICE, CallServer_Run.ipcInfo, CallServer_Run.ixVideoIp);
							break;
						case VIDEO_SWITCH_ICON_FUNCTION_IPC:
							SwitchIPCOrIXMonitor(VIDEO_SOURCE_IPC, CallServer_Run.ipcInfo, CallServer_Run.ixVideoIp);
							break;
						case VIDEO_SWITCH_ICON_FUNCTION_IPC_LIST:
							StartInitOneMenu(MENU_110_CALL_IPC,0,1);
							break;
					}
					#endif
					break;
				case ICON_233_MonitorPageUp:
					
					break;
				case ICON_208_MonitorCapture:
					#if 0
					usleep(500000);
					if(one_vd_record.state)
					{
						SaveCallingCallRecordProcess();
					}
					else
					{
						if(menu27_memo_flag == 1)
						{	
							menu27_memo_flag = 0;
							record_temp.type		= VIDEO_RECORD; 
							record_temp.subType 	= LOCAL;
							record_temp.property	= NORMAL;
							record_temp.target_node = 0;		//czn_20170329
							record_temp.target_id	= 0;
							strcpy( record_temp.name, CallServer_Run.source_dev.name);
							strcpy( record_temp.input, "---");
							strcpy(record_temp.relation, menu27_memofilename);
							api_register_one_call_record( &record_temp );
						}

						if(CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)
						{
							if(memo_video_record_start(NULL) == 0)
							{
								menu27_memo_flag = 1;
								strncpy(menu27_memofilename,one_vd_record.filename,100);
							}
						}
					}
					#endif
					break;			
				case ICON_209_MonitorUnlock1:
					if(lockProperty.lock1Enable)			//czn_20191123
						API_CallServer_LocalUnlock1(CallServer_Run.call_type);
					//API_BeCalled_Unlock1();	//lyx_20171223	
					break;			
				case ICON_210_MonitorUnlock2:
					if(lockProperty.lock2Enable)			//czn_20191123
						API_CallServer_LocalUnlock2(CallServer_Run.call_type);
					break;
				case ICON_211_MonitorTalk:
					if(CallServer_Run.state == CallServer_Ring)
					{
						API_CallServer_LocalAck(CallServer_Run.call_type);
					}
					else if(CallServer_Run.state == CallServer_Ack)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
		
					break;
				case ICON_212_MonitorReturn:
					if(CallServer_Run.state != CallServer_Wait)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
				#endif	
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_MonitorStart:
				if(get_pane_type() != 1 && get_pane_type() != 5)
				{
					listInit = ListPropertyDefault();
					InitMenuListXY(listInit, pipShowx, pipShowy, pipShoww, pipShowh, 0, 2, ICON_IconView2);
					API_ProgBarDisplay(pipShowx, pipShowy, pipShoww, 1, COLOR_WHITE);
					API_ProgBarDisplay(pipShowx, pipShowy, 1, pipShowh, COLOR_WHITE);
					API_ProgBarDisplay(pipShowx, pipShowy+pipShowh, pipShoww, 1, COLOR_WHITE);
					API_ProgBarDisplay(pipShowx+pipShoww, pipShowy, 1, pipShowh, COLOR_WHITE);
				}
				
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				data = (arg + sizeof(SYS_WIN_MSG));
				if(data->win == 1)
				{
					vd_record_notice_disp(data->onOff, data->win);
					#if 0
					if((!data->onOff) && (!menu27_memo_auto))
					{
						SaveVdRecordProcess(0);
					}
					#endif
				}
				else if(data->win == 2)
				{
					#if 0
					if(!data->onOff)
						SaveVdRecordProcess(1);
					#endif
				}
				break;	
			case MSG_7_BRD_SUB_INFORM_FISH_EYE:
				//API_FishEye_Inform_Process((char*)(arg+sizeof(SYS_WIN_MSG)), CallServer_Run.s_addr.code + 0x34);
				break;	
				
			case MSG_7_BRD_SUB_BecalledTalkOn:
			case MSG_7_BRD_SUB_GLCallTalkOn:	
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
				
			case MSG_7_BRD_SUB_BecalledName:
			case MSG_7_BRD_SUB_GLCallName:	
				dispString = (char*)(arg+sizeof(SYS_WIN_MSG));
				len = *dispString;
				API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, 300, 40);
				API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, ++dispString, len,CallMenuDisp_Name_Font, STR_UTF8, 0);
				break;

			case MSG_7_BRD_SUB_BeCalledUnlock1:
				UnlockReportProcess(0);
				break;

			case MSG_7_BRD_SUB_BeCalledUnlock2:
				UnlockReportProcess(1);
				break;
				
			//case MSG_7_BRD_SUB_BeCalledDivert:
			case MSG_7_BRD_SUB_BecalledOff:						
			case MSG_7_BRD_SUB_GLCallOff:
				if(CallServer_Run.state != CallServer_Wait && CallServer_Run.rule_act == 0&&(CallServer_Run.call_type == IxCallScene1_Passive||CallServer_Run.call_type == IxCallScene10_Passive))
				{
					API_CallServer_LocalBye(CallServer_Run.call_type);
				}
				
				API_TimeLapseHide();
				API_TimeLapseStop();
				
				//if((judge_autotest_targetIsself()!=0&&GetLastNMenu() ==MENU_086_CallTestStat))//|| GetLastNMenu() ==MENU_118_TestQC)		//czn_20190412
					popDisplayLastMenu();
				//else
				//	CloseOneMenu();
				break;

			case MSG_7_BRD_SUB_BeCalledDivert:		//czn_20181219
			case MSG_7_BRD_SUB_PhoneTalkOn:
				#if 0
				if(judge_autotest_targetIsself()!=0)		//czn_20190412
					StartInitOneMenu(MENU_053_DIVERT,0,0);
				else
					StartInitOneMenu(MENU_053_DIVERT,0,1);
				#endif
				menu27_divert_flag=1;
				CloseOneMenu();
				break;
								
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}



	
}

#if 0
int menu27_memo_flag = 0;
char menu27_memofilename[100];
int menu27_memo_sprite_flag = 0;

int menu27_subicon_select = 0;
#define	MENU27_ICON_MAX	4
#define MENU27_SUB2_VAL_DISP_OFFSETX		108
#define MENU27_SUB2_VAL_DISP_OFFSETY		25
static VIDEO_SWITCH_ICON_FUNCTION videoSwitchIconFunction;


void display_recording_image( int on )
{

	if( on )
	{
		API_SpriteDisplay_XY(CallMenuDisp_Record_x,CallMenuDisp_Record_y,SPRITE_RECORDING);	
		usleep(500000);
	}
	else
	{
		API_SpriteClose(CallMenuDisp_Record_x,CallMenuDisp_Record_y,SPRITE_RECORDING);
	}
}

void VideoIconDisplay(void)
{
	if(CallServer_Run.ixVideoSourceValid == ENABLE && CallServer_Run.ipcVideoSourceValid == ENABLE)
	{
		videoSwitchIconFunction = (CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)? VIDEO_SWITCH_ICON_FUNCTION_IPC : VIDEO_SWITCH_ICON_FUNCTION_IX;
	}
	else if(CallServer_Run.ixVideoSourceValid == DISABLE && CallServer_Run.ipcVideoSourceValid == ENABLE)
	{
		videoSwitchIconFunction = VIDEO_SWITCH_ICON_FUNCTION_NONE;
	}
	else if(CallServer_Run.ixVideoSourceValid == ENABLE && CallServer_Run.ipcVideoSourceValid == DISABLE)
	{
		if(CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)
		{
			videoSwitchIconFunction = (Get_IPC_MonRes_Num())? VIDEO_SWITCH_ICON_FUNCTION_IPC_LIST : VIDEO_SWITCH_ICON_FUNCTION_NONE;
		}
		else
		{
			videoSwitchIconFunction = VIDEO_SWITCH_ICON_FUNCTION_IX;
		}
	}
	else
	{
		videoSwitchIconFunction = (Get_IPC_MonRes_Num())? VIDEO_SWITCH_ICON_FUNCTION_IPC_LIST : VIDEO_SWITCH_ICON_FUNCTION_NONE;
	}

	switch(videoSwitchIconFunction)
	{
		case VIDEO_SWITCH_ICON_FUNCTION_NONE:
			API_SpriteDisplay_XY(640,0,SPRITE_VideoNoBinding); 
			break;
		case VIDEO_SWITCH_ICON_FUNCTION_IX:
			API_SpriteDisplay_XY(640,0,SPRITE_VideoBindingIX); 
			break;
		case VIDEO_SWITCH_ICON_FUNCTION_IPC:
			API_SpriteDisplay_XY(640,0,SPRITE_VideoBindingIPC); 
			break;
		case VIDEO_SWITCH_ICON_FUNCTION_IPC_LIST:
			API_SpriteDisplay_XY(640,0,SPRITE_VideoBindingIPCList); 
			break;
	}
}

//呼叫自动开锁处理
void AutoUnlockProcess(void)
{
	if(GetAutoUnlock())
	{
		sleep(2);
		if(lockProperty.lock1Enable)
		{
			API_CallServer_LocalUnlock1(CallServer_Run.call_type);
		}

		if(lockProperty.lock2Enable)
		{
			API_CallServer_LocalUnlock2(CallServer_Run.call_type);
		}
	}
}

//呼叫插璜摘机
void PlugswitchTalkOn(void)
{
	// lzh_20191009_s
	if(CallServer_Run.state == CallServer_Ring)//&& api_get_plugswitch_status() )
	{
		usleep(1200*1000);
		API_CallServer_LocalAck(CallServer_Run.call_type);
	}	
	// lzh_20191009_e
}

//呼叫自动开锁处理
void DisplayVideoNameProcess(void)
{
	char tempData[101];

	if(CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)
	{
		tempData[0] = strlen(CallServer_Run.ixVideoName);
		strncpy(tempData+1, CallServer_Run.ixVideoName, 100);
	}
	else
	{
		tempData[0] = strlen(CallServer_Run.ipcVideoName);
		strncpy(tempData+1, CallServer_Run.ipcVideoName, 100);
	}
	
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
}

// select: 1选择IX设备，0选择IPC
int SwitchIPCOrIXMonitor(VIDEO_SOURCE source, onvif_login_info_t ipc, int ix_ip)
{
	char tempData[101];

	API_SpriteDisplay_XY(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
	if(CallServer_Run.videoSource == VIDEO_SOURCE_IPC)
	{
		api_ipc_stop_mointor_one_stream(0);
	}
	else if(CallServer_Run.videoSource == VIDEO_SOURCE_IX_DEVICE)
	{
		close_monitor_client();
	}

	if(source == VIDEO_SOURCE_IX_DEVICE)
	{
		printf("1111111ix_ip%08x\n",ix_ip);
		if(open_monitor_client_remote(ix_ip,0,REASON_CODE_CALL,1,150,NULL,NULL) == 0)
		{
			CallServer_Run.videoSource = VIDEO_SOURCE_IX_DEVICE;
			DisplayVideoNameProcess();
			usleep(200000);
		}
		else
		{
			BEEP_ERROR();
		}
	}
	else if(source == VIDEO_SOURCE_IPC)
	{
		#if 0
		printf("1111111rtsp_url%s\n",ipc.dat);
		if(ipc.dat[0])
		{
			if(SwitchIPCMonitor(ipc) == 0)
			{
				CallServer_Run.videoSource = VIDEO_SOURCE_IPC;
				DisplayVideoNameProcess();
			}
			else
			{
				BEEP_ERROR();
			}
		}
		else
		{
			BEEP_ERROR();
		}
		#endif
	}
	VideoIconDisplay();
	API_SpriteClose(CALL_SYSTEM_SPRITE_INFORM_X, CALL_SYSTEM_SPRITE_INFORM_Y,SPRITE_SystemBusy);
}

static void SaveCallingCallRecordProcess(void)
{
	CALL_RECORD_DAT_T record_temp;

	if(API_RecordEnd() != 0)				//cao_20161230
	{
		menu27_memo_flag = 0;
	}
	
	if(menu27_memo_flag == 1)
	{	
		menu27_memo_flag = 0;
		record_temp.type		= VIDEO_RECORD; 
		record_temp.subType 	= LOCAL;
		record_temp.property	= NORMAL;
		record_temp.target_node = 0;		//czn_20170329
		record_temp.target_id	= 0;
		strcpy( record_temp.name, CallServer_Run.source_dev.name);
		strcpy( record_temp.input, "---");
		strcpy(record_temp.relation, menu27_memofilename);
		api_register_one_call_record( &record_temp );
	}
}

#endif

