#include "MENU_public.h"
#include "listen_subscriber_list.h"
//#include "../../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"
//#include "../../task_Caller/task_Caller.h"
//#include "../../task_BeCalled/task_BeCalled.h"

// lzh_20191009_s
//#include "../../audio_service/obj_plugswitch.h"
// lzh_20191009_e
static int ListenTalkIconNum=0;
static unsigned char ListenTalkMonEn=0;
static unsigned char ListenTalkMonState=0;
const ICON_PIC_TYPE ListenTalkIconTable[] = 
{
	ICON_PIC_Exit,
   	ICON_PIC_TALK,
   	ICON_PIC_IPC2,
    	ICON_PIC_UNLOCK1,
	ICON_PIC_UNLOCK2,
};


static void ListenTalkIconListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < ListenTalkIconNum)
		{
			pDisp->disp[pDisp->dispCnt].iconType = ListenTalkIconTable[index];

			pDisp->dispCnt++;
		}
	}
}

int GetListenTalkIconNum()
{
	return ListenTalkIconNum;
}

void ListenTalkIconListShow(void)
{
	LIST_INIT_T listInit;
	listInit = ListPropertyDefault();
	
	if(lockProperty.lock1Enable && lockProperty.lock2Enable)
	{
		ListenTalkIconNum = 5;
		listInit.listType = ICON_1X5;
		listInit.listCnt = 5;
		listInit.fun = ListenTalkIconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 160, 720, 960, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 128, 536, 768, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 150*96
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}

	}
	else if(lockProperty.lock1Enable && lockProperty.lock2Enable==0)
	{
		ListenTalkIconNum = 4;
		listInit.listType = ICON_1X4;
		listInit.listCnt = 4;
		listInit.fun = ListenTalkIconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 240, 720, 800, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 192, 536, 640, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 100*64
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}
	}
	else if(ListenTalkMonEn)
	{
		ListenTalkIconNum = 3;
		listInit.listType = ICON_1X3;
		listInit.listCnt = 3;
		listInit.fun = ListenTalkIconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 240, 720, 800, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 192, 536, 640, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 100*64
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}
	}
	else
	{
		ListenTalkIconNum = 2;
		listInit.listType = ICON_1X2;
		listInit.listCnt = 2;
		listInit.fun = ListenTalkIconListPage;
		if( get_pane_type() == 0 )//ix481 160*80
			InitMenuListXY(listInit, 240, 720, 800, 80, 1, 0, ICON_888_ListView);
		else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
			InitMenuListXY(listInit, 192, 536, 640, 64, 1, 0, ICON_888_ListView);
		else if(get_pane_type() == 5)	//ix470v 100*64
		{
			listInit.listType = ICON_2X3;
			InitMenuListXY(listInit, 0, 832, 600, 192, 1, 0, ICON_888_ListView);
		}
	}
}
void ListenTalkIconListClose(void)
{
	if( get_pane_type() == 0 )//ix481
		API_ListAlpha_Close(1, 160, 720, 960, 80, OSD_LAYER_CURSOR);
	else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47
		API_ListAlpha_Close(1, 128, 536, 768, 64, OSD_LAYER_CURSOR);
	else if(get_pane_type() == 5)	//ix470v
		API_ListAlpha_Close(1, 0, 832, 600, 192, OSD_LAYER_CURSOR);
	MenuListDisable(0);

}
void ListenNoVideo_Display(void)
{
	if( get_pane_type() == 0 )//ix481 160*80
	{
		;
	}
	else if( get_pane_type() == 2 || get_pane_type() == 8 || get_pane_type() == 4)//ix482/47 128*64
	{
		Set_ds_show_pos(1,0,0,bkgd_w, bkgd_h);
		API_JpegPlaybackStart(ListenNoVideo_FILENAME);
	}
	else if(get_pane_type() == 5)	//ix470v 150*96
	{
		Set_ds_show_pos(1,0,0,bkgd_w,(bkgd_h-64)/2);
		API_JpegPlaybackStart(ListenNoVideo_V_FILENAME);
	}
}
void ListenNoVideo_Close(void)
{
	API_JpegPlaybackClose();
	Clear_ds_show_layer(1);
}
void MENU_141_ListenTalk_Init(int uMenuCnt)
{
	char nametemp[42];
	API_TimeLapseStart(COUNT_RUN_UP,0);
	Power_Down_Timer_Stop();
	lockProperty.lock1Enable=0;
	lockProperty.lock2Enable=0;
	ListenTalkMonEn=0;
	//API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, ++dispString, len,CallMenuDisp_Name_Font, STR_UTF8, 0);
	if(Get_listenTalk_State()==listenTalk_Broadcast)
	{
		API_OsdUnicodeStringDisplayWithIdAndAsc(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color,MESG_TEXT_InnerBroadcast, 1,NULL,NULL, 0);
	}
	else if(Get_listenTalk_State()==listenTalk_BeBroadcast)
	{
		BEEP_CONFIRM();
		API_OsdUnicodeStringDisplayWithIdAndAsc(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color,MESG_TEXT_BeBroadcast, 1,NULL,NULL, 0);
	}
	else if(Get_listenTalk_State()==listenTalk_AsServer)
	{
		API_OsdUnicodeStringDisplayWithIdAndAsc(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color,MESG_TEXT_BeListened, 1,NULL,NULL, 0);
	}
	else if(Get_listenTalk_State()==listenTalk_ListenClient)
	{
		if(GetLastNMenu()==MENU_002_MONITOR)
		{
			ListenTalkMonEn=1;
			lockProperty.lock1Enable=1;
			ListenTalkMonState=0;
		}
		
		nametemp[0]=' ';
		Get_ListenSerName(nametemp+1);
		API_OsdUnicodeStringDisplayWithIdAndAsc(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color,MESG_TEXT_ListenTo, 1,NULL,nametemp, 0);
	}
	
	
	ListenNoVideo_Display();
	
	ListenTalkIconListShow();
	SetLayerAlpha(8);
}

void MENU_141_ListenTalk_Exit(void)
{
	Close_ListenTalk();

	API_TimeLapseHide();
	API_TimeLapseStop();
	ListenTalkIconListClose();
	Api_Ds_Show_Stop(1);
	ListenNoVideo_Close();
}

void MENU_141_ListenTalk_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	char *dispString;
	int len;
	char dev_num[11];
	char name[41];
	//CALL_ERR_CODE *pErrorCode;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
				case KEY_UP:
					
					break;
				case KEY_DOWN:
					
					break;
				case KEY_TALK:
					if(Get_listen_Talking()==0)
					{
						if(Start_Listen_Talking()==0)
						{
							API_TimeLapseStart(COUNT_RUN_UP,0);
							API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
						}
						Power_Down_Timer_Stop();
					}
					else
					{
						Close_ListenTalk();
						popDisplayLastMenu();
					}
					break;

				case KEY_BACK:
					
					break;
					
				case KEY_UNLOCK:
					//API_CallServer_LocalUnlock1(CallServer_Run.call_type);
					
					if(ListenTalkMonEn==1)
					{
						if(ListenTalk_Unlock(0) == 0) 	//czn_20161227
						{
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
						}
					}
					Power_Down_Timer_Stop();
					break;	
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			#if 0
			if(GetCurIcon() == 0)
			{
				if(GetMenuListEn(0) == 1)
					ListenTalkIconListClose();
				else
					ListenTalkIconListShow();
			}
			else
			#endif
			{
				switch(GetCurIcon())
				{
					case ICON_258_IntercomTalk:
						if(Get_listen_Talking()==0)
						{
							if(Start_Listen_Talking()==0)
							{
								API_TimeLapseStart(COUNT_RUN_UP,0);
								API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
							}
						}
						else
						{
							Close_ListenTalk();
							popDisplayLastMenu();
						}
						break;
					
					case ICON_257_IntercomClose:
						Close_ListenTalk();
						popDisplayLastMenu();
						break;
					case ICON_888_ListView:
						listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
						if(listIcon.mainIcon == 0)
						{
							//ExitQuadMenu();
							//popDisplayLastMenu();
							Close_ListenTalk();
							popDisplayLastMenu();
						}
						else if(listIcon.mainIcon == 1)
						{
							if(Get_listen_Talking()==0)
							{
								if(Start_Listen_Talking()==0)
								{
									API_TimeLapseStart(COUNT_RUN_UP,0);
									API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
								}
								Power_Down_Timer_Stop();
							}
							else
							{
								Close_ListenTalk();
								popDisplayLastMenu();
							}
						}
						else if(listIcon.mainIcon == 2)
						{
							if(ListenTalkMonState==0)
							{
								Get_ListenSerDevNum(dev_num);
								Get_ListenSerName(name);
								//printf("111111111111---%s:%s\n",dev_num,name);
								ListenNoVideo_Close();
								sleep(1);
								Api_Ds_Show(1, 0,dev_num,name);
								ListenTalkMonState=1;
							}
							else
							{
								Api_Ds_Show_Stop(1);
								sleep(3);
								ListenNoVideo_Display();
								ListenTalkMonState=0;
							}
							Power_Down_Timer_Stop();
						}
						else if(listIcon.mainIcon == 3)
						{
							if(ListenTalk_Unlock(0) == 0) 	//czn_20161227
							{
								API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
							}
							Power_Down_Timer_Stop();
						}
						break;	
				}
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			
			case MSG_7_BRD_SUB_ListenTalkClose:
				popDisplayLastMenu();
				break;
			case MSG_7_BRD_SUB_IntercomCallTalkOn:
			case MSG_7_BRD_SUB_InnerCallTalkOn:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;
				
			case MSG_7_BRD_SUB_MonitorUnlock1:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK);
				break;

			case MSG_7_BRD_SUB_MonitorUnlock2:
				API_SpriteDisplay_XY(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				usleep(1000000);
				ImReportIxminiUnlock();
				API_SpriteClose(CallMenuDisp_Unlock_x,CallMenuDisp_Unlock_y,SPRITE_UNLOCK2);
				break;
			case MSG_7_BRD_SUB_IntercomBeCallOn:
			case MSG_7_BRD_SUB_InnerBeCallOn:
				StartInitOneMenu(MENU_013_CALLING,0,0);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
			case MSG_7_BRD_SUB_BecalledOn:	
				StartInitOneMenu(MENU_027_CALLING2,0,0);
				API_TimeLapseStart(COUNT_RUN_UP,0);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


