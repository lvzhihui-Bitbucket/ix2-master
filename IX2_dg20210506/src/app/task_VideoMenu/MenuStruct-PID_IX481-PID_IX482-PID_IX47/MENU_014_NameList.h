
#ifndef _MENU_014_H
#define _MENU_014_H

typedef struct
{

}NAME_LIST_DAT_T;

#include "MENU_public.h"

int GetNamelistNums(void);

int GetOneNamelistInfo(int index,int *dev_type,char *dev_name,char *dev_input,Global_Addr_Stru *dev_addr);

int SearchNamelistByInput(char *dev_input);

int SearchNamelistByAddr(Global_Addr_Stru dev_addr);

#endif


