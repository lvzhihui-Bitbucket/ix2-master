#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "MENU_public.h"
#include "obj_SYS_VER_INFO.h"
#include "define_Command.h"
#include "MENU_130_FileList.h"
#include "cJSON.h"

#define	FIEL_LIST_ICON_MAX		6
/*
	fileList格式
	[
		["1.txt", "/mnt/nand1-2/Customerized/"],
		["1.txt", "/mnt/nand1-2/Customerized/"],
		["1.txt", "/mnt/nand1-2/Customerized/"],
		["1.txt", "/mnt/nand1-2/Customerized/"]
	]

*/

static cJSON *fileList = NULL;
static int fileListTitleId;
static int fileListPageSelect;

static int fileListIconSelect;
static int fileListConfirm;
static int fileListConfirmSelect;


static void GetFileList(char *dir, cJSON *fileList)
{
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
	cJSON *file = NULL;
	char temp[100];
	char* path;
	
		
    if((dp = opendir(dir)) == NULL)
	{
        dprintf("cannot open directory: %s\n", dir);
        return;
    }
	
	chdir(dir);
	
    while((entry = readdir(dp)) != NULL) 
	{
        lstat(entry->d_name,&statbuf);
        if(S_ISDIR(statbuf.st_mode))
		{
           
            if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0)
            {
                continue;
			}
			GetFileList(entry->d_name, fileList);
        }
        else
        {
			if(fileList)
			{
				file = cJSON_CreateArray();
				cJSON_AddItemToArray(fileList, file);
				
				//添加文件名
				cJSON_InsertItemInArray(file, 0, cJSON_CreateString(entry->d_name));
				path = getcwd(NULL, 0);
				if(path == NULL)  
				{  
					dprintf("getcwd error\n");	
				}  
				else  
				{  
					//添加文件路径
					cJSON_InsertItemInArray(file, 1, cJSON_CreateString(path));
					free(path);  
				}  
				
				//添加文件修改时间
				struct tm* pModifytime=localtime(&(statbuf.st_mtime));
				strftime(temp, 100, "%Y-%m-%d %H:%M:%S", pModifytime);
				cJSON_InsertItemInArray(file, 2, cJSON_CreateString(temp));

				//添加文件大小
				cJSON_InsertItemInArray(file, 3, cJSON_CreateNumber(statbuf.st_size));
				
			}
		}
    }
	
    chdir("..");
	
    closedir(dp);
}
 
void EnterFileListMenu(const char* dir, int titleId)
{
	char *jsonString;
	
	if(fileList != NULL)
	{
		cJSON_Delete(fileList);
	}
	
	fileList = cJSON_CreateArray();

	GetFileList(dir, fileList);

	fileListTitleId = titleId;
	
	StartInitOneMenu(MENU_130_FileList, 0, 1);
}

static void ExitFileListMenu(void)
{
	if(fileList != NULL)
	{
		cJSON_Delete(fileList);
		fileList = NULL;
	}
}



static void MenuListGetFileListPage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	char value[100];
	char size[100];
	cJSON* file;
	cJSON* nameItem;
	cJSON* timeItem;
	cJSON* sizeItem;

	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < cJSON_GetArraySize(fileList))
		{

			file = cJSON_GetArrayItem(fileList, index);
			nameItem = cJSON_GetArrayItem(file, 0);
			timeItem = cJSON_GetArrayItem(file, 2);
			sizeItem = cJSON_GetArrayItem(file, 3);

			if(sizeItem->valuedouble >= 1024*1024*1024)
			{
				snprintf(size, 100, "%.2fGB", sizeItem->valuedouble/(1024*1024*1024.0));
			}
			else if(sizeItem->valuedouble >= 1024*1024)
			{
				snprintf(size, 100, "%.2fMB", sizeItem->valuedouble/(1024*1024.0));
			}
			else if(sizeItem->valuedouble >= 1024)
			{
				snprintf(size, 100, "%.2fKB", sizeItem->valuedouble/(1024.0));
			}
			else
			{
				snprintf(size, 100, "%dB", sizeItem->valuedouble);
			}

			snprintf(value, 100, "%s [%s] [%s]", cJSON_GetStringValue(nameItem), cJSON_GetStringValue(timeItem), size);
			
			pDisp->disp[pDisp->dispCnt].strLen = 2*api_ascii_to_unicode(value, strlen(value), pDisp->disp[pDisp->dispCnt].str);		
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;

			pDisp->dispCnt++;
		}
	}
}

void MENU_130_FileList_Init(int uMenuCnt)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
		
	API_GetOSD_StringWithID(fileListTitleId, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
	
	listInit = ListPropertyDefault();
	
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = cJSON_GetArraySize(fileList);
	listInit.fun = MenuListGetFileListPage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	listInit.ediEn = 1;
	
	if(GetLastNMenu() == MENU_093_ResInfo)
	{
		fileListPageSelect = 0;
	}

	listInit.currentPage = fileListPageSelect;
	
	InitMenuList(listInit);
}

void MENU_130_FileList_Exit(void)
{	
	MenuListDisable(0);
}

void MENU_130_FileList_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	cJSON* file;

	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					ExitFileListMenu();
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					ExitFileListMenu();
					GoHomeMenu();
					break;
				
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon > MENU_LIST_ICON_NONE)
					{
						if(listIcon.subIcon == 2)
						{
							fileListIconSelect = listIcon.mainIcon % FIEL_LIST_ICON_MAX;
							
							if(ConfirmSelect(&fileListConfirm, &fileListConfirmSelect, &fileListIconSelect, 0, FIEL_LIST_ICON_MAX))
							{
								return;
							}
							ClearConfirm(&fileListConfirm, &fileListConfirmSelect, FIEL_LIST_ICON_MAX);
							file = cJSON_GetArrayItem(fileList, listIcon.mainIcon);
							DeleteFileProcess(cJSON_GetStringValue(cJSON_GetArrayItem(file, 1)), cJSON_GetStringValue(cJSON_GetArrayItem(file, 0)));
							cJSON_DeleteItemFromArray(fileList, listIcon.mainIcon);
							AddOrDeleteMenuListItem(-1);
							BEEP_CONFIRM();
						}
						else
						{
							ClearConfirm(&fileListConfirm, &fileListConfirmSelect, FIEL_LIST_ICON_MAX);
							file = cJSON_GetArrayItem(fileList, listIcon.mainIcon);
							if(!EnterViewFileMenu(cJSON_GetStringValue(cJSON_GetArrayItem(file, 1)), cJSON_GetStringValue(cJSON_GetArrayItem(file, 0))))
							{
								BEEP_ERROR();
							}
						}
					}
					else if(listIcon.mainIcon < MENU_LIST_ICON_NONE)
					{
						fileListPageSelect = MenuListGetCurrentPage();
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}


