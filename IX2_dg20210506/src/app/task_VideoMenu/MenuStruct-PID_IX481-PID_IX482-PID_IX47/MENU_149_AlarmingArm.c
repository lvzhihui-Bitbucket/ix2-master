#include "MENU_public.h"
#include "task_CallServer.h"
#include "obj_ImNameListTable.h"
#include "obj_GetIpByNumber.h"

int ArmMode_Select = 0;

const int AlarmZoneStateXY[8][2]=
{
	{(626+48),15+120},
	{626+48+200,15+120},	
	{626+48,15+120+245},
	{626+48+200,15+120+245},
	#if 0
	{472+48,15+70},
	{472+48+164,15+70},	
	{472+48,15+70+205},
	{472+48+164,15+70+205},	
	#endif
	#if 0
	{620,15+102*0},
	{620,15+102*1},
	{620,15+102*2},
	{620,15+102*3},
	#endif
};
const int AlarmZoneStateXY_V[4][2]=
{
	{100,730},
	{400,730},	
	{100,900},
	{400,900},
	
};
void DisplayAlarmingZoneState(int ArmedMode)
{
	//API_EnableOsdUpdate();

	
	int i,j;
	int AlarmingZoneState;
	int AlarmingZoneMask;
	char disp[50];
	AlarmingZoneState = API_Alarming_SenserState_Update();
	if(ArmedMode>=0)
	{
		AlarmingZoneMask = Get_AlarmingZone_ByArmedMode(ArmMode_Select);
	}
	else
	{
		AlarmingZoneMask = 0;
	}
	
	for(i=0,j=0;i<4;i++)
	{
		if( get_pane_type() == 1 || get_pane_type() == 5 )
		{
			API_SpriteClose(AlarmZoneStateXY_V[i][0], AlarmZoneStateXY_V[i][1],SPRITE_ZoneCaution);
			API_OsdStringClearExt(AlarmZoneStateXY_V[j][0]-48, AlarmZoneStateXY_V[i][1]+72,164,40);
		}
		else
		{
			API_SpriteClose(AlarmZoneStateXY[i][0], AlarmZoneStateXY[i][1],SPRITE_ZoneCaution);
			API_OsdStringClearExt(i%2?826:626, AlarmZoneStateXY[i][1]+72,164,40);
		}
		if(AlarmingZoneMask&(0x01<<i))
		{
			if(AlarmingZoneState&(0x01<<i))
			{
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_SpriteDisplay_XY(AlarmZoneStateXY_V[j][0], AlarmZoneStateXY_V[j][1], SPRITE_ZoneCaution);
				else
					API_SpriteDisplay_XY(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1], SPRITE_ZoneCaution);
			}
			else
			{
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_SpriteDisplay_XY(AlarmZoneStateXY_V[j][0], AlarmZoneStateXY_V[j][1], SPRITE_ZoneOk);
				else
					API_SpriteDisplay_XY(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1], SPRITE_ZoneOk);
			}
			if(Get_AlarmingZoneName(i,disp) == 0)
			{
				//API_OsdStringDisplayExt(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1]+42, COLOR_BLACK, disp, strlen(disp),0,STR_UTF8,0);
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_OsdStringCenterDisplayExt(AlarmZoneStateXY_V[j][0]-48, AlarmZoneStateXY_V[j][1]+72, COLOR_WHITE, disp, strlen(disp),2,STR_UTF8,164,40);
				else
					API_OsdStringCenterDisplayExt(j%2?826:626, AlarmZoneStateXY[j][1]+72, COLOR_WHITE, disp, strlen(disp),2,STR_UTF8,164,40);
			}
			j++;
		}
	}
	AlarmingZoneMask = Get_Alarming24hZone();
	for(i=0;i<4;i++)
	{
		//API_SpriteClose(AlarmZoneStateXY[i][0], AlarmZoneStateXY[i][1],SPRITE_DxMasterCaution);
		//API_OsdStringClearExt(AlarmZoneStateXY[i][0], AlarmZoneStateXY[i][1]+42,180,40);
		if(AlarmingZoneMask&(0x01<<i))
		{
			if(AlarmingZoneState&(0x01<<i))
			{
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_SpriteDisplay_XY(AlarmZoneStateXY_V[j][0], AlarmZoneStateXY_V[j][1], SPRITE_24hZoneCaution);
				else
					API_SpriteDisplay_XY(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1], SPRITE_24hZoneCaution);
			}
			else
			{
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_SpriteDisplay_XY(AlarmZoneStateXY_V[j][0], AlarmZoneStateXY_V[j][1], SPRITE_24hZoneOk);
				else	
					API_SpriteDisplay_XY(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1], SPRITE_24hZoneOk);
			}
			if(Get_AlarmingZoneName(i,disp) == 0)
			{
				//API_OsdStringDisplayExt(AlarmZoneStateXY[j][0], AlarmZoneStateXY[j][1]+42, COLOR_BLACK, disp, strlen(disp),0,STR_UTF8,0);
				if( get_pane_type() == 1 || get_pane_type() == 5 )
					API_OsdStringCenterDisplayExt(AlarmZoneStateXY_V[j][0]-48, AlarmZoneStateXY_V[j][1]+72, COLOR_WHITE, disp, strlen(disp),2,STR_UTF8,164,40);
				else
					API_OsdStringCenterDisplayExt(j%2?826:626, AlarmZoneStateXY[j][1]+72, COLOR_WHITE, disp, strlen(disp),2,STR_UTF8,164,40);
			}
			j++;
		}
	}
	//API_EnableOsdUpdate();
}

void MENU_149_AlarmingArm_Init(int uMenuCnt)
{
	char disp[80];
	int disp_len;
	if(GetLastNMenu()==MENU_001_MAIN)
	{
		ArmMode_Select=0;
		AlarmInformPara_Init();
		AddAllGlToAlarmInformPara();
		if(GetImNamelistGLNum() == 0)
		{
			API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
		}	
	}
	API_GetOSD_StringWithID(MESG_TEXT_AlarmTile,NULL,0,NULL,0,disp,&disp_len);
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		API_OsdStringCenterDisplayExt(0, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,600,40);
	else
		API_OsdStringCenterDisplayExt(0, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,606,40);
	API_GetOSD_StringWithID(MESG_TEXT_Disarmed,NULL,0,NULL,0,disp,&disp_len);
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		API_OsdStringCenterDisplayExt(0, 635, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,600,40);
	else
		API_OsdStringCenterDisplayExt(606, 25, COLOR_WHITE, disp, disp_len,1,STR_UNICODE,418,40);
	API_MenuIconDisplaySelectOn(ICON_ArmMode1+ArmMode_Select);
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		API_OsdUnicodeStringDisplay(25, 102, DISPLAY_TITLE_COLOR, MESG_TEXT_ArmmodeTitle, 2, GetIconXY(ICON_AlarmInformMode1).x-25);
	else
		API_OsdUnicodeStringDisplay(25, 90, DISPLAY_TITLE_COLOR, MESG_TEXT_ArmmodeTitle, 2, GetIconXY(ICON_AlarmInformMode1).x-25);
	API_OsdUnicodeStringDisplay(GetIconXY(ICON_AlarmInformMode1).x+25, 90, DISPLAY_TITLE_COLOR, MESG_TEXT_AlarmInformServiceTitle, 2, 0);
	
	API_OsdUnicodeStringDisplay(GetIconXY(ICON_ArmMode1).x+65, GetIconXY(ICON_ArmMode1).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_ArmMode1, 2, GetIconXY(ICON_AlarmInformMode1).x-25);
	API_OsdUnicodeStringDisplay(GetIconXY(ICON_ArmMode2).x+65, GetIconXY(ICON_ArmMode2).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_ArmMode2, 2, GetIconXY(ICON_AlarmInformMode1).x-25);
	API_OsdUnicodeStringDisplay(GetIconXY(ICON_ArmMode3).x+65, GetIconXY(ICON_ArmMode3).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_ArmMode3, 2, GetIconXY(ICON_AlarmInformMode1).x-25);
	API_OsdUnicodeStringDisplay(GetIconXY(ICON_ArmMode4).x+65, GetIconXY(ICON_ArmMode4).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_ArmMode4, 2, GetIconXY(ICON_AlarmInformMode1).x-25);

	//API_OsdUnicodeStringDisplay(GetIconXY(ICON_AlarmInformMode1).x+25, GetIconXY(ICON_ArmMode2).y+20, DISPLAY_LIST_COLOR, MESG_TEXT_ArmMode2, 1, 0);
	
	
	sprintf(disp,"(%d)",GetAlarmInformParaGlNums());
	API_OsdUnicodeStringDisplayWithIdAndAsc(GetIconXY(ICON_AlarmInformMode1).x+25, GetIconXY(ICON_AlarmInformMode1).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_AlarmInformMode1, 1, NULL,disp,GetIconXY(ICON_AlarmInformMode1).x-25);
	
	
	//API_OsdStringDisplayExt(GetIconXY(ICON_AlarmInformMode1).x+25, GetIconXY(ICON_AlarmInformMode1).y+25, DISPLAY_LIST_COLOR, "GL (1)", strlen("GL (1)"),1,STR_UTF8,0);
	//API_OsdStringDisplayExt(GetIconXY(ICON_ArmMode2).x+25, GetIconXY(ICON_ArmMode2).y+20, DISPLAY_LIST_COLOR, "Stay", strlen("Stay"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
	DisplayAlarmingZoneState(ArmMode_Select);
	
}




void MENU_149_AlarmingArm_Exit(void)
{
	
}

void MENU_149_AlarmingArm_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	one_vtk_dat* pOneRecord;
	int len;
	//char temp[ONE_LIST_MAX_LEN];
	int updateCnt,x,y;	//czn_20190221
	char display[100];
	int armed_delay;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

					
					
				//case ICON_007_PublicList1:
				//case ICON_008_PublicList2:
				//case ICON_009_PublicList3:
				//case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					if(GetAlarmingState()==0)
					{
						API_Alarming_Arming_NormalZone(0);
					}
					else
					{
						API_Alarming_Disarming_NormalZone();
						API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Arming     ", strlen("Arming     "),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
					}
					break;
				case ICON_201_PageDown:
					
					break;			
				case ICON_202_PageUp:
					
					break;	
				case ICON_AlarmSettingExit:
					popDisplayLastMenu();
					break;
				case ICON_Arming:
					API_Alarming_Arming_NormalZone(ArmMode_Select);
					break;
				case ICON_ArmMode1:
				case ICON_ArmMode2:
				case ICON_ArmMode3:
				case ICON_ArmMode4:
					if(ArmMode_Select != (GetCurIcon()-ICON_ArmMode1))
					{
						API_MenuIconDisplaySelectOff(ICON_ArmMode1+ArmMode_Select);
						ArmMode_Select = (GetCurIcon()-ICON_ArmMode1);
						API_MenuIconDisplaySelectOn(ICON_ArmMode1+ArmMode_Select);
						DisplayAlarmingZoneState(ArmMode_Select);
					}
					break;
				case ICON_AlarmInformMode1:
					StartInitOneMenu(MENU_151_AlarmInformService,0,1);
					break;
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_Armed_Delay:
				if(GetAlarmingState()==1)
				{
						
					armed_delay = *((int*)(arg+sizeof(SYS_WIN_MSG)));
					if(armed_delay>0)
					{
						sprintf(display," %ds",armed_delay);
						MessageReportDisplay(NULL,MESG_TEXT_EffectiveAfter, display, COLOR_RED, 1, 10);
						BEEP_KEY();
						//API_OsdStringDisplayExt(DISPLAY_STATE_X, DISPLAY_STATE_Y, DISPLAY_STATE_COLOR,display,strlen(display),1,STR_UTF8,0);		
					}
					else
					{
						MessageClose();
						//API_OsdStringClearExt(DISPLAY_STATE_X, DISPLAY_STATE_Y,480-DISPLAY_STATE_X,30);
						BEEP_CONFIRM();
						//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+4*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_LIST_COLOR, "Disarming", strlen("Disarming"),1,STR_UTF8,DISPLAY_ICON_X_WIDTH);
						popDisplayLastMenu();
					}
				}
				break;
			case MSG_7_BRD_MsgClose_TouchClick:
				API_Alarming_Disarming_NormalZone();
				break;
			case MSG_7_BRD_SUB_NameListUpdate:
				AddAllGlToAlarmInformPara();
				sprintf(display,"(%d)",GetAlarmInformParaGlNums());
				API_OsdStringClearExt(GetIconXY(ICON_AlarmInformMode1).x+25, GetIconXY(ICON_AlarmInformMode1).y+25,400-(GetIconXY(ICON_AlarmInformMode1).x+25),40);
				API_OsdUnicodeStringDisplayWithIdAndAsc(GetIconXY(ICON_AlarmInformMode1).x+25, GetIconXY(ICON_AlarmInformMode1).y+25, DISPLAY_LIST_COLOR, MESG_TEXT_AlarmInformMode1, 1, NULL,display,GetIconXY(ICON_AlarmInformMode1).x-25);
				break;
			case MSG_7_BRD_AlarmingSenserUpdate:
				DisplayAlarmingZoneState(ArmMode_Select);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


