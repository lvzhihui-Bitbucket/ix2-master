#include "MENU_public.h"
#include "MENU_005_CallScene.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_CallServer.h"	//czn_20181227
#include "obj_CallSceneCurPara.h"
#define	CallScene_ICON_MAX	6

int CallSceneIconSelect;
int CallScenePageSelect;

int CallSceneSetting;
#if 0
int NoDisturbSetting = 0;		//czn_20170805
OS_TIMER NoDisturbTimer;
time_t NoDisturb8H_StartTime;
#endif

int callSceneIconNum;

const CallSceneIcon callSceneIconTable[] = 
{
	{ICON_066_NormalUse,					MESG_TEXT_Icon_066_NormalUse},    
	{ICON_067_NoDisturb8H,					MESG_TEXT_Icon_067_NoDisturb8H},
	{ICON_068_NoDisturbAlways,				MESG_TEXT_Icon_068_NoDisturbAlways},
	{ICON_069_DivertCallIfNoAnswer,			MESG_TEXT_Icon_069_DivertCallIfNoAnswer},
	{ICON_070_DivertCallSimultaneously, 	MESG_TEXT_Icon_070_DivertCallSimultaneously},
};
const CallSceneIcon callSceneIconTable1[] = 
{
	{ICON_066_NormalUse,					MESG_TEXT_Icon_066_NormalUse},    
	{ICON_067_NoDisturb8H,					MESG_TEXT_Icon_067_NoDisturb8H},
	{ICON_068_NoDisturbAlways,				MESG_TEXT_Icon_068_NoDisturbAlways},
};

const CallSceneIcon callSceneIconTable2[] = 
{
	{ICON_066_NormalUse,					MESG_TEXT_Icon_066_NormalUse},    
	{ICON_067_NoDisturb8H,					MESG_TEXT_Icon_067_NoDisturb8H},
	{ICON_068_NoDisturbAlways,				MESG_TEXT_Icon_068_NoDisturbAlways},
	{ICON_069_DivertCallIfNoAnswer,			MESG_TEXT_Icon_069_DivertCallIfNoAnswer},
	{ICON_070_DivertCallSimultaneously, 	MESG_TEXT_Icon_070_DivertCallSimultaneously},
	{ICON_DivertToDev, 	MESG_TEXT_DivertToDev},
};
const CallSceneIcon callSceneIconTable3[] = 
{
	{ICON_066_NormalUse,					MESG_TEXT_Icon_066_NormalUse},    
	{ICON_067_NoDisturb8H,					MESG_TEXT_Icon_067_NoDisturb8H},
	{ICON_068_NoDisturbAlways,				MESG_TEXT_Icon_068_NoDisturbAlways},
	{ICON_DivertToDev, 	MESG_TEXT_DivertToDev},
};
CallSceneIcon *pcallSceneIconTable=NULL;
// lzh_20181016_s
// 该函数仅仅在切换语言时调用，从而生产新的cache字符串文件
void DisplayCallScenePageIcon_CachePush(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	uint8 pageNum;
	uint8 setValue;
	char display[6];
	POS pos;
	SIZE hv;
	
	API_DisableOsdUpdate();

	for(i = 0; i < CallScene_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		val_x = x+(hv.h - pos.x)/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		if(page*CallScene_ICON_MAX+i < callSceneIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, callSceneIconTable[i+page*CallScene_ICON_MAX].iConText, 1, hv.h-x);
		}
	}
	API_menu_display_cache_push_string(MENU_005_CALL_SCENE,page,x,y,520/*DISPLAY_ICON_X_WIDTH*/,320);

	API_EnableOsdUpdate();		
}

void DisplayCallScenePageIcon_CachePop(uint8 page)
{
	API_menu_display_cache_pop_string(MENU_005_CALL_SCENE,page);
}

static void DisplayCallScenePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	char temp[20];
	POS pos;
	SIZE hv;

	API_DisableOsdUpdate();
	API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
	if(atoi(temp))	
	{
		 callSceneIconNum = sizeof(callSceneIconTable)/sizeof(callSceneIconTable[0]);
	}
	else
	{
		 callSceneIconNum = sizeof(callSceneIconTable1)/sizeof(callSceneIconTable1[0]);
	}

	//if( page != 0 )
	{
		for(i = 0; i < CallScene_ICON_MAX; i++)
		{
			OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
			//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

			//API_OsdStringClearExt(x, y, DISPLAY_ICON_X_WIDTH, CLEAR_STATE_H);
			API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
			if(page*CallScene_ICON_MAX+i < callSceneIconNum)
			{
				if(atoi(temp))	
				{
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, callSceneIconTable[i+page*CallScene_ICON_MAX].iConText, 1, hv.h-x);
				}
				else
				{
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, callSceneIconTable1[i+page*CallScene_ICON_MAX].iConText, 1, hv.h-x);
				}
			}
		}
		pageNum = callSceneIconNum/CallScene_ICON_MAX + (callSceneIconNum%CallScene_ICON_MAX ? 1 : 0);

		DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	}
	//else
	//{
	//	DisplayCallScenePageIcon_CachePop(page);
	//}
	API_EnableOsdUpdate();
}

void MENU_005_CallScene_Init(int uMenuCnt)
{
	int x, y;
	char temp[20];
	POS pos;
	SIZE hv;
	if(strstr(GetCustomerizedName(),"TrsDev")!=NULL)
	{
		MENU_005_CallScene_Init_TrsDev(uMenuCnt);
		return;
	}
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_CallScene, 2, 0);

	API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
	
	CallSceneSetting = Get_NoDisturbSetting();
	if(CallSceneSetting == 0 && atoi(temp))
	{
		API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
		CallSceneSetting = atoi(temp);
	}
	
	CallSceneIconSelect = CallSceneSetting%CallScene_ICON_MAX;

	CallScenePageSelect = 0;
	DisplayCallScenePageIcon(CallScenePageSelect);
	ListSelect(CallSceneIconSelect, 1);
	//API_MenuIconDisplaySelectOn(ICON_205_CallScene);
	API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);	
	GetIconTextInfo(ICON_218_PublicSetTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, pos.y, DISPLAY_TITLE_COLOR,MESG_TEXT_CallScene,1, 0);
	API_SpriteDisplay_XY(GetIconXY(ICON_218_PublicSetTitle).x, GetIconXY(ICON_218_PublicSetTitle).y, SPRITE_CallScene);
	
	API_Event_IoServer_InnerRead_All(CallSceneMenuMode, (uint8*)temp);
		
	if(atoi(temp) == 1&&callSceneIconNum>3&&Get_SipSer_State() == 1)	//czn_20181227
	{
		API_OsdUnicodeStringDisplay(GetIconXY(ICON_176_KeyState).x,GetIconXY(ICON_176_KeyState).y+18, DISPLAY_LIST_COLOR,MESG_TEXT_ICON_340_CallSceneSipTest, 1, bkgd_w-(GetIconXY(ICON_176_KeyState).x));
	}
}

void MENU_005_CallScene_Exit(void)
{
	if(strstr(GetCustomerizedName(),"TrsDev")!=NULL)
	{
		MENU_005_CallScene_Exit_TrsDev();
		return;
	}
	API_CallServer_RemoteBye(IxCallScene5,NULL);	//czn_20181227
}

void MENU_005_CallScene_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int y;
	sip_account_Info sip_acc;		//czn_20181227
	char temp[20];
	if(strstr(GetCustomerizedName(),"TrsDev")!=NULL)
	{
		MENU_005_CallScene_Process_TrsDev(arg);
		return;
	}
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					BusySpriteDisplay(1);
					ListSelect(CallSceneSetting, 0);
					
					CallSceneSetting = CallSceneIconSelect;
					ListSelect(CallSceneSetting, 1);
					
					if(Set_NoDisturbSetting(CallSceneSetting) == 0)
					{
						sprintf(temp, "%d", CallSceneSetting);
						API_Event_IoServer_InnerWrite_All(CallScene_SET, temp);
					}

					
					usleep(200000);
					popDisplayLastMenu();
					break;
				case KEY_UP:
					PublicUpProcess(&CallSceneIconSelect, CallScene_ICON_MAX);					
					break;
				case KEY_DOWN:
					PublicDownProcess(&CallSceneIconSelect, CallScene_ICON_MAX);					
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_176_KeyState:
				case ICON_340_CallSceneSipTest:	//czn_20181227
					API_Event_IoServer_InnerRead_All(CallSceneMenuMode, (uint8*)temp);
					if(atoi(temp)==1&&callSceneIconNum>3&&Get_SipSer_State() == 1)
					{
						Get_SipConfig_DirvertAccount(sip_acc.sip_account);
						API_CallServer_Invite(IxCallScene5,0,&sip_acc);
					}
					break;

				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					CallSceneSetting = CallScenePageSelect*CallScene_ICON_MAX + GetCurIcon() - ICON_007_PublicList1;
					
					if(CallSceneSetting >= callSceneIconNum)
					{
						return;
					}
					BusySpriteDisplay(1);
					ListSelect(CallSceneIconSelect, 0);
					CallSceneIconSelect = GetCurIcon() - ICON_007_PublicList1;
					ListSelect(CallSceneIconSelect, 1);
	
					if(Set_NoDisturbSetting(CallSceneSetting) == 0)
					{
						sprintf(temp, "%d", CallSceneSetting);
						API_Event_IoServer_InnerWrite_All(CallScene_SET, temp);
					}
					
					//DisplayCallScenePageIcon(CallScenePageSelect);
					
					usleep(200000);
					popDisplayLastMenu();

					API_MsSyncCallSceneSet(CallSceneSetting);//czn_20190221
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}
////////////////////////////////////////////////////////////////////////////////
static void MenuListGetCallScenePage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int recordLen, unicode_len;
	short unicode_buf[200];
	char DisplayBuffer[50] = {0};
	IPC_ONE_DEVICE ipcRecord;
	char name_temp[41];
	char name_temp2[41];
	char bdRmMs[11];
	
	pDisp->dispCnt = 0;
	bprintf("111111:%d\n",CallSceneIconSelect);
	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < callSceneIconNum)
		{
			if(index==CallSceneIconSelect)
				pDisp->disp[pDisp->dispCnt].preSprType = PRE_SPR_SELECT;
			else
				pDisp->disp[pDisp->dispCnt].preSprType=PRE_SPR_NONE;
			API_GetOSD_StringWithID2(pcallSceneIconTable[index].iConText, unicode_buf, &unicode_len);
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			if(memcmp(GetSysVerInfo_ms(),"01",2)==0&&index==(callSceneIconNum-1))
				pDisp->disp[pDisp->dispCnt].sufSprType=SUF_SPR_Setting;
			else
				pDisp->disp[pDisp->dispCnt].sufSprType=SUF_SPR_NONE;
		}
		pDisp->dispCnt++;
	}
		
	
}

// lzh_20181016_e
void CallSceneListShow(void)
{
	int unicode_len;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	char temp[5];
	
	API_GetOSD_StringWithID2(MESG_TEXT_CallScene, unicode_buf, &unicode_len);
	listInit = ListPropertyDefault();
	API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
	if(atoi(temp))	
	{
		pcallSceneIconTable=callSceneIconTable2;
		 callSceneIconNum = sizeof(callSceneIconTable2)/sizeof(callSceneIconTable2[0]);
	}
	else
	{
		pcallSceneIconTable=callSceneIconTable3;
		 callSceneIconNum = sizeof(callSceneIconTable3)/sizeof(callSceneIconTable3[0]);
		 if(CallSceneIconSelect==5)
		 	CallSceneIconSelect=3;
	}
	bprintf("111111:%d\n",CallSceneIconSelect);
	listInit.selEn = 1;
	listInit.ediEn = 1;
	listInit.listCnt = callSceneIconNum;
	
	if( get_pane_type() == 1 || get_pane_type() == 5 )
	{
		listInit.listType = TEXT_10X1;
		listInit.textOffset = 0;
		listInit.textValOffset = 150;
	}		
	else
	{
		listInit.listType = TEXT_6X1;
		listInit.textOffset = 0;
		listInit.textValOffset = 400;
	}
	listInit.textSize = 0;
	//listInit.textAlign = 8;
	listInit.fun = MenuListGetCallScenePage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	InitMenuList(listInit);

}

void MENU_005_CallScene_Init_TrsDev(int uMenuCnt)
{
	int x, y;
	char temp[20];
	POS pos;
	SIZE hv;
	//OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	//API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_CallScene, 2, 0);

	API_Event_IoServer_InnerRead_All(SIP_ENABLE, temp);
	
	CallSceneSetting = Get_NoDisturbSetting();
	if(CallSceneSetting == 0)
	{
		char call_scene[5];
		API_Event_IoServer_InnerRead_All(CallScene_SET, call_scene);
		if(atoi(call_scene)==5||atoi(temp))
			CallSceneSetting = atoi(call_scene);
	}
	bprintf("111111:%d\n",CallSceneIconSelect);
	CallSceneIconSelect = CallSceneSetting%CallScene_ICON_MAX;
	bprintf("111111:%d\n",CallSceneIconSelect);
	CallScenePageSelect = 0;
	if(memcmp(GetSysVerInfo_ms(),"01",2)==0&&CallSceneSetting==5)
	{
		char disp[100];
		int unicode_len;
		GetCallTransferParaTarget(0,NULL,disp);
		sprintf(temp," %s,%s",disp,GetCallTransferState()?"ON":"OFF");
		API_GetOSD_StringWithID(MESG_TEXT_CallSceneDivertState, NULL, 0, temp, strlen(temp), disp, &unicode_len);
		x = GetIconXY(ICON_176_KeyState).x;
		y = GetIconXY(ICON_176_KeyState).y;
		API_OsdStringDisplayExt(x, y,DISPLAY_STATE_COLOR, disp,unicode_len, 1,STR_UNICODE, 0);
	}
	CallSceneListShow();
	
	//DisplayCallScenePageIcon(CallScenePageSelect);
	//ListSelect(CallSceneIconSelect, 1);
	//API_MenuIconDisplaySelectOn(ICON_205_CallScene);
	API_MenuIconDisplaySelectOn(ICON_218_PublicSetTitle);	
	GetIconTextInfo(ICON_218_PublicSetTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, pos.y, DISPLAY_TITLE_COLOR,MESG_TEXT_CallScene,1, 0);
	API_SpriteDisplay_XY(GetIconXY(ICON_218_PublicSetTitle).x, GetIconXY(ICON_218_PublicSetTitle).y, SPRITE_CallScene);
	
	API_Event_IoServer_InnerRead_All(CallSceneMenuMode, (uint8*)temp);
		
	if(atoi(temp) == 1&&callSceneIconNum>3&&Get_SipSer_State() == 1)	//czn_20181227
	{
		API_OsdUnicodeStringDisplay(GetIconXY(ICON_176_KeyState).x,GetIconXY(ICON_176_KeyState).y+18, DISPLAY_LIST_COLOR,MESG_TEXT_ICON_340_CallSceneSipTest, 1, bkgd_w-(GetIconXY(ICON_176_KeyState).x));
	}
}

void MENU_005_CallScene_Exit_TrsDev(void)
{
	API_CallServer_RemoteBye(IxCallScene5,NULL);	//czn_20181227
	MenuListDisable(0);
}

void MENU_005_CallScene_Process_TrsDev(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int y;
	sip_account_Info sip_acc;		//czn_20181227
	char temp[20];
	LIST_ICON listIcon;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_176_KeyState:
				case ICON_340_CallSceneSipTest:	//czn_20181227
					API_Event_IoServer_InnerRead_All(CallSceneMenuMode, (uint8*)temp);
					if(atoi(temp)==1&&callSceneIconNum>3&&Get_SipSer_State() == 1)
					{
						Get_SipConfig_DirvertAccount(sip_acc.sip_account);
						API_CallServer_Invite(IxCallScene5,0,&sip_acc);
					}
					break;

				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					bprintf("!!!!!%d:%d\n",listIcon.mainIcon,callSceneIconNum);
					CallSceneSetting = listIcon.mainIcon;
					if(CallSceneSetting<0||CallSceneSetting >= callSceneIconNum)
					{
						return;
					}
					if(listIcon.subIcon == 0)
					{
						
						BusySpriteDisplay(1);
						//ListSelect(CallSceneIconSelect, 0);
						
						//ListSelect(CallSceneIconSelect, 1);
		
						
						
						//DisplayCallScenePageIcon(CallScenePageSelect);
						
						

						
						
						if(pcallSceneIconTable[CallSceneSetting].iCon==ICON_066_NormalUse)
						{
							Set_CallSceneCurPara_Scene(CallScene_NomalUse_Int);
							API_CallTransfer_Close();
						}
						else if(pcallSceneIconTable[CallSceneSetting].iCon==ICON_068_NoDisturbAlways||pcallSceneIconTable[CallSceneSetting].iCon==ICON_067_NoDisturb8H)
						{
							Set_CallSceneCurPara_Scene(CallScene_NoDisturb_Int);
							API_CallTransfer_Close();
							//Set_NoDisturbSetting();
						}
						else if(pcallSceneIconTable[CallSceneSetting].iCon==ICON_070_DivertCallSimultaneously||pcallSceneIconTable[CallSceneSetting].iCon==ICON_069_DivertCallIfNoAnswer)
						{
							Set_CallSceneCurPara_Scene(CallScene_DivertToPhone_Int);
							API_CallTransfer_Close();
						}
						else if(pcallSceneIconTable[CallSceneSetting].iCon==ICON_DivertToDev)
						{
							if(memcmp(GetSysVerInfo_ms(),"01",2)==0&&API_CallTransfer_Start()!=0)
							{
								BEEP_ERROR();
								BusySpriteDisplay(0);
								return;
							}
							Set_CallSceneCurPara_Scene(CallScene_DivertToDev_Int);
							CallSceneSetting=5;
						}
						
						if(Set_NoDisturbSetting(CallSceneSetting) == 0)
						{
							sprintf(temp, "%d", CallSceneSetting);
							API_Event_IoServer_InnerWrite_All(CallScene_SET, temp);
						}
						CallSceneIconSelect = CallSceneSetting;
						CallSceneListShow();
						API_MsSyncCallSceneSet(CallSceneSetting);//czn_20190221
						usleep(200000);
						popDisplayLastMenu();
					}
					else if(listIcon.subIcon == 1)
					{
						
						//Enter_CallTransferParaSettingRootMenu(0);
					}
					else if(listIcon.subIcon == 2)
					{
						if(memcmp(GetSysVerInfo_ms(),"01",2)==0&&pcallSceneIconTable[CallSceneSetting].iCon==ICON_DivertToDev)
							Enter_CallTransferParaSettingRootMenu(0);
					}

					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}
#if 0
int Get_NoDisturb8H_RemainTime(int *remain_hour,int *remain_min)
{
	unsigned char hour;
	unsigned char minute;
	unsigned char week;
	unsigned char year;
	unsigned char day;
	unsigned char month;
	
	struct tm tm_temp;
	time_t start_time,cur_time;
	int hold_min,remain_allmin;
	
	#if 0
	minute = NoDisturb8H_StartTime.RTC_Minutes;
	hour = NoDisturb8H_StartTime.RTC_Hours;
	week = NoDisturb8H_StartDate.RTC_WeekDay;
	year = NoDisturb8H_StartDate.RTC_Year;
	day = NoDisturb8H_StartDate.RTC_Date;
	month = NoDisturb8H_StartDate.RTC_Month;
	
	
	memset(&tm_temp,0,sizeof(struct tm));
	tm_temp.tm_hour = BcdToBin(hour);
	tm_temp.tm_min = BcdToBin(minute);
	tm_temp.tm_wday = BcdToBin(week);
	tm_temp.tm_year = BcdToBin(year);
	tm_temp.tm_mday = BcdToBin(day);
	tm_temp.tm_mon = BcdToBin(month);

	start_time = mktime(&tm_temp);

	RTC_DateTypeDef curdate = RtcGetDate();
	RTC_TimeTypeDef curtime = RtcGetTime();

	minute = curtime.RTC_Minutes;
	hour = curtime.RTC_Hours;
	week = curdate.RTC_WeekDay;
	year = curdate.RTC_Year;
	day = curdate.RTC_Date;
	month = curdate.RTC_Month;

	memset(&tm_temp,0,sizeof(struct tm));
	tm_temp.tm_hour = BcdToBin(hour);
	tm_temp.tm_min = BcdToBin(minute);
	tm_temp.tm_wday = BcdToBin(week);
	tm_temp.tm_year = BcdToBin(year);
	tm_temp.tm_mday = BcdToBin(day);
	tm_temp.tm_mon = BcdToBin(month);

	cur_time = mktime(&tm_temp);
	#else
	start_time = NoDisturb8H_StartTime;
	cur_time = time(NULL);
	#endif
	hold_min = (cur_time - start_time)/60;
	
	if(hold_min < 0)
	{
		if(remain_hour != NULL && remain_min!= NULL)
		{
			*remain_hour = 0;
			*remain_min = 0;
		}
		return 0;
	}

	//remain_allmin = 5 - hold_min;
	remain_allmin = 60*8 - hold_min;

	if(remain_allmin <= 0)
	{
		if(remain_hour != NULL && remain_min!= NULL)
		{
			*remain_hour = 0;
			*remain_min = 0;
		}
		return 0;
	}

	if(remain_hour != NULL && remain_min!= NULL)
	{
		*remain_hour = remain_allmin/60;
		*remain_min = remain_allmin%60;
	}

	return remain_allmin;
}

int Get_NoDisturbSetting(void)
{
	if(NoDisturbSetting == 1)
	{
		if(Get_NoDisturb8H_RemainTime(NULL,NULL) == 0)
			NoDisturbSetting = 0; 
	}
	return NoDisturbSetting;
}

void NoDisturbTimer_Callback(void)
{
	if(NoDisturbSetting == 1)
	{
		if(Get_NoDisturb8H_RemainTime(NULL,NULL) == 0)
		{
			NoDisturbSetting = 0; 
			API_LedDisplay_CloudBeDisturbed(); 	//czn_20170926		//API_LED_DISTURB_OFF();
			OS_StopTimer(&NoDisturbTimer);
		}
		else
		{
			OS_RetriggerTimer(&NoDisturbTimer);
		}
		return;
	}

	OS_StopTimer(&NoDisturbTimer);
}

int Set_NoDisturbSetting(int CallScene)
{
	static int nodisturbtimer_init = 0;
	
	if(nodisturbtimer_init == 0)
	{
		nodisturbtimer_init = 1;
		OS_CreateTimer(&NoDisturbTimer, NoDisturbTimer_Callback, 60000/25);
	}
	
	if(CallScene == 1 || CallScene == 2)
	{
		NoDisturbSetting = CallScene;

		if(NoDisturbSetting == 1)
		{
			// Get the current date time
			#if 0
			NoDisturb8H_StartDate = RtcGetDate();
			NoDisturb8H_StartTime= RtcGetTime();
			#else
			NoDisturb8H_StartTime = time(NULL);
			#endif

			API_LedDisplay_NotDisturb();		//czn_20170926		//API_LED_DISTURB_ON();
			OS_RetriggerTimer(&NoDisturbTimer);
		}
		else
		{
			API_LedDisplay_NotDisturb();		//czn_20170926		//API_LED_DISTURB_ON();
			OS_StopTimer(&NoDisturbTimer);
		}
	}
	else
	{
		NoDisturbSetting = 0;
		API_LedDisplay_CloudBeDisturbed();		//czn_20170926		API_LED_DISTURB_OFF();
		OS_StopTimer(&NoDisturbTimer);
	}

	return NoDisturbSetting;
}
#endif
