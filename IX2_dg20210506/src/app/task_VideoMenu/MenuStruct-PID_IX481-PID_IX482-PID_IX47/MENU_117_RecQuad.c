#include "MENU_public.h"
#include "onvif_discovery.h"
#include "MENU_117_RecQuad.h"
#include "obj_business_manage.h"		
#include "obj_IPCTableSetting.h"
#include "MENU_116_MultiRec.h"

#if 0
#define quadIpcMax		4
extern OS_TIMER timer_IPC_monitor_stop;
extern int IPC_monitor_time;

int ipc_quad_type[4]={-1,-1,-1,-1};
int quad_id;
unsigned char dvrQuadNum;
int dvrQuadIndex[4];
extern int recIpcIndex[4];

void DisplayIpcName(int x, int y, char* name)
{
	API_OsdStringDisplayExt(x, y, COLOR_WHITE, name, strlen(name), 0, STR_UTF8, 0);

}
void ipc_record_notice(int on, int x, int y)
{
	if(on)
	{
		API_SpriteDisplay_XY(x,y,SPRITE_RECORDING); 
	}
	else
	{
		API_SpriteClose(x,y,SPRITE_RECORDING);	
	}

}

unsigned char GetdvrQuadNum(void)
{
	int i;
	dvrQuadNum = 0;
	for(i=0; i<quadIpcMax; i++)
	{
		if(recIpcIndex[i] != -1)
			dvrQuadIndex[dvrQuadNum++] = recIpcIndex[i];
	}
	return dvrQuadNum;
}


int API_DvrQuart_start( void )
{
#if 0
	int i,x,y,w,h;	
	IPC_ONE_DEVICE dat;
	char temp[20];
	int vdtype;
	int channel;

	if(GetdvrQuadNum() == 0)
	{
		return -1;
	}
	//if(API_Business_Request(Business_State_MonitordvrQuad) == 0)
	//{
	//	API_Beep(BEEP_TYPE_DI3);
	//	return -1;
	//}
	API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, temp);
	IPC_monitor_time = atoi(temp);
	API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);

	for( i = 0; i < dvrQuadNum; i++ )
	{
		GetIpcCacheRecord(dvrQuadIndex[i], &dat);
		if(ipc_check_online(dat.IP, dat.ID) != 0)
		{
			continue;
		}
		GetdvrQuadPos(i,&x,&y,&w,&h);
		channel = (multi_vd_record[i].state == 0) ? dat.CH_QUAD : dat.CH_APP;
		printf("API_DvrQuart_start index:%d channel:%d w:%d h:%d\n", i, channel, dat.CH_DAT[channel].width, dat.CH_DAT[channel].height);
		if((dvrQuadNum == 1) && (multi_vd_record[i].state == 1))
		{
			vdtype = dvr_quad_to_full(0);
		}
		else
		{
			vdtype = api_ipc_start_mointor_size(i,dat.CH_DAT[channel].rtsp_url, 0, dat.CH_DAT[channel].width, dat.CH_DAT[channel].height, 0, x, y, w, h);
		}
		ipc_quad_type[i] = vdtype;
	}
	//sleep(1);
	API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
	StartInitOneMenu(MENU_117_RecQuad,0,1);
	return 0;
#endif
}

void IpcShowToFull(int id)
{
#if 0
	int i,x,y,w,h;	
	int ret=-1;
	if(id> dvrQuadNum -1)
		return;
	if(multi_vd_record[id].state)
	{
		ret = dvr_quad_to_full(id);
		if(!ret)
			quad_id = id;
		
		for( i = 0; i < dvrQuadNum; i++ )
		{
			GetdvrQuadPos(i,&x,&y,&w,&h);
			API_OsdStringClearExt(x+bkgd_w/4,y+10, bkgd_w/4, 40);
			ipc_record_notice(0, x+(bkgd_w/2)-32,y+10);
		}
		
		for( i = 0; i < dvrQuadNum; i++ )
			api_h264_show_stop(i);
	}
	else
	{
		for( i = 0; i < dvrQuadNum; i++ )
		{
			close_one_layer(i + 1);
			GetdvrQuadPos(i,&x,&y,&w,&h);
			API_OsdStringClearExt(x+bkgd_w/4,y+10, bkgd_w/4, 40);
		}
		ret = set_h264_show_pos(id+1,0,0,bkgd_w,bkgd_h);
		if(!ret)
			quad_id = id;
	}
#endif
}
void IpcShowToQuad(void)
{
#if 0
	int i,x,y,w,h;	
	char Dsname[21];
	IPC_ONE_DEVICE dat;
	if(multi_vd_record[quad_id].state)
	{
		dvr_full_to_quad(quad_id);
		quad_id = 4;
		for( i = 0; i < dvrQuadNum; i++ )
		{
			GetdvrQuadPos(i,&x,&y,&w,&h);
			if(ipc_quad_type[i] != -1)
			{
				dvr_show_to_quad(i,x,y,w,h);
			}
			GetIpcCacheRecord(dvrQuadIndex[i], &dat);
			DisplayIpcName(x+bkgd_w/4,y+10,dat.NAME);
			ipc_record_notice(multi_vd_record[i].state, x+(bkgd_w/2)-32,y+10);
		}
	}
	else
	{
		//clear_one_layer(quad_id+1, bkgd_w, bkgd_h);
		close_one_layer(quad_id+1);
		quad_id = 4;
		for( i = 0; i < dvrQuadNum; i++ )
		{
			GetdvrQuadPos(i,&x,&y,&w,&h);
			set_h264_show_pos(i+1,x,y,w,h);
			GetIpcCacheRecord(dvrQuadIndex[i], &dat);
			DisplayIpcName(x+bkgd_w/4,y+10,dat.NAME);
		}
	}
#endif	
}
#endif

void MENU_117_RecQuad_Init(int uMenuCnt)
{
#if 0
	int i,x,y,w,h;	
	IPC_ONE_DEVICE dat;
	
	OS_RetriggerTimer(&timer_IPC_monitor_stop);
	Power_Down_Timer_Stop();

	//API_BG_Display(3);
	quad_id = 4;
	API_TimeLapseStart(COUNT_RUN_UP , 0);
	for( i = 0; i < dvrQuadNum; i++ )
	{
		GetIpcCacheRecord(dvrQuadIndex[i], &dat);
		GetdvrQuadPos(i,&x,&y,&w,&h);
		DisplayIpcName(x+bkgd_w/4,y+10,dat.NAME);
		//ipc_record_notice(multi_vd_record[i].state, x+(bkgd_w/2)-32,y+10);
	}
#endif
}


void MENU_117_RecQuad_Exit(void)
{
#if 0
	int i;
	printf("MENU_117_RecQuad_Exit!!!\n");
	dvr_full_to_quad(quad_id);
	for( i = 0; i < dvrQuadNum; i++ )
	{
		api_ipc_stop_mointor_one_stream(i);
	}
	OS_StopTimer(&timer_IPC_monitor_stop);
	API_TimeLapseStop();
	//API_Business_Close(Business_State_MonitordvrQuad);
#endif
}

void MENU_117_RecQuad_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int ret;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_TALK:
					//multi_video_record_start();
					CloseOneMenu();
					break;
				case KEY_UNLOCK:
					CloseOneMenu();
				break;
				default:		
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			#if 0
			if(dvrQuadNum <= 1)
			{
				return;
			}
			if( quad_id == 4 )
			{
				ret = TouchToQuadId(pglobal_win_msg->wparam, pglobal_win_msg->lparam);
				printf("TouchToQuadId id = %d\n", ret);
				IpcShowToFull(ret);
			}
			else
			{
				IpcShowToQuad();
			}
			#endif
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
				
			case MSG_7_BRD_SUB_IPC_MonitorOff:
				CloseOneMenu();
				break;
			case MSG_7_BRD_SUB_RECORD_NOTICE:
				//SaveMultiRecordProcess(*(char*)(arg+sizeof(SYS_WIN_MSG)));
			break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
}


