#include "MENU_public.h"
#include "cJSON.h"

#define	PwdEdit_ICON_MAX	5
extern cJSON *pwdList;
extern int devIpManage;
cJSON *item=NULL;
char pwdName[21];
char pwdCode[10]={0};
char pwdCodeinput[10];

static IconAndText_t *PwdEditIconTable=NULL;
int PwdEditIconNum;
int PwdEditIconSelect;
int PwdEditPageSelect;
int PwdDelConfirm;
int PwdDelConfirmSelect;

const IconAndText_t PwdEditIconTable1[] = 
{
	{ICON_PwdEditNew, 					MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_PwdEditName,					MESG_TEXT_IPC_UserName},    
	{ICON_PwdEditSave, 					MESG_TEXT_IPC_Save},
	{ICON_PwdEditDelete, 				MESG_TEXT_Delete},
};
const IconAndText_t PwdEditIconTable2[] = 
{
	{ICON_PwdEditNew, 					MESG_TEXT_ICON_PrivateUnlockCode},
	{ICON_PwdEditName,					MESG_TEXT_IPC_UserName},    
	{ICON_PwdEditSave, 					MESG_TEXT_IPC_Save},
};

void EnterPwdEditMenu(int index)
{
	item = cJSON_GetArrayItem(pwdList,index);
	StartInitOneMenu(MENU_144_ix611PwdEdit,0,1);
}
void EnterPwdAddMenu(void)
{
	item = NULL;
	StartInitOneMenu(MENU_144_ix611PwdEdit,0,1);
}

static void DisplayPwdEditPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y, val_x;
	int pageNum;
	POS pos;
	SIZE hv;
		
	for(i = 0; i < PwdEdit_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			val_x = x+(hv.h - pos.x);
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			val_x = x+(hv.h - pos.x)/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*PwdEdit_ICON_MAX+i < PwdEditIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, PwdEditIconTable[i+page*PwdEdit_ICON_MAX].iConText, 1, val_x - x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(PwdEditIconTable[i+page*PwdEdit_ICON_MAX].iCon)
			{
				case ICON_PwdEditName:					
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, pwdName, strlen(pwdName), 1, STR_UTF8, hv.h-x);
					break;	
				case ICON_PwdEditNew:					
					API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, pwdCode, strlen(pwdCode), 1, STR_UTF8, hv.h-x);
					break;
			}
		}
	}
	pageNum = PwdEditIconNum/PwdEdit_ICON_MAX + (PwdEditIconNum%PwdEdit_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
}

void MENU_144_ix611PwdEdit_Init(int uMenuCnt)
{
	char display[100];
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	PwdEditIconSelect = 0;
	PwdEditPageSelect = 0;
	PwdDelConfirm = 0;
	PwdDelConfirmSelect = 0;
	
	if(item)
	{
		int id = GetEventItemInt(item, "ID");
		char* code = GetEventItemString(item, "CODE");	
		snprintf(display, 100, "[%d]%s", id, code);
		API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, display, strlen(display), 1, STR_UTF8, 0);
		PwdEditIconNum = sizeof(PwdEditIconTable1)/sizeof(PwdEditIconTable1[0]);
		PwdEditIconTable = PwdEditIconTable1;
		if(GetLastNMenu() == MENU_143_ix611PwdList)
		{
			GetJsonDataPro(item, "NOTE", pwdName);
			strcpy(pwdCode, code);
		}
	}
	else
	{
		API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, MESG_TEXT_PwdManagerAdd, 1, 0);
		PwdEditIconNum = sizeof(PwdEditIconTable2)/sizeof(PwdEditIconTable2[0]);
		PwdEditIconTable = PwdEditIconTable2;
		if(GetLastNMenu() == MENU_143_ix611PwdList)
		{
			sprintf(pwdName, "-");
			memset(pwdCode, 0, 8);
		}
	}
	DisplayPwdEditPageIcon(PwdEditPageSelect);
}


void MENU_144_ix611PwdEdit_Exit(void)
{

}

int PwdEditSave(int id)
{
	int ret;
	cJSON* Where;
	cJSON* newitem;

	printf("PwdEditSave id=%d\n",id);
    Where = cJSON_CreateObject();
	cJSON_AddNumberToObject(Where, "ID", id);

	newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "IX_ADDR", "");
	cJSON_AddNumberToObject(newitem, "ID", id);
	cJSON_AddStringToObject(newitem, "CODE", pwdCode);
	cJSON_AddStringToObject(newitem, "NOTE", pwdName);

	ret = API_RemoteTableUpdate(devIpManage, "PRIVATE_PWD", Where, newitem);
	//printf("API_RemoteTableUpdate ret=%d\n",ret);
	
	cJSON_Delete(Where);
	cJSON_Delete(newitem);
	return ret;
}
int PwdAddSave(cJSON* newitem)
{
	int ret = API_RemoteTableAdd(devIpManage, "PRIVATE_PWD", newitem);
	printf("API_RemoteTableAdd ret=%d\n",ret);
	return ret;
}
int PwdDelSave(int id)
{
	cJSON* Where;
	Where = cJSON_CreateObject();
	cJSON_AddNumberToObject(Where, "ID", id);
	return API_RemoteTableDelete(devIpManage, "PRIVATE_PWD", Where);
}

int confirm_set_pwd_code1(const char* input)
{
	int i;
	int len = strlen(input);
	if(len == 0 || len > 8)
	{
		return 0;
	}
	for( i = 0; i < len; i++ )
	{
		if( !isdigit(input[i]) )
		{
			return 0;
		}
	}
	strcpy(pwdCode, input);
	return 1;
}


void MENU_144_ix611PwdEdit_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_UNLOCK:
					break;
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
					
				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					PwdEditIconSelect = PwdEditPageSelect*PwdEdit_ICON_MAX + GetCurIcon() - ICON_007_PublicList1;
					
					if(PwdEditIconSelect < PwdEditIconNum)
					{
						switch(PwdEditIconTable[PwdEditIconSelect].iCon)
						{
							case ICON_PwdEditName:
								ClearConfirm(&PwdDelConfirm, &PwdDelConfirmSelect, PwdEdit_ICON_MAX);
								EnterKeypadMenu(KEYPAD_CHAR, MESG_TEXT_IPC_UserName, pwdName, 20, COLOR_WHITE, pwdName, 1, NULL);
							break;	
							case ICON_PwdEditNew:
								ClearConfirm(&PwdDelConfirm, &PwdDelConfirmSelect, PwdEdit_ICON_MAX);
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_PrivateUnlockCode, pwdCodeinput, 8, COLOR_WHITE, pwdCode, 1, confirm_set_pwd_code1);
							break;	
							case ICON_PwdEditSave:
								ClearConfirm(&PwdDelConfirm, &PwdDelConfirmSelect, PwdEdit_ICON_MAX);
								if(item)
								{
									if(pwdCode[0])
									{
										PwdEditSave(GetEventItemInt(item, "ID"));
										popDisplayLastMenu();
									}
									else
									{
										BEEP_ERROR();
									}									
								}
								else
								{
									if(pwdCode[0])
									{
										cJSON *new = API_RemoteTableNewRecord(devIpManage, "PRIVATE_PWD");
										cJSON_ReplaceItemInObjectCaseSensitive(new, "CODE", cJSON_CreateString(pwdCode));
										cJSON_ReplaceItemInObjectCaseSensitive(new, "NOTE", cJSON_CreateString(pwdName));
										PwdAddSave(new);
										cJSON_Delete(new);
										popDisplayLastMenu();
									}
									else
									{
										BEEP_ERROR();
									}										
								}
								
							break;	
							case ICON_PwdEditDelete:
								if(ConfirmSelect(&PwdDelConfirm, &PwdDelConfirmSelect, &PwdEditIconSelect, PwdEditPageSelect, PwdEdit_ICON_MAX))
								{
									return;
								}
								if(item)
								{
									PwdDelSave(GetEventItemInt(item, "ID"));
								}
								popDisplayLastMenu();
							break;	
						}
					}
					break;
					
				case ICON_201_PageDown:
					break;
				case ICON_202_PageUp:
					break;

					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}

