#include "MENU_public.h"
#include "MENU_006_SetCallTune.h"
#include "MenuSetCallTune_Business.h"

//#define	SetCallTune_ICON_MAX	5
int setCallTuneIconMax;
int setCallTuneIconSelect;
static int setCallTunePageSelect;
#if 0
int	 		ringNum;
RingList 	*pRingList = NULL;
#endif
//cao_20181020_s
unsigned char inputRingTime[3] = {0};
//cao_20181020_e

// zfz_20190601
const CallTuneIcon callTuneSetMode0[] = 
{
	{ICON_077_RingVolume,			MESG_TEXT_RingVolume, 			MESG_TEXT_RingVolumeSet, 			DAY_CALL_VOLUME},
	{ICON_099_RingTime, 			MESG_TEXT_ICON_099_RingTime,	MESG_TEXT_ICON_099_RingTime,		CALL_TUNE_TIME_LIMIT},
	{ICON_078_DS_SetTune,			MESG_TEXT_DS_SetTune, 			MESG_TEXT_DS_RING_SET, 				DS_TUNE_SELECT},
	{ICON_079_CDS_SetTune,			MESG_TEXT_CDS_SetTune, 			MESG_TEXT_CDS_RING_SET, 			CDS_TUNE_SELECT},
	{ICON_080_OS_SetTune,			MESG_TEXT_OS_SetTune, 			MESG_TEXT_OS_RING_SET, 				OS_TUNE_SELECT},
	{ICON_081_MSG_SetTune,			MESG_TEXT_MSG_SetTune, 			MESG_TEXT_MSG_RING_SET, 			MSG_TUNE_SELECT},
	{ICON_089_DoreBell_SetTune,		MESG_TEXT_DoreBell_SetTune, 	MESG_TEXT_DoreBell_RING_SET, 		DOORBELL_TUNE_SELECT},
	{ICON_090_Intercom_SetTune,		MESG_TEXT_Intercom_SetTune, 	MESG_TEXT_Intercom_RING_SET, 		INTERCOM_TUNE_SELECT},
	{ICON_091_InnerCall_SetTune,	MESG_TEXT_InnerCall_SetTune, 	MESG_TEXT_InnerCall_RING_SET, 		INNERCALL_TUNE_SELECT},
	{ICON_092_Alarm_SetTune,		MESG_TEXT_Alarm_SetTune, 		MESG_TEXT_ALARM_RING_SET, 			ALARM_TUNE_SELECT},
};

const unsigned char CallTuneSettingMode0Num = sizeof(callTuneSetMode0)/sizeof(callTuneSetMode0[0]);

const CallTuneIcon callTuneSetMode1[] = 
{
	{ICON_077_RingVolume,			MESG_TEXT_RingVolume, 			MESG_TEXT_RingVolumeSet, 			DAY_CALL_VOLUME},
	{ICON_099_RingTime, 			MESG_TEXT_ICON_099_RingTime,	MESG_TEXT_ICON_099_RingTime,		CALL_TUNE_TIME_LIMIT},
	{ICON_078_DS_SetTune,			MESG_TEXT_DS_SetTune, 			MESG_TEXT_DS_RING_SET, 				DS_TUNE_SELECT},
	{ICON_079_CDS_SetTune,			MESG_TEXT_CDS_SetTune, 			MESG_TEXT_CDS_RING_SET, 			CDS_TUNE_SELECT},
	{ICON_080_OS_SetTune,			MESG_TEXT_OS_SetTune, 			MESG_TEXT_OS_RING_SET, 				OS_TUNE_SELECT},
	//{ICON_081_MSG_SetTune,			MESG_TEXT_MSG_SetTune, 			MESG_TEXT_MSG_RING_SET, 			MSG_TUNE_SELECT},
	{ICON_089_DoreBell_SetTune,		MESG_TEXT_DoreBell_SetTune, 	MESG_TEXT_DoreBell_RING_SET, 		DOORBELL_TUNE_SELECT},
	{ICON_090_Intercom_SetTune,		MESG_TEXT_Intercom_SetTune, 	MESG_TEXT_Intercom_RING_SET, 		INTERCOM_TUNE_SELECT},
	{ICON_091_InnerCall_SetTune,	MESG_TEXT_InnerCall_SetTune, 	MESG_TEXT_InnerCall_RING_SET, 		INNERCALL_TUNE_SELECT},
	//{ICON_092_Alarm_SetTune,		MESG_TEXT_Alarm_SetTune, 		MESG_TEXT_ALARM_RING_SET, 			ALARM_TUNE_SELECT},
};

const unsigned char CallTuneSettingMode1Num = sizeof(callTuneSetMode1)/sizeof(callTuneSetMode1[0]);

CallTuneIcon *callTuneSet=NULL;
int CallTuneSettingNum=0;


void RingSelectSet(int value)
{
	char temp[20];
		
	sprintf(temp, "%d", pRingList[value].id);
	API_Event_IoServer_InnerWrite_All(callTuneSet[setCallTuneIconSelect+setCallTunePageSelect*setCallTuneIconMax].ioId, temp);

	BusySpriteDisplay(0);
	API_RingStop();
	API_RingTuneSelect(atoi(temp));
}

void RingVolumeSet(int value)
{
	char buff[5];
	sprintf(buff, "%d", value);
	API_Event_IoServer_InnerWrite_All(DAY_CALL_VOLUME, buff);
	BusySpriteDisplay(0);
	API_RingStop();
	API_RingVolSelect(value);
}

//cao_20181020_s
int SaveRingTimeSet(void)
{
	uint8 ringTime;
	
	ringTime = atoi(inputRingTime);

	if(ringTime >= 3 && ringTime <= 90)
	{
		char temp[20];
		
		sprintf(temp, "%d", ringTime);
		API_Event_IoServer_InnerWrite_All(CALL_TUNE_TIME_LIMIT, temp);
		return 1;
	}
	else
	{
		BEEP_ERROR();
		return 0;
	}
}
//cao_20181020_e

#if 1
// lzh_20181016_s
// 该函数仅仅在切换语言时调用，从而生产新的cache字符串文件
void DisplayTuneSettingPageIcon_CachePush(uint8 page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	uint8 setValue;
	char display[6];
	POS pos;
	SIZE hv;
	
	API_DisableOsdUpdate();
	
	for(i = 0; i < setCallTuneIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;

		API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
		if(page*setCallTuneIconMax+i < CallTuneSettingNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, callTuneSet[i+page*setCallTuneIconMax].setId, 1, (hv.h - pos.x)/2);
			//API_menu_display_cache_push_string(MENU_011_SETTING,page*setCallTuneIconMax+i,x,y,DISPLAY_ICON_X_WIDTH,CLEAR_STATE_H);
		}
	}
	API_menu_display_cache_push_string(MENU_011_SETTING,page,x,y,520/*DISPLAY_ICON_X_WIDTH*/,320);

	API_EnableOsdUpdate();		
}

void DisplayTuneSettingPageIcon_CachePop(uint8 page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	uint8 setValue;
	char display[6];
	POS pos;
	SIZE hv;
	
	API_DisableOsdUpdate();

	//if( page != 0 )
	{
		for(i = 0; i < setCallTuneIconMax; i++)
		{
			OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
			if(get_pane_type() == 5 )
			{
				x = pos.x+DISPLAY_DEVIATION_X;
				y = pos.y+5;
				API_OsdStringClearExt(x, y, bkgd_w-x, 80);
			}
			else
			{
				x = pos.x+DISPLAY_DEVIATION_X;
				y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
				API_OsdStringClearExt(x, y, bkgd_w-x, 40);
			}
			if(page*setCallTuneIconMax+i < CallTuneSettingNum)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, callTuneSet[i+page*setCallTuneIconMax].setId, 1, hv.h-x);
			}
		}
	}
	/*else
	{
		API_menu_display_cache_pop_string(MENU_011_SETTING,page);
		for(i = 0; i < setCallTuneIconMax; i++)
		{
			x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
			y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
			if(page*setCallTuneIconMax+i >= CallTuneSettingNum)
			{
				API_OsdStringClearExt(x, y, bkgd_w-x, CLEAR_STATE_H);
			}
		}
	}*/	
	API_EnableOsdUpdate();		
}
#endif 

static void DisplayTuneSettingPageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y;
	uint8 pageNum;
	uint8 setValue;
	char display[6];
	POS pos;
	SIZE hv;
	
	DisplayTuneSettingPageIcon_CachePop(page);

	for(i = 0; i < setCallTuneIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x += 120;
			y += 40;
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x += (hv.h - pos.x)/2;
		}
		if(page*setCallTuneIconMax+i < CallTuneSettingNum)
		{
			char temp[20];
			API_Event_IoServer_InnerRead_All(callTuneSet[i+page*setCallTuneIconMax].ioId, (uint8*)&temp);
			setValue = atoi(temp);
			//czn_20190221
			if(callTuneSet[i+page*setCallTuneIconMax].setId == MESG_TEXT_ICON_099_RingTime)
			{
				snprintf(display, 6, "[%02ds]", setValue);
			}
			else if(callTuneSet[i+page*setCallTuneIconMax].setId == MESG_TEXT_RingVolume && setValue == 0)
			{
				//x += (hv.h - pos.x)/2;
				API_OsdUnicodeStringDisplayWithIdAndAsc(x, y, COLOR_RED,MESG_TEXT_RingVolMute,1, "[","]",hv.h - x);
				continue;
			}
			else
			{
				snprintf(display, 6, "[%02d]", setValue);
			}
			//x += (hv.h - pos.x)/2;
			API_OsdStringDisplayExt(x, y, COLOR_RED, display, strlen(display),1,STR_UTF8, hv.h - x);
		}
	}

	pageNum = CallTuneSettingNum/setCallTuneIconMax + (CallTuneSettingNum%setCallTuneIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);	
}

// lzh_20181016_e


void TuneSetSelectProcess(int index)
{
	CallTuneIcon tempSelect;
	char tempChar[30];
	uint8 setValue;
	int i, j;
	int len;	//czn_20190221

	if(index >= CallTuneSettingNum)
	{
		return;
	}
	
	tempSelect = callTuneSet[index];

	if(tempSelect.iCon == ICON_077_RingVolume)
	{
		//czn_20190221_s
		//snprintf(tempChar, 30, "[%02d] %s", pRingList[i].id, pRingList[i].name);
		API_GetOSD_StringWithID(MESG_TEXT_RingVolMute, NULL, 0, NULL, 0, publicSettingDisplay[0]+1, &len);
		publicSettingDisplay[0][0] = len;
		//strcpy(tempChar,"mute");
		//for(publicSettingDisplay[0][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[0][0]; j += 2)
		//{
		//	publicSettingDisplay[0][j] = tempChar[j/2];
		//}
	
		for(i = 1; i <= 5; i++)
		{
			publicSettingDisplay[i][0] = 2;
			publicSettingDisplay[i][1] = i-1+'1';
		}
		API_Event_IoServer_InnerRead_All(tempSelect.ioId, tempChar);
		setValue = atoi(tempChar);
		if(setValue > 5)
		{
			setValue = 5;
		}
		EnterPublicSettingMenu(MESG_TEXT_ICON_024_CallTune, tempSelect.titleId, 6, setValue, RingVolumeSet);
		//czn_20190221_e
	}
	//cao_20181020_s
	else if(tempSelect.iCon == ICON_099_RingTime)
	{
		API_Event_IoServer_InnerRead_All(tempSelect.ioId, tempChar);
		setValue = atoi(tempChar);
		snprintf(inputRingTime,3,"%02d",setValue);
		EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_099_RingTime, inputRingTime, 2, COLOR_WHITE, inputRingTime, 1, SaveRingTimeSet);
		return;
	}
	//cao_20181020_e
	else
	{
		for(i = 0; i < ringNum; i++)
		{
			snprintf(tempChar, 30, "[%02d] %s", pRingList[i].id, pRingList[i].name);
			for(publicSettingDisplay[i][0] = strlen(tempChar) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
			{
				publicSettingDisplay[i][j] = tempChar[j/2];
				publicSettingDisplay[i][j+1]=0;
			}
		}
		
		API_Event_IoServer_InnerRead_All(tempSelect.ioId, tempChar);
		setValue = atoi(tempChar);
		for(i = 0; i < ringNum; i++)
		{
			if(setValue == pRingList[i].id)
			{
				setValue = i;
				break;
			}
		}
		if(i == ringNum)
		{
			setValue = 0;
		}
		
		EnterPublicSettingMenu(MESG_TEXT_ICON_024_CallTune, tempSelect.titleId, ringNum, setValue, RingSelectSet);
	}
	StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
}

static void MenuListGetCallTunePage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen, unicode_len;
	char unicode_buf[200];
	char display[80] = {0};
	
	int stringId;
	uint8 setValue;
	char temp[20];
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax + i;
		if(index < CallTuneSettingNum)
		{
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;

			API_GetOSD_StringWithID(callTuneSet[index].setId, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;

			unicode_len = 0;

			API_Event_IoServer_InnerRead_All(callTuneSet[index].ioId, (uint8*)&temp);
			setValue = atoi(temp);
			//czn_20190221
			if(callTuneSet[index].setId == MESG_TEXT_ICON_099_RingTime)
			{
				snprintf(display, 6, "[%02ds]", setValue);
				displayLen = strlen(display);
				unicode_len = 2*api_ascii_to_unicode(display,displayLen,unicode_buf);
			}
			else if(callTuneSet[index].setId == MESG_TEXT_RingVolume && setValue == 0)
			{
				API_GetOSD_StringWithID(MESG_TEXT_RingVolMute, "[", strlen("["), "]", strlen("]"), unicode_buf, &unicode_len); 
			}
			else
			{
				snprintf(display, 6, "[%02d]", setValue);
				displayLen = strlen(display);
				unicode_len = 2*api_ascii_to_unicode(display,displayLen,unicode_buf);
			}
			
			memcpy(pDisp->disp[pDisp->dispCnt].val, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].valLen = unicode_len;
			pDisp->dispCnt++;
		}
	}

}

void MENU_006_SetCallTune_Init(int uMenuCnt)
{
	#if 0
	{
		int unicode_len;
		char unicode_buf[200];
		LIST_INIT_T listInit;
		
		API_MenuIconDisplaySelectOn(ICON_024_CallTune);
		API_GetOSD_StringWithIcon(ICON_024_CallTune, unicode_buf, &unicode_len);
		
		listInit = ListPropertyDefault();
		
		listInit.listType = TEXT_6X1;
		listInit.listCnt = CallTuneSettingNum;
		listInit.valEn = 1;
		listInit.textValOffset = 600;
		listInit.fun = MenuListGetCallTunePage;
		listInit.titleStr = unicode_buf;
		listInit.titleStrLen = unicode_len;
		
		if(GetLastNMenu() != MENU_001_MAIN)
		{
			listInit.currentPage = setCallTunePageSelect;
		}
		
		InitMenuList(listInit);
	}
	#else
	{
		POS pos;
		SIZE hv;
		char temp[10];
		API_Event_IoServer_InnerRead_All(CallTuneMenuMode, (uint8*)temp);
		
		if(atoi(temp) == 1)
		{
			callTuneSet = callTuneSetMode1;
			CallTuneSettingNum = CallTuneSettingMode1Num;
		}
		else
		{
			callTuneSet = callTuneSetMode0;
			CallTuneSettingNum = CallTuneSettingMode0Num;
		}
		OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
		if(GetLastNMenu() == MENU_001_MAIN)
		{
			setCallTunePageSelect = 0;
		}
		setCallTuneIconMax = GetListIconNum();
		API_MenuIconDisplaySelectOn(ICON_024_CallTune);
		// lzh_20181016_s
		//API_OsdStringClearExt(DISPLAY_TITLE_X,DISPLAY_TITLE_Y,DISPLAY_TITLE_WIDTH,CLEAR_STATE_H);
		// lzh_20181016_e	
		DisplayTuneSettingPageIcon(setCallTunePageSelect);
		
		API_OsdUnicodeStringDisplayWithIcon(pos.x, hv.v/2, DISPLAY_TITLE_COLOR, ICON_024_CallTune, 1, 0);
	}
	#endif
}

void MENU_006_SetCallTune_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_024_CallTune);
	MenuListDisable(0);
}

void MENU_006_SetCallTune_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	CallTuneIcon tempSelect;
	// 虚拟按键处理
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
					break;
					
				case KEY_UP:
					PublicListUpProcess(&setCallTuneIconSelect, &setCallTunePageSelect, setCallTuneIconMax, CallTuneSettingNum, (DispListPage)DisplayTuneSettingPageIcon);
					break;
					
				case KEY_DOWN:
					PublicListDownProcess(&setCallTuneIconSelect, &setCallTunePageSelect, setCallTuneIconMax, CallTuneSettingNum, (DispListPage)DisplayTuneSettingPageIcon);
					break;
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			dprintf("GetCurIcon() = %d\n", GetCurIcon());
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&setCallTunePageSelect, setCallTuneIconMax, CallTuneSettingNum, (DispListPage)DisplayTuneSettingPageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&setCallTunePageSelect, setCallTuneIconMax, CallTuneSettingNum, (DispListPage)DisplayTuneSettingPageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					setCallTuneIconSelect = GetCurIcon()-ICON_007_PublicList1;

					TuneSetSelectProcess(setCallTunePageSelect*setCallTuneIconMax+setCallTuneIconSelect);

					break;
					
				case ICON_888_ListView:
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon >= 0)
					{
						setCallTunePageSelect = MenuListGetCurrentPage();
						TuneSetSelectProcess(listIcon.mainIcon);
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_WIFI_CONNECTING:
			case MSG_7_BRD_SUB_WIFI_CONNECTED:
			case MSG_7_BRD_SUB_WIFI_OPEN:
			case MSG_7_BRD_SUB_WIFI_CLOSE:
			case MSG_7_BRD_SUB_WIFI_DISCONNECT:
				
				break;
				
			case MSG_7_BRD_SUB_WIFI_SEARCHING:
				break;
			case MSG_7_BRD_SUB_WIFI_SEARCH_OVER:
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}
#if 0
uint8 LoadRingPlayList(const char* fileName)
{
	#define ONE_RECORD_LEN  	100
	
	char 			buff[ONE_RECORD_LEN];
	char 			*pos1,*pos2;
	unsigned char 	processState;	//处理状态
	FILE 			*file = NULL;
	RingList 		tempOneRing;
	RingList		*pTemp;
	int				ringNameLen;
	int				ringId;

	if( (file=fopen(fileName,"r")) == NULL )
	{
		ringNum = 0;
		return 0;
	}
	
	for( ringNum = 0, memset(buff, 0, ONE_RECORD_LEN); fgets(buff,ONE_RECORD_LEN-1,file) != NULL && ringNum<30; memset(buff, 0, ONE_RECORD_LEN))
	{
		for(processState = 0; processState < 2;)
		{
			switch(processState)
			{
				case 0:
					pos1 = strchr( buff, '<');
					if( pos1 != NULL )
					{
						ringId = atoi(++pos1);
						if(ringId > 255)
						{
							processState = 2;
						}
						else
						{
							tempOneRing.id = ringId;
							processState = 1;
						}
					}
					else
					{
						processState = 2;
					}
					break;
				case 1:
					pos2 = strstr( pos1, "/mnt/nand1-2/rings/");
					if( pos2 != NULL )
					{
						pos2 += strlen("/mnt/nand1-2/rings/");
						pos1 = strchr( pos2, '\"');
						if( pos1 != NULL )
						{
							*pos1 = 0;
							ringNum++;
							ringNameLen = (strlen(pos2)>20? 20:strlen(pos2));
							memcpy(tempOneRing.name, pos2, ringNameLen);
							tempOneRing.name[ringNameLen] = 0;
							pTemp = pRingList;
							pRingList = realloc(pTemp, ringNum*sizeof(RingList));
							if(pRingList != NULL)
							{
								memcpy((char*)(pRingList+ringNum-1), (char*)&tempOneRing, sizeof(RingList));
							}
							else
							{
								free(pTemp);
								ringNum = 0;
								return ringNum;
							}
						}
					}
					processState = 2;
					break;		
			}
		}
	}

	return ringNum;
}

unsigned char GetRingId(int index)
{
	unsigned char ret = 0;
	
	if(index < ringNum)
	{
		ret = pRingList[index].id;
	}
	return ret;
}
#endif

