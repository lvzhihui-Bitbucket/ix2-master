#include "MENU_public.h"
#include "obj_IPCTableSetting.h"
#include "MENU_036_ListEdit.h"
//#define	ipcSearchingIconMax		5

int ipcSearchingIconMax;
int ipcSearchingIconSelect;
int ipcSearchingPageSelect;
int ipcSearchingIndex;



void DisplayOnePageSearchingIPC(uint8 page)
{
	int i, x, y, maxPage, listStart, maxList;
	char display[100];
	char ip[20];
	char name[100];
	POS pos;
	SIZE hv;
	
	maxList = get_ipc_device_total();
	
	maxPage = maxList/ipcSearchingIconMax + (maxList%ipcSearchingIconMax ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, maxPage);

	listStart = page*ipcSearchingIconMax;
	
	for( i = 0; i < ipcSearchingIconMax; i++ )
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//x = DISPLAY_LIST_X+DISPLAY_DEVIATION_X;
		//y = DISPLAY_LIST_Y+i*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y;
		API_OsdStringClearExt(x, y, hv.h - x, 40);
		if(listStart+i < maxList)
		{
			//onvif_discovery_device_list_get_name(listStart+i, name);
			//onvif_discovery_device_list_get_ip(listStart+i, ip);
			ipc_discovery_device_get_addr(listStart+i, ip, name);
			snprintf(display, 100, "[%s] %s", ip, name);
			API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, display, strlen(display), 1, STR_UTF8, 0);
		}
	}

	
}


void MENU_060_IPC_Searching_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_IPC_Searching,1, 0);
	API_MenuIconDisplaySelectOn(ICON_026_InstallerSetup);
	//if(GetLastNMenu() == MENU_036_LIST_EDIT)
	{
		BusySpriteDisplay(1);
		if(GetIpcSetSelect()==0)
			ChangeDefaultGateway(NET_ETH0, NULL);
		else
			ChangeDefaultGateway(NET_WLAN0, NULL);
		onvif_device_discovery();
		usleep(100*1000);
		ChangeDefaultGateway(NULL, NULL);
		BusySpriteDisplay(0);
	}
	ipcSearchingIconSelect = 0;
	ipcSearchingPageSelect = 0;
	ipcSearchingIconMax = GetListIconNum();
	DisplayOnePageSearchingIPC(ipcSearchingPageSelect);
}


void MENU_060_IPC_Searching_Exit(void)
{
	//SaveIpcToFile();
}

void MENU_060_IPC_Searching_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char ip[20];
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;

				case ICON_047_Home:
					GoHomeMenu();
					break;
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					ipcSearchingIconSelect = GetCurIcon() - ICON_007_PublicList1;
					ipcSearchingIndex = ipcSearchingPageSelect*ipcSearchingIconMax + ipcSearchingIconSelect;
					
					if(ipcSearchingIndex >= get_ipc_device_total())
					{
						return;
					}

					ipc_discovery_device_list_get_url(ipcSearchingIndex, ip);
					//strcpy(ipcRecord.USER, "admin");
					//strcpy(ipcRecord.PWD, "admin");
					//StartInitOneMenu(MENU_055_IPC_LOGIN,0,1);
					EnterIpcLoginMenu(ip, NULL, NULL, 0);
					break;
				case ICON_201_PageDown:
					PublicPageDownProcess(&ipcSearchingPageSelect, ipcSearchingIconMax, get_ipc_device_total(), (DispListPage)DisplayOnePageSearchingIPC);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&ipcSearchingPageSelect, ipcSearchingIconMax, get_ipc_device_total(), (DispListPage)DisplayOnePageSearchingIPC);
					break;			
			}
		}
	}
	
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}




