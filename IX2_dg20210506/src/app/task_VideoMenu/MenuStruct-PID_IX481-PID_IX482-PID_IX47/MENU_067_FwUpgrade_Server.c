#include "MENU_067_FwUpgrade_Server.h"


#define	FwUpgradeServer_ICON_MAX	5



int FwUpgradeServerIconSelect;
int FwUpgradeServerPage;



static void DisplayOnePageFwUpgradeServer(int page);
void FwUpgradeServerSelectFlagDisplay(void);
int Update_FwUpgrade_OtherServer(char *server);
int Edit_FwUpgradeSer_Confirm(char *server);
char other_server[16];
static int OtherSer_EditConfim = 0;

void MENU_067_FwUpgradeServer_Init(int uMenuCnt)
{	
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_045_Upgrade);
	FwUpgradeServerIconSelect = 0;
	FwUpgradeServerPage = 0;
	API_OsdStringClearExt(pos.x, hv.v/2, 300, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_FwUpgrage_Server,1, 0);
	if(OtherSer_EditConfim)
	{
		OtherSer_EditConfim = 0;
		Update_FwUpgrade_OtherServer(other_server);
		Update_FwUpgradeServer_Select(0);
		API_DisableOsdUpdate();
		DisplayOnePageFwUpgradeServer(FwUpgradeServerPage);
		API_EnableOsdUpdate();
		usleep(200000);
		EnterInstallSubMenu(MENU_066_FwUpgrade, 0);		
		//popDisplayLastMenu();
	}
	else
	{
		API_DisableOsdUpdate();
		DisplayOnePageFwUpgradeServer(FwUpgradeServerPage);
		API_EnableOsdUpdate();
	}
}

void MENU_067_FwUpgradeServer_Exit(void)
{
	int i;

	for(i=0;i<FwUpgradeServer_ICON_MAX;i++)
	{
		ListSelect(i, 0);
	}
	
	API_RingStop();
	API_MenuIconDisplaySelectOff(ICON_045_Upgrade);
}

void MENU_067_FwUpgradeServer_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	uint8 setValue;
	int i, x, y;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				case ICON_047_Home:
					GoHomeMenu();
					break;
				case ICON_201_PageDown:
					//PublicPageDownProcess(&publicSettingPage, PublicSetting_ICON_MAX, publicSettingMaxNum, (DispListPage)DisplayOnePagepublicSetting);
					break;			
				case ICON_202_PageUp:
					//PublicPageUpProcess(&publicSettingPage, PublicSetting_ICON_MAX, publicSettingMaxNum, (DispListPage)DisplayOnePagepublicSetting);
					break;			
					
				case ICON_007_PublicList1:
					Get_FwUpgrade_OtherServer(other_server);
					EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_Server, other_server, 15, COLOR_WHITE, other_server, 1, Edit_FwUpgradeSer_Confirm);
					break;
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					FwUpgradeServerIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					//if(publicSettingSave != publicSettingValue)
					{
						Update_FwUpgradeServer_Select(FwUpgradeServerIconSelect);
						FwUpgradeServerSelectFlagDisplay();
						//BusySpriteDisplay(1);
						usleep(200000);
						//PublicSettingUpdateValue(publicSettingValue);
					}
					//popDisplayLastMenu();
					EnterInstallSubMenu(MENU_066_FwUpgrade, 0);	
					break;
			}
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}
static void DisplayOnePageFwUpgradeServer(int page)
{
	uint8 i = 0;
	uint16 x, y;
	uint8 pageNum;
	char disp[40];
	int disp_len;
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	OSD_GetIconInfo(ICON_007_PublicList1, &pos, &hv);
	if(get_pane_type() == 5 )
	{
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+5;
		API_OsdStringClearExt(x, y, bkgd_w-x, 80);
	}
	else
	{
		x = pos.x+DISPLAY_DEVIATION_X;
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		API_OsdStringClearExt(x, y, bkgd_w-x, 40);
	}
	ListSelect(0, 0);
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	disp_len = Get_FwUpgradeServer_Disp(0, disp);
	if(disp_len > 0)
	{
		
		//y1= y;//VALUE_DEVIATION_Y;
		API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, disp, disp_len, 1, STR_UTF8, hv.h-x);
	}
	else
	{
		//API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, "[Input IP address]", strlen("[Input IP address]"),1,STR_UTF8, hv.h-x);
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_InputIPaddress, 1, hv.h-x);
	}
	
	if(page*FwUpgradeServer_ICON_MAX+0 == Get_FwUpgradeServer_Select())
	{
		ListSelect(0, 1);	//r
	}

	for(i = 1; i < FwUpgradeServer_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		ListSelect(i, 0);
		y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//if(page*PublicSetting_ICON_MAX+i < publicSettingMaxNum)
		{
			disp_len = Get_FwUpgradeServer_Disp(i, disp);
			if(strcmp(disp,"SD card")==0)
			{
				API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, MESG_TEXT_Icon_058_SDCard, 1, hv.h - x);
			}
			else
				API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, disp, disp_len,1,STR_UTF8, hv.h - x);
			if(page*FwUpgradeServer_ICON_MAX+i == Get_FwUpgradeServer_Select())
			{
				ListSelect(i, 1);
			}
		}
	}
	
	//API_EnableOsdUpdate();

	//pageNum = publicSettingMaxNum/PublicSetting_ICON_MAX + (publicSettingMaxNum%PublicSetting_ICON_MAX ? 1 : 0);
	
	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, 1);
}

void FwUpgradeServerSelectFlagDisplay(void)
{
	int x, y, i;
	
	for(i = 0; i < FwUpgradeServer_ICON_MAX; i++)//r
	{
		ListSelect(i, 0);		
		//if(publicSettingPage*PublicSetting_ICON_MAX+i < publicSettingMaxNum)
		{
			if( i == Get_FwUpgradeServer_Select())
			{
				ListSelect(i, 1);
			}
		}
	}

}

int Edit_FwUpgradeSer_Confirm(char *server)
{
	if(strlen(server) <= 0)
		return 0;
	
	API_Event_IoServer_InnerWrite_All(FWUPGRADE_OTHER_SER, (uint8*)server);
	OtherSer_EditConfim = 1;
	return 1;
}


