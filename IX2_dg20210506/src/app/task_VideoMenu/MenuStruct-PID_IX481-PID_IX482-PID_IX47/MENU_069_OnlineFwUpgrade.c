#include "MENU_069_OnlineFwUpgrade.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_RemoteUpgrade.h"
#include "MenuBackFwUpgrade_Business.h"

static RemoteUpgrade_T remoteUpgradeCmdData;

#define MAX_SET_NUM 	5
#define	RemoteFwUpgrade_ICON_MAX	5

static int RemoteFwUpgradeIconSelect;
static int RemoteFwUpgradePageSelect;
static int RemoteFwUpgrade_State;
static int RemoteFwUpgradeRebootTime;

static int  RemoteFwUpgrade_Ser_Select;
static char RemoteFwUpgrade_Ser_Disp[5][40];
static char RemoteFwUpgrade_Code[7] = {0};

static int	 RemoteFwUpgradeLine2StrId;
static int	 RemoteFwUpgradeLine3StrId;
static int  RemoteFwUpgradeLine4StrId;
static char RemoteFwUpgradeLine2ValueStr[40];
static char RemoteFwUpgradeLine3ValueStr[40];
static char RemoteFwUpgradeLine4ValueStr[40];
static int RemoteFwUpgradeTimeCnt = 0;
#if 0
void GetRemoteFwUpgradeInfo(int id, char* value, RemoteUpgrade_T data)
{
	char temp[30];
	char *pos1, *pos2;

	snprintf(temp, 30, "<ID=%02d,Value=", id);
	pos1 = strstr(data.data, temp);
	
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			value[0] = 0;
			memcpy(value, pos1 + strlen("<ID=01,Value="), pos2 - pos1 - strlen("<ID=01,Value="));
		}
	}
}
#endif
void Menu069DecodeRemoteUpgradeData(void)
{
	#define VALUE_LEN	500

	int i, *pos1;
	char temp[VALUE_LEN];
	
	RemoteFwUpgrade_State = remoteUpgradeCmdData.state;

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_SERVER_DISP_ID, temp, remoteUpgradeCmdData);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 5; i++, pos1 = strtok(NULL,","))
	{
		strcpy(RemoteFwUpgrade_Ser_Disp[i], pos1);
	}
	
	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_SERVER_SELECT_ID, temp, remoteUpgradeCmdData);
	RemoteFwUpgrade_Ser_Select = atoi(temp);
	RemoteFwUpgrade_Ser_Select  =  (RemoteFwUpgrade_Ser_Select >= 5) ? 1 : RemoteFwUpgrade_Ser_Select;

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_UPGRADE_CODE_ID, temp, remoteUpgradeCmdData);
	strcpy(RemoteFwUpgrade_Code, temp);

	strcpy(RemoteFwUpgradeLine2ValueStr, "");
	strcpy(RemoteFwUpgradeLine3ValueStr, "");
	strcpy(RemoteFwUpgradeLine4ValueStr, "");

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE2_DISP_ID, temp, remoteUpgradeCmdData);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			RemoteFwUpgradeLine2StrId = atoi(pos1);
		}
		else
		{
			strcpy(RemoteFwUpgradeLine2ValueStr, pos1);
		}
	}

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE3_DISP_ID, temp, remoteUpgradeCmdData);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			RemoteFwUpgradeLine3StrId = atoi(pos1);
		}
		else
		{
			strcpy(RemoteFwUpgradeLine3ValueStr, pos1);
		}
	}

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE4_DISP_ID, temp, remoteUpgradeCmdData);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			RemoteFwUpgradeLine4StrId = atoi(pos1);
		}
		else
		{
			strcpy(RemoteFwUpgradeLine4ValueStr, pos1);
		}
	}

	
	//memset(temp, 0, VALUE_LEN);
	//GetRemoteFwUpgradeInfo(FW_INFO_UPGRADE_REBOOT_TIME_ID, temp, remoteUpgradeCmdData);
	//RemoteFwUpgradeRebootTime = atoi(temp);

}

void Menu069EncodeRemoteUpgradeData(void)
{
	#define VALUE_LEN	500
	char tempData[VALUE_LEN];
	
	memset(&remoteUpgradeCmdData, 0, sizeof(remoteUpgradeCmdData));
	
	remoteUpgradeCmdData.OP_CODE = UpgradeCMD_OP_CODE_GET_STATE;
	remoteUpgradeCmdData.state = RemoteFwUpgrade_State;

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%s,%s,%s,%s,%s>", FW_INFO_SERVER_DISP_ID, RemoteFwUpgrade_Ser_Disp[0],
	RemoteFwUpgrade_Ser_Disp[1], RemoteFwUpgrade_Ser_Disp[2], RemoteFwUpgrade_Ser_Disp[3], RemoteFwUpgrade_Ser_Disp[4]);
	strcat(remoteUpgradeCmdData.data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d>", FW_INFO_SERVER_SELECT_ID, RemoteFwUpgrade_Ser_Select);
	strcat(remoteUpgradeCmdData.data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%s>", FW_INFO_UPGRADE_CODE_ID, RemoteFwUpgrade_Code);
	strcat(remoteUpgradeCmdData.data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE2_DISP_ID, RemoteFwUpgradeLine2StrId, RemoteFwUpgradeLine2ValueStr);
	strcat(remoteUpgradeCmdData.data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE3_DISP_ID, RemoteFwUpgradeLine3StrId, RemoteFwUpgradeLine3ValueStr);
	strcat(remoteUpgradeCmdData.data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE4_DISP_ID, RemoteFwUpgradeLine4StrId, RemoteFwUpgradeLine4ValueStr);
	strcat(remoteUpgradeCmdData.data, tempData);
	
	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d>", FW_INFO_UPGRADE_REBOOT_TIME_ID, RemoteFwUpgradeRebootTime);
	strcat(remoteUpgradeCmdData.data, tempData);
}

const IconAndText_t RemoteFwUpgradeIconTable[] = 
{
	{ICON_FwUpgrade_Server,			MESG_TEXT_ICON_FwUpgrage_Server},    
	{ICON_FwUpgrade_DLCode,			MESG_TEXT_ICON_FwUpgrage_DLCode},
	{ICON_FwUpgrade_CodeInfo,		NULL},
	{ICON_FwUpgrade_Notice,			NULL},
	{ICON_FwUpgrade_Operation,		NULL},
};

const int RemoteFwUpgradeIconNum = sizeof(RemoteFwUpgradeIconTable)/sizeof(RemoteFwUpgradeIconTable[0]);

static void DisplayRemoteFwUpgradePageIcon(uint8 page)
{
	uint8 i;
	uint16 x, y,x1,y1;
	int pageNum;
	char display[100];
	POS pos;
	SIZE hv;

	for(i = 0; i < RemoteFwUpgrade_ICON_MAX; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			x1 = x + 100;
			y1 = y + 40;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			x1 =pos.x+ (hv.h - pos.x)/2;
			y1 =y;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*RemoteFwUpgrade_ICON_MAX+i < RemoteFwUpgradeIconNum)
		{
			switch(RemoteFwUpgradeIconTable[i+page*RemoteFwUpgrade_ICON_MAX].iCon)
			{
				case ICON_FwUpgrade_Server:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RemoteFwUpgradeIconTable[i+page*RemoteFwUpgrade_ICON_MAX].iConText, 1, bkgd_w-x);
					if(RemoteFwUpgrade_Ser_Disp[RemoteFwUpgrade_Ser_Select]  != 0)
					{
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, RemoteFwUpgrade_Ser_Disp[RemoteFwUpgrade_Ser_Select], strlen(RemoteFwUpgrade_Ser_Disp[RemoteFwUpgrade_Ser_Select]), 1, STR_UTF8, bkgd_w-x1);
					}
					break;
				
				case ICON_FwUpgrade_DLCode:
					API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RemoteFwUpgradeIconTable[i+page*RemoteFwUpgrade_ICON_MAX].iConText, 1, bkgd_w-x);
					if(RemoteFwUpgrade_Code[0]  != 0)
					{
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, RemoteFwUpgrade_Code, strlen(RemoteFwUpgrade_Code), 1, STR_UTF8, bkgd_w-x1);
					}
					break;
					
				case ICON_FwUpgrade_CodeInfo:
					if(RemoteFwUpgradeLine2StrId != 0)
					{
						#if 0
						if(RemoteFwUpgradeLine2ValueStr[0] == 0)
						{
							len = bkgd_w-x;
						}
						else
						{
							len = (hv.h - pos.x)/2;
						}
						#endif
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RemoteFwUpgradeLine2StrId, 1, bkgd_w-x);
					}
					
					if(RemoteFwUpgradeLine2ValueStr[0] != 0)
					{
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, RemoteFwUpgradeLine2ValueStr, strlen(RemoteFwUpgradeLine2ValueStr), 1, STR_UTF8, bkgd_w-x1);
					}
					break;
					
				case ICON_FwUpgrade_Notice:
					if(RemoteFwUpgradeLine3StrId != 0)
					{
						if(RemoteFwUpgradeLine3StrId == MESG_TEXT_FwUpgrage_InstallOk)
						{
							RemoteFwUpgradeRebootTime = 100;
						}
						#if 0
						if(RemoteFwUpgradeLine3ValueStr[0] == 0)
						{
							len = bkgd_w-x;
						}
						else
						{
							len = (hv.h - pos.x)/2;
						}
						#endif
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RemoteFwUpgradeLine3StrId, 1, bkgd_w-x);
					}
					
					if(RemoteFwUpgradeLine3ValueStr[0] != 0)
					{
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, RemoteFwUpgradeLine3ValueStr, strlen(RemoteFwUpgradeLine3ValueStr), 1, STR_UTF8, bkgd_w-x1);
					}
					break;
					
				case ICON_FwUpgrade_Operation:
					if(RemoteFwUpgradeLine4StrId != 0)
					{
						#if 0
						if(RemoteFwUpgradeLine4ValueStr[0] == 0)
						{
							len = bkgd_w-x;
						}
						else
						{
							len = (hv.h - pos.x)/2;
						}
						#endif
						API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, RemoteFwUpgradeLine4StrId, 1, bkgd_w-x);
					}
					
					if(RemoteFwUpgradeLine4ValueStr[0] != 0)
					{
						API_OsdStringDisplayExt(x1, y1, DISPLAY_STATE_COLOR, RemoteFwUpgradeLine4ValueStr, strlen(RemoteFwUpgradeLine4ValueStr), 1, STR_UTF8, bkgd_w-x1);
					}
					break;

			}
		}
	}
	pageNum = RemoteFwUpgradeIconNum/RemoteFwUpgrade_ICON_MAX + (RemoteFwUpgradeIconNum%RemoteFwUpgrade_ICON_MAX ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

void RemoteUpgradeServerSet(int select)
{
	if(select < MAX_SET_NUM && select > 0)
	{
		RemoteFwUpgrade_Ser_Select = select;
		Menu069EncodeRemoteUpgradeData();
		API_RemoteUpgradeSetInfo(GetSearchOnlineIp(), &remoteUpgradeCmdData, 1);
	}
}

int RemoteUpgradeCodeSet(char *code)
{
	int i;
	
	if(strlen(code) != 6)
		return 0;
	
	for(i = 0;i < 6;i++)
	{
		if(code[i] < '0' || i > '9')
			return 0;
	}
	
	strcpy(RemoteFwUpgrade_Code, code);

	Menu069EncodeRemoteUpgradeData();
	
	API_RemoteUpgradeSetInfo(GetSearchOnlineIp(), &remoteUpgradeCmdData, 1);

	return 1;
}

int InputRemoteUpgradeServerSet(char* input)
{
	if(strlen(input))
	{
		strcpy(RemoteFwUpgrade_Ser_Disp[0], input);
		RemoteFwUpgrade_Ser_Select = 0;
	}
	else
	{
		strcpy(RemoteFwUpgrade_Ser_Disp[0], "Please input ip");
	}

	Menu069EncodeRemoteUpgradeData();
	
	API_RemoteUpgradeSetInfo(GetSearchOnlineIp(), &remoteUpgradeCmdData, 1);
	popDisplayLastNMenu(2);
	return -1;
}


void RemoteUpgradeInputServerProcess(void)
{
	EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_Server, RemoteFwUpgrade_Ser_Disp[0], 32, COLOR_WHITE, RemoteFwUpgrade_Ser_Disp[0], 1, InputRemoteUpgradeServerSet);
}


void MENU_069_OnlineFwUpgrade_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_MenuIconDisplaySelectOn(ICON_045_Upgrade);
	API_OsdStringClearExt(pos.x, hv.v/2, 300, CLEAR_STATE_H);
	API_OsdUnicodeStringDisplay(pos.x, hv.v/2, DISPLAY_TITLE_COLOR,MESG_TEXT_ICON_FwUpgrage,1, 0);

	if(API_RemoteUpgradeStart(GetSearchOnlineIp(), &remoteUpgradeCmdData, 4) != 0)
	{
		BEEP_ERROR();
		popDisplayLastMenu();
		return;
	}
	
	printf("remoteUpgradeCmdData.data = %s.\n", remoteUpgradeCmdData.data);
	
	Menu069DecodeRemoteUpgradeData();
	
	API_DisableOsdUpdate();
	DisplayRemoteFwUpgradePageIcon(RemoteFwUpgradePageSelect);
	API_EnableOsdUpdate();

	if(RemoteFwUpgrade_State == FwUpgrade_State_Downloading || RemoteFwUpgrade_State == FwUpgrade_State_Intalling)
	{
		RemoteFwUpgradeTimeCnt = 30;
	}
}

void MENU_069_OnlineFwUpgrade_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_045_Upgrade);
	RemoteFwUpgradeTimeCnt = 0;
}

void MENU_069_OnlineFwUpgrade_Process(void* arg)
{
	
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int i, j, setValue;
	
	uint8 temp;
	int stringId;
	int x, y, len;
	char display[100];
	SYS_VER_INFO_T sysinfo;
	int error_code;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
							
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					API_RemoteUpgradeOperation(GetSearchOnlineIp(), UpgradeCMD_OP_CODE_CANCLE, &remoteUpgradeCmdData, 1);
					if(GetLastNMenu() == MENU_025_SEARCH_ONLINE)
					{
						popDisplayLastNMenu(2);
					}
					else
					{
						popDisplayLastMenu();
					}
					break;
					
					
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					RemoteFwUpgradeIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(RemoteFwUpgradePageSelect*RemoteFwUpgrade_ICON_MAX+RemoteFwUpgradeIconSelect >= RemoteFwUpgradeIconNum)
					{
						return;
					}
					
					switch(RemoteFwUpgradeIconTable[RemoteFwUpgradePageSelect*RemoteFwUpgrade_ICON_MAX+RemoteFwUpgradeIconSelect].iCon)
					{
						case ICON_FwUpgrade_Server:
							if(RemoteFwUpgrade_State == FwUpgrade_State_Uncheck ||RemoteFwUpgrade_State == FwUpgrade_State_CheckOk)
							{
								setValue = RemoteFwUpgrade_Ser_Select;
								for(i = 0; i < MAX_SET_NUM; i++)
								{
									for(publicSettingDisplay[i][0] = strlen(RemoteFwUpgrade_Ser_Disp[i]) * 2, j = 1; j < publicSettingDisplay[i][0]; j += 2)
									{
										publicSettingDisplay[i][j] = RemoteFwUpgrade_Ser_Disp[i][j/2];
									}
								}
								EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_FwUpgrage_Server, MAX_SET_NUM, setValue, RemoteUpgradeServerSet);
								StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							}
							break;

						case ICON_FwUpgrade_DLCode:
							if(RemoteFwUpgrade_State == FwUpgrade_State_Uncheck ||RemoteFwUpgrade_State == FwUpgrade_State_CheckOk)
							{
								EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_DLCode, RemoteFwUpgrade_Code, 6, COLOR_WHITE, RemoteFwUpgrade_Code, 1, RemoteUpgradeCodeSet);
							}
							break;
							
						case ICON_FwUpgrade_CodeInfo:
							
							break;
							
						case ICON_FwUpgrade_Notice:
							break;	
							
						case ICON_FwUpgrade_Operation:
							BusySpriteDisplay(1);
							API_RemoteUpgradeOperation(GetSearchOnlineIp(), UpgradeCMD_OP_CODE_OPERATION, &remoteUpgradeCmdData, 1);
							RemoteFwUpgradeTimeCnt = 30;
							break;
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_OtaOperationTimeCnt:
				AutoPowerOffReset();
				if(RemoteFwUpgradeRebootTime)
				{
					snprintf(display, 50, "Rebooting  %ds!", RemoteFwUpgradeRebootTime);
					if(RemoteFwUpgradeRebootTime <= 80 && (RemoteFwUpgradeRebootTime%3) == 1)
					{
						int ip, upTime;
						if(API_GetIpByMFG_SN(GetOnlineManageMFG_SN(), 1, &ip, &upTime) == 0)
						{
							RemoteFwUpgradeRebootTime = 0;
							snprintf(display, 50, "IP after restart:%03d.%03d.%03d.%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
							//API_OsdStringClearExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+1*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, 527, 40);
							//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+2*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);		
							snprintf(display, 50, "Reboot time %ds!", upTime+4);
						}
						else
						{
							if(RemoteFwUpgradeRebootTime == 1)
							{
								RemoteFwUpgradeRebootTime = 0;
								strcpy(display, "Reboot error!");
							}
						}
					}
					API_DisableOsdUpdate();
					//API_OsdStringClearExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+1*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, 527, 40);
					//API_OsdStringDisplayExt(DISPLAY_LIST_X+DISPLAY_DEVIATION_X, DISPLAY_LIST_Y+1*DISPLAY_LIST_SPACE+DISPLAY_DEVIATION_Y, DISPLAY_STATE_COLOR, display, strlen(display), 1, STR_UTF8, 0);		
					API_EnableOsdUpdate();
				}
				else
				{
					if(API_RemoteUpgradeGetState(GetSearchOnlineIp(), &remoteUpgradeCmdData, 1) == 0)
					{
						Menu069DecodeRemoteUpgradeData();
										
						API_DisableOsdUpdate();
						DisplayRemoteFwUpgradePageIcon(RemoteFwUpgradePageSelect);
						API_EnableOsdUpdate();
					}
				}
				break;
			case MSG_7_BRD_SUB_OtaReturn:
				popDisplayLastNMenu(2);
				break;
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//setInstallerPageSelect = 0;
		//DisplaySetInstallerPageIcon(setInstallerPageSelect);
	}
}

void OnlineFwUpgradeTimerProcess(void)
{
	if(RemoteFwUpgradeTimeCnt > 0 || RemoteFwUpgradeRebootTime > 0)
	{
		if(RemoteFwUpgrade_State != FwUpgrade_State_Downloading && RemoteFwUpgrade_State != FwUpgrade_State_Intalling)
		{
			if(RemoteFwUpgradeTimeCnt) RemoteFwUpgradeTimeCnt--;
		}
		
		if(RemoteFwUpgradeRebootTime) RemoteFwUpgradeRebootTime--;

		if(RemoteFwUpgradeTimeCnt > 0 || RemoteFwUpgradeRebootTime > 0)
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaOperationTimeCnt);
		}
	}
}


