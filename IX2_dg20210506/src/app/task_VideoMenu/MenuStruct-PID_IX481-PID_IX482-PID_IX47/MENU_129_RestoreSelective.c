#include "MENU_public.h"
#include "MENU_129_RestoreSelective.h"

#define	RESTORE_SELECTIVE_ICON_MAX		6

static int RestoreSelectiveConfirm;
static int RestoreSelectiveStart;
static int RestoreSelect[6];

const IconAndText_t RestoreSelectiveIconTableMode0[] = 
{
	{ICON_RestoreInstallerDefault,		MESG_TEXT_ICON_RestoreInstallerDefault},
	{ICON_RestoreUserDefault,			MESG_TEXT_ICON_RestoreUserDefault},
	{ICON_ClearCustomizer,				MESG_TEXT_ICON_ClearCustomizer},
	{ICON_ClearRes,						MESG_TEXT_ICON_ClearRes},
	{ICON_ClearUserData,				MESG_TEXT_ICON_ClearUserData},
	{ICON_ClearBackup,					MESG_TEXT_ICON_ClearBackup},
};

const int RestoreSelectiveIconNumMode0 = sizeof(RestoreSelectiveIconTableMode0)/sizeof(RestoreSelectiveIconTableMode0[0]);

const IconAndText_t RestoreSelectiveIconTableMode1[] = 
{
	{ICON_RestoreInstallerDefault,		MESG_TEXT_ICON_RestoreInstallerDefault},
	{ICON_RestoreUserDefault,			MESG_TEXT_ICON_RestoreUserDefault},
	//{ICON_ClearCustomizer,				MESG_TEXT_ICON_ClearCustomizer},
	{ICON_ClearRes,						MESG_TEXT_ICON_ClearRes},
	{ICON_ClearUserData,				MESG_TEXT_ICON_ClearUserData},
	{ICON_ClearBackup,					MESG_TEXT_ICON_ClearBackup},
};

const int RestoreSelectiveIconNumMode1 = sizeof(RestoreSelectiveIconTableMode1)/sizeof(RestoreSelectiveIconTableMode1[0]);

static IconAndText_t *RestoreSelectiveIconTable=NULL;
static int RestoreSelectiveIconNum=0;

static void MenuListGetRestoreSelectivePage(int currentPage, int onePageListMax, LIST_DISP_T* pDisp)
{	
	int i, index;
	int displayLen, unicode_len;
	char unicode_buf[200];
	char display[80] = {0};
	int stringId;
	
	pDisp->dispCnt = 0;

	for(i = 0; i < onePageListMax; i++)
	{
		index = currentPage*onePageListMax+i;
		if(index < RestoreSelectiveIconNum)
		{
			pDisp->disp[pDisp->dispCnt].iconType = ICON_PIC_NONE;
			pDisp->disp[pDisp->dispCnt].preSprType = RestoreSelect[index];
			API_GetOSD_StringWithID(RestoreSelectiveIconTable[index].iConText, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
			memcpy(pDisp->disp[pDisp->dispCnt].str, unicode_buf, unicode_len);
			pDisp->disp[pDisp->dispCnt].strLen = unicode_len;
			pDisp->dispCnt++;
		}
	}

}


void MENU_129_RestoreSelective_Init(int uMenuCnt)
{
	int unicode_len;
	int i;
	char unicode_buf[200];
	LIST_INIT_T listInit;
	char temp[5];
	API_Event_IoServer_InnerRead_All(RestoreSelMenuMode, (uint8*)temp);
	if(atoi(temp)==1)
	{
		RestoreSelectiveIconTable = RestoreSelectiveIconTableMode1;
		RestoreSelectiveIconNum = RestoreSelectiveIconNumMode1;
	}
	else
	{
		RestoreSelectiveIconTable = RestoreSelectiveIconTableMode0;
		RestoreSelectiveIconNum = RestoreSelectiveIconNumMode0;
	}
	RestoreSelectiveConfirm = 0;
	RestoreSelectiveStart = 0;
	
	for(i = 0; i < RestoreSelectiveIconNum; i++)
	{
		RestoreSelect[i] = 0;
	}
	
	API_GetOSD_StringWithID(MESG_TEXT_ICON_RestoreDefaultSelective, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
	
	listInit = ListPropertyDefault();
	
	listInit.selEn = 1;
	listInit.textOffset = 0;
	if( get_pane_type() == 1 || get_pane_type() == 5 )
		listInit.listType = TEXT_10X1;
	else	
		listInit.listType = TEXT_6X1;
	listInit.listCnt = RestoreSelectiveIconNum;
	listInit.fun = MenuListGetRestoreSelectivePage;
	listInit.titleStr = unicode_buf;
	listInit.titleStrLen = unicode_len;
	
	if(GetLastNMenu() != MENU_001_MAIN)
	{
		listInit.currentPage = 0;
	}
	
	InitMenuList(listInit);
}

void MENU_129_RestoreSelective_Exit(void)
{
	MenuListDisable(0);
}

void MENU_129_RestoreSelective_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	LIST_ICON listIcon;
	int unicode_len;
	char unicode_buf[200];
	int i, ret;
	POS pos;
	SIZE hv;

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					OSD_GetIconInfo(ICON_218_PublicSetTitle, &pos, &hv);
					if(RestoreSelectiveConfirm)
					{
						API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 1, 0);
						RestoreSelectiveConfirm = 0;
					}
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;
				
				case ICON_218_PublicSetTitle:
					if(RestoreSelectiveStart)
					{
						OSD_GetIconInfo(ICON_218_PublicSetTitle, &pos, &hv);
						
						if(RestoreSelectiveConfirm)
						{
							API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 0);
							RestoreSelectiveConfirm = 0;
						}
						else
						{
							RestoreSelectiveConfirm = 1;
							API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 1);
							break;
						}
				
						for(ret = 0, i = 0; i < RestoreSelectiveIconNum; i++)
						{
							if(RestoreSelect[i] != PRE_SPR_NONE)
							{
								switch(RestoreSelectiveIconTable[i].iCon)
								{
									case ICON_RestoreInstallerDefault:
										if(!API_Event_IoServer_RestoreDefaults(0))
										{										
											ret = 1;
										}
										break;
									case ICON_RestoreUserDefault:
										if(!API_Event_IoServer_RestoreDefaults(1))
										{										
											ret = 1;
										}
										break;
									case ICON_ClearCustomizer:
										DeleteFileProcess(CustomerFileDir, "*");
										ret = 1;
										break;
									case ICON_ClearRes:
										DeleteFileProcess(UserResFolder, "*");
										ret = 1;
										break;
									case ICON_ClearUserData:
										DeleteFileProcess(UserDataFolder, "*");
										ret = 1;
										break;
									case ICON_ClearBackup:
										DeleteFileProcess(INSTALLER_BAK_PATH, "*");
										ret = 1;
										break;
								}
							}
						}

						if(ret)
						{
							BEEP_CONFIRM();
							UpdateOkAndWaitingForReboot();
						}
					}
					break;

				case ICON_888_ListView:					
					OSD_GetIconInfo(ICON_218_PublicSetTitle, &pos, &hv);
					if(RestoreSelectiveConfirm)
					{
						API_OsdPicDisplay(OSD_LAYER_SPRITE, pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, SPRITE_IF_CONFIRM, 0, 0);
						RestoreSelectiveConfirm = 0;
					}
					
					listIcon = MenuListIconClick(pglobal_win_msg->wparam, pglobal_win_msg->lparam, ICON_888_ListView);
					if(listIcon.mainIcon > MENU_LIST_ICON_NONE)
					{
						if(RestoreSelect[listIcon.mainIcon])
						{
							RestoreSelect[listIcon.mainIcon] = PRE_SPR_NONE;
						}
						else
						{
							RestoreSelect[listIcon.mainIcon] = PRE_SPR_SELECT;
						}
						MenuListSetCurrentPage(MenuListGetCurrentPage());
						
						for(RestoreSelectiveStart = 0, i = 0; i < RestoreSelectiveIconNum; i++)
						{
							if(RestoreSelect[i] != PRE_SPR_NONE)
							{
								RestoreSelectiveStart = 1;
								break;
							}
						}

						if(RestoreSelectiveStart)
						{
							API_GetOSD_StringWithID(MESG_TEXT_ICON_Start, NULL, 0, NULL, 0, unicode_buf, &unicode_len); 
							API_OsdDisplayExtString(pos.x, pos.y, hv.h, 70, ALIGN_MIDDLE, unicode_buf, unicode_len, 1, DISPLAY_TITLE_COLOR);
							//API_OsdUnicodeStringDisplay(GetIconXY(ICON_218_PublicSetTitle).x+64, GetIconXY(ICON_218_PublicSetTitle).y+17, DISPLAY_TITLE_COLOR, MESG_TEXT_ICON_Start,1, 202);
						}
						else
						{
							//API_OsdStringClearExt(GetIconXY(ICON_218_PublicSetTitle).x+64, GetIconXY(ICON_218_PublicSetTitle).y+17, 202, 40);
							API_OsdStringClearExt(pos.x, pos.y, hv.h, 70);
						}
					}
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


