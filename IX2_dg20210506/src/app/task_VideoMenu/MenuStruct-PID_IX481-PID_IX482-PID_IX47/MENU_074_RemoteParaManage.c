#include "MENU_public.h"

//#define	RemoteParaManageIconMax	5
int RemoteParaManageIconMax;
char RemoteParaManage[10][5];
int RemoteParaManageNum;
char RemoteParaValues[500];
static int ParaGroupNumber;

extern ParaMenu_T OneRemotePararecord;
extern ParaProperty_t oneRemoteParaproperty;

int RemoteParaManageIconSelect;
int RemoteParaManagePageSelect;

void SetParaGroupNumber(int number)
{
	ParaGroupNumber = number;
}

int GetParaGroupNumber(void)
{
	return ParaGroupNumber;
}

static void DisplayRemoteParaManagePageIcon(int page)
{
	int i, j, index;
	uint16 x, y;
	int pageNum;
	char oneParaValue[500];
	int pos1, pos2;
	char *pIndex1, *pIndex2;
	ParaProperty_t property;
	int dataIndex, flag;
	POS pos;
	SIZE hv;

	sprintf(RemoteParaValues, "<Group=%d, Index=%d, Number=%d>", GetParaGroupNumber(), page*RemoteParaManageIconMax, RemoteParaManageIconMax);
	API_io_server_UDP_to_read_remote(GetSearchOnlineIp(), 0xFFFFFFFF, RemoteParaValues);
	
	//printf("RemoteParaValues =%s\n", RemoteParaValues);


	for(dataIndex = 0, i = 0; RemoteParaValues[dataIndex] != 0; dataIndex++)
	{
		if(RemoteParaValues[dataIndex] == '<')
		{
			flag = 1;
			pos1 = dataIndex+1;
		}
		else if(RemoteParaValues[dataIndex] == '>')
		{
			if(flag == 1)
			{
				pos2 = dataIndex;
				
				memcpy(oneParaValue, RemoteParaValues + pos1, pos2 - pos1);
				oneParaValue[pos2 - pos1] = 0;
				
				if((pIndex1 = strstr(oneParaValue, "maxNum=")) != NULL)
				{
					
					RemoteParaManageNum = atoi(pIndex1+strlen("maxNum="));
				}
				else if((pIndex1 = strstr(oneParaValue, "ID=")) != NULL)
				{
					pIndex1 += strlen("ID=");

					pIndex2 = strchr(pIndex1, ',');
					if(pIndex2 == NULL)
					{
						return;
					}

					if((int)(pIndex2-pIndex1) != 4)
					{
						return;
					}
					
					memcpy(RemoteParaManage[i], pIndex1, (int)(pIndex2-pIndex1));
					RemoteParaManage[i][(int)(pIndex2-pIndex1)] = 0;
					pIndex2++;
					
					if((pIndex1 = strstr(pIndex2, "Value=")) != NULL)
					{
						pIndex1 += strlen("Value=");

						strcpy(OneRemotePararecord.value, pIndex1);
					}


					char *disp;
					ComboBoxSelect_T tempParaComboBox;
					
					if(!GetParaMenuRecord(RemoteParaManage[i], &tempParaComboBox))
					{
					
						API_Event_IoServer_InnerRead_Property(RemoteParaManage[i], &property);
						
						if(property.editType)
						{
							for(j=0, tempParaComboBox.value = 0; j<tempParaComboBox.num; j++)
							{
								if(!strcmp(tempParaComboBox.data[j].value, OneRemotePararecord.value))
								{
									tempParaComboBox.value = j;
									break;
								}
							}
							disp = tempParaComboBox.data[tempParaComboBox.value].comboBox;
						}
						else
						{
							disp = OneRemotePararecord.value;
						}

						
						OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
						if(get_pane_type() == 5 )
						{
							x = pos.x+DISPLAY_DEVIATION_X;
							y = pos.y+5;
							API_OsdStringClearExt(x, y, bkgd_w-x, 80);
						}
						else
						{
							x = pos.x+DISPLAY_DEVIATION_X;
							y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
							API_OsdStringClearExt(x, y, bkgd_w-x, 40);
						}
						API_OsdStringDisplayExt(x, y, DISPLAY_LIST_COLOR, tempParaComboBox.name, tempParaComboBox.nameLen, 1, STR_UNICODE, hv.h-x);			
						if(get_pane_type() == 5 )
						{
							x += 100;
							y += 40;
						}
						else
						{
							x += (hv.h - pos.x)/2;
						}
						
						if( !strcmp(RemoteParaManage[i], VIDEO_PROXY_SET) || 
							!strcmp(RemoteParaManage[i], EXT_MODULE_CONFIG) || 
							!strcmp(RemoteParaManage[i], IXRLC_RelayOption) || 
							!strcmp(RemoteParaManage[i], DEVICE_FLOOR) || 
							!strcmp(RemoteParaManage[i], IXRLC_RelayMap))
						{
							;
						}
						else
						{
							API_OsdStringDisplayExt(x, y, DISPLAY_STATE_COLOR, disp, strlen(disp), 1, STR_UTF8, hv.h - x);
						}
					}
					i++;
				}
				
				flag = 0;
			}
					
		}

	}

	for(; i<RemoteParaManageIconMax; i++)
	{
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		//x = pos.x+DISPLAY_DEVIATION_X;
		//y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
		//API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
	}

	pageNum = RemoteParaManageNum/RemoteParaManageIconMax + (RemoteParaManageNum%RemoteParaManageIconMax ? 1 : 0);

	DisplaySchedule2(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
}
 


void MENU_074_RemoteParaManage_Init(int uMenuCnt)
{
	if(GetLastNMenu() == MENU_022_ONLINE_MANAGE)
	{
		RemoteParaManagePageSelect = 0;
	}
	RemoteParaManageIconMax = GetListIconNum();
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	DisplayRemoteParaManagePageIcon(RemoteParaManagePageSelect);
	
}

void MENU_074_RemoteParaManage_Exit(void)
{
	API_MenuIconDisplaySelectOff(ICON_049_Parameter);
}

void MENU_074_RemoteParaManage_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&RemoteParaManagePageSelect, RemoteParaManageIconMax, RemoteParaManageNum, (DispListPage)DisplayRemoteParaManagePageIcon);
					break;			
				case ICON_202_PageUp:
					PublicPageUpProcess(&RemoteParaManagePageSelect, RemoteParaManageIconMax, RemoteParaManageNum, (DispListPage)DisplayRemoteParaManagePageIcon);
					break;			
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
				case ICON_012_PublicList6:
				case ICON_013_PublicList7:
				case ICON_014_PublicList8:
				case ICON_015_PublicList9:
				case ICON_016_PublicList10:
					if(GetParaGroupNumber()==1 || GetParaGroupNumber()==2)
						return;
					RemoteParaManageIconSelect = GetCurIcon() - ICON_007_PublicList1;
					
					if(RemoteParaManagePageSelect  * RemoteParaManageIconMax + RemoteParaManageIconSelect < RemoteParaManageNum)
					//if(RemoteParaManageIconSelect < RemoteParaManageIconMax)
					{
#if 1
							int ComboxSetRemoteParaValue(int value);
							extern ComboBoxSelect_T OneParaComboBox;

							GetParaMenuRecord(RemoteParaManage[RemoteParaManageIconSelect], &OneParaComboBox);
							strcpy(OneRemotePararecord.paraId, RemoteParaManage[RemoteParaManageIconSelect]);
							API_io_server_UDP_to_read_one_remote(GetSearchOnlineIp(), RemoteParaManage[RemoteParaManageIconSelect], OneRemotePararecord.value);
						
							if(!strcmp(RemoteParaManage[RemoteParaManageIconSelect], VIDEO_PROXY_SET))
							{
								GetVideoProxySettingValue();
								StartInitOneMenu(MENU_111_VIDEO_PROXY_SET,0,1);
								return;
							}
							else if(!strcmp(RemoteParaManage[RemoteParaManageIconSelect], EXT_MODULE_CONFIG))
							{
								GetExtModuleConfigValue();
								StartInitOneMenu(MENU_138_ModuleSelect,0,1);
								return;
							}
							else if(!strcmp(RemoteParaManage[RemoteParaManageIconSelect], IXRLC_RelayMap))
							{
								GetRlcRelayMapConfigValue();
								StartInitOneMenu(MENU_133_RlcRelayMapSetting,0,1);
								return;
							}
							else if(!strcmp(RemoteParaManage[RemoteParaManageIconSelect], IXRLC_RelayOption))
							{
								GetRlcRelayOptionConfigValue();
								StartInitOneMenu(MENU_134_RlcRelayOptionSetting,0,1);
								return;
							}
							else if(!strcmp(RemoteParaManage[RemoteParaManageIconSelect], DEVICE_FLOOR))
							{
								InitDeviceFloorSettingData(1, OneRemotePararecord.value);
								StartInitOneMenu(MENU_135_DeviceFloorSetting,0,1);
								return;
							}
							
							API_Event_IoServer_InnerRead_Property(RemoteParaManage[RemoteParaManageIconSelect], &oneRemoteParaproperty);
						
							if(oneRemoteParaproperty.readWrite & 0x02)
							{
								if(!oneRemoteParaproperty.editType)
								{
									int SetRemoteParaValue(const char* pData);
									
									EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_ParaValue, OneRemotePararecord.value, 30, COLOR_WHITE, OneRemotePararecord.value, 1, SetRemoteParaValue);
								}
								else
								{
									int i;
							
									OneParaComboBox.editType = oneRemoteParaproperty.editType;
									
									for(i=0, OneParaComboBox.value = 0; i<OneParaComboBox.num; i++)
									{
										if(!strcmp(OneParaComboBox.data[i].value, OneRemotePararecord.value))
										{
											OneParaComboBox.value = i;
											break;
										}
									}
						
									EnterParaPublicMenu(0, OneParaComboBox.value, ComboxSetRemoteParaValue);
								}
							}
							else
							{
								BEEP_ERROR();
							}
#else
						extern ParaMenu_T OneRemotePararecord;
						GetParaMenuRecord(RemoteParaManage[RemoteParaManageIconSelect], &OneRemotePararecord);
						StartInitOneMenu(MENU_075_OneRemotePara,0,1);
#endif
					}
					break;
					
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		//EnterSettingMenu(MENU_008_SET_INSTALLER, 0);
	}
}

