
#ifndef _MENU_136_H
#define _MENU_136_H

#include "MENU_public.h"
#include "MENU_Keyboard.h"

void EnterMulLangKeypadMenu(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitInputString, unsigned char highLight, InputOK callback);
void EnterMulLangNoStack(unsigned char type, int title, char* pInput, uint8 inputMaxLen, int Color, char* pInitInputString, unsigned char highLight, InputOK callback);


#endif


