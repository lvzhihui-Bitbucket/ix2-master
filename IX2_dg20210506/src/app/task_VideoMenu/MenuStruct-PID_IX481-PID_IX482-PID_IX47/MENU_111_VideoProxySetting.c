#include "MENU_public.h"

#define IPC_LIST_MAX_CNT					100

#define	VideoProxy_ICON_MAX		5
int videoProxyIconSelect;
int videoProxyPageSelect;

extern ParaMenu_T OneRemotePararecord;
static VIDEO_PROXY_JSON videoProxySettingObj;

const char *videoProxyType[2] = {"IPC", "IX"};
static char* ipcListName[IPC_LIST_MAX_CNT]={NULL};
char ipcName[IPC_LIST_MAX_CNT][41];
static int ipcSelect;

const IconAndText_t videoProxyIconTable[] = 
{
	{ICON_CallVideoSelect,				MESG_TEXT_ICON_CallVideoSelect},    
	{ICON_IPC_Agent,					MESG_TEXT_ICON_IPC_Agent},
	{ICON_IX_Agent,						MESG_TEXT_ICON_IX_Agent},
};

const int videoProxySettingIconNum = sizeof(videoProxyIconTable)/sizeof(videoProxyIconTable[0]);

char** GetIPCListNameBuffer(void)
{
	int i;
	//IPC_RECORD ipcRecord;
	IPC_ONE_DEVICE ipcRecord;
	
	ipcSelect = GetIpcNum()+1;

	for(i = 0; i < GetIpcNum(); i++)
	{
		GetIpcCacheRecord(i, &ipcRecord);
		strncpy(ipcName[i], ipcRecord.NAME, 41);
		ipcListName[i] = ipcName[i];
		if(!strcmp(videoProxySettingObj.ipcDevice.NAME, ipcRecord.NAME))
		{
			ipcSelect = i; 
		}
	}
	//strcpy(ipcName[i], "Cancel");
	//ipcListName[i] = ipcName[i];
	
	return ipcListName;
}


static void DisplayVideoProxySettingPageIcon(uint8 page)
{
	uint8 i, j;
	uint16 x, y;
	int pageNum;
	char display[11];
	POS pos;
	SIZE hv;
	
	//API_DisableOsdUpdate();
	
	for(i = 0; i < VideoProxy_ICON_MAX; i++)
	{
		//x = GetIconXY(ICON_007_PublicList1+i).x+DISPLAY_DEVIATION_X;
		//y = GetIconXY(ICON_007_PublicList1+i).y+DISPLAY_DEVIATION_Y;
		OSD_GetIconInfo(ICON_007_PublicList1+i, &pos, &hv);
		if(get_pane_type() == 5 )
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+5;
			API_OsdStringClearExt(x, y, bkgd_w-x, 80);
		}
		else
		{
			x = pos.x+DISPLAY_DEVIATION_X;
			y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
			API_OsdStringClearExt(x, y, bkgd_w-x, 40);
		}
		if(page*VideoProxy_ICON_MAX+i < videoProxySettingIconNum)
		{
			API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, videoProxyIconTable[i+page*VideoProxy_ICON_MAX].iConText, 1, hv.h - x);
			if(get_pane_type() == 5 )
			{
				x += 120;
				y += 40;
			}
			else
			{
				x += (hv.h - pos.x)/2;
			}
			switch(videoProxyIconTable[i+page*VideoProxy_ICON_MAX].iCon)
			{
				case ICON_CallVideoSelect:
					//API_OsdStringDisplayExt(x, y, COLOR_RED, videoProxySettingObj.type, strlen(videoProxySettingObj.type),1,STR_UTF8, hv.h - x);
					if(strcmp(videoProxySettingObj.type, "IPC")==0)
					{
						API_OsdUnicodeStringDisplay(x, y, COLOR_RED, MESG_TEXT_VideoProxySourceIpc, 1, hv.h-x);
					}
					else
					{
						API_OsdUnicodeStringDisplay(x, y, COLOR_RED, MESG_TEXT_VideoProxySourceIx, 1, hv.h-x);
					}
					break;
					
				case ICON_IPC_Agent:
					API_OsdStringDisplayExt(x, y, COLOR_RED, videoProxySettingObj.ipcDevice.NAME, strlen(videoProxySettingObj.ipcDevice.NAME),1,STR_UTF8, hv.h - x);
					break;
					
				case ICON_IX_Agent:
					for(j = 0; j < 10 && videoProxySettingObj.ixDevice.addr[j]; j++)
					{
						display[j] = toupper(videoProxySettingObj.ixDevice.addr[j]);
					}
					display[j] = 0;

					if(display[0] == 0 || !strcmp(display, "MYSELF"))
					{
						//API_OsdStringDisplayExt(x, y, COLOR_RED, "Myself", strlen("Myself"),1,STR_UTF8, hv.h - x);
						API_OsdUnicodeStringDisplay(x, y, COLOR_RED, MESG_TEXT_VideoProxySourceMyself, 1, hv.h-x);
					}
					else if(!strcmp(display, "NONE"))
					{
						API_OsdStringDisplayExt(x, y, COLOR_RED, "None", strlen("None"),1,STR_UTF8, hv.h - x);
					}
					else
					{
						API_OsdStringDisplayExt(x, y, COLOR_RED, videoProxySettingObj.ixDevice.addr, strlen(videoProxySettingObj.ixDevice.addr),1,STR_UTF8, hv.h - x);
					}
					break;
					
				default:
					break;
			}
		}
	}
	pageNum = videoProxySettingIconNum/VideoProxy_ICON_MAX + (videoProxySettingIconNum%VideoProxy_ICON_MAX ? 1 : 0);

	DisplaySchedule(MENU_SCHEDULE_POS_X, MENU_SCHEDULE_POS_Y, page, pageNum);
	
	//API_EnableOsdUpdate();
}

//cao_20181020_s


void GetVideoProxySettingValue(void)
{
	ParseVideoProxyObject(OneRemotePararecord.value, &videoProxySettingObj);
}


void MENU_111_VideoProxySetting_Init(int uMenuCnt)
{
	POS pos;
	SIZE hv;
	videoProxyPageSelect = 0;
	videoProxyIconSelect = 0;

	OSD_GetIconInfo(ICON_175_KeyTitle, &pos, &hv);
	API_OsdStringDisplayExt(pos.x, hv.v/2, DISPLAY_LIST_COLOR, OneRemotePararecord.name, OneRemotePararecord.nameLen, 1, STR_UNICODE, 0);			
	
	API_MenuIconDisplaySelectOn(ICON_049_Parameter);
	DisplayVideoProxySettingPageIcon(videoProxyPageSelect);


	
}

void MENU_111_VideoProxySetting_Exit(void)
{
	
}

void SetVideoProxyType(int select)
{
	char *jsonString = NULL;
	
	if(select < 2)
	{
		strcpy(videoProxySettingObj.type, videoProxyType[select]);
		jsonString = CreateVideoProxyObject(videoProxySettingObj);
		SetRemoteParaValue(jsonString);
		if(jsonString != NULL)
		{
			free(jsonString);
		}
	}
}

void SetVideoProxyIPC(int select)
{
	char *jsonString = NULL;
	//IPC_RECORD ipcRecord;
	IPC_ONE_DEVICE ipcRecord;

	if(select < GetIpcNum())
	{
		ipcSelect = select; 
		
		GetIpcCacheRecord(select, &ipcRecord);
		strcpy(videoProxySettingObj.ipcDevice.ID, ipcRecord.ID);
		strcpy(videoProxySettingObj.ipcDevice.NAME, ipcRecord.NAME);
		strcpy(videoProxySettingObj.ipcDevice.USER, ipcRecord.USER);
		strcpy(videoProxySettingObj.ipcDevice.PWD, ipcRecord.PWD);
		strcpy(videoProxySettingObj.ipcDevice.IP, ipcRecord.IP);
		videoProxySettingObj.ipcDevice.CH = ipcRecord.CH_ALL;
		strcpy(videoProxySettingObj.ipcInfo.rtsp_url, ipcRecord.CH_DAT[1].rtsp_url);
		videoProxySettingObj.ipcInfo.width = ipcRecord.CH_DAT[1].width;
		videoProxySettingObj.ipcInfo.height = ipcRecord.CH_DAT[1].height;
		videoProxySettingObj.ipcInfo.vd_type = ipcRecord.CH_DAT[1].vd_type;

		jsonString = CreateVideoProxyObject(videoProxySettingObj);
		SetRemoteParaValue(jsonString);
		if(jsonString != NULL)
		{
			free(jsonString);
		}
	}
	else
	{
		ipcSelect = select; 
		strcpy(videoProxySettingObj.type, "IX");
		memset(videoProxySettingObj.ipcDevice.NAME, 0, 40);
		memset(videoProxySettingObj.ipcInfo.rtsp_url, 0, 200);

		jsonString = CreateVideoProxyObject(videoProxySettingObj);
		SetRemoteParaValue(jsonString);
		if(jsonString != NULL)
		{
			free(jsonString);
		}		
	}
}

int SetVideoProxyIX(const char* data)
{
	char *jsonString = NULL;

	strcpy(videoProxySettingObj.ixDevice.addr, data);
	
	jsonString = CreateVideoProxyObject(videoProxySettingObj);
	SetRemoteParaValue(jsonString);
	if(jsonString != NULL)
	{
		free(jsonString);
	}
	
	return 1;
}

void MENU_111_VideoProxySetting_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	int len;
	int i;
	char temp[20];

	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
					
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				case ICON_200_Return:
					popDisplayLastMenu();
					break;
				
				case ICON_047_Home:
					GoHomeMenu();
					break;

				case ICON_201_PageDown:
					PublicPageDownProcess(&videoProxyPageSelect, VideoProxy_ICON_MAX, videoProxySettingIconNum, (DispListPage)DisplayVideoProxySettingPageIcon);
					break;
				case ICON_202_PageUp:
					PublicPageUpProcess(&videoProxyPageSelect, VideoProxy_ICON_MAX, videoProxySettingIconNum, (DispListPage)DisplayVideoProxySettingPageIcon);
					break;
				case ICON_007_PublicList1:
				case ICON_008_PublicList2:
				case ICON_009_PublicList3:
				case ICON_010_PublicList4:
				case ICON_011_PublicList5:
					videoProxyIconSelect = GetCurIcon() - ICON_007_PublicList1;
					if(videoProxyPageSelect*VideoProxy_ICON_MAX+videoProxyIconSelect >= videoProxySettingIconNum)
					{
						return;
					}
					
					switch(videoProxyIconTable[videoProxyPageSelect*VideoProxy_ICON_MAX+videoProxyIconSelect].iCon)
					{
						case ICON_CallVideoSelect:
							API_GetOSD_StringWithID(MESG_TEXT_VideoProxySourceIpc,NULL,0, NULL,0,&publicSettingDisplay[0][1],&len);
							publicSettingDisplay[0][0]=len;
							API_GetOSD_StringWithID(MESG_TEXT_VideoProxySourceIx,NULL,0, NULL,0,&publicSettingDisplay[1][1],&len);
							publicSettingDisplay[1][0]=len;
							//InitPublicSettingMenuDisplay(2,videoProxyType);
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_CallVideoSelect, 2, strcmp(videoProxySettingObj.type, "IPC") ? 1 : 0, SetVideoProxyType);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
							
						case ICON_IPC_Agent:
							InitPublicSettingMenuDisplay(GetIpcNum(),GetIPCListNameBuffer());
							API_GetOSD_StringWithID(MESG_TEXT_ProxyIpcCancel, NULL, 0, NULL, 0,&publicSettingDisplay[GetIpcNum()][1], &len);		//FOR_INDEXA
							publicSettingDisplay[GetIpcNum()][0] = len;
							EnterPublicSettingMenu(MESG_TEXT_ICON_026_InstallerSetup, MESG_TEXT_ICON_IPC_Agent, GetIpcNum()+1, ipcSelect, SetVideoProxyIPC);
							StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
							break;
							
						case ICON_IX_Agent:
							EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_IX_Agent, videoProxySettingObj.ixDevice.addr, 10, COLOR_WHITE, videoProxySettingObj.ixDevice.addr, 1, SetVideoProxyIX);
							break;
					}
					break;			
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


