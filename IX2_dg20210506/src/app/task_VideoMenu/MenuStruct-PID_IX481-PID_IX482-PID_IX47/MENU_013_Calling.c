#include "MENU_public.h"
#include "../../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"
//#include "../../task_Caller/task_Caller.h"
//#include "../../task_BeCalled/task_BeCalled.h"

// lzh_20191009_s
//#include "../../audio_service/obj_plugswitch.h"
// lzh_20191009_e

void MENU_013_Calling_Init(int uMenuCnt)
{
	if(CallServer_Run.state == CallServer_Ring && api_get_plugswitch_status() )
	{
		API_CallServer_LocalAck(CallServer_Run.call_type);
	}	

}

void MENU_013_Calling_Exit(void)
{
	if(CallServer_Run.state != CallServer_Wait && CallServer_Run.call_type != IxCallScene1_Passive)			//czn_20190117
	{
		API_CallServer_LocalBye(CallServer_Run.call_type);
	}


	API_TimeLapseHide();
	API_TimeLapseStop();
}

void MENU_013_Calling_Process(void* arg)
{
	SYS_WIN_MSG *pglobal_win_msg = (SYS_WIN_MSG*)arg;
	char *dispString;
	int len;
	CALL_ERR_CODE *pErrorCode;
	
	// ���ⰴ������
	if( pglobal_win_msg->type == MSG_3_VKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch( pglobal_win_msg->wparam )
			{
				case KEY_MENU:
					
				case KEY_UP:
					
					break;
				case KEY_DOWN:
					
					break;
				case KEY_TALK:
					if(CallServer_Run.call_type == IxCallScene2_Active|| 
						CallServer_Run.call_type == IxCallScene3_Active)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
					else
					{
						if(CallServer_Run.state == CallServer_Ring)
						{
							API_CallServer_LocalAck(CallServer_Run.call_type);
						}
						else if(CallServer_Run.state == CallServer_Ack)
						{
							API_CallServer_LocalBye(CallServer_Run.call_type);
						}
					}
					break;

				case KEY_BACK:
					API_CallServer_LocalBye(CallServer_Run.call_type);
					break;
					
				case KEY_UNLOCK:
					//API_CallServer_LocalUnlock1(CallServer_Run.call_type);
					break;	
				default:		//czn_20170120
					DefaultPublicVkeyProcessing(pglobal_win_msg->wparam);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_2_TKEY )
	{
		if( pglobal_win_msg->status == TOUCHCLICK )
		{
			switch(GetCurIcon())
			{
				// lzh_20181108_s
				if( check_key_id_contineous( GetCurIcon(), pglobal_win_msg->happen_time, ICON_241_MonitorFishEye4 ) == 1 )
				{
					API_menu_create_bitmap_file(-1,-1,-1);				
				}
				// lzh_20181108_e
				case ICON_258_IntercomTalk:
					if(CallServer_Run.call_type == IxCallScene2_Active|| 
						CallServer_Run.call_type == IxCallScene3_Active)
					{
						API_CallServer_LocalBye(CallServer_Run.call_type);
					}
					else
					{
						if(CallServer_Run.state == CallServer_Ring)
						{
							API_CallServer_LocalAck(CallServer_Run.call_type);
						}
						else if(CallServer_Run.state == CallServer_Ack)
						{
							API_CallServer_LocalBye(CallServer_Run.call_type);
						}
					}
					break;
				
				case ICON_257_IntercomClose:
					API_CallServer_LocalBye(CallServer_Run.call_type);
					break;
			}
		}
	}
	else if( pglobal_win_msg->type == MSG_7_BRD)
	{
		switch( pglobal_win_msg->status )
		{
			case MSG_7_BRD_SUB_CallError:

				pErrorCode = (CALL_ERR_CODE*)(arg+sizeof(SYS_WIN_MSG));
				switch(*pErrorCode)
				{
					case CALL_LINK_BUSY:
						CloseOneMenu();
						break;

					case CALL_LINK_ERROR1:
						API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError1);
						usleep(1500000);
						popDisplayLastMenu();
						break;	
						
					case CALL_LINK_ERROR2:
						API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError2);
						usleep(1500000);
						popDisplayLastMenu();
						break;	
						
					case CALL_LINK_ERROR3:
						API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_LinkError3);
						usleep(1500000);
						popDisplayLastMenu();
						break;
				}

				break;
				
			case MSG_7_BRD_SUB_IntercomCallStartting:
			case MSG_7_BRD_SUB_InnerCallStartting:
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;
				
			case MSG_7_BRD_SUB_IntercomCallOn:
			case MSG_7_BRD_SUB_InnerCallOn:
				API_SpriteClose(CALLER_SYSTEM_SPRITE_INFORM_X(bkgd_w), CALLER_SYSTEM_SPRITE_INFORM_Y(bkgd_h),SPRITE_SystemBusy);
				break;

			case MSG_7_BRD_SUB_IntercomCallTalkOn:
			case MSG_7_BRD_SUB_InnerCallTalkOn:
			case MSG_7_BRD_SUB_GLCallTalkOn:	
				API_TimeLapseStart(COUNT_RUN_UP,0);
				API_SpriteDisplay_XY(CallMenuDisp_Talking_x,CallMenuDisp_Talking_y,SPRITE_TALKING);
				break;
				
			case MSG_7_BRD_SUB_IntercomCallName:
			case MSG_7_BRD_SUB_GLCallName:		
			//case MSG_7_BRD_SUB_InnerCallName:
				dispString = (char*)(arg+sizeof(SYS_WIN_MSG));
				len = *dispString;
				
				API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, ++dispString, len,CallMenuDisp_Name_Font, STR_UTF8, 0);
				break;
				
			case MSG_7_BRD_SUB_InnerCallName:
				API_OsdUnicodeStringDisplay(CallMenuDisp_Name_x, CallMenuDisp_Name_y,CallMenuDisp_Name_Color,MESG_TEXT_CusInnerCall,CallMenuDisp_Name_Font, 0);
				break;
				
			case MSG_7_BRD_SUB_IntercomCallOff:
			case MSG_7_BRD_SUB_InnerCallOff:
				popDisplayLastMenu();
				break;

			case MSG_7_BRD_SUB_IntercomBeCallOff:
			case MSG_7_BRD_SUB_InnerBeCallOff:
				CloseOneMenu();
				break;
				
			default:
				DefaultPublicInformProcessing(pglobal_win_msg->status, arg);
				break;
		}
	}
	else if(pglobal_win_msg->type == MSG_8_DIP)
	{
		EnterSettingMenu(MENU_008_SET_INSTALLER, 1);
	}
}


