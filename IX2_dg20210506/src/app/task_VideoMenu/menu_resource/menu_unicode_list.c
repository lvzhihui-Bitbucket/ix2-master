
#include "menu_unicode_list.h"

const char* UNICODE_HEAD_TYPE_TAB[] = 
{
	"ICON_TEXT_",
	"LABE_TEXT_",
	"MESG_TEXT_",
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// icon unicode string list service
///////////////////////////////////////////////////////////////////////////////////////////////////////
struct list_head unicode_str_list,*plist_unicode_str_list,*plist_temp;

//初始化链表头
void init_unicode_str_list( void )
{
	static int gui_list_initial_ok = 0;
	if( !gui_list_initial_ok )
	{
		INIT_LIST_HEAD(&unicode_str_list); 
		gui_list_initial_ok = 1;
	}
}

//添加节点
void add_unicode_str_node( one_unicode_str_node* pnew_node )
{
	list_add_tail(&pnew_node->list,&unicode_str_list);     
}

//删除节点
void del_unicode_str_node( one_unicode_str_node* pnew_node )
{
	list_del_init(&pnew_node->list);            
}

//判断链表是否为空
int unicode_str_list_is_empty( void )
{
	return list_empty(&unicode_str_list);
}

//遍历链表，打印结果
void printf_each_unicode_str_node(void)
{
	list_for_each(plist_unicode_str_list,&unicode_str_list)
	{
		one_unicode_str_node* pnode = list_entry(plist_unicode_str_list,one_unicode_str_node,list);
		printf("type=%d,id=%d,org_ptr=%s,tra_ptr=%s\n",pnode->data.type,pnode->data.id,pnode->data.org_ptr,pnode->data.tra_ptr);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
// API interface
///////////////////////////////////////////////////////////////////////////////////////////////////////
void api_init_unicode_str_list( void )
{
	init_unicode_str_list();
}

void api_del_all_unicode_str_node( void )
{
	list_for_each_safe(plist_unicode_str_list,plist_temp,&unicode_str_list)
	{
		one_unicode_str_node* pnode = list_entry(plist_unicode_str_list,one_unicode_str_node,list);
		if( pnode == NULL ) continue;
		// free node data
		if( pnode->data.org_ptr != NULL ) free( pnode->data.org_ptr);
		if( pnode->data.tra_ptr != NULL ) free( pnode->data.tra_ptr);
		pnode->data.org_ptr = NULL;
		pnode->data.tra_ptr = NULL;
		del_unicode_str_node( pnode );
		free( pnode );
	}	
}

int api_add_one_unicode_str_node( int type, int id, char* org_ptr, int org_len, char* tra_ptr, int tra_len )
{
	// create node data
	one_unicode_str_node* pnew_node = malloc(sizeof(one_unicode_str_node));
	pnew_node->data.type 	= type;
	pnew_node->data.id 		= id;
	pnew_node->data.org_len	= org_len;
	pnew_node->data.tra_len = tra_len;
	pnew_node->data.org_ptr	= NULL;
	pnew_node->data.tra_ptr = NULL;
	if( org_ptr != NULL ) 
	{
		pnew_node->data.org_ptr = malloc(org_len);
		memcpy( pnew_node->data.org_ptr, org_ptr, org_len );
	}
	if( tra_ptr != NULL ) 
	{
		pnew_node->data.tra_ptr = malloc(tra_len);
		memcpy( pnew_node->data.tra_ptr, tra_ptr, tra_len );
	}
	add_unicode_str_node(pnew_node);
	
	return 0;
}

int api_del_one_iunicode_str_node( int type, int id )
{
	list_for_each(plist_unicode_str_list,&unicode_str_list)
	{
		one_unicode_str_node* pnode = list_entry(plist_unicode_str_list,one_unicode_str_node,list);
		// free node data
		if( pnode->data.type == type && pnode->data.id == id  )
		{
			if( pnode->data.org_ptr != NULL ) free( pnode->data.org_ptr);
			if( pnode->data.tra_ptr != NULL ) free( pnode->data.tra_ptr);
			pnode->data.org_ptr = NULL;
			pnode->data.tra_ptr = NULL;
			del_unicode_str_node( pnode );
			free( pnode );
			return 1;
		}
	}
	return 0;
}

int api_get_one_unicode_both_str( int type, int id, char* porg_ptr, int* porg_len, char* ptra_ptr, int* ptra_len  )
{
	list_for_each(plist_unicode_str_list,&unicode_str_list)
	{
		one_unicode_str_node* pnode = list_entry(plist_unicode_str_list,one_unicode_str_node,list);
		// compare
		if( pnode->data.type == type && pnode->data.id == id  )
		{
			// get org string
			if( porg_ptr != NULL && porg_len != NULL )
			{
				memcpy( porg_ptr, pnode->data.org_ptr, pnode->data.org_len );
				*porg_len = pnode->data.org_len;				
			}
			// get unicode string
			if( ptra_ptr != NULL && ptra_len != NULL )
			{
				memcpy( ptra_ptr, pnode->data.tra_ptr, pnode->data.tra_len );
				*ptra_len = pnode->data.tra_len;				
			}			
			return 1;
		}
	}
	return 0;
}

int api_get_one_unicode_str( char* porg_ptr, int org_len, char* ptra_ptr, int* ptra_len  )
{
	list_for_each(plist_unicode_str_list,&unicode_str_list)
	{
		one_unicode_str_node* pnode = list_entry(plist_unicode_str_list,one_unicode_str_node,list);
		// compare
		if( (org_len == pnode->data.org_len) && ( memcmp( pnode->data.org_ptr, porg_ptr, org_len) == 0) )
		{
			// get unicode string
			if( ptra_ptr != NULL && ptra_len != NULL )
			{
				memcpy( ptra_ptr, pnode->data.tra_ptr, pnode->data.tra_len );
				*ptra_len = pnode->data.tra_len;				
			}			
			return 1;
		}
	}
	return 0;
}

/***************************************************************************************************************************************

***************************************************************************************************************************************/
char* find_one_unicode_str( char* praw_buffer, char* praw_buffer_end, char* div1, char* div2, char** ppstr_ptr, int* pstr_len )
{
	int i,str_start_pos,str_over_pos;
	
	// 先找到第一个分割符
	for( i = 0; praw_buffer[i] != div1[0] || praw_buffer[i+1] != div1[1]; i+=2 )
	{
		if( (praw_buffer+i) > praw_buffer_end ) return NULL;
	}

	i += 2; // 跳开 ‘,’字符 (2个字节)

	// 去掉起始符后面的空格符
	//for( ; praw_buffer[i] == ' ' && praw_buffer[i+1] == 0; i+=2 )
	//{
	//	if( (praw_buffer+i) > praw_buffer_end ) return NULL;
	//}
		
	// 得到有效字符串的起始位置
	str_start_pos = i;

	// 再找到第二个分割符
	for( ; praw_buffer[i] != div2[0] || praw_buffer[i+1] != div2[1]; i+=2 )
	{
		if( (praw_buffer+i) > praw_buffer_end ) return NULL;
	}
		
	// 去掉结束符前面的空格符
	for( ; praw_buffer[i-2] == ' ' && praw_buffer[i-1] == 0; i-=2 );

	// 得到有效字符串的结束位置
	str_over_pos = i;

	// 得到有效数据长度
	if( pstr_len != NULL ) 
	{
		*pstr_len = str_over_pos-str_start_pos;
	}
	// icon str data
	if( ppstr_ptr != NULL ) 
	{
		*ppstr_ptr = (praw_buffer+str_start_pos);
	}
	
	return (praw_buffer+i);
}

// 从一个字符串中得到icon的索引、数据指针、数据大小 
// praw_buffer 数据包统一为unicode little endian 格式
int api_get_node_data_from_raw_buffer( char* praw_buffer, int raw_buf_len, int* ptype, int * pid, char** pporg_ptr, int* porg_len, char** pptra_ptr, int* ptra_len )
{	
	int 	i,j;
	char 	res_id[4];
	int		unicode_head_max;
	int 	unicode_head_len;
	char* 	unicode_head_ptr;
	char* 	unicode_search_ptr;

	int 	unicode_type = -1;
		
	// --------------------------------------------search for unicode type
	unicode_head_max = sizeof(UNICODE_HEAD_TYPE_TAB)/sizeof(char*);
	for( i = 0; i < unicode_head_max; i++ )
	{
		unicode_head_len = strlen(UNICODE_HEAD_TYPE_TAB[i]);
		unicode_head_ptr = UNICODE_HEAD_TYPE_TAB[i];
		
		for( j = 0; j < unicode_head_len; j++ )
		{
			if( praw_buffer[j*2] != unicode_head_ptr[j] )
				break;
		}
		if( j == unicode_head_len )
		{
			unicode_type = i;
			break;
		}
	}
	if( unicode_type == -1 )
		return -1;

	if( raw_buf_len < (unicode_head_len+4)*2 )
		return -1;

	*ptype = unicode_type;

	// unicode长度需要乘以2
	unicode_head_len <<= 1;

	if( unicode_type == UNICODE_STR_TYPE_ICON )
	{
		// --------------------------------------------search for res_id
		res_id[0] = praw_buffer[unicode_head_len+0]; // digit 1
		res_id[1] = praw_buffer[unicode_head_len+2]; // digit 2
		res_id[2] = praw_buffer[unicode_head_len+4]; // digit 3
		res_id[3] = 0;
		*pid = atoi(res_id);
		
		// --------------------------------------------search for orginal string	
		
		unicode_search_ptr = praw_buffer+unicode_head_len;
		
		unicode_search_ptr = find_one_unicode_str( unicode_search_ptr, praw_buffer+raw_buf_len, ",", ",", pporg_ptr, porg_len );
		if( unicode_search_ptr == NULL )
			return -1;	
		
	}
	else if( unicode_type == UNICODE_STR_TYPE_MESG )
	{
		// --------------------------------------------search for orginal string	

		unicode_search_ptr = praw_buffer+unicode_head_len;
		// mesg类型查找'_'和','之间的字符串
		unicode_search_ptr = find_one_unicode_str( unicode_search_ptr-2, praw_buffer+raw_buf_len, "_", ",", pporg_ptr, porg_len );
		if( unicode_search_ptr == NULL )
			return -1;	

		// --------------------------------------------search for res_id
		// mesg类型无资源id数据
		*pid = 0;
	}
	
	// --------------------------------------------search for translation string	
	
	//printf("tra_search_ptr=0x%08x->",unicode_search_ptr);

	unicode_search_ptr = find_one_unicode_str( unicode_search_ptr, praw_buffer+raw_buf_len, ",", "\r", pptra_ptr, ptra_len );
	if( unicode_search_ptr == NULL )
		return -1;	

	//printf("end_ptr=0x%08x\n",unicode_search_ptr);

	return 0;	
}

int api_ascii_to_unicode( char* pascii, int ascii_len, char* puicode )
{
	int i;
	for( i = 0; i < ascii_len; i++ )
	{
		puicode[i*2+0] = pascii[i];
		puicode[i*2+1] = 0;		
	}
	return i;
}

/*****************************************************************************
 * 将一个字符的Unicode(UCS-2和UCS-4)编码转换成UTF-8编码.
 *
 * 参数:
 *    unic     字符的Unicode编码值
 *    pOutput  指向输出的用于存储UTF8编码值的缓冲区的指针
 *    outsize  pOutput缓冲的大小
 *
 * 返回值:
 *    返回转换后的字符的UTF8编码所占的字节数, 如果出错则返回 0 .
 *
 * 注意:
 *     1. UTF8没有字节序问题, 但是Unicode有字节序要求;
 *        字节序分为大端(Big Endian)和小端(Little Endian)两种;
 *        在Intel处理器中采用小端法表示, 在此采用小端法表示. (低地址存低位)
 *     2. 请保证 pOutput 缓冲区有最少有 6 字节的空间大小!
 ****************************************************************************/
int enc_unicode_to_utf8_one(unsigned long unic, unsigned char *pOutput, int outSize)
{
    if((pOutput != NULL) || (outSize >= 6))
	{
		return 0;
	}

    if ( unic <= 0x0000007F )
    {
        // * U-00000000 - U-0000007F:  0xxxxxxx
        *pOutput     = (unic & 0x7F);
        return 1;
    }
    else if ( unic >= 0x00000080 && unic <= 0x000007FF )
    {
        // * U-00000080 - U-000007FF:  110xxxxx 10xxxxxx
        *(pOutput+1) = (unic & 0x3F) | 0x80;
        *pOutput     = ((unic >> 6) & 0x1F) | 0xC0;
        return 2;
    }
    else if ( unic >= 0x00000800 && unic <= 0x0000FFFF )
    {
        // * U-00000800 - U-0000FFFF:  1110xxxx 10xxxxxx 10xxxxxx
        *(pOutput+2) = (unic & 0x3F) | 0x80;
        *(pOutput+1) = ((unic >>  6) & 0x3F) | 0x80;
        *pOutput     = ((unic >> 12) & 0x0F) | 0xE0;
        return 3;
    }
    else if ( unic >= 0x00010000 && unic <= 0x001FFFFF )
    {
        // * U-00010000 - U-001FFFFF:  11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        *(pOutput+3) = (unic & 0x3F) | 0x80;
        *(pOutput+2) = ((unic >>  6) & 0x3F) | 0x80;
        *(pOutput+1) = ((unic >> 12) & 0x3F) | 0x80;
        *pOutput     = ((unic >> 18) & 0x07) | 0xF0;
        return 4;
    }
    else if ( unic >= 0x00200000 && unic <= 0x03FFFFFF )
    {
        // * U-00200000 - U-03FFFFFF:  111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        *(pOutput+4) = (unic & 0x3F) | 0x80;
        *(pOutput+3) = ((unic >>  6) & 0x3F) | 0x80;
        *(pOutput+2) = ((unic >> 12) & 0x3F) | 0x80;
        *(pOutput+1) = ((unic >> 18) & 0x3F) | 0x80;
        *pOutput     = ((unic >> 24) & 0x03) | 0xF8;
        return 5;
    }
    else if ( unic >= 0x04000000 && unic <= 0x7FFFFFFF )
    {
        // * U-04000000 - U-7FFFFFFF:  1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        *(pOutput+5) = (unic & 0x3F) | 0x80;
        *(pOutput+4) = ((unic >>  6) & 0x3F) | 0x80;
        *(pOutput+3) = ((unic >> 12) & 0x3F) | 0x80;
        *(pOutput+2) = ((unic >> 18) & 0x3F) | 0x80;
        *(pOutput+1) = ((unic >> 24) & 0x3F) | 0x80;
        *pOutput     = ((unic >> 30) & 0x01) | 0xFC;
        return 6;
    }

    return 0;
}

int printf_one_unicode_str( int type, int id )
{
	if( !unicode_str_list_is_empty() )
	{
		char org_buf[UNICODE_STRING_MAX_LEN],tra_buf[UNICODE_STRING_MAX_LEN];
		int	 org_len,tra_len;
		if( api_get_one_unicode_both_str(type,id,org_buf,&org_len,tra_buf,&tra_len) )
		{		
			int i;
			printf("[type=%d,id=%d]",type,id);		
			printf("[org]");
			for( i = 0; i < org_len; i++ )
			{
				printf("%02x,",org_buf[i]);
			}
			printf("[tra]");
			for( i = 0; i < tra_len; i++ )
			{
				printf("%02x,",tra_buf[i]);
			}
			printf("\n");
		}
		else
		{
			printf("type=%d,id=%d not exist!\n",type,id);		
		}
	}
	else
		printf("icon list is empty!\n");
}


