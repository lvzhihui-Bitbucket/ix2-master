#ifndef _OBJ_LANGUAGE_INI_H_
#define _OBJ_LANGUAGE_INI_H_


#define MENU_LANGUAGE_MAX_CNT					20

typedef struct
{
	int		cnt;	
	char	name[MENU_LANGUAGE_MAX_CNT][100];
	char	displayName[MENU_LANGUAGE_MAX_CNT][30];
	char invert_flag[MENU_LANGUAGE_MAX_CNT];	//FOR_HEBREW
} MENU_LANGUAGE_LIST_T;




#endif


