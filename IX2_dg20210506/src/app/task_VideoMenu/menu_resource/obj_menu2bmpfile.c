
// lzh_20181108_s

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/statfs.h>
#include <linux/fs.h>
#include <unistd.h>
#include "utility.h"
#include "obj_menu2bmpfile.h"
#include "define_file.h"

//生成BMP图片(无颜色表的位图):在RGB(A)位图数据的基础上加上文件信息头和位图信息头
int GenBmpFile(U8 *pData, U8 bitCountPerPix, U32 width, U32 height, const char *filename);

//获取BMP文件的位图数据(无颜色表的位图):丢掉BMP文件的文件信息头和位图信息头，获取其RGB(A)位图数据
U8* GetBmpData(U8 *bitCountPerPix, U32 *width, U32 *height, const char* filename);

//释放GetBmpData分配的空间
void FreeBmpData(U8 *pdata);

//生成BMP图片(无颜色表的位图):在RGB(A)位图数据的基础上加上文件信息头和位图信息头 
int GenBmpFile(U8 *pData, U8 bitCountPerPix, U32 width, U32 height, const char *filename) 
{ 
	int osd_bytes_per_pixel;

	// 保存的图片总是24bits格式
	osd_bytes_per_pixel = bitCountPerPix >> 3;
	bitCountPerPix = 24;
		
    FILE *fp = fopen(filename, "wb+"); 
    if(!fp) 
    { 
        printf("fopen failed : %s, %d\n", __FILE__, __LINE__); 
        return 0; 
    } 
    U32 bmppitch = ((width*bitCountPerPix + 31) >> 5) << 2; 
    U32 filesize = bmppitch*height; 

	fseek(fp,0,SEEK_SET); 		 

    BITMAPFILE bmpfile; 
    bmpfile.bfHeader.bfType = 0x4D42; 
    bmpfile.bfHeader.bfSize = filesize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER); 
    bmpfile.bfHeader.bfReserved1 = 0; 
    bmpfile.bfHeader.bfReserved2 = 0; 
    bmpfile.bfHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER); 
    bmpfile.biInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    bmpfile.biInfo.bmiHeader.biWidth = width; 
    bmpfile.biInfo.bmiHeader.biHeight = height; 
    bmpfile.biInfo.bmiHeader.biPlanes = 1; 
    bmpfile.biInfo.bmiHeader.biBitCount = bitCountPerPix; 
    bmpfile.biInfo.bmiHeader.biCompression = 0; 
    bmpfile.biInfo.bmiHeader.biSizeImage = 0; 
    bmpfile.biInfo.bmiHeader.biXPelsPerMeter = 0; 
    bmpfile.biInfo.bmiHeader.biYPelsPerMeter = 0; 
    bmpfile.biInfo.bmiHeader.biClrUsed = 0; 
    bmpfile.biInfo.bmiHeader.biClrImportant = 0; 

    fwrite(&(bmpfile.bfHeader), sizeof(BITMAPFILEHEADER), 1, fp); 
    fwrite(&(bmpfile.biInfo.bmiHeader), sizeof(BITMAPINFOHEADER), 1, fp); 

	U8 *pEachLinBuf = (U8*)malloc(bmppitch); 
    memset(pEachLinBuf, 0, bmppitch); 
    U8 BytePerPix = bitCountPerPix >> 3; 
    U32 pitch = width * BytePerPix; 

    if(pEachLinBuf) 
    { 
        int h,w; 
        for(h = height-1; h >= 0; h--) 
        { 
            for(w = 0; w < width; w++) 
            { 
                //copy by a pixel 
                if( osd_bytes_per_pixel == 2 )
            	{
            		short rgb;
            		char r,g,b;
					rgb = pData[h*width*osd_bytes_per_pixel + w*osd_bytes_per_pixel + 1];
					rgb <<= 8;
					rgb += pData[h*width*osd_bytes_per_pixel + w*osd_bytes_per_pixel + 0];					
					r = (rgb&0xf800)>>8;
					g = (rgb&0x07e0)>>3;
					b = (rgb&0x001f)<<3;					
					pEachLinBuf[w*BytePerPix+0] = b; 
					pEachLinBuf[w*BytePerPix+1] = g;             		
					pEachLinBuf[w*BytePerPix+2] = r;             		
            	}
				else
				{
	                pEachLinBuf[w*BytePerPix+0] = pData[h*pitch + w*BytePerPix + 0]; 
	                pEachLinBuf[w*BytePerPix+1] = pData[h*pitch + w*BytePerPix + 1]; 
	                pEachLinBuf[w*BytePerPix+2] = pData[h*pitch + w*BytePerPix + 2]; 
				}
            } 

            fwrite(pEachLinBuf, bmppitch, 1, fp); 
        } 
        free(pEachLinBuf); 
    } 
    fclose(fp); 
	sync();
    return 1; 
} 

//获取BMP文件的位图数据(无颜色表的位图):丢掉BMP文件的文件信息头和位图信息头，获取其RGB(A)位图数据 
U8* GetBmpData(U8 *bitCountPerPix, U32 *width, U32 *height, const char* filename) 
{ 
    FILE *pf = fopen(filename, "rb"); 
    if(!pf) 
    { 
        printf("fopen failed : %s, %d\n", __FILE__, __LINE__); 
        return NULL; 
    } 

    BITMAPFILE bmpfile; 
    fread(&(bmpfile.bfHeader), sizeof(BITMAPFILEHEADER), 1, pf); 
    fread(&(bmpfile.biInfo.bmiHeader), sizeof(BITMAPINFOHEADER), 1, pf); 

    if(bitCountPerPix) 
    { 
        *bitCountPerPix = bmpfile.biInfo.bmiHeader.biBitCount; 
    } 

    if(width) 
    { 
        *width = bmpfile.biInfo.bmiHeader.biWidth; 
    } 

    if(height) 
    { 
        *height = bmpfile.biInfo.bmiHeader.biHeight; 
    } 
	
    U32 bmppicth = (((*width)*(*bitCountPerPix) + 31) >> 5) << 2; 
    U8 *pdata = (U8*)malloc((*height)*bmppicth); 
    U8 *pEachLinBuf = (U8*)malloc(bmppicth); 

    memset(pEachLinBuf, 0, bmppicth); 
	
    U8 BytePerPix = (*bitCountPerPix) >> 3; 
    U32 pitch = (*width) * BytePerPix; 
    if(pdata && pEachLinBuf) 
    { 
        int w, h; 
        for(h = (*height) - 1; h >= 0; h--) 
        { 
            fread(pEachLinBuf, bmppicth, 1, pf); 
            for(w = 0; w < (*width); w++) 
            { 
                pdata[h*pitch + w*BytePerPix + 0] = pEachLinBuf[w*BytePerPix+0]; 
                pdata[h*pitch + w*BytePerPix + 1] = pEachLinBuf[w*BytePerPix+1]; 
                pdata[h*pitch + w*BytePerPix + 2] = pEachLinBuf[w*BytePerPix+2]; 
            } 
        } 
        free(pEachLinBuf); 
    } 
    fclose(pf); 
    return pdata; 
} 

//释放GetBmpData分配的空间 
void FreeBmpData(U8 *pdata) 
{ 
    if(pdata) 
    { 
        free(pdata); 
        pdata = NULL; 
    } 
} 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
char* get_osd_display_attr(int* ptr_max_size, int* ptr_width, int* ptr_height, int* ptr_bpp_len );

// 0/ok, 1/ no sdcard, 2/sdcard err
int create_bmpfile_for_menu( int menu_id, int sub_id, int sub2_id )
{
	time_t t;
	struct tm *tblock; 
	DisableUpdate();

	// 检查sdcard不存在或者创建不成功
    if(MakeDir(menu_bmp_path) != 0)
    {
		EnableUpdate();
		return 1;
    }

	// 覆盖掉指定的文件
	char* ptr_bmp_buf;
	int max_size, width, height, bpp_len;	
	char cache_path[250];
	
	t = time(NULL); 
	tblock=localtime(&t);
	//int year=tblock->tm_year;
	snprintf(cache_path,250,"%smenu%03d_bmp%d_%d_%02d%02d%02d%02d%02d%02d.bmp",menu_bmp_path,menu_id,sub_id,sub2_id,tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	//snprintf(cache_path,250,"%smenu%03d_bmp%d_%d_%d.bmp",menu_bmp_path,menu_id,sub_id,sub2_id,year);
	if( access( cache_path, F_OK ) < 0 )
		printf("%s just is NOT existed, new!!\n",cache_path);
	else
		printf("%s just is existed, recovered!!\n",cache_path);

	ptr_bmp_buf = get_osd_display_attr( &max_size, &width, &height, &bpp_len);

	GenBmpFile((U8*)ptr_bmp_buf, (bpp_len==2)?16:24, width, height, cache_path);	//生成BMP文件

	printf("create_bmpfile_for_menu ok!![%s]\n",cache_path);

	EnableUpdate();
	
	return 0;
}

// lzh_20181108_e

