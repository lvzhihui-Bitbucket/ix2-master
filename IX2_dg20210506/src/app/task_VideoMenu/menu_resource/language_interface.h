
#ifndef _LANGUAGE_INTERFACE_H_
#define _LANGUAGE_INTERFACE_H_


#include "obj_language_dat.h"
#include "obj_language_ini.h"

char* get_global_mesg_org_with_id( int id );
char* get_global_mesg_tra_with_id( int id );
int get_global_mesg_id_with_org( char* org_ptr );
char* get_global_mesg_tra_with_org( char* org_ptr );

int export_global_mesg_text_file( void );

int load_menu_unicode_resource( int language_cnt );

#endif

