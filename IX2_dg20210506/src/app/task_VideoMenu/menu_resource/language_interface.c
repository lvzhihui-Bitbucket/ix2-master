
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "menu_unicode_list.h"
#include "language_interface.h"
#include "../task_VideoMenu.h"
#include "../../task_io_server/task_IoServer.h"



#define MENU_LANG_FILE_PATH		"/mnt/nand1-2/res/"
#define MENU_LANG_MESG_FILE		"/mnt/nand1-2/res/menu_lang_mesg.def"
	
#define UNICODE_FILE_TYPE_UNKNOW			0
#define	UNICODE_FILE_TYPE_RAW_LITTLE		1
#define UNICODE_FILE_TYPE_RAW_BIG			2
#define UNICODE_FILE_TYPE_UTF8				3
	
const char UNICODE_RAW_LITTLE_FLAG[2]	= {0xff,0xfe};		// little endian
const char UNICODE_RAW_BIG_FLAG[2]		= {0xfe,0xff};		// big endian
const char UNICODE_UTF8_FLAG[3] 		= {0xef,0xbb,0xbf};
static int cur_unicode_type = 0;

int utf82unicode( char* pin, int in_len, short* pout );


// 加载指定语言的unicode文件
int load_menu_unicode_resource( int language_cnt )
{    
	char langId[4];
	char fileName[100];
	char fullPathName[200];
	// 初始化链表
	api_init_unicode_str_list();

	if( !unicode_str_list_is_empty() )
	{
		api_del_all_unicode_str_node();
	}

	// 加载资源文件到链表

	FILE *stream = NULL;
	
	//get_cur_language_id(langId);
	//get_one_language_pack_file(langId, fileName);
	//snprintf(fullPathName, 200, "%s%s", MENU_LANG_FILE_PATH, fileName);
	
	MenuLanguageListInit();
	GetMenuLanguageFileName(fullPathName);

	stream = fopen(fullPathName,"r");
	if( stream == NULL )
	{
		if(GetMenuLanguageListCnt())
		{
			GetMenuLanguageListRecord(0, fullPathName);
			API_Event_IoServer_InnerWrite_All(MENU_LANGUAGE_SELECT, fullPathName);

			stream = fopen(fullPathName,"r");
			if( stream == NULL )
			{
				printf("open menu language locale:%s failed...\n", fullPathName);
				return -1;
			}
		}	
		else
		{
			printf("open menu language locale:%s failed...\n", fullPathName);
			return -1;
		}
	}
	
	set_cur_unicode_invert_flag(GetMenuLanguageInvertFlag());		//FOR_HEBREW
	// read unicode flag
	char UNICODE_FLAG[3];
	fread(UNICODE_FLAG,1,2,stream);

	cur_unicode_type = UNICODE_FILE_TYPE_UNKNOW;
		
	if( (UNICODE_FLAG[0] == UNICODE_RAW_LITTLE_FLAG[0]) && (UNICODE_FLAG[1] == UNICODE_RAW_LITTLE_FLAG[1]) )
	{
		cur_unicode_type = UNICODE_FILE_TYPE_RAW_LITTLE;
	}
	else if( (UNICODE_FLAG[0] == UNICODE_RAW_BIG_FLAG[0]) && (UNICODE_FLAG[1] == UNICODE_RAW_BIG_FLAG[1]) )
	{
		cur_unicode_type = UNICODE_FILE_TYPE_RAW_BIG;
	}
	else if( (UNICODE_FLAG[0] == UNICODE_UTF8_FLAG[0]) && (UNICODE_FLAG[1] == UNICODE_UTF8_FLAG[1]) )
	{
		fread(UNICODE_FLAG+2,1,1,stream);
		if( UNICODE_FLAG[2] == UNICODE_UTF8_FLAG[2] )
		{
			cur_unicode_type = UNICODE_FILE_TYPE_UTF8;
		}
	}

	if( cur_unicode_type == UNICODE_FILE_TYPE_RAW_LITTLE )
	{
		printf("unicode little endian file format\n");	
		// read unicode string
		fseek( stream, 2, SEEK_SET );		
	}
	else if( cur_unicode_type == UNICODE_FILE_TYPE_RAW_BIG )
	{
		printf("unicode big endian file format\n");	
		// read unicode string
		fseek( stream, 2, SEEK_SET );		
	}
	else if( cur_unicode_type == UNICODE_FILE_TYPE_UTF8 )
	{
		printf("unicode utf8 file format\n");	
		// read unicode string
		fseek( stream, 3, SEEK_SET );		
	}
	else
	{
		printf("unknow file format\n");
		fclose( stream );
		return -1;		
	}
	
	int row_counter = 0;
	char szBuffer[UNICODE_STRING_MAX_LEN] = {0};
	short unBuffer[UNICODE_STRING_MAX_LEN] = {0};
	while( 1 )
	{
		if( fgets(szBuffer,UNICODE_STRING_MAX_LEN,stream) == NULL )	// 新的一行会自动补零
		{
		 	printf("over!\n");
			break;
		}
		else
		{
			row_counter++;
			
			//-----------------------------------------------------------------------------------------
			// 解析数据，添加到字符串链表
			int type, id,org_len, tra_len;
			char* porg_ptr;
			char* ptra_ptr;

			if( cur_unicode_type == UNICODE_FILE_TYPE_UTF8 )
			{			
				int utf8_len,unicode_len;
				
				utf8_len = strlen( szBuffer );
				unicode_len = utf82unicode( szBuffer, utf8_len, unBuffer );

				if( api_get_node_data_from_raw_buffer( (char*)unBuffer, unicode_len*2, &type,&id, &porg_ptr, &org_len, &ptra_ptr, &tra_len  ) != -1 )
				{
					api_add_one_unicode_str_node(type,id,porg_ptr,org_len,ptra_ptr,tra_len);
				}
			}
			else if( cur_unicode_type == UNICODE_FILE_TYPE_RAW_BIG )
			{
				// linux fgets读取到0a换行符后结束，dos下为000d00、000a，不需要移动一个字节去掉后面的00
				// fseek( stream, 1, SEEK_CUR );				
				
				// 将大端对齐方式更改为小端对齐方式
				int i;
				char temp;				
				for( i = 0; i < UNICODE_STRING_MAX_LEN; i+=2 )
				{
					temp 			= szBuffer[i];
					szBuffer[i] 	= szBuffer[i+1];
					szBuffer[i+1]	= temp;
				}
				
				if( api_get_node_data_from_raw_buffer( szBuffer, UNICODE_STRING_MAX_LEN, &type,&id, &porg_ptr, &org_len, &ptra_ptr, &tra_len  ) != -1 )
				{
					api_add_one_unicode_str_node(type,id,porg_ptr,org_len,ptra_ptr,tra_len);
				}				
			}
			else if( cur_unicode_type == UNICODE_FILE_TYPE_RAW_LITTLE )
			{
				// linux fgets读取到0a换行符后结束，dos下为0d00、0a00，故需要移动一个字节去掉后面的00
				fseek( stream, 1, SEEK_CUR );				
				
				if( api_get_node_data_from_raw_buffer( szBuffer,UNICODE_STRING_MAX_LEN, &type,&id, &porg_ptr, &org_len, &ptra_ptr, &tra_len  ) != -1 )
				{
					api_add_one_unicode_str_node(type,id,porg_ptr,org_len,ptra_ptr,tra_len);
				}
			}
			//-----------------------------------------------------------------------------------------			
			#if 0
			int i;
			printf("num=%d:",row_counter);			
			for( i = 0; i < 90; i++ )
			{
				printf("%02x,",szBuffer[i]);
			}
			printf("\n");
			#endif
		}
	}
	fclose( stream );	
	
	//printf_each_unicode_str_node();

	//printf_one_unicode_str(UNICODE_STR_TYPE_MESG,2);
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// mesg fuction list
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern const char* UNICODE_HEAD_TYPE_TAB[];

char* get_global_mesg_org_with_id( int id )
{
	int i = 0;

	while( global_mesg_text_tab[i].org_ptr != NULL )
	{
		if( global_mesg_text_tab[i].id == id )
			break;
		i++;
	}
	if(global_mesg_text_tab[i].org_ptr == NULL)
	{
		return NULL;
	}
	return global_mesg_text_tab[i].org_ptr+strlen(UNICODE_HEAD_TYPE_TAB[UNICODE_STR_TYPE_MESG]);
}

char* get_global_mesg_tra_with_id( int id )
{
	int i = 0;

	while( global_mesg_text_tab[i].tra_ptr != NULL )
	{
		if( global_mesg_text_tab[i].id == id )
			break;
		i++;
	}
	return global_mesg_text_tab[i].tra_ptr;
}

int get_global_mesg_id_with_org( char* org_ptr )
{
	int i = 0;
	
	while( global_mesg_text_tab[i].tra_ptr != NULL )
	{
		if( strcmp( global_mesg_text_tab[i].org_ptr, org_ptr ) == 0 )
			break;
		i++;
	}
	return global_mesg_text_tab[i].id;
}

char* get_global_mesg_tra_with_org( char* org_ptr )
{
	int i = 0;
	
	while( global_mesg_text_tab[i].tra_ptr != NULL )
	{
		if( strcmp( global_mesg_text_tab[i].org_ptr, org_ptr ) == 0 )
			break;
		i++;
	}
	return global_mesg_text_tab[i].tra_ptr;
}

int export_global_mesg_text_file( void )
{
	FILE *stream = NULL;
	stream = fopen(MENU_LANG_MESG_FILE,"w+");
	if( stream == NULL )
	{
		printf("open menu language locale failed...\n");
		return -1;
	}

	// write unicode flag
	fwrite(UNICODE_RAW_LITTLE_FLAG,1,2,stream);

	char szBuffer[300];

	int row_cnt = 0;
	
	while( global_mesg_text_tab[row_cnt].org_ptr != NULL )
	{
		int i,msg_len;
		// org string save as unicode
		for( msg_len = 0,i = 0; i <  strlen(global_mesg_text_tab[row_cnt].org_ptr); i++, msg_len++ )
		{
			szBuffer[msg_len*2+0] = global_mesg_text_tab[row_cnt].org_ptr[i];
			szBuffer[msg_len*2+1] = 0;
		}
		// save ','
		szBuffer[msg_len*2+0] = ',';
		szBuffer[msg_len*2+1] = 0;
		msg_len++;
		// tra string save as unicode
		for( i = 0; i <  strlen(global_mesg_text_tab[row_cnt].tra_ptr); i++, msg_len++ )
		{
			szBuffer[msg_len*2+0] = global_mesg_text_tab[row_cnt].tra_ptr[i];
			szBuffer[msg_len*2+1] = 0;
		}
		szBuffer[msg_len*2+0] = 0x0d;
		szBuffer[msg_len*2+1] = 0;
		msg_len++;
		szBuffer[msg_len*2+0] = 0x0a;
		szBuffer[msg_len*2+1] = 0;
		msg_len++;
		
		fwrite(szBuffer,1,msg_len*2,stream);		
		
		row_cnt++;
	}

	fclose(stream);

	sync();

	printf("==========export_global_mesg_text:total=%d=======\n",row_cnt);
	
	return row_cnt;
}

//FOR_HEBREW_S
int cur_unicode_invert_flag = 0;
int get_cur_unicode_invert_flag(void)
{
	return cur_unicode_invert_flag;
}
int set_cur_unicode_invert_flag(int new_state)
{
	cur_unicode_invert_flag = new_state;
}

void Unicode_Invert(char *unicode,int len)
{
	char acii_buff[100];
	char acii_buff2[100];
	//char head_buff[100];
	int head_len=0;
	int acii_flag2 = 0;
	int acii_len2 = 0;
	char temp_buff[100];
	int acii_flag = 0;
	int acii_len = 0;
	int index = 0;
	int i,j,k;
	int invert_flag = 0;
	
	
	for(i = 0;i < (len/2);i++)
	{
		
		if(unicode[i*2+1] == 0)
		{
			if(index==0)
			{
				head_len++;
				continue;
			}
			if(acii_flag == 0)
			{
				if(unicode[i*2] == ' ' ||unicode[i*2] == ':')
				{
					temp_buff[index*2] = unicode[i*2];
					temp_buff[index*2+1] = unicode[i*2+1];
					index++;
				}
				else
				{
					acii_len2 = 0;
					acii_flag2 = 0;
					acii_len = 0;
					acii_flag = 1;
					acii_buff[acii_len*2] = unicode[i*2];
					acii_buff[acii_len*2+1] = unicode[i*2+1];
					acii_len ++;
				}
			}
			else
			{
				if(unicode[i*2] == ' ' ||unicode[i*2] == ':')
				{
					acii_len2 = 0;
					acii_flag2 = 0;
					acii_buff2[acii_len2*2] = unicode[i*2];
					acii_buff2[acii_len2*2+1] = unicode[i*2+1];
					acii_len2++;
					
					for(k = i+1;k< (len/2);k++)
					{
						if(unicode[k*2+1] != 0)
							break;
						if(unicode[k*2+1] == 0 &&  unicode[k*2] != ' ' && unicode[k*2] != ':')
						{
							acii_buff2[acii_len2*2] = unicode[k*2];
							acii_buff2[acii_len2*2+1] = unicode[k*2+1];
							acii_len2++;
							acii_flag2=1;
							break;
						}
						acii_buff2[acii_len2*2] = unicode[k*2];
						acii_buff2[acii_len2*2+1] = unicode[k*2+1];
						acii_len2++;
					}
					if(acii_flag2)
					{
						memcpy(acii_buff+acii_len*2,acii_buff2,acii_len2*2);
						acii_len+=acii_len2;
						i+=(acii_len2-1);
					}
					else if(k== (len/2)||unicode[k*2+1] != 0)
					{
						#if 0
						if(index==0)
						{
							//memcpy(unicode,acii_buff,acii_len*2);
							acii_len = 0;
							acii_flag = 0;
							if(acii_flag2)
								head_len=acii_len;
							else
								head_len=acii_len+acii_len2;
						}
						else
						#endif
						{
							for(j = acii_len;j > 0;j --)
							{
								temp_buff[index*2] = acii_buff[(j-1)*2];
								temp_buff[index*2+1] = acii_buff[(j-1)*2+1];
								index++;
							}
							acii_len = 0;
							acii_flag = 0;
							
							temp_buff[index*2] = unicode[i*2];
							temp_buff[index*2+1] = unicode[i*2+1];
							index++;
						}
					}
				}
				else
				{
					acii_buff[acii_len*2] = unicode[i*2];
					acii_buff[acii_len*2+1] = unicode[i*2+1];
					acii_len ++;
				}
			}
		}
		else
		{
			invert_flag = 1;
			if(acii_flag == 1)
			{
				#if 0
				if(index==0)
				{
					memcpy(unicode,acii_buff,acii_len*2);
					head_len=acii_len;
				}
				else
				#endif
				{
					if(acii_len==1&&acii_buff[0]=='[')
						acii_buff[0]=']';
					else if(acii_len==1&&acii_buff[0]==']')
						acii_buff[0]='[';
					for(j = acii_len;j > 0;j --)
					{
						temp_buff[index*2] = acii_buff[(j-1)*2];
						temp_buff[index*2+1] = acii_buff[(j-1)*2+1];
						index++;
					}
				}
				acii_len = 0;
				acii_flag = 0;
			}
			
			temp_buff[index*2] = unicode[i*2];
			temp_buff[index*2+1] = unicode[i*2+1];
			index++;
		}
	}
	if(acii_flag)
	{
		if(acii_len==1&&acii_buff[0]=='[')
			acii_buff[0]=']';
		else if(acii_len==1&&acii_buff[0]==']')
			acii_buff[0]='[';
		for(j = acii_len;j > 0;j --)
		{
			temp_buff[index*2] = acii_buff[(j-1)*2];
			temp_buff[index*2+1] = acii_buff[(j-1)*2+1];
			index++;
		}
		acii_len = 0;
		acii_flag = 0;
	}
	
	if(invert_flag == 0)
		return;
	if(index+head_len != (len/2))
		return;
	for(i = 0;i < index;i++)
	{
		#if 0
		if(temp_buff[(index-1-i)*2+1] == 0&&temp_buff[(index-1-i)*2] =='[' )
		{
			temp_buff[(index-1-i)*2] =']';		
		}
		else if(temp_buff[(index-1-i)*2+1] == 0&&temp_buff[(index-1-i)*2] ==']' )
		{
			temp_buff[(index-1-i)*2] ='[';		
		}
		#endif
		unicode[(i+head_len)*2] = temp_buff[(index-1-i)*2];
		unicode[(i+head_len)*2+1] = temp_buff[(index-1-i)*2+1];
	}
}

const unsigned short Arbic_Position[][4]=  // first, last, middle, alone
{

	{ 0xfe80, 0xfe80, 0xfe80, 0xfe80},   // 0x621

	{ 0xfe82, 0xfe81, 0xfe82, 0xfe81},

	{ 0xfe84, 0xfe83, 0xfe84, 0xfe83},

	{ 0xfe86, 0xfe85, 0xfe86, 0xfe85},

	{ 0xfe88, 0xfe87, 0xfe88, 0xfe87},

	{ 0xfe8a, 0xfe8b, 0xfe8c, 0xfe89},

	{ 0xfe8e, 0xfe8d, 0xfe8e, 0xfe8d},

	{ 0xfe90, 0xfe91, 0xfe92, 0xfe8f},   // 0x628

	{ 0xfe94, 0xfe93, 0xfe94, 0xfe93},

	{ 0xfe96, 0xfe97, 0xfe98, 0xfe95},   // 0x62A

	{ 0xfe9a, 0xfe9b, 0xfe9c, 0xfe99},

	{ 0xfe9e, 0xfe9f, 0xfea0, 0xfe9d},

	{ 0xfea2, 0xfea3, 0xfea4, 0xfea1},

	{ 0xfea6, 0xfea7, 0xfea8, 0xfea5},

	{ 0xfeaa, 0xfea9, 0xfeaa, 0xfea9},

	{ 0xfeac, 0xfeab, 0xfeac, 0xfeab},   // 0x630  

	{ 0xfeae, 0xfead, 0xfeae, 0xfead},

	{ 0xfeb0, 0xfeaf, 0xfeb0, 0xfeaf},

	{ 0xfeb2, 0xfeb3, 0xfeb4, 0xfeb1},

	{ 0xfeb6, 0xfeb7, 0xfeb8, 0xfeb5},

	{ 0xfeba, 0xfebb, 0xfebc, 0xfeb9},

	{ 0xfebe, 0xfebf, 0xfec0, 0xfebd},

	{ 0xfec2, 0xfec3, 0xfec4, 0xfec1},

	{ 0xfec6, 0xfec7, 0xfec8, 0xfec5},  // 0x638

	{ 0xfeca, 0xfecb, 0xfecc, 0xfec9},

	{ 0xfece, 0xfecf, 0xfed0, 0xfecd},  //0x63A

	{ 0x63b, 0x63b, 0x63b, 0x63b},

	{ 0x63c, 0x63c, 0x63c, 0x63c},

	{ 0x63d, 0x63d, 0x63d, 0x63d},

	{ 0x63e, 0x63e, 0x63e, 0x63e},

	{ 0x63f, 0x63f, 0x63f, 0x63f},

	{ 0x640, 0x640, 0x640, 0x640},   // 0x640

	{ 0xfed2, 0xfed3, 0xfed4, 0xfed1},

	{ 0xfed6, 0xfed7, 0xfed8, 0xfed5},

	{ 0xfeda, 0xfedb, 0xfedc, 0xfed9},

	{ 0xfede, 0xfedf, 0xfee0, 0xfedd},

	{ 0xfee2, 0xfee3, 0xfee4, 0xfee1},

	{ 0xfee6, 0xfee7, 0xfee8, 0xfee5},

	{ 0xfeea, 0xfeeb, 0xfeec, 0xfee9},

	{ 0xfeee, 0xfeed, 0xfeee, 0xfeed},   // 0x648

	{ 0xfef0, 0xfef3, 0xfef4, 0xfeef},

	{0xfef2, 0xfef3, 0xfef4, 0xfef1},   // 0x64A

};

static unsigned short theSet1[24]={
	0x62c,0x62d,0x62e,0x647,0x639,0x63a,0x641,0x642,
       0x62b,0x635,0x636,0x637,0x643,0x645,0x646,0x62a,
       0x644,0x628,0x64a,0x633,0x634,0x638,0x626,0x640};
static unsigned short theSet2[36]={
       0x62c,0x62d,0x62e,0x647,0x639,0x63a,0x641,0x642,
       0x62b,0x635,0x636,0x637,0x643,0x645,0x646,0x62a,
       0x644,0x628,0x64a,0x633,0x634,0x638,0x626,
       0x627,0x623,0x625,0x622,0x62f,0x630,0x631,0x632,
       0x648,0x624,0x629,0x649,0x640};	
static unsigned short arabic_specs[][3]=
{
	{0x622,0xFEF5,0xFEF6},
	{0x623,0xFEF7,0xFEF8},
	{0x625,0xFEF9,0xFEFA},
	{0x627,0xFEFB,0xFEFC},
};	

int Arbic_Position_Process(char *unicode,int len,char *position_buff1,int *position_len)
{
	int set1_flag=0;
	int set2_flag=0;
	int i,j;
	unsigned short arbic_ch=0;
	unsigned short arbic_ch_pre=0;
	unsigned short arbic_ch_next=0;
	int out_len=0;
	char position_buff[200];
	
	for(i = 0;i < (len/2);i++)
	{
		arbic_ch = unicode[i*2+1];
		arbic_ch = (arbic_ch<<8)|unicode[i*2];
		if(arbic_ch>=0x600&&arbic_ch<=0x6FF)
			break;
	}
	if(i < (len/2))
	{
		for(i = 0;i < (len/2);i++)
		{
			
			set1_flag=0;
			set2_flag=0;
			
			arbic_ch = unicode[i*2+1];
			arbic_ch = (arbic_ch<<8)|unicode[i*2];
			if(arbic_ch<0x621||arbic_ch>0x64a)
			{
				position_buff[out_len*2]= arbic_ch;
				position_buff[out_len*2+1]= (arbic_ch>>8);
				out_len++;
				continue;
			}
			if(i>0)
			{
				arbic_ch_pre= unicode[(i-1)*2+1];
				arbic_ch_pre = (arbic_ch_pre<<8)|unicode[(i-1)*2];
				if(arbic_ch_pre>0x64a&&i>1)
				{
					arbic_ch_pre= unicode[(i-2)*2+1];
					arbic_ch_pre= (arbic_ch_pre<<8)|unicode[(i-2)*2];
				}
				for(j=0;j<24;j++)
				{
					if(arbic_ch_pre == theSet1[j])
					{
						set1_flag=1;
						break;
					}
				}
			}
			
			if(i<((len/2)-1))
			{
				arbic_ch_next = unicode[(i+1)*2+1];
				arbic_ch_next = (arbic_ch_next<<8)|unicode[(i+1)*2];
				if(arbic_ch_next>0x64a&&i<((len/2)-2))
				{
					arbic_ch_next = unicode[(i+2)*2+1];
					arbic_ch_next = (arbic_ch_next<<8)|unicode[(i+2)*2];
				}
				for(j=0;j<36;j++)
				{
					if(arbic_ch_next== theSet2[j])
					{
						set2_flag=1;
						break;
					}
				}
			}
			//if(arbic_ch>=0x600&&arbic_ch<=0x6FF)
			{
				if(arbic_ch==0x644)
				{
					for(j=0;j<4;j++)
					{
						if(arbic_ch_next==arabic_specs[j][0])
						{
							if(set1_flag==1)
							{
								position_buff[out_len*2]= arabic_specs[j][2];
								position_buff[out_len*2+1]= (arabic_specs[j][2]>>8);
							}
							else
							{
								position_buff[out_len*2]= arabic_specs[j][1];
								position_buff[out_len*2+1]= (arabic_specs[j][1]>>8);
							}
							++i;
							#if 0
							if(++i<((len/2)-1))
							{
								//arbic_ch=arbic_ch_next;
								//if(i<((len/2)-1))
								{
								//	arbic_ch_next = unicode[(i+1)*2+1];
								//	arbic_ch_next = (arbic_ch_next<<8)|unicode[(i+1)*2];
								}
							}
							#endif
							break;
						}
					}
					#if 0
					if(j>=4)
					{
						if(set1_flag==0&&set2_flag==0)
						{
							position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][3];
							position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][3]>>8);
						}
						else if(set1_flag==1&&set2_flag==0)
						{
							position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][0];
							position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][0]>>8);
						}
						else if(set1_flag==0&&set2_flag==1)
						{
							position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][1];
							position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][1]>>8);
						}
						else if(set1_flag==1&&set2_flag==1)
						{
							position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][2];
							position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][2]>>8);
						}
					}
					#endif
				}
				if(arbic_ch!=0x644||j>=4)
				{
					if(set1_flag==0&&set2_flag==0)
					{
						position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][3];
						position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][3]>>8);
					}
					else if(set1_flag==1&&set2_flag==0)
					{
						position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][0];
						position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][0]>>8);
					}
					else if(set1_flag==0&&set2_flag==1)
					{
						position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][1];
						position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][1]>>8);
					}
					else if(set1_flag==1&&set2_flag==1)
					{
						position_buff[out_len*2]= Arbic_Position[arbic_ch-0X621][2];
						position_buff[out_len*2+1]= (Arbic_Position[arbic_ch-0X621][2]>>8);
					}	
				}
			}
			
			out_len++;
			//arbic_ch_pre=arbic_ch;
			//arbic_ch=arbic_ch_next;
		}
		*position_len=out_len*2;
		memcpy(position_buff1,position_buff,*position_len);
	}
}
