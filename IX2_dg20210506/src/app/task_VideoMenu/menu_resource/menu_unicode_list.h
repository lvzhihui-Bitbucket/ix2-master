
#ifndef _MENU_UNICODE_LIST_H_
#define	_MENU_UNICODE_LIST_H_

#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <list.h>
#include <stdlib.h>
#include <string.h>

#define UNICODE_STRING_MAX_LEN		300

#define UNICODE_STR_TYPE_ICON		0
#define UNICODE_STR_TYPE_LABE		1
#define UNICODE_STR_TYPE_MESG		2

typedef struct
{
	int 	type;				// 	字符串类型
	int 	id;					// 	字符串编号
	int 	org_len;			// 	字符串原始长度
	char*	org_ptr;			// 	字符串原始数据指针
	int 	tra_len;			//	字符串国际化长度
	char*	tra_ptr;			//	字符串国际化数据指针
}one_unicode_str;

typedef struct
{ 
	one_unicode_str		data;		
 	struct list_head 	list;
} one_unicode_str_node;

void api_init_unicode_str_list( void );

int api_add_one_unicode_str_node( int type, int id, char* org_ptr, int org_len, char* tra_ptr, int tra_len );

int api_get_one_unicode_both_str( int type, int id, char* porg_ptr, int* porg_len, char* ptra_ptr, int* ptra_len );

int api_get_one_unicode_str( char* porg_ptr, int org_len, char* ptra_ptr, int* ptra_len  );

int api_get_node_data_from_raw_buffer( char* praw_buffer, int raw_buf_len, int* ptype, int * pid, char** pporg_ptr, int* porg_len, char** pptra_ptr, int* ptra_len );

int api_ascii_to_unicode( char* pascii, int ascii_len, char* puicode );

int printf_one_unicode_str( int type, int id );


#endif

