#ifndef _bmplib_h
#define _bmplib_h

typedef unsigned char  ebmpBYTE;
typedef unsigned short ebmpWORD;
typedef unsigned int  ebmpDWORD;

typedef struct RGBpixel {
	unsigned char Blue;
	unsigned char Green;
	unsigned char Red;
	//ebmpBYTE Alpha;
} RGBpixel; 

typedef struct 
{
	int w;
	int h;
	RGBpixel *pixel;
}BMP;


typedef struct{
	ebmpWORD  bfType;
	ebmpDWORD bfSize;
	ebmpWORD  bfReserved1;
	ebmpWORD  bfReserved2;
	ebmpDWORD bfOffBits; 
}BMFH;

typedef struct{
ebmpDWORD biSize;
ebmpDWORD biWidth;
ebmpDWORD biHeight;
ebmpWORD  biPlanes;
ebmpWORD  biBitCount;
ebmpDWORD biCompression;
ebmpDWORD biSizeImage;
ebmpDWORD biXPelsPerMeter;
ebmpDWORD biYPelsPerMeter;
ebmpDWORD biClrUsed;
ebmpDWORD biClrImportant;
}BMIH;

typedef struct
{
	FILE *fp;
	BMFH bmfh;
	BMIH bmih;
	int	cur_line;
	int	onelinebuff_lenth;
	unsigned char *onelinebuff;
}BMP_FILE;



int ReadFromFile( const char* FileName,BMP **pbmp);
void RGBpixeldisplay(int x,int y,RGBpixel *pixel);
void releaseBmp(BMP *pbmp);
BMP_FILE *OpenBMPFile(char *FileName);
void CloseBMPFile(BMP_FILE* bmpfile);
int BMPFileReadOneLine(BMP_FILE* bmpfile);
#endif

