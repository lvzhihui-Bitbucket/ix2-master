
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/socket.h> 
#include <netinet/in.h>   
#include <net/if.h>

#include <sys/mman.h>
#include <linux/fb.h>
#include <iconv.h>
#include <wchar.h>

#include <sys/stat.h>
#include <ft2build.h> 
#include FT_FREETYPE_H

#include "gui_lib.h"
#include "gui_font.h"
#include "gui_osdlayers.h"
#include "task_VideoMenu.h"

FT_Library 		library;
FT_Face 		face;
FT_Face 		face2;
// lzh_20181221_s
FT_Face 		face3;
// lzh_20181221_e


#define UNICODE_STRING_MAX_LEN		300
#define MAX_STR_CONVERT_LEN 		80
#define SX_MAX						80	// 24 
#define SY_MAX						80	// 24

#ifdef COLOR_FORMAT_RGB565
#define 		FNT_BPP_LEN			2	// color data format: 2byts\3byts\4bytes
#else
#define 		FNT_BPP_LEN			3	// color data format: 2byts\3byts\4bytes
#endif
char* 			font_buf;
char*			fnt_pbuf;

/*******************************************************************************************
 * @fn: 	init_ext_font
 *
 * @brief:	��ʼ����չfont
 *
 * @param:	none
 *
 * @return: -1/err��0/ok
 *******************************************************************************************/
#if 0
#define EXTEND_FONT_W		   16 //16
#define EXTEND_FONT_H		   16 //24

#define EXTEND2_FONT_W		   26//22 //16
#define EXTEND2_FONT_H		   26//24 //24
// lzh_20181221_s
#define EXTEND3_FONT_W		   18
#define EXTEND3_FONT_H		   20
// lzh_20181221_e
#endif

#define EXTEND_FONT_H_MAX		32
#define EXTEND_FONT_W_MAX		32
#define EXTEND_FONT2_NAME		EXTEND_FONT1_NAME
// lzh_20181221_s
#define EXTEND_FONT3_NAME		EXTEND_FONT1_NAME
// lzh_20181221_e
//czn_20190412_s
#define LOGVIEW_FONT_W		 	EXTEND_FONT_W_MAX
#define LOGVIEW_FONT_H		  	EXTEND_FONT_H_MAX
#define LOGVIEW_FONT_face		face2
//czn_20190412_e

const int EXT_FONT_HW[][6]=
{
	18, 18, 26, 26, 30, 30,
	18, 18, 26, 26, 28, 28,
	16, 16, 22, 24, 26, 26,
	26,26,40,40,32,32,
	16, 16, 22, 24, 26, 26,	// IX47
	16, 16, 20, 20, 24, 24,	// IX470V
	16, 16, 22, 24, 26, 26,	// DX470
	16, 16, 20, 20, 24, 24,	// DX470V
	16, 16, 20, 20, 24, 24,	// DX482
	16, 16, 22, 24, 26, 26,	// IX482SE
	16, 16, 22, 24, 26, 26,	// DX470-SE
};
int ext_font1_w,ext_font1_h,ext_font2_w,ext_font2_h,ext_font3_w,ext_font3_h;

int init_ext_font(int type)
{ 
	int error; 

	ext_font1_w = EXT_FONT_HW[type][0];
	ext_font1_h = EXT_FONT_HW[type][1];
	ext_font2_w = EXT_FONT_HW[type][2];
	ext_font2_h = EXT_FONT_HW[type][3];
	ext_font3_w = EXT_FONT_HW[type][4];
	ext_font3_h = EXT_FONT_HW[type][5];
	
	// load ext font	
	error = FT_Init_FreeType( &library ); 
	if( error ) 
	{ 
	        printf( "FT_Init_FreeType error:%d\n ", error ); 
	        return -1; 
	} 

	////////////////////////////////////////////////////////////////////////////////
	error = FT_New_Face( library,EXTEND_FONT1_NAME,0,&face ); 
	
	if( error ) 
	{ 
	        printf( "FT_New_Face error:%d\n ", error ); 
	        return -1; 
	} 

	// �趨ΪUNICODE��Ĭ��Ҳ��	
	error = FT_Select_Charmap( face, FT_ENCODING_UNICODE ); 
	if( error ) 
	{ 
	        printf( "FT_Select_Charmap error:%d\n ", error ); 
	        return -1; 
	} 

	printf( "num_glyphs: %d\n",(int)face->num_glyphs);
	printf( "num_faces: %d\n",(int)face->num_faces);

	// �趨�����ַ����ߺͷֱ���  == �˴������ز�׼ȷ��12x16��ʵ����ʾ�����Ŀ���ԼΪ7���߶�ԼΪ18����ʾ�ַ�����Ч����Ϊ5x10
	//error = FT_Set_Pixel_Sizes( face, EXTEND_FONT_W, EXTEND_FONT_H );
	error = FT_Set_Pixel_Sizes( face, ext_font1_w, ext_font1_h );

	////////////////////////////////////////////////////////////////////////////////
	error = FT_New_Face( library,EXTEND_FONT2_NAME,0,&face2 ); 
	
	if( error ) 
	{ 
	        printf( "FT_New_Face error:%d\n ", error ); 
	        return -1; 
	} 

	// �趨ΪUNICODE��Ĭ��Ҳ��	
	error = FT_Select_Charmap( face2, FT_ENCODING_UNICODE ); 
	if( error ) 
	{ 
	        printf( "FT_Select_Charmap error:%d\n ", error ); 
	        return -1; 
	} 

	printf( "num_glyphs: %d\n",(int)face2->num_glyphs);
	printf( "num_faces: %d\n",(int)face2->num_faces);

	// �趨�����ַ����ߺͷֱ���  == �˴������ز�׼ȷ��12x16��ʵ����ʾ�����Ŀ���ԼΪ7���߶�ԼΪ18����ʾ�ַ�����Ч����Ϊ5x10
	//error = FT_Set_Pixel_Sizes( face2, EXTEND2_FONT_W, EXTEND2_FONT_H );
	error = FT_Set_Pixel_Sizes( face2, ext_font2_w, ext_font2_h );

	// lzh_20181221_s
	////////////////////////////////////////////////////////////////////////////////
	error = FT_New_Face( library,EXTEND_FONT3_NAME,0,&face3 ); 
	
	if( error ) 
	{ 
	        printf( "FT_New_Face error:%d\n ", error ); 
	        return -1; 
	} 

	// �趨ΪUNICODE��Ĭ��Ҳ��	
	error = FT_Select_Charmap( face3, FT_ENCODING_UNICODE ); 
	if( error ) 
	{ 
	        printf( "FT_Select_Charmap error:%d\n ", error ); 
	        return -1; 
	} 

	printf( "num_glyphs: %d\n",(int)face3->num_glyphs);
	printf( "num_faces: %d\n",(int)face3->num_faces);

	// �趨�����ַ����ߺͷֱ���  == �˴������ز�׼ȷ��12x16��ʵ����ʾ�����Ŀ���ԼΪ7���߶�ԼΪ18����ʾ�ַ�����Ч����Ϊ5x10
	//error = FT_Set_Pixel_Sizes( face3, EXTEND3_FONT_W, EXTEND3_FONT_H );
	error = FT_Set_Pixel_Sizes( face3, ext_font3_w, ext_font3_h );
	// lzh_20181221_e

	// global var, not release!!!
	font_buf = malloc(FNT_BPP_LEN*SX_MAX*SY_MAX*2);
	
	return 0; 
}

/*******************************************************************************************
 * @fn: 	init_inner_font
 *
 * @brief:	��ʼ���ڲ�font
 *
 * @param:	none
 *
 * @return: -1/err��0/ok
 *******************************************************************************************/
int init_inner_font(void)
{
	// load inner font	
	FILE *fp;
	int fd;
	struct stat filestat;
	
	if( (fp=fopen(INNER_FONT_NAME,"rb")) == NULL )
	{
		printf( "parse error:%s\n",strerror(errno) );
		return -1;
	}
	fd = fileno(fp);
	fstat(fd,&filestat);
	fnt_pbuf = malloc(filestat.st_size);
	fread( fnt_pbuf, filestat.st_size, 1, fp );
	fclose(fp);
	return 0;	
}

/*******************************************************************************************
 * @fn: 	display_string
 *
 * @brief:	��osd����ʾһ�������ʽ�ַ���
 *
 * @param:  x 			- x��ʼ����
 * @param:  y 			- y��ʼ����
 * @param:  fgcolor		- ǰ��ɫ
 * @param:  bgcolor		- ����ɫ
 * @param:  string		- �ַ���ָ��
 *
 * @return: none
 *******************************************************************************************/
 #define INNER_FONT_W			16	//12 //16
 #define INNER_FONT_H			24  //16 //24
 #define INNER_ROW_BYTES		((INNER_FONT_W+7)/8)
 #define INNER_FNT_SIZE			(INNER_ROW_BYTES*INNER_FONT_H)
void display_string( int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string )
{
	int i,j,k,l,m;
	char chr;
	char chrdat[INNER_FNT_SIZE];

	unsigned char r,g,b,a;

	char* prgba_fg = (char*)&fgcolor;
	char* prgba_bg = (char*)&bgcolor;

	m = 0;
	while( *string )
	{
		chr = *string++;
		memcpy( chrdat, fnt_pbuf + chr*INNER_FNT_SIZE, INNER_FNT_SIZE );
		for( i = 0; i < INNER_FONT_H; i++ )
		{
			for( j = 0, l = 0; j < INNER_ROW_BYTES; j++ )
			{
				chr = chrdat[i*INNER_ROW_BYTES + j];
				for( k = 0; k < 8; k++ )
				{				
					if( chr&0x80 )
					{
						#if 1
						memcpy(font_buf+(i*INNER_FONT_W+l)*FNT_BPP_LEN,prgba_fg,FNT_BPP_LEN);
						#else				
						b = ((fgcolor>>16)&0xff);
						g = ((fgcolor>>8)&0xff);
						r = (fgcolor&0xff);
						font_buf[(i*INNER_FONT_W+l)*3+0] = b;
						font_buf[(i*INNER_FONT_W+l)*3+1] = g;
						font_buf[(i*INNER_FONT_W+l)*3+2] = r; 				
						#endif
					}
					else
					{
						#if 1
						memcpy(font_buf+(i*INNER_FONT_W+l)*FNT_BPP_LEN,prgba_bg,FNT_BPP_LEN);
						#else						
						b = ((bgcolor>>16)&0xff);
						g = ((bgcolor>>8)&0xff);
						r = (bgcolor&0xff);
						font_buf[(i*INNER_FONT_W+l)*3+0] = b;
						font_buf[(i*INNER_FONT_W+l)*3+1] = g;
						font_buf[(i*INNER_FONT_W+l)*3+2] = r; 				
						#endif
					}
					
					chr <<= 1;
					if( ++l >= INNER_FONT_W ) break;
				}
			}
		}
		WriteOneOsdLayer( OSD_LAYER_STRING, x+m*INNER_FONT_W, y, INNER_FONT_W, INNER_FONT_H, (char*)font_buf );
		m++;
	}
	UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,m*INNER_FONT_W,INNER_FONT_H,0);
}

void clear_string( int x, int y, int len )
{
	int i;
	char chrdat[INNER_FNT_SIZE];
	for( i = 0; i < len; i++ )
	{
		memset( chrdat, 0, INNER_FNT_SIZE );
		WriteOneOsdLayer( OSD_LAYER_STRING, x+i*INNER_FONT_W, y, INNER_FONT_W, INNER_FONT_H, (char*)font_buf );
	}
	UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,len*INNER_FONT_W,INNER_FONT_H,1);
}
	
/*******************************************************************************************
 * @fn: 	draw_bitmap
 *
 * @brief:	��osd����ʾһ��bitmap���ݿ�
 *
 * @param:  layer_id	- ��ʾ�Ĳ�
 * @param:  x 		- x��ʼ����
 * @param:  y 		- y��ʼ����
 * @param:  sx	 	- ���� - ��ʾ�������Ŀ���
 * @param:  sy	 	- �߶� - ��ʾ�������ĸ߶�
 * @param:  fgcolor	- ǰ��ɫ
 * @param:  bgcolor	- ����ɫ
 * @param:  pbitmap	- λͼ����ָ��
 * @param:  width		- λͼ�������ֽڸ���
 *
 * @return: none
 *******************************************************************************************/
void draw_bitmap( int layer_id, int x, int y, int sx, int sy, unsigned int fgcolor, unsigned int bgcolor, char* pbitmap, int width )
{
	int i,j,k,l;
	char dat;

	char* prgba_fg = (char*)&fgcolor;
	char* prgba_bg = (char*)&bgcolor;
	
	for( i = 0; i < sy; i++ )
	{
		l = 0;
		for( j = 0; j < width; j++ )
		{
			dat = pbitmap[i*width + j];
			for( k = 0; k < 8; k++ )
			{	
				#ifdef COLOR_FORMAT_RGB565
				short *pfont16=&font_buf[(i*sx+l)*FNT_BPP_LEN];
				if( dat&0x80 )
				{
					*pfont16=fgcolor;
				}
				else
				{
					*pfont16=bgcolor;
				}
				#else
				if( dat&0x80 )
				{
					memcpy(font_buf+(i*sx+l)*FNT_BPP_LEN,prgba_fg,FNT_BPP_LEN);
				}
				else
				{
					memcpy(font_buf+(i*sx+l)*FNT_BPP_LEN,prgba_bg,FNT_BPP_LEN);
				}
				#endif
				dat <<= 1;
				if( ++l >= sx ) break;
			}
			if( l >= sx ) break;
		}
	}
	WriteOneOsdLayer( layer_id, x,y,sx,sy,(char*)font_buf );
}

// lzh_20180804_s
/*******************************************************************************************
 * @fn: 	draw_dot
 *
 * @brief:	��osd����ʾһ����
 *
 * @param:  layer_id	- ��ʾ�Ĳ�
 * @param:  dotx 		- x��ʼ����
 * @param:  doty 		- y��ʼ����
 * @param:  dotw	 	- ���� - ��ʾ�������Ŀ���
 * @param:  doth	 	- �߶� - ��ʾ�������ĸ߶�
 * @param:  color		- ��ɫ
 *
 * @return: none
 *******************************************************************************************/
void draw_dot( int layer_id, int dotx, int doty, int dotw, int doth, unsigned int color )
{
	int i,j;
	unsigned char r,g,b;

	char* prgba_fg = (char*)&color;
	
	for(i=0;i<doth;i++)
	{
	    for(j=0;j<dotw;j++)
	    {
			memcpy(font_buf+(i*dotw+j)*FNT_BPP_LEN,prgba_fg,FNT_BPP_LEN);
	    }
	}
	WriteOneOsdLayer( layer_id, dotx,doty,dotw,doth,(char*)font_buf );	
}

/*******************************************************************************************
 * @fn: 	draw_scaler_bitmap
 *
 * @brief:	��osd����ʾһ��bitmap���ݿ�
 *
 * @param:  layer_id	- ��ʾ�Ĳ�
 * @param:  x 		- x��ʼ����
 * @param:  y 		- y��ʼ����
 * @param:  sx	 	- ���� - ��ʾ�������Ŀ���
 * @param:  sy	 	- �߶� - ��ʾ�������ĸ߶�
 * @param:  fgcolor	- ǰ��ɫ
 * @param:  bgcolor	- ����ɫ
 * @param:  pbitmap	- λͼ����ָ�� - ÿ���ֽ�Ϊһ������
 * @param:  bitmast	- �����ֽڵ���Ч���� 1/fgcolor��0/bgcolor
 * @param:  dotw - 	  ÿ��������ʾ�Ŀ���
 * @param:  doth - 	  ÿ��������ʾ�ĸ߶�
 *
 * @return: none
 *******************************************************************************************/
void draw_scaler_bitmap( int layer_id, int x, int y, int sx, int sy, unsigned int fgcolor, unsigned int bgcolor, char* pbitmap, unsigned char bitmast, int dotw, int doth )
{
	int i,j,dispx,dispy;

	for(i=0;i<sy;i++)
	{
		dispy = y+i*doth;			
	    for(j=0;j<sx;j++)
	    {
	    	dispx = x+j*dotw;
	        if(pbitmap[i*sx+j]&bitmast)
	        {
				draw_dot( layer_id, dispx, dispy, dotw, doth, fgcolor );
	        }
	        else
	        {
				draw_dot( layer_id, dispx, dispy, dotw, doth, bgcolor );
	        }
	    }
	}	
}
// lzh_20180804_e

/*******************************************************************************************
 * @fn: 	draw_unicode
 *
 * @brief:	��osd����ʾһ��unicode�ַ���
 *
 * @param:  x 			- x��ʼ����
 * @param:  y 			- y��ʼ����
 * @param:  fgcolor		- ǰ��ɫ
 * @param:  bgcolor		- ����ɫ
 * @param:  textstring	- �ַ���ָ��
 * @param:  len			- �ַ�������
 * @param:  pwidth			- �ַ�����ʾ�Ŀ���
 * @param:  pheight		- �ַ�����ʾ�ĸ߶�
 *
 * @return: ����ʾ���ַ�����
 *******************************************************************************************/
int draw_unicode( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int* pwidth, int* pheight )	//czn_20190412
{ 
	int i;
	int pen_x, pen_y,chr_w,str_w;
    int error; 

	int 		ascender,descender;
    FT_UInt 	glyph_index; 
    FT_ULong 	ul_char; 
		
	pen_x = x;
	pen_y = y;

	ascender	= LOGVIEW_FONT_face->size->metrics.ascender >> 6;
	descender	= LOGVIEW_FONT_face->size->metrics.descender>>6;
	// �õ��ַ����ĸ߶�
	if( pheight != NULL ) 
	{
		*pheight	= ascender - descender;
		//printf( "ext string height=%d\n",*pheight);
	}

	str_w = 0;
	
    for( i = 0; i < len; i += 2 ) 
    { 
		ul_char = (textstring[i+1]<<8) | textstring[i]; 

		// �����ַ�����
		glyph_index = FT_Get_Char_Index( LOGVIEW_FONT_face, ul_char );

		// װ������ͼ�����β�
		error = FT_Load_Glyph(LOGVIEW_FONT_face, glyph_index, FT_LOAD_DEFAULT);	// FT_LOAD_NO_SCALE); //
		if( error ) 
		{
			printf( "FT_Load_Glyph():%d\n ", error ); 
		}

		// ת��Ϊλͼ���� 
		if (LOGVIEW_FONT_face->glyph->format != FT_GLYPH_FORMAT_BITMAP)
		{
			error = FT_Render_Glyph(LOGVIEW_FONT_face->glyph, FT_RENDER_MODE_MONO); 
			if( error ) 
			{
				//printf( "FT_Render_Glyph():%d\n ", error ); 
				return 0;
			}
		}		
		/*��ʾ�ַ���bitmap*/ 
		// face->glyph->bitmap_top: ����ԭ�㵽��Ч���ص���ߵ���룬1����Ϊ��λ
		// face->size->metrics.ascenderΪ����ԭ�㵽�ַ��Ķ�����룬1/64����Ϊ��λ
		// face->size->metrics.descenderΪ����ԭ�㵽�ַ��ĵ׶˾��룬������1/64����Ϊ��λ
		// face->glyph->advance.x��ʾ��Ч�ַ��Ŀ��ȣ����ַ�x������1/64����Ϊ��λ
		// face->glyph->bitmap.pitchˮƽ���ص��ֽ���
		#if 0
		ascender = face->size->metrics.ascender >> 6;
		//printf( "width=%d,rows=%d,pitch=%d,bitmap_left=%d,bitmap_top=%d,ascender=%d\n",face->glyph->bitmap.width,face->glyph->bitmap.rows,face->glyph->bitmap.pitch,face->glyph->bitmap_left,face->glyph->bitmap_top,ascender);
		draw_bitmap( layer_id, pen_x + face->glyph->bitmap_left, pen_y+ascender-face->glyph->bitmap_top, face->glyph->bitmap.pitch*8, face->glyph->bitmap.rows, fgcolor, bgcolor, (char*)face->glyph->bitmap.buffer ); 
		pen_x += face->glyph->advance.x >> 6; 
		pen_x += 1;
		#else
		
		chr_w = LOGVIEW_FONT_face->glyph->advance.x >> 6;
		
		//draw_bitmap( layer_id, pen_x + face->glyph->bitmap_left, pen_y+ascender-face->glyph->bitmap_top, face->glyph->bitmap.pitch*8, face->glyph->bitmap.rows, fgcolor, bgcolor, (char*)face->glyph->bitmap.buffer ); 
		draw_bitmap( layer_id, pen_x + LOGVIEW_FONT_face->glyph->bitmap_left, pen_y+ascender-LOGVIEW_FONT_face->glyph->bitmap_top, LOGVIEW_FONT_face->glyph->bitmap.width, LOGVIEW_FONT_face->glyph->bitmap.rows, fgcolor, bgcolor, (char*)LOGVIEW_FONT_face->glyph->bitmap.buffer, LOGVIEW_FONT_face->glyph->bitmap.pitch );
		
		pen_x += chr_w; 
		str_w += chr_w;
			
		#endif
		
    } 

	if( pwidth != NULL )
	{
		*pwidth = str_w;
		//printf( "ext string width=%d\n",*pwidth);
	}
	return i;
}

typedef struct 
{
	int		chr_w;			// �ַ����ʹ�õĿ���
	int		bitmap_left;	// ����ԭ�㵽��Ч���ص�����߾���
	int		bitmap_top;		// ����ԭ�㵽��Ч���ص���ߵ����
	int		bmp_width;		// �ַ���ʾ��Ч����
	int		bmp_rows;		// �ַ���ʾ��Ч�߶�
	int		bmp_pitch;		// �ַ��������ֽ���
	char	bmp_dat[(EXTEND_FONT_H_MAX+2)*(EXTEND_FONT_W_MAX/8+1)];		// �ַ�������
} one_face_glyph;

typedef struct
{
	int				chr_cnt;		// һ���ַ��ĸ���
	int				row_width;		// һ�еĿ���
	int				row_height;		// һ�еĸ߶�
	int				ascender;		// ����ԭ�㵽�ַ��Ķ������ - ����
	int				descender;		// ����ԭ�㵽�ַ��ĵ׶˾��� - ����
	one_face_glyph 	row_glyph_buf[MAX_STR_CONVERT_LEN];	
} one_row_glyph;

static one_row_glyph global_unicode_row1;
static one_row_glyph global_unicode_row2;
static one_row_glyph global_unicode_row3;
static one_row_glyph global_unicode_row4;

int fill_one_unicode_row_buffer( char* textstring, int len, FT_Face myface, one_row_glyph* punicode_row, one_row_glyph* punicode_row2 )
{
	int 		error; 
    FT_UInt 	glyph_index; 
    FT_ULong 	ul_char; 

	int i = 0;
	// ��ʾ��һ��
	if( punicode_row != NULL )
	{
		punicode_row->ascender		= myface->size->metrics.ascender>>6;
		punicode_row->descender		= myface->size->metrics.descender>>6;
		punicode_row->row_height	= punicode_row->ascender - punicode_row->descender;

		punicode_row->chr_cnt 		= 0;
		punicode_row->row_width		= 0;
			
	    for( i = 0; i < len; i += 2 ) 
	    { 
			ul_char = (textstring[i+1]<<8) | textstring[i];

			if( punicode_row2 != NULL && ul_char == 0x0020 )	// ' '��Ϊ���з�
			{
				i += 2;	// �����ո�
				break;
			}
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );

			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}

			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return -1;
				}
			}		
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w 		= myface->glyph->advance.x>>6;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_left 	= myface->glyph->bitmap_left;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_top	= myface->glyph->bitmap_top;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_width	= myface->glyph->bitmap.width;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_rows		= myface->glyph->bitmap.rows;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
			memcpy( punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );

			punicode_row->row_width += punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w;

			punicode_row->chr_cnt++;
	    }
	}
	
	if( punicode_row2 != NULL )
	{
		punicode_row2->ascender		= myface->size->metrics.ascender>>6;
		punicode_row2->descender 	= myface->size->metrics.descender>>6;
		punicode_row2->row_height	= punicode_row2->ascender - punicode_row2->descender;
		
		punicode_row2->chr_cnt		= 0;
		punicode_row2->row_width 	= 0;
	
		for( ; i < len; i += 2 ) 
		{ 
			ul_char = (textstring[i+1]<<8) | textstring[i];
		
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );
		
			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}
		
			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return 0;
				}
			}		
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].chr_w		= myface->glyph->advance.x>>6;
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bitmap_left= myface->glyph->bitmap_left;
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bitmap_top	= myface->glyph->bitmap_top;
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bmp_width	= myface->glyph->bitmap.width;
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bmp_rows 	= myface->glyph->bitmap.rows;
			punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
			memcpy( punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );
		
			punicode_row2->row_width += punicode_row2->row_glyph_buf[punicode_row2->chr_cnt].chr_w;
		
			punicode_row2->chr_cnt++;
		} 
	
	}	
	return 0;
}

int fill_one_unicode_row_buffer2( char* textstring, int len, FT_Face myface, one_row_glyph** punicode_row, int max_width, int max_height)
{
	int 		error; 
    FT_UInt 	glyph_index; 
    FT_ULong 	ul_char; 
	int line;
	int i = 0;
	one_row_glyph saveRow;
	int saveI;
	int blankSpaceFlag;
	int height;
	int maxLine;
	
	
	height = (myface->size->metrics.ascender>>6) - (myface->size->metrics.descender>>6);
	maxLine = max_height/height;
	
	for( i = 0, line = 0; punicode_row[line] != NULL && line < maxLine; line++)
	{
		punicode_row[line]->ascender		= myface->size->metrics.ascender>>6;
		punicode_row[line]->descender		= myface->size->metrics.descender>>6;
		punicode_row[line]->row_height	= punicode_row[line]->ascender - punicode_row[line]->descender;

		punicode_row[line]->chr_cnt 		= 0;
		punicode_row[line]->row_width		= 0;
		
			
		// ��ʾһ��
	    for(blankSpaceFlag = 0; i < len; i += 2 ) 
	    { 
			ul_char = (textstring[i+1]<<8) | textstring[i];
			
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );

			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}

			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return 0;
				}
			}		

			if(ul_char == 0x0020 )	// ' '��Ϊ���з�
			{
				saveRow = *punicode_row[line];
				saveI = i;
				blankSpaceFlag = 1;
			}

			if(punicode_row[line]->row_width + (myface->glyph->advance.x>>6) > max_width)
			{
				if(blankSpaceFlag)
				{
					i = saveI+2;	//����ȥ���ո�
					*punicode_row[line] = saveRow;
				}
				break;
			}
			else
			{
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].chr_w		= myface->glyph->advance.x>>6;
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bitmap_left	= myface->glyph->bitmap_left;
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bitmap_top	= myface->glyph->bitmap_top;
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bmp_width	= myface->glyph->bitmap.width;
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bmp_rows 	= myface->glyph->bitmap.rows;
				punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
				memcpy( punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );
				
				punicode_row[line]->row_width += punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt].chr_w;
				punicode_row[line]->chr_cnt++;
			}
	    }
		
		if(i >= len)
		{
			return line+1;
		}
	}
	

	//�ַ�̫������ʾ���£�����"..."�滻���������ַ�
	if(i < len && punicode_row[line]->chr_cnt >= 3)
	{
		int j;
		
		for(j = 0; j < 3; j++) 
		{ 
			ul_char = '.';
		
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );
		
			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}
		
			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return 0;
				}
			}		
		
			punicode_row[line]->row_width -= punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].chr_w;
			
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].chr_w		= myface->glyph->advance.x>>6;
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bitmap_left	= myface->glyph->bitmap_left;
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bitmap_top	= myface->glyph->bitmap_top;
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bmp_width	= myface->glyph->bitmap.width;
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bmp_rows 	= myface->glyph->bitmap.rows;
			punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bmp_pitch	= myface->glyph->bitmap.pitch;
			memcpy( punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );

			punicode_row[line]->row_width += punicode_row[line]->row_glyph_buf[punicode_row[line]->chr_cnt-j-1].chr_w;

		} 
	}

	return line+1;
}

int get_one_unicode_string_size( char* textstring, int len, int fnt_type, one_row_glyph* punicode_row, int width)
{
	int 		error; 
    FT_UInt 	glyph_index; 
    FT_ULong 	ul_char; 

	int i, j;
	FT_Face myface;
	
	int pointW;

	if( fnt_type == 1 )
	{
		myface = face2;
	}
	// lzh_20181221_s
	else if( fnt_type == 2 )
	{
		myface = face3;
	}
	// lzh_20181221_e
	else
	{
		myface = face;
	}

	if( punicode_row != NULL )
	{
		punicode_row->ascender		= myface->size->metrics.ascender>>6;
		punicode_row->descender 	= myface->size->metrics.descender>>6;
		punicode_row->row_height	= punicode_row->ascender - punicode_row->descender;

		punicode_row->chr_cnt		= 0;
		punicode_row->row_width 	= 0;

		//��������ַ���ʾ���ȣ����㡰...���Ŀ���
		if(width)
		{
			ul_char = '.';
		
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );
		
			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}
		
			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return -1;
				}
			}		
			pointW = (myface->glyph->advance.x>>6) * 3;
			if(width < pointW)
			{
				printf( "The display range is too small!!! width = %d, \"...\"=%d\n ", width, pointW); 
				return -1;
			}
		}
		
		for( i = 0; i < len; i += 2 )
		{ 
			ul_char = (textstring[i+1]<<8) | textstring[i];

			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );

			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}

			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return -1;
				}
			}		
			
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w		= myface->glyph->advance.x>>6;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_left	= myface->glyph->bitmap_left;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_top	= myface->glyph->bitmap_top;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_width	= myface->glyph->bitmap.width;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_rows 	= myface->glyph->bitmap.rows;
			punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
			memcpy( punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );

			punicode_row->row_width += punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w;
			punicode_row->chr_cnt++;

			if(width)
			{
				//��ʾ�ַ�����
				if(punicode_row->row_width > width)
				{
					//ȥ���������ֲ���Ԥ��λ�����ӡ�...��
					for( ; punicode_row->row_width + pointW > width && punicode_row->chr_cnt > 0; )
					{
						punicode_row->row_width -= punicode_row->row_glyph_buf[--punicode_row->chr_cnt].chr_w;
					}
					
					ul_char = '.';
					
					// �����ַ�����
					glyph_index = FT_Get_Char_Index( myface, ul_char );
					
					// װ������ͼ�����β�
					error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
					if( error ) 
					{
						printf( "FT_Load_Glyph():%d\n ", error ); 
					}
					
					// ת��Ϊλͼ���� 
					if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
					{
						error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
						if( error ) 
						{
							//printf( "FT_Render_Glyph():%d\n ", error ); 
							return -1;
						}
					}		
					
					//װ�ء�...���ַ�
					for(j = 0; j < 3; j++) 
					{ 
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w		= myface->glyph->advance.x>>6;
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_left	= myface->glyph->bitmap_left;
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].bitmap_top	= myface->glyph->bitmap_top;
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_width	= myface->glyph->bitmap.width;
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_rows 	= myface->glyph->bitmap.rows;
						punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
						memcpy( punicode_row->row_glyph_buf[punicode_row->chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );
						punicode_row->row_width += punicode_row->row_glyph_buf[punicode_row->chr_cnt].chr_w;
						punicode_row->chr_cnt++;
					}
					break;
				}
			}
		}
		return 0;
	}
	else
		return -1;		
}
	

int draw_one_unicode_row_buffer( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, one_row_glyph* punicode_row )
{ 
	int i,pen_x, pen_y;
	int ascender,descender,x_offset,y_offset;
		
	pen_x = x;
	pen_y = y;

	ascender	= punicode_row->ascender;
	descender	= punicode_row->descender;
	
    for( i = 0; i < punicode_row->chr_cnt; i++ ) 
    { 
    	x_offset = punicode_row->row_glyph_buf[i].bitmap_left;
		y_offset = ascender-punicode_row->row_glyph_buf[i].bitmap_top;
		draw_bitmap( layer_id, pen_x + x_offset, pen_y+y_offset, punicode_row->row_glyph_buf[i].bmp_width, punicode_row->row_glyph_buf[i].bmp_rows, fgcolor, bgcolor, (char*)punicode_row->row_glyph_buf[i].bmp_dat, punicode_row->row_glyph_buf[i].bmp_pitch);
		pen_x += punicode_row->row_glyph_buf[i].chr_w; 
    } 

	return i;
} 

int display_unicode_string_align_center( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int max_width, int max_height, int fnt_type )
{
	int x_offset;

	FT_Face target_face;

	if( fnt_type == 1 )
		target_face = face2;
	// lzh_20181221_s
	else if( fnt_type == 2 )
		target_face = face3;
	// lzh_20181221_e	
	else
		target_face = face;
	
	// �Ȳ���һ���Ƿ��ܹ���ʾ���
	fill_one_unicode_row_buffer( textstring, len, target_face, &global_unicode_row1, NULL );
	// ������ʾ��ʼλ��
	if( global_unicode_row1.row_width <= max_width )
	{
		x_offset = ((max_width - global_unicode_row1.row_width)/2);
		draw_one_unicode_row_buffer( layer_id, x+x_offset, y, fgcolor, bgcolor, &global_unicode_row1);
	}
	else
	{
		// ������ʾ
		fill_one_unicode_row_buffer( textstring, len, target_face, &global_unicode_row1, &global_unicode_row2 );
		x_offset = (max_width - global_unicode_row1.row_width)/2;
		draw_one_unicode_row_buffer( layer_id, x+x_offset, y, fgcolor, bgcolor, &global_unicode_row1);
		x_offset = (max_width - global_unicode_row2.row_width)/2;
		draw_one_unicode_row_buffer( layer_id, x+x_offset, y+global_unicode_row1.row_height, fgcolor, bgcolor, &global_unicode_row2);			
	}
	return 0;
}

//���֧��4����ʾ
int display_unicode_string_align_center2( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int max_width, int max_height, int fnt_type )
{
	int x_offset;
	int i, line;
	
	one_row_glyph* pUnicodeRow[5] = {&global_unicode_row1, &global_unicode_row2, &global_unicode_row3, &global_unicode_row4, NULL};

	FT_Face target_face;

	if(len == 0)
	{
		return -1;
	}
	
	if( fnt_type == 1 )
		target_face = face2;
	// lzh_20181221_s
	else if( fnt_type == 2 )
		target_face = face3;
	// lzh_20181221_e	
	else
		target_face = face;
	
	// �Ȳ���һ���Ƿ��ܹ���ʾ���
	line = fill_one_unicode_row_buffer2(textstring, len, target_face, pUnicodeRow, max_width, max_height);
	// ������ʾ��ʼλ��
	y += (max_height - pUnicodeRow[0]->row_height*line)/2;
	for(i = 0; i < line; i++)
	{
		x_offset = (max_width - pUnicodeRow[i]->row_width)/2;
		if(i > 0)
		{
			y += pUnicodeRow[i-1]->row_height;
		}
		draw_one_unicode_row_buffer( layer_id, x+x_offset, y, fgcolor, bgcolor, pUnicodeRow[i]);
	}
	return 0;
}

int display_unicode_string_align_left( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int fnt_type, int width )
{
	FT_Face myface;
    FT_ULong ul_char; 
    FT_UInt glyph_index; 
	int i;
	int error; 
	int fnt_w;
	
	if( fnt_type == 1 )
	{
		fnt_w = ext_font2_w-6;
		myface = face2;
	}
	// lzh_20181221_s	
	else if( fnt_type == 2 )
	{
		fnt_w = ext_font3_w-6;
		myface = face3;
	}
	// lzh_20181221_e
	else
	{
		fnt_w = ext_font1_w-5;
		myface = face;
	}

	global_unicode_row2.ascender 	= myface->size->metrics.ascender>>6;
	global_unicode_row2.descender	= myface->size->metrics.descender>>6;
	global_unicode_row2.row_height	= global_unicode_row2.ascender - global_unicode_row2.descender;
	
	global_unicode_row2.chr_cnt		= 0;
	global_unicode_row2.row_width	= 0;
	
	for(i = 0; i < len; i += 2 ) 
	{ 
		ul_char = (textstring[i+1]<<8) | textstring[i];
	
		// �����ַ�����
		glyph_index = FT_Get_Char_Index( myface, ul_char );
	
		// װ������ͼ�����β�
		error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
		if( error ) 
		{
			printf( "FT_Load_Glyph():%d\n ", error ); 
		}
	
		// ת��Ϊλͼ���� 
		if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
		{
			error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
			if( error ) 
			{
				//printf( "FT_Render_Glyph():%d\n ", error ); 
				return 0;
			}
		}		
	
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].chr_w		= myface->glyph->advance.x>>6;
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bitmap_left	= myface->glyph->bitmap_left;
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bitmap_top 	= myface->glyph->bitmap_top;
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_width	= myface->glyph->bitmap.width;
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_rows		= myface->glyph->bitmap.rows;
		global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
		memcpy( global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );
		global_unicode_row2.row_width += global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].chr_w;
		global_unicode_row2.chr_cnt++;

		if(global_unicode_row2.row_width+fnt_w*2 > width && (len/2) > global_unicode_row2.chr_cnt+2)
		{
			global_unicode_row2.row_width -= global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].chr_w;
			global_unicode_row2.chr_cnt--;
			break;
		}
	} 

	if(i < len)
	{
		for(i = 0; i < 6; i += 2 ) 
		{ 
			ul_char = '.';
		
			// �����ַ�����
			glyph_index = FT_Get_Char_Index( myface, ul_char );
		
			// װ������ͼ�����β�
			error = FT_Load_Glyph(myface, glyph_index, FT_LOAD_DEFAULT);	
			if( error ) 
			{
				printf( "FT_Load_Glyph():%d\n ", error ); 
			}
		
			// ת��Ϊλͼ���� 
			if (myface->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				error = FT_Render_Glyph(myface->glyph, FT_RENDER_MODE_MONO); 
				if( error ) 
				{
					//printf( "FT_Render_Glyph():%d\n ", error ); 
					return 0;
				}
			}		
		
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].chr_w		= myface->glyph->advance.x>>6;
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bitmap_left	= myface->glyph->bitmap_left;
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bitmap_top 	= myface->glyph->bitmap_top;
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_width	= myface->glyph->bitmap.width;
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_rows		= myface->glyph->bitmap.rows;
			global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_pitch	= myface->glyph->bitmap.pitch;
			memcpy( global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].bmp_dat, (char*)myface->glyph->bitmap.buffer, myface->glyph->bitmap.pitch*myface->glyph->bitmap.rows );
			global_unicode_row2.row_width += global_unicode_row2.row_glyph_buf[global_unicode_row2.chr_cnt].chr_w;
			global_unicode_row2.chr_cnt++;
		} 
		global_unicode_row2.row_width += (fnt_w*3); 		
	}

	draw_one_unicode_row_buffer( layer_id, x, y, fgcolor, bgcolor, &global_unicode_row2);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int utf82unicode( char* pin, int in_len, short* pout )
{
	int i,out_len;
	char type[3],utf8[3];
	short wchar_code;

	out_len = 0;
		
	for( i = 0; i < in_len; i++ )
	{		
		utf8[0] = pin[i];
		utf8[1] = pin[i+1];
		utf8[2] = pin[i+2];
		type[0] = utf8[0]&0x80;
		type[1] = utf8[0]&0xe0;
		type[2] = utf8[0]&0xf0;
		if( type[0] == 0x00 )		// 1 byte
		{
			wchar_code = utf8[0];
		}
		else if( type[1] == 0xc0 )	// 2 bytes
		{
			wchar_code = utf8[0]&(~0xe0);
			wchar_code <<= 6;
			wchar_code |= (utf8[1]&(~0xc0));
			i += 1;
		}
		else if( type[2] == 0xe0 )	// 3 bytes
		{
			wchar_code = (utf8[0]&(~0xf0));
			wchar_code <<= 6;
			wchar_code += (utf8[1]&(~0xc0));
			wchar_code <<= 6;
			wchar_code |= (utf8[2]&(~0xc0));
			i += 2;
		}
		else
		{
			return -1;
		}
		pout[out_len++] = wchar_code;
		//dh_20190809_s
		if(out_len >= UNICODE_STRING_MAX_LEN)
			break;
		//dh_20190809_e
	}
	return out_len;
}
int unicode2utf8(short *pin,int in_len,char *pout)
{
	int i,j;
	for(i = 0,j=0;i<in_len;i++)
	{
		if ( pin[i] <= 0x0000007F )
		{
		    // * U-00000000 - U-0000007F:  0xxxxxxx
		    pout[j]     = (pin[i] & 0x7F);
		    j+=1;
		}
		else if ( pin[i] >= 0x00000080 && pin[i] <= 0x000007FF )
		{
		    // * U-00000080 - U-000007FF:  110xxxxx 10xxxxxx
		    pout[j+1]= (pin[i] & 0x3F) | 0x80;
		    pout[j]     = ((pin[i] >> 6) & 0x1F) | 0xC0;
		    j+=2;
		}
		else if ( pin[i] >= 0x00000800 && pin[i] <= 0x0000FFFF )
		{
		    // * U-00000800 - U-0000FFFF:  1110xxxx 10xxxxxx 10xxxxxx
		    pout[j+2]= (pin[i] & 0x3F) | 0x80;
		    pout[j+1] = ((pin[i] >>  6) & 0x3F) | 0x80;
		    pout[j]     = ((pin[i] >> 12) & 0x0F) | 0xE0;
		    j+=3;
		}
	}
	pout[j]=0;
	return j;
}
int iconv2unicode( int format, char* pin, short* pout )
{
	iconv_t h;
	
	char	utf8_buf[250];
	int		utf8_len=250;
	
	int		unicode_len;
	
	char* pin_tmp 	= pin;
	char* pout_tmp 	= utf8_buf;
	
	size_t  size_in_tmp	= strlen(pin);
	size_t  size_out_tmp	= utf8_len;
	
	/*  iconv_t iconv_open(const char *tocode, const char *fromcode);
	˵������Ҫ���������ֱ����ת��
	������tocode 	  Ŀ�����
	������fromcode  ԭ����
	���أ�һ��ת����� ��iconv��iconv_close����
	*/
	if( format == STR_GB2312 )
		h = iconv_open( "utf-8","gb2312");
	else if(  format == STR_GBK )
		h = iconv_open( "utf-8","gbk");
	else if(  format == STR_BIG5 )
		h = iconv_open( "utf-8","big5");
	else if(  format == STR_UNICODE )
		return 0;
	else
		return -1;
	if( h == 0 )
	{			
		printf("iconv open error\n");
		return -1;
	}
	//size_t iconv(iconv_t cd,char **inbuf,size_t *inbytesleft,char **outbuf,size_t *outbytesleft);
	//�˺�����inbuf�ж�ȡ�ַ�,ת���������outbuf��,inbytesleft���Լ�¼��δת�����ַ���,outbytesleft���Լ�¼��������ʣ��ռ䡣
	//iconv�������޸Ĳ���in�Ͳ���outָ����ָ��ĵط���Ҳ����˵���ڵ���iconv����֮ǰ�����ǵ�in��inbufָ���Լ�out��outbufָ��ָ�����ͬһ���ڴ����򣬵��ǵ���֮��outָ����ָ��ĵط��Ͳ���outbuf�ˣ�ͬ��inָ�롣����Ҫ
	//char*in=inbuf;char*out=outbuf;����һ�£�ʹ�û����ͷ��ڴ��ʱ��ҲҪʹ��ԭ�ȵ��Ǹ�ָ��outbuf��inbuf
	if( iconv(h, &pin_tmp, &size_in_tmp, &pout_tmp, &size_out_tmp) == -1 )
	{
		printf("iconv convert error\n");
		iconv_close(h);
		return -1;		
	}
	utf8_len = utf8_len-size_out_tmp;
	utf8_buf[utf8_len] = 0;
	
	#if 0	
	printf("%s:\n",utf8_buf);
	for( i = 0; i < utf8_len; i++ )
	{
		printf("%02x,",utf8_buf[i]);
	}
	printf("\n");
	#endif

	iconv_close(h);

	// utf-8 convert unicode
	//setlocale(LC_ALL,"");
	//size_t mbstowcs(wchar_t *wcstr,const char *mbstr,size_t count);
	//unicode_len = mbstowcs(NULL,utf8_buf,0)+1;		// �õ���Ҫת���ĳ���
	//unicode_len = mbstowcs(pout,utf8_buf,utf8_len<<1);	// �õ��Ѿ�ת���ĳ���

	unicode_len = utf82unicode(utf8_buf,utf8_len,pout);
	
	if( unicode_len <= 0 )
	{
		printf("mbstowcs convert error\n");
		return -1;
	}	
#if 0	
	printf("%s:\n",pout);
	for( i = 0; i < unicode_len; i++ )
	{
		printf("%02x,",pout[i]);
	}
	printf("\n");
#endif	
	return unicode_len;
}  

/*******************************************************************************************
 * @fn: 	display_ext_string
 *
 * @brief:	��osd����ʾһ�������ʽ�ַ���
 *
 * @param:  x 			- x��ʼ����
 * @param:  y 			- y��ʼ����
 * @param:  fgcolor		- ǰ��ɫ
 * @param:  bgcolor		- ����ɫ
 * @param:  string		- �ַ���ָ��
 * @param:  format		- �ַ�����ʽ: 0/unicode, 1/GB2312, 2/GBK, 3/BIG5, 4/utf8
 *
 * @return: none
 *******************************************************************************************/
void display_ext_string( int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string, int str_len, int fnt_type, int format, int width)
{
	short unicode_buf[UNICODE_STRING_MAX_LEN];//MAX_STR_CONVERT_LEN //dh_20190809
	int unicode_len;
	int unicode_len2;

	if( format == STR_UNICODE )
	{
		unicode_len = str_len/2;
		memcpy( unicode_buf, string, str_len );
	}
	else if( format == STR_UTF8 )
		unicode_len = utf82unicode(string,str_len,unicode_buf);
	else
		unicode_len = iconv2unicode(format,string,unicode_buf);
	if(get_cur_unicode_invert_flag())		//FOR_HEBREW
	{
		unicode_len2=unicode_len*2;
		Arbic_Position_Process(unicode_buf,unicode_len2,unicode_buf,&unicode_len2);
		Unicode_Invert(unicode_buf,unicode_len2);
		unicode_len=unicode_len2/2;
	}
	
			
	if( unicode_len >= 0 )
	{		
		int clear_width,clear_height;
		get_one_unicode_string_size( (char*)unicode_buf, unicode_len*2, fnt_type, &global_unicode_row1, width);
		
		clear_width 	= global_unicode_row1.row_width;
		clear_height	= global_unicode_row1.row_height;	
		SetOneOsdLayerRectColor( OSD_LAYER_STRING, x, y, clear_width, clear_height, bgcolor);
		draw_one_unicode_row_buffer( OSD_LAYER_STRING,x, y, fgcolor, bgcolor, &global_unicode_row1);				
		UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,clear_width,clear_height,0);
	}
}

void display_unicode_str( int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* unicode_buf, int buf_len, int fnt_type, int width )
{
	int clear_width,clear_height;

	get_one_unicode_string_size( (char*)unicode_buf, buf_len, fnt_type, &global_unicode_row1, width );
	
	clear_width 	= global_unicode_row1.row_width;
	clear_height	= global_unicode_row1.row_height;
	SetOneOsdLayerRectColor( OSD_LAYER_STRING, x, y, clear_width, clear_height, bgcolor);
	draw_one_unicode_row_buffer( OSD_LAYER_STRING,x, y, fgcolor, bgcolor, &global_unicode_row1);
	
	UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,clear_width,clear_height,0);
}
/*******************************************************************************************
 * @fn:		GetStrDispXY
 *
 * @brief:	���ݶ��뷽ʽ��ȡ��ʼ�����
 *
 * @param:  nx, ny ��ȡ����ʼ�����
 *
 * @return: 0
 *******************************************************************************************/
static void GetStrDispXY(int dispType, int ox, int oy, int w, int h, int strw, int strh, int* nx, int* ny)
{
	int differw, differh;
	
	differw = w-strw;
	differh = h-strh;
	
	switch(dispType)
	{
		case ALIGN_LEFT_DOWN:
			*nx = ox;
			*ny = oy+differh;
			break;
		case ALIGN_LEFT_UP:
			*nx = ox;
			*ny = oy;
			break;
		case ALIGN_LEFT:
			*nx = ox;
			*ny = oy+differh/2;
			break;
		case ALIGN_RIGHT:
			*nx = ox+differw;
			*ny = oy+differh/2;
			break;
		case ALIGN_RIGHT_DOWN:
			*nx = ox+differw;
			*ny = oy+differh;
			break;
		case ALIGN_RIGHT_UP:
			*nx = ox+differw;
			*ny = oy;
			break;
		case ALIGN_UP:
			*nx = ox+differw/2;
			*ny = oy;
			break;
		case ALIGN_DOWN:
			*nx = ox+differw/2;
			*ny = oy+differh;
			break;
		case ALIGN_MIDDLE:
			*nx = ox+differw/2;
			*ny = oy+differh/2;
			break;
			
		default:
			*nx = ox;
			*ny = oy;
			break;
	}

	//dprintf("%d[%d,%d,%d,%d][%d,%d,%d,%d]\n", dispType, ox, oy, w, h, strw, strh, *nx, *ny);
}

void display_ext_string_one_page_list(MenuStrMulti_T list)
{
	short unicode_buf[UNICODE_STRING_MAX_LEN];//MAX_STR_CONVERT_LEN //dh_20190809
	int unicode_len, i;
	int updateXstart = 0;
	int updateYstart = 0;
	int updateXend = 0;
	int updateYend = 0;
	int x, y;

	for(i = 0; i < list.dataCnt && i < MAX_STR_DISP_LINE_MAX; i++)
	{
		if(list.data[i].format == STR_UNICODE )
		{
			unicode_len = list.data[i].str_len/2;
			memcpy( unicode_buf, list.data[i].str_dat, list.data[i].str_len );
		}
		else if( list.data[i].format == STR_UTF8 )
			unicode_len = utf82unicode(list.data[i].str_dat,list.data[i].str_len,unicode_buf);
		else
			unicode_len = iconv2unicode(list.data[i].format,list.data[i].str_dat,unicode_buf);
		if(get_cur_unicode_invert_flag())		//FOR_HEBREW
		{
			int unicode_len2;
			unicode_len2=unicode_len*2;
			Arbic_Position_Process(unicode_buf,unicode_len2,unicode_buf,&unicode_len2);
			Unicode_Invert(unicode_buf,unicode_len2);
			unicode_len=unicode_len2/2;
		}		
		if( unicode_len >= 0 )
		{		
			get_one_unicode_string_size( (char*)unicode_buf, unicode_len*2, list.data[i].fnt_type, &global_unicode_row1, list.data[i].width);

			GetStrDispXY(list.data[i].align, list.data[i].x, list.data[i].y, list.data[i].width, list.data[i].height, global_unicode_row1.row_width, global_unicode_row1.row_height, &x, &y);

			draw_one_unicode_row_buffer( OSD_LAYER_STRING, x, y, list.data[i].fg, list.data[i].uBgColor, &global_unicode_row1);				

			updateXstart = (updateXstart < x ? updateXstart : x);
			updateYstart = (updateYstart < y ? updateYstart : y);
			
			updateXend = (updateXend > global_unicode_row1.row_width + x ? updateXend : global_unicode_row1.row_width + x);
			updateYend = (updateYend > global_unicode_row1.row_height + y ? updateYend : global_unicode_row1.row_height + y);
		}
	}
	
	UpdateOsdLayerBuf2(0, updateXstart, updateYstart,updateXend - updateXstart,updateYend - updateYstart, 0);
}

/*******************************************************************************************
 * @fn: 	clear_ext_string
 *
 * @brief:	��osd�����һ�������ʽ�ַ���
 *
 * @param:  x 			- x��ʼ����
 * @param:  y 			- y��ʼ����
 * @param:  fgcolor		- ǰ��ɫ
 * @param:  bgcolor		- ����ɫ
 * @param:  string		- �ַ���ָ��
 *
 * @return: none
 *******************************************************************************************/
void clear_ext_string( int x, int y, int width, int height, int update )
{
	SetOneOsdLayerRectColor( OSD_LAYER_STRING, x, y, width, height, COLOR_KEY);
	if( update )
	UpdateOsdLayerBuf2(OSD_LAYER_STRING, x, y, width, height,1);
}


void display_ext_string2( int x, int y, int layer_id, unsigned int fgcolor, unsigned int bgcolor, char* string, int str_len, int fnt_type, int format, int xsize, int ysize)
{
	short unicode_buf[UNICODE_STRING_MAX_LEN];//MAX_STR_CONVERT_LEN //dh_20190809
	int unicode_len;
	int unicode_len2;
	if( format == STR_UNICODE )
	{
		unicode_len = str_len/2;
		memcpy( unicode_buf, string, str_len );
	}
	else if( format == STR_UTF8 )
		unicode_len = utf82unicode(string,str_len,unicode_buf);
	else
		unicode_len = iconv2unicode(format,string,unicode_buf);
	if(get_cur_unicode_invert_flag())		//FOR_HEBREW
	{
		unicode_len2=unicode_len*2;
		Arbic_Position_Process(unicode_buf,unicode_len2,unicode_buf,&unicode_len2);
		Unicode_Invert(unicode_buf,unicode_len2);
		unicode_len=unicode_len2/2;
	}			
	if( unicode_len >= 0 )
	{		
		SetOneOsdLayerRectColor( layer_id, x, y, xsize, ysize, bgcolor);
		display_unicode_string_align_center2(layer_id, x, y, fgcolor, bgcolor, (char*)unicode_buf, unicode_len*2, xsize, ysize, fnt_type);
		UpdateOsdLayerBuf2(OSD_LAYER_STRING, x,y,xsize,ysize,0);
	}
}

void clear_ext_string2( int x, int y, int layer_id, int width, int height, int update )
{
	SetOneOsdLayerRectColor( layer_id, x, y, width, height, COLOR_KEY);
	if( update )
	UpdateOsdLayerBuf2(OSD_LAYER_STRING, x, y, width, height,1);
}

void display_unicode_str2( int x, int y, int layer_id, unsigned int fgcolor, unsigned int bgcolor, char* unicode_buf, int buf_len, int fnt_type, int xsize, int ysize)
{
	SetOneOsdLayerRectColor( layer_id, x, y, xsize, ysize, bgcolor);
	display_unicode_string_align_center2(layer_id, x, y, fgcolor, bgcolor, (char*)unicode_buf, buf_len, xsize, ysize, fnt_type);
	UpdateOsdLayerBuf2(OSD_LAYER_STRING, x,y,xsize,ysize,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// logview alyer support
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define LOGVIEW_BG_COLOR	COLOR_SLIVER_GRAY
void clear_logview_screen(void)
{
	//SetOneOsdLayerColor(OSD_LAYER_LOG,LOGVIEW_BG_COLOR);
}	

void clear_logview_string( int x, int y, int width )
{
	//SetOneOsdLayerRectColor( OSD_LAYER_LOG, x, y, width, EXTEND_FONT_H, LOGVIEW_BG_COLOR );
}

void display_logview_string( int x, int y, unsigned int fgcolor, char* string, int format )
{
#if 0
	int width,height;
	short unicode_buf[LOG_VIEW_MAX_COL+2];
	int unicode_len = iconv2unicode(STR_GB2312,string,unicode_buf);
	
	if( unicode_len >= 0 )
	{
		// first clear string buffer
		SetOneOsdLayerRectColor( OSD_LAYER_LOG, x, y, unicode_len*8, LOGVIEW_FONT_H+8, LOGVIEW_BG_COLOR );	//czn_20190412
		// then display string
		draw_unicode( OSD_LAYER_LOG, x, y, fgcolor, LOGVIEW_BG_COLOR, (char*)unicode_buf, unicode_len*2, &width, &height );
		UpdateOsdLayerBuf2(OSD_LAYER_LOG,x,y,width,height,0);		
	}
#endif	
}
//czn_20181110_s
void draw_bitmap_re( int layer_id, int x, int y, int sx, int sy, unsigned int fgcolor, unsigned int bgcolor, char* pbitmap, int width )
{
	int i,j,k,l;
	char dat;
	char* prgba_fg = (char*)&fgcolor;
	char* prgba_bg = (char*)&bgcolor;
	for(i = 0;i < (sx+4)*(sy+4);i++)
		memcpy(font_buf+(i)*FNT_BPP_LEN,prgba_bg,FNT_BPP_LEN);
	//font_buf[i] = bgcolor;
	
	for( i = 0; i < sy; i++ )
	{
		l = 0;
		for( j = 0; j < width; j++ )
		{
			dat = pbitmap[i*width + j];
			for( k = 0; k < 8; k++ )
			{
			
				if( dat&0x80 )
				{
					memcpy(font_buf+((i+2)*(sx+4)+l+2)*FNT_BPP_LEN,prgba_fg,FNT_BPP_LEN);
				}
				else
				{
					memcpy(font_buf+((i+2)*(sx+4)+l+2)*FNT_BPP_LEN,prgba_bg,FNT_BPP_LEN);
				}
				dat <<= 1;
				if( ++l >= sx ) break;
			}
			if( l >= sx ) break;
		}
	}
	
	WriteOneOsdLayer( layer_id, x-2,y-2,sx+4,sy+4,(char*)font_buf );
	//WriteOneOsdLayer( layer_id, x-2,y-2,sx+4,sy+4,(char*)font_buf );
}
//czn_20181110_e
//czn_20181110_s
int draw_one_unicode_row_buffer_re( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, one_row_glyph* punicode_row,int redisp_s,int redisp_e)
{ 
	int i,pen_x, pen_y;
	int ascender,descender,x_offset,y_offset;
		
	pen_x = x;
	pen_y = y;

	ascender	= punicode_row->ascender;
	descender	= punicode_row->descender;
	
    for( i = 0; i < punicode_row->chr_cnt; i++ ) 
    { 
    	x_offset = punicode_row->row_glyph_buf[i].bitmap_left;
		y_offset = ascender-punicode_row->row_glyph_buf[i].bitmap_top;
		#if 1
		if(i >= redisp_s && i <= redisp_e)
		{
			draw_bitmap_re( layer_id, pen_x + x_offset, pen_y+y_offset, punicode_row->row_glyph_buf[i].bmp_width, punicode_row->row_glyph_buf[i].bmp_rows, bgcolor,fgcolor, (char*)punicode_row->row_glyph_buf[i].bmp_dat, punicode_row->row_glyph_buf[i].bmp_pitch);
		}
		else
		#endif
		{
			draw_bitmap( layer_id, pen_x + x_offset, pen_y+y_offset, punicode_row->row_glyph_buf[i].bmp_width, punicode_row->row_glyph_buf[i].bmp_rows, fgcolor, bgcolor, (char*)punicode_row->row_glyph_buf[i].bmp_dat, punicode_row->row_glyph_buf[i].bmp_pitch);
		}
		pen_x += punicode_row->row_glyph_buf[i].chr_w; 
    } 

	return i;
} 
//czn_20181110_e
//czn_20181110_s
int display_unicode_string_align_center_re( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int max_width, int max_height, int fnt_type,int redisp_s,int redisp_e)
{
	int x_offset;

	FT_Face target_face;

	if( fnt_type == 1 )
		target_face = face2;
	else
		target_face = face;
	
	// �Ȳ���һ���Ƿ��ܹ���ʾ���
	fill_one_unicode_row_buffer( textstring, len, target_face, &global_unicode_row1, NULL );
	// ������ʾ��ʼλ��
	if( global_unicode_row1.row_width <= max_width )
	{
		x_offset = ((max_width - global_unicode_row1.row_width)/2);
		draw_one_unicode_row_buffer_re( layer_id, x+x_offset, y, fgcolor, bgcolor, &global_unicode_row1,redisp_s,redisp_e);
	}
	else
	{
		return -1;
	}
	return 0;
}
//czn_20181110_e
