
#ifndef _GUI_FONT_H_
#define _GUI_FONT_H_

/*
typedef struct FT_Bitmap_

 {

   int			   rows;		   // 位图的行数

   int			   width;		   // 位图的宽度,也表示每行有多少个像素

   int			   pitch;		   // 偏移值,当往上时为负数,往下时为正数,它的绝对值为位图的一行所占的字节数.不够一个字节当一个字节算.

   unsigned char* buffer;		  // 指向位图缓冲区的指针

   short		   num_grays;	   // 该值只应用于FT_PIXEL_MODE_GRAY模式

   char 		   pixel_mode;	   // 像素模式

   char 		   palette_mode;   // 色块像素模式

   void*		   palette; 	   // 调色板

 } FT_Bitmap;

*/
#define	STR_UNICODE		0
#define	STR_GB2312		1
#define	STR_GBK			2
#define	STR_BIG5		3
#define	STR_UTF8		4

extern int ext_font1_w,ext_font1_h,ext_font2_w,ext_font2_h,ext_font3_w,ext_font3_h;

int init_ext_font(int type);
int init_inner_font(void);



/*******************************************************************************************
 * @fn: 	draw_bitmap
 *
 * @brief:	在osd层显示一个bitmap数据块
 *
 * @param:  x 		- x起始坐标
 * @param:  y 		- y起始坐标
 * @param:  sx	 	- 宽度
 * @param:  sy	 	- 高度
 * @param:  fgcolor	- 前景色
 * @param:  bgcolor	- 背景色
 * @param:  pbitmap	- 位图数据指针
 *
 * @return: none
 *******************************************************************************************/
void draw_bitmap( int layer_id, int x, int y, int sx, int sy, unsigned int fgcolor, unsigned int bgcolor, char* pbitmap, int width );

// lzh_20180804_s
/*******************************************************************************************
 * @fn: 	draw_scaler_bitmap
 *
 * @brief:	在osd层显示一个bitmap数据块
 *
 * @param:  layer_id	- 显示的层
 * @param:  x 		- x起始坐标
 * @param:  y 		- y起始坐标
 * @param:  sx	 	- 宽度 - 显示点阵区的宽度
 * @param:  sy	 	- 高度 - 显示点阵区的高度
 * @param:  fgcolor	- 前景色
 * @param:  bgcolor	- 背景色
 * @param:  pbitmap	- 位图数据指针 - 每个字节为一个像素
 * @param:  bitmast	- 像素字节的有效掩码 1/fgcolor，0/bgcolor
 * @param:  dotw - 	  每个像素显示的宽度
 * @param:  doth - 	  每个像素显示的高度
 *
 * @return: none
 *******************************************************************************************/
void draw_scaler_bitmap( int layer_id, int x, int y, int sx, int sy, unsigned int fgcolor, unsigned int bgcolor, char* pbitmap, unsigned char bitmast, int dotw, int doth );
// lzh_20180804_e

/*******************************************************************************************
 * @fn: 	draw_unicode
 *
 * @brief:	在osd层显示一个unicode字符串
 *
 * @param:  x 			- x起始坐标
 * @param:  y 			- y起始坐标
 * @param:  fgcolor		- 前景色
 * @param:  bgcolor		- 背景色
 * @param:  textstring	- 字符串指针
 * @param:  len			- 字符串长度
 *
 * @return: none
 *******************************************************************************************/
int draw_unicode( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int* pwidth, int* pheight  );

/*******************************************************************************************
 * @fn: 	display_ext_string
 *
 * @brief:	在osd层显示一个任意格式字符串
 *
 * @param:  x 			- x起始坐标
 * @param:  y 			- y起始坐标
 * @param:  fgcolor		- 前景色
 * @param:  bgcolor		- 背景色
 * @param:  string		- 字符串指针
 * @param:  format		- 字符串格式: 0/unicode, 1/GB2312, 2/GBK, 3/BIG5
 *
 * @return: none
 *******************************************************************************************/
void display_ext_string( int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string, int str_len, int fnt_type, int format, int width);

void clear_ext_string( int x, int y, int width, int height, int update );

void OSD_StringDisplayExt(int x,int y,int uColor, int uBgColor, const unsigned char *ptrStr, int str_len, int fnt_type, int format, int width);

/*******************************************************************************************
 * @fn: 	display_string
 *
 * @brief:	在osd层显示一个任意格式字符串
 *
 * @param:  x 			- x起始坐标
 * @param:  y 			- y起始坐标
 * @param:  fgcolor		- 前景色
 * @param:  bgcolor		- 背景色
 * @param:  string		- 字符串指针
 * @param:  format		- 字符串格式: 0/unicode, 1/GB2312, 2/GBK, 3/BIG5
 *
 * @return: none
 *******************************************************************************************/
void display_string( int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string );

void clear_string( int x, int y, int len );


////////////////////////////////////
#define LOG_VIEW_MAX_COL	(60)	//czn_20190412

void clear_logview_screen(void);
void display_logview_string( int x, int y, unsigned int fgcolor, char* string, int format );
void clear_logview_string( int x, int y, int width );

void DisableUpdate(void);
void EnableUpdate(void);
int IsUpdateEnable(void);

#endif


