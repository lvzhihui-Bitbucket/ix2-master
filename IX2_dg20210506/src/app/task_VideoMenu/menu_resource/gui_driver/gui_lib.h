
#ifndef _GUI_LIB_H
#define _GUI_LIB_H

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"

#define OSD_ROTATE_NONE		0
#define OSD_ROTATE_LEFT		1
#define OSD_ROTATE_RIGHT	2
#define OSD_ROTATE_MODE		OSD_ROTATE_NONE

#if 0

#define	AK_VO_CREATE_USER_LAYER		ak_vo_create_bg_layer
#define	USE_GUI_GROUP_1				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_1				AK_VO_LAYER_BG_1
#define	USE_GUI_GROUP_2				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_2				AK_VO_LAYER_BG_2
#define	USE_GUI_GROUP_3				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_3				AK_VO_LAYER_BG_3
#define	USE_GUI_GROUP_4				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_4				AK_VO_LAYER_BG_4
#define	USE_GUI_GROUP_5				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_5				AK_VO_LAYER_BG_5

#define REFRESH_DEFAULT				(USE_GUI_GROUP_1&(1<<USE_GUI_LAYER_1))

#else

#define	AK_VO_CREATE_USER_LAYER		ak_vo_create_gui_layer
#define	USE_GUI_GROUP_1				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_1				AK_VO_LAYER_GUI_1
#define	USE_GUI_GROUP_2				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_2				AK_VO_LAYER_GUI_2
#define	USE_GUI_GROUP_3				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_3				AK_VO_LAYER_GUI_3
#define	USE_GUI_GROUP_4				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_4				AK_VO_LAYER_GUI_4
#define	USE_GUI_GROUP_5				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_5				AK_VO_LAYER_GUI_5

#endif

#define REFRESH_LAYER1			(USE_GUI_GROUP_1&(1<<USE_GUI_LAYER_1))
#define REFRESH_LAYER2			(USE_GUI_GROUP_2&(1<<USE_GUI_LAYER_2))
#define REFRESH_LAYER3			(USE_GUI_GROUP_3&(1<<USE_GUI_LAYER_3))
#define REFRESH_LAYER4			(USE_GUI_GROUP_4&(1<<USE_GUI_LAYER_4))
#define REFRESH_LAYER5			(USE_GUI_GROUP_5&(1<<USE_GUI_LAYER_5))

#define BYTEH1(x)	((x>>24)&0xff)
#define BYTEH2(x)	((x>>16)&0xff)
#define BYTEH3(x)	((x>>8)&0xff)
#define BYTEH4(x)	((x&0xff)
#define SWAPINT(x)  (x=((x&0xFF)<<24)|((x&0xFF00)<<8)|((x&0xFF0000)>>8)|((x&0xFF000000)>>24))

//#define TDE_DISP_SPRITE

//#define VO_MULTI_LAYERS

#if 1 //!defined (PID_DX470_V25)
#define COLOR_FORMAT_RGB565
#endif

#ifndef VO_MULTI_LAYERS
#define REFRESH_DEFAULT				(REFRESH_LAYER1)
#else
#define REFRESH_DEFAULT				(REFRESH_LAYER1|REFRESH_LAYER2|REFRESH_LAYER3|REFRESH_LAYER4|REFRESH_LAYER5)
#endif

#ifdef COLOR_FORMAT_RGB565
#define GP_FORMAT_RGBXXX		GP_FORMAT_RGB565
#else
#define GP_FORMAT_RGBXXX		GP_FORMAT_RGB888
#endif

#define LEN_OFFSET_V                4                                           //yuv��uֵƫ���� y=0 u=h*w v=h*w+h*w/4
#define YUV_TEST_PRINT              100
#define DEV_GUI                     "/dev/fb0"                                  //gui lcdʹ�õ��豸����
#define SIZE_ERROR                  0xffffffff
#define USEC2SEC                    1000000

#ifdef COLOR_FORMAT_RGB565
#define BITS_PER_PIXEL              16
#define LEN_COLOR_R                  5
#define LEN_COLOR_G                  6
#define LEN_COLOR_B                  5
#else
#define BITS_PER_PIXEL              24
#define LEN_COLOR_R                  8
#define LEN_COLOR_G                  8
#define LEN_COLOR_B                  8
#endif

#define DUAL_FB_FIX                 fb_fix_screeninfo_param.reserved[ 0 ]
#define DUAL_FB_VAR                 fb_var_screeninfo_param.reserved[ 0 ]

#ifndef AK_SUCCESS
#define AK_SUCCESS 0
#endif

#ifndef AK_FAILED
#define AK_FAILED -1
#endif

#ifdef COLOR_FORMAT_RGB565
enum color_offset {
	OFFSET_RED = 11,
	OFFSET_GREEN = 5,
	OFFSET_BLUE = 0,
};
#else
enum color_offset {
	OFFSET_RED = 16,
	OFFSET_GREEN = 8,
	OFFSET_BLUE = 0,
};
#endif
typedef struct _gui_base_attr_
{
	int 			fd;				// fb device file descripter
	char*			pframebuf;		// fb mmap pointer	
	int 			max_w;			// fb disp max width
	int 			max_h;			// fb disp max height	
	int 			bpp_len;		// fb disp bpp bytes
	unsigned long 	max_size;		// fb buf size

	int				osd_offset;		// osd offset
	int				fix_smem_len;	
	//
	char*			osdlayer_tmp;		// ������ʾ���������壬�����osdlayer_buf������������д�뵽osd_ram
	char*			osdlayer_buf;		// ȫ����ʾ���������壬�������������Ч�Ĳ�����д�뵽osd_ram
	char*			osdlayer_tde;

	char*			osdlayer_buf_rotatedext1;	// ��ת���������ext1
	char*			osdlayer_buf_rotatedext2;	// ��ת���������ext2
	char*			osdlayer_buf_rotatedext3;	// ��ת���������ext3
	
	struct ak_tde_layer tde;
} gui_base_attr_t;

extern gui_base_attr_t	gui_attr;


/*******************************************************************************************
 * @fn:		gui_attr_initial
 *
 * @brief:	��ʾ�豸�ĳ�ʼ��: ����ʾ�豸�������豸����
 *
 * @param:  none
 *
 * @return: -1/err, 0/ok
 *******************************************************************************************/
int gui_attr_initial(void);

void UpdateOsdLayerBuf(int clrscr );
void UpdateOsdLayerBuf2(int id, int x, int y, int sx, int sy, int clsscr);


//////////////////////////////////////////////////////////////////////////////////////////////
// RGB565
#ifdef COLOR_FORMAT_RGB565
#define COLOR_KEY		0x0000	//0x0000	//0x0400  //0x0000
#define COLOR_RED		0xF800
#define COLOR_GREEN		0x07E0
#define COLOR_BLUE		0x001F
#define COLOR_YELLOW	0xff00
#define COLOR_WHITE		0xffff
#define COLOR_BLACK		0x0001
#define COLOR_MESSAGE_BACK		0x07ff

#define COLOR_SLIVER_GRAY	0x5ACB // 0x8255
#define COLOR_WHITE			0xffff

#define COLOR_LIST_MENU_ICON	0xCF1F		//list�˵���Icon��ɫ

#else
// RGB888
#define COLOR_KEY		0x00000000	//0x0000	//0x0400  //0x0000
#define COLOR_BLACK		0x00000001
#define COLOR_RED		0x00ff0000
#define COLOR_GREEN		0x0000ff00
#define COLOR_YELLOW	0x00ffff00
#define COLOR_BLUE		0x000000ff
#define COLOR_MESSAGE_BACK		0x0007ffff

#define COLOR_SLIVER_GRAY	0x00028255
#define COLOR_WHITE			0xffffffff

#define COLOR_LIST_MENU_ICON	0xCF1F		//list�˵���Icon��ɫ

#endif

void clearscreen(int update);
void osd_Initial(void);
// lzh_20181108_s
char* get_osd_display_attr(int* ptr_max_size, int* ptr_width, int* ptr_height, int* ptr_bpp_len );
// lzh_20181108_e

void ak_vo_refresh_cmd_set(int bits);
void ak_ui_refresh_cmd_set(int bits);

//  lzh_20190608_s
void StoreOsdLayerBuf(void);
void ResetMenuExtBuffer1(void);
void ResetMenuExtBuffer2(void);
void ResetMenuExtBuffer3(void);
void ResetMenuExtBufferAll(void);

#define EXTBUF1_MENU_CNT		1
#define EXTBUF2_MENU_CNT		2
#define EXTBUF3_MENU_CNT		11

void UpdateOsdLayerBuf_vram2extbuf(int menu_cnt);
int UpdateOsdLayerBuf_extbuf2vram(int menu_cnt);
//	lzh_20190608_e

void set_menu_with_video_on(void);
void set_menu_with_video_off(void);

#endif
