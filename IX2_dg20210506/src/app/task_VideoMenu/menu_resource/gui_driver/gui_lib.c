
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/mman.h>
#include <linux/fb.h>

#include "gui_lib.h"
#include "gui_osdlayers.h"
#include "task_VideoMenu.h"

extern OSD_LAYER osd_layers[];

gui_base_attr_t				gui_attr;
struct fb_fix_screeninfo 	fb_fix_screeninfo_param;  //��Ļ�̶�����
struct fb_var_screeninfo 	fb_var_screeninfo_param;  //��Ļ�����ò���


//////////////////////////////////////////////////////////////////////////////
static int					refresh_state;
static int 					refresh_cmd = 0;
//static ak_mutex_t			refresh_mutex;
static pthread_mutex_t	refresh_mutex;
static ak_pthread_t			refresh_id;
static ak_sem_t				refresh_sem;
static ak_cond_t			refresh_con;
#define REFRESH_TIMER		30 	// 20 ms

static int menu_with_video_on = 0;

void set_menu_with_video_on(void)
{
	menu_with_video_on = 1;
	//refresh_cmd=0;
}

void set_menu_with_video_off(void)
{
	menu_with_video_on = 0;
}

// ͬһ���ˢ����Ҫ����ִ�У���©��ʾ
// ��ͬ���ˢ�¿��Ժϲ�ִ�У���ʡʱ��

void ak_ui_refresh_cmd_set(int layer)
{
#if 1
	if( menu_with_video_on )
	{
		//ak_thread_mutex_lock( &refresh_mutex );
		pthread_mutex_lock( &refresh_mutex );
		refresh_cmd |= layer;
		//ak_thread_mutex_unlock( &refresh_mutex );
		pthread_mutex_unlock( &refresh_mutex );
	}
	else
#endif
	{
		pthread_mutex_lock( &refresh_mutex );
		ak_vo_refresh_screen(layer);
		pthread_mutex_unlock( &refresh_mutex );
	}
}

void ak_vo_refresh_cmd_set(int layer)
{
	//ak_thread_mutex_lock( &refresh_mutex );
	pthread_mutex_lock( &refresh_mutex );
	refresh_cmd |= layer;
	//ak_thread_mutex_unlock( &refresh_mutex );
	//ak_vo_refresh_screen(layer);
	pthread_mutex_unlock( &refresh_mutex );
}

void* ak_vo_refresh_process(void)
{
	ak_mutexattr_t	refresh_attr;
	//ak_thread_mutex_init(&refresh_mutex,&refresh_attr);
	pthread_mutex_init(&refresh_mutex,0);
	refresh_cmd 	= 0;
	refresh_state 	= 1;
 	unsigned long nsec;
	struct timespec	wait_time;
	while(1)
	{	
		clock_gettime(CLOCK_REALTIME, &wait_time);
		nsec = wait_time.tv_nsec + REFRESH_TIMER*1000000;
		wait_time.tv_nsec = nsec%1000000000;
		wait_time.tv_sec += nsec/1000000000;
		ak_thread_sem_timedwait(&refresh_sem,&wait_time);
		
		//ak_thread_mutex_lock( &refresh_mutex );
		pthread_mutex_lock( &refresh_mutex );
		#if 1
		if( menu_with_video_on )
		{
			refresh_cmd |= REFRESH_DEFAULT;
			ak_vo_refresh_screen(refresh_cmd);
			//refresh_cmd=0;
		}
		#endif
		//ak_thread_mutex_unlock( &refresh_mutex );
		pthread_mutex_unlock( &refresh_mutex );
	}
	return NULL;
}

void ak_vo_refresh_start(void)
{
	refresh_state = 0;
	ak_thread_create(&refresh_id, ak_vo_refresh_process, NULL, ANYKA_THREAD_MIN_STACK_SIZE, -1);	
	printf("ak_vo_refresh_start ok!\n");	
}

void ak_vo_refresh_stop(void)
{
	if( refresh_state )
	{
		refresh_state = 0;
		ak_thread_cancel(refresh_id);
		ak_thread_join(refresh_id);
		ak_thread_mutex_destroy(&refresh_mutex);
	}
	printf("ak_vo_refresh_stop ok!\n");	
}
/*******************************************************************************************
 * @fn:		gui_attr_initial
 *
 * @brief:	��ʾ�豸�ĳ�ʼ��: ����ʾ�豸�������豸����
 *
 * @param:  none
 *
 * @return: -1/err, 0/ok
 *******************************************************************************************/
#include "MENU_public.h"

int gui_attr_initial(void)
{
#ifdef TDE_DISP_SPRITE		
    ak_tde_open( );
#endif

    gui_attr.fd = open( DEV_GUI, O_RDWR );
	if( gui_attr.fd < 0 ) 
	{
		printf("Can not open fb0!\n");
		return -1;
	}

	
	if (ioctl(gui_attr.fd, FBIOGET_FSCREENINFO, &fb_fix_screeninfo_param) < 0) 
	{
		perror("ioctl FBIOGET_FSCREENINFO");
		close(gui_attr.fd);
		return -1;
	}

	
	if( ioctl(gui_attr.fd, FBIOGET_VSCREENINFO, &fb_var_screeninfo_param) < 0) 
	{
		perror("ioctl FBIOGET_VSCREENINFO");
		close(gui_attr.fd);
		return -1;
	}

	gui_attr.pframebuf = mmap(NULL, fb_fix_screeninfo_param.smem_len, PROT_READ|PROT_WRITE, MAP_SHARED, gui_attr.fd, 0);
	if( gui_attr.pframebuf == MAP_FAILED )
	{
		printf("LCD Video Map failed!\n");
	}
	memset( gui_attr.pframebuf, 0 , fb_fix_screeninfo_param.smem_len );	
	fb_var_screeninfo_param.xres 			= fb_var_screeninfo_param.xres_virtual;
	fb_var_screeninfo_param.yres 			= fb_var_screeninfo_param.yres_virtual;
	fb_var_screeninfo_param.bits_per_pixel 	= BITS_PER_PIXEL;
	fb_var_screeninfo_param.red.offset 		= OFFSET_RED;
	fb_var_screeninfo_param.red.length 		= LEN_COLOR_R ;
	fb_var_screeninfo_param.green.offset 	= OFFSET_GREEN;
	fb_var_screeninfo_param.green.length 	= LEN_COLOR_G ;
	fb_var_screeninfo_param.blue.offset 	= OFFSET_BLUE;
	fb_var_screeninfo_param.blue.length 	= LEN_COLOR_B ;
	if (ioctl(gui_attr.fd, FBIOPUT_VSCREENINFO, &fb_var_screeninfo_param) < 0) 
	{
		close( gui_attr.fd );
		return -1;
	}
	
    if ( ( DUAL_FB_FIX == AK_TRUE ) && ( DUAL_FB_VAR != 0 ) ) 
	{
        DUAL_FB_VAR = 0;
        ioctl( gui_attr.fd, FBIOPUT_VSCREENINFO, &fb_var_screeninfo_param ) ;
    }
	
	gui_attr.max_size 			= fb_var_screeninfo_param.xres * fb_var_screeninfo_param.yres * fb_var_screeninfo_param.bits_per_pixel / 8;	
	if( get_pane_type() == IX481H ||get_pane_type() == IX850 ||get_pane_type() == IX470V ||get_pane_type() == DX470V)	
	{
		
		gui_attr.max_w			= fb_var_screeninfo_param.yres;		// notice here��just yres
		gui_attr.max_h			= fb_var_screeninfo_param.xres;		// notice here��just xres
	
	}
	else
	{
		gui_attr.max_w			= fb_var_screeninfo_param.xres;		// notice here��just xres
		gui_attr.max_h			= fb_var_screeninfo_param.yres;		// notice here��just yres		
	}
	gui_attr.bpp_len			= fb_var_screeninfo_param.bits_per_pixel/8;
	gui_attr.fix_smem_len		= fb_fix_screeninfo_param.smem_len;
	gui_attr.osdlayer_buf 		= ak_mem_dma_alloc(MODULE_ID_VO,gui_attr.max_size);	
	gui_attr.osdlayer_tmp 		= ak_mem_dma_alloc(MODULE_ID_VO,gui_attr.max_size);	
	gui_attr.osdlayer_tde		= NULL; //ak_mem_dma_alloc(MODULE_ID_VO,gui_attr.max_size);	
	gui_attr.osdlayer_buf_rotatedext1 = malloc(gui_attr.max_size);
	gui_attr.osdlayer_buf_rotatedext2 = malloc(gui_attr.max_size);
	gui_attr.osdlayer_buf_rotatedext3 = malloc(gui_attr.max_size);
	printf("var.bits_per_pixel = %d,var.xres = %d,var.yres = %d\n", fb_var_screeninfo_param.bits_per_pixel,fb_var_screeninfo_param.xres,fb_var_screeninfo_param.yres);
	printf("fix.smem_len = 0x%x\n", fb_fix_screeninfo_param.smem_len);

#ifdef TDE_DISP_SPRITE		
	gui_attr.tde.format_param 	= GP_FORMAT_RGBXXX;
	gui_attr.tde.width			= gui_attr.max_w;
	gui_attr.tde.height			= gui_attr.max_h;
	gui_attr.tde.pos_left 		= 0;
	gui_attr.tde.pos_top		= 0;
	gui_attr.tde.pos_width		= gui_attr.max_w;
	gui_attr.tde.pos_height		= gui_attr.max_h;		
	gui_attr.tde.phyaddr 		= fb_fix_screeninfo_param.smem_start;		
#endif	
//////////////////test
    if ( gui_attr.fd > 0 )
	{
        if( gui_attr.pframebuf != NULL ) 
		{
            munmap( gui_attr.pframebuf, gui_attr.fix_smem_len );
			gui_attr.pframebuf = NULL;
        }
        if( close( gui_attr.fd ) == 0 ) 
		{
            gui_attr.fd = 0 ;
        }
    }
//////////////////test
	//////////////////////////////////////////////////////////////////////////////////
	int ret;
    struct ak_vo_param	param;

    /* fill the struct ready to open */
	param.width  = gui_attr.max_w;
	param.height = gui_attr.max_h;
    param.format = GP_FORMAT_RGBXXX;  	//format to output
    #ifdef PID_DX482
	param.rotate = AK_GP_ROTATE_180;
  	#else
	if( get_pane_type() == IX481H ||get_pane_type() == IX850 )	
    	param.rotate = AK_GP_ROTATE_90;    	//rotate value
    else if( get_pane_type() == IX482 || get_pane_type() == DX482 || get_pane_type() == IX482SE)
		param.rotate = AK_GP_ROTATE_180;		//AK_GP_ROTATE_NONE;
	else if	( get_pane_type() == IX470V || get_pane_type() == DX470V)
	{
		param.rotate = AK_GP_ROTATE_270;
	}
	//#if defined (PID_DX470_V25)
	//else if( get_pane_type() == DX470_V25 )
		//param.rotate = AK_GP_ROTATE_180;		// ips
	//	param.rotate = AK_GP_ROTATE_NONE;		// not ips
	//#endif
	else
		param.rotate = AK_GP_ROTATE_NONE;
	#endif
    /* open vo */
    ret = ak_vo_open(&param, DEV_NUM);  //open vo

    if(ret != 0)
    {
        /* open failed return -1 */
        ak_print_error_ex(MODULE_ID_VO, "ak_vo_open failed![%d]\n",ret);
        return AK_FAILED;	
    }

    /* use the double buff mode */
    //ak_vo_set_fbuffer_mode(AK_VO_BUFF_SINGLE);
    ak_vo_set_fbuffer_mode(AK_VO_BUFF_DOUBLE);
	//ak_vo_set_layer_alpha(USE_GUI_LAYER_1,15);

	InitAllOsdLayers( gui_attr.max_w, gui_attr.max_h, gui_attr.bpp_len);

	ak_vo_refresh_start();
	printf("...........................ak_vo_create_gui_layer ok!..................\n");

	return 0;
}

int gui_attr_close(void)
{
	ak_vo_refresh_stop();

    ak_vo_destroy_layer(USE_GUI_LAYER_1);
	ak_vo_close(DEV_NUM);
	
#ifdef TDE_DISP_SPRITE		
    ak_tde_close( );
#endif

    if ( gui_attr.fd > 0 )
	{
        if( gui_attr.pframebuf != NULL ) 
		{
            munmap( gui_attr.pframebuf, gui_attr.fix_smem_len );
			gui_attr.pframebuf = NULL;
        }
        if( close( gui_attr.fd ) == 0 ) 
		{
            gui_attr.fd = 0 ;
            return AK_SUCCESS;
        }
        else 
		{

            return AK_FAILED;
        }
    }
    else 
	{
        return AK_SUCCESS;
    }	
}

/*******************************************************************************************
 * @fn:		clearscreen
 *
 * @brief:	���������Ļ��ʾ
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void clearscreen(int update )
{
	ClearAllOsdLayersWithoutLogWin();
	if( update )
	{
		//UpdateOsdLayerBuf(1);
		UpdateAllLayer();
		//SetLayerAlpha(15);
		
	}
}

/*******************************************************************************************
���뻺���
*******************************************************************************************/
int ClearOsdLayerBuff( int x, int y, int xsize, int ysize, int color )
{
#if 0
	int i,j,k,offset;
	char* pchar;
	char* prgba_fg = (char*)&color;   

	for( i = 0; i < ysize; i++ )
	{
		offset	= ((i+y)*gui_attr.max_w + x)*gui_attr.bpp_len;
		
		pchar = (char*)(gui_attr.osdlayer_buf+offset);
		
		for( j = 0; j < xsize; j++ )
		{
			for( k = 0; k < gui_attr.bpp_len; k++ )
			{
				pchar[j*gui_attr.bpp_len+k] = prgba_fg[gui_attr.bpp_len-k];
			}
		}				
	}	
#else
	int i,len,offset;
	len = xsize*gui_attr.bpp_len;
	for( i = 0; i < ysize; i++ )
	{
		offset	= ((i+y)*gui_attr.max_w + x)*gui_attr.bpp_len;
		memset( (char*)(gui_attr.osdlayer_buf+offset), 0, len );
	}	
#endif
	return 0;
}

void ReadOsdLayerBuf( int x, int y, int xsize, int ysize, char* pdat )
{
	int i,len;
	int offset;
	len = xsize*gui_attr.bpp_len;	
	for( i = 0; i < ysize; i++ )
	{
		offset	= ((i+y)*gui_attr.max_w + x)*gui_attr.bpp_len;
		memcpy(  pdat+i*len, gui_attr.osdlayer_buf+offset, len );
	}
}

static int update_enable = 1;
static struct ak_vo_obj gui_obj;

void DisableUpdate(void)
{
	update_enable = 0;
}

void EnableUpdate(void)
{
	update_enable = 1;
}

int IsUpdateEnable(void)
{
	return update_enable?1:0;
}

void UpdateAllLayer(void)
{
	static short *p_black=NULL;
	if(p_black==NULL)
	{
		int i;
		p_black=malloc(gui_attr.max_size);
		if(p_black!=NULL)
		{
			for(i=0;i<gui_attr.max_size/2;i++)
				p_black[i]=COLOR_BLACK;
		}
	}
	if( update_enable )
	{	
		printf("Menu Clear all !!!\n");
		if(p_black==NULL)
		{
			memset(gui_attr.osdlayer_buf, 1, gui_attr.max_size);
			memset(gui_attr.osdlayer_tmp, 1, gui_attr.max_size);
		}
		else
		{
			memcpy(gui_attr.osdlayer_buf,p_black,gui_attr.max_size);
			memcpy(gui_attr.osdlayer_tmp,p_black,gui_attr.max_size);
		}
		
		/* set obj src info*/	
		gui_obj.format 					= GP_FORMAT_RGBXXX;
		//gui_obj.cmd 					= GP_OPT_BLIT;
		gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY;
		gui_obj.colorkey.coloract 		= COLOR_DELETE; 
		gui_obj.vo_layer.width 			= gui_attr.max_w;
		gui_obj.vo_layer.height 		= gui_attr.max_h;
		gui_obj.colorkey.color_min		= 1; //0xfffff8;			/* min value */
		gui_obj.colorkey.color_max		= 1; //0xfffff8;			/* max value */ 		
		gui_obj.vo_layer.clip_pos.top 	= 0;
		gui_obj.vo_layer.clip_pos.left 	= 0;
		gui_obj.vo_layer.clip_pos.width = gui_attr.max_w;
		gui_obj.vo_layer.clip_pos.height= gui_attr.max_h;

		ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_buf, &(gui_obj.vo_layer.dma_addr));

		/* set dst_layer 1 info*/
		gui_obj.dst_layer.top 			= 0;
		gui_obj.dst_layer.left 			= 0;
		gui_obj.dst_layer.width 		= gui_attr.max_w;
		gui_obj.dst_layer.height 		= gui_attr.max_h;

		/* display obj 1*/
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_1);
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);

	}
}


void SetLayerAlpha(int alpha)
{
	ak_vo_set_layer_alpha(AK_VO_LAYER_GUI_2, alpha);
	//ak_vo_set_layer_alpha(AK_VO_LAYER_VIDEO_1, alpha);
}

/* clsscr = 1: clear 
   clsscr = 0: display */
void UpdateOsdLayerBuf(int clsscr)
{
	if( update_enable )
	{
#ifndef TDE_DISP_SPRITE	

#ifndef VO_MULTI_LAYERS	

		CopyAllOsdLayers2Buffer(gui_attr.osdlayer_buf);
	
		/* set obj src info*/	
		gui_obj.format 					= GP_FORMAT_RGBXXX;
		gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY;
		//gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
		//gui_obj.alpha 				= 15;
		gui_obj.colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		if( clsscr )
		{
			gui_obj.colorkey.color_min		= 1; //0xfffff8;			/* min value */
			gui_obj.colorkey.color_max		= 1; //0xfffff8;			/* max value */
		}
		else
		{
			gui_obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
			gui_obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */			
		}
		gui_obj.vo_layer.width 			= gui_attr.max_w;
		gui_obj.vo_layer.height 		= gui_attr.max_h;
		gui_obj.vo_layer.clip_pos.top 	= 0;
		gui_obj.vo_layer.clip_pos.left 	= 0;
		gui_obj.vo_layer.clip_pos.width = gui_attr.max_w;
		gui_obj.vo_layer.clip_pos.height= gui_attr.max_h;

		ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_buf, &(gui_obj.vo_layer.dma_addr));

		/* set dst_layer 1 info*/
		gui_obj.dst_layer.top 			= 0;
		gui_obj.dst_layer.left 			= 0;
		gui_obj.dst_layer.width 		= gui_attr.max_w;
		gui_obj.dst_layer.height 		= gui_attr.max_h;

		/* display obj 1*/
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_1);
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#else
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#endif

#else
		int i;
		for( i = 0; i < OSD_LAYER_MAX; i ++ )
		{
			ReadOneOsdLayer(i,0,0,gui_attr.max_w,gui_attr.max_h,gui_attr.osdlayer_tmp);
			tde_display_sptrite(0,0,gui_attr.max_w,gui_attr.max_h,gui_attr.osdlayer_tmp);		
		}
#endif
	}
}

#define RD_MIN_HSIZE	18
#define RD_MIN_VSIZE	18

void UpdateOsdLayerBuf2(int id, int x, int y, int sx, int sy, int clsscr)
{
	if( update_enable )
	{
		if( (x == 0) && (y == 0) && (sx == 0) && (sy == 0) )
			return;

		if( (x > gui_attr.max_w) || (y > gui_attr.max_h) )
			return;

#ifndef VO_MULTI_LAYERS	

		if( sx < RD_MIN_HSIZE ) { sx = RD_MIN_HSIZE; }
		if( sy < RD_MIN_VSIZE ) { sy = RD_MIN_VSIZE; }
		
		if( x+sx > 	gui_attr.max_w ) 	sx = gui_attr.max_w - x;
		if( y+sy > 	gui_attr.max_h) 	sy = gui_attr.max_h - y;
		
		ClearOsdLayerBuff(x,y,sx,sy,COLOR_KEY);
		CopyAllOsdLayers2Buffer2(x,y,sx,sy,gui_attr.osdlayer_buf);
		ReadOsdLayerBuf(x,y,sx,sy,gui_attr.osdlayer_tmp);

		//struct ak_vo_obj gui_obj;		
		/* set obj src info*/	
		gui_obj.format 						= GP_FORMAT_RGBXXX;
		gui_obj.cmd 						= GP_OPT_BLIT|GP_OPT_COLORKEY; //|GP_OPT_TRANSPARENT;
		//gui_obj.alpha 					= 15;	
		gui_obj.colorkey.coloract 			= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		gui_obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
		gui_obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */	
		gui_obj.vo_layer.width 				= sx;
		gui_obj.vo_layer.height 			= sy;
		gui_obj.vo_layer.clip_pos.top 		= 0;
		gui_obj.vo_layer.clip_pos.left 		= 0;
		gui_obj.vo_layer.clip_pos.width 	= sx;
		gui_obj.vo_layer.clip_pos.height 	= sy;
		
		ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_tmp, &(gui_obj.vo_layer.dma_addr));
		
		// set dst_layer 1 info
		gui_obj.dst_layer.top 				= y;
		gui_obj.dst_layer.left 				= x;
		gui_obj.dst_layer.width 			= sx;
		gui_obj.dst_layer.height 			= sy;

		// display obj 1
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_1);
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);		
#else
		OSD_LAYER* pOsdLayer = &osd_layers[id]; 
		//ak_vo_refresh_screen(pOsdLayer->ak_vo_group&(1<<pOsdLayer->ak_vo_layer));
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#endif		
	}
}

void UpdateOsdLayerBufAlpha(int clsscr)
{
	if( update_enable )
	{
#ifndef TDE_DISP_SPRITE	

#ifndef VO_MULTI_LAYERS	

		CopyAllOsdLayers2Buffer(gui_attr.osdlayer_buf);
	
		/* set obj src info*/	
		gui_obj.format 					= GP_FORMAT_RGBXXX;
		gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
		gui_obj.alpha 				= 5;
		gui_obj.colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		if( clsscr )
		{
			gui_obj.colorkey.color_min		= 1; //0xfffff8;			/* min value */
			gui_obj.colorkey.color_max		= 1; //0xfffff8;			/* max value */
		}
		else
		{
			gui_obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
			gui_obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */			
		}
		gui_obj.vo_layer.width 			= gui_attr.max_w;
		gui_obj.vo_layer.height 		= gui_attr.max_h;
		gui_obj.vo_layer.clip_pos.top 	= 0;
		gui_obj.vo_layer.clip_pos.left 	= 0;
		gui_obj.vo_layer.clip_pos.width = gui_attr.max_w;
		gui_obj.vo_layer.clip_pos.height= gui_attr.max_h;

		ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_buf, &(gui_obj.vo_layer.dma_addr));

		/* set dst_layer 1 info*/
		gui_obj.dst_layer.top 			= 0;
		gui_obj.dst_layer.left 			= 0;
		gui_obj.dst_layer.width 		= gui_attr.max_w;
		gui_obj.dst_layer.height 		= gui_attr.max_h;

		/* display obj 1*/
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_1);
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#else
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#endif

#else
		int i;
		for( i = 0; i < OSD_LAYER_MAX; i ++ )
		{
			ReadOneOsdLayer(i,0,0,gui_attr.max_w,gui_attr.max_h,gui_attr.osdlayer_tmp);
			tde_display_sptrite(0,0,gui_attr.max_w,gui_attr.max_h,gui_attr.osdlayer_tmp);		
		}
#endif
	}
}

void UpdateOsdLayerAlpha(int id, int x, int y, int sx, int sy, int clsscr)
{
	
	if( !update_enable && !clsscr)
	{
		return;
	}

	printf("UpdateOsdLayerAlpha id=%d clsscr=%d\n",id, clsscr);
	if( (x == 0) && (y == 0) && (sx == 0) && (sy == 0) )
		return;

	if( (x > gui_attr.max_w) || (y > gui_attr.max_h) )
		return;


	if( sx < RD_MIN_HSIZE ) { sx = RD_MIN_HSIZE; }
	if( sy < RD_MIN_VSIZE ) { sy = RD_MIN_VSIZE; }
	
	if( x+sx > 	gui_attr.max_w ) 	sx = gui_attr.max_w - x;
	if( y+sy > 	gui_attr.max_h) 	sy = gui_attr.max_h - y;
	
	ClearOsdLayerBuff(x,y,sx,sy,COLOR_KEY);
	CopyAllOsdLayers2Buffer2(x,y,sx,sy,gui_attr.osdlayer_buf);
	ReadOsdLayerBuf(x,y,sx,sy,gui_attr.osdlayer_tmp);

	//struct ak_vo_obj gui_obj;		
	/* set obj src info*/	
	gui_obj.format 						= GP_FORMAT_RGBXXX;
	if( clsscr )
	{
		gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY;
	}
	else
	{
		gui_obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
		gui_obj.alpha					= 10;	
	}
	gui_obj.colorkey.coloract 			= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
	gui_obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
	gui_obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */ 		
	gui_obj.vo_layer.width 				= sx;
	gui_obj.vo_layer.height 			= sy;
	gui_obj.vo_layer.clip_pos.top 		= 0;
	gui_obj.vo_layer.clip_pos.left 		= 0;
	gui_obj.vo_layer.clip_pos.width 	= sx;
	gui_obj.vo_layer.clip_pos.height 	= sy;
	
	ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_tmp, &(gui_obj.vo_layer.dma_addr));
	
	// set dst_layer 1 info
	gui_obj.dst_layer.top 				= y;
	gui_obj.dst_layer.left 				= x;
	gui_obj.dst_layer.width 			= sx;
	gui_obj.dst_layer.height 			= sy;

	// display obj 1
	if(id == 2)
	{
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_1);
		ak_ui_refresh_cmd_set(REFRESH_DEFAULT); 	
	}
	else
	{
		ak_vo_add_obj(&gui_obj, USE_GUI_LAYER_2);
		ak_ui_refresh_cmd_set(REFRESH_LAYER2);	
	}
}

void osd_Initial(void)
{
	printf("===================osd_Initial===================================\n");

	gui_attr_initial();
	//UpdateOsdLayerBuf(1);		
}

char* get_osd_display_attr( int* ptr_max_size, int* ptr_width, int* ptr_height, int* ptr_bpp_len )
{
	*ptr_max_size	= gui_attr.max_size;
	*ptr_width		= gui_attr.max_w;
	*ptr_height		= gui_attr.max_h;
	*ptr_bpp_len	= gui_attr.bpp_len;
	return gui_attr.osdlayer_buf;
}


//  lzh_20190608_s
static int extbuf1_menu_buffed_flag = 0;
static int extbuf2_menu_buffed_flag = 0;
static int extbuf3_menu_buffed_flag = 0;

void ResetMenuExtBuffer1(void)
{
	extbuf1_menu_buffed_flag = 0;
}
void ResetMenuExtBuffer2(void)
{
	extbuf3_menu_buffed_flag = 0;
}
void ResetMenuExtBuffer3(void)
{
	extbuf3_menu_buffed_flag = 0;
}
void ResetMenuExtBufferAll(void)
{
	extbuf1_menu_buffed_flag = 0;
	extbuf2_menu_buffed_flag = 0;
	extbuf3_menu_buffed_flag = 0;
}

void StoreOsdLayerBuf(void)
{
	CopyAllOsdLayers2Buffer(gui_attr.osdlayer_buf); 	
}

void UpdateOsdLayerBuf_vram2extbuf(int menu_cnt)
{
	char* temp;
	if( menu_cnt == EXTBUF1_MENU_CNT )
	{
		temp = gui_attr.osdlayer_buf_rotatedext1;
		extbuf1_menu_buffed_flag = 1;
	}
	else if( menu_cnt == EXTBUF2_MENU_CNT )
	{			
		temp = gui_attr.osdlayer_buf_rotatedext2;
		extbuf2_menu_buffed_flag = 1;			
	}
	else if( menu_cnt == EXTBUF3_MENU_CNT )
	{
		temp = gui_attr.osdlayer_buf_rotatedext3;
		extbuf3_menu_buffed_flag = 1;			
	}
	else
		temp = 0;

	if( temp )
	{
		//StoreOsdLayerBuf();
		CopyAllOsdLayers2Buffer(temp); 	
		//memcpy( temp, gui_attr.osdlayer_buf, gui_attr.max_size);
	}
}

extern OSD_LAYER osd_layers[OSD_LAYER_MAX];

int UpdateOsdLayerBuf_extbuf2vram(int menu_cnt)
{
	char* temp;
	if( menu_cnt == EXTBUF1_MENU_CNT && extbuf1_menu_buffed_flag )
		temp = gui_attr.osdlayer_buf_rotatedext1;
	else if( menu_cnt == EXTBUF2_MENU_CNT && extbuf2_menu_buffed_flag )
		temp = gui_attr.osdlayer_buf_rotatedext2;
	else if( menu_cnt == EXTBUF3_MENU_CNT && extbuf3_menu_buffed_flag )
		temp = gui_attr.osdlayer_buf_rotatedext3;
	else
		temp = 0;

	if( temp )
	{
		memcpy( (char*)osd_layers[0].vo_out_info.vir_addr, temp, gui_attr.max_size );
		ak_vo_refresh_screen(REFRESH_DEFAULT); //REFRESH_LAYER1);			
		return 1;
	}
	else
		return 0;
}
//  lzh_20190608_e

