
#ifndef _GUI_OSDLAYERS_H
#define  _GUI_OSDLAYERS_H

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"

#include "gui_lib.h"

typedef struct _OSD_LAYER_
{
	int		en;
	int		width;
	int		height;
	int		bpp_len;
	int		buf_size;
	char*	pbuf;		
	struct 	ak_tde_layer 	tde;
	struct 	ak_vo_layer_in	layer;	
	struct 	ak_vo_layer_out	vo_out_info;	
	struct 	ak_vo_obj 		obj;
	int  	ak_vo_group;
	int		ak_vo_layer;	
}OSD_LAYER;

#define OSD_LAYER_BKGD		0
#define OSD_LAYER_CURSOR	1
#define OSD_LAYER_STRING	2
#define OSD_LAYER_SPRITE	3
#define OSD_LAYER_LOG		4
#define OSD_LAYER_MAX		OSD_LAYER_LOG

int InitAllOsdLayers( int w, int h, int bpp_len );

int SetOneOsdLayerEnable( int id );
int SetOneOsdLayerDisable( int id );
int SetOneOsdLayerColor( int id, int color );
int SetOneOsdLayerRectColor( int id, int x, int y, int sx, int sy,  int color );

int ClearOneOsdLayer( int id );
int ClearAllOsdLayers( void );
int ClearAllOsdLayersWithoutLogWin( void );

int ReadOneOsdLayer( int id, int x, int y, int sx, int sy,  char* pbuf );
int WriteOneOsdLayer( int id, int x, int y, int sx, int sy,  const char* pbuf );

int CopyAllOsdLayers2Buffer( char* pbuf );
int CopyAllOsdLayers2Buffer2( int x, int y, int sx, int sy, char* player_buf );

// lzh_20181016_s
// 读取一个层的区块数据保存到文件
int ReadOneOsdLayer_to_file( int id, int x, int y, int sx, int sy, FILE* file );
// 读取文件中的数据写入到一个层的区域
int WriteOneOsdLayer_fm_file( int id, int x, int y, int sx, int sy, FILE* file );
// lzh_20181016_e

int tde_layer2fb( int layer_id, int rx, int ry, int rw, int rh );
int tde_display_sptrite( int rx, int ry, int rw, int rh,  const char* pbuf );


#endif
