
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/mman.h>
#include <linux/fb.h>

#include "gui_lib.h"
#include "gui_osdlayers.h"

extern gui_base_attr_t	gui_attr;

OSD_LAYER osd_layers[OSD_LAYER_MAX];

static int ak_vo_groups[OSD_LAYER_MAX]=
{
	USE_GUI_GROUP_1,
	USE_GUI_GROUP_2,
	USE_GUI_GROUP_3,
	USE_GUI_GROUP_4,
	USE_GUI_GROUP_5,	
};
static int ak_vo_layers[OSD_LAYER_MAX]=
{
	USE_GUI_LAYER_1,		// 16-OSD_LAYER_BKGD
	USE_GUI_LAYER_2,		// 17-OSD_LAYER_CURSOR
	USE_GUI_LAYER_3,		// 18-OSD_LAYER_STRING
	USE_GUI_LAYER_4,		// 19-OSD_LAYER_SPRITE
	USE_GUI_LAYER_5,		// 20-OSD_LAYER_LOG
};

int ak_layer_in_create( OSD_LAYER* player, int ak_vo_group, int ak_vo_layer, int w, int h ) 
{
	int ret;

	player->ak_vo_group					= ak_vo_group;
	player->ak_vo_layer					= ak_vo_layer;
	player->layer.create_layer.height 	= h;
	player->layer.create_layer.width  	= w;
    player->layer.create_layer.left  	= 0;
    player->layer.create_layer.top   	= 0;
    player->layer.format             	= GP_FORMAT_RGBXXX;  /* gui layer format */

    player->layer.layer_opt          	= GP_OPT_COLORKEY; //|GP_OPT_TRANSPARENT; /* support the colorkey opt */
	//player->layer.alpha			 	= 0;
    player->layer.colorkey.coloract  	= COLOR_DELETE;	//COLOR_KEEP;       /* keep the color */
    player->layer.colorkey.color_min 	= 0; //0xfffff8;        	/* min value */
    player->layer.colorkey.color_max 	= 0; //0xfffff8;        	/* max value */

    ret = AK_VO_CREATE_USER_LAYER(&player->layer, ak_vo_layer, &player->vo_out_info);
    if(ret != 0)
    {
        ak_print_error_ex(MODULE_ID_VO, "ak_vo_create_gui_layer failed![%d]\n",ret);
        ak_vo_destroy_layer(ak_vo_layer);
        return AK_FAILED;	
    }
	printf("---------------ak_layer_create layer[%d] ok!------------\n", ak_vo_layer); 
	return 0;
}

#define NOT_USE_VO_ADD_OBJ
int ak_layer_in_write( OSD_LAYER* player, int x, int y, int w, int h, char* pbuf ) 
{
	if( w == 0 || h == 0 )
		return -1;
#ifndef NOT_USE_VO_ADD_OBJ	

	#define	RD_MIN_HSIZE	18
	#define RD_MIN_VSIZE	18
	char rd_pbuf[RD_MIN_VSIZE*1280*3];
	
	//if( w < 18 ) w = 18;	// ??
	//if( h < 18 ) h = 18;	// ??
	int rd_en,rd_w,rd_h,i,len,offset;
	rd_w 	= w;
	rd_h 	= h;
	rd_en 	= 0;
	if( rd_w < RD_MIN_HSIZE ) { rd_w = RD_MIN_HSIZE; rd_en = 1; }
	if( rd_h < RD_MIN_VSIZE ) { rd_h = RD_MIN_VSIZE; rd_en = 1; }
	if( rd_en )
	{
		// read back
		len = rd_w*player->bpp_len;
		for( i = 0; i < rd_h; i++ )
		{
			offset	= ((i+y)*player->width + x)*player->bpp_len;
			memcpy(  rd_pbuf+i*len, (char*)player->vo_out_info.vir_addr+offset, len );
		}
		//write cover
		len = w*player->bpp_len;
		for( i = 0; i < h; i++ )
		{
			offset	= i*rd_w*player->bpp_len;
			memcpy( rd_pbuf+offset, pbuf+i*len, len );
		}
	}
	player->obj.format					= GP_FORMAT_RGBXXX;
	player->obj.cmd 					= GP_OPT_BLIT|GP_OPT_COLORKEY; //|GP_OPT_TRANSPARENT;
	//player->obj.alpha 				= 15;	
	player->obj.colorkey.coloract		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
	player->obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
	player->obj.colorkey.color_max		= 0; //0xfffff8;			/* max value */ 		

	player->obj.vo_layer.width			= (rd_en?rd_w:w);
	player->obj.vo_layer.height 		= (rd_en?rd_h:h);
	player->obj.vo_layer.clip_pos.top	= 0;
	player->obj.vo_layer.clip_pos.left	= 0;
	player->obj.vo_layer.clip_pos.width = (rd_en?rd_w:w);
	player->obj.vo_layer.clip_pos.height= (rd_en?rd_h:h);

	if( rd_en )
		ak_mem_dma_vaddr2paddr(rd_pbuf, &player->obj.vo_layer.dma_addr);
	else
		ak_mem_dma_vaddr2paddr(pbuf, &player->obj.vo_layer.dma_addr);
	
	/* set dst_layer 1 info*/
	player->obj.dst_layer.top			= y;
	player->obj.dst_layer.left			= x;
	player->obj.dst_layer.width 		= (rd_en?rd_w:w);
	player->obj.dst_layer.height		= (rd_en?rd_h:h);
	
	/* display obj 1*/
	ak_vo_add_obj(&player->obj, player->ak_vo_layer);
#else
	int i,len,offset;
	len = w*player->bpp_len;
	for( i = 0; i < h; i++ )
	{
		offset	= ((i+y)*player->width + x)*player->bpp_len;
		memcpy( (char*)player->vo_out_info.vir_addr+offset, pbuf+i*len, len );
	}
#endif
	return 0;
}
int InitOsdLayer( OSD_LAYER* pOsdLayer, int ak_vo_group, int ak_vo_layer, int w, int h, int bpp_len, int en )
{
	pOsdLayer->width	= w;
	pOsdLayer->height	= h;
	pOsdLayer->bpp_len	= bpp_len;
	pOsdLayer->buf_size	= w*h*bpp_len;
	
#ifndef VO_MULTI_LAYERS 	
	//pOsdLayer->pbuf 	= ak_mem_dma_alloc(MODULE_ID_VO,pOsdLayer->buf_size);	
	pOsdLayer->pbuf 	= malloc(pOsdLayer->buf_size);	
	if( pOsdLayer->pbuf == NULL )
	{
		pOsdLayer->en 		= 0;
		printf("osd_layer init fail!\n"); 
	}
	else
	{
		pOsdLayer->en 		= en;
		printf("osd_layer init ok!\n"); 
		
		#ifdef TDE_DISP_SPRITE		
		pOsdLayer->tde.format_param = GP_FORMAT_RGBXXX;
		pOsdLayer->tde.width		= pOsdLayer->width;
		pOsdLayer->tde.height 		= pOsdLayer->height;
		pOsdLayer->tde.pos_left		= 0;
		pOsdLayer->tde.pos_top		= 0;
		pOsdLayer->tde.pos_width	= pOsdLayer->width;
		pOsdLayer->tde.pos_height 	= pOsdLayer->height;		
		ak_mem_dma_vaddr2paddr( pOsdLayer->pbuf, (unsigned long*)&pOsdLayer->tde.phyaddr );		
		#endif
		
		if( ak_vo_layer == USE_GUI_LAYER_1 || ak_vo_layer == USE_GUI_LAYER_2)
		{
			ak_layer_in_create(pOsdLayer,ak_vo_group,ak_vo_layer,w,h);	
		}
	}	
#else
	pOsdLayer->pbuf		= NULL;
	pOsdLayer->en		= en;	
	ak_layer_in_create(pOsdLayer,ak_vo_group,ak_vo_layer,w,h);
	printf("osd_layer init ok!\n"); 	
#endif	
	return 0;
}

int SetOneOsdLayerEnable( int id )
{
	osd_layers[id].en = 1;
	return 0;
}

int SetOneOsdLayerDisable( int id )
{
	osd_layers[id].en = 0;
	return 0;
}

/*
int color,little endian: B0.B1.B2.B3:r.g.b.a
display data array, lo->hi: (b.g.r)-(b.g.r)-(b.g.r)-(b.g.r)...
*/
int SetOneOsdLayerColor( int id, int color )
{
	OSD_LAYER* pOsdLayer = &osd_layers[id];
	int i,k,len;

	char* 	prgba_fg = (char*)&color;	
	len = pOsdLayer->buf_size/pOsdLayer->bpp_len;
	
#ifndef VO_MULTI_LAYERS	
	for( i = 0; i < len; i++ )
	{
		for( k = 0; k< pOsdLayer->bpp_len; k++ )
			pOsdLayer->pbuf[i*pOsdLayer->bpp_len+k] = prgba_fg[pOsdLayer->bpp_len-k];
	}	
#else
	for( i = 0; i < len; i++ )
	{
		for( k = 0; k< pOsdLayer->bpp_len; k++ )
			gui_attr.osdlayer_tmp[i*pOsdLayer->bpp_len+k] = prgba_fg[pOsdLayer->bpp_len-k];
	}
	ak_layer_in_write(pOsdLayer,0,0,pOsdLayer->width,pOsdLayer->height,gui_attr.osdlayer_tmp);
#endif	
	return 0;
}

/*
int color,little endian: B0.B1.B2.B3:r.g.b.a
display data array, lo->hi: (b.g.r)-(b.g.r)-(b.g.r)-(b.g.r)...
*/
int SetOneOsdLayerRectColor( int id, int x, int y, int sx, int sy,  int color )
{
	int i,j,k,offset;
	OSD_LAYER* pOsdLayer = &osd_layers[id];	

	char* 	prgba_fg = (char*)&color;
	short*	pshort;
	int*	pint;	

#ifndef VO_MULTI_LAYERS	
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		
		if( pOsdLayer->bpp_len == 2 )
		{
			pshort = (short*)(pOsdLayer->pbuf+offset);
			for( j = 0; j < sx; j++ )
			{
				pshort[j] = color;
			}			
		}
		else
		{
			pint = (int*)(pOsdLayer->pbuf+offset);
			for( j = 0; j < sx; j++ )
			{
				pint[j] = color;
			}			
		}
	}	
#else
	pchar = (char*)gui_attr.osdlayer_tmp;
	offset = 0;
	for( i = 0; i < sy*sx; i++ )
	{
		for( k = 0; k < pOsdLayer->bpp_len; k++ )
		{
			pchar[offset++] = prgba_fg[pOsdLayer->bpp_len-k];
		}
	}	
	ak_layer_in_write(pOsdLayer,x,y,sx,sy,pchar);
#endif	
	
	return 0;
}

/*
int color,little endian: B0.B1.B2.B3:r.g.b.a
display data array, lo->hi: (b.g.r)-(b.g.r)-(b.g.r)-(b.g.r)...
*/
void memcpy_NoChromeKey( char* ptar, char* psrc, int size, int bpplen, int chromekey  )
{
	int 		len	= size/bpplen;
	int			i,k;

	if( bpplen != 2 )
	{
		char* 		prgba_fg = (char*)&chromekey;   
		
		for( i = 0; i < len; i++ )
		{
			for( k = 0; k < bpplen; k++ )
			{
				if( psrc[i*bpplen+k] != prgba_fg[bpplen-k] )
					break;
			}
			if( k != bpplen )
			{
				memcpy( ptar+i*bpplen, psrc+i*bpplen, bpplen);
			}
		}
	}
	else
	{
		short *pshort_tar = (short*)ptar;
		short *pshort_src = (short*)psrc;
		for( i = 0; i < len; i++ )
		{
			if( pshort_src[i] != 0 )
			{
				pshort_tar[i] = pshort_src[i]; 
			}
		}	
	}
}

int ReadOneOsdLayer( int id, int x, int y, int sx, int sy,  char* pbuf )
{
	int i,len;
	int offset;
	OSD_LAYER* pOsdLayer = &osd_layers[id];	
	len = sx*pOsdLayer->bpp_len;	

	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		memcpy(  pbuf+i*len, pOsdLayer->pbuf+offset, len );
	}
	return 0;
}

int WriteOneOsdLayer( int id, int x, int y, int sx, int sy,  const char* pbuf )
{
	int i,len;
	int offset;
	OSD_LAYER* pOsdLayer = &osd_layers[id];	

	// lzh_20180804_s
	if( x > pOsdLayer->width ) return -1;
	if( y > pOsdLayer->height) return -1;	
	if( sx > pOsdLayer->width ) return -1;
	if( sy > pOsdLayer->height) return -1;	
	// lzh_20180804_e

	// 超过范围处理 lzh_20160509
	if( (x+sx) > pOsdLayer->width ) 	sx = pOsdLayer->width - x;
	if( (y+sy) > pOsdLayer->height )	sy = pOsdLayer->height - y;
	
#ifndef VO_MULTI_LAYERS	
	if( sx==pOsdLayer->width && sy==pOsdLayer->height )
	{
		memcpy( pOsdLayer->pbuf, pbuf, pOsdLayer->buf_size );
	}
	else
	{
		len = sx*pOsdLayer->bpp_len;	
		for( i = 0; i < sy; i++ )
		{
			offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
			memcpy( pOsdLayer->pbuf+offset, pbuf+i*len, len );
		}	
	}
#else
	ak_layer_in_write(pOsdLayer,x,y,sx,sy,pbuf);
#endif	
	
	return 0;
}

// 将源层的数据copy到目标层的缓冲中
int CopyOsdLayer( OSD_LAYER* pSrcLayer, int x, int y, int sx, int sy,  char* pTarLayerBuf )
{
	int i,len;
	int offset;

	// lzh_20180804_s
	if( x > pSrcLayer->width ) return -1;
	if( y > pSrcLayer->height) return -1;	
	if( sx > pSrcLayer->width ) return -1;
	if( sy > pSrcLayer->height) return -1;	
	// lzh_20180804_e

	// 超过范围处理 lzh_20160509
	if( (x+sx) > pSrcLayer->width ) 	sx = pSrcLayer->width - x;
	if( (y+sy) > pSrcLayer->height )	sy = pSrcLayer->height - y;
	
	len = sx*pSrcLayer->bpp_len;	
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pSrcLayer->width + x)*pSrcLayer->bpp_len;
#ifndef VO_MULTI_LAYERS			
		memcpy( pTarLayerBuf+offset, pSrcLayer->pbuf+offset, len );
#else
		memcpy( pTarLayerBuf+offset, (char*)pSrcLayer->vo_out_info.vir_addr+offset, len );
#endif
	}
	return 0;
}

int CopyOsdLayerWithChromeKey( OSD_LAYER* pSrcLayer, int x, int y, int sx, int sy,  char* pTarLayerBuf )
{
	int i,len;
	int offset;

	// lzh_20180804_s
	if( x > pSrcLayer->width ) return -1;
	if( y > pSrcLayer->height) return -1;	
	if( sx > pSrcLayer->width ) return -1;
	if( sy > pSrcLayer->height) return -1;	
	// lzh_20180804_e

	// 超过范围处理 lzh_20160509
	if( (x+sx) > pSrcLayer->width ) 	sx = pSrcLayer->width - x;
	if( (y+sy) > pSrcLayer->height )	sy = pSrcLayer->height - y;
	
	len = sx*pSrcLayer->bpp_len;	
	for( i = 0; i < sy; i++ )
	{
		// 超过范围处理 lzh_20160509
		if( (i+y) >= pSrcLayer->height )
			break;	
		offset	= ((i+y)*pSrcLayer->width + x)*pSrcLayer->bpp_len;
#ifndef VO_MULTI_LAYERS					
		memcpy_NoChromeKey( pTarLayerBuf+offset, pSrcLayer->pbuf+offset, len, pSrcLayer->bpp_len, COLOR_KEY );
#else
		memcpy_NoChromeKey( pTarLayerBuf+offset, (char*)pSrcLayer->vo_out_info.vir_addr+offset, len, pSrcLayer->bpp_len, COLOR_KEY );
#endif
	}
	return 0;
}

int InitAllOsdLayers( int w, int h, int bpp_len )
{
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		InitOsdLayer(&osd_layers[i],ak_vo_groups[i],ak_vo_layers[i],w,h,bpp_len, 1);
	}
	return 0;	
}

int ClearOneOsdLayer( int id )
{
#ifndef VO_MULTI_LAYERS			
	memset( osd_layers[id].pbuf, 0, osd_layers[id].buf_size );
#else	
	memset( (char*)osd_layers[id].vo_out_info.vir_addr, 0, gui_attr.max_size );
#endif	
	return 0;
}

int ClearOneOsdLayer2( int id, int x, int y, int sx, int sy )
{
#ifndef VO_MULTI_LAYERS			
	OSD_LAYER* pOsdLayer = &osd_layers[id];
	int i,len,offset;
	len = sx*pOsdLayer->bpp_len;
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		memset( (char*)pOsdLayer->pbuf+offset, 0, len );
	}	
#else
	OSD_LAYER* pOsdLayer = &osd_layers[id];
	int i,len,offset;
	len = sx*pOsdLayer->bpp_len;
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		memset( (char*)pOsdLayer->vo_out_info.vir_addr+offset, 0, len );
	}	
#endif	
	return 0;	
}

int ClearAllOsdLayers( void )
{
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		ClearOneOsdLayer(i);
	}
	return 0;
}

int ClearAllOsdLayersWithoutLogWin( void )
{
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		if( i != OSD_LAYER_LOG )
		{
			ClearOneOsdLayer(i);
		}
	}
	return 0;
}

int CopyAllOsdLayers2Buffer( char* pbuf )
{
	int i;
#ifndef VO_MULTI_LAYERS	
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		if( osd_layers[i].en )
		{
			if( i == 0 )
				memcpy( pbuf, osd_layers[i].pbuf, osd_layers[i].buf_size );
			else
				memcpy_NoChromeKey( pbuf, osd_layers[i].pbuf, osd_layers[i].buf_size, osd_layers[i].bpp_len, COLOR_KEY );
		}
	}
#else
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		if( osd_layers[i].en )
		{
			if( i == 0 )
				memcpy( pbuf, (char*)osd_layers[i].vo_out_info.vir_addr, osd_layers[i].buf_size );
			else
				memcpy_NoChromeKey( pbuf, (char*)osd_layers[i].vo_out_info.vir_addr, osd_layers[i].buf_size, osd_layers[i].bpp_len, COLOR_KEY );
		}
	}
#endif
	
	return 0;	
}

int CopyAllOsdLayers2Buffer2( int x, int y, int sx, int sy, char* player_buf )
{
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i ++ )
	{
		if( osd_layers[i].en )	
		{
			if( i == 0 )
			{
				CopyOsdLayer( &osd_layers[i],x,y,sx,sy,player_buf );
			}
			else
			{
				CopyOsdLayerWithChromeKey( &osd_layers[i],x,y,sx,sy,player_buf );
			}
		}
	}
	return 0;	
}

// 读取一个层的区块数据保存到文件
int ReadOneOsdLayer_to_file( int id, int x, int y, int sx, int sy, FILE* file )
{
	int i,len;
	int offset;
	OSD_LAYER* pOsdLayer = &osd_layers[id];	

	if( x > pOsdLayer->width ) return -1;
	if( y > pOsdLayer->height) return -1;	
	if( sx > pOsdLayer->width ) return -1;
	if( sy > pOsdLayer->height) return -1;	
	if( (x+sx) > pOsdLayer->width ) 	sx = pOsdLayer->width - x;
	if( (y+sy) > pOsdLayer->height )	sy = pOsdLayer->height - y;
	
	len = sx*pOsdLayer->bpp_len;	
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		fwrite( pOsdLayer->pbuf+offset, len, 1, file );
	}
	return 0;
}

// 读取文件中的数据写入到一个层的区域
int WriteOneOsdLayer_fm_file( int id, int x, int y, int sx, int sy, FILE* file )
{
	int i,len;
	int offset;
	OSD_LAYER* pOsdLayer = &osd_layers[id];	
	char* pbuf;

	if( x > pOsdLayer->width ) return -1;
	if( y > pOsdLayer->height) return -1;	
	if( sx > pOsdLayer->width ) return -1;
	if( sy > pOsdLayer->height) return -1;	
	if( (x+sx) > pOsdLayer->width ) 	sx = pOsdLayer->width - x;
	if( (y+sy) > pOsdLayer->height )	sy = pOsdLayer->height - y;
	
	len = sx*pOsdLayer->bpp_len;	

	pbuf = malloc(len); if( pbuf == NULL ) return -1;
	
	for( i = 0; i < sy; i++ )
	{
		offset	= ((i+y)*pOsdLayer->width + x)*pOsdLayer->bpp_len;
		fread( pbuf, len, 1, file );
		memcpy( pOsdLayer->pbuf+offset, pbuf, len );
	}	

	if( pbuf != NULL ) free(pbuf);
	
	return 0;
}

int tde_layer2fb( int layer_id, int rx, int ry, int rw, int rh )
{
	struct ak_tde_cmd tde_cmd_param;
	OSD_LAYER* psrc_layer = &osd_layers[layer_id];

	psrc_layer->tde.pos_left	= rx;
	psrc_layer->tde.pos_top		= ry;
	psrc_layer->tde.pos_width	= rw;
	psrc_layer->tde.pos_height	= rh;
#if 0
    FILE *pFILE;
    struct stat stat_buf;
    stat( PIC_BG_FILE , &stat_buf );
	pFILE = fopen( PIC_BG_FILE , "rb" );
	fseek(pFILE, 0, SEEK_SET);
	fread( ( char * )psrc_layer->pbuf, 1, stat_buf.st_size, pFILE);
	fclose( pFILE );
#endif	
	ak_tde_opt_scale( &psrc_layer->tde, &gui_attr.tde );
	return 0;
}

// tde para just according to mipi panel para info, so the coordinates para need to rotate 90 degree
// left = gui_attr.max_h - ry - rh;
// top = rx
int tde_display_sptrite( int rx, int ry, int rw, int rh, const char* pbuf )
{
	struct ak_tde_layer tde_src;
	struct ak_tde_cmd tde_cmd_param;
    memset( &tde_cmd_param , 0 , sizeof( struct ak_tde_cmd ) );

	tde_src.format_param 			= GP_FORMAT_RGBXXX;
	tde_src.width					= rw;
	tde_src.height					= rh;
	tde_src.pos_left				= 0;
	tde_src.pos_top					= 0;
	tde_src.pos_width				= rw;
	tde_src.pos_height				= rh;
	ak_mem_dma_vaddr2paddr(gui_attr.osdlayer_tde, ( unsigned long * )&tde_src.phyaddr);	
	memcpy( ( char * )gui_attr.osdlayer_tde, pbuf, rw*rh*gui_attr.bpp_len );

	gui_attr.tde.width				= gui_attr.max_h;	// change tde w -> panel h
	gui_attr.tde.height				= gui_attr.max_w;	// change tde h -> panel w
    gui_attr.tde.pos_left 			= gui_attr.max_h - ry - rh;
    gui_attr.tde.pos_top 			= rx;
    gui_attr.tde.pos_width 			= tde_src.pos_width;
    gui_attr.tde.pos_height 		= tde_src.pos_height;
		
	// GP_OPT_SCALE, GP_OPT_BLIT, GP_OPT_FILLRECT, GP_OPT_ROTATE, GP_OPT_TRANSPARENT, GP_OPT_COLORKEY
	tde_cmd_param.opt 						= GP_OPT_ROTATE|GP_OPT_TRANSPARENT;
	tde_cmd_param.alpha 					= 10;
	tde_cmd_param.rotate_param 				= 1; // 90 degree
	tde_cmd_param.colorkey_param.color_min 	= 0xFFFF00;
	tde_cmd_param.colorkey_param.color_max 	= 0xFFFFFF;
	tde_cmd_param.colorkey_param.coloract	= 0;
    tde_cmd_param.tde_layer_src 			= tde_src;
    tde_cmd_param.tde_layer_dst 			= gui_attr.tde;
    ak_tde_opt( &tde_cmd_param ); 
    return 0;
}

