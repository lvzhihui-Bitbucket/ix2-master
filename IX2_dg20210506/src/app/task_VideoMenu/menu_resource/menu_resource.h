
#ifndef _MENU_RESOURCE_H_
#define	_MENU_RESOURCE_H_

#include "gui_lib.h"
#include "gui_font.h"
#include "gui_osdlayers.h"

#include "menu_unicode_list.h"
#include "language_interface.h"

extern char* pbkgdBuf[];
extern char* piconBuf[];
extern char* ptextBuf[];

extern int	bkgd_w;
extern int 	bkgd_h;
extern char	bkgd_path[];
extern char	icon_path[];
extern char	text_path[];

#define 	POS_SHIFT_LEFT_BITS		0

#define 	ALIGN_LEFT					0
#define 	ALIGN_LEFT_UP				1
#define 	ALIGN_LEFT_DOWN				2
#define 	ALIGN_UP					3
#define 	ALIGN_DOWN					4
#define 	ALIGN_RIGHT					5
#define 	ALIGN_RIGHT_UP				6
#define 	ALIGN_RIGHT_DOWN			7
#define 	ALIGN_MIDDLE				8



#pragma pack(1)

typedef struct _POS
{
	unsigned short x;
	unsigned short y;
} POS;

typedef struct _SIZE
{
	unsigned short h;
	unsigned short v;
} SIZE;

typedef struct _CODE
{
	unsigned short WinTab;
	unsigned short index;
} CODE;


//控件结构类型
typedef struct _BMP_ATTR 
{
	CODE		code;
	POS			pos;
	SIZE		size;
} ICON,struOneCtrl;

#define MAX_LANGUAGE_PACK			64			//最多的语言包个数
#define MAX_LANGUAGE_NAME_LEN		25			//语言包名称长度
#define MAX_MENU_ITEM				100			//最多的功能菜单号
#define MAX_LABEL_TYPE				20			//最多的功能LABEL类型

//单个语言包头结构定义
typedef struct
{
	unsigned char 	en;								//使能标志：0/disable，1/enable
	unsigned short	id;								//语言id
	unsigned char	name[MAX_LANGUAGE_NAME_LEN];	//语言名称
	unsigned char	reserve[4];						//保留
	unsigned int 	m_addr[MAX_MENU_ITEM];			//100个菜单的首地址
	unsigned int 	l_addr[MAX_LABEL_TYPE];			//20个label类型的首地址
} LANGUAGE_TYPE;

typedef struct
{
	 unsigned char label_id;		//作为显示label名称用
	 unsigned char label_attr;		//label的属性（保留）
	 unsigned char label_cnt;		//label的个数
	 unsigned char label_width;		//label的宽度（字符为单位）
	 unsigned char label_reserve[4];//（保留）
} LABEL_STRUCT;

//每个菜单的最多ICON数
#define MAX_ICON_PER_MENU		100
#define MAX_TAB_PER_MENU		40
//每个菜单的属性数据区长度（其后是Font数据）
//#define MAX_ONE_MENU_BYTES		( 10 + MAX_ICON_PER_MENU*sizeof(ICON)*2 )   // include Textctrl
#define MAX_ONE_MENU_BYTES		( 16 + MAX_ICON_PER_MENU*sizeof(ICON)*3 + 4 )   // include Textctrl & include SpriteCtrl

typedef  struct  _MenuData_ 
{
	//数据区
	unsigned short 	fnt_menu_id; 	   //作为显示菜单名称用
	unsigned short 	spi_menu_id;		   //子菜单功能编号
	unsigned short 	spi_disp_hpos;    //子菜单水平显示位置
	unsigned short 	rev1;		   //保留
	unsigned short 	rev2;		   //保留
	unsigned short 	rev3;		   //保留
	unsigned short 	rev4;		   //保留
	unsigned short 	icon_max;	   //菜单的icon个数
	struOneCtrl	  	ListCtrl[MAX_ICON_PER_MENU];  //icon属性
	//20151121
	struOneCtrl	  	TextCtrl[MAX_ICON_PER_MENU];  //text属性
	//20210908
	unsigned short	spri_rsv;
	unsigned short	spri_max;
	struOneCtrl	   SpriCtrl[MAX_ICON_PER_MENU];	//sprite属性
		
	//操作变量
	unsigned short	cntMenu;						//当前菜单编号
	unsigned short	lastCntMenu;					//上一个菜单编号
	unsigned short	cntMenuMode;					//当前菜单初始化场景
	unsigned short	menu_enable;					//当前菜单有效	
	unsigned short	curTKey;						//当前触摸屏选中的ICON或是BMP，相对于ListCtrl[]结构
	unsigned short	tabIcon[MAX_TAB_PER_MENU];		//有TAB属性的ICON按TAB顺序排列的列表
	unsigned short	maxIcon;						//有TAB属性的ICON最大个数，相对于tabIcon[]结构
	unsigned short	curIcon;						//当前光标偏移量，相对于tabIcon[]结构
	unsigned short	oldIcon;						//上一个光标偏移量，相对于tabIcon[]结构
	unsigned short	ntabIcon[MAX_ICON_PER_MENU];	//无TAB属性的ICON或图片列表,不包括WIN0的BMP图
	unsigned short	maxNIcon;						//无TAB属性的ICON最大个数，相对于tabNone[]结构	
	//
	unsigned short 	oldIconIndex;		//上次功能icon
	unsigned short 	curIconIndex;		//当前功能icon

	unsigned short	cntSubMenu;		//czn_20170301
	unsigned short	customflag;		//lzh_20210909
} struOneMenuData;

//菜单运行轨迹结构
#define MAX_MENU_STACK 12
typedef struct _MENU_TRACE_ 
{
	unsigned short uTraceCnt;
	unsigned short uMenuCnt[MAX_MENU_STACK];
	unsigned short uIconCnt[MAX_MENU_STACK];
} struMenuTrace;

#define	IMAGE_TYPE_BKGD		0
#define	IMAGE_TYPE_ICON		1
#define	IMAGE_TYPE_TEXT		2

typedef struct _IMG_ADDR_TAB_NODE_
{
	int 	normal;		// image 正常显示的数据区起始地址
	int 	selected;	// image 选中显示的数据区起始地址
	short	width;		// image 的高度（像素为单位）
	short	height;		// image 的宽带（像素为单位）
	char	type;		// image 类型
	char	language;	// image 语言编号 0/default， 1 - 255
	short 	next_node;	// image 相关联的下一个节点（在misc.idx）文件中查找
}IMG_ADDR_TAB_NODE;

#define IMAGE_NODE_SIZE		sizeof(IMG_ADDR_TAB_NODE)

#pragma pack()

/*******************************************************************************************
 * @fn:		gui_res_initial
 *
 * @brief:	gui资源的初始化，分别加载资源文件的索引文件到内存(底图索引、icon索引、text索引、text扩展索引、menu运行参数)
 *
 * @param:  none
 *
 * @return: 0
 *******************************************************************************************/
int gui_res_initial(void);

unsigned long GetCurLanguageMenuStartAddr(unsigned short menu_cnt);
int LoadCurLanguageLabel(unsigned char label_index,unsigned char label_sn, int x, int y, int color);

unsigned short DisplayOneMenu(unsigned short uMenuCnt,unsigned short uIconCnt);

void DisableOneMenu(void);

int DisplayOneSprite(int ubkgdCnt, int x, int y );
int ClearOneSprite(int ubkgdCnt, int x, int y, int mode );

/*******************************************************************************************
 * @fn:		LoadMenuBackground
 *
 * @brief:	显示指定编号的底图
 *
 * @param:  x 			- 底图显示在屏幕的x坐标
 * @param:  y			- 底图显示在屏幕的y坐标
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuBackground(int bkgdcnt, int x, int y);


/*******************************************************************************************
 * @fn:		LoadMenuAllIcons
 *
 * @brief:	显示当前菜单的所有icon
 *
 * @param:  none
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuAllIcons( void );

/*******************************************************************************************
 * @fn:		LoadMenuAllTexts
 *
 * @brief:	显示当前菜单的所有text
 *
 * @param:  language_cnt 	- 显示的text的语言编号
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuAllTexts(int language_cnt);

/*******************************************************************************************
 * @fn:		DisplayRawIcon
 *
 * @brief:	显示或是反显一个ICON
 *
 * @param:  uIndex 	- icon索引
 * @param:  uDispOn - 0 反显， 1 正显
 *
 * @return: none
 *******************************************************************************************/
void DisplayRawIcon(unsigned short uIndex, int x, int y, int uDispOn);

/*******************************************************************************************
 * @fn:		DisplayOneMenuIcon
 *
 * @brief:	显示或是反显一个ICON，uIconCnt为菜单数据中的偏移量
 *
 * @param:  uIconCnt - 显示的ICON索引
 * @param:  uDispOn - 0 反显， 1 正显
 *
 * @return: none
 *******************************************************************************************/
void DisplayOneMenuIcon(unsigned short uIconCnt,unsigned char uDispOn);

/*******************************************************************************************
 * @fn:		DisplayOneFuncIcon
 *
 * @brief:	显示一个指定功能的icon
 *
 * @param:  icon_func	- icon的功能编号
 * @param:  uDispOn 	- icon显示模式，1/正常显示，0/反显
 *
 * @return: 0
 *******************************************************************************************/
int DisplayOneFuncIcon(unsigned short icon_func,unsigned char uDispOn);


/*******************************************************************************************
 * @fn: 	DisplayCurIconCursor
 *
 * @brief:	显示当前的光标(清除上一个光标)
 *
 * @param:	none
 *
 * @return: 0
 *******************************************************************************************/
void DisplayCurIconCursor(void);

/*******************************************************************************************
 * @fn: 	DisplayNextCursor
 *
 * @brief:	移动ICON光标
 *
 * @param:	uNext：0/上移；1/下移
 *
 * @return: 0
 *******************************************************************************************/
int DisplayNextCursor(unsigned char uNext);

/*******************************************************************************************
 * @fn:		RestoreOldIcon
 *
 * @brief:	恢复上一个光标
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void RestoreOldIcon(void);

/*******************************************************************************************
 * @fn:		SelectCurIcon
 *
 * @brief:	显示当前选中的光标
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void SelectCurIcon(void);

unsigned int TouchValueToTouchKey(unsigned short x, unsigned short  y);

int TouchKeyIsInsideOfDispPane(unsigned short x, unsigned short  y);

unsigned char TouchKeyMapToVirtKey(unsigned short x, unsigned short  y);

int TouchKeyMapToIcon(unsigned short x, unsigned short  y);

unsigned short TouchKeyMapToSimuIcon( unsigned short x, unsigned short y, ICON* ptrIconTab, unsigned short maxIcon );


int get_pane_type(void);
int set_pane_type(int type);

// lzh_20210910_s
int DisplayOneIconWithSprite(unsigned short icon_func, int offset);
// lzh_20210910_e

#endif

