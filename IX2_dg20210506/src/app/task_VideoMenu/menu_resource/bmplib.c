#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/socket.h>   
#include <sys/un.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h> 
#include <netinet/in.h>    
#include <netinet/if_ether.h>   
#include <net/if.h>   
#include <net/if_arp.h>   
#include <arpa/inet.h>     

#include <assert.h>
#include "bmplib.h"

typedef	unsigned int	bool;
#define true		1
#define false		0	




void RGBpixeldisplay(int x,int y,RGBpixel *pixel)
{
	printf("(%d,%d)[%d:%d:%d]",x,y,pixel->Red,pixel->Green,pixel->Blue);
}

void BMFHdisplay(BMFH data)
{
#if 0
	 printf("!!!!!!!!!!!!!!BMFHdisplay\n");
	 printf("bfType: %d\n" ,data.bfType);
	 printf("bfSize: %d\n" ,data.bfSize);
	 printf("bfReserved1: %d\n" ,data.bfReserved1);
	 printf("bfReserved2: %d\n" ,data.bfReserved2);
	 printf("bfOffBits: %d\n" ,data.bfOffBits);
#endif	 
}

void BMIHdisplay(BMIH data)
{
#if 0
	 printf("!!!!!!!!!!!!!!BMIHdisplay\n");
	 printf("biSize: %d\n" ,data.biSize);
	 printf("biWidth: %d\n" ,data.biWidth);
	 printf( "biHeight: %d\n" ,data.biHeight);
	 printf("biPlanes:  %d\n" ,data.biPlanes);
	 printf("biBitCount:  %d\n" ,data.biBitCount);
	 printf("biCompression: %d\n" ,data.biCompression);
	 printf("biSizeImage: %d\n" ,data.biSizeImage);
	 printf("biXPelsPerMeter: %d\n" ,data.biXPelsPerMeter);
	 printf( "biYPelsPerMeter: %d\n" ,data.biYPelsPerMeter);
	 printf("biClrUsed:  %d\n" ,data.biClrUsed);
	 printf("biClrImportant: %d\n" ,data.biClrImportant);
#endif	 
}



#define SafeFread(buffer,size,number,fp)		fread(buffer,size,number,fp)
int ReadBMPFromFile( const char* FileName,BMP **pbmp)
{ 

	 FILE* fp = fopen( FileName, "rb" );
	 if( fp == NULL )
	 {
	  	return -1;
	 }
 
	 // read the file header 
 
 	BMFH bmfh;
 	bool NotCorrupted = true;
 
 	NotCorrupted &= SafeFread( (char*) &(bmfh.bfType) , sizeof(ebmpWORD), 1, fp);
 	#if 0
 	bool IsBitmap = false;
	 
	 if( IsBigEndian() && bmfh.bfType == 16973 )
	 { IsBitmap = true; }
	 if( !IsBigEndian() && bmfh.bfType == 19778 )
	 { IsBitmap = true; }
	 
	 if( !IsBitmap ) 
	 {
	  if( EasyBMPwarnings )
	  {
	   cout << "EasyBMP Error: " << FileName 
	        << " is not a Windows BMP file!" << endl; 
	  }
	  fclose( fp ); 
	  return false;
	 }
	#endif
	 NotCorrupted &= SafeFread( (char*) &(bmfh.bfSize) , sizeof(ebmpDWORD) , 1, fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmfh.bfReserved1) , sizeof(ebmpWORD) , 1, fp);
	 NotCorrupted &= SafeFread( (char*) &(bmfh.bfReserved2) , sizeof(ebmpWORD) , 1, fp);
	 NotCorrupted &= SafeFread( (char*) &(bmfh.bfOffBits) , sizeof(ebmpDWORD) , 1 , fp);
 #if 0
 if( IsBigEndian() ) 
 { bmfh.SwitchEndianess(); }
 #endif
 // read the info header

	 BMIH bmih; 
	 
	 NotCorrupted &= SafeFread( (char*) &(bmih.biSize) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biWidth) , sizeof(ebmpDWORD) , 1 , fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmih.biHeight) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biPlanes) , sizeof(ebmpWORD) , 1, fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmih.biBitCount) , sizeof(ebmpWORD) , 1, fp);

	 NotCorrupted &= SafeFread( (char*) &(bmih.biCompression) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biSizeImage) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biXPelsPerMeter) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biYPelsPerMeter) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biClrUsed) , sizeof(ebmpDWORD) , 1 , fp);
	 NotCorrupted &= SafeFread( (char*) &(bmih.biClrImportant) , sizeof(ebmpDWORD) , 1 , fp);

	 BMFHdisplay(bmfh);
	 BMIHdisplay(bmih);


 
 //XPelsPerMeter = bmih.biXPelsPerMeter;
// YPelsPerMeter = bmih.biYPelsPerMeter;
 
 // if bmih.biCompression 1 or 2, then the file is RLE compressed
 

 
 // if bmih.biCompression > 3, then something strange is going on 
 // it's probably an OS2 bitmap file.
 

 // set the bit depth
 
 int TempBitDepth = (int) bmih.biBitCount;
 if(    TempBitDepth != 1  && TempBitDepth != 4 
     && TempBitDepth != 8  && TempBitDepth != 16
     && TempBitDepth != 24 && TempBitDepth != 32 )
 {
  	return -1;
 }
 int BitDepth = TempBitDepth;
 int Height = bmih.biHeight;
 int Width = bmih.biWidth;
 //SetBitDepth( (int) bmih.biBitCount ); 
 
 // set the size


  
 // some preliminaries

 
 
 // skip blank data if bfOffBits so indicates
 
 int BytesToSkip = bmfh.bfOffBits - 54;;
#if 0
 if( BytesToSkip > 0 && BitDepth != 16 )
 {
  if( EasyBMPwarnings )
  {
   cout << "EasyBMP Warning: Extra meta data detected in file " << FileName << endl
        << "                 Data will be skipped." << endl;
  }
  ebmpBYTE* TempSkipBYTE;
  TempSkipBYTE = new ebmpBYTE [BytesToSkip];
  SafeFread( (char*) TempSkipBYTE , BytesToSkip , 1 , fp);   
  delete [] TempSkipBYTE;
 } 
  #endif
 // This code reads 1, 4, 8, 24, and 32-bpp files 
 // with a more-efficient buffered technique.

 int i,j,z;
 BMP *bmp_temp;
 if( BitDepth != 16 )
 {
 	bmp_temp =  malloc(sizeof(BMP));
	 bmp_temp->w = Width;
	 bmp_temp->h=Height;
	 bmp_temp->pixel = malloc(sizeof(RGBpixel)*bmp_temp->w*bmp_temp->h);
	 int BufferSize = (int) ( (Width*BitDepth) / 8.0 );
	  while( 8*BufferSize < Width*BitDepth )
	  { BufferSize++; }
	  while( BufferSize % 4 )
	  { BufferSize++; }
	  printf("!!!!!!!!!BufferSize=%d\n",BufferSize);
	  ebmpBYTE* Buffer;
	  Buffer = malloc(sizeof(ebmpBYTE)*BufferSize);
	  j= Height-1;
	  while( j > -1 )
	  {
	   int BytesRead = (int) fread( (char*) Buffer, 1, BufferSize, fp );
		   if( BytesRead < BufferSize )
		   {
		   	printf("datareaderr j=%d,%d<%d\n",j,BytesRead,BufferSize);
		   	 j = -1; 
		   }
		   else
		   {
		   		RGBpixeldisplay(1,j,(RGBpixel*)&Buffer[3]);
				RGBpixeldisplay(2,j,(RGBpixel*)&Buffer[6]);
				RGBpixeldisplay(3,j,(RGBpixel*)&Buffer[9]);
				#if 1
				for(z= 0;z<Width;z++)
					memcpy(&bmp_temp->pixel[j*Width+z],&Buffer[z*3],3);

				RGBpixeldisplay(1,j,(RGBpixel*)&bmp_temp->pixel[j*Width+1]);
				RGBpixeldisplay(2,j,(RGBpixel*)&bmp_temp->pixel[j*Width+2]);
				RGBpixeldisplay(3,j,(RGBpixel*)&bmp_temp->pixel[j*Width+3]);
				#endif
				printf("\n");
			   #if 0
			    bool Success = false;
			    if( BitDepth == 1  )
				{ Success = Read1bitRow(  Buffer, BufferSize, j ); }
			    if( BitDepth == 4  )
				{ Success = Read4bitRow(  Buffer, BufferSize, j ); }
			    if( BitDepth == 8  )
				{ Success = Read8bitRow(  Buffer, BufferSize, j ); }
			    if( BitDepth == 24 )
				{ Success = Read24bitRow( Buffer, BufferSize, j ); }
				if( BitDepth == 32 )
				{ Success = Read32bitRow( Buffer, BufferSize, j ); }
			    if( !Success )
			    {
			     if( EasyBMPwarnings )
			     {
			      cout << "EasyBMP Error: Could not read enough pixel data!" << endl;
				 }
				 j = -1;
			    }
				#endif
		   }   
	  	 j--;
	  }
 	free(Buffer);
 }

 *pbmp = bmp_temp;
 fclose(fp);
 return 0;
}

void releaseBmp(BMP *pbmp)
{
	if(pbmp!=NULL)
	{
		if(pbmp->pixel!=NULL)
			free(pbmp->pixel);
		free(pbmp);
	}
}

BMP_FILE *OpenBMPFile(char *FileName)
{
	if(FileName== NULL)
		return NULL;
	BMP_FILE *bmpfile = malloc(sizeof(BMP_FILE));
	if(bmpfile == NULL)
	{
		goto OpenBMPFileErr;
	}
	memset(bmpfile,0,sizeof(BMP_FILE));
	bmpfile->fp = fopen( FileName, "rb" );
	 if( bmpfile->fp == NULL )
	 {
	  	goto OpenBMPFileErr;
	 }
 	//BMFH bmfh;
	//BMIH bmih;

	
 	bool NotCorrupted = true;
 
 	NotCorrupted &= SafeFread( (char*) &(bmpfile->bmfh.bfType) , sizeof(ebmpWORD), 1, bmpfile->fp);
 	
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmfh.bfSize) , sizeof(ebmpDWORD) , 1, bmpfile->fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmfh.bfReserved1) , sizeof(ebmpWORD) , 1, bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmfh.bfReserved2) , sizeof(ebmpWORD) , 1, bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmfh.bfOffBits) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);

	  
	 
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biSize) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biWidth) , sizeof(ebmpDWORD) , 1 , bmpfile->fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biHeight) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biPlanes) , sizeof(ebmpWORD) , 1, bmpfile->fp); 
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biBitCount) , sizeof(ebmpWORD) , 1, bmpfile->fp);

	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biCompression) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biSizeImage) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biXPelsPerMeter) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biYPelsPerMeter) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biClrUsed) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);
	 NotCorrupted &= SafeFread( (char*) &(bmpfile->bmih.biClrImportant) , sizeof(ebmpDWORD) , 1 , bmpfile->fp);

	 BMFHdisplay(bmpfile->bmfh);
	 BMIHdisplay(bmpfile->bmih);
	
	 int TempBitDepth = (int) bmpfile->bmih.biBitCount;
	 if(    TempBitDepth != 1  && TempBitDepth != 4 
	     && TempBitDepth != 8  && TempBitDepth != 16
	     && TempBitDepth != 24 && TempBitDepth != 32 )
	  {
	  	goto OpenBMPFileErr;
	 }

 		int BitDepth = TempBitDepth;
 	int Height = bmpfile->bmih.biHeight;
	int Width = bmpfile->bmih.biWidth;
	 if( BitDepth != 16 )
	 {
		 int BufferSize = (int) ( (Width*BitDepth) / 8.0 );
		  while( 8*BufferSize < Width*BitDepth )
		  { BufferSize++; }
		  while( BufferSize % 4 )
		  { BufferSize++; }
		  //printf("!!!!!!!!!BufferSize=%d\n",BufferSize);
		  bmpfile->onelinebuff_lenth = BufferSize;
		  bmpfile->onelinebuff = malloc(sizeof(ebmpBYTE)*BufferSize);
		  if(bmpfile->onelinebuff==NULL)
		  {
		  	goto OpenBMPFileErr;
		 }	
		  bmpfile->cur_line = Height-1;

		  return bmpfile;
	 }

 OpenBMPFileErr:
 	if(bmpfile != NULL)
 	{
 		if(bmpfile->fp!=NULL)
			fclose(bmpfile->fp);
		if(bmpfile->onelinebuff!=NULL)
			free(bmpfile->onelinebuff);
		free(bmpfile);
	}
 	return NULL;
}

void CloseBMPFile(BMP_FILE* bmpfile)
{
	if(bmpfile != NULL)
 	{
 		if(bmpfile->fp!=NULL)
			fclose(bmpfile->fp);
		if(bmpfile->onelinebuff!=NULL)
			free(bmpfile->onelinebuff);
		free(bmpfile);
	}
}

int BMPFileReadOneLine(BMP_FILE* bmpfile)
{
	int BytesRead;
	if(bmpfile==NULL)
		return -1;
	  if(bmpfile->cur_line> -1)
	 {
		BytesRead  = (int) fread( (char*) bmpfile->onelinebuff, 1, bmpfile->onelinebuff_lenth, bmpfile->fp);
		   if( BytesRead < bmpfile->onelinebuff_lenth)
		   {
		   	bmpfile->cur_line = -1;
		   	return -1;
		   }
		   else
		   {
		   		
		   }  
		    bmpfile->cur_line--;
			
		return BytesRead;
	  }

	return -1;
}
void Pixel888to565Copy(unsigned short *tar_buff,char *bmp_dat,int pixel_num)
{
	RGBpixel *pixel;
	#if 1
	unsigned short dat565;
	int i;
	for(i = 0;i < pixel_num;i++)
	{
		dat565 = 0;
		pixel = (RGBpixel*)bmp_dat;
		bmp_dat+=3;
		dat565 = (((unsigned short)(pixel->Red&0xf8))<<8);
		dat565 |= (((unsigned short)(pixel->Green&0xfc))<<3);
		dat565 |= (((unsigned short)(pixel->Blue&0xf8))>>3);

		tar_buff[i] = dat565;
	}
	#else
	int i;
	pixel = (RGBpixel*)bmp_dat;
	for( i = 0;i < pixel_num; i++,pixel++ )
	{
		tar_buff[i*3+0] = pixel->Blue;		
		tar_buff[i*3+1] = pixel->Green;		
		tar_buff[i*3+2] = pixel->Red;	
	}	
	#endif
}
	
