
#ifndef _OBJ_LANGUAGE_DAT_H_
#define _OBJ_LANGUAGE_DAT_H_

struct MESG_TEXT_T
{
	int		id;
	char* 	org_ptr;
	char*	tra_ptr;
} ;

#define MESG_TEXT(ID,ORG,TRA)	{ID,ORG,TRA}

extern const struct MESG_TEXT_T global_mesg_text_tab[];

/*-----------------------------------------------------------------------------------
global unicode string define
-----------------------------------------------------------------------------------*/
#define MESG_TEXT_Settings					1
#define MESG_TEXT_WirelessSetting			2
#define MESG_TEXT_LanSetting				3
#define MESG_TEXT_WIFI_Input				4
#define MESG_TEXT_WIFI_INFO					5
#define MESG_TEXT_WIFI_Connecting			6
#define MESG_TEXT_WIFI_Connected			7
#define MESG_TEXT_WIFI_Disconnected			8
#define MESG_TEXT_WIFI_ON					9
#define MESG_TEXT_WIFI_OFF					10
#define MESG_TEXT_WIFI_Searching			11
#define MESG_TEXT_Monitor					12
#define MESG_TEXT_Intercom					13
#define MESG_TEXT_Namelist					14
#define MESG_TEXT_CallRecord				15
#define MESG_TEXT_NoDisturb					16
#define MESG_TEXT_Setting					17
#define MESG_TEXT_SetGeneral				18
#define MESG_TEXT_SetInstaller				19
#define MESG_TEXT_About						20
#define MESG_TEXT_WifiSSID					21
#define MESG_TEXT_WifiPWD					22
#define MESG_TEXT_IncomingCallList			23
#define MESG_TEXT_OutgoingCallList			24
#define MESG_TEXT_MissedCallList			25
#define MESG_TEXT_PlaybackList				26
#define MESG_TEXT_CallRecordDeltail			27
#define MESG_TEXT_HaveNoSDCard				28
#define MESG_TEXT_KeypadHelpUp				29
#define MESG_TEXT_KeypadHelpDown			30
#define MESG_TEXT_KeypadHelpLeft			31
#define MESG_TEXT_KeypadHelpRight			32
#define MESG_TEXT_KeypadHelpEnter			33
#define MESG_TEXT_KeypadHelpDel				34
#define MESG_TEXT_KeypadHelpSwitch			35
#define MESG_TEXT_DateTimeSet				36
#define MESG_TEXT_Date						37
#define MESG_TEXT_Time						38
// zfz_20190601 start
#define MESG_TEXT_DS_RING_SET				39
#define MESG_TEXT_CDS_RING_SET				40
#define MESG_TEXT_OS_RING_SET				41
#define MESG_TEXT_MSG_RING_SET				42
#define MESG_TEXT_DoreBell_RING_SET			43
#define MESG_TEXT_Intercom_RING_SET			44
#define MESG_TEXT_InnerCall_RING_SET		45
#define MESG_TEXT_ALARM_RING_SET			46
#define MESG_TEXT_DS_SetTune				47
#define MESG_TEXT_CDS_SetTune				48
#define MESG_TEXT_OS_SetTune				49
#define MESG_TEXT_MSG_SetTune				50
#define MESG_TEXT_DoreBell_SetTune			51
#define MESG_TEXT_Intercom_SetTune			52
#define MESG_TEXT_InnerCall_SetTune			53
#define MESG_TEXT_Alarm_SetTune				54
//end
#define MESG_TEXT_Node						55
#define MESG_TEXT_Addr						56
#define MESG_TEXT_DeviceType				57
#define MESG_TEXT_SW_Ver					58
#define MESG_TEXT_NodeId					59
#define MESG_TEXT_SerialNo					60
#define MESG_TEXT_IPAddress					61
#define MESG_TEXT_HW_Address				62
#define MESG_TEXT_SubnetMask				63
#define MESG_TEXT_DefaultRoute				64
#define MESG_TEXT_DS						65
#define MESG_TEXT_DTAddress					66
#define MESG_TEXT_GU						67
#define MESG_TEXT_linked					68
#define MESG_TEXT_unlink					69
#define MESG_TEXT_CallrecordPropertyNormal	70
#define MESG_TEXT_CallrecordPropertyMissed	71
#define MESG_TEXT_TargetNode				72
#define MESG_TEXT_Name						73
#define MESG_TEXT_TargetAddr				74
#define MESG_TEXT_Time1						75
#define MESG_TEXT_Video						76
#define MESG_TEXT_12h						77
#define MESG_TEXT_24h						78
#define MESG_TEXT_m_d_y						79
#define MESG_TEXT_d_m_y						80
#define MESG_TEXT_LanguageSelectTitle		81
#define MESG_TEXT_SlaveRegister				82
#define MESG_TEXT_SlaveSn					83
#define MESG_TEXT_SlaveName					84
#define MESG_TEXT_MasterSn					85
#define MESG_TEXT_MasterName				86
#define MESG_TEXT_MasterRegister			87
#define MESG_TEXT_MasterDeviceManage		88
#define MESG_TEXT_UnifiedSetting			89
#define MESG_TEXT_Master					90
#define MESG_TEXT_Slave						91
#define MESG_TEXT_AddSlave					92
#define MESG_TEXT_PhoneNum					93
#define MESG_TEXT_PhoneIpAddr				94
#define MESG_TEXT_PhoneName					95
#define MESG_TEXT_CallScene					96
#define MESG_TEXT_HaveNoDevice				97
#define MESG_TEXT_SipConfig					98
#define MESG_TEXT_SipServer					99
#define MESG_TEXT_SipPort					100
#define MESG_TEXT_SipDivert					101
#define MESG_TEXT_SipAccount				102
#define MESG_TEXT_SipPassword				103
#define MESG_TEXT_MasterSlaveSet			104
#define MESG_TEXT_DateAndTime				105
#define MESG_TEXT_Language					106
#define MESG_TEXT_SDCardInfo				107
#define MESG_TEXT_TotalSize					108
#define MESG_TEXT_FreeSize					109
#define MESG_TEXT_Format					110
#define MESG_TEXT_RegisterNewSlave			111
#define MESG_TEXT_SalveInfo					112
#define MESG_TEXT_AddIpSmartPhone			113
#define MESG_TEXT_MyID						114
#define MESG_TEXT_MasterID					115
#define MESG_TEXT_RegisteredTo				116
#define MESG_TEXT_Registered				117
#define MESG_TEXT_Unegistered				118
#define MESG_TEXT_PressMenuToRegist			119
#define MESG_TEXT_SipOnline					120		//czn_20170725
#define MESG_TEXT_ErrSipServer				121		//czn_20170725
#define MESG_TEXT_InputNumCall				122
#define MESG_TEXT_Enable					123
#define MESG_TEXT_Disable					124
#define MESG_TEXT_IntercomEnable			125
#define MESG_TEXT_RingVolume				126
#define MESG_TEXT_RingVolumeSet				127
#define MESG_TEXT_MasterOn					128		//czn_20170803
#define MESG_TEXT_MasterOff					129		
#define MESG_TEXT_NoMaster					130		
#define MESG_TEXT_SDCard					131
#define MESG_TEXT_Restore					132
#define MESG_TEXT_WaitingForReboot			133
#define MESG_TEXT_RegisterPhone				134
#define MESG_TEXT_PhoneIP					135
#define MESG_TEXT_PhoneManage				136
#define MESG_TEXT_ErrSipAccount				137
#define MESG_TEXT_Icon_012_Namelist			138
#define MESG_TEXT_Icon_013_InnerCall		139
#define MESG_TEXT_Icon_014_GuardStation		140
#define MESG_TEXT_Icon_015_InputNumbers		141
#define MESG_TEXT_Icon_066_NormalUse					142
#define MESG_TEXT_Icon_067_NoDisturb8H					143
#define MESG_TEXT_Icon_068_NoDisturbAlways				144
#define MESG_TEXT_Icon_069_DivertCallIfNoAnswer			145
#define MESG_TEXT_Icon_070_DivertCallSimultaneously		146
#define MESG_TEXT_Icon_042_CurrentAddress				147
#define MESG_TEXT_Icon_045_IntercomEnable				148
#define MESG_TEXT_Icon_093_FW_Upgrade					149
#define MESG_TEXT_Icon_043_MasterSlaver					150
#define MESG_TEXT_ICON_044_Parameter					151
#define MESG_TEXT_Icon_029_WifiSwitch					152
#define MESG_TEXT_Icon_030_LanSetting					153
#define MESG_TEXT_Icon_031_RegistPhone					154
#define MESG_TEXT_Icon_032_RegistSlave					155
#define MESG_TEXT_Icon_071_SipConfig					156
#define MESG_TEXT_Icon_041_DateAndTime					157
#define MESG_TEXT_Icon_056_Language						158
#define MESG_TEXT_Icon_057_MonitorTime					159
#define MESG_TEXT_Icon_058_SDCard						160
#define MESG_TEXT_Icon_059_RestoreToDefault				161
#define MESG_TEXT_ICON_024_CallTune						162
#define MESG_TEXT_ICON_025_general						163
#define MESG_TEXT_ICON_026_InstallerSetup				164
#define MESG_TEXT_ICON_027_wireless						165
#define MESG_TEXT_ICON_033_WifiSSID						166
#define MESG_TEXT_ICON_034_WifiPWD						167
#define MESG_TEXT_ICON_035_WifiConnect					168
#define MESG_TEXT_ICON_052_DateMode						169
#define MESG_TEXT_ICON_053_TimeMode						170
#define MESG_TEXT_ICON_054_Date							171
#define MESG_TEXT_ICON_055_Time							172
#define MESG_TEXT_ICON_072_SipServer					173
#define MESG_TEXT_ICON_073_SipPort						174
#define MESG_TEXT_ICON_074_SipDivert					175
#define MESG_TEXT_ICON_075_SipAccount					176
#define MESG_TEXT_ICON_076_SipPassword					177
#define MESG_TEXT_ICON_060_SDInformation				178
#define MESG_TEXT_ICON_061_CopyImage					179
#define MESG_TEXT_ICON_062_Format						180
#define MESG_TEXT_ICON_083_RestoreGeneralData			181
#define MESG_TEXT_ICON_084_RestoreUserData				182
#define MESG_TEXT_ICON_085_RestoreWirelessData			183

//czn_20181219_s
#define MESG_TEXT_ICON_FwUpgrage_Server					184
#define MESG_TEXT_ICON_FwUpgrage_DLCode					185
#define MESG_TEXT_FwUpgrage_Check				    	186
#define MESG_TEXT_FwUpgrage_Install				    	187
#define MESG_TEXT_FwUpgrage_CodeInfo					188
#define MESG_TEXT_FwUpgrage_CodeSize					189
#define MESG_TEXT_FwUpgrage_Cennecting			    	190
#define MESG_TEXT_FwUpgrage_Downloading			   		191
#define MESG_TEXT_FwUpgrage_Installing			    	192
#define MESG_TEXT_FwUpgrage_ErrConnecting				193
#define MESG_TEXT_FwUpgrage_ErrNoFile					194
#define MESG_TEXT_FwUpgrage_ErrDownload					195
#define MESG_TEXT_FwUpgrage_ErrNoSpace					196
#define MESG_TEXT_ICON_FwUpgrage				    	197
#define MESG_TEXT_FwUpgrage_ErrInvalidCode		    	198
#define MESG_TEXT_FwUpgrage_InstallOk					199
#define MESG_TEXT_FwUpgrage_InstallFail					200
#define MESG_TEXT_FwUpgrage_Canceling					201
#define MESG_TEXT_FwUpgrage_ErrMismatchedType			202
#define MESG_TEXT_FwUpgrage_PleaseInsertSDCard			203
//czn_20181219_e


#define MESG_TEXT_ICON_310_FW_UpdateCode				204
#define MESG_TEXT_ICON_311_FW_UpdateServer				205
#define MESG_TEXT_ICON_312_FW_UpdateStart				206
#define MESG_TEXT_FW_Update								207
#define MESG_TEXT_InputFWUpdateCode						208
#define MESG_TEXT_InputFWUpdateServer					209
#define MESG_TEXT_InputName								210
#define MESG_TEXT_MonListEdit							211
#define MESG_TEXT_NamelistEdit							212
#define MESG_TEXT_Icon_046_Reboot						213
#define MESG_TEXT_InputSystemPassword					214
#define MESG_TEXT_SystemSettings						215
#define MESG_TEXT_Icon_313_DivertTimeSet				216
#define MESG_TEXT_InputDivertTime						217
#define MESG_TEXT_Icon_314_ReRegister					218
#define MESG_TEXT_ICON_315_BroadcastTimeSet				219
#define MESG_TEXT_InputBroadcastTime					220
#define MESG_TEXT_ICON_316_MicVolumeSet					221
#define MESG_TEXT_ICON_317_SpeakerVolumeSet				222
#define MESG_TEXT_InputMicVolume						223
#define MESG_TEXT_InputSpeakerVolume					224
#define MESG_TEXT_ICON_028_About						225
#define MESG_TEXT_HW_Ver								226
#define MESG_TEXT_ICON_318_VideoResolution				227
#define MESG_TEXT_ICON_259_IPC_Setting					228
#define MESG_TEXT_IPC_Setting							229
#define MESG_TEXT_IPC_Name								230
#define MESG_TEXT_IPC_UserName							231
#define MESG_TEXT_IPC_UserPwd							232
#define MESG_TEXT_IPC_Channel							233
#define MESG_TEXT_IPC_Save								234
#define MESG_TEXT_IPC_Login								235
#define MESG_TEXT_Add_IPC_BySearching					236
#define MESG_TEXT_IPC_MonitorList						237
#define MESG_TEXT_IPC_MonitorListEdit					238
#define MESG_TEXT_Add_IPC_ByManual						239
#define MESG_TEXT_IPC_MonitorListDelete					240
#define MESG_TEXT_ICON_268_IPC_Resolution				241
#define MESG_TEXT_ICON_272_IPC_Preview					242
#define MESG_TEXT_ICON_270_IPC_Login					243
#define MESG_TEXT_IPC_Manage							244
#define MESG_TEXT_ICON_271_MonListManage				245
#define MESG_TEXT_ICON_275_IPC_List						246
#define MESG_TEXT_IPC_Searching							247
#define MESG_TEXT_ICON_275_IPC_IpAddr					248
#define MESG_TEXT_ICON_320_DivertVideoResolution		249
#define MESG_TEXT_ICON_321_DivertMicVolumeSet			250
#define MESG_TEXT_ICON_322_DivertSpeakerVolumeSet		251
#define MESG_TEXT_MonitorListSelect						252
#define MESG_TEXT_ICON_277_IPC_DHCP						253
#define MESG_TEXT_InputBdRmMs							254
#define MESG_TEXT_ICON_047_IpAddress					255
#define MESG_TEXT_ICON_048_CallNumber					256
#define MESG_TEXT_ICON_049_OnsiteTools					257
#define MESG_TEXT_ICON_050_PCUtility					258
#define MESG_TEXT_ICON_051_ListManage					259
#define MESG_TEXT_ICON_094_IPAssigned					260
#define MESG_TEXT_ICON_095_IPAddress					261
#define MESG_TEXT_ICON_096_IPSubnetMask 				262
#define MESG_TEXT_ICON_097_IPGateway					263
#define MESG_TEXT_ICON_098_AddressHealthCheck			264
#define	MESG_TEXT_ICON_278_Global_Nbr					265
#define	MESG_TEXT_ICON_279_RM_ADDR						266
#define	MESG_TEXT_ICON_280_MS_Number					267
#define	MESG_TEXT_ICON_281_Name							268
#define	MESG_TEXT_ICON_282_Local_Nbr					269

#define	MESG_TEXT_ICON_283_AutoCallBackRequest			270
#define	MESG_TEXT_ICON_284_SearchAndProg				271
#define	MESG_TEXT_ICON_285_ProgCall_NbrByDSCalling		272
#define	MESG_TEXT_ICON_286_AssignCall_NbrToDSButton		273
#define	MESG_TEXT_ICON_287_RestoreDefaultCall_Nbr		274

#define	MESG_TEXT_ICON_288_BD_NBR						275
#define	MESG_TEXT_ICON_289_Device						276
#define	MESG_TEXT_ICON_290_Search						277

#define	MESG_TEXT_SearchOnline							278
#define	MESG_TEXT_OnlineManage							279
#define	MESG_TEXT_ICON_291_SearchTime					280
#define	MESG_TEXT_ICON_292_Reboot						281

#define	MESG_TEXT_ICON_293_UpdateAllListBySearch		282
#define	MESG_TEXT_ICON_294_ManageImList					283
#define	MESG_TEXT_ICON_295_ManageDsList					284
#define	MESG_TEXT_ICON_296_ManageMsList					285
#define	MESG_TEXT_ICON_297_ManageIPCList				286
#define	MESG_TEXT_AddBySearch							287
#define	MESG_TEXT_AddByManual							288
#define	MESG_TEXT_ICON_099_RingTime						289
#define	MESG_TEXT_ICON_336_InternetTime					290
#define	MESG_TEXT_ICON_341_DivertLog					291		//czn_20181027
#define MESG_TEXT_ICON_342_DivertClose					292
#define	MESG_TEXT_ICON_340_CallSceneSipTest				293		//czn_20181030
#define	MESG_TEXT_ICON_343_AutoReboot					294
#define	MESG_TEXT_RegisterInprogress					295
#define	MESG_TEXT_RegisterFailNoRespose					296
#define	MESG_TEXT_RegisterFailBadCredentials			297
#define MESG_TEXT_AutoMuteSupport						298		//czn_20181117
#define MESG_TEXT_y_m_d									299
#define	MESG_TEXT_ICON_338_TimeUpdate					300
#define	MESG_TEXT_ICON_337_TimeServer					301
#define MESG_TEXT_ICON_086_RestoreInstallerData			302
#define MESG_TEXT_ICON_087_RestoreAndBackup				303
#define MESG_TEXT_ICON_300_ShortcutSetting				304
#define MESG_TEXT_ICON_301_RemoveShortcut				305
#define MESG_TEXT_ICON_302_MontorList					306
#define MESG_TEXT_DeleteIpSmartPhone					307
#define MESG_TEXT_ICON_303_SipUseDefault				308
#define MESG_TEXT_ICON_304_RemoteMonCode				309
#define MESG_TEXT_ICON_305_RemoteCallCode				310
#define MESG_TEXT_ICON_306_SipDivPwd					311
#define MESG_TEXT_MonitorTimeSet						312
#define MESG_TEXT_ShortcutSet							313
#define MESG_TEXT_ICON_307_SdFwUpdate					314
#define MESG_TEXT_ICON_308_TimeZone						315
#define MESG_TEXT_ICON_309_TimeAutoUpdate				316
#define MESG_TEXT_RegisterOk							317
#define MESG_TEXT_RegisterError							318
#define MESG_TEXT_RegisterExist							319
#define MESG_TEXT_InputMonCode							320
#define MESG_TEXT_InputCallCode							321

#define MESG_TEXT_ICON_280_DS_Number				322

#define MESG_TEXT_ICON_ParaId						323
#define MESG_TEXT_ICON_ParaName						324
#define MESG_TEXT_ICON_ParaDesc						325
#define MESG_TEXT_ICON_ParaValue					326

#define MESG_TEXT_ICON_ParaGroup0					327
#define MESG_TEXT_ICON_ParaGroup1					328
#define MESG_TEXT_ICON_ParaGroup2					329
#define MESG_TEXT_ICON_ParaGroup3					330
#define MESG_TEXT_ICON_ParaGroup4					331
#define MESG_TEXT_ICON_ParaGroup5					332

#define MESG_TEXT_AutoCallbackList					333		//czn_20190216
//czn_20190221_s
#define MESG_TEXT_Icon256_Edit						334
#define MESG_TEXT_Icon256_Sync						335	
#define MESG_TEXT_ICON_293_SyncAllListFromMaster	336
#define MESG_TEXT_ICON_SyncSipConfigFromMaster		337
#define MESG_TEXT_RingVolMute						338
//czn_20190221_e
#define MESG_TEXT_ICON_AutoSetupWizard				339
#define MESG_TEXT_ICON_IM_Extensions				340
#define MESG_TEXT_ICON_OutdoorStations				341
#define MESG_TEXT_ICON_IP_Camera					342

//czn_20190412_s
#define MESG_TEXT_ICON_AutotestSource				343
#define MESG_TEXT_ICON_AutotestTarget				344
#define MESG_TEXT_ICON_AutotestCount				345
#define MESG_TEXT_ICON_AutotestInterval				346
#define MESG_TEXT_ICON_AutotestStart				347
#define MESG_TEXT_ICON_Autotest					348
#define MESG_TEXT_TestStatCallNum					349
#define MESG_TEXT_TestStatCallPer					350
#define MESG_TEXT_TestStatDivertNum				351
#define MESG_TEXT_TestStatDivertPer				352
#define MESG_TEXT_TestStatDivertVideoNum		353
#define MESG_TEXT_ICON_AutotestLogClear		354
#define MESG_TEXT_ICON_ViewAutotestLog				355
#define MESG_TEXT_AutotestLog							356
#define MESG_TEXT_ICON_AutotestLogCopy					357
#define MESG_TEXT_ICON_AutotestTools				358
#define MESG_TEXT_ICON_AutotestStatistics				359
//czn_20190412_e
#define MESG_TEXT_ICON_100_SipEnable				360
#define MESG_TEXT_ICON_EnterCallBtnBinding			361
#define MESG_TEXT_ICON_PrivateUnlockCode			362
#define MESG_TEXT_ICON_PublicUnlockCode				363
#define MESG_TEXT_ICON_CardManagement				364
#define MESG_TEXT_SwUpgrage_NoUpdatesAvailable	365		//czn_20190506

#define MESG_TEXT_ListSyncToServer					366		//czn_20190520
#define MESG_TEXT_ListSyncFromServer				367
#define MESG_TEXT_CARD_ID							368 // 20190521
#define MESG_TEXT_CARD_Delete						369

#define MESG_TEXT_ICON_ListCheck					370	//czn_20190525
#define MESG_TEXT_ICON_ListSync						371	

#define MESG_TEXT_ICON_SelectSystemTypeVS			372
#define MESG_TEXT_ICON_SelectSystemTypeSS			373
#define MESG_TEXT_ICON_SelectSystemTypeNS			374
#define MESG_TEXT_ICON_SelectSystemTypeRestore		375
#define MESG_TEXT_InputRM_Nbr						376
#define MESG_TEXT_InputBD_RM_Nbr					377
#define MESG_TEXT_ICON_UpdateCallNbrByRes			378
#define MESG_TEXT_UpdateCallNbr_Tips1				379
#define MESG_TEXT_UpdateCallNbr_Tips2				380
#define MESG_TEXT_UpdateCallNbr_Tips3				381
#define MESG_TEXT_ICON_Summary						382
#define MESG_TEXT_ICON_Checklist					383
#define MESG_TEXT_IM_Call_Nbr_SET					384
#define MESG_TEXT_Online_Device							385
#define MESG_TEXT_R8001_Device							386
#define MESG_TEXT_Programed							387
#define MESG_TEXT_Program_TIP1						388
#define MESG_TEXT_Program_TIP2						389
#define MESG_TEXT_Program_TIP3						390
#define MESG_TEXT_ICON_RES_TO_SD					391
#define MESG_TEXT_ICON_RES_FROM_SD					392
#define MESG_TEXT_ICON_RES_TO_SYNC					393
#define MESG_TEXT_ICON_RES_FROM_SYNC				394
#define MESG_TEXT_ICON_RES_FORMAT					395
#define MESG_TEXT_ICON_RES_Manger					396
#define MESG_TEXT_SelectRES_ID						397
#define MESG_TEXT_UNUSED_RES						398
#define MESG_TEXT_UNMATCHED_RES					399
#define MESG_TEXT_ICON_RES_INFO					400
#define MESG_TEXT_R8001_Name						401
#define MESG_TEXT_R8002_Name						402
#define MESG_TEXT_R8003_Name						403
#define MESG_TEXT_R8004_Name						404
#define MESG_TEXT_R8005_Name						405
#define MESG_TEXT_R8006_Name						406
#define MESG_TEXT_R8007_Name						407
#define MESG_TEXT_R8008_Name						408
#define MESG_TEXT_R8009_Name						409
#define MESG_TEXT_R8010_Name						410
#define MESG_TEXT_R8011_Name						411
#define MESG_TEXT_R8012_Name						412
#define MESG_TEXT_ICON_BATCH_UPDATE					413
#define MESG_TEXT_ICON_BATCH_CALL_NBR				414
#define MESG_TEXT_ICON_BATCH_REBOOT					415
#define MESG_TEXT_ICON_BATCH_PWD					416

#define MESG_TEXT_RES_SYNC_ERR						417
#define MESG_TEXT_ICON_BACKUP_RESTORE				418
#define MESG_TEXT_ICON_OperationSelect				419
#define MESG_TEXT_ICON_RangeSelect					420
#define MESG_TEXT_ICON_BackupSelect					421
#define MESG_TEXT_ICON_Location						422
#define MESG_TEXT_ICON_Start						423

#define MESG_TEXT_ICON_Backup						424
#define MESG_TEXT_ICON_RestoreFromBackup			425
#define MESG_TEXT_ICON_RestoreFactorySettings		426
#define MESG_TEXT_ICON_RestoreDefaultSelective		427

#define MESG_TEXT_ICON_NewBackup					428
#define MESG_TEXT_ICON_ListOnline					429
#define MESG_TEXT_ClearRCS							430
#define MESG_TEXT_OS_BINDING						431
#define MESG_TEXT_OS_BINDING_OK						432
#define MESG_TEXT_ICON_AutoCloseAfterUnlock			433
#define MESG_TEXT_ICON_AutoUnlock					434

#define MESG_TEXT_ICON_CallVideoSelect				435
#define MESG_TEXT_ICON_IPC_Agent					436
#define MESG_TEXT_ICON_IX_Agent						437
#define MESG_TEXT_InputManagerPassword				438

#define MESG_TEXT_ICON_ExtModuleS4					439
#define MESG_TEXT_ICON_ExtModuleMK					440
#define MESG_TEXT_ICON_ExtModuleDR					441

#define MESG_TEXT_ICON_ExtModuleS4K1				442
#define MESG_TEXT_ICON_ExtModuleS4K2				443
#define MESG_TEXT_ICON_ExtModuleS4K3				444
#define MESG_TEXT_ICON_ExtModuleS4K4				445
#define MESG_TEXT_SAVE_ExtModuleS4Table				446

#define MESG_TEXT_ICON_ExtKeyCfgNoUse				447
#define MESG_TEXT_ICON_ExtKeyCfgGL					448
#define MESG_TEXT_ICON_ExtKeyCfgCancel				449
#define MESG_TEXT_ICON_ExtKeyCfgInput				450
#define MESG_TEXT_ICON_ExtKeyCfgImList				451

#define MESG_TEXT_RebootLog							452
#define	MESG_TEXT_ICON_SearchTest					453
#define MESG_TEXT_ICON_TlogRecordDevice				454
#define MESG_TEXT_ICON_TlogRecordConfig				455

#define MESG_TEXT_CusCodeUnlock						1000		
#define MESG_TEXT_CusNamelist						1001
#define MESG_TEXT_CusSearch							1002

#define MESG_TEXT_CusMoreUser						1003
#define MESG_TEXT_CusEnterPwd						1004
#define MESG_TEXT_CusIpStatic						1005
#define MESG_TEXT_CusAutoTestDevSelf					1006
#define MESG_TEXT_CusAutoTestFinish					1007
#define MESG_TEXT_CusOnsiteSearchAll					1008
#define MESG_TEXT_CusOnsiteSearchIm					1009
#define MESG_TEXT_CusOnsiteSearchOs					1010
#define MESG_TEXT_CusOnsiteSearchDs					1011
#define MESG_TEXT_CusOnsiteSearchGl					1012
#define MESG_TEXT_CusMsMaster						1013
#define MESG_TEXT_CusMsSlave						1014
#define MESG_TEXT_CusIPCListUpdate					1015
#define MESG_TEXT_CusIPCListItems					1016
#define MESG_TEXT_CusInnerCall						1017
#define MESG_TEXT_CheckIPNoRepeat					1018
#define MESG_TEXT_CheckIPRepeat						1019
#define MESG_TEXT_DoorIsOpen					1020
#define MESG_TEXT_DoorOpenError					1021
#define MESG_TEXT_RecordCH1						1022
#define MESG_TEXT_RecordCH2						1023
#define MESG_TEXT_RecordCH3						1024
#define MESG_TEXT_RecordCH4						1025
#define MESG_TEXT_RecordSet						1026
#define MESG_TEXT_ChannelApply					1027
#define MESG_TEXT_IpcScenario1					1028
#define MESG_TEXT_IpcScenario2					1029
#define MESG_TEXT_IpcScenario3					1030
#define MESG_TEXT_IpcScenario4					1031
#define MESG_TEXT_TimeStart						1032
#define MESG_TEXT_TimeEnd						1033
#define MESG_TEXT_Week							1034
#define MESG_TEXT_Select						1035
#define MESG_TEXT_Delete						1036

#define MESG_TEXT_BackupInstallerBak1			1037
#define MESG_TEXT_BackupInstallerBak0			1038
#define MESG_TEXT_DeleteInstallerBak1			1039
#define MESG_TEXT_DeleteInstallerBak0			1040
#define MESG_TEXT_InstallerBak1					1041
#define MESG_TEXT_RestoreFromOtherDevice		1042

#define MESG_TEXT_WLAN_IPAddress				1043
#define MESG_TEXT_WLAN_HW_Address				1044
#define MESG_TEXT_WLAN_SubnetMask				1045
#define MESG_TEXT_WLAN_DefaultRoute				1046

#define MESG_TEXT_ICON_RestoreInstallerDefault	1047
#define MESG_TEXT_ICON_RestoreUserDefault		1048
#define MESG_TEXT_ICON_ClearCustomizer			1049
#define MESG_TEXT_ICON_ClearRes					1050
#define MESG_TEXT_ICON_ClearUserData			1051
#define MESG_TEXT_ICON_ClearBackup				1052
#define MESG_TEXT_ICON_ViewCustomizer			1053
#define MESG_TEXT_ICON_ViewRes					1054
#define MESG_TEXT_ICON_ViewUserData				1055
#define MESG_TEXT_ICON_ViewBackup				1056

#define MESG_TEXT_RlcRelayMapInitialFloor			1057
#define MESG_TEXT_RlcRelayMapNum					1058
#define MESG_TEXT_RlcPublicTime						1059
#define MESG_TEXT_RlcPrivateTime					1060
#define MESG_TEXT_RlcRelayMode						1061
#define MESG_TEXT_RlcRelayActivateTime1				1062
#define MESG_TEXT_RlcRelayActivateTime2				1063
#define MESG_TEXT_RlcRelayDelayTime					1064
#define MESG_TEXT_PublicTime						1065
#define MESG_TEXT_PublicFloor						1066
#define MESG_TEXT_PrivateTime						1067
#define MESG_TEXT_PrivateFloor						1068
#define MESG_TEXT_CallliftEn						1069
#define MESG_TEXT_CallliftTime						1070
#define MESG_TEXT_RlcCallliftTime					1071
#define MESG_TEXT_CallliftFloor						1072
#define MESG_TEXT_Calllift							1073

#define MESG_TEXT_ICON_Overview					1074
#define MESG_TEXT_EditName_Enable					1075
#define MESG_TEXT_EditNameEnable					1076
#define MESG_TEXT_EditNameDisable					1077

#define MESG_TEXT_MainWelcome					2000
#define	MESG_TEXT_Keypad_Prompt				2001
#define MESG_TEXT_IX_NET_MODE			2002
#define MESG_TEXT_LAN_SETTING				2003
#define MESG_TEXT_WLAN_SETTING			2004
#define MESG_TEXT_NETMODE_LAN			2005
#define MESG_TEXT_NETMODE_WLAN			2006

#define MESG_TEXT_ICON_WifiDisconnect		2007
#define MESG_TEXT_ICON_WifiAutoConnect		2008
#define MESG_TEXT_ALARM						2009
#define MESG_TEXT_QuadMonitor				2010

#define MESG_TEXT_KeyboardLanguage					2011
#define MESG_TEXT_ICON_KeyBeepCtrl					2012
#define MESG_TEXT_InputIPaddress						2013
#define MESG_TEXT_ProxyIpcCancel						2014
#define MESG_TEXT_ICON_IPERF_TEST						2015
#define MESG_TEXT_CusDHCPAUTOIP							2016
#define MESG_TEXT_Networktest								2017
#define MESG_TEXT_SipNetworkSetting							2018
#define MESG_TEXT_Enable2									2019
#define MESG_TEXT_Disable2									2020
#define MESG_TEXT_NoFile						2021
#define MESG_TEXT_CusAutoipDhcp						2022
#define MESG_TEXT_VideoProxySourceIpc				2023
#define MESG_TEXT_VideoProxySourceIx				2024
#define MESG_TEXT_VideoProxySourceMyself			2025
#define MESG_TEXT_AutoSettingFoundMonitor			2026
#define MESG_TEXT_AutoSettingFoundOS				2027
#define MESG_TEXT_AutoSettingRenameTo				2028
#define MESG_TEXT_AutoSettingFoundIPC				2029
#define MESG_TEXT_ICON_MonitorDoorStation				2030
#define MESG_TEXT_DoorbellVideo						2031

#define MESG_TEXT_DS1_SetTune				2032
#define MESG_TEXT_DS2_SetTune				2033
#define MESG_TEXT_DS3_SetTune				2034
#define MESG_TEXT_DS4_SetTune				2035
#define MESG_TEXT_GuartCall_SetTune			2036
#define MESG_TEXT_ICON_DivertWithMonitor	2037		
#define	MESG_TEXT_ICON_UnlockTime			2038
#define	MESG_TEXT_ICON_UnlockMode			2039
#define	MESG_TEXT_InnerBroadcast			2040
#define	MESG_TEXT_BeBroadcast				2041
#define	MESG_TEXT_ListenTo					2042
#define	MESG_TEXT_BeListened				2043
#define 	MESG_TEXT_ICON_BabyroomEn		2044
#define	MESG_TEXT_ICON_RM_List				2045
#define	MESG_TEXT_RmPassword				2046
#define	MESG_TEXT_ICON_InnerBrdCall			2047
#define	MESG_TEXT_FLASH_CHECK				2048
#define 	MESG_TEXT_PIP						2049
#define	MESG_TEXT_PwdManager				2050
#define	MESG_TEXT_PwdManagerAdd				2051
#define MESG_TEXT_UnlockCodeLen				2052
#define MESG_TEXT_UnlockCode1				2053
#define MESG_TEXT_UnlockCode2				2054
#define MESG_TEXT_UnlockCodeMore			2055
#define MESG_TEXT_ICON_FwUpgrage_Server2	2056
#define MESG_TEXT_FwUpgrage_InstallOrNot	2057

#define MESG_TEXT_ArmmodeTitle							2060
#define MESG_TEXT_AlarmInformServiceTitle				2061
#define MESG_TEXT_ArmMode1							2062
#define MESG_TEXT_ArmMode2							2063
#define MESG_TEXT_ArmMode3							2064
#define MESG_TEXT_ArmMode4							2065
#define MESG_TEXT_AlarmInformMode1					2066
#define MESG_TEXT_AlarmInformMode2					2067
#define MESG_TEXT_AlarmInformMode3					2068
#define MESG_TEXT_AlarmInformMode4					2069
#define MESG_TEXT_AlarmTile								2070
#define MESG_TEXT_Armed								2071
#define MESG_TEXT_Disarmed								2072
#define MESG_TEXT_AlarmingZone							2073	
#define MESG_TEXT_EffectiveAfter							2074
#define MESG_TEXT_HappenAAlarming						2075

#define MESG_TEXT_RestoreWifiSettings		2076
#define MESG_TEXT_Unlock1List		2077
#define MESG_TEXT_Unlock2List		2078

#define MESG_TEXT_ICON_BACKUP_RESTORE2					2079
#define MESG_TEXT_UnlockRecord							2080
#define MESG_TEXT_CertSetup								2081
#define MESG_TEXT_CertCreate							2082
#define MESG_TEXT_CertDelete							2083
#define MESG_TEXT_CertView								2084
#define MESG_TEXT_CertCheck								2085
#define MESG_TEXT_CertLog								2086
#define MESG_TEXT_MasterAddSet							2087
#define MESG_TEXT_MasterDelSet							2088
#define MESG_TEXT_UserCardAdd							2089
#define MESG_TEXT_UserCardDel							2090
#define MESG_TEXT_UserCardDelAll						2091
#define MESG_TEXT_UserCardList							2092

#define MESG_TEXT_DivertToDev						2093
#define MESG_TEXT_CallSceneAutoMode					2094
#define MESG_TEXT_CallSceneAutoModeSw				2095
#define MESG_TEXT_CallSceneAutoStartTime				2096
#define MESG_TEXT_CallSceneAutoEndTime				2097
#define MESG_TEXT_CallSceneMode						2098
#define MESG_TEXT_CallSceneDivertList					2099
#define MESG_TEXT_CallSceneAllowList					2100
#define MESG_TEXT_CallSceneAllowNoDev				2101
#define MESG_TEXT_CallSceneAllowAllDev				2102
#define MESG_TEXT_CallSceneAllowSpecily				2103
#define MESG_TEXT_CallSceneSelectByNamelist			2104
#define MESG_TEXT_CallSceneModeAllways				2105
#define MESG_TEXT_CallSceneModeIn8H					2106
#define MESG_TEXT_CallSceneModeIfNoAnswer			2107
#define MESG_TEXT_CallSceneDivertState					2108
#define MESG_TEXT_CallSceneWaitingTime					2109

#define MESG_TEXT_ICON_710_CertSetup					10710	//>10000 for icon_id+10000
#define MESG_TEXT_ICON_500_EventRecord					10500

#endif


