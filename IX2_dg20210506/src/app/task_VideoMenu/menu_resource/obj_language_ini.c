#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "obj_language_ini.h"
#include "../../task_io_server/task_IoServer.h"
#include "define_file.h"


MENU_LANGUAGE_LIST_T menuLanguageList;
char* menuLanguageListDisplayBuffer[MENU_LANGUAGE_MAX_CNT]={NULL};

void MenuLanguageListInit(void)
{
	menuLanguageList.cnt = 0;
	
	GetMenuLanguageListName(CustomerizedLanguageSDCoardFolder, &menuLanguageList);

	if(menuLanguageList.cnt == 0)
	{
		GetMenuLanguageListName(CustomerizedLanguageNandFolder, &menuLanguageList);

		if(menuLanguageList.cnt == 0)
		{
			GetMenuLanguageListName(MENU_LANG_FILE_PATH, &menuLanguageList);
		}
	}
}


int GetMenuLanguageListName(const char * dir, MENU_LANGUAGE_LIST_T *pMenuLanguageList)
{
	char cmd[100];
	char linestr[100];
	int ret = 0;
	char *backupDir;
	char *p;
	int i;
	
	snprintf(cmd, 100, "ls -l %s | grep .csv | awk '{print $9}'", dir);
	
	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}
		
		if(strlen(linestr) == 0)
		{
			continue;
		}
		
		snprintf(pMenuLanguageList->name[pMenuLanguageList->cnt++], 100, "%s%s", dir, linestr);

		p = strstr(linestr, ".csv");
		if(p != NULL)
		{
			p[0] = 0;
		}
		if(memcmp(linestr,"REV_",4)==0)		//FOR_HEBREW
		{
			snprintf(pMenuLanguageList->displayName[pMenuLanguageList->cnt-1], 30, "%s", linestr+4);
			pMenuLanguageList->invert_flag[pMenuLanguageList->cnt-1] = 1;
		}
		else
		{
			snprintf(pMenuLanguageList->displayName[pMenuLanguageList->cnt-1], 30, "%s", linestr);
			pMenuLanguageList->invert_flag[pMenuLanguageList->cnt-1] = 0;
		}
		
		if(pMenuLanguageList->cnt >= MENU_LANGUAGE_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}

int GetMenuLanguageListCnt(void)
{
	return menuLanguageList.cnt;
}

int GetMenuLanguageListRecord(int index, char* record)
{
	if(index >= menuLanguageList.cnt)
	{
		record[0] = 0;
		return -1;
	}

	strcpy(record, menuLanguageList.name[index]);
	return 0;
}

char** GetMenuLanguageListBuffer(void)
{
	int i;

	for(i = 0; i < menuLanguageList.cnt; i++)
	{
		menuLanguageListDisplayBuffer[i] = menuLanguageList.displayName[i];
	}
	
	return menuLanguageListDisplayBuffer;
}

int GetMenuLanguageIndex(void)
{
	char record[100] = {0};
	int i;
	
    API_Event_IoServer_InnerRead_All(MENU_LANGUAGE_SELECT, record);

	for(i = 0; i < menuLanguageList.cnt; i++)
	{
		if(!strcmp(record, menuLanguageList.name[i]))
		{
			break;
		}
	}
	
	return i;
}

int GetMenuLanguageFileName(char* fileName)
{
    API_Event_IoServer_InnerRead_All(MENU_LANGUAGE_SELECT, fileName);
	
	return 0;
}

int GetMenuLanguageDisplayName(char* name)
{
	char record[100] = {0};
	int i;
	
    API_Event_IoServer_InnerRead_All(MENU_LANGUAGE_SELECT, record);

	for(i = 0; i < menuLanguageList.cnt; i++)
	{
		if(!strcmp(record, menuLanguageList.name[i]))
		{
			break;
		}
	}

	if(name != NULL)
	{
		strcpy(name, menuLanguageList.displayName[i]);
	}
	
	return 0;
}



int SetMenuLanguage(int index)
{
	if(index >= menuLanguageList.cnt)
	{
		return -1;
	}

    API_Event_IoServer_InnerWrite_All(MENU_LANGUAGE_SELECT, menuLanguageList.name[index]);
	
	load_menu_unicode_resource();
	
	return 0;
}

int SetMenuLanguageDefault(void)
{

	load_menu_unicode_resource(0);
	API_Event_IoServer_InnerWrite_All(MENU_LANGUAGE_SELECT, menuLanguageList.name[0]);
	
	return 0;
}

int GetMenuLanguageInvertFlag(void)		//FOR_HEBREW
{
	int index;
	index = GetMenuLanguageIndex();
	
	if(index >= menuLanguageList.cnt)
	{
		return 0;
	}
	
	return menuLanguageList.invert_flag[index];
}
