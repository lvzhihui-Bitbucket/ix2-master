
#include "task_survey.h"
#include "sys_msg_process.h"
#include "task_VideoMenu.h"
#include "bmplib.h"
#include "MENU_public.h"

extern gui_base_attr_t		gui_attr;


#ifdef PID_IX47
	int m_PANEL_TYPE = IX481H;
#elif defined(PID_DX470)	
	int m_PANEL_TYPE = IX481H;
#elif defined(PID_DX482)	
	int m_PANEL_TYPE = IX481H;

//#elif defined(PID_DX470_V25)
//	int m_PANEL_TYPE = DX470_V25;
#else

	int m_PANEL_TYPE = IX47;
#endif	

int bkgd_w 	= 0;
int bkgd_h	= 0;

const int PANEL_RESOLUTION_TAB[IXTYPE_MAX*2]=
{
// 	width, 	height
	1280,	800,		// IX481H
	800,	1280,		// IX481V
	1024,	600,		// IX482
	480,	800,		// IX850
	1024,	600,		// IX47
	600,	1024,		// IX470V
	1024,	600,		// DX470 6
	600,	1024,		// DX470V 7
	1024,	600,		// DX482
	1024,	600,		// IX482se
	// lzh_20231115_s
	//800,	480,		// DX470_V25	// ips panel
	1024,	600,		// DX470_V25	// not ips
	// lzh_20231115_e
};

int get_pane_type(void)
{
	return m_PANEL_TYPE;
}

int set_pane_type(int type)
{
	printf("set_pane_type = %d\n", type);
	m_PANEL_TYPE = type;
	bkgd_w = PANEL_RESOLUTION_TAB[m_PANEL_TYPE*2+0];
	bkgd_h = PANEL_RESOLUTION_TAB[m_PANEL_TYPE*2+1];	
	return 0;
}

//�������Ĳ˵�������
#define	MAX_MENU_LIST		100
struOneMenuData 			OneMenuData;
IMG_ADDR_TAB_NODE			img_info;
#if 0
char 						osd_dat_path[] 	= "/mnt/nand1-2/res/res.dat";
char 						uim_bin_path[] 	= "/mnt/nand1-2/res/bkgd.idx";
char 						uii_bin_path[] 	= "/mnt/nand1-2/res/icon.idx";
char 						uit_bin_path[] 	= "/mnt/nand1-2/res/text.idx";
char 						uis_bin_path[] 	= "/mnt/nand1-2/res/spri.idx";
char 						mis_bin_path[] 	= "/mnt/nand1-2/res/misc.idx";
char 						ukz_bin_path[] 	= "/mnt/nand1-2/res/menu.dat";
#endif
char 						osd_dat_path[100];
char 						uim_bin_path[100];
char 						uii_bin_path[100];
char 						uit_bin_path[100];
char 						uis_bin_path[100];
char 						mis_bin_path[100];
char 						ukz_bin_path[100];

char*						uim_pbuf 		= NULL;
char*						uii_pbuf 		= NULL;
char*						uit_pbuf 		= NULL;
char*						uis_pbuf 		= NULL;
char*						mis_pbuf 		= NULL;
char*						ukz_pbuf 		= NULL;

// lzh_20210909_s
char 						osd_dat_path_custom[100];
char 						uim_bin_path_custom[100];
char 						uii_bin_path_custom[100];
char 						uit_bin_path_custom[100];
char 						uis_bin_path_custom[100];
char 						mis_bin_path_custom[100];
char 						ukz_bin_path_custom[100];

char*						uim_pbuf_custom 		= NULL;
char*						uii_pbuf_custom 		= NULL;
char*						uit_pbuf_custom 		= NULL;
char*						uis_pbuf_custom 		= NULL;
char*						mis_pbuf_custom 		= NULL;
char*						ukz_pbuf_custom 		= NULL;
// lzh_20210909_e
/*******************************************************************************************
 * @fn:		menu_resource_initial
 *
 * @brief:	gui��Դ�ĳ�ʼ�����ֱ������Դ�ļ��������ļ����ڴ�??(��ͼ������icon������text������text��չ������menu���в���)
 *
 * @param:  none
 *
 * @return: 0
 *******************************************************************************************/
int menu_resource_initial(void)
{
	FILE *fp;
	int fd;
	struct stat filestat;
	char* menuPath;

	menuPath = (get_pane_type() == IX481V || get_pane_type() == IX470V || get_pane_type() == DX470V) ? MENU_V_FILE_PATH : MENU_FILE_PATH;	
	snprintf(osd_dat_path, 100, "%sres.dat", menuPath);
	snprintf(uim_bin_path, 100, "%sbkgd.idx", menuPath);
	snprintf(uii_bin_path, 100, "%sicon.idx", menuPath);
	snprintf(uit_bin_path, 100, "%stext.idx", menuPath);
	snprintf(uis_bin_path, 100, "%sspri.idx", menuPath);
	snprintf(mis_bin_path, 100, "%smisc.idx", menuPath);
	snprintf(ukz_bin_path, 100, "%smenu.dat", menuPath);
	// lzh_20210909_s
	menuPath = (get_pane_type() == IX481V || get_pane_type() == IX470V || get_pane_type() == DX470V) ? MENU_V_FILE_PATH_CUSTOM : MENU_FILE_PATH_CUSTOM;	
	snprintf(osd_dat_path_custom, 100, "%sres.dat", menuPath);
	snprintf(uim_bin_path_custom, 100, "%sbkgd.idx", menuPath);
	snprintf(uii_bin_path_custom, 100, "%sicon.idx", menuPath);
	snprintf(uit_bin_path_custom, 100, "%stext.idx", menuPath);
	snprintf(uis_bin_path_custom, 100, "%sspri.idx", menuPath);
	snprintf(mis_bin_path_custom, 100, "%smisc.idx", menuPath);
	snprintf(ukz_bin_path_custom, 100, "%smenu.dat", menuPath);
	// lzh_20210909_e

	//load bkgd	idx
	fp = fopen(uim_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open ukz bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uim_pbuf = malloc(filestat.st_size);
		fread( uim_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	// custom
	fp = fopen(uim_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open custom ukz bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uim_pbuf_custom = malloc(filestat.st_size);
		fread( uim_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}

	//load icon idx
	fp = fopen(uii_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uii_pbuf = malloc(filestat.st_size);
		fread( uii_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	//custom
	fp = fopen(uii_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open custom uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uii_pbuf_custom = malloc(filestat.st_size);
		fread( uii_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}

	//load text idx
	fp = fopen(uit_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uit_pbuf = malloc(filestat.st_size);
		fread( uit_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	//custom
	fp = fopen(uit_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uit_pbuf_custom = malloc(filestat.st_size);
		fread( uit_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}

	//load sprite idx
	fp = fopen(uis_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open uis bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uis_pbuf = malloc(filestat.st_size);
		fread( uis_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	//custom
	fp = fopen(uis_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open custom uis bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		uis_pbuf_custom = malloc(filestat.st_size);
		fread( uis_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}


	//load misc idx
	fp = fopen(mis_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		mis_pbuf = malloc(filestat.st_size);
		fread( mis_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	//custom
	fp = fopen(mis_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open custom uii bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		mis_pbuf_custom = malloc(filestat.st_size);
		fread( mis_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}
	
	//menu ukz
	fp = fopen(ukz_bin_path, "rb");
	if( fp == NULL )
	{
		eprintf("open ukz bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		ukz_pbuf = malloc(filestat.st_size);
		fread( ukz_pbuf, filestat.st_size, 1, fp );
		fclose(fp);
	}
	//custom
	fp = fopen(ukz_bin_path_custom, "rb");
	if( fp == NULL )
	{
		eprintf("open custom ukz bin file failed!\n");
	}
	else
	{
		fd = fileno(fp);
		fstat(fd,&filestat);
		ukz_pbuf_custom = malloc(filestat.st_size);
		fread( ukz_pbuf_custom, filestat.st_size, 1, fp );
		fclose(fp);
	}

	osd_Initial();

	init_inner_font();
	init_ext_font(get_pane_type());
	init_log_win();
	//export_global_mesg_text_file();
	load_menu_unicode_resource(0);
			
	return 0;	
}

/*******************************************************************************************
 * @fn:		GetBkgdImageInfo
 *
 * @brief:	�õ�ָ����ͼ��������Ϣ�������������Ƶ����ݰ�
 *
 * @param:  img_idx - ��ͼ���к�
 *
 * @return: 0
 *******************************************************************************************/
int GetBkgdImageInfo( int img_idx )
{
	if( OneMenuData.customflag && uim_pbuf_custom != NULL )
	{
		memcpy( (unsigned char*)&img_info, 	uim_pbuf_custom + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
		{
			//printf("uim_pbuf_custom have no background[%d] image+++++++++++\n",img_idx);

			memcpy( (unsigned char*)&img_info, 	uim_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
			if( img_info.normal == -1 )
				return -1;
			else
				return 0;
		}
		else
			return 1;
	}
	else
	{
		memcpy( (unsigned char*)&img_info, 	uim_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
			return -1;
		else
			return 0;
	}
}

/*******************************************************************************************
 * @fn:		GetIconImageInfo
 *
 * @brief:	�õ�ָ��icon��������Ϣ
 *
 * @param:  img_idx - icon���ܺ�
 *
 * @return: 0
 *******************************************************************************************/
int GetIconImageInfo( int img_idx )
{
	if( OneMenuData.customflag && uii_pbuf_custom != NULL )
	{
		memcpy( (unsigned char*)&img_info, 	uii_pbuf_custom + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
		{
			printf("uii_pbuf_custom have no icon[%d] image++++++++++++\n",img_idx);

			memcpy( (unsigned char*)&img_info, 	uii_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
			if( img_info.normal == -1 )
				return -1;
			else
				return 0;
		}
		else
			return 1;
	}
	else
	{
		memcpy( (unsigned char*)&img_info, 	uii_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
			return -1;
		else
			return 0;
	}
}

/*******************************************************************************************
 * @fn:		GetTextImageInfo
 *
 * @brief:	�õ�ָ��icon�����Ե�������Ϣ
 *
 * @param:  img_idx - icon��Ӧtext���ܺ�
 * @param:  language_cnt - icon��Ӧtext���ܺŵ����Ա��??
 *
 * @return: 0
 *******************************************************************************************/
int GetTextImageInfo( int img_idx, int language_cnt )
{
	if( OneMenuData.customflag && uit_pbuf_custom != NULL )
	{
		memcpy( (unsigned char*)&img_info,	uit_pbuf_custom + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		#if 1
		//if( img_info.normal == -1 )
		if( img_info.width== -1 )
		{
			//printf("uit_pbuf_custom have no text[%d] image++++++++++++\n",img_idx);

			memcpy( (unsigned char*)&img_info,	uit_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
			if( img_info.normal == -1 )
				return -1;
			else
				return 0;
		}
		else
		#endif
			return 1;
	}
	else
	{
		memcpy( (unsigned char*)&img_info,	uit_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
			return -1;
		else
			return 0;
	}
}

/*******************************************************************************************
 * @fn:		GetSpriteImageInfo
 *
 * @brief:	�õ�ָ��sprite�����Ե�������Ϣ
 *
 * @param:  img_idx - icon��Ӧsprite���ܺ�
 * @param:  language_cnt - icon��Ӧsprite���ܺŵ����Ա��??
 *
 * @return: 0
 *******************************************************************************************/
int GetSpriteImageInfo( int img_idx, int language_cnt )
{
	if( OneMenuData.customflag && uis_pbuf_custom != NULL )
	{
		memcpy( (unsigned char*)&img_info,	uis_pbuf_custom + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
		{
			//printf("uis_pbuf_custom have no sprite[%d] image++++++++++++\n",img_idx);

			memcpy( (unsigned char*)&img_info,	uis_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
			if( img_info.normal == -1 )
				return -1;
			else
				return 0;
		}
		else
			return 1;
	}
	else
	{
		//printf("image%d img_info:addr0=%04x,addr1=%04x,iconh=%d,iconv=%d\n", img_idx,img_info.normal,img_info.selected,img_info.width,img_info.height);
		memcpy( (unsigned char*)&img_info,	uis_pbuf + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
		if( img_info.normal == -1 )
		{
			if(uis_pbuf_custom != NULL)
			{
				memcpy( (unsigned char*)&img_info,	uis_pbuf_custom + img_idx*IMAGE_NODE_SIZE, IMAGE_NODE_SIZE );
				if( img_info.normal == -1 )
				{
					;
				}
				else
					return 1;
			}	
			return -1;
		}
		else
			return 0;
		
	}
}

/*******************************************************************************************
 * @fn:		GetPicDispXY
 *
 * @brief:	���ݶ��뷽ʽ��ȡ��ʼ�����??
 *
 * @param:  nx, ny ��ȡ����ʼ�����??
 *
 * @return: 0
 *******************************************************************************************/
static void GetPicDispXY(int dispType, int ox, int oy, int ow, int oh, int w, int h, int* nx, int* ny)
{
	switch(dispType)
	{
		case ALIGN_LEFT_DOWN:
			*nx = ox;
			*ny = oy + (oh - h);
			break;
		case ALIGN_LEFT_UP:
			*nx = ox;
			*ny = oy;
			break;
		case ALIGN_LEFT:
			*nx = ox;
			*ny = oy + (oh - h)/2;
			break;
		case ALIGN_RIGHT:
			*nx = ox + (ow - w);
			*ny = oy + (oh - h)/2;
			break;
		case ALIGN_RIGHT_DOWN:
			*nx = ox + (ow - w);
			*ny = oy + (oh - h);
			break;
		case ALIGN_RIGHT_UP:
			*nx = ox + (ow - w);
			*ny = oy;
			break;
		case ALIGN_UP:
			*nx = ox + (ow - w)/2;
			*ny = oy;
			break;
		case ALIGN_DOWN:
			*nx = ox + (ow - w)/2;
			*ny = oy + (oh - h);
			break;
		case ALIGN_MIDDLE:
			*nx = ox + (ow - w)/2;
			*ny = oy + (oh - h)/2;
			break;
			
		default:
			*nx = ox;
			*ny = oy;
			break;
	}
	
	//dprintf("%d [%d, %d, %d, %d] [%d, %d, %d, %d]\n", dispType, ox, oy, ow, oh, w, h, *nx, *ny);
}
/*******************************************************************************************
 * @fn:		ReadMenuDataFile
 *
 * @brief:	�Ӳ˵����������ļ��ж�ȡ����
 *
 * @param:  baseaddr 	- �ļ���ƫ����
 * @param:  ptrBuffer 	- ��ȡ�����ݻ���
 * @param:  length 		- ��ȡ�����ݳ���
 *
 * @return: 0
 *******************************************************************************************/
unsigned char ReadMenuDataFile(unsigned long baseaddr, unsigned char* ptrBuffer,unsigned short length)
{
	memcpy( ptrBuffer, ukz_pbuf+baseaddr, length );
	return 0;
}

unsigned char ReadMenuDataFile_custom(unsigned long baseaddr, unsigned char* ptrBuffer,unsigned short length)
{
	if( ukz_pbuf_custom != NULL )
	{
		memcpy( ptrBuffer, ukz_pbuf_custom+baseaddr, length );
		return 0;
	}
	else
		return -1;
}

/*******************************************************************************************
 * @fn:		GetCurLanguageMenuStartAddr
 *
 * @brief:	�õ���ǰ���԰��Ĳ˵���������ʼ��ַ
 *
 * @param:  none
 *
 * @return: 0/unavaliable,else/the menu start address
 *******************************************************************************************/
unsigned long GetCurLanguageMenuStartAddr(unsigned short menu_cnt )
{
	unsigned long 	lFlashAddr;
	unsigned long 	lMenuStartAddr;
	
	// first search customization menu data

	//�õ�flash�����԰���ƫ�Ƶ�ַ
	lFlashAddr = 128;		//�����ֿ���ǰ���??128���ֽ�
	//�õ���ǰ���԰���ƫ�Ƶ�ַ
	lFlashAddr += 32;				//�������԰�ͷ
	lFlashAddr += (menu_cnt*4);		//������ǰ�˵��ĵ�ַָ�봦
	//��ȡ��ǰ�˵�������ʼ��ַ
	lMenuStartAddr = 0;
	ReadMenuDataFile_custom(lFlashAddr,(unsigned char*)&lMenuStartAddr, 4);
	if( lMenuStartAddr ) //�õ���ǰ�˵�������ʼ��ֵַΪ0����ʾ�޴˲˵�
	{
		ReadMenuDataFile_custom(lMenuStartAddr, (unsigned char*)&OneMenuData, MAX_ONE_MENU_BYTES);	// 6ms
		if( menu_cnt == OneMenuData.fnt_menu_id)
		{
			OneMenuData.customflag = 1;
			printf("OneMenuData.customflag = 1 >>>>>>>>>>>>>>>>>>>>>\n");
			return (lMenuStartAddr+MAX_ONE_MENU_BYTES);
		}
	}

	// then search normal menu data

	//�õ�flash�����԰���ƫ�Ƶ�ַ
	lFlashAddr = 128;		//�����ֿ���ǰ���??128���ֽ�
	//�õ���ǰ���԰���ƫ�Ƶ�ַ
	lFlashAddr += 32;				//�������԰�ͷ
	lFlashAddr += (menu_cnt*4);		//������ǰ�˵��ĵ�ַָ�봦
	//��ȡ��ǰ�˵�������ʼ��ַ
	lMenuStartAddr = 0;
	ReadMenuDataFile(lFlashAddr,(unsigned char*)&lMenuStartAddr, 4);
	if( lMenuStartAddr ) //�õ���ǰ�˵�������ʼ��ֵַΪ0����ʾ�޴˲˵�
	{
		ReadMenuDataFile(lMenuStartAddr, (unsigned char*)&OneMenuData, MAX_ONE_MENU_BYTES);	// 6ms
		if( menu_cnt == OneMenuData.fnt_menu_id)
		{
			OneMenuData.customflag = 0;
			printf("OneMenuData.customflag = 0 >>>>>>>>>>>>>>>>>>>>>\n");
			return (lMenuStartAddr+MAX_ONE_MENU_BYTES);
		}
	}
	else
		return 0;
}

/*******************************************************************************************
 * @fn:		LoadCurLanguageLabel
 *
 * @brief:	�õ���ǰ���԰���Label
 *
 * @param:  none
 *
 * @return: 0
 *******************************************************************************************/ 
#define LABEL_FONT_W		   12 //16
#define LABEL_FONT_H		   16 //24

int LoadCurLanguageLabel(unsigned char label_index,unsigned char label_sn, int x, int y, int color)
{	
	unsigned long 	lFlashAddr;
	unsigned long 	lLabelStartAddr;
	LABEL_STRUCT	tmpLabelStruct;
	//�õ�flash�����԰���ƫ�Ƶ�ַ
	lFlashAddr = 128 + GetCurLanguage()*sizeof(LANGUAGE_TYPE);		//�����ֿ���ǰ���??128���ֽ�
	lFlashAddr += 32;				//�������԰�ͷ
	lFlashAddr += 400;				//�����˵�������
	lFlashAddr += label_index*4;	//������ǰ�˵��ĵ�ַָ�봦
	
	//��ȡ��ǰ�˵����ĸ�����label����ʼ��ַ
	if( OneMenuData.customflag )
		ReadMenuDataFile_custom(lFlashAddr,(unsigned char*)&lLabelStartAddr, 4);
	else
		ReadMenuDataFile(lFlashAddr,(unsigned char*)&lLabelStartAddr, 4);

	//��ȡ��ǰ�˵����ĸ�����label������������
	if( OneMenuData.customflag )
		ReadMenuDataFile_custom(lLabelStartAddr, (unsigned char*)&tmpLabelStruct, sizeof(LABEL_STRUCT)-4 ); //���������ݲ���ȡ
	else
		ReadMenuDataFile(lLabelStartAddr, (unsigned char*)&tmpLabelStruct, sizeof(LABEL_STRUCT)-4 ); //���������ݲ���ȡ

	printf("label_sn=%d,label_id=%d,label_cnt=%d,label_widt=%d\n",label_sn,tmpLabelStruct.label_id,tmpLabelStruct.label_cnt,tmpLabelStruct.label_width);

	//label��Ų��ܳ������ֵ
	if( label_sn >= tmpLabelStruct.label_cnt ) 
		return -1;
	
	int label_row_bytes = (tmpLabelStruct.label_width*LABEL_FONT_W + 31)/32;  // 4�ֽڶ���
	label_row_bytes <<= 2; // ÿ������4�ֽ�
	lFlashAddr = lLabelStartAddr + sizeof(LABEL_STRUCT) + label_sn*label_row_bytes*LABEL_FONT_H;

	//��ʾlable������
	draw_bitmap( OSD_LAYER_STRING, x, y, tmpLabelStruct.label_width*LABEL_FONT_W, LABEL_FONT_H, color, COLOR_KEY, ukz_pbuf+lFlashAddr, label_row_bytes );
	//update display
	UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,tmpLabelStruct.label_width*LABEL_FONT_W, LABEL_FONT_H,0);			
	return 0;
}


/*******************************************************************************************
 * @fn:		LoadMenuBackground
 *
 * @brief:	��ʾָ����ŵĵ��?
 *
 * @param:  x 			- ��ͼ��ʾ����Ļ��x����
 * @param:  y			- ��ͼ��ʾ����Ļ��y����
 * @param:	bkgdcnt 	- ��ͼ�ǵı��??
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuBackground(int bkgdcnt, int x, int y)
{
	FILE*	fp;
	int 	xsize, ysize,offset_dat;

	if( GetBkgdImageInfo(bkgdcnt) == 1 )
		fp = fopen( osd_dat_path_custom, "rb" );
	else
		fp = fopen( osd_dat_path, "rb" );

	if( fp == NULL )
	{
		printf("open osd dat file failed!\n");
		return -1;
	}

	xsize 		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;

	if( offset_dat != -1 )
	{
		fseek(fp,offset_dat,SEEK_SET);
		printf("background x=%d,y=%d,xsize=%d,ysize=%d\n",x,y,xsize,ysize);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);
		//display one background image		
		WriteOneOsdLayer( OSD_LAYER_BKGD, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
	}
	fclose(fp);	
	fp = NULL;	
	return 0;
}


/*******************************************************************************************
 * @fn:		LoadMenuSprite
 *
 * @brief:	��ʾָ����ŵĵ��?
 *
 * @param:  x 			- ��ͼ��ʾ����Ļ��x����
 * @param:  y			- ��ͼ��ʾ����Ļ��y����
 * @param:	bkgdcnt 	- ��ͼ�ǵı��??
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuSprite(int bkgdcnt, int x, int y, int language_cnt )
{
	FILE*	fp;
	int 	xsize, ysize,offset_dat;
		
	//GetBkgdImageInfo(bkgdcnt);
	if( GetSpriteImageInfo( bkgdcnt, language_cnt ) == 1 )
		fp = fopen( osd_dat_path_custom, "rb" );
	else
		fp = fopen( osd_dat_path, "rb" );

	if( fp == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;		
	}

	xsize 		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;

	if( offset_dat != -1 )
	{
		fseek(fp,offset_dat,SEEK_SET);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);		
		WriteOneOsdLayer( OSD_LAYER_SPRITE, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
	}
	fclose(fp);	
	fp = NULL;		
	return 0;
}

/*******************************************************************************************
 * @fn:		ClearMenuSprite
 *
 * @brief:	���ָ����ŵĵ�ͼ
 *
 * @param:  x 			- ��ͼ��ʾ����Ļ��x����
 * @param:  y			- ��ͼ��ʾ����Ļ��y����
 * @param:	bkgdcnt 	- ��ͼ�ǵı��??
 *
 * @return: 0
 *******************************************************************************************/
int ClearMenuSprite(int layer,int bkgdcnt, int x, int y, int language_cnt, int mode)
{
	int 	xsize, ysize;

	//GetBkgdImageInfo(bkgdcnt);
	GetSpriteImageInfo( bkgdcnt, language_cnt );

	xsize 		= img_info.width;
	ysize		= img_info.height;
	
	memset( gui_attr.osdlayer_tmp, mode, xsize*ysize*gui_attr.bpp_len);

	WriteOneOsdLayer( layer, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
	return 0;
}

/*******************************************************************************************
 * @fn:		LoadMenuAllIcons
 *
 * @brief:	��ʾ��ǰ�˵�������icon
 *
 * @param:  osdlayer - ��ʾ�Ĳ�����0/��ͼ�㣬1/osd��
 *
 * @return: 0
 *******************************************************************************************/
int LoadMenuAllIcons( void )
{
	FILE*	fp;
	FILE*   fp1;
	FILE*	fp2;
	int 	i, x, y, xsize, ysize;
	int	 	offset_dat;

	fp1 = fopen( osd_dat_path_custom, "rb" );
	fp2 = fopen( osd_dat_path, "rb" );

	if( fp2 == NULL  )
	{
		eprintf("open osd dat file failed!\n");
		return -1;		
	}

	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( GetIconImageInfo(OneMenuData.ListCtrl[i].code.index) == 1 )
			fp = fp1;
		else
			fp = fp2;

		x 			= OneMenuData.ListCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
		y 			= OneMenuData.ListCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
		xsize 		= img_info.width;
		ysize		= img_info.height;		
		offset_dat	= img_info.normal;

		if( offset_dat == -1 )
			continue;

		fseek(fp,offset_dat,SEEK_SET);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);
		//display one background image
		WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
	}
	
	if( fp1 ) fclose(fp1);
	if( fp2 ) fclose(fp2);
	fp = NULL;

	return i;
}

/*******************************************************************************************
 * @fn:		LoadMenuAllTexts
 *
 * @brief:	��ʾ��ǰ�˵�������text
 *
 * @param:  osdlayer 		- ��ʾ�Ĳ�����0/��ͼ�㣬1/osd��
 * @param:  language_cnt 	- ��ʾ��text�����Ա��??
 *
 * @return: 0
 *******************************************************************************************/

int display_unicode_string_align_center( int layer_id, int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* textstring, int len, int max_width, int max_height, int fnt_type );

int LoadMenuAllTexts( int language_cnt)
{
	FILE*	fp;
	int i;

	if( OneMenuData.customflag && osd_dat_path_custom != NULL )
		fp = fopen( osd_dat_path_custom, "rb" );
	else
		fp = fopen( osd_dat_path, "rb" );

	if( fp == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;		
	}
		
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		DisplayOneMenuText(fp,i,language_cnt );
	}
	fclose(fp);
	fp = NULL;	
	return i;
}

unsigned char GetCurMenuSpi(unsigned short uMenuCnt)
{
	unsigned short spiId;

	set_menu_with_video_off();
	#if defined(PID_IX850)	
	spiId = uMenuCnt;
	#else
	switch(uMenuCnt)
	{
		case MENU_001_MAIN:
			spiId = 1;
			break;
		case MENU_002_MONITOR:
			spiId = 2;
			break;
		case MENU_014_NAMELIST:
			spiId = 3;
			break;
		case MENU_004_CALL_RECORD:
			spiId = 4;
			break;
		case MENU_027_CALLING2:
		case MENU_028_MONITOR2:
		case MENU_056_IPC_MONITOR:
		case MENU_117_RecQuad:
		case MENU_142_DX_MONITOR:	
		case MENU_141_ListenTalk:		
			spiId = 5;
			set_menu_with_video_on();			
			break;
		case MENU_113_MonQuart:
			spiId = 6;
			set_menu_with_video_on();			
			break;
		case MENU_017_KEYPAD_CHAR:
		case MENU_019_KEYPAD:
			spiId = 8;
			break;
		case MENU_018_KEYPAD_NUM:
		case MENU_137_MulLangKeyPad_Num:
			spiId = 9;
			break;
		case MENU_090_CardManage:	
		case MENU_100_CardList:
		case MENU_101_CardAdd:
		case MENU_102_CardDelAll:
		case MENU_103_CardInfo:
			spiId = 10;
			break;
		case MENU_011_SETTING:
			spiId = 11;
			break;
		case MENU_015_INSTALL_SUB:
			spiId = 12;
			break;
		case MENU_026_PLAYBACK:
		case MENU_122_DvrPlay:
			spiId = 13;
			set_menu_with_video_on();			
			break;
		#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
			case MENU_156_PLAYBACK2:
			spiId = 13;
			set_menu_with_video_on();			
			break;
		#endif
		case MENU_013_CALLING:
		case MENU_031_CALLING3:
		case MENU_032_CALLING4:
		//case MENU_141_ListenTalk:	
			spiId = 14;
			break;
		case MENU_022_ONLINE_MANAGE:
		case MENU_074_RemoteParaManage:
			spiId = 15;
			break;
		case MENU_054_IPC_SETTING:
			spiId = 16;
			break;
		case MENU_108_QuickAccess:
			spiId = 17;
			break;
		case MENU_109_Info:
			spiId = 18;
			set_menu_with_video_on();			
			break;
		case MENU_030_SHORTCUT_SET:
			spiId = 20;
			break;
		case MENU_125_NM_MAIN:
		case MENU_126_NM_LanSetting:	
			spiId = 21;
			break;
		case MENU_127_NM_WlanSetting:
		case MENU_128_NM_WifiInfo:	
			if(get_pane_type()==6 || get_pane_type()==7 || get_pane_type()==8 )
			{
				spiId = 7;
			}
			else
			{
				spiId = 21;
			}
			break;
		case MENU_057_IPC_LIST:
			spiId = 22;
			break;
		case MENU_136_MulLangKeyPad:
			spiId = 23;
			break;
		#if defined(PID_DX470)||defined(PID_DX482)
		case MENU_144_RM_Display:
			spiId = 24;
			break;
		#endif
		#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
		case MENU_149_Arming:
			spiId = 25;
			break;
		case MENU_153_DisarmingSubKeyPad:
			spiId=26;
			break;
		case MENU_154_DisarmingSubZoneDisp:
			spiId=27;
			break;
		case MENU_010_ABOUT:

			if(get_pane_type()==5)
				spiId = 7;
			else
				spiId=28;
			break;	
		#endif
		default:
			spiId = 7;
			break;
	}
	#endif
	return spiId;
}


/*******************************************************************************************
 * @fn:		DisplayOneMenu
 *
 * @brief:	��ʾһ��ָ���Ĳ˵�
 *
 * @param:  uMenuCnt	- �˵����??
 * @param:  uSubMenuCnt - �Ӳ˵����?? (Ϊ0��ʾ���Ӳ˵�)
 *
 * @return: 0
 *******************************************************************************************/

unsigned short DisplayOneMenu(unsigned short uMenuCnt,unsigned short uSubMenuCnt)
{
	unsigned short i,icon_tab,icon_win;
	unsigned long 	lFontPageAddr;

	OneMenuData.menu_enable = 0;

	printf("SwitchOneMenu id= %d\n", uMenuCnt);

	//if(uMenuCnt > 11 && uMenuCnt < 27 || uMenuCnt >28)
	//	uMenuCnt = GetCurMenuSpi(uMenuCnt);
	//printf("DisplayOneMenu id= %d\n", uMenuCnt);
	//�õ�һ���˵����ַ���icon����
	lFontPageAddr = GetCurLanguageMenuStartAddr(GetCurMenuSpi(uMenuCnt));

	if( lFontPageAddr == 0 ) 
	{
		printf("have no menu data!\n\r");
		return 0;
	}
	if(uMenuCnt == 1)
	{	
		
		if(JudgeIsInstallerMode()==0)
		{
			
			for( i = 0; i < OneMenuData.icon_max; i++ )
			{
				if(OneMenuData.ListCtrl[i].code.index == ICON_ExitProg)
				{	
					
					break;
				}
			}
			if(i==(OneMenuData.icon_max-1))
			{
				;
			}
			else
			{
				i++;
				for(; i < OneMenuData.icon_max; i++ )
				{
					OneMenuData.ListCtrl[i-1] = OneMenuData.ListCtrl[i];
					//OneMenuData.TextCtrl[i-1] = OneMenuData.TextCtrl[i];
				}
			}
			for( i = 0; i < OneMenuData.icon_max; i++ )
			{
				if(OneMenuData.TextCtrl[i].code.index == ICON_ExitProg)
				{	
					
					break;
				}
			}
			if(i==(OneMenuData.icon_max-1))
			{
				OneMenuData.icon_max --;
			}
			else
			{
				i++;
				for(; i < OneMenuData.icon_max; i++ )
				{
					//OneMenuData.ListCtrl[i-1] = OneMenuData.ListCtrl[i];
					OneMenuData.TextCtrl[i-1] = OneMenuData.TextCtrl[i];
					if(i==(OneMenuData.icon_max-1))
					{
						OneMenuData.icon_max --;
						break;
					}
				}
			}
		}
		#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
		AlarmingIconDisp_Deal();
		#endif
	}
	//���浱ǰFONT ����
	OneMenuData.cntMenuMode = 0;
	//disp all osd win control
	OneMenuData.maxIcon = 0;
	OneMenuData.maxNIcon = 0;
	
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		#if 0		// WinTab������Ϊ�����������ԣ�������Ϊ����sprite��Ϣ
		//get tab serial list			
		icon_tab = OneMenuData.ListCtrl[i].code.WinTab;
		if( icon_tab >= 1 )
		{
			OneMenuData.tabIcon[icon_tab-1] = i;
			OneMenuData.maxIcon++;
		}
		else if( icon_tab == 0 )
		#endif 
		{
			OneMenuData.ntabIcon[OneMenuData.maxNIcon++] = i;
		}
	}
	//get curIcon and oldIcon
	OneMenuData.curIcon = 0;
	OneMenuData.oldIcon = OneMenuData.maxIcon-1;
	
	//�õ�ʵ�ʵ�ICON�ڲ˵��е�����ֵ
	OneMenuData.curTKey = OneMenuData.tabIcon[OneMenuData.curIcon];
	OneMenuData.curIconIndex = OneMenuData.ListCtrl[OneMenuData.curTKey].code.index;
	
	//get menu number
	OneMenuData.lastCntMenu = OneMenuData.cntMenu;
	OneMenuData.cntMenu = uMenuCnt;
	OneMenuData.cntSubMenu = uSubMenuCnt;		//czn_20170301

	if(OneMenuData.spi_menu_id)
	{
		printf("LoadMenuBackground: spi_menu_id=%d,spi_disp_hpos=%d\n",OneMenuData.spi_menu_id, OneMenuData.spi_disp_hpos);
		//LoadMenuBackground( OneMenuData.spi_menu_id, OneMenuData.spi_disp_hpos, 0);  // 420ms
		LoadMenuBackground( OneMenuData.spi_menu_id, OneMenuData.spi_disp_hpos, (OneMenuData.spi_menu_id==3)? 720:0);  // 420ms //574
	}
	
	//��ʾ���е�icon
	LoadMenuAllIcons();		// 16ms

	//��ʾ���е�text
	LoadMenuAllTexts(GetCurLanguage());		// 48ms

	OneMenuData.menu_enable = 1;
	
	return 1;
}

void DisableOneMenu(void)
{
	OneMenuData.menu_enable = 0;
	OneMenuData.cntMenu = 0;
}

int DisplayOneSprite(int ubkgdCnt, int x, int y )
{
	LoadMenuSprite( ubkgdCnt, x, y, GetCurLanguage());
	//ˢ����ʾ	
	UpdateOsdLayerBuf2(OSD_LAYER_SPRITE,x,y,img_info.width,img_info.height,0);
	return 0;
}

int IfCustomIconExist(int icon_id,char *bmp_name)
{
	FILE *fp;
	char name[200];

	snprintf(name, 200, "/mnt/sdcard/Customerized/ListIcon/%d.bmp", icon_id);
	if((fp=fopen(name,"r"))!=NULL)
	{
		printf("CustomIconExist id=%d\n",icon_id);
		fclose(fp);
		strcpy(bmp_name,name);
		return 1;		
	}
	return 0;
}


int DisplayMultiSprite(MenuSpriteMulti_T sprite)
{
	int i;
	int language_cnt;
	FILE*	fp;
	FILE*   fp1;
	FILE*	fp2;
	int x, y, xsize, ysize,offset_dat;
	int updateXstart = 0;
	int updateYstart = 0;
	int updateXend = 0;
	int updateYend = 0;

	char bmpname[200];
	
	fp1 = fopen( osd_dat_path_custom, "rb" );
	fp2 = fopen( osd_dat_path, "rb" );

	if( fp2 == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;		
	}
	
	for(i = 0; i < sprite.dataCnt && i < MAX_STR_DISP_LINE_MAX; i++)
	{
		if( GetSpriteImageInfo(sprite.data[i].spriteId, language_cnt) == 1 )
			fp = fp1;
		else
			fp = fp2;
			
		xsize		= img_info.width;
		ysize		= img_info.height;
		offset_dat	= img_info.normal;
		
		GetPicDispXY(sprite.data[i].align, sprite.data[i].x, sprite.data[i].y, sprite.data[i].width, sprite.data[i].height, xsize, ysize, &x, &y);
		
		if(sprite.data[i].enable)
		{
			if(IfCustomIconExist(sprite.data[i].spriteId,bmpname) == 0)
			{
				if( offset_dat != -1 )
				{
					fseek(fp,offset_dat,SEEK_SET);
					fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp); 	
				}
			}
			else
			{
				BMP_FILE *bmpfile = NULL;
				int cur_line;
				bmpfile = OpenBMPFile(bmpname);
				if(bmpfile!=NULL)
				{
					cur_line = (bmpfile->cur_line>= img_info.height)?(img_info.height-1):bmpfile->cur_line;
					xsize = (xsize>bmpfile->bmih.biWidth)?bmpfile->bmih.biWidth:xsize;
					while((cur_line)>=0)
					{
						if(BMPFileReadOneLine(bmpfile)>0)
						{
							Pixel888to565Copy((unsigned short*)(gui_attr.osdlayer_tmp+cur_line*xsize*gui_attr.bpp_len),bmpfile->onelinebuff,xsize);
						}
						cur_line--;
					}
					CloseBMPFile(bmpfile);
				}
			}
		}
		else
		{
			memset( gui_attr.osdlayer_tmp, 0, xsize*ysize*gui_attr.bpp_len);
		}
		
		WriteOneOsdLayer( sprite.data[i].layerId, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
		
		updateXstart = (updateXstart < x ? updateXstart : x);
		updateYstart = (updateYstart < y ? updateYstart : y);
		
		updateXend = (updateXend > xsize + x ? updateXend : xsize + x);
		updateYend = (updateYend > ysize + y ? updateYend : ysize + y);
		
	}

	if( fp1 ) fclose(fp1); 
	if( fp2 ) fclose(fp2); 
	fp = NULL;		

	//ˢ����ʾ	
	if(sprite.data[0].alpha)
	{
		UpdateOsdLayerAlpha(sprite.data[0].alpha, updateXstart, updateYstart, updateXend-updateXstart, updateYend-updateYstart,0);
	}
	else
	{
		UpdateOsdLayerBuf2(0, updateXstart, updateYstart, updateXend-updateXstart, updateYend-updateYstart,0);
	}
	return 0;
}

int ClearListViewAlpha(int id, int x,int y,int sx,int sy, int layer)
{
	printf("ClearListViewAlpha x=%d y=%d sx=%d sy=%d \n",x, y, sx, sy);
	
	//memset( gui_attr.osdlayer_tmp, 0, sx*sy*gui_attr.bpp_len);
	//WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, sx, sy, gui_attr.osdlayer_tmp );
	SetOneOsdLayerRectColor(layer, x, y, sx, sy, COLOR_KEY);
	UpdateOsdLayerAlpha(id,x,y,sx, sy,1);
	return 0;
}


int ClearOneSprite(int ubkgdCnt, int x, int y, int mode )
{
	ClearMenuSprite(OSD_LAYER_SPRITE, ubkgdCnt, x, y,GetCurLanguage(), mode);
	//ˢ����ʾ	
	UpdateOsdLayerBuf2(OSD_LAYER_SPRITE,x,y,img_info.width,img_info.height,1);
	return 0;
}

int DisplayOneSpriteAlpha(int ubkgdCnt, int x, int y )
{
	LoadMenuSprite( ubkgdCnt, x, y, GetCurLanguage());
	//ˢ����ʾ	
	UpdateOsdLayerAlpha(OSD_LAYER_SPRITE,x,y,img_info.width,img_info.height,0);
	return 0;
}

int ClearOneSpriteAlpha(int ubkgdCnt, int x, int y )
{
	ClearMenuSprite(OSD_LAYER_SPRITE, ubkgdCnt, x, y,GetCurLanguage(), 0);
	//ˢ����ʾ	
	UpdateOsdLayerAlpha(OSD_LAYER_SPRITE,x,y,img_info.width,img_info.height,1);
	return 0;
}

int DisplayOneBmp(int layer,char *bmp_name,int x,int y,int sx,int sy)
{
	//char bmp_name[100];
	int width;
	//if(IfCustomMenuBmpPage(bmppage_id,bmp_name) == 1)
	{
		BMP_FILE *bmpfile = NULL;
		int cur_line;
		
		bmpfile = OpenBMPFile(bmp_name);
		//xsize 		= img_info.width;
		//ysize		= img_info.height;
		width = (bmpfile->bmih.biWidth >sx)?sx:bmpfile->bmih.biWidth;
		cur_line = (bmpfile->cur_line >= sy)? (sy-1):bmpfile->cur_line;
		printf("DisplayOneBmpPage width=%d,cur_line=%d,%d,%d\n",width,cur_line,gui_attr.max_w,gui_attr.max_h);
		int line_cnt = 0;
		if(bmpfile!=NULL)
		{
			while(cur_line>=0)
			{
				if(BMPFileReadOneLine(bmpfile)>0)
				{
					Pixel888to565Copy((unsigned short*)(gui_attr.osdlayer_tmp+cur_line*width*gui_attr.bpp_len),bmpfile->onelinebuff,width);
					line_cnt++;
				}
				cur_line--;
			}
			#if 0
			WriteOneOsdLayer( layer, x, y, width, sy, gui_attr.osdlayer_tmp );
			CloseBMPFile(bmpfile);
			printf("x=%d,y=%d,sx=%d,sy=%d\n",x,y,sx,sy);
			UpdateOsdLayerBuf2(layer,x,y,sx,sy,0);
			#else
			WriteOneOsdLayer( layer, x, y, width, line_cnt, gui_attr.osdlayer_tmp );
			CloseBMPFile(bmpfile);
			printf("x=%d,y=%d,sx=%d,sy=%d,line_cnt=%d\n",x,y,sx,sy,line_cnt);
			UpdateOsdLayerBuf2(layer,x,y,width, line_cnt,0);
			#endif
		}
	}
}

int DisplayOneProgBar(int x,int y,int sx,int sy, int color)
{
	//printf("DisplayOneProgBar sx=%d\n",sx);
	SetOneOsdLayerRectColor( OSD_LAYER_SPRITE, x, y, sx, sy, color);
	UpdateOsdLayerBuf2(OSD_LAYER_SPRITE,x,y,sx, sy,0);
	return 0;
}
int ClearOneProgBar(int x,int y,int sx,int sy)
{
	//memset( gui_attr.osdlayer_tmp, 0, sx*sy*gui_attr.bpp_len);
	//WriteOneOsdLayer( OSD_LAYER_SPRITE, x, y, sx, sy, gui_attr.osdlayer_tmp );
	
	//printf("ClearOneProgBar sx=%d\n",sx);
	SetOneOsdLayerRectColor( OSD_LAYER_SPRITE, x, y, sx, sy, COLOR_KEY);
	UpdateOsdLayerBuf2(OSD_LAYER_SPRITE,x,y,sx, sy,1);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************************
 * @fn:		GetCurMenuCnt
 *
 * @brief:	�õ���ǰ�˵����??
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
unsigned char GetCurMenuCnt(void)
{
	return OneMenuData.cntMenu;
}
// lzh_20181108_s
unsigned char GetCurSubMenuCnt(void)
{
	return OneMenuData.cntSubMenu;
}
unsigned char GetCurSubMenuCursor(void)
{
	return OneMenuData.curIconIndex;
}
// lzh_20181108_e

/*******************************************************************************************
 * @fn:		GetCurrentIcon
 *
 * @brief:	�õ���ǰICON��
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
unsigned char GetCurrentIcon( void )
{    
    return OneMenuData.curIcon;
}

unsigned short GetLastNMenu(void)
{
	return OneMenuData.lastCntMenu;
}



/*******************************************************************************************
 * @fn:		DisplayRawIcon
 *
 * @brief:	��ʾ���Ƿ���һ��ICON
 *
 * @param:  uIndex 	- icon����
 * @param:  uDispOn - 1: ���ԣ� 0: ����
 *
 * @return: none
 *******************************************************************************************/
void DisplayRawIcon(unsigned short uIndex, int x, int y, int uDispOn)
{
	FILE*	fp;
	int 	xsize, ysize;
	int 	offset_dat;

	if( OneMenuData.customflag && osd_dat_path_custom != NULL )
		fp = fopen( osd_dat_path_custom, "rb" );
	else
		fp = fopen( osd_dat_path, "rb" );

	if( fp == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return;
	}

	// display icon
	GetIconImageInfo( uIndex );
	xsize		= img_info.width;
	ysize		= img_info.height;
	if( !uDispOn )
		offset_dat	= img_info.normal;
	else
		offset_dat	= img_info.selected;

	if( offset_dat != -1 )
	{
		fseek(fp,offset_dat,SEEK_SET);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);

		WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		UpdateOsdLayerBuf2(OSD_LAYER_CURSOR,x,y,xsize,ysize,0);
	}
	fclose(fp); 	
}

// 20151121_s
/*******************************************************************************************
 * @fn:		DisplayRawText
 *
 * @brief:	��ʾһ��TEXT
 *
 * @param:  uIndex 	- text����
 *
 * @return: 
 *******************************************************************************************/
int DisplayRawText(FILE* fp, unsigned short uIndex, int align, int fnt_type, int x, int y, int language_cnt)
{
	int 	offset_dat;

	int xsize,ysize;
	char unicode_buf[200];
	int unicode_len;

	// display icon
	GetTextImageInfo( uIndex, language_cnt );
	xsize		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;

	//printf("DisplayRawText:xsize[%d],ysize[%d],normal[%d]\n",xsize,ysize,offset_dat);
	
	if( api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, uIndex, NULL, NULL, unicode_buf,&unicode_len ) )
	{	
		if( unicode_len > 0 )
		{
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
			{
				Arbic_Position_Process(unicode_buf,unicode_len,unicode_buf,&unicode_len);
				Unicode_Invert(unicode_buf,unicode_len);
			}
			if( align == 0 ) 	
				// lzh_20181221_s
				//display_unicode_string_align_center(OSD_LAYER_CURSOR, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 0);
				display_unicode_string_align_center(OSD_LAYER_CURSOR, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 1);
				// lzh_20181221_e
			else
				display_unicode_string_align_left(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, (get_pane_type()==5 || get_pane_type()==7)? 0:1, img_info.width);	
			return 0;
		}
		else
			return -1;
	}
	API_GetOSD_StringWithID2(10000+uIndex, unicode_buf,&unicode_len);
	if(unicode_len>0)
	{
		if(get_cur_unicode_invert_flag())		//FOR_HEBREW
		{
			Arbic_Position_Process(unicode_buf,unicode_len,unicode_buf,&unicode_len);
			Unicode_Invert(unicode_buf,unicode_len);
		}
		if( align == 0 ) 	
			// lzh_20181221_s
			//display_unicode_string_align_center(OSD_LAYER_CURSOR, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 0);
			display_unicode_string_align_center(OSD_LAYER_CURSOR, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 1);
			// lzh_20181221_e
		else
			display_unicode_string_align_left(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, (get_pane_type()==5 || get_pane_type()==7)? 0:1, img_info.width);	
		return 0;
	}
	else
	{		
		if( uIndex == 0 )
			return -1;

		if( fp == NULL )
		{
			return -1;
		}
				
		if( offset_dat != -1 )
		{	
			fseek(fp,offset_dat,SEEK_SET);
			fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);
			WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		}

		return 0;
	}	
}
// 20151121_e

/*******************************************************************************************
 * @fn:		DisplayOneMenuIcon
 *
 * @brief:	��ʾ���Ƿ���һ��ICON��uIconCntΪ�˵������е�ƫ����
 *
 * @param:  uIconCnt - ��ʾ��ICON����
 * @param:  uDispOn - 1 ���ԣ� 0 ����
 *
 * @return: none
 *******************************************************************************************/
void DisplayOneMenuIcon(unsigned short uIconCnt,unsigned char uDispOn)
{
	int i, x, y;

	i	= OneMenuData.ListCtrl[uIconCnt].code.index;
	x	= OneMenuData.ListCtrl[uIconCnt].pos.x<<POS_SHIFT_LEFT_BITS;
	y	= OneMenuData.ListCtrl[uIconCnt].pos.y<<POS_SHIFT_LEFT_BITS;

	DisplayRawIcon(i,x,y,uDispOn);
}


//20151121_s
/*******************************************************************************************
 * @fn:		DisplayOneMenuText
 *
 * @brief:	��ʾ���Ƿ���һ��ICON��uIconCntΪ�˵������е�ƫ����
 *
 * @param:  uIconCnt - ��ʾ��ICON����
 * @param:  uDispOn - 1 ���ԣ� 0 ����
 *
 * @return: none
 *******************************************************************************************/
void DisplayOneMenuText(FILE* file,unsigned short uIconCnt, int language_cnt )
{
	int i, x, y;
	int align,fnt_type;

	i		= OneMenuData.TextCtrl[uIconCnt].code.index;
	x		= OneMenuData.TextCtrl[uIconCnt].pos.x;
	y		= OneMenuData.TextCtrl[uIconCnt].pos.y;	
	align	= (OneMenuData.TextCtrl[uIconCnt].code.WinTab&0xff);
	fnt_type= (OneMenuData.TextCtrl[uIconCnt].code.WinTab>>8)&0xff;
	//printf("===uIconCnt[%d]==i[%d],x[%d],y[%d],align[%d],fnt_type[%d]======\n",uIconCnt,i,x,y,align,fnt_type);
	DisplayRawText( file, i, align,fnt_type, x,y,language_cnt);	
}
//20151121_e

// lzh_20210910_s

union sprite_packet
{
	struct packet_bit
	{
		unsigned char idx : 8;		// high  8 bits is relate sprite image index in sprite-zone
		unsigned char num : 8;		// low   8 bits is relate sprite image numbers
	} bit;
	unsigned short tab;
};

// display one icon with sprite offset
int DisplayOneIconWithSprite(unsigned short icon_func, int offset)
{
	FILE*	fp;
	FILE*	fp1;
	FILE*	fp2;
	union sprite_packet sprite_tab;
	int		sprite_func;
	char 	unicode_buf[200];
	int 	unicode_len;
	int 	i, x, y;
	int 	xsize, ysize;
	int 	offset_dat;
	int 	align;
	int 	fnt_type;
	
	int 	updateXstart,updateYstart,updateXend, updateYend;
	
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.index == icon_func )
		{
			sprite_tab.tab = OneMenuData.ListCtrl[i].code.WinTab;
			x	= OneMenuData.ListCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
			y	= OneMenuData.ListCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
			break;
		}
			
	}
	if( i == OneMenuData.icon_max )
		return -1;
				
	fp1 = fopen( osd_dat_path_custom, "rb" );
	fp2 = fopen( osd_dat_path, "rb" );
	if( fp2 == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;
	}
	
	updateXstart = 0;
	updateYstart = 0;
	updateXend = 0;
	updateYend = 0;

	// display icon
	if( GetIconImageInfo( icon_func ) == 1 )
		fp = fp1;
	else
		fp = fp2;

	xsize		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;
	// -----------------------------------------------------------------
	if( sprite_tab.bit.num )
	{
			if( offset >= sprite_tab.bit.num ) offset = 0;
			// get sprite index
			i = sprite_tab.bit.idx + offset;
			// get sprite image info
			sprite_func = OneMenuData.SpriCtrl[i].code.index;
			x	= OneMenuData.SpriCtrl[i].pos.x;
			y	= OneMenuData.SpriCtrl[i].pos.y;
			xsize = OneMenuData.SpriCtrl[i].size.h;
			ysize = OneMenuData.SpriCtrl[i].size.v;
			printf("sprite_tab:num[%d],idx[%d],sprite_func=%d,x=%d,y=%d,h=%d,v=%d\n",sprite_tab.bit.num,i,sprite_func,x,y,xsize,ysize);

			// display sprite

			// get display attribute
			if( GetSpriteImageInfo( sprite_func, 0 ) == 1 )
				fp = fp1;
			else
				fp = fp2;

			xsize		= img_info.width;
			ysize		= img_info.height;
			offset_dat	= img_info.normal;

			printf("sprite_image:x=%d,y=%d,w=%d,h=%d,offset_dat=%08x\n",x,y,xsize,ysize,offset_dat);

			// get display data
			fseek(fp,offset_dat,SEEK_SET);
			fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);

			WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
			
			updateXstart = x;
			updateYstart = y;
			updateXend = updateXstart + xsize;
			updateYend = updateYstart + ysize;
	}
	// -----------------------------------------------------------------
	else
	{
		if( offset_dat != -1 )
		{		
			fseek(fp,offset_dat,SEEK_SET);
			fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);

			WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
			
			updateXstart = x;
			updateYstart = y;
			updateXend = updateXstart + xsize;
			updateYend = updateYstart + ysize;
		}
	}		
	//20151121_s
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.TextCtrl[i].code.index == icon_func )
		{
			align	= (OneMenuData.TextCtrl[i].code.WinTab&0xff);
			fnt_type= (OneMenuData.TextCtrl[i].code.WinTab>>8)&0xff;
			x		= OneMenuData.TextCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
			y		= OneMenuData.TextCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
			break;
		}
	}
	if( i == OneMenuData.icon_max )
	{
		printf("get text none\n");
		goto err;
	}

	// display text	
	GetTextImageInfo( icon_func, GetCurLanguage() );
	xsize		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;

	printf("GetTextImageInfo:x=%d,y=%d,xsize=%d,ysize=%d\n",x,y,xsize,ysize);
	
	if( api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, icon_func, NULL, NULL, unicode_buf,&unicode_len ) )
	{			
		printf("api_get_one_unicode_both_str:len=%d\n",unicode_len);
		if( unicode_len > 0 )
		{
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
			{
				Arbic_Position_Process(unicode_buf,unicode_len,unicode_buf,&unicode_len);
				Unicode_Invert(unicode_buf,unicode_len);
			}
			if( align == 0 )		
				// lzh_20181221_s
				display_unicode_string_align_center(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 1);
				// lzh_20181221_e
			else
				display_unicode_string_align_left(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, 1, img_info.width);
		}
		else
		{
			goto err;
		}
	}
	else
	{		
		printf("api_get_one_unicode_both_str:from res dat\n");
		if( icon_func == 0 )
		{
			goto err;
		}
				
		if( offset_dat != -1 )
		{	
			printf("api_get_one_unicode_both_str:x=%d,y=%d,w=%d,h=%d\n",x, y, xsize, ysize);
		
			//fseek(fp,offset_dat,SEEK_SET);
			//fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);
			//WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		}
	}	
	
	updateXstart = (updateXstart > x ? x : updateXstart);
	updateYstart = (updateYstart > y ? y : updateYstart);
	updateXend = (updateXend < x+xsize ? x+xsize : updateXend);
	updateYend = (updateYend < y+ysize ? y+ysize : updateYend);
	

	UpdateOsdLayerBuf2(OSD_LAYER_CURSOR,updateXstart, updateYstart, updateXend - updateXstart, updateYend - updateYstart,0);		
	if( fp1 ) fclose(fp1);
	if( fp2 ) fclose(fp2);
	fp = NULL;
	return 0;
	
	err:		
	UpdateOsdLayerBuf2(OSD_LAYER_CURSOR,updateXstart, updateYstart, updateXend - updateXstart, updateYend - updateYstart,0);
	if( fp1 ) fclose(fp1);
	if( fp2 ) fclose(fp2);
	fp = NULL;
	return -1;
}
// lzh_20210910_e

/*******************************************************************************************
 * @fn:		DisplayOneFuncIcon
 *
 * @brief:	��ʾһ��ָ�����ܵ�icon
 *
 * @param:  icon_func	- icon�Ĺ��ܱ��??
 * @param:  uDispOn 	- icon��ʾģʽ��0/������ʾ��1/����
 *
 * @return: 0
 *******************************************************************************************/

int DisplayOneFuncIcon(unsigned short icon_func,unsigned char uDispOn)
{
	FILE*	fp;
	FILE*	fp1;
	FILE*	fp2;
	char 	unicode_buf[200];
	int 	unicode_len;
	int 	i, x, y;
	int 	xsize, ysize;
	int 	offset_dat;
	int 	align;
	int 	fnt_type;
	
	int 	updateXstart,updateYstart,updateXend, updateYend;
	
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.index == icon_func )
		{
			x	= OneMenuData.ListCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
			y	= OneMenuData.ListCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
			break;
		}
			
	}
	if( i == OneMenuData.icon_max )
		return -1;
				
	fp1 = fopen( osd_dat_path_custom, "rb" );
	fp2 = fopen( osd_dat_path, "rb" );
	if( fp2 == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;
	}
	
	updateXstart = 0;
	updateYstart = 0;
	updateXend = 0;
	updateYend = 0;

	// display icon
	if( GetIconImageInfo( icon_func ) == 1 )
		fp = fp1;
	else
		fp = fp2;

	xsize		= img_info.width;
	ysize		= img_info.height;
	if( !uDispOn )
		offset_dat	= img_info.normal;
	else
		offset_dat	= img_info.selected;

	if( offset_dat != -1 )
	{		
		fseek(fp,offset_dat,SEEK_SET);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);

		WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		
		updateXstart = x;
		updateYstart = y;
		updateXend = updateXstart + xsize;
		updateYend = updateYstart + ysize;
	}

	//20151121_s
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.TextCtrl[i].code.index == icon_func )
		{
			align	= (OneMenuData.TextCtrl[i].code.WinTab&0xff);
			fnt_type= (OneMenuData.TextCtrl[i].code.WinTab>>8)&0xff;
			x		= OneMenuData.TextCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
			y		= OneMenuData.TextCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
			break;
		}
	}
	if( i == OneMenuData.icon_max )	
	{
		printf("get text none\n");
		goto err;
	}

	// display text	
	GetTextImageInfo( icon_func, GetCurLanguage() );
	xsize		= img_info.width;
	ysize		= img_info.height;
	offset_dat	= img_info.normal;

	printf("GetTextImageInfo:x=%d,y=%d,xsize=%d,ysize=%d\n",x,y,xsize,ysize);
	
	if( api_get_one_unicode_both_str( UNICODE_STR_TYPE_ICON, icon_func, NULL, NULL, unicode_buf,&unicode_len ) )
	{			
		printf("api_get_one_unicode_both_str:len=%d\n",unicode_len);
		if( unicode_len > 0 )
		{
			if(get_cur_unicode_invert_flag())		//FOR_HEBREW
			{
				Arbic_Position_Process(unicode_buf,unicode_len,unicode_buf,&unicode_len);
				Unicode_Invert(unicode_buf,unicode_len);
			}
			if( align == 0 )		
				// lzh_20181221_s
				display_unicode_string_align_center(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, xsize, ysize, 1);
				// lzh_20181221_e
			else
				display_unicode_string_align_left(OSD_LAYER_STRING, x, y, COLOR_WHITE, COLOR_KEY, (char*)unicode_buf, unicode_len, (get_pane_type()==5 || get_pane_type()==7)? 0:1, img_info.width);
		}
		else
		{
			goto err;
		}
	}
	else
	{		
		printf("api_get_one_unicode_both_str:from res dat\n");
		if( icon_func == 0 )
		{
			goto err;
		}
				
		if( offset_dat != -1 )
		{	
			printf("api_get_one_unicode_both_str:x=%d,y=%d,w=%d,h=%d\n",x, y, xsize, ysize);
		
			//fseek(fp,offset_dat,SEEK_SET);
			//fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);
			//WriteOneOsdLayer( OSD_LAYER_CURSOR, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		}
	}	
	
	updateXstart = (updateXstart > x ? x : updateXstart);
	updateYstart = (updateYstart > y ? y : updateYstart);
	updateXend = (updateXend < x+xsize ? x+xsize : updateXend);
	updateYend = (updateYend < y+ysize ? y+ysize : updateYend);
	

	UpdateOsdLayerBuf2(OSD_LAYER_CURSOR,updateXstart, updateYstart, updateXend - updateXstart, updateYend - updateYstart,0);		
	if( fp1 ) fclose(fp1);
	if( fp2 ) fclose(fp2);
	fp = NULL;
	return 0;
	
	err:		
	UpdateOsdLayerBuf2(OSD_LAYER_CURSOR,updateXstart, updateYstart, updateXend - updateXstart, updateYend - updateYstart,0);
	if( fp1 ) fclose(fp1);
	if( fp2 ) fclose(fp2);
	fp = NULL;
	return -1;
}

int DisplayOneIconAlpha(unsigned short icon_func,unsigned char uDispOn)
{
	FILE*	fp;
	int 	i, x, y;
	int 	xsize, ysize;
	int 	offset_dat;
	
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.index == icon_func )
		{
			x	= OneMenuData.ListCtrl[i].pos.x<<POS_SHIFT_LEFT_BITS;
			y	= OneMenuData.ListCtrl[i].pos.y<<POS_SHIFT_LEFT_BITS;
			break;
		}
			
	}
	if( i == OneMenuData.icon_max )
		return -1;

	if( OneMenuData.customflag && osd_dat_path_custom != NULL )
		fp = fopen( osd_dat_path_custom, "rb" );
	else
		fp = fopen( osd_dat_path, "rb" );

	if( fp == NULL )
	{
		eprintf("open osd dat file failed!\n");
		return -1;
	}
	
	// display icon
	GetIconImageInfo( icon_func );
	xsize		= img_info.width;
	ysize		= img_info.height;
	if( !uDispOn )
	{
		offset_dat	= img_info.selected;
		memset( gui_attr.osdlayer_tmp, 0, xsize*ysize*gui_attr.bpp_len);
		WriteOneOsdLayer( OSD_LAYER_SPRITE, x, y, xsize, ysize, gui_attr.osdlayer_tmp );
		UpdateOsdLayerAlpha(OSD_LAYER_SPRITE,x,y,xsize,ysize,1);
		//usleep(100);
	}
	else
	{
		offset_dat	= img_info.normal;
	}

	if( offset_dat != -1 )
	{
		fseek(fp,offset_dat,SEEK_SET);
		fread( gui_attr.osdlayer_tmp, xsize*ysize*gui_attr.bpp_len, 1, fp);

		WriteOneOsdLayer( OSD_LAYER_SPRITE, x, y, xsize, ysize, gui_attr.osdlayer_tmp);
		UpdateOsdLayerAlpha(OSD_LAYER_SPRITE,x,y,xsize,ysize,0);
		
	}
	fclose(fp);
	return 0;

}

/*******************************************************************************************
 * @fn:		RestoreOldIcon
 *
 * @brief:	�ָ���һ�����??
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void RestoreOldIcon(void)
{	
	DisplayOneFuncIcon(OneMenuData.oldIconIndex,0);
}

void RestoreOldIconAlpha(void)
{	
	DisplayOneIconAlpha(OneMenuData.oldIconIndex,0);
}

/*******************************************************************************************
 * @fn:		SelectCurIcon
 *
 * @brief:	��ʾ��ǰѡ�еĹ��??
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void SelectCurIcon(void)
{	
	DisplayOneFuncIcon(OneMenuData.curIconIndex,1);
}

void SelectCurIconAlpha(void)
{	
	DisplayOneIconAlpha(OneMenuData.curIconIndex,1);
}

/*******************************************************************************************
 * @fn:		LookForIconOffset
 *
 * @brief:	ͨ��CODE�Ų��Ҹ�ICON��MENU�е�ƫ����
 *
 * @param:  uCode/���ܱ��??
 *
 * @return: offset
 *******************************************************************************************/
unsigned short LookForIconOffset(unsigned short uCode)
{
	unsigned short i;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( uCode != OneMenuData.ListCtrl[i].code.index )
			continue;
		else
			return i;
	}
	return 0xffff;
}

/*******************************************************************************************
 * @fn:		LookForIconCodeThroughTab
 *
 * @brief:	ͨ��TABֵ���Ҹ�ICON��CODE����
 *
 * @param:  uTab
 *
 * @return: 0xff - ��Чicon��others - code����
 *******************************************************************************************/
unsigned short LookForIconCodeThroughTab(unsigned short uTab)
{
	unsigned short i;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.WinTab == uTab )           
        {
            OneMenuData.curIcon = OneMenuData.ListCtrl[i].code.index;
			return OneMenuData.ListCtrl[i].code.index;
        }
		else
			continue;
	}
	return 0xff;
}

/*
�ƶ�ICON���??0/���ƣ�1/����
*/
int DisplayNextCursor(unsigned char uNext)
{
	if( uNext )
	{
		OneMenuData.oldIcon = OneMenuData.curIcon;
		OneMenuData.curIcon++;
		if( OneMenuData.curIcon >= OneMenuData.maxIcon )
			OneMenuData.curIcon = 0;
	}	
	else
	{
		OneMenuData.oldIcon = OneMenuData.curIcon;
		if( OneMenuData.curIcon == 0 )
			OneMenuData.curIcon = OneMenuData.maxIcon-1;
		else
			OneMenuData.curIcon--;
	}
	//�õ�ʵ�ʵ�ICON�ڲ˵��е�����ֵ
	OneMenuData.curTKey = OneMenuData.tabIcon[OneMenuData.curIcon];
	
	OneMenuData.oldIconIndex = OneMenuData.curIconIndex;		//�ϴι���icon
	DisplayOneFuncIcon(OneMenuData.oldIconIndex,0);
	
	OneMenuData.curIconIndex = OneMenuData.ListCtrl[OneMenuData.curTKey].code.index; //��ǰ����icon
	
	DisplayOneFuncIcon(OneMenuData.curIconIndex,1);
	return 0;
}

/*
������?��ָ����λ��
*/
void FixedCurIconCursor(unsigned short uCur)
{
	if( uCur < OneMenuData.icon_max )
	{
		OneMenuData.oldIcon = OneMenuData.curIcon;
		OneMenuData.curIcon = uCur;
		
		//�õ�ʵ�ʵ�ICON�ڲ˵��е�����ֵ
		OneMenuData.curTKey = OneMenuData.tabIcon[OneMenuData.curIcon];
		
		OneMenuData.oldIconIndex = OneMenuData.curIconIndex;		//�ϴι���icon
		DisplayOneFuncIcon(OneMenuData.oldIconIndex,0);
		
		OneMenuData.curIconIndex = OneMenuData.ListCtrl[OneMenuData.curTKey].code.index;		//��ǰ����icon
		DisplayOneFuncIcon(OneMenuData.curIconIndex,1);
	}
}
/*
ͨ��CODE�Ų��Ҹ�ICON��MENU�ṹ�е�λ��
*/
void LookForIconPosition(unsigned short uCode,POS* uPos)
{
	unsigned char i;
	i = LookForIconOffset(uCode);
	uPos->x = OneMenuData.ListCtrl[i].pos.x;
	uPos->y = OneMenuData.ListCtrl[i].pos.y;		
}

/*
�жϴ�����������iconֵ
1.��������OneMenuData.tabIcon[]������TAB���Եİ������и�����İ�����õ�OneMenuData.curTKeyֵ�������OneMenuData.ListCtrl[]����ƫ������������1
2.���ǰ���TAB����ICONʱ��������OneMenuData.ntabIcon[]�����и�����İ������?���õ�OneMenuData.curTKeyֵ�������OneMenuData.ListCtrl[]����ƫ������������1
3.���κ���Ч�����򷵻�-1
*/

// Tx: ��������x���� (Txmax: ��������x��������?)
// Ty: ��������y���� (Tymax: ��������y��������?)
// Dx: ��ʾ����x���� (Dxmax: ��ʾ����x��������?)
// Dy: ��ʾ����y���� (Dymax: ��ʾ����y��������?)
// Dx = (Tymax - Ty*Dxmax/Tymax) 
// Dy = (Tx*Dymax/Txmax)

#if 1
#define Txmax	320		// ����ֵ����ʵ�ʲ���
#define Tymax	480		// ����ֵ����ʵ�ʲ���
#define	Dxmax	320		// ��ʾˮƽ�ֱ���
#define Dymax	240		// ��ʾ��ֱ�ֱ���
#else
#define Txmax	760 //800		// ����ֵ����ʵ�ʲ���
#define Tymax	472 //480		// ����ֵ����ʵ�ʲ���
#define	Dxmax	320		// ��ʾˮƽ�ֱ���
#define Dymax	560		// ��ʾ��ֱ�ֱ���
#endif

// function: 	������������ϵת������ʾ������ϵ
// input: 	x - ������x����
// input: 	y - ������y����
// return: 	��16bitΪ��ʾ����ϵ��x�����ֵ����??16bitΪ��ʾ����ϵy������?

unsigned int TouchValueToTouchKey(unsigned short x, unsigned short  y)
{
	unsigned int coordinate;
	// ��Ҫ��������������ֵת��Ϊ��ʾ��������ֵ
	float dx,dy,tx,ty;
	tx = x;
	ty = y;
	dx = (Dxmax - ty*Dxmax/Tymax);
	dy = (tx*Dymax)/Txmax;
	if( dx < 0 ) dx = 0;
	if( dy < 0 ) dy = 0;
	if( dx > Dxmax ) dx = Dxmax;

	coordinate = (unsigned int)dx;
	coordinate <<= 16;
	coordinate += (unsigned int)dy;	
	return coordinate;
}

#define	PANE_X_MAX	480		// ��ʾˮƽ�ֱ���
#define PANE_Y_MAX	272		// ��ʾ��ֱ�ֱ���
int TouchKeyIsInsideOfDispPane(unsigned short x, unsigned short  y)
{
	if( (x <= PANE_X_MAX) &&  (y <= PANE_Y_MAX) )
		return 1;
	else
		return 0;
}
//czn_20161209_s
// logview
#define VKEY_LOGVIEW_POS_X 		30
#define VKEY_LOGVIEW_POS_Y 		280
#define VKEY_LOGVIEW_HALF_SZ	(40/2)
#if 0
// menu
#define VKEY_MENU_POS_X 	100
#define VKEY_MENU_POS_Y 	390
#define VKEY_MENU_HALF_SZ	(40/2)
// up
#define VKEY_UP_POS_X 		100
#define VKEY_UP_POS_Y 		330
#define VKEY_UP_HALF_SZ		(40/2)
// down
#define VKEY_DOWN_POS_X 	100
#define VKEY_DOWN_POS_Y 	430
#define VKEY_DOWN_HALF_SZ	(30/2)
// left
#define VKEY_LEFT_POS_X 	50
#define VKEY_LEFT_POS_Y 	390
#define VKEY_LEFT_HALF_SZ	(30/2)
// right
#define VKEY_RIGHT_POS_X 	140
#define VKEY_RIGHT_POS_Y 	390
#define VKEY_RIGHT_HALF_SZ	(30/2)
// unlock1
#define VKEY_UNLOCK1_POS_X 		250
#define VKEY_UNLOCK1_POS_Y 		330		//czn_20161008
#define VKEY_UNLOCK1_HALF_SZ	(40/2)
// unlock2
#define VKEY_UNLOCK2_POS_X 		250
#define VKEY_UNLOCK2_POS_Y 		430
#define VKEY_UNLOCK2_HALF_SZ	(40/2)
// power
#define VKEY_POWER_POS_X 		42
#define VKEY_POWER_POS_Y 		550
#define VKEY_POWER_HALF_SZ	(40/2)
// mesg
#define VKEY_MESG_POS_X 		120
#define VKEY_MESG_POS_Y 		550
#define VKEY_MESG_HALF_SZ	(40/2)
// alarm
#define VKEY_ALARM_POS_X 		206
#define VKEY_ALARM_POS_Y 		550
#define VKEY_ALARM_HALF_SZ	(40/2)
// call
#define VKEY_CALL_POS_X 		270
#define VKEY_CALL_POS_Y 		550
#define VKEY_CALL_HALF_SZ	(40/2)
#else
// MONITOR
#define VKEY_MONITOR_POS_X 		65
#define VKEY_MONITOR_POS_Y 		350		//czn_20161008
#define VKEY_MONITOR_HALF_SZ		(40/2)
// HOME
#define VKEY_HOME_POS_X 			160
#define VKEY_HOME_POS_Y 			350		//czn_20161008
#define VKEY_HOME_HALF_SZ			(40/2)
// OUTCALL
#define VKEY_OUTCALL_POS_X 		250
#define VKEY_OUTCALL_POS_Y 		350		//czn_20161008
#define VKEY_OUTCALL_HALF_SZ		(40/2)
// RECORD
#define VKEY_RECORD_POS_X 			65
#define VKEY_RECORD_POS_Y 			430		//czn_20161008
#define VKEY_RECORD_HALF_SZ		(40/2)
// unlock1
#define VKEY_UNLOCK1_POS_X 		250
#define VKEY_UNLOCK1_POS_Y 		430		//czn_20161008
#define VKEY_UNLOCK1_HALF_SZ		(40/2)
// unlock2
#define VKEY_UNLOCK2_POS_X 		160
#define VKEY_UNLOCK2_POS_Y 		430
#define VKEY_UNLOCK2_HALF_SZ		(40/2)
// power
#define VKEY_POWER_POS_X 			42
#define VKEY_POWER_POS_Y 			550
#define VKEY_POWER_HALF_SZ		(40/2)
// mesg
#define VKEY_MUTE_POS_X 			120
#define VKEY_MUTE_POS_Y 			550
#define VKEY_MUTE_HALF_SZ			(40/2)
// alarm
#define VKEY_ALARM_POS_X 			206
#define VKEY_ALARM_POS_Y 			550
#define VKEY_ALARM_HALF_SZ		(40/2)
// call
#define VKEY_INCOMINGCALL_POS_X 		270
#define VKEY_INCOMINGCALL_POS_Y 		550
#define VKEY_INCOMINGCALL_HALF_SZ		(40/2)
#endif
//czn_20161209

//�ؼ��ṹ����
typedef struct
{
	unsigned short x;
	unsigned short y;
} VKEY_POS;

typedef struct
{
	int			key;
	VKEY_POS	lt;
	VKEY_POS	rb;
} VKEY_DATA;

const VKEY_DATA VirtKey[] = 
{
	// logview
	{
		.key 	= 0,//cao_20170509 VKEY_LOGVIEW,
		.lt 	= {VKEY_LOGVIEW_POS_X-VKEY_LOGVIEW_HALF_SZ,VKEY_LOGVIEW_POS_Y-VKEY_LOGVIEW_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_LOGVIEW_POS_X+VKEY_LOGVIEW_HALF_SZ,VKEY_LOGVIEW_POS_Y+VKEY_LOGVIEW_HALF_SZ},		// x2, y2,
	},
	//czn_20161209_s
	#if 0
	// menu
	{
		.key 	= VKEY_MENU,
		.lt 	= {VKEY_MENU_POS_X-VKEY_MENU_HALF_SZ,VKEY_MENU_POS_Y-VKEY_MENU_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_MENU_POS_X+VKEY_MENU_HALF_SZ,VKEY_MENU_POS_Y+VKEY_MENU_HALF_SZ},		// x2, y2,
	},
	// up
	{
		.key	= VKEY_UP,
		.lt		= {VKEY_UP_POS_X-VKEY_UP_HALF_SZ,VKEY_UP_POS_Y-VKEY_UP_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_UP_POS_X+VKEY_UP_HALF_SZ,VKEY_UP_POS_Y+VKEY_UP_HALF_SZ},		// x2, y2,
	},
	// down
	{
		.key	= VKEY_DOWN,
		.lt		= {VKEY_DOWN_POS_X-VKEY_DOWN_HALF_SZ,VKEY_DOWN_POS_Y-VKEY_DOWN_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_DOWN_POS_X+VKEY_DOWN_HALF_SZ,VKEY_DOWN_POS_Y+VKEY_DOWN_HALF_SZ},		// x2, y2,
	},
	// left
	{
		.key	= VKEY_LEFT,
		.lt		= {VKEY_LEFT_POS_X-VKEY_LEFT_HALF_SZ,VKEY_LEFT_POS_Y-VKEY_LEFT_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_LEFT_POS_X+VKEY_LEFT_HALF_SZ,VKEY_LEFT_POS_Y+VKEY_LEFT_HALF_SZ},		// x2, y2,
	},
	// right
	{
		.key	= VKEY_RIGHT,
		.lt		= {VKEY_RIGHT_POS_X-VKEY_RIGHT_HALF_SZ,VKEY_RIGHT_POS_Y-VKEY_RIGHT_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_RIGHT_POS_X+VKEY_RIGHT_HALF_SZ,VKEY_RIGHT_POS_Y+VKEY_RIGHT_HALF_SZ},		// x2, y2,
	},
	
	// unlock1
	{
		.key	= VKEY_UNLOCK1,
		.lt		= {VKEY_UNLOCK1_POS_X-VKEY_UNLOCK1_HALF_SZ,VKEY_UNLOCK1_POS_Y-VKEY_UNLOCK1_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_UNLOCK1_POS_X+VKEY_UNLOCK1_HALF_SZ,VKEY_UNLOCK1_POS_Y+VKEY_UNLOCK1_HALF_SZ},		// x2, y2,
	},
	// unlock2
	{
		.key	= VKEY_UNLOCK2,
		.lt		= {VKEY_UNLOCK2_POS_X-VKEY_UNLOCK2_HALF_SZ,VKEY_UNLOCK2_POS_Y-VKEY_UNLOCK2_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_UNLOCK2_POS_X+VKEY_UNLOCK2_HALF_SZ,VKEY_UNLOCK2_POS_Y+VKEY_UNLOCK2_HALF_SZ},		// x2, y2,
	},

	// power		//czn_bl
	{
		.key	= VKEY_POWER,
		.lt		= {VKEY_POWER_POS_X-VKEY_POWER_HALF_SZ,VKEY_POWER_POS_Y-VKEY_POWER_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_POWER_POS_X+VKEY_POWER_HALF_SZ,VKEY_POWER_POS_Y+VKEY_POWER_HALF_SZ},		// x2, y2,
	},
	// mesg		//->vkey
	{
		.key	= VKEY_MESG,
		.lt		= {VKEY_MESG_POS_X-VKEY_MESG_HALF_SZ,VKEY_MESG_POS_Y-VKEY_MESG_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_MESG_POS_X+VKEY_MESG_HALF_SZ,VKEY_MESG_POS_Y+VKEY_MESG_HALF_SZ},		// x2, y2,
	},
	// alarm
	{
		.key	= VKEY_ALARM,
		.lt		= {VKEY_ALARM_POS_X-VKEY_ALARM_HALF_SZ,VKEY_ALARM_POS_Y-VKEY_ALARM_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_ALARM_POS_X+VKEY_ALARM_HALF_SZ,VKEY_ALARM_POS_Y+VKEY_ALARM_HALF_SZ},		// x2, y2,
	},
	// call//->incomingcall
	{
		.key	= VKEY_CALL,
		.lt		= {VKEY_CALL_POS_X-VKEY_CALL_HALF_SZ,VKEY_CALL_POS_Y-VKEY_CALL_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_CALL_POS_X+VKEY_CALL_HALF_SZ,VKEY_CALL_POS_Y+VKEY_CALL_HALF_SZ},		// x2, y2,
	},
	#else
	// MONITOR
	{
		.key	= 0,//cao_20170509 VKEY_MONITOR,
		.lt		= {VKEY_MONITOR_POS_X-VKEY_MONITOR_HALF_SZ,VKEY_MONITOR_POS_Y-VKEY_MONITOR_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_MONITOR_POS_X+VKEY_MONITOR_HALF_SZ,VKEY_MONITOR_POS_Y+VKEY_MONITOR_HALF_SZ},		// x2, y2,
	},
	// HOME
	{
		.key	= 0,//cao_20170509 VKEY_HOME,
		.lt		= {VKEY_HOME_POS_X-VKEY_HOME_HALF_SZ,VKEY_HOME_POS_Y-VKEY_HOME_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_HOME_POS_X+VKEY_HOME_HALF_SZ,VKEY_HOME_POS_Y+VKEY_HOME_HALF_SZ},		// x2, y2,
	},
	// OUTCALL
	{
		.key	= 0,//cao_20170509 VKEY_OUTCALL,
		.lt		= {VKEY_OUTCALL_POS_X-VKEY_OUTCALL_HALF_SZ,VKEY_OUTCALL_POS_Y-VKEY_OUTCALL_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_OUTCALL_POS_X+VKEY_OUTCALL_HALF_SZ,VKEY_OUTCALL_POS_Y+VKEY_OUTCALL_HALF_SZ},		// x2, y2,
	},
	// record
	{
		.key	= 0,//cao_20170509 VKEY_RECORD,
		.lt		= {VKEY_RECORD_POS_X-VKEY_RECORD_HALF_SZ,VKEY_RECORD_POS_Y-VKEY_RECORD_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_RECORD_POS_X+VKEY_RECORD_HALF_SZ,VKEY_RECORD_POS_Y+VKEY_RECORD_HALF_SZ},		// x2, y2,
	},
	// unlock1
	{
		.key	= 0,//cao_20170509 VKEY_UNLOCK1,
		.lt		= {VKEY_UNLOCK1_POS_X-VKEY_UNLOCK1_HALF_SZ,VKEY_UNLOCK1_POS_Y-VKEY_UNLOCK1_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_UNLOCK1_POS_X+VKEY_UNLOCK1_HALF_SZ,VKEY_UNLOCK1_POS_Y+VKEY_UNLOCK1_HALF_SZ},		// x2, y2,
	},
	// unlock2
	{
		.key	= 0,//cao_20170509 VKEY_UNLOCK2,
		.lt		= {VKEY_UNLOCK2_POS_X-VKEY_UNLOCK2_HALF_SZ,VKEY_UNLOCK2_POS_Y-VKEY_UNLOCK2_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_UNLOCK2_POS_X+VKEY_UNLOCK2_HALF_SZ,VKEY_UNLOCK2_POS_Y+VKEY_UNLOCK2_HALF_SZ},		// x2, y2,
	},
	// power		
	{
		.key	= 0,//cao_20170509 VKEY_POWER,
		.lt		= {VKEY_POWER_POS_X-VKEY_POWER_HALF_SZ,VKEY_POWER_POS_Y-VKEY_POWER_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_POWER_POS_X+VKEY_POWER_HALF_SZ,VKEY_POWER_POS_Y+VKEY_POWER_HALF_SZ},		// x2, y2,
	},
	// mut	
	#if 0		//czn_20170316
	{
		.key	= VKEY_MUTE,
		.lt		= {VKEY_MUTE_POS_X-VKEY_MUTE_HALF_SZ,VKEY_MUTE_POS_Y-VKEY_MUTE_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_MUTE_POS_X+VKEY_MUTE_HALF_SZ,VKEY_MUTE_POS_Y+VKEY_MUTE_HALF_SZ},		// x2, y2,
	},
	#endif
	// alarm
	{
		.key	= 0,//cao_20170509 VKEY_ALARM,
		.lt		= {VKEY_ALARM_POS_X-VKEY_ALARM_HALF_SZ,VKEY_ALARM_POS_Y-VKEY_ALARM_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_ALARM_POS_X+VKEY_ALARM_HALF_SZ,VKEY_ALARM_POS_Y+VKEY_ALARM_HALF_SZ},		// x2, y2,
	},
	// incomingcall
	#if 0	//czn_20170316
	{
		.key	= VKEY_INCOMINGCALL,
		.lt		= {VKEY_INCOMINGCALL_POS_X-VKEY_INCOMINGCALL_HALF_SZ,VKEY_INCOMINGCALL_POS_Y-VKEY_INCOMINGCALL_HALF_SZ},		// x1, y1,
		.rb		= {VKEY_INCOMINGCALL_POS_X+VKEY_INCOMINGCALL_HALF_SZ,VKEY_INCOMINGCALL_POS_Y+VKEY_INCOMINGCALL_HALF_SZ},		// x2, y2,
	},
	#endif
	#endif
	//czn_20161209
};

unsigned char TouchKeyMapToVirtKey(unsigned short x, unsigned short  y)
{
	int i;
	VKEY_DATA oneIcon;

	for( i = 0; i < sizeof(VirtKey)/sizeof(VKEY_DATA); i++ )
	{
		oneIcon = VirtKey[i];
		if( (x >= oneIcon.lt.x) && (x <= (oneIcon.rb.x)) &&
			(y >= oneIcon.lt.y) && (y <= (oneIcon.rb.y)) )
			return oneIcon.key;
		else
			continue;
	}
	return 0;
	
}

int TouchKeyMapToIcon(unsigned short x, unsigned short  y)
{
	int tmpx,tmpy;
	unsigned short uCurIcon;
	struOneCtrl tmpIcon;
	POS KeyGrid;

	//if( !OneMenuData.menu_enable )
	//	return -1;

	// դ�����??��֤��icon������ֵһ��
	#ifdef PID_DX482
	KeyGrid.x = y;
	KeyGrid.y = bkgd_h - x;
	#else
	if( m_PANEL_TYPE == IX481H )
	{
		KeyGrid.x = (bkgd_w-x);
		KeyGrid.y = (bkgd_h-y);		
	}
	else if( m_PANEL_TYPE == IX481V )
	{
		KeyGrid.x = bkgd_w - y;
		KeyGrid.y = x;
	}
	else if( m_PANEL_TYPE == IX850 )
	{
		KeyGrid.x = x;
		KeyGrid.y = y;
	}
	else if( m_PANEL_TYPE == IX482)
	{
		KeyGrid.x = y;
		KeyGrid.y = bkgd_h - x;
	}
	else if( m_PANEL_TYPE == IX47 || m_PANEL_TYPE == DX470)
	{
		#ifdef PID_DX470_V25
		KeyGrid.x = x;
		KeyGrid.y = y;
		#else
		KeyGrid.x = y;
		KeyGrid.y = x;
		#endif
	}
	else if( m_PANEL_TYPE == IX470V || m_PANEL_TYPE == DX470V)
	{
		#ifdef PID_DX470_V25
		KeyGrid.x = (bkgd_w-y);
		KeyGrid.y = x;
		#else
		KeyGrid.x = (bkgd_w-x);
		KeyGrid.y = y;
		#endif
	}
	else if( m_PANEL_TYPE == DX482)
	{
		KeyGrid.x = y;
		KeyGrid.y = bkgd_h - x;
	}
	#if 0 
	else if( m_PANEL_TYPE == DX470_V25)
	{
		KeyGrid.x = x;
		KeyGrid.y = y;
	}
	#endif
	else
	{
		KeyGrid.x = y;
		KeyGrid.y = x;
	}
	#endif
	printf("KeyGrid.x=%d,KeyGrid.y=%d,x=%d,y=%d,m_PANEL_TYPE=%d\n",KeyGrid.x,KeyGrid.y,x,y,m_PANEL_TYPE);

	// �����ϴε�icon����ֵ
	OneMenuData.oldIconIndex = OneMenuData.curIconIndex;

	if(IfMenuListPress(KeyGrid.x, KeyGrid.y, &OneMenuData.curIconIndex))
	{
		printf("curIconIndex = %d\n",OneMenuData.curIconIndex);
		return 1;
	}
	
	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.tabIcon[uCurIcon]];
		if( (KeyGrid.x >= tmpIcon.pos.x) && (KeyGrid.x <= (tmpIcon.size.h)) &&
			(KeyGrid.y >= tmpIcon.pos.y) && (KeyGrid.y <= (tmpIcon.size.v)) )
		{
			if( uCurIcon != OneMenuData.curIcon )
			{
				OneMenuData.oldIcon = OneMenuData.curIcon;
				OneMenuData.curIcon = uCurIcon;
			}
			//�õ�ʵ�ʵ�ICON�ڲ˵��е�����ֵ
			OneMenuData.curTKey = OneMenuData.tabIcon[uCurIcon];
			OneMenuData.curIconIndex = OneMenuData.ListCtrl[OneMenuData.curTKey].code.index;
			printf("match is tab icon = %d\n",OneMenuData.curIconIndex);
			return 1;
		}
	}
	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxNIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.ntabIcon[uCurIcon]];
		if( (KeyGrid.x >= tmpIcon.pos.x) && (KeyGrid.x <= (tmpIcon.size.h)) &&
			(KeyGrid.y >= tmpIcon.pos.y) && (KeyGrid.y <= (tmpIcon.size.v)) )
		{
			//�õ�ʵ�ʵ�ICON�ڲ˵��е�����ֵ
			OneMenuData.curTKey = OneMenuData.ntabIcon[uCurIcon];
			OneMenuData.curIconIndex = OneMenuData.ListCtrl[OneMenuData.curTKey].code.index;
			printf("match no tab icon = %d\n",OneMenuData.curIconIndex);
			return 1;
		}
	}
	OneMenuData.curIconIndex = 0;
	return 0;
}

/*
�����������Ƿ���������ICON�����һ��ICON
0�� ��ICON����
xx�����ذ���ICON��ICON���ܱ��??
*/
unsigned short TouchKeyMapToSimuIcon( unsigned short x, unsigned short y, ICON* ptrIconTab, unsigned short maxIcon )
{
	unsigned short i;
	ICON oneIcon;
	POS KeyGrid;

	KeyGrid.x = x;
	KeyGrid.y = y;
	
	for( i = 0; i < maxIcon; i++ )
	{
		oneIcon = ptrIconTab[i];
		if( (KeyGrid.x >= oneIcon.pos.x) && (KeyGrid.x <= (oneIcon.size.h)) &&
			(KeyGrid.y >= oneIcon.pos.y) && (KeyGrid.y <= (oneIcon.size.v)) )
			return oneIcon.code.index;
		else
			continue;
	}
	return 0;
}
/*
�õ�ָ��CODE�����ICON�ڲ˵��е�λ��
*/
short CodeNumberToMenuIndex(unsigned short uCodeNum)
{
	unsigned short i;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.index == uCodeNum )
			break;
	}
	if( i == OneMenuData.icon_max )
		return -1;
	else
		return i;
}

unsigned short savedIcon;
void SaveIcon(unsigned short icon) 
{
    savedIcon = icon;
}

unsigned short GetSavedIcon(void) 
{
    return savedIcon;
}

uint16 GetCurIcon(void)
{
	return OneMenuData.curIconIndex;
}

POS OSD_GetIconXY(uint16 iconNum)
{
	uint16 uCurIcon;
	struOneCtrl tmpIcon;

	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.tabIcon[uCurIcon]];
		if( tmpIcon.code.index == iconNum)
		{
			return tmpIcon.pos;
		}
	}
	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxNIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.ntabIcon[uCurIcon]];
		if( tmpIcon.code.index == iconNum)
		{
			return tmpIcon.pos;
		}
	}
	
	POS temp = {0, 0};
	return temp;

}

POS GetIconXYByTableNum(int table)
{
	uint16 uCurIcon;
	struOneCtrl tmpIcon;

	//��ɨ����TAB���Ե�ICON
	if(table <= OneMenuData.maxIcon && table > 0)
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.tabIcon[table-1]];
		return tmpIcon.pos;
	}
	else
	{
		POS temp = {0, 0};
		return temp;
	}
}

int OSD_GetIconInfo(unsigned short iconNum, POS* pos, SIZE* xy)
{
#if 0
	uint16 uCurIcon;
	struOneCtrl tmpIcon;

	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.tabIcon[uCurIcon]];
		if( tmpIcon.code.index == iconNum)
		{
			pos->x = tmpIcon.pos.x;
			pos->y = tmpIcon.pos.y;
			xy->h = tmpIcon.size.h;
			xy->v = tmpIcon.size.v;
			return;
		}
	}
	//��ɨ����TAB���Ե�ICON
	for( uCurIcon = 0; uCurIcon < OneMenuData.maxNIcon; uCurIcon++ )
	{
		tmpIcon = OneMenuData.ListCtrl[OneMenuData.ntabIcon[uCurIcon]];
		if( tmpIcon.code.index == iconNum)
		{
			pos->x = tmpIcon.pos.x;
			pos->y = tmpIcon.pos.y;
			xy->h = tmpIcon.size.h;
			xy->v = tmpIcon.size.v;
			return;
		}
	}
	pos->x = 0;
	pos->y = 0;
	xy->h = 0;
	xy->v = 0;
#endif
	
	int ret = 0;
	int i;
	
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.ListCtrl[i].code.index == iconNum )
		{
			if(pos != NULL)
			{
				pos->x = OneMenuData.ListCtrl[i].pos.x;
				pos->y = OneMenuData.ListCtrl[i].pos.y;
			}
			
			if(xy != NULL)
			{
				xy->h = OneMenuData.ListCtrl[i].size.h;
				xy->v = OneMenuData.ListCtrl[i].size.v;
			}
			ret = 1;
			break;
		}
	}

	return ret;
}
void Get_SpriteSize(int id, unsigned short* xsize, unsigned short* ysize)
{
	GetSpriteImageInfo( id, GetCurLanguage() );
	
	*xsize = img_info.width;
	*ysize = img_info.height;

}
void GetIconTextInfo(unsigned short iconNum, POS* pos, SIZE* xy)
{
	int i;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if( OneMenuData.TextCtrl[i].code.index == iconNum )
		{
			pos->x = OneMenuData.TextCtrl[i].pos.x;
			pos->y = OneMenuData.TextCtrl[i].pos.y;
			xy->h = OneMenuData.TextCtrl[i].size.h;
			xy->v = OneMenuData.TextCtrl[i].size.v;
			break;
		}
	}
}
#if defined(PID_IX850)
#else
int GetListIconNum(void)
{
	int i,index;
	int pageListNum = 0;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		index = OneMenuData.ListCtrl[i].code.index;
		if(((index >= ICON_007_PublicList1)&&(index <= ICON_016_PublicList10)) || ((index >= ICON_short_list1) && (index <= ICON_short_list10)))
		{
			pageListNum++;
		}
	}
	return pageListNum;

}
#endif
/*******************************************************************************************
 * @fn:		DisplayOneListFuncIcon
 *
 * @brief:	ѡ�кͷ���ĳ������Icon
 * @param:	layerId	���??
 * @param:  x, y, xsize, ysize	ѡ������
 * @param:  color				������ɫ
 * @param:  uDispOn 			- 0/͸����1/����
 *
 * @return: 0
 *******************************************************************************************/
int OsdLayerFillColor(int layerId, int x, int y, int xsize, int ysize, int color, int uDispOn, int alpha)
{
	if(uDispOn)
	{
		SetOneOsdLayerRectColor(layerId, x, y, xsize, ysize, color);
	}
	else
	{
		SetOneOsdLayerRectColor(layerId, x, y, xsize, ysize, COLOR_KEY);
	}
	
	//dprintf("x=%d,y=%d,xsize=%d,ysize=%d,color=%d,alpha=%d\n", x, y, xsize, ysize, color, alpha);
	if(alpha)
	{
		UpdateOsdLayerAlpha(alpha,x,y,xsize,ysize, 0);
	}
	else
	{
		UpdateOsdLayerBuf2(0, x, y, xsize, ysize, 0);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// lzh_20181016_s
char menu_display_cache_path[] 	= "/mnt/nand1-2/share/";

struct cache_file_head
{
	int x;
	int y;
	int w;
	int h;
};

int menu_display_cache_push( int menu_id, int layer_id, int cache_number, int x, int y, int width, int height )
{
	FILE* file;
	char cache_path[250];
	struct cache_file_head one_cache_head;

	snprintf(cache_path,250,"%smenu%03d_layer%d_cache%02d",menu_display_cache_path,menu_id,layer_id,cache_number);

	printf("menu_display_cache_push: %s\n",cache_path);
	
	file = fopen( cache_path, "wb+");
	
	if(file == NULL)
	{
		return -1;
	}

	one_cache_head.x = x;
	one_cache_head.y = y;
	one_cache_head.w = width;
	one_cache_head.h = height;

	fseek(file,0,SEEK_SET); 		 

	// д��cache�����ʼλ�á����ȡ��߶�??
	fwrite( (void*)&one_cache_head, sizeof(one_cache_head), 1, file);

	ReadOneOsdLayer_to_file(layer_id,one_cache_head.x,one_cache_head.y,one_cache_head.w,one_cache_head.h,file);
	
	fclose(file);

	printf("menu_display_cache_push ok!!!\n");
	
	return 0;
}

int menu_display_cache_pop( int menu_id, int layer_id, int cache_number )
{
	FILE* file;
	char cache_path[250];
	struct cache_file_head one_cache_head;

	snprintf(cache_path,250,"%smenu%03d_layer%d_cache%02d",menu_display_cache_path,menu_id,layer_id,cache_number);

	printf("menu_display_cache_pop: %s\n",cache_path);

	file = fopen( cache_path, "rb");

	if(file == NULL)
	{
		return -1;
	}

	fseek(file,0,SEEK_SET);

	// ��ȡcache�����ʼλ�á����ȡ��߶�??
	fread( (void*)&one_cache_head, sizeof(one_cache_head), 1, file);

	printf("menu_display_cache_pop,x=%d,y=%d,w=%d,h=%d\n",one_cache_head.x,one_cache_head.y,one_cache_head.w,one_cache_head.h);

	WriteOneOsdLayer_fm_file(layer_id,one_cache_head.x,one_cache_head.y,one_cache_head.w,one_cache_head.h,file);

	fclose(file);

	UpdateOsdLayerBuf2(layer_id,one_cache_head.x,one_cache_head.y,one_cache_head.w,one_cache_head.h,0);

	printf("menu_display_cache_pop ok!!!\n");
	
	return 0;
}


// ������Ҫcache�Ĳ˵���ʼ��

void DisplayTuneSettingPageIcon_CachePush(uint8 page);
void DisplayCallScenePageIcon_CachePush(uint8 page);


void all_display_menu_cache_push_initial(void)
{
//	DisplayTuneSettingPageIcon_CachePush(0);
//	DisplayCallScenePageIcon_CachePush(0);
}
// lzh_20181016_e
void DisplayCenterString(int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string, int str_len, int fnt_type, int format, int width, int height)
{
	
	short unicode_buf[50];
	int unicode_len;
	int unicode_len2;

	if( format == STR_UNICODE )
	{
		unicode_len = str_len/2;
		memcpy( unicode_buf, string, str_len );
	}
	else if( format == STR_UTF8 )
		unicode_len = utf82unicode(string,str_len,unicode_buf);
	else
		unicode_len = iconv2unicode(format,string,unicode_buf);
	if(get_cur_unicode_invert_flag())		//FOR_HEBREW
	{
		unicode_len2=unicode_len*2;
		Arbic_Position_Process(unicode_buf,unicode_len2,unicode_buf,&unicode_len2);
		Unicode_Invert(unicode_buf,unicode_len2);
		unicode_len=unicode_len2/2;
	}	
			
	if(unicode_len > 0)
	{
		SetOneOsdLayerRectColor( OSD_LAYER_STRING, x, y, width, height, COLOR_KEY);
		display_unicode_string_align_center(OSD_LAYER_STRING, x, y, fgcolor, bgcolor, (char*)unicode_buf, unicode_len*2, width, height, fnt_type);
		//UpdateOsdLayerBuf2(x,y,width,height);
		UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,width,height,0);
	}
}

//czn_20181110_s
void DisplayCenterStringRe(int x, int y, unsigned int fgcolor, unsigned int bgcolor, char* string, int str_len, int fnt_type, int format, int width, int height,int redisp_s,int redisp_e)
{
	
	short unicode_buf[50];
	int unicode_len;

	if( format == STR_UNICODE )
	{
		unicode_len = str_len/2;
		memcpy( unicode_buf, string, str_len );
	}
	else if( format == STR_UTF8 )
		unicode_len = utf82unicode(string,str_len,unicode_buf);
	else
		unicode_len = iconv2unicode(format,string,unicode_buf);
			
	if(unicode_len > 0)
	{
		SetOneOsdLayerRectColor( OSD_LAYER_STRING, x, y, width, height, COLOR_KEY);
		display_unicode_string_align_center_re(OSD_LAYER_STRING, x, y, fgcolor, bgcolor, (char*)unicode_buf, unicode_len*2, width, height, fnt_type,redisp_s,redisp_e);
		UpdateOsdLayerBuf2(OSD_LAYER_STRING,x,y,width,height,0);
	}
}
int DeleteOneIcon(int icon)
{
	int i;
	int rev=0;
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if(OneMenuData.ListCtrl[i].code.index == icon)
		{	
			rev=1;
			break;
		}
	}
	if(i==(OneMenuData.icon_max-1))
	{
		;
	}
	else
	{
		i++;
		for(; i < OneMenuData.icon_max; i++ )
		{
			OneMenuData.ListCtrl[i-1] = OneMenuData.ListCtrl[i];
			//OneMenuData.TextCtrl[i-1] = OneMenuData.TextCtrl[i];
		}
	}
	for( i = 0; i < OneMenuData.icon_max; i++ )
	{
		if(OneMenuData.TextCtrl[i].code.index == icon)
		{	
			
			break;
		}
	}
	if(i==(OneMenuData.icon_max-1))
	{
		OneMenuData.icon_max --;
	}
	else
	{
		i++;
		for(; i < OneMenuData.icon_max; i++ )
		{
			//OneMenuData.ListCtrl[i-1] = OneMenuData.ListCtrl[i];
			OneMenuData.TextCtrl[i-1] = OneMenuData.TextCtrl[i];
			if(i==(OneMenuData.icon_max-1))
			{
				OneMenuData.icon_max --;
				break;
			}
		}
	}
	return rev;
}
#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
void AlarmingIconDisp_Deal(void)
{
	int i;
	static int have_alarm_icon=1;
	if(have_alarm_icon==0)
		return;
	if(Get_AlarmingPara_Switch()==1)
	{
		if(GetAlarmingState()!=0)
		{
			if(DeleteOneIcon(ICON_ArmingSetting)==0)
				have_alarm_icon=0;
		}
		else
		{
			if(DeleteOneIcon(ICON_DisarmingSetting)==0)
				have_alarm_icon=0;
		}
	}
	else
	{
		
		if(DeleteOneIcon(ICON_ArmingSetting)==0)
			have_alarm_icon=0;
		else
		{
			DeleteOneIcon(ICON_DisarmingSetting);
		}
			
	}
	
}
#endif