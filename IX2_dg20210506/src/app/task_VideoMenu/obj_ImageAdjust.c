
#include "obj_Video.h"

#define MAX_BRIGHT_CNT			10
#define MAX_COLOR_CNT			10

#define BRIGHT_PHY_DAT_MIN		0x10
#define COLOR_PHY_DAT_MIN		0x30
//lzh_20140808
#define CONTRAST_PHY_DAT_MIN	0x20

#define BRIGHT_STEP				16
#define COLOR_STEP				16
//lzh_20140808
#define CONTRAST_STEP			16

/*******************************************************************************************
 * @fn：	Video_ImageParaSet
 *
 * @brief:	视频亮度和色度逻辑参数设置
 *
 * @param:   	logBright - 亮度逻辑值，logColor - 色度逻辑, logContrast - 对比度逻辑值
 *
 * @return: 	none
 *******************************************************************************************/
void Video_ImageParaSet(unsigned char logBright, unsigned char logColor, unsigned char logContrast)
{
	unsigned char BrightPar,ColorPar;
	unsigned char ContrastPar;		//lzh_20140808

	if( logBright > MAX_BRIGHT_CNT)	logBright = MAX_BRIGHT_CNT;
	if( logColor > MAX_COLOR_CNT) 	logColor = MAX_COLOR_CNT;	

	BrightPar = BRIGHT_PHY_DAT_MIN + logBright*BRIGHT_STEP;
	ColorPar	= COLOR_PHY_DAT_MIN  + logColor*COLOR_STEP;
	//lzh_20140808
	ContrastPar = CONTRAST_PHY_DAT_MIN  + logContrast*CONTRAST_STEP;
		
	//IC_WriteByte(TWIC_P0,0x69,BrightPar);
	//IC_WriteByte(TWIC_P0,0x6c,ColorPar);
	//lzh_20140808
	//IC_WriteByte(TWIC_P2,0x08,ContrastPar);

	printf("set image bright:0x%02x,color:0x%02x,contrast:0x%02x\n\r",BrightPar,ColorPar,ContrastPar);
	
}

/*******************************************************************************************
 * @fn：	Video_ImageScaleSet
 *
 * @brief:	视频scaler 设置
 *
 * @param:  	scale - scaler模式
 *
 * @return: 	none
 *******************************************************************************************/
void Video_ImageScaleSet(VIDEO_SCALE scale)
{
	if( scale == SCALE_4_3 )
	{
		//IC_WriteByte(TWIC_P0,0x73,0x1b);		// 4:3
	}
	else
	{
		//IC_WriteByte(TWIC_P0,0x73,0x15);		// 16:9
	}	
}


