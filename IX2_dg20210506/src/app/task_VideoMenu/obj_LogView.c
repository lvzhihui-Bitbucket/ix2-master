//20181017
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
	  
#include "task_VideoMenu.h"
#include "obj_LogView.h"

LOG_VIEW_PAGE	one_logview_page;

/****************************************************************************************************************************************
 * @fn      init_log_win()
 *
 * @brief   log显示初始化，需要在task_VideoMenu中调用，等待osd层初始化完成后，初始化该View层的显示层缺省底色及相关参数
 *
  * @param   none 
 *
 * @return  0 - ok, other - err
 ***************************************************************************************************************************************/
int init_log_win(void)
{
#if 0
	SetOneOsdLayerDisable( OSD_LAYER_LOG );
	// clear buffer
	clear_logview_screen();
	one_logview_page.in = 0;
	one_logview_page.out = 0;
	one_logview_page.disp_cnt = 0;
#endif
	return 0;
}

 /****************************************************************************************************************************************
 * @fn      turn_on_log_win()
 *
 * @brief   开启LogWin显示，负责开启logView层显示，关闭其他层显示
 *
  * @param   none 
 *
 * @return  0 - ok, other - err
 ***************************************************************************************************************************************/
int turn_on_log_win(void)
{
#if 0
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i++ )
	{
		if( i == OSD_LAYER_LOG )
			SetOneOsdLayerEnable( i );
		else
			SetOneOsdLayerDisable( i );			
	}
	UpdateOsdLayerBuf(0);
#endif	
	return 0;
}

/****************************************************************************************************************************************
* @fn	   turn_off_log_win()
*
* @brief   关闭LogWin显示，负责关闭iew层显示，开启其他层示
*
 * @param	none 
*
* @return  0 - ok, other - err
***************************************************************************************************************************************/
int turn_off_log_win(void)
{
#if 0
	int i;
	for( i = 0; i < OSD_LAYER_MAX; i++ )
	{
		if( i != OSD_LAYER_LOG )
			SetOneOsdLayerEnable( i );
		else
			SetOneOsdLayerDisable( i );			
	}
	UpdateOsdLayerBuf(0);
#endif	
	return 0;
}

int clear_log_win(void)
{
#if 0
	clear_logview_screen();			
	UpdateOsdLayerBuf(0);			
	one_logview_page.disp_cnt = 0;
#endif
}

#define LOGVIEW_STR_DISP_X		0
#define LOGVIEW_STR_DISP_Y		0
#define LOGVIEW_STR_DISP_H		40	//czn_20190412		//24
/****************************************************************************************************************************************
* @fn	   add_rec_to_log_view()
*
* @brief   添加一条记录到LogView缓冲页，若记录长度超过一行的长度自动换行，若满屏会自动将上一条推到顶端，接着显示下一条
*
* @param		prect - 显示字符串指针 
*
* @return  0 - ok, other - err
***************************************************************************************************************************************/
int add_rec_to_log_view(char* prec)
{
#if 0
	int str_len;
	int i,blk_cnt,mod_cnt;
		
	// 记录超长处理
	str_len = strlen(prec);
	if( str_len > LOG_VIEW_MAX_COL*2 )
	{
		str_len = LOG_VIEW_MAX_COL*2;
	}

	blk_cnt = str_len/LOG_VIEW_MAX_COL;
	mod_cnt = str_len%LOG_VIEW_MAX_COL;
	
	// 记录加入
	for( i = 0; i < blk_cnt; i++ )
	{
		memset(one_logview_page.row[one_logview_page.in].row_buf,' ',LOG_VIEW_MAX_COL+2 );
		
		if( i == 0 )
		{
			one_logview_page.row[one_logview_page.in].row_buf[0] = '#';		//  head flag
			one_logview_page.row[one_logview_page.in].rec_head = 1;
		}
		else
		{
			one_logview_page.row[one_logview_page.in].row_buf[0] = ' ';		// 空格
			one_logview_page.row[one_logview_page.in].rec_head = 0;
		}	
		memcpy(one_logview_page.row[one_logview_page.in].row_buf+1,prec,LOG_VIEW_MAX_COL );
		one_logview_page.row[one_logview_page.in].row_buf[LOG_VIEW_MAX_COL+1] = ' ';
		one_logview_page.row[one_logview_page.in].row_buf[LOG_VIEW_MAX_COL+2] = 0;
		if( ++one_logview_page.in >= LOG_VIEW_MAX_ROW ) one_logview_page.in = 0;
		prec += LOG_VIEW_MAX_COL;
	}
	if( mod_cnt )
	{
		memset(one_logview_page.row[one_logview_page.in].row_buf,' ',LOG_VIEW_MAX_COL+2 );

		if( blk_cnt == 0 )
		{
			one_logview_page.row[one_logview_page.in].row_buf[0] = '#';		//  head flag
			one_logview_page.row[one_logview_page.in].rec_head = 1;
		}
		else
		{
			one_logview_page.row[one_logview_page.in].row_buf[0] = ' ';		// 空格
			one_logview_page.row[one_logview_page.in].rec_head = 0;
		}	
		memcpy(one_logview_page.row[one_logview_page.in].row_buf+1,prec,mod_cnt );
		one_logview_page.row[one_logview_page.in].row_buf[mod_cnt+1] = ' ';
		one_logview_page.row[one_logview_page.in].row_buf[mod_cnt+2] = 0;
		if( ++one_logview_page.in >= LOG_VIEW_MAX_ROW ) one_logview_page.in = 0;		
		prec += mod_cnt;
	}
	
	// 记录输出显示
	while( one_logview_page.out != one_logview_page.in )
	{
		// 满屏处理
		if( one_logview_page.disp_cnt >= LOG_VIEW_MAX_ROW )
		{
			clear_logview_screen();			
			UpdateOsdLayerBuf(0);			
			one_logview_page.disp_cnt = 0;
			//将one_logview_page.out调整到上一条有效记录
			do
			{
				if( !one_logview_page.out )
					one_logview_page.out = LOG_VIEW_MAX_ROW-1;
				else
					one_logview_page.out--;
			}
			while( !one_logview_page.row[one_logview_page.out].rec_head );
		}
		else
		{
			display_logview_string( LOGVIEW_STR_DISP_X,LOGVIEW_STR_DISP_Y+one_logview_page.disp_cnt*LOGVIEW_STR_DISP_H, COLOR_WHITE, one_logview_page.row[one_logview_page.out].row_buf, STR_GB2312 );
			// 记录输出调整
			if( ++one_logview_page.out >= LOG_VIEW_MAX_ROW ) one_logview_page.out = 0;
			// 显示计数调整
			one_logview_page.disp_cnt++;
		}		
	}	
#endif	
	return 0;
}



