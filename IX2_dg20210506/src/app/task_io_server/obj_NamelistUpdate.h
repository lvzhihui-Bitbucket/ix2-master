
#ifndef _OBJ_NAMELISTUPDATE_H
#define _OBJ_NAMELISTUPDATE_H

// Define Object Property

// Define Object Function - Private
void NamelistUpdate_Timeout(void);

// Define Object Function - Public

void NamelistUpdate_Initial(void);

unsigned char API_NamelistUpdate_Start(void);
unsigned char API_NameListUpdate_List(unsigned char id, unsigned char len, unsigned char *ptrDat);
unsigned char API_NamelistUpdate_End(void);

void API_NameListUpdate_Start_UI(void);
void API_NameListUpdate_End_UI(void);
void API_NameListUpdate_List_UI(void);

#endif


