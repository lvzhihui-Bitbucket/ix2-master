/*
  Copyright (c) 2009-2017 Dave Gamble and cJSON contributors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "obj_VideoProxySetting.h"
#include "define_file.h"

/*
{
    "TYPE":"IX",
    "IX":
    	{
    		"ADDR": "0099990101"
    	},
    	
    "IPC":{
    	"ID": "", 
    	"NAME":"ipc", 
    	"IP": "192.168.243.1119", 
    	"CH": 2, 
    	"User": "",
    	"PWD": "",
    	},

};
*/


char* CreateVideoProxyObject(VIDEO_PROXY_JSON obj)
{
    cJSON *root = NULL;
    cJSON *ixDevice = NULL;
    cJSON *ipcDevic = NULL;
	cJSON *ipcInfo = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "TYPE", obj.type);
    cJSON_AddItemToObject(root, "IX", ixDevice = cJSON_CreateObject());
    cJSON_AddStringToObject(ixDevice, "ADDR", obj.ixDevice.addr);

	if(obj.ipcDevice.NAME[0])
	{
		cJSON_AddItemToObject(root, "IPC", ipcDevic = cJSON_CreateObject());
		cJSON_AddStringToObject(ipcDevic, "NAME", obj.ipcDevice.NAME);
	}
	#if 0
    cJSON_AddItemToObject(root, "IPC", ipcDevic = cJSON_CreateObject());
    cJSON_AddStringToObject(ipcDevic, "ID", obj.ipcDevice.ID);
    cJSON_AddStringToObject(ipcDevic, "NAME", obj.ipcDevice.NAME);
    cJSON_AddStringToObject(ipcDevic, "IP", obj.ipcDevice.IP);
    cJSON_AddNumberToObject(ipcDevic, "CH", obj.ipcDevice.CH);
    cJSON_AddStringToObject(ipcDevic, "USER", obj.ipcDevice.USER);
    cJSON_AddStringToObject(ipcDevic, "PWD", obj.ipcDevice.PWD);
	#endif
	if(obj.ipcInfo.rtsp_url[0])
	{
		cJSON_AddItemToObject(root, "IPC_INFO", ipcInfo = cJSON_CreateObject());
		cJSON_AddStringToObject(ipcInfo, "RTSP_URL", obj.ipcInfo.rtsp_url);
		cJSON_AddNumberToObject(ipcInfo, "W", obj.ipcInfo.width);
		cJSON_AddNumberToObject(ipcInfo, "H", obj.ipcInfo.height);
		cJSON_AddNumberToObject(ipcInfo, "CODE", obj.ipcInfo.vd_type);
	}
	
	string = cJSON_Print(root);
	if(string != NULL)
	{	
		printf("VIDEO_PROXY_JSON=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}
//void CreateVideoProxyObjectWithInfo(
cJSON *IPCInfo_Obj = NULL;
void CreateVideoProxyIpcInfoObject(login_dat_t ipcInfo)
{
	IPCInfo_Obj=cJSON_CreateObject();
	cJSON_AddStringToObject(IPCInfo_Obj, "RTSP_URL", ipcInfo.rtsp_url);
	cJSON_AddNumberToObject(IPCInfo_Obj, "CH", 1);
	cJSON_AddNumberToObject(IPCInfo_Obj, "W", ipcInfo.width);
	cJSON_AddNumberToObject(IPCInfo_Obj, "H", ipcInfo.height);
}

void FreeVideoProxyIpcInfoObject(void)
{
	if(IPCInfo_Obj!=NULL)
	{
		cJSON_Delete(IPCInfo_Obj);
		IPCInfo_Obj = NULL;
	}
}

char* AddIpcInfoToVideoProxyObject(const char* json)
{
	char *re_str = NULL;
	if(IPCInfo_Obj!=NULL)
	{
		 cJSON *VideoProxyObject = cJSON_Parse(json);
		 if(VideoProxyObject!=NULL)
		 {
			cJSON_AddItemReferenceToObject(VideoProxyObject, "IPC_INFO", IPCInfo_Obj);
			re_str= cJSON_Print(VideoProxyObject);
			cJSON_Delete(VideoProxyObject);
		 }
	}
	return re_str;
}

int ParseVideoProxyObject(const char* json, VIDEO_PROXY_JSON* pObj)
{
    int status = 0;

    const cJSON *type = NULL;
    const cJSON *ixDevice = NULL;
    const cJSON *ipcDevic = NULL;
	const cJSON *ipcinfo = NULL;
	
	memset(pObj, 0, sizeof(VIDEO_PROXY_JSON));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *VideoProxyObject = cJSON_Parse(json);
    if (VideoProxyObject == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	/* 获取名为“TYPE”的值 */
    ParseJsonString(VideoProxyObject, "TYPE", pObj->type, VIDEO_PROXY_DATA_LEN);

	/* 获取名为“IX“的值，它是cJSON 结构*/
	ixDevice = cJSON_GetObjectItemCaseSensitive(VideoProxyObject, "IX");
    if (ixDevice != NULL)
    {
		/* 获取名为“ADDR“的值，它是string*/
		ParseJsonString(ixDevice, "ADDR", pObj->ixDevice.addr, VIDEO_PROXY_DATA_LEN);
    }


	/* 获取名为“IPC“的值，它是cJSON 结构*/
	ipcDevic = cJSON_GetObjectItemCaseSensitive(VideoProxyObject, "IPC");
    if (ipcDevic != NULL)
    {
		/* 获取名为“ID“的值，它是string*/
		ParseJsonString(ipcDevic, "ID", pObj->ipcDevice.ID, VIDEO_PROXY_DATA_LEN);
		/* 获取名为“NAME“的值，它是string*/
		ParseJsonString(ipcDevic, "NAME", pObj->ipcDevice.NAME, VIDEO_PROXY_DATA_LEN);
		/* 获取名为“IP“的值，它是string*/
		ParseJsonString(ipcDevic, "IP", pObj->ipcDevice.IP, VIDEO_PROXY_DATA_LEN);
		/* 获取名为“CH“的值，它是整数*/
		ParseJsonNumber(ipcDevic, "CH", &(pObj->ipcDevice.CH));
		/* 获取名为“USER“的值，它是string*/
		ParseJsonString(ipcDevic, "USER", pObj->ipcDevice.USER, VIDEO_PROXY_DATA_LEN);
		/* 获取名为“PWD“的值，它是string*/
		ParseJsonString(ipcDevic, "PWD", pObj->ipcDevice.PWD, VIDEO_PROXY_DATA_LEN);
    }

	pObj->have_ipcinfo=0;
	ipcinfo = cJSON_GetObjectItemCaseSensitive(VideoProxyObject, "IPC_INFO");
	if(ipcinfo!=NULL)
	{
		pObj->have_ipcinfo=1;

		ParseJsonString(ipcinfo, "RTSP_URL", pObj->ipcInfo.rtsp_url, 200);
		ParseJsonNumber(ipcinfo, "W", &(pObj->ipcInfo.width));
		ParseJsonNumber(ipcinfo, "H", &(pObj->ipcInfo.height));
		ParseJsonNumber(ipcinfo, "CODE", &(pObj->ipcInfo.vd_type));
	}
    status = 1;

end:
	
    cJSON_Delete(VideoProxyObject);
	
    return status;
}


char* GetVideoProxyParameter(void)
{
	FILE* file = NULL;
	char* json = NULL;
	
	if((file=fopen(VIDEO_PROXY_FILE_NAME,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);
	}

	return json;

}

void SetVideoProxyParameter(const char* string)
{
	FILE	*file = NULL;
	
	if((file=fopen(VIDEO_PROXY_FILE_NAME,"w+")) != NULL)
	{
		fputs(string, file);
		
		fclose(file);
	}
}


