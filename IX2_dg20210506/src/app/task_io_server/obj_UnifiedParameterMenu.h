/**
  ******************************************************************************
  * @file    obj_UnifiedParameterMenu.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_UnifiedParameterMenu_H
#define _obj_UnifiedParameterMenu_H

// Define Object Property-------------------------------------------------------
#define COMBO_BOX							"comboBox"


#define NAME_LEN			100
#define DESCRIBE_LEN		100
#define COMBO_BOX_LEN		100
#define VALUE_LEN			500


#define COMBO_BOX_MAX		30		//COMBO_BOX最大选择数量


typedef struct
{
	char paraId[5];
	int  nameLen;
	char name[NAME_LEN+1];
	char describe[DESCRIBE_LEN+1];
	char comboBox[COMBO_BOX_LEN+1];
	char value[VALUE_LEN+1];
} ParaMenu_T;

typedef struct
{
	char value[VALUE_LEN+1];
	char comboBox[COMBO_BOX_LEN+1];
}ComboBox;


typedef struct
{
	int			title;
	int			saveValue;
	int			value;
	int			editType;
	int 		(*ParaComboBoxUpdateValueFunction)(int);
	int 		num;
	int  		nameLen;
	char 		name[NAME_LEN+1];
	ComboBox 	data[COMBO_BOX_MAX];
}ComboBoxSelect_T;


// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------

// Define Object Function - Other-----------------------------------------------


#endif
