/**
  ******************************************************************************
  * @file    task_IoServer.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "task_IoServer.h"
#include "vdp_IoServer_State.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "obj_ResourceTable.h"		//czn_20160827
#include "obj_NamelistUpdate.h"


Loop_vdp_common_buffer	vdp_io_server_mesg_queue;
Loop_vdp_common_buffer	vdp_io_server_sync_queue;
vdp_task_t				task_io_server;
uint8 					flag_switch_io;

// lzh_20160601_e
int io_server_table_cmd_process( REMOTE_TABLE_CMD_REQ* ptable_cmd_req, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp );
int io_server_inner_table_cmd_rsp( unsigned char msg_target_id, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp );
int io_server_uart_table_cmd_rsp( unsigned char msg_target_id, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp );

void vdp_io_server_mesg_data_process(char* msg_data, int len);
void* vdp_io_server_task( void* arg );
	
void vtk_TaskInit_io_server(int priority)
{
	init_vdp_common_queue(&vdp_io_server_mesg_queue, 2000, (msg_process)vdp_io_server_mesg_data_process, &task_io_server);
	init_vdp_common_queue(&vdp_io_server_sync_queue, 2000, NULL, 								  		&task_io_server);
	init_vdp_common_task(&task_io_server, MSG_ID_IOServer, vdp_io_server_task, &vdp_io_server_mesg_queue, &vdp_io_server_sync_queue);

	
}


void exit_vdp_io_server_task(void)
{
	exit_vdp_common_queue(&vdp_io_server_mesg_queue);
	exit_vdp_common_queue(&vdp_io_server_sync_queue);
	exit_vdp_common_task(&task_io_server);
}

void* vdp_io_server_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	IoServerStateInit();
	load_io_data_table();
	load_io_data_setting_table();
	load_io_data_menu_table();
	API_Load_ResourceTable();
	load_ip_device_table();
	api_load_call_record();
	memo_vd_record_init();
	memo_vd_playback_init();
	#if !defined(PID_DX470)&&!defined(PID_DX482)
	MonRes_Table_Init();
	#endif
	LoadSearchTimeoutParameter();
	// Namelist更新对象初始化
	NamelistUpdate_Initial();

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_io_server_mesg_data_process(char* msg_data,int len)
{
	IoRemote_S ioMsg;
	REMOTE_TABLE_CMD_REQ_IF	*preq_msg;
	REMOTE_TABLE_CMD_RSP		rsp_msg;
	preq_msg = (REMOTE_TABLE_CMD_REQ_IF*)msg_data;
	
	flag_switch_io = IO_SWITCH_DISABLE;

	memcpy(&ioMsg, msg_data, len>sizeof(ioMsg) ? sizeof(ioMsg) : len);
	
	switch(ioMsg.head.msg_type )
	{
		//本地读参数
		case IO_SERVER_READ_LOCAL:
			InnerRead(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
			
		//本地写参数
		case IO_SERVER_WRITE_LOCAL:
			InnerWrite(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
			
		//本地按等级恢复参数默认值
		case IO_SERVER_FACTORY_DEFAULT_BY_LEVEL_LOCAL:
			InnerFactoryDefaultByLevel(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
			
		//本地按编号恢复参数默认值
		case IO_SERVER_FACTORY_DEFAULT_BY_ID_LOCAL:
			InnerFactoryDefaultById(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;

		//判断本地参数值是否符合范围
		case IO_SERVER_JUDGE_RANGE_LOCAL:
			InnerJudgeRange(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;

		case IO_SERVER_READ_DEFAULT_LOCAL:
			InnerReadDefault(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
		case IO_SERVER_READ_PROPERTY_LOCAL:
			ReadProperty(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
		case IO_SERVER_READ_GROUP_LOCAL:
			InnerReadGroup(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;





		//通过udp读写参数
		case IO_SERVER_UDP_BE_READ_BY_REMOTE:
			BeRemoteRead(&ioMsg);
			break;
		case IO_SERVER_UDP_BE_WRITE_BY_REMOTE:
			BeRemoteWrite(&ioMsg);
			break;
		case IO_SERVER_UDP_TO_READ_REMOTE:
		case IO_SERVER_UDP_TO_WRITE_REMOTE: 
			ToReadToWriteRemote(&ioMsg);
			ReadWriteRemoteResponse(&ioMsg);
			break;

		case IO_SERVER_SAVE_DATA_TO_FILE:
			SaveIoDataTable();
			break;
			
		case IO_SERVER_UDP_TO_INVITE_FACTORY_DEFAULT:
			ToInviteFactoryDefault(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;
		case IO_SERVER_UDP_BE_INVITE_FACTORY_DEFAULT:
			BeInviteFactoryDefault(&ioMsg);
			break;			

		case IO_SERVER_UDP_TO_WRITE_DEFAULT_REMOTE:
			ToReadToWriteRemote(&ioMsg);
			ReadWriteRemoteResponse(&ioMsg);
			break;
			
		case IO_SERVER_UDP_BE_WRITE_DEFAULT_REMOTE:
			BeRemoteWriteDefault(&ioMsg);
			break;

		case PROPERTY_VALUE_INNER_TABLE_OPERATE:
			dprintf("PROPERTY_VALUE_INNER_TABLE_OPERATE start\n");
			
			if( io_server_table_cmd_process(&preq_msg->msg_dat,&rsp_msg) == 0 )
			{
				dprintf("PROPERTY_VALUE_INNER_TABLE_OPERATE ok\n");	
			}
			else
			{
				dprintf("PROPERTY_VALUE_INNER_TABLE_OPERATE er\n");				
			}
			io_server_inner_table_cmd_rsp( preq_msg->head.msg_source_id, &rsp_msg );
			break;
			
		case PROPERTY_VALUE_UART_TABLE_OPERATE:
			if( io_server_table_cmd_process(&preq_msg->msg_dat,&rsp_msg) == 0 )
			{
				dprintf("PROPERTY_VALUE_UART_TABLE_OPERATE ok\n");	
			}
			else
			{
				dprintf("PROPERTY_VALUE_UART_TABLE_OPERATE er\n");				
			}
			io_server_uart_table_cmd_rsp( preq_msg->head.msg_source_id, &rsp_msg );
			break;
			
		case IO_SERVER_RESTORE_DEFAULTS:
			RestoreUserOrInstallerDefaults(&ioMsg);
			ReadWritelocalResponse(&ioMsg);
			break;	
			
		default:
			break;
		}
	flag_switch_io = IO_SWITCH_ENABLE;
}

uint8 IfIoServerIsBusy(void)
{
	uint8 wait_s_count;
	
	if (flag_switch_io == IO_SWITCH_ENABLE)	//允许访问
	{
		;
	}
	else
	{
		wait_s_count = 10+50;	//最长延时5S
		while (wait_s_count > 10)	
		{
			usleep(100000);			//每次延时100ms
			wait_s_count--;
			if (flag_switch_io == IO_SWITCH_ENABLE)
			{
				wait_s_count = 0;		
			}
		}	
		if (wait_s_count != 0)
		{
			return (1);	//访问失败
		}
	}
	return 0;
}

void HexToDec(char* hex, char* dec)
{
	char* tempPos;
	unsigned int value;
	
	tempPos = strstr(hex,"0x");
	if(tempPos != NULL)
	{
		value = strtoul(tempPos,NULL,0);
		sprintf(hex, "%u", value);
	}
	
	strcpy(dec, hex);
}


// msg_id - 申请读的id
int API_Event_IoServer_InnerRead_All(char* property_id, char *ptr_data )
{
	IoRemote_S  data;
	int len;
	IoRemote_S temp;
	int value;
	char* tempPos;
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_READ_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;

	vdp_task_t* pTask = OS_GetTaskID();
	p_vdp_common_buffer pdb = 0;


	if( pTask != NULL && pTask->p_syc_buf!=NULL&&data.head.msg_source_id != MSG_ID_IOServer)
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		//WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_LOCAL, (char*)&temp, &len, 1 );
		
		while(pop_vdp_common_queue( pTask->p_syc_buf,&pdb,1)>0)
		{
			purge_vdp_common_queue(pTask->p_syc_buf);
		}
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		InnerRead(&data);
	}

	if(data.msg_dat.result)
	{
		strcpy(ptr_data,"0");
		return -2;
	}
	else
	{
		HexToDec(data.msg_dat.ptr_data, ptr_data);
		return 0;
	}
}


int API_Event_IoServer_InnerWrite_All(char* property_id, char *ptr_data)
{
	IoRemote_S  data;	
	int len;
	IoRemote_S temp;
	int *pValue;

	if(strlen(ptr_data)+1 > IO_DATA_LENGTH)
	{
		return -1;
	}

	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_WRITE_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;
	strcpy(data.msg_dat.ptr_data, ptr_data);
	
	vdp_task_t* pTask = OS_GetTaskID();


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}

		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_WRITE_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_WRITE_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
		
	}
	else
	{
		InnerWrite(&data);
	}
	
	if(!data.msg_dat.result && !strcmp(data.msg_dat.ptr_data, ptr_data))
	{
		return 0;
	}
	else
	{
		return -2;
	}
	
}

int API_Event_IoServer_RestoreDefaults(int userOrInstaller)
{
	IoRemote_S  data;	
	int len;
	IoRemote_S temp;

	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_RESTORE_DEFAULTS;
	data.head.msg_sub_type 	= userOrInstaller;
	
	vdp_task_t* pTask = OS_GetTaskID();


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			dprintf("IO_Server busy!!!\n");
			return -3;
		}

		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_RESTORE_DEFAULTS, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_RESTORE_DEFAULTS, (char*)&data, &len, 5000 ) != 1)
		{
			dprintf("no ACK !!!\n");
			return -1;
		}
		
	}
	else
	{
		RestoreUserOrInstallerDefaults(&data);
	}

	
	if(!data.msg_dat.result)
	{
		return 0;
	}
	else
	{
		dprintf("response error !!!\n");
		return -1;
	}
	
}



int API_io_server_save_data_file(void)
{
	io_server_type  data;
	data.head.msg_target_id 	= MSG_ID_IOServer;
	data.head.msg_source_id	= MSG_ID_IOServer;			
	data.head.msg_type		= IO_SERVER_SAVE_DATA_TO_FILE;
	push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data) );
	return 0;
}

//按等级恢复出厂设置
int API_Event_IoServer_Inner_factoryDefaultByLevel(int level)
{
	IoRemote_S  data;
	int len;
	IoRemote_S temp;
	int value;
	char* tempPos;
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_FACTORY_DEFAULT_BY_LEVEL_LOCAL;
	data.head.msg_sub_type 	= level;

	vdp_task_t* pTask = OS_GetTaskID();
	


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_FACTORY_DEFAULT_BY_LEVEL_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_FACTORY_DEFAULT_BY_LEVEL_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		InnerFactoryDefaultByLevel(&data);
	}

	if(data.msg_dat.result)
	{
		return -2;
	}
	else
	{
		return 0;
	}
}


int API_Event_IoServer_Inner_factoryDefaultById(char* property_id)
{
	IoRemote_S  data;	
	int len;
	IoRemote_S temp;
	int *pValue;
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_FACTORY_DEFAULT_BY_ID_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;
	
	vdp_task_t* pTask = OS_GetTaskID();


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}

		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_FACTORY_DEFAULT_BY_ID_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_FACTORY_DEFAULT_BY_ID_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		InnerFactoryDefaultById(&data);
	}
	
	if(!data.msg_dat.result)
	{
		return 0;
	}
	else
	{
		return -2;
	}
}



int API_Event_IoServer_Inner_judgeRange(char* property_id, char *ptr_data)
{
	IoRemote_S  data;	
	int len;
	IoRemote_S temp;
	int *pValue;
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_JUDGE_RANGE_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;
	strcpy(data.msg_dat.ptr_data, ptr_data);
	
	vdp_task_t* pTask = OS_GetTaskID();


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}

		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_JUDGE_RANGE_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_JUDGE_RANGE_LOCAL, (char*)&data, &len, 5000 ) > 0)
		{
			if(!data.msg_dat.result)
			{
				return 0;
			}
			else
			{
				return -2;
			}
		}
		return -3;
	}
	else
	{
		InnerJudgeRange(&data);

		if(!data.msg_dat.result)
		{
			return 0;
		}
		else
		{
			return -2;
		}
		return -3;
	}
}

//读取参数默认值
int API_Event_IoServer_InnerRead_Default(char* property_id, char *ptr_data )
{
	IoRemote_S  data;
	int len;
	IoRemote_S temp;
	int value;
	char* tempPos;
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_READ_DEFAULT_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;

	vdp_task_t* pTask = OS_GetTaskID();
	


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_DEFAULT_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_DEFAULT_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		InnerReadDefault(&data);
	}

	
	if(data.msg_dat.result)
	{
		return -2;
	}
	else
	{
		HexToDec(data.msg_dat.ptr_data, ptr_data);
		return 0;
	}
}


int API_Event_IoServer_InnerRead_Property(char* property_id, ParaProperty_t *property)
{
	IoRemote_S  data;
	int len;
	IoRemote_S temp;
	int value;
	char* tempPos;
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_READ_PROPERTY_LOCAL;
	data.head.msg_sub_type 	= 0;
	snprintf(data.msg_dat.property_id, 5, "%s", property_id);
	data.msg_dat.result		= 1;

	vdp_task_t* pTask = OS_GetTaskID();
	

	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_PROPERTY_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_PROPERTY_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		ReadProperty(&data);
	}

	if(data.msg_dat.result)
	{
		return -2;
	}
	else
	{
		property->editType 	= data.msg_dat.editType;
		property->source	= data.msg_dat.source;
		property->readWrite	= data.msg_dat.readWrite;
		property->effective = data.msg_dat.effective;
		return 0;
	}
}


int API_Event_IoServer_InnerRead_Group(char *paraData)
{
	IoRemote_S  data;
	int len;
	IoRemote_S temp;
	int value;
	char* tempPos;
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_READ_GROUP_LOCAL;
	data.head.msg_sub_type 	= 0;
	strcpy(data.msg_dat.ptr_data, paraData);
	data.msg_dat.result		= 1;

	vdp_task_t* pTask = OS_GetTaskID();
	

	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_GROUP_LOCAL, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_READ_GROUP_LOCAL, (char*)&data, &len, 5000 ) != 1)
		{
			return -3;
		}
	}
	else
	{
		InnerReadGroup(&data);
	}

	if(data.msg_dat.result)
	{
		return -2;
	}
	else
	{
		if(paraData != NULL)
		{
			strcpy(paraData, data.msg_dat.ptr_data);
		}
		return 0;
	}
}





int API_io_server_UDP_to_invite_factoryDefault(int targetIp, uint16 addr)
{
	IoRemote_S  data;
	int dataLen;
	IoRemote_S temp;

	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_UDP_TO_INVITE_FACTORY_DEFAULT;
	data.head.msg_sub_type 	= 0;
	data.targetIp				= targetIp;
	data.cmd					= UDP_IO_SERVER_CMD_DEFAULT;
	data.addr					= addr;
	data.msg_dat.result		= 1;
	
	vdp_task_t* pTask = OS_GetTaskID();


	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_INVITE_FACTORY_DEFAULT, (char*)&temp, &dataLen, 1 );
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_INVITE_FACTORY_DEFAULT, (char*)&data, &dataLen, 5000 ) > 0)
		{	
			if(data.msg_dat.result)
			{
				return -2;
			}
			else
			{
				return 0;
			}
		}
		return -3;
	}
	else
	{
		return -1;
	}
}

int API_io_server_UDP_to_read_remote(int targetIp, int addr, char *ptr_data)
{
	IoRemote_S  data;
	IoRemote_S temp;
	int dataLen;

	if(strlen(ptr_data)+1 > IO_DATA_LENGTH)
	{
		return -5;
	}

	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_UDP_TO_READ_REMOTE;
	data.head.msg_sub_type 	= 0;
	data.targetIp			= targetIp;
	data.cmd				= UDP_IO_SERVER_CMD_R;
	data.addr				= addr;
	data.msg_dat.result		= 1;
	strcpy(data.msg_dat.ptr_data, ptr_data);
	
	vdp_task_t* pTask = OS_GetTaskID();

	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_READ_REMOTE, (char*)&temp, &dataLen, 1);
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_READ_REMOTE, (char*)&data, &dataLen, 5000 ) > 0)
		{
			if(data.msg_dat.result)
			{
				return -2;
			}
			else
			{
				dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
				memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
				ptr_data[dataLen] = 0;
				return 0;
			}
		}
		return -3;
	}
	else
	{
		ToReadToWriteRemote(&data);
		if(data.msg_dat.result)
		{
			return -2;
		}
		else
		{
			dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
			memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
			ptr_data[dataLen] = 0;
			return 0;
		}
	}
}

int API_io_server_UDP_to_read_one_remote(int targetIp, char* paraId, char *ptr_data)
{
	char display[600];
	char *pos1, *pos2;
	int len, ret;
	
	sprintf(display, "<ID=%s>", paraId);
	ret = API_io_server_UDP_to_read_remote(targetIp, 0xFFFFFFFF, display);
	pos1 = strstr(display, "Value=");
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			len = ((int)(pos2-pos1))-strlen("Value=");
			memcpy(ptr_data, pos1+strlen("Value="), len);
			ptr_data[len] = 0;
			ret = 0;
		}
		else
		{
			ret = -1;
		}
	}
	else
	{
		ret = -1;
	}
	
	return ret;
}

int API_io_server_UDP_to_write_remote(int targetIp, int addr, char *ptr_data)
{
	IoRemote_S  data;
	IoRemote_S temp;
	int dataLen;

	if(strlen(ptr_data)+1 > IO_DATA_LENGTH)
	{
		return -5;
	}

	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_UDP_TO_WRITE_REMOTE;
	data.head.msg_sub_type 	= 0;
	data.targetIp			= targetIp;
	data.cmd				= UDP_IO_SERVER_CMD_W;
	data.addr				= addr;
	data.msg_dat.result		= 1;
	
	strcpy(data.msg_dat.ptr_data, ptr_data);
	
	vdp_task_t* pTask = OS_GetTaskID();

	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_WRITE_REMOTE, (char*)&temp, &dataLen, 1);
			
		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(data.msg_dat.ptr_data)+1);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_WRITE_REMOTE, (char*)&data, &dataLen, 5000 ) > 0)
		{
			if(data.msg_dat.result)
			{
				return -2;
			}
			else
			{
				dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
				memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
				ptr_data[dataLen] = 0;
				return 0;
			}
		}
		return -3;
	}
	else
	{
		ToReadToWriteRemote(&data);
		if(data.msg_dat.result)
		{
			return -2;
		}
		else
		{
			dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
			memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
			ptr_data[dataLen] = 0;
			return 0;
		}
	}
}

int API_io_server_UDP_to_write_one_remote(int targetIp, char* paraId, char *ptr_data)
{
	char display[600];
	char *pos1, *pos2;
	int len, ret;
	
	if(strlen(ptr_data) + strlen("<ID=1000,Value=>") >= IO_DATA_LENGTH)
	{
		return -5;
	}
	
	sprintf(display, "<ID=%s,Value=%s>", paraId, ptr_data);
	ret = API_io_server_UDP_to_write_remote(targetIp, 0xFFFFFFFF, display);
	pos1 = strstr(display, "Value=");
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			len = ((int)(pos2-pos1))-strlen("Value=");
			memcpy(ptr_data, pos1+strlen("Value="), len);
			ptr_data[len] = 0;
			ret = 0;
		}
		else
		{
			ret = -1;
		}
	}
	else
	{
		ret = -1;
	}
	
	return ret;
}


int API_io_server_UDP_to_write_remote_factoryDefault(int targetIp, char* ptr_data)
{
	IoRemote_S  data;
	IoRemote_S temp;
	int dataLen;

	if(strlen(ptr_data) > IO_DATA_LENGTH)
	{
		return -5;
	}

	memset(&data, 0, sizeof(data));
	
	// 组织io数据
	data.head.msg_target_id	= MSG_ID_IOServer;
	data.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	data.head.msg_type 		= IO_SERVER_UDP_TO_WRITE_DEFAULT_REMOTE;
	data.head.msg_sub_type 	= 0;
	data.targetIp			= targetIp;
	data.cmd				= UDP_IO_SERVER_CMD_ONE_DEFAULT;
	data.addr				= 0xFFFFFFFF;
	data.msg_dat.result		= 1;
	
	strncpy(data.msg_dat.ptr_data, ptr_data, IO_DATA_LENGTH+1);
	
	vdp_task_t* pTask = OS_GetTaskID();

	if( pTask != NULL )
	{
		if(IfIoServerIsBusy())		//IO_Server忙
		{
			return -4;
		}
		
		WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_WRITE_DEFAULT_REMOTE, (char*)&temp, &dataLen, 1);
			
		dataLen = sizeof(data) - IO_DATA_LENGTH + strlen(data.msg_dat.ptr_data);

		push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&data, dataLen);
		// 等待ack应答
		if(WaitForBusinessACK( pTask->p_syc_buf, IO_SERVER_UDP_TO_WRITE_DEFAULT_REMOTE, (char*)&data, &dataLen, 5000 ) > 0)
		{
			if(data.msg_dat.result)
			{
				return -2;
			}
			else
			{
				dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
				memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
				ptr_data[dataLen] = 0;
				return 0;
			}
		}
		return -3;
	}
	else
	{
		ToReadToWriteRemote(&data);
		if(data.msg_dat.result)
		{
			return -2;
		}
		else
		{
			dataLen = strlen(data.msg_dat.ptr_data)+1 > IO_DATA_LENGTH ? IO_DATA_LENGTH-1 : strlen(data.msg_dat.ptr_data);
			memcpy( ptr_data, data.msg_dat.ptr_data, dataLen);
			ptr_data[dataLen] = 0;
			return 0;
		}
	}
}

int API_io_server_UDP_to_write_one_remote_factoryDefault(int targetIp, char* paraId, char *ptr_data)
{
	char display[IO_DATA_LENGTH+1];
	char *pos1, *pos2;
	int len, ret;
	
	sprintf(display, "<ID=%s>", paraId);
	
	
	ret = API_io_server_UDP_to_write_remote_factoryDefault(targetIp, display);
	pos1 = strstr(display, "Value=");
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			len = ((int)(pos2-pos1))-strlen("Value=");
			if(len == 0)
			{
				ret = -2;
			}
			memcpy(ptr_data, pos1+strlen("Value="), len);
			ptr_data[len] = 0;
			ret = 0;
		}
		else
		{
			ret = -1;
		}
	}
	else
	{
		ret = -1;
	}
	
	return ret;
}



int API_io_server_UDP_CMD_process(char* pData, int len)
{
	return push_vdp_common_queue(&vdp_io_server_mesg_queue, pData, len);
}


// lzh_20160603_s
// 内部请求表操作接口
int API_io_server_inner_table_cmd_req( REMOTE_TABLE_CMD_REQ* ptable_cmd_req, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp )
{
	REMOTE_TABLE_CMD_REQ_IF	req_msg;
	REMOTE_TABLE_CMD_RSP_IF	rsp_msg;
	int	len = sizeof(REMOTE_TABLE_CMD_RSP_IF);
	int ret;

	req_msg.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	req_msg.head.msg_target_id	= MSG_ID_IOServer;
	req_msg.head.msg_type		= PROPERTY_VALUE_INNER_TABLE_OPERATE;
	req_msg.head.msg_sub_type	= 0;
	req_msg.msg_dat				= *ptable_cmd_req;

	dprintf("API_io_server_inner_table_cmd_req start: len=%d\n",sizeof(REMOTE_TABLE_CMD_REQ_IF));

	push_vdp_common_queue(&vdp_io_server_mesg_queue, (char*)&req_msg, sizeof(REMOTE_TABLE_CMD_REQ_IF) );
	// 等待ack应答
	ret = WaitForBusinessACK( GetTaskAccordingMsgID(req_msg.head.msg_source_id)->p_syc_buf, PROPERTY_VALUE_INNER_TABLE_OPERATE, (char*)&rsp_msg, &len, 5000 );
	if( ret > 0 )
	{
		*ptable_cmd_rsp = rsp_msg.msg_dat;
		dprintf("API_io_server_inner_table_cmd_req ok\n");
		return 0;
	}
	else
	{
		dprintf("API_io_server_inner_table_cmd_req er\n");
		return -1;
	}
}

// 应答内部表操作接口
int io_server_inner_table_cmd_rsp( unsigned char msg_target_id, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp )
{
	REMOTE_TABLE_CMD_RSP_IF	rsp_msg;

	rsp_msg.head.msg_source_id	= MSG_ID_IOServer;
	rsp_msg.head.msg_target_id	= msg_target_id;
	rsp_msg.head.msg_type		= PROPERTY_VALUE_INNER_TABLE_OPERATE|COMMON_RESPONSE_BIT;
	rsp_msg.head.msg_sub_type	= 0;
	rsp_msg.msg_dat				= *ptable_cmd_rsp;

	push_vdp_common_queue(GetTaskAccordingMsgID(msg_target_id)->p_syc_buf, (char*)&rsp_msg, sizeof(REMOTE_TABLE_CMD_RSP_IF) );
	
	dprintf("API_io_server_inner_table_cmd_rsp ok\n");
	return 0;
}

// 应答串口表操作接口
int io_server_uart_table_cmd_rsp( unsigned char msg_target_id, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp )
{
	REMOTE_TABLE_CMD_RSP_IF	rsp_msg;

	rsp_msg.head.msg_source_id	= MSG_ID_IOServer;
	rsp_msg.head.msg_target_id	= msg_target_id;
	rsp_msg.head.msg_type		= PROPERTY_VALUE_UART_TABLE_OPERATE|COMMON_RESPONSE_BIT;
	rsp_msg.head.msg_sub_type	= 0;
	rsp_msg.msg_dat				= *ptable_cmd_rsp;

	//vdp_uart_send_data((char*)&rsp_msg, sizeof(REMOTE_TABLE_CMD_RSP_IF));

	dprintf("io_server_uart_table_cmd_rsp ok\n");
	return 0;
}

//czn_20161008_s
int io_server_table_cmd_process( REMOTE_TABLE_CMD_REQ* ptable_cmd_req, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp )	//czn_20160708
{
	ip_table_t 		ip_value_items;
	dev_table_t 	dev_value_items;
	int				ip_match_state;		//  -2 /表错误，-1/无匹配项，0 /无匹配结果， 1-n/ 匹配ok，匹配的个数
	int				dev_match_state;	//  -2 /表错误，-1/无匹配项，0 /无匹配结果， 1-n/ 匹配ok，匹配的个数
	one_vtk_table*	pseach_ip_table = NULL;
	one_vtk_table*	pseach_dev_table = NULL;
		
	// init rsp cmd
	ptable_cmd_rsp->tab_type 	= ptable_cmd_req->tab_type;
	ptable_cmd_rsp->opt_cmd 	= ptable_cmd_req->opt_cmd;
	ptable_cmd_rsp->ip_state 	= 0;
	ptable_cmd_rsp->dev_state 	= 0;
	
	if( ptable_cmd_req->tab_type == GLOBAL_DEVICE_TABLE )
	{
		// input数据
		if( ptable_cmd_req->opt_cmd == TABLE_OPT_CMD_GET_VALUE_BY_INPUT )
		{
			// 判断IP段是否为空
			if( ptable_cmd_req->ip_input[0] != 0 )
			{
			 	// 在IP表中搜索符合键值的所有记录保存到临时表中
				pseach_ip_table = search_vtk_table_with_key_value( pip_map_table, TBGW_KEY_PREINPUT,ptable_cmd_req->ip_input, 0);
				//printf_one_table(pseach_ip_table);
									
				if( pseach_ip_table == NULL )
					ip_match_state = -2;	// 表错误
					
				else if( pseach_ip_table->record_cnt == 0 )
					ip_match_state = 0;		// 无匹配
					
				else
				{				
					ip_match_state = pseach_ip_table->record_cnt;		//  匹配ok					
					// 得到表中的第一条记录
					if( get_ip_table_record_items(pseach_ip_table,0,&ip_value_items) == -1 )
					{
						ip_match_state = 0;
					}
				}				
			}
			else
			{
				ip_match_state = -1;	// 无匹配项
			}
			
			// 判断DEV段是否为空
			if( ptable_cmd_req->dev_input[0] != 0 )
			{
			 	// 在DEV表中搜索符合键值的所有记录保存到临时表中
				pseach_dev_table = search_vtk_table_with_key_value( pdevice_map_table, TBRM_KEY_INPUT,ptable_cmd_req->dev_input,0);
				//printf_one_table(pseach_dev_table);
				if( pseach_dev_table == NULL )
					dev_match_state = -2;	// 表错误
					
				else if( pseach_dev_table->record_cnt == 0 )	// 无匹配结果
				{
					// 未匹配到DEVICE表，若IP段无匹配项，则需要在IP段中查找 				
					if( ip_match_state == -1 )
					{
						// 在IP表中搜索符合键值的所有记录保存到临时表中					
						pseach_ip_table = search_vtk_table_with_key_value( pip_map_table, TBGW_KEY_PREINPUT,ptable_cmd_req->dev_input,1);
						//printf_one_table(pseach_ip_table);
						
						if( pseach_ip_table == NULL )							
							dev_match_state = -2;	// 表错误
							
						else if( pseach_ip_table->record_cnt == 0 )
							dev_match_state = 0; 	// 无匹配
							
						else
						{
							dev_match_state = -1;	// 无匹配项 - ip有匹配项			
							ip_match_state = pseach_ip_table->record_cnt;	//	匹配ok					
							// 得到表中的第一条记录
							if( get_ip_table_record_items(pseach_ip_table,0,&ip_value_items) == -1 )
							{
								ip_match_state = -2;
							}
						}
					}
					else
					{
						dev_match_state = 0;		// 无匹配
					}
				}
				else
				{
					dev_match_state = pseach_dev_table->record_cnt;		//  匹配ok	

					int i;
					// 从第一条记录开始搜索，直到匹配成功为止
					for( i = 0; i < pseach_dev_table->record_cnt; i++ )
					{
						if( get_device_table_record_items(pseach_dev_table,i,&dev_value_items) == 0 )
						{
							// 若已有IP表搜索，则需要匹配IP表的IPNODE_ID是否匹配；若IP表搜索到多条，则需要逐条匹配
							if( ip_match_state > 0 )
							{
								int j;
								for( j = 0; j < pseach_ip_table->record_cnt; j++ )
								{
									// 得到ip表中的一条记录
									if( get_ip_table_record_items(pseach_ip_table,j,&ip_value_items) == -1 )
									{
										ip_match_state = -2;
										break;
									}								
									else if( dev_value_items.ipnode_id == ip_value_items.ipnode_id )
									{
										// IP表和DEVICE表都匹配正确
										ip_match_state 	= 1;	//	匹配唯一ok										
										dev_match_state = 1; 	//	匹配唯一ok										
										break;
									}						
								}
								if( j == pseach_ip_table->record_cnt )
								{
									bprintf("dev_value_items.ipnode_id == ip_value_items.ipnode_id,no match\n"); 							
								}
								else
								{
									bprintf("dev_value_items.ipnode_id == ip_value_items.ipnode_id,match=%d\n",j); 							
									break;
								}
							}
							// 无IP表搜索项，搜索到dev的记录同时需要得到ip记录属性
							else if( ip_match_state == -1 )
							{
								// 在IP表中搜索符合键值的所有记录保存到临时表中
								one_vtk_dat* ptemp_rec;
								int ipnode_id_index;
								char ipnode_str[20];
								int ipnode_id_len;
								ipnode_id_len = 20;
								ipnode_id_index = get_keyname_index( pseach_dev_table, TBRM_KEY_GWSN);		
								ptemp_rec = get_one_vtk_record_without_keyvalue( pseach_dev_table, i );
								
								bprintf("-------------get_one_vtk_record_without_keyvalue = %s\n",ptemp_rec->pdat);								
								
								get_one_record_string( ptemp_rec, ipnode_id_index, ipnode_str, &ipnode_id_len );
								ipnode_str[ipnode_id_len] = 0;
								
								bprintf("-------------get_one_record_string = %s,len=%d\n",ipnode_str,ipnode_id_len);
								
								pseach_ip_table = search_vtk_table_with_key_value( pip_map_table, TBGW_KEY_GWSN,(unsigned char*)ipnode_str, 1);			
								//printf_one_table(pseach_ip_table);
								if( pseach_ip_table == NULL )
								{
									dev_match_state = -2;	// 表错误										
								}
								else
								{
									dev_match_state = 1;	//	匹配ok										
									if( get_ip_table_record_items(pseach_ip_table,0,&ip_value_items) == 0 )
									{
										ip_match_state	= 1;	//  获取ok
									}
									else
									{
										ip_match_state	= -2;	// 表错误	
									}
								}	
								break;							
							}
							// 无IP表搜索结果
							else
							{							
								// 匹配到DEVICE表
								break;							
							}							
						}
						else
						{
							dev_match_state = -2;	// 表错误	
							break;
						}
					}
					if( i == pseach_dev_table->record_cnt )
					{
						ip_match_state = 0;		//  未匹配
						dev_match_state = 0;	//  未匹配
					}
				}
			}
			else
			{
				dev_match_state = -1;	// 无匹配项
			}
			
			// 得到匹配结果
			ptable_cmd_rsp->ip_state 		= ip_match_state;
			ptable_cmd_rsp->dev_state 		= dev_match_state;
			ptable_cmd_rsp->ip_table_value	= ip_value_items;
			ptable_cmd_rsp->dev_table_value	= dev_value_items;
		}
	}

	// 释放临时表
	release_one_vtk_tabl(pseach_ip_table);
	release_one_vtk_tabl(pseach_dev_table);
	
	dprintf("io_server_table_cmd_process: ip_match_state = %d, dev_match_state=%d\n",ip_match_state,dev_match_state);

	return 0;
}

int IX_DeviceSelectNetwork(void)
{
	char temp[5];
	int ret = 3;
	
	if(API_Event_IoServer_InnerRead_All(IP_DeviceSeletNetwork, temp) == 0)
	{
		ret = atoi(temp);
	}
	
	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
