/**
  ******************************************************************************
  * @file    obj_LockProperty.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_LockProperty_H
#define _obj_LockProperty_H

// Define Object Property-------------------------------------------------------

typedef struct
{
	int lock1Enable;
	int lock2Enable;
} LOCK_PROPERTY_T;


// Define Object Function - Public----------------------------------------------
void GetLockProperty(int ip, LOCK_PROPERTY_T* property);

// Define Object Function - Private---------------------------------------------

// Define Object Function - Other-----------------------------------------------


#endif
