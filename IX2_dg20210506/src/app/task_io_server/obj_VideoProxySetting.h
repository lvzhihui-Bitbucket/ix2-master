/**
  ******************************************************************************
  * @file    obj_VideoProxySetting.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

  
#ifndef obj_VideoProxySetting_H
#define obj_VideoProxySetting_H

#include "obj_IPCTableSetting.h"

#define VIDEO_PROXY_DATA_LEN			100

/*
{
    "type":"IX",
    "IX":
    	{
    		"addr": "0099990101"
    	},
    	
    "IPC":{
    	"ID": "", 
    	"NAME":"ipc", 
    	"IP": "192.168.243.1119", 
    	"CH": 2, 
    	"User": "",
    	"PWD": "",
    	},

};
*/

typedef struct 
{
	char	addr[VIDEO_PROXY_DATA_LEN];
} IX_DEVICE ;

typedef struct 
{
	char	ID[VIDEO_PROXY_DATA_LEN];
	char	NAME[VIDEO_PROXY_DATA_LEN];
	char	IP[VIDEO_PROXY_DATA_LEN];
	int		CH;
	char	USER[VIDEO_PROXY_DATA_LEN];
	char	PWD[VIDEO_PROXY_DATA_LEN];
} IPC_DEVICE ;

typedef struct 
{
	char 		type[VIDEO_PROXY_DATA_LEN];
	IX_DEVICE 	ixDevice;
	IPC_DEVICE 	ipcDevice;
	int have_ipcinfo;
	login_dat_t ipcInfo;
	//onvif_login_info_t 	ipcInfo;
} VIDEO_PROXY_JSON;



#endif
