/**
  ******************************************************************************
  * @file    obj_CustomerizedIoTable.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_CustomerizedIoTable_H
#define _obj_CustomerizedIoTable_H

// Define Object Property-------------------------------------------------------

#define VALUE_LEN			500
#define ID_LEN				4
#define BUFF_LEN			500


#define PARA_ID								"paraId"		//参数序号
#define NAME								"name"			//参数名字
#define EFFECTIVE							"effective"		//参数名字
#define COMMON								"common"		//远程访问参数分组
#define LEVEL								"level"			//参数恢复等级
#define GROUP								"group"			//本地访问参数分组
#define SOURCE								"source"		//参数来源
#define PROPERTY							"property"		//参数读写属性 1只读，2只写，3可读写
#define VALUE_TYPE							"valueType"		//参数值类型，0数值，1字符串
#define DUFAULT								"default"		//参数默认值
#define	EDIT_TYPE							"editType"		//编辑类型 0输入型，1选择框
#define MIN_VALUE							"min"			//参数最小值
#define MAX_VALUE							"max"			//参数最大值
#define VALUE								"value"			//参数当前值

typedef struct
{
	char paraId[ID_LEN+1];
	char value[VALUE_LEN+1];
} CUSTOMERIAED_IO_T;


// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------

// Define Object Function - Other-----------------------------------------------


#endif
