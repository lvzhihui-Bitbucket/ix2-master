
#ifndef _OBJ_MEMO_H
#define _OBJ_MEMO_H

typedef struct
{
	int					state;
	char				filename[100];
	int					max_len;
	void 				*pRelation;
	int 				spriteState;
    pthread_mutex_t 	lock;	
} vd_record_t;


typedef struct
{
	int					state;
	char				filename[100];
	int					max_len;
	int					cur_len;
	void 				*pRelation;	
    pthread_mutex_t 	lock;
} vd_playback_t;

// 得到sd卡的容量:
typedef struct
{
	//sd卡的状态
	int		sd_status;
	
	//sd卡的容量
	int		sd_capacity;
	
	//SD卡总的视频文件容量
	int 	video_capacity;
	
	//SD卡未读视频文件的个数
	int		video_nread;
	
	// SD卡视频文件的个数
	int 	video_nls;
} MEDIA_INFO;

extern vd_record_t		one_vd_record;

//record api---------------------------------------------------------------------------------------
int create_video_record_filename( char* pfilename );

// 录像实例初始化
void memo_vd_record_init(void);

// 启动录像: prelation - 为关联的CALL_RECORD_DAT_T指针
// return: 0/ok, -1/err
int memo_video_record_start( void* prelation );

// 停止录像: 座位录像结束后的回调函数，也可手动停止，停止后写入了关联的记录
void memo_vd_record_stop(void);

//playback api-------------------------------------------------------------------------------------
// 播放实例初始化
void memo_vd_playback_init(void);

// 启动播放: filename - 播放的文件名全路径， prelation - 为关联的CALL_RECORD_DAT_T指针
// return: 0/ok, -1/err
int memo_video_playback_start( char* filename, void* prelation );

// 暂停播放
// return: 1/ok, 0/err
int memo_video_playback_pause( void );

// 继续播放
// return: 1/ok, 0/err
int memo_video_playback_continue( void );

// 停止播放
void memo_vd_playback_stop(void);

//media info api-------------------------------------------------------------------------------------
// 格式化SD 卡
int memo_format_sdcard(void);

// 得到sd card 的容量信息
// return: 0/ok，1/err
int memo_get_sdcard_inform(void);

#endif


