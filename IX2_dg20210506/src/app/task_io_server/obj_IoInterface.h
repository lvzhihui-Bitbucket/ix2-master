/**
  ******************************************************************************
  * @file    obj_IoInterface.h
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"

typedef int IO_bool;

#define IO_Identifier                            "IO"

#define NDM_Config		                "NDM_Config"
    #define NDM_S_BOOT		                "NDM_S_BOOT"
    #define NDM_C_BOOT		                "NDM_C_BOOT"
    #define NDM_S_TIME		                "NDM_S_TIME"
    #define NDM_S_GET_PB  		            "NDM_S_GET_PB"
    #define NDM_C_TIME		                "NDM_C_TIME"
    #define NDM_C_CHECK		                "NDM_C_CHECK"
    #define NDM_S_IP_ADDR		              "NDM_S_IP_ADDR"
    #define NDM_S_MFG_SN		              "NDM_S_MFG_SN"

#define DEV_Manage		                "DEV_Manage"
    #define CERT_GoLive_Timer		           "CERT_GoLive_Timer"
    #define CHECK_TB_TIME		               "CHECK_TB_TIME"
    #define TB_TRANSFER_DIR		             "TB_TRANSFER_DIR"

    #define Search_Role                         "Search_Role"
    #define Search_Last_Server_Enable           "Search_Last_Server_Enable"
    #define Search_Server_Inform_Timer          "Search_Server_Inform_Timer"
    #define Search_Device_Min_Timer             "Search_Device_Min_Timer"
    #define Search_Device_Timer                 "Search_Device_Timer"
    #define Iperf_Test_Time                     "Iperf_Test_Time"

#define ElogConfig                           "ElogConfig"
    #define ELOG_FILE_SIZE                      "ELOG_FILE_SIZE"
    #define ELOG_PRINTF_LVL                     "ELOG_PRINTF_LVL"
    #define ELOG_FILE_LVL                       "ELOG_FILE_LVL"
    #define ELOG_UDP_LVL                        "ELOG_UDP_LVL"
    #define ELOG_IX_LVL                         "ELOG_IX_LVL"
    #define ELOG_UDP_IP                         "ELOG_UDP_IP"
    #define EventLogCfg                         "EventLogCfg"
    #define TLogServerEnable                    "TLogServerEnable"
    #define TLogIX_ADDR                         "TLogIX_ADDR"

#define NetConfig                           "NetConfig"
    #define IX_NT                             "IX_NT"
    #define Search_Record                       "Search_Record"
    #define LAN_Search_Timeout                  "LAN_Search_Timeout"
        #define Lan_S1_Timeout                  "Lan_S1_Timeout"
        #define Lan_S1_IntervalTime                 "Lan_S1_IntervalTime"
        #define Lan_S2_Enable                       "Lan_S2_Enable"
        #define Lan_S2_Timeout                      "Lan_S2_Timeout"
        #define Lan_S2_IntervalTime                 "Lan_S2_IntervalTime"
        #define Lan_S2_MinCnt                        "Lan_S2_MinCnt"


    #define WLAN_Search_Timeout                 "WLAN_Search_Timeout"
        #define Wlan_S1_Timeout                      "Wlan_S1_Timeout"
        #define Wlan_S1_IntervalTime                 "Wlan_S1_IntervalTime"
        #define Wlan_S2_Enable                       "Wlan_S2_Enable"
        #define Wlan_S2_Timeout                      "Wlan_S2_Timeout"
        #define Wlan_S2_IntervalTime                 "Wlan_S2_IntervalTime"
        #define Wlan_S2_MinCnt                       "Wlan_S2_MinCnt"
	
//#define RADAR_MOTION_DISTANCE_LEVEL    	"RADAR_MOTION_DISTANCE_LEVEL"

int API_LoadIoDefineFile(const char* ioDefineFilePath);
int API_LoadIoCusFile(const char* ioCusFilePath);

/*******************************************************************
 * const cJSON * API_Para_Read_Public(const char* key);
 * 
 * 注意返回的cJSON指针，使用完之后
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放
 *******************************************************************/
const cJSON * API_Para_Read_Public(const char* key);
int API_Para_Read_Int(const char* paraName);
int API_Para_Read_String(const char* paraName, char* value);
char* API_Para_Read_String2(const char* paraName);

int API_Para_Write_Public(const cJSON* value);
int API_Para_Write_Int(const char* paraName, int value);
int API_Para_Write_String(const char* paraName, char* value);

/*******************************************************************
 * const cJSON* API_Para_Read_Default(const char* paraName)
 * const cJSON* API_Para_ReadByID_Default(const char* paraId);
 * 注意返回的cJSON指针，使用完之后
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放
 *******************************************************************/
const cJSON* API_Para_Read_Default(const char* paraName);
int API_Para_Restore_Default(const char* paraName);

void InitIOServer(void);


//返回1校验通过，返回0校验不通过
//校验文件中不存在参数名当作校验通过处理
int API_IO_Check(char* key, cJSON* value);

//为了使用方便，提供了string和int校验接口
int API_IO_CheckString(char* key, char* value);
int API_IO_CheckInt(char* key, int value);

//即刻保存IO参数
void API_IOSaveImmediately(void);

void MyElogJson(int level, const char* name, const cJSON *json);
void MyElogHex(int level, const char* hex, int len);
void MyPrintJson(const char* name, const cJSON *json);
/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
