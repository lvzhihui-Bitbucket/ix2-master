

#ifndef _obj_ip_device_table_H
#define _obj_ip_device_table_H

#include "obj_TableProcess.h"

//czn_20160812_s
#define TBGW_KEY_GWSN				"GATEWAY_SN"
#define TBGW_KEY_DEVTYPE			"DEVICE_TYPE"
#define TBGW_KEY_PROPERTY			"PROPERTY"
#define TBGW_KEY_IPADDR			"IP_ADDR"
#define TBGW_KEY_EXMODE			"EXTEND_MODE"
#define TBGW_KEY_PREINPUT			"INPUT_PREFIX"
#define TBGW_KEY_NAME				"NAME"

#define TBRM_KEY_GWSN				"GATEWAY_SN"
#define TBRM_KEY_INPUT				"INPUT"
#define TBRM_KEY_NAME				"NAME"
#define TBRM_KEY_ADDR				"ADDR"
#define TBRM_KEY_VIRTUALDEV		"VIRTUAL_DEVICE"
//czn_20160812_e

// lzh_20160601_s
extern one_vtk_table * pdevice_map_table;
extern one_vtk_table * pip_map_table;
extern one_vtk_table * pvtk_gl_device_table;
extern one_vtk_table * pvtk_cds_device_table;
extern one_vtk_table * pvtk_im_device_table;
extern one_vtk_table * pvtk_ds_device_table;
extern one_vtk_table * pvtk_temp_device_table;		// uart远程命令生成的临时表指针，仅允许1个
// lzh_20160601_e

//czn_20160708_s
typedef enum
{
	IPDevType_Gateway_IPG = 0,
	IPDevType_EntryUnit_IPG,
	IPDevType_EntryUnit,
	IPDevType_GuardUnit,
	IPDevType_PCStation,
	IPDevType_Unkown,
}IPDevType_Enum;

typedef enum
{
	IPG_Extend_NoExtend,
	IPG_Extend_Dt32,
	IPG_Extend_Dt128,
	IPG_Extend_AtSys,
	IPG_Extend_Unkown,
}IPG_ExtendMode_Enum;

//czn_20160708_e



#pragma pack(1)

#define MAX_IP_STR_LEN		8
#define MAX_DEV_STR_LEN		8
#define MAX_DEV_NAME_LEN	20
//czn_20160708_s
typedef struct
{
	short			ipnode_id;
	char				device_type;
	char				proterty;
	int				ip_addr;
	char				extend_mode;
	unsigned char		ip_input[MAX_IP_STR_LEN+1];
	unsigned char		name[MAX_DEV_NAME_LEN+1];
	
} ip_table_t;

typedef struct
{
	short				ipnode_id;		//czn_20170222
	unsigned char		ip_input[MAX_IP_STR_LEN+1];
	unsigned char		name[MAX_DEV_NAME_LEN+1];
	short			logic_addr;
	short			virtual_device;
} dev_table_t;
//czn_20160708_e
#pragma pack()

// 加载房号和ip对照表
int load_ip_device_table(void);

// 释放房号和ip对照表
int free_ip_device_table(void);		//czn_20160708

// 从IP对照表中得到记录的所有字段数据
int get_ip_table_record_items( one_vtk_table* ptable, int index,  ip_table_t* pip_table_value );

// 从DEV对照表中得到记录的所有字段数据
int get_device_table_record_items( one_vtk_table* ptable, int index, dev_table_t* pdev_table_value );

int GatewayId_Trs2_IpAddr(unsigned short gw_id);//czn_20170118
unsigned short IpAddr_Trs2_GatewayId(int ip_addr);//czn_20170118
int Judge_DevIsGs(int gw_id);		//czn_20170222
//czn_20170407_s
int get_ip_table_record_items_byfield( one_vtk_table* ptable, int index, char *name,char *input,short *node_id,char *device_type,char *proterty,int *ip_addr,char *extend_mode);
int get_device_table_record_items_byfield( one_vtk_table* ptable, int index,char *name,char *input,short *node_id,short *logic_addr,short *virtual_device);
//czn_20170407_e
#endif


