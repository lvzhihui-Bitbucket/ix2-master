
#include "task_IoServer.h"
//#include "../task_debug_sbu/task_debug_sbu.h"

#include "obj_IPDeviceTable.h"
#include "obj_ResourceTable.h"		//czn_20160827

//czn_20160812

// lzh_20160601_s
one_vtk_table * pdevice_map_table 		= NULL;
one_vtk_table * pip_map_table 			= NULL;

one_vtk_table * pvtk_gl_device_table 	= NULL;
one_vtk_table * pvtk_cds_device_table 	= NULL;
one_vtk_table * pvtk_im_device_table 	= NULL;
one_vtk_table * pvtk_ds_device_table 	= NULL;
one_vtk_table * pvtk_temp_device_table	= NULL;		// uart远程命令生成的临时表指针，仅允许1个

//czn_20170407_s
int gwtb_ipaddr_keyname_index;
int gwtb_nodeid_keyname_index;
int gwtb_devicetype_keyname_index;
int gwtb_extendmode_keyname_index;
int gwtb_name_keyname_index;
int gwtb_input_keyname_index;
int gwtb_property_keyname_index;

int rmtb_nodeid_keyname_index;
int rmtb_logicaddr_keyname_index;
int rmtb_input_keyname_index;
int rmtb_name_keyname_index;
int rmtb_virtualdev_keyname_index;

int GetIpTable_Keyname_Index(void)
{
	if( (pip_map_table != NULL) && (pdevice_map_table != NULL) )
	{
		gwtb_ipaddr_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_IPADDR);
		gwtb_nodeid_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_GWSN);
		gwtb_devicetype_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_DEVTYPE);
		gwtb_extendmode_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_EXMODE);
		gwtb_name_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_NAME);
		gwtb_input_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_PREINPUT);
		gwtb_property_keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_PROPERTY);

		rmtb_nodeid_keyname_index = get_keyname_index(pdevice_map_table, TBRM_KEY_GWSN);
		rmtb_logicaddr_keyname_index = get_keyname_index(pdevice_map_table, TBRM_KEY_ADDR);
		rmtb_input_keyname_index = get_keyname_index(pdevice_map_table, TBRM_KEY_INPUT);
		rmtb_name_keyname_index = get_keyname_index(pdevice_map_table, TBRM_KEY_NAME);
		rmtb_virtualdev_keyname_index = get_keyname_index(pdevice_map_table, TBRM_KEY_VIRTUALDEV);
	}

	return 0;
}
//czn_20170407_e

//czn_20170406_s
int IpTable_Resolve(void)
{
// 分析基础数据表，得到4个设备类型的数据表(中心设备类型、小区主机设备类型、分机设备类型、主机设备类型)
	if( (pip_map_table != NULL) && (pdevice_map_table != NULL) )
	{		
		// 1. 生成pvtk_gl_device_table
		pvtk_gl_device_table	= create_one_vtk_table(pip_map_table->keyname_cnt,pip_map_table->pkeyname,pip_map_table->pkeyname_len);
		one_vtk_dat*	gl_rec;
		int 			gl_cnt = 0;
		//czn_20161008_s
		int			keyname_index,input_str_len;
		unsigned char 	input_str[MAX_DEV_NAME_LEN+1];
		
		keyname_index = get_keyname_index(pip_map_table, TBGW_KEY_IPADDR);
		while(1)
		{
			gl_rec = get_one_vtk_record( pip_map_table, TBGW_KEY_DEVTYPE,"GuardUnit", gl_cnt);
			if( gl_rec == NULL )
			{
				int gl_cnt1 = 0;
				while(1)
				{
					gl_rec = get_one_vtk_record( pip_map_table, TBGW_KEY_DEVTYPE,"PCStation", gl_cnt1);
					if( gl_rec == NULL )
						break;
					else
					{
						input_str_len = MAX_DEV_NAME_LEN;
						get_one_record_string( gl_rec, keyname_index, input_str, &input_str_len );
						input_str[input_str_len] = 0;
						
						if(inet_addr(input_str) != GetLocalIpByDevice(NET_ETH0))
						{
							add_one_vtk_record( pvtk_gl_device_table, gl_rec );
						}
						
						gl_cnt1++;
						gl_cnt++;
					}
				}
				break;
			}
			else
			{
				input_str_len = MAX_DEV_NAME_LEN;
				get_one_record_string( gl_rec, keyname_index, input_str, &input_str_len );
				input_str[input_str_len] = 0;
				
				if(inet_addr(input_str) != GetLocalIpByDevice(NET_ETH0))
				{
					add_one_vtk_record( pvtk_gl_device_table, gl_rec );
				}
				
				gl_cnt++;
			}
		}
		//czn_20161008_e
		//printf_one_table(pvtk_gl_device_table);

		// 2. 生成pvtk_cds_device_table
		pvtk_cds_device_table	= create_one_vtk_table(pip_map_table->keyname_cnt,pip_map_table->pkeyname,pip_map_table->pkeyname_len);
		one_vtk_dat*	cds_rec;
		int 			cds_cnt = 0;
		while( 1 )
		{
			cds_rec = get_one_vtk_record( pip_map_table, TBGW_KEY_DEVTYPE,"EntryUnit_IPG", cds_cnt);
			if( cds_rec == NULL )
			{
				int cds_cnt1 = 0;
				while(1)
				{
					cds_rec = get_one_vtk_record( pip_map_table, TBGW_KEY_DEVTYPE,"EntryUnit", cds_cnt1);
					if( cds_rec == NULL )
						break;
					else
					{
						add_one_vtk_record( pvtk_cds_device_table, cds_rec );	//czn_20161008
						cds_cnt1++;
						cds_cnt++;
					}
				}
				break;
			}
			else
			{
				add_one_vtk_record( pvtk_cds_device_table, cds_rec );
				cds_cnt++;
			}
		}
		//printf_one_table(pvtk_cds_device_table);	
	}

	return 0;
}
//czn_20170406_e

#define RMTB_DEFAULT	"/mnt/nand1-2/DeviceMapTable.csv"
#define GWTB_DEFAULT	"/mnt/nand1-2/IPMapTable.csv"
// 加载房号和ip对照表
int load_ip_device_table(void)		//czn_20160708
{
	//czn_20160827_s
	ResourceTb_OneRecord_Stru rmtb_record,gwtb_record;
	if(API_ResourceTb_GetOneRecord(RID1001_TB_ROOM, &rmtb_record) != 0)
	{
		strcpy(rmtb_record.filename,RMTB_DEFAULT);
		//return -1;
	}
	if(API_ResourceTb_GetOneRecord(RID1002_TB_GATEWAY,&gwtb_record) != 0)
	{
		strcpy(gwtb_record.filename,GWTB_DEFAULT);
		//return -1;
	}
	
	// 加载基础数据表
	pip_map_table		= load_vtk_table_file(gwtb_record.filename);
	pdevice_map_table	= load_vtk_table_file(rmtb_record.filename);
	//czn_20160827_e
	if( pip_map_table != NULL  )
	{
		printf("load table from \"/mnt/nand1-2/ip_map_table.csv\".\n");
		if( 0 )
		{
			//printf_one_table(pip_map_table);
		}
	}
	if( pdevice_map_table != NULL )
	{
		printf("load table from \"/mnt/nand1-2/device_map_table.csv\".\n");
		if( 0 )
		{			
			//printf_one_table(pdevice_map_table);
		}
	}
	//czn_20170407_s
	GetIpTable_Keyname_Index();
	IpTable_Resolve();
	//czn_20170407_e
	return 0;
}


int free_ip_device_table(void)	//czn_20170406
{
	free_vtk_table_file_buffer(pip_map_table);
	pip_map_table = NULL;
	free_vtk_table_file_buffer(pdevice_map_table);
	pdevice_map_table = NULL;
	release_one_vtk_tabl(pvtk_gl_device_table);
	pvtk_gl_device_table = NULL;
	release_one_vtk_tabl(pvtk_cds_device_table);
	pvtk_cds_device_table = NULL;
	release_one_vtk_tabl(pvtk_ds_device_table);
	pvtk_ds_device_table = NULL;
	release_one_vtk_tabl(pvtk_im_device_table);
	pvtk_im_device_table = NULL;

	return 0;
}

// 从IP对照表中得到记录的所有字段数据
int get_ip_table_record_items( one_vtk_table* ptable, int index, ip_table_t* pip_table_value )	//czn_20170408
{
	unsigned char 	input_str[MAX_DEV_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		int keyname_index;	
		// GATEWAY_SN
		keyname_index = gwtb_nodeid_keyname_index;//get_keyname_index( ptable, TBGW_KEY_GWSN );		
		input_str_len = MAX_DEV_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pip_table_value->ipnode_id = atol(input_str);
		// DEVICE_TYPE
		keyname_index = gwtb_devicetype_keyname_index;//get_keyname_index( ptable, TBGW_KEY_DEVTYPE);		
		input_str_len = MAX_DEV_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		///pip_table_value->device_type = atol(input_str);	
		//czn_20160614_s
		//pip_table_value->device_type = atol(input_str);	
		if(strcmp(input_str,"Gateway_IPG") == 0)
		{
			pip_table_value->device_type = IPDevType_Gateway_IPG;
		}
		else if(strcmp(input_str,"GuardUnit") == 0)
		{
			pip_table_value->device_type = IPDevType_GuardUnit;
		}
		else if(strcmp(input_str,"PCStation") == 0)
		{
			pip_table_value->device_type = IPDevType_PCStation;
		}
		else if(strcmp(input_str,"EntryUnit_IPG") == 0)
		{
			pip_table_value->device_type = IPDevType_EntryUnit_IPG;
		}
		else if(strcmp(input_str,"EntryUnit") == 0)
		{
			pip_table_value->device_type = IPDevType_EntryUnit;
		}
		else
		{
			pip_table_value->device_type = IPDevType_Unkown;
		}
		//czn_20160614_e		
		// PROPERTY
		keyname_index = gwtb_property_keyname_index;//get_keyname_index( ptable, TBGW_KEY_PROPERTY);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pip_table_value->proterty = atol(input_str);	
		// IP_ADDR
		keyname_index = gwtb_ipaddr_keyname_index;//get_keyname_index( ptable, TBGW_KEY_IPADDR);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pip_table_value->ip_addr = inet_addr(input_str);
		// EXTEND_MODE
		keyname_index = gwtb_extendmode_keyname_index;//get_keyname_index( ptable, TBGW_KEY_EXMODE);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		//pip_table_value->sys_config = atol(input_str);
		if(strcmp(input_str,"NOEXTEND") == 0)
		{
			pip_table_value->extend_mode = IPG_Extend_NoExtend;
		}
		else if(strcmp(input_str,"DT_32") == 0)
		{
			pip_table_value->extend_mode = IPG_Extend_Dt32;
		}
		else if(strcmp(input_str,"DT_128") == 0)
		{
			pip_table_value->extend_mode = IPG_Extend_Dt128;
		}
		else if(strcmp(input_str,"AT_SYS") == 0)
		{
			pip_table_value->extend_mode = IPG_Extend_AtSys;
		}
		else
		{
			pip_table_value->extend_mode = IPG_Extend_Unkown;
		}
		// NAME 			
		keyname_index = gwtb_name_keyname_index;//get_keyname_index( ptable, TBGW_KEY_NAME);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pip_table_value->name,input_str,input_str_len+1);		
		// INPUT
		keyname_index = gwtb_input_keyname_index;//get_keyname_index( ptable, TBGW_KEY_PREINPUT);
		input_str_len = MAX_IP_STR_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pip_table_value->ip_input,input_str,input_str_len+1);
		return 0;
	}
	else
		return -1;
}

// 从DEV对照表中得到记录的所有字段数据
int get_device_table_record_items( one_vtk_table* ptable, int index, dev_table_t* pdev_table_value )	//czn_20170408
{
	unsigned char 	input_str[MAX_DEV_NAME_LEN+1];
	int 			input_str_len;	
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		int keyname_index;
		// GATEWAY_SN
		keyname_index = rmtb_nodeid_keyname_index;//get_keyname_index( ptable, TBRM_KEY_GWSN);
		input_str_len = MAX_DEV_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pdev_table_value->ipnode_id = atol(input_str);
		// INPUT
		keyname_index = rmtb_input_keyname_index;//get_keyname_index( ptable, TBRM_KEY_INPUT);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pdev_table_value->ip_input,input_str,input_str_len+1);	
		//dprintf("dev-input_str=%s\n",pdev_table_value->ip_input);
		// NAME 			
		keyname_index = rmtb_name_keyname_index;//get_keyname_index( ptable, TBRM_KEY_NAME);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pdev_table_value->name,input_str,input_str_len+1);						
		// LOGIC_ADDR
		keyname_index = rmtb_logicaddr_keyname_index;//get_keyname_index( ptable, TBRM_KEY_ADDR);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pdev_table_value->logic_addr = atol(input_str);
		// VIRTUAL_DEVICE
		keyname_index = rmtb_virtualdev_keyname_index;//get_keyname_index( ptable, TBRM_KEY_VIRTUALDEV);
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pdev_table_value->virtual_device = atol(input_str);
		dprintf("gwid=%d,addr=%d,input_str=%s\n",pdev_table_value->ipnode_id,pdev_table_value->logic_addr,pdev_table_value->ip_input);
		return 0;
	}
	else
		return -1;
	
}
//czn_20170407_s
// 从IP对照表中得到记录的所需字段数据
int get_ip_table_record_items_byfield( one_vtk_table* ptable, int index, char *name,char *input,short *node_id,char *device_type,char *proterty,int *ip_addr,char *extend_mode)
{
	unsigned char 	input_str[MAX_DEV_NAME_LEN+1];
	int 			input_str_len;
	one_vtk_dat*	precord;
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		int keyname_index;	
		// GATEWAY_SN
		if(node_id != NULL)
		{
			keyname_index = gwtb_nodeid_keyname_index;//get_keyname_index( ptable, TBGW_KEY_GWSN );		
			input_str_len = MAX_DEV_NAME_LEN;		
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*node_id = atol(input_str);
		}
		// DEVICE_TYPE
		if(device_type != NULL)
		{
			keyname_index = gwtb_devicetype_keyname_index;//get_keyname_index( ptable, TBGW_KEY_DEVTYPE);		
			input_str_len = MAX_DEV_NAME_LEN;		
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			///pip_table_value->device_type = atol(input_str);	
			//czn_20160614_s
			//pip_table_value->device_type = atol(input_str);	
			if(strcmp(input_str,"Gateway_IPG") == 0)
			{
				*device_type = IPDevType_Gateway_IPG;
			}
			else if(strcmp(input_str,"GuardUnit") == 0)
			{
				*device_type = IPDevType_GuardUnit;
			}
			else if(strcmp(input_str,"PCStation") == 0)
			{
				*device_type = IPDevType_PCStation;
			}
			else if(strcmp(input_str,"EntryUnit_IPG") == 0)
			{
				*device_type = IPDevType_EntryUnit_IPG;
			}
			else if(strcmp(input_str,"EntryUnit") == 0)
			{
				*device_type = IPDevType_EntryUnit;
			}
			else
			{
				*device_type = IPDevType_Unkown;
			}
		}
		//czn_20160614_e		
		// PROPERTY
		if(proterty != NULL)
		{
			keyname_index = gwtb_property_keyname_index;//get_keyname_index( ptable, TBGW_KEY_PROPERTY);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*proterty = atol(input_str);	
		}
		if(ip_addr != NULL)
		{
			// IP_ADDR
			keyname_index = gwtb_ipaddr_keyname_index;//get_keyname_index( ptable, TBGW_KEY_IPADDR);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*ip_addr = inet_addr(input_str);
		}
		
			// EXTEND_MODE
		if(extend_mode != NULL)
		{	
			keyname_index = gwtb_extendmode_keyname_index;//get_keyname_index( ptable, TBGW_KEY_EXMODE);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			//pip_table_value->sys_config = atol(input_str);
			if(strcmp(input_str,"NOEXTEND") == 0)
			{
				*extend_mode = IPG_Extend_NoExtend;
			}
			else if(strcmp(input_str,"DT_32") == 0)
			{
				*extend_mode = IPG_Extend_Dt32;
			}
			else if(strcmp(input_str,"DT_128") == 0)
			{
				*extend_mode = IPG_Extend_Dt128;
			}
			else if(strcmp(input_str,"AT_SYS") == 0)
			{
				*extend_mode = IPG_Extend_AtSys;
			}
			else
			{
				*extend_mode = IPG_Extend_Unkown;
			}
		}
		// NAME 		
		if(name != NULL)
		{
			keyname_index = gwtb_name_keyname_index;//get_keyname_index( ptable, TBGW_KEY_NAME);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			memcpy(name,input_str,input_str_len+1);
		}
		
		// INPUT
		if(input != NULL)
		{
			keyname_index = gwtb_input_keyname_index;//get_keyname_index( ptable, TBGW_KEY_PREINPUT);
			input_str_len = MAX_IP_STR_LEN;		
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			memcpy( input,input_str,input_str_len+1);
		}
		return 0;
	}
	else
		return -1;
}

// 从DEV对照表中得到记录的所需字段数据
int get_device_table_record_items_byfield( one_vtk_table* ptable, int index,char *name,char *input,short *node_id,short *logic_addr,short *virtual_device)
{
	unsigned char 	input_str[MAX_DEV_NAME_LEN+1];
	int 			input_str_len;	
	one_vtk_dat*	precord;
	dev_table_t* pdev_table_value;
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		int keyname_index;
		// GATEWAY_SN
		if(node_id != NULL)
		{
			keyname_index = rmtb_nodeid_keyname_index;//get_keyname_index( ptable, TBRM_KEY_GWSN);
			input_str_len = MAX_DEV_NAME_LEN;		
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*node_id = atol(input_str);
		}
		// INPUT
		if(input != NULL)
		{
			keyname_index = rmtb_input_keyname_index;//get_keyname_index( ptable, TBRM_KEY_INPUT);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			memcpy(input,input_str,input_str_len+1);	
		}
		//dprintf("dev-input_str=%s\n",pdev_table_value->ip_input);
		// NAME 
		if(name != NULL)
		{
			keyname_index = rmtb_name_keyname_index;//get_keyname_index( ptable, TBRM_KEY_NAME);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			memcpy( name,input_str,input_str_len+1);
		}
		// LOGIC_ADDR
		if(logic_addr != NULL)
		{
			keyname_index = rmtb_logicaddr_keyname_index;//get_keyname_index( ptable, TBRM_KEY_ADDR);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*logic_addr = atol(input_str);
		}
		// VIRTUAL_DEVICE
		if(virtual_device != NULL)
		{
			keyname_index = rmtb_virtualdev_keyname_index;//get_keyname_index( ptable, TBRM_KEY_VIRTUALDEV);
			input_str_len = MAX_DEV_NAME_LEN;
			get_one_record_string( precord, keyname_index, input_str, &input_str_len );
			input_str[input_str_len] = 0;
			*virtual_device = atol(input_str);
		}
		//dprintf("gwid=%d,addr=%d,input_str=%s\n",pdev_table_value->ipnode_id,pdev_table_value->logic_addr,pdev_table_value->ip_input);
		return 0;
	}
	else
		return -1;
	
}
//czn_20170407_e
//czn_20161219
int GatewayId_Trs2_IpAddr(unsigned short gw_id)	//czn_20170118
{
	int ipaddr;
	int i;
	ip_table_t  ip_table_value;
	
	if(pip_map_table != NULL)
	{
		for(i = 0;i < pip_map_table->record_cnt;i++)
		{
			if(get_ip_table_record_items(pip_map_table,i,&ip_table_value) != 0)
				continue;
			if(ip_table_value.ipnode_id == gw_id)
				return ip_table_value.ip_addr;
		}
		
	}
	
	ipaddr = GetLocalIpByDevice(NET_ETH0);
	return (ipaddr&0x00ffffff)|(gw_id<<24);
}
//czn_20170118_s

//cao_20170410
int GetIpFromTableByNodeId(unsigned short id)
{
	int i;
	ip_table_t  ip_table_value;
	
	if(pip_map_table != NULL)
	{
		for(i = 0;i < pip_map_table->record_cnt;i++)
		{
			if(get_ip_table_record_items(pip_map_table,i,&ip_table_value) != 0)
				continue;
			if(ip_table_value.ipnode_id == id)
				return ip_table_value.ip_addr;
		}
		
	}

	return 0;
}
//cao_20170410

unsigned short IpAddr_Trs2_GatewayId(int ip_addr)
{
	int i;
	ip_table_t  ip_table_value;
	
	if(pip_map_table != NULL)
	{
		for(i = 0;i < pip_map_table->record_cnt;i++)
		{
			if(get_ip_table_record_items(pip_map_table,i,&ip_table_value) != 0)
				continue;
			if(ip_table_value.ip_addr == ip_addr)
				return ip_table_value.ipnode_id;
		}
		
	}
	
	unsigned short default_id = (ip_addr&0xff000000)>>24;
	
	return default_id;
}
//czn_20170118_e

//czn_20170222_s
int Judge_DevIsGs(int gw_id)
{
	int i;
	ip_table_t  ip_table_value;
	
	if(pip_map_table != NULL)
	{
		for(i = 0;i < pip_map_table->record_cnt;i++)
		{
			if(get_ip_table_record_items(pip_map_table,i,&ip_table_value) != 0)
				continue;
			if(ip_table_value.ipnode_id == gw_id)
			{
				if(ip_table_value.device_type == IPDevType_GuardUnit)
				{
					return 1;
				}
				return 0;
			}
		}
		
	}
	
	return 0;
}
//czn_20170222_e


