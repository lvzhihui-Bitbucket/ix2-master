/**
  ******************************************************************************
  * @file    obj_IoServer_State.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_IoServer_State_H
#define _obj_IoServer_State_H

#include "task_IoServer.h"
#include "obj_TableProcess.h"
#include "define_file.h"
#include "cJSON.h"

// Define Object Property-------------------------------------------------------
/******************************************************************************
ioValue��ʽ

{
        "value":        {
                "1058": "ggggg",
                "1057": "1",
                "1001": "0099888801"
        }
}

*******************************************************************************/

typedef struct
{
	OS_TIMER		saveTimer;
	uint8 			mustSaveFlag;
	one_vtk_table*	pTable;
	cJSON*			ioSetting;
	cJSON*			ioValue;
	cJSON*			paraMenuTable;
} IO_STATE_S;



// Define Object Function - Public----------------------------------------------
void IoServerStateInit(void);
uint8 InnerRead(IoRemote_S *pIoMsg);
uint8 InnerWrite(IoRemote_S *pIoMsg);
uint8 InnerReadWriteVar(IoRemote_S *pIoMsg, uint8 readWriteFlag);

void ToInviteFactoryDefault(IoRemote_S* pData);
void BeInviteFactoryDefault(IoRemote_S* pData);

int ToRead_ToWrite_DT_Process(IoRemote_S *pData);

void BeReadByRemote(IoRemote_S *pData);
void BeWriteByRemote(IoRemote_S *pData);
uint8 IoResponseToRemote(IoRemote_S* pData );

uint8 ToReadToWriteRemote(IoRemote_S* pData);
uint8 ReadWriteRemoteResponse(IoRemote_S* pdata);

// Define Object Function - Private---------------------------------------------
void SaveIoDataTimerCallback(void);

void load_io_data_table(void);
uint8 SaveIoDataTable(void);
uint8 ReadWritelocalResponse( IoRemote_S* pIoMsg);

// Define Object Function - Other-----------------------------------------------


#endif
