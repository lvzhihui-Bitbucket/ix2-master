/*
  Copyright (c) 2009-2017 Dave Gamble and cJSON contributors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../cJSON/cJSON.h"
#include "obj_OtherParameterSetting.h"



char* GetJsonStringFromFile(const char* filePath)
{
	FILE* file = NULL;
	char* json = NULL;
	
	if((file=fopen(filePath,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size+1)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		json[size] = 0;
		
		fclose(file);
	}

	return json;
}

void SetJsonStringToFile(const char* filePath, const char* string)
{
	FILE	*file = NULL;
	
	if((file=fopen(filePath,"w+")) != NULL)
	{
		fputs(string, file);
		
		fclose(file);
		sync();
	}
}


/*
{
    "S4":[ 1, 2, 3 ],
    "MK":[ 15 ],
	"DR":[ 16 ]
};
*/


char* CreateExtModuleObject(EXT_MODULE_JSON obj)
{
    cJSON *root = NULL;
    cJSON *ixDevice = NULL;
    cJSON *ipcDevic = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

    cJSON_AddItemToObject(root, "S4", cJSON_CreateIntArray(obj.s4, obj.s4Nbr));
    cJSON_AddItemToObject(root, "MK", cJSON_CreateIntArray(obj.mk, obj.mkNbr));
    cJSON_AddItemToObject(root, "DR", cJSON_CreateIntArray(obj.dr, obj.drNbr));

	
	string = cJSON_Print(root);
	if(string != NULL)
	{	
		printf("ExtModuleObject_JSON=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}

int ParseExtModuleObject(const char* json, EXT_MODULE_JSON* pObj)
{
    int status = 0;
	int iCnt;
	cJSON * pSub;

    const cJSON *s4 = NULL;
    const cJSON *mk = NULL;
   	const cJSON *dr = NULL;

	memset(pObj, 0, sizeof(EXT_MODULE_JSON));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *ExtModuleObject = cJSON_Parse(json);
    if (ExtModuleObject == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
	/* 获取名为“S4”的值 */
	s4 = cJSON_GetObjectItemCaseSensitive( ExtModuleObject, "S4");
	if( s4 != NULL )
	{
		pObj->s4Nbr = cJSON_GetArraySize ( s4 );
	 
		for( iCnt = 0 ; iCnt < pObj->s4Nbr ; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(s4, iCnt);
			
			if(NULL == pSub ){ continue ; }
	 
			pObj->s4[iCnt] = pSub->valueint ;
		}
	}

	/* 获取名为“MK”的值 */
	mk = cJSON_GetObjectItemCaseSensitive( ExtModuleObject, "MK");
	if( mk != NULL )
	{
		pObj->mkNbr = cJSON_GetArraySize ( mk );
	 
		for( iCnt = 0 ; iCnt < pObj->mkNbr ; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(mk, iCnt);
			
			if(NULL == pSub ){ continue ; }
	 
			pObj->mk[iCnt] = pSub->valueint ;
		}
	}

	/* 获取名为“DR”的值 */
	dr = cJSON_GetObjectItemCaseSensitive( ExtModuleObject, "DR");
	if( dr != NULL )
	{
		pObj->drNbr = cJSON_GetArraySize ( dr );
	 
		for( iCnt = 0 ; iCnt < pObj->drNbr ; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(dr, iCnt);
			
			if(NULL == pSub ){ continue ; }
	 
			pObj->dr[iCnt] = pSub->valueint ;
		}
	}
	
/*
	printf("pObj->drNbr=%d\n", pObj->s4Nbr);
	for( iCnt = 0 ; iCnt < pObj->s4Nbr; iCnt ++ )
	{
		printf("pObj->s4[%d]=%d\n", iCnt, pObj->s4[iCnt]);
	}
	
	printf("pObj->mkNbr=%d\n", pObj->mkNbr);
	for( iCnt = 0 ; iCnt < pObj->mkNbr ; iCnt ++ )
	{
		printf("pObj->mk[%d]=%d\n", iCnt, pObj->mk[iCnt]);
	}

	printf("pObj->drNbr=%d\n", pObj->drNbr);
	for( iCnt = 0 ; iCnt < pObj->drNbr ; iCnt ++ )
	{
		printf("pObj->dr[%d]=%d\n", iCnt, pObj->dr[iCnt]);
	}
*/
	
    status = 1;

end:
	
    cJSON_Delete(ExtModuleObject);
	
    return status;
}



/*
{
    "Device type":		"IX471",
    "Code info":		"IX-471 STD F/W",
	"Code size":		20405924,
	"App Code":			"947100",
	"App Ver":			"V1.0.0.1911251030"
}
*/


char* CreateFwVerVerifyObject(FW_VER_VERIFY_T obj)
{
    cJSON *root = NULL;
	char* string;
	
    root = cJSON_CreateObject();
	
    cJSON_AddStringToObject(root, "Device type", obj.deviceType);
    cJSON_AddStringToObject(root, "Code info", obj.codeInfo);
    cJSON_AddNumberToObject(root, "Code size", obj.codeSize);
    cJSON_AddStringToObject(root, "App Code", obj.appCode);
    cJSON_AddStringToObject(root, "App Ver", obj.appVer);

	
	string = cJSON_Print(root);
	if(string != NULL)
	{	
		printf("FwVerVerify=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}

int ParseFwVerVerifyObject(const char* json, FW_VER_VERIFY_T* pData)
{
    int status = 0;

	memset(pData, 0, sizeof(FW_VER_VERIFY_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
    ParseJsonString(root, "Device type", pData->deviceType, OTHER_PARA_DATA_LEN);
    ParseJsonString(root, "Code info", pData->codeInfo, OTHER_PARA_DATA_LEN);
    ParseJsonString(root, "App Code", pData->appCode, OTHER_PARA_DATA_LEN);
    ParseJsonString(root, "App Ver", pData->appVer, OTHER_PARA_DATA_LEN);
    ParseJsonNumber(root, "Code size", &(pData->codeSize));
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void IPAssignedEffectiveImmediately(int select)
{
	if(select >= 0 && select <= 1)
	{
		if(select)//DHCP
		{
			API_DHCP_SaveEnable(1);
		}
		else		 //静态
		{
			API_DHCP_SaveEnable(0);
			SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
			ResetNetWork();
			Dhcp_Autoip_CheckIP();		//czn_20190604
		}
	}
}

