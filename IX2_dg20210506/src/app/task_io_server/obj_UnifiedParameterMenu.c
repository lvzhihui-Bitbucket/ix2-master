/**
  ******************************************************************************
  * @file    obj_UnifiedParameterMenu.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<errno.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_UnifiedParameterMenu.h"	


int GetParaMenuRecord(const char* paraId, ComboBoxSelect_T* record)
{
	cJSON*	comboBoxs;
	cJSON*	oneComboBox;
	cJSON*	tempJson;
	int i;
	char tempName[400];
	int nameLen;

	record->num = 0;
	comboBoxs = cJSON_GetObjectItemCaseSensitive(GetIoParaMenuTable(), COMBO_BOX);
	
	if(comboBoxs != NULL)
	{
		oneComboBox = cJSON_GetObjectItemCaseSensitive(comboBoxs, paraId);
		if(oneComboBox != NULL)
		{
			record->num = (cJSON_GetArraySize(oneComboBox) < COMBO_BOX_MAX ? cJSON_GetArraySize(oneComboBox) : COMBO_BOX_MAX);
			
			for(i = 0; i< record->num; i++)
			{
				tempJson = cJSON_GetArrayItem(cJSON_GetArrayItem(oneComboBox, i), 0);
				strcpy(record->data[i].value, cJSON_GetStringValue(tempJson));
				
				tempJson = cJSON_GetArrayItem(cJSON_GetArrayItem(oneComboBox, i), 1);
				strcpy(record->data[i].comboBox, cJSON_GetStringValue(tempJson));
			}
		}
	}
	
	API_GetOSD_StringWithString(paraId, tempName, &nameLen);
	memcpy(record->name, tempName, nameLen);
	record->nameLen = nameLen;

	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
