/**
  ******************************************************************************
  * @file    obj_IoServer_State.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "task_IoServer.h"	
#include "vdp_IoServer_State.h"
#include "vtk_udp_stack_io_server.h"
#include "MenuSipConfig_Business.h"	//20210428
#include "obj_VideoProxySetting.h"
#include "define_file.h"
#include "cJSON.h"

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<errno.h>

#define IO_SETTING_JSON_KEY_VALUE			"value"

#define BUFF_LEN							500

#define PARA_ID								"paraId"		//�������
#define NAME								"name"			//��������
#define EFFECTIVE							"effective"		//��������
#define COMMON								"common"		//Զ�̷��ʲ�������
#define LEVEL								"level"			//�����ָ��ȼ�
#define GROUP								"group"			//���ط��ʲ�������
#define SOURCE								"source"		//������Դ
#define PROPERTY							"property"		//������д���� 1ֻ����2ֻд��3�ɶ�д
#define VALUE_TYPE							"valueType"		//����ֵ���ͣ�0��ֵ��1�ַ���
#define DUFAULT								"default"		//����Ĭ��ֵ
#define	EDIT_TYPE							"editType"		//�༭���� 0�����ͣ�1ѡ���
#define MIN_VALUE							"min"			//������Сֵ
#define MAX_VALUE							"max"			//�������ֵ
#define VALUE								"value"			//������ǰֵ


IO_STATE_S	ioProperty={0};

//IO SERVER �������ṹ����
const unsigned char* IO_DATA_KEYNAME_TAB[] = 
{
	PARA_ID,
	NAME,
	EFFECTIVE,
	COMMON,
	LEVEL,
	GROUP,
	SOURCE,
	PROPERTY,
	VALUE_TYPE,
	DUFAULT,
	EDIT_TYPE,
	MIN_VALUE,
	MAX_VALUE,
	VALUE,
};

const int IO_DATA_KEYNAME_CNT = sizeof(IO_DATA_KEYNAME_TAB)/sizeof(unsigned char*);

int GetIoDataKeynameIndex(const char* keyName)
{
	int i;
	for(i = 0; i < IO_DATA_KEYNAME_CNT; i++)
	{
		if(!strcmp(keyName, IO_DATA_KEYNAME_TAB[i]))
		{
			break;
		}
	}

	return i;
}

//IO SERVER ״̬��ʼ��
void IoServerStateInit(void)
{
	ioProperty.mustSaveFlag 	= 0;
	ioProperty.pTable			= NULL;
	ioProperty.ioSetting		= NULL;
	ioProperty.ioValue			= NULL;
	ioProperty.paraMenuTable	= NULL;
	
	flag_switch_io 			= IO_SWITCH_ENABLE;
	OS_CreateTimer(&ioProperty.saveTimer, SaveIoDataTimerCallback, 5000/25);
}

//��ʱ���ڲ���ֵ���ļ���
void SaveIoDataTimerCallback(void)
{
	if(ioProperty.mustSaveFlag)
	{
		API_io_server_save_data_file();
		OS_RetriggerTimer(&ioProperty.saveTimer);
	}
	else
	{
		OS_StopTimer(&ioProperty.saveTimer);
	}
}

//���ز���ֵ���ڴ���
void load_io_data_table(void)
{
	char* jsonString;
			
	//1������IO���������Ĭ��ֵ��
	free_vtk_table_file_buffer(ioProperty.pTable);		
	ioProperty.pTable = load_vtk_table_file(IO_DATA_DEFAULT_FILE_NAME);
	if(ioProperty.pTable == NULL)
	{
		dprintf("Have no %s!!!\n", IO_DATA_DEFAULT_FILE_NAME);
		return;
	}



	//2������IO��������Ĭ��ֵ��
	WriteIoTableDefaultValue();
}

//���ز���ֵ���ڴ���
void load_io_data_setting_table(void)
{
	char* jsonString;

	//3������IO��������ֵ��
	if(ioProperty.ioSetting != NULL)
	{
		cJSON_Delete(ioProperty.ioSetting);
		ioProperty.ioSetting = NULL;
	}

	jsonString = GetJsonStringFromFile(IO_DATA_VALUE_NAME);
	if(jsonString != NULL)
	{
		ioProperty.ioSetting = cJSON_Parse(jsonString);
		free(jsonString);
		jsonString = NULL;
	}

	if(ioProperty.ioSetting == NULL)
	{
		ioProperty.ioSetting = cJSON_CreateObject();
		dprintf("Have no %s!!!\n", IO_DATA_VALUE_NAME);
	}
	
	ioProperty.ioValue = cJSON_GetObjectItemCaseSensitive(ioProperty.ioSetting, IO_SETTING_JSON_KEY_VALUE);
	if(ioProperty.ioValue == NULL)
	{
		ioProperty.ioValue = cJSON_CreateObject();
		cJSON_AddItemToObject(ioProperty.ioSetting, IO_SETTING_JSON_KEY_VALUE, ioProperty.ioValue);
		dprintf("Have no %s!!!\n", IO_SETTING_JSON_KEY_VALUE);
	}
}

void load_io_data_menu_table(void)
{
	char* jsonString;
	
	//4������IO������ʾ�˵���
	if(ioProperty.paraMenuTable != NULL)
	{
		cJSON_Delete(ioProperty.paraMenuTable);
		ioProperty.paraMenuTable = NULL;
	}
	
	jsonString = GetJsonStringFromFile(PARA_MENU_FILE_NAME);
	if(jsonString != NULL)
	{
		ioProperty.paraMenuTable = cJSON_Parse(jsonString);
		free(jsonString);
		jsonString = NULL;
	}

	if(ioProperty.paraMenuTable == NULL)
	{
		ioProperty.paraMenuTable = cJSON_CreateObject();
		dprintf("Have no %s!!!\n", PARA_MENU_FILE_NAME);
	}
}


cJSON* GetIoParaMenuTable(void)
{
	return ioProperty.paraMenuTable;
}


int GetIoDataKeyValue(one_vtk_dat* oneRecord, const char* keyName, char* value)
{
	int valueLen;
	int modifyJsonFlag = 0;
	int i;
	
	if(!strcmp(keyName, DUFAULT))
	{
		valueLen = BUFF_LEN;
		get_one_record_string(oneRecord, GetIoDataKeynameIndex(EDIT_TYPE), value, &valueLen);
		value[valueLen] = 0;
		if(atoi(value) == 3 || atoi(value) == 4)
		{
			modifyJsonFlag = 1;
		}
	}

	valueLen = BUFF_LEN;
	get_one_record_string(oneRecord, GetIoDataKeynameIndex(keyName), value, &valueLen);
	value[valueLen] = 0;

	if(modifyJsonFlag)
	{	
		//�ļ��е�json��ʽ��ʹ��';'���','����Ҫת������
		for(i = 0; i < valueLen; i++)
		{
			if(value[i] == ';')
			{
				value[i] = ',';
			}
		}
	}
}

int WriteOneIoDataDefaultValue(one_vtk_dat* oneRecord, char* paraId, char* name, char* effective, char* common, char* level, char* group, char* source, char* property, char* valueType, char* defaultValue, char* editType, char* min, char* max, char* value)
{
	char tempBuff[BUFF_LEN] = {0};

	snprintf(tempBuff, BUFF_LEN, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", paraId, name, effective, common, level, group, source, property, valueType, defaultValue, editType, min, max, value);
	
	oneRecord->len = strlen(tempBuff);
	oneRecord->pdat = realloc(oneRecord->pdat, oneRecord->len);
	memcpy(oneRecord->pdat, tempBuff, oneRecord->len);
}

int WriteOneIoDataValue(char* paraId, char* defaultValue, char* value)
{
	cJSON * json;
	
	while(cJSON_GetObjectItem(ioProperty.ioValue, paraId))
	{
		cJSON_DeleteItemFromObject(ioProperty.ioValue, paraId);
	}

	//����ֵ������Ĭ��ֵ���滻������������ֵ
	if(strcmp(defaultValue, value))
	{
		cJSON_AddStringToObject(ioProperty.ioValue, paraId, value);
	}

	if(strcmp(paraId, DHCP_ENABLE)==0)
	{
		//IPAssignedEffectiveImmediately(atoi(value));
	}
	else if(strcmp(paraId, TimeZone)==0)
	{
		set_timezone(atoi(value) - 12);
	}
	else if(!strcmp(paraId, IxDeviceRemoteSwitch))
	{
		IxBuilderTcpClientSwitchLoad();
	}
	else if(!strcmp(paraId, LAN_Search_Timeout) || !strcmp(paraId, WLAN_Search_Timeout))
	{
		LoadSearchTimeoutParameter();
	}
	else if(!strcmp(paraId, AUTO_REBOOT))
	{
		auto_reboot_everyday_enable(atoi(value));
	}
}

static int ReadExternParameter(char* paraId, char* data)
{
	int ret;
	extern SipCfg_T sipCfg;
	char* tempData;
	
	switch(atoi(paraId))
	{
		case IP_Address_INT:
			strcpy(data, GetSysVerInfo_IP());
			ret = 0;
			break;
			
		case FWVersion_INT:
			strcpy(data, GetSysVerInfo_swVer());
			ret = 0;
			break;
	
		case HWVersion_INT:
			strcpy(data, "26710");
			ret = 0;
			break;
			
		case UpTime_INT:
			sprintf(data, "%s", TimeToString(GetSystemBootTime()));
			ret = 0;
			break;
			
		case SerialNumber_INT:
			strcpy(data, GetSysVerInfo_Sn());
			ret = 0;
			break;
		case MacAddress_INT:
			strcpy(data, GetSysVerInfo_MAC());
			ret = 0;
			break;
		case SubnetMask_INT:
			strcpy(data, GetSysVerInfo_mask());
			ret = 0;
			break;
		case DefaultRoute_INT:
			strcpy(data, GetSysVerInfo_gateway());
			ret = 0;
			break;
			
		case SIP_SERVER_INT:
			strcpy(data, sipCfg.server);
			ret = 0;
			break;
			
		case SIP_PORT_INT:
			strcpy(data, sipCfg.port);
			ret = 0;
			break;
			
		case SIP_DIVERT_INT:
			strcpy(data, sipCfg.divert);
			ret = 0;
			break;
				
		case SIP_ACCOUNT_INT:
			strcpy(data, sipCfg.account);
			ret = 0;
			break;
				
		case SIP_PASSWORD_INT:
			strcpy(data, sipCfg.password);
			ret = 0;
			break;
	
		case SIP_MON_CODE_INT:
			strcpy(data, sipCfg.monCode);
			ret = 0;
			break;
			
		case SIP_CALL_CODE_INT:
			strcpy(data, sipCfg.callCode);
			ret = 0;
			break;
			
		case SIP_DIV_PWD_INT:
			strcpy(data, sipCfg.divPwd);
			ret = 0;
			break;
		
		case VIDEO_PROXY_SET_INT:
			tempData = GetJsonStringFromFile(VIDEO_PROXY_FILE_NAME);
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
			break;
		case EXT_MODULE_CONFIG_INT:
			tempData = GetJsonStringFromFile(EXT_MODULE_FILE_NAME);
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
			break;
		case Divert_Ability_INT:
			tempData = GetDivertAbilityPara();
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
			break;
		case WIFI_SSID_PARA_INT:
			tempData = GetJsonStringFromFile(WIFI_SSID_FILE_NAME);
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
			break;
		#if defined(PID_IX47)||defined(PID_IX482)
		case CallTransferRunningPara_INT:
#if !defined PID_DX470_V25
			tempData = GetCallTransferRunningPara();
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
#endif			
			break;

		case CallTransferPermitPara_INT:
#if !defined PID_DX470_V25
			tempData = GetCallTransferPermitPara();
			if(tempData == NULL)
			{
				ret = -1;
			}
			else
			{
				strcpy(data, tempData);
				free(tempData);
				ret = 0;
			}
#endif			
			break;
		#endif
		default:
			ret = 1;
			break;
	}
	
	return ret;

}

static int WriteExternParameter(char* paraId, char* data)
{
	int ret;
	extern SipCfg_T sipCfg;
	switch(atoi(paraId))
	{
		case IP_Address_INT:
			ret = !SetIpAddress(data);
			break;
			
		case SubnetMask_INT:
			ret = !SetSubNetMask(data);
			break;
			
		case DefaultRoute_INT:
			ret = !SetGateway(data);
			break;

		case SerialNumber_INT:
			ret = 1;
			break;
			
		case SIP_SERVER_INT:
			strcpy(sipCfg.server, data);
			SaveSipServerToServerIp();
			SaveSipConfigFile();
			ret = 0;
			break;
			
		case SIP_PORT_INT:
			strcpy(sipCfg.port, data);
			SaveSipConfigFile();
			ret = 0;
			break;
			
		case SIP_DIVERT_INT:
			strcpy(sipCfg.divert, data);
			SaveSipConfigFile();
			ret = 0;
			break;
				
		case SIP_ACCOUNT_INT:
			strcpy(sipCfg.account, data);
			SaveSipConfigFile();
			ret = 0;
			break;
				
		case SIP_PASSWORD_INT:
			strcpy(sipCfg.password, data);
			SaveSipConfigFile();
			ret = 0;
			break;
	
		case SIP_MON_CODE_INT:
			strcpy(sipCfg.monCode, data);
			SaveSipConfigFile();
			ret = 0;
			break;
			
		case SIP_CALL_CODE_INT:
			strcpy(sipCfg.callCode, data);
			SaveSipConfigFile();
			ret = 0;
			break;
			
		case SIP_DIV_PWD_INT:
			strcpy(sipCfg.divPwd_change, data);
			change_password_of_divert_account2();
			SaveSipConfigFile();
			ret = 0;
			break;
		case VIDEO_PROXY_SET_INT:
			SetJsonStringToFile(VIDEO_PROXY_FILE_NAME, data);
			break;
		case EXT_MODULE_CONFIG_INT:
			SetJsonStringToFile(EXT_MODULE_FILE_NAME, data);
			break;
		case WIFI_SSID_PARA_INT:
			SetJsonStringToFile(WIFI_SSID_FILE_NAME, data);
			break;
		default:
			ret = 1;
			break;
	}
	
	return ret;

}

int WriteExternParameterDefualt(char* paraId, char* data)
{
	int ret;
	extern SipCfg_T sipCfg;
	SipCfg_T sipCfg_temp;
	switch(atoi(paraId))
	{
	
		case IP_Address_INT:
			ret = !SetIpAddress(data);
			break;
			
		case SubnetMask_INT:
			ret = !SetSubNetMask(data);
			break;
			
		case DefaultRoute_INT:
			ret = !SetGateway(data);
			break;
			
		case SerialNumber_INT:
			ret = 1;
			break;
			
		case SIP_SERVER_INT:
			sipCfg_temp = sipCfg;
			strcpy(sipCfg_temp.server, data);
			//SaveSipServerToServerIp();
			SaveSipDefaultConfigFile(sipCfg_temp);
			ret = 0;
			break;
			
		case SIP_PORT_INT:
			sipCfg_temp = sipCfg;
			strcpy(sipCfg_temp.port, data);
			SaveSipDefaultConfigFile(sipCfg_temp);
			ret = 0;
			break;
		#if 0	
		case SIP_DIVERT_INT:
			strcpy(sipCfg.divert, data);
			SaveSipConfigFile();
			ret = 0;
			break;
				
		case SIP_ACCOUNT_INT:
			strcpy(sipCfg.account, data);
			SaveSipConfigFile();
			ret = 0;
			break;
		
		case SIP_PASSWORD_INT:
			strcpy(sipCfg.password, data);
			SaveSipConfigFile();
			ret = 0;
			break;
		#endif
		case SIP_MON_CODE_INT:
			sipCfg_temp = sipCfg;
			strcpy(sipCfg_temp.monCode, data);
			SaveSipDefaultConfigFile(sipCfg_temp);
			ret = 0;
			break;
			
		case SIP_CALL_CODE_INT:
			sipCfg_temp = sipCfg;
			strcpy(sipCfg_temp.callCode, data);
			SaveSipDefaultConfigFile(sipCfg_temp);
			ret = 0;
			break;
		#if 0	
		case SIP_DIV_PWD_INT:
			strcpy(sipCfg.divPwd_change, data);
			change_password_of_divert_account(sipCfg.divPwd_change);
			SaveSipConfigFile();
			ret = 0;
			break;
		#endif
		default:
			ret = 1;
			break;
	}
	
	return ret;

}

int ReadOneRecordValue(one_vtk_dat* oneRecord, int* pType, char* pValue)
{
	char value[BUFF_LEN] = {0};
	char source[BUFF_LEN] = {0};
	char property[BUFF_LEN] = {0};
	char paraId[BUFF_LEN] = {0};
	char effective[BUFF_LEN] = {0};
	cJSON* json;
	
	if(pValue == NULL)
	{
		return -1;
	}

	pValue[0] = 0;

	GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
	//������Ч
	if(atoi(effective) == 0)
	{
		return -1;
	}
		
	GetIoDataKeyValue(oneRecord, PROPERTY, property);
	//���ɶ�
	if(!(atoi(property) & 0x01))
	{
		return -1;
	}

	//��ȡ��Դ
	GetIoDataKeyValue(oneRecord, SOURCE, source);
	
	if(pType != NULL)
	{
		//��ȡ��������
		GetIoDataKeyValue(oneRecord, VALUE_TYPE, value);
		*pType = atoi(value);
	}

	GetIoDataKeyValue(oneRecord, PARA_ID, paraId);

	//Ĭ��ֵ��Դ
	if(atoi(source) == 9)
	{
		return ReadExternParameter(paraId, pValue);
	}
	else if(atoi(source) == 1)
	{
		json = cJSON_GetObjectItem(ioProperty.ioValue, paraId);
		
		if (cJSON_IsString(json) && (json->valuestring != NULL))
		{
			strcpy(value, json->valuestring);
		}
		else
		{
			GetIoDataKeyValue(oneRecord, DUFAULT, value);
		}

		HexToDec(value, pValue);
		return 0;
	}
}



//��ȡ���ز���ֵ
uint8 InnerRead(IoRemote_S *pIoMsg)
{
	char value[BUFF_LEN] = {0};
	char source[BUFF_LEN] = {0};
	char property[BUFF_LEN] = {0};
	one_vtk_dat* oneRecord;
	int dataType;

	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id,0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}

	pIoMsg->msg_dat.result = ReadOneRecordValue(oneRecord, &dataType, pIoMsg->msg_dat.ptr_data);
	pIoMsg->msg_dat.ptr_type = dataType;

	return pIoMsg->msg_dat.result;
}

//��ȡ����
uint8 ReadProperty(IoRemote_S *pIoMsg)
{
	char value[BUFF_LEN] = {0};
	one_vtk_dat* oneRecord;

	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id,0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}
	
	//��ȡ��������
	
	GetIoDataKeyValue(oneRecord, EDIT_TYPE, value);
	pIoMsg->msg_dat.editType= atoi(value);

	GetIoDataKeyValue(oneRecord, SOURCE, value);
	pIoMsg->msg_dat.source = atoi(value);

	GetIoDataKeyValue(oneRecord, EFFECTIVE, value);
	pIoMsg->msg_dat.effective = atoi(value);
	
	GetIoDataKeyValue(oneRecord, COMMON, value);
	pIoMsg->msg_dat.common = atoi(value);

	GetIoDataKeyValue(oneRecord, PROPERTY, value);
	pIoMsg->msg_dat.readWrite = atoi(value);

	pIoMsg->msg_dat.result = 0;
	return 0;
}


//�ָ����ز���ֵĬ��ֵ
int InnerFactoryDefaultByLevel(IoRemote_S *pIoMsg)
{
	one_vtk_dat* oneRecord;

	char paraId[BUFF_LEN];
	char level[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char defaultValue[BUFF_LEN];
	
	char tempBuff[BUFF_LEN] = {0};
	int i;
	
	for(i = 0; i<ioProperty.pTable->record_cnt; i++)
	{
		oneRecord = get_one_vtk_record_without_keyvalue(ioProperty.pTable, i);
		if(oneRecord == NULL)
		{
			continue;
		}

		GetIoDataKeyValue(oneRecord, PARA_ID, paraId);
		GetIoDataKeyValue(oneRecord, LEVEL, level);
		GetIoDataKeyValue(oneRecord, SOURCE, source);
		GetIoDataKeyValue(oneRecord, PROPERTY, property);
		GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);


		if(atoi(source) == 1)
		{
			if(atoi(property)&0x02)
			{
				if(atoi(level) == pIoMsg->head.msg_sub_type)
				{
					WriteOneIoDataValue(paraId, defaultValue, defaultValue);
					ioProperty.mustSaveFlag = 1;
					OS_RetriggerTimer(&ioProperty.saveTimer);
				}
			}
		}
		else if(atoi(source) == 9)
		{
			//if(!strcmp(pIoMsg->msg_dat.property_id, SerialNumber))
			//{
			//	strcpy(pIoMsg->msg_dat.ptr_data, GetSysVerInfo_Sn());
			//	pIoMsg->msg_dat.result = 0;
			//}
			
			
		}
	}
	
	pIoMsg->msg_dat.result = 0;
	return 0;
}

//�ָ����ز���ֵĬ��ֵ
int InnerFactoryDefaultById(IoRemote_S *pIoMsg)
{
	one_vtk_dat* oneRecord;

	char paraId[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char defaultValue[BUFF_LEN];
		
	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id, 0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}

	GetIoDataKeyValue(oneRecord, PARA_ID, paraId);
	GetIoDataKeyValue(oneRecord, SOURCE, source);
	GetIoDataKeyValue(oneRecord, PROPERTY, property);
	GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);
	
	if(atoi(source) == 1)
	{
		if(atoi(property)&0x02)
		{
			//if(atoi(level) == pIoMsg->head.msg_sub_type)
			{
				WriteOneIoDataValue(paraId, defaultValue, defaultValue);
				ioProperty.mustSaveFlag = 1;
				OS_RetriggerTimer(&ioProperty.saveTimer);
			}
		}
	}
	else if(atoi(source) == 9)
	{
		//if(!strcmp(pIoMsg->msg_dat.property_id, SerialNumber))
		//{
		//	strcpy(pIoMsg->msg_dat.ptr_data, GetSysVerInfo_Sn());
		//	pIoMsg->msg_dat.result = 0;
		//}
		
		
	}
	
	pIoMsg->msg_dat.result = 0;
	return 0;
}

int JudgeRange(int dataType, char* dataStr,  char* maxStr, char* minStr)
{
	unsigned long data, max, min;
	int i;

	if(strlen(dataStr) == 0)
	{
		return -4;
	}

	max = strtoul(maxStr,NULL,0);
	min = strtoul(minStr,NULL,0);
	
	//�ַ�����
	if(dataType == DATA_TYPE_STR)
	{
		data = strlen(dataStr);
	}
	//��������
	else
	{
		for(i = 0; dataStr[i] != 0; i++)
		{
			if(dataStr[i] < '0' || dataStr[i] > '9')
			{
				return -3;
			}
		}
		data = strtoul(dataStr,NULL,0);
	}
	
	//����ֵ̫��
	if(data > max)
	{
		return -1;
	}
	//����ֵ̫С��
	else if(data < min)
	{
		return -2;
	}
	//����ֵ�ʺϷ�Χ
	else
	{
		return 0;
	}
}


//�жϷ�Χ�Ƿ񳬳�
int InnerJudgeRange(IoRemote_S *pIoMsg)
{
	one_vtk_dat* oneRecord;

	char valueType[BUFF_LEN];
	char min[BUFF_LEN];
	char max[BUFF_LEN];
	
	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id, 0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}
	GetIoDataKeyValue(oneRecord, VALUE_TYPE, valueType);
	GetIoDataKeyValue(oneRecord, MIN_VALUE, min);
	GetIoDataKeyValue(oneRecord, MAX_VALUE, max);
	
	pIoMsg->msg_dat.result = JudgeRange(atoi(valueType), pIoMsg->msg_dat.ptr_data, max, min);
	
	return pIoMsg->msg_dat.result;
}


//��ȡ���ز���Ĭ��ֵ
int InnerReadDefault(IoRemote_S *pIoMsg)
{
	char source[BUFF_LEN] = {0};
	char value[BUFF_LEN] = {0};
	unsigned int valueLen = BUFF_LEN;
	one_vtk_dat* oneRecord;

	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id,0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}
	GetIoDataKeyValue(oneRecord, SOURCE, source);
	
	valueLen = BUFF_LEN;
	get_one_record_string(oneRecord, GetIoDataKeynameIndex(SOURCE), source, &valueLen);
	source[valueLen] = 0;
	if(atoi(source) == 1)
	{
		//��ȡ��������
		GetIoDataKeyValue(oneRecord, DUFAULT, value);
		//��ȡĬ��ֵ
		strcpy(pIoMsg->msg_dat.ptr_data, value);
		pIoMsg->msg_dat.result = 0;
	}
	else if(atoi(source) == 9)
	{
		pIoMsg->msg_dat.result = 0;
	}
	
	//printf("InnerRead id=%s, type=%d, result=%d, data=%s.\n", pIoMsg->msg_dat.property_id, pIoMsg->msg_dat.ptr_type, pIoMsg->msg_dat.result, pIoMsg->msg_dat.ptr_data);
	return 0;
}

int GetIoValueById(char* id, int* type, char* data)
{
	one_vtk_dat* oneRecord;

	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, id, 0);
	if(oneRecord == NULL)
	{
		return -1;
	}

	return ReadOneRecordValue(oneRecord, type, data);

}

//���ض�ȡ����������Ⱥ
int InnerReadGroup(IoRemote_S *pData)
{
	one_vtk_dat* oneRecord;
	int valueLen;	
	char effective[BUFF_LEN];
	char source[BUFF_LEN];
	char common[BUFF_LEN];
	char groupValue[BUFF_LEN];
	char property[BUFF_LEN];
	char value[BUFF_LEN];
	char valueDec[BUFF_LEN];
	char property_id[5];
	int dataIndex;
	int flag;
	int pos1, pos2;
	char* pIndex1, *pIndex2;
	char record[BUFF_LEN];
	char returnData[IO_DATA_LENGTH] = {0};
	char propertyIdGroup[30][5];
	
	for(dataIndex = 0; pData->msg_dat.ptr_data[dataIndex] != 0; dataIndex++)
	{
		if(pData->msg_dat.ptr_data[dataIndex] == '<')
		{
			flag = 1;
			pos1 = dataIndex+1;
		}
		else if(pData->msg_dat.ptr_data[dataIndex] == '>')
		{
			if(flag == 1)
			{
				pos2 = dataIndex;
				
				memcpy(record, &(pData->msg_dat.ptr_data[pos1]), pos2 - pos1);
				record[pos2 - pos1] = 0;
				
				if((pIndex1 = strstr(record, "Group=")) != NULL)
				{
					int group;
					int index;
					int number;
					char temp[10];
					int j, k;

					pIndex1 += strlen("Group=");

					pIndex2 = strchr(pIndex1, ',');
					if(pIndex2 == NULL)
					{
						return -1;
					}

					memcpy(temp, pIndex1, (int)(pIndex2-pIndex1));
					temp[(int)(pIndex2-pIndex1)] = 0;

					group = atoi(temp);

					pIndex2++;

					pIndex1 = strstr(pIndex2, "Index=");
					if(pIndex1 == NULL)
					{
						return -1;
					}
					
					pIndex1 += strlen("Index=");
					pIndex2 = strchr(pIndex1, ',');
					if(pIndex2 == NULL)
					{
						return -1;
					}

					memcpy(temp, pIndex1, (int)(pIndex2-pIndex1));
					temp[(int)(pIndex2-pIndex1)] = 0;

					index = atoi(temp);

					pIndex2++;

					pIndex1 = strstr(pIndex2, "Number=");
					if(pIndex1 == NULL)
					{
						return -1;
					}
					
					pIndex1 += strlen("Number=");

					strcpy(temp, pIndex1);
					
					number = atoi(temp);

					pIndex2++;
					#if 0
					for(j = 0, k = 0; j < ioProperty.pTable->record_cnt; j++)
					{
						oneRecord = get_one_vtk_record_without_keyvalue(ioProperty.pTable, j);
						
						if(oneRecord == NULL)
						{
							continue;
						}

						//������Ч
						GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
						if(atoi(effective) == 0)
						{
							continue;
						}

						GetIoDataKeyValue(oneRecord, GROUP, groupValue);
						if(atoi(groupValue) != group)
						{
							continue;
						}
						
						k++;
					}
					#else
					for(j = 0, k = 0; j < 3; j++)
					{
						snprintf(property_id, 5, "%04d", atoi(ParameterGroup1Part1)+(group-1)*3 + j);
						
						GetIoValueById(property_id, NULL, value);
						
						//printf("property_id = %s, value = %s.\n", property_id, value);
						
						if(strcmp(value, "-"))
						{
							for(pos1 = strtok(value," "); pos1 != NULL; pos1 = strtok(NULL," "))
							{
								//printf("pos1 = %s.\n", pos1);
								if(strlen(pos1) == 4)
								{
									strcpy(propertyIdGroup[k++], pos1);
									if(k >= 30)
									{
										break;
									}
								}
							}
						}
					}
					#endif
					
					//printf("k = %d\n", k);

					sprintf(returnData, "<maxNum=%d>", k);

					if(index >= k)
					{
						return -1;
					}

					if(index + number >= k)
					{
						number = k - index;
					}

					#if 0
					for(j = 0, k = 0; j < ioProperty.pTable->record_cnt; j++)
					{
						oneRecord = get_one_vtk_record_without_keyvalue(ioProperty.pTable, j);
						
						if(oneRecord == NULL)
						{
							continue;
						}

						//������Ч
						GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
						if(atoi(effective) == 0)
						{
							continue;
						}
						
						GetIoDataKeyValue(oneRecord, GROUP, groupValue);
						if(atoi(groupValue) != group)
						{
							continue;
						}
						

						//��ָ����index��ʼ��ȡ
						if(k++ < index)
						{
							continue;
						}


						pData->msg_dat.result = ReadOneRecordValue(oneRecord, NULL, valueDec);

						GetIoDataKeyValue(oneRecord, PARA_ID, property_id);

						snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", property_id, valueDec);
						if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
						{
							pData->msg_dat.result = 0;
							strcat(returnData, record);
						}
						else
						{
							break;
						}
						
						//��ȡnumber������ֵ
						if(k >= index + number)
						{
							break;
						}
						
					}
					#else
					
					for(j = 0; j < number; j++)
					{
						oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, propertyIdGroup[index + j], 0);
						
						//dprintf("propertyIdGroup[%d + %d] = %s\n", index, j, propertyIdGroup[index + j]);

						if(oneRecord == NULL)
						{
							continue;
						}

						//������Ч
						GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
						if(atoi(effective) == 0)
						{
							strcpy(valueDec, "");
						}
						else
						{
							pData->msg_dat.result = ReadOneRecordValue(oneRecord, NULL, valueDec);
						}
						
						snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", propertyIdGroup[index + j], valueDec);

						//dprintf("record = %s.\n", record);
						
						if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
						{
							pData->msg_dat.result = 0;
							strcat(returnData, record);
						}
						else
						{
							break;
						}						
					}
					#endif
					
				}
				else if(strstr(record, "ID=") != NULL)
				{
					memcpy(property_id, record+strlen("ID="), 4);
					property_id[4] = 0;
					
					oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, property_id,0);
					if(oneRecord == NULL)
					{
						continue;
					}

					pData->msg_dat.result = ReadOneRecordValue(oneRecord, NULL, valueDec);

					snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", property_id, valueDec);
					if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
					{
						pData->msg_dat.result = 0;
						strcat(returnData, record);
					}
					else
					{
						break;
					}
				}
			}
			flag = 0;
		}
	}
	
	strcpy(pData->msg_dat.ptr_data, returnData);
	return pData->msg_dat.result;
}

//д���ز���ֵ
uint8 InnerWrite(IoRemote_S *pIoMsg)
{
	one_vtk_dat* oneRecord;

	char paraId[BUFF_LEN];
	char effective[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char valueType[BUFF_LEN];
	char defaultValue[BUFF_LEN];
	char min[BUFF_LEN];
	char max[BUFF_LEN];
	int result;
		

	pIoMsg->msg_dat.property_id[4] = 0;
	oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, pIoMsg->msg_dat.property_id, 0);
	if(oneRecord == NULL)
	{
		pIoMsg->msg_dat.result = 1;
		return 1;
	}
	
	GetIoDataKeyValue(oneRecord, PARA_ID, paraId);
	GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
	GetIoDataKeyValue(oneRecord, SOURCE, source);
	GetIoDataKeyValue(oneRecord, PROPERTY, property);
	GetIoDataKeyValue(oneRecord, VALUE_TYPE, valueType);
	GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);
	GetIoDataKeyValue(oneRecord, MIN_VALUE, min);
	GetIoDataKeyValue(oneRecord, MAX_VALUE, max);

	//����д
	if(!(atoi(property) & 0x02))
	{
		pIoMsg->msg_dat.result = -1;
		return -1;
	}

	//д�뷶Χ����
	result = JudgeRange(atoi(valueType), pIoMsg->msg_dat.ptr_data, max, min);
	if(result != 0 )
	{
		pIoMsg->msg_dat.result = result;
		return result;
	}

	//�洢ֵ��������ļ�
	if(atoi(source) == 9)
	{
		pIoMsg->msg_dat.result = WriteExternParameter(pIoMsg->msg_dat.property_id, pIoMsg->msg_dat.ptr_data);
		return pIoMsg->msg_dat.result;
	}
	//�洢ֵ��io������
	else if(atoi(source) == 1)
	{
		WriteOneIoDataValue(paraId, defaultValue, pIoMsg->msg_dat.ptr_data);
		if(strcmp(DHCP_ENABLE,paraId)==0)
		{
			char dev_num[20];
			if(ReadIpPolicyFromMyConfig(dev_num))
			{
				if(atoi(pIoMsg->msg_dat.ptr_data)!=atoi(dev_num))
				{
					WriteIpPolicyToMyConfig(pIoMsg->msg_dat.ptr_data);
				}
			}
			else
			{
				WriteIpPolicyToMyConfig(pIoMsg->msg_dat.ptr_data);
			}
		}
		pIoMsg->msg_dat.result = 0;
		ioProperty.mustSaveFlag = 1;
		OS_RetriggerTimer(&ioProperty.saveTimer);
		return 0;
	}
}

//�������ֵ�������ļ�����
uint8 SaveIoDataTable(void)
{
	char* jsonString;
	
	if(ioProperty.mustSaveFlag)
	{
		ioProperty.mustSaveFlag = 0;
		jsonString = cJSON_Print(ioProperty.ioSetting);
		SetJsonStringToFile(IO_DATA_VALUE_NAME, jsonString);
		free(jsonString);
	}
	
	OS_StopTimer(&ioProperty.saveTimer);
	return 0;
}

//��д���ز�����Ӧ
uint8 ReadWritelocalResponse( IoRemote_S* pIoMsg)
{
	IoRemote_S  data;
	int len;

	data.head.msg_target_id 	= pIoMsg->head.msg_source_id;
	data.head.msg_source_id	= pIoMsg->head.msg_target_id;			
	data.head.msg_type		= (pIoMsg->head.msg_type|COMMON_RESPONSE_BIT);
	data.head.msg_sub_type	= 0;
	strcpy(data.msg_dat.property_id, pIoMsg->msg_dat.property_id);
	data.msg_dat.ptr_type 	= pIoMsg->msg_dat.ptr_type;
	data.msg_dat.result		= pIoMsg->msg_dat.result;
	data.msg_dat.editType 	= pIoMsg->msg_dat.editType;
	data.msg_dat.source		= pIoMsg->msg_dat.source;
	data.msg_dat.readWrite	= pIoMsg->msg_dat.readWrite;
	data.msg_dat.effective	= pIoMsg->msg_dat.effective;
	strcpy( data.msg_dat.ptr_data, pIoMsg->msg_dat.ptr_data);

	vdp_task_t* pTask = GetTaskAccordingMsgID(data.head.msg_target_id);	
	
	len = sizeof(data)-IO_DATA_LENGTH + strlen(data.msg_dat.ptr_data)+1;

	
	//printf("id=%s,type=%d,result=%d,value=%s.\n",data.msg_dat.property_id, data.msg_dat.ptr_type, data.msg_dat.result, data.msg_dat.ptr_data);

	push_vdp_common_queue(pTask->p_syc_buf, (char*)&data, len);

	return 0;
}

//�ָ��������ô���
void FactoryDefaultProcess(IoRemote_S *pData)
{

	//����Ҫ�������ݵ��ļ�
	ioProperty.mustSaveFlag = 0;
	OS_StopTimer(&ioProperty.saveTimer);
	//ɾ��ԭ�����ļ�
	if(remove(IO_DATA_VALUE_NAME))
	{
		pData->msg_dat.result = 1;
	}
	else
	{
		//���¼��ز�������ֵ��
		load_io_data_setting_table();
		
		pData->msg_dat.result = 0;
		SysVerInfoInit(1);
	}
}


void ToInviteFactoryDefault(IoRemote_S* pData)
{
	IoRemote_S  rspData;
	
	//�����ָ���������
	if(pData->addr == 0xFFFF && pData->targetIp == 0xFFFFFFFF)
	{
		FactoryDefaultProcess(pData);
	}
	else
	{
		ToReadToWriteRemote(pData);
	}
}


void BeInviteFactoryDefault(IoRemote_S* pData)
{

	UDP_IO_CMD_t  sData;

	//�����ָ���������
	if(pData->addr == 0xFFFF)
	{
		FactoryDefaultProcess(pData);
	}
	//������DT���豸�ָ���������
	else
	{
		pData->msg_dat.result = 1;
	}

	sData.msg_type				= (pData->cmd + 0x80)&0xFF;
	sData.device_address		= pData->addr;
	strcpy(sData.property_id, pData->msg_dat.property_id);
	sData.result			= pData->msg_dat.result;
	api_udp_io_server_send_rsp(pData->targetIp, pData->cmd + 0x80, pData->id, (const char*)&sData, sizeof(sData)-IO_CMD_DATA_LEN);
}

//д���ز���ֵ
int RestoreUserOrInstallerDefaults(IoRemote_S *pIoMsg)
{
	pIoMsg->msg_dat.result = RestoreDefaults(ioProperty.ioValue, pIoMsg->head.msg_sub_type);
	ioProperty.mustSaveFlag = 1;
	OS_RetriggerTimer(&ioProperty.saveTimer);
	SaveIoDataTable();
	return 0;
}


//��Զ�̶�ȡ��������
int BeRemoteRead(IoRemote_S *pData)
{
	one_vtk_dat* oneRecord;
	int valueLen;
	char source[BUFF_LEN];
	char effective[BUFF_LEN];
	char common[BUFF_LEN];
	char groupValue[BUFF_LEN];
	char property[BUFF_LEN];
	char value[BUFF_LEN];
	char valueDec[BUFF_LEN];
	char property_id[5];
	int dataIndex;
	int flag;
	int pos1, pos2;
	char* pIndex1, *pIndex2;
	char record[BUFF_LEN];
	char returnData[IO_DATA_LENGTH] = {0};
	char propertyIdGroup[30][5];
	
	if(pData->addr == 0xFFFFFFFF)
	{
		for(dataIndex = 0; pData->msg_dat.ptr_data[dataIndex] != 0; dataIndex++)
		{
			if(pData->msg_dat.ptr_data[dataIndex] == '<')
			{
				flag = 1;
				pos1 = dataIndex+1;
			}
			else if(pData->msg_dat.ptr_data[dataIndex] == '>')
			{
				if(flag == 1)
				{
					pos2 = dataIndex;
					
					memcpy(record, &(pData->msg_dat.ptr_data[pos1]), pos2 - pos1);
					record[pos2 - pos1] = 0;
					
					if((pIndex1 = strstr(record, "Group=")) != NULL)
					{
						int group;
						int index;
						int number;
						char temp[10];
						int j, k;

						pIndex1 += strlen("Group=");

						pIndex2 = strchr(pIndex1, ',');
						if(pIndex2 == NULL)
						{
							return -1;
						}

						memcpy(temp, pIndex1, (int)(pIndex2-pIndex1));
						temp[(int)(pIndex2-pIndex1)] = 0;

						group = atoi(temp);

						pIndex2++;

						pIndex1 = strstr(pIndex2, "Index=");
						if(pIndex1 == NULL)
						{
							return -1;
						}
						
						pIndex1 += strlen("Index=");
						pIndex2 = strchr(pIndex1, ',');
						if(pIndex2 == NULL)
						{
							return -1;
						}


						memcpy(temp, pIndex1, (int)(pIndex2-pIndex1));
						temp[(int)(pIndex2-pIndex1)] = 0;

						index = atoi(temp);

						pIndex2++;

						pIndex1 = strstr(pIndex2, "Number=");
						if(pIndex1 == NULL)
						{
							return -1;
						}
						
						pIndex1 += strlen("Number=");

						strcpy(temp, pIndex1);
						
						number = atoi(temp);

						pIndex2++;
#if 0
						for(j = 0, k = 0; j < ioProperty.pTable->record_cnt; j++)
						{
							oneRecord = get_one_vtk_record_without_keyvalue(ioProperty.pTable, j);
							
							if(oneRecord == NULL)
							{
								continue;
							}
						
							//������Ч
							GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
							if(atoi(effective) == 0)
							{
								continue;
							}
						
							GetIoDataKeyValue(oneRecord, GROUP, groupValue);
							if(atoi(groupValue) != group)
							{
								continue;
							}
							
							k++;
						}
#else
						for(j = 0, k = 0; j < 3; j++)
						{
							snprintf(property_id, 5, "%04d", atoi(ParameterGroup1Part1)+(group-1)*3 + j);
							
							GetIoValueById(property_id, NULL, value);
							
							//printf("property_id = %s, value = %s.\n", property_id, value);
							
							if(strcmp(value, "-"))
							{
								for(pos1 = strtok(value," "); pos1 != NULL; pos1 = strtok(NULL," "))
								{
									printf("pos1 = %s.\n", pos1);
									if(strlen(pos1) == 4)
									{
										strcpy(propertyIdGroup[k++], pos1);
										if(k >= 30)
										{
											break;
										}
									}
								}
							}
						}
#endif

						sprintf(returnData, "<maxNum=%d>", k);
						
						if(index >= k)
						{
							return -1;
						}
						
						if(index + number >= k)
						{
							number = k - index;
						}
						
#if 0
						for(j = 0, k = 0; j < ioProperty.pTable->record_cnt; j++)
						{
							oneRecord = get_one_vtk_record_without_keyvalue(ioProperty.pTable, j);
							
							if(oneRecord == NULL)
							{
								continue;
							}
							
							//������Ч
							GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
							if(atoi(effective) == 0)
							{
								continue;
							}

							GetIoDataKeyValue(oneRecord, COMMON, groupValue);
							if(atoi(groupValue) != group)
							{
								continue;
							}

							//��ָ����index��ʼ��ȡ
							if(k++ < index)
							{
								continue;
							}

							//��ȡnumber������ֵ
							if(k > index + number)
							{
								break;
							}

							pData->msg_dat.result = ReadOneRecordValue(oneRecord, NULL, valueDec);

							GetIoDataKeyValue(oneRecord, PARA_ID, property_id);

							snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", property_id, valueDec);
							if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
							{
								pData->msg_dat.result = 0;
								strcat(returnData, record);
							}
							else
							{
								break;
							}
						}
#else
						
						for(j = 0; j < number; j++)
						{
							oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, propertyIdGroup[index + j], 0);
							
							if(oneRecord == NULL)
							{
								continue;
							}
						
							ReadOneRecordValue(oneRecord, NULL, valueDec);
							
							snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", propertyIdGroup[index + j], valueDec);
						
							//printf("record = %s.\n", record);
							
							if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
							{
								pData->msg_dat.result = 0;
								strcat(returnData, record);
							}
							else
							{
								pData->msg_dat.result = -1;
								break;
							}						
						}
#endif
						
					}
					else if(strstr(record, "ID=") != NULL)
					{
						memcpy(property_id, record+strlen("ID="), 4);
						property_id[4] = 0;
						
						oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, property_id,0);
						if(oneRecord == NULL)
						{
							continue;
						}

						ReadOneRecordValue(oneRecord, NULL, valueDec);

						snprintf(record, BUFF_LEN, "<ID=%s,Value=%s>", property_id, valueDec);
						if(strlen(returnData)+strlen(record)+1 <= IO_DATA_LENGTH)
						{
							pData->msg_dat.result = 0;
							strcat(returnData, record);
						}
						else
						{
							pData->msg_dat.result = -1;
							break;
						}
					}
						
				}
				flag = 0;
			}
		}
		
		strcpy(pData->msg_dat.ptr_data, returnData);
		IoResponseToRemote(pData);
	}
	return 0;
}

//��Զ�̶�ȡ��������
int BeRemoteWrite(IoRemote_S *pData)
{
	one_vtk_dat* oneRecord;
	int dataIndex;
	int flag;
	int pos1, pos2;

	char paraId[BUFF_LEN];
	char effective[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char valueType[BUFF_LEN];
	char defaultValue[BUFF_LEN];
	char min[BUFF_LEN];
	char max[BUFF_LEN];
	char value[BUFF_LEN];
	int valueLen;
	char *p1, *p2;
	
	char tempBuff[BUFF_LEN] = {0};
	if(pData->addr == 0xFFFFFFFF)
	{
		for(dataIndex = 0; pData->msg_dat.ptr_data[dataIndex] != 0; dataIndex++)
		{
			if(pData->msg_dat.ptr_data[dataIndex] == '<')
			{
				flag = 1;
				pos1 = dataIndex;
			}
			else if(pData->msg_dat.ptr_data[dataIndex] == '>')
			{
				if(flag == 1)
				{
					pos2 = dataIndex;

					memcpy(tempBuff, pData->msg_dat.ptr_data+pos1, pos2-pos1);
					tempBuff[pos2-pos1] = 0;
					
					p1 = strstr(tempBuff, "ID=");
					if(p1 == NULL)
					{
						continue;
					}
					
					p2 = strstr(tempBuff, "Value=");
					if(p2 == NULL)
					{
						continue;
					}
					
					memcpy(paraId, p1+strlen("ID="), 4);
					paraId[4] = 0;
					strcpy(value, p2+strlen("Value="));
					
					
					oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, paraId,0);
					if(oneRecord == NULL)
					{
						continue;
					}
					
					GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
					GetIoDataKeyValue(oneRecord, SOURCE, source);
					GetIoDataKeyValue(oneRecord, PROPERTY, property);
					GetIoDataKeyValue(oneRecord, VALUE_TYPE, valueType);
					GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);
					GetIoDataKeyValue(oneRecord, MIN_VALUE, min);
					GetIoDataKeyValue(oneRecord, MAX_VALUE, max);
					
					//����д
					if(!(atoi(property) & 0x02))
					{
						continue;
					}
															
					if(JudgeRange(atoi(valueType), value, max, min) != 0 )
					{
						continue;
					}
					//�洢ֵ��������ļ�
					if(atoi(source) == 9)
					{
						if(WriteExternParameter(paraId, value) == 0)
						{
							pData->msg_dat.result = 0;
						}
					}
					//�洢ֵ��io������
					else if(atoi(source) == 1)
					{
						WriteOneIoDataValue(paraId, defaultValue, value);
						pData->msg_dat.result = 0;
					}

				}
				flag = 0;
			}
		}
		
		IoResponseToRemote(pData);
		
		if(pData->msg_dat.result == 0)
		{
			ioProperty.mustSaveFlag = 1;
			OS_RetriggerTimer(&ioProperty.saveTimer);
		}
	}
	return 0;
}

//��Զ��д��������Ĭ��ֵ
int BeRemoteWriteDefault(IoRemote_S *pData)
{
	one_vtk_dat* oneRecord;
	int dataIndex;
	int flag;
	int pos1, pos2;

	char paraId[BUFF_LEN];
	char name[BUFF_LEN];
	char effective[BUFF_LEN];
	char common[BUFF_LEN];
	char level[BUFF_LEN];
	char group[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char valueType[BUFF_LEN];
	char defaultValue[BUFF_LEN];
	char editType[BUFF_LEN];
	char min[BUFF_LEN];
	char max[BUFF_LEN];
	char value[BUFF_LEN];
	int valueLen;
	char *p1, *p2;
	
	char tempBuff[BUFF_LEN] = {0};
	char tempRspData[BUFF_LEN] = {0};
	
	
	if(pData->addr == 0xFFFFFFFF)
	{
		for(dataIndex = 0; pData->msg_dat.ptr_data[dataIndex] != 0; dataIndex++)
		{
			if(pData->msg_dat.ptr_data[dataIndex] == '<')
			{
				flag = 1;
				pos1 = dataIndex;
			}
			else if(pData->msg_dat.ptr_data[dataIndex] == '>')
			{
				if(flag == 1)
				{
					pos2 = dataIndex;

					memcpy(tempBuff, pData->msg_dat.ptr_data+pos1, pos2-pos1);
					tempBuff[pos2-pos1] = 0;
					
					p1 = strstr(tempBuff, "ID=");
					if(p1 == NULL)
					{
						continue;
					}
					
					memcpy(paraId, p1+strlen("ID="), 4);
					paraId[4] = 0;

					oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, paraId,0);
					if(oneRecord == NULL)
					{
						continue;
					}
					
					GetIoDataKeyValue(oneRecord, NAME, name);
					GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
					GetIoDataKeyValue(oneRecord, COMMON, common);
					GetIoDataKeyValue(oneRecord, LEVEL, level);
					GetIoDataKeyValue(oneRecord, GROUP, group);
					GetIoDataKeyValue(oneRecord, SOURCE, source);
					GetIoDataKeyValue(oneRecord, PROPERTY, property);
					GetIoDataKeyValue(oneRecord, VALUE_TYPE, valueType);
					GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);
					GetIoDataKeyValue(oneRecord, EDIT_TYPE, editType);
					GetIoDataKeyValue(oneRecord, MIN_VALUE, min);
					GetIoDataKeyValue(oneRecord, MAX_VALUE, max);

					
					//����д
					if(!(atoi(property) & 0x02))
					{
						continue;
					}
															
					if(JudgeRange(atoi(valueType), defaultValue, max, min) != 0 )
					{
						continue;
					}
					
					//�洢ֵ��������ļ�
					if(atoi(source) == 9)
					{
						sprintf(value, "<ID=%s>", paraId);
					}
					//�洢ֵ��io������
					else if(atoi(source) == 1)
					{
						WriteOneIoDataValue(paraId, defaultValue, defaultValue);
						pData->msg_dat.result = 0;
						sprintf(value, "<ID=%s,Value=%s>", paraId, defaultValue);
					}
					
					if(strlen(value) + strlen(tempRspData) < BUFF_LEN)
					{
						strcat(tempRspData, value);
					}
					else
					{
						break;
					}

				}
				flag = 0;
			}
		}
		
		strcpy(pData->msg_dat.ptr_data, tempRspData);
		
		IoResponseToRemote(pData);
		
		if(pData->msg_dat.result == 0)
		{
			ioProperty.mustSaveFlag = 1;
			OS_RetriggerTimer(&ioProperty.saveTimer);
		}
	}
	return 0;
}

//ͨ��UDP ��ӦԶ�̶�д
uint8 IoResponseToRemote(IoRemote_S* pData )
{
	UDP_IO_CMD_t  sData;
	int len;

	sData.msg_type			= (pData->cmd + 0x80)&0xFF;
	sData.device_address	= htons(pData->addr);
	strcpy(sData.property_id, pData->msg_dat.property_id);
	sData.result			= pData->msg_dat.result;
	sData.ptr_type			= pData->msg_dat.ptr_type;
	strcpy(sData.realData, pData->msg_dat.ptr_data);

	len = sizeof(sData)-IO_CMD_DATA_LEN+strlen(sData.realData)+1;
	
	api_udp_io_server_send_rsp(pData->targetIp, pData->cmd + 0x80, pData->id, (const char*)&sData, len);
	return 0;
}

//ͨ��UDP ��дԶ���豸
uint8 ToReadToWriteRemote(IoRemote_S* pData)
{
	UDP_IO_CMD_t  	sData;
	UDP_IO_CMD_t  	rData;
	unsigned int 	rlen;
	unsigned int	slen;
	
	sData.msg_type 			= pData->cmd & 0xFF;
	sData.device_address	= pData->addr;
	strcpy(sData.property_id, pData->msg_dat.property_id);
	strcpy(sData.realData, pData->msg_dat.ptr_data);

	slen = sizeof(sData)-IO_CMD_DATA_LEN+strlen(sData.realData)+1;
	rlen = sizeof(sData);

	if(api_udp_io_server_send_req(pData->targetIp, pData->cmd, (char*)&sData, slen, (char*)&rData, &rlen))
	{
		pData->msg_dat.result = 1;
		return 1;
	}
	else
	{
		if(rData.result)
		{
			pData->msg_dat.result = 2;			
			return 2;
		}
		else
		{
			pData->msg_dat.result = rData.result;
			strcpy(pData->msg_dat.ptr_data, rData.realData);	
			return 0;
		}
	}
}

//��дԶ��IP �豸ʱ�Ա��������Ӧ
uint8 ReadWriteRemoteResponse(IoRemote_S* pdata)
{
	IoRemote_S  data;
	vdp_task_t* pTask;

	data.head.msg_target_id 	= pdata->head.msg_source_id;
	data.head.msg_source_id	= pdata->head.msg_target_id;	
	data.head.msg_type		= (pdata->head.msg_type|COMMON_RESPONSE_BIT);
	data.head.msg_sub_type	= 0;

	data.addr					= pdata->addr;
	data.targetIp				= pdata->targetIp;
	strcpy(data.msg_dat.property_id, pdata->msg_dat.property_id);
	data.msg_dat.result		= pdata->msg_dat.result;
	data.msg_dat.ptr_type	= pdata->msg_dat.ptr_type;
	strcpy( data.msg_dat.ptr_data, pdata->msg_dat.ptr_data);

	pTask = GetTaskAccordingMsgID(pdata->head.msg_source_id);

	push_vdp_common_queue(pTask->p_syc_buf, (char*)&data, sizeof(data)-IO_DATA_LENGTH+strlen(pdata->msg_dat.ptr_data)+1);
	return 0;
}

void ReloadIoDataTable(void)
{
	//�ͷ�ԭ��������
	free_vtk_table_file_buffer(ioProperty.pTable);
	//���¼��ز�����
	load_io_data_table();
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
