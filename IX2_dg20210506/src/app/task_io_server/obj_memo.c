

#include "task_IoServer.h"
#include "task_Power.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"

#include "video_playback.h"
#include "jpeg_dec.h"
#include "define_file.h"

vd_record_t		one_vd_record;
vd_playback_t	one_vd_playback;

int create_video_record_filename( char* pfilename )
{
	#define VIDEO_RECORD_FILENAME_LEN	100
	int len = VIDEO_RECORD_FILENAME_LEN;
	char strtime[VIDEO_RECORD_FILENAME_LEN+1];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	if(Judge_SdCardLink() == 1)     //lyx 20170619 SD卡存在得到录衔募名，不存在得到拍照文件名
		sprintf( strtime,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	else
		sprintf( strtime,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);

	strtime[len-1]=0;
	memcpy( pfilename, strtime, len ); 
	return len;
}

// 录像实例初始化
void memo_vd_record_init(void)
{
	one_vd_record.state		= 0;
	one_vd_record.pRelation = NULL;
	one_vd_record.max_len 	= 0;	
	pthread_mutex_init( &one_vd_record.lock, 0);

	jpegState_init();
}

void memo_vd_record_notice_disp(int on)
{
	char data[1];
	data[0] = on;
		
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_RECORD_NOTICE, data, 1);
}

// 停止录像: 座位录像结束后的回调函数，也可手动停止，停止后写入了关联的记录
void memo_vd_record_stop(void)
{
	CALL_RECORD_DAT_T* pcall_record;
		
	pthread_mutex_lock( &one_vd_record.lock );
	
	if( one_vd_record.state )
	{
		one_vd_record.state = 0;
		bprintf("+++++++++stop video record, filename:%s\n+++++++++++",one_vd_record.filename);	
		one_vd_record.spriteState = 0;
		memo_vd_record_notice_disp(one_vd_record.spriteState);
	}
	
	pthread_mutex_unlock( &one_vd_record.lock );
}


char* get_lastest_record_name(void)
{
	return one_vd_record.filename;
}
void vd_record_stop(int index);
// 启动录像: prelation - 为关联的CALL_RECORD_DAT_T指针
// return: 0/ok, -2/空间满了， -3/创建目录失败， -1/其他失败
int memo_video_record_start( void* prelation )		//czn_20170303
{
	int result = -1;
	char temp[20];
	int recTime;
	API_Event_IoServer_InnerRead_All(AutoRecordEnable, temp);

	if(!atoi(temp))
	{
		return -1;
	}
	
	//memo_vd_record_stop();	//cao_20161230
	API_RecordEnd();		//cao_20161230

	pthread_mutex_lock( &one_vd_record.lock );
	
	one_vd_record.pRelation = prelation;

	
	create_video_record_filename(one_vd_record.filename);

	//strcpy(one_vd_record.filename,"test1.avi");

	printf("-------filename:%s------\r\n", one_vd_record.filename);     //lyx 20170620

	#if 0
	if(Judge_SdCardLink() == 1)   ////lyx 20170619  有SD卡启动录像，无SD卡启动拍照
	{
		result = API_RecordStart( one_vd_record.filename, 10, memo_vd_record_stop );
		one_vd_record.state = 1;   	//开始录像
		bprintf("+++++++++++start video record, filename:%s+++++++++++\n",one_vd_record.filename);
	}
	else
	{
		result = API_JpegCaptureStart(one_vd_record.filename,memo_vd_record_stop);
		one_vd_record.state = 2;	//开始拍照
		bprintf("+++++++++++start jpeg capture, filename:%s+++++++++++\n",one_vd_record.filename);
	}
	#endif
	if(Judge_SdCardLink() == 1)
	{
		recTime = atoi(temp);
	}
	else
	{
		recTime = 3;
	}
	result = API_RecordStart_mux(0, 0, 15, one_vd_record.filename, recTime, 0, vd_record_stop);
	if( result == 0 )
	{
		SetWinRecState(0, 1);	//开始录像
		vd_record_inform(1, 0+1);	
	}
	#if 0
	if( result == 0 )
	{
		one_vd_record.spriteState = 1;
		memo_vd_record_notice_disp(one_vd_record.spriteState);	
	}
	else
	{
		one_vd_record.state = 0;
		strcpy(one_vd_record.filename, "-");
	}
	#endif
	pthread_mutex_unlock( &one_vd_record.lock );

	return result;
}

// 播放实例初始化
void memo_vd_playback_init(void)
{
	one_vd_playback.state		= 0;
	one_vd_playback.pRelation 	= NULL;
	one_vd_playback.max_len		= 0;	
	pthread_mutex_init( &one_vd_playback.lock, 0);

	jpegState_init();   //lyx 20170627
}

void memo_vd_playback_second_step(int sec)
{
	struct {int curnLen; int maxLen;} data;
	
//	data.curnLen = ++one_vd_playback.cur_len;
	data.curnLen = sec;
	data.maxLen = one_vd_playback.max_len;
	
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_PLYBAK_STEP, (char*) &data, 8);
}
// 启动播放: filename - 播放的文件名全路径， prelation - 为关联的CALL_RECORD_DAT_T指针
// return: 0/ok, -1/err

char* saveFilename;
void* savePrelation;

int memo_video_playback_start( char* filename, void* prelation )
{
	int len;
	int ret;
	char temp[10];
	int res;
	char fullFileName[100];

	saveFilename = filename;
	savePrelation = prelation;
	
	pthread_mutex_lock( &one_vd_playback.lock );

	strcpy(temp , filename + strlen(filename) - 3);

	if (strcmp( temp,"avi") == 0 )          //lyx 20170619
	{
		//API_POWER_RING_OFF();
		res = API_PlaybackStart( filename, &len, memo_vd_playback_second_step ) ;
		one_vd_playback.state 		= 1;
	}
	else
	{
		snprintf(fullFileName, 100, "%s/%s", JPEG_STORE_DIR, filename);

		#if 0
		if( get_pane_type() == 1 )
		{
			set_h264_show_pos(1,0,0,800,1014);
		}
		else if( get_pane_type() == 0 )
		{
			set_h264_show_pos(1,0,0,1013,800);
		}
		else
		{
			set_h264_show_pos(1,0,0,824,600);
		}
		#endif
		res =  API_JpegPlaybackStart(fullFileName);	
		one_vd_playback.state 		= 3;
	}
	
	if( res == 0 )	
	{
		one_vd_playback.pRelation	= prelation;
		strcpy(one_vd_playback.filename,filename);
		one_vd_playback.max_len = len;
		one_vd_playback.cur_len = 0;
		ret = 0;
	}
	else
	{
		ret = -1;
	}
	
	pthread_mutex_unlock( &one_vd_playback.lock );

	return ret;
}

// 暂停播放
// return: 1/ok, 0/err
int memo_video_playback_pause( void )
{
	if( one_vd_playback.state == 1 )
	{
		one_vd_playback.state = 2;
		API_PlaybackPause();
		return 1;
	}
	else
		return 0;
}

// 继续播放
// return: 1/ok, 0/err
int memo_video_playback_continue( void )
{
	if( one_vd_playback.state == 2 )
	{
		one_vd_playback.state = 1;
		API_PlaybackContinue();
		return 1;
	}
	else
		return 0;
}

// 重新播放
int memo_video_replay( void )
{
	if(API_GetPlaybackState() == RECORD_PLAYBACK_IDLE)
	{
		memo_video_playback_start(saveFilename, savePrelation);
	}
	else if(API_GetPlaybackState() == RECORD_PLAYBACK_END)
	{
		one_vd_playback.cur_len = 0;
		API_PlaybackReplay();
	}
	else if(API_GetPlaybackState() == RECORD_PLAYBACK_RUN)
	{
		API_PlaybackPause();
	}
	else if(API_GetPlaybackState() == RECORD_PLAYBACK_PAUSE)
	{
		API_PlaybackContinue();
	}

	return API_GetPlaybackState();
}


// 停止播放
void memo_vd_playback_stop(void)
{
	pthread_mutex_lock( &one_vd_playback.lock );
	
	if ( ( one_vd_playback.state ==1) ||( one_vd_playback.state ==2) )     //lyx 20170619
	{
		one_vd_playback.state = 0;
		API_PlaybackClose();
		bprintf("+++++++++stop video playback, filename:%s\n+++++++++++",one_vd_playback.filename);		
	}
	else if  ( one_vd_playback.state ==3 ) 
	{
		one_vd_playback.state = 0;	
		API_JpegPlaybackClose();
		bprintf("+++++++++stop jpeg playback, filename:%s\n+++++++++++",one_vd_playback.filename);
	}
	pthread_mutex_unlock( &one_vd_playback.lock );
}


// 格式化SD 卡
int memo_format_sdcard(void)
{
	return 0;
}

// 得到sd card 的容量信息
// return: 0/ok，1/err
int memo_get_sdcard_inform(void)
{
	return 0;
}


