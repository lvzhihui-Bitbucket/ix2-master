/**
  ******************************************************************************
  * @file    obj_OtherParameterSetting.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

  
#ifndef obj_OtherParameterSetting_H
#define obj_OtherParameterSetting_H

#define OTHER_PARA_DATA_LEN			50
#define MODULE_MAX_NUM				50


typedef struct 
{
	int s4[MODULE_MAX_NUM];
	int mk[MODULE_MAX_NUM];
	int dr[MODULE_MAX_NUM];
	int s4Nbr;
	int mkNbr;
	int drNbr;
} EXT_MODULE_JSON;



typedef struct 
{
	char 		deviceType[OTHER_PARA_DATA_LEN+1];
	char 		codeInfo[OTHER_PARA_DATA_LEN+1];
	char 		appCode[OTHER_PARA_DATA_LEN+1];
	char 		appVer[OTHER_PARA_DATA_LEN+1];
	int 		codeSize;
} FW_VER_VERIFY_T;



#endif
