

#ifndef _obj_call_record_H
#define _obj_call_record_H

//cao_20160718
#define EVENT_TYPE					"EVENT_TYPE"
#define EVENT_SUB_TYPE				"EVENT_SUB_TYPE"
#define EVENT_PROPERTY				"EVENT_PROPERTY"
#define EVENT_TARGET_NODE				"TARGET_NODE"		//czn_20170329
#define EVENT_TARGET_ID				"TARGET_ID"
#define EVENT_TARGET_IP				"TARGET_IP"
#define EVENT_INPUT					"INPUT"
#define EVENT_NAME					"NAME"
#define EVENT_TIME					"TIME"
#define EVENT_RELATION				"RELATION"

extern one_vtk_table * pcall_record_table;
extern one_vtk_table * pcall_record_incoming;
extern one_vtk_table * pcall_record_outgoing;
extern one_vtk_table * pcall_record_missed;
extern one_vtk_table * pvideo_record;

//cao_20160718
#define		EVENT_NAME_MAX		20
#define 	EVENT_TIME_MAX		19
#define 	EVENT_RELATION_MAX	40	
#define 	EVENT_INPUT_MAX			8

#define MAX_RECORD_ITEMS			201

//event type
typedef enum
{
	CALL_RECORD = 0,
	VIDEO_RECORD,
	ALARM_RECORD,
	TABLE_INFO,	//czn_20170414
}EVENT_TYPE_E;
typedef enum
{
	// call record
	OUT_GOING = 0,
	IN_COMING,
	// video record
	LOCAL = 2,
	REMOTE = 3,
	// alarm record
	ARM = 4,
	DISARM = 5,
	ALARM = 6,
}EVENT_SUB_TYPE_E;
typedef enum
{	
	// call recoard
	NORMAL = 0,
	MISSED	= 0x01,
	CALL_FAIL	= 0x20,		//czn_20190819
	RECORD_READ = 0x40,
	RECORD_DELETED = 0x80
}EVENT_PROPERTY_E;


typedef struct
{
	EVENT_TYPE_E 		type;
	EVENT_SUB_TYPE_E 	subType;
	EVENT_PROPERTY_E 	property;
	//
	int 				target_id;
	int					target_node;
	char				input[EVENT_INPUT_MAX +2];
	char 				name[EVENT_NAME_MAX+2];
	char				time[EVENT_TIME_MAX+1];
	char				relation[EVENT_RELATION_MAX+2];	//cao_20160718
}CALL_RECORD_DAT_T;

typedef enum
{
	CallRecord_FilterType_IncomingCall = 0,
	CallRecord_FilterType_OutgoingCall,
	CallRecord_FilterType_MissCall,
	CallRecord_FilterType_VideoRecord,
	CallRecord_FilterType_UnreadMissCall,
}CallRecord_FilterType_E;

// obj_call_record api
int api_load_call_record( void );
int api_register_one_call_record( CALL_RECORD_DAT_T* pcall_record_dat );
int api_free_call_record( void );

// 从callrecord记录中得到的所有字段数据
int get_callrecord_table_record_items( one_vtk_table* ptable, int index, CALL_RECORD_DAT_T* pcall_record_value );

int create_call_record_index_table(void);
int free_call_record_index_table(void);

int create_video_record_index_table(void);
int free_video_record_index_table(void);
//czn_20170306_s
int call_record_data_convert(CALL_RECORD_DAT_T *pcall_record_dat,char *recordBuffer);	
int call_record_modify(one_vtk_dat*	precord,int act_type);
	#define call_record_mark_read(precord)				call_record_modify(precord,0)
	#define call_record_mark_delete(precord)				call_record_modify(precord,1)
	#define call_record_mark_delete_video(precord)		call_record_modify(precord,2)
int call_record_filter(int filter_type);	
int call_record_get_total_index(one_vtk_dat*	precord);
int call_record_get_total_index_inner(one_vtk_dat*	precord);
int get_callrecord_table_record_items_inner( one_vtk_table* ptable, int index, CALL_RECORD_DAT_T* pcall_record_value );
void miss_call_register(void);
void miss_call_one_have_read(void);
void miss_call_adjust(int adjust_cnt);
void call_record_delete_all(void);	//czn_20170321
//czn_20170306_e

//czn_20170414_s
int RecordTb_CircleWrite_Init(void);
int RecordTb_CircleWrite_AddOneRecord(int record_len,char *record_buff);
int RecordTb_CircleWrite_Add_IndexTransfer(int actual_index);
//czn_20170414_e

int call_record_allmiss_mark_read(void);
#endif


