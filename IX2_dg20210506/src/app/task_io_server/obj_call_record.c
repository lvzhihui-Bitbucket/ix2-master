
#include "task_IoServer.h"
//#include "../task_debug_sbu/task_debug_sbu.h"
#include "task_Led.h"
#include "obj_call_record.h"
#include "task_VideoMenu.h"


one_vtk_table * pcall_record_table 		= NULL;

one_vtk_table * pcall_record_incoming 	= NULL;
one_vtk_table * pcall_record_outgoing	= NULL;
one_vtk_table * pcall_record_missed	= NULL;
one_vtk_table * pvideo_record		= NULL;

const unsigned char* CALL_RECORD_KEYNAME_TAB[] = 
{
	EVENT_TYPE,
	EVENT_SUB_TYPE,
	EVENT_PROPERTY,
	EVENT_TARGET_ID,
	EVENT_TARGET_NODE,		//czn_20170329
	//EVENT_TARGET_IP,
	EVENT_INPUT,
	EVENT_NAME,
	EVENT_TIME,
	EVENT_RELATION,
};

struct 
{
	int record_index;
	int start_index;
}RecordTb_CircleWrite_Run;

const int CALLRECORD_KEYNAME_CNT = sizeof(CALL_RECORD_KEYNAME_TAB)/sizeof(unsigned char*);

pthread_mutex_t Call_Record_Lock = PTHREAD_MUTEX_INITIALIZER;

// 加载呼叫记录表
int api_load_call_record(void)		//czn_20170321
{
	FILE* file 		= NULL;
	int i,str_len;
	
	// 加载基础数据表
	pcall_record_table = load_vtk_table_file( CALL_RECORD_FILE_NAME);
	if( pcall_record_table != NULL  )
	{
		printf("load table from \"/mnt/nand1-2/call_record_table.csv\".\n");
		if( 1 )
		{
			//printf_one_table(pcall_record_table);
		}
		
		if(pcall_record_table->keyname_cnt != CALLRECORD_KEYNAME_CNT)
		{
			remove(CALL_RECORD_FILE_NAME);
			free_vtk_table_file_buffer(pcall_record_table);
			goto ReCreateRecordTable;
		}

		for(i = 0; i < pcall_record_table->keyname_cnt; i++)
		{
			str_len = strlen(CALL_RECORD_KEYNAME_TAB[i]);
			
			if(pcall_record_table->pkeyname[i].len != str_len)
			{
				remove(CALL_RECORD_FILE_NAME);
				free_vtk_table_file_buffer(pcall_record_table);
				goto ReCreateRecordTable;
			}

			if(memcmp(pcall_record_table->pkeyname[i].pdat,CALL_RECORD_KEYNAME_TAB[i],str_len) != 0)
			{
				remove(CALL_RECORD_FILE_NAME);
				free_vtk_table_file_buffer(pcall_record_table);
				goto ReCreateRecordTable;
			}
		}
		RecordTb_CircleWrite_Init();

		call_record_filter(CallRecord_FilterType_UnreadMissCall);		//czn_20170805
		return 0;
	}
	
ReCreateRecordTable:
	pcall_record_table = malloc(sizeof(one_vtk_table));
	
	pthread_mutex_init( &pcall_record_table->lock, 0);

	pcall_record_table->keyname_cnt = CALLRECORD_KEYNAME_CNT;	
	pcall_record_table->pkeyname = malloc(pcall_record_table->keyname_cnt * sizeof(one_vtk_dat));
	for( i = 0; i < pcall_record_table->keyname_cnt; i++ )
	{
		pcall_record_table->pkeyname[i].len = strlen(CALL_RECORD_KEYNAME_TAB[i]);
		pcall_record_table->pkeyname[i].pdat = malloc(pcall_record_table->pkeyname[i].len);
		memcpy(pcall_record_table->pkeyname[i].pdat, CALL_RECORD_KEYNAME_TAB[i], pcall_record_table->pkeyname[i].len);	
		//printf("--cnt=%d,len=%d,str=%s\n",pcall_record_table->keyname_cnt,pcall_record_table->pkeyname[i].len,pcall_record_table->pkeyname[i].pdat);
	}		

	pcall_record_table->pkeyname_len = malloc(pcall_record_table->keyname_cnt * sizeof(int));

	for( i = 0; i < pcall_record_table->keyname_cnt; i++ )
	{
		pcall_record_table->pkeyname_len[i] = 40;
	}
	
	pcall_record_table->record_cnt = 0;
	pcall_record_table->precord = NULL;
	
	//printf_one_table(pcall_record_table);
	save_vtk_table_file_buffer( pcall_record_table, 0, 0, CALL_RECORD_FILE_NAME);
	sync();

	RecordTb_CircleWrite_Init();
	return 0;
}

void call_record_delete_all(void)
{
	int i;
	CALL_RECORD_DAT_T record_dat;
	char cmd[100];
	
	pthread_mutex_lock(&Call_Record_Lock);
	
	if(pcall_record_table != NULL)
	{
		for(i = 0;i < pcall_record_table->record_cnt;i ++)
		{
			if( get_callrecord_table_record_items_inner(pcall_record_table,i,&record_dat) != 0)
				continue;
			if(strcmp(record_dat.relation,"-") != 0)
			{
				Delete_VideoFile(record_dat.relation);
			}
			snprintf(cmd,100,"rm -r %s",JPEG_STORE_DIR);
			system(cmd);
		}
	}
	
	remove(CALL_RECORD_FILE_NAME);
	free_vtk_table_file_buffer(pcall_record_table);
	pcall_record_table = NULL;
	api_load_call_record();
	release_one_vtk_tabl(pcall_record_incoming);
	pcall_record_incoming = NULL;
	release_one_vtk_tabl(pcall_record_outgoing);
	pcall_record_outgoing = NULL;
	release_one_vtk_tabl(pvideo_record);
	pvideo_record = NULL;
	
	pthread_mutex_unlock(&Call_Record_Lock);

	sync();
}

void api_reload_call_record(void)
{
	free_vtk_table_file_buffer(pcall_record_table);
	pcall_record_table = NULL;
	api_load_call_record();
	release_one_vtk_tabl(pcall_record_incoming);
	pcall_record_incoming = NULL;
	release_one_vtk_tabl(pcall_record_outgoing);
	pcall_record_outgoing = NULL;
	release_one_vtk_tabl(pvideo_record);
	pvideo_record = NULL;
}

// 保存一条呼叫记录数据
int api_register_one_call_record( CALL_RECORD_DAT_T* pcall_record_dat )	//czn_20170303
{
	one_vtk_dat	*pTempRecord = NULL;
	char			recordBuffer[BUFF_ONE_RECORD_LEN];
	char 			tempBuffer[BUFF_ONE_KEY_NAME_LEN];
	int			temp;
	int			bufferIndex = 0;
	int			i;
	unsigned char  *precord_dat;
	CALL_RECORD_DAT_T record_temp;
	
	char strtime[30];

	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	snprintf( pcall_record_dat->time,EVENT_TIME_MAX, "%02d/%02d/%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);
	//snprintf( pcall_record_dat->time,EVENT_TIME_MAX, "%02d:%02d",tblock->tm_hour,tblock->tm_min);
	# if 1
	pthread_mutex_lock(&Call_Record_Lock);
	
	if(pcall_record_table == NULL)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	//if(pcall_record_table->record_cnt >= MAX_RECORD_ITEMS)	//czn_20170321
	{
	//	pthread_mutex_unlock(&Call_Record_Lock);
	//	return -1;
	}
	
	bufferIndex = call_record_data_convert(pcall_record_dat,recordBuffer);
	
	#if 0
	precord_dat = malloc(bufferIndex);
	if(precord_dat == NULL)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	if(pcall_record_table->record_cnt++ == 0)
	{
		pcall_record_table->precord = NULL;
	}

	pTempRecord = pcall_record_table->precord;
	pcall_record_table->precord = malloc(pcall_record_table->record_cnt * sizeof(one_vtk_dat));

	if(pcall_record_table->precord == NULL)		//czn_20170303
	{
		pcall_record_table->record_cnt--;
		pcall_record_table->precord = pTempRecord;
		free(precord_dat);
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	if(pTempRecord != NULL)
	{
		for(i = 0; i < pcall_record_table->record_cnt; i++)
		{
			 pcall_record_table->precord[i] = pTempRecord[i];
		}
		free(pTempRecord);
	}
	
	


	pcall_record_table->precord[pcall_record_table->record_cnt-1].len = bufferIndex;
	pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat = precord_dat;
	memcpy(pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat, recordBuffer, bufferIndex);
	
	save_vtk_table_file_buffer( pcall_record_table, pcall_record_table->record_cnt - 1, 1, CALL_RECORD_FILE_NAME);
	if(pcall_record_dat->property & MISSED)
	{
		miss_call_register();
	}
	#endif
	if(bufferIndex == -1)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	i = RecordTb_CircleWrite_AddOneRecord(bufferIndex,recordBuffer);

	if(pcall_record_dat->property & MISSED)
	{
		miss_call_register();
	}
	pthread_mutex_unlock(&Call_Record_Lock);
	//sync();
	#endif
	return i;
}



// 从callrecord记录中得到的所有字段数据
int get_callrecord_table_record_items( one_vtk_table* ptable, int index, CALL_RECORD_DAT_T* pcall_record_value )
{
	//unsigned char 	input_str[40+1];
	//int 			input_str_len,i;	
	//one_vtk_dat*	precord;
	int result;
	
	pthread_mutex_lock(&Call_Record_Lock);
	result = get_callrecord_table_record_items_inner(ptable,index,pcall_record_value);
	pthread_mutex_unlock(&Call_Record_Lock);
	
	return result; 
	#if 0
	pthread_mutex_lock(&Call_Record_Lock);
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		if(ptable != pcall_record_table)
		{
			if(call_record_get_total_index_inner(precord) == -1)
			{
				pthread_mutex_unlock(&Call_Record_Lock);
				return -1;
			}
		}
		int keyname_index;
		// EVENT_TYPE
		keyname_index = get_keyname_index( ptable, EVENT_TYPE );
		input_str_len = MAX_DEV_NAME_LEN;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pcall_record_value->type= atol(input_str);
		
		// EVENT_SUB_TYPE
		keyname_index = get_keyname_index( ptable, EVENT_SUB_TYPE );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;		
		pcall_record_value->subType= atol(input_str);
		
		// EVENT_PROPERTY 			
		keyname_index = get_keyname_index( ptable, EVENT_PROPERTY );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;		
		//pcall_record_value->property= atol(input_str);
		sscanf(input_str,"%02x",&pcall_record_value->property);

		// EVENT_TARGET_ID
		keyname_index = get_keyname_index( ptable, EVENT_TARGET_ID );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		sscanf(input_str,"%04x",&pcall_record_value->target_id);
		//pcall_record_value->target_id= atol(input_str);
		
		// EVENT_TARGET_IP
		keyname_index = get_keyname_index( ptable, EVENT_TARGET_IP );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		sscanf(input_str,"%08x",&pcall_record_value->target_ip);
		//pcall_record_value->target_ip= atol(input_str);

		// EVENT_INPUT
		keyname_index = get_keyname_index( ptable, EVENT_INPUT );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 8;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->input,&input_str[i + 1],9);
		
		// EVENT_NAME
		keyname_index = get_keyname_index( ptable, EVENT_NAME );
		input_str_len = MAX_DEV_NAME_LEN + 1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 20;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->name,&input_str[i + 1],21);
		//memcpy( pcall_record_value->name,input_str,input_str_len+1);	

		// EVENT_TIME
		keyname_index = get_keyname_index( ptable, EVENT_TIME );
		input_str_len = MAX_DEV_NAME_LEN;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pcall_record_value->time,input_str,input_str_len+1);	

		// EVENT_RELATION
		keyname_index = get_keyname_index( ptable, EVENT_RELATION );
		input_str_len = 40;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 40;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->relation,&input_str[i + 1],41);
		pthread_mutex_unlock(&Call_Record_Lock);
		//memcpy( pcall_record_value->relation,input_str,input_str_len+1);	
		return 0;
	}
	else
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	#endif
}

int create_call_record_index_table(void)
{
	one_vtk_dat*	pcall_record;
	int				call_record_cnt;

	//free_call_record_index_table();
	if( pcall_record_incoming != NULL )	release_one_vtk_tabl(pcall_record_incoming);
	if( pcall_record_outgoing != NULL )	release_one_vtk_tabl(pcall_record_outgoing);
	if( pcall_record_missed != NULL )	release_one_vtk_tabl(pcall_record_missed);
	
	if( pcall_record_table == NULL )
		return -1;

	// 1. 生成pcall_record_incoming
	pcall_record_incoming	= create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);
	
	call_record_cnt = 0;
	while(1)
	{
		pcall_record = get_one_vtk_record( pcall_record_table, EVENT_SUB_TYPE, "01", call_record_cnt);

		if( pcall_record == NULL )
		{
			break;
		}
		else
		{
			add_one_vtk_record( pcall_record_incoming, pcall_record );
			call_record_cnt++;
		}
	}
	//printf_one_table(pcall_record_incoming);
	
	// 2. 生成pcall_record_outgoing
	pcall_record_outgoing	= create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);
	call_record_cnt = 0;
	while(1)
	{
		pcall_record = get_one_vtk_record( pcall_record_table, EVENT_SUB_TYPE, "00", call_record_cnt);
		if( pcall_record == NULL )
		{
			break;
		}
		else
		{
			add_one_vtk_record( pcall_record_outgoing, pcall_record );
			call_record_cnt++;
		}
	}
	//printf_one_table(pcall_record_outgoing);

	// 3. 生成pcall_record_missed
	pcall_record_missed	= create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);
	call_record_cnt = 0;
	while(1)
	{
		pcall_record = get_one_vtk_record( pcall_record_table, EVENT_PROPERTY,"01", call_record_cnt);
		if( pcall_record == NULL )
		{
			break;
		}
		else
		{
			add_one_vtk_record( pcall_record_missed, pcall_record );
			call_record_cnt++;
		}
	}
	//printf_one_table(pcall_record_missed);
	
	return 0;	
}

int free_call_record_index_table(void)
{
	release_one_vtk_tabl(pcall_record_incoming);
	release_one_vtk_tabl(pcall_record_outgoing);
	release_one_vtk_tabl(pcall_record_missed);
	return 0;
}

int create_video_record_index_table(void)
{
	one_vtk_dat*	pcall_record;
	int			call_record_cnt;

	//free_video_record_index_table();
	if( pvideo_record != NULL )	release_one_vtk_tabl(pvideo_record);

	if( pcall_record_table == NULL )
		return -1;

	// 1. 生成pvideo_record
	pvideo_record	= create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);
	
	call_record_cnt = 0;
	while(1)
	{
		pcall_record = get_one_vtk_record( pcall_record_table, EVENT_TYPE, "01", call_record_cnt);

		if( pcall_record == NULL )
		{
			break;
		}
		else
		{
			add_one_vtk_record( pvideo_record, pcall_record );
			call_record_cnt++;
		}
	}
	//printf_one_table(pvideo_record);
	return 0;	
}

int free_video_record_index_table(void)
{
	release_one_vtk_tabl(pvideo_record);
	return 0;
}
//czn_20170306_s
int call_record_data_convert(CALL_RECORD_DAT_T *pcall_record_dat,char *recordBuffer)
{
	char 		tempBuffer[BUFF_ONE_KEY_NAME_LEN];
	int 			temp;
	
	memset(tempBuffer,' ',40);
	tempBuffer[0] = '/';
	if((temp = strlen(pcall_record_dat->input)) > 8)
	{
		return -1;	
	}
	strncpy(tempBuffer+ 9 -temp,pcall_record_dat->input,temp + 1);
	strcpy(pcall_record_dat->input,tempBuffer);
	
	memset(tempBuffer,' ',40);
	tempBuffer[0] = '/';
	if((temp = strlen(pcall_record_dat->name)) > 20)
	{
		return -1;	
	}
	strncpy(tempBuffer+ 21 -temp,pcall_record_dat->name,temp + 1);
	strcpy(pcall_record_dat->name,tempBuffer);

	memset(tempBuffer,' ',40);
	tempBuffer[0] = '/';
	if((temp = strlen(pcall_record_dat->relation)) > 39)
	{
		return -1;	
	}
	strncpy(tempBuffer+ 40 -temp,pcall_record_dat->relation,temp + 1);
	strcpy(pcall_record_dat->relation,tempBuffer);
	
	memset(recordBuffer, 0, BUFF_ONE_RECORD_LEN);
	//czn_20170329
	snprintf(recordBuffer,BUFF_ONE_RECORD_LEN,"%02d,%02d,%02x,%04x,%04x,%s,%s,%s,%s",pcall_record_dat->type,pcall_record_dat->subType,pcall_record_dat->property,
		pcall_record_dat->target_id,pcall_record_dat->target_node,pcall_record_dat->input,pcall_record_dat->name,pcall_record_dat->time,pcall_record_dat->relation);

	return  strlen(recordBuffer);
}
int call_record_modify(one_vtk_dat*	precord,int act_type)
{
	int i,record_len;
	CALL_RECORD_DAT_T call_record;
	
	char	recordBuffer[BUFF_ONE_RECORD_LEN];
	bprintf("!!!!!!!!!!call_record_mark_read do1++++++\n");
	pthread_mutex_lock(&Call_Record_Lock);
	if(pcall_record_table == NULL)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	for(i = 0;i < pcall_record_table->record_cnt;i++)
	{
		if(pcall_record_table->precord[i].pdat ==  precord->pdat)
			break;
	}
	
	if(i >= pcall_record_table->record_cnt)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	if(get_callrecord_table_record_items_inner(pcall_record_table,i,&call_record) != 0)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	if(act_type == 0)	//mark read
	{
		if(call_record.property & RECORD_READ)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return 0;
		}
		
		call_record.property |= RECORD_READ;
		
		if(call_record.type == CALL_RECORD && call_record.subType == IN_COMING)
		{
			miss_call_one_have_read();
		}
	}
	else if(act_type == 1)	//delete
	{
		call_record.property |= RECORD_DELETED;
	}
	else if(act_type == 2)	//delete video
	{
		//cao_20181020_s
#if 0
		if(strcmp(call_record.relation,"-") == 0)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return 0;
		}

		Delete_VideoFile(call_record.relation);
		
		strcpy(call_record.relation,"-");

		if(call_record.type == VIDEO_RECORD)
		{
			call_record.property |= RECORD_DELETED;
		}
#else
		
		if(strcmp(call_record.relation,"-") != 0)
		{
			Delete_VideoFile(call_record.relation);
			
			strcpy(call_record.relation,"-");
		}	

		call_record.property |= RECORD_DELETED;
#endif
		//cao_20181020_e
	}

	record_len = call_record_data_convert(&call_record,recordBuffer);

	if(pcall_record_table->precord[i].len != record_len)
	{
		bprintf("!!!!!!!!!len not = len=%d new len =%d :%s\n",pcall_record_table->precord[i].len,record_len,recordBuffer);
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}

	memcpy(pcall_record_table->precord[i].pdat,recordBuffer,record_len);
	bprintf("!!!!!!!!!!call_record_mark_read do2++++++++++\n");
	modify_vtk_table_file_buffer(pcall_record_table,i,1,CALL_RECORD_FILE_NAME);
	sync();
	pthread_mutex_unlock(&Call_Record_Lock);
	return 0;
}

int call_record_delete_video(int num)	
{
	int i,record_len;
	CALL_RECORD_DAT_T call_record;
	int index_buff[20],vd_cnt;
	int index;
	
	char	recordBuffer[BUFF_ONE_RECORD_LEN];

	pthread_mutex_lock(&Call_Record_Lock);
	
	if(pcall_record_table == NULL)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	vd_cnt = 0;
	for(i = 0; i < pcall_record_table->record_cnt;i ++)
	{
		index=RecordTb_CircleWrite_Run.start_index+i;
		if(index>=pcall_record_table->record_cnt)
			index=index-pcall_record_table->record_cnt;
		if(get_callrecord_table_record_items_inner( pcall_record_table, index, &call_record) != 0)
			continue;
		
		if(!(call_record.property & RECORD_DELETED))
		{
			if(strstr(call_record.relation, ".avi") != NULL)
			{
				if(vd_cnt < num&&vd_cnt<20)
				{
					index_buff[vd_cnt++] = index;
				}
				else
				{
					break;
				}
				
			}
		}

	}
	for(i = 0; i < vd_cnt ; i++)
	{
		if(get_callrecord_table_record_items_inner(pcall_record_table, index_buff[i], &call_record) != 0)
		{
			continue;
		}

		bprintf("!!!!!!!!!!call_record_delete_video =%s\n", call_record.relation);
		Delete_VideoFile(call_record.relation);
		strcpy(call_record.relation,"-");
		call_record.property |= RECORD_DELETED;

		record_len = call_record_data_convert(&call_record,recordBuffer);

		if(pcall_record_table->precord[index_buff[i]].len != record_len)
		{
			break;
		}

		memcpy(pcall_record_table->precord[index_buff[i]].pdat,recordBuffer,record_len);
		bprintf("!!!!!!!!!!call_record_delete_video do++++++++++\n");
		modify_vtk_table_file_buffer(pcall_record_table, index_buff[i], 1, CALL_RECORD_FILE_NAME);
	}
	sync();
	pthread_mutex_unlock(&Call_Record_Lock);
	return 0;
}


int call_record_filter(int filter_type)
{
	int i;
	int virtual_index;
	CALL_RECORD_DAT_T record_dat;
	
	pthread_mutex_lock(&Call_Record_Lock);
	if(filter_type == CallRecord_FilterType_IncomingCall)
	{
		if(pcall_record_incoming != NULL)
		{
			release_one_vtk_tabl(pcall_record_incoming);
			pcall_record_incoming = NULL;
		}

		if( pcall_record_table == NULL )
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		// 1. 生成pcall_record_incoming
		pcall_record_incoming	= create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);

		if(pcall_record_incoming == NULL)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		for(i = 0; i < pcall_record_table->record_cnt;i ++)
		{
			if((virtual_index = RecordTb_CircleWrite_Add_IndexTransfer(i)) == -1)
			{
				continue;
			}
			if(get_callrecord_table_record_items_inner( pcall_record_table, virtual_index, &record_dat) != 0)
				continue;
			
			if(!(record_dat.property & RECORD_DELETED))
			{
				if(record_dat.type == CALL_RECORD && record_dat.subType == IN_COMING)
				{
					add_one_vtk_record( pcall_record_incoming, &pcall_record_table->precord[virtual_index]);
				}
			}
		}
		//printf_one_table(pcall_record_incoming);
	}
	
	if(filter_type == CallRecord_FilterType_OutgoingCall)
	{
		if(pcall_record_outgoing != NULL)
		{
			release_one_vtk_tabl(pcall_record_outgoing);
			pcall_record_outgoing = NULL;
		}

		if( pcall_record_table == NULL )
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		// 1. 生成pcall_record_incoming
		pcall_record_outgoing = create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);

		if(pcall_record_outgoing == NULL)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		for(i = 0; i < pcall_record_table->record_cnt;i ++)
		{
			if((virtual_index = RecordTb_CircleWrite_Add_IndexTransfer(i)) == -1)
			{
				continue;
			}
			
			if(get_callrecord_table_record_items_inner( pcall_record_table, virtual_index, &record_dat) != 0)
				continue;
			
			if(!(record_dat.property & RECORD_DELETED))
			{
				if(record_dat.type == CALL_RECORD && record_dat.subType == OUT_GOING)
				{
					add_one_vtk_record( pcall_record_outgoing, &pcall_record_table->precord[virtual_index]);
				}
			}
		}
		//printf_one_table(pcall_record_incoming);
	}

	if(filter_type == CallRecord_FilterType_MissCall)
	{
		if(pcall_record_missed!= NULL)
		{
			release_one_vtk_tabl(pcall_record_missed);
			pcall_record_missed = NULL;
		}

		if( pcall_record_table == NULL )
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		// 1. 生成pcall_record_incoming
		pcall_record_missed = create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);

		if(pcall_record_missed == NULL)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		for(i = 0; i < pcall_record_table->record_cnt;i ++)
		{
			if((virtual_index = RecordTb_CircleWrite_Add_IndexTransfer(i)) == -1)
			{
				continue;
			}
			
			if(get_callrecord_table_record_items_inner( pcall_record_table, virtual_index, &record_dat) != 0)
				continue;
			
			if(!(record_dat.property & RECORD_DELETED))
			{
				if(record_dat.type == CALL_RECORD && (record_dat.property & MISSED))
				{
					add_one_vtk_record( pcall_record_missed, &pcall_record_table->precord[virtual_index]);
				}
			}
		}
		//miss_call_adjust(pcall_record_missed->record_cnt);
		//printf_one_table(pcall_record_incoming);
	}

	if(filter_type == CallRecord_FilterType_UnreadMissCall)
	{
		if( pcall_record_table == NULL )
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		int unread_num = 0;
		

		for(i = 0; i < pcall_record_table->record_cnt;i ++)
		{
			if((virtual_index = RecordTb_CircleWrite_Add_IndexTransfer(i)) == -1)
			{
				continue;
			}
			
			if(get_callrecord_table_record_items_inner( pcall_record_table, virtual_index, &record_dat) != 0)
				continue;
			
			if(!(record_dat.property & RECORD_DELETED))
			{
				if(record_dat.type == CALL_RECORD && (record_dat.property & MISSED) && (!(record_dat.property & RECORD_READ)))
				{
					unread_num ++;
				}
			}
		}
		miss_call_adjust(unread_num);
		//printf_one_table(pcall_record_incoming);
	}

	if(filter_type == CallRecord_FilterType_VideoRecord)
	{
		if(pvideo_record != NULL)
		{
			release_one_vtk_tabl(pvideo_record);
			pvideo_record = NULL;
		}

		if( pcall_record_table == NULL )
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		// 1. 生成pcall_record_incoming
		pvideo_record = create_one_vtk_table(pcall_record_table->keyname_cnt,pcall_record_table->pkeyname,pcall_record_table->pkeyname_len);

		if(pvideo_record == NULL)
		{
			pthread_mutex_unlock(&Call_Record_Lock);
			return -1;
		}

		for(i = 0; i < pcall_record_table->record_cnt;i ++)
		{
			if((virtual_index = RecordTb_CircleWrite_Add_IndexTransfer(i)) == -1)
			{
				continue;
			}
			
			if(get_callrecord_table_record_items_inner( pcall_record_table, virtual_index, &record_dat) != 0)
				continue;
			
			if(!(record_dat.property & RECORD_DELETED))
			{
				if(record_dat.type == VIDEO_RECORD)
				{
					add_one_vtk_record( pvideo_record, &pcall_record_table->precord[virtual_index]);
				}
			}
		}
		//printf_one_table(pcall_record_incoming);
	}
	pthread_mutex_unlock(&Call_Record_Lock);
	return 0;
}
int call_record_get_total_index(one_vtk_dat*	precord)
{
	int i;
	pthread_mutex_lock(&Call_Record_Lock);
	
	if(pcall_record_table == NULL)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	for(i = 0;i < pcall_record_table->record_cnt;i++)
	{
		if(pcall_record_table->precord[i].pdat ==  precord->pdat)
			break;
	}
	
	if(i >= pcall_record_table->record_cnt)
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	pthread_mutex_unlock(&Call_Record_Lock);
	return i;
}

int call_record_get_total_index_inner(one_vtk_dat*	precord)
{
	int i;
	
	if(pcall_record_table == NULL)
	{
		return -1;
	}
	
	for(i = 0;i < pcall_record_table->record_cnt;i++)
	{
		if(pcall_record_table->precord[i].pdat ==  precord->pdat)
			break;
	}
	
	if(i >= pcall_record_table->record_cnt)
	{
		return -1;
	}
	
	return i;
}

int get_callrecord_table_record_items_inner( one_vtk_table* ptable, int index, CALL_RECORD_DAT_T* pcall_record_value )
{
	unsigned char 	input_str[40+1];
	int 			input_str_len,i;	
	one_vtk_dat*	precord;
	
	
	
	precord = get_one_vtk_record_without_keyvalue( ptable, index);
	
	if( precord != NULL )
	{
		if(ptable != pcall_record_table)
		{
			if(call_record_get_total_index_inner(precord) == -1)
			{
				return -1;
			}
		}
		int keyname_index;
		// EVENT_TYPE
		keyname_index = get_keyname_index( ptable, EVENT_TYPE );
		input_str_len = MAX_DEV_NAME_LEN+1;		
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		pcall_record_value->type= atol(input_str);
		
		// EVENT_SUB_TYPE
		keyname_index = get_keyname_index( ptable, EVENT_SUB_TYPE );
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;		
		pcall_record_value->subType= atol(input_str);
		
		// EVENT_PROPERTY 			
		keyname_index = get_keyname_index( ptable, EVENT_PROPERTY );
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;		
		//pcall_record_value->property= atol(input_str);
		sscanf(input_str,"%02x",&pcall_record_value->property);

		// EVENT_TARGET_ID
		keyname_index = get_keyname_index( ptable, EVENT_TARGET_ID );
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		sscanf(input_str,"%04x",&pcall_record_value->target_id);
		//pcall_record_value->target_id= atol(input_str);
		
		// EVENT_TARGET_IP
		keyname_index = get_keyname_index( ptable, EVENT_TARGET_NODE);
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		sscanf(input_str,"%04x",&pcall_record_value->target_node);	//czn_20170329
		//pcall_record_value->target_ip= atol(input_str);

		// EVENT_INPUT
		keyname_index = get_keyname_index( ptable, EVENT_INPUT );
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 8;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->input,&input_str[i + 1],9);
		
		// EVENT_NAME
		keyname_index = get_keyname_index( ptable, EVENT_NAME );
		input_str_len = MAX_DEV_NAME_LEN + 1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 20;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->name,&input_str[i + 1],21);
		//memcpy( pcall_record_value->name,input_str,input_str_len+1);	

		// EVENT_TIME
		keyname_index = get_keyname_index( ptable, EVENT_TIME );
		input_str_len = MAX_DEV_NAME_LEN+1;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		memcpy( pcall_record_value->time,input_str,input_str_len+1);	

		// EVENT_RELATION
		keyname_index = get_keyname_index( ptable, EVENT_RELATION );
		input_str_len = 41;
		get_one_record_string( precord, keyname_index, input_str, &input_str_len );
		input_str[input_str_len] = 0;
		for(i = 0;i < 40;i++)
		{
			if(input_str[i + 1] != ' ')
				break;
		}
		strncpy(pcall_record_value->relation,&input_str[i + 1],41);
		//memcpy( pcall_record_value->relation,input_str,input_str_len+1);	
		return 0;
	}
	else
	{
		return -1;
	}
}

int miss_call_cnt = 0;

void miss_call_register(void)
{
	miss_call_cnt ++;
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_HAVE_MISSCALL);

	API_LedDisplay_MissEvent();

//cao_20170509	ALARM_LED_ON();
}

void miss_call_one_have_read(void)
{
	if(miss_call_cnt > 0)
	{
		miss_call_cnt --;
	}
	
	if(miss_call_cnt == 0)
	{
//cao_20170509		ALARM_LED_OFF();
	}
}

void miss_call_adjust(int adjust_cnt)
{
	miss_call_cnt = adjust_cnt;
	if(miss_call_cnt > 0)
	{
//cao_20170509		ALARM_LED_ON();
		API_LedDisplay_MissEvent();
	}
	else
	{
//cao_20170509		ALARM_LED_OFF();
	}
}

int get_miss_call_nums(void)
{
	return miss_call_cnt;
}
//czn_20170306_e

//czn_20170414_s

int Update_CircleWrite_Record(int index,int start_index)
{
	if(pcall_record_table == NULL)
	{
		return -1;
	}
	
	CALL_RECORD_DAT_T one_record = {0};
	char	recordBuffer[BUFF_ONE_RECORD_LEN];
	int record_len;
	char *precord_dat;
	
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	snprintf( one_record.time,EVENT_TIME_MAX, "%02d/%02d/%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);
	
	one_record.type = TABLE_INFO;
	one_record.target_id = start_index;
	one_record.subType = 0;
	one_record.property = 0;
	one_record.target_node = 0;
	//one_record.time[0] = '-';
	one_record.relation[0] = '-';
	one_record.input[0] = '-';
	one_record.name[0] = '-';
	
	record_len = call_record_data_convert(&one_record,recordBuffer);

	if(pcall_record_table->record_cnt == 0)
	{
		precord_dat = malloc(record_len);
		if(precord_dat == NULL)
		{
			return -1;
		}
		pcall_record_table->record_cnt ++;
		
		pcall_record_table->precord = malloc(pcall_record_table->record_cnt * sizeof(one_vtk_dat));

		if(pcall_record_table->precord == NULL)		//czn_20170303
		{
			free(precord_dat);
			return -1;
		}
		pcall_record_table->precord[pcall_record_table->record_cnt-1].len = record_len;
		pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat = precord_dat;
		memcpy(pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat, recordBuffer, record_len);
		
		save_vtk_table_file_buffer( pcall_record_table, pcall_record_table->record_cnt - 1, 1, CALL_RECORD_FILE_NAME);
		
	}
	else
	{
		if(pcall_record_table->precord[0].len != record_len)
		{
			precord_dat = malloc(record_len);
			if(precord_dat == NULL)
			{
				return -1;
			}
			if(pcall_record_table->precord[0].pdat != NULL)
			{
				free(pcall_record_table->precord[0].pdat);
			}
			pcall_record_table->precord[0].len = record_len;
			pcall_record_table->precord[0].pdat = precord_dat;
		}

		memcpy(pcall_record_table->precord[0].pdat, recordBuffer, record_len);

		modify_vtk_table_file_buffer(pcall_record_table,0,1,CALL_RECORD_FILE_NAME);
	}
	
	RecordTb_CircleWrite_Run.record_index = index;
	RecordTb_CircleWrite_Run.start_index = start_index;

	return 0;
}

int RecordTb_CircleWrite_Init(void)
{
	int i;
	CALL_RECORD_DAT_T one_record;
	
	RecordTb_CircleWrite_Run.record_index = -1;
	RecordTb_CircleWrite_Run.start_index = 0;

	
	if(pcall_record_table == NULL)
	{
		return -1;
	}

	//for(i = 0;i < pcall_record_table->record_cnt;i++)
	if(pcall_record_table->record_cnt > 0)
	{
		if(get_callrecord_table_record_items_inner(pcall_record_table,0,&one_record) == 0)
		{
			if(one_record.type == TABLE_INFO)
			{
				if(one_record.target_id < pcall_record_table->record_cnt)
				{
					RecordTb_CircleWrite_Run.record_index = 0;
					RecordTb_CircleWrite_Run.start_index = one_record.target_id;
					return 0;
				}
			}
			
		}
	}

	Update_CircleWrite_Record(0,1);

	sync();
	
	return 0;
}

int RecordTb_CircleWrite_AddOneRecord(int record_len,char *record_buff)
{
	char *precord_dat;
	one_vtk_dat	*pTempRecord = NULL;
	int i;
	CALL_RECORD_DAT_T one_record;
	
	if(pcall_record_table == NULL || RecordTb_CircleWrite_Run.record_index == -1)
	{
		return -1;
	}

	if(pcall_record_table->record_cnt < MAX_RECORD_ITEMS)
	{	
		if(RecordTb_CircleWrite_Run.start_index != 1)
		{
			Update_CircleWrite_Record(0,1);
		}
		
		precord_dat = malloc(record_len);
		if(precord_dat == NULL)
		{
			return -1;
		}
		
		if(pcall_record_table->record_cnt++ == 0)
		{
			pcall_record_table->precord = NULL;
		}

		pTempRecord = pcall_record_table->precord;
		pcall_record_table->precord = malloc(pcall_record_table->record_cnt * sizeof(one_vtk_dat));

		if(pcall_record_table->precord == NULL)		//czn_20170303
		{
			pcall_record_table->record_cnt--;
			pcall_record_table->precord = pTempRecord;
			free(precord_dat);
			return -1;
		}
		
		if(pTempRecord != NULL)
		{
			for(i = 0; i < pcall_record_table->record_cnt; i++)
			{
				 pcall_record_table->precord[i] = pTempRecord[i];
			}
			free(pTempRecord);
		}
		
		


		pcall_record_table->precord[pcall_record_table->record_cnt-1].len = record_len;
		pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat = precord_dat;
		memcpy(pcall_record_table->precord[pcall_record_table->record_cnt-1].pdat, record_buff, record_len);
		
		save_vtk_table_file_buffer( pcall_record_table, pcall_record_table->record_cnt - 1, 1, CALL_RECORD_FILE_NAME);

		sync();
	}
	else
	{
		if(pcall_record_table->precord[RecordTb_CircleWrite_Run.start_index].len != record_len)
		{
			return -1;
		}
		
		if(get_callrecord_table_record_items_inner(pcall_record_table,RecordTb_CircleWrite_Run.start_index,&one_record) == 0)
		{
			if(strcmp(one_record.relation,"-") != 0)
			{
				Delete_VideoFile(one_record.relation);
			}
		}
		
		memcpy(pcall_record_table->precord[RecordTb_CircleWrite_Run.start_index].pdat,record_buff,record_len);
		bprintf("!!!!!!!!!!call_record_mark_read do2++++++++++\n");
		modify_vtk_table_file_buffer(pcall_record_table,RecordTb_CircleWrite_Run.start_index,1,CALL_RECORD_FILE_NAME);

		if(++RecordTb_CircleWrite_Run.start_index == MAX_RECORD_ITEMS)
		{
			RecordTb_CircleWrite_Run.start_index = 1;
		}

		Update_CircleWrite_Record(0,RecordTb_CircleWrite_Run.start_index);

		sync();
	}

	return 0;
}

int RecordTb_CircleWrite_Add_IndexTransfer(int actual_index)
{
	if(pcall_record_table == NULL || RecordTb_CircleWrite_Run.record_index == -1)
	{
		return -1;
	}
	
	if(actual_index >= (pcall_record_table->record_cnt - 1))
	{
		return -1;
	}
	
	int virtual_index = actual_index + RecordTb_CircleWrite_Run.start_index;

	if(virtual_index >= MAX_RECORD_ITEMS)
	{
		virtual_index = virtual_index - MAX_RECORD_ITEMS + 1;
	}

	if(virtual_index >= pcall_record_table->record_cnt)
	{
		return -1;
	}

	return virtual_index;
}
//czn_20170414_e
int call_record_allmiss_mark_read(void)
{
	int i,record_len;
	CALL_RECORD_DAT_T record_dat;
	char	recordBuffer[BUFF_ONE_RECORD_LEN];
	
	pthread_mutex_lock(&Call_Record_Lock);
	
	if( pcall_record_table == NULL )
	{
		pthread_mutex_unlock(&Call_Record_Lock);
		return -1;
	}
	
	for(i = 0; i < pcall_record_table->record_cnt;i ++)
	{
		if(get_callrecord_table_record_items_inner( pcall_record_table, i, &record_dat) != 0)
			continue;
		
		if(!(record_dat.property & RECORD_DELETED))
		{
			if(record_dat.type == CALL_RECORD && (record_dat.property & MISSED) && (!(record_dat.property & RECORD_READ)))
			{
				record_dat.property |= RECORD_READ;

				record_len = call_record_data_convert(&record_dat,recordBuffer);

				if(pcall_record_table->precord[i].len != record_len)
				{
					continue;
				}

				memcpy(pcall_record_table->precord[i].pdat,recordBuffer,record_len);
				modify_vtk_table_file_buffer(pcall_record_table,i,1,CALL_RECORD_FILE_NAME);
			}
		}
		
	}
	sync();
	
	miss_call_cnt = 0;
	API_LedDisplay_CheckEvent();
	
	pthread_mutex_unlock(&Call_Record_Lock);

	return 0;
}

