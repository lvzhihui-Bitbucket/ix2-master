/**
  ******************************************************************************
  * @file    obj_UnifiedParameterMenu.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_LockProperty.h"	
#include "task_IoServer.h"	
#include "obj_menu_data.h"
#include "MENU_public.h"

LOCK_PROPERTY_T lockProperty;

void GetLockProperty(int ip, LOCK_PROPERTY_T* property)
{
	char display[100];
	char tempBuff[100];
	char *pIndex1, *pIndex2;
	int pos1, pos2;
	int dataIndex, flag, i;

	memset(property, 0, sizeof(LOCK_PROPERTY_T));
	
	if(ip == 0)
	{
		return;
	}
	
	sprintf(display, "<ID=%s><ID=%s>", UNLOCK1_ENABLE, UNLOCK2_ENABLE);
	API_io_server_UDP_to_read_remote(ip, 0xFFFFFFFF, display);
	
	for(dataIndex = 0, i = 0; display[dataIndex] != 0; dataIndex++)
	{
		if(display[dataIndex] == '<')
		{
			flag = 1;
			pos1 = dataIndex+1;
		}
		else if(display[dataIndex] == '>')
		{
			if(flag == 1)
			{
				pos2 = dataIndex;
				
				memcpy(tempBuff, display + pos1, pos2 - pos1);
				tempBuff[pos2 - pos1] = 0;
				
				if((pIndex1 = strstr(tempBuff, "ID=")) != NULL)
				{
					pIndex1 += strlen("ID=");

					pIndex2 = strchr(pIndex1, ',');
					if(pIndex2 == NULL)
					{
						return;
					}

					if((int)(pIndex2-pIndex1) != 4)
					{
						return;
					}
					
					*pIndex2 = 0;
					pIndex2++;

					if(!strcmp(pIndex1, UNLOCK1_ENABLE))
					{
						if((pIndex1 = strstr(pIndex2, "Value=")) != NULL)
						{
							pIndex1 += strlen("Value=");
						
							property->lock1Enable = atoi(pIndex1);
						}
					}
					else if(!strcmp(pIndex1, UNLOCK2_ENABLE))
					{
						if((pIndex1 = strstr(pIndex2, "Value=")) != NULL)
						{
							pIndex1 += strlen("Value=");
						
							property->lock2Enable = atoi(pIndex1);
						}
					}

					i++;
				}
				flag = 0;
			}
		}
	}
}
#if defined(PID_IX850)
#else
void DisplayLockSprite(int ip)
{
	GetLockProperty(ip, &lockProperty);

	if(lockProperty.lock1Enable == 0)
	{
		API_SpriteDisplay_XY(GetIconXY(ICON_209_MonitorUnlock1).x,GetIconXY(ICON_209_MonitorUnlock1).y,SPRITE_Lock1Disable);	
	}
	
	if(lockProperty.lock2Enable == 0)
	{
		API_SpriteDisplay_XY(GetIconXY(ICON_210_MonitorUnlock2).x,GetIconXY(ICON_210_MonitorUnlock2).y,SPRITE_Lock2Disable);	
	}
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
