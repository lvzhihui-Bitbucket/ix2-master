
/**
  ******************************************************************************
  * @file    obj_NamelistUpdate.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.11.15 - 习近平 李克强 张德江 俞正声 刘云山 刘岐山 张高丽
  * @brief   This file contains the functions of obj_NamelistUpdate.c
  ******************************************************************************
  *	@NamelistUpdate接口函数
  *	@NamelistUpdate属性：NamelistUpdate_En 使能标志、NamelistUpdate_To 使能定时器
  *	@namelistUpdate方法：
		API_NamelistUpdate_Start：	启动Namelist更新
		API_NamelistUpdate_End:		更新Namelist完成
		API_NameListUpdate_List:	更新一条数据

  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include "../utility.h"
#include "obj_TableProcess.h"
#include "obj_NamelistUpdate.h"

int 		NamelistUpdate_En;
OS_TIMER 	NamelistUpdate_To;
one_vtk_table* nameListTable;
unsigned char  saveId;

/*******************************************************************************************
 * @fn：	NamelistUpdate_Initial
 *
 * @brief:	Block对象初始化
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void NamelistUpdate_Initial(void)
{
	NamelistUpdate_En = 0;
	nameListTable = NULL;
	
	nameListTable = load_vtk_table_file(NAME_LIST_FILE_NAME);
	
	//初始化timerPeriod，不启动
	OS_CreateTimer(&NamelistUpdate_To,NamelistUpdate_Timeout,2000);
}

/*******************************************************************************************
 * @fn：	NamelistUpdate_Timeout_Process
 *
 * @brief:	Namelist更新期间等待接收有效数据超时处理
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void NamelistUpdate_Timeout(void)
{
	API_NamelistUpdate_End();
}

/*******************************************************************************************
 * @fn：	API_NamelistUpdate_Start
 *
 * @brief:	开启Namelist更新
 * @param:  none
 *
 * @return: 0 - OK，1 - 已在更新中， 2 - 不允许更新
 *******************************************************************************************/
unsigned char API_NamelistUpdate_Start(void)	//zxj_20121120
{
	if( NamelistUpdate_En )
		return 1;
	
	//清除namelist表
	remove(NAME_LIST_FILE_NAME);
	sync();
	
	free_vtk_table_file_buffer(nameListTable);
	
	nameListTable = NULL;
	
	//创建namelist表
	one_vtk_table tempTable;

	tempTable.keyname_cnt = 2;
					
	tempTable.pkeyname = malloc(tempTable.keyname_cnt*sizeof(one_vtk_dat));
	tempTable.pkeyname[0].len = strlen("NAME");
	tempTable.pkeyname[0].pdat = malloc(tempTable.pkeyname[0].len);
	strncpy((char*)tempTable.pkeyname[0].pdat, "NAME", tempTable.pkeyname[0].len);
	
	tempTable.pkeyname[1].len = strlen("ADDR");
	tempTable.pkeyname[1].pdat = malloc(tempTable.pkeyname[1].len);
	strncpy((char*)tempTable.pkeyname[1].pdat, "ADDR", tempTable.pkeyname[1].len);

	tempTable.pkeyname_len = malloc(tempTable.keyname_cnt*sizeof(int));
	tempTable.pkeyname_len[0] = 40;
	tempTable.pkeyname_len[1] = 40;
			
	nameListTable = create_one_vtk_table(tempTable.keyname_cnt, tempTable.pkeyname, tempTable.pkeyname_len);

	if(nameListTable == NULL)
	{
		return 1;
	}
	
	// 启动更新模式
	NamelistUpdate_En = 1;
	saveId = 0xFF;
	
	// 触发计时器
	OS_RetriggerTimer(&NamelistUpdate_To);	
	
	// 更新提示信息
	API_NameListUpdate_Start_UI();
	
	return 0;	
}

/*******************************************************************************************
 * @fn：	API_NamelistUpdate_End
 *
 * @brief:	更新你Namelist结束
 * @param:  none
 *
 * @return: 0 - OK，1 - 更新错误
 *******************************************************************************************/
unsigned char API_NamelistUpdate_End(void)
{
	if( !NamelistUpdate_En )
		return 1;

	save_vtk_table_file_buffer(nameListTable, 0, nameListTable->record_cnt, NAME_LIST_FILE_NAME);
	
	// 关闭更新模式
	NamelistUpdate_En = 0;
	saveId = 0xFF;
	
	//停止timer
	OS_StopTimer(&NamelistUpdate_To);
	
	// 更新提示信息
	API_NameListUpdate_End_UI();
	
	return 0;	
}

/*******************************************************************************************
 * @fn：	API_NameListUpdate_List
 *
 * @brief:	更新Namelist数据
 *
 * @param:  id - 分机地址
 * @param:  len - 更新的数据长度
 * @param:  ptrDat - 更新的数据指针
 *
 * @return: 0 - OK，1 - 更新错误
 *******************************************************************************************/
unsigned char API_NameListUpdate_List(unsigned char id, unsigned char len, unsigned char *ptrDat)	//zxj_20121121
{
	if( !NamelistUpdate_En )
		return 1;
	
	if(nameListTable == NULL)
		return 1;
	
	if(saveId == id)
		return 1;
	
	one_vtk_dat 	oneRecord;
	unsigned char	tempBuffer2[50];
	unsigned char	tempBuffer1[50];

	saveId = id;
	
	memcpy(tempBuffer1, ptrDat, len);
	tempBuffer1[len] = 0;
	
	snprintf(tempBuffer2, 50, "%s,%02d", tempBuffer1+1, tempBuffer1[0] - 0x30);

	oneRecord.len = strlen(tempBuffer2);
	oneRecord.pdat = malloc(oneRecord.len);
	memcpy(oneRecord.pdat, tempBuffer2, oneRecord.len);
	
	add_one_vtk_record(nameListTable, &oneRecord);

	// 触发计时器
	OS_RetriggerTimer(&NamelistUpdate_To);
		
	// 更新提示信息
	API_NameListUpdate_List_UI();
	
	return 0;
}


/*******************************************************************************************
 * @fn：	API_NameListUpdate_Start_UI
 *
 * @brief:	启动Namelist更新的UI处理
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void API_NameListUpdate_Start_UI(void)
{	
	//API_LED_MEMO_ON();
}

/*******************************************************************************************
 * @fn：	API_NameListUpdate_End_UI
 *
 * @brief:	Namelist更新结束的UI处理
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void API_NameListUpdate_End_UI(void)
{	
	//API_LED_MEMO_OFF();
}

/*******************************************************************************************
 * @fn：	API_NameListUpdate_List_UI
 *
 * @brief:	Namelist数据更新的UI处理
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void API_NameListUpdate_List_UI(void)	//will add 
{
	;
}


