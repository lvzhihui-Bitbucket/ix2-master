/**
  ******************************************************************************
  * @file    task_IoServer.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef VDP_IO_SERVER_H
#define VDP_IO_SERVER_H

#include "task_survey.h"
#include "obj_IPDeviceTable.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "obj_OtherParameterSetting.h"


// Define Task Vars and Structures----------------------------------------------

#define IO_SWITCH_ENABLE			0
#define IO_SWITCH_DISABLE			1

#define DATA_TYPE_MUN				0
#define DATA_TYPE_STR				1

#define IO_WRITE_DEFAULT_VALUE		"Default_Value"

#define IO_DATA_LENGTH				500

#define IP_Address					"1000"					//IP��ַ
#define BD_RM_MS_NUM				"1001"					//�豸��ַ
#define MY_GLOBAL_NUM				"1002"					//ȫ�ַ���
#define MY_LOCAL_NUM				"1003"					//��������
#define FWVersion					"1004"					//�����汾
#define HWVersion					"1005"					//Ӳ���汾
#define UpgradeTime					"1006"					//����ʱ��
#define UpgradeCode					"1007"					//���´���
#define UpTime						"1008"					//����ʱ��
#define SerialNumber				"1009"					//���к�
#define ParaDeviceType				"1010"					//�豸����
#define DeviceModel					"1011"					//
#define AreaCode					"1012"					//��λ������
#define TransferState				"1013"					//����״̬
#define MacAddress					"1014"					//MAC��ַ
#define SubnetMask					"1015"					//��������
#define DefaultRoute				"1016"					//����



#define DAY_CALL_VOLUME				"1017"					//��������
#define NIGHT_CALL_VOLUME			"1018"					//Ŀǰ����
#define DAY_NIGHIT_DELIMIT			"1019"					//Ŀǰ����
#define VIDEO_DISPLAY_BRIGHT		"1020"					//��Ƶ��ʾ����
#define VIDEO_DISPLAY_COLOR			"1021"					//��Ƶ��ʾɫ��
#define MONITOR_TIME_LIMIT			"1022"					//����ʱ��
#define TIME_FORMAT					"1023"					//ʱ���ʽ
#define DATE_FORMAT					"1024"					//���ڸ�ʽ
#define RTC_SERVER_ENABLE			"1025"					//Ŀǰ����
#define TALK_VOLUME                 "1026"                  //Ŀǰ����
#define VIDEO_DISPLAY_CONTRAST		"1027"					//��Ƶ��ʾ�Աȶ�
#define CALL_TUNE_COUNT_LIMIT		"1028"					//Ŀǰ����
#define CALL_TUNE_TIME_LIMIT		"1029"					//������������
#define CALL_TUNE_CONTROL_PRIORITY	"1030"					//Ŀǰ����

#define DS_TUNE_SELECT				"1031"					//Ŀǰ����
#define CDS_TUNE_SELECT				"1032"					//Ŀǰ����
#define OS_TUNE_SELECT				"1033"					//Ŀǰ����
#define MSG_TUNE_SELECT				"1034"					//Ŀǰ����
#define DOORBELL_TUNE_SELECT		"1035"					//Ŀǰ����
#define INTERCOM_TUNE_SELECT		"1036"					//Ŀǰ����
#define INNERCALL_TUNE_SELECT		"1037"					//Ŀǰ����
#define ALARM_TUNE_SELECT			"1038"					//Ŀǰ����
#define MASTER_SLAVE_SET			"1039"					//Ŀǰ����
#define CallScene_SET				"1040"					//Ŀǰ����
#define IntercomEnable				"1041"					//Ŀǰ����
#define MissedCall					"1042"					//Ŀǰ����
#define MainShortcut1Select			"1043"					//Ŀǰ����
#define MainShortcut2Select			"1044"					//Ŀǰ����
#define MainShortcut3Select			"1045"					//Ŀǰ����
#define MainShortcut4Select			"1046"					//Ŀǰ����
#define FISH_EYE_CONTROL			"1047"					//Ŀǰ����
#define TimeZone					"1048"					//ʱ��
#define TimeAutoUpdateEnable		"1049"					//ʱ���Զ�����
#define AutoRecordEnable			"1050"					//Ŀǰ����
#define DivertTime					"1051"					//Ŀǰ����
#define MasterBroadcastTime			"1052"					//Ŀǰ����
#define MicVolumeSet				"1053"					//��˷�����
#define SpeakVolumeSet				"1054"					//��������
#define VedioResolutionSet			"1055"					//Ŀǰ����
#define DHCP_ENABLE					"1056"					//DHCPʹ��
#define SYSTEM_TYPE					"1057"					//ϵͳ���ͣ�����/С��
#define MY_NAME						"1058"					//��������
#define TIME_SERVER					"1059"					//ʱ�������
#define AUTO_REBOOT					"1060"					//Ŀǰ����
#define DIVERT_AUTO_MUTE			"1061"					//Ŀǰ����
#define DivertVedioResolutionSet	"1062"					//Ŀǰ����
#define IPC_DHCP_Enable				"1063"					//IPC��DHCPʹ��
#define VIDEO_FORMAT				"1064"					//Ŀǰ����
#define FWUPGRADE_SER_SELECT		"1065"					//�̼����·�����ѡ��
#define FWUPGRADE_OTHER_SER			"1066"					//�̼����·�����
#define FWUPGRADE_FW_CODE			"1067"					//�̼����´���

#define SIP_SERVER					"1068"					//SIP������
#define SIP_PORT					"1069"					//SIP�˿�
#define SIP_DIVERT					"1070"					//SIPת���ʺ�
#define SIP_ACCOUNT					"1071"					//SIP�ʺ�
#define SIP_PASSWORD				"1072"					//SIP�ʺ�����
#define SIP_MON_CODE				"1073"					//SIP������
#define SIP_CALL_CODE				"1074"					//SIP������
#define SIP_DIV_PWD					"1075"					//SIPת���ʺ�����

#define VIDEO_DISPLAY_SCENE			"1076"					//Ŀǰ����										
#define UNLOCK1_TIP_DISPLAY_TIME	"1077"					//Ŀǰ����									
#define ManagePassword				"1078"					//��������
#define VOICE_LANGUAGE_SELECT       "1079"					//������ʾ����ѡ��
#define VOICE_VOLUME    			"1080"					//��������
#define VOICE_PLAY_MODE				"1081"					//��������ģʽ��1û��������Ĭ�ϣ�0û����������
#define CALL_FAIL_INEXISTENT_IOID	"1082"					//���������ļ�ѡ��
#define CALL_FAIL_DND_IOID			"1083"					//���������ļ�ѡ��	
#define CALL_SUCC_IOID				"1084"					//���������ļ�ѡ��	
#define CALL_TALK_IOID				"1085"					//���������ļ�ѡ��	
#define CALL_CLOSE_IOID				"1086"					//���������ļ�ѡ��	
#define SYS_BUSY_IOID				"1087"					//���������ļ�ѡ��	
#define UNLOCK_COMMAND_IOID			"1088"					//���������ļ�ѡ��	
#define UNLOCK_BUTTON_IOID			"1089"					//���������ļ�ѡ��	
#define VOLUME_ADJUST_IOID			"1090"					//���������ļ�ѡ��	

#define SIP_ENABLE					"1091"					//Ŀǰ����
#define SIP_ACCOUNT_TYPE			"1092"					//Ŀǰ����
#define Autoip_IPMask				"1093"					//AutoIp����
#define Autoip_IPGateway			"1094"					//AutoIp����
#define UnlockPassword				"1095"					//��������
#define CODE_UNLOCK_DISABLE			"1096"					//
#define USER_QUERY_ENABLE			"1097"					//
#define REMOTE_CHANGE_ENABLE		"1098"					//
#define PROTECTION_ENABLE			"1099"					//
#define PRIVATE_UNLOCK_CODE			"1100"					//Ŀǰ����	
#define DEVICE_DISP_NAME_ONLY		"1101"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_SIMPLIFIED		"1102"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_USE_G_L_NUM		"1103"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_MS		"1104"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_IM		"1105"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_DS		"1106"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_OS		"1107"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_GL		"1108"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_AC		"1109"					//��ʾ�豸��Ϣ����
#define DEVICE_DISP_PREFIX_COMMON	"1110"					//��ʾ�豸��Ϣ����
#define FAST_MON_KEY_SHORTCUT		"1111"					//talk������ݷ�ʽ����

//czn_20190601_s		
#define Autoip_IPSegment				"1112"				//AutoIp������
#define Screen_Contrast_Mode            "1113"				//��Ļ�Ա�ģʽ		
#define NoInputStartCallTime            "1114"				//û�����Զ�����ʱ��
#define NoInputStartCallLeastLen        "1115"				//û�����Զ�����
#define InputImmediatelyStartCallLen    "1116"				//�Զ��������볤��

#define UNLOCK1_ENABLE					"1117"
#define UNLOCK1_TYPE					"1118"	
#define UNLOCK1_MODE					"1119"	
#define UNLOCK1_EXT_ADDR				"1120"
#define UNLOCK1_EXT_CH					"1121"
#define UNLOCK1_TIMING					"1122"
#define UNLOCK1_RETRIG					"1123"
#define UNLOCK1_PROTECTION_TIMING		"1124"

#define UNLOCK2_ENABLE					"1125"
#define UNLOCK2_TYPE					"1126"
#define UNLOCK2_MODE					"1127"
#define UNLOCK2_EXT_ADDR				"1128"
#define UNLOCK2_EXT_CH					"1129"
#define UNLOCK2_TIMING					"1130"
#define UNLOCK2_RETRIG					"1131"
#define UNLOCK2_PROTECTION_TIMING		"1132"
#define RebootIfNetErrorTimeout			"1133"

#define ParameterGroup1Part1			"1134"
#define ParameterGroup1Part2			"1135"
#define ParameterGroup1Part3			"1136"
#define ParameterGroup2Part1			"1137"
#define ParameterGroup2Part2			"1138"
#define ParameterGroup2Part3			"1139"
#define ParameterGroup3Part1			"1130"
#define ParameterGroup3Part2			"1141"
#define ParameterGroup3Part3			"1142"
#define ParameterGroup4Part1			"1143"
#define ParameterGroup4Part2			"1144"
#define ParameterGroup4Part3			"1145"
#define ParameterGroup5Part1			"1146"
#define ParameterGroup5Part2			"1147"
#define ParameterGroup5Part3			"1148"
#define MENU_LANGUAGE_SELECT       		"1149"			//�˵�����ѡ��

#define BAR_TITLE_DEFAULT				"1150"			//Ŀǰ����	
#define InstallerPassword				"1151"			//Ŀǰ����	
#define UnitIntercom					"1152"			//���� ����Ԫ��Intercom
#define OverUnitIntercom				"1153"			//���� �絥Ԫ��Intercom
#define OverUnitMonitor					"1154"			//���� ȫ��Χ����
#define DivertWithPTTAndJpg				"1155"			//
#define ExtUnitPwdEnable				"1156"			//ExtUnit��������
#define AutoCloseAfterUnlockEnable		"1157"			//�����Զ��һ�


#define GerneralPwdEnable				"1158"			//Gerneral��������
#define ManagePwdEnable					"1159"			//Manage��������
#define SipChangePwdEnable				"1160"			//Sipconfig��������
#define SipConfirmPwdEnable				"1161"			//SipConfirm��������
#define InstallerRemoteDis				"1162"			//InstallerԶ��ѯ�ʽ�ֹ
#define InstallerPwdPlace					"1163"			//Installer��������λ��
#define GerneralPwdPlace					"1164"			//Gernel��������λ��
#define ManagePwdPlace					"1165"			//Manage��������λ��
#define ExtUnitPwdPlace					"1166"			//ExtUnit��������λ��
#define InstallerPwdAlways				"1167"			//ExtUnit��������λ��
#define ZportMode						"1168"			//stm8 Z1-Z4��ģʽ����

#define VIDEO_PROXY_SET					"1169"
#define IX_BUILDER_ACCOUNT1				"1170"
#define IX_BUILDER_PWD1					"1171"
#define IX_BUILDER_ACCOUNT2				"1172"
#define IX_BUILDER_PWD2					"1173"

#define ShortcutDispColor				"1174"				//white 0xffff
#define IconText1Color					"1175"
#define IconText2Color					"1176"
#define DisplayTitleColor				"1177"	//DISPLAY_TITLE_COLOR
#define DisplayListColor				"1178"	//DISPLAY_LIST_COLOR
#define DisplayStateColor				"1179"	//DISPLAY_STATE_COLOR
#define CallMenuDispNameColor			"1180"	//CallMenuDisp_Name_Color

#define MAINMENU_MODE					"1181"			//���˵�ģʽ
#define MAINMENU_BUILDINGMAP_HIGH			"1182"
#define MAINMENU_BUILDINGTEXT_HIGH			"1183"			//
#define MAINMENU_CODEUNLOCKTEXT_HIGH		"1184"
#define MAINMENU_TEXT_COLOR				"1185"
#define DIALMENU_MODE					"1186"			//DIAL�˵�ģʽ
#define DIALMENU_ACT_SPRITEID				"1187"	
#define KP_BACKSPACE_MODE				"1188"
#define SCREEN_DISPLAY_BRIGHT			"1189"		//�����ȵ���
#define RAIN_COVER_ENABLE				"1190"
#define EXT_MODULE_CONFIG				"1191"		//�ⲿģ������

#define GerneralMenuMode					"1192"		//

#define TEST_MIC						"1193"		//�ֻ�����mic����
#define TEST_SPK						"1194"		//�ֻ�����mic����
#define TEST_MUSIC						"1195"		//�ֻ�������������
#define TEST_AUTO_LOCK					"1196"		//�ֻ������Զ�����
#define TEST_AUTO_TALK					"1197"		//�ֻ������Զ�ժ��
#define TEST_MODE						"1198"		//�ֻ�����ģʽ����

#define FWVersionAndVerify				"1199"		//��ϸ�İ汾��Ϣ��FWУ��
#define ParameterVersion				"1200"		//�������ļ��汾

#define LIFT_CTRL_ENABLE				"1201"
#define DEVICE_FLOOR					"1202"
#define DS_LIFT_CTRL_ENABLE				"1203"
#define LIFT_CONTROLLER_IN_LIFT_ENABLE	"1204"
#define LIFT_CONTROLLER_RELAY_TIME		"1205"
#define LIFT_CONTROLLER_RELAY_MODE		"1206"
#define LIFT_CONTROLLER_FLOOR_TIME		"1207"

#define LIFT_CONTROLLER1_RELAY1_FLOOR	"1208"
#define LIFT_CONTROLLER1_RELAY2_FLOOR	"1209"
#define LIFT_CONTROLLER1_RELAY3_FLOOR	"1210"
#define LIFT_CONTROLLER1_RELAY4_FLOOR	"1211"
#define LIFT_CONTROLLER1_RELAY5_FLOOR	"1212"
#define LIFT_CONTROLLER1_RELAY6_FLOOR	"1213"
#define LIFT_CONTROLLER1_RELAY7_FLOOR	"1214"
#define LIFT_CONTROLLER1_RELAY8_FLOOR	"1215"
#define LIFT_CONTROLLER1_RELAY9_FLOOR	"1216"
#define LIFT_CONTROLLER1_RELAY10_FLOOR	"1217"
#define LIFT_CONTROLLER1_RELAY11_FLOOR	"1218"
#define LIFT_CONTROLLER1_RELAY12_FLOOR	"1219"
#define LIFT_CONTROLLER1_RELAY13_FLOOR	"1220"
#define LIFT_CONTROLLER1_RELAY14_FLOOR	"1221"
#define LIFT_CONTROLLER2_RELAY1_FLOOR	"1222"
#define LIFT_CONTROLLER2_RELAY2_FLOOR	"1223"
#define LIFT_CONTROLLER2_RELAY3_FLOOR	"1224"
#define LIFT_CONTROLLER2_RELAY4_FLOOR	"1225"
#define LIFT_CONTROLLER2_RELAY5_FLOOR	"1226"
#define LIFT_CONTROLLER2_RELAY6_FLOOR	"1227"
#define LIFT_CONTROLLER2_RELAY7_FLOOR	"1228"
#define LIFT_CONTROLLER2_RELAY8_FLOOR	"1229"
#define LIFT_CONTROLLER2_RELAY9_FLOOR	"1230"
#define LIFT_CONTROLLER2_RELAY10_FLOOR	"1231"
#define LIFT_CONTROLLER2_RELAY11_FLOOR	"1232"
#define LIFT_CONTROLLER2_RELAY12_FLOOR	"1233"
#define LIFT_CONTROLLER2_RELAY13_FLOOR	"1234"
#define LIFT_CONTROLLER2_RELAY14_FLOOR	"1235"

#define ExtRingMode						"1240"
#define ManagerMenuMode						"1241"
#define SipCfgMenuMode						"1242"
#define EditUserNameDis						"1243"
#define GLListDis							"1244"
#define MesgTestSnDisp						"1245"
#define InstallerMenuMode						"1246"
#define ParaManaMenuMode					"1247"
#define AutoUnlockIo							"1248"
#define AutoUnlockEditEn						"1249"
#define AutoCloseScreenTime					"1250"

#define CallTransferPara						"1251"
#define CallTransferRunningPara				"1252"
#define CallTransferPermitPara					"1253"

#define CallNumMenuMode						"1254"
#define ManagerEditUserNameDis				"1255"
#define AboutMenuMode						"1256"
#define KeyBeepDis							"1257"
#define MonitorDsDis							"1258"
#define VideoProxyMenuMode						"1259"

#define EditNameKeyPad					"1260"
#define MLKP_LANGUAGE_SELECT       		"1261"

#define IXRLC_RelayOption					"1500"
#define IXRLC_RelayMap						"1501"

#define Alarming_Setting					"1601"

#define MIC_VOLUME_GAIN					"1700"		// 
#define SPEAK_VOLUME_GAIN				"1701"		//
#define AEC_CTRL_ENABLE					"1702"		// 
#define AGC_CTRL_ENABLE					"1703"		//
#define NR_CTRL_ENABLE					"1704"		//


#define PANEL_TYPE							"1705"
#define WIFI_SWITCH							"1706"		// wifi����
#define SIP_NetworkSetting					"1707"
#define IP_DeviceSeletNetwork				"1708"		//1, 2, 3-- WLAN/LAN/AUTO
#define IxDeviceRemoteSwitch				"1709"		//IxDeviceԶ��������������



#define IX_NET_MODE							"1710"
#define Divert_Ability						"1711"
#define WIFI_SSID_PARA						"1712"
#define PIP_SHOW_POS						"1713"



#define IO_Group							"1714"
#define NET_Timeout_Group					"1715"
#define LAN_Search_Timeout					"1716"
#define WLAN_Search_Timeout					"1717"
#define CallTuneMenuMode					"1718"
#define CallSceneMenuMode					"1719"
#define RestoreSelMenuMode					"1720"
#define OnsiteToolsMenuMode					"1721"
#define DoorBellVideoSource					"1722"
#define InnerBroadcastEn							"1723"
#define BabyRoomNums							"1724"
#define BabyRoomEn							"1725"	
#define AutoUnlockStartTime					"1726"
#define AutoUnlockEndTime					"1727"

#define MicVolumeSet_APP				"1801"					//��˷�����
#define SpeakVolumeSet_APP				"1802"					//��������
#define MIC_VOLUME_GAIN_APP				"1803"		// 
#define SPEAK_VOLUME_GAIN_APP			"1804"		//
#define MicVolumeSet_DX				"1805"					//��˷�����
#define SpeakVolumeSet_DX				"1806"					//��������

#define DX_IM_ADDR					"2000"
#define DS1_TUNE_SELECT				"2001"
#define DS2_TUNE_SELECT				"2002"
#define DS3_TUNE_SELECT				"2003"
#define DS4_TUNE_SELECT				"2004"
#define GUARTCALL_TUNE_SELECT		"2005"
#define DIVERT_WITH_LOCAL			"2006"
#define PIP_Disable					"2007"
#define DX_AREA_CODE				"2008"
#define APP_MON_TALK_EN				"2009"
#define CustomerizedName			"2010"
#define AppVideoRes			"2011"



#define IP_Address_INT					1000

#define FWVersion_INT					1004
#define HWVersion_INT					1005
#define UpTime_INT						1008
#define SerialNumber_INT				1009
#define MacAddress_INT					1014
#define SubnetMask_INT					1015
#define DefaultRoute_INT				1016

#define SIP_SERVER_INT					1068
#define SIP_PORT_INT					1069
#define SIP_DIVERT_INT					1070
#define SIP_ACCOUNT_INT					1071
#define SIP_PASSWORD_INT				1072
#define SIP_MON_CODE_INT				1073
#define SIP_CALL_CODE_INT				1074
#define SIP_DIV_PWD_INT					1075
#define VIDEO_PROXY_SET_INT				1169
#define EXT_MODULE_CONFIG_INT			1191
#define Divert_Ability_INT				1711
#define WIFI_SSID_PARA_INT					1712

#define CallTransferPara_INT				1251
#define CallTransferRunningPara_INT		1252
#define CallTransferPermitPara_INT			1253

// Define task interface-------------------------------------------------------------

#pragma pack(1)

typedef struct 
{
	char 	property_id[5];
	uint8 	result;
	uint8 	ptr_type;
	uint8 	editType;
	uint8 	source;
	uint8 	readWrite;
	uint8 	effective;
	uint8	common;
	uint8	ptr_data[IO_DATA_LENGTH+1];

} IoServer_STRUCT ;

typedef struct 
{
	VDP_MSG_HEAD	head;
	IoServer_STRUCT	msg_dat;
} io_server_type;

typedef struct 
{
	VDP_MSG_HEAD	head;
	int				targetIp;
	int				cmd;
	int				id;
	int				addr;
	IoServer_STRUCT	msg_dat;
} IoRemote_S ;

extern uint8 		flag_switch_io;

#pragma pack()


#define IO_SERVER_READ_LOCAL								1
#define IO_SERVER_WRITE_LOCAL								2

#define IO_SERVER_UDP_BE_READ_BY_REMOTE					3
#define IO_SERVER_UDP_BE_WRITE_BY_REMOTE				4
#define IO_SERVER_UDP_TO_READ_REMOTE					5
#define IO_SERVER_UDP_TO_WRITE_REMOTE					6
#define IO_SERVER_SAVE_DATA_TO_FILE						9
#define IO_SERVER_UDP_TO_INVITE_FACTORY_DEFAULT		12
#define IO_SERVER_UDP_BE_INVITE_FACTORY_DEFAULT		13

#define IO_SERVER_BE_READ_BY_DT_DEVICE					14
#define IO_SERVER_BE_WRITE_BY_DT_DEVICE					15



#define PROPERTY_VALUE_INNER_TABLE_OPERATE		30		// inner������
#define PROPERTY_VALUE_UART_TABLE_OPERATE		31		// uart������

#define IO_SERVER_READ_DEFAULT_LOCAL						32

#define IO_SERVER_FACTORY_DEFAULT_BY_LEVEL_LOCAL			33
#define IO_SERVER_FACTORY_DEFAULT_BY_ID_LOCAL				34
#define IO_SERVER_JUDGE_RANGE_LOCAL							35
#define IO_SERVER_READ_PROPERTY_LOCAL						36
#define IO_SERVER_READ_GROUP_LOCAL							37

#define IO_SERVER_UDP_TO_WRITE_DEFAULT_REMOTE				38
#define IO_SERVER_UDP_BE_WRITE_DEFAULT_REMOTE				39

#define IO_SERVER_RESTORE_DEFAULTS							40


// lzh_20160912_s
#define UDP_IO_SERVER_CMD_R					0x1101			// IO server ͨ��ָ�� - ��
#define UDP_IO_SERVER_CMD_R_RSP				0x1181			// IO server ͨ��ָ�� - ��Ӧ��
#define UDP_IO_SERVER_CMD_W					0x1102			// IO server ͨ��ָ�� - ��
#define UDP_IO_SERVER_CMD_W_RSP				0x1182			// IO server ͨ��ָ�� - ��Ӧ��
#define UDP_IO_SERVER_CMD_DEFAULT			0x1103			// IO server ͨ��ָ�� - �ָ���������
#define UDP_IO_SERVER_CMD_DEFAULT_RSP		0x1183			// IO server ͨ��ָ�� - �ָ���������Ӧ��
#define UDP_IO_SERVER_CMD_ONE_DEFAULT		0x1104			// IO server ͨ��ָ�� - �ָ�����������������
#define UDP_IO_SERVER_CMD_ONE_DEFAULT_RSP	0x1184			// IO server ͨ��ָ�� - �ָ�����������������Ӧ��
// lzh_20160912_e



//tab_type
#define GLOBAL_DEVICE_TABLE		0		// 	pdevice_map_table
#define GLOBAL_IP_TABLE			1		// 	pip_map_table
#define GL_DEVICE_TABLE			2		//   pvtk_gl_device_table
#define CDS_DEVICE_TABLE		3		// 	pvtk_cds_device_table
#define IM_DEVICE_TABLE			4		// 	pvtk_im_device_table
#define DS_DEVICE_TABLE			5		// 	pvtk_ds_device_table
#define TEMP_DEVICE_TABLE		6		//	pvtk_temp_device_table
//opt_cmd
#define TABLE_OPT_CMD_CREATE_TEMP_TABLE					0		//  ����search_vtk_table_with_key_value �õ���ʱ��
#define TABLE_OPT_CMD_RELEASE_TEMP_TABLE				1		//  ����release_one_vtk_tabl �ͷ���ʱ��
#define TABLE_OPT_CMD_GET_TABLE_BASE_INFO				2		//  �õ����ĳ��ȵ���Ϣ

#define TABLE_OPT_CMD_GET_VALUE_BY_INPUT				3		//  ��ip_input,dev_input�õ���Ҫ������: name, ipnode_id, ip, logic_addr

typedef struct
{
	char			tab_type;
	char			opt_cmd;
	unsigned char	ip_input[MAX_IP_STR_LEN+1];
	unsigned char 	dev_input[MAX_DEV_STR_LEN+1];
} REMOTE_TABLE_CMD_REQ;

#define REMOTE_TABLE_CMD_RSP_STATE_TAB_ERR		-2
#define REMOTE_TABLE_CMD_RSP_STATE_TAB_IGNORE	-1
#define REMOTE_TABLE_CMD_RSP_STATE_ERR			0
#define REMOTE_TABLE_CMD_RSP_STATE_OK			1
#define REMOTE_TABLE_CMD_RSP_STATE_OK2			2	// ���ƥ�������ظ�

typedef struct
{
	char			tab_type;
	char			opt_cmd;
	char			ip_state;		//  -2 /IP������-1/��ƥ���0 /��ƥ������ 1-n/ ƥ��ok��ƥ��ĸ���
	char			dev_state;		//  -2 /DEV������-1/��ƥ���0 /��ƥ������ 1-n/ ƥ��ok��ƥ��ĸ���
	ip_table_t		ip_table_value;
	dev_table_t		dev_table_value;
} REMOTE_TABLE_CMD_RSP;

typedef struct 
{
	VDP_MSG_HEAD			head;
	REMOTE_TABLE_CMD_REQ	msg_dat;
} REMOTE_TABLE_CMD_REQ_IF;

typedef struct 
{
	VDP_MSG_HEAD			head;
	REMOTE_TABLE_CMD_RSP	msg_dat;
} REMOTE_TABLE_CMD_RSP_IF;

typedef struct
{
	int editType;
	int source;
	int readWrite;
	int effective;
} ParaProperty_t;


#pragma pack()

void vtk_TaskInit_io_server(int priority);
int API_Event_IoServer_InnerRead_All(char* property_id, char *ptr_data );
int API_Event_IoServer_InnerWrite_All(char* property_id, char *ptr_data );
int API_Event_IoServer_InnerRead_Property(char* property_id, ParaProperty_t *property);



int API_io_server_UDP_to_read_remote(int targetIp, int addr, char *ptr_data);
int API_io_server_UDP_to_write_remote(int targetIp, int addr, char *ptr_data);

int API_io_server_save_data_file(void);
int API_io_server_UDP_to_invite_factoryDefault(int targetIp, uint16 addr);
int API_IoServer_BeResponse(uint16 device_address, uint16 property_id, uint8 no_of_elem, uint8 start_index, uint8* ptr_data) ;

#define API_Event_IoServer_Factory_Default() 		API_io_server_UDP_to_invite_factoryDefault(0xFFFFFFFF, 0xFFFF)
			
int API_io_server_UDP_CMD_process(char* pData, int len);



int io_server_table_cmd_process( REMOTE_TABLE_CMD_REQ* ptable_cmd_req, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp );

// �ڲ�����������ӿ�
int API_io_server_inner_table_cmd_req( REMOTE_TABLE_CMD_REQ* ptable_cmd_req, REMOTE_TABLE_CMD_RSP* ptable_cmd_rsp );

#endif

