/**
  ******************************************************************************
  * @file    obj_UnifiedParameterMenu.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.11.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<errno.h>

#include "obj_TableProcess.h"
#include "obj_CustomerizedIoTable.h"	
#include "vdp_IoServer_State.h"

one_vtk_table*	pCustomerizedIoTable;


const unsigned char* CustomerizedIo_TAB[] = 
{
	PARA_ID,
	VALUE,
};

const int CustomerizedIo_KEYNAME_CNT = sizeof(CustomerizedIo_TAB)/sizeof(unsigned char*);

int GetCustomerizedIoKeynameIndex(const char* keyName)
{
	int i;
	for(i = 0; i < CustomerizedIo_KEYNAME_CNT; i++)
	{
		if(!strcmp(keyName, CustomerizedIo_TAB[i]))
		{
			break;
		}
	}

	return i;
}


int CustomerizedIoTableInit(void)
{
	pCustomerizedIoTable = load_vtk_table_file(CUSTOMERIAED_IO_TABLE);
}

int GetCustomerizedIoRecordCnt(void)
{
	if(pCustomerizedIoTable == NULL)
	{
		return 0;
	}

	return pCustomerizedIoTable->record_cnt;
}


int GetCustomerizedIoRecord(int index, CUSTOMERIAED_IO_T* pRecord)
{
	one_vtk_dat* oneRecord;
	int valueLen;
	
	memset(pRecord, 0, sizeof(CUSTOMERIAED_IO_T));

	oneRecord = get_one_vtk_record_without_keyvalue(pCustomerizedIoTable, index);
	if(oneRecord == NULL)
	{
		return -1;
	}

	//读取ID
	valueLen = ID_LEN;
	get_one_record_string(oneRecord, GetCustomerizedIoKeynameIndex(PARA_ID), pRecord->paraId, &valueLen);
	pRecord->paraId[valueLen] = 0;

	//读取VALUE
	valueLen = VALUE_LEN;
	get_one_record_string(oneRecord, GetCustomerizedIoKeynameIndex(VALUE), pRecord->value, &valueLen);
	pRecord->value[valueLen] = 0;
	
	return 0;
}

int WriteIoTableDefaultValue(void)
{
	one_vtk_dat* oneRecord;
	CUSTOMERIAED_IO_T record;
	extern IO_STATE_S	ioProperty;

	char paraId[BUFF_LEN];
	char name[BUFF_LEN];
	char effective[BUFF_LEN];
	char common[BUFF_LEN];
	char level[BUFF_LEN];
	char group[BUFF_LEN];
	char source[BUFF_LEN];
	char property[BUFF_LEN];
	char valueType[BUFF_LEN];
	char defaultValue[BUFF_LEN];
	char editType[BUFF_LEN];
	char min[BUFF_LEN];
	char max[BUFF_LEN];
	
	char tempBuff[BUFF_LEN] = {0};
	int i;
	
	CustomerizedIoTableInit();

	if(GetCustomerizedIoRecordCnt() == NULL)
	{
		return -3;
	}
	if(ioProperty.pTable == NULL)
	{
		return -3;
	}
	
	for(i = 0; i < GetCustomerizedIoRecordCnt(); i++)
	{
		if(GetCustomerizedIoRecord(i, &record) == -1)
		{
			continue;
		}

		oneRecord = get_one_vtk_record(ioProperty.pTable, PARA_ID, record.paraId, 0);
		if(oneRecord == NULL)
		{
			continue;
		}
		
		//读取数据类型

		GetIoDataKeyValue(oneRecord, PARA_ID, paraId);
		GetIoDataKeyValue(oneRecord, NAME, name);
		GetIoDataKeyValue(oneRecord, EFFECTIVE, effective);
		GetIoDataKeyValue(oneRecord, COMMON, common);
		GetIoDataKeyValue(oneRecord, LEVEL, level);
		GetIoDataKeyValue(oneRecord, GROUP, group);
		GetIoDataKeyValue(oneRecord, SOURCE, source);
		GetIoDataKeyValue(oneRecord, PROPERTY, property);
		GetIoDataKeyValue(oneRecord, VALUE_TYPE, valueType);
		GetIoDataKeyValue(oneRecord, DUFAULT, defaultValue);
		GetIoDataKeyValue(oneRecord, EDIT_TYPE, editType);
		GetIoDataKeyValue(oneRecord, MIN_VALUE, min);
		GetIoDataKeyValue(oneRecord, MAX_VALUE, max);

		//数值型
		if(atoi(valueType) == 0)
		{
			if(atoi(record.value) > atoi(max) || atoi(record.value) < atoi(min))
			{
				continue;
			}
		}
		//字符型
		else
		{
			if(strlen(record.value) > atoi(max) || strlen(record.value) < atoi(min))
			{
				continue;
			}
		}

		if(atoi(source) == 1)
		{
			WriteOneIoDataDefaultValue(oneRecord, paraId, name, effective, common, level, group, source, property, valueType, record.value, editType, min, max, record.value);
		}
		else if(atoi(source) == 9)
		{
			WriteExternParameterDefualt(record.paraId, record.value);
		}
	}

	return 0;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
