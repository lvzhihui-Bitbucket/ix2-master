
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#include "utility.h"
#include "uart.h"
#include "vdp_uart.h"
#include "slipframe.h"

// req status
#define   FW_UART_STATUS_IDLE          0
#define   FW_UART_STATUS_INIT          1
#define   FW_UART_STATUS_RD_VER        2
#define   FW_UART_STATUS_START         3
#define   FW_UART_STATUS_ISP_RESET     4
#define   FW_UART_STATUS_DATA          5
#define   FW_UART_STATUS_OVER          6

// command
#define   FW_UART_READ_VERSION_REQ     1
#define   FW_UART_READ_VERSION_RSP     2
#define   FW_UART_UPGRADE_START_REQ    3
#define   FW_UART_UPGRADE_START_RSP    4
#define   FW_UART_UPGRADE_DATA_REQ     5
#define   FW_UART_UPGRADE_DATA_RSP     6
#define   FW_UART_UPGRADE_OVER_REQ     7
#define   FW_UART_UPGRADE_OVER_RSP     8
#define   FW_UART_UPGRADE_SUCCESSFULL  9

#define		FW_UART_UPGRADE_BUSY_WAIT_TIME  5

#define		FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT       5000        //5s

#pragma pack(2)  

#define FW_UART_UPGRADE_DATA_MAX        128

typedef struct
{
    unsigned char   cmd;
} FW_UART_READ_VERSION_REQ_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   vchar;
    unsigned char   result;
} FW_UART_READ_VERSION_RSP_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   block_total;
    unsigned char   block_size;
} FW_UART_UPGRADE_START_REQ_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   block_total;
    unsigned char   block_size;
    unsigned char   result;
} FW_UART_UPGRADE_START_RSP_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   block_num;
    unsigned char   data_len;
    unsigned char   data[FW_UART_UPGRADE_DATA_MAX];
} FW_UART_UPGRADE_DATA_REQ_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   block_num;
    unsigned char   data_len;
    unsigned char   result;
} FW_UART_UPGRADE_DATA_RSP_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   checksumh;
    unsigned char   checksuml;
} FW_UART_UPGRADE_OVER_REQ_T;

typedef struct
{
    unsigned char   cmd;
    unsigned char   checksumh;
    unsigned char   checksuml;
    unsigned char   result;
} FW_UART_UPGRADE_OVER_RSP_T;

typedef struct
{
    int                             status;
    FW_UART_READ_VERSION_REQ_T      req_ver;
    FW_UART_READ_VERSION_RSP_T      rsp_ver;
    FW_UART_UPGRADE_START_REQ_T     req_start;
    FW_UART_UPGRADE_START_RSP_T     rsp_start;
    FW_UART_UPGRADE_DATA_REQ_T      req_data;
    FW_UART_UPGRADE_DATA_RSP_T      rsp_data;
    FW_UART_UPGRADE_OVER_REQ_T      req_over;
    FW_UART_UPGRADE_OVER_RSP_T      rsp_over;
    int                             resend_cnt;

    unsigned char                   fw_version;
    unsigned char                   fw_checksumh;
    unsigned char                   fw_checksuml;
    unsigned char*                  fw_file_buf;
    int                             fw_file_all;
    int                             fw_file_cnt;
    
	pthread_mutex_t 	            lock;
	pthread_cond_t	                cond;
    unsigned char                   fw_uart_recv_buf[FW_UART_UPGRADE_DATA_MAX];
    int                             fw_uart_recv_cnt;
    int                             fw_uart_run_flag;

    pthread_t						fw_uart_process_pid;
	pthread_attr_t 		            fw_uart_process_attr;
    int                             fw_uart_msg_timeout;
	msg_process 			        process;
} FW_UART_UPGRADE_INS_T;

#pragma pack()  

FW_UART_UPGRADE_INS_T   one_fw_ins =
{
    .status = 0,
    .fw_file_buf = NULL,
};

// callback to notify
#define CALLBACK_MSG_READ_VERSION_REQ           0
#define CALLBACK_MSG_READ_VERSION_RSP_OK        1
#define CALLBACK_MSG_READ_VERSION_RSP_ERR       2
#define CALLBACK_MSG_READ_VERSION_RSP_TIMEOUT   3
#define CALLBACK_MSG_UPGRADE_START_REQ          4
#define CALLBACK_MSG_UPGRADE_START_RSP_OK       5
#define CALLBACK_MSG_UPGRADE_START_RSP_ERR      6
#define CALLBACK_MSG_UPGRADE_START_RSP_TIMEOUT  7
#define CALLBACK_MSG_UPGRADE_DATA_REQ           8
#define CALLBACK_MSG_UPGRADE_DATA_RSP_OK        9
#define CALLBACK_MSG_UPGRADE_DATA_RSP_ERR       10
#define CALLBACK_MSG_UPGRADE_DATA_RSP_TIMEOUT   11
#define CALLBACK_MSG_UPGRADE_OVER_REQ           12
#define CALLBACK_MSG_UPGRADE_OVER_RSP_OK        13
#define CALLBACK_MSG_UPGRADE_OVER_RSP_ERR       14
#define CALLBACK_MSG_UPGRADE_OVER_RSP_TIMEOUT   15
#define CALLBACK_MSG_UPGRADE_SUCCESSFULL        16

static void callback_for_notify(FW_UART_UPGRADE_INS_T* pins, unsigned char msg_id, unsigned char blk_all, unsigned blk_cur )
{
    unsigned char msg_dat[10];    
    printf(">>>callback_for_notify[msg_id:%d,blk_all:%d,blk_cur:%d]>>>\n",msg_id,blk_all,blk_cur);
    if( pins->process != NULL )
    {
        msg_dat[0] = msg_id;
        msg_dat[1] = blk_all;
        msg_dat[2] = blk_cur;
		(*pins->process)((void*)msg_dat,3);
    }
}


static unsigned short get_checksum(unsigned char *src_buf, int src_len )
{
    unsigned short checksum = 0;
    for( int i = 0; i < src_len; i++ )
    {
        checksum += src_buf[i];
    }
    return checksum;
}

static int copy_one_block_data(unsigned char *src_buf, int src_len, int block_size, unsigned char *tar_buf, int block_cnt )
{
    int pos = block_size*block_cnt;
    if( pos >= src_len )
        return 0;
    int len = src_len-pos;
    len = (len>block_size)?block_size:len;
    memcpy( tar_buf, src_buf+pos, len);
    return len;
}

static int wait_uart_response( FW_UART_UPGRADE_INS_T* pins, int timeout )
{
    struct timespec abstime;
    struct timeval now;
    int nsec;

    pthread_mutex_lock( &pins->lock );
    while( pins->fw_uart_recv_cnt == 0 )
    {
        if( timeout )
        {
            gettimeofday(&now, NULL);
            nsec = now.tv_usec * 1000 + (timeout % 1000) * 1000000;
            abstime.tv_nsec = nsec % 1000000000;
            abstime.tv_sec = now.tv_sec + nsec / 1000000000 + timeout / 1000;
            if( pthread_cond_timedwait( &pins->cond, &pins->lock, &abstime ) == ETIMEDOUT )
            {
                pthread_mutex_unlock( &pins->lock );
                return 1;
            }
        }
        else
        {
            pthread_cond_wait( &pins->cond, &pins->lock );
        }
    }
	pthread_mutex_unlock( &pins->lock );
	return 0;
}

extern int api_uart_send_pack( unsigned char cmd, char* pbuf, unsigned int len );

int fw_uart_req_send_pack( FW_UART_UPGRADE_INS_T* pins, unsigned char cmd )
{
    unsigned char*  pbuf;
    int             len;

    switch( cmd )
    {
        case FW_UART_READ_VERSION_REQ:
            pins->fw_uart_msg_timeout  = 0;     // 2s
            pins->req_ver.cmd = cmd;
            pbuf = (unsigned char*)&pins->req_ver;
            len = sizeof(pins->req_ver);

            api_uart_send_pack(UART_TYPE_FIRMWARE_M2S, pbuf, len);
            callback_for_notify( pins, CALLBACK_MSG_READ_VERSION_REQ, 0, 0 );
            break;

        case FW_UART_UPGRADE_START_REQ:
            pins->fw_uart_msg_timeout  = 0;     // 2s
            pins->req_start.cmd = cmd;
            pbuf = (unsigned char*)&pins->req_start;
            len = sizeof(pins->req_start);

            api_uart_send_pack(UART_TYPE_FIRMWARE_M2S, pbuf, len);
            callback_for_notify( pins, CALLBACK_MSG_UPGRADE_START_REQ, pins->req_start.block_total, 0 );
            break;

        case FW_UART_UPGRADE_DATA_REQ:
            pins->fw_uart_msg_timeout  = 0;     // 2s
            pins->req_data.cmd = cmd;
            pbuf = (unsigned char*)&pins->req_data;
            len = sizeof(pins->req_data) - FW_UART_UPGRADE_DATA_MAX + pins->req_data.data_len;

            api_uart_send_pack(UART_TYPE_FIRMWARE_M2S, pbuf, len);
            callback_for_notify( pins, CALLBACK_MSG_UPGRADE_DATA_REQ, pins->req_start.block_total, pins->req_data.block_num );
            break;

        case FW_UART_UPGRADE_OVER_REQ:
            pins->fw_uart_msg_timeout  = 0;     // 2s
            pins->req_over.cmd = cmd;
            pbuf = (unsigned char*)&pins->req_over;
            len = sizeof(pins->req_over);

            api_uart_send_pack(UART_TYPE_FIRMWARE_M2S, pbuf, len);
            callback_for_notify( pins, CALLBACK_MSG_UPGRADE_OVER_REQ, pins->req_start.block_total, pins->req_data.block_num );
            break;
    }
    return 0;
}

void* task_fw_uart_process(void* arg )
{
    int size;
    FW_UART_READ_VERSION_RSP_T      *prsp_ver;
    FW_UART_UPGRADE_START_RSP_T     *prsp_start;
    FW_UART_UPGRADE_DATA_RSP_T      *prsp_data;
    FW_UART_UPGRADE_OVER_RSP_T      *prsp_over;

    FW_UART_UPGRADE_INS_T* pins = (FW_UART_UPGRADE_INS_T*)arg;
    unsigned char*  pdb = NULL;

    // send start command
    pins->fw_file_cnt          = 0;
    pins->resend_cnt           = 1;

    pins->fw_uart_recv_cnt      = 0;

	pthread_mutex_lock( &pins->lock );	
    pins->fw_uart_run_flag	= 1;
	pthread_mutex_unlock( &pins->lock );

    // send version confirm
    pins->status = FW_UART_STATUS_RD_VER;
    fw_uart_req_send_pack( pins, FW_UART_READ_VERSION_REQ );
    printf("============[FW_UART_READ_VERSION_REQ]===============\n");

	while( pins->fw_uart_run_flag )
	{
        if( wait_uart_response(pins, pins->fw_uart_msg_timeout) == 0 )
		{            
            pdb = (unsigned char*)pins->fw_uart_recv_buf;
            // response process
            switch( pdb[0] )
            {
                case FW_UART_READ_VERSION_RSP:
                    prsp_ver = (FW_UART_READ_VERSION_RSP_T*)pdb;
                    printf(">>>>FW_UART_READ_VERSION_RSP version[%02x], request version[%02x]>>>\n",prsp_ver->result,pins->fw_version);
                    if( !pins->fw_version || !prsp_ver->result || ( (prsp_ver->vchar == 'v') && (pins->fw_version == prsp_ver->result) ) )
                    {
                        // send notify message
                        //callback_for_notify( pins, CALLBACK_MSG_READ_VERSION_RSP_OK, 0, 0 );

                        pins->req_start.block_size  = 128;
                        pins->req_start.block_total = pins->fw_file_all/pins->req_start.block_size;
                        if( pins->fw_file_all%pins->req_start.block_size ) pins->req_start.block_total++;

                        // send start block
                        pins->status = FW_UART_STATUS_START;
                        fw_uart_req_send_pack( pins, FW_UART_UPGRADE_START_REQ );
                        printf("============[FW_UART_UPGRADE_START_REQ]===============\n");
                    }
                    else
                    {
                        pins->fw_uart_msg_timeout  = FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT;
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_READ_VERSION_RSP_ERR, 0, 0 );
                    }
                    break;

                case FW_UART_UPGRADE_START_RSP:
                    prsp_start = (FW_UART_UPGRADE_START_RSP_T*)pdb;
                    if( prsp_start->result == 0 )
                    {
                        // send notify message
                        //callback_for_notify( pins, CALLBACK_MSG_UPGRADE_START_RSP_OK, pins->req_start.block_total, 0 );

                        // justify block size
                        if( pins->req_start.block_size != prsp_start->block_size )
                        {
                            pins->req_start.block_size = prsp_start->block_size;
                            pins->req_start.block_total = pins->fw_file_all/pins->req_start.block_size;
                            if( pins->fw_file_all%pins->req_start.block_size ) pins->req_start.block_total++;
                        }
                        #if 0
                        // start first data block
                        pins->req_data.block_num  = 0;
                        pins->req_data.data_len = copy_one_block_data(pins->fw_file_buf,pins->fw_file_all,pins->req_start.block_size,pins->req_data.data, pins->req_data.block_num);
                        pins->status = FW_UART_STATUS_DATA;
                        fw_uart_req_send_pack( pins, FW_UART_UPGRADE_DATA_REQ );
                        printf("============[FW_UART_UPGRADE_DATA_REQ(%d)]===============\n",pins->req_data.block_num);
                        #else
                        pins->status = FW_UART_STATUS_ISP_RESET;
                        pins->fw_uart_msg_timeout  = FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT;
                        #endif
                    }
                    else
                    {
                        pins->fw_uart_msg_timeout  = FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT;
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_START_RSP_ERR, pins->req_start.block_total, 0  );
                    }
                    break;

                case FW_UART_UPGRADE_DATA_RSP:
                    prsp_data = (FW_UART_UPGRADE_DATA_RSP_T*)pdb;
                    if( prsp_data->result == 0 )
                    {
                        // send notify message
                        //callback_for_notify( pins, CALLBACK_MSG_UPGRADE_DATA_RSP_OK, pins->req_start.block_total, pins->req_data.block_num );

                        pins->req_data.block_num++;
                        if( pins->req_data.block_num >= pins->req_start.block_total )
                        {
                            // send over block
                            pins->status = FW_UART_STATUS_OVER;
                            pins->req_over.checksuml = pins->fw_checksumh;
                            pins->req_over.checksumh = pins->fw_checksuml;
                            fw_uart_req_send_pack( pins, FW_UART_UPGRADE_OVER_REQ );
                            printf("============[FW_UART_UPGRADE_OVER_REQ]===============\n");
                        }
                        else
                        {
                            // send next data block
                            pins->req_data.data_len = copy_one_block_data(pins->fw_file_buf,pins->fw_file_all,pins->req_start.block_size,pins->req_data.data, pins->req_data.block_num);
                            pins->status = FW_UART_STATUS_DATA;
                            fw_uart_req_send_pack( pins, FW_UART_UPGRADE_DATA_REQ );
                            printf("============[FW_UART_UPGRADE_DATA_REQ(%d)]===============\n",pins->req_data.block_num);
                        }
                    }
                    else
                    {
                        pins->fw_uart_msg_timeout  = FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT;
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_DATA_RSP_ERR, pins->req_start.block_total, pins->req_data.block_num  );
                    }
                    break;

                case FW_UART_UPGRADE_OVER_RSP:
                    prsp_over = (FW_UART_UPGRADE_OVER_RSP_T*)pdb;
                    if( prsp_over->result == 0 )
                    {
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_OVER_RSP_OK, 0, 0 );

                        // send successful
                        pthread_mutex_lock( &pins->lock );	
                        pins->fw_uart_run_flag	= 0;
                        pthread_mutex_unlock( &pins->lock );
                        printf("============[FW_UART_UPGRADE_OVER_RSP: successful!!!]===============\n");
						// send notify message
						callback_for_notify( pins, CALLBACK_MSG_UPGRADE_SUCCESSFULL, 0, 0 );
                    }
                    else
                    {
                        pins->fw_uart_msg_timeout  = FW_UART_UPGRADE_WAIT_UART_MSG_TIMEOUT;
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_OVER_RSP_ERR, 0, 0 );
                    }
                    break;
            }
            pins->fw_uart_recv_cnt = 0;
		}
        else
        {
            // timeout process
            switch( pins->status )
            {
                case FW_UART_STATUS_IDLE:
                case FW_UART_STATUS_INIT:
                    break;

                case FW_UART_STATUS_RD_VER:
                    if( pins->resend_cnt )
                    {
                        pins->resend_cnt--;
                        fw_uart_req_send_pack( pins, FW_UART_READ_VERSION_REQ );
                        printf("============[FW_UART_READ_VERSION_REQ-2times]===============\n");
                    }
                    else
                    {
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_READ_VERSION_RSP_TIMEOUT, 0, 0 );
                        printf("============[FW_UART_READ_VERSION_REQ-timeout]===============\n");
                        pthread_mutex_lock( &pins->lock );	
                        pins->fw_uart_run_flag	= 0;
                        pthread_mutex_unlock( &pins->lock );
                    }
                    break;

                case FW_UART_STATUS_START:
                    if( pins->resend_cnt )
                    {
                        pins->resend_cnt--;                   
                        fw_uart_req_send_pack( pins, FW_UART_UPGRADE_START_REQ );
                        printf("============[FW_UART_UPGRADE_START_REQ-2times]===============\n");
                    }
                    else
                    {
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_START_RSP_TIMEOUT, pins->req_start.block_total, 0  );
                        printf("============[FW_UART_UPGRADE_START_REQ-timeout]===============\n");
                        pthread_mutex_lock( &pins->lock );	
                        pins->fw_uart_run_flag	= 0;
                        pthread_mutex_unlock( &pins->lock );
                    }
                    break;

                case FW_UART_STATUS_ISP_RESET:
                    // start first data block
                    pins->req_data.block_num  = 0;
                    pins->req_data.data_len = copy_one_block_data(pins->fw_file_buf,pins->fw_file_all,pins->req_start.block_size,pins->req_data.data, pins->req_data.block_num);
                    pins->status = FW_UART_STATUS_DATA;
                    fw_uart_req_send_pack( pins, FW_UART_UPGRADE_DATA_REQ );
                    printf("============[FW_UART_UPGRADE_DATA_REQ(%d)]===============\n",pins->req_data.block_num);
                    break;

                case FW_UART_STATUS_DATA:
                    if( pins->resend_cnt )
                    {
                        pins->resend_cnt--;                   
                        fw_uart_req_send_pack( pins, FW_UART_UPGRADE_DATA_REQ );
                        printf("============[FW_UART_UPGRADE_DATA_REQ-2times]===============\n");
                    }
                    else
                    {
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_DATA_RSP_TIMEOUT, pins->req_start.block_total, pins->req_data.block_num  );
                        printf("============[FW_UART_UPGRADE_DATA_REQ-timeout]===============\n");
                        pthread_mutex_lock( &pins->lock );	
                        pins->fw_uart_run_flag	= 0;
                        pthread_mutex_unlock( &pins->lock );
                    }
                    break;

                case FW_UART_STATUS_OVER:
                    if( pins->resend_cnt )
                    {
                        pins->resend_cnt--;                   
                        fw_uart_req_send_pack( pins, FW_UART_UPGRADE_OVER_REQ );
                        printf("============[FW_UART_UPGRADE_OVER_REQ-2times]===============\n");
                    }
                    else
                    {
                        // send notify message
                        callback_for_notify( pins, CALLBACK_MSG_UPGRADE_OVER_RSP_TIMEOUT, 0, 0 );
                        printf("============[FW_UART_UPGRADE_OVER_REQ-timeout]===============\n");
                        pthread_mutex_lock( &pins->lock );	
                        pins->fw_uart_run_flag	= 0;
                        pthread_mutex_unlock( &pins->lock );
                    }
                    break;
            }
        }
	}
    // free file buffer
    if( pins->fw_file_buf != NULL )
    {
        free( pins->fw_file_buf );
        printf("=====free fw_file_buf=====\n");
    }

    pins->status = FW_UART_STATUS_IDLE;
	return 0;
}

int api_fw_uart_upgrade_recv(unsigned char* precvbuf, int len )
{
	pthread_mutex_lock( &one_fw_ins.lock );
    if( !one_fw_ins.fw_uart_recv_cnt )
    {
        one_fw_ins.fw_uart_recv_cnt = len;
        memcpy( one_fw_ins.fw_uart_recv_buf, precvbuf, len );
		pthread_cond_signal( &one_fw_ins.cond );
	}
    pthread_mutex_unlock( &one_fw_ins.lock );
    return 0;
}

int api_fw_uart_upgrade_stop( void )
{
	pthread_mutex_lock( &one_fw_ins.lock );	
	one_fw_ins.fw_uart_run_flag	= 0;
	one_fw_ins.status = FW_UART_STATUS_IDLE;
	pthread_mutex_unlock( &one_fw_ins.lock );
	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ );
    return 0;
}

int api_fw_uart_upgrade_wait_over( void )
{
    // wait last over
    if( one_fw_ins.status != FW_UART_STATUS_IDLE )
    {
		int wait_to = 0;
		while(1)
		{
			if( one_fw_ins.status != FW_UART_STATUS_IDLE )
			{
				usleep(1000*1000);
				if( ++wait_to >= FW_UART_UPGRADE_BUSY_WAIT_TIME )	// 5s
                {
                    one_fw_ins.status = FW_UART_STATUS_IDLE;
                    printf( "api_start_one_fw_uart_upgrade timeout !!!\n"); 
					return 1;
                }
 			}
			else
                break;
 		}
    }
    return 0;
}

int api_fw_uart_upgrade_start_buff( unsigned char* pfilebuf, int len, int fw_version, msg_process process )
{
    unsigned short checksum;

    // wait last over
    //if( api_fw_uart_upgrade_wait_over() )
    //    return 1;

    // init
    one_fw_ins.status = FW_UART_STATUS_INIT;

	pthread_mutex_init( &one_fw_ins.lock, NULL);
    pthread_cond_init( &one_fw_ins.cond, NULL );
    one_fw_ins.fw_uart_run_flag     = 0;
    if( pfilebuf != NULL )
    {
         if( one_fw_ins.fw_file_buf != NULL ) free(one_fw_ins.fw_file_buf);
        one_fw_ins.fw_file_all   = len;
        one_fw_ins.fw_file_buf   = (unsigned char*)malloc(one_fw_ins.fw_file_all);
        memcpy( one_fw_ins.fw_file_buf, pfilebuf, one_fw_ins.fw_file_all); 
    }
    one_fw_ins.fw_version   = fw_version;
    checksum                = get_checksum(one_fw_ins.fw_file_buf,one_fw_ins.fw_file_all);
    one_fw_ins.fw_checksumh = checksum&0xff;
    one_fw_ins.fw_checksuml = (checksum>>8)&0xff;
    one_fw_ins.process      = process;
    // start
	pthread_attr_init( &one_fw_ins.fw_uart_process_attr );
	pthread_attr_setdetachstate(&one_fw_ins.fw_uart_process_attr,PTHREAD_CREATE_DETACHED);
 	if( pthread_create(&one_fw_ins.fw_uart_process_pid, 0, task_fw_uart_process, (void*)&one_fw_ins) )
	{
        free( one_fw_ins.fw_file_buf );
        one_fw_ins.status = FW_UART_STATUS_IDLE;
        printf( "api_start_one_fw_uart_upgrade start fail !!!\n"); 
        return -1;
	}
    printf( "api_start_one_fw_uart_upgrade start ok !!!\n"); 
    return 0;
}

int api_fw_uart_upgrade_start_file( char* filename, int fw_version, msg_process process )
{
    // wait last over
    if( api_fw_uart_upgrade_wait_over() )
        return 1;

	FILE *fp;
    // init
    one_fw_ins.status = FW_UART_STATUS_INIT;

	if( ( fp = fopen(filename,"rb") ) == NULL )
	{
        one_fw_ins.status = FW_UART_STATUS_IDLE;
		printf("open file %s failed!!!\n",filename);
		return -1;
	}
	struct stat filestat;
    int fd = fileno(fp);
	fstat(fd,&filestat);
    one_fw_ins.fw_file_all = filestat.st_size;
	one_fw_ins.fw_file_buf = malloc(filestat.st_size);
	fread( one_fw_ins.fw_file_buf, one_fw_ins.fw_file_all, 1, fp );
	fclose(fp);
    return api_fw_uart_upgrade_start_buff(NULL,0,fw_version,process);
}

