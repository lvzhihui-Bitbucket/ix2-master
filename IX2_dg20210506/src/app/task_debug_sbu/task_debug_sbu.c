//20181017 allpath
#if 1 //IX2_TEST 
#include "task_debug_sbu.h"

#include "obj_LogView.h"
#include "logdoc.h"

#include "vtk_udp_stack_c5_ipc_cmd.h"

int debug_log_doc_level = 10;
int debug_log_view_level = 200;

Loop_vdp_common_buffer	vdp_debug_sbu_inner_queue;
vdp_task_t				task_debug_sbu;

// lzh_20170426_s
#define					LOG_SYNC_TIMEOUT	15	// s
OS_TIMER				log_sync_timer;
void 					LogSyncTimerCallback(void);	
// lzh_20170426_e

void vdp_debug_sbu_inner_data_process(char* msg_data, int len);
void* vdp_debug_sbu_task( void* arg );
	
void vtk_TaskInit_debug_sbu(int priority)
{	
	InitialLogFileLines();
	OpenLogFile();
	init_vdp_common_queue(&vdp_debug_sbu_inner_queue, 500, (msg_process)vdp_debug_sbu_inner_data_process, &task_debug_sbu);
	init_vdp_common_task(&task_debug_sbu, MSG_ID_DEBUG_SBU, vdp_debug_sbu_task, &vdp_debug_sbu_inner_queue, NULL);

	// lzh_20170426_s
	OS_CreateTimer(&log_sync_timer, LogSyncTimerCallback, LOG_SYNC_TIMEOUT*1000/25);
	// lzh_20170426_e

	dprintf("vdp_debug_sbu_task starting............\n");
}

void exit_vdp_debug_sbu_task(void)
{
	exit_vdp_common_queue(&vdp_debug_sbu_inner_queue);
	exit_vdp_common_task(&task_debug_sbu);
}

void* vdp_debug_sbu_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_debug_sbu_inner_data_process(char* msg_data,int len)
{
	int desc_len,remain_len;
	
	desc_len = msg_data[0];	// 得到描叙的长度

	// 长度为0则需要写入到log文件
	if( desc_len == 0 )
	{
		// 回写数据到文件
		CloseLogFile();
		// 判断文件是否超长
		JustifyLogFileLength();	
		// 再次打开文件
		OpenLogFile();
		printf("sync to log doc ok!\n");		
	}
	else
	{
		char *pviewdat = (msg_data+1);
		printf("desc_len = %d,0x%02x,0x%02x\n",desc_len, pviewdat[0],pviewdat[1],pviewdat[7],pviewdat[8]);	
		// 写入到logFile
		write_rec_to_log_doc(pviewdat);
		// 启动同步定时器
		OS_RetriggerTimer(&log_sync_timer);
	}
}

void LogSyncTimerCallback(void)
{
	static char sync_cmd = 0;
	push_vdp_common_queue(&vdp_debug_sbu_inner_queue, &sync_cmd, 1 );
}


/*
time_t long型，表示从1970年1月1日到现在经过的秒数。
struct tm 
{
	int tm_sec;      // 秒 – 取值区间为[0,59] 
	int tm_min;      // 分 - 取值区间为[0,59] 
	int tm_hour;     // 时 - 取值区间为[0,23] 
	int tm_mday;     // 一个月中的日期 - 取值区间为[1,31] 
	int tm_mon;      // 月份（从一月开始，0代表一月） - 取值区间为[0,11] 
	int tm_year;     // 年份，其值等于实际年份减去1900 
	int tm_wday;     // 星期 – 取值区间为[0,6]，其中0代表星期天，1代表星期一，以此类推 
	int tm_yday;     // 从每年的1月1日开始的天数 – 取值区间为[0,365]，其中0代表1月1日，1代表1月2日
};
*/
// 加入的log信息格式如下:
// 1、时间日期(15): 	160412_11:20:30
// 2、业务类型(6):		S_CAL0
// 3、描叙(64):		非通信-Call start, LINKING.../通信-[192.168.13.1];UDP;T
// 4、等级(1):		0-9，数字越大越高
// 5、细节:			通信类型的数据

/****************************************************************************************************************************************
 * @fn      API_add_log_item()
 *
 * @brief   加入一个log信息
 *
 * @param   level 		- 保存到文档或view的等级，0-9，数字越大登记越高
 * @param   pbusiness 	- 业务类型，固定为6个字符长度，如: 格式为S_XXXX
 * @param   pdes 		- 业务描叙，最长为64个字节，描叙中空格用'_'来替代，空格作为excel的分隔符 (若为通信格式，则格式为:[IP];UDP/TCP;T/R)
 * @param   pdetail	- 业务细节，一般作为通信数据包显示
 *
 * @return  0 - ok, other - err
 ***************************************************************************************************************************************/
int API_add_log_item( int level, char* pbusiness, char* pdes, char* pdetail )
{
#if 1
	char strout[MAX_PATH_LENGTH+1];
	char strtime[MAX_PATH_LENGTH];
	char des_temp[LOG_DESC_LEN+1];
	
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	sprintf( strtime,"%02d%02d%02d_%02d:%02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	strout[0] = strlen(pdes); // 保存描述的长度
	if(strcmp(pbusiness,Log_CallTest_Title) == 0)
	{
		int i;
		for(i = 0;i < strout[0];i++)
		{
			if(pdes[i] == ' ')
			{
				des_temp[i]='_';
			}
			else
			{
				des_temp[i]=pdes[i];
			}
		}
		des_temp[strout[0]] = '\0';
		snprintf( strout+1, MAX_PATH_LENGTH, "%s %s %s %d %s \n",strtime,pbusiness,des_temp,level,(pdetail==NULL)?" ":pdetail);
	}
	else
		snprintf( strout+1, MAX_PATH_LENGTH, "%s %s %s %d %s \n",strtime,pbusiness,pdes,level,(pdetail==NULL)?" ":pdetail);
	#if 1
	// 写入到logdoc
	if( level >= debug_log_doc_level )
	{	
		push_vdp_common_queue(&vdp_debug_sbu_inner_queue, strout, 1+strlen(strout+1)+1 );
	}
	#endif
	#if 1
	// 写入到logView - 时间-业务类型-描述	
	if( level >= debug_log_view_level )
	{	
		char strview[MAX_PATH_LENGTH];
		char *pviewdat = (strout+1);
		int remain_len = strout[0]+LOG_VIEW_FIXED_LEN-6; 
		strview[0] = pviewdat[7];
		strview[1] = pviewdat[8];
		strview[2] = pviewdat[10];
		strview[3] = pviewdat[11];
		strview[4] = pviewdat[13];
		strview[5] = pviewdat[14];
		
		if(strcmp(pbusiness,Log_CallTest_Title) == 0)
		{
			strview[6] = ' ';
			//memcpy(strview+7,pviewdat+15+14,remain_len-14);
			memcpy(strview+7,pdes+6,remain_len-14);
			remain_len -= 13;
		}
		else
			memcpy(strview+6,pviewdat+15,remain_len);
		
		strview[6+remain_len] = 0;
		API_OsdLogViewAddRec(strview);
	}	
	#endif
#endif

	return 0;	
}

//lzh_20160704_s
int get_upgrade_data_package_len( int cmd )
{
	int len = 0;
	switch( cmd )
	{
	case CMD_DEVICE_TYPE_CODE_VER_REQ:
		len = sizeof(fw_link_request);
		break;
	case CMD_DEVICE_TYPE_CODE_VER_RSP:
		len = sizeof(fw_link_response);
		break;
	case CMD_DEVICE_TYPE_DOWNLOAD_START_REQ:
		len = sizeof(fw_download_start_request);
		break;
	case CMD_DEVICE_TYPE_DOWNLOAD_START_RSP:
		len = sizeof(fw_download_start_response);
		break;
	case CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_REQ:
		len = sizeof(fw_download_verify_request);
		break;
	case CMD_DEVICE_TYPE_DOWNLOAD_VERIFY_RSP:
		len = sizeof(fw_download_verify_response);
		break;
	case CMD_DEVICE_TYPE_UPDATE_START_REQ:
		len = sizeof(fw_download_update_request);
		break;
	case CMD_DEVICE_TYPE_UPDATE_START_RSP:
		len = sizeof(fw_download_update_response);
		break;
	}
	return len;
}

int api_fireware_upgrade_cmd_send( int target_ip, int cmd, char* pdat  )
{
	int len = get_upgrade_data_package_len(cmd);
	
	if( api_udp_c5_ipc_send_data( target_ip ,cmd, pdat, len) == -1 )
	{
		bprintf("api_fireware_upgrade_cmd_send fail\n");
		return -1;
	}
	else
	{
		bprintf("api_fireware_upgrade_cmd_send ok\n");
		return 0;
	}		
}


int api_fireware_upgrade_apply( int target_ip, fw_link_request* presult  )
{
	fw_link_response	fw_link_rsp;
	int len = sizeof(fw_link_request);
	if( api_udp_c5_ipc_send_req( target_ip,CMD_DEVICE_TYPE_CODE_VER_REQ,(char*)presult,sizeof(fw_link_request), &fw_link_rsp, &len) == -1 )
	{
		bprintf("api_fireware_upgrade_apply fail\n");
		return -1;
	}
	else
	{
		bprintf("api_fireware_upgrade_apply ok\n");
		return 0;
	}		
}
//lzh_20160704_e
#endif

