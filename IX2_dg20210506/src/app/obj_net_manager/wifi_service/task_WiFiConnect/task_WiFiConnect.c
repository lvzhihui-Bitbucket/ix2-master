/**
  ******************************************************************************
  * @file    task_Unlock.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "task_WiFiConnect.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#include "cJSON.h"

	  
Loop_vdp_common_buffer  	vdp_WiFi_mesg_queue;
Loop_vdp_common_buffer  	vdp_WiFi_sync_queue;
vdp_task_t			  		task_WiFi;
	  
void vdp_WiFi_mesg_data_process(char* msg_data, int len);
void* vdp_WiFi_task( void* arg );
		  
void vtk_TaskInit_WiFi(int priority)
{
	init_vdp_common_queue(&vdp_WiFi_mesg_queue, 500, (msg_process)vdp_WiFi_mesg_data_process, &task_WiFi);
	init_vdp_common_queue(&vdp_WiFi_sync_queue, 500, NULL,									&task_WiFi);
	init_vdp_common_task(&task_WiFi, MSG_ID_WiFi, vdp_WiFi_task, &vdp_WiFi_mesg_queue, &vdp_WiFi_sync_queue);
	
	wifi_printf("vdp_WiFi_task starting............\n");
}

void exit_vdp_WiFi_task(void)
{
	exit_vdp_common_queue(&vdp_WiFi_mesg_queue);
	exit_vdp_common_queue(&vdp_WiFi_sync_queue);
	exit_vdp_common_task(&task_WiFi);
}
  
void* vdp_WiFi_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	
	WiFiStateInit();

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

 int wifiTaskIsEanble;

void WifiSycResponse( MSG_WIFI* msg)
{
	MSG_WIFI  data;

	data.head.msg_target_id = msg->head.msg_source_id;
	data.head.msg_source_id	= msg->head.msg_target_id;			
	data.head.msg_type		= (msg->head.msg_type|COMMON_RESPONSE_BIT);
	data.head.msg_sub_type	= 0;

	vdp_task_t* pTask = GetTaskAccordingMsgID(data.head.msg_target_id);	
	if((pTask != NULL) && (pTask != &task_WiFi))
	{
		push_vdp_common_queue(pTask->p_syc_buf, (char*)&data, sizeof(data.head));
	}
}

char* CreateWifiJsonData(char* ssid, char* pwd)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "ssid", ssid);
	cJSON_AddStringToObject(root, "pwd", pwd);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

int ParseWifiJsonData(const char* json, char* ssid, char* pwd)
{
    int status = 0;

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	if(ssid != NULL)
	{
		ParseJsonString(root, "ssid", ssid, 15);
	}
	
	if(pwd != NULL)
	{
		ParseJsonString(root, "pwd", pwd, 12);
	}
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void vdp_WiFi_mesg_data_process(char* msg_data,int len)
{
	MSG_WIFI* msg;
	int i;
	char* wifiData;

	msg = (MSG_WIFI*)msg_data;
	wifiTaskIsEanble = 0;

	switch(msg->head.msg_type )
	{
		case MSG_TYPE_WIFI_OPEN:
			WiFiOpen(1);
			// lzh_20181025_s			
			//WifiSycResponse(msg);
			//WifiSycResponse(msg);
			// lzh_20181025_e			
			break;
		case MSG_TYPE_WIFI_CLOSE:
			WiFiClose();
			// lzh_20181025_s			
			//WifiSycResponse(msg);
			//WifiSycResponse(msg);
			// lzh_20181025_e			
			break; 
		case MSG_TYPE_WIFI_CONNECT:
			if( !IfCallServerBusy())
			{
				start_wpa_supplicant_server("wlan0");
				if(WiFiConnect(msg->wifi_ssid,msg->wifi_pwd) != 0)
				{
					wifiData = CreateWifiJsonData(msg->wifi_ssid, msg->wifi_pwd);
					if(wifiData != NULL)
					{

						if(GetWifiStopConnecting()==0)
							API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CONNECT_FAILED, wifiData, strlen(wifiData)+1);
						free(wifiData);
					}
				}
			}
			else
			{
				if(wifiRun.wifiConnect != 2)
				{
					stop_wpa_supplicant_server();
				}
			}
			//WifiSycResponse(msg);
			break;
		case MSG_TYPE_WIFI_DISCONNECT:
			WiFiDisconnect();
			//WifiSycResponse(msg);
			break;
		case MSG_TYPE_WIFI_SEARCH:
			WiFiSearch();
			//WifiSycResponse(msg);
			break;
		case MSG_TYPE_WIFI_POLLING:
			if(GetWifiStopConnecting()!=0)
			{
				wifiRun.stopConneting=0;
				break;
			}
			if(!IfCallServerBusy())
			{
				start_wpa_supplicant_server("wlan0");
				WiFiPolling();
			}
			else
			{
				if(wifiRun.wifiConnect != 2)
				{
					stop_wpa_supplicant_server();
				}
			}
			break;
		case MSG_TYPE_WIFI_DELETE_SSID:
			for(i = 0; i<3; i++)
			{
				if(!strcmp(msg->wifi_ssid, wifiRun.saveSsid[i]) && !strcmp(msg->wifi_pwd, wifiRun.savePwd[i]))
				{
					if(i == 0)
					{
						strcpy(wifiRun.saveSsid[0], wifiRun.saveSsid[1]);
						strcpy(wifiRun.savePwd[0], wifiRun.savePwd[1]);
						strcpy(wifiRun.saveSsid[1], wifiRun.saveSsid[2]);
						strcpy(wifiRun.savePwd[1], wifiRun.savePwd[2]);
						memset(wifiRun.saveSsid[2], 0, WIFI_SSID_LENGTH+1);
						memset(wifiRun.savePwd[2], 0, WIFI_PWD_LENGTH+1);
					}
					else if(i == 1)
					{
						strcpy(wifiRun.saveSsid[1], wifiRun.saveSsid[2]);
						strcpy(wifiRun.savePwd[1], wifiRun.savePwd[2]);
						memset(wifiRun.saveSsid[2], 0, WIFI_SSID_LENGTH+1);
						memset(wifiRun.savePwd[2], 0, WIFI_PWD_LENGTH+1);
					}
					else if(i == 2)
					{
						memset(wifiRun.saveSsid[2], 0, WIFI_SSID_LENGTH+1);
						memset(wifiRun.savePwd[2], 0, WIFI_PWD_LENGTH+1);
					}
				}
			}
			SaveWIFISsidToFile();
			break;
		case MSG_TYPE_WIFI_ADD_SSID:
			SaveWifiSsid(msg->wifi_ssid, msg->wifi_pwd);
			break;
		case MSG_TYPE_WIFI_CLEAR_SSID:
			API_WifiClearSSID();
			break;
	}
	
	wifiTaskIsEanble = 1;
}

static int IfWifiIsBusy(void)
{
	uint8 wait_s_count;
	
	if (wifiTaskIsEanble)	//允许访问
	{
		;
	}
	else
	{
		wait_s_count = 10+200;	//最长延时20S
		while (wait_s_count > 10)	
		{
			usleep(100000);			//每次延时100ms
			wait_s_count--;
			if (wifiTaskIsEanble)
			{
				wait_s_count = 0;		
			}
		}	
		if (wait_s_count != 0)
		{
			return (1);	//访问失败
		}
	}
	return 0;
}

/*------------------------------------------------------------------------
						发送消息
------------------------------------------------------------------------*/
int API_WifiConnect(uint8 msg_type, uint8* ssid , uint8* pwd)
{
	MSG_WIFI	msg;	
	MSG_WIFI	temp;
	int 		len;
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_WiFi;
	msg.head.msg_type 		= msg_type;
	msg.head.msg_sub_type 	= 0;
	if(ssid != NULL)
		strcpy(msg.wifi_ssid, ssid);
	else
		msg.wifi_ssid[0] = 0;
	if(pwd != NULL)
		strcpy(msg.wifi_pwd, pwd);
	else
		msg.wifi_pwd[0] = 0;

	vdp_task_t* pTask = OS_GetTaskID();

	// 压入本地队列
	if( pTask != NULL )
	{
		//if(IfWifiIsBusy())		//wifi忙
		//{
		//	wifi_printf("11111111111111 API_Wifi is busy\n");
		//	return -2;
		//}		
		//WaitForBusinessACK( pTask->p_syc_buf, msg.head.msg_type, (char*)&temp, &len, 1 );
			
		push_vdp_common_queue(&vdp_WiFi_mesg_queue,  &msg, sizeof(MSG_WIFI));

		//cao_20181029_s
		wifiRun.stopConneting = 1;
		return 0;
		//cao_20181029_e
		
		// lzh_20181025_s
//		if( msg_type != MSG_TYPE_WIFI_OPEN && msg_type != MSG_TYPE_WIFI_CLOSE )
//			return 0;
//		if(WaitForBusinessACK( pTask->p_syc_buf, msg.head.msg_type, (char*)&temp, &len, 10000) > 0)
//		{
//			return 0;
//		}
		// lzh_20181025_e
	}
	
	return -1;
}
int JudgeIfWifiConnected(void)
{
	if(wifiRun.wifiSwitch&&wifiRun.wifiConnect==2)
	{
		return 1;
	}
	return 0;
}
	
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
