/**
  ******************************************************************************
  * @file    obj_Unlock_State.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <string.h>
#include "task_WiFiConnect.h"
#include "obj_WiFi_State.h"
#include "wpa_supplicant.h"
#include "iwlist.h"
#include "task_IoServer.h"
#include "obj_SYS_VER_INFO.h"
#include "task_survey.h"
#include "task_VideoMenu.h"
#include "elog.h"
#include "cJSON.h"
int wifi_connect_state_process(int state);


WIFI_RUN wifiRun;
extern int wifiTaskIsEanble;
extern Loop_vdp_common_buffer  	vdp_WiFi_mesg_queue;
extern int wpaSupplicantServerState;

#define POLLING_CYCLE_TIME		5

void WifiConnectPollingTimerCallback()
{
	static int wifiConnect1_time=0;
	if(wifiRun.wifiSwitch)
	{
		//printf("11111111111%s:%d\n",__func__,__LINE__);
		if(wifiTaskIsEanble)
		{
			VDP_MSG_HEAD	msg;
			msg.msg_source_id	= MSG_ID_WiFi;
			msg.msg_target_id	= MSG_ID_WiFi;
			msg.msg_type		= MSG_TYPE_WIFI_POLLING;
			msg.msg_sub_type	= 0;	
			if(wifiRun.wifiConnect!=1)
			{
				wifiConnect1_time=0;
				push_vdp_common_queue(&vdp_WiFi_mesg_queue,  &msg, sizeof(VDP_MSG_HEAD));
			}
			else
			{
				wifiConnect1_time+=wifiRun.pollingCycle;
				if(wifiConnect1_time>=300)
				{
					wifiConnect1_time=0;
					push_vdp_common_queue(&vdp_WiFi_mesg_queue,  &msg, sizeof(VDP_MSG_HEAD));
				}
			}
		}
		OS_SetTimerPeriod(&wifiRun.connectPollingTimer, (1000/25) * wifiRun.pollingCycle);
		OS_RetriggerTimer(&wifiRun.connectPollingTimer);
		
		wifiRun.tryConnetTime += wifiRun.pollingCycle;
	}
	else
	{
		//printf("11111111111%s:%d\n",__func__,__LINE__);
		OS_StopTimer(&wifiRun.connectPollingTimer);
	}
}


void SaveWifiSsid(char* wifi_ssid, char* wifi_pwd)
{
	int i;
	
	if(wifi_ssid[0] == 0)
	{
		return;
	}
	
	for(i = 0; i<3; i++)
	{
		if(!strcmp(wifi_ssid, wifiRun.saveSsid[i]) && !strcmp(wifi_pwd, wifiRun.savePwd[i]))
			break;
	}
	
	if(i == 0)
	{
		return;
	}
	else if(i == 1)
	{
		strcpy(wifiRun.saveSsid[1], wifiRun.saveSsid[0]);
		strcpy(wifiRun.savePwd[1], wifiRun.savePwd[0]);
		strcpy(wifiRun.saveSsid[0], wifi_ssid);
		strcpy(wifiRun.savePwd[0], wifi_pwd);
	}
	else if(i == 2 || i == 3)
	{
		strcpy(wifiRun.saveSsid[2], wifiRun.saveSsid[1]);
		strcpy(wifiRun.savePwd[2], wifiRun.savePwd[1]);
		strcpy(wifiRun.saveSsid[1], wifiRun.saveSsid[0]);
		strcpy(wifiRun.savePwd[1], wifiRun.savePwd[0]);
		strcpy(wifiRun.saveSsid[0], wifi_ssid);
		strcpy(wifiRun.savePwd[0], wifi_pwd);
	}

	SaveWIFISsidToFile();

}

//判断WIFI是否连接，返回0不连接，返回1连接
int IfWifiIsConnected(char* wifi_ssid)
{
	//没有连接
	if(!wifiRun.wifiConnect)
	{
		return 0;
	}
	
	//没有连接
	if(WifiStatusQuery("wlan0", &wifiRun.curWifi))
	{
		wifi_printf("IfWifiIsConnected not connectted\n");
		return 0;
	}
	//ssid相同
	else if (!strcmp(wifiRun.curWifi.ESSID, wifi_ssid))
	{
		wifi_printf("IfWifiIsConnected is connectted\n");
		return 1;
	}
	//ssid不同
	else
	{
		if (strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[0]) && 
			strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[1]) && 
			strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[2])
			)
		{
			//已经连接，但是SSID不支持utf8，此时不比较SSID
			strcpy(wifiRun.curWifi.ESSID, wifi_ssid);
			return 1;
		}
		else
		{
			return 0;
		}
		wifi_printf("IfWifiIsConnected not connectted wifiRun.curWifi.ESSID = %s, wifi_ssid = %s\n", wifiRun.curWifi.ESSID, wifi_ssid);
	}
}

//通过DHCP获取IP地址
void GetIpByDhcp(void)
{
	//DHCP已经打开
	if(wifiRun.dhcpState)
	{
		if(GetLocalIpByDevice(NET_WLAN0) != -1 /* && GetLocalGatewayByDevice(NET_WLAN0) != -1*/)
		{
			wifi_connect_state_process(2);
		}
		else
		{
			log_i("WIFI restart dhcp");
			//wifiRun.dhcpState = 0;
			WlanUdhcpcClose();
			//wifiRun.dhcpState = 1;
			if( WlanUdhcpc(NET_WLAN0) == 0 )
			{
				log_i("WIFI dhcp succ");
				wifi_connect_state_process(2);					
			}
			else
			{
				log_w("WIFI dhcp fail");
				wifi_connect_state_process(0);
				wifi_printf("GetIpByDhcp Error!\n");
			}
		}
	}
	//DHCP还没打开
	else
	{
		//wifiRun.dhcpState = 1;
		if( WlanUdhcpc(NET_WLAN0) == 0 )
		{
			log_i("WIFI dhcp succ");
			wifi_connect_state_process(2);					
		}
		else
		{
			log_w("WIFI dhcp fail");
			wifi_printf("GetIpByDhcp Error!\n");
			wifi_connect_state_process(0);
		}
	}
}

// lzh_20181025_s
extern void wifi_power_control( int state );
// lzh_20181025_e

void WiFiStateInit(void)
{
	char tempChar[10];
	
	wifiRun.tryConnetTime = 0;
	wifiRun.wifiSwitch = 0;
	wifiRun.wifiConnect = 0;
	wifiRun.wifiSearch = 0;
	wifiRun.connectWifiNum = 0;
	wifiRun.updateReportFlag = 0;
	wifiRun.pollingCycle = POLLING_CYCLE_TIME;
	wifiRun.dhcpState = 0;
	wifiTaskIsEanble = 0;
	wifiRun.saveCurSsid[0] = 0;
	wifiRun.saveCurPwd[0] = 0;
	LoadWIFISsid();
	
	OS_CreateTimer(&wifiRun.connectPollingTimer, WifiConnectPollingTimerCallback, (1000/25) * wifiRun.pollingCycle);

	API_Event_IoServer_InnerRead_All(WIFI_SWITCH, tempChar);
	if(atoi(tempChar))
	{
		WiFiOpen(0);
	}
	// lzh_20181025_s
	else
	{
		//wifi_power_control(0);
	}
	// lzh_20181025_e		
	
	wifiTaskIsEanble = 1;
}

void WiFiOpen(int with_search)
{
	char tempChar[10];
	
	// lzh_20181025_s
	//wifi_power_control(1);
	//usleep(1000*1000);
	// lzh_20181025_e
	log_i("WIFI open");
	wifiRun.tryConnetTime = 0;

	if(wifiRun.wifiSwitch)
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_OPEN);
		if(with_search)
			WiFiSearch();
	}
	else
	{
		if(!IfconfigWlanUp("wlan0"))
		{
			wifiRun.wifiSwitch = 1;
			wifiRun.pollingCycle = POLLING_CYCLE_TIME;	
			
			API_Event_IoServer_InnerRead_All(WIFI_SWITCH, tempChar);
			if(atoi(tempChar) == 0)
			{
				snprintf(tempChar, 10, "%d", wifiRun.wifiSwitch);
				API_Event_IoServer_InnerWrite_All(WIFI_SWITCH, tempChar);
			}
			
			OS_SetTimerPeriod(&wifiRun.connectPollingTimer, (1000/25) * wifiRun.pollingCycle);
			OS_RetriggerTimer(&wifiRun.connectPollingTimer);
			
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_OPEN);
			if(with_search)
				WiFiSearch();

			wifiRun.connectWifiNum = 0;

			if(wifiRun.saveSsid[wifiRun.connectWifiNum][0] == 0)
			{
				wifi_connect_state_process(0);
				return;
			}
			
			SetSupplicantConfig(wifiRun.saveSsid[wifiRun.connectWifiNum], wifiRun.savePwd[wifiRun.connectWifiNum]);

			strcpy(wifiRun.saveCurSsid, wifiRun.saveSsid[wifiRun.connectWifiNum]);
			strcpy(wifiRun.saveCurPwd, wifiRun.savePwd[wifiRun.connectWifiNum]);
			
			//cao_20181020_s
			strcpy(wifiRun.connectingSsid, wifiRun.saveSsid[wifiRun.connectWifiNum]);
			//cao_20181020_e

			wifiRun.stopConneting = 0;
			if( start_wpa_supplicant_server("wlan0") == 0 )
			{
				wifi_printf("WiFiConnect successful at openning[%s][%s]\n\n", wifiRun.saveSsid[wifiRun.connectWifiNum], wifiRun.savePwd[wifiRun.connectWifiNum]);
				WifiStatusQuery("wlan0", &wifiRun.curWifi);
				strcpy(wifiRun.curWifi.ESSID, wifiRun.connectingSsid);
				
				wifi_connect_state_process(1);
				
				// 连接成功则启动的dhcp服务器得到ip地址和网关	
				log_i("WIFI connect succ,ssid:%s",wifiRun.curWifi.ESSID);
				GetIpByDhcp();
			}
			else
			{
				wifi_connect_state_process(0);
			}
		}
		else
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CLOSE);
		}
	}	
	
	SysVerInfoUpdateIp(1);
}

void WiFiClose(void)
{
	char tempChar[10];
	log_i("WIFI close");
	if(wifiRun.wifiSwitch)
	{
		wifi_connect_state_process(0);
	
		stop_wpa_supplicant_server();
		
		if(!IfconfigWlanDown("wlan0"))
		{
			OS_StopTimer(&wifiRun.connectPollingTimer);

			wifiRun.wifiSwitch = 0;
			
			snprintf(tempChar, 10, "%d", wifiRun.wifiSwitch);
			API_Event_IoServer_InnerWrite_All(WIFI_SWITCH, tempChar);

			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CLOSE);
		}
		else
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_OPEN);
		}
	}
	else
	{
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CLOSE);
	}
	
	// lzh_20181025_s	正常退出才能够关闭电源
	//usleep(100*1000);	
	//wifi_power_control(0);
	//usleep(100*1000);	
	// lzh_20181025_e		
	
	SysVerInfoUpdateIp(1);
}


int WiFiConnect(char* wifi_ssid, char* wifi_pwd)
{
	uint8 i;

	if(wifi_ssid[0] == 0)
	{
		return -1;
	}
	
	strcpy(wifiRun.saveCurSsid, wifi_ssid);
	strcpy(wifiRun.saveCurPwd, wifi_pwd);
	
	//cao_20181020_s
	strcpy(wifiRun.connectingSsid, wifi_ssid);
	//cao_20181020_e

	//判断要连接的WIFI是否已经连接
	if(IfWifiIsConnected(wifi_ssid))
	{
		wifi_connect_state_process(2);
		wifi_printf("wifi had connected ssid = %s, pwd = %s\n", wifi_ssid, wifi_pwd);
		return 0;
	}
	else
	{
		wifi_connect_state_process(0);
	}
	
	wifi_connect_state_process(1);	
	
	wifi_printf("wifi try to connecte ssid = %s, pwd = %s\n", wifi_ssid, wifi_pwd);

	// lzh_20171119_s
	// 更改连接配置表
	SetSupplicantConfig(wifi_ssid, wifi_pwd);
	change_wpa_supplicant_configure();

	int connect_times;
	int wapState;
	int wapLastState = WPA_STATE_ERROR;
	int MAX_CONNECT_TIMES = 10;
	int connect_all_times;
	
	wifiRun.stopConneting = 0;
	
	for(connect_all_times = 120, connect_times = 0; (connect_times < MAX_CONNECT_TIMES) && (connect_all_times > 0); connect_times++, connect_all_times--)
	{
		//cao_20181029_s
		if(wifiRun.stopConneting)
		{
			break;
		}
		//cao_20181029_e

		
		wapState = WifiStatusQuery("wlan0", &wifiRun.curWifi);
		if(wapLastState != wapState)
		{
			connect_times = 0;
		}
		
		wifi_printf("WiFiConnect wapState = %d, connect_times = %d\n", wapState, connect_times);	
		
		wapLastState = wapState;
		switch(wapState)
		{
			case WPA_STATE_COMPLETED:
				MAX_CONNECT_TIMES = 10;
				//已经连接的SSID相同，或者没办法判断SSID，因为wpa_cli不支持utf8
				if (!strcmp(wifiRun.curWifi.ESSID, wifi_ssid) || (strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[0]) && strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[1]) && strcmp(wifiRun.curWifi.ESSID, wifiRun.saveSsid[2])))
				{
					//已经连接，但是SSID不支持utf8，此时不比较SSID
					log_i("WIFI connect succ,ssid:%s",wifi_ssid);
					GetIpByDhcp();
					strcpy(wifiRun.curWifi.ESSID, wifi_ssid);
					wifi_printf("WiFiConnect successful [%s][%s]\n", wifi_ssid, wifi_pwd);
					return 0;
				}
				break;
				
			case WPA_STATE_SCANNING:
				MAX_CONNECT_TIMES = 10;
				break;
			case WPA_STATE_ASSOCIATING:
				MAX_CONNECT_TIMES = 5;
				break;
			case WPA_STATE_ASSOCIATED:
				MAX_CONNECT_TIMES = 10*2;
				break;
			case WPA_STATE_DISCONNECTED:
				MAX_CONNECT_TIMES = 3;
				break;
			case WPA_STATE_ERROR:
				MAX_CONNECT_TIMES = 3;
				break;
		}
		
		sleep(1);
		
	}

	// 连接不成功处理
	wifi_connect_state_process(0);	
	wifi_printf("WiFiConnect failed...\n");		
	
	return -1;
}

void WiFiDisconnect(void)
{
	wifi_connect_state_process(0);
}

void ReorderWiFiList(void)
{
	struct wifi_data wifiData;
	int wifiCnt, i;

	if(wifiRun.wifiConnect)
	{
		wifiData = wifiRun.wifiData;
		
		for(wifiCnt = 0; wifiCnt < wifiData.DataCnt; wifiCnt++)
		{
			if(!strcmp(wifiData.WifiInfo[wifiCnt].ESSID, wifiRun.curWifi.ESSID))
			{
				break;
			}
		}
			
		//没有搜索到当前连接的wifi
		if(wifiCnt >= wifiData.DataCnt)
		{
			wifiRun.wifiData.DataCnt = wifiData.DataCnt + 1;
			memcpy(&wifiRun.wifiData.WifiInfo[1], &wifiData.WifiInfo[0], sizeof(struct wifi_info)*wifiData.DataCnt);
			wifiRun.wifiData.WifiInfo[0] = wifiRun.curWifi;
		}
		//搜索到当前连接的wifi
		else
		{
			wifiRun.wifiData.DataCnt = wifiData.DataCnt;
			for(i = 0; i < wifiData.DataCnt; i++)
			{
				if(i == wifiCnt)
				{
					wifiRun.wifiData.WifiInfo[0] = wifiData.WifiInfo[i];
					
				}
				else if(i < wifiCnt)
				{
					wifiRun.wifiData.WifiInfo[i+1] = wifiData.WifiInfo[i];
				}
				else
				{
					wifiRun.wifiData.WifiInfo[i] = wifiData.WifiInfo[i];
				}
			}
		}
	}
}

void WiFiSearch(void)
{
	struct wifi_data wifiData;
	uint8 wifiCnt, i;
	
	if(wifiRun.wifiSwitch)
	{
		wifiRun.wifiSearch = 1;
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_SEARCHING);
		
		iwlist("wlan0", &wifiRun.wifiData);

		ReorderWiFiList();

		wifiRun.wifiSearch = 0;
/*
		wifi_printf("WiFiSearch DataCnt=%d\n", wifiRun.wifiData.DataCnt);	
		
		for(i = 0; i < wifiRun.wifiData.DataCnt; i++)
		{
			wifi_printf("ESSID[%d]=%s, LEVEL[%d]=%d\n", i, wifiRun.wifiData.WifiInfo[i].ESSID, i, wifiRun.wifiData.WifiInfo[i].LEVEL);
		}
*/		
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_SEARCH_OVER);
	}
}

WIFI_RUN GetWiFiState(void)
{
	return wifiRun;
}

int GetWifiConnected(void)
{
	return wifiRun.networkEnable;
}

int GetWifiStopConnecting(void)
{
	return wifiRun.stopConneting;
}

void SetWiFiDhcpState(int state)
{
	wifiRun.dhcpState = state;
}

// 定时5秒扫描一下3个待连接的ap
void WiFiPolling(void)
{
	int i;
	char tempSaveSsid[WIFI_SSID_LENGTH+1];
	char tempSavePwd[WIFI_PWD_LENGTH+1];
	
	if(wifiRun.wifiSwitch)
	{
		//没有连接
		if(!IfWifiIsConnected(wifiRun.saveCurSsid))
		{
			extern Loop_vdp_common_buffer  	vdp_WiFi_mesg_queue;
			
			printf("wifiRun.tryConnetTime = %d,time = %d,connectWifiNum=%d\n", wifiRun.tryConnetTime, time(NULL),wifiRun.connectWifiNum);

			//30分钟连接不上，关闭wifi
			#if 0
			if(wifiRun.tryConnetTime >= 25*60)
			{
				WiFiClose();
				return;
			}
			#endif
			wifi_connect_state_process(0);

			for(i = 0; i < 3; i++)
			{
				if(wifiRun.saveSsid[wifiRun.connectWifiNum][0] == 0)
				{
					if(++wifiRun.connectWifiNum >= 3)
					{
						wifiRun.connectWifiNum = 0;
					}
				}
				else
				{
					break;
				}
			}
			printf("111111111,wifiRun.connectWifiNum=%d\n",wifiRun.connectWifiNum);
			//全部为空SSID
			if(i >= 3)
			{
				return;
			}
			
			strcpy(tempSaveSsid, wifiRun.saveSsid[wifiRun.connectWifiNum]);
			strcpy(tempSavePwd, wifiRun.savePwd[wifiRun.connectWifiNum]);
			printf("222222222,wifiRun.connectWifiNum=%d,%s:%s\n",wifiRun.connectWifiNum,tempSaveSsid, tempSavePwd);
			SaveWifiSsid(tempSaveSsid, tempSavePwd);
			
			WiFiConnect(tempSaveSsid, tempSavePwd);

			if(++wifiRun.connectWifiNum >= 3)
			{
				wifiRun.connectWifiNum = 0;
			}
		}
		//已经连接
		else
		{
			//SaveWifiSsid(wifiRun.saveCurSsid, wifiRun.saveCurPwd);
			GetIpByDhcp();
		}
	}
	else
	{
		OS_StopTimer(&wifiRun.connectPollingTimer);
	}
}

void API_StopWifiPolling(void)
{
	OS_StopTimer(&wifiRun.connectPollingTimer);
}

void API_StartWifiPolling(void)
{
	if(wifiRun.wifiSwitch)
	{
		OS_RetriggerTimer(&wifiRun.connectPollingTimer);
	}
}

void API_WifiReConnect(void)
{
	uint8 temp;

	wifiRun.connectWifiNum = 0;
	wifiRun.pollingCycle = POLLING_CYCLE_TIME;	
	
	wifiRun.saveCurSsid[0] = 0;
	wifiRun.saveCurPwd[0] = 0;
	
	LoadWIFISsid();
	
	API_WifiCNCT(wifiRun.saveSsid[wifiRun.connectWifiNum], wifiRun.savePwd[wifiRun.connectWifiNum]);
}

// 状态机切换处理
// 状态机切换处理
int wifi_connect_state_process(int state)
{
	uint8 timeAutoUpdateEnable;
	uint8 timeZone;
	static int timeAutoUpdateCnt;
	char sip_net_sel[5];
	switch( state )
	{
		case 2:			
			// 连接成功则启动的dhcp服务器得到ip地址和网关	
			wifiRun.tryConnetTime = 0;
			wifiRun.wifiConnect = 2;
			wifiRun.pollingCycle = POLLING_CYCLE_TIME*12;	

			//cao_20181020_s
			OS_SetTimerPeriod(&wifiRun.connectPollingTimer, (1000/25) * wifiRun.pollingCycle);
			OS_RetriggerTimer(&wifiRun.connectPollingTimer);
			//cao_20181020_e
			API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
			if(atoi(sip_net_sel)==1)
			{
				if(!PingNetwork("131.188.3.220") || !PingNetwork("163.177.151.109"))
				{
					//log_i("WIFI Internet check ok");
					wifiRun.networkEnable = 1;
					API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
					
					{
						AutoRegistration();
						//rejoin_multicast_group();
					}
					ResetNetworkWLANCheck();
				}
				if(!PingNetwork(GetSysVerInfo_gateway_by_device(NET_WLAN0)))
				{
					ResetNetworkWLANCheck();
				}
				else
				{
					log_w("WIFI Internet check fail");
					wifiRun.networkEnable = 0;
				}
			}
			if(!wifiRun.updateReportFlag)
			{
				wifiRun.updateReportFlag = 1;
				IsHaveUpdateNewRs();
			}

			/*
			if(wifiRun.networkEnable)
			{
				if(timeAutoUpdateCnt < 2)
				{
					timeAutoUpdateCnt++;
					API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, (uint8*)&timeAutoUpdateEnable);
					if(timeAutoUpdateEnable)
					{
						char timeServer[50] = {0};
						API_Event_IoServer_InnerRead_All(TimeZone, (uint8*)&timeZone);
						API_Event_IoServer_InnerRead_All(TIME_SERVER, (uint8*)&timeServer);
						sync_time_from_sntp_server(inet_addr(timeServer), timeZone-12);
					}		
				}
				//cao_20181025_s
				//else if(timeAutoUpdateCnt >= 30)
				//{
				//	timeAutoUpdateCnt = 0;
				//}
				//cao_20181025_e
			}
			*/

			if(inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0)) != GetLocalIpByDevice(NET_WLAN0))
			{
				wlan_rejoin_multicast_group();
				SysVerInfoUpdateIp(1);
			}
			else
			{
				SysVerInfo_WifiSignalLev_Update(wifiRun.curWifi.LEVEL);
			}
			#if 0
			if(atoi(sip_net_sel)==0)
			{
				if(!PingNetwork(GetSysVerInfo_gateway_by_device(NET_WLAN0)))
				{
					ResetNetworkWLANCheck();
				}
			}
			#endif
			wlan_rejoin_multicast_group();
			SetNetworkIsStarted(NET_WLAN0, 1);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CONNECTED);	
			break;

		case 1:
			wifiRun.wifiConnect = 1;
			timeAutoUpdateCnt = 0;
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CONNECTING);			
			break;
			
		case 0:
			//wifiRun.dhcpState = 0;
			WlanUdhcpcClose();
			SupplicantDisconnect(); 	
			wifiRun.wifiConnect = 0;
			wifiRun.pollingCycle = POLLING_CYCLE_TIME;
			//cao_20181020_s
			OS_SetTimerPeriod(&wifiRun.connectPollingTimer, (1000/25) * wifiRun.pollingCycle);
			OS_RetriggerTimer(&wifiRun.connectPollingTimer);
			//cao_20181020_e
			SysVerInfoUpdateIp(1);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_DISCONNECT);			
			break;
		default:
			break;
	}
}

void LoadWIFISsid(void)
{
	FILE	*file = NULL;
	int 	i;
	char buff[500];
	char	*pos1, *pos2, *pos3;

	for(i = 0; i < 3; i++)
	{
		memset(wifiRun.saveSsid[i], 0, WIFI_SSID_LENGTH+1);
		memset(wifiRun.savePwd[i], 0, WIFI_PWD_LENGTH+1);
	}
	
	for(i = 0; i < 3; i++)
	{
		strcpy(wifiRun.saveSsid[i], "");
		strcpy(wifiRun.savePwd[i], "");
	}
	#if 0
	if( (file=fopen(WIFI_CONFIG_FILE,"r")) == NULL )
	{
		return;
	}

	for(i = 0; fgets(buff,200,file) != NULL && i < 3; )
	{
		pos1 = strstr(buff, "[SSID]=");
		pos2 = strstr(buff, "[PWD]=");
		pos3 = strstr(buff, "[END]");
		
		if(pos1 != NULL && pos2 != NULL && pos3 != NULL)
		{
			memcpy(wifiRun.saveSsid[i], pos1+strlen("[SSID]="), strlen(pos1)-strlen(pos2)-strlen("[SSID]="));
			memcpy(wifiRun.savePwd[i], pos2+strlen("[PWD]="), strlen(pos2)-strlen(pos3)-strlen("[PWD]="));
			
			wifi_printf("Read wifi(%d) [%s][%s]\n", i+1, wifiRun.saveSsid[i], wifiRun.savePwd[i]);
			
			i++;
		}
	}

	fclose(file);
	#endif
	
	if(API_Event_IoServer_InnerRead_All(WIFI_SSID_PARA, buff)==0)
	{
		cJSON *ssid_arr=cJSON_Parse(buff);
		if(ssid_arr!=NULL&&cJSON_IsArray(ssid_arr))
		{
			for(i=0;i<cJSON_GetArraySize(ssid_arr)&&i<3;i++)
			{
				ParseJsonString(cJSON_GetArrayItem(ssid_arr,i),"SSID",wifiRun.saveSsid[i],WIFI_SSID_LENGTH);
				ParseJsonString(cJSON_GetArrayItem(ssid_arr,i),"PWD",wifiRun.savePwd[i],WIFI_PWD_LENGTH);
			}
			cJSON_Delete(ssid_arr);
		}

	}

}
void SaveWIFISsidToFile(void)
{
	int 	i;
	#if 0
	if( (file=fopen(WIFI_CONFIG_FILE,"w+")) == NULL )
	{
		return NULL;
	}
	
	fputs("[WIFI CONFIG]\n", file);
	#endif
	
	cJSON *ssid_arr=cJSON_CreateArray();
	cJSON *one_item;
	char *ssid_arr_str;
	if(ssid_arr!=NULL)
	{
		for(i=0;i<3&&wifiRun.saveSsid[i][0] != 0;i++)
		{
			//snprintf( buff, 200,  "[SSID]=%s[PWD]=%s[END]\n", wifiRun.saveSsid[i], wifiRun.savePwd[i]);
			//fputs(buff, file);
			//wifi_printf("Save wifi(%d) [%s][%s]\n", i+1, wifiRun.saveSsid[i], wifiRun.savePwd[i]);
			one_item=cJSON_CreateObject();
			cJSON_AddStringToObject(one_item,"SSID",wifiRun.saveSsid[i]);
			cJSON_AddStringToObject(one_item,"PWD",wifiRun.savePwd[i]);
			cJSON_AddItemToArray(ssid_arr,one_item);
		}
		ssid_arr_str=cJSON_Print(ssid_arr);
		if(ssid_arr_str!=NULL)
		{
			API_Event_IoServer_InnerWrite_All(WIFI_SSID_PARA, ssid_arr_str);
			free(ssid_arr_str);
		}
		cJSON_Delete(ssid_arr);
	}

}

void RestoreWIFIConfigToDefault(void)
{
	uint8 tempChar[100];
	int i;
	
	for(i = 0; i < 3; i++)
	{
		wifiRun.saveSsid[i][0] = 0;
		wifiRun.savePwd[i][0] = 0;
	}
	#if 0
	snprintf(tempChar,100, "rm %s\n", WIFI_CONFIG_FILE);
	system(tempChar);
	
	sync();
	#endif
	cJSON *ssid_arr=cJSON_CreateArray();
	cJSON *one_item;
	char *ssid_arr_str;
	if(ssid_arr!=NULL)
	{
		ssid_arr_str=cJSON_Print(ssid_arr);
		if(ssid_arr_str!=NULL)
		{
			API_Event_IoServer_InnerWrite_All(WIFI_SSID_PARA, ssid_arr_str);
			free(ssid_arr_str);
		}
		cJSON_Delete(ssid_arr);
	}
	
	API_Event_IoServer_InnerRead_Default(WIFI_SWITCH, tempChar); 
	API_Event_IoServer_InnerWrite_All(WIFI_SWITCH, tempChar);

	API_io_server_save_data_file();
}

int GetWlanSavedSsidNum(void)
{
	int rev = 0;
	int i;
	for(i=0;i<3;i++)
	{
		if(wifiRun.saveSsid[i][0]==0)
			break;
		rev++;
	}
	return rev;
}
int GetOneWlanSavedSsid(int index,char *ssid, char *pwd)
{
	if(index>=3)
		return -1;
	if(wifiRun.saveSsid[index][0]==0)
		return -1;
	if(ssid!=NULL)
		strcpy(ssid,wifiRun.saveSsid[index]);
	if(pwd!=NULL)
		strcpy(pwd,wifiRun.savePwd[index]);
	return 0;
}

int ClearWlanSavedSsid(void)
{
	uint8 tempChar[100];
	int i;
	
	for(i = 0; i < 3; i++)
	{
		wifiRun.saveSsid[i][0] = 0;
		wifiRun.savePwd[i][0] = 0;
	}

	#if 0
	snprintf(tempChar,100, "rm %s\n", WIFI_CONFIG_FILE);
	system(tempChar);
	
	sync();
	#endif
	cJSON *ssid_arr=cJSON_CreateArray();
	cJSON *one_item;
	char *ssid_arr_str;
	if(ssid_arr!=NULL)
	{
		ssid_arr_str=cJSON_Print(ssid_arr);
		if(ssid_arr_str!=NULL)
		{
			API_Event_IoServer_InnerWrite_All(WIFI_SSID_PARA, ssid_arr_str);
			free(ssid_arr_str);
		}
		cJSON_Delete(ssid_arr);
	}
	return 0;
}

int get_wifi_connect_state(void)
{
	return wifiRun.wifiConnect;
}
char *get_wifi_cur_ssid(void)
{
	return wifiRun.saveCurSsid;
}
int get_wifi_switch(void)
{
	return wifiRun.wifiSwitch;
}
int get_wifi_signal_level(void)
{
	int lev=0;
	lev|=wifiRun.curWifi.LEVEL;
	if(wifiRun.curWifi.LEVEL&0x80)
		lev|=0xffffff00;
	return lev;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
