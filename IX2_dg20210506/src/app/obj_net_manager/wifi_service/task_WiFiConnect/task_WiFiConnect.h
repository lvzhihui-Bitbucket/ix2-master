/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_WiFi_H
#define _task_WiFi_H

#include "RTOS.h"
#include "obj_WiFi_State.h"
#include "task_survey.h"


// Define Task Vars and Structures----------------------------------------------

#define	WIFI_PRINTF

#ifdef	WIFI_PRINTF
#define	wifi_printf(fmt,...)	printf("[UDP]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	wifi_printf(fmt,...)
#endif

// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
typedef struct	
{	
	VDP_MSG_HEAD 	head;
	char			wifi_ssid[WIFI_SSID_LENGTH+1];
	char			wifi_pwd[WIFI_PWD_LENGTH+1];
} MSG_WIFI;


//msg_type
#define MSG_TYPE_WIFI_OPEN			0
#define MSG_TYPE_WIFI_CLOSE			1
#define MSG_TYPE_WIFI_CONNECT		2
#define MSG_TYPE_WIFI_DISCONNECT		3
#define MSG_TYPE_WIFI_SEARCH			4
#define MSG_TYPE_WIFI_POLLING			5
#define MSG_TYPE_WIFI_DELETE_SSID		6
#define MSG_TYPE_WIFI_ADD_SSID			7
#define MSG_TYPE_WIFI_CLEAR_SSID			8


// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_WiFi(int priority);



// Define Task others-----------------------------------------------------------



// Define API-------------------------------------------------------------------
int API_WifiConnect(uint8 msg_type, uint8* ssid , uint8* pwd);
WIFI_RUN GetWiFiState(void);

#define API_WifiOpen()					API_WifiConnect(MSG_TYPE_WIFI_OPEN, NULL, NULL)
#define API_WifiClose()					API_WifiConnect(MSG_TYPE_WIFI_CLOSE, NULL, NULL)
#define API_WifiCNCT(ssid, pwd)			API_WifiConnect(MSG_TYPE_WIFI_CONNECT, ssid, pwd)
#define API_WifiDisCNCT()				API_WifiConnect(MSG_TYPE_WIFI_DISCONNECT, NULL, NULL)
#define API_WifiSearch()				API_WifiConnect(MSG_TYPE_WIFI_SEARCH, NULL, NULL)
#define API_WifiDeleteSSID(ssid, pwd)	API_WifiConnect(MSG_TYPE_WIFI_DELETE_SSID, ssid, pwd)
#define API_WifiAddSSID(ssid, pwd)		API_WifiConnect(MSG_TYPE_WIFI_ADD_SSID, ssid, pwd)
#define API_WifiClearSSID()				API_WifiConnect(MSG_TYPE_WIFI_CLEAR_SSID, NULL, NULL)

void API_StopWifiPolling(void);
void API_StartWifiPolling(void);

#endif
