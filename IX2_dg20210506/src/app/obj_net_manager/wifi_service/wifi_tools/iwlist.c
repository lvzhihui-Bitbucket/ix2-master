/*
 * iwlist.c
 *
 *  Created on: 2016��11��29��
 *      Author: root
 */

#include <stdio.h>
#include <libgen.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/wireless.h>

#include "util.h"
#include "event.h"
#include "iwlist.h"
//struct wifi_data WifiData;

inline double freq2double( struct iw_freq *freq )
{
	return ((double) freq->m) * pow( 10, freq->e );
}


void wifi_show( struct iw_event *iwe, struct wifi_data* pWifiData )
{
	char tmp[TMP_LENGTH];
	int i;
	
	memset( tmp, 0, TMP_LENGTH );
	
	switch( iwe->cmd )
	{
		case SIOCGIWAP:
		{
			DEG_P( "=======================================\n" );
			memset( &pWifiData->WifiInfo[pWifiData->DataCnt], 0, sizeof(struct wifi_info) );
			
			mac_addr( &iwe->u.ap_addr, tmp );
			strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].MAC, tmp );
			DEG_P( "MAC: %s\n", pWifiData->WifiInfo[pWifiData->DataCnt].MAC );
			
			break;
		}
			
		case SIOCGIWFREQ:
		{
			double freq = freq2double( &iwe->u.freq );
			
			if( freq <= 1000 ) /* It's Channel */
				DEG_P( "Channel: %d\n", (int) freq );
			else /* It's freq */
			{
				double2string( freq, tmp );
				
				DEG_P( "Frequency: %sHz\n", tmp );
			}
			break;
		}
			
		case SIOCGIWENCODE:
		{
			char key[IW_ENCODING_TOKEN_MAX];
			
			memset( key, 0, sizeof(key) );
			memcpy( (void*) key, iwe->u.encoding.pointer, iwe->u.encoding.length );
			DEG_P( "Encryption key: " );
			
			if( iwe->u.encoding.flags & IW_ENCODE_DISABLED )
				DEG_P( "off\n" );
			else
			{
				encoding_key( tmp, TMP_LENGTH, (const unsigned char *) key, iwe->u.encoding.length, iwe->u.encoding.flags );
				DEG_P( "%s", tmp );
				
				if( (iwe->u.data.flags & IW_ENCODE_INDEX) > 1 )
					DEG_P( "\t[%d]", iwe->u.data.flags & IW_ENCODE_INDEX );
				if( iwe->u.data.flags & IW_ENCODE_RESTRICTED )
					DEG_P( "\tSecurity mode:restricted" );
				if( iwe->u.data.flags & IW_ENCODE_OPEN )
					DEG_P( "\tSecurity mode:open" );
				DEG_P( "\n" );
			}
			
			break;
		}
			
		case SIOCGIWESSID:
		{
			char essid[IW_ESSID_MAX_SIZE + 1];
			memset( essid, 0, sizeof(essid) );
			
			if( iwe->u.essid.pointer && iwe->u.essid.length )
				memcpy( essid, iwe->u.essid.pointer, iwe->u.essid.length );
			
			if( iwe->u.essid.flags )
			{
				strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].ESSID, essid );
				DEG_P( "ESSID:'%s'\n", pWifiData->WifiInfo[pWifiData->DataCnt].ESSID );
			}
			else
				DEG_P( "ESSID:off/any/hidden\n" );
			
			break;
		}
			
		case SIOCGIWRATE:
		{
			double2string( iwe->u.bitrate.value, tmp );
			
			strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].BIT_RATE, tmp );
			DEG_P( "Bit Rate: %sb/s\n", pWifiData->WifiInfo[pWifiData->DataCnt].BIT_RATE );
			
			break;
		}
			
		case SIOCGIWMODE:
		{
			const char *mode[] =
			{ "Auto", "Ad-Hoc", "Managed", "Master", "Repeater", "Secondary", "Monitor", "Unknown/bug" };
			
			strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].MODE, mode[iwe->u.mode] );
			
			DEG_P( "Mode: %s\n", pWifiData->WifiInfo[pWifiData->DataCnt].MODE );
			break;
		}
			
		case IWEVQUAL:
		{
			pWifiData->WifiInfo[pWifiData->DataCnt].QUALITY = iwe->u.qual.qual;
			pWifiData->WifiInfo[pWifiData->DataCnt].LEVEL = iwe->u.qual.level;
			pWifiData->WifiInfo[pWifiData->DataCnt].NOISE = iwe->u.qual.noise;
			
			DEG_P( "Quality: %d  Level:%d  Noise: %d\n", pWifiData->WifiInfo[pWifiData->DataCnt].QUALITY, pWifiData->WifiInfo[pWifiData->DataCnt].LEVEL,
					pWifiData->WifiInfo[pWifiData->DataCnt].NOISE );
			
			
			for(i = 0; i < pWifiData->DataCnt; i++)
			{
				if(!memcmp(pWifiData->WifiInfo[i].MAC, pWifiData->WifiInfo[pWifiData->DataCnt].MAC, 17) || !strcmp(pWifiData->WifiInfo[i].ESSID, pWifiData->WifiInfo[pWifiData->DataCnt].ESSID))
					break;
			}
			
			if(i == pWifiData->DataCnt)
			{
				if((pWifiData->WifiInfo[pWifiData->DataCnt].ESSID[0] != 0) && 
					(pWifiData->WifiInfo[pWifiData->DataCnt].MAC[0] != 0)
				  )
				{
					pWifiData->DataCnt++;
				}
			}
			break;
		}
			
		case IWEVCUSTOM:
		{
			char custom[IW_CUSTOM_MAX + 1];
			
			if( (iwe->u.data.pointer) && (iwe->u.data.length) )
				memcpy( custom, iwe->u.data.pointer, iwe->u.data.length );
			custom[iwe->u.data.length] = '\0';
			
			DEG_P( "Extra: %s\n", custom );
			
			break;
		}
			
		case IWEVGENIE:
		{
//			iw_print_gen_ie( iwe->u.data.pointer, iwe->u.data.length );
			int offset = 0;
			unsigned char *buffer = iwe->u.data.pointer;
			
			/* Loop on each IE, each IE is minimum 2 bytes */
			while( offset <= (iwe->u.data.length - 2) )
			{
				/* Check IE type */
				switch( buffer[offset] )
				{
					case 0xdd: /* WPA1 (and other) */
						if( strlen( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE ) > 0 )
							strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE, "WPA1 / WPA2" );
						else
							strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE, "WPA1" );
						break;
						
					case 0x30: /* WPA2 */
						if( strlen( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE ) > 0 )
							strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE, "WPA1 / WPA2" );
						else
							strcpy( pWifiData->WifiInfo[pWifiData->DataCnt].IEEE, "WPA2" );
						break;
				}
				DEG_P( "IE: %s\n", pWifiData->WifiInfo[pWifiData->DataCnt].IEEE );
				/* Skip over this IE to the next one in the list. */
				offset += buffer[offset + 1] + 2;
			}
			break;
		}
			
		default:
		{
			DEG_P( "[DEBUG.IOCTL] cmd = 0x%X, len = %d\n", iwe->cmd, iwe->len );
			break;
		}
	}
}

int wifi_scan( int fd, const char *wlan, struct iwreq *req )
{
	if( fd < 0 || NULL == wlan || NULL == req )
		return 0;
	
	/* set interface */
	strcpy( req->ifr_ifrn.ifrn_name, wlan );
	
	/* set param for scanning */
	req->u.data.pointer = NULL;
	req->u.data.length = 0;
	req->u.data.flags = IW_SCAN_ALL_ESSID | IW_SCAN_ALL_FREQ |
	IW_SCAN_ALL_MODE | IW_SCAN_ALL_RATE;
	
	/* begin scanning  */
	if( ioctl( fd, SIOCSIWSCAN, req ) == -1 )
	{
		DEG_P( "ioctl( fd, SIOCSIWSCAN, req ) failed %s\n", strerror( errno ) );
		return 0;
	}
	
	return 1;
}

int wifi_read( int fd, const char *wlan, struct iwreq *req )
{
	int count;
	
	if( fd < 0 || NULL == wlan || NULL == req )
		return 0;
	
	/* set interface */
	strcpy( req->ifr_ifrn.ifrn_name, wlan );
	
	req->u.data.flags = 0;
	req->u.data.length = MAX_LENGTH;
	req->u.data.pointer = (unsigned char *) malloc( MAX_LENGTH );
	
	if(req->u.data.pointer == NULL)
	{
		return 0;
	}
	
	memset( req->u.data.pointer, 0, MAX_LENGTH );
	
	/* waiting for complete */
	for( count = 0; count < MAX_COUNT; count++ )
	{
		sleep( 1 );
		
		/* try to get result */
		if( ioctl( fd, SIOCGIWSCAN, req ) != -1 )
			return 1;
#if	1
		else
			DEG_P( "[DEBUG] errno=%d, strerror=%s\n",
			errno, strerror( errno ) );
#endif
	}

	if(req->u.data.pointer)
	{
		free(req->u.data.pointer);
		req->u.data.pointer = NULL;
	}
	
	return 0;
}

int iwlist( const char* pwlan, struct wifi_data* pWifiData )
{
	int sock = 0;
	char wlan[ESSID_LEN];
	struct iwreq req;
	struct event_iter evi;
	struct iw_event iwe;
	int ret;
	int pwlan_len;
	int i;
	int wifiVersion = 2;
	
	memset(pWifiData, 0, sizeof(struct wifi_data));
	
	/* open socket */
	sock = socket( AF_INET, SOCK_DGRAM, 0 );
	if( sock < 0 )
	{
		DEG_P( "create sock failed\n" );
		return -1;
	}
	
	pwlan_len = strlen( pwlan );
	if( pwlan_len > ESSID_LEN )
		pwlan_len = ESSID_LEN;
	
	memset( wlan, 0, ESSID_LEN );
	memcpy( wlan, pwlan, pwlan_len );
	
	/* scan */
	if( wifi_scan( sock, wlan, &req ) == 0 )
	{
		close( sock );
		DEG_P( "scan failed\n" );
		return -1;
	}
	
	/* get result */
	if( wifi_read( sock, wlan, &req ) == 0 )
	{
		if(req.u.data.pointer)
		{
			free(req.u.data.pointer);
			req.u.data.pointer = NULL;
		}
		close( sock );
		DEG_P( "get result failed\n" );
		return -1;
	}
	
	/* show result */
	event_iter_init( &evi, &req );

	while( (ret = event_iter_next( &evi, &iwe )) != 1 )
	{
		if( 0 == ret )
			wifi_show( &iwe, pWifiData );
		if(pWifiData->DataCnt >= 29) break;
	}

	for(i = 0; wifiVersion == 4 && i < pWifiData->DataCnt; i++)
	{
		pWifiData->WifiInfo[i].LEVEL = pWifiData->WifiInfo[i].LEVEL*2/5;
	}
	
	close( sock );

	if(req.u.data.pointer)
	{
		free(req.u.data.pointer);
		req.u.data.pointer = NULL;
	}

	DEG_P( "ok\n" );
	return 0;
}
