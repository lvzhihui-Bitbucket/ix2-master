/*
 * event.h
 *
 *  Created on: 2016��11��29��
 *      Author: root
 */

#ifndef APP_EVENT_H_
#define APP_EVENT_H_

struct event_iter
{
	void *stream;
	unsigned short length;
	void *event;
	void *value;
};

void
event_iter_init( struct event_iter *iter, struct iwreq *iwr );
void
event_iter_term( struct event_iter *evi );

/* 0 = success, 1 = finish, -1 = error */
int
event_iter_next( struct event_iter *evi, struct iw_event *iwe );

#endif /* APP_EVENT_H_ */
