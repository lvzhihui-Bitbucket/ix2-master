/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_DHCP_H
#define _task_DHCP_H

#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_survey.h"


// Define Task Vars and Structures----------------------------------------------

#define	DHCP_PRINTF

#ifdef	DHCP_PRINTF
#define	DHCP_printf(fmt,...)	printf("[UDP]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	DHCP_printf(fmt,...)
#endif

// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
typedef struct
{	
	VDP_MSG_HEAD 	head;
} MSG_DHCP;


//msg_type
#define MSG_TYPE_DHCP_REFRESH		0
#define MSG_TYPE_DHCP_SAVE_ENABLE	1
#define MSG_TYPE_DHCP_STATE_UPDATE			2
#define MSG_TYPE_DHCP_CHECK_INTERNET		3
#define MSG_TYPE_DHCP_CHECKIP_RSP		4

// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_DHCP(int priority);



// Define Task others-----------------------------------------------------------
void LoadDHCP_Enable(void);
void SaveDHCP_Enable(int state);


// Define API-------------------------------------------------------------------
int API_DHCP_SaveEnable(int state);

int GetDhcpEnable(void);

#endif
