/**
  ******************************************************************************
  * @file    task_DHCP.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  //czn_20181227
  
#include "task_DHCP.h"
#include "obj_SYS_VER_INFO.h"
#include "task_IoServer.h"
#include "define_file.h"
#include "unix_socket.h"
#include "task_ListUpdate.h"
#include "cJSON.h"
int init_autoip_client( void );
int Dhcp_Autoip_Open(char *ifn,int dhcp_timout,int autoip_sleep);
int Dhcp_Autoip_Close(void);

Loop_vdp_common_buffer  	vdp_DHCP_mesg_queue;
Loop_vdp_common_buffer  	vdp_DHCP_mesg_sync_queue;
vdp_task_t			  		task_DHCP;
OS_TIMER					dhcpEanbleTimer;
int 						dhcpEanbleTimeCnt;
int						IpActionType = 0;	//0,unkown,1,dhcp,2,autoip,3,static ip,
int						InternetState = 0;
int 					internetTimeUpdateFlag;
int						IpRepeatFlag = 0;

OS_TIMER					CheckInternetTimer;
void CheckInternetTimerCallback(void);
void CheckInternet_Process(void);

void vdp_DHCP_mesg_data_process(char* msg_data, int len);
void* vdp_DHCP_task( void* arg );
int Dhcp_Autoip_CheckIP(void);
		  
void vtk_TaskInit_DHCP(int priority)
{
	init_vdp_common_queue(&vdp_DHCP_mesg_queue, 200, (msg_process)vdp_DHCP_mesg_data_process, &vdp_DHCP_mesg_queue);
	init_vdp_common_queue(&vdp_DHCP_mesg_sync_queue, 200, NULL, 								  		&task_DHCP);
	init_vdp_common_task(&task_DHCP, MSG_ID_DHCP, vdp_DHCP_task, &vdp_DHCP_mesg_queue, &vdp_DHCP_mesg_sync_queue);
	
	DHCP_printf("vdp_DHCP_task starting............\n");
}

void exit_vdp_DHCP_task(void)
{
	exit_vdp_common_queue(&vdp_DHCP_mesg_queue);
	exit_vdp_common_task(&task_DHCP);
}


//��ʱ�������״̬
void dhcpEanbleTimerCallback(void)
{
	if(++dhcpEanbleTimeCnt >= 20)
	{
		OS_StopTimer(&dhcpEanbleTimer);
		API_DHCP_SaveEnable(0);
	}
	else
	{
		OS_RetriggerTimer(&dhcpEanbleTimer);
	}
}
#if 0
void Open_Autoip_Server(void)
{
	FILE* pf   = NULL;
	char cmd_str[200]={'\0'};
	int pid;
	//char local_file[80];
	//char ip_str[20];

	//memset( local_file, 0, 80);
	//memset( ip_str, 0, 20);
	
	//ConvertIpInt2IpStr( DxFileTrs_C_Run.server_ip,ip_str );
	//snprintf(local_file,100,"%s/%s",DxFileTrs_C_Run.src_dir,DxFileTrs_C_Run.src_file);
	printf("Open_Autoip_Server:start!!!!!\n");
	//snprintf(cmd_str,200,"busybox script -c \"/mnt/nand1-1/dhcpclient -d -i eth0 -t 10s -T 60s\"  /mnt/nand1-2/outtemp.txt");
	snprintf(cmd_str,200,"/mnt/nand1-1/autoip.sh");
	system(cmd_str);
	while(1)
		{
		sleep(10);
		printf("test1111\n");
		}
	//system("cd /mnt/nand1-2");
	//snprintf(cmd_str,200,"busybox script -c ./script_test.sh  /mnt/nand1-2/outtempX.txt",DxFileTrs_S_Run.src_dir);
	printf("start_tftp_download --time:%d \n",time(NULL));

	if( (pf = popen( cmd_str, "r" )) == NULL )
	{
		printf( "start_tftpd_download error:%s\n",strerror(errno) );
		return;
	}
	printf("Open_Autoip_Server:pipe open ok!!!!!\n");
	
	
	//usleep(5000000);
	//return;
	fd_set fds;
	struct timeval tv={1,0};
	int ret,fd;

	fd = fileno(pf);
	
	while(1)
	{
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 5;
		tv.tv_usec = 0;	
		printf("Thread_Tftp_Server start1 fd =%d\n",fd);
		#if 1
		ret = select( fd + 1, &fds, NULL, NULL, &tv );
		printf("Thread_Tftp_Server start2 ret=%d\n",ret);
		#if 0
		if(ret == 0 ||ret == -1)
		{
			DxFileTrs_S_Run.trs_result = -1;
			//pclose( pf );
			goto Tftp_Server_Thread_Exit;
		}
		#endif
		#endif
		if( FD_ISSET( fd, &fds ) )
		{
			while(fgets(cmd_str,200,pf)!=NULL)
			//fread(cmd_str,1,1,pf);
			
			printf("auto ip pipe print:%s\n",cmd_str);
		}
			
	}

}
#endif
void* vdp_DHCP_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	
	sleep(2);
	init_autoip_client();
	//
	#if 1
	//LoadDHCP_Enable();
	char temp[20];
	api_nm_if_init();
	sleep(1);
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	if(atoi(temp))
	{
		SetIpActionType(0);
		usleep(100*1000*(((unsigned int)MyRand()%100)+1));
		Dhcp_Autoip_Open("eth0",6,300);
	}
	else
	{
		SetIpActionType(3);
		ResetNetWork();
		SysVerInfoUpdateIp(0);
	}
	//OS_CreateTimer(&dhcpEanbleTimer, dhcpEanbleTimerCallback, 1000/25);
	OS_CreateTimer(&CheckInternetTimer,CheckInternetTimerCallback,30000/25);
	if(IpActionType == 3)
	{
		Dhcp_Autoip_CheckIP();
		OS_RetriggerTimer(&CheckInternetTimer);
	}

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
	#endif
}


 int GetIpSetting(char *ip,char *gw,char *mask);	//return 0 dhcp,1,static ip
 
void vdp_DHCP_mesg_data_process(char* msg_data,int len)
{
	MSG_DHCP* msg;
	SYS_VER_INFO_T info;

	msg = (MSG_DHCP*)msg_data;
	char ip[16],gw[16],mask[16];

	switch(msg->head.msg_type )
	{
		case MSG_TYPE_DHCP_REFRESH:
			#if 0
			dhcpState = 1;
			OS_RetriggerTimer(&dhcpEanbleTimer);
			dhcpEanbleTimeCnt = 0;
			system("udhcpc");
			OS_StopTimer(&dhcpEanbleTimer);
			if(dhcpState)
			{
				SysVerInfoInit(0);

				info = GetSysVerInfo();
				if(!strcmp(info.gw, "255.255.255.255"))
				{
					API_DHCP_Refresh();
				}
				else
				{
					SetNetWork(NULL, info.ip, info.mask, info.gw);
					DeviceFindIpRsp();
				}
			}
			#endif
			break;
		case MSG_TYPE_DHCP_SAVE_ENABLE:
			if(msg->head.msg_sub_type == 0)
			{
				Dhcp_Autoip_Close();
				SetIpActionType(3);
				//Dhcp_Autoip_CheckIP();
				OS_RetriggerTimer(&CheckInternetTimer);
			}
			else
			{
				SetIpActionType(0);
				Dhcp_Autoip_Open("eth0",6,300);
			}
			IpRepeatFlag = 0;
			InternetState = 0;
			break;
		case MSG_TYPE_DHCP_STATE_UPDATE:
			SetIpActionType(msg->head.msg_sub_type);
			if(IpActionType == 1)
			{
				
			}
			else
			{
				#if 0
				if(GetIpSetting(ip,gw,mask))
				{
					Dhcp_Autoip_Close();
					sleep(2);
					SYS_VER_INFO_T sys_info = GetSysVerInfo();
					strcpy(sys_info.ip,ip);
					strcpy(sys_info.gw,gw);
					strcpy(sys_info.mask,mask);
					SetSysVerInfo(sys_info);
					SetIpActionType(3);
					break;
				}
				#endif
			}
			SysVerInfoUpdateIp(0);
			
			SetNetworkIsStarted(NET_ETH0, 1);
			
			CheckInternet_Process();

			InternetTimeAutoUpdate();
			
			//CheckIP_Process();
			//if(IpActionType == 2)				//czn_20190604
			{
				Dhcp_Autoip_CheckIP();
			}
			#if	(!defined(PID_DX470))&&(!defined(PID_DX482))
			//#ifndef PID_DX470
			API_ListUpdate(MSG_TYPE_UPDATE_VIDEO_RESOURCE_TABLE);
			#endif
			OS_RetriggerTimer(&CheckInternetTimer);
			break;
		case MSG_TYPE_DHCP_CHECK_INTERNET:
			#if defined(PID_DX470)||defined(PID_DX482)
			#else
			SetNetworkIsStarted(NET_ETH0, 1);
			#endif
			CheckInternet_Process();
			if(IpRepeatFlag&&IpActionType!=2)
				Dhcp_Autoip_CheckIP();
			//跟证书冲突，先隐掉 StaticIpCheck();
			break;
		case MSG_TYPE_DHCP_CHECKIP_RSP:
			if(msg->head.msg_sub_type>0)
			{
				if(IpActionType == 2)	
				{
					printf("!!!!!!autoip restart-------\n");
					Dhcp_Autoip_Close();
					SetIpActionType(3);
					sleep(1);
					SetIpActionType(0);
					Dhcp_Autoip_Open("eth0",3,300);
					InternetState = 0;
					IpRepeatFlag = 0;
				}
				else
				{
					IpRepeatFlag = 1;
				}
			}
			else
			{
				IpRepeatFlag = 0;
			}
			break;
	}
	
	
}

int UdhcpcClose( void )
{
	char KO[300];
	FILE *pf = NULL;
	char *p = NULL;
	char linestr[250];
	int ret = -1;
	int pidnum;

	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ps | grep udhcpc" );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		DHCP_printf( "open UdhcpcClose error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "grep" ) != NULL )
		{
			;
		}
		else
			if( strstr( linestr, "udhcpc" ) != NULL )	//  udhcpc -i wlan0 -R
			{
				p = strtok( linestr, " " );
				pidnum = atoi( p );
				//kill( (pid_t) pidnum, 0 );
				
				memset( KO, 0, sizeof(KO) );
				strcat( KO, "kill " );
				strcat( KO, p );
				system( KO );
				//pox_system( KO );
				DHCP_printf( "kill Udhcpc ok --------%d---- \n", pidnum );
			}
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;
}



/*------------------------------------------------------------------------
						������Ϣ
------------------------------------------------------------------------*/
int API_DHCP_Refresh(void)
{
	MSG_DHCP	msg;	

	
	UdhcpcClose();
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_DHCP;
	msg.head.msg_type 		= MSG_TYPE_DHCP_REFRESH;
	msg.head.msg_sub_type 	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();

	// ѹ�뱾�ض���
	//if( pTask != NULL )
	{
		push_vdp_common_queue(&vdp_DHCP_mesg_queue,  &msg, sizeof(MSG_DHCP));
		return 0;
	}
	
//	return -1;
}

int API_DHCP_SaveEnable(int state)
{
	MSG_DHCP	msg;	

	
	//UdhcpcClose();
	
	msg.head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id	= MSG_ID_DHCP;
	msg.head.msg_type		= MSG_TYPE_DHCP_SAVE_ENABLE;
	msg.head.msg_sub_type	= state;
	//vdp_task_t* pTask = OS_GetTaskID();

	// ѹ�뱾�ض���
	//if( pTask != NULL )
	{
		push_vdp_common_queue(&vdp_DHCP_mesg_queue,  &msg, sizeof(MSG_DHCP));
		return 0;
	}
	
//	return -1;
}

////////////////////////////////////////////////////////
unix_socket_t	autoip_cliet_socket;

#define	C2S_AUTOIP_OPEN_REQ			0X01
#define	C2S_AUTOIP_CLOSE_REQ			0X02
#define	S2C_AUTOIP_STATE_UPDATE		0X03
#define	C2S_AUTOIP_CHECKIP_REQ		0X04
#define	S2C_AUTOIP_OPEN_RSP			0X81
#define	S2C_AUTOIP_CLOSE_RSP			0X82
#define	S2C_AUTOIP_CHECKIP_RSP		0X84

#pragma pack(1)

typedef struct
{
	unsigned char cmd;
	unsigned char ifname[10];
	unsigned char dhcp_timeout;
	unsigned short autoip_sleep;
	char ip_segment[16];		//czn_20190408
	char ip_mask[16];
	char ip_gateway[16];
}AUTOIP_CMD_STRU;

typedef struct
{
	unsigned char cmd;
	unsigned char rsp_state;
}AUTOIP_RSP_STRU;

#pragma pack()

void autoip_client_socket_recv_data(char* pbuf, int len);

int init_autoip_client( void )
{
	init_unix_socket(&autoip_cliet_socket,0,LOCAL_AUTOIP_SERVICE_FILE,autoip_client_socket_recv_data);		// ��������
	create_unix_socket_create(&autoip_cliet_socket);
	usleep( 100000 );

	dprintf("init_autoip_client OK............\n");
	return 0;
}

void autoip_client_socket_recv_data(char* pbuf, int len)
{
	AUTOIP_RSP_STRU* pcmd = (AUTOIP_RSP_STRU*)pbuf;
	MSG_DHCP	msg;
	switch( pcmd->cmd )
	{
		case S2C_AUTOIP_OPEN_RSP:
			printf("S2C_AUTOIP_OPEN_RSP : %d\n",pcmd->rsp_state);
			break;
			
		case S2C_AUTOIP_CLOSE_RSP:
			printf("S2C_AUTOIP_CLOSE_RSP : %d\n",pcmd->rsp_state);
			break;

		case S2C_AUTOIP_STATE_UPDATE:
			printf("S2C_AUTOIP_STATE_UPDATE : %d\n",pcmd->rsp_state);
				
			//UdhcpcClose();
			
			msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
			msg.head.msg_target_id 	= MSG_ID_DHCP;
			msg.head.msg_type 		= MSG_TYPE_DHCP_STATE_UPDATE;
			msg.head.msg_sub_type 	= pcmd->rsp_state;
			push_vdp_common_queue(&vdp_DHCP_mesg_queue,  &msg, sizeof(MSG_DHCP));
			break;	
		case S2C_AUTOIP_CHECKIP_RSP:		//czn_20190604
			printf("---S2C_AUTOIP_CHECKIP_RSP : %d----\n",pcmd->rsp_state);
			if(GetMenuWaitCheckFlag())
			{
				if(pcmd->rsp_state)
				{
					SetUnhealthyFlag(1);
				}
				else
				{
					SetUnhealthyFlag(0);
				}
				SetMenuWaitCheckFlag(0);
				break;
			}
				
			msg.head.msg_source_id 	= 0;
			msg.head.msg_target_id 	= MSG_ID_DHCP;
			msg.head.msg_type 		= MSG_TYPE_DHCP_CHECKIP_RSP;
			msg.head.msg_sub_type 	= pcmd->rsp_state;
			push_vdp_common_queue(&vdp_DHCP_mesg_queue,  &msg, sizeof(MSG_DHCP));
			#if 0
			if(pcmd->rsp_state>0)
			{
				API_DHCP_SaveEnable(0);
			
				API_DHCP_SaveEnable(1);
			}
			#endif
			break;
			
	}	

}



int Dhcp_Autoip_Open(char *ifn,int dhcp_timout,int autoip_sleep)
{
	AUTOIP_CMD_STRU send_cmd;

	send_cmd.cmd = C2S_AUTOIP_OPEN_REQ;
	send_cmd.dhcp_timeout = dhcp_timout;
	send_cmd.autoip_sleep = autoip_sleep;
	strcpy(send_cmd.ifname,ifn);
		//czn_20190408_s
	API_Event_IoServer_InnerRead_All(Autoip_IPSegment, send_cmd.ip_segment);
	API_Event_IoServer_InnerRead_All(Autoip_IPMask, send_cmd.ip_mask);
	API_Event_IoServer_InnerRead_All(Autoip_IPGateway, send_cmd.ip_gateway);
	//czn_20190408_e

	unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}

int Dhcp_Autoip_Close(void)
{
	AUTOIP_CMD_STRU send_cmd;

	send_cmd.cmd = C2S_AUTOIP_CLOSE_REQ;
	send_cmd.dhcp_timeout = 0;
	send_cmd.autoip_sleep = 0;
	

	unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}

int CheckSameIP(char *checkip)
{
	char cmd[200];
	sprintf(cmd,"arping -D -c 3 %s",checkip);
	FILE *pf = popen(cmd,"r");
	//int prtline = 0;
	char linestr[250];
	char *pstr1,*pstr2;
	int rev = 0;
	//int sameip_num;
	//int	nw_status = 0;
	//static int nw_status_save = 0xff;
	
	if(pf == NULL)
	{
		printf("open pipe error\n");
		//OS_RetriggerTimer(&timer_NwStatus_Polling);
		return;
	}
	while(fgets(linestr,250,pf) != NULL)
	{
		if((pstr1=strstr(linestr,"Received")) != NULL)
		{
			pstr1+=strlen("Received ");
			if(*pstr1 != '0')
			{
				//*pstr2 = 0;
				rev = 1;//atoi(pstr1);
			}
			//nw_status = 1;
			//break;
		}
		printf("!!!!!!!!!CheckSameIP:%s,%d\n",linestr,rev);
	}
	
	pclose(pf);

	return rev;
	//OS_RetriggerTimer(&timer_NwStatus_Polling);
}

int Dhcp_Autoip_CheckIP(void)
{
	//AUTOIP_CMD_STRU send_cmd;
	AUTOIP_RSP_STRU rsp_cmd;
	//send_cmd.cmd = C2S_AUTOIP_CHECKIP_REQ;
	//send_cmd.dhcp_timeout = dhcp_timout;
	//send_cmd.autoip_sleep = autoip_sleep;
	//strcpy(send_cmd.ifname,ifn);
		//czn_20190408_s
	//API_Event_IoServer_InnerRead_All(Autoip_IPSegment, send_cmd.ip_segment);
	//API_Event_IoServer_InnerRead_All(Autoip_IPMask, send_cmd.ip_mask);
	//API_Event_IoServer_InnerRead_All(Autoip_IPGateway, send_cmd.ip_gateway);
	//czn_20190408_e
	//strcpy(send_cmd.ip_segment,GetSysVerInfo_IP());
	rsp_cmd.rsp_state=CheckSameIP(GetSysVerInfo_IP());
	rsp_cmd.cmd=S2C_AUTOIP_CHECKIP_RSP;
	autoip_client_socket_recv_data((char*)&rsp_cmd,sizeof(AUTOIP_RSP_STRU));
	//unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}


int GetIpActionType(void)	//0,unkown,1,dhcp,2,autoip,3,static ip,
{
	return IpActionType;
}

void SetIpActionType(int type)	//0,unkown,1,dhcp,2,autoip,3,static ip,
{
	IpActionType = type;
}

void SetTimeUpdateCnt(int cnt)
{
	internetTimeUpdateFlag = cnt;
}

int GetIpSetting(char *ip,char *gw,char *mask)	//return 0 dhcp,1,static ip
{
	char input[11];
	Global_Addr_Stru addr;
	int i;
	int ret = 0;
	
	#if 0
	for(i = 0;i < GetNamelistNums();i ++)
	{
		GetOneNamelistInfo(i,NULL,NULL,input,&addr);
		if(strstr(GetSysVerInfo_BdRmMs(),input)!= NULL)
		{
			if(addr.ip != 0)
			{
				SYS_VER_INFO_T sys_info = GetSysVerInfo();
				struct in_addr temp; 
				ret = 1;
				if(ip	!= NULL)
				{
					temp.s_addr = addr.ip;  
					strcpy(ip,inet_ntoa(temp));
				}
				if(gw != NULL)
				strcpy(gw,"192.168.243.159");
				if(mask!= NULL)
				strcpy(mask,sys_info.mask);
				
			}
		}
	}
	#endif
	return ret;
}

void CheckInternetTimerCallback(void)
{
	MSG_DHCP	msg;	

	
	//UdhcpcClose();
	
	msg.head.msg_source_id	= 0;//GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id	= MSG_ID_DHCP;
	msg.head.msg_type		= MSG_TYPE_DHCP_CHECK_INTERNET;
	msg.head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();

	
	push_vdp_common_queue(&vdp_DHCP_mesg_queue,  &msg, sizeof(MSG_DHCP));

	OS_RetriggerTimer(&CheckInternetTimer);
	
}
void CheckInternet_Process(void)
{
	char sip_net_sel[5];
	API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
	if(atoi(sip_net_sel)==0)
	{
		if(!PingNetwork("131.188.3.220") || !PingNetwork("163.177.151.109"))
		{
			if(InternetState == 0)
			{
				internetTimeUpdateFlag = 3;
			}
			
			InternetState = 1;
			//API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
			//if(atoi(sip_net_sel)==0)
			{
				#if !defined(PID_IXSE)
				AutoRegistration();
				rejoin_multicast_group();
				#endif
			}
			ResetNetworkCardCheck();
		}
		else
		{
			rejoin_multicast_group();
			InternetState = 0;
		}
	}
	else
	{
		rejoin_multicast_group();
	}
}
#if 0
void CheckIP_Process(void)
{
	int i = 3;
	while(i--)
	{
		if(API_CheckIP(1000))
		{
			API_DHCP_SaveEnable(0);
			
			API_DHCP_SaveEnable(1);

			break;
		}
		sleep(1);
	}
}
#endif

void InternetTimeAutoUpdate(void)
{
	
	//if(InternetState && internetTimeUpdateFlag)
	if(internetTimeUpdateFlag)
	{
		unsigned char timeAutoUpdateEnable;
		unsigned char timeZone;
		unsigned char timeServer[30];
		
		API_Event_IoServer_InnerRead_All(TimeAutoUpdateEnable, timeServer);
		timeAutoUpdateEnable = atoi(timeServer);
		
		if(timeAutoUpdateEnable)
		{
			API_Event_IoServer_InnerRead_All(TimeZone, timeServer);
			timeZone = atoi(timeServer);
			
			API_Event_IoServer_InnerRead_All(TIME_SERVER, timeServer);
			if(sync_time_from_sntp_server(inet_addr(timeServer), timeZone-12) != -1)
			{
				internetTimeUpdateFlag = 0;
			}
			else
			{
				internetTimeUpdateFlag--;
			}
		}		
	}
	
}

int GetInternetState(void)
{
	return InternetState;
}

int GetIpRepeatFlag(void)
{
	return IpRepeatFlag;
}

int get_lan_link(void)
{
	return 1;
}
#define IPErrFile		"/mnt/nand1-2/IPErr.json"
#define MyConfigFile		"/mnt/nand1-2/MyConfig.json"
pthread_mutex_t MyConfig_lock = PTHREAD_MUTEX_INITIALIZER;
void WriteMyConfig(const char *key_name,const char *value)
{
	char *pstr;
	cJSON *my_cfg=NULL;
	pthread_mutex_lock(&MyConfig_lock);
	pstr=GetJsonStringFromFile(MyConfigFile);
	if(pstr!=NULL)
	{
		my_cfg=cJSON_Parse(pstr);
		free(pstr);
	}
	if(my_cfg==NULL)
		my_cfg=cJSON_CreateObject();
	cJSON_DeleteItemFromObject(my_cfg,key_name);
	cJSON_AddStringToObject(my_cfg,key_name,value);

	pstr=cJSON_Print(my_cfg);
	if(pstr!=NULL)
	{
		SetJsonStringToFile(MyConfigFile,pstr);
		free(pstr);
	}
	cJSON_Delete(my_cfg);
	pthread_mutex_unlock(&MyConfig_lock);
}
int ReadMyConfig(const char *key_name,char *value)
{
	char *pstr;
	cJSON *my_cfg=NULL;
	cJSON *node;
	int ret=0;
	pthread_mutex_lock(&MyConfig_lock);
	pstr=GetJsonStringFromFile(MyConfigFile);
	if(pstr==NULL)
	{
		pthread_mutex_unlock(&MyConfig_lock);
		return 0;
	}
	if(pstr!=NULL)
	{
		my_cfg=cJSON_Parse(pstr);
		free(pstr);
	}
	if(my_cfg==NULL)
	{
		pthread_mutex_unlock(&MyConfig_lock);
		return 0;
	}
	node=cJSON_GetObjectItemCaseSensitive(my_cfg,key_name);
	if(node!=NULL)
	{
		strcpy(value,node->valuestring);
		ret=1;
	}
	cJSON_Delete(my_cfg);
	pthread_mutex_unlock(&MyConfig_lock);
	return ret;
}
void WriteDevNumToMyConfig(char *value)
{
	WriteMyConfig("DevNum",value);
}
int ReadDevNumFromMyConfig(char *value)
{
	return ReadMyConfig("DevNum",value);
}

void WriteIpPolicyToMyConfig(char *value)
{
	WriteMyConfig("IpPolicy",value);
}
int ReadIpPolicyFromMyConfig(char *value)
{
	return ReadMyConfig("IpPolicy",value);
}
int WriteStaticIpToMyConfig(char *cfg_ip_addr,char *cfg_mask,char *cfg_gw)
{
	if(cfg_ip_addr!=NULL)
		WriteMyConfig("static_ip_addr",cfg_ip_addr);
	if(cfg_mask!=NULL)
		WriteMyConfig("static_ip_mask",cfg_mask);
	if(cfg_gw!=NULL)
		WriteMyConfig("static_ip_gw",cfg_gw);
}
int ReadStaticIpToMyConfig(char *cfg_ip_addr,char *cfg_mask,char *cfg_gw)
{
	int ret=0;
	if(cfg_ip_addr!=NULL)
		ret|=ReadMyConfig("static_ip_addr",cfg_ip_addr);
	ret=(ret<<1);
	if(cfg_mask!=NULL)
		ret|=ReadMyConfig("static_ip_mask",cfg_mask);
	ret=(ret<<1);
	if(cfg_gw!=NULL)
		ret|=ReadMyConfig("static_ip_gw",cfg_gw);
	return ret;
}
int MyConfigStaticCfg(char *cfg_ip_addr,char *cfg_mask,char *cfg_gw)
{
	char val[20];
	if(ReadIpPolicyFromMyConfig(val))
	{
		if(atoi(val)==0)
		{
			if(ReadStaticIpToMyConfig(cfg_ip_addr,cfg_mask,cfg_gw)==7)
				return 1;
		}
	}
	return 0;
}
void WriteStaticCfgErrorLog(char *cfg_ip_addr,char *cfg_mask,char *cfg_gw)
{
	char *pstr;
	cJSON *Errlog=NULL;
	cJSON *onelog=NULL;
	char buff[100];
	time_t t;
	struct tm *tblock; 
	
	printf("WriteStaticCfgErrorLog :\ncur_ip:%s\ncur_mask:%s\ncur_gw:%s\ncfg_ip:%s\ncfg_mask:%s\ncfg_gw:%s\n",GetSysVerInfo_IP(),GetSysVerInfo_mask(),GetSysVerInfo_gateway(),cfg_ip_addr,cfg_mask,cfg_gw);
	pstr=GetJsonStringFromFile(IPErrFile);
	if(pstr!=NULL)
	{
		Errlog=cJSON_Parse(pstr);
		free(pstr);
	}
	
	if(Errlog==NULL)
		Errlog=cJSON_CreateArray();
	int cnt =0;
	while(cJSON_GetArraySize(Errlog)>=20&&cnt++<5)
		cJSON_DeleteItemFromArray(Errlog,0);
	onelog=cJSON_CreateObject();
	cJSON_AddItemToArray(Errlog,onelog);
		
	t = time(NULL); 
	tblock=localtime(&t);

	snprintf( buff,100, "%02d/%02d/%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);

	cJSON_AddStringToObject(onelog,"time",buff);
	cJSON_AddStringToObject(onelog,"cur_ip",GetSysVerInfo_IP());
	cJSON_AddStringToObject(onelog,"cur_mask",GetSysVerInfo_mask());
	cJSON_AddStringToObject(onelog,"cur_gw",GetSysVerInfo_gateway());
	cJSON_AddStringToObject(onelog,"cfg_ip",cfg_ip_addr);
	cJSON_AddStringToObject(onelog,"cfg_mask",cfg_mask);
	cJSON_AddStringToObject(onelog,"cfg_gw",cfg_gw);

	pstr=cJSON_Print(Errlog);
	if(pstr!=NULL)
	{
		SetJsonStringToFile(IPErrFile,pstr);
		free(pstr);
	}
	cJSON_Delete(Errlog);
}
int GetIPErrLogItemNum(void)
{
	char *pstr;
	cJSON *Errlog=NULL;
	int ret=0;
	pstr=GetJsonStringFromFile(IPErrFile);
	if(pstr!=NULL)
	{
		Errlog=cJSON_Parse(pstr);
		free(pstr);
		ret=cJSON_GetArraySize(Errlog);
		cJSON_Delete(Errlog);
	}
	return ret;
}

void StaticIpCheck(void)
{
	static int StaticIp_Error=0;
	static int StaticIp_Check_Timer=0;
	char cfg_ip_addr[20];
	char cfg_mask[20];
	char cfg_gw[20];
	char mac[20]={0};
	int i;
	StaticIp_Check_Timer++;
	if(StaticIp_Check_Timer!=2&&GetMenuOnState())
		return;
	if(StaticIp_Check_Timer!=2&&(StaticIp_Check_Timer%10)!=0)
		return;
	if(StaticIp_Check_Timer==2||StaticIp_Check_Timer%20==0)
		rejoin_multicast_group();
	#if 0
	if(StaticIp_Check_Timer%10==0)
	{
		char bd_rm_ms[11];
		if(ReadDevNumFromMyConfig(bd_rm_ms)==1)
		{
			if(strcmp(GetSysVerInfo_BdRmMs(),bd_rm_ms)!=0)
			{
				SetSysVerInfo_BdRmMs(bd_rm_ms);
			}
		}
		else
		{
			WriteDevNumToMyConfig(GetSysVerInfo_BdRmMs());
		}
	}
	#endif
	if(StaticIp_Check_Timer%10==0)
	{
		char val[11];
		char io_buff[5];
		if(ReadIpPolicyFromMyConfig(val)==1)
		{
			API_Event_IoServer_InnerRead_All(DHCP_ENABLE, io_buff);
			if(atoi(val)!=atoi(io_buff))
			{
				//SetSysVerInfo_BdRmMs(bd_rm_ms);
				API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, val);
				API_DHCP_SaveEnable(atoi(val));
			}
		}
		#if 0
		else
		{
			WriteDevNumToMyConfig(GetSysVerInfo_BdRmMs());
		}
		#endif
	}
	if(MyConfigStaticCfg(cfg_ip_addr,cfg_mask,cfg_gw))
	{
		if(StaticIp_Check_Timer%30==0)
			SysVerInfoInit(0);
		if(inet_addr(GetSysVerInfo_IP())!=inet_addr(cfg_ip_addr)||
			inet_addr(GetSysVerInfo_mask())!=inet_addr(cfg_mask)||
			inet_addr(GetSysVerInfo_gateway())!=inet_addr(cfg_gw))
		{
			ResetNetWork();
			usleep(200*1000);
			SysVerInfoInit(0);
			if(inet_addr(GetSysVerInfo_IP())!=inet_addr(cfg_ip_addr)||
			inet_addr(GetSysVerInfo_mask())!=inet_addr(cfg_mask)||
			inet_addr(GetSysVerInfo_gateway())!=inet_addr(cfg_gw))
			{
				WriteStaticCfgErrorLog(cfg_ip_addr,cfg_mask,cfg_gw);
				for(i = 0; i<6; i++)
				{
					memcpy(mac+3*i, GetSysVerInfo_Sn()+2*i, 2);
					if(i == 5)
					{
						mac[3*i+2] = 0;
					}
					else
					{
						mac[3*i+2] = ':';
					}
				}
				SetNetWork(mac,cfg_ip_addr,cfg_mask,cfg_gw);
				SetNetWork_new(mac,cfg_ip_addr,cfg_mask,cfg_gw);
				//ResetNetWork();
				usleep(200*1000);
				SysVerInfoInit(0);
			}
		}
	}
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
