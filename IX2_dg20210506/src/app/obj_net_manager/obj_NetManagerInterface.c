/**
  ******************************************************************************
  * @file    task_DHCP.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  //czn_20181227
  
#include "obj_SYS_VER_INFO.h"
#include "task_IoServer.h"

#include "unix_socket.h"
#include "task_WiFiConnect.h"
#include "obj_NetManagerInterface.h"
#include "cJSON.h"

static int nm_net_mode = NM_MODE_LAN;
static int nm_lan_mode = LAN_MODE_DHCP_AUTOIP;

void api_nm_if_init(void)
{
	char temp[5];	
	API_Event_IoServer_InnerRead_All(IX_NET_MODE, temp);
	nm_net_mode=atoi(temp);
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	nm_lan_mode =atoi(temp);
	SysVerInfo_NetMode_Update(nm_net_mode);
}

int api_nm_if_set_net_mode(int new_mode)
{
	if(new_mode<NM_MODE_LAN||new_mode>NM_MODE_WLAN)
		return -1;
	nm_net_mode = new_mode;
	char temp[20];	
	sprintf(temp, "%d", nm_net_mode);
	API_Event_IoServer_InnerWrite_All(IX_NET_MODE, temp);
	SysVerInfo_NetMode_Update(nm_net_mode);
	return nm_net_mode;
}
int api_nm_if_get_net_mode(void)
{
	return nm_net_mode;
}

int api_nm_if_judge_include_dev(const char *rm_num,int target_ip)
{
	if(memcmp(rm_num,GetSysVerInfo_BdRmMs(),8)==0)
		return 1;
	if(api_nm_if_get_net_mode()==NM_MODE_LAN&&strcmp(GetNetDeviceNameByTargetIp(target_ip), NET_ETH0)==0)
		return 1;

	if(api_nm_if_get_net_mode()==NM_MODE_WLAN&&strcmp(GetNetDeviceNameByTargetIp(target_ip), NET_WLAN0)==0)
		return 1;
	
	return 0;
}

const char *api_nm_if_get_sys_ip(void)
{
	if(nm_net_mode==NM_MODE_WLAN)
	{
		return GetSysVerInfo_IP_by_device(NET_WLAN0);
	}
	else
	{
		return GetSysVerInfo_IP_by_device(NET_ETH0);
	}
}

const char *api_nm_if_get_sys_gw(void)
{
	if(nm_net_mode==NM_MODE_WLAN)
	{
		return GetSysVerInfo_gateway_by_device(NET_WLAN0);
	}
	else
	{
		return GetSysVerInfo_gateway_by_device(NET_ETH0);
	}
}

const char *api_nm_if_get_sys_mask(void)
{
	if(nm_net_mode==NM_MODE_WLAN)
	{
		return GetSysVerInfo_mask_by_device(NET_WLAN0);
	}
	else
	{
		return GetSysVerInfo_mask_by_device(NET_ETH0);
	}
}

int api_nm_if_get_lan_mode(void)
{
	return nm_lan_mode;
}

int API_GetIOLanMode(void)
{
	int ret;
	char temp[5];	
	API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
	ret = atoi(temp);
	return ret;
}

int api_nm_if_set_lan_mode(int new_mode,char *ip,char *gw,char *mask)
{
	if(new_mode== LAN_MODE_STATIC)
	{
		if(ip==NULL||gw==NULL||mask==NULL)
			return -1;
		nm_lan_mode=new_mode;
		API_DHCP_SaveEnable(0);

		SetNetWork(NULL, ip, mask, gw);

		ResetNetWork();
		Dhcp_Autoip_CheckIP();		//czn_20190604
		SysVerInfoUpdateIp(0);
		
	}
	else
	{
		nm_lan_mode=new_mode;
		API_Event_IoServer_InnerWrite_All(Autoip_IPSegment, ip);
		API_Event_IoServer_InnerWrite_All(Autoip_IPGateway, gw);
		API_Event_IoServer_InnerWrite_All(Autoip_IPMask, mask);
		API_DHCP_SaveEnable(1);
	}

	char temp[20];
		
	sprintf(temp, "%d", nm_lan_mode);
	API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);

	return nm_lan_mode;
}

int api_nm_if_set_lan_static(char *ip,char *gw,char *mask)
{
	char temp[20];

	nm_lan_mode=LAN_MODE_STATIC;
	sprintf(temp, "%d", nm_lan_mode);
	API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
	API_DHCP_SaveEnable(0);

	SetNetWork(NULL, ip, mask, gw);

	ResetNetWork();
	Dhcp_Autoip_CheckIP();		//czn_20190604
	SysVerInfoUpdateIp(0);

	return nm_lan_mode;
}

const char *api_nm_if_get_lan_ip(void)
{
	return GetSysVerInfo_IP_by_device(NET_ETH0);
}

const char *api_nm_if_get_lan_gw(void)
{
	return GetSysVerInfo_gateway_by_device(NET_ETH0);
}

const char *api_nm_if_get_lan_mask(void)
{
	return GetSysVerInfo_mask_by_device(NET_ETH0);
}

void api_nm_if_get_lan_autoip_segment(char *ip_segment)
{
	API_Event_IoServer_InnerRead_All(Autoip_IPSegment, ip_segment);
}

void api_nm_if_get_lan_autoip_gw(char *gw)
{
	API_Event_IoServer_InnerRead_All(Autoip_IPGateway, gw);
}

void api_nm_if_get_lan_autoip_mask(char *mask)
{
	API_Event_IoServer_InnerRead_All(Autoip_IPMask, mask);
}

const char *api_nm_if_get_wlan_ip(void)
{
	return GetSysVerInfo_IP_by_device(NET_WLAN0);
}

const char *api_nm_if_get_wlan_gw(void)
{
	return GetSysVerInfo_gateway_by_device(NET_WLAN0);
}

const char *api_nm_if_get_wlan_mask(void)
{
	return GetSysVerInfo_mask_by_device(NET_WLAN0);
}

int api_nm_if_get_saved_ssid_num(void)
{
	return GetWlanSavedSsidNum();
}
int api_nm_if_get_one_saved_ssid(int index,char *ssid,char *pwd)
{
	GetOneWlanSavedSsid(index,ssid,pwd);
}

int api_nm_if_clear_saved_ssid(void)
{
	API_WifiClearSSID();
	return  0;
}

#define NM_BACKUP_NET_MODE					"NET_MODE"
#define NM_BACKUP_LAN_SETTING					"LAN_SETTING"
#define NM_BACKUP_LAN_MODE					"LAN_MODE"
#define NM_BACKUP_LAN_STATIC_IP				"LAN_STATIC_IP"
#define NM_BACKUP_LAN_STATIC_GW				"LAN_STATIC_GW"
#define NM_BACKUP_LAN_STATIC_MASK			"LAN_STATIC_MASK"
#define NM_BACKUP_LAN_AUTOIP_SEGMENT		"LAN_AUTOIP_SEGMENT"
#define NM_BACKUP_LAN_AUTOIP_GW				"LAN_AUTOIP_GW"
#define NM_BACKUP_LAN_AUTOIP_MASK			"LAN_AUTOIP_MASK"
#define NM_BACKUP_WLAN_SETTING				"WLAN_SETTING"
#define NM_BACKUP_WLAN_SSID_ARRAY			"WLAN_SSID_ARRAY"
#define NM_BACKUP_WLAN_SSID					"SSID"
#define NM_BACKUP_WLAN_PWD					"PWD"
int api_nm_if_backup(const char *file_path)
{
	int i;
	cJSON *root = NULL;
	
	char* string;
	char disp[50];
	int rev=0;
	root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, NM_BACKUP_NET_MODE, api_nm_if_get_net_mode());
	cJSON *lan_setting=cJSON_AddObjectToObject(root, NM_BACKUP_LAN_SETTING);
	cJSON *wlan_setting=cJSON_AddObjectToObject(root, NM_BACKUP_WLAN_SETTING);
	cJSON *ssid_array=cJSON_AddArrayToObject(wlan_setting,NM_BACKUP_WLAN_SSID_ARRAY);
	cJSON_AddNumberToObject(lan_setting,NM_BACKUP_LAN_MODE,api_nm_if_get_lan_mode());
	if(api_nm_if_get_lan_mode())
	{
		api_nm_if_get_lan_autoip_segment(disp);
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_AUTOIP_SEGMENT,disp);
		api_nm_if_get_lan_autoip_gw(disp);
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_AUTOIP_GW,disp);
		api_nm_if_get_lan_autoip_mask(disp);
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_AUTOIP_MASK,disp);
	}
	else
	{
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_STATIC_IP,api_nm_if_get_lan_ip());
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_STATIC_GW,api_nm_if_get_lan_gw());
		cJSON_AddStringToObject(lan_setting,NM_BACKUP_LAN_STATIC_MASK,api_nm_if_get_lan_mask());
	}
	for(i=0;i<api_nm_if_get_saved_ssid_num();i++)
	{
		char ssid[41];
		char pwd[41];
		if(api_nm_if_get_one_saved_ssid(i,ssid,pwd)==0)
		{
			cJSON *one_ssid=cJSON_CreateObject();
			cJSON_AddItemToArray(ssid_array,one_ssid);
			cJSON_AddStringToObject(one_ssid,NM_BACKUP_WLAN_SSID,ssid);
			cJSON_AddStringToObject(one_ssid,NM_BACKUP_WLAN_PWD,pwd);
		}
	}

	string = cJSON_Print(root);
	FILE *pf=fopen(file_path,"w");
	if(pf != NULL)
	{
		fputs(string,pf);
		fclose(pf);
		sync();
	}
	else
	{
		rev=-1;
	}
	free(string);
	cJSON_free(root);
	return rev;
}

int api_nm_if_restore(const char *file_path)
{
	int rev=0;
	int i;
	FILE *pf=fopen(file_path,"r");
	if(pf==NULL)
		return -1;
	fseek(pf,0,SEEK_END);
	int file_len=ftell(pf);
	char *string=malloc(file_len);
	if(string==NULL)
	{
		fclose(pf);
		return -1;
	}
	fseek(pf,0,SEEK_SET);
	fread(string,1,file_len,pf);
	fclose(pf);
	cJSON *root=cJSON_Parse(string);
	free(string);
	if(root!=NULL)
	{
		cJSON *net_mode=cJSON_GetObjectItemCaseSensitive(root,NM_BACKUP_NET_MODE);
		cJSON *lan_setting=cJSON_GetObjectItemCaseSensitive(root,NM_BACKUP_LAN_SETTING);
		cJSON *wlan_setting=cJSON_GetObjectItemCaseSensitive(root,NM_BACKUP_WLAN_SETTING);
		if(net_mode!=NULL)
		{
			api_nm_if_set_net_mode(((int)net_mode->valuedouble));
		}
		if(lan_setting!=NULL)
		{
			cJSON *lan_mode=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_MODE);
			if(lan_mode!=NULL)
			{
				char para1[21]={0},para2[21]={0},para3[21]={0};
				cJSON *one_node;
				if(((int)lan_mode->valuedouble))
				{
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_AUTOIP_SEGMENT);
					if(one_node!=NULL)
					{
						strncpy(para1,one_node->valuestring,20);
					}
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_AUTOIP_GW);
					if(one_node!=NULL)
					{
						strncpy(para2,one_node->valuestring,20);
					}
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_AUTOIP_MASK);
					if(one_node!=NULL)
					{
						strncpy(para3,one_node->valuestring,20);
					}
				}
				else
				{
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_STATIC_IP);
					if(one_node!=NULL)
					{
						strncpy(para1,one_node->valuestring,20);
					}
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_STATIC_GW);
					if(one_node!=NULL)
					{
						strncpy(para2,one_node->valuestring,20);
					}
					one_node=cJSON_GetObjectItemCaseSensitive(lan_setting,NM_BACKUP_LAN_STATIC_MASK);
					if(one_node!=NULL)
					{
						strncpy(para3,one_node->valuestring,20);
					}
				}
				api_nm_if_set_lan_mode(((int)lan_mode->valuedouble),para1,para2,para3);
			}
			
		}
		if(wlan_setting!=NULL)
		{
			cJSON *ssid_array=cJSON_GetObjectItemCaseSensitive(wlan_setting,NM_BACKUP_WLAN_SSID_ARRAY);
			if(ssid_array!=NULL)
			{
				cJSON *one_ssid,*ssid,*pwd;
				
				api_nm_if_clear_saved_ssid();
				for(i=0;i<cJSON_GetArraySize(ssid_array);i++)
				{
					one_ssid=cJSON_GetArrayItem(ssid_array,i);
					ssid=cJSON_GetObjectItemCaseSensitive(one_ssid,NM_BACKUP_WLAN_SSID);
					pwd=cJSON_GetObjectItemCaseSensitive(one_ssid,NM_BACKUP_WLAN_PWD);
					if(ssid!=NULL&&pwd!=NULL)
						API_WifiAddSSID(ssid->valuestring,pwd->valuestring);
				}
			}
		}
		cJSON_free(root);
	}
	else
	{
		rev=-1;
	}
	return rev;	
}

int JudgeIfWlanDevice(void)
{
	FILE *pf = popen("ifconfig wlan0","r");
	int prtline = 0;
	char linestr[250];

	
	if(pf == NULL)
	{
		return 0;
	}
	while(fgets(linestr,250,pf) != NULL)
	{
		if(strstr(linestr,"Link encap") != NULL)
		{
			return 1;
		}
	}
	return 0;
}
void SoftwareResetLanNetWork(void)
{
	//if(select >= 0 && select <= 1)
	{
		char temp[20];
		
		
		API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
		
		API_DHCP_SaveEnable(0);
		//SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
		usleep(100*1000);
		ResetNetWork();
		//Dhcp_Autoip_CheckIP();		//czn_20190604
		
		if(atoi(temp))
		{
			sleep(2);
			API_DHCP_SaveEnable(1);
		}
		
	}
}
void SoftwareResetWlanNetWork(void)
{
	API_WifiClose();
	usleep(200*1000);
	API_WifiOpen();
}

char *Get_NetState_Para(void)
{
	cJSON *root,*lan_state,*wlan_state;
	char temp[50];
	char *para_string;
	//printf("Get_NetState_Para1111111111\n");
	root=cJSON_CreateObject();
	lan_state=cJSON_CreateObject();
	wlan_state=cJSON_CreateObject();
	//printf("Get_NetState_Para22222222222222\n");
	if(root==NULL||lan_state==NULL||wlan_state==NULL)
		return NULL;
	//cJSON_AddStringToObject(root,"Net Mode",(GetSysVerInfo_NetMode()==0? "LAN":"WLAN"));
	
	cJSON_AddItemToObject(root,"Lan State",lan_state);
	cJSON_AddItemToObject(root,"Wlan State",wlan_state);
	//printf("Get_NetState_Para3333333333\n");
	cJSON_AddStringToObject(lan_state,"Link State",GetSysVerInfo_LanLink()==1?"Linked":"Unlink");
	
	if(GetSysVerInfo_LanActType()==LAN_ACT_DHCP)
	{
		strcpy(temp,"DHCP");
	}
	else if(GetSysVerInfo_LanActType()==LAN_ACT_AUTOIP)
	{
		strcpy(temp,"Auto IP");
	}
	else if(GetSysVerInfo_LanActType()==LAN_ACT_STATIC)
	{
		strcpy(temp,"Static IP");
	}
	else
	{
		strcpy(temp,"Unkown");
	}
	cJSON_AddStringToObject(lan_state,"Act Type",temp);
	cJSON_AddStringToObject(lan_state,"MAC",GetSysVerInfo_mac_by_device(NET_ETH0));
	cJSON_AddStringToObject(lan_state,"IP",GetSysVerInfo_IP_by_device(NET_ETH0));
	cJSON_AddStringToObject(lan_state,"MASK",GetSysVerInfo_mask_by_device(NET_ETH0));
	cJSON_AddStringToObject(lan_state,"GATEWAY",GetSysVerInfo_gateway_by_device(NET_ETH0));
	//printf("Get_NetState_Para4444444444444444\n");
	cJSON_AddStringToObject(wlan_state,"Switch",GetSysVerInfo_WlanEn()?"ON":"OFF");
	if(GetSysVerInfo_WlanConnect()==WLAN_ACT_CONNECTED)
	{
		strcpy(temp,"Connected");
	}
	else if(GetSysVerInfo_WlanConnect()==WLAN_ACT_CONNECTING)
	{
		strcpy(temp,"Connecting");
	}
	else
	{
		strcpy(temp,"Disconnected");
	}
	cJSON_AddStringToObject(wlan_state,"Connect",temp);
	cJSON_AddStringToObject(wlan_state,"SSID",GetSysVerInfo_WlanCurSsid());
	sprintf(temp,"%ddb",GetSysVerInfo_WifiSignalLev());
	cJSON_AddStringToObject(wlan_state,"Signal Level",temp);
	cJSON_AddStringToObject(wlan_state,"MAC",GetSysVerInfo_mac_by_device(NET_WLAN0));
	cJSON_AddStringToObject(wlan_state,"IP",GetSysVerInfo_IP_by_device(NET_WLAN0));
	cJSON_AddStringToObject(wlan_state,"MASK",GetSysVerInfo_mask_by_device(NET_WLAN0));
	cJSON_AddStringToObject(wlan_state,"GATEWAY",GetSysVerInfo_gateway_by_device(NET_WLAN0));
	//printf("Get_NetState_Para55555555555555555\n");
	para_string=cJSON_Print(root);
	printf("%s\n",para_string);
	cJSON_Delete(root);
	return para_string;
}
cJSON *NetConfig_Para=NULL;
cJSON *LanConfig_Para=NULL;
cJSON *WlanConfig_Para=NULL;
cJSON *NetmodeConfig_Para=NULL;
void API_NetConfigPara_LoadFromIO(void)
{
	cJSON *io_para;
	cJSON *one_para;
	char temp[500];
	if(NetConfig_Para!=NULL)
		cJSON_Delete(NetConfig_Para);
	NetConfig_Para=cJSON_CreateObject();
	if(NetConfig_Para!=NULL)
	{
		LanConfig_Para=cJSON_CreateObject();
		WlanConfig_Para=cJSON_CreateObject();
		NetmodeConfig_Para=cJSON_CreateObject();
		if(NetmodeConfig_Para!=NULL)
		{
			cJSON_AddItemToObject(NetConfig_Para,"Net Mode",NetmodeConfig_Para);
			io_para=cJSON_AddObjectToObject(NetmodeConfig_Para,"IO_PARA");
			if(io_para!=NULL)
			{
				API_Event_IoServer_InnerRead_All(IX_NET_MODE, temp);
				cJSON_AddStringToObject(io_para,IX_NET_MODE,temp);
			}
		}
		if(LanConfig_Para!=NULL)
		{
			cJSON_AddItemToObject(NetConfig_Para,"Lan Config",LanConfig_Para);
			io_para=cJSON_AddObjectToObject(LanConfig_Para,"IO_PARA");
			if(io_para!=NULL)
			{
				API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
				cJSON_AddStringToObject(io_para,DHCP_ENABLE,temp);
				#if 0
				one_para=cJSON_CreateObject();
				if(one_para!=NULL)
				{
					cJSON_AddStringToObject(one_para,DHCP_ENABLE,temp);
					cJSON_AddItemToArray(io_para,one_para);
				}
				#endif
				API_Event_IoServer_InnerRead_All(Autoip_IPSegment, temp);
				cJSON_AddStringToObject(io_para,Autoip_IPSegment,temp);
				
				API_Event_IoServer_InnerRead_All(Autoip_IPGateway, temp);
				cJSON_AddStringToObject(io_para,Autoip_IPGateway,temp);
				
				API_Event_IoServer_InnerRead_All(Autoip_IPMask, temp);
				cJSON_AddStringToObject(io_para,Autoip_IPMask,temp);
				
				API_Event_IoServer_InnerRead_All(IP_Address, temp);
				cJSON_AddStringToObject(io_para,IP_Address,temp);

				API_Event_IoServer_InnerRead_All(SubnetMask, temp);
				cJSON_AddStringToObject(io_para,SubnetMask,temp);

				API_Event_IoServer_InnerRead_All(DefaultRoute, temp);
				cJSON_AddStringToObject(io_para,DefaultRoute,temp);
			}
		}
		if(WlanConfig_Para!=NULL)
		{
			cJSON_AddItemToObject(NetConfig_Para,"Wlan Config",WlanConfig_Para);
			io_para=cJSON_AddObjectToObject(WlanConfig_Para,"IO_PARA");
			if(io_para!=NULL)
			{
				API_Event_IoServer_InnerRead_All(WIFI_SWITCH, temp);
				cJSON_AddStringToObject(io_para,WIFI_SWITCH,temp);

				API_Event_IoServer_InnerRead_All(WIFI_SSID_PARA, temp);
				one_para=cJSON_Parse(temp);
				cJSON_AddItemToObject(io_para,WIFI_SSID_PARA,one_para);
			}
		}
		
		char *string=cJSON_Print(NetConfig_Para);
		if(string!=NULL)
		{
			SetJsonStringToFile("/mnt/nfs/netConfig.txt",string);
			printf("%s\n",string);
			#if 0
			NetConfig_Para=cJSON_Parse(string);
			WlanConfig_Para=cJSON_GetObjectItemCaseSensitive(NetConfig_Para,"Wlan Config");
			io_para=cJSON_GetObjectItemCaseSensitive(WlanConfig_Para,"IO_PARA");
			#endif
			free(string);
		}
	}
}

int API_NetConfigPara_WriteAll(int write_io,char *json_para)
{
	
}
int API_NetConfigPara_ReadAll(char *json_para)
{
	
}
int API_NetConfigPara_EffectAll(void)
{

}
int API_NetConfigPara_WriteNetMode(int write_io,int net_mode)
{
	
}

int API_NetConfigPara_ReadNetMode(void)
{
	
}
int API_NetConfigPara_EffectNetMode(void)
{

}
int API_NetConfigPara_WriteLan(int write_io,int dhcp_en,char *ip,char *mask,char *gw)
{
	
}

int API_NetConfigPara_ReadLan(int *dhcp_en,char *ip,char *mask,char *gw)
{
	
}
int API_NetConfigPara_EffectLan(void)
{

}
int API_NetConfigPara_WriteWlan(int write_io,int sw,char *ssid_setting)
{
	
}

int API_NetConfigPara_ReadWlan(int *sw,char *ssid_setting)
{
	
}

int API_NetConfigPara_EffectWlan(void)
{
	
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
