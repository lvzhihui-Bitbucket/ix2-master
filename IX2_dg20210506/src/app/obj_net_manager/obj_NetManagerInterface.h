/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_NetManagerInterface_H
#define _obj_NetManagerInterface_H

typedef enum
{
	NM_MODE_LAN=0,
	NM_MODE_WLAN,
}NM_MODE_E;

typedef enum
{
	LAN_MODE_STATIC=0,
	LAN_MODE_DHCP_AUTOIP,
}LAN_MODE_E;

const char *api_nm_if_get_lan_ip(void);
const char *api_nm_if_get_lan_gw(void);
const char *api_nm_if_get_lan_mask(void);


void api_nm_if_get_lan_autoip_segment(char *ip_segment);


void api_nm_if_get_lan_autoip_gw(char *gw);

void api_nm_if_get_lan_autoip_mask(char *mask);

const char *api_nm_if_get_wlan_ip(void);
const char *api_nm_if_get_wlan_gw(void);
const char *api_nm_if_get_wlan_mask(void);

int api_nm_if_judge_include_dev(const char *rm_num,int target_ip);
#endif
