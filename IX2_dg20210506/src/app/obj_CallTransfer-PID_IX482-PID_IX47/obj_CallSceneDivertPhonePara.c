
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include "obj_CallSceneDivertPhonePara.h"
#include "obj_ImNameListTable.h"
#include "obj_IpCacheTable.h"

#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
#include "obj_MsSyncCallScene.h"
//#include"obj_AlarmingZone.h"
//#include"obj_AlarmingPara.h"

#define CallSceneDivertPhonePara_FILE_NAME			"/mnt/nand1-2/Settings/CallSceneDivertPhonePara.txt"


cJSON *CallSceneDivertPhoneRoot =NULL;

#define CSDPara_Key_WorkMode			"WORK_MODE"




void CallSceneDivertPhoneParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	int scene_type;
	CallSceneDivertPhoneRoot =NULL;
	if((file=fopen(CallSceneDivertPhonePara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 CallSceneDivertPhoneRoot = cJSON_Parse(json);
		 free(json);
		
	}
	
	 if(CallSceneDivertPhoneRoot == NULL)
	 {
		CallSceneDivertPhoneRoot = cJSON_CreateObject();
	 }
	 if(CallSceneDivertPhoneRoot!=NULL)
	 {
		json_node = cJSON_GetObjectItemCaseSensitive(CallSceneDivertPhoneRoot, CSDPara_Key_WorkMode);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallSceneDivertPhoneRoot, CSDPara_Key_WorkMode);
			cJSON_AddNumberToObject(CallSceneDivertPhoneRoot,CSDPara_Key_WorkMode,CallSceneDivertPhone_Mode_Always);
		}
		
	 }
}

void SaveCallSceneDivertPhoneParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(CallSceneDivertPhoneRoot);
	if(string!=NULL)
	{
		if((file=fopen(CallSceneDivertPhonePara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("CallSceneDivertPhonePara:\n%s\n",string);
		free(string);

	}
}


int Get_CallSceneDivertPhonePara_WorkMode(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneDivertPhoneRoot,CSDPara_Key_WorkMode);
	if(cJSON_IsNumber(one_node))
	{
		rev=one_node->valuedouble;
	}
	
	return rev;
}

int Set_CallSceneDivertPhonePara_WorkMode(int new_mode)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneDivertPhoneRoot,CSDPara_Key_WorkMode);
	if(cJSON_IsNumber(one_node))
	{
		rev=cJSON_SetNumberValue(one_node, new_mode);
	}
	
	return rev;
}


void CallSceneDivertPhoneParaMenuSettingSave(void)
{
	SaveCallSceneDivertPhoneParaToFile();
	//API_Alarming_Para_Update();
	#if 0
	char *string;
	cJSON *para_obj = cJSON_CreateObject();
		
	if(para_obj)
	{
		cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_DivertPhone,CallSceneDivertPhoneRoot);
		string=cJSON_Print(para_obj);
		if(string)
		{
			API_MsSyncCallScene2Set(string);
			free(string);
		}
		cJSON_Delete(para_obj);
		
	}
	#endif
	//API_MsSyncCallScene2Set();
}



int CallSceneDivertPhoneParaRootMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			//strcpy(disp,"Always");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeAllways, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 1:
			//strcpy(disp,"If no answer");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeIfNoAnswer, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	if(Get_CallSceneDivertPhonePara_WorkMode()==index)
		rev = 1;
	return rev;
}

int CallSceneDivertPhoneParaRootMenuValueDisp(int index,char *disp,int *unicode_len)
{
	char buff[20];
	switch(index)
	{
	
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallSceneDivertPhoneParaRootMenuSlecet(int index)
{
	int rev = 1;
	if(index<2&& index!=Get_CallSceneDivertPhonePara_WorkMode())
	{
		Set_CallSceneDivertPhonePara_WorkMode(index);
		JsonParaSettingDispReflush();
		Set_JsonParaSettingHaveChange(1);
	}
	
	return rev;
}

int CallSceneDivertPhoneParaRootMenuReturn(void)
{
	popDisplayLastMenu();
	return 0;
}

void Enter_CallSceneDivertPhoneParaSettingRootMenu(int select)
{
	ParaMenu_T para_menu;
	
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	
	Set_JsonParaSettingCurItemNums(2);

		
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(1);
	
	Set_JsonParaSettingToKeypad(0);
	//GetParaMenuRecord(Alarming_Setting, &para_menu);
	//para_menu.name[para_menu.nameLen]= 0;
	//Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	//Set_JsonParaSettingTitle(0,"Mode");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneMode, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	Set_JsonParaSettingMenuSaveFunc(CallSceneDivertPhoneParaMenuSettingSave);
	
	Set_JsonParaSettingMenuItemDispFunc(CallSceneDivertPhoneParaRootMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallSceneDivertPhoneParaRootMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallSceneDivertPhoneParaRootMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallSceneDivertPhoneParaRootMenuReturn);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		JsonParaSettingDispReflush();
	}
}

