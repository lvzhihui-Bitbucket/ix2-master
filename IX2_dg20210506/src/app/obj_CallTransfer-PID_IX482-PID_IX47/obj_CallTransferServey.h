/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_CallTransferSurvey_H
#define _obj_CallTransferSurvey_H
typedef struct
{
	VDP_MSG_HEAD	head;
}CallTransferMsg_T;

typedef enum
{
	CallTransferMsg_Start=0,
	CallTransferMsg_Close,
	CallTransferMsg_TimerTrigger,
}CallTransferMsg_E;

typedef struct
{
	int state;
	int timer;
	//int ArmMode;
}CallTransferSurveyRun_t;

typedef enum
{
	CallTransferSurvey_State_Idle=0,
	CallTransferSurvey_State_Running,
}CallTransferSurvey_State_E;


#endif


