
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include"obj_CallSceneAutoModePara.h"
#include "obj_ImNameListTable.h"
#include "obj_IpCacheTable.h"

#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
//#include"obj_AlarmingZone.h"
//#include"obj_AlarmingPara.h"

#define CallSceneAutoModePara_FILE_NAME			"/mnt/nand1-2/Settings/CallSceneAutoModePara.txt"


cJSON *CallSceneAutoModeRoot =NULL;

#define CSAPara_Key_StartTime			"START_TIME"
#define CSAPara_Key_EndTime		"END_TIME"

char automode_editbuff[8];

void CallSceneAutoModeParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	int scene_type;
	CallSceneAutoModeRoot =NULL;
	if((file=fopen(CallSceneAutoModePara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 CallSceneAutoModeRoot = cJSON_Parse(json);
		 free(json);
		
	}
	
	 if(CallSceneAutoModeRoot == NULL)
	 {
		CallSceneAutoModeRoot = cJSON_CreateObject();
	 }
	 if(CallSceneAutoModeRoot!=NULL)
	 {
		json_node = cJSON_GetObjectItemCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_StartTime);
		if(json_node == NULL||!cJSON_IsString(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_StartTime);
			cJSON_AddStringToObject(CallSceneAutoModeRoot,CSAPara_Key_StartTime,"0000");
		}
		
		json_node = cJSON_GetObjectItemCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_EndTime);
		if(json_node == NULL||!cJSON_IsString(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_EndTime);
			cJSON_AddStringToObject(CallSceneAutoModeRoot,CSAPara_Key_EndTime,"0800");
		}
		
		
	 }
}

void SaveCallSceneAutoModeParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(CallSceneAutoModeRoot);
	if(string!=NULL)
	{
		if((file=fopen(CallSceneAutoModePara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("CallSceneCurPara:\n%s\n",string);
		free(string);
	}
}

int Get_CallSceneAutoMode_StartTime(char *start)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneAutoModeRoot,CSAPara_Key_StartTime);
	if(cJSON_IsString(one_node))
	{
		strcpy(start,one_node->valuestring);
	}
	
	return rev;
}
int Set_CallSceneAutoMode_StartTime(char *start)
{
	int rev = 1;
	cJSON *one_node;
	int i;
	
	if(strlen(start)!=4)
		return -1;
	for(i=0;i<4;i++)
	{
		if(start[i]>'9'||start[i]<'0')
			return 0;
	}
	
	if(start[0]>'2'||start[2]>'6'||(start[0]=='2'&&start[1]>'3')||(start[2]=='6'&&start[3]>'0'))
		return 0;
	
	cJSON_DeleteItemFromObjectCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_StartTime);
	cJSON_AddStringToObject(CallSceneAutoModeRoot,CSAPara_Key_StartTime,start);
	Set_JsonParaSettingHaveChange(1);
	return rev;
}

int Get_CallSceneAutoMode_EndTime(char *end)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneAutoModeRoot,CSAPara_Key_EndTime);
	if(cJSON_IsString(one_node))
	{
		strcpy(end,one_node->valuestring);
	}
	
	return rev;
}
int Set_CallSceneAutoMode_EndTime(char *end)
{
	int rev = 1;
	cJSON *one_node;
	int i;
	
	if(strlen(end)!=4)
		return -1;
	for(i=0;i<4;i++)
	{
		if(end[i]>'9'||end[i]<'0')
			return 0;
	}
	
	if(end[0]>'2'||end[2]>'6'||(end[0]=='2'&&end[1]>'3')||(end[2]=='6'&&end[3]>'0'))
		return 0;
	
	cJSON_DeleteItemFromObjectCaseSensitive(CallSceneAutoModeRoot, CSAPara_Key_EndTime);
	cJSON_AddStringToObject(CallSceneAutoModeRoot,CSAPara_Key_EndTime,end);
	Set_JsonParaSettingHaveChange(1);
	return rev;
}

int IfCallSceneAutoModeInRunning(void)
{
	
	
	int start_h,start_m,end_h,end_m;
	char buff1[5],buff2[3]={0};
	int across_day = 0;
	Get_CallSceneAutoMode_StartTime(buff1);
	memcpy(buff2,buff1,2);
	start_h=atoi(buff2);
	start_m=atoi(buff1+2);
	Get_CallSceneAutoMode_EndTime(buff1);
	memcpy(buff2,buff1,2);
	end_h=atoi(buff2);
	end_m=atoi(buff1+2);

	if(end_h<start_h||((end_h==start_h)&&(end_m<start_m)))
		across_day = 1;
	
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	if(across_day)
	{
		if(tblock->tm_hour>start_h||(tblock->tm_hour==start_h&&tblock->tm_min>=start_m)||
			tblock->tm_hour<end_h||(tblock->tm_hour==end_h&&tblock->tm_min<=end_m))
			return 1;
	}
	else
	{
		if((tblock->tm_hour>start_h||(tblock->tm_hour==start_h&&tblock->tm_min>=start_m))&&
			(tblock->tm_hour<end_h||(tblock->tm_hour==end_h&&tblock->tm_min<=end_m)))
			return 1;
	}
	return 0;
}

void CallSceneAutoModeParaMenuSettingSave(void)
{
	SaveCallSceneAutoModeParaToFile();
	//API_Alarming_Para_Update();
}



int CallSceneAutoModeParaRootMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			//strcpy(disp,"Switch");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAutoModeSw, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 1:
			//strcpy(disp,"Start time");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAutoStartTime, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 2:
			//strcpy(disp,"End time");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAutoEndTime, NULL, 0, NULL, 0, disp, unicode_len);
			break;		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return rev;
}

int CallSceneAutoModeParaRootMenuValueDisp(int index,char *disp,int *unicode_len)
{
	char buff[20];
	switch(index)
	{
		case 0:
			if(Get_CallSceneCurPara_AutoMode())
			{
				strcpy(disp,"ON");
			}
			else
			{
				strcpy(disp,"OFF");
			}
			*unicode_len = 0;
			break;
		case 1:
			Get_CallSceneAutoMode_StartTime(buff);
			memcpy(disp,buff,2);
			disp[2]=':';
			memcpy(disp+3,buff+2,2);
			disp[5]=0;
			*unicode_len = 0;
			break;
		case 2:
			Get_CallSceneAutoMode_EndTime(buff);
			memcpy(disp,buff,2);
			disp[2]=':';
			memcpy(disp+3,buff+2,2);
			disp[5]=0;
			*unicode_len = 0;
			break;
		
		default:
	
			break;
	}
	return 0;
}

int CallSceneAutoModeParaRootMenuSlecet(int index)
{
	int rev = 0;
	
	switch(index)
	{
		case 0:
			if(Get_CallSceneCurPara_AutoMode())
			{
				Set_CallSceneCurPara_AutoMode(0);
			}
			else
			{
				Set_CallSceneCurPara_AutoMode(1);
			}
			//Set_JsonParaSettingHaveChange(1);
			Enter_CallSceneAutoModeParaSettingRootMenu(0);
			break;
		case 1:
			//Enter_AlarmingParaSettingZoneArrayMenu(0,index);
			Set_JsonParaSettingToKeypad(1);
			Get_CallSceneAutoMode_StartTime(automode_editbuff);
			EnterKeypadMenu(KEYPAD_NUM,0, automode_editbuff, 20, COLOR_WHITE, automode_editbuff, 1, Set_CallSceneAutoMode_StartTime);
			break;
		case 2:
			//Enter_AlarmingParaSettingArmModeArrayMenu(0,index);
			Set_JsonParaSettingToKeypad(1);
			Get_CallSceneAutoMode_EndTime(automode_editbuff);
			EnterKeypadMenu(KEYPAD_NUM,0, automode_editbuff, 20, COLOR_WHITE, automode_editbuff, 1, Set_CallSceneAutoMode_EndTime);
			break;
		case 4:
			break;
	}
	return rev;
}

int CallSceneAutoModeParaRootMenuReturn(void)
{
	//popDisplayLastMenu();
	Enter_CallTransferParaSettingRootMenu(0);
	return 0;
}

void Enter_CallSceneAutoModeParaSettingRootMenu(int select)
{
	ParaMenu_T para_menu;
	bprintf("111111 %d\n",select);
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	
	Set_JsonParaSettingCurItemNums(3);

		
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	//GetParaMenuRecord(Alarming_Setting, &para_menu);
	//para_menu.name[para_menu.nameLen]= 0;
	//Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	//Set_JsonParaSettingTitle(0,"Auto Mode");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneAutoMode, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	//Set_JsonParaSettingMenuSaveFunc(CallSceneAutoModeParaMenuSettingSave);
	
	Set_JsonParaSettingMenuItemDispFunc(CallSceneAutoModeParaRootMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallSceneAutoModeParaRootMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallSceneAutoModeParaRootMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallSceneAutoModeParaRootMenuReturn);
	bprintf("111111 %d\n",select);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		bprintf("111111 %d\n",select);
		JsonParaSettingDispReflush();
	}
}

