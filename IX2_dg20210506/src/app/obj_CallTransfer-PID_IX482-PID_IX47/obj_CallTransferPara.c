
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include"obj_CallTransferPara.h"
#include "obj_ImNameListTable.h"
#include "obj_IpCacheTable.h"
#include "obj_GetInfoByIp.h"

#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
#include "obj_MsSyncCallScene.h"
//#include"obj_AlarmingZone.h"
//#include"obj_AlarmingPara.h"

#define CallTransferPara_FILE_NAME			"/mnt/nand1-2/Settings/CallTransferPara.txt"


cJSON *CallTransferRoot =NULL;

//#define CTPara_Key_Switch			"SWITCH"
//#define CTPara_Key_StartupMode	"STARTUP_MODE"
#define CTPara_Key_OpenTime		"OPEN_TIME"
#define CTPara_Key_CloseTime		"CLOSE_TIME"
#define CTPara_Key_WorkMode		"WORK_MODE"
#define CTPara_Key_WaitingTime		"WAITING_TIME"
#define CTPara_Key_DivertList		"DIVERT_LIST"
#define CTPara_Key_PermitList		"PERMIT_LIST"
#define CTPara_Key_State			"STATE"
#define CTPara_Key_Mode			"MODE"
#define CTPara_Key_TargetInfo		"TARGET_INFO"
#define CTPara_Key_TargetNum		"TARGET_NUM"
#define CTPara_Key_TargetName		"TARGET_NAME"
#define CTPara_Key_DevArray		"DEV_ARRAY"
#define CTPara_Key_DevNum		"DEV_NUM"
#define CTPara_Key_DevAddr		"DEV_ADDR"
//char *json_test=0;
//int json_size_test=0;
void CallTransferParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	CallTransferRoot =NULL;
	if((file=fopen(CallTransferPara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			printf("!!!!!!!!!!!!!!!!CallTransferParaReload err1\n");
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);
		 CallTransferRoot = cJSON_Parse(json);
		
		// free(json);
		
	}
	
	 if(CallTransferRoot == NULL)
	 {
		CallTransferRoot = cJSON_CreateObject();
	 }
	 if(CallTransferRoot!=NULL)
	 {
	 	#if 0
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot, CTPara_Key_Switch);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_Switch);
			cJSON_AddNumberToObject(CallTransferRoot,CTPara_Key_Switch,0);
		}
		
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot, CTPara_Key_State);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_State);
			cJSON_AddNumberToObject(CallTransferRoot,CTPara_Key_State,0);
		}
		#endif
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot, CTPara_Key_WorkMode);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_WorkMode);
			cJSON_AddNumberToObject(CallTransferRoot,CTPara_Key_WorkMode,CallTransfer_Mode_Always);
		}
		#if 0
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot, CTPara_Key_StartupMode);
		if(json_node == NULL||!cJSON_IsObject(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_StartupMode);
			json_node = cJSON_AddObjectToObject(CallTransferRoot,CTPara_Key_StartupMode);
			cJSON_AddNumberToObject(json_node,CTPara_Key_Mode,CallTransfer_StartupMode_Manual);
			cJSON_AddStringToObject(json_node,CTPara_Key_OpenTime,"2200");
			cJSON_AddStringToObject(json_node,CTPara_Key_CloseTime,"0800");
		}
		#endif
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot, CTPara_Key_DivertList);
		if(json_node == NULL||!cJSON_IsArray(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_DivertList);
			cJSON_AddArrayToObject(CallTransferRoot,CTPara_Key_DivertList);
		}
		//if(json_node!=NULL)
		//	printf("!!!!!!!!!**************buff=%08x,size%08x,pp=%08x\n",json_test,json_size_test,json_node->string);
	 }
}

void SaveCallTransferParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(CallTransferRoot);
	if(string!=NULL)
	{
		if((file=fopen(CallTransferPara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("CallTransferPara:\n%s\n",string);
		free(string);
	}
}
#if 0
int Get_CallTransferPara_Switch(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_Switch);
	if(cJSON_IsNumber(one_node))
	{
		rev = one_node->valuedouble;
	}
	
	return rev;
}

int Set_CallTransferPara_Switch(int new_val)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_Switch);
	if(cJSON_IsNumber(one_node))
	{
		rev = cJSON_SetNumberValue(one_node, new_val);
	}
	
	return rev;
}
#endif
int GetCallTransferParaTargetNum(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_DivertList);
	if(cJSON_IsArray(one_node))
	{
		rev = cJSON_GetArraySize(one_node);
	}
	
	return rev;
}
int GetCallTransferParaTargetCommon(cJSON *tar_list,int index,char *num,char *name)
{
	cJSON *one_node;
	one_node = cJSON_GetArrayItem(tar_list,index);
	//printf("!!!!!!!!!**************buff=%08x,size%08x,pp=%08x\n",json_test,json_size_test,);
	if(num!=NULL)
		ParseJsonString(one_node,CTPara_Key_TargetNum,num,10);
	if(name!=NULL)
		ParseJsonString(one_node,CTPara_Key_TargetName,name,20);
	return 0;
}
int GetCallTransferParaTarget(int index,char *num,char *name)
{
	//strcpy(name,"xxxxx");
	//strcpy(num,"00990003");
	int rev = 0;
	cJSON *tar_list;
	cJSON *one_node;
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_DivertList);
	if(!cJSON_IsArray(tar_list)||index>=cJSON_GetArraySize(tar_list))
	{
		rev =-1;
	}
	else
	{
		rev=GetCallTransferParaTargetCommon(tar_list,index,num,name);
	}
	
	return rev;
}
int AddOneCallTransferParaTargetCommon(cJSON *tar_list,char *num,char *name)
{
	cJSON *one_node;
	
	one_node = cJSON_CreateObject();
	
	if(one_node==NULL)
		return -1;
	cJSON_AddItemToArray(tar_list,one_node);
	cJSON_AddStringToObject(one_node,CTPara_Key_TargetNum,num);
	cJSON_AddStringToObject(one_node,CTPara_Key_TargetName,name);
	return 0;
}
int AddOneCallTransferParaTarget(char *num,char *name)
{
	cJSON *tar_list;
	
	
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_DivertList);
	
	if(tar_list == NULL||!cJSON_IsArray(tar_list))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_DivertList);
		cJSON_AddArrayToObject(CallTransferRoot,CTPara_Key_DivertList);
	}
	
	return AddOneCallTransferParaTargetCommon(tar_list,num,name);
}

int DelOneCallTransferParaTarget(char *num)
{
	int rev = 0;
	cJSON *tar_list;
	cJSON *one_node;
	char buff[20];
	int i;
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_DivertList);
	if(tar_list==NULL||!cJSON_IsArray(tar_list))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(CallTransferRoot, CTPara_Key_DivertList);
		cJSON_AddArrayToObject(CallTransferRoot,CTPara_Key_DivertList);
		return 0;
	}
	for(i=0;i<cJSON_GetArraySize(tar_list);i++)
	{
		one_node = cJSON_GetArrayItem(tar_list,i);
		ParseJsonString(one_node,CTPara_Key_TargetNum,buff,10);
		if(strcmp(buff,num)==0)
		{
			cJSON_DeleteItemFromArray(tar_list,i);
			break;
		}
	}
	
	return 0;
}

int JustIfCallTransferTarget(char *num)
{
	int i;
	char tar_num[20];
	
	for(i = 0;i<GetCallTransferParaTargetNum();i++)
	{
		GetCallTransferParaTarget(i,tar_num,NULL);
		if(strcmp(num,tar_num)==0)
			return 1;
	}
	return 0;
}

cJSON *EditDivertList_JsonArray=NULL;

void LoadEditDivertListJsonArray(void)
{
	int i;
	char num[20];
	char name[41];
	IM_NameListRecord_T record;
	
	if(EditDivertList_JsonArray!=NULL)
	{
		cJSON_Delete(EditDivertList_JsonArray);
		EditDivertList_JsonArray=NULL;
	}
	EditDivertList_JsonArray = cJSON_CreateArray();
	
	for(i=0;i<GetCallTransferParaTargetNum();i++)
	{
		GetCallTransferParaTarget(i,num,name);
		AddOneCallTransferParaTargetCommon(EditDivertList_JsonArray,num,name);
	}
	
	for(i=0;i<GetImNameListRecordCnt()||(GetImNameListRecordCnt()==0&&i<GetImNamelistTempTableNum());i++)
	{
		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(i,&record) != 0)
				continue;
		}
		else
		{
			if(GetImNameListRecordItems(i,	&record) != 0)
				continue;
		}
		if(JustIfCallTransferTarget(record.BD_RM_MS)==0)
		{
			AddOneCallTransferParaTargetCommon(EditDivertList_JsonArray,record.BD_RM_MS,strcmp(record.R_Name,"-")==0?record.name1:record.R_Name);
		}	
	}
}

void ReleaseEditDivertListJsonArray(void)
{
	if(EditDivertList_JsonArray!=NULL)
	{
		cJSON_Delete(EditDivertList_JsonArray);
		EditDivertList_JsonArray=NULL;
	}
}

int GetEditDivertListJsonArrayNum(void)
{
	return cJSON_GetArraySize(EditDivertList_JsonArray);
}

int GetOneEditDivertListJsonArray(int index,char *num,char *name)
{
	//strcpy(name,"xxxxx");
	//strcpy(num,"00990003");
	int rev = 0;
	
	
	rev=GetCallTransferParaTargetCommon(EditDivertList_JsonArray,index,num,name);
	
	
	return rev;
}

int GetCallTransferParaMode(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_WorkMode);
	if(cJSON_IsNumber(one_node))
	{
		rev = one_node->valuedouble;
	}
	
	return rev;
}

int Set_CallTransferParaMode(int new_val)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_WorkMode);
	if(cJSON_IsNumber(one_node))
	{
		rev = cJSON_SetNumberValue(one_node, new_val);
	}
	
	return rev;
}
int GetCallTransferWaitAnswerTime(void)
{
	int rev = 30;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_WaitingTime);
	if(cJSON_IsNumber(one_node))
	{
		rev = one_node->valuedouble;
	}
	
	return rev;
}
int Set_CallTransferWaitAnswerTime(int new_val)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferRoot,CTPara_Key_WaitingTime);
	if(one_node==NULL)
	{
		cJSON_AddNumberToObject(CallTransferRoot,CTPara_Key_WaitingTime,new_val);
	}
	else if(cJSON_IsNumber(one_node))
	{
		rev = cJSON_SetNumberValue(one_node, new_val);
	}
	
	return rev;
}
//extern cJSON *CallTransferTargetList;
//extern cJSON *CallSceneNoDisturbRoot;
//extern cJSON *CallSceneDivertPhoneRoot;
extern cJSON *CallSceneAutoModeRoot;
extern cJSON *CallTransferPermitRoot;


void CallTransferParaMenuSettingSave(void)
{
	cJSON *para_obj;
	char *para_str;
	SaveCallTransferParaToFile();
	SaveCallSceneAutoModeParaToFile();
	SaveCallTransferPermitParaToFile();
	//API_Alarming_Para_Update();
	#if 0
	para_obj = cJSON_CreateObject();
	if(para_obj)
	{
		cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_DivertDev,CallTransferRoot);
		cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_AutoMode,CallSceneAutoModeRoot);
		cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_DivertDevAllow,CallTransferPermitRoot);
		para_str=cJSON_Print(para_obj);
		if(para_str)
		{
			API_MsSyncCallScene2Set(para_str);
			free(para_str);
		}
		cJSON_Delete(para_obj);
		
	}
	#endif
	//API_MsSyncCallScene2Set();
}



int CallTransferParaRootMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			//strcpy(disp,"Auto");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAutoMode, NULL, 0, NULL, 0, disp, unicode_len);	
			break;
		case 1:
			//strcpy(disp,"Mode");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneMode, NULL, 0, NULL, 0, disp, unicode_len);
			break;		
		case 2:
			//strcpy(disp,"Divert list");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneDivertList, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 3:
			//strcpy(disp,"Allow List");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowList, NULL, 0, NULL, 0, disp, unicode_len);
			break;	
		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return rev;
}

int CallTransferParaRootMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		case 0:
			if(Get_CallSceneCurPara_AutoMode())
			{
				strcpy(disp,"ON");
			}
			else
			{
				strcpy(disp,"OFF");
			}
			*unicode_len = 0;
			break;
		case 1:
			//strcpy(disp,"Always");
			//*unicode_len = 0;
			if(GetCallTransferParaMode()==CallTransfer_Mode_IfNoanswer)
				API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeIfNoAnswer, NULL, 0, NULL, 0, disp, unicode_len);
			else
				API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeAllways, NULL, 0, NULL, 0, disp, unicode_len);
			break;	
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallTransferParaRootMenuSlecet(int index)
{
	int rev = 0;
	bprintf("111111 %d\n",index);
	switch(index)
	{
		case 0:
			bprintf("111111 %d\n",index);
			Enter_CallSceneAutoModeParaSettingRootMenu(0);
			break;
		case 1:
			Enter_CallTransferWorkModeMenu(0);
			break;
		case 2:
			bprintf("111111 %d\n",index);
			Enter_CallTransferParaSettingDiverlistMenu(0);
			break;
		case 3:
			//Enter_AlarmingParaSettingZoneArrayMenu(0,index);
			bprintf("111111 %d\n",index);
			Enter_CallTransferPermitParaSettingRootMenu(0);
			break;	
	}
	return rev;
}

int CallTransferParaRootMenuReturn(void)
{
	popDisplayLastMenu();
	return 0;
}

void Enter_CallTransferParaSettingRootMenu(int select)
{
	ParaMenu_T para_menu;

	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(CallTransferRoot);
	Set_JsonParaSettingCurNodeIndex(0);
	
	Set_JsonParaSettingCurItemNums(memcmp(GetSysVerInfo_BdRmMs()+8,"01",2)==0?4:3);

		
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	//GetParaMenuRecord(Alarming_Setting, &para_menu);
	//para_menu.name[para_menu.nameLen]= 0;
	//Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	//Set_JsonParaSettingTitle(0,"Divert to device");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_DivertToDev, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	Set_JsonParaSettingMenuSaveFunc(CallTransferParaMenuSettingSave);
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferParaRootMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferParaRootMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferParaRootMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferParaRootMenuReturn);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		JsonParaSettingDispReflush();
	}
}
int CallTransferWorkModeMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			//strcpy(disp,"Switch");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneMode, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 1:
			//strcpy(disp,"Start time");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneWaitingTime, NULL, 0, NULL, 0, disp, unicode_len);
			break;
	
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return rev;
}
int CallTransferWorkModeMenuValueDisp(int index,char *disp,int *unicode_len)
{
	char buff[20];
	switch(index)
	{
		case 0:
			if(GetCallTransferParaMode()==CallTransfer_Mode_IfNoanswer)
				API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeIfNoAnswer, NULL, 0, NULL, 0, disp, unicode_len);
			else
				API_GetOSD_StringWithID(MESG_TEXT_CallSceneModeAllways, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 1:
			sprintf(disp,"%d",GetCallTransferWaitAnswerTime());
			
			*unicode_len = 0;
			break;
		
		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}
int Save_CallTransferWaitAnswerTime(char *time)
{
	int rev = 1;
	
	int i;
	int len;
	int temp;
	if((len=strlen(time))>2)
		return -1;
	for(i=0;i<len;i++)
	{
		if(time[i]>'9'||time[i]<'0')
			return 0;
	}
	temp=atoi(time);
	if(temp<5||temp>40)
		return 0;
	Set_CallTransferWaitAnswerTime(temp);
	Set_JsonParaSettingHaveChange(1);
	return rev;
}
int CallTransferWorkModeMenuSlecet(int index)
{
	int rev = 0;
	static char editbuff[5];
	switch(index)
	{
		case 0:
			if(GetCallTransferParaMode()==CallTransfer_Mode_IfNoanswer)
			{
				Set_CallTransferParaMode(CallTransfer_Mode_Always);
			}
			else
			{
				Set_CallTransferParaMode(CallTransfer_Mode_IfNoanswer);
			}
			Set_JsonParaSettingHaveChange(1);
			Enter_CallTransferWorkModeMenu(0);
			break;
		case 1:
			//Enter_AlarmingParaSettingZoneArrayMenu(0,index);
			Set_JsonParaSettingToKeypad(1);
			sprintf(editbuff,"%d",GetCallTransferWaitAnswerTime());
			EnterKeypadMenu(KEYPAD_NUM,0, editbuff, 2, COLOR_WHITE, editbuff, 1, Save_CallTransferWaitAnswerTime);
			break;
		
		
	}
	return rev;
}

int CallTransferWorkModeMenuReturn(void)
{
	//popDisplayLastMenu();
	Enter_CallTransferParaSettingRootMenu(0);
	return 0;
}
void Enter_CallTransferWorkModeMenu(int select)
{
	ParaMenu_T para_menu;
	bprintf("111111 %d\n",select);
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	
	Set_JsonParaSettingCurItemNums(2);

		
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	//GetParaMenuRecord(Alarming_Setting, &para_menu);
	//para_menu.name[para_menu.nameLen]= 0;
	//Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	//Set_JsonParaSettingTitle(0,"Auto Mode");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneMode, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	//Set_JsonParaSettingMenuSaveFunc(CallSceneAutoModeParaMenuSettingSave);
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferWorkModeMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferWorkModeMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferWorkModeMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferWorkModeMenuReturn);
	bprintf("111111 %d\n",select);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		bprintf("111111 %d\n",select);
		JsonParaSettingDispReflush();
	}
}

int CallTransferParaDiverlistMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	if(index ==0)
	{
		//strcpy(disp,"Select by namelist");
		//*unicode_len = 0;
		API_GetOSD_StringWithID(MESG_TEXT_CallSceneSelectByNamelist, NULL, 0, NULL, 0, disp, unicode_len);
	}
	else if(index<=GetCallTransferParaTargetNum())
	{
		GetCallTransferParaTarget(index-1,tar_num,tar_name);
		get_device_addr_and_name_disp_str(0,tar_num, NULL, NULL, tar_name, disp);
		*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferParaDiverlistMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallTransferParaDiverlistMenuSlecet(int index)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			Enter_CallTransferParaSettingEditDiverlistMenu(0);
			break;
	}
	return rev;
}

int CallTransferParaDiverlistMenuReturn(void)
{
	Enter_CallTransferParaSettingRootMenu(0);
	return 0;
}

void Enter_CallTransferParaSettingDiverlistMenu(int select)
{
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	Set_JsonParaSettingCurItemNums(GetCallTransferParaTargetNum()+1);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	//Set_JsonParaSettingTitle(0,"Divert list");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneDivertList, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferParaDiverlistMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferParaDiverlistMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferParaDiverlistMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferParaDiverlistMenuReturn);
		
	JsonParaSettingDispReflush();
}

int CallTransferParaEditDiverlistMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	IM_NameListRecord_T record;
	if(GetOneEditDivertListJsonArray(index,tar_num,tar_name) == 0)
	{
		get_device_addr_and_name_disp_str(0,tar_num, NULL, NULL, tar_name, disp);
		*unicode_len = 0;
		if(JustIfCallTransferTarget(tar_num))
		{
			rev=1;
		}
	}
	else
	{
		disp[0]=0;
		*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferParaEditDiverlistMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallTransferParaEditDiverlistMenuSlecet(int index)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	IM_NameListRecord_T record;
	if(GetOneEditDivertListJsonArray(index,tar_num,tar_name)  == 0)
	{
		//get_device_addr_and_name_disp_str(0,record.BD_RM_MS, NULL, NULL, (strcmp(record.R_Name,"-")==0?record.name1:record.R_Name), disp);
		//*unicode_len = 0;
		if(JustIfCallTransferTarget(tar_num))
		{
			DelOneCallTransferParaTarget(tar_num);
			rev=0;
		}
		else
		{
			if(AddOneCallTransferParaTarget(tar_num, tar_name)==0)
				rev=1;
		}
		Set_JsonParaSettingHaveChange(1);
	}
	else
	{
		//disp[0]=0;
		//*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferParaEditDiverlistMenuReturn(void)
{
	ReleaseEditDivertListJsonArray();
	Set_JsonParaSettingInformFilterFunc(NULL);
	Enter_CallTransferParaSettingDiverlistMenu(0);
	return 0;
}

int CallTransferParaEditDiverlistFilter(int inform,void *arg)
{
	switch(inform)
	{
		case MSG_7_BRD_SUB_NameListUpdate:
			LoadEditDivertListJsonArray();
			Set_JsonParaSettingCurItemNums(GetEditDivertListJsonArrayNum());
			Set_JsonParaSettingCurPage(0);
			Set_JsonParaSettingCurLine(0);
			JsonParaSettingDispReflush();
			return 1;
	}
	return 0;
}
void Enter_CallTransferParaSettingEditDiverlistMenu(int select)
{
	LoadEditDivertListJsonArray();
	if(GetImNameListRecordCnt() == 0)
		API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);
	
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	Set_JsonParaSettingCurItemNums(GetEditDivertListJsonArrayNum());
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(1);
	
	Set_JsonParaSettingToKeypad(0);
	//Set_JsonParaSettingTitle(0,"Select by namelist");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneSelectByNamelist, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferParaEditDiverlistMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferParaEditDiverlistMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferParaEditDiverlistMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferParaEditDiverlistMenuReturn);
	Set_JsonParaSettingInformFilterFunc(CallTransferParaEditDiverlistFilter);	
	JsonParaSettingDispReflush();
}

cJSON *CallTransferTargetList =NULL;

int CallTransferCreateTargetList(void)
{
	int i,j;
	char tar_num[20];
	char tar_name[40];
	IpCacheRecord_T ip_record[MAX_CALL_TARGET_NUM];
	IpCacheRecord_T ip_record_temp;
	int search_num;
	int rev = 0;
	
	if(CallTransferTargetList !=NULL)
		cJSON_Delete(CallTransferTargetList);
	CallTransferTargetList = cJSON_CreateArray();
	//printf("!!!!!!!%s:%d\n",__func__,__LINE__);
	bprintf("11111111111111111:%d\n",GetCallTransferParaTargetNum());
	for(i=0;i<GetCallTransferParaTargetNum();i++)
	{
		cJSON *one_target;
		cJSON *one_target_list;
		cJSON *one_target_list_node;
		struct in_addr temp;
		memset(ip_record, 0, MAX_CALL_TARGET_NUM * sizeof(IpCacheRecord_T));
		GetCallTransferParaTarget(i,tar_num,tar_name);
		if(memcmp(tar_num+8,"01",2)==0)
		{
			memcpy(tar_num+8,"00",2);
		}
		//printf("!!!!!!!%s:%d:%s\n",__func__,__LINE__,tar_num);
		search_num = API_GetIpByInput(tar_num,ip_record);
		
		//printf("!!!!!!!%s:%d:%d\n",__func__,__LINE__,search_num);
		if(search_num>0&&rev<MAX_CALL_TARGET_NUM)
		{
			if(search_num>1&&memcmp(tar_num+8,"00",2)==0&&memcmp(ip_record[0].BD_RM_MS+8,"01",2)!=0)
			{
				for(j=1;j<search_num;j++)
				{
					if(memcmp(ip_record[j].BD_RM_MS+8,"01",2)==0)
					{
						ip_record_temp = ip_record[0];
						ip_record[0] = ip_record[j];
						ip_record[j] = ip_record_temp;
					}
				}
			}
			bprintf("11111111111111111:%d\n",GetCallTransferParaTargetNum());
			if(memcmp(tar_num+8,"00",2)==0)
			{
				if(JustIfCallTransferBePermit(ip_record[0].ip,GetSysVerInfo_BdRmMs())!=1)
					continue;
			}
			bprintf("11111111111111111:%d\n",GetCallTransferParaTargetNum());
			//GetCallTransferParaTargetNum()
			one_target = cJSON_CreateObject();
			cJSON_AddItemToArray(CallTransferTargetList,one_target);
			cJSON_AddStringToObject(one_target,CTPara_Key_TargetNum,tar_num);
			cJSON_AddStringToObject(one_target,CTPara_Key_TargetName,tar_name);
			one_target_list = cJSON_CreateArray();
			cJSON_AddItemToObject(one_target,CTPara_Key_DevArray,one_target_list);
			for(j=0;j<search_num&&rev<MAX_CALL_TARGET_NUM;j++)
			{
				printf("!!!!!!!%s:%d:%08x\n",__func__,__LINE__,ip_record[j].ip);
				rev++;
				one_target_list_node=cJSON_CreateObject();
				cJSON_AddItemToArray(one_target_list,one_target_list_node);
				cJSON_AddStringToObject(one_target_list_node,CTPara_Key_DevNum, ip_record[j].BD_RM_MS);
				temp.s_addr = ip_record[j].ip;  
				cJSON_AddStringToObject(one_target_list_node,CTPara_Key_DevAddr, inet_ntoa(temp));	
			}
		}
		//printf("!!!!!!!%s:%d:%d\n",__func__,__LINE__,rev);
	}
	return rev;
}


int CallTransferUpdateTargetList(void)
{
	int i,j;
	char tar_num[20];
	char tar_name[40];
	IpCacheRecord_T ip_record[32];
	IpCacheRecord_T ip_record_temp;
	int search_num;
	int rev = 0;
	int target_num;
	int online_target = 0;

	if(CallTransferTargetList == NULL||(target_num=cJSON_GetArraySize(CallTransferTargetList))==0)
	{
		goto ReCreatTagetList;
	}
	
	for(i=0;i<target_num;i++)
	{
		cJSON *one_target;
		cJSON *one_target_list;
		cJSON *one_target_list_node;
		
		
		one_target = cJSON_GetArrayItem(CallTransferTargetList,i);

		if(one_target!=NULL)
		{
			one_target_list=cJSON_GetObjectItemCaseSensitive(one_target,CTPara_Key_DevArray);
			for(j=0;j<cJSON_GetArraySize(one_target_list);j++)
			{
				char ip_buff[20];
				char num_buff[20];
				DeviceInfo dev_info;
				one_target_list_node=cJSON_GetArrayItem(one_target_list,j);
				ParseJsonString(one_target_list_node,CTPara_Key_DevNum,num_buff,10);
				ParseJsonString(one_target_list_node,CTPara_Key_DevAddr,ip_buff,20);
				if(API_GetInfoByIp(inet_addr(ip_buff), 2, &dev_info)!=0)
				{
					goto ReCreatTagetList;
				}
				if(memcmp(num_buff,dev_info.BD_RM_MS,10)!=0)
				{
					goto ReCreatTagetList;
				}
				online_target++;
			}
		}
		
	}
	if(online_target>0)
		return online_target;

	ReCreatTagetList:
		return CallTransferCreateTargetList();
}


#if !defined PID_DX470_V25
char* GetCallTransferRunningPara(void)
{
	cJSON *root = NULL;
	char* string;
	char disp[50];
	root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, CTPara_Key_State, GetCallTransferState());
	if(GetCallTransferState())
	{
		
		cJSON_AddNumberToObject(root, CTPara_Key_Mode,GetCallTransferParaMode());
		cJSON_AddNumberToObject(root, CTPara_Key_WaitingTime,GetCallTransferWaitAnswerTime());
		cJSON_AddItemReferenceToObject(root, CTPara_Key_TargetInfo, CallTransferTargetList);
		
	}


	string = cJSON_Print(root);
	if(string != NULL)
	{	
	printf("FwVerVerify=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}
#endif

int ParseCallTransferPara(const char* json_print, CallTransferRunningParaStru_t* pParaStru)
{
    int status = 0;
	int temp;

	int i,j;
	
	memset(pParaStru, 0, sizeof(CallTransferRunningParaStru_t));

    /* ����һ�����ڽ����� cJSON �ṹ */
    cJSON *JsonObject = cJSON_Parse(json_print);
	cJSON *target_info;
	cJSON *dev_array;
	cJSON *one_target;
	cJSON *one_dev;
	char 	target_num[20];
	char target_name[41];
	char buff[21];
    if (JsonObject == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = -1;
        goto end;
    }
	
	ParseJsonNumber(JsonObject,CTPara_Key_State,&temp);
	if(temp==0)
	{
		goto end;
	}
	pParaStru->state= temp;
	ParseJsonNumber(JsonObject,CTPara_Key_Mode,&temp);
	pParaStru->mode= temp;
	target_info = cJSON_GetObjectItemCaseSensitive(JsonObject, CTPara_Key_TargetInfo);
	if(target_info==NULL)
	{
		pParaStru->state= 0;
		goto end;
	}
	for(i=0;i<cJSON_GetArraySize(target_info);i++)
	{
		one_target=cJSON_GetArrayItem(target_info,i);
		ParseJsonString(one_target,CTPara_Key_TargetNum,target_num,10);
		ParseJsonString(one_target,CTPara_Key_TargetName,target_name,40);
		dev_array = cJSON_GetObjectItemCaseSensitive(one_target, CTPara_Key_DevArray);
		for(j=0;j<cJSON_GetArraySize(dev_array);j++)
		{
			one_dev=cJSON_GetArrayItem(dev_array,j);
			ParseJsonString(one_dev,CTPara_Key_DevNum,pParaStru->Target_List[pParaStru->target_num].bd_rm_ms,10);
			ParseJsonString(one_dev,CTPara_Key_DevAddr,buff,20);
			pParaStru->Target_List[pParaStru->target_num].ip_addr=inet_addr(buff);
			strcpy(pParaStru->Target_List[pParaStru->target_num].name,target_name);
			pParaStru->target_num++;
		}
		
	}
end:
	if(JsonObject!=NULL)
   	 	cJSON_Delete(JsonObject);
	
    return status;
}



