/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"
#include "task_VideoMenu.h"
#include "task_Led.h"
#include "obj_IpCacheTable.h"
//#include"obj_AlarmingZone.h"
#include"obj_CallTransferServey.h"
#include"obj_CallSceneCurPara.h"
#include "cJSON.h"


//obj_AlarmingSurvey_c


OS_TIMER	CallTransferSurveyTimer;

Loop_vdp_common_buffer  	vdp_CallTransferSurvey_mesg_queue;
Loop_vdp_common_buffer  	vdp_CallTransferSurvey_mesg_sync_queue;


vdp_task_t			  	task_CallTransferSurvey;
void CallTransferSurvey_Process(char* msg_data, int len);
void* vdp_CallTransferSurvey_task( void* arg );
void CallTransferSurveyCallback(void);
int CallTransfer_Start_Process(void);
int CallTransfer_Close_Process(void);



CallTransferSurveyRun_t CallTransferSurveyRun;
//int armedTotalDelay = 0;
//int DisarmedTotalDelay = 0;
void* vdp_CallTransferSurvey_task( void* arg );
void CallTransfer_Timer_Polling(void);
		  
void vtk_TaskInit_CallTransferSurvey(int priority)
{
	init_vdp_common_queue(&vdp_CallTransferSurvey_mesg_queue, 200, (msg_process)CallTransferSurvey_Process, &task_CallTransferSurvey);
	init_vdp_common_queue(&vdp_CallTransferSurvey_mesg_sync_queue, 200, NULL, 	&task_CallTransferSurvey);
	init_vdp_common_task(&task_CallTransferSurvey, MSG_ID_CallTransferSurvey, vdp_CallTransferSurvey_task, &vdp_CallTransferSurvey_mesg_queue, &vdp_CallTransferSurvey_mesg_sync_queue);
	
	//DHCP_printf("vdp_DHCP_task starting............\n");
}

void* vdp_CallTransferSurvey_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	
	sleep(5);
	//CallTransferParaReload();
	//CallTransferPermitParaReload();
	
	CallTransferSurveyRun.state=CallTransferSurvey_State_Idle;
	OS_CreateTimer(&CallTransferSurveyTimer,CallTransferSurveyCallback,60*1000/25);
	
	CallSceneParaReload();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
	
}

void CallTransferSurvey_Process(char* msg_data,int len)
{
	CallTransferMsg_T *pmsg = (CallTransferMsg_T*)msg_data;
	int temp;
	int result;
	//printf("!!!zone1 state=%d,senser_state=%d,val=%d,msg=%d\n",AlarmingZone[0].state,AlarmingZone[0].senser_state,AlarmingZone[0].senser_val,pmsg->head.msg_type);
	switch(pmsg->head.msg_type)
	{
		case CallTransferMsg_Start:
			result = CallTransfer_Start_Process();
			CallTransferSurvey_Respones(pmsg->head.msg_source_id,pmsg->head.msg_type,result);
			break;
		case CallTransferMsg_Close:
			CallTransfer_Close_Process();
			break;
		case CallTransferMsg_TimerTrigger:
			CallTransfer_Timer_Polling();
			break;
		
	}
}

void CallTransferSurveyCallback(void)
{
	CallTransferMsg_T	send_msg;	
	
	send_msg.head.msg_source_id = 0;
	send_msg.head.msg_target_id = MSG_ID_CallTransferSurvey;
	send_msg.head.msg_type = CallTransferMsg_TimerTrigger;
	
	push_vdp_common_queue(&vdp_CallTransferSurvey_mesg_queue, (char *)&send_msg, sizeof(CallTransferMsg_T));
}

void CallTransfer_Timer_Polling(void)
{
	if(Get_CallSceneCurPara_Scene()!= CallScene_DivertToDev_Int)
	{
		CallTransfer_Close_Process();
		return;
	}
	if(Get_CallSceneCurPara_AutoMode())
	{
		if(!IfCallSceneAutoModeInRunning())
		{
			CallTransferSurveyRun.state = CallTransferSurvey_State_Idle;
		}
		else if(CallTransferSurveyRun.state == CallTransferSurvey_State_Idle)
		{
			if(CallTransferUpdateTargetList()>0)
			{
				CallTransferSurveyRun.state = CallTransferSurvey_State_Running;
				CallTransferSurveyRun.timer =0;
			}
		}
		
	}
	else if(CallTransferSurveyRun.state == CallTransferSurvey_State_Idle)
	{
		CallTransferSurveyRun.timer=0;
		if(CallTransferUpdateTargetList()>0)
		{
			OS_SetTimerPeriod(&CallTransferSurveyTimer,60*1000/25);
			CallTransferSurveyRun.timer = 0;
			CallTransferSurveyRun.state = CallTransferSurvey_State_Running;
		}
			
	}

	if(CallTransferSurveyRun.state == CallTransferSurvey_State_Running)
	{
		if(++CallTransferSurveyRun.timer>=10)
		{
			CallTransferSurveyRun.timer=0;
			CallTransferUpdateTargetList();
		}
		//OS_RetriggerTimer(&CallTransferSurveyTimer);
	}
	
	OS_RetriggerTimer(&CallTransferSurveyTimer);
	
}

int API_CallTransfer_Start(void)
{
	CallTransferMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = CallTransferMsg_Start;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_CallTransferSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	ptask = GetTaskAccordingMsgID(send_msg.head.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	
	send_len = sizeof(CallTransferMsg_T);
	if(push_vdp_common_queue(&vdp_CallTransferSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	if(ptask!=NULL&&ptask ->p_syc_buf != NULL)
	{
		rev_len = 5;
		if(WaitForBusinessACK(ptask ->p_syc_buf,msg_type,rev,&rev_len,5000) == 1)
		{
			if(rev_len >= 5)
			{
				if(rev[4] != 0)
				{
					return -1;
				}
			}
		}
		else
			return -1;
	}
	
	return 0;
}


int API_CallTransfer_Close(void)
{
	CallTransferMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = CallTransferMsg_Close;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_CallTransferSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(CallTransferMsg_T);
	if(push_vdp_common_queue(&vdp_CallTransferSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}


void CallTransferSurvey_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}

int GetCallTransferState(void)
{
	return CallTransferSurveyRun.state;
}



int CallTransferReqToTarget(void)
{
	return 0;
}

int CallTransfer_Start_Process(void)
{
	int auto_mode;
	bprintf("11111111111111111\n");
	if(CallTransferCreateTargetList()<=0)
	{
		CallTransferSurveyRun.state == CallTransferSurvey_State_Idle;
		if(Get_CallSceneCurPara_Scene()== CallScene_DivertToDev_Int&&Get_CallSceneCurPara_AutoMode()==0)
		{
			OS_SetTimerPeriod(&CallTransferSurveyTimer,10*1000/25);
			OS_RetriggerTimer(&CallTransferSurveyTimer);
		}
		return 1;
	}
	bprintf("11111111111111111\n");
	auto_mode = Get_CallSceneCurPara_AutoMode();
	if(auto_mode)
	{
		if(!IfCallSceneAutoModeInRunning())
		{
			CallTransferSurveyRun.state = CallTransferSurvey_State_Idle;
			CallTransferSurveyRun.timer =0;
			OS_RetriggerTimer(&CallTransferSurveyTimer);
			return 0;
		}
	}
	bprintf("11111111111111111\n");
	if(CallTransferSurveyRun.state == CallTransferSurvey_State_Running)
	{
		OS_RetriggerTimer(&CallTransferSurveyTimer);
		return 0;
	}
	
	bprintf("11111111111111111\n");
	#if 0
	if(CallTransferReqToTarget()!=0)
	{
		return 1;
	}
	#endif
	CallTransferSurveyRun.state = CallTransferSurvey_State_Running;

	CallTransferSurveyRun.timer =0;
	
	OS_RetriggerTimer(&CallTransferSurveyTimer);

	return 0;
}

int CallTransfer_Close_Process(void)
{
	CallTransferSurveyRun.state = CallTransferSurvey_State_Idle;
	return 0;
}
///////////////////////////////////////

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

