
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include"obj_CallSceneCurPara.h"
#include "obj_ImNameListTable.h"
#include "obj_IpCacheTable.h"

#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
#include "obj_MsSyncCallScene.h"
extern cJSON *CallSceneNoDisturbRoot;
extern cJSON *CallSceneDivertPhoneRoot;
extern cJSON *CallSceneAutoModeRoot;
extern cJSON *CallTransferRoot;
//#include"obj_AlarmingZone.h"
//#include"obj_AlarmingPara.h"

#define CallSceneCurPara_FILE_NAME			"/mnt/nand1-2/Settings/CallSceneCurPara.txt"
#define CallScenePara_Dir						"/mnt/nand1-2/Settings/"

cJSON *CallSceneCurParaRoot =NULL;

#define CSCPara_Key_CurScene			"CUR_SCENE"
#define CSCPara_Key_AutoMode		"AUTO_MODE"
#define CSCPara_Key_AutoModePara	"AUTO_MODE_PARA"
#define CSCPara_Key_CurScenePara		"CUR_SCENE_PARA"

const char *CallScene_Type_Str[]=
{
	"Nomal use",
	"No disturb",
	"Divert to phone",
	"Divert to dev",
};


void CallSceneCurParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	int scene_type;
	CallSceneCurParaRoot =NULL;
	if((file=fopen(CallSceneCurPara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 CallSceneCurParaRoot = cJSON_Parse(json);
		 free(json);
		
	}
	
	 if(CallSceneCurParaRoot == NULL)
	 {
		CallSceneCurParaRoot = cJSON_CreateObject();
	 }
	 if(CallSceneCurParaRoot!=NULL)
	 {
		json_node = cJSON_GetObjectItemCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_CurScene);
		if(json_node == NULL||!cJSON_IsString(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_CurScene);
			cJSON_AddStringToObject(CallSceneCurParaRoot,CSCPara_Key_CurScene,CallScene_Type_Str[CallScene_NomalUse_Int]);
		}
		#if 0
		scene_type = ParseCallScene_Type(json_node->valuestring);
		if(scene_type==CallScene_NomalUse_Int)
		{

		}
		#endif
		json_node = cJSON_GetObjectItemCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_AutoMode);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_AutoMode);
			cJSON_AddNumberToObject(CallSceneCurParaRoot,CSCPara_Key_AutoMode,0);
		}
		
		
	 }
}

void SaveCallSceneCurParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(CallSceneCurParaRoot);
	if(string!=NULL)
	{
		if((file=fopen(CallSceneCurPara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("CallSceneCurPara:\n%s\n",string);
		free(string);

		
	}
}

void CallSceneParaReload(void)
{
	DIR *dir;
	
    	dir = opendir(CallScenePara_Dir);

	if(dir == NULL)
	{
		mkdir(CallScenePara_Dir,777);	
	}
	else
	{
		closedir(dir);
	}
	
	

	CallSceneCurParaReload();
	CallSceneNoDisturbParaReload();
	CallSceneDivertPhoneParaReload();
	CallTransferParaReload();
	CallTransferPermitParaReload();
	CallSceneAutoModeParaReload();

	if(Get_CallSceneCurPara_Scene()==CallScene_DivertToDev_Int)
	{
		CallTransfer_Start_Process();
	}
	else if(Get_CallSceneCurPara_Scene()==CallScene_NoDisturb_Int)
	{
		Set_CallSceneCurPara_Scene(CallScene_NomalUse_Int);
	}
}
int ParseCallScene_Type(char *str)
{
	int i;
	for(i=0;i<CallScene_Max;i++)
	{
		if(strcmp(CallScene_Type_Str[i],str)==0)
		{
			return i;
		}
	}
	return 0;
}
int Get_CallSceneCurPara_Scene(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneCurParaRoot,CSCPara_Key_CurScene);
	if(cJSON_IsString(one_node))
	{
		rev=ParseCallScene_Type(one_node->valuestring);
	}
	
	return rev;
}
int Set_CallSceneCurPara_Scene(int new_scene)
{
	int rev = 0;
	cJSON *one_node;
	char *string;
	if(new_scene<CallScene_Max)
	{
		cJSON_DeleteItemFromObjectCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_CurScene);
		cJSON_AddStringToObject(CallSceneCurParaRoot,CSCPara_Key_CurScene,CallScene_Type_Str[new_scene]);
		SaveCallSceneCurParaToFile();

		#if 0
		cJSON *para_obj = cJSON_CreateObject();
		
		if(para_obj)
		{
			cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_Cur,CallSceneCurParaRoot);
			if(Get_CallSceneCurPara_AutoMode())
			{
				cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_AutoMode,CallSceneAutoModeRoot);
			}
			if(new_scene==CallScene_NomalUse_Int)
			{

			}
			else if(new_scene==CallScene_NoDisturb_Int)
			{
				cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_NoDisturb,CallSceneNoDisturbRoot);
			}
			else if(new_scene==CallScene_DivertToPhone_Int)
			{
				cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_DivertPhone,CallSceneDivertPhoneRoot);
			}
			else if(new_scene==CallScene_DivertToDev_Int)
			{
				cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_DivertDev,CallTransferRoot);
			}
			string=cJSON_Print(para_obj);
			if(string)
			{
				API_MsSyncCallScene2Set(string);
				free(string);
			}
			cJSON_Delete(para_obj);
			
		}
		#endif
		//API_MsSyncCallScene2Set();
	}
	return rev;
}

int Get_CallSceneCurPara_AutoMode(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneCurParaRoot,CSCPara_Key_AutoMode);
	if(cJSON_IsNumber(one_node))
	{
		rev=one_node->valuedouble;
	}
	
	return rev;
}

int Set_CallSceneCurPara_AutoMode(int new_mode)
{
	int rev = 0;
	cJSON *one_node;
	char *string;
	one_node = cJSON_GetObjectItemCaseSensitive(CallSceneCurParaRoot,CSCPara_Key_AutoMode);
	if(cJSON_IsNumber(one_node))
	{
		rev=cJSON_SetNumberValue(one_node, new_mode);

		SaveCallSceneCurParaToFile();
		#if 0
		cJSON *para_obj = cJSON_CreateObject();
		
		if(para_obj)
		{
			cJSON_AddItemReferenceToObject(para_obj,CallSceneParaType_Cur,CallSceneCurParaRoot);
			string=cJSON_Print(para_obj);
			if(string)
			{
				API_MsSyncCallScene2Set(string);
				free(string);
			}
			cJSON_Delete(para_obj);
			
		}
		#endif
		//API_MsSyncCallScene2Set();
	}
	
	return rev;
}



char* GetCallSceneCurPara(void)
{
	char *string = NULL;
	int scene_type;
	
	scene_type = Get_CallSceneCurPara_Scene();
	if(scene_type==CallScene_NomalUse_Int)
	{

	}
	else if(scene_type==CallScene_NoDisturb_Int)
	{
		cJSON_AddItemReferenceToObject(CallSceneCurParaRoot,CSCPara_Key_CurScenePara,CallSceneNoDisturbRoot);
	}
	else if(scene_type==CallScene_DivertToPhone_Int)
	{
		cJSON_AddItemReferenceToObject(CallSceneCurParaRoot,CSCPara_Key_CurScenePara,CallSceneDivertPhoneRoot);
	}
	else if(scene_type==CallScene_DivertToDev_Int)
	{
		cJSON_AddItemReferenceToObject(CallSceneCurParaRoot,CSCPara_Key_CurScenePara,CallTransferRoot);
	}

	if(Get_CallSceneCurPara_AutoMode())
	{
		cJSON_AddItemReferenceToObject(CallSceneCurParaRoot,CSCPara_Key_AutoModePara,CallSceneAutoModeRoot);
	}
	
	string = cJSON_Print(CallSceneCurParaRoot);
	if(string != NULL)
	{	
		printf("CallTransferPermit=%s\n", string);
	}
	//cJSON_Delete(root);
	cJSON_DeleteItemFromObjectCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_CurScenePara);
	cJSON_DeleteItemFromObjectCaseSensitive(CallSceneCurParaRoot, CSCPara_Key_AutoModePara);
	
	return string;
}


