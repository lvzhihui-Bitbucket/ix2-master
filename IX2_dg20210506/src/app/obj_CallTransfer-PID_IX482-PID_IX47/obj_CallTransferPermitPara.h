#ifndef _obj_CallTransferPermitPara_H
#define _obj_CallTransferPermitPara_H

#include "../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"

typedef enum
{
	CallTransferPermit_Rule_NoDevice=0,
	CallTransferPermit_Rule_AllDevice,
	CallTransferPermit_Rule_SpecifyAllowed,
}CallTransferPermitRule_E;

void Enter_CallTransferPermitParaSettingRootMenu(int select);
void Enter_CallTransferPermitParaSettinglistMenu(int select);
void Enter_CallTransferPermitParaSettingEditlistMenu(int select);

#endif
