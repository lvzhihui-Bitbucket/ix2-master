#ifndef _obj_CallSceneCurPara_H
#define _obj_CallSceneCurPara_H

#include "obj_CallSceneDivertPhonePara.h"
#include "obj_CallSceneNoDisturbPara.h"

typedef enum
{
	CallScene_NomalUse_Int=0,
	CallScene_NoDisturb_Int,
	CallScene_DivertToPhone_Int,
	CallScene_DivertToDev_Int,
	CallScene_Max,
}CallScene_Type_Int_E;

int Get_CallSceneCurPara_Scene(void);
int Set_CallSceneCurPara_Scene(int new_scene);

#endif
