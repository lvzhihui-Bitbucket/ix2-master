
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include"obj_CallTransferPermitPara.h"
#include "obj_ImNameListTable.h"
#include "obj_IpCacheTable.h"

#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
//#include"obj_AlarmingZone.h"
//#include"obj_AlarmingPara.h"

#define CallTransferPermitPara_FILE_NAME			"/mnt/nand1-2/Settings/CallTransferPermit.txt"


cJSON *CallTransferPermitRoot =NULL;

#define CTPPara_Key_Rule				"RULE"
#define CTPPara_Key_PermitList			"PERMIT_LIST"
#define CTPPara_Key_PermitNum		"NUM"
#define CTPPara_Key_PermitName		"NAME"

void CallTransferPermitParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	CallTransferPermitRoot =NULL;
	if((file=fopen(CallTransferPermitPara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 CallTransferPermitRoot = cJSON_Parse(json);
		 free(json);
		
	}
	
	 if(CallTransferPermitRoot == NULL)
	 {
		CallTransferPermitRoot = cJSON_CreateObject();
	 }
	 if(CallTransferPermitRoot!=NULL)
	 {
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot, CTPPara_Key_Rule);
		if(json_node == NULL||!cJSON_IsNumber(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferPermitRoot, CTPPara_Key_Rule);
			cJSON_AddNumberToObject(CallTransferPermitRoot,CTPPara_Key_Rule,CallTransferPermit_Rule_NoDevice);
		}
		
		
		
		json_node = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot, CTPPara_Key_PermitList);
		if(json_node == NULL||!cJSON_IsArray(json_node))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(CallTransferPermitRoot, CTPPara_Key_PermitList);
			cJSON_AddArrayToObject(CallTransferPermitRoot,CTPPara_Key_PermitList);
		}
	 }
}

void SaveCallTransferPermitParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(CallTransferPermitRoot);
	if(string!=NULL)
	{
		if((file=fopen(CallTransferPermitPara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("CallTransferPermitPara:\n%s\n",string);
		free(string);
	}
}

int Get_CallTransferPermitPara_Rule(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_Rule);
	if(cJSON_IsNumber(one_node))
	{
		rev = one_node->valuedouble;
	}
	
	return rev;
}

int Set_CallTransferPermitPara_Rule(int new_val)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_Rule);
	if(cJSON_IsNumber(one_node))
	{
		rev = cJSON_SetNumberValue(one_node, new_val);
	}
	
	return rev;
}
int GetCallTransferPermitNum(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_PermitList);
	if(cJSON_IsArray(one_node))
	{
		rev = cJSON_GetArraySize(one_node);
	}
	
	return rev;
}
int GetCallTransferPermitCommon(cJSON *tar_list,int index,char *num,char *name)
{
	cJSON *one_node;
	one_node = cJSON_GetArrayItem(tar_list,index);
	if(num!=NULL)
		ParseJsonString(one_node,CTPPara_Key_PermitNum,num,10);
	if(name!=NULL)
		ParseJsonString(one_node,CTPPara_Key_PermitName,name,20);
	return 0;
}
int GetCallTransferPermit(int index,char *num,char *name)
{
	//strcpy(name,"xxxxx");
	//strcpy(num,"00990003");
	int rev = 0;
	cJSON *tar_list;
	cJSON *one_node;
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_PermitList);
	if(!cJSON_IsArray(tar_list)||index>=cJSON_GetArraySize(tar_list))
	{
		rev =-1;
	}
	else
	{
		rev=GetCallTransferPermitCommon(tar_list,index,num,name);
	}
	
	return rev;
}
int AddOneCallTransferPermitCommon(cJSON *tar_list,char *num,char *name)
{
	cJSON *one_node;
	
	one_node = cJSON_CreateObject();
	
	if(one_node==NULL)
		return -1;
	cJSON_AddItemToArray(tar_list,one_node);
	cJSON_AddStringToObject(one_node,CTPPara_Key_PermitNum,num);
	cJSON_AddStringToObject(one_node,CTPPara_Key_PermitName,name);
	return 0;
}
int AddOneCallTransferPermit(char *num,char *name)
{
	cJSON *tar_list;
	
	
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_PermitList);
	
	if(tar_list == NULL||!cJSON_IsArray(tar_list))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(CallTransferPermitRoot, CTPPara_Key_PermitList);
		cJSON_AddArrayToObject(CallTransferPermitRoot,CTPPara_Key_PermitList);
	}
	
	return AddOneCallTransferPermitCommon(tar_list,num,name);
}

int DelOneCallTransferPermit(char *num)
{
	int rev = 0;
	cJSON *tar_list;
	cJSON *one_node;
	char buff[20];
	int i;
	tar_list = cJSON_GetObjectItemCaseSensitive(CallTransferPermitRoot,CTPPara_Key_PermitList);
	if(tar_list==NULL||!cJSON_IsArray(tar_list))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(CallTransferPermitRoot, CTPPara_Key_PermitList);
		cJSON_AddArrayToObject(CallTransferPermitRoot,CTPPara_Key_PermitList);
		return 0;
	}
	for(i=0;i<cJSON_GetArraySize(tar_list);i++)
	{
		one_node = cJSON_GetArrayItem(tar_list,i);
		ParseJsonString(one_node,CTPPara_Key_PermitNum,buff,10);
		if(strcmp(buff,num)==0)
		{
			cJSON_DeleteItemFromArray(tar_list,i);
			break;
		}
	}
	
	return 0;
}

int JustIfCallTransferPermit(char *num)
{
	int i;
	char tar_num[20];
	
	for(i = 0;i<GetCallTransferPermitNum();i++)
	{
		GetCallTransferPermit(i,tar_num,NULL);
		if(strcmp(num,tar_num)==0)
			return 1;
	}
	return 0;
}

cJSON *EditPermitList_JsonArray=NULL;

void LoadEditPermitListJsonArray(void)
{
	int i;
	char num[20];
	char name[41];
	IM_NameListRecord_T record;
	
	if(EditPermitList_JsonArray!=NULL)
	{
		cJSON_Delete(EditPermitList_JsonArray);
		EditPermitList_JsonArray=NULL;
	}
	EditPermitList_JsonArray = cJSON_CreateArray();
	
	for(i=0;i<GetCallTransferPermitNum();i++)
	{
		GetCallTransferPermit(i,num,name);
		AddOneCallTransferPermitCommon(EditPermitList_JsonArray,num,name);
	}
	
	for(i=0;i<GetImNameListRecordCnt()||(GetImNameListRecordCnt()==0&&i<GetImNamelistTempTableNum());i++)
	{
		if(GetImNameListRecordCnt() == 0)
		{
			if(GetImNameListTempRecordItems(i,&record) != 0)
				continue;
		}
		else
		{
			if(GetImNameListRecordItems(i,	&record) != 0)
				continue;
		}
		if(JustIfCallTransferPermit(record.BD_RM_MS)==0)
		{
			AddOneCallTransferPermitCommon(EditPermitList_JsonArray,record.BD_RM_MS,strcmp(record.R_Name,"-")==0?record.name1:record.R_Name);
		}	
	}
}

void ReleaseEditPermitListJsonArray(void)
{
	if(EditPermitList_JsonArray!=NULL)
	{
		cJSON_Delete(EditPermitList_JsonArray);
		EditPermitList_JsonArray=NULL;
	}
}

int GetEditPermitListJsonArrayNum(void)
{
	return cJSON_GetArraySize(EditPermitList_JsonArray);
}

int GetOneEditPermitListJsonArray(int index,char *num,char *name)
{
	//strcpy(name,"xxxxx");
	//strcpy(num,"00990003");
	int rev = 0;
	
	
	rev=GetCallTransferPermitCommon(EditPermitList_JsonArray,index,num,name);
	
	
	return rev;
}



void CallTransferPermitParaMenuSettingSave(void)
{
	SaveCallTransferPermitParaToFile();
	//API_Alarming_Para_Update();
}



int CallTransferPermitParaRootMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			//strcpy(disp,"No device allowed");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowNoDev, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 1:
			//strcpy(disp,"All device allowed");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowAllDev, NULL, 0, NULL, 0, disp, unicode_len);
			break;
		case 2:
			//strcpy(disp,"Specify allowed");
			//*unicode_len = 0;
			API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowSpecily, NULL, 0, NULL, 0, disp, unicode_len);
			break;		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	if(Get_CallTransferPermitPara_Rule()==index)
		rev = 1;
	return rev;
}

int CallTransferPermitParaRootMenuValueDisp(int index,char *disp,int *unicode_len)
{
	
	int rev = 0;
	switch(index)
	{
		
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	//if(Get_CallTransferPermitPara_Rule()==index)
	//	rev = 1;
	return rev;
}

int CallTransferPermitParaRootRightListDisp(int index)
{
	int rev = 0;
	switch(index)
	{
		case 2:
			rev=SUF_SPR_Setting;
			break;		
		default:
			
			break;
	}
	
	return rev;
}

int CallTransferPermitParaRootMenuSlecet(int index)
{

	int rev = 1;
	if(index<3&& index!=Get_CallTransferPermitPara_Rule())
	{
		Set_CallTransferPermitPara_Rule(index);
		JsonParaSettingDispReflush();
	}
	
	return rev;
}

int CallTransferPermitParaRootRightListSelect(int index)
{
	int rev = 0;
	switch(index)
	{
		case 2:
			rev=0;
			Enter_CallTransferPermitParaSettinglistMenu(0);
			break;		
		default:
			break;
	}
	
	return rev;
}

int CallTransferPermitParaRootMenuReturn(void)
{
	Enter_CallTransferParaSettingRootMenu(0);
	return 0;
}

void Enter_CallTransferPermitParaSettingRootMenu(int select)
{
	ParaMenu_T para_menu;

	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(CallTransferPermitRoot);
	Set_JsonParaSettingCurNodeIndex(0);
	
	Set_JsonParaSettingCurItemNums(3);

		
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(1);
	
	Set_JsonParaSettingToKeypad(0);
	//GetParaMenuRecord(Alarming_Setting, &para_menu);
	//para_menu.name[para_menu.nameLen]= 0;
	//Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	//Set_JsonParaSettingTitle(0,"Allowed device");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowList, NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	//Set_JsonParaSettingMenuSaveFunc(CallTransferPermitParaMenuSettingSave);
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferPermitParaRootMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferPermitParaRootMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferPermitParaRootMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferPermitParaRootMenuReturn);
	Set_JsonParaSettingListRightDispFunc(CallTransferPermitParaRootRightListDisp);
	Set_JsonParaSettingListRightSelectFunc(CallTransferPermitParaRootRightListSelect);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		JsonParaSettingDispReflush();
	}
}

int CallTransferPermitParalistMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	if(index ==0)
	{
		//strcpy(disp,"Select by namelist");
		//*unicode_len = 0;
		API_GetOSD_StringWithID(MESG_TEXT_CallSceneSelectByNamelist,NULL, 0, NULL, 0, disp, unicode_len);
	}
	else if(index<=GetCallTransferPermitNum())
	{
		GetCallTransferPermit(index-1,tar_num,tar_name);
		get_device_addr_and_name_disp_str(0,tar_num, NULL, NULL, tar_name, disp);
		*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferPermitParalistMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallTransferPermitParalistMenuSlecet(int index)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			Enter_CallTransferPermitParaSettingEditlistMenu(0);
			break;
	}
	return rev;
}

int CallTransferPermitParalistMenuReturn(void)
{
	Enter_CallTransferPermitParaSettingRootMenu(0);
	return 0;
}

void Enter_CallTransferPermitParaSettinglistMenu(int select)
{
	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	Set_JsonParaSettingCurItemNums(GetCallTransferPermitNum()+1);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	//Set_JsonParaSettingTitle(0,"Specify device");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneAllowSpecily,NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferPermitParalistMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferPermitParalistMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferPermitParalistMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferPermitParalistMenuReturn);
		
	JsonParaSettingDispReflush();
}

int CallTransferPermitParaEditlistMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	IM_NameListRecord_T record;
	if(GetOneEditPermitListJsonArray(index,tar_num,tar_name) == 0)
	{
		get_device_addr_and_name_disp_str(0,tar_num, NULL, NULL, tar_name, disp);
		*unicode_len = 0;
		if(JustIfCallTransferPermit(tar_num))
		{
			rev=1;
		}
	}
	else
	{
		disp[0]=0;
		*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferPermitParaEditlistMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int CallTransferPermitParaEditlistMenuSlecet(int index)
{
	int rev = 0;
	char tar_num[20];
	char tar_name[41];
	IM_NameListRecord_T record;
	if(GetOneEditPermitListJsonArray(index,tar_num,tar_name)  == 0)
	{
		//get_device_addr_and_name_disp_str(0,record.BD_RM_MS, NULL, NULL, (strcmp(record.R_Name,"-")==0?record.name1:record.R_Name), disp);
		//*unicode_len = 0;
		if(JustIfCallTransferPermit(tar_num))
		{
			DelOneCallTransferPermit(tar_num);
			rev=0;
		}
		else
		{
			if(AddOneCallTransferPermit(tar_num, tar_name)==0)
				rev=1;
		}
		Set_JsonParaSettingHaveChange(1);
	}
	else
	{
		//disp[0]=0;
		//*unicode_len = 0;
	}
	
	return rev;
}

int CallTransferPermitParaEditlistMenuReturn(void)
{
	ReleaseEditPermitListJsonArray();
	Set_JsonParaSettingInformFilterFunc(NULL);
	Enter_CallTransferPermitParaSettinglistMenu(0);
	return 0;
}

int CallTransferPermitParaEditlistFilter(int inform,void *arg)
{
	switch(inform)
	{
		case MSG_7_BRD_SUB_NameListUpdate:
			LoadEditPermitListJsonArray();
			Set_JsonParaSettingCurItemNums(GetEditPermitListJsonArrayNum());
			Set_JsonParaSettingCurPage(0);
			Set_JsonParaSettingCurLine(0);
			JsonParaSettingDispReflush();
			return 1;
	}
	return 0;
}
void Enter_CallTransferPermitParaSettingEditlistMenu(int select)
{
	LoadEditPermitListJsonArray();
	if(GetImNameListRecordCnt() == 0)
		API_ListUpdate(MSG_TYPE_UPDATE_NAMELIST_TABLE);

	ParaPublicSettingMenu_Init();
	
	Set_JsonParaSettingCurNode(NULL);
	Set_JsonParaSettingCurNodeIndex(0);
	Set_JsonParaSettingCurItemNums(GetEditPermitListJsonArrayNum());
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(1);
	
	Set_JsonParaSettingToKeypad(0);
	//Set_JsonParaSettingTitle(0,"Select by namelist");
	char disp[100];
	int unicode_len;
	API_GetOSD_StringWithID(MESG_TEXT_CallSceneSelectByNamelist,NULL, 0, NULL, 0, disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	
	Set_JsonParaSettingMenuItemDispFunc(CallTransferPermitParaEditlistMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(CallTransferPermitParaEditlistMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(CallTransferPermitParaEditlistMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(CallTransferPermitParaEditlistMenuReturn);
	Set_JsonParaSettingInformFilterFunc(CallTransferPermitParaEditlistFilter);	
	JsonParaSettingDispReflush();
}

#if !defined PID_DX470_V25
char* GetCallTransferPermitPara(void)
{
	char *string = NULL;
	string = cJSON_Print(CallTransferPermitRoot);
	if(string != NULL)
	{	
		printf("CallTransferPermit=%s\n", string);
	}
	//cJSON_Delete(root);

	return string;
}
#endif

int JustIfCallTransferBePermit(int ip,char *num)
{
	int rev = 0;
	int i;
	char tar_num[20];
	cJSON *one_node;
	char json_string[500]={0};
	cJSON *JsonObject = NULL;
	//API_io_server_UDP_to_read_remote
	if(API_io_server_UDP_to_read_one_remote(ip, CallTransferPermitPara, json_string) != 0)
	{
		printf("!!!!!!!%s:%d:%s\n",__func__,__LINE__,json_string);
		sprintf(json_string, "<ID=%s>", CallTransferPermitPara);
		API_io_server_UDP_to_read_remote(ip, 0xFFFFFFFF, json_string);
		printf("!!%08x,!!!!!%s:%d:%s\n",ip,__func__,__LINE__,json_string);
		goto JustIfCallTransferBePermit_End;
	}
	printf("!!!!!!!%s:%d:%s\n",__func__,__LINE__,json_string);
	JsonObject = cJSON_Parse(json_string);
	
	if(JsonObject==NULL)
	{
		goto JustIfCallTransferBePermit_End;
	}
	printf("!!!!!!!%s:%d\n",__func__,__LINE__);
	one_node = cJSON_GetObjectItemCaseSensitive(JsonObject,CTPPara_Key_Rule);
	if(!cJSON_IsNumber(one_node))
	{
		goto JustIfCallTransferBePermit_End;
	}
	printf("!!!!!!!%s:%d:%d:%f\n",__func__,__LINE__,one_node->valueint,one_node->valuedouble);
	if(one_node->valueint==CallTransferPermit_Rule_NoDevice)
	{
		goto JustIfCallTransferBePermit_End;
	}
	if(one_node->valueint==CallTransferPermit_Rule_AllDevice)
	{
		rev = 1;
		goto JustIfCallTransferBePermit_End;
	}	
	printf("!!!!!!!%s:%d\n",__func__,__LINE__);
	one_node = cJSON_GetObjectItemCaseSensitive(JsonObject,CTPPara_Key_PermitList);
	if(!cJSON_IsArray(one_node))
	{
		goto JustIfCallTransferBePermit_End;
	}
	printf("!!!!!!!%s:%d\n",__func__,__LINE__);
	for(i = 0;i<cJSON_GetArraySize(one_node);i++)
	{
		
		GetCallTransferPermitCommon(one_node,i,tar_num,NULL);
		printf("!!!!!!!%s:%d:%s:%s\n",__func__,__LINE__,num,tar_num);
		if(strcmp(num,tar_num)==0)
		{
			rev = 1;
			break;
		}
	}
JustIfCallTransferBePermit_End:
	printf("!!!!!!!%s:%d:%d\n",__func__,__LINE__,rev);
	if(JsonObject!=NULL)
	{
		cJSON_Delete(JsonObject);
	}
	return rev;
}
