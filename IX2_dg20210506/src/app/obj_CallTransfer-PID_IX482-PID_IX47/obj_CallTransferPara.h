#ifndef _obj_CallTransferPara_H
#define _obj_CallTransferPara_H

#include "../obj_IxSys_CallBusiness/task_CallServer/task_CallServer.h"

typedef enum
{
	CallTransfer_Mode_Always=0,
	CallTransfer_Mode_Simultaneously,
	CallTransfer_Mode_Ifbusy,
	CallTransfer_Mode_IfNoanswer,
}CallTransferWorkMode_E;

typedef enum
{
	CallTransfer_StartupMode_Manual=0,
	CallTransfer_StartupMode_AutoByTiming,
}CallTransferStartupMode_E;

typedef struct
{
	int state;
	int mode;
	int target_num;
	Call_Dev_Info Target_List[MAX_CALL_TARGET_NUM];
}CallTransferRunningParaStru_t;

void Enter_CallTransferParaSettingRootMenu(int select);
void Enter_CallTransferParaSettingDiverlistMenu(int select);
void Enter_CallTransferParaSettingEditDiverlistMenu(int select);

#endif
