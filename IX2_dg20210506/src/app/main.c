/**
  ******************************************************************************
  * @file    main.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file is the beginner of all tasks
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */

#include "task_Hal.h"
#include "task_Sundry.h"
#include "task_survey.h"
#include "task_VideoMenu.h"
#include "task_debug_sbu.h"
#include "task_DHCP.h"
#include "define_file.h"

#include "RTOS.h"
#include "mul_timer.h"

#include "obj_multi_timer.h"

#include "ip_video_cs_control.h"
#include "vdp_uart.h"

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"
#include "tcp_server_process.h"
#include "elog.h"


int init_linphone_if_service( void );

#if 0
int remap2ttyusb0(void)  
{  
	    fflush(stdout);  
	    setvbuf(stdout,NULL,_IONBF,0);  
	    //intf("test stdout\n");  
	    int save_fd = dup(STDOUT_FILENO); // �����׼��� �ļ������� ע:����һ��Ҫ�� dup ����һ���ļ�������. ��Ҫ�� = ������Winodws�µľ��.  
	    int fd = open("/dev/ttyUSB0",(O_RDWR | O_CREAT), 0644);  
	    dup2(fd,STDOUT_FILENO); // �������´򿪵��ļ��������滻�� ��׼���  
	    dup2(fd,STDIN_FILENO); // �������´򿪵��ļ��������滻�� ��׼���  
	    dup2(fd,STDERR_FILENO); // �������´򿪵��ļ��������滻�� ��׼���  
	    printf("start ttyUSB0 console device!!!\n");  
	      
	    //�ٻָ�������׼���. ���ַ�ʽ  
	    //����1 �б��� ��׼��������  
	    //dup2(save_fd,STDOUT_FILENO);  
	      
	    //����2 û�б��� ��׼��������  
	    // int ttyfd = open("/dev/tty",(O_RDWR), 0644);  
	    //dup2(ttyfd,STDOUT_FILENO);  
	    //printf("test tty\n");  
	    return 0;
}  
#endif
int main( void )
{
	API_DeleteTempFile();

#ifndef SYNC_PROCESS
	struct sigaction myAction;
	myAction.sa_sigaction = DebugBacktrace;
	sigemptyset(&myAction.sa_mask);
	myAction.sa_flags = SA_RESTART | SA_SIGINFO;
	sigaction(SIGSEGV, &myAction, NULL);
	sigaction(SIGUSR1, &myAction, NULL);
	sigaction(SIGFPE, &myAction, NULL);
	sigaction(SIGILL, &myAction, NULL);
	sigaction(SIGBUS, &myAction, NULL);
	sigaction(SIGABRT, &myAction, NULL);
	sigaction(SIGSYS, &myAction, NULL);	
	sigaction(SIGTERM, &myAction, NULL);	
#else
	sigset_t sigmask;
	sigemptyset(&sigmask);
	sigaddset(&sigmask,SIGALRM);
	sigprocmask(SIG_BLOCK,&sigmask,NULL);
#endif	

    sdk_run_config config;
    config.mem_trace_flag = SDK_RUN_NORMAL;
	config.dma_mem_trace_flag = SDK_RUN_NORMAL;
    config.audio_tool_server_flag = 0;
    config.isp_tool_server_flag = 0;

    ak_sdk_init( &config );
#if !defined(PID_IXSE)
	ak_its_start(8765);
	ak_ats_start(8012);

	if( ak_tde_open() != ERROR_TYPE_NO_ERROR )
	{
		ak_print_error_ex(MODULE_ID_APP, "ak tde open error.\n");
        	return -1;
    }
#endif	

    ak_print_normal_ex(MODULE_ID_VDEC, "ak_print_normal_ex------------\n");
    ak_print_normal_ex(MODULE_ID_VDEC, "vdec lib version: %s\n\n", ak_vdec_get_version());

    printf("vdec lib version: %s\n\n", ak_vdec_get_version());
    printf("TDE lib version is [%s]\n", ak_tde_get_version()); 

	//remap2ttyusb0();

	GlobalResourceInit();
	gpio_initial();

	//ak_ats_start(8012);
	
	my_elog_init();
	
#if !defined(PID_IXSE)
	Init_vdp_uart();
#endif

	init_all_timer();
	
	MakeDir(TEMP_Folder);
	MakeDir(INSTALLER_BAK_PATH);
	MakeDir(SettingFolder);	
	MakeDir(CustomerFileDir);
	MakeDir(UserDataFolder);	
	MakeDir(UserResFolder);
	MakeDir(LOG_DIR);
	InitIOServer();

	vtk_TaskInit_Event();
	vtk_TaskInit_io_server(0);
	init_video_cs_service();
	vtk_TaskInit_sundry( 0 );	
	vtk_TaskInit_video_menu( 0 );	
	vtk_TaskInit_debug_sbu( 0 );	
	IXS_ProxyInit();

#if !defined(PID_IXSE)
	vtk_TaskInit_StackAI(0);
#endif

	init_c5_ipc_instance();
	init_udp_io_server_instance();
	init_device_update_instance();

	init_vdp_callserver_task();

	init_sntp_client();	

#if !defined(PID_IXSE)||defined(PID_IXSE_V2)
	init_linphone_if_service();
#endif

	vtk_TaskInit_DHCP(0);		// avoid dead!!

	vtk_TaskInit_Hal(0);
	vtk_TaskInit_survey(0);		

#if !defined(PID_IXSE)
	Stm8HeartBeatServiceInit();   
#else
	ThreadHeartbeat_Init();	  	
#endif
	
	vtk_TaskInit_Ota_client_ifc();		//czn_20181219

	vtk_TaskInit_ResSync();	//czn_20190520
	vtk_TaskInit_listUpdate();

#if !defined(PID_IXSE)
	vtk_TaskInit_WiFi(0);
#endif

	vtk_TaskInit_VtkMediaTrans(0);
	
	IxBuilderTcpServerInit();

	vtk_TaskInit_IxProxy(0);
	vtk_TaskInit_IxProxy_client(0);

	//ak_drv_wdt_open(10);
	vtk_TaskInit_ipc_business(0);
	vtk_taskInit_ds_monitor(0);
	init_listen_subscriber_list();
	//#if defined(PID_DX470)||defined(PID_DX482)
	vtk_TaskInit_DXMonitor();
	//#endif
	#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
	vtk_TaskInit_AlarmingSurvey(0);
	#endif
	task_ShellInit();
	#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)
	localMemSpaceManager(2);
	#endif
	#if defined(PID_IX47)||defined(PID_IX482)
	vtk_TaskInit_CallTransferSurvey(0);
	#endif
	while(1)
	{
		ShellCmdUartInputProcess();
	}
	vtk_Task_Hal_Join();

	//ak_drv_wdt_close();
	
	return 0;
}

