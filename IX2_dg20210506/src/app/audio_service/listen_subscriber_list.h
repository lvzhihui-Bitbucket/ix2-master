
#ifndef _LISTEN_SUBSCRIBER_LIST_H
#define _LISTEN_SUBSCRIBER_LIST_H
#include <sys/socket.h>
#include <pthread.h>
#include "utility.h"
// 订阅状态机
#if 0
typedef enum
{
	IDLE,
	ACTIVATE_SREVICE,
	IN_SERVICE,
	DEACTIVATE_SERVICE,
} subscriber_state;
#endif
typedef struct
{
	int				reg_ip;			// 登记的客户端IP
	struct timeval	reg_tv;			// 登记的客户端申请时间 	
	int				reg_period;		// 登记的客户端申请播放时间 (s)
	int				reg_timer;		// 登记的客户端定时器
} listen_subscriber_data;

#define MAX_LISTEN_SUBUSCRIBER_LIST_COUNT		50

typedef struct
{	
	int             s_sock;
	//int             s_ip;
	//int             s_port;
   	struct sockaddr_in s_addr;
	listen_subscriber_data		dat[MAX_LISTEN_SUBUSCRIBER_LIST_COUNT];	
	int					counter;
    	pthread_mutex_t 	lock;	
} listen_subscriber_list;

#pragma pack(1)
typedef struct
{
	uint8 ctrl_type;
	int 	trans_ip;
	short trans_port;
	int 		period;
	uint8 trans_type;
	uint8 listen_scene;
	uint8 ctrl_data1;
	uint8 ctrl_data2;
}ListenTalk_Req_t;

typedef struct
{
	uint8 ctrl_type;
	uint8 rsp;
	int 	trans_ip;
	short trans_port;
}ListenTalk_Rsp_t;
#pragma pack()

#define Listen_Ctrl_Subscriber  		0
#define Listen_Ctrl_Desubscriber  		1
#define Listen_Ctrl_Cancel 				2
#define Listen_Ctrl_ForceSubscriber  	3
#define Listen_Ctrl_ReqTalk  			4

enum
{
	listenTalk_Idle=0,
	listenTalk_AsServer,
	listenTalk_BeBroadcast,
	listenTalk_ListenClient,
	listenTalk_Broadcast,
};
/**
 * @fn:		init_subscriber_list
 *
 * @brief:	订阅列表初始化
 *
 * @param:	none
 *
 * @return: none
 */
int init_subscriber_list( void );

/**
 * @fn:		get_total_subscriber_list
 *
 * @brief:	得到指定通道订阅列表的计数
 *
 * @param:	channel	- 发送通道
 * @param:	type	- 发送类型 - 0/组播，1/点播
 *
 * @return: 返回个数
 */
int get_total_listen_subscriber_list(int type );

/**
 * @fn:		delete_all_userlist
 *
 * @brief:	释放所有列表的订阅节点
 *
 * @param:	none
 *
 * @return: 0
 */
int delete_all_listen_userlist(int ext_ip);

/**
 * @fn:		activate_subscriber_list_ext
 *
 * @brief:	添加目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 * @param:	reg_period	- 订阅时长
 *
 * @return: 总数
 */
int activate_listen_subscriber_list( int type, int reg_ip, int reg_period ,int send_port,int send_ip);

/**
 * @fn:		deactivate_subscriber_list_ext
 *
 * @brief:	释放目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 *
 * @return: 总数
 */
int deactivate_listen_subscriber_list(int reg_ip);

int Send_ListenCtrlForceSubscriber_Req(int target_ip,int period,int trans_type,int trans_ip,int trans_port);

int Send_ListenCtrlForceSubscriber_Rsp(int target_ip,int sn,int rsp);

#endif

