


#include "task_IoServer.h"
#include "elog.h"

// lzh_20181224_s
void api_spk_volume_set( int vol )
{
	//vol = vol * 10;
	//alsa_card_set_level( vol, CARD_PLAYBACK );
}
int api_spk_volume_get( void )
{
	//int vol;
	
	//vol = alsa_card_get_level( CARD_PLAYBACK );

	//vol = vol / 10;
	
	return 0;
}

static void api_spk_power_on( void )
{
	//API_POWER_UDP_TALK_ON();		//czn_20170522
}

static void api_spk_power_off( void )
{
	//API_POWER_TALK_OFF();
}
#if	defined(PID_DX470)||defined(PID_DX482)
static int SPK_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};

static int SPK_GAIN_STEP_TAB[7]=
{
	//  0 -  6
	0,1,2,3,4,5,6
};

int LoadAudioPlaybackVol( AU_IO_TYPE au_type )
{
	char temp[20];
	int vol;	

	if( au_type == DS_DX_APP )
	{
		API_Event_IoServer_InnerRead_All(SpeakVolumeSet_APP, temp);
		printf("LoadAudioPlaybackVol(DS_DX_APP) = %d\n", atoi(temp));
		log_d("LoadAudioPlaybackVol(DS_DX_APP) = %d\n", atoi(temp));
	}
	else
	{
		API_Event_IoServer_InnerRead_All(SpeakVolumeSet, temp);
		printf("LoadAudioPlaybackVol(DS_DX) = %d\n", atoi(temp));
		log_d("LoadAudioPlaybackVol(DS_DX) = %d\n", atoi(temp));
	}
	return SPK_VOLUME_STEP_TAB[atoi(temp)];
}
int LoadAudioPlaybackGain( AU_IO_TYPE au_type )
{
	char temp[20];

	if( au_type == DS_DX_APP )
	{
		API_Event_IoServer_InnerRead_All(SPEAK_VOLUME_GAIN_APP, temp);
		printf("LoadAudioPlaybackGain(DS_DX_APP) = %d\n", atoi(temp));
		log_d("LoadAudioPlaybackGain(DS_DX_APP) = %d\n", atoi(temp));
	}
	else
	{
		API_Event_IoServer_InnerRead_All(SPEAK_VOLUME_GAIN, temp);
		printf("LoadAudioPlaybackGain(DS_DX) = %d\n", atoi(temp));
		log_d("LoadAudioPlaybackGain(DS_DX) = %d\n", atoi(temp));
	}

	return SPK_GAIN_STEP_TAB[atoi(temp)];

}
#else
int SPEAK_STEP_TAB[10]=
{
	0,-20,-15,-10,-5,1,5,10,15,20
};
int LoadAudioPlaybackVol( void )
{
	char temp[20];
	int vol;
	
	API_Event_IoServer_InnerRead_All(SpeakVolumeSet, temp);
	printf("LoadAudioPlaybackVol = %d\n", atoi(temp));
	//vol = atoi(temp) > 9? 9 : atoi(temp);
	return atoi(temp);

}

int LoadAudioPlaybackGain( void )
{
	char temp[20];
	API_Event_IoServer_InnerRead_All(SPEAK_VOLUME_GAIN, temp);
	printf("LoadAudioPlaybackGain = %d\n", atoi(temp));
	return atoi(temp);

}
#endif
void LoadLinPhoneAudioPlaybackVol( void )	//czn_20170108
{
	//extern int speakVolume;
	
	api_spk_volume_set( 7 );		

	api_spk_power_on();
}

void SetAudioPlaybackMute( void )
{
}

