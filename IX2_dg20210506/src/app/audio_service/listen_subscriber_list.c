/**
 * Description: 订阅机制列表处理服务
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 * 3个码流通道2中发送类型，供3x2个订阅列表
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <linux/fb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include <dirent.h>
#include <ortp/ortp.h>

#include "listen_subscriber_list.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "au_service.h"
#include "task_VideoMenu.h"
#include "obj_GetIpByNumber.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "task_IoServer.h"


listen_subscriber_list	server_listen_userlist_uni;
listen_subscriber_list	server_listen_userlist_mul;

static int listenTalk_State=0;
static int listenTalk_ServerIp=0;
static int listen_Talking=0;
static char listenSerName[41]={0};
static char listenSerDevNum[11]={0};


void Set_listenTalk_ServerIp(int ip)
{
	listenTalk_ServerIp=ip;
}

int Get_listenTalk_ServerIp(void)
{
	return listenTalk_ServerIp;
}

void Set_listenTalk_State(int state)
{
	listenTalk_State=state;
}

int Get_listenTalk_State(void)
{
	return listenTalk_State;
}
void Set_listen_Talking(int state)
{
	listen_Talking=state;
}

int Get_listen_Talking(void)
{
	return listen_Talking;
}
void Get_ListenSerName(char *name)
{
	if(Get_listenTalk_State()==listenTalk_ListenClient)
	{
		strcpy(name,listenSerName);
	}
}
void Set_ListenSerName(char *name)
{
	strcpy(listenSerName,name);
}

void Get_ListenSerDevNum(char *dev_num)
{
	if(Get_listenTalk_State()==listenTalk_ListenClient)
	{
		strcpy(dev_num,listenSerDevNum);
	}
}
void Set_ListenSerDevNum(char *dev_num)
{
	strcpy(listenSerDevNum,dev_num);
}
/**
 * @fn:		init_subscriber_list
 *
 * @brief:	订阅列表初始化
 *
 * @param:	none
 *
 * @return: none
 */
int init_listen_subscriber_list( void )
{
	
	server_listen_userlist_uni.counter	= 0;
	server_listen_userlist_uni.s_sock=0;
	pthread_mutex_init( &server_listen_userlist_uni.lock, 0);
	server_listen_userlist_mul.counter	= 0;
	server_listen_userlist_mul.s_sock=0;
	pthread_mutex_init( &server_listen_userlist_mul.lock, 0);
	return 0;
}

/**
 * @fn:		get_total_subscriber_list
 *
 * @brief:	得到指定通道订阅列表的计数
 *
 * @param:	channel	- 发送通道
 * @param:	type	- 发送类型 - 0/组播，1/点播
 *
 * @return: 返回个数
 */
int get_total_listen_subscriber_list(int type )
{
	int i;
	listen_subscriber_list *plist;
	if(type==0)
		plist=&server_listen_userlist_uni;
	else
		plist=&server_listen_userlist_mul;
	pthread_mutex_lock( &plist->lock );	
	
	i = plist->counter;
	
	pthread_mutex_unlock( &plist->lock );	

	return i;
}

/**
 * @fn:		delete_all_userlist
 *
 * @brief:	释放所有列表的订阅节点
 *
 * @param:	none
 *
 * @return: 0
 */
int delete_all_listen_userlist(int ext_ip)
{
	int i;
	listen_subscriber_data sub_data_temp;
	int ext_flag=0;
	pthread_mutex_lock( &server_listen_userlist_uni.lock );
	ext_flag=0;
	for(i=0;i<server_listen_userlist_uni.counter;i++)
	{
		if(server_listen_userlist_uni.dat[i].reg_ip==ext_ip)
		{
			ext_flag=1;
			server_listen_userlist_uni.dat[0]=server_listen_userlist_uni.dat[i];
		}
		else
		{
			Send_ListenCtrlCancel_Req(server_listen_userlist_uni.dat[i].reg_ip);
		}
	}
	if(ext_flag==1)
	{
		server_listen_userlist_uni.counter=1;
	}
	else
	{
		if(server_listen_userlist_uni.s_sock!=0)
		{
			close(server_listen_userlist_uni.s_sock);
			server_listen_userlist_uni.s_sock=0;
		}
		server_listen_userlist_uni.counter=0;
	}
	pthread_mutex_unlock( &server_listen_userlist_uni.lock );

	
	pthread_mutex_lock( &server_listen_userlist_mul.lock );
	ext_flag=0;
	for(i=0;i<server_listen_userlist_mul.counter;i++)
	{
		if(server_listen_userlist_mul.dat[i].reg_ip==ext_ip)
		{
			ext_flag=1;
			server_listen_userlist_mul.dat[0]=server_listen_userlist_mul.dat[i];
		}
		else
		{
			Send_ListenCtrlCancel_Req(server_listen_userlist_mul.dat[i].reg_ip);
		}
	}
	if(ext_flag==1)
	{
		server_listen_userlist_mul.counter=1;
	}
	else
	{
		if(server_listen_userlist_mul.s_sock!=0)
		{
			close(server_listen_userlist_mul.s_sock);
			server_listen_userlist_mul.s_sock=0;
		}
		server_listen_userlist_mul.counter=0;
	}
	pthread_mutex_unlock( &server_listen_userlist_mul.lock );
}

/**
 * @fn:		activate_subscriber_list_ext
 *
 * @brief:	添加目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 * @param:	reg_period	- 订阅时长
 *
 * @return: 总数
 */
int activate_listen_subscriber_list( int type, int reg_ip, int reg_period ,int send_port,int send_ip)
{
	struct timeval tv;	
	int i;

	listen_subscriber_list *plist;
	if(type==0)
		plist=&server_listen_userlist_uni;
	else
		plist=&server_listen_userlist_mul;

	pthread_mutex_lock( &plist->lock );
	
	if( plist->counter >= MAX_LISTEN_SUBUSCRIBER_LIST_COUNT )
	{
		pthread_mutex_unlock( &plist->lock );	
		return -1;
	}
	
	for( i = 0; i < plist->counter; i++ )
	{
		if( plist->dat[i].reg_ip == reg_ip )
		{
			pthread_mutex_unlock( &plist->lock );	
			return -1;
		}
	}
	
	gettimeofday(&tv, 0);
	plist->dat[plist->counter].reg_tv		= tv;
	plist->dat[plist->counter].reg_ip		= reg_ip;
	plist->dat[plist->counter].reg_period	= reg_period;
	plist->dat[plist->counter].reg_timer	= 0;
	
	plist->counter++;
	i = plist->counter;
	if(i==1)
	{
		if( ( plist->s_sock = socket( PF_INET, SOCK_DGRAM, 0 ) ) == -1 )
		{
		    plist->s_sock=0;
		}
		bzero( &plist->s_addr, sizeof(plist->s_addr) );
		plist->s_addr.sin_family 		= AF_INET;
		plist->s_addr.sin_addr.s_addr	= send_ip;
		plist->s_addr.sin_port		    =  htons( send_port );//send_port;
	}
	pthread_mutex_unlock( &plist->lock );	
	
	return i;
}

/**
 * @fn:		deactivate_subscriber_list_ext
 *
 * @brief:	释放目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 *
 * @return: 总数
 */
int deactivate_listen_subscriber_list_bytype( int type, int reg_ip)
{
	int i;
	int rev = -1; 
	listen_subscriber_list *plist;
	if(type==0)
		plist=&server_listen_userlist_uni;
	else
		plist=&server_listen_userlist_mul;
	pthread_mutex_lock( &plist->lock );	

	if(plist->counter==0)
	{
		pthread_mutex_unlock( &plist->lock );
		return rev;
	}
	
	for( i = 0; i < plist->counter; i++ )
	{
		if( plist->dat[i].reg_ip == reg_ip )
		{
			plist->dat[i].reg_tv 		= plist->dat[plist->counter-1].reg_tv;
			plist->dat[i].reg_ip 		= plist->dat[plist->counter-1].reg_ip;
			plist->dat[i].reg_period	= plist->dat[plist->counter-1].reg_period;
			plist->dat[i].reg_timer		= plist->dat[plist->counter-1].reg_timer;
			plist->counter--;
			rev= plist->counter;
			break;
		}
	}
	//i = plist->counter;
	if(rev==0)
	{
		if(plist->s_sock!=0)
		{
			close(plist->s_sock);
			plist->s_sock=0;
		}
	}
	pthread_mutex_unlock( &plist->lock );	
	
	return rev;
}
int deactivate_listen_subscriber_list( int reg_ip)
{
	int rev;
	if((rev=deactivate_listen_subscriber_list_bytype(0,reg_ip))<0)
	{
		rev=deactivate_listen_subscriber_list_bytype(1,reg_ip);
	}
	return rev;
}
void listen_subscriber_list_send(char *data,int len)
{
	int i;
	pthread_mutex_lock( &server_listen_userlist_uni.lock );
	
	for(i=0;i<server_listen_userlist_uni.counter;i++)
	{
		if(server_listen_userlist_uni.s_sock!=0)
		{
			server_listen_userlist_uni.s_addr.sin_addr.s_addr	= server_listen_userlist_uni.dat[i].reg_ip;
			sendto( server_listen_userlist_uni.s_sock, data,len, 0, &server_listen_userlist_uni.s_addr, sizeof(server_listen_userlist_uni.s_addr) );
			//printf("listen_subscriber_list_send :%08x\n",server_listen_userlist_uni.dat[i].reg_ip);
		}
	}
	pthread_mutex_unlock( &server_listen_userlist_uni.lock );

	pthread_mutex_lock( &server_listen_userlist_mul.lock );
	
	
	if(server_listen_userlist_mul.counter>0&&server_listen_userlist_mul.s_sock!=0)
	{
		server_listen_userlist_uni.s_addr.sin_addr.s_addr	= server_listen_userlist_uni.dat[i].reg_ip;
		sendto( server_listen_userlist_uni.s_sock, data,len, 0, &server_listen_userlist_uni.s_addr, sizeof(server_listen_userlist_uni.s_addr) );
	}
	
	pthread_mutex_unlock( &server_listen_userlist_mul.lock );
}
void ListenTalkCmd_Process(int source_ip,int cmd,int sn,uint8 *pkt , uint16 len)
{
	ListenTalk_Req_t *ListenTalkCmd = (ListenTalk_Req_t *)pkt;
	char temp[5];
	if(cmd==CMD_OLDMULTALK_REQ)
	{
		
	}
	if(cmd==CMD_LISTENTALK_REQ)
	{
		if(ListenTalkCmd->ctrl_type==Listen_Ctrl_ForceSubscriber)
		{
			API_Event_IoServer_InnerRead_All(BabyRoomEn, temp);
			if(atoi(temp)==1)
			{
				Send_ListenCtrlForceSubscriber_Rsp(source_ip,sn,1);
				return;
			}
			if(Get_listenTalk_State()!=listenTalk_BeBroadcast)
			{
				Set_listenTalk_State(listenTalk_BeBroadcast);
			}
			Send_ListenCtrlForceSubscriber_Rsp(source_ip,sn,0);
			Set_listenTalk_ServerIp(source_ip);
			API_talk_on_by_type2(IX_IX,0,0, AUDIO_SERVER_UNICAST_PORT,ListenTalkCmd->trans_type);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_ListenTalkStart);
		}
		if(ListenTalkCmd->ctrl_type==Listen_Ctrl_Subscriber)
		{
			API_Event_IoServer_InnerRead_All(BabyRoomEn, temp);
			if(ListenTalkCmd->trans_type==0&&atoi(temp)==1)
			{	
				if(Get_listenTalk_State()==listenTalk_Idle||(Get_listenTalk_State()==listenTalk_AsServer&&Get_listen_Talking()==0))
				{
					if(activate_listen_subscriber_list(ListenTalkCmd->trans_type,source_ip,ListenTalkCmd->period,AUDIO_SERVER_UNICAST_PORT,0)>0)
					{
						Send_ListenCtrlSubscriber_Rsp(source_ip,sn,0,0,AUDIO_SERVER_UNICAST_PORT);
						if(Get_listenTalk_State()!=listenTalk_AsServer)
						{
							Set_listenTalk_State(listenTalk_AsServer);
						}

						if(get_au_service_state()==0)
						{
							API_talk_on_by_type2(IX_IX,0,0, AUDIO_CLIENT_UNICAST_PORT,0);
							Set_listen_Talking(0);
							API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_ListenTalkStart);
						}
					}
				}
				else
					Send_ListenCtrlSubscriber_Rsp(source_ip,sn,1,0,0);
			}
			else
				Send_ListenCtrlSubscriber_Rsp(source_ip,sn,1,0,0);
		}
		if(ListenTalkCmd->ctrl_type==Listen_Ctrl_Cancel)
		{
			Send_ListenCtrlCancel_Rsp(source_ip,sn,0);
			deactivate_listen_subscriber_list(Get_listenTalk_ServerIp());
			Set_listenTalk_State(listenTalk_Idle);
			API_talk_off();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_ListenTalkClose);
		}
		if(ListenTalkCmd->ctrl_type==Listen_Ctrl_Desubscriber)
		{
			deactivate_listen_subscriber_list(source_ip);
			Send_ListenCtrlDesubscriber_Rsp(source_ip,sn,0);
			if(get_total_listen_subscriber_list(0)==0&&get_total_listen_subscriber_list(1)==0)
			{
				API_talk_off();
				Set_listenTalk_State(listenTalk_Idle);
				Set_listen_Talking(0);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_ListenTalkClose);
			}
		}
		if(ListenTalkCmd->ctrl_type==Listen_Ctrl_ReqTalk)
		{
			if(Get_listenTalk_State()==listenTalk_AsServer&&Get_listen_Talking()==0)
			{
				Send_ListenCtrlTalk_Rsp(source_ip,sn,0);
				delete_all_listen_userlist(source_ip);
				Set_listen_Talking(1);
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_IntercomCallTalkOn);
			}
			else
				Send_ListenCtrlTalk_Rsp(source_ip,sn,1);
		}
	}
}

int Send_ListenCtrlSubscriber_Req(int target_ip,int period,int trans_type,int *trans_ip,int *trans_port)
{
	ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&send_cmd,0,sizeof(ListenTalk_Req_t));
	send_cmd.ctrl_type=Listen_Ctrl_Subscriber;
	send_cmd.trans_ip=0;
	send_cmd.trans_port=0;
	send_cmd.trans_type = trans_type;
	send_cmd.period=period;

	if(api_udp_c5_ipc_send_req(target_ip,CMD_LISTENTALK_REQ,(char*)&send_cmd,sizeof(ListenTalk_Req_t),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.rsp != 0)
	{
		return -1;
	}

	*trans_ip=rsp_cmd.trans_ip;
	*trans_port=rsp_cmd.trans_port;
	
	return 0;
}

int Send_ListenCtrlSubscriber_Rsp(int target_ip,int sn,int rsp,int trans_ip,int trans_port)
{
	//ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	//int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&rsp_cmd,0,sizeof(ListenTalk_Rsp_t));
	rsp_cmd.ctrl_type=Listen_Ctrl_ForceSubscriber;
	rsp_cmd.trans_ip=trans_ip;
	rsp_cmd.trans_port=trans_port;
	rsp_cmd.rsp=rsp;
	

	if(api_udp_c5_ipc_send_rsp(target_ip,CMD_LISTENTALK_REP,sn,(char*)&rsp_cmd,sizeof(ListenTalk_Rsp_t)) != 0)
	{
		return -1;
	}

	return 0;
}

int Send_ListenCtrlForceSubscriber_Req(int target_ip,int period,int trans_type,int trans_ip,int trans_port)
{
	ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&send_cmd,0,sizeof(ListenTalk_Req_t));
	send_cmd.ctrl_type=Listen_Ctrl_ForceSubscriber;
	send_cmd.trans_ip=trans_ip;
	send_cmd.trans_port=trans_port;
	send_cmd.trans_type = trans_type;
	send_cmd.period=period;

	if(api_udp_c5_ipc_send_req(target_ip,CMD_LISTENTALK_REQ,(char*)&send_cmd,sizeof(ListenTalk_Req_t),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.rsp != 0)
	{
		return -1;
	}
	if(Get_listenTalk_State()!=listenTalk_Broadcast)
	{
		Set_listenTalk_State(listenTalk_Broadcast);
	}
	activate_listen_subscriber_list(trans_type,target_ip,period,trans_port,trans_ip);

	if(get_au_service_state()==0)
	{
		API_talk_on_by_type2(IX_IX,0,0, trans_port,0);
	}

	return 0;
}

int Send_ListenCtrlForceSubscriber_Rsp(int target_ip,int sn,int rsp)
{
	//ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	//int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&rsp_cmd,0,sizeof(ListenTalk_Rsp_t));
	rsp_cmd.ctrl_type=Listen_Ctrl_ForceSubscriber;
	rsp_cmd.rsp=rsp;
	

	if(api_udp_c5_ipc_send_rsp(target_ip,CMD_LISTENTALK_REP,sn,(char*)&rsp_cmd,sizeof(ListenTalk_Rsp_t)) != 0)
	{
		return -1;
	}

	return 0;
}
int Send_ListenCtrlCancel_Req(int target_ip)
{
	ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&send_cmd,0,sizeof(ListenTalk_Req_t));
	send_cmd.ctrl_type=Listen_Ctrl_Cancel;
	send_cmd.trans_ip=0;
	send_cmd.trans_port=0;
	send_cmd.trans_type = 0;
	send_cmd.period=0;

	if(api_udp_c5_ipc_send_req(target_ip,CMD_LISTENTALK_REQ,(char*)&send_cmd,sizeof(ListenTalk_Req_t),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.rsp != 0)
	{
		return -1;
	}

	return 0;
}

int Send_ListenCtrlCancel_Rsp(int target_ip,int sn,int rsp)
{
	//ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	//int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&rsp_cmd,0,sizeof(ListenTalk_Rsp_t));
	rsp_cmd.ctrl_type=Listen_Ctrl_Cancel;
	rsp_cmd.rsp=rsp;
	

	if(api_udp_c5_ipc_send_rsp(target_ip,CMD_LISTENTALK_REP,sn,(char*)&rsp_cmd,sizeof(ListenTalk_Rsp_t)) != 0)
	{
		return -1;
	}

	return 0;
}

int Send_ListenCtrlDesubscriber_Req(int target_ip)
{
	ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&send_cmd,0,sizeof(ListenTalk_Req_t));
	send_cmd.ctrl_type=Listen_Ctrl_Desubscriber;
	send_cmd.trans_ip=0;
	send_cmd.trans_port=0;
	send_cmd.trans_type = 0;
	send_cmd.period=0;

	if(api_udp_c5_ipc_send_req(target_ip,CMD_LISTENTALK_REQ,(char*)&send_cmd,sizeof(ListenTalk_Req_t),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.rsp != 0)
	{
		return -1;
	}
	return 0;
}

int Send_ListenCtrlDesubscriber_Rsp(int target_ip,int sn,int rsp)
{
	//ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	//int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&rsp_cmd,0,sizeof(ListenTalk_Rsp_t));
	rsp_cmd.ctrl_type=Listen_Ctrl_Desubscriber;
	rsp_cmd.rsp=rsp;
	

	if(api_udp_c5_ipc_send_rsp(target_ip,CMD_LISTENTALK_REP,sn,(char*)&rsp_cmd,sizeof(ListenTalk_Rsp_t)) != 0)
	{
		return -1;
	}

	return 0;
}
int Send_ListenCtrlTalk_Req(int target_ip)
{
	ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&send_cmd,0,sizeof(ListenTalk_Req_t));
	send_cmd.ctrl_type=Listen_Ctrl_ReqTalk;
	send_cmd.trans_ip=0;
	send_cmd.trans_port=0;
	send_cmd.trans_type = 0;
	send_cmd.period=0;

	if(api_udp_c5_ipc_send_req(target_ip,CMD_LISTENTALK_REQ,(char*)&send_cmd,sizeof(ListenTalk_Req_t),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.rsp != 0)
	{
		return -1;
	}
	return 0;
}

int Send_ListenCtrlTalk_Rsp(int target_ip,int sn,int rsp)
{
	//ListenTalk_Req_t send_cmd;
	ListenTalk_Rsp_t rsp_cmd;
	//int rsp_len = sizeof(ListenTalk_Rsp_t);

	memset(&rsp_cmd,0,sizeof(ListenTalk_Rsp_t));
	rsp_cmd.ctrl_type=Listen_Ctrl_ReqTalk;
	rsp_cmd.rsp=rsp;
	

	if(api_udp_c5_ipc_send_rsp(target_ip,CMD_LISTENTALK_REP,sn,(char*)&rsp_cmd,sizeof(ListenTalk_Rsp_t)) != 0)
	{
		return -1;
	}

	return 0;
}
void Close_ListenTalk(void)
{
	if(Get_listenTalk_State()==listenTalk_AsServer||Get_listenTalk_State()==listenTalk_Broadcast)
	{
		delete_all_listen_userlist(0);
		if(get_au_service_state()!=0)
		{
			API_talk_off();
		}
		Set_listen_Talking(0);
		Set_listenTalk_State(listenTalk_Idle);
	}
	if(Get_listenTalk_State()==listenTalk_BeBroadcast||Get_listenTalk_State()==listenTalk_ListenClient)
	{
		Send_ListenCtrlDesubscriber_Req(Get_listenTalk_ServerIp());
		delete_all_listen_userlist(0);
		if(get_au_service_state()!=0)
		{
			API_talk_off();
		}
		Set_listen_Talking(0);
		Set_listenTalk_State(listenTalk_Idle);
	}
}

int Start_ListenSer(char *dev_num,int trans_type)
{
	int trans_ip;
	int trans_port;
	int ser_ip;
	GetIpRspData data;
	if(API_GetIpNumberFromNet(dev_num, NULL, NULL, 2, 8, &data) != 0)
	{
		return -1;
	}
	ser_ip=data.Ip[0];
	if(Send_ListenCtrlSubscriber_Req(ser_ip,300,trans_type,&trans_ip,&trans_port)!=0)
		return -1;
	Set_ListenSerDevNum(dev_num);
	Set_listenTalk_ServerIp(ser_ip);
	Set_listenTalk_State(listenTalk_ListenClient);
	API_talk_on_by_type2(IX_IX,trans_ip,0, trans_port,trans_type);
	return 0;
}
int Start_Listen_Talking(void)
{
	if(Get_listenTalk_State()==listenTalk_ListenClient&&Get_listen_Talking()==0)
	{
		if(Send_ListenCtrlTalk_Req(Get_listenTalk_ServerIp())==0)
		{
			activate_listen_subscriber_list(0,Get_listenTalk_ServerIp(),0,AUDIO_SERVER_UNICAST_PORT,0);
			Set_listen_Talking(1);
			return 0;
		}
	}

	return -1;	
}

int ListenTalk_Unlock(int locknum)
{
	if(Get_listenTalk_State()==listenTalk_ListenClient)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd;
		VtkUnicastCmd.call_type		= DsAndGl;
		VtkUnicastCmd.call_code		= locknum;
		
		//return api_udp_c5_ipc_send_data_by_nodeid(t_addr.gatewayid,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
		return api_udp_c5_ipc_send_data(Get_listenTalk_ServerIp(),VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,VtkUnicastCmd_Stru_WithoutSip_Length);
	}

	return -1;	
}