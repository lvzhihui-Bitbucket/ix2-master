
#ifndef _alsa_h
#define _alsa_h

#include "ak_ai.h"
#include "ak_ao.h"
#include "ak_common.h"
#include "ak_log.h"
#include "ak_mem.h"

 #include "RTOS.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <alsa/asoundlib.h>



#define ms_message	printf
#define ms_warning	printf
#define ms_debug		printf
#define ms_error		printf


struct _AlsaData
{
	char* pcmdev;
	char* mixdev;
};
typedef struct _AlsaData AlsaData;

enum CardMixerElem
{
	CARD_MASTER,
	CARD_PLAYBACK,
	CARD_CAPTURE,
	CARD_RING,	
};
typedef enum CardMixerElem CardMixerElem;


struct AlsaCtrl
{
	char* pcmdev;
	char* pcmdev_r;
	char* pcmdev_w;	
	char* mixdev;
	snd_pcm_t* handle;
	snd_pcm_t* handle_r;
	snd_pcm_t* handle_w;
	int rate;
	int nchannels;
	uint64_t read_samples;
};
typedef struct AlsaCtrl AlsaCtrl_t;

void scale_down( int16_t* samples, int count );
void scale_up( int16_t* samples, int count );

void alsa_card_set_level(  int level, CardMixerElem e );
int alsa_card_get_level( CardMixerElem e );

int alsa_ring_init( int rate, int channels, int vol );
void alsa_ring_uninit( struct AlsaCtrl* obj );
int alsa_ring_write( int handle, unsigned char *buf, int nsamples );

struct AlsaCtrl* alsa_write_init( int def_buf_cnt,  int rate, int channels, int vol, int gain );
struct AlsaCtrl* alsa_read_init( int def_buf_cnt,  int rate, int channels, int vol, int gain );

int alsa_read( snd_pcm_t* handle, unsigned char* buf, int nsamples );
int alsa_write( snd_pcm_t *handle, unsigned char *buf, int nsamples );


void alsa_write_uninit( struct AlsaCtrl* obj );
void alsa_read_uninit( struct AlsaCtrl* obj );

#endif

