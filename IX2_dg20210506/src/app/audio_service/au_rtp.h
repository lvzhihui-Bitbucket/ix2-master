
#ifndef _AU_RTP_H_
#define _AU_RTP_H_

#include "au_service.h"

/********************************************************************************
@function:
	audio rtp transfer initial before starting
@parameters:
	ip:	        server ip address
	port:	    server port
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_init( char* ip, short port );

/********************************************************************************
@function:
	start audio rtp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_start( void );

/********************************************************************************
@function:
	stop audio rtp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_stop( void );

/********************************************************************************
@function:
	set audio rtp output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_set_output( cb_io output );

/********************************************************************************
@function:
	get audio rtp input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_rtp_get_input( void );

/********************************************************************************
@function:
	get audio rtp output callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_rtp_get_output( void );

/********************************************************************************
@function:
	fetch rtp received packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_rtp_pop( unsigned char* pbuff, int limit );

#endif
