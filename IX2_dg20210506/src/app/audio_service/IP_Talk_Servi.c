
#include "IP_Talk_Servi.h"

#include "au_service.h"

AUDIO_CODE_t audio_code = G711_ALAW;


void SetAudioCode( AUDIO_CODE_t code )
{
	audio_code = code;
}

AUDIO_CODE_t GetAudioCode( void )
{
	return audio_code;
}

void LoadAudioCode( void )
{
	SetAudioCode( G711_ALAW );
}

int api_talk_turn_on( void )
{
	printf("%s\n",__FUNCTION__);

	AUDIO_DATA_t net;
	GetAudioType( &net );
	
	au_service_set_udp(net.target_ip_address,net.source_port,net.target_ip_address,net.target_port,net.mul_flag);

	au_service_start(net.type);
	return 0;
}

int api_talk_turn_off( void )
{
	printf("%s\n",__FUNCTION__);

	au_service_stop();
	delete_all_listen_userlist(0);

	return 0;
}

// lzh_20220415_s
void dx_audio_ds2dx_start()
{
	au_service_start(DS_DX);
}

void dx_audio_ds2dx_stop(void)
{
	au_service_stop();
}

void dx_audio_ds2app_start()
{
	// should first call "au_service_set_rtp"
	au_service_start(DS_DX_APP);
}

void dx_audio_ds2app_stop(void)
{
	au_service_stop();
}

// lzh_20220415_e
