

#ifndef _PLAYER_h
#define _PLAYER_h

#include"waveheader.h"

void api_ring_volume_set( int vol );
int api_ring_volume_get( void );

int api_RingPlay( int logicTune );
int api_WavePlayer_Stop( void );

int ms_read_wav_header_from_fd( wave_header_t *header, int fd );

#endif


