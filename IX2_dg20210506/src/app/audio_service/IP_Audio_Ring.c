
#include "waveheader.h"
#include "alsa.h"
#include "obj_PlayList.h"
#include "RTOS.h"
#include "OSQ.h"

#include "elog.h"
#include "task_Power.h"
//#define ms_message(...)
//#define ms_warning(...)
//#define ms_debug(...)
//#define ms_error(...)


typedef enum
{
	AUDIO_RING_IDLE,
	AUDIO_RING_PRE,
	AUDIO_RING_RUN,
	AUDIO_RING_STOP,	 
}
AUDIO_RING_STATE_t;

typedef struct
{
	int fd;
	int rate;
	int nchannels;
	int hsize;
	int samplesize;
	uint16 bits;	//cao_20170405
	BOOL is_raw;
} WAVE_DATA;


pthread_t task_Ring_dec;
pthread_attr_t attr_Ring_dec;

//pthread_t T_task_Ring_dec;
//pthread_attr_t T_attr_Ring_dec;


WAVE_DATA wave_data;
//WAVE_DATA T_wave_data;

//AlsaCtrl_t* ring_alsa;
//AlsaCtrl_t* T_ring_alsa;

static unsigned char TuneID = 0;
AUDIO_RING_STATE_t  AudioRingState = AUDIO_RING_IDLE;

int ringVolume = 0;
int RING_STEP_TAB[10]=
{
	0,-20,-15,-10,-5,1,5,10,15,20
};
void api_ring_volume_set( int vol )
{
	if(vol > 9)	
		vol = 9;
	ringVolume = RING_STEP_TAB[vol];
}

int api_ring_volume_get( void )
{
	return ringVolume;
}

static void api_ring_power_on( void )
{

}

static void api_ring_power_off( void )
{

}

static void LoadAudioRingVol( void )
{
	api_ring_volume_set( 1 );

	api_ring_power_on();
}


int ms_read_wav_header_from_fd( wave_header_t *header, int fd )
{
	int count;
	int skip;
	int hsize = 0;
	riff_t *riff_chunk 			=& header->riff_chunk;
	format_t *format_chunk	=& header->format_chunk;
	data_t *data_chunk		=& header->data_chunk;
	
	unsigned long len = 0;
	
	len = read( fd, (char*)riff_chunk, sizeof(riff_t) );
	
	if( len != sizeof(riff_t) )
	{
		goto not_a_wav;
	}
	
	if( 0 != strncmp(riff_chunk->riff, "RIFF", 4) || 0 != strncmp(riff_chunk->wave, "WAVE", 4) )
	{
		goto not_a_wav;
	}
	
	len = read( fd, (char*)format_chunk, sizeof(format_t) );
	
	if( len != sizeof(format_t) )
	{
		ms_warning("Wrong wav header: cannot read file");
		goto not_a_wav;
	}
	
	if( (skip = le_uint32(format_chunk->len) - 0x10) > 0 )
	{
		lseek( fd, skip, SEEK_CUR );
	}
	
	hsize = sizeof(wave_header_t) - 0x10 + le_uint32(format_chunk->len);
	
	count = 0;
	do{
		len = read( fd, data_chunk, sizeof(data_t) );

		if( len != sizeof(data_t) )
		{
			ms_warning("Wrong wav header: cannot read file\n");
			goto not_a_wav;
		}

		if( strncmp(data_chunk->data, "data", 4) != 0 )
		{
			ms_warning("skipping chunk=%s len=%i\n", data_chunk->data, data_chunk->len);
			lseek( fd, le_uint32(data_chunk->len), SEEK_CUR );
			count++;
			hsize += len + le_uint32(data_chunk->len);
		}
		else
		{
			hsize += len;
			break;
		}
	}while( count < 30 );

	return hsize;
	
	not_a_wav:
		/*rewind*/
		lseek( fd, 0, SEEK_SET );
		return -1;
}

static int read_wav_header( WAVE_DATA* data )
{
	wave_header_t header;
	format_t *format_chunk =& header.format_chunk;
	int ret = ms_read_wav_header_from_fd( &header, data->fd );
	
	if( ret == -1 ) 
		goto not_a_wav;
	
	data->rate		= le_uint32( format_chunk->rate );
	data->nchannels	= le_uint16( format_chunk->channel );
	
	if( data->nchannels == 0 ) 
		goto not_a_wav;
	
	data->bits = header.format_chunk.bitpspl;	//cao_20170405
	
	data->samplesize	= le_uint16( format_chunk->blockalign ) / data->nchannels;
	data->hsize		= ret;
	
	#ifdef WORDS_BIGENDIAN
	if( le_uint16(format_chunk->blockalign) == le_uint16(format_chunk->channel) * 2 )
		data->swap=TRUE;
	#endif
	
	data->is_raw = FALSE;
	
	return 0;

	not_a_wav:
	{
		/*rewind*/
		lseek( data->fd, 0, SEEK_SET );
		data->hsize 	= 0;
		data->is_raw = TRUE;
		return -1;
	}
}


static int player_open( WAVE_DATA* data, void *arg )
{
	const char *file = (const char*)arg;
	
	if( (data->fd = open(file,O_RDONLY|O_BINARY))==-1 )
	{
		ms_warning("Failed to open %s\n",file);
		return -1;
	}
	
	if( read_wav_header( data ) != 0 && strstr(file,".wav") )
	{
		ms_warning("File %s has .wav extension but wav header could be found.\n",file);
	}
	
	//ms_message("%s opened: rate=%i,channel=%i\n",file,d->rate,d->nchannels);
	printf("%s opened: rate=%i,channel=%i,d->samplesize=%i\n",file, data->rate, data->nchannels, data->samplesize);
	return 0;
}

pthread_mutex_t WavePlayerApi_Lock = PTHREAD_MUTEX_INITIALIZER;	

static void AudioRingCleanup( void *arg )	
{	
	pthread_mutex_lock(&WavePlayerApi_Lock);
	printf( " AudioRingCleanup \n ");
	alsa_ring_uninit( NULL );
	if(wave_data.fd != 0)				
	{
		close(wave_data.fd);
		wave_data.fd = 0;
	}
	AudioRingState = AUDIO_RING_STOP;		
	#ifdef PID_IXSE
	API_POWER_RING_OFF();
	#endif
	//printf( " >>>>>>>>>AUDIO_RING_IDLE<<<<<<<<<< \n ");
	pthread_mutex_unlock(&WavePlayerApi_Lock);	

	log_d( " >>>>>>>>>AUDIO_RING_IDLE<<<<<<<<<< \n ");	
}

static void  vtk_TaskProcessEvent_Ring( void )
{
	int hand_id;
	int size	= 0;
	int err 	= 0;
	char buff[4096];	
	static int init_flag=0;

	wave_data.fd = 0;

	PlayList_GetFileName( buff, TuneID );

	pthread_cleanup_push( AudioRingCleanup, NULL );
	
	if( player_open( &wave_data, buff ) == -1 )
	{
		goto ring_thread_exit;
	}
	hand_id = alsa_ring_init( wave_data.rate, wave_data.nchannels, ringVolume );
	if( hand_id == -1 )
	{
		goto ring_thread_exit;
	}

	//LoadAudioRingVol();
	//set_speaker_en(hand_id, 1);

	pthread_mutex_lock(&WavePlayerApi_Lock);
	AudioRingState = AUDIO_RING_RUN;
	//printf( " >>>>>>>>>AUDIO_RING_RUN<<<<<<<<<< \n ");	
	pthread_mutex_unlock(&WavePlayerApi_Lock);

	log_d( " >>>>>>>>>AUDIO_RING_RUN<<<<<<<<<< \n ");	
	#ifdef PID_IXSE
	if(init_flag==0)
	{
		init_flag=1;
		usleep(500*1000);
	}
	API_POWER_RING_ON();
	#endif
	
	//#ifdef PID_IXSE
	//rtp_process_set_high_prio();
	//#endif
	
	while( AudioRingState == AUDIO_RING_RUN)
	{
		memset(buff, 0x00, sizeof(buff));
		size = read( wave_data.fd, buff, sizeof(buff) );
		if( size == 0 )
		{
			//ak_ao_wait_play_finish(hand_id);
            ak_print_normal(MODULE_ID_AO, "\n\t read to the end of file\n");
			break;
		}
		//#ifndef PID_IXSE
		usleep(10);
		//#endif
		err = alsa_ring_write( hand_id, (unsigned char*)buff, size);
		//printf("=========alsa_ring_write ret = %d====================\n", err);
		if( err < 0)
			break;	
	}
ring_thread_exit:
	pthread_cleanup_pop( 1 );	
}

int api_RingPlay( int logicTune )
{
	int ret;
	#if 1
	pthread_mutex_lock(&WavePlayerApi_Lock);	
	if( AudioRingState == AUDIO_RING_IDLE )
	{
		AudioRingState = AUDIO_RING_PRE;
		//printf( " >>>>>>>>>AUDIO_RING_PRE<<<<<<<<<< \n ");	

		log_d( " >>>>>>>>>AUDIO_RING_PRE<<<<<<<<<< \n ");	

		pthread_mutex_unlock(&WavePlayerApi_Lock);

		TuneID = logicTune;
		
		pthread_attr_init( &attr_Ring_dec );
		if( pthread_create(&task_Ring_dec, &attr_Ring_dec, (void*)vtk_TaskProcessEvent_Ring, NULL ) != 0 )
		{
			printf( "Create task_Audio_dec pthread error! \n" );
		}
		ret = 0;
	}
	else 
	{
		pthread_mutex_unlock(&WavePlayerApi_Lock);	
		ret = 1;
	}
	#else
	return 0;
	#endif
}

int api_WavePlayer_Stop( void )
{
	#if 1
	int ret;
	pthread_mutex_lock(&WavePlayerApi_Lock);	
	if( AudioRingState == AUDIO_RING_IDLE )
	{
		pthread_mutex_unlock(&WavePlayerApi_Lock);	
		ret = 1;
	}
	else
	{	
		pthread_mutex_unlock(&WavePlayerApi_Lock);

		log_d( " >>>>>>>>>api_WavePlayer_Stop - 1 <<<<<<<<<< \n ");	

		int cnt = 0;
		while( AudioRingState != AUDIO_RING_RUN &&AudioRingState!=AUDIO_RING_STOP)
		{
			usleep(100*1000);
			if( ++cnt >= 50 )
				break;
		}
		AudioRingState=AUDIO_RING_STOP;
		//pthread_cancel( task_Ring_dec ) ;
		pthread_join( task_Ring_dec, NULL );
		AudioRingState=AUDIO_RING_IDLE;
		ret = 0;		

		log_d( " >>>>>>>>>api_WavePlayer_Stop - 2 <<<<<<<<<< \n ");	
	}
	#else
	return 0;
	#endif
}
int IfWavePlayer_Idle(void)
{
	if( AudioRingState == AUDIO_RING_IDLE ||AudioRingState == AUDIO_RING_STOP)
		return 1;
	return 0;
}
static char TuneFileName[100];

extern pthread_mutex_t WavePlayerApi_Lock;
static void  vtk_TaskProcessEvent_Ring_DS( void )
{
	int hand_id;
	int size		= 0,i;
	int err 		= 0;
	int samples	= 0;	
	char buff[1024];	
	//char* buffer;
	//API_add_log_item(10,"S_RING","enter ringplay thread",NULL);
	//pthread_mutex_lock(&WavePlayerApi_Lock);
	//PlayList_GetFileName( buff, TuneID );
	
	//czn_20161212_s
	//ring_alsa = NULL;
	wave_data.fd = 0;
	pthread_cleanup_push( AudioRingCleanup, NULL );
	
	if(player_open( &wave_data, TuneFileName) == -1)
	{
		API_add_log_item(10,"S_RING","open file wrong",NULL);
		//pthread_cleanup_pop( 1 );
		pthread_mutex_unlock(&WavePlayerApi_Lock);
	 	goto ring_thread_exit;
	}
	//czn_20161212_e
	
	hand_id = alsa_ring_init( wave_data.rate, wave_data.nchannels, ringVolume );
	if( hand_id == -1 )
	{
		goto ring_thread_exit;
	}

	//LoadAudioRingVol();
	//set_speaker_en(hand_id, 1);

	pthread_mutex_lock(&WavePlayerApi_Lock);
	AudioRingState = AUDIO_RING_RUN;
	//printf( " >>>>>>>>>AUDIO_RING_RUN<<<<<<<<<< \n ");	
	pthread_mutex_unlock(&WavePlayerApi_Lock);
	
	while( 1 )
	{
		memset(buff, 0x00, sizeof(buff));
		size = read( wave_data.fd, buff, sizeof(buff) );
		if( size == 0 )
		{
			//ak_ao_wait_play_finish(hand_id);
            ak_print_normal(MODULE_ID_AO, "\n\t read to the end of file\n");
			break;
		}
		usleep(10);
		err = alsa_ring_write( hand_id, (unsigned char*)buff, size);
		//printf("=========alsa_ring_write ret = %d====================\n", err);
		if( err < 0)
			break;	
	}
ring_thread_exit:
	//czn_20160104_s
	//pthread_mutex_lock(&WavePlayerApi_Lock);
	pthread_cleanup_pop( 1 );	
	//pthread_mutex_unlock(&WavePlayerApi_Lock);
	//czn_20160104_e
}

void api_RingPlay_DS(const char *fileName)
{
	if( AudioRingState == AUDIO_RING_IDLE )
	{
		AudioRingState = AUDIO_RING_RUN;

		//TuneID = logicTune;

		strcpy(TuneFileName, fileName);
		
		pthread_attr_init( &attr_Ring_dec );
		//czn_20161221	pthread_attr_setdetachstate( &attr_Ring_dec, PTHREAD_CREATE_DETACHED );
		if( pthread_create(&task_Ring_dec, &attr_Ring_dec, (void*)vtk_TaskProcessEvent_Ring_DS, NULL ) != 0 )
		{
			printf( "Create task_Audio_dec pthread error! \n" );
			exit(1);
		}
#if 0
		pthread_attr_init( &T_attr_Ring_dec );
		pthread_attr_setdetachstate( &T_attr_Ring_dec, PTHREAD_CREATE_DETACHED );
		if( pthread_create(&T_task_Ring_dec, &T_attr_Ring_dec, (void*)T_vtk_TaskProcessEvent_Ring, NULL ) != 0 )
		{
			printf( "Create task_Audio_dec pthread error! \n" );
			exit(1);
		}		
#endif		
	}
}

int write_wav_header_to_fd( FILE *fd)
{
	int count;
	int skip;
	int hsize = 0;
	int data_len;
	wave_header_t header;
	fseek(fd, 0, SEEK_END);
	data_len = ftell(fd);
	data_len-=(sizeof(riff_t)+sizeof(format_t)+sizeof(data_t));
	memcpy(header.riff_chunk.riff,"RIFF", 4);
	header.riff_chunk.len=data_len+24;
	memcpy(header.riff_chunk.wave,"WAVE", 4);

	memcpy(header.format_chunk.fmt,"fmt ", 4);
	header.format_chunk.len=16;
	header.format_chunk.type=1;
	header.format_chunk.channel=1;
	header.format_chunk.rate=8000;
	header.format_chunk.bps=8000*1*16/8;
	header.format_chunk.blockalign=1*16/2;
	header.format_chunk.bitpspl=16;

	memcpy(header.data_chunk.data,"data", 4);
	header.data_chunk.len=data_len;
	
	riff_t *riff_chunk 			=& header.riff_chunk;
	format_t *format_chunk	=& header.format_chunk;
	data_t *data_chunk		=& header.data_chunk;
	fseek( fd, 0, SEEK_SET );
	fwrite(riff_chunk, sizeof(riff_t), 1, fd);
	fwrite(format_chunk, sizeof(format_t), 1, fd);
	fwrite(data_chunk, sizeof(data_t), 1, fd);
	
	
}
FILE *create_wave_file(char *file_name)
{
	FILE * fp=fopen(file_name, "w");
	fseek( fp, sizeof(riff_t)+sizeof(format_t)+sizeof(data_t), SEEK_SET );
	return fp;
}
void wave_file_write(FILE *fd,char *data,int len)
{
	fwrite(data, len, 1, fd);
}

