
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ortp/ortp.h>

#include "au_alsa.h"
#include "au_udp.h"
#include "au_rtp.h"
#include "elog_forcall.h"

extern void AU_RLCON_SWITCH( unsigned char iEnable );
// switch audio line-up channel:
// dx - ds:     AU_RLCON_SWITCH(0);
// app - ds:    AU_RLCON_SWITCH(1);

#define AU_SERVICE_IDLE         0
#define AU_SERVICE_START        1
#define AU_SERVICE_RUN          2
#define AU_SERVICE_STOP         3
typedef struct
{
    int 	        state;      // running state
    AU_IO_TYPE      type;       // 
    pthread_t	    tid;        // capture thread

    int             src_ip;
    short           src_port;
    int             tar_ip;
    short           tar_port;
    int 		    mul_flag;
	
    char            rtp_ip[20];
    short           rtp_port;
}AU_SERVICE_TYPE;

AU_SERVICE_TYPE  one_au_service_ins={0};

static void* au_service_process_clean( void* arg )
{	
	printf("%s\n",__FUNCTION__);
    AU_SERVICE_TYPE* pins = (AU_SERVICE_TYPE*)arg;
    au_alsa_stop();
    au_udp_stop();
    au_rtp_stop();
}

int rtp_process_set_high_prio(void);
#if	defined(PID_DX470)||defined(PID_DX482)
static sem_t au_sem;
void trig_au_sem(void)
{
	sem_post(&au_sem);
}
#endif
static void* au_service_processing( void* arg )
{
    AU_SERVICE_TYPE* pins;
    mblk_t *im;
    unsigned char pdat[2048];
    int dat_len;

	pthread_cleanup_push( au_service_process_clean, arg );
	#if	defined(PID_DX470)||defined(PID_DX482)
		sem_init(&au_sem,0,0);
	#endif
    pins = (AU_SERVICE_TYPE*)arg;

    switch( pins->type )
    {
        case DS_DX:
            
            printf(">>>>au_service_processing: DS_DX >>>>\n");
            log_d(">>>>au_service_processing: DS_DX >>>>\n");
            au_alsa_start(pins->type);
            break;

        case DS_DX_APP:
            //AU_RLCON_SWITCH(1);
            printf(">>>>au_service_processing: DS_DX_APP >>>>\n");
            log_d(">>>>au_service_processing: DS_DX_APP >>>>\n");
            au_alsa_set_output(au_rtp_get_input());
            au_rtp_set_output(au_alsa_get_input());
            au_alsa_start(pins->type);
            au_rtp_start();
            break;

        case IX_APP:
            printf(">>>>au_service_processing: ix471-app >>>>\n");
            log_d(">>>>au_service_processing: ix471-app >>>>\n");
            au_alsa_set_output(au_rtp_get_input());
            au_rtp_set_output(au_alsa_get_input());
            au_alsa_start(pins->type);
            au_rtp_start();
            break;

        case IX_IX2_APP:
            printf(">>>>au_service_processing: ds-app >>>>\n");
            log_d(">>>>au_service_processing: ds-app >>>>\n");
            au_udp_set_output(au_rtp_get_input());
            au_rtp_set_output(au_udp_get_input());
            au_udp_start();
            au_rtp_start();        
            break;

        case IX_IX:
            printf(">>>>au_service_processing: ix471-dx >>>>\n");
            log_d(">>>>au_service_processing: ix471-dx >>>>\n");
            au_alsa_set_output(au_udp_get_input());
            au_udp_set_output(au_alsa_get_input());
            au_alsa_start(pins->type);
            au_udp_start();
            break;
    }

    rtp_process_set_high_prio();

    one_au_service_ins.state   = AU_SERVICE_RUN;

    log_d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> au_service_start - 2 <<<<<<<<<<<<<<<<<<<<<<<<<<\n");

	while( one_au_service_ins.state== AU_SERVICE_RUN)
	{
		#if	defined(PID_DX470)||defined(PID_DX482)
		if( sem_wait_ex2(&au_sem, 100) !=0)
			continue;
		#endif
        // DS_DX_APP
        if( pins->type == DS_DX_APP )  
        {
            // get alsa data
            #if 0
            if( dat_len = au_alsa_pop(pdat,2048) )
            {
                printf(">>>>>>>>>>> au_alsa_pop >>>>>>>>>\n");
            }
            // get udp data
            if( dat_len = au_rtp_pop(pdat, 2048) )
            {
                printf(">>>>>>>>>>> au_rtp_pop >>>>>>>>>\n");
            }
		#endif
		 if( (dat_len = au_rtp_pop(pdat, 2048))>0 )
            {
                //printf(">>>>>>>>>>> au_rtp_pop >>>>>>>>>%d\n",dat_len);
                (*(au_alsa_get_input()))(pdat,dat_len);
		//continue;
            }	
        }

        // ix471-app  1
        else if( pins->type == IX_APP )  
        {
            // get alsa data
            if( dat_len = au_alsa_pop(pdat,2048) )
            {
                printf(">>>>>>>>>>> au_alsa_pop >>>>>>>>>\n");
            }
            // get udp data
            if( dat_len = au_rtp_pop(pdat, 2048) )
            {
                printf(">>>>>>>>>>> au_rtp_pop >>>>>>>>>\n");
            }
        }
        // ds-app 2
        else if( pins->type == IX_IX2_APP )
        {
            // get udp data
            if( dat_len = au_udp_pop(pdat,2048) )
            {
                //printf(">>>>>>>>>>> au_udp_pop >>>>>>>>>\n");
            }
            // get rtp data
            if( dat_len = au_rtp_pop(pdat, 2048) )
            {
                printf(">>>>>>>>>>> au_rtp_pop,len=%d >>>>>>>>>\n",dat_len);
            }
        }
        // ix471-dx  0
        else if( pins->type == IX_IX )
        {
            // get alsa data
            if( dat_len = au_alsa_pop(pdat,2048) )
            {
                //printf(">>>>>>>>>>> au_alsa_pop >>>>>>>>>\n");
            }
            // get udp data
            if( dat_len = au_udp_pop(pdat, 2048) )
            {
                //printf(">>>>>>>>>>> au_udp_pop >>>>>>>>>\n");
            }
        }
	#if	defined(PID_DX470)||defined(PID_DX482)
	#else
     	usleep(100*1000); 
	#endif
	}	
	pthread_cleanup_pop( 1 );
}

/********************************************************************************
@function:
	start audio service
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_start(AU_IO_TYPE type)
{
    if( one_au_service_ins.state == AU_SERVICE_IDLE )
    {
        one_au_service_ins.state    = AU_SERVICE_START;
        one_au_service_ins.type     = type;
        
        log_d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> au_service_start - 1 <<<<<<<<<<<<<<<<<<<<<<<<<<\n");

        au_alsa_init(type);

        if( one_au_service_ins.type == DS_DX_APP )
        {
            au_rtp_init(one_au_service_ins.rtp_ip,one_au_service_ins.rtp_port);
        }
        else
        {
            au_rtp_init(one_au_service_ins.rtp_ip,one_au_service_ins.rtp_port);
            if( au_udp_init(one_au_service_ins.src_ip,one_au_service_ins.src_port,one_au_service_ins.tar_ip,one_au_service_ins.tar_port,one_au_service_ins.mul_flag) == -1 )
            {
                one_au_service_ins.state   = AU_SERVICE_IDLE;
                printf(">>>>>>>>>>>au_service_start failed!>>>>>>>>>>>>>>\n");
                return -1;
            }
        }
        pthread_create(&one_au_service_ins.tid, NULL, au_service_processing, (void*)&one_au_service_ins);
        return 0;
    }
    else
        return -1;
}

/********************************************************************************
@function:
	stop audio service
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_stop(void)
{
    if( one_au_service_ins.state == AU_SERVICE_IDLE )
    {
        return -1;
    }

    log_d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> au_service_stop - 1 <<<<<<<<<<<<<<<<<<<<<<<<<<\n");

    int wait_cnt = 0;
    while( one_au_service_ins.state != AU_SERVICE_RUN )
    {
        usleep(100*1000);
        if( ++wait_cnt > 50 )
        {
            one_au_service_ins.state = AU_SERVICE_IDLE;
            log_d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> au_service_stop - 1.1 <<<<<<<<<<<<<<<<<<<<<<<<<<\n");
            return -1;
        }
    }
	 one_au_service_ins.state=AU_SERVICE_STOP;
    printf("%s start\n",__FUNCTION__);
    //pthread_cancel( one_au_service_ins.tid ) ;
    pthread_join( one_au_service_ins.tid, NULL );
    printf("%s end\n",__FUNCTION__);
    
    one_au_service_ins.state   = AU_SERVICE_IDLE;

    log_d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> au_service_stop - 2 <<<<<<<<<<<<<<<<<<<<<<<<<<\n");
    return 0;
}

/********************************************************************************
@function:
	set audio service transfer type
@parameters:
	type:	    type
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_type(AU_IO_TYPE type)
{
    printf("============au_service_set_type: state[%d],type[%d]=========\n",one_au_service_ins.state,one_au_service_ins.type);
    if( one_au_service_ins.state == AU_SERVICE_IDLE )
    {	
        one_au_service_ins.type     = type;
        return 0;
    }
    else
        return -1;
}

/********************************************************************************
@function:
	set audio service udp parameters
@parameters:
	src_ip:	    source ip address
	src_port:	source port
	tar_ip:	    target ip address
	tar_port:	target port
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_udp(int src_ip, short src_port, int tar_ip, short tar_port,int mul_flag)
{
    if( one_au_service_ins.state == AU_SERVICE_IDLE )
    {	
        one_au_service_ins.src_ip   = src_ip;
        one_au_service_ins.src_port = src_port;
        one_au_service_ins.tar_ip   = tar_ip;
        one_au_service_ins.tar_port = tar_port;
	one_au_service_ins.mul_flag=mul_flag;
		
        return 0;
    }
    else
        return -1;
}

/********************************************************************************
@function:
	set audio service rtp parameters
@parameters:
	ip:	        server ip address
	port:	    server port
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_rtp(char* ip, short port)
{
    if( one_au_service_ins.state == AU_SERVICE_IDLE )
    {	
        memcpy( one_au_service_ins.rtp_ip, ip, 20);
        one_au_service_ins.rtp_port = port;
        return 0;
    }
    else
        return -1;
}
int get_au_service_state(void)
{
	return one_au_service_ins.state;
}




