﻿

#include <stdio.h>
#include <stdint.h>
#include <memory.h>
#include <stdlib.h>

#include "alsa.h"
#include "elog.h"
//#include "obj_IoInterface.h"

#define USE_AK_LIB_1_06		1
// lzh_20210331_s
#if USE_AK_LIB_1_06

#if defined(PID_IXSE) 
/*
    设置各种音频处理参数结构体为默认值，默认值是从ak_audio_config.h抄的。
    参数仅作参考，实际参数要根据不同产品独立设定。
    1：NEAR(ai), ak_audio_nr_attr
    2：NEAR(ai), ak_audio_agc_attr
    3：NEAR(ai), ak_audio_aec_attr
    4：NEAR(ai), ak_audio_aslc_attr
    5：FAR(ao),  ak_audio_nr_attr
    6：FAR(ao),  ak_audio_aslc_attr
    7：NEAR(ai), ak_ai_set_eq_attr
    8：FAR(ao),  ak_ai_set_eq_attr
*/
static void setup_default_audio_argument(void *audio_args, char args_type)
{
	#if 1
    struct ak_audio_agc_attr    default_ai_agc_attr     = {24576, 4, 0, 20, 0, 1};
    struct ak_audio_aec_attr    default_ai_aec_attr     = {0, 1024, 1024, 0, 512, 1, 16384};
    struct ak_audio_nr_attr     default_ai_nr_attr      = {-30, 0, 1};
    struct ak_audio_aslc_attr   default_ai_aslc_attr    = {9830, 0, 0};
	#else
    struct ak_audio_nr_attr     default_ai_nr_attr      = {-40, 0, 1};
    struct ak_audio_agc_attr    default_ai_agc_attr     = {24576, 4, 0, 80, 0, 1};
    //struct ak_audio_aec_attr    default_ai_aec_attr     = {0, 1024, 1024, 0, 512, 1, 0};
    struct ak_audio_aec_attr    default_ai_aec_attr     = {0, 1024, 1024, 0, 512, 1, 0};
    struct ak_audio_aslc_attr   default_ai_aslc_attr    = {32768, 5, 0};
	#endif
    struct ak_audio_eq_attr     default_ai_eq_attr      = {
    0,
    10,
    {50, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {717, 717, 717, 717, 717, 717, 717, 717, 717, 717},
    {TYPE_HPF, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1},
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    struct ak_audio_nr_attr     default_ao_nr_attr      = {0, 0, 0};
    struct ak_audio_aslc_attr   default_ao_aslc_attr    = {32768, 0, 0};
    struct ak_audio_eq_attr     default_ao_eq_attr      = {
    0,
    10,
    {50, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {717, 717, 717, 717, 717, 717, 717, 717, 717, 717},
    {TYPE_HPF, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1, TYPE_PF1},
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    switch (args_type) {
    case 1:
        *(struct ak_audio_nr_attr*)audio_args = default_ai_nr_attr;
        break;
    case 2:
        *(struct ak_audio_agc_attr*)audio_args = default_ai_agc_attr;
        break;
    case 3:
        *(struct ak_audio_aec_attr*)audio_args = default_ai_aec_attr;
        break;
    case 4:
        *(struct ak_audio_aslc_attr*)audio_args = default_ai_aslc_attr;
        break;
    case 5:
        *(struct ak_audio_nr_attr*)audio_args = default_ao_nr_attr;
        break;
    case 6:
        *(struct ak_audio_aslc_attr*)audio_args = default_ao_aslc_attr;
        break;
    case 7:
        *(struct ak_audio_eq_attr*)audio_args = default_ai_eq_attr;
        break;
    case 8:
        *(struct ak_audio_eq_attr*)audio_args = default_ao_eq_attr;
        break;

    default:
        break;
    }

    return;
}

#else
static void setup_default_audio_argument(void *audio_args, char args_type)
{
    struct ak_audio_nr_attr     default_ai_nr_attr      = {-40, 0, 1};
    struct ak_audio_agc_attr    default_ai_agc_attr     = {24576, 4, 0, 20, 0, 1};
    struct ak_audio_aec_attr    default_ai_aec_attr     = {0, 1024, 1024, 0, 512, 1};
    struct ak_audio_aslc_attr   default_ai_aslc_attr    = {9830, 0, 0};

    struct ak_audio_nr_attr     default_ao_nr_attr      = {-40, 0, 1};
    struct ak_audio_aslc_attr   default_ao_aslc_attr    = {9830, 0, 0};

    switch(args_type)
    {
    case 1:
	//default_ai_nr_attr.noise_suppress_db=API_Para_Read_Int(AiNr_noise_suppress_db);
	//default_ai_nr_attr.crc=API_Para_Read_Int(AiNr_crc);
	//default_ai_nr_attr.enable=API_Para_Read_Int(AiNr_enable);
        *(struct ak_audio_nr_attr*)audio_args = default_ai_nr_attr;
        break;
    case 2:
        *(struct ak_audio_agc_attr*)audio_args = default_ai_agc_attr;
        break;
    case 3:
        *(struct ak_audio_aec_attr*)audio_args = default_ai_aec_attr;
        break;
    case 4:
        *(struct ak_audio_aslc_attr*)audio_args = default_ai_aslc_attr;
        break;
    case 5:
	//default_ao_nr_attr.noise_suppress_db=API_Para_Read_Int(AoNr_noise_suppress_db);
	//default_ao_nr_attr.crc=API_Para_Read_Int(AoNr_crc);
	//default_ao_nr_attr.enable=API_Para_Read_Int(AoNr_enable);
        *(struct ak_audio_nr_attr*)audio_args = default_ao_nr_attr;
        break;
    case 6:
        *(struct ak_audio_aslc_attr*)audio_args = default_ao_aslc_attr;
        break;
    default:
        break;
    }

    return;
}
#endif

#endif
// lzh_20210331_e

/*in case of troubles with a particular driver, try incrementing ALSA_PERIOD_SIZE
to 512, 1024, 2048, 4096...
then try incrementing the number of periods*/
#define ALSA_PERIODS 			8
#define ALSA_PERIOD_SIZE 		1024

void scale_down( int16_t* samples, int count )
{
	int i;
	for( i = 0; i < count; ++i )
		samples[i] = samples[i] >> 1;
}

void scale_up( int16_t* samples, int count )
{
	int i;
	for( i = 0; i < count; ++i )
		samples[i] = samples[i] << 1;
}

typedef enum 
{
	CAPTURE, 
	PLAYBACK, 
	CAPTURE_SWITCH, 
	PLAYBACK_SWITCH
} MixerAction;

static int get_mixer_element( snd_mixer_t *mixer, const char *name, MixerAction action )
{
}


static void set_mixer_element( snd_mixer_t *mixer, const char *name, int level, MixerAction action )
{
}

// lzh_20181224_s
static void set_mixer_element2( snd_mixer_t *mixer, const char *name, int level, MixerAction action )
{
}

// lzh_20181224_e

void alsa_card_set_level(  int level, CardMixerElem e )
{	
}

// lzh_20181224_s
void alsa_card_set_level2(  int level, CardMixerElem e )
{	
}
// lzh_20181224_e

int alsa_card_get_level( CardMixerElem e )
{
}

void set_speaker_en(int handle, int en)
{
	if(en)
		ak_ao_set_speaker(handle, AUDIO_FUNC_ENABLE);
	else
		ak_ao_set_speaker(handle, AUDIO_FUNC_DISABLE);
}

int ao_ring_id = -1;
int alsa_ring_init( int rate, int channels, int vol )
{
	struct ak_audio_out_param ao_param;
	ao_param.pcm_data_attr.sample_bits = AK_AUDIO_SMPLE_BIT_16;
	ao_param.pcm_data_attr.channel_num = channels;
	ao_param.pcm_data_attr.sample_rate = rate;
#if defined(PID_IXSE) 
	ao_param.dev_id = DEV_DAC;
	#else
	ao_param.dev_id = 0;
	#endif
	#if defined(PID_DX470)||defined(PID_DX482)
	alsa_write_all();
	if(ao_ring_id==-1)
		return -1;
	ak_ao_reset_params(ao_ring_id,&ao_param);
	#else
	/* open ao */
	if(ak_ao_open(&ao_param, &ao_ring_id)) 
	{
		printf("=========ak_ao_open error!====================\n");
		ao_ring_id=-1;
		return -1;
	}
	#endif

	ak_ao_set_gain(ao_ring_id, 0);
    ak_ao_clear_frame_buffer(ao_ring_id);
    ak_ao_set_dev_buf_size(ao_ring_id, AK_AUDIO_DEV_BUF_SIZE_4096);
	if(vol == 0)
	{
		ak_ao_set_gain(ao_ring_id, 0);
	}
	else
	{
		ak_ao_set_gain(ao_ring_id, 2);	// 0:mute,6:max
	}
    ak_ao_set_volume(ao_ring_id, vol);
	//int gain_dat;
    //ak_ao_get_gain(ao_ring_id, &gain_dat);
	//printf("***ak_ao_get_gain[%d]***\n",gain_dat);
    //ak_ao_get_volume(ao_ring_id, &gain_dat);
	//printf("***ak_ao_get_volume[%d]***\n",gain_dat);
	
	if(vol == 0)
		ak_ao_set_speaker(ao_ring_id, AUDIO_FUNC_DISABLE);
	else
		ak_ao_set_speaker(ao_ring_id, AUDIO_FUNC_ENABLE);

	return ao_ring_id;
	
}

void alsa_ring_uninit( struct AlsaCtrl* obj )
{
	printf("=========alsa_ring_exit!!!===handle_id = %d=================\n", ao_ring_id);
	//ak_ao_wait_play_finish();
	/* close ao */
	//ak_ao_set_speaker(ao_ring_id, AUDIO_FUNC_DISABLE);
	if(ao_ring_id!=-1)
	{
    	ak_ao_set_volume(ao_ring_id, 0);	// -90-20
		ak_ao_set_gain(ao_ring_id, 0);	// 0:mute,6:max
		ak_ao_set_speaker(ao_ring_id, AUDIO_FUNC_DISABLE);
		#if defined(PID_DX470)||defined(PID_DX482)
		#else
		ak_ao_close(ao_ring_id);
		ao_ring_id=-1;
		#endif
	}
}

int alsa_ring_write( int handle, unsigned char *buf, int nsamples )
{
	int send_len;
	if (ak_ao_send_frame(handle, buf, nsamples, &send_len)) 
	{
		ak_print_error_ex(MODULE_ID_AO, "write pcm to DA error!\n");
		return -1;
	}
	else
	{
		return send_len;
	}	
}

int ao_handle_id = -1;

struct AlsaCtrl* alsa_write_init( int def_buf_len,  int rate, int channels, int vol, int gain )
{
	struct ak_audio_out_param ao_param;
	ao_param.pcm_data_attr.sample_bits = AK_AUDIO_SMPLE_BIT_16;
	ao_param.pcm_data_attr.channel_num = channels; //1; //channels;
	ao_param.pcm_data_attr.sample_rate = rate; //8000; //rate;
	ao_param.dev_id = 0;
	#if defined(PID_DX470)||defined(PID_DX482)
	alsa_write_all();
	if(ao_handle_id==-1)
		return NULL;
	ak_ao_reset_params(ao_handle_id,&ao_param);
	#else
	/* open ao */
	if(ak_ao_open(&ao_param, &ao_handle_id)) 
	{
		ao_handle_id=-1;
		printf("ak_ao_open failed!\n");
		log_d("ak_ao_open failed!\n");
		return NULL;
	}
	#endif
	// disable
    //ak_ao_clear_frame_buffer(ao_handle_id);
	// disable
    //ak_ao_set_dev_buf_size(ao_handle_id, AK_AUDIO_DEV_BUF_SIZE_4096);
	#if USE_AK_LIB_1_06
	int gain_dat=-1;
    ak_ao_set_volume(ao_handle_id, vol);
    ak_ao_get_volume(ao_handle_id, &gain_dat);
	printf("ak_ao_get_vol[%d]\n",gain_dat);
	log_d("ak_ao_get_vol[%d]\n",gain_dat);	
	//if(API_Para_Read_Int(Aec_En)==1)
	{
		// lzh_20210331_s
		struct ak_audio_nr_attr nr_attr;
		setup_default_audio_argument(&nr_attr, 5);
		ak_ao_set_nr_attr(ao_handle_id, &nr_attr);
		// lzh_20210331_e
	}

	ak_ao_set_gain(ao_handle_id, gain); // 0:mute,6:max
    ak_ao_get_gain(ao_handle_id, &gain_dat);
	printf("ak_ao_get_gain[%d]\n",gain_dat);
	log_d("ak_ao_get_gain[%d]\n",gain_dat);
	#endif
 	ak_ao_set_speaker(ao_handle_id, AUDIO_FUNC_ENABLE);

	return 1;
}

void alsa_write_uninit( struct AlsaCtrl* obj )
{
	//ak_ao_wait_play_finish(ao_handle_id);
	/* close ao */
	#if defined(PID_DX470)||defined(PID_DX482)
	 ak_ao_set_speaker(ao_handle_id, AUDIO_FUNC_DISABLE);
	//alsa_write_close_all();
	#else
	ak_ao_close(ao_handle_id);
	#endif
}

int alsa_write( snd_pcm_t *handle, unsigned char *buf, int nsamples )
{
	int send_len;
	/* send frame and play */
	//ak_print_error_ex(MODULE_ID_AO, "write pcm to DA %d bytes!\n",nsamples);
	if (ak_ao_send_frame(ao_handle_id, buf, nsamples, &send_len)) 
	{
		ak_print_error_ex(MODULE_ID_AO, "write pcm to DA error!\n");
		return -1;
	}
	else
	{
		//ak_print_error_ex(MODULE_ID_AO, "write pcm to DA %d bytes ok!\n",send_len);
		return send_len;
	}	
}

int ao_handle_all = -1;
//#if defined(PID_DX470)||defined(PID_DX482)
void alsa_write_all(void)
{
	if(ao_handle_all==-1)
	{
		struct ak_audio_out_param ao_param;
		ao_param.pcm_data_attr.sample_bits = AK_AUDIO_SMPLE_BIT_16;
		ao_param.pcm_data_attr.channel_num = 1; //1; //channels;
		ao_param.pcm_data_attr.sample_rate = 8000; //8000; //rate;
		ao_param.dev_id = 0;
		
		#if 1
		if(ak_ao_open(&ao_param, &ao_handle_all)) 
		{
			return ;
		}
		ao_handle_id=ao_handle_all;
		ao_ring_id=ao_handle_all;
		
		#endif
	}
}
 #if  0
void alsa_write_close_all(void)
{
	if(ao_handle_all!=-1)
	{
		ak_ao_close(ao_handle_all);
		ao_handle_all=-1;
		ao_handle_id=-1;
		ao_ring_id=-1;
	}
}
 #endif
//#endif

/*
 * get_frame_len: get frame length
 * encode_type[IN]: encode type
 * sample_rate[IN]: sample rate
 * channel[IN]: channel number
 * return: 
 */
static int get_pcm_frame_len(int sample_rate, int channel)
{
    int frame_len = 960;

    switch (sample_rate)
    {
    case AK_AUDIO_SAMPLE_RATE_8000:
        //frame_len = (channel == AUDIO_CHANNEL_MONO) ? 960 :1600;
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 1280 :1600;
        break;
    case AK_AUDIO_SAMPLE_RATE_11025:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 882 :1764;
        break;
    case AK_AUDIO_SAMPLE_RATE_12000:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 960 :1920;
        break;
    case AK_AUDIO_SAMPLE_RATE_16000:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 1280 :2560;
        break;
    case AK_AUDIO_SAMPLE_RATE_22050:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 1764 :3528;
        break;
    case AK_AUDIO_SAMPLE_RATE_24000:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 1920 :2840;
        break;
    case AK_AUDIO_SAMPLE_RATE_32000:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 2560 :5120;
        break;
    case AK_AUDIO_SAMPLE_RATE_44100:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 3528 :5292;
        break;
    case AK_AUDIO_SAMPLE_RATE_48000:
        frame_len = (channel == AUDIO_CHANNEL_MONO) ? 4800 :5760;
        break;

    default:
        break;
    }

    ak_print_notice_ex(MODULE_ID_AENC, "frame_len=%d\n", frame_len);

    return frame_len;
}


int ai_handle_id = -1;
struct frame frame = {0};

void set_aec_en(void)
{
	//ak_ai_enable_aec(ai_handle_id, AUDIO_FUNC_ENABLE);
	//printf("=======set_aec_en ======\n");
	ak_ai_enable_aec(ai_handle_id, AUDIO_FUNC_DISABLE);
}
void set_agc_en(void)
{
	//printf("=======set_agc_en ======\n");
	ak_ai_enable_agc(ai_handle_id, AUDIO_FUNC_ENABLE);
}
void set_nr_en(void)
{
	ak_ai_enable_nr(ai_handle_id, AUDIO_FUNC_ENABLE);
	printf("=======set_nr_en ======\n");
}


struct AlsaCtrl* alsa_read_init( int def_buf_cnt,  int rate, int channels, int vol, int gain )
{
	int ret;

#if defined(PID_IXSE) 
    struct ak_audio_in_param param;
    memset(&param, 0, sizeof(struct ak_audio_in_param));
    ak_print_error_ex(MODULE_ID_AI, "sample_rate=%d\n", rate);
    param.pcm_data_attr.sample_rate = 8000; //rate;               // set sample rate
    param.pcm_data_attr.sample_bits = AK_AUDIO_SMPLE_BIT_16;    // sample bits only support 16 bit
    param.pcm_data_attr.channel_num = 1; //channels;    				// channel number
    param.dev_id = DEV_DAC;     
    if (ak_ai_open(&param, &ai_handle_id)) 
    {
        ak_print_normal(MODULE_ID_AI, "*** ak_ai_open failed. ***\n");
        log_d("*** ak_ai_open failed. ***\n");
		ai_handle_id	= -1;
		return NULL;
    }

    ret = ak_ai_set_frame_length(ai_handle_id, def_buf_cnt);
    if (ret) 
    {
        ak_print_normal(MODULE_ID_AI, "*** set ak_ai_set_frame_interval failed. ***\n");
        log_d("*** set ak_ai_set_frame_interval failed. ***\n");
        ak_ai_close(ai_handle_id);
    }

	if (ak_ai_set_source(ai_handle_id, AI_SOURCE_MIC)) {
		ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_source failed\n");
		return AK_FAILED;
	}

	/* enable_nr, nr only support 8000 or 16000 sample rate */
	struct ak_audio_nr_attr nr_attr;
	setup_default_audio_argument(&nr_attr, 1);
	if (ak_ai_set_nr_attr(ai_handle_id, &nr_attr)) {
		ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_nr_attr failed\n");
		return AK_FAILED;
	}

	/*enable_agc, agc only support 8000 or 16000 sample rate */ 
	struct ak_audio_agc_attr agc_attr;
	setup_default_audio_argument(&agc_attr, 2);
	if (ak_ai_set_agc_attr(ai_handle_id, &agc_attr)) {
		ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_agc_attr failed\n");
		return AK_FAILED;
	}

	/*enable_aec, aec only support 8000 or 16000 sample rate, aec will real open when ai and ao all open */ 
	struct ak_audio_aec_attr aec_attr;
	setup_default_audio_argument(&aec_attr, 3);
	if (ak_ai_set_aec_attr(ai_handle_id, &aec_attr)) {
		ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_aec_attr failed\n");
		return AK_FAILED;
	}

	if (ak_ai_set_gain(ai_handle_id, 5)) {
		ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_gain failed\n");
		return AK_FAILED;
	}

    struct ak_audio_aslc_attr ai_aslc_attr;
    setup_default_audio_argument(&ai_aslc_attr, 4);
    if (ak_ai_set_aslc_attr(ai_handle_id, &ai_aslc_attr)) {
        ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_aslc_attr failed\n");
        return AK_FAILED;
    }

    struct ak_audio_eq_attr ai_eq_attr;
    setup_default_audio_argument(&ai_eq_attr, 7);
    if (ak_ai_set_eq_attr(ai_handle_id, &ai_eq_attr)) {
        ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_eq_attr failed\n");
        return AK_FAILED;
    }

    int frame_len = 0;
    if (ak_ai_get_frame_length(ai_handle_id, &frame_len)) {
        ak_print_error_ex(MODULE_ID_APP, "ak_ai_set_frame_length failed\n");
        return AK_FAILED;
    }
    //frame_interval = ak_audio_len_to_interval(&param.pcm_data_attr, frame_len);

	#else
    struct ak_audio_in_param param;
    memset(&param, 0, sizeof(struct ak_audio_in_param));
    ak_print_error_ex(MODULE_ID_AI, "sample_rate=%d\n", rate);
    param.pcm_data_attr.sample_rate = 8000; //rate;               // set sample rate
    param.pcm_data_attr.sample_bits = AK_AUDIO_SMPLE_BIT_16;    // sample bits only support 16 bit
    param.pcm_data_attr.channel_num = 1; //channels;    				// channel number
    param.dev_id = 0;     
    if (ak_ai_open(&param, &ai_handle_id)) 
    {
        ak_print_normal(MODULE_ID_AI, "*** ak_ai_open failed. ***\n");
        log_d("*** ak_ai_open failed. ***\n");
		ai_handle_id	= -1;
		return NULL;
    }

    ret = ak_ai_set_frame_length(ai_handle_id, def_buf_cnt);
    if (ret) 
    {
        ak_print_normal(MODULE_ID_AI, "*** set ak_ai_set_frame_interval failed. ***\n");
        log_d("*** set ak_ai_set_frame_interval failed. ***\n");
        ak_ai_close(ai_handle_id);
    }

    /* set source, source include mic and linein */
    ret = ak_ai_set_source(ai_handle_id, AI_SOURCE_MIC);
    //ret = ak_ai_set_source(ai_handle_id, AI_SOURCE_LINEIN);
    if (ret) 
    {
        ak_print_normal(MODULE_ID_AI, "*** set ak_ai_open failed. ***\n");
        log_d("*** set ak_ai_open failed. ***\n");
        ak_ai_close(ai_handle_id);
		return NULL;
    }
#if USE_AK_LIB_1_06
	int gain_dat=-1;
	ak_ai_set_gain(ai_handle_id, gain);
    ret = ak_ai_get_gain(ai_handle_id, &gain_dat);
	printf("ak_ai_get_gain[%d]\n",gain_dat);
	log_d("ak_ai_get_gain[%d]\n",gain_dat);

    ak_ai_set_volume(ai_handle_id, vol);
    ret = ak_ai_get_volume(ai_handle_id, &gain_dat);
	printf("ak_ai_get_volume[%d]\n",gain_dat);
	log_d("ak_ai_get_volume[%d]\n",gain_dat);
	
	//if(API_Para_Read_Int(Aec_En)==1)
	{
		// enable ai nr
		struct ak_audio_nr_attr nr_attr;
		setup_default_audio_argument(&nr_attr, 1);
		ak_ai_set_nr_attr(ai_handle_id, &nr_attr);
		// enable ai_agc
		struct ak_audio_agc_attr agc_attr;
		setup_default_audio_argument(&agc_attr, 2);
		ak_ai_set_agc_attr(ai_handle_id, &agc_attr);
		// enable ai_aec
		// software aec config
		// ak_ai_set_aec_attr
		struct ak_audio_aec_attr aec_attr;
		setup_default_audio_argument(&aec_attr, 3);
		ak_ai_set_aec_attr(ai_handle_id, &aec_attr);

	}
	#endif

    ret = ak_ai_clear_frame_buffer(ai_handle_id);
    if (ret) 
    {
        ak_print_error(MODULE_ID_AI, "*** set ak_ai_clear_frame_buffer failed. ***\n");
        log_d("*** set ak_ai_clear_frame_buffer failed. ***\n");
    }
	#endif
	#if defined(PID_DX470)||defined(PID_DX482)
	#else
	usleep(300*1000);

	ret = ak_ai_start_capture(ai_handle_id);
	if (ret) 
	{
		ak_print_error(MODULE_ID_AI, "*** ak_ai_start_capture failed. ***\n");
		log_d("*** ak_ai_start_capture failed. ***\n");
		ak_ai_close(ai_handle_id);
		ai_handle_id = -1;
	}
	#endif
	return NULL;
}

int alsa_read( snd_pcm_t* handle, unsigned char* buf, int nsamples )
{
	int ret;
	
	ret = ak_ai_get_frame(ai_handle_id, &frame, 0);
	if (ret) 
	{
		if (ERROR_AI_NO_DATA == ret)
		{
			ak_sleep_ms(10);
		}	
		return -1;
	}
	
	if (!frame.data || frame.len <= 0)
	{
		ak_ai_release_frame(ai_handle_id, &frame);
		return -1;
	}

	//printf("alsa_read:len=%d,ts=%d.\n",frame.len,frame.ts);
	
	memcpy( buf, frame.data, frame.len );

	ret = frame.len;
	
	ak_ai_release_frame(ai_handle_id, &frame);

	return ret;
}

void alsa_read_uninit( struct AlsaCtrl* obj )
{
	int ret;
	ret = ak_ai_stop_capture(ai_handle_id);
	if (ret) 
	{
		ak_print_error(MODULE_ID_AI, "*** ak_ai_stop_capture failed. ***\n");
	}

	ret = ak_ai_close(ai_handle_id);
	if (ret) 
	{
		ak_print_normal(MODULE_ID_AI, "*** ak_ai_close failed. ***\n");
	}
}
void set_mic_gain(int gain)
{
	ak_ai_set_gain(ai_handle_id, gain);
}
int get_mic_gain(void) 
{
	int gain_dat;
   	if(ak_ai_get_gain(ai_handle_id, &gain_dat)==0)
   	{
		return gain_dat;
	}
	return -999;
}
void set_mic_vol(int vol)
{
	ak_ai_set_volume(ai_handle_id, vol);
}
int get_mic_vol(void) 
{
	int vol_dat;
   	if(ak_ai_get_volume(ai_handle_id, &vol_dat)==0)
   	{
		return vol_dat;
	}
	return -999;
}

void set_spk_gain(int gain)
{
	ak_ao_set_gain(ao_handle_id, gain);
}
int get_spk_gain(void) 
{
	int gain_dat;
   	if(ak_ao_get_gain(ao_handle_id, &gain_dat)==0)
   	{
		return gain_dat;
	}
	return -999;
}
void set_spk_vol(int vol)
{
	ak_ao_set_volume(ao_handle_id, vol);
}
int get_spk_vol(void) 
{
	int vol_dat;
   	if(ak_ao_get_volume(ao_handle_id, &vol_dat)==0)
   	{
		return vol_dat;
	}
	return -999;
}

