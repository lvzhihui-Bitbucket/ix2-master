
#include "task_IoServer.h"
#include "elog.h"

void api_mic_volume_set( int vol )
{
	//vol = vol * 10;
	//alsa_card_set_level( vol, CARD_CAPTURE );
}

int api_mic_volume_get( void )
{
	//int vol;
	
	//vol = alsa_card_get_level( CARD_CAPTURE );

	//vol = vol / 10;
	
	return 0;
}

static void api_mic_power_on( void )
{
	//API_POWER_UDP_TALK_ON();			//czn_20170522
}

static void api_mic_power_off( void )
{
	//API_POWER_TALK_OFF();
}
#if	defined(PID_DX470)||defined(PID_DX482)
static int MIC_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};

static int MIC_GAIN_STEP_TAB[9]=
{
	//  0 -  8
	0,1,2,3,4,5,6,7,8
};

int LoadAudioCaptureVol( AU_IO_TYPE au_type )
{
	char temp[20];
	int vol;
	
	if( au_type == DS_DX_APP )
	{
		API_Event_IoServer_InnerRead_All(MicVolumeSet_APP, temp);
		printf("LoadAudioCaptureVol(DS_DX_APP) = %d\n", atoi(temp));
		log_d("LoadAudioCaptureVol(DS_DX_APP) = %d\n", atoi(temp));
	}
	else
	{
		API_Event_IoServer_InnerRead_All(MicVolumeSet, temp);
		printf("LoadAudioCaptureVol(DS_DX) = %d\n", atoi(temp));
		log_d("LoadAudioCaptureVol(DS_DX) = %d\n", atoi(temp));
	}
	return MIC_VOLUME_STEP_TAB[atoi(temp)];
}

int LoadAudioCaptureGain( AU_IO_TYPE au_type )
{
	char temp[20];

	if( au_type == DS_DX_APP )
	{
		API_Event_IoServer_InnerRead_All(MIC_VOLUME_GAIN_APP, temp);
		printf("LoadAudioCaptureGain(DS_DX_APP) = %d\n", atoi(temp));
		log_d("LoadAudioCaptureGain(DS_DX_APP) = %d\n", atoi(temp));
	}
	else
	{
		API_Event_IoServer_InnerRead_All(MIC_VOLUME_GAIN, temp);
		printf("LoadAudioCaptureGain(DS_DX) = %d\n", atoi(temp));
		log_d("LoadAudioCaptureGain(DS_DX) = %d\n", atoi(temp));
	}

	return MIC_GAIN_STEP_TAB[atoi(temp)];
}
#else
int MIC_STEP_TAB[41]=
{
	//0,-20,-15,-10,-5,1,5,10,15,20
	-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
	//0,-20,-16,-12,-8,-4,1,4,8,12
};
int LoadAudioCaptureVol( void )
{
	char temp[20];
	int vol;
	
	API_Event_IoServer_InnerRead_All(MicVolumeSet, temp);
	printf("LoadAudioCaptureVol = %d\n", atoi(temp));
	//vol = atoi(temp) > 9? 9 : atoi(temp);
	return MIC_STEP_TAB[atoi(temp)];

}
int LoadAudioCaptureGain( void )
{
	char temp[20];
	API_Event_IoServer_InnerRead_All(MIC_VOLUME_GAIN, temp);
	printf("LoadAudioCaptureGain = %d\n", atoi(temp));
	return atoi(temp);

}
#endif
void SetAudioCaptureMute( void )
{
}
