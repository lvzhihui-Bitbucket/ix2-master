
#ifndef _AU_UDP_H_
#define _AU_UDP_H_

#include "au_service.h"

/********************************************************************************
@function:
	au_udp_init:    audio udp transfer initial before starting
@parameters:
	s_ip:	        send ip address
	s_port:	        send ip port
	r_ip:	        recv ip address
	r_port:	        recv ip port
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_init( unsigned int s_ip, unsigned short s_port, unsigned int r_ip, unsigned short r_port,int mul_flag );

/********************************************************************************
@function:
	au_udp_start:   start audio udp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_start( void );

/********************************************************************************
@function:
	au_udp_stop:    stop audio udp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_stop( void );

/********************************************************************************
@function:
	au_udp_set_output:  set audio udp output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_set_output( cb_io output );

/********************************************************************************
@function:
	au_udp_get_input:   get audio udp input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_udp_get_input( void );


/********************************************************************************
@function:
	au_udp_pop: fetch udp received packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_udp_pop( unsigned char* pbuff, int limit );

#endif
