
#ifndef _AU_SERVICE_H_
#define _AU_SERVICE_H_

#include "rtp_utility.h"

typedef enum
{
    DS_DX,          // DT主机 - DX分机
    DS_DX_APP,      // DT主机 - DX分机 - APP
    IX_IX,          // IX主机/分机 - IX分机
    IX_APP,         // IX主机 - APP
    IX_IX2_APP,     // IX主机 - IX2分机(双网卡) - APP
}AU_IO_TYPE;

// udp
#define UDP_AUDIO_G711_DATA_LEN		64
// RTP
#define AU_RTP_PACK_SIZE           	160

typedef int (*cb_io)( const unsigned char* pbuff, int len );

/********************************************************************************
@function:
	set audio service transfer type
@parameters:
	type:	    type
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_type(AU_IO_TYPE type);

/********************************************************************************
@function:
	set audio service udp parameters
@parameters:
	src_ip:	    source ip address
	src_port:	source port
	tar_ip:	    target ip address
	tar_port:	target port
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_udp(int src_ip, short src_port, int tar_ip, short tar_port,int mul_flag);

/********************************************************************************
@function:
	set audio service rtp parameters
@parameters:
	ip:	        server ip address
	port:	    server port
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_set_rtp(char* ip, short port);

/********************************************************************************
@function:
	start audio service
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_start(AU_IO_TYPE type);

/********************************************************************************
@function:
	stop audio service
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_service_stop(void);

#endif
