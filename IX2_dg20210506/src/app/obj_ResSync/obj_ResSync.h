/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_ResSync_H
#define _obj_ResSync_H

#define RESID_8001							8001
#define RESID_8002_DSCallLimit				8002
#define RESID_8003_IMMonLimit				8003
#define RESID_8004_IMIPCList				8004
#define RESID_8005_DSBtnList				8005
#define RESID_8006_3rdSipList				8006
#define RESID_8007_DSVmList					8007
#define RESID_8008_IMNamelist				8008
#define RESID_8009_DSCardData				8009
#define RESID_8010_DSNamelist				8010
#define RESID_8011							    8011
#define RESID_8012_IMCallLimit			8012
#define RESID_8020_IoPara					  8020
#define RESID_8021_LanIPCList				8021
#define RESID_8022_WlanIPCList			8022
#define RESID_8023_APPIPCList			  8023

#define RESID_8801_InstallerBakDefault		8801
#define RESID_8802_InstallerBak1			8802
#define RESID_8803_InstallerBak2			8803
#define RESID_8804_EasyLog				8804

#define RES_DOWNLOAD_NO_SDCARD				"NO_SDCARD"
#define RES_DOWNLOAD_CHECK_OK				"CHECK_OK"
#define RES_DOWNLOAD_INSTALLING				"INSTALLING"
#define RES_DOWNLOAD_INSTALL_OK				"INSTALL_OK"
#define RES_DOWNLOAD_INSTALL_FAIL			"INSTALL_FAIL"
#define RES_DOWNLOAD_DOWNLOADING			"DOWNLOADING"
#define RES_DOWNLOAD_CONNECTING				"CONNECTING"
#define RES_DOWNLOAD_REBOOT					"REBOOT"
#define RES_DOWNLOAD_WAITING_REBOOT			"WAITING_REBOOT"
#define RES_DOWNLOAD_CANCEL					"CANCEL"

#define RES_DOWNLOAD_ERR_TYPE				"ERR_TYPE"
#define RES_DOWNLOAD_ERR_NO_FILE			"ERR_NO_FILE"
#define RES_DOWNLOAD_ERR_CODE				"ERR_CODE"
#define RES_DOWNLOAD_ERR_NO_SPASE			"ERR_NO_SPASE"
#define RES_DOWNLOAD_ERR_CONNECT			"ERR_CONNECT"
#define RES_DOWNLOAD_ERR_DOWNLOAD			"ERR_DOWNLOAD"


typedef struct
{
	int		res_id;
	char*	(*act_file_path)(void);
	int 	(*get_list_nums)(void);
	void 	(*res_reload)(void);
	void 	(*list_delete_all)(void);
}ResReloadTable_T;

extern const ResReloadTable_T ResReloadTable[];
extern const int ResReloadTable_num;

void RecvResSyncCmd_Process(int source_ip,int cmd,char *pbuff);

void SaveResFileFromActFile(int ResId,char *Rm_Addr,int level);
int Api_ResSyncToDevice(int ResId,char *Device_Addr);
int Api_ResSyncFromServer(int ResId,char *Device_Addr);


#endif


