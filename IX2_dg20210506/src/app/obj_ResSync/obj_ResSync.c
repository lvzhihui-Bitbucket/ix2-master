/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
//#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_GetIpByNumber.h"
#include "obj_ResSync.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "obj_TableProcess.h"
#include "task_IxProxy.h"
#include "obj_InstallerBackup.h"
#include "elog_file_cfg.h"
#include "define_file.h"

typedef struct
{
	int				WaitFlag;			//等待回应标记
	int				rand;				//随机数
	int				res_id;
	char				dev_Addr[11];
	//int				ip_addr;
	int				result;
	int				trans_result;
	sem_t 			sem;
	char			tarFileDir[100];
	char			tarFileName[40];
	char			srcFileDir[100];
	char			srcFileName[40];
}SyncResWaitRsp_T;

#define	Max_SyncResWait			5
#define	Max_SyncResWaitTime		500
#define	Max_SyncResWaitTransTime	5000


static SyncResWaitRsp_T SyncResWaitRsp[Max_SyncResWait]={0};

typedef struct
{
	int		rand;				//随机数
	int		res_id;
	char	server_addr[11];
	char	cleint_addr[11];
	int		result;
	int		trans_result;
	char	tarFileDir[100];
	char	tarFileName[40];
	char	srcFileDir[100];
	char	srcFileName[40];
}SyncResMsg_T;


extern void Ipc_Reload(void);
extern void WlanIpc_Reload(void);
extern void GetAppIpcListFromFile(void);
extern void R8001_Reload(void);
extern void R8001_format(void);
extern int get_r8001_record_cnt(void);
extern void MonRes_ReLoad(void);
extern void ClearMonRes_Table(void);
extern int Get_MonRes_Num(void);
extern void ImNameList_Reload(void);
extern void ClearImNamelist_Table(void);
extern int GetImNameListRecordCnt(void);
void ExtModuleKeyTableInit(void);
int DeleteExtModuleKeyTable(void);
int GetExtModuleKeyNum(void);

char* GetR8001FileName(void)
{
	return R8001_TABLE_NAME;
}
char* GetR8003FileName(void)
{
	return MON_LIST_FILE_NAME;
}
char* GetR8021FileName(void)
{
	return IPC_SETUP_FILE_NAME;
}
char* GetR8005FileName(void)
{
	return EXT_MODULE_KEY_TABLE_FILE_NAME;
}
char* GetR8008FileName(void)
{
	return IM_NAME_LIST_TABLE_NAME;
}

char* GetR8020FileName(void)
{
	return IO_DATA_DEFAULT_FILE_NAME;
}
char* GetR8022FileName(void)
{
	return IPC_WLAN_SETUP_FILE_NAME;
}

char* GetR8023FileName(void)
{
	return APP_IPC_LIST_FILE_NAME;
}

char* GetR8801FileName(void)
{
	return GetBakFileByNumber(0);
}
char* GetR8802FileName(void)
{
	return GetBakFileByNumber(1);
}

char* GetR8803FileName(void)
{
	return GetBakFileByNumber(2);
}

char *GetR8804FileName(void)
{
	return ELOG_FILE_NAME;
}

const ResReloadTable_T ResReloadTable[] = 
{
	{RESID_8001,						GetR8001FileName,	get_r8001_record_cnt,	R8001_Reload,		R8001_format},	
	{RESID_8003_IMMonLimit,				GetR8003FileName,	Get_MonRes_Num,			MonRes_ReLoad,		ClearMonRes_Table},	
	//{RESID_8004_IMIPCList,				GetR8004FileName,	NULL,					IPC_MonRes_Reload,	NULL},	
	{RESID_8005_DSBtnList,				GetR8005FileName,   GetExtModuleKeyNum,ExtModuleKeyTableInit,DeleteExtModuleKeyTable},
	{RESID_8008_IMNamelist,				GetR8008FileName,	GetImNameListRecordCnt,	ImNameList_Reload,	ClearImNamelist_Table},	
	{RESID_8020_IoPara,					GetR8020FileName,	NULL,					NULL,								NULL},
	{RESID_8021_LanIPCList,				GetR8021FileName,	NULL,					Ipc_Reload,							NULL},	
	{RESID_8022_WlanIPCList,			GetR8022FileName,	NULL,					WlanIpc_Reload,						NULL},	
	{RESID_8023_APPIPCList,				GetR8023FileName,	NULL,					GetAppIpcListFromFile,				NULL},	

	{RESID_8801_InstallerBakDefault,	GetR8801FileName,	NULL,					NULL,				NULL},
	{RESID_8802_InstallerBak1,			GetR8802FileName,	NULL,					NULL,				NULL},
	{RESID_8803_InstallerBak2,			GetR8803FileName,	NULL,					NULL,				NULL},
	{RESID_8804_EasyLog,				GetR8804FileName,	NULL,					NULL,				NULL},
};

const int ResReloadTable_num = sizeof(ResReloadTable)/sizeof(ResReloadTable[0]);


int GetLocalResById(int ResId,char *Rm_Addr,char *res_file);

void CheckResFileDir(void)
{
	MakeDir(ResFileDir);
}
void CheckPublicResFileDir(void)
{
	DeleteFileProcess(PublicResFileDir, NULL);
	MakeDir(PublicResFileDir);
}

char *GetResPathById(int res_id)
{
	int i;
	
	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == res_id)
		{
			return ResReloadTable[i].act_file_path();
		}
	}

	return NULL;
}
void ResReLoad(int res_id)
{
	int i;
	
	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == res_id)
		{
			if(ResReloadTable[i].res_reload!= NULL)
				ResReloadTable[i].res_reload();
			break;
		}
	}
}

void UpdateResFileToActFile(int ResId,char *Rm_Addr)
{
	char res_file[40];
	char res_path[80];
	char cmd_buff[200];
	char res_md5[16];
	char act_md5[16];
	int i;
	FILE *pf;
	int res_len,act_len;

	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == ResId&&ResReloadTable[i].act_file_path!=NULL)
			break;
	}

	if(i >= ResReloadTable_num)
	{
		return;
	}

	if(GetLocalResById(ResId,Rm_Addr,res_file) != 0)
	{
		return;
	}
	
	pf = fopen(res_path,"r");
	if(pf == NULL)
		return;
	
	fseek(pf,0,SEEK_END);
	res_len = ftell(pf);
	fclose(pf);
	
	pf = fopen(ResReloadTable[i].act_file_path(),"r");
	if(pf == NULL)
		goto Update_Act;
	
	fseek(pf,0,SEEK_END);
	act_len = ftell(pf);
	fclose(pf);

	if(res_len != act_len)
		goto Update_Act;
	
	sprintf(res_path,"%s/%s",ResFileDir,res_file);
	FileMd5_Calculate(res_path,res_md5);
	FileMd5_Calculate(ResReloadTable[i].act_file_path(),act_md5);

	if(memcmp(res_md5,act_md5,16) == 0)
		return;
Update_Act:
	sprintf(cmd_buff,"cp %s %s",res_path,ResReloadTable[i].act_file_path());

	system(cmd_buff);
	sync();
}
void SaveResFileFromActFile(int ResId,char *Rm_Addr,int level)
{
	char res_file[40];
	//char res_path[80];
	char rm_temp[11] = {0};
	char cmd_buff[200];
	//char res_md5[16];
	//char act_md5[16];
	int i;
	
	CheckResFileDir();
	
	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == ResId&&ResReloadTable[i].act_file_path!=NULL)
			break;
	}

	if(i >= ResReloadTable_num)
	{
		return;
	}

	if(level == 2)
	{
		memcpy(rm_temp,Rm_Addr,10);
		rm_temp[10] = 0;
	}
	else if(level == 1)
	{
		memcpy(rm_temp,Rm_Addr,8);
		rm_temp[8] = 0;
	}
	else
	{
		memcpy(rm_temp,Rm_Addr,4);
		rm_temp[4] = 0;
	}
	
	//sprintf(cmd_buff,"%s/R%04d_%s.csv",ResFileDir,ResId,rm_temp);
	//FileMd5_Calculate(cmd_buff,res_md5);
	//FileMd5_Calculate(ResReloadTable[i].act_file_path,act_md5);

	//if(memcmp(res_md5,act_md5) == 0)
	//	return;
	
	sprintf(cmd_buff,"cp %s %s/R%04d.%s.csv",ResReloadTable[i].act_file_path(),ResFileDir,ResId,rm_temp);
	system(cmd_buff);
	sync();
	
}

int GetLocalResById(int ResId,char *Rm_Addr,char *res_file)
{
	char res_file_temp[40];
	char res_path[80];
	char rm_temp[11];
	FILE *pf;
	//dh_20190705_s
	struct dirent *entry;
    	DIR *dir;

	if(res_file == NULL)
	{
		return -1;
	}

	res_file[0] = 0;

	dir = opendir(ResFileDir);
	while ((entry=readdir(dir))!=NULL)	
	{
		if(ResId == 8000)
		{
			if(strstr(entry->d_name,"R8000") != NULL)
			{
				sprintf(res_file,"%s",entry->d_name);
				closedir(dir);
				return 0;
			}
		}
		else
		{
			sprintf(res_file_temp,"R%04d.%s",ResId,Rm_Addr);
			if(strstr(entry->d_name,res_file_temp) != NULL)
			{
				sprintf(res_file,"%s",entry->d_name);
				closedir(dir);
				return 0;
			}
			memcpy(rm_temp,Rm_Addr,8);
			rm_temp[8] = 0;
			sprintf(res_file_temp,"R%04d.%s",ResId,rm_temp);
			if(strstr(entry->d_name,res_file_temp) != NULL)
			{
				sprintf(res_file,"%s",entry->d_name);
				closedir(dir);
				return 0;
			}
			memcpy(rm_temp,Rm_Addr,4);
			rm_temp[4] = 0;
			sprintf(res_file_temp,"R%04d.%s",ResId,rm_temp);
			if(strstr(entry->d_name,res_file_temp) != NULL)
			{
				sprintf(res_file,"%s",entry->d_name);
				closedir(dir);
				return 0;
			}
			sprintf(res_file_temp,"R%04d.9999",ResId);
			if(strstr(entry->d_name,res_file_temp) != NULL)
			{
				sprintf(res_file,"%s",entry->d_name);
				closedir(dir);
				return 0;
			}
		}	
	}	
	closedir(dir);
	//dh_20190705_e
	return -1;
}
//dh_20190705_s

void ResReloadByLevel(int ResId, int level)
{
	char rm_temp[11] = {0};
	char cmd_buff[200],fileName[200];
	struct dirent *entry;
    	DIR *dir;
	if(level == 0)	//9999
	{
		sprintf(fileName,"R%4d.9999",ResId);
	}
	else if(level == 1)// 4 bd
	{
		memcpy(rm_temp,GetSysVerInfo_BdRmMs(),4);
		rm_temp[4] = 0;
		sprintf(fileName,"R%4d.%s",ResId,rm_temp);
	}
	else if(level == 2)	//8 rm
	{
		memcpy(rm_temp,GetSysVerInfo_BdRmMs(),8);
		rm_temp[8] = 0;
		sprintf(fileName,"R%4d.%s",ResId,rm_temp);
	}
	else
	{
		sprintf(fileName,"R%4d.%s",ResId,GetSysVerInfo_BdRmMs());
	}
    	dir = opendir(PublicResFileDir);
	while ((entry=readdir(dir))!=NULL)
	{
		if(strstr(entry->d_name,fileName) != NULL)
		{
			sprintf(fileName,"%s%s",PublicResFileDir,entry->d_name);
			sprintf(cmd_buff,"cp %s %s",fileName,GetResPathById(ResId));
			system(cmd_buff);
			ResReLoad(ResId);
			break;
		}
	}
	closedir(dir);
}
void PubResReloadToActFile(void)
{
	int i;
	char temp1[20];
	char temp2[20];
	API_Event_IoServer_InnerRead_All(OverUnitIntercom,temp1);//IM跨单元的Intercom使能
	API_Event_IoServer_InnerRead_All(OverUnitMonitor,temp2);//允许 全范围监视

	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == 8008 && !atoi(temp1))
		{
			ResReloadByLevel(8008, 1);
		}
		else if(ResReloadTable[i].res_id == 8003 && !atoi(temp2))
		{
			ResReloadByLevel(8003, 1);
		}
		else
		{
			ResReloadByLevel(ResReloadTable[i].res_id, 0);
		}
	}
}

void ResUnzipToActFile(void)
{
	char cmd_buff[200],fileName[200];
	char dir1[200],dir2[200];
	struct dirent *entry;
    
	DIR *dir;
    dir = opendir(ResFileDir);
	while ((entry=readdir(dir))!=NULL)
	{
		if(strstr(entry->d_name,".zip") != NULL)
		{
			sprintf(fileName,"%s/%s",ResFileDir,entry->d_name);
			sprintf(cmd_buff,"unzip -o %s -d %s",fileName,PublicResFileDir);
			system(cmd_buff);
			sync();
			break;
		}
		else if(strstr(entry->d_name,".tar") != NULL)
		{
			sprintf(fileName,"%s/%s",ResFileDir,entry->d_name);

			strcpy(dir1, ResFileDir);
			while(dir1[strlen(dir1)] == '/')
			{
				dir1[strlen(dir1)] = 0;
			}
			strcpy(dir2, dir1);

			sprintf(cmd_buff,"tar -xvf %s -C %s %s", fileName, dirname(dir1), basename(dir2));

			dprintf("%s\n", cmd_buff);

			system(cmd_buff);
			sync();
			break;
		}
	}
	closedir(dir);
	
	PubResReloadToActFile();
}
void CheckResIdFile(int id)
{
	char cmd_buff[200],fileName[200];
	struct dirent *entry;
	DIR *dir;

	dir = opendir(ResFileDir);
	if(dir == NULL)
	{
		return;
	}

	while ((entry=readdir(dir))!=NULL)
	{
		sprintf(fileName,"R%4d",id);
		if(strstr(entry->d_name,fileName) != NULL)
		{
			sprintf(fileName,"%s%s",ResFileDir,entry->d_name);
			break;
		}
	}
	closedir(dir);
	if(entry == NULL)
	{
		return;
	}
	snprintf(cmd_buff,200, "rm %s", fileName);
	system(cmd_buff);
	sync();
}
void ResClearId(int id)
{
	int i;
	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(id == 8000)
		{
			if(ResReloadTable[i].list_delete_all != NULL)
				ResReloadTable[i].list_delete_all();
		}
		else
		{
			if(ResReloadTable[i].res_id == id)
			{
				if(ResReloadTable[i].list_delete_all != NULL)
				{
					ResReloadTable[i].list_delete_all();
				}
				break;
			}
		}
	}
}
//dh_20190705_e

int ResReloadById(int ResId)
{
	char res_file[40];
	//char res_path[80];
	char cmd_buff[200];
	int i;
	
	for(i = 0;i < ResReloadTable_num;i++)
	{
		if(ResReloadTable[i].res_id == ResId)
			break;
	}

	if(i >= ResReloadTable_num)
		return -1;
	
	if(GetLocalResById(ResId,GetSysVerInfo_BdRmMs(),res_file) != 0)
	{
		return -2;
	}
	
	if(ResReloadTable[i].act_file_path != NULL)
	{
		sprintf(cmd_buff,"cp %s/%s %s",ResFileDir,res_file,ResReloadTable[i].act_file_path());

		system(cmd_buff);
		sync();
	}
	
	if(ResReloadTable[i].res_reload != NULL)
		ResReloadTable[i].res_reload();

	return 0;
	
}

int TransResToDevice(char *res_file,int device_ip)
{
	if(api_server_tftp_trans(device_ip,ResFileDir,res_file,ResFileDir,res_file) == 0)
	{
		return 0;
	}
	if(api_server_tftp_trans(device_ip,ResFileDir,res_file,IX1_ResFileDir,res_file) == 0)
	{
		return 0;
	}
	return -1;
}

int TransResToDeviceDir(int device_ip, char *resDir, char *resFile, char *tarDir, char* tarFile)
{
	if(api_server_tftp_trans(device_ip, resDir, resFile, tarDir, tarFile) == 0)
	{
		return 0;
	}
	
	return -1;
}

int TransResToDeviceAppointDir(char *src_dir,char *tar_dir,char *res_file,int device_ip)
{
	if(api_server_tftp_trans(device_ip,src_dir,res_file,tar_dir,res_file) == 0)
	{
		return 0;
	}
	
	return -1;
}

int WaitResTransFinished(int ResId,SyncResWaitRsp_T *pSyncResWaitRsp)
{
	if(pSyncResWaitRsp->WaitFlag ==0)
	{
		return -1;
	}
	
	if(pSyncResWaitRsp->trans_result == 1)
	{
		pSyncResWaitRsp->WaitFlag = 0;
		return 0;
	}
	
	sem_wait_ex2(&pSyncResWaitRsp->sem,Max_SyncResWaitTransTime);
	
	if(pSyncResWaitRsp->trans_result != 0)
	{
		if(pSyncResWaitRsp->trans_result == 1)
		{
			pSyncResWaitRsp->WaitFlag = 0;
			return 0;
		}
		else
		{
			pSyncResWaitRsp->WaitFlag = 0;
			return -1;
		}
	}
	pSyncResWaitRsp->WaitFlag = 0;
	return -1;
}


int SendTransResToDeviceReq(int target_ip,char *server_addr, char* cleint_addr, int ResId, char* srcDir, char* srcFile,  char* tarDir, char* tarFile, SyncResWaitRsp_T **pSyncResWaitRsp)
{
	SyncResMsg_T sendData;
	
	int i;

	for(i = 0;i < Max_SyncResWait&&SyncResWaitRsp[i].WaitFlag == 1;i++);

	if(i>= Max_SyncResWait)
		return -1;

	memset(&sendData, 0, sizeof(sendData));
	
	*pSyncResWaitRsp = &SyncResWaitRsp[i];

	sendData.rand = MyRand();
	sendData.res_id = ResId;
	
	SyncResWaitRsp[i].rand = sendData.rand;
	SyncResWaitRsp[i].res_id = ResId;
	SyncResWaitRsp[i].result = 0;
	
	if(server_addr != NULL)
	{
		strcpy(sendData.server_addr, server_addr);
	}

	if(cleint_addr != NULL)
	{
		strcpy(sendData.cleint_addr, cleint_addr);
	}
	if(srcDir != NULL)
	{
		strcpy(sendData.srcFileDir, srcDir);
	}
	if(srcFile != NULL)
	{
		strcpy(sendData.srcFileName, srcFile);
	}

	if(tarDir != NULL)
	{
		strcpy(sendData.tarFileDir, tarDir);
	}

	if(tarFile != NULL)
	{
		strcpy(sendData.tarFileName, tarFile);
	}
		
	if(cleint_addr != NULL)
	{
		strcpy(SyncResWaitRsp[i].dev_Addr,cleint_addr);
	}
	sem_init(&SyncResWaitRsp[i].sem,0,0); 
	SyncResWaitRsp[i].WaitFlag = 1;
	
	api_udp_device_update_send_data3(target_ip, htons(CMD_RES_UPLOAD_REQ), (char*)&sendData, sizeof(sendData) );

	sem_wait_ex2(&SyncResWaitRsp[i].sem,Max_SyncResWaitTime);
	
	if(SyncResWaitRsp[i].result != 0)
	{
		if(SyncResWaitRsp[i].result == 1)
		{
			return 0 ;
		}
		else
		{
			SyncResWaitRsp[i].WaitFlag = 0;
			return -1;
		}
	}
	
	SyncResWaitRsp[i].WaitFlag = 0;
	return -1;
}

int SendTransResToDeviceRsp(int target_ip,int rand,int ResId,int result)
{
	SyncResMsg_T sendData = {0};
	
	sendData.rand = rand;
	sendData.res_id = ResId;
	sendData.result = result;
	strcpy(sendData.cleint_addr,GetSysVerInfo_BdRmMs());
	
	api_udp_device_update_send_data3(target_ip, htons(CMD_RES_UPLOAD_RSP), (char*)&sendData, sizeof(sendData) );

	return 0;
}

int SendTransResToDeviceRsp2(int target_ip,int rand,int ResId,int result, char* dir, char* file)
{
	SyncResMsg_T sendData = {0};
	
	sendData.rand = rand;
	sendData.res_id = ResId;
	sendData.result = result;
	strcpy(sendData.cleint_addr,GetSysVerInfo_BdRmMs());
	strcpy(sendData.tarFileDir, dir);
	strcpy(sendData.tarFileName, file);
	
	api_udp_device_update_send_data3(target_ip, htons(CMD_RES_UPLOAD_RSP), (char*)&sendData, sizeof(sendData) );

	return 0;
}

void RecvTransResToDeviceRsp(SyncResMsg_T* pData)
{
	int i;
	for(i = 0;i < Max_SyncResWait;i++)
	{
		if(SyncResWaitRsp[i].WaitFlag)
		{
			if(SyncResWaitRsp[i].res_id == pData->res_id && SyncResWaitRsp[i].rand == pData->rand)
			{
				SyncResWaitRsp[i].result = pData->result;
				strcpy(SyncResWaitRsp[i].srcFileDir, pData->srcFileDir);
				strcpy(SyncResWaitRsp[i].tarFileDir, pData->tarFileDir);
				strcpy(SyncResWaitRsp[i].srcFileName, pData->srcFileName);
				strcpy(SyncResWaitRsp[i].tarFileName, pData->tarFileName);
				sem_post(&SyncResWaitRsp[i].sem);
				break;
			}
		}
	}
}

int SendResTransResult(int target_ip,int rand,int ResId,int result)
{
	SyncResMsg_T sendData = {0};
	
	sendData.rand = rand;
	sendData.res_id = ResId;
	sendData.result = result;
	strcpy(sendData.server_addr,GetSysVerInfo_BdRmMs());
	
	api_udp_device_update_send_data3(target_ip, htons(CMD_RES_TRANS_RESULT), (char*)&sendData, sizeof(sendData) );
}

void RecvResTransResult(char *Device_Addr,int rand,int ResId,int result)
{
	int i;
	for(i = 0;i < Max_SyncResWait;i++)
	{
		//printf("!!!!!!!!!!!!RecvResTransResult:i=%d,rand=%d:%d,rid=%d:%d,wf=%d,result=%d\n",i,rand,SyncResWaitRsp[i].rand,ResId,SyncResWaitRsp[i].res_id,SyncResWaitRsp[i].WaitFlag,result);
		if(SyncResWaitRsp[i].WaitFlag)
		{
			if(SyncResWaitRsp[i].res_id == ResId&&SyncResWaitRsp[i].rand == rand)
			{
				SyncResWaitRsp[i].trans_result = result;
				sem_post(&SyncResWaitRsp[i].sem);
				break;
			}
		}
	}
}




int JustIfRecvResTrans(int ResId,char *Source_Addr)
{
	return 1;
}

int SendSyncResFromServerReq(int target_ip, char *server_addr, char* cleint_addr, int ResId, char* srcDir, char* srcFile, char* tarDir, char* tarFile, SyncResWaitRsp_T **pSyncResWaitRsp)
{
	SyncResMsg_T sendData = {0};
	
	int i;

	for(i = 0;i < Max_SyncResWait&&SyncResWaitRsp[i].WaitFlag == 1;i++);

	if(i>= Max_SyncResWait)
		return -1;

	*pSyncResWaitRsp = &SyncResWaitRsp[i];

	sendData.rand = MyRand();
	sendData.res_id = ResId;
	if(server_addr != NULL)
	{
		strcpy(sendData.server_addr, server_addr);
	}

	if(cleint_addr != NULL)
	{
		strcpy(sendData.cleint_addr, cleint_addr);
	}

	if(srcDir != NULL)
	{
		snprintf(sendData.srcFileDir, 100, "%s", srcDir);
	}
	
	if(srcFile != NULL)
	{
		snprintf(sendData.srcFileName, 40, "%s", srcFile);
	}
	
	if(tarDir != NULL)
	{
		snprintf(sendData.tarFileDir, 100, "%s", tarDir);
	}
	
	if(tarFile != NULL)
	{
		snprintf(sendData.tarFileName, 40, "%s", tarFile);
	}
	
	SyncResWaitRsp[i].rand = sendData.rand;
	SyncResWaitRsp[i].res_id = ResId;
	SyncResWaitRsp[i].result = 0;
	SyncResWaitRsp[i].trans_result = 0;
	if(server_addr != NULL)
	{
		strcpy(SyncResWaitRsp[i].dev_Addr,server_addr);
	}
	sem_init(&SyncResWaitRsp[i].sem,0,0); 
	SyncResWaitRsp[i].WaitFlag = 1;
	
	api_udp_device_update_send_data3(target_ip, htons(CMD_RES_DOWNLOAD_REQ), (char*)&sendData, sizeof(sendData) );

	sem_wait_ex2(&SyncResWaitRsp[i].sem,Max_SyncResWaitTime);
	
	
	if(SyncResWaitRsp[i].result != 0)
	{
		if(SyncResWaitRsp[i].result == 1)
			return 0 ;
		else
		{
			SyncResWaitRsp[i].WaitFlag = 0;
			return -1;
		}
	}
	
	if(SyncResWaitRsp[i].trans_result != 0)
	{
		if(SyncResWaitRsp[i].trans_result == 1)
			return 0;
		else
		{
			SyncResWaitRsp[i].WaitFlag = 0;
			return -1;
		}
	}
	
	SyncResWaitRsp[i].WaitFlag = 0;
	return -1;
}

int SendSyncResFromServerRsp(int source_ip,int rand,int ResId,int result)
{
	SyncResMsg_T sendData = {0};
	
	sendData.rand = rand;
	sendData.res_id = ResId;
	sendData.result = result;
	strcpy(sendData.server_addr,GetSysVerInfo_BdRmMs());
	
	api_udp_device_update_send_data3(source_ip, htons(CMD_RES_DOWNLOAD_RSP), (char*)&sendData, sizeof(sendData) );
}

void RecvSyncResFromServerRsp(char *Device_Addr,int rand,int ResId,int result)
{
	int i;
	for(i = 0;i < Max_SyncResWait;i++)
	{
		if(SyncResWaitRsp[i].WaitFlag)
		{
			if(SyncResWaitRsp[i].res_id == ResId&&SyncResWaitRsp[i].rand == rand)
			{
				SyncResWaitRsp[i].result = result;
				sem_post(&SyncResWaitRsp[i].sem);
				break;
			}
		}
	}
}




//return 0,ok,-1,本地资源不存在-2,目标不接受3,res传输失败
int Api_ResSyncToDevice(int ResId,char *Device_Addr)
{
	char res_file[40];
	SyncResWaitRsp_T *pSyncResWaitRsp;
	GetIpRspData data;
	CheckResFileDir();
	if(GetLocalResById(ResId,GetSysVerInfo_BdRmMs(),res_file) != 0)
	{
		return -1;
	}
	
	if(API_GetIpNumberFromNet(Device_Addr, NULL, NULL, 2, 1, &data) != 0)
		return -2;
	
	if(SendTransResToDeviceReq(data.Ip[0],NULL, Device_Addr, ResId, NULL, NULL, NULL, NULL, &pSyncResWaitRsp) != 0)
	{
		return -2;
	}

	if(TransResToDevice(res_file,data.Ip[0]) != 0)
	{
		SendResTransResult(data.Ip[0],pSyncResWaitRsp->rand,ResId,-1);
		pSyncResWaitRsp->WaitFlag = 0;
		return -3;
	}

	SendResTransResult(data.Ip[0],pSyncResWaitRsp->rand,ResId,1);
	pSyncResWaitRsp->WaitFlag = 0;
	return 0;
}

int Api_ResSyncToDeviceAppointDirFile(int ResId,char *Device_Addr,int target_ip,char *Dir,char *res_file)
{
	//char res_file[40];
	SyncResWaitRsp_T *pSyncResWaitRsp;
	//GetIpRspData data;
	//CheckResFileDir();
	#if 0
	if(GetLocalResById(ResId,GetSysVerInfo_BdRmMs(),res_file) != 0)
	{
		if(memcmp(GetSysVerInfo_bd(),"0000",4) == 0)
		{
			if(GetLocalResById(ResId,Device_Addr,res_file) != 0)
			{
				return -1;
			}
		}
		else
			return -1;
	}
	#endif
	//if(API_GetIpNumberFromNet(Device_Addr, NULL, NULL, 2, 1, &data) != 0)
	//	return -4;
	
	if(SendTransResToDeviceReq(target_ip,NULL, Device_Addr,ResId,NULL, NULL, NULL, NULL, &pSyncResWaitRsp) != 0)
	{
		return -2;
	}

	if(TransResToDeviceAppointDir(Dir,ResFileDir,res_file,target_ip) != 0)
	{
		if(TransResToDeviceAppointDir(Dir,IX1_ResFileDir,res_file,target_ip) != 0)
		{
			SendResTransResult(target_ip,pSyncResWaitRsp->rand,ResId,-1);
			pSyncResWaitRsp->WaitFlag = 0;
			return -3;
		}
	}

	SendResTransResult(target_ip,pSyncResWaitRsp->rand,ResId,1);
	pSyncResWaitRsp->WaitFlag = 0;
	return 0;
}

void R8100_Reload(void)
{
	char cmd_buff[200],fileName[200];
	struct dirent *entry;
	DIR *dir;
	dir = opendir(ResFileDir);
	while ((entry=readdir(dir))!=NULL)
	{
		printf("R8100_Reload22222222%s\n",entry->d_name);
		if(strstr(entry->d_name,"R8100") != NULL)
		{
			sprintf(fileName,"%s%s",ResFileDir,entry->d_name);
			if(strstr(fileName, ".zip") != NULL)
			{
				DelCustomerFile();
				snprintf(cmd_buff, 200, "unzip -o %s -d /mnt/nand1-2/", fileName);
			}
			else if(strstr(fileName, ".tar.gz") != NULL)
			{
				DelCustomerFile();
				snprintf(cmd_buff, 200, "tar -zxvf %s -C /mnt/nand1-2/ Customerized/", fileName);
			}
			system(cmd_buff);
			snprintf(cmd_buff,200,"rm %d",fileName);
			system(cmd_buff);
			ReloadCustomerFile();
			sync();
			break;
		}
	}
	closedir(dir);
	
}
//teturn 0,trans ok, -1,reject,-2 trans err
int RecvTransResToDeviceReq(int source_ip, SyncResMsg_T* pSyncResMsg)
{
	int i;
	int installResult;
	if(!strcmp(pSyncResMsg->server_addr, IxDeviceFlag))
	{
		int rebootFlag;
	
		for(i = 0;i < Max_SyncResWait && SyncResWaitRsp[i].WaitFlag== 1; i++);
		
		if(i>= Max_SyncResWait)
		{
			SendTransResToDeviceRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			return -1;
		}
		//*pSyncResWaitRsp = &SyncResWaitRsp[i];
		SyncResWaitRsp[i].rand = pSyncResMsg->rand;
		SyncResWaitRsp[i].res_id = pSyncResMsg->res_id;
		SyncResWaitRsp[i].result = 0;
		SyncResWaitRsp[i].trans_result = 0;
		strcpy(SyncResWaitRsp[i].dev_Addr,pSyncResMsg->server_addr);
		
		sem_init(&SyncResWaitRsp[i].sem,0,0); 
		SyncResWaitRsp[i].WaitFlag = 1;
	
		MakeDir(pSyncResMsg->tarFileDir);
		CheckResIdFile(pSyncResMsg->res_id);//dh_20190705

		SendTransResToDeviceRsp2(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,1, pSyncResMsg->tarFileDir, pSyncResMsg->tarFileName);
		if(WaitResTransFinished(pSyncResMsg->res_id,&SyncResWaitRsp[i])!=0)
			return -2;
		if(pSyncResMsg->res_id != FILE_MANAGE_RID)
		{
			IxDeviceFileDownloadReport(source_ip, RES_DOWNLOAD_INSTALLING, NULL, pSyncResMsg->res_id);
			installResult = ResUpdateProcess(pSyncResMsg->res_id, pSyncResMsg->tarFileDir, pSyncResMsg->tarFileName, &rebootFlag);
			
			if(installResult >= 0)
			{
				IxDeviceFileDownloadReport(source_ip, RES_DOWNLOAD_INSTALL_OK, NULL, pSyncResMsg->res_id);
				if(rebootFlag)
				{
					WriteRebootFlag(source_ip, 1);
					SoftRestar();
				}
			}
			else
			{
				IxDeviceFileDownloadReport(source_ip, RES_DOWNLOAD_INSTALL_FAIL, NULL, pSyncResMsg->res_id);
			}
		}
	}
	else
	{
		CheckResFileDir();
		if(JustIfRecvResTrans(pSyncResMsg->res_id, pSyncResMsg->server_addr) == 0)
		{
			SendTransResToDeviceRsp(source_ip, pSyncResMsg->rand, pSyncResMsg->res_id,-1);
			return -1;
		}
		for(i = 0;i < Max_SyncResWait&&SyncResWaitRsp[i].WaitFlag== 1;i++);
		
		if(i>= Max_SyncResWait)
		{
			SendTransResToDeviceRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			return -1;
		}
		//*pSyncResWaitRsp = &SyncResWaitRsp[i];
		SyncResWaitRsp[i].rand = pSyncResMsg->rand;
		SyncResWaitRsp[i].res_id = pSyncResMsg->res_id;
		SyncResWaitRsp[i].result = 0;
		SyncResWaitRsp[i].trans_result = 0;
		strcpy(SyncResWaitRsp[i].dev_Addr,pSyncResMsg->server_addr);
		sem_init(&SyncResWaitRsp[i].sem,0,0); 
		SyncResWaitRsp[i].WaitFlag = 1;
		
		CheckResIdFile(pSyncResMsg->res_id);//dh_20190705
		SendTransResToDeviceRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,1);
		//printf("RecvTransResToDeviceReq !!!!!!!!!!!!i=%d\n",i);
		if(WaitResTransFinished(pSyncResMsg->res_id,&SyncResWaitRsp[i])!=0)
			return -2;
		//printf("RecvTransResToDeviceReq ????????????\n");
		
		//dh_20190705
		if(pSyncResMsg->res_id == 8000)
		{
			CheckPublicResFileDir();
			ResUnzipToActFile();
		}
		else if(pSyncResMsg->res_id == 8100)
		{
			R8100_Reload();
		}
		else
		{
			ResReloadById(pSyncResMsg->res_id);
		
		}
	}

	return 0;
}

//return 0,ok,-1,ser不接受-2,trans err
int Api_ResSyncFromServer(int ResId,char *Device_Addr)
{
	SyncResWaitRsp_T *pSyncResWaitRsp;
	GetIpRspData data;
	
	CheckResFileDir();
	
	if(API_GetIpNumberFromNet(Device_Addr, NULL, NULL, 2, 1, &data) != 0)
		return -3;
	
	CheckResIdFile(ResId);//dh_20190705
	if(SendSyncResFromServerReq(data.Ip[0],Device_Addr, GetSysVerInfo_BdRmMs(), ResId, NULL, NULL, NULL, NULL, &pSyncResWaitRsp) != 0)
	{
		dprintf("!!!!!!!Api_ResSyncFromServer fail -1\n");
		return -1;
	}

	if(WaitResTransFinished(ResId,pSyncResWaitRsp)!=0)
	{
		dprintf("!!!!!!!Api_ResSyncFromServer fail -2\n");
		return -2;
	}	

	//dh_20190705
	if(ResId == 8000)
	{
		CheckPublicResFileDir();
		ResUnzipToActFile();
	}
	else
	{
		ResReloadById(ResId);
	}
	
	return 0;
}

//return 0,trans ok,-1 no res,-2 trans err
int RecvSyncResFromServerReq(int source_ip, SyncResMsg_T* pSyncResMsg)
{
	int ret, i;
	char res_dir[100] = {0};
	char res_file[40] = {0};
	char path[150] = {0};
	char* pResDirAndFile;

	//IX_Device读取文件
	if(!strcmp(pSyncResMsg->cleint_addr, IxDeviceFlag))
	{
		pResDirAndFile = GetResPathById(pSyncResMsg->res_id);
		if(pResDirAndFile == NULL)
		{
			snprintf(res_dir, 100, "%s", pSyncResMsg->srcFileDir);
			snprintf(res_file, 40, "%s", pSyncResMsg->srcFileName);
		}
		else
		{
			snprintf(path, 150, "%s", pResDirAndFile);
			snprintf(res_file, 40, "%s", basename(path));
			snprintf(path, 150, "%s", pResDirAndFile);
			snprintf(res_dir, 100, "%s", dirname(path));
		}

		snprintf(path, 150, "%s/%s", res_dir, res_file);
		dprintf("path=%s\n", path);

		if(!IsFileExist(path))
		{
			SendSyncResFromServerRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			dprintf("File not exist!!! path=%s\n", path);
			return -1;
		}

		SendSyncResFromServerRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id, 1);
		ret = TransResToDeviceDir(source_ip, res_dir, res_file, pSyncResMsg->tarFileDir, pSyncResMsg->tarFileName);
		
		if(ret ==0)
		{
			SendResTransResult(source_ip,pSyncResMsg->rand, pSyncResMsg->res_id,1);
			return 0;
		}
		else
		{
			SendResTransResult(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			return -2;
		}
	}
	
	//原来的res同步
	else
	{
		CheckResFileDir();
		FileTrsResFileSave(pSyncResMsg->res_id);
		if(GetLocalResById(pSyncResMsg->res_id, pSyncResMsg->cleint_addr,res_file) != 0)
		{
			SendSyncResFromServerRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			dprintf("!!!!!!!RecvSyncResFromServerReq fail -1\n");
			return -1;
		}
		
		SendSyncResFromServerRsp(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,1);
		
		ret = TransResToDevice(res_file,source_ip);

		if(ret !=0)
		{
			SendResTransResult(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,-1);
			dprintf("!!!!!!!RecvSyncResFromServerReq fail -2\n");
			return -2;
		}
		
		SendResTransResult(source_ip,pSyncResMsg->rand, pSyncResMsg->res_id,1);
		return 0;
	}		
}

int PushUdpRecvMsgToResSyncQueue(int source_ip,int cmd,char *pbuff);

void RecvResSyncCmd_Process(int source_ip,int cmd, char *pbuff)
{
	SyncResMsg_T *pSyncResMsg = (SyncResMsg_T *)pbuff;

	switch(cmd)
	{
		case CMD_RES_UPLOAD_REQ:
			//RecvTransResToDeviceReq(source_ip,pSyncResMsg->rand,pSyncResMsg->res_id,pSyncResMsg->server_addr);
			PushUdpRecvMsgToResSyncQueue(source_ip,cmd,pbuff);
			break;

		case CMD_RES_UPLOAD_RSP:
			RecvTransResToDeviceRsp((SyncResMsg_T*)pbuff);
			break;

		case CMD_RES_DOWNLOAD_REQ:
			RecvSyncResFromServerReq(source_ip, pSyncResMsg);
			break;

		
		case CMD_RES_DOWNLOAD_RSP:
			RecvSyncResFromServerRsp(pSyncResMsg->server_addr, pSyncResMsg->rand, pSyncResMsg->res_id, pSyncResMsg->result);
			break;	

		case CMD_RES_TRANS_RESULT:
			RecvResTransResult(pSyncResMsg->server_addr, pSyncResMsg->rand, pSyncResMsg->res_id, pSyncResMsg->result);
			break;
	}
}
typedef struct
{
	VDP_MSG_HEAD msg_head;
	int cmd;
	int ip_addr;
	//int rand;
	SyncResMsg_T SyncResMsg;
}SyncResTaskMsg_T;
Loop_vdp_common_buffer	ressync_mesg_queue;
Loop_vdp_common_buffer	ressync_sync_queue;
vdp_task_t				task_ressync = {0};
void* ressync_mesg_task( void* arg );
void ressync_mesg_data_process(char* msg_data,int len);

void vtk_TaskInit_ResSync(void)
{
	init_vdp_common_queue(&ressync_mesg_queue, 500, (msg_process)ressync_mesg_data_process, &task_ressync);
	init_vdp_common_queue(&ressync_sync_queue, 100, NULL, 								  &task_ressync);
	init_vdp_common_task(&task_ressync, MSG_ID_ResSync, ressync_mesg_task, &ressync_mesg_queue, &ressync_sync_queue);

	//dprintf("survey task starting............\n");
}

void ressync_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pVdpUdp = (VDP_MSG_HEAD*)msg_data;
	SyncResTaskMsg_T *pSyncResTaskMsg = (SyncResTaskMsg_T *)(msg_data);

	// 判断是否为系统服务
	switch(pSyncResTaskMsg->cmd)
	{
		case CMD_RES_DOWNLOAD_REQ:
			RecvSyncResFromServerReq(pSyncResTaskMsg->ip_addr, &(pSyncResTaskMsg->SyncResMsg));
			break;

		case CMD_RES_UPLOAD_REQ:
			RecvTransResToDeviceReq(pSyncResTaskMsg->ip_addr, &(pSyncResTaskMsg->SyncResMsg));
			break;
	}
}

void* ressync_mesg_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int PushUdpRecvMsgToResSyncQueue(int source_ip,int cmd,char *pbuff)
{
	SyncResTaskMsg_T send_msg;
	send_msg.msg_head.msg_type 		= 0;
	send_msg.msg_head.msg_sub_type	= 0;
	send_msg.cmd = cmd;
	send_msg.ip_addr = source_ip;
	
	memcpy(&send_msg.SyncResMsg,pbuff,sizeof(SyncResMsg_T));
	
	//if (OS_Q_Put(&q_phone, &send_msg_to_caller, sizeof(CALLER_STRUCT)))
	if(push_vdp_common_queue(&ressync_mesg_queue, (char *)&send_msg, sizeof(SyncResTaskMsg_T)) ==0 )
	{
		return;
	}
}

void IxDeviceFileDownloadReport(int reportIp, char* state, char* msg, int resId)
{
	char strTemp[10];

	snprintf(strTemp, 10, "%04d", resId);
	
	IxReportResDownload(&reportIp , 1, "UDP", state, msg, strTemp, NULL, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

