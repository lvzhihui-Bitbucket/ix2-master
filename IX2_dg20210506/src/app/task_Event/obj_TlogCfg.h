﻿/**
  ******************************************************************************
  * @file    obj_TlogCfg.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_TlogCfg_H
#define _obj_TlogCfg_H

typedef struct
{	
	char* type;
	char* name;
}EVENT_LIST_S;

typedef struct
{	
	int enable;
	char* name;
}TLOG_CFG_S;

typedef struct
{	
	int cnt;
	TLOG_CFG_S cfg[50];
}TLOG_LIST_S;

TLOG_LIST_S GetTlogList(void);


#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

