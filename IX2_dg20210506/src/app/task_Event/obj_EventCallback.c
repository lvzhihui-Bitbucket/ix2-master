﻿/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "elog.h"
#include "define_string.h"
#include "define_file.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "ota_client_ifc.h"
#include "obj_IoInterface.h"

const char* GetEventItemString(cJSON* event,  const char* itemName)
{
	char* item = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, itemName));
	item = (item != NULL ? item : "");

	return item;
}

int GetEventItemInt(cJSON* event,  const char* itemName)
{
	int ret;
	cJSON* item = cJSON_GetObjectItemCaseSensitive(event, itemName);
	if(cJSON_IsNumber(item))
	{
		ret = item->valueint;
	}
	else if(cJSON_IsString(item))
	{
		ret = atoi(item->valuestring);
	}
	else if(cJSON_IsTrue(item))
	{
		ret = 1;
	}
	else
	{
		ret = 0;
	}

	return ret;
}

//证书配置回调函数
static int CertConfigCallback(cJSON *cmd)
{
	char* opcode = GetEventItemString(cmd, "OPCODE");

	if(!strcmp(opcode, "CreateDcfg"))
	{
		API_Create_DCFG_SELF("CERT_ETH0", NULL);
	}
	else if(!strcmp(opcode, "CreateCert"))
	{
		char* dcfgName = GetEventItemString(cmd, "DCFG_NAME");
		dcfgName = (dcfgName[0] == 0 ? CERT_DCFG_SELF_NAME : dcfgName);
		API_Create_CERT(dcfgName);
	}
	else if(!strcmp(opcode, "CheckGoLiveResult"))
	{
		API_CheckGoLiveResult();
	}
	else if(!strcmp(opcode, "GoLive"))
	{
		API_Go_Live();
	}
	else if(!strcmp(opcode, "DeleteCert"))
	{
		API_Delete_CERT();
	}
	else if(!strcmp(opcode, "CheckCert"))
	{
		int time = GetEventItemInt(cmd, "TIME");
		if(time)
		{
			API_Para_Write_Int(CERT_GoLive_Timer, time);
		}
		API_Check_CERT();
	}
	else if(!strcmp(opcode, "CreateCertSelf"))
	{
		API_Create_DCFG_SELF("CERT_ETH0", NULL);
		API_Create_CERT(CERT_DCFG_SELF_NAME);
		API_Go_Live();
	}
	
	return 1;
}

//远程事件记录
int EventRemoteLogCallback(cJSON *cmd)
{
	cJSON* record = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_EventData), 1);
	if(record)
	{
		char temp[100];
		cJSON_AddStringToObject(record, "MY_TIME", GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
		API_TB_AddByName(TB_NAME_LOG_TB, record);
		cJSON_Delete(record);
	}
}

int EventMissCallCallback(cJSON *cmd)
{
	char* state = GetEventItemString(cmd, "STATE");
	if(!strcmp(state, "ON"))
	{
		#ifdef PID_IXSE
		//setMissCallState(1);
		#endif
	}
	else
	{
		#ifdef PID_IXSE
		//setMissCallState(0);
		#endif
	}
}

int EventUpdateReportCallback(cJSON *cmd)
{
	#if defined(PID_IXSE)||defined(PID_IX47)||defined(PID_IX482)		
		RemoteUpdate_Report(GetEventItemString(cmd, "MSG"));
	#endif		
}

int Event_FwUpdateStart(cJSON *cmd)
{
	OtaClientPara para;
	char* server = GetEventItemString(cmd, "Server");
	char* code = GetEventItemString(cmd, "Code");
	
	if(server && code && server[0] && code[0])
	{
		snprintf(para.server_ip, 16, "%s", server);
		snprintf(para.file_code, 15, "%s", code);
		para.file_type = 1;
		strcpy(para.dev_type, GetSysVerInfo_type());
		strcpy(para.dev_ver, GetSysVerInfo_swVer());
		strcpy(para.dev_id, GetSysVerInfo_Sn());
		para.reportIpCnt = 0;
		para.reportIp[0] = 0;
		para.space = 0;
		para.downloadType = OtaDlTypeFtp;
		API_OtaDownload(1, para);
	}

	return 1;
}

int NDM_S_CtrlCallback(cJSON *cmd);
int NDM_C_CtrlCallback(cJSON *cmd);
int NDM_C_ResponeCallback(cJSON *cmd);
//Tlog客户端在线情况检测
int EvenTLogClientCheckProcess(cJSON *cmd);
//Tlog客户端开启或停止
int EventTLogClientCtrlProcess(cJSON *cmd);
int EventCardIdReportCallback(cJSON *cmd);

int IXS_ProxyReqUpdateTb_CallBack(cJSON *event);
int EventIperfTestStartCallback(cJSON *cmd);
int EventIperfTestReportCallback(cJSON *cmd){}
int EventUpdateTbCallback(cJSON *event){}
int EventUpdateTbUnchangeCallback(cJSON *event){}
int IXS_ProxyNetConnectedCallback(cJSON *event);

void EventCallbackFunctionRegister(void)
{
	API_EventCallbackFunctionAdd("EventRemoteLogCallback", EventRemoteLogCallback, NULL);
	API_EventCallbackFunctionAdd("CertConfigCallback", CertConfigCallback, NULL);
	API_EventCallbackFunctionAdd("EventMissCallCallback", EventMissCallCallback, NULL);	
	API_EventCallbackFunctionAdd("EventUpdateReportCallback", EventUpdateReportCallback, NULL);	
	API_EventCallbackFunctionAdd("Event_FwUpdateStart", Event_FwUpdateStart, NULL);
	API_EventCallbackFunctionAdd("NDM_S_CtrlCallback", NDM_S_CtrlCallback, NULL);
	API_EventCallbackFunctionAdd("NDM_C_CtrlCallback", NDM_C_CtrlCallback, NULL);
	API_EventCallbackFunctionAdd("NDM_C_ResponeCallback", NDM_C_ResponeCallback, NULL);
	API_EventCallbackFunctionAdd("EvenTLogClientCheckProcess", EvenTLogClientCheckProcess, NULL);
	API_EventCallbackFunctionAdd("EventTLogClientCtrlProcess", EventTLogClientCtrlProcess, NULL);
#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)	
	API_EventFilterCallbackFunctionAdd("EventCardIdReportCallback", EventCardIdReportCallback, NULL);		
#endif	
	API_EventCallbackFunctionAdd("IXS_ProxyReqUpdateTb_CallBack", IXS_ProxyReqUpdateTb_CallBack, NULL);	
	API_EventCallbackFunctionAdd("EventIperfTestStartCallback", EventIperfTestStartCallback, NULL);	
	API_EventCallbackFunctionAdd("EventIperfTestReportCallback", EventIperfTestReportCallback, NULL);	
	API_EventCallbackFunctionAdd("EventUpdateTbCallback", EventUpdateTbCallback, NULL);	
	API_EventCallbackFunctionAdd("EventUpdateTbUnchangeCallback", EventUpdateTbUnchangeCallback, NULL);	
	API_EventCallbackFunctionAdd("IXS_ProxyNetConnectedCallback", IXS_ProxyNetConnectedCallback, NULL);

	API_EventConfigAdd(EventRemoteElog, "EventRemoteLogCallback", 1, NULL);
	API_EventConfigAdd(EventCertConfig, "CertConfigCallback", 1, NULL);
	API_EventConfigAdd(EventMissCall, "EventMissCallCallback", 1, NULL);
	API_EventConfigAdd(EventUpdateReportToRemote, "EventUpdateReportCallback", 1, NULL);
	API_EventConfigAdd(EventUpdateStart, "Event_FwUpdateStart", 1, NULL);
	API_EventConfigAdd(EventTLogClientCheckOnline, "EvenTLogClientCheckProcess", 1, NULL);
	API_EventConfigAdd(EventTLogClientCtrl, "EventTLogClientCtrlProcess", 1, NULL);
	
	API_EventConfigAdd(EventNDM_S_Ctrl, "NDM_S_CtrlCallback", 0, NULL);
	API_EventConfigAdd(EventNDM_C_Ctrl, "NDM_C_CtrlCallback", 0, NULL);
	API_EventConfigAdd(EventNDM_C_CtrlResponse, "NDM_C_ResponeCallback", 0, NULL);
#if defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)	
	API_EventConfigAdd(EventCardIdReport, "EventCardIdReportCallback", 1, NULL);
#endif	

	API_EventConfigAdd(Event_IXS_ReqUpdateTb, "IXS_ProxyReqUpdateTb_CallBack", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "EventUpdateTbCallback", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbUnchanged, "EventUpdateTbUnchangeCallback", 1, NULL);
	API_EventConfigAdd(EventNetLinkState, "IXS_ProxyNetConnectedCallback", 1, NULL);

	API_EventConfigAdd(Event_IPERF_TEST_START, "EventIperfTestStartCallback", 1, NULL);
	API_EventConfigAdd(Event_IPERF_TEST_REPORT, "EventIperfTestReportCallback", 1, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

