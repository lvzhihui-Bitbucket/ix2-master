/**
  ******************************************************************************
  * @file    task_Event.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Event_H
#define _task_Event_H

#include "cJSON.h"
#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------

typedef int (*EventCmdCallBack)(cJSON *cmd);
typedef int (*EventFilterCallBack)(cJSON *cmd);

typedef struct
{
		vdp_task_t			  		    task;
		Loop_vdp_common_buffer  	msgQ;
		vdp_task_t			  		    loopTask;
		Loop_vdp_common_buffer  	loopMsgQ;
		Loop_vdp_common_buffer  	loopSyncQ;
    cJSON                     *data;
    cJSON                     *eventCfg;
}EVENT_RUN_S;

typedef struct
{
    pthread_t	                tid;
  	EventCmdCallBack	        callback;
  	cJSON	                    *jsonPara;
}ParallelEventRun_S;

extern EVENT_RUN_S eventRun;

int vtk_TaskInit_Event(void);
int API_Event(const char* jsonString);
int API_Event_By_Name(const char* eventName);

int API_EventCallbackFunctionAdd(const char* callBackName, EventCmdCallBack callBack, const char* notes);
int API_EventCallbackFunctionDelete(const char* callBackName);

#define EVENT_KEY_EventName			"EventName"
#define EVENT_KEY_Debug			    "Debug"
#define EVENT_KEY_Message			  "Message"

#define EventRemoteElog                         "EventRemoteElog"       //远程log
#define EventTLogClientCheckOnline              "EventTLogClientCheckOnline"
#define EventTLogClientCtrl                     "EventTLogClientCtrl"
#define EventCertConfig                         "EventCertConfig"       //证书配置
#define EventMissCall                           "EventMissCall"
#define EventUpdateReportToRemote               "EventUpdateReportToRemote"
#define EventUpdateStart                        "EventUpdateStart"

#define EventNDM_S_Ctrl             "EventNDM_S_Ctrl"         //NDM服务器控制
#define EventNDM_S_CheckEnd         "EventNDM_S_CheckEnd"    //NDM服务器定时检测客户端在线情况结束
#define EventNDM_C_Ctrl             "EventNDM_C_Ctrl"       //NDM客户端控制
#define EventNDM_C_CtrlResponse     "EventNDM_C_CtrlResponse"       //NDM客户端控制应答
#define EventCardIdReport           "EventCardIdReport"

#define Event_VideoSerError               "Event_VideoSerError"

#define Event_IXS_ReqUpdateTb				              "IXS_ReqUpdateTb"
#define Event_IXS_ReportTbChanged				          "IXS_ReportTbChanged"
#define Event_IXS_ReportTbUnchanged				        "IXS_ReportTbUnchanged"

#define Event_IPERF_TEST_START				            "IPERF_TEST_START"
#define Event_IPERF_TEST_REPORT				            "IPERF_TEST_REPORT"
#define EventNetLinkState                         "EventNetLinkState"



int API_Event(const char* jsonString);
int API_Event_Json(cJSON* jsonEvent);
int API_Event_By_Name(const char* eventName);
int API_Event_NameAndMsg(const char* name, const char* format, ...);
int API_EventDebug(const char* eventName, const char* format, ...);
int API_Event_NameAndDebugAndMsg(const char* name, const char* debugInfo, const char* format, ...);
#endif


