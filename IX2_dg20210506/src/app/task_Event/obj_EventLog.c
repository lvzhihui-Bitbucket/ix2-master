﻿/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "elog.h"
#include "utility.h"
#include "define_string.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "cJSON.h"
#include "obj_NDM.h"
#include "obj_TableSurver.h"

//#define IX2V_Tlog

static int eventLogIp = 0;

static int TLogClientStart(char* value)
{
	int ret = 0;
	if(value && (strlen(value) == 10))
	{
		#ifdef IX2V_Tlog
		cJSON* ipTb = cJSON_CreateArray();
		if(IXS_GetByNBR(API_Para_Read_String2(IX_NT), NULL, value, NULL, ipTb) > 0)
		{
			char* ipString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(ipTb, 0), IX2V_IP_ADDR));
			if(ipString)
			{
				int ip = inet_addr(ipString);
				if(ip != -1)
				{
					eventLogIp = ip;
					ret = 1;					
					API_PublicInfo_Write_String(PB_TlogIX_ADDR, value);
					dprintf("EventLog IP=%s\n", ipString);
				}
			}
		}
		cJSON_Delete(ipTb);
		#else
		#include "obj_GetIpByNumber.h";
		GetIpRspData searchData;
		if(API_GetIpNumberFromNet(value, NULL, NULL, 3, 1, &searchData) == 0)
		{
			if(searchData.cnt)
			{
				eventLogIp = searchData.Ip[0];
				ret = 1;
				API_PublicInfo_Write_String(PB_TlogIX_ADDR, value);;
				dprintf("EventLog IP=%s\n", my_inet_ntoa2(eventLogIp));
			}
		}
		#endif
	}
	return ret;
}

static int TLogClientStop(char* value)
{
	eventLogIp = 0;
	API_PublicInfo_Write_String(PB_TlogIX_ADDR, "");
	return 1;
}

//Settingcode开启EventLog
int SettingCodeEventLogStart(char* value)
{
	int ret = TLogClientStart(value);
	if(ret)
	{
		API_Para_Write_String(TLogIX_ADDR, value);
	}
	return ret;
}


//Settingcode关闭EventLog
int SettingCodeEventLogStop(char* value)
{
	API_Para_Write_String(TLogIX_ADDR, "");
	return TLogClientStop(value);
}

int EventLog(cJSON* event)
{
	if(eventLogIp)
	{
		int typeEventSize;
		int typeEventCnt;
		cJSON* typeElement;
		cJSON* eventElement;
		cJSON* eventLogCfg = API_Para_Read_Public(EventLogCfg);
		char* eventName = GetEventItemString(event,  EVENT_KEY_EventName);
		char* cfgEventType;
		char* cfgEventName;

		if(cJSON_IsArray(eventLogCfg))
		{
			cJSON_ArrayForEach(typeElement, eventLogCfg)
			{
				if(cJSON_IsArray(typeElement))
				{
					typeEventSize = cJSON_GetArraySize(typeElement);
					if(typeEventSize > 1)
					{
						cfgEventType = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, 0));
						if(cfgEventType)
						{
							for(typeEventCnt = 1; typeEventCnt < typeEventSize; typeEventCnt++)
							{
								cfgEventName = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, typeEventCnt));
								if(cfgEventName && !strcmp(eventName, cfgEventName))
								{
									char temp[100];
									char* tempString;
									cJSON* remoteEvent = cJSON_CreateObject();
									cJSON_AddStringToObject(remoteEvent, IX2V_EventName, EventRemoteElog);
									cJSON* eventLogRecord = cJSON_AddObjectToObject(remoteEvent, IX2V_EventData);

									cJSON_AddStringToObject(eventLogRecord, IX2V_TIME, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
									cJSON_AddStringToObject(eventLogRecord, IX2V_MFG_SN, API_PublicInfo_Read_String(PB_MFG_SN));
									cJSON_AddStringToObject(eventLogRecord, IX2V_IX_ADDR, API_PublicInfo_Read_String(PB_IX_ADDR));
									cJSON_AddStringToObject(eventLogRecord, IX2V_TYPE, cfgEventType);
									cJSON_AddStringToObject(eventLogRecord, IX2V_EventName, cfgEventName);

									cJSON* eventData = cJSON_CreateObject();
									cJSON_ArrayForEach(eventElement, event)
									{
										if(eventElement->string && strcmp(eventElement->string, IX2V_EventName))
										{
											cJSON_AddItemToObject(eventData, eventElement->string, cJSON_Duplicate(eventElement, 1));
										}
									}
									tempString = cJSON_PrintUnformatted(eventData);
									cJSON_Delete(eventData);

									cJSON_AddStringToObject(eventLogRecord, IX2V_EventData, tempString);
									if(tempString)
									{
										free(tempString);
									}

									if(API_XD_EventJson(eventLogIp, remoteEvent) == 0)
									{
										//发送记录失败
									}
									cJSON_Delete(remoteEvent);
									return 1;
								}
							}
						}
					}
				}
			}
		}

		if(eventLogCfg)
		{
			cJSON_Delete(eventLogCfg);
		}
	}
	return 0;
}

void TLogInit(void)
{
	static int tlogInitFlag = 0;
	if(!tlogInitFlag)
	{
		tlogInitFlag = 1;
		//服务端
		if(API_Para_Read_Int(TLogServerEnable))
		{
			API_TLogServerCtrl(1);
		}

		//客户端
		char* ixAddr = API_Para_Read_String2(TLogIX_ADDR);
		if(ixAddr)
		{
			TLogClientStart(ixAddr);
		}
	}
}

static int TLogClientCheckTimerCb(int time)
{
	API_Event_By_Name(EventTLogClientCheckOnline);
	return 1;
}

//返回：0 --失败；1 --成功
int API_TLogServerCtrl(int enable)
{
	char* ipString;
	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		cJSON* element;
		cJSON_ArrayForEach(element, View)
		{
			//获坖所有客户端的状思
			ipString = GetEventItemString(element, NDM_RES_IP_ADDR);			
			API_TLogClientCtrl(inet_addr(ipString), enable);
		}
		API_Add_TimingCheck(TLogClientCheckTimerCb, API_Para_Read_Int(NDM_S_TIME));
	}
	else
	{
		API_Del_TimingCheck(TLogClientCheckTimerCb);
	}
	cJSON_Delete(View);
	cJSON_Delete(Where);
	API_Para_Write_Int(TLogServerEnable, enable);
	API_PublicInfo_Write_String(PB_TlogServerState, enable ? "ON":"OFF");
	return 1;
}

//Tlog客户端在线情况检测
int EvenTLogClientCheckProcess(cJSON *cmd)
{
	if(API_Para_Read_Int(TLogServerEnable))
	{
		cJSON* View = cJSON_CreateArray();
		cJSON* Where = cJSON_CreateObject();
		cJSON_AddItemToObject(Where, NDM_RES_ENABLE, cJSON_CreateTrue());
		if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
		{
			cJSON* element;
			cJSON* tlogIXAddr;
			char* ipString;
			cJSON_ArrayForEach(element, View)
			{
				//获坖所有客户端的状思
				ipString = GetEventItemString(element, NDM_RES_IP_ADDR);
				tlogIXAddr = API_GetRemotePb(inet_addr(ipString), PB_TlogIX_ADDR);
				if(cJSON_IsString(tlogIXAddr) && strcmp(tlogIXAddr->valuestring, GetSysVerInfo_BdRmMs()))
				{
					API_TLogClientCtrl(inet_addr(ipString), 1);
				}
				if(tlogIXAddr)
				{
					cJSON_Delete(tlogIXAddr);
				}
			}
		}
		cJSON_Delete(View);
		cJSON_Delete(Where);
	}
	return 1;
}

//返回：0 --失败；1 --成功
int API_TLogClientCtrl(int ip, int enable)
{
	int ret;
	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventTLogClientCtrl);
	cJSON_AddStringToObject(event, "CMD", enable ? "Start" : "Stop");
	if(enable)
	{
		cJSON_AddStringToObject(event, TLogIX_ADDR, GetSysVerInfo_BdRmMs());
		cJSON_AddItemToObject(event, EventLogCfg, cJSON_Duplicate(API_Para_Read_Public(EventLogCfg), 1));
	}
	ret = API_XD_EventJson(ip, event);
	cJSON_Delete(event);
	return ret;
}

int EventTLogClientCtrlProcess(cJSON *cmd)
{
	char* operate = GetEventItemString(cmd, "CMD");
	if(!strcmp(operate, "Start"))
	{
		char* ixAddr = GetEventItemString(cmd, TLogIX_ADDR);
		cJSON* config = cJSON_GetObjectItemCaseSensitive(cmd, EventLogCfg);
		if(cJSON_IsArray(config))
		{
			cJSON *tempValue = cJSON_CreateObject();
			cJSON_AddItemToObject(tempValue, EventLogCfg, cJSON_Duplicate(config, 1));
			API_Para_Write_Public(tempValue);
			cJSON_Delete(tempValue);
		}
		TLogClientStart(ixAddr);
	}
	else if(!strcmp(operate, "Stop"))
	{
		TLogClientStop(NULL);
	}

	return 0;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

