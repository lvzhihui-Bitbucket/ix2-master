﻿/**
  ******************************************************************************
  * @file    obj_TlogMenu.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "elog.h"
#include "utility.h"
#include "cJSON.h"
#include "obj_GetIpByNumber.h";
#include "define_file.h"
#include "task_Event.h"
#include "define_string.h"
#include "obj_IoInterface.h"
#include "obj_TlogCfg.h"

static EVENT_LIST_S EVENT_LIST[] = {
	{"Reboot", 				"EventPowerOnAndConnected"},
	{"Reboot", 				"EventWillReboot"},
	{"Call", 				"EventBECall"},
	{"Call", 				"EventCall"},
	{"Call", 				"EventBECallTalk"},
	{"Call", 				"EventBECallBye"},
	{"Mon", 				"EventMonOpen"},
	{"Mon", 				"EventMonClose"},
	{"Unlock", 				"EventCallUnlock"},
	{"Type", 				"EventCertConfig"},
	{"Type", 				"EventRemoteElog"},
	{"Type", 				"Event10"},
	{"Type", 				"Event11"},
	{"Type", 				"Event12"},
};

static int EVENT_LIST_CNT = sizeof(EVENT_LIST)/sizeof(EVENT_LIST[0]);

static int SaveTlogConfig(cJSON* tlogCfg)
{
	int ret;
	cJSON *tempValue = cJSON_CreateObject();
	cJSON_AddItemToObject(tempValue, EventLogCfg, cJSON_Duplicate(tlogCfg, 1));
	ret = API_Para_Write_Public(tempValue);
	cJSON_Delete(tempValue);
	return ret;
}

static int SetTlogEvent(const char* type, const char* name)
{
	int eventCnt;
	int typeCnt;
	int typeIndex;
	int eventIndex;
	int addFlag = 0;
	cJSON* typeElement;
	cJSON* eventElement;
	cJSON* typeJson;

	if(!type || !name)
	{
		return addFlag;
	}

	cJSON* eventConfig = cJSON_Duplicate(API_Para_Read_Public(EventLogCfg), 1);
	typeCnt = cJSON_GetArraySize(eventConfig);
	for(typeIndex = typeCnt-1; typeIndex >= 0 && addFlag == 0; typeIndex--)
	{
		typeElement = cJSON_GetArrayItem(eventConfig, typeIndex);
		eventCnt = cJSON_GetArraySize(typeElement);
		if(eventCnt >= 2)
		{
			typeJson = cJSON_GetArrayItem(typeElement, 0);
			for(eventIndex = eventCnt-1; eventIndex >= 1; eventIndex--)
			{
				eventElement = cJSON_GetArrayItem(typeElement, eventIndex);
				if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, name))
				{
					if(typeJson && typeJson->valuestring && !strcmp(typeJson->valuestring, type))
					{
						//已经存在，并且类型相同
						addFlag = 2;
						break;
					}
					else
					{
						cJSON_DeleteItemFromArray(typeElement, eventIndex);
					}
				}
			}
		}
		else
		{
			cJSON_DeleteItemFromArray(eventConfig, typeIndex);
		}
	}

	if(!addFlag)
	{
		cJSON_ArrayForEach(typeElement, eventConfig)
		{
			eventElement = cJSON_GetArrayItem(typeElement, 0);
			if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, type))
			{
				cJSON_AddItemToArray(typeElement, cJSON_CreateString(name));
				//添加成功
				addFlag = 1;
				break;
			}
		}
	}

	if(!addFlag)
	{
		typeJson = cJSON_CreateArray();
		cJSON_AddItemToArray(typeJson, cJSON_CreateString(type));
		cJSON_AddItemToArray(typeJson, cJSON_CreateString(name));
		cJSON_AddItemToArray(eventConfig, typeJson);
		//添加成功
		addFlag = 1;
	}

	if(addFlag == 1)
	{
		SaveTlogConfig(eventConfig);
	}
	cJSON_Delete(eventConfig);

	return addFlag;
}

static int DelTlogEvent(const char* name)
{
	int eventCnt;
	int typeCnt;
	int typeIndex;
	int eventIndex;
	int ret = 0;
	cJSON* typeElement;
	cJSON* eventElement;

	if(!name)
	{
		return ret;
	}

	cJSON* eventConfig = cJSON_Duplicate(API_Para_Read_Public(EventLogCfg), 1);
	typeCnt = cJSON_GetArraySize(eventConfig);
	for(typeIndex = typeCnt-1; typeIndex >= 0; typeIndex--)
	{
		typeElement = cJSON_GetArrayItem(eventConfig, typeIndex);
		eventCnt = cJSON_GetArraySize(typeElement);
		if(eventCnt >= 2)
		{
			for(eventIndex = eventCnt-1; eventIndex >= 1; eventIndex--)
			{
				eventElement = cJSON_GetArrayItem(typeElement, eventIndex);
				if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, name))
				{
					cJSON_DeleteItemFromArray(typeElement, eventIndex);
					ret = 1;
				}
			}
		}
		else
		{
			cJSON_DeleteItemFromArray(eventConfig, typeIndex);
		}
	}

	if(ret)
	{
		SaveTlogConfig(eventConfig);
	}
	cJSON_Delete(eventConfig);

	return ret;
}

static int SearchTlogEvent(const char* name)
{
	int eventCnt;
	int typeCnt;
	int typeIndex;
	int eventIndex;
	int ret = 0;
	cJSON* typeElement;
	cJSON* eventElement;

	if(!name)
	{
		return ret;
	}

	cJSON* eventConfig = API_Para_Read_Public(EventLogCfg);
	typeCnt = cJSON_GetArraySize(eventConfig);
	for(typeIndex = typeCnt-1; typeIndex >= 0; typeIndex--)
	{
		typeElement = cJSON_GetArrayItem(eventConfig, typeIndex);
		eventCnt = cJSON_GetArraySize(typeElement);
		if(eventCnt >= 2)
		{
			for(eventIndex = eventCnt-1; eventIndex >= 1; eventIndex--)
			{
				eventElement = cJSON_GetArrayItem(typeElement, eventIndex);
				if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, name))
				{
					ret = 1;
				}
			}
		}
	}

	return ret;
}

TLOG_LIST_S GetTlogList(void)
{
	int i;
	TLOG_LIST_S tLogList;

	for(tLogList.cnt = 0, i = 0; i < EVENT_LIST_CNT; i++)
	{
		tLogList.cfg[i].name = EVENT_LIST[i].name;
		tLogList.cfg[i].enable = SearchTlogEvent(tLogList.cfg[i].name);
		tLogList.cnt++;
	}
	return tLogList;
}

int SetTlogEventEnable(const char* name, int enable)
{
	int ret = 0;
	if(enable)
	{
		int i;
		for(i = 0; i < EVENT_LIST_CNT; i++)
		{
			if(name && EVENT_LIST[i].name && !strcmp(name, EVENT_LIST[i].name))
			{
				ret = SetTlogEvent(EVENT_LIST[i].type, name);
				break;
			}
		}
	}
	else
	{
		ret = DelTlogEvent(name);
	}

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

