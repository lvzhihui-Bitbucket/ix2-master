#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include "task_survey.h"
#include "cJSON.h"
#include "obj_GetIpByNumber.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_SearchIpByFilter.h"
#include "obj_TableProcess.h"

#define MaxAlarmRecord	500

cJSON *AlarmingRecord_Json = NULL;
cJSON *AlarmingRecord_Cur = NULL;
cJSON *AlarmingRecord_CurArray = NULL;
cJSON *AlarmingRecord_UnconfirmArray = NULL;
extern cJSON *AlarmInformPara;


void AlamingRecordInformService(char *json_string);
void Alaming24HRecordInformService(char *json_string);

//cJSON *AlarmingRecord_SaveArray = NULL;
void AlarmingRecord_Init(void)
{
	FILE *file;
	char *json;
	int i,j;
	AlarmingRecord_UnconfirmArray = cJSON_CreateArray();
	if((file=fopen(AlarmingRecord_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 AlarmingRecord_Json = cJSON_Parse(json);
		 free(json);
		 if(!cJSON_IsArray(AlarmingRecord_Json))
		 {
			cJSON_Delete(AlarmingRecord_Json);
			AlarmingRecord_Json = NULL;
		 }
		
		 if(AlarmingRecord_Json!=NULL&&cJSON_GetArraySize(AlarmingRecord_Json)>0)
		 {
		 	cJSON *one_node;
		 	for(i=cJSON_GetArraySize(AlarmingRecord_Json);i>0;i--)
		 	{
		 		one_node = cJSON_GetArrayItem(AlarmingRecord_Json,i-1);
		 	
				if(one_node!=NULL)
				{
					cJSON *status;
					cJSON *type;
					status = cJSON_GetObjectItemCaseSensitive(one_node,"STATUS");
					type = cJSON_GetObjectItemCaseSensitive(one_node,"TYPE");
					if(status!=NULL&&type!=NULL)
					{
						if(strcmp(status->valuestring,"RUNNING")==0&&strcmp(type->valuestring,"ARMED")==0)
						{
							AlarmingRecord_Cur=one_node;
							AlarmingRecord_CurArray = cJSON_GetObjectItemCaseSensitive(one_node,"RECORD");
							AlarmInformPara=cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(one_node,"INFORM_PARA"),1);
							//AlarmInformPara = cJSON_GetObjectItemCaseSensitive(one_node,"INFORM_PARA");
							for(j=cJSON_GetArraySize(AlarmingRecord_CurArray);j>0;j--)
							{
								one_node = cJSON_CreateNumber((int)cJSON_GetArrayItem(AlarmingRecord_CurArray,j-1));
								cJSON_InsertItemInArray(AlarmingRecord_UnconfirmArray,0,one_node);
							}
							break;
						}
						else if(strcmp(status->valuestring,"Unconfirm")==0)
						{
							one_node = cJSON_CreateNumber((int)one_node);
							cJSON_InsertItemInArray(AlarmingRecord_UnconfirmArray,0,one_node);
						}
						
					}
				}
		 	}
			
		 }
	}
	
	if(AlarmingRecord_Json == NULL)
		AlarmingRecord_Json = cJSON_CreateArray();	
	//if(AlarmingRecord_UnconfirmArray == NULL)
	//	AlarmingRecord_UnconfirmArray = cJSON_CreateArray();
	
}

void SaveAlarmRecordToFile(void)
{
	while(cJSON_GetArraySize(AlarmingRecord_Json)>MaxAlarmRecord)
	{
		cJSON_DeleteItemFromArray(AlarmingRecord_Json,0);
	}
	FILE	*file = NULL;
	char *string=cJSON_Print(AlarmingRecord_Json);
	if(string!=NULL)
	{
		if((file=fopen(AlarmingRecord_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		//printf("AlarmingRecord_Json:\n%s\n",string);
		free(string);
	}
}

void MoveAllCurAlarmRecordToSave(void)
{
	//int i;
	cJSON *one_record;
	cJSON *type;
	cJSON *status;
	int have_move = 0;
	if(AlarmingRecord_Cur == NULL)
		return;
	#if 0
	for(i=0;i<cJSON_GetArrayItem(AlarmingRecord_CurArray),i++)
	{
		one_record = cJSON_GetArrayItem(AlarmingRecord_CurArray,i);
		if(one_record!=NULL)
		{
			type = cJSON_GetObjectItemCaseSensitive(one_record,"TYPE");
			if(strcmp(type->valuestring,"24H")==0)
			{
				one_record = cJSON_DetachItemFromArray(AlarmingRecord_CurArray,i);
				i--;
				cJSON_AddItemToArray(AlarmingRecord_Json,one_record);
			}
		}
		
	}
	#endif
	cJSON_DeleteItemFromObjectCaseSensitive(AlarmingRecord_Cur,"STATUS");
	cJSON_AddStringToObject(AlarmingRecord_Cur,"STATUS","END");
	AlarmingRecord_Cur = NULL;
	AlarmingRecord_CurArray = NULL;
	while(cJSON_GetArraySize(AlarmingRecord_UnconfirmArray)>0)
	{
		one_record = cJSON_GetArrayItem(AlarmingRecord_UnconfirmArray,0);
		one_record=(cJSON *)one_record->valueint;
		status = cJSON_GetObjectItemCaseSensitive(one_record,"STATUS");
		type = cJSON_GetObjectItemCaseSensitive(one_record,"TYPE");
		if(strcmp(type->valuestring,"24H_Alarming")==0&&strcmp(status->valuestring,"Unconfirm")==0)
		{
			cJSON_DeleteItemFromObjectCaseSensitive(one_record,"STATUS");
			cJSON_AddStringToObject(one_record,"STATUS","Confirm");
			//cJSON_DeleteItemFromArray(AlarmingRecord_UnconfirmArray,i);
		}
		cJSON_DeleteItemFromArray(AlarmingRecord_UnconfirmArray,0);
	}
	SaveAlarmRecordToFile();
}
void InitCurArmedRecord(int armed_mode,int include_zone,int bypass_zone)
{
	int cur_time;
	cur_time = time(NULL);
	AlarmingRecord_Cur = cJSON_CreateObject();
	cJSON_AddItemToArray(AlarmingRecord_Json,AlarmingRecord_Cur);
	cJSON_AddStringToObject(AlarmingRecord_Cur,"TYPE","ARMED");
	cJSON_AddStringToObject(AlarmingRecord_Cur,"STATUS","RUNNING");
	cJSON_AddNumberToObject(AlarmingRecord_Cur,"ARMED_MODE",armed_mode);
	cJSON_AddNumberToObject(AlarmingRecord_Cur,"TIME",cur_time);
	cJSON_AddNumberToObject(AlarmingRecord_Cur,"INCLUDE_ZONE",include_zone);
	cJSON_AddNumberToObject(AlarmingRecord_Cur,"BYPASS_ZONE",bypass_zone);
	cJSON *inform=cJSON_Duplicate(AlarmInformPara,1);
	cJSON_AddItemToObject(AlarmingRecord_Cur,"INFORM_PARA",inform);
	AlarmingRecord_CurArray=cJSON_AddArrayToObject(AlarmingRecord_Cur,"RECORD");
	while(cJSON_GetArraySize(AlarmingRecord_UnconfirmArray)>0)
	{
		cJSON_DeleteItemFromArray(AlarmingRecord_UnconfirmArray,0);
	}
	SaveAlarmRecordToFile();
	
}
void AddOneCurAlarmRecord(int zone_index,char *zone_name)
{
	cJSON *one_record;
	int cur_time;
	
	if(AlarmingRecord_CurArray==NULL)
		return;
	
	one_record = cJSON_CreateObject();
	if(one_record!=NULL)
	{
		cur_time = time(NULL);
		cJSON_AddStringToObject(one_record,"TYPE","ARMED_Alarming");
		cJSON_AddStringToObject(one_record,"STATUS","Unconfirm");
		cJSON_AddNumberToObject(one_record,"INDEX",zone_index);
		cJSON_AddNumberToObject(one_record,"TIME",cur_time);
		cJSON_AddStringToObject(one_record,"NAME",zone_name);

		cJSON_AddItemToArray(AlarmingRecord_CurArray,one_record);

		cJSON* unconfirm_record = cJSON_CreateNumber((int)one_record);
		cJSON_AddItemToArray(AlarmingRecord_UnconfirmArray,unconfirm_record);
		SaveAlarmRecordToFile();
	}
	cJSON_AddStringToObject(one_record,"DEV_NUM",GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(one_record,"DEV_NAME",GetSysVerInfo_name());
	char *string=cJSON_Print(one_record);
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NUM");
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NAME");
	if(string!=NULL)
	{
		//printf("AlarmingRecord_Json:\n%s\n",string);
		AlamingRecordInformService(string);
		
		free(string);
	}
	else
	{
		//printf("AlarmingRecord_Json:Fail%d\n",AlarmingRecord_Json);
	}
	
}

void AddOne24hAlarmRecord(int zone_index,char *zone_name)
{
	cJSON *one_record;
	int cur_time;
	
	//if(AlarmingRecord_CurArray==NULL)
	//	return;
	
	one_record = cJSON_CreateObject();
	if(one_record!=NULL)
	{
		cur_time = time(NULL);
		cJSON_AddStringToObject(one_record,"TYPE","24H_Alarming");
		cJSON_AddStringToObject(one_record,"STATUS","Unconfirm");
		cJSON_AddNumberToObject(one_record,"INDEX",zone_index);
		cJSON_AddNumberToObject(one_record,"TIME",cur_time);
		cJSON_AddStringToObject(one_record,"NAME",zone_name);

		cJSON_AddItemToArray(AlarmingRecord_Json,one_record);

		cJSON* unconfirm_record = cJSON_CreateNumber((int)one_record);
		cJSON_AddItemToArray(AlarmingRecord_UnconfirmArray,unconfirm_record);
		SaveAlarmRecordToFile();
	}
	cJSON_AddStringToObject(one_record,"DEV_NUM",GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(one_record,"DEV_NAME",GetSysVerInfo_name());
	char *string=cJSON_Print(one_record);
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NUM");
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NAME");
	if(string!=NULL)
	{
		printf("AlarmingRecord_Json:\n%s\n",string);
		Alaming24HRecordInformService(string);
		
		free(string);
	}
	else
	{
		//printf("AlarmingRecord_Json:Fail%d\n",AlarmingRecord_Json);
	}
}

void AddOneSosAlarmRecord(int zone_index,char *zone_name)
{
	cJSON *one_record;
	int cur_time;
	
	//if(AlarmingRecord_CurArray==NULL)
	//	return;
	
	one_record = cJSON_CreateObject();
	if(one_record!=NULL)
	{
		cur_time = time(NULL);
		cJSON_AddStringToObject(one_record,"TYPE","SOS_Alarming");
		cJSON_AddStringToObject(one_record,"STATUS","Unconfirm");
		cJSON_AddNumberToObject(one_record,"INDEX",8+zone_index);
		cJSON_AddNumberToObject(one_record,"TIME",cur_time);
		cJSON_AddStringToObject(one_record,"NAME",zone_name);

		cJSON_AddItemToArray(AlarmingRecord_Json,one_record);

		cJSON* unconfirm_record = cJSON_CreateNumber((int)one_record);
		cJSON_AddItemToArray(AlarmingRecord_UnconfirmArray,unconfirm_record);
		SaveAlarmRecordToFile();
	}
	cJSON_AddStringToObject(one_record,"DEV_NUM",GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(one_record,"DEV_NAME",GetSysVerInfo_name());
	char *string=cJSON_Print(one_record);
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NUM");
	cJSON_DeleteItemFromObjectCaseSensitive(one_record,"DEV_NAME");
	if(string!=NULL)
	{
		printf("AlarmingRecord_Json:\n%s\n",string);
		Alaming24HRecordInformService(string);
		
		free(string);
	}
	else
	{
		//printf("AlarmingRecord_Json:Fail%d\n",AlarmingRecord_Json);
	}
}

int GetAlarmRecordZoneIndex(cJSON *one_record,int *zone_index)
{
	cJSON *item = cJSON_GetObjectItemCaseSensitive(one_record, "INDEX");
	if(item==NULL||!cJSON_IsNumber(item))
		return -1;
	if(zone_index!=NULL)
		*zone_index = item->valuedouble;
	return 0;
}

int GetAlarmRecordHappenTime(cJSON *one_record,int *happen_time)
{
	cJSON *item = cJSON_GetObjectItemCaseSensitive(one_record, "TIME");
	if(item==NULL||!cJSON_IsNumber(item))
		return -1;
	if(happen_time!=NULL)
		*happen_time = item->valuedouble;
	return 0;
}

int GetAlarmRecordZoneName(cJSON *one_record,char *zone_name)
{
	cJSON *item = cJSON_GetObjectItemCaseSensitive(one_record, "NAME");
	if(item==NULL||!cJSON_IsString(item))
		return -1;
	if(zone_name!=NULL&&item->valuestring!=NULL)
		strcpy(zone_name,item->valuestring);
	return 0;
}

int GetCurAlarmRecordNums(void)
{
	return cJSON_GetArraySize(AlarmingRecord_UnconfirmArray);
}
int  GetCurAlarmRecordItem(int index,int *zone_index,char *zone_name,int *happen_time)
{
	if(cJSON_GetArraySize(AlarmingRecord_UnconfirmArray)<=index)
	{
		return -1;
	}

	cJSON *one_node,*one_record;
	one_node = cJSON_GetArrayItem(AlarmingRecord_UnconfirmArray,index);
	one_record=(cJSON *)one_node->valueint;

	GetAlarmRecordZoneIndex(one_record, zone_index);
	GetAlarmRecordHappenTime(one_record, happen_time);
	GetAlarmRecordZoneName(one_record, zone_name);
	
	return 0;
}

void Alarm24hRecordConfirm(void)
{
	int i;
	cJSON *record;
	cJSON *status;
	cJSON *type;
	for(i=0;i<cJSON_GetArraySize(AlarmingRecord_UnconfirmArray);i++)
	{
		record = cJSON_GetArrayItem(AlarmingRecord_UnconfirmArray,i);
		record=(cJSON *)record->valueint;
		status = cJSON_GetObjectItemCaseSensitive(record,"STATUS");
		type = cJSON_GetObjectItemCaseSensitive(record,"TYPE");
		if(strcmp(type->valuestring,"24H_Alarming")==0&&strcmp(status->valuestring,"Unconfirm")==0)
		{
			cJSON_DeleteItemFromObjectCaseSensitive(record,"STATUS");
			cJSON_AddStringToObject(record,"STATUS","Confirm");
			cJSON_DeleteItemFromArray(AlarmingRecord_UnconfirmArray,i);
			i--;
		}
	}
	SaveAlarmRecordToFile();
}
int CheckLastArmed(void)
{
	if(AlarmingRecord_Cur!=NULL)
		return 1;
	return 0;
}
int GetLastArmedZoneMark(void)
{
	int rev;
	ParseJsonNumber(AlarmingRecord_Cur,"INCLUDE_ZONE",&rev);
	return rev;
}
int GetLastArmedBypassZoneMark(void)
{
	int rev;
	ParseJsonNumber(AlarmingRecord_Cur,"BYPASS_ZONE",&rev);
	return rev;
}
int GetLastArmedMode(void)
{
	int rev;
	ParseJsonNumber(AlarmingRecord_Cur,"ARMED_MODE",&rev);
	return rev;
}

#if 1
int GetCurArmInfoDisp(char *disp)
{
	//AlarmingRecord_Cur

	int i;
	//char disp_temp[101];
	if(AlarmingRecord_Cur==NULL)
	{
		strcpy(disp,"Only 24H zone armed");
		return 0;
	}
	#if 0
	const char *mode_disp[]=
	{
		"Away",
		"Stay",
		"Sleep",
		"Customized"
	};
	#endif
	int happen_time; 
	ParseJsonNumber(AlarmingRecord_Cur,"TIME",&happen_time);
	int armed_mode=GetLastArmedMode();
	int bypass_mark=GetLastArmedBypassZoneMark();
	
	struct tm *tblock; 	
	
	tblock=localtime(&happen_time);

	//snprintf(disp,100, "%02d/%02d %02d:%02d \"%s\" Armed",tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,mode_disp[armed_mode]);
	snprintf(disp,100, "%02d/%02d %02d:%02d Armed",tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);

	if(bypass_mark>0)
	{
		char bypass_zone[100] = {0};
		char bypass_name[40];
		strcpy(bypass_zone,",Bypass:");
		for(i=0;i<8;i++)
		{
			if(bypass_mark&(0x01<<i))
			{
				Get_AlarmingZoneName(i,bypass_name);
				strcat(bypass_zone,bypass_name);	
				strcat(bypass_zone," ");
			}
		}
		strcat(disp,bypass_zone);
	}	
	return 0;
}
#endif

void AlamingRecordInformService(char *json_string)
{
	int i;
	char gl_num[11];
	GetIpRspData data;
	int len;
	len = strlen(json_string)+1;
	for(i=0;i<GetAlarmInformParaGlNums();i++)
	{
		GetAlarmInformParaGlItems(i,gl_num);
		if(API_GetIpNumberFromNet(gl_num, NULL, NULL, 2, 1, &data) != 0)
			continue;
		if(api_udp_c5_ipc_send_data(data.Ip[0],CMD_ALARMING_RECORD,json_string,len) != 0)
		{
			;
		}
	}
}
void Alaming24HRecordInformService(char *json_string)
{
	if(GetAlarmingState()&&GetAlarmInformParaGlNums()>0)
	{
		AlamingRecordInformService(json_string);
	}
	else
	{
		//char input_buff[11];
		//GetIpRspData data;
		int i;
		int len;
		extern SearchIpRspData searchOnlineListData;
		
		len = strlen(json_string)+1;
		//strcpy(input_buff,"00000000");
		
		if(API_SearchIpByFilter("0000", TYPE_GL, 2, 0, &searchOnlineListData) == 0)
		{
			for(i=0;i<searchOnlineListData.deviceCnt;i++)
			{
				if(api_udp_c5_ipc_send_data(searchOnlineListData.data[i].Ip,CMD_ALARMING_RECORD,json_string,len) != 0)
				{
					;
				}
			}
		}
		//memcpy(input_buff,GetSysVerInfo_bd(),4);
		if(API_SearchIpByFilter(GetSysVerInfo_bd(), TYPE_GL, 2, 0, &searchOnlineListData)  == 0)
		{
			for(i=0;i<searchOnlineListData.deviceCnt;i++)
			{
				if(api_udp_c5_ipc_send_data(searchOnlineListData.data[i].Ip,CMD_ALARMING_RECORD,json_string,len) != 0)
				{
					;
				}
			}
		}

	}
}

