/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_AlarmingSurvey_H
#define _obj_AlarmingSurvey_H
typedef struct
{
	VDP_MSG_HEAD	head;
	union
	{
		int				zone_index;
		int				Arming_mode;
	};
	char				buff[4];
}AlarmingMsg_T;

typedef enum
{
	AlarmingMsg_Arming_NormalZone=0,
	AlarmingMsg_Arming_24hZone,
	AlarmingMsg_Disarming_NormalZone,
	AlarmingMsg_Disarming_24hZone,
	AlarmingMsg_Disarming_AllZone,
	AlarmingMsg_ParaReload,
	AlarmingMsg_SenserReload,
	AlarmingMsg_SenserEnvent,
	AlarmingMsg_DelayTimeout,
	AlarmingMsg_24hZoneAlarmingConfirm,
	AlarmingMsg_IconSosPress,
	AlarmingMsg_IconSosRelease,
}AlarmingMsg_E;

typedef struct
{
	int state;
	int ArmMode;
}AlarmingSurveyRun_t;
typedef enum
{
	AlarmingSurvey_State_Disarmed,
	AlarmingSurvey_State_Armed,
	AlarmingSurvey_State_24hAlarming,
}AlarmingSurvey_State_E;
#endif


