#ifndef _obj_AlarmingZone_H
#define _obj_AlarmingZone_H

#define Max_AlarmingZone		4

typedef enum
{
	AlarmingZone_State_Disarmed = 0,
	AlarmingZone_State_ArmedDelay,
	AlarmingZone_State_Armed,
	AlarmingZone_State_DisarmedDelay,
	AlarmingZone_State_Alarming,
	AlarmingZone_State_Bypass,
}AlarmingZone_State_E;

typedef enum
{
	AlarmingZone_Type_Unused=0,
	AlarmingZone_Type_24H,
	AlarmingZone_Type_Immediately,
	AlarmingZone_Type_Delay,
	AlarmingZone_Type_SosIcon,
}AlarmingZone_Type_E;

typedef enum
{
	AlarmingSenser_Type_NO=0,
	AlarmingSenser_Type_NC,
}AlarmingSenser_Type_E;

typedef enum
{
	AlarmingSenser_State_Invalid=0,
	AlarmingSenser_State_Valid,
}AlarmingSenser_State_E;

typedef struct
{
	int state;
	int type;
	//int binding_senser;
	int senser_type;
	int senser_val;
	int senser_state;
	int armed_delay;
	int disarmed_delay;
	int timer;
}AlarmingZone_T;

extern AlarmingZone_T AlarmingZone[];

#endif