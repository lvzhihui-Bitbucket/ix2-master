#ifndef _obj_AlarmingPara_H
#define _obj_AlarmingPara_H

void Enter_AlarmingParaSettingRootMenu(int select);
void Enter_AlarmingParaSettingZoneArrayMenu(int select,int cur_node_index);
void Enter_AlarmingParaSettingZoneMenu(int select,int cur_node_index,cJSON *one_zone);
void Enter_AlarmingParaSettingArmModeArrayMenu(int select,int cur_node_index);
void Enter_AlarmingParaSettingArmModeMenu(int select,int cur_node_index,cJSON *one_mode);

#endif