  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"	
#include "vdp_uart.h" 
#include"obj_AlarmingServey.h"
#include"obj_AlarmingHd.h"

static int sync_read_senser_flag = 0;

int AlarmingSenserVal_Read(int mask)
{	
	#ifdef PID_IXSE
	return get_ixse_alarm_fd();
	#else
	vdp_task_t*  ptask;
	ptask = GetTaskAccordingMsgID(GetMsgIDAccordingPid(pthread_self()));
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	printf("111111%s:%d,%d\n",__func__,__LINE__);
	sync_read_senser_flag = 1;
	api_uart_send_pack(UART_TYPE_READ_LINE, NULL, 0);	
	AlarmingMsg_T rev_msg;
	int rev_len = sizeof(AlarmingMsg_T);
	if(WaitForBusinessACK(ptask ->p_syc_buf,AlarmingMsg_SenserEnvent,(char*)&rev_msg,&rev_len,1000) == 1)
	{
		sync_read_senser_flag = 0;
		if(rev_len >= sizeof(AlarmingMsg_T))
		{	
			printf("111111%s:%d,%d\n",__func__,__LINE__,rev_msg.buff[0]);
			return rev_msg.buff[0];
		}
	}
	printf("111111%s:%d\n",__func__,__LINE__);
	sync_read_senser_flag = 0;
	return -1;
	#endif
}

extern Loop_vdp_common_buffer  	vdp_AlarmingSurvey_mesg_queue;
extern Loop_vdp_common_buffer  	vdp_AlarmingSurvey_mesg_sync_queue;

void  RevAlarmingSenser_Event(int rev_val)
{
	AlarmingMsg_T	send_msg;	
	
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_SenserEnvent;
	//��֯������Ϣ��BeCalled
	send_msg.head.msg_source_id = 0;
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	send_msg.buff[0] = rev_val;
	
	
	send_len = sizeof(AlarmingMsg_T);
	printf("111111%s:%d,%d\n",__func__,__LINE__,sync_read_senser_flag);
	if(sync_read_senser_flag)
	{
		printf("111111%s:%d\n",__func__,__LINE__);
		send_msg.head.msg_type|=0x80;
		push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_sync_queue, (char *)&send_msg, send_len);
	}
	else
	{
		push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len);
	}
}

