#ifndef _obj_AlarmingHd_H
#define _obj_AlarmingHd_H
#define AlarmingAllSenser_Mask				0
#define AlarmingSenser1_Mask				0x01
#define AlarmingSenser2_Mask				0x02
#define AlarmingSenser3_Mask				0x04
#define AlarmingSenser4_Mask				0x08
#define AlarmingSenser5_Mask				0x10
#define AlarmingSenser6_Mask				0x20
#define AlarmingSenser7_Mask				0x40
#define AlarmingSenser8_Mask				0x80
#endif