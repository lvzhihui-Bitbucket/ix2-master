#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include "task_survey.h"
#include "cJSON.h"
#include "task_IxProxy.h"
#include"obj_AlarmingZone.h"

extern const char*  AlarmingZoneTypeDisp[];
extern const char*  AlarmingZoneSenserTypeDisp[];
const char*  AlarmingZoneSenserStateDisp[]=
{
	"Valid",
	"Invalid",
};

const char*  AlarmingZoneStateDisp[]=
{
	"Disarmed",
	"ArmedDelay",
	"Armed",
	"DisarmedDelay",
	"Alarming",
	"Bypass",
};

#define AlarmRm_Area		"Area"
#define AlarmRm_Opt		"Operation"
	#define AlarmRm_Opt_Arm		"ARM"
	#define AlarmRm_Opt_Disarm	"DISARM"
	#define AlarmRm_Opt_GetState	"GetState"
#define AlarmRm_Section		"Section"
#define AlarmRm_Value		"Value"
#define AlarmRm_Result			"Result"
#define AlarmRm_Dat				"Data"
	#define AlarmRm_AlState			"AlarmState"	
	#define AlarmRm_ZoneInfo		"ZoneInfo"
		#define AlarmRm_ZoneIndex		"Index"
		#define AlarmRm_ZoneName		"Name"
		#define AlarmRm_SenserState		"SenserState"
		#define AlarmRm_ZoneType			"Type"
		#define AlarmRm_ZoneState		"State"
	#define AlarmRm_ArmModeInfo				"ArmModeInfo"
		#define AlarmRm_ArmModeIndex		"Index"
		#define AlarmRm_ArmModeMask			"ZoneMask"


int AlarmingRemoteGetState_Process(cJSON *rsp_json);
int AlarmingRemoteDisarm_Process(cJSON *rsp_json);	
int AlarmingRemoteArm_Process(cJSON *rsp_json);

int AlarmingRemoteOpt_Process(char *cmd_buff)
{
	cJSON *rsp_json;
	cJSON *rsp_node;
	cJSON *remote_opt;
	cJSON *one_opt;
	int opt_nums;
	int i;
	char opt_code[100];
	remote_opt = cJSON_Parse(cmd_buff);
	if(remote_opt==NULL)
		return -1;
	rsp_json= cJSON_CreateArray();
	opt_nums= cJSON_GetArraySize(remote_opt);
	for(i=0;i<opt_nums;i++)
	{
		printf("1111111AlarmingRemoteOpt_Process:%d\n",i);
		one_opt= cJSON_GetArrayItem(remote_opt,i);
		ParseJsonString(one_opt, AlarmRm_Opt, opt_code, 29);
		printf("22222222AlarmingRemoteOpt_Process:%s\n",opt_code);
		if(strcmp(opt_code,AlarmRm_Opt_Arm)==0)
		{
			rsp_node = cJSON_Duplicate(one_opt,1);
			AlarmingRemoteArm_Process(rsp_node);
			cJSON_AddItemToArray(rsp_json,rsp_node);
		}
		else if(strcmp(opt_code,AlarmRm_Opt_Disarm)==0)
		{
			rsp_node = cJSON_Duplicate(one_opt,1);
			AlarmingRemoteDisarm_Process(rsp_node);
			cJSON_AddItemToArray(rsp_json,rsp_node);
		}
		else if(strcmp(opt_code,AlarmRm_Opt_GetState)==0)
		{
			
			rsp_node = cJSON_Duplicate(one_opt,1);
			AlarmingRemoteGetState_Process(rsp_node);
			cJSON_AddItemToArray(rsp_json,rsp_node);
		}
	}
	cJSON_Delete(remote_opt);
	char *rsp_cmd;
	rsp_cmd = cJSON_Print(rsp_json);
	cJSON_Delete(rsp_json);
	printf("AlarmingRemoteOpt_Process:\n%s\n",rsp_cmd);
	free(rsp_cmd);
}

int AlarmingRemoteDisarm_Process(cJSON *rsp_json)
{
	#if 0
	if(GetAlarmingState()==AlarmingSurvey_State_24hAlarming)
	{
		API_24hZoneAlarming_Confirm();
	}
	else
	#endif
	{
		API_Alarming_Disarming_NormalZone();
	}

	cJSON_AddStringToObject(rsp_json,AlarmRm_Result,RESULT_SUCC);

	return 0;
}

int AlarmingRemoteArm_Process(cJSON *rsp_json)
{
	cJSON *dat;
	cJSON *val_node;
	int ret = 0;
	char buff[100];
	int mode_index;
	dat = cJSON_GetObjectItemCaseSensitive(rsp_json, AlarmRm_Dat);
	if(dat!=NULL)
	{
		ParseJsonString(dat, AlarmRm_Section, buff, 99);
		if(strcmp(buff,AlarmRm_ArmModeInfo)==0)
		{
			val_node = cJSON_GetObjectItemCaseSensitive(rsp_json, AlarmRm_Value);
			ParseJsonNumber(val_node, AlarmRm_ArmModeIndex,&mode_index);
			if(API_Alarming_Arming_NormalZone(mode_index)==0)
			{
				ret = 1;
			}
		}
	}
	if(ret ==1)
		cJSON_AddStringToObject(rsp_json,AlarmRm_Result,RESULT_SUCC);
	else
		cJSON_AddStringToObject(rsp_json,AlarmRm_Result,RESULT_ERR01);

	return 0;
}

int AlarmingRemoteGetState_Process(cJSON *rsp_json)
{
	char buff[100];
	cJSON *rsp_data;
	cJSON *AlState,*ZoneInfo,*ArmModeInfo;
	cJSON *val_node,*val_sub_node;
	int i;
	
	rsp_data = cJSON_CreateArray();
	AlState = cJSON_CreateObject();
	ZoneInfo = cJSON_CreateObject();
	ArmModeInfo = cJSON_CreateObject();

	cJSON_AddStringToObject(rsp_json,AlarmRm_Result,RESULT_SUCC);
	cJSON_AddItemToObject(rsp_json,AlarmRm_Dat,rsp_data);
	cJSON_AddItemToArray(rsp_data,AlState);
	cJSON_AddItemToArray(rsp_data,ZoneInfo);
	cJSON_AddItemToArray(rsp_data,ArmModeInfo);

	cJSON_AddStringToObject(AlState,AlarmRm_Section,AlarmRm_AlState);
	val_node = cJSON_CreateObject();
	cJSON_AddItemToObject(AlState,AlarmRm_Value,val_node);
	if(GetAlarmingState()==0)
	{
		strcpy(buff,"Disarmed");
	}
	else
	{
		strcpy(buff,"Armed");
	}
	cJSON_AddStringToObject(val_node,AlarmRm_AlState,buff);

	
	cJSON_AddStringToObject(ZoneInfo,AlarmRm_Section,AlarmRm_ZoneInfo);
	val_node = cJSON_CreateArray();
	cJSON_AddItemToObject(ZoneInfo,AlarmRm_Value,val_node);
	for(i=0;i<4;i++)
	{
		val_sub_node = cJSON_CreateObject();
		cJSON_AddItemToArray(val_node,val_sub_node);
		cJSON_AddNumberToObject(val_sub_node,AlarmRm_ZoneIndex,i);
		Get_AlarmingZoneName(i,buff);
		cJSON_AddStringToObject(val_sub_node,AlarmRm_ZoneName,buff);
		cJSON_AddStringToObject(val_sub_node,AlarmRm_ZoneType,AlarmingZoneTypeDisp[Get_AlarmingPara_ZoneType(i)]);
		cJSON_AddStringToObject(val_sub_node,AlarmRm_ZoneState,AlarmingZoneStateDisp[AlarmingZone[i].state]);
		cJSON_AddStringToObject(val_sub_node,AlarmRm_SenserState,AlarmingZoneSenserStateDisp[AlarmingZone[i].senser_state]);
	}

	cJSON_AddStringToObject(ArmModeInfo,AlarmRm_Section,AlarmRm_ArmModeInfo);
	val_node = cJSON_CreateArray();
	cJSON_AddItemToObject(ArmModeInfo,AlarmRm_Value,val_node);
	
	for(i=0;i<4;i++)
	{
		val_sub_node = cJSON_CreateObject();
		cJSON_AddItemToArray(val_node,val_sub_node);
		cJSON_AddNumberToObject(val_sub_node,AlarmRm_ArmModeIndex,i);
		cJSON_AddNumberToObject(val_sub_node,AlarmRm_ArmModeMask,Get_AlarmingZone_ByArmedMode(i));
	}

	return 0;
}

void AlarmingRemoteTest(void)
{
	cJSON *test;
	cJSON *one_opt;
	cJSON *dat;
	cJSON *val;
	char *print_test;
	#if 0
	test = cJSON_CreateArray();
	one_opt = cJSON_CreateObject();
	cJSON_AddItemToArray(test,one_opt);
	cJSON_AddStringToObject(one_opt,AlarmRm_Area,"Alarm");
	cJSON_AddStringToObject(one_opt,AlarmRm_Opt,AlarmRm_Opt_GetState);
	#endif
	test = cJSON_CreateArray();
	one_opt = cJSON_CreateObject();
	cJSON_AddItemToArray(test,one_opt);
	cJSON_AddStringToObject(one_opt,AlarmRm_Area,"Alarm");
	cJSON_AddStringToObject(one_opt,AlarmRm_Opt,AlarmRm_Opt_Arm);
	dat = cJSON_AddObjectToObject(one_opt,AlarmRm_Dat);
	cJSON_AddStringToObject(dat,AlarmRm_Section,AlarmRm_ArmModeInfo);
	val = cJSON_AddObjectToObject(dat,AlarmRm_Value);
	cJSON_AddNumberToObject(val,AlarmRm_ArmModeIndex,0);
	
	print_test = cJSON_Print(test);
	cJSON_Delete(test);
	printf("AlarmingRemoteTest:\n%s\n",print_test);
	AlarmingRemoteOpt_Process(print_test);
	free(print_test);
}