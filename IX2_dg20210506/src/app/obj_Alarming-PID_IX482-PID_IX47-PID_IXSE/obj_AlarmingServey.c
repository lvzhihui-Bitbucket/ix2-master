/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"
#include "task_VideoMenu.h"
#include "task_Led.h"
#include"obj_AlarmingZone.h"
#include"obj_AlarmingServey.h"



//obj_AlarmingSurvey_c


OS_TIMER	AlarmingSurveyTimer;

Loop_vdp_common_buffer  	vdp_AlarmingSurvey_mesg_queue;
Loop_vdp_common_buffer  	vdp_AlarmingSurvey_mesg_sync_queue;


vdp_task_t			  	task_AlarmingSurvey;
void AlarmingSurvey_Process(char* msg_data, int len);
void* vdp_AlarmingSurvey_task( void* arg );
void AlarmingSurveyCallback(void);
void AlarmingSosPress_Process(void);
void AlarmingSosRelease_Process(void);
void SenserEvent_Deal(int senser_val);
void AlarmingZone_DelayTimeout(int zone_index);
void Alarming_Arming_NormalZone(int Arming_mode);
void Alarming_Arming_24hZone(void);
void Alarming_Disarming_NormalZone(void);
void Alarming_Disarming_24hZone(void);
void AlarmingSurvey_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);
void Alarming24hConfirm_Process(void);
void LastArmedRecover(void);

AlarmingSurveyRun_t AlarmingSurveyRun;
int armedTotalDelay = 0;
int DisarmedTotalDelay = 0;
		  
void vtk_TaskInit_AlarmingSurvey(int priority)
{
	init_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, 200, (msg_process)AlarmingSurvey_Process, &task_AlarmingSurvey);
	init_vdp_common_queue(&vdp_AlarmingSurvey_mesg_sync_queue, 200, NULL, 								  		&task_AlarmingSurvey);
	init_vdp_common_task(&task_AlarmingSurvey, MSG_ID_AlarmingSurvey, vdp_AlarmingSurvey_task, &vdp_AlarmingSurvey_mesg_queue, &vdp_AlarmingSurvey_mesg_sync_queue);
	
	//DHCP_printf("vdp_DHCP_task starting............\n");
}

void* vdp_AlarmingSurvey_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	
	sleep(5);
	AlarmingSurveyRun.state=AlarmingSurvey_State_Disarmed;
	printf("111111%s:%d\n",__func__,__LINE__);
	AlarmingRecord_Init();
	printf("111111%s:%d\n",__func__,__LINE__);
	AlarmingParaReload();
	printf("111111%s:%d\n",__func__,__LINE__);
	AlarmingZone_Init();
	printf("111111%s:%d\n",__func__,__LINE__);
	OS_CreateTimer(&AlarmingSurveyTimer,AlarmingSurveyCallback,1000/25);
	Alarming_Arming_24hZone();
	printf("111111%s:%d\n",__func__,__LINE__);
	LastArmedRecover();
	printf("111111%s:%d\n",__func__,__LINE__);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
	
}

void AlarmingSurvey_Process(char* msg_data,int len)
{
	AlarmingMsg_T *pmsg = (AlarmingMsg_T*)msg_data;
	int temp;
	printf("!!!zone1 state=%d,senser_state=%d,val=%d,msg=%d\n",AlarmingZone[0].state,AlarmingZone[0].senser_state,AlarmingZone[0].senser_val,pmsg->head.msg_type);
	switch(pmsg->head.msg_type)
	{
		case AlarmingMsg_Arming_NormalZone:
			Alarming_Arming_NormalZone(pmsg->Arming_mode);
			AlarmingSurvey_Respones(pmsg->head.msg_source_id,pmsg->head.msg_type,0);
			break;
		case AlarmingMsg_Arming_24hZone:
			Alarming_Arming_24hZone();
			break;
		case AlarmingMsg_Disarming_NormalZone:
			Alarming_Disarming_NormalZone();
			break;
		case AlarmingMsg_Disarming_24hZone:
			Alarming_Disarming_24hZone();
			break;
		case AlarmingMsg_ParaReload:
			Alarming_Disarming_24hZone();
			AlarmingZonePara_Reload();
			if(Get_AlarmingPara_Switch()==1)
			{
				Alarming_Arming_24hZone();
			}
			else
			{
				Alarming_Disarming_NormalZone();
			}
			break;
		case AlarmingMsg_SenserReload:
			temp=AlarmingSenserState_Reload();
			AlarmingSurvey_Respones(pmsg->head.msg_source_id,pmsg->head.msg_type,(temp<0)?0xff:temp);
			break;
		case AlarmingMsg_SenserEnvent:
			//if(pmsg->buff[0])
			printf("111111%s:%d,%d\n",__func__,__LINE__,pmsg->buff[0]);
			if(Get_AlarmingPara_Switch()==1)
			{
				SenserEvent_Deal(pmsg->buff[0]);
			}
			break;
		case AlarmingMsg_DelayTimeout:
			AlarmingZone_DelayTimeout(pmsg->zone_index);
			break;
		case AlarmingMsg_24hZoneAlarmingConfirm:
			Alarming24hConfirm_Process();
			break;
		case AlarmingMsg_IconSosPress:
			AlarmingSosPress_Process();
			break;
		case AlarmingMsg_IconSosRelease:
			AlarmingSosRelease_Process();
			break;	
	}
}

void AlarmingSurveyCallback(void)
{
	int i;
	int retrig_flag=0;
	AlarmingMsg_T send_msg;
	
	
	if(armedTotalDelay>0)
	{
		armedTotalDelay--;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_Armed_Delay, (char*)&armedTotalDelay, sizeof(armedTotalDelay));
	}
	
	if(DisarmedTotalDelay>0)
	{
		DisarmedTotalDelay--;
		API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_Disarmed_Delay, (char*)&DisarmedTotalDelay, sizeof(DisarmedTotalDelay));
	}
	
	for(i = 0;i<Max_AlarmingZone;i++)
	{
		if(AlarmingZone[i].state==AlarmingZone_State_ArmedDelay)
		{
			if(++AlarmingZone[i].timer>=AlarmingZone[i].armed_delay)
			{
				//组织发送消息给BeCalled
				send_msg.head.msg_source_id = 0;
				send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
				send_msg.head.msg_type = AlarmingMsg_DelayTimeout;
				send_msg.head.msg_sub_type = 0;
				send_msg.zone_index = i;
				
				if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, sizeof(AlarmingMsg_T)) != 0)
				{
					retrig_flag = 1;
				}
			}
			else
				retrig_flag = 1;
		}
		else if(AlarmingZone[i].state==AlarmingZone_State_DisarmedDelay)
		{
			if(++AlarmingZone[i].timer>=AlarmingZone[i].disarmed_delay)
			{
				//组织发送消息给BeCalled
				send_msg.head.msg_source_id = 0;
				send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
				send_msg.head.msg_type = AlarmingMsg_DelayTimeout;
				send_msg.head.msg_sub_type = 0;
				send_msg.zone_index = i;
				
				if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, sizeof(AlarmingMsg_T)) != 0)
				{
					retrig_flag = 1;
				}
			}
			else
			{
				
				retrig_flag = 1;
			}
		}
		
	}
	
	if(retrig_flag)
	{
		OS_RetriggerTimer(&AlarmingSurveyTimer);
	}
}

void SenserEvent_Deal(int senser_val)
{
	int i;
	int senser_state = 0;
	for(i = 0;i<Max_AlarmingZone;i++)
	{
		
		AlarmingZone[i].senser_val = (senser_val&(0x01<<i))? 1:0;
		printf("111111%s:%d,%d:%d\n",__func__,__LINE__,i,AlarmingZone[i].senser_val);
		
		AlarmingZone[i].senser_state = AlarmingSenserState_Update(AlarmingZone[i].senser_type,AlarmingZone[i].senser_val);
		if(AlarmingZone[i].senser_state == AlarmingSenser_State_Valid)
			senser_state |= (0x01<<i);
		if(AlarmingZone[i].senser_state == AlarmingSenser_State_Valid&&(AlarmingZone[i].state == AlarmingZone_State_Armed||AlarmingZone[i].state ==AlarmingZone_State_Alarming))
		{
			if(AlarmingZone[i].type==AlarmingZone_Type_Delay&&AlarmingZone[i].disarmed_delay>0)
			{
				AlarmingZone[i].state = AlarmingZone_State_DisarmedDelay;
				AlarmingZone[i].timer = 0;
				OS_RetriggerTimer(&AlarmingSurveyTimer);
				API_LedDisplay_DisarmDelay();

				if(DisarmedTotalDelay==0||DisarmedTotalDelay>AlarmingZone[i].disarmed_delay)
				{
					DisarmedTotalDelay = AlarmingZone[i].disarmed_delay;
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_Disarmed_Delay, (char*)&DisarmedTotalDelay, sizeof(DisarmedTotalDelay));
				}
			}
			else
			{
				if(AlarmingZone[i].type==AlarmingZone_Type_24H&&AlarmingSurveyRun.state == AlarmingSurvey_State_Disarmed)
				{
					AlarmingSurveyRun.state=AlarmingSurvey_State_24hAlarming;
				}
				AlarmingZone[i].state = AlarmingZone_State_Alarming;
				AlarmingZone_Valid(i);
			}
		}
		
	}
	
	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_AlarmingSenserUpdate, (char*)&senser_state, sizeof(senser_state));	
}
void AlarmingZone_DelayTimeout(int zone_index)
{
	if(AlarmingZone[zone_index].state == AlarmingZone_State_DisarmedDelay)
	{
		AlarmingZone[zone_index].state = AlarmingZone_State_Alarming;
		AlarmingZone_Valid(zone_index);
	}
	else if(AlarmingZone[zone_index].state == AlarmingZone_State_ArmedDelay)
	{
		AlarmingZone[zone_index].state = AlarmingZone_State_Armed;

		if(armedTotalDelay<=0)
		{
			API_LedDisplay_Armed();
		}
	}
	
		
}
void Alarming_Arming_NormalZone(int Arming_mode)
{
	int AlarmingZoneMask = 0;
	int BypassZoneMask = 0;
	int i;
	
	AlarmingZoneMask=Get_AlarmingZone_ByArmedMode(Arming_mode);
	armedTotalDelay = 0;
	DisarmedTotalDelay = 0;
	if(AlarmingZoneMask>0)
	{
		for(i = 0;i < Max_AlarmingZone;i++)
		{
			if(AlarmingZoneMask&(0x01<<i))
			{
				if(AlarmingZone[i].senser_state == AlarmingSenser_State_Valid)
				{
					AlarmingZone[i].state = AlarmingZone_State_Bypass;
					AlarmingZone[i].timer = 0;
					BypassZoneMask|=(0x01<<i);
					continue;
				}
				if(AlarmingZone[i].type==AlarmingZone_Type_Delay)
				{
					AlarmingZone[i].state = AlarmingZone_State_ArmedDelay;
					AlarmingZone[i].timer = 0;
					if(AlarmingZone[i].armed_delay>armedTotalDelay)
					{
						armedTotalDelay = AlarmingZone[i].armed_delay;
					}
				}
				if(AlarmingZone[i].type==AlarmingZone_Type_Immediately)
				{
					AlarmingZone[i].state = AlarmingZone_State_Armed;
					AlarmingZone[i].timer = 0;
				}
			}
		}
		if(AlarmingZoneMask==BypassZoneMask)
			return;
		InitCurArmedRecord(Arming_mode,AlarmingZoneMask,BypassZoneMask);
		if(armedTotalDelay>0)
		{
			API_LedDisplay_ArmDelay();
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_Armed_Delay, (char*)&armedTotalDelay, sizeof(armedTotalDelay));
			OS_RetriggerTimer(&AlarmingSurveyTimer);
		}
		else
		{
			API_LedDisplay_Armed();
		}
		AlarmingSurveyRun.state=AlarmingSurvey_State_Armed;
		AlarmingSurveyRun.ArmMode = Arming_mode;
		//MoveAllCurAlarmRecordToSave();
	}
}

void LastArmedRecover(void)
{
	if(CheckLastArmed()==0)
		return;
	printf("111111%s:%d\n",__func__,__LINE__);
	int zone_mask=GetLastArmedZoneMark();
	int bypass_zone_mask = GetLastArmedBypassZoneMark();
	int arming_mode = GetLastArmedMode();
	int i;
	int alarming_flag=0;
	if(zone_mask==0||zone_mask==bypass_zone_mask)
		return;
	printf("111111%s:%d\n",__func__,__LINE__);
	LastArmedRecordDispRecover();
	printf("111111%s:%d\n",__func__,__LINE__);
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		if(bypass_zone_mask&(0x01<<i))
			continue;
		if(zone_mask&(0x01<<i))
		{
			AlarmingZone[i].state = AlarmingZone_State_Armed;
			AlarmingZone[i].timer = 0;
			if(AlarmingZone[i].senser_state == AlarmingSenser_State_Valid)
			{
				AlarmingZone[i].state = AlarmingZone_State_Alarming;
				AlarmingZone_Valid(i);
				alarming_flag = 1;
			}
		}
	}
	printf("111111%s:%d\n",__func__,__LINE__);
	if(alarming_flag==0&&GetCurAlarmRecordNums()>0)
	{
		int zone_index,happen_time;
		alarming_flag = 1;
		API_LedDisplay_Alarming();
		if(GetCurAlarmRecordItem(GetCurAlarmRecordNums()-1,&zone_index,NULL,&happen_time)==0)
		{
			if(abs(happen_time-time(NULL))<60)
			{
				AlarmingZone_Valid(zone_index);
			}
		}
	}
	printf("111111%s:%d\n",__func__,__LINE__);	
	if(alarming_flag == 0)
		API_LedDisplay_Armed();
	
	AlarmingSurveyRun.state=AlarmingSurvey_State_Armed;
	AlarmingSurveyRun.ArmMode = arming_mode;
	printf("111111%s:%d\n",__func__,__LINE__);
}
void Alarming_Arming_24hZone(void)
{
	int i;
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		if(AlarmingZone[i].type==AlarmingZone_Type_24H)
		{
			AlarmingZone[i].state = AlarmingZone_State_Armed;
			AlarmingZone[i].timer = 0;
		}
	}
	
	OS_RetriggerTimer(&AlarmingSurveyTimer);
}

void Alarming_Disarming_NormalZone(void)
{
	int i;
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		if((AlarmingZone[i].state == AlarmingZone_State_Armed||
			AlarmingZone[i].state == AlarmingZone_State_ArmedDelay||
			AlarmingZone[i].state == AlarmingZone_State_DisarmedDelay||
			AlarmingZone[i].state == AlarmingZone_State_Alarming||
			AlarmingZone[i].state == AlarmingZone_State_Bypass)&&
			AlarmingZone[i].type!=AlarmingZone_Type_24H)
		{
			AlarmingZone[i].state = AlarmingZone_State_Disarmed;
			AlarmingZone[i].timer = 0;
			AlarmingZone_Invalid(i);
		}

		AlarmingSurveyRun.state=AlarmingSurvey_State_Disarmed;
	}
	API_LedDisplay_Disarmed();
	MoveAllCurAlarmRecordToSave();
	//OS_RetriggerTimer(&AlarmingSurveyTimer);
}

void Alarming_Disarming_24hZone(void)
{
	int i;
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		if(AlarmingZone[i].type==AlarmingZone_Type_24H)
		{
			AlarmingZone[i].state = AlarmingZone_State_Disarmed;
			AlarmingZone[i].timer = 0;
			AlarmingZone_Invalid(i);
		}
	}
	//OS_RetriggerTimer(&AlarmingSurveyTimer);
}

void Alarming24hConfirm_Process(void)
{
	int i;
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		if(AlarmingZone[i].type==AlarmingZone_Type_24H)
		{
			AlarmingZone[i].state = AlarmingZone_State_Armed;
			AlarmingZone[i].timer = 0;
			AlarmingZone_Invalid(i);
		}
	}
	if(AlarmingSurveyRun.state == AlarmingSurvey_State_24hAlarming)
	{
		API_LedDisplay_Disarmed();
		AlarmingSurveyRun.state=AlarmingSurvey_State_Disarmed;
	}
}

void AlarmingSosPress_Process(void)
{ 
	AlarmingSos_Valid(0); 
}

void AlarmingSosRelease_Process(void)
{ 
	AlarmingSos_Invalid(0); 
}
int API_Alarming_Arming_NormalZone(int Arming_mode)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_Arming_NormalZone;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	send_msg.Arming_mode = Arming_mode;
	
	
	
	
	ptask = GetTaskAccordingMsgID(send_msg.head.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	if(ptask ->p_syc_buf != NULL)
	{
		rev_len = 5;
		if(WaitForBusinessACK(ptask ->p_syc_buf,msg_type,rev,&rev_len,1000) == 1)
		{
			if(rev_len >= 5)
			{
				if(rev[4] != 0)
				{
					return -1;
				}
			}
		}
		else
			return -1;
	}
	
	return 0;
}

int API_Alarming_Arming_24hZone(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_Arming_24hZone;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

int API_Alarming_Disarming_NormalZone(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_Disarming_NormalZone;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

int API_Alarming_Disarming_24hZone(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_Disarming_24hZone;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}


int API_Alarming_SenserState_Update(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_SenserReload;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	

	
	ptask = GetTaskAccordingMsgID(send_msg.head.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	if(ptask ->p_syc_buf != NULL)
	{
		rev_len = 5;
		if(WaitForBusinessACK(ptask ->p_syc_buf,msg_type,rev,&rev_len,1000) == 1)
		{
			if(rev_len >= 5)
			{
				if(rev[4] == 0xff)
				{
					return -1;
				}
				else
					return rev[4];
				
			}
		}
		else
			return -1;
	}
	
	return 0;
}

int API_Alarming_Para_Update(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_ParaReload;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

int API_24hZoneAlarming_Confirm(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_24hZoneAlarmingConfirm;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

int API_SosAlarming_Press(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_IconSosPress;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

int API_SosAlarming_Release(void)
{
	AlarmingMsg_T	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int send_len;
	//OS_TASK_EVENT MyEvents;	
	int msg_type = AlarmingMsg_IconSosRelease;
	//组织发送消息给BeCalled
	send_msg.head.msg_source_id = GetMsgIDAccordingPid(pthread_self());
	send_msg.head.msg_target_id = MSG_ID_AlarmingSurvey;
	send_msg.head.msg_type = msg_type;
	
	
	send_len = sizeof(AlarmingMsg_T);
	if(push_vdp_common_queue(&vdp_AlarmingSurvey_mesg_queue, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	
	return 0;
}

void AlarmingSurvey_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}

int GetAlarmingState(void)
{
	return AlarmingSurveyRun.state;
}

int GetAlarmingArmMode(void)
{
	return AlarmingSurveyRun.ArmMode;
}
///////////////////////////////////////

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

