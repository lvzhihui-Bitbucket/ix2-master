  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include "task_survey.h"
#include"obj_AlarmingZone.h"
#include"obj_AlarmingAction.h"

#include "task_Beeper.h"
#include "task_Ring.h"
#include "task_Led.h"
#include "task_VideoMenu.h"

int AlarmingAction_ZoneMask=0;

void AlarmingZone_Valid(int index_zone)
{
	char zone_name[41];
	int happen_time;
	char disp[100];

	AlarmingAction_ZoneMask |= (1<<index_zone);
	//BEEP_T6();
	API_RingPlay(RING_ALARM);
	Get_AlarmingZoneName(index_zone,zone_name);
	//if(Get_AlarmingPara_ZoneType(index_zone)== AlarmingZone_Type_24H)
	if(AlarmingZone[index_zone].type==AlarmingZone_Type_24H)
	{
		AddOne24hAlarmRecord(index_zone,zone_name);
	}
	else
	{
		AddOneCurAlarmRecord(index_zone,zone_name);
	}
	GetCurAlarmRecordItem(GetCurAlarmRecordNums()-1,NULL,zone_name,&happen_time);
	API_LedDisplay_Alarming();

	struct tm *tblock; 	
	
	tblock=localtime(&happen_time);

	snprintf(disp,100, "%02d/%02d %02d:%02d %s happen a alarming",tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,zone_name);

	API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_AlarmingHappen, (char*)disp, strlen(disp)+1);
}

void AlarmingZone_Invalid(int index_zone)
{
	AlarmingAction_ZoneMask &= (~(1<<index_zone));
	if(AlarmingAction_ZoneMask==0)
	{
		API_RingStop();
		API_LedDisplay_AlarmingCancel();
	}
}



int GetLastAlarmRecordDisp(int last_index,char *disp)
{
	char zone_name[41];
	int happen_time;
	if((GetCurAlarmRecordNums()-last_index)<=0)
		return -1;
	GetCurAlarmRecordItem(GetCurAlarmRecordNums()-1-last_index,NULL,zone_name,&happen_time);

	struct tm *tblock; 	
	
	tblock=localtime(&happen_time);

	snprintf(disp,100, "%02d/%02d %02d:%02d %s happen a alarming",tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,zone_name);

	return 0;
}

int AlarmingSos_State=0;
int Get_AlarmingSOS_State(void)
{
	return AlarmingSos_State;
}

void Set_AlarmingSOS_State(int new_state)
{
	AlarmingSos_State = new_state;
}

void AlarmingSos_Valid(int index_zone)
{
	char zone_name[41];
	int happen_time;
	char disp[100];
	
	//BEEP_T6();
	
	//Get_AlarmingZoneName(index_zone,zone_name);
	//if(Get_AlarmingPara_ZoneType(index_zone)== AlarmingZone_Type_24H)
	
	AlarmingAction_ZoneMask |= (1<<(Max_AlarmingZone+index_zone));
	Set_AlarmingSOS_State(1);
	API_RingPlay(RING_ALARM);
	AddOneSosAlarmRecord(index_zone,"SOS");
	
	//GetCurAlarmRecordItem(GetCurAlarmRecordNums()-1,NULL,zone_name,&happen_time);
	API_LedDisplay_Alarming();

	//struct tm *tblock; 	
	
	//tblock=localtime(&happen_time);

	//snprintf(disp,100, "%02d/%02d %02d:%02d %s happen a alarming",tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,zone_name);

	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_IconSosChange);
}

void AlarmingSos_Invalid(int index_zone)
{
	AlarmingAction_ZoneMask &= (~(1<<(Max_AlarmingZone+index_zone)));

	if(AlarmingAction_ZoneMask==0)
	{
		API_RingStop();
		API_LedDisplay_AlarmingCancel();
	}
	Set_AlarmingSOS_State(0);
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_IconSosChange);
}
/////////////////////////

