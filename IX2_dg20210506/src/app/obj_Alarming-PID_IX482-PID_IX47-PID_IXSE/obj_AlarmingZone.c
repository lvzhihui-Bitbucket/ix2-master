#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"
#include"obj_AlarmingHd.h"
#include"obj_AlarmingZone.h"

AlarmingZone_T AlarmingZone[Max_AlarmingZone];

void Set_AlarmingZone_Type(int AlarmingZone_index,AlarmingZone_Type_E AlarmingZone_Type)
{
	if(AlarmingZone_index >=Max_AlarmingZone)
		return;
	
	AlarmingZone[AlarmingZone_index].type=AlarmingZone_Type;
}

void Set_AlarmingSenser_Type(int AlarmingZone_index,AlarmingSenser_Type_E AlarmingSenser_Type)
{
	if(AlarmingZone_index >=Max_AlarmingZone)
		return;
	
	AlarmingZone[AlarmingZone_index].senser_type=AlarmingSenser_Type;
}

void Set_AlarmingZone_State(int AlarmingZone_index,AlarmingZone_State_E AlarmingZone_State)
{
	if(AlarmingZone_index >=Max_AlarmingZone)
		return;
	AlarmingZone[AlarmingZone_index].senser_state = AlarmingZone_State;
}

void AlarmingZonePara_Reload(void)
{
	int i;
	for(i = 0;i < Max_AlarmingZone;i++)
	{
		AlarmingZone[i].type=Get_AlarmingPara_ZoneType(i);
		AlarmingZone[i].senser_type = Get_AlarmingPara_SenserType(i);
		
		if(AlarmingZone[i].type == AlarmingZone_Type_Delay)
		{
			AlarmingZone[i].armed_delay=Get_AlarmingPara_ArmedDelay(i);
			AlarmingZone[i].disarmed_delay=Get_AlarmingPara_DisarmedDelay(i);
		}
		else
		{
			AlarmingZone[i].armed_delay=0;
			AlarmingZone[i].disarmed_delay=0;
		}
		
	}
}


int AlarmingSenserState_Update(int senser_type,int senser_val)
{
	printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
	if(senser_type == AlarmingSenser_Type_NO)
	{
		printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
		if(senser_val == 1)
		{
			printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
			return AlarmingSenser_State_Invalid;
		}
		printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
		return AlarmingSenser_State_Valid;
	}
	else
	{
		printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
		if(senser_val == 0)
		{
			printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
			return AlarmingSenser_State_Invalid;
		}
		printf("111111%s:%d,%d:%d\n",__func__,__LINE__,senser_type,senser_val);
		return AlarmingSenser_State_Valid;
	}
}

int AlarmingSenserState_Reload(void)
{
	int i;
	int senser_val;
	int senser_state = 0;
	
	senser_val = AlarmingSenserVal_Read(AlarmingAllSenser_Mask);
	printf("111111%s:%d,%d\n",__func__,__LINE__,senser_val);
	if(senser_val<0)
		return -1;

	for(i = 0;i<Max_AlarmingZone;i++)
	{
		AlarmingZone[i].senser_val = (senser_val&(0x01<<i))? 1:0;

		AlarmingZone[i].senser_state = AlarmingSenserState_Update(AlarmingZone[i].senser_type,AlarmingZone[i].senser_val);
		if(AlarmingZone[i].senser_state)
			senser_state|=(0x01<<i);
	}
	printf("111111%s:%d,%d\n",__func__,__LINE__,senser_state);
	return senser_state;
}

void AlarmingZoneState_Init(void)
{
	int i;
	for(i = 0;i<Max_AlarmingZone;i++)
	{
		Set_AlarmingZone_State(i,AlarmingZone_State_Disarmed);
	}
}

void AlarmingZone_Init(void)
{
	AlarmingZoneState_Init();
	AlarmingZonePara_Reload();
	AlarmingSenserState_Reload();
}
