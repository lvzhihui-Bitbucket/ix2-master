  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "task_survey.h"


#include "cJSON.h"
#include "task_VideoMenu.h"
#include "MENU_152_JsonParaSettingPublic.h"		
#include "obj_UnifiedParameterMenu.h"
#include "obj_TableProcess.h"
#include"obj_AlarmingZone.h"
#include"obj_AlarmingPara.h"



cJSON *AlarmingParaRoot =NULL;
cJSON *AlarmingParaZoneArray =NULL;
cJSON *AlarmingParaArmModeArray =NULL;
const char*  AlarmingZoneTypeDisp[]=
{
	"Unused",
	"24H",
	"Immediately",
	"Delay"
};
const char*  AlarmingZoneSenserTypeDisp[]=
{
	"NO",
	"NC",
};
void CheckAlarmingParaZoneArray(cJSON *zone_array)
{
	int i;
	cJSON *one_node;
	char buff[100];
	if(cJSON_GetArraySize(zone_array)==0)
	{
		while(cJSON_GetArraySize(zone_array)>0)
		{
			cJSON_DeleteItemFromArray(zone_array,0);
		}
		for(i = 0;i<8;i++)
		{
			one_node = cJSON_CreateObject();
			cJSON_AddNumberToObject(one_node,"ID",i);
			sprintf(buff,"Zone%d",i+1);
			cJSON_AddStringToObject(one_node,"NAME",buff);
			cJSON_AddNumberToObject(one_node,"TYPE",AlarmingZone_Type_Delay);
			cJSON_AddNumberToObject(one_node,"SenserType",AlarmingSenser_Type_NO);
			cJSON_AddNumberToObject(one_node,"ArmDelay",15);
			cJSON_AddNumberToObject(one_node,"DisarmDelay",15);

			cJSON_AddItemToArray(zone_array,one_node);
		}
	}
}

void CheckAlarmingParaArmModeArray(cJSON *ArmMode)
{
	int i;
	char buff[50];
	cJSON *one_node;
	if(cJSON_GetArraySize(ArmMode)==0)
	{
		
		for(i = 0;i<4;i++)
		{
			one_node = cJSON_CreateObject();
			cJSON_AddNumberToObject(one_node,"ID",i);
			sprintf(buff,"MODE",i+1);
			cJSON_AddStringToObject(one_node,"NAME",buff);
			cJSON_AddNumberToObject(one_node,"ZoneMask",15);

			cJSON_AddItemToArray(ArmMode,one_node);
		}
	}

}

void AlarmingParaReload(void)
{
	FILE *file;
	char *json;
	cJSON *json_node;
	AlarmingParaRoot =NULL;
	if((file=fopen(AlarmingPara_FILE_NAME,"r"))!= NULL)
	{
	
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if ( (json = malloc(size)) == NULL )
		{
			fclose(file);
			return NULL;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		fclose(file);

		 AlarmingParaRoot = cJSON_Parse(json);
		 free(json);
		
	}
	
	 if(AlarmingParaRoot == NULL)
	 {
		AlarmingParaRoot = cJSON_CreateObject();
	 }
	 if(AlarmingParaRoot!=NULL)
	 {
		json_node = cJSON_GetObjectItemCaseSensitive(AlarmingParaRoot, "SWITCH");
		if(json_node == NULL)
		{
			//AlarmingRecord_CurArray = cJSON_CreateArray();
			cJSON_AddNumberToObject(AlarmingParaRoot,"SWITCH",0);
		}

		AlarmingParaZoneArray = cJSON_GetObjectItemCaseSensitive(AlarmingParaRoot, "ZONE");
		if(AlarmingParaZoneArray == NULL||!cJSON_IsArray(AlarmingParaZoneArray))
		{
			//AlarmingRecord_CurArray = cJSON_CreateArray();
			cJSON_DeleteItemFromObjectCaseSensitive(AlarmingParaRoot, "ZONE");
			AlarmingParaZoneArray = cJSON_AddArrayToObject(AlarmingParaRoot,"ZONE");
		}
		CheckAlarmingParaZoneArray(AlarmingParaZoneArray);
		
		AlarmingParaArmModeArray = cJSON_GetObjectItemCaseSensitive(AlarmingParaRoot, "ARM_MODE");
		if(AlarmingParaArmModeArray==NULL||!cJSON_IsArray(AlarmingParaArmModeArray))
		{
			AlarmingParaArmModeArray = cJSON_AddArrayToObject(AlarmingParaRoot,"ARM_MODE");
		}
		CheckAlarmingParaArmModeArray(AlarmingParaArmModeArray);
	 }
}

void SaveAlarmingParaToFile(void)
{
	FILE	*file = NULL;
	char *string=cJSON_Print(AlarmingParaRoot);
	if(string!=NULL)
	{
		if((file=fopen(AlarmingPara_FILE_NAME,"w+")) != NULL)
		{
			fputs(string, file);
			
			fclose(file);
		}
		printf("AlarmingPara:\n%s\n",string);
		free(string);
	}
}

int Get_AlarmingPara_Switch(void)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(AlarmingParaRoot,"SWITCH");
	if(cJSON_IsNumber(one_node))
	{
		rev = one_node->valuedouble;
	}
	
	return rev;
}

int Set_AlarmingPara_Switch(int new_val)
{
	int rev = 0;
	cJSON *one_node;
	one_node = cJSON_GetObjectItemCaseSensitive(AlarmingParaRoot,"SWITCH");
	if(cJSON_IsNumber(one_node))
	{
		rev = cJSON_SetNumberValue(one_node, new_val);
	}
	
	return rev;
}

AlarmingZone_Type_E Get_AlarmingPara_ZoneType(int AlarmingZone_index)
{
	cJSON *one_node;
	AlarmingZone_Type_E rev = AlarmingZone_Type_Unused;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"TYPE");
		if(cJSON_IsNumber(one_node))
		{
			rev = one_node->valuedouble;
		}
	}
	return rev;
	#if 0
	if(AlarmingZone_index>=4)
	{
		return AlarmingZone_Type_Unused;
	}
	#if 0
	else if(AlarmingZone_index==3)
	{
		return AlarmingZone_Type_24H;
	}
	#endif
	return AlarmingZone_Type_Delay;
	#endif
}
AlarmingZone_Type_E Set_AlarmingPara_ZoneType(int AlarmingZone_index,int new_val)
{
	cJSON *one_node;
	AlarmingZone_Type_E rev = -1;
	int i;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"TYPE");
		if(cJSON_IsNumber(one_node))
		{
			rev = cJSON_SetNumberValue(one_node, new_val);
			if(rev == AlarmingZone_Type_24H||rev==AlarmingZone_Type_Unused)
			{
				for(i=0;i<4;i++)
				{
					int zone_mask;
					zone_mask=Get_AlarmingZone_ByArmedMode(i);
					if(zone_mask&(0x01<<AlarmingZone_index))
					{
						zone_mask^=(0x01<<AlarmingZone_index);
						Set_AlarmingZone_ByArmedMode(i,zone_mask);
					}
				}
			}
		}
	}
	return rev;
}

AlarmingSenser_Type_E Get_AlarmingPara_SenserType(int AlarmingZone_index)
{
	cJSON *one_node;
	AlarmingSenser_Type_E rev = AlarmingSenser_Type_NO;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"SenserType");
		if(cJSON_IsNumber(one_node))
		{
			rev = one_node->valuedouble;
		}
	}
	return rev;
}
AlarmingSenser_Type_E Set_AlarmingPara_SenserType(int AlarmingZone_index,int new_val)
{
	cJSON *one_node;
	AlarmingSenser_Type_E rev = -1;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"SenserType");
		if(cJSON_IsNumber(one_node))
		{
			rev = cJSON_SetNumberValue(one_node,new_val);
		}
	}
	return rev;
}
int Get_AlarmingPara_ArmedDelay(int AlarmingZone_index)
{
	cJSON *one_node;
	int rev = 10;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"ArmDelay");
		if(cJSON_IsNumber(one_node))
		{
			rev = one_node->valuedouble;
		}
	}
	return rev;
}

int Set_AlarmingPara_ArmedDelay(int AlarmingZone_index,int new_val)
{
	cJSON *one_node;
	int rev = -1;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"ArmDelay");
		if(cJSON_IsNumber(one_node))
		{
			rev = cJSON_SetNumberValue(one_node, new_val);
		}
	}
	return rev;
}
int Get_AlarmingPara_DisarmedDelay(int AlarmingZone_index)
{
	cJSON *one_node;
	int rev = 10;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"DisarmDelay");
		if(cJSON_IsNumber(one_node))
		{
			rev = one_node->valuedouble;
		}
	}
	return rev;
}
int Set_AlarmingPara_DisarmedDelay(int AlarmingZone_index,int new_val)
{
	cJSON *one_node;
	int rev = -1;
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,AlarmingZone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"DisarmDelay");
		if(cJSON_IsNumber(one_node))
		{
			rev=cJSON_SetNumberValue(one_node, new_val);
		}
	}
	return rev;
}
int Get_AlarmingZone_ByArmedMode(int Arming_mode)
{
	cJSON *one_node;
	int rev = 15;
	one_node = cJSON_GetArrayItem(AlarmingParaArmModeArray,Arming_mode);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"ZoneMask");
		if(cJSON_IsNumber(one_node))
		{
			rev = one_node->valuedouble;
		}
	}
	return rev;
}



int Set_AlarmingZone_ByArmedMode(int Arming_mode,int zone_mask)
{
	cJSON *one_node;
	int rev = -1;
	one_node = cJSON_GetArrayItem(AlarmingParaArmModeArray,Arming_mode);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"ZoneMask");
		if(cJSON_IsNumber(one_node))
		{
			rev = cJSON_SetNumberValue(one_node, zone_mask);
		}
	}
	return rev;
}

int Get_Alarming24hZone(void)
{
	int rev = 0;
	int i;
	for(i=0;i<Max_AlarmingZone;i++)
	{
		if(Get_AlarmingPara_ZoneType(i) == AlarmingZone_Type_24H)
			rev|=(0x01<<i);
	}
	return rev;
}

int Get_AlarmingZoneName(int zone_index,char *str)
{
	cJSON *one_node;
	
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,zone_index);
	if(one_node!=NULL)
	{
		one_node = cJSON_GetObjectItemCaseSensitive(one_node,"NAME");
		if(cJSON_IsString(one_node))
		{
			strcpy(str,cJSON_GetStringValue(one_node));
			return 0;
		}
	}
	return -1;
}

int Set_AlarmingZoneName(int zone_index,char *str)
{
	cJSON *one_node;
	
	one_node = cJSON_GetArrayItem(AlarmingParaZoneArray,zone_index);
	if(one_node!=NULL&&str!=NULL&&strlen(str)<40)
	{
		cJSON_DeleteItemFromObjectCaseSensitive(one_node,"NAME");
		cJSON_AddStringToObject(one_node,"NAME",str);
	}
	return 0;
}

char AlarmingParaSettingInputBuff[40];

void AlarmingParaMenuSettingSave(void)
{
	SaveAlarmingParaToFile();
	API_Alarming_Para_Update();
}



int AlarmingParaRootMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			strcpy(disp,"Switch");
			*unicode_len = 0;
			break;
		case 1:
			strcpy(disp,"Zone");
			*unicode_len = 0;
			break;
		case 2:
			strcpy(disp,"Arming Mode");
			*unicode_len = 0;
			break;
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return rev;
}

int AlarmingParaRootMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		case 0:
			if(Get_AlarmingPara_Switch())
			{
				strcpy(disp,"[Enable]");
			}
			else
			{
				strcpy(disp,"[Disable]");
			}
			*unicode_len = 0;
			break;
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int AlarmingParaRootMenuSlecet(int index)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			if(Get_AlarmingPara_Switch())
			{
				Set_AlarmingPara_Switch(0);
			}
			else
			{
				Set_AlarmingPara_Switch(1);
			}
			Set_JsonParaSettingHaveChange(1);
			Enter_AlarmingParaSettingRootMenu(0);
			break;
		case 1:
			Enter_AlarmingParaSettingZoneArrayMenu(0,index);
			break;
		case 2:
			Enter_AlarmingParaSettingArmModeArrayMenu(0,index);
			break;
	}
	return rev;
}

int AlarmingParaRootMenuReturn(void)
{
	popDisplayLastMenu();
	return 0;
}

void Enter_AlarmingParaSettingRootMenu(int select)
{
	ComboBoxSelect_T para_menu;
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	Set_JsonParaSettingCurNode(AlarmingParaRoot);
	Set_JsonParaSettingCurNodeIndex(0);
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	Set_JsonParaSettingCurItemNums(Get_AlarmingPara_Switch()?3:1);

	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);	
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	Set_JsonParaSettingToKeypad(0);
	#if 1
	GetParaMenuRecord(Alarming_Setting, &para_menu);
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	//para_menu.name[para_menu.nameLen]= 0;
	Set_JsonParaSettingTitle(para_menu.nameLen,para_menu.name);
	#else
	Set_JsonParaSettingTitle(0,"Alarming Setting");
	#endif
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	Set_JsonParaSettingMenuSaveFunc(AlarmingParaMenuSettingSave);
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	Set_JsonParaSettingMenuItemDispFunc(AlarmingParaRootMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(AlarmingParaRootMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(AlarmingParaRootMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(AlarmingParaRootMenuReturn);
	printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
	if(GetCurMenuCnt() != MENU_152_JsonParaSettingPublic)
	{
		printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
		Set_JsonParaSettingHaveChange(0);
		StartInitOneMenu(MENU_152_JsonParaSettingPublic,0,1);
	}
	else
	{
		printf("222222222%s:%d:%d\n",__func__,__LINE__,select);
		JsonParaSettingDispReflush();
	}
}


int AlarmingParaZoneArrayMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char zone_name[40];
	switch(index)
	{
		case 0:
		case 1:
		case 2:
		case 3:
			Get_AlarmingZoneName(index,zone_name);
			snprintf(disp,100,"%d %s %s %s",index+1,zone_name,
				AlarmingZoneTypeDisp[Get_AlarmingPara_ZoneType(index)],
				AlarmingZoneSenserTypeDisp[Get_AlarmingPara_SenserType(index)]);
			*unicode_len = 0;
			break;
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
		
	}
	return rev;
}

int AlarmingParaZoneArrayMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int AlarmingParaZoneArrayMenuSlecet(int index)
{
	int rev = 0;
	Enter_AlarmingParaSettingZoneMenu(0,index,NULL);
	return rev;
}

int AlarmingParaZoneArrayMenuReturn(void)
{
	printf("222222222%s:%d:%d\n",__func__,__LINE__,Get_JsonParaSettingCurNodeIndex());
	Enter_AlarmingParaSettingRootMenu(Get_JsonParaSettingCurNodeIndex());
	return 0;
}

void Enter_AlarmingParaSettingZoneArrayMenu(int select,int cur_node_index)
{
	Set_JsonParaSettingCurNode(AlarmingParaZoneArray);
	Set_JsonParaSettingCurNodeIndex(cur_node_index);
	Set_JsonParaSettingCurItemNums(4);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	Set_JsonParaSettingTitle(0,"Alarming Zone");
	
	
	
	Set_JsonParaSettingMenuItemDispFunc(AlarmingParaZoneArrayMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(AlarmingParaZoneArrayMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(AlarmingParaZoneArrayMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(AlarmingParaZoneArrayMenuReturn);
		
	JsonParaSettingDispReflush();
}
int AlarmingParaZoneMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	switch(index)
	{
		case 0:
			strcpy(disp,"Name");
			*unicode_len = 0;
			break;
		case 1:
			strcpy(disp,"Type");
			*unicode_len = 0;
			break;
		case 2:
			strcpy(disp,"Senser Type");
			*unicode_len = 0;
			break;
		case 3:
			strcpy(disp,"Exit Delay");
			*unicode_len = 0;
			break;	
		case 4:
			strcpy(disp,"Enter Delay");
			*unicode_len = 0;
			break;	
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
		
	}
	return rev;
}

int AlarmingParaZoneMenuValueDisp(int index,char *disp,int *unicode_len)
{
	int val;
	switch(index)
	{
		case 0:
			Get_AlarmingZoneName(Get_JsonParaSettingCurNodeIndex(),disp);
			*unicode_len = 0;
			break;
		case 1:
			val = Get_AlarmingPara_ZoneType(Get_JsonParaSettingCurNodeIndex());
			#if 0
			if(val == AlarmingZone_Type_Unused)
			{
				strcpy(disp,"Unused");
			}
			if(val == AlarmingZone_Type_24H)
			{
				strcpy(disp,"24H");
			}
			if(val == AlarmingZone_Type_Immediately)
			{
				strcpy(disp,"Immediately");
			}
			if(val == AlarmingZone_Type_Delay)
			{
				strcpy(disp,"Delay");
			}
			#endif
			strcpy(disp,AlarmingZoneTypeDisp[val]);
			*unicode_len = 0;
			break;
		case 2:
			val = Get_AlarmingPara_SenserType(Get_JsonParaSettingCurNodeIndex());
			#if 0
			if(val == AlarmingSenser_Type_NO)
			{
				strcpy(disp,"NO");
			}
			if(val == AlarmingSenser_Type_NC)
			{
				strcpy(disp,"NC");
			}
			#endif
			strcpy(disp,AlarmingZoneSenserTypeDisp[val]);
			*unicode_len = 0;
			break;	
		case 3:
			val = Get_AlarmingPara_ArmedDelay(Get_JsonParaSettingCurNodeIndex());
			sprintf(disp,"%d",val);
			
			*unicode_len = 0;
			break;
		case 4:
			val = Get_AlarmingPara_DisarmedDelay(Get_JsonParaSettingCurNodeIndex());
			sprintf(disp,"%d",val);
			
			*unicode_len = 0;
			break;
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int SaveAlarmingParaZoneName(const char *data)
{
	Set_AlarmingZoneName(Get_JsonParaSettingCurNodeIndex(),data);
	Set_JsonParaSettingHaveChange(1);
	return 1;
}

int SaveAlarmingParaZoneArmDelay(const char *data)
{
	int val;
	int i;
	i = strlen(data);
	if(i == 0)
		return 0;
	for(;i>0;i--)
	{
		if(data[i-1]>'9'||data[i-1]<'0')
			return 0;
	}
	val = atoi(data);
	if(val>180)
		val = 180;
	Set_AlarmingPara_ArmedDelay(Get_JsonParaSettingCurNodeIndex(),val);
	Set_JsonParaSettingHaveChange(1);
	return 1;
}

int SaveAlarmingParaZoneDisarmDelay(const char *data)
{
	int val;
	int i;
	i = strlen(data);
	if(i == 0)
		return 0;
	for(;i>0;i--)
	{
		if(data[i-1]>'9'||data[i-1]<'0')
			return 0;
	}
	val = atoi(data);
	if(val>180)
		val = 180;
	Set_AlarmingPara_DisarmedDelay(Get_JsonParaSettingCurNodeIndex(),val);
	Set_JsonParaSettingHaveChange(1);
	return 1;
}

void SaveAlarmingParaType(int select)
{
	if(select>=4)
		return;
	Set_AlarmingPara_ZoneType(Get_JsonParaSettingCurNodeIndex(), select);
	Set_JsonParaSettingHaveChange(1);
}

void SaveAlarmingParaSenserType(int select)
{
	if(select>=2)
		return;
	Set_AlarmingPara_SenserType(Get_JsonParaSettingCurNodeIndex(), select);
	Set_JsonParaSettingHaveChange(1);
}

int AlarmingParaZoneMenuSlecet(int index)
{
	int rev = 0;
	
	switch(index)
	{
		case 0:
			Set_JsonParaSettingToKeypad(1);
			Get_AlarmingZoneName(Get_JsonParaSettingCurNodeIndex(),AlarmingParaSettingInputBuff);	
			EnterKeypadMenu(KEYPAD_CHAR,MESG_TEXT_ICON_ParaValue, AlarmingParaSettingInputBuff, 20, COLOR_WHITE, AlarmingParaSettingInputBuff, 1, SaveAlarmingParaZoneName);
			break;
		case 1:
			Set_JsonParaSettingToKeypad(1);
			InitPublicSettingMenuDisplay(4,AlarmingZoneTypeDisp);
			EnterPublicSettingMenu(MESG_TEXT_ICON_044_Parameter, 0, 4, Get_AlarmingPara_ZoneType(Get_JsonParaSettingCurNodeIndex()), SaveAlarmingParaType);
			StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
			break;
		case 2:
			Set_JsonParaSettingToKeypad(1);
			InitPublicSettingMenuDisplay(2,AlarmingZoneSenserTypeDisp);
			EnterPublicSettingMenu(MESG_TEXT_ICON_044_Parameter, 0, 2, Get_AlarmingPara_SenserType(Get_JsonParaSettingCurNodeIndex()), SaveAlarmingParaSenserType);
			StartInitOneMenu(MENU_012_PUBLIC_SETTING,0,1);
			break;
		case 3:
			Set_JsonParaSettingToKeypad(1);
			sprintf(AlarmingParaSettingInputBuff,"%d",Get_AlarmingPara_ArmedDelay(Get_JsonParaSettingCurNodeIndex()));	
			EnterKeypadMenu(KEYPAD_NUM,MESG_TEXT_ICON_ParaValue, AlarmingParaSettingInputBuff, 3, COLOR_WHITE, AlarmingParaSettingInputBuff, 1, SaveAlarmingParaZoneArmDelay);
			break;
		case 4:
			Set_JsonParaSettingToKeypad(1);
			sprintf(AlarmingParaSettingInputBuff,"%d",Get_AlarmingPara_DisarmedDelay(Get_JsonParaSettingCurNodeIndex()));	
			EnterKeypadMenu(KEYPAD_NUM,MESG_TEXT_ICON_ParaValue, AlarmingParaSettingInputBuff, 3, COLOR_WHITE, AlarmingParaSettingInputBuff, 1, SaveAlarmingParaZoneDisarmDelay);
			break;
	}
	return rev;
}

int AlarmingParaZoneMenuReturn(void)
{
	Enter_AlarmingParaSettingZoneArrayMenu(Get_JsonParaSettingCurNodeIndex(),1);
	return 0;
}
void Enter_AlarmingParaSettingZoneMenu(int select,int cur_node_index,cJSON *one_zone)
{
	char disp[20];
	Set_JsonParaSettingCurNode(one_zone);
	Set_JsonParaSettingCurNodeIndex(cur_node_index);
	Set_JsonParaSettingCurItemNums(5);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	sprintf(disp,"Zone %d",select+1);
	Set_JsonParaSettingTitle(0,disp);
	
	
	
	Set_JsonParaSettingMenuItemDispFunc(AlarmingParaZoneMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(AlarmingParaZoneMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(AlarmingParaZoneMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(AlarmingParaZoneMenuReturn);
		
	JsonParaSettingDispReflush();
}

int AlarmingParaArmModeArrayMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	
	switch(index)
	{
		case 0:
			API_GetOSD_StringWithID(MESG_TEXT_ArmMode1, NULL, 0, NULL, 0,disp, unicode_len);
			break;
		case 1:
			API_GetOSD_StringWithID(MESG_TEXT_ArmMode2, NULL, 0, NULL, 0,disp, unicode_len);
			break;
		case 2:
			API_GetOSD_StringWithID(MESG_TEXT_ArmMode3, NULL, 0, NULL, 0,disp, unicode_len);
			break;
		case 3:
			API_GetOSD_StringWithID(MESG_TEXT_ArmMode4, NULL, 0, NULL, 0,disp, unicode_len);
			break;
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
		
	}
	return rev;
}

int AlarmingParaArmModeArrayMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int AlarmingParaArmModeArrayMenuSlecet(int index)
{
	int rev = 0;
	Enter_AlarmingParaSettingArmModeMenu(0,index,NULL);
	return rev;
}

int AlarmingParaArmModeArrayMenuReturn(void)
{
	Enter_AlarmingParaSettingRootMenu(Get_JsonParaSettingCurNodeIndex());
	return 0;
}
void Enter_AlarmingParaSettingArmModeArrayMenu(int select,int cur_node_index)
{
	char disp[20];
	Set_JsonParaSettingCurNode(AlarmingParaArmModeArray);
	Set_JsonParaSettingCurNodeIndex(cur_node_index);
	Set_JsonParaSettingCurItemNums(4);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(0);
	
	Set_JsonParaSettingToKeypad(0);
	sprintf(disp,"Arming Mode");
	Set_JsonParaSettingTitle(0,disp);
	
	
	
	Set_JsonParaSettingMenuItemDispFunc(AlarmingParaArmModeArrayMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(AlarmingParaArmModeArrayMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(AlarmingParaArmModeArrayMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(AlarmingParaArmModeArrayMenuReturn);
		
	JsonParaSettingDispReflush();
}

int ArmModeItemNums = 0;
int ArmModeValidZoneMask = 0;
int AlarmingParaArmModeMenuItemDisp(int index,char *disp,int *unicode_len)
{
	int rev = 0;
	char zone_name[40];
	int zone_mask;
	int i,j;
	
	zone_mask=Get_AlarmingZone_ByArmedMode(Get_JsonParaSettingCurNodeIndex());
	if(index<8)
	{
		for(i=0,j=0;i<8;i++)
		{
			if(ArmModeValidZoneMask&(0x01<<i))
			{
				if(j++==index)
					break;
			}
			
		}
		
		Get_AlarmingZoneName(i, zone_name);
		sprintf(disp,"%s",zone_name);
		*unicode_len = 0;
		if(zone_mask&(0x01<<i))
		{
			//printf("AlarmingParaArmModeMenuItemDisp %d select\n",index);
			rev = 1;
		}
	}
	else
	{
		disp[0]=0;
		*unicode_len = 0;
	}
	
	return rev;
}

int AlarmingParaArmModeMenuValueDisp(int index,char *disp,int *unicode_len)
{
	switch(index)
	{
		default:
			disp[0]=0;
			*unicode_len = 0;
			break;
	}
	return 0;
}

int AlarmingParaArmModeMenuSlecet(int index)
{
	int rev = 0;
	int zone_mask;
	int i,j;
	zone_mask=Get_AlarmingZone_ByArmedMode(Get_JsonParaSettingCurNodeIndex());
	if(index<8)
	{
		for(i=0,j=0;i<8;i++)
		{
			if(ArmModeValidZoneMask&(0x01<<i))
			{
				if(j++==index)
					break;
			}
		}
		zone_mask^=(0x01<<i);
		Set_AlarmingZone_ByArmedMode(Get_JsonParaSettingCurNodeIndex(), zone_mask);
		
		if(zone_mask&(0x01<<i))
			rev = 1;
		Set_JsonParaSettingHaveChange(1);
	}
	return rev;
}

int AlarmingParaArmModeMenuReturn(void)
{
	Enter_AlarmingParaSettingArmModeArrayMenu(Get_JsonParaSettingCurNodeIndex(),2);
	return 0;
}
void Enter_AlarmingParaSettingArmModeMenu(int select,int cur_node_index,cJSON *one_mode)
{
	char disp[40];
	int unicode_len;
	int i;
	Set_JsonParaSettingCurNode(one_mode);
	Set_JsonParaSettingCurNodeIndex(cur_node_index);
	ArmModeValidZoneMask = Get_Alarming24hZone();
	ArmModeValidZoneMask = (~ArmModeValidZoneMask)&0x0f;
	for(ArmModeItemNums = 0,i=0;i<8;i++)
	{
		if(ArmModeValidZoneMask&(0x01<<i))
			ArmModeItemNums++;
	}
	Set_JsonParaSettingCurItemNums(ArmModeItemNums);
	Set_JsonParaSettingCurPage(select/JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingCurLine(select%JsonParaSetting_ICON_MAX);
	Set_JsonParaSettingDispMode(1);
	
	Set_JsonParaSettingToKeypad(0);
	//sprintf(disp,"Arming Mode");
	//Set_JsonParaSettingTitle(MESG_TEXT_ArmMode1+cur_node_index,NULL);
	API_GetOSD_StringWithID(MESG_TEXT_ArmMode1+cur_node_index, NULL, 0, NULL, 0,disp, &unicode_len);
	Set_JsonParaSettingTitle(unicode_len,disp);
	
	
	
	Set_JsonParaSettingMenuItemDispFunc(AlarmingParaArmModeMenuItemDisp);
	Set_JsonParaSettingMenuValueDispFunc(AlarmingParaArmModeMenuValueDisp);
	Set_JsonParaSettingMenuSelectFunc(AlarmingParaArmModeMenuSlecet);
	Set_JsonParaSettingMenuReturnFunc(AlarmingParaArmModeMenuReturn);
		
	JsonParaSettingDispReflush();
}
