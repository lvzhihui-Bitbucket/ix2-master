/**
  ******************************************************************************
  * @file    task_Monitor.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_LocalMonitor_H
#define _task_LocalMonitor_H

#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"

typedef struct LOCALMONITOR_RUN_STRU
{
	uint8	state;			//监视状态
	uint8	talk;			//监视启动通话标志
	uint16	partner_IA;		//被监视者之地址
	uint16	timer;			//监视定时
	uint8	slaveMaster;
	union
	{
		int		master_ip;
		int		slave_ip;
	};
	uint16	limit_time;
}	LOCALMONITOR_RUN;
extern LOCALMONITOR_RUN	Mon_Run;
// Define Task Vars and Structures----------------------------------------------
// zfz
//监视工作状态
#define MON_STATE_OFF	0			//空闲状态
#define MON_LINKING     	1
#define MON_STATE_ON	2			//监视显示
#define MON_STATE_DXSLAVE_ON		3		//监视显示
#define MON_STATE_PHONE_ON		4
#define MON_STATE_PHONE_NEXT		5

	//监视通话状态
#define MON_TALK_OFF	0		//通话关闭
#define MON_TALK_ON		1		//通话开启

#define MON_LINK_OK			0x01

//#define SYSTEM_SPRITE_INFORM_X		230
//#define SYSTEM_SPRITE_INFORM_Y		193



	//软定时_MONITOR定时结束



#pragma pack(1)

// Define task interface-------------------------------------------------------------
typedef struct {
	unsigned char 		msg_target_id;					// 消息的目标对象
	unsigned char 		msg_source_id;					// 消息的源对象
	unsigned char 		msg_type;						// 消息的类型
	unsigned char  		msg_sub_type;					//消息的子类型
	unsigned char 		call_type;
	unsigned short		address;						//地址(源设备 或 目标设备)
	unsigned char			command_type;					//指令类型
	//int					slave_ip;
	int 					win_id;
} MONITOR_STRUCT ;

#pragma pack()

typedef enum
{
	MON_LINK_ERROR,			//监视申请链路失败
	MON_LINK_BUSY, 			
	MON_LINK_ERROR1, 		
	MON_LINK_ERROR2,		
	MON_LINK_ERROR3,		
	MON_LINK_ERROR4,		
	MON_SWITCH_ERROR,		//监视切换失败
	
}MON_ERR_CODE;

	//msg_type
#define MON_CONTROL_ON					1
#define MON_CONTROL_TALK_ON				2
#define MON_CONTROL_TALK_OFF			3
#define MON_CONTROL_UNLOCK1				4
#define MON_CONTROL_UNLOCK2				5
#define MON_CONTROL_CANCEL				6
#define MON_CONTROL_CANCEL_FOR_FUNCTION	7	
#define MON_TIMER_STOP					8
#define MON_CONTROL_OFF					9
#define MON_CONTROL_OFF_MENU			10	//DT39_Monitor	本机关机,不关菜单
#define MON_NEXT                        11
#define MON_QSW_DEV_DISP                12
#define MON_CONTROL_DXSLAVE_ON		13
#define MON_CONTROL_PHONE_ON			14
#define MON_CONTROL_FISHEYE_ON			15		//czn_20181117
#define MON_VD_SRC_NEXT                 16
#define MON_MAINCALL_CLOSE		17
#define MON_VI_OPEN				18
#define MON_VI_CLOSE					19
#define MON_CONTROL_FISHEYE_SWITCH			20		//czn_20181117
#define MON_CONTROL_PHONE_TALK_ON				21
#define MON_CONTROL_PHONE_NEXT				22
#define MON_VI_CLOSE_KEEPPWR					23

// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_DXMonitor(void);	
void vtk_TaskProcessEvent_DXMonitor(void *Msg,int len);


// Define Task others-----------------------------------------------------------
void DXMonitor_Timer_Stop(void);


// Define API-------------------------------------------------------------------
//unsigned char API_LocalMonitorTimeReset(void);

uint8 API_DXMonitor_Common(uint8 msg_type_temp , uint16 address_temp , uint8 command_type_temp,int win_id);
void LocalMonitor_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);

	/*uint8 API_Event_Monitor_On(uint16 mon_address)
	入口:	监视地址

	处理:	发送消息给Monitor,并等待Monitor的event应答;   

	出口:
		0x00 = Monitor应答event = 接受此消息;
		0x01 = Monitor应答event = 不接受此消息;
		0xff = Monitor无需event应答;
	*/
	#define API_Event_DXMonitor_On(mon_address,win_id) 			\
			API_DXMonitor_Common(MON_CONTROL_ON , mon_address , NULL,win_id)	

    #define API_DXMonitor_Next(mon_address)  API_DXMonitor_Common(MON_NEXT , mon_address , NULL,NULL)
     #define API_DXMonitor_PhoneNext(mon_address)  API_DXMonitor_Common(MON_CONTROL_PHONE_NEXT , mon_address , NULL,NULL)           
	

	/*uint8 API_Monitor_Talk_On(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	暂无使用
	*/
	#define API_DXMonitor_Talk_On()		API_DXMonitor_Common(MON_CONTROL_TALK_ON , NULL , NULL,NULL)	


	/*uint8 API_Monitor_Talk_Off(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	暂无使用
	*/
	#define API_DXMonitor_Talk_Off()		API_DXMonitor_Common(MON_CONTROL_TALK_OFF , NULL , NULL,NULL)	


	/*uint8 API_Monitor_Off(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	暂无使用
	*/
	#define API_DXMonitor_Off()			API_DXMonitor_Common(MON_CONTROL_OFF , NULL , NULL,NULL)	


	/*uint8 API_Event_Monitor_Cancel(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	
		0x00 = Monitor应答event = 接受此消息;
		0x01 = Monitor应答event = 不接受此消息;
		0xff = Monitor无需event应答;
	*/
	#define API_Event_DXMonitor_Cancel()		API_DXMonitor_Common(MON_CONTROL_CANCEL , NULL , NULL,NULL)	


	/*uint8 API_Monitor_Unlock1(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	暂无使用
	*/
	#define API_DXMonitor_Unlock1()	API_DXMonitor_Common(MON_CONTROL_UNLOCK1 , NULL , NULL,NULL)	


	/*uint8 API_Monitor_Unlock2(void)
	入口:	无

	处理:	发送消息给Monitor

	出口:	暂无使用
	*/
	#define API_DXMonitor_Unlock2()	API_DXMonitor_Common(MON_CONTROL_UNLOCK2 , NULL , NULL,NULL)	

	#define API_Event_Maincall_Close()		API_DXMonitor_Common(MON_MAINCALL_CLOSE, NULL , NULL,NULL)

	#define API_DXMonitor_Phone_Talk_On()		API_DXMonitor_Common(MON_CONTROL_PHONE_TALK_ON, NULL , NULL,NULL)	
	
void linphone_mon_lasttime_update(void);
void video_block_for_linphone_mon(void);

#endif
