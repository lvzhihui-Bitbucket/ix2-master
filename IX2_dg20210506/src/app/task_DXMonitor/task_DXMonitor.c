/**
  ******************************************************************************
  * @file    task_Monitor.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include "task_DXMonitor.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
#include "obj_menu_data.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
//#include "obj_memo.h"
#include "define_Command.h"
#include "stack212.h"
#include "obj_UnitSR.h"
#include "task_CallServer.h"
#include "task_IoServer.h"
#include "task_Power.h"
#include "obj_ImNameListTable.h"
#include "cJSON.h"
LOCALMONITOR_RUN	Mon_Run;

//����ʱ_MONITOR��ʱ����
OS_TIMER timer_dxmonitor_stop;
Loop_vdp_common_buffer	vdp_dxmon_mesg_queue;
Loop_vdp_common_buffer	vdp_dxmon_sync_queue;
vdp_task_t				task_dxmon;
static int fisheye_addr=0x34;
static int fisheye_mode=0;


void* vdp_dxmon_task( void* arg );

//void local_mon_lasttime_update(void);
//int judge_local_mon_block(void);
/*------------------------------------------------------------------------
						task_init
------------------------------------------------------------------------*/
void vtk_TaskInit_DXMonitor(void)
{
	//init
	Mon_Run.state = MON_STATE_OFF;
	Mon_Run.talk = MON_TALK_OFF;	

	init_vdp_common_queue(&vdp_dxmon_mesg_queue, 1000, (msg_process)vtk_TaskProcessEvent_DXMonitor, &task_dxmon);
	init_vdp_common_queue(&vdp_dxmon_sync_queue, 100, NULL, 								  &task_dxmon);
	init_vdp_common_task(&task_dxmon, MSG_ID_LocalMonitor, vdp_dxmon_task, &vdp_dxmon_mesg_queue, &vdp_dxmon_sync_queue);

	//��������ʱ(δ����)
	OS_CreateTimer(&timer_dxmonitor_stop, DXMonitor_Timer_Stop, 1000/25);
	#if defined(PID_DX470)||defined(PID_DX482)
	MonRes_Table_Init();
	#endif
}

void* vdp_dxmon_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	
	uint8 slaveMaster;
	#if defined(PID_DX470)||defined(PID_DX482)
	#if 0
	usleep(15000000);
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	if(slaveMaster == 0)
	{
		API_VideoTurnOn();
		//usleep(1000000);
		API_VideoTurnOff();
	}
	#endif
	load_DxMonRes_list();
	load_DxNamelist_list();
	#if 0
	vi_dev_open();
	sleep(1);
	vi_read_fail_deal();
	vi_dev_close();
	#endif
	api_ak_vi_ch_stream_encode(0,1);
	sleep(1);
	api_ak_vi_ch_stream_encode(0,0);
	#if 0
	//sleep(3);
	API_VideoTurnOn(0);
	sleep(2);
	API_VideoTurnOff();
	sleep(1);
	#endif
	#endif
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			#if defined(PID_DX470)||defined(PID_DX482)
			(*ptask->p_msg_buf->process)(pdb,size);
			#endif
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int DXMonitor_Camera_Open(unsigned short addr)
{
#if 1
	if(addr == DS1_ADDRESS)
	{
		API_Stack_APT_Without_ACK(0x34, MON_ON);
	}
	else if ((addr >= DS2_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		API_Stack_APT_Without_ACK(addr, MON_ON_APPOINT);
	}
#endif
	//return VideoClient_Request(addr,0x40,30);
}

void DXMonitor_Camera_Close(unsigned short addr)
{
	#if 1
	if ((addr >= DS1_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		API_Stack_APT_Without_ACK(addr, MON_OFF);
	}
	#endif
	//VideoClient_Close(addr);
}

int DXMonitor_Camera_Link(unsigned short addr)
{
	if(addr == DS1_ADDRESS)
	{
		return 1;	
	}
	else if ((addr >= DS2_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		if(InstructionCheck(addr, ST_LINK_MR)== 1)
		{
			return 1;
		}
		return 0;
	}
	else
	{
		return 1;
	}
}

void DXMonitor_Camera_Talk(unsigned short addr)
{
	if ((addr >= DS1_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		API_Stack_APT_Without_ACK(addr,ST_TALK);
	}
}

void DXMonitor_Camera_Unlock(unsigned short addr)
{
	if ((addr >= DS1_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		API_Stack_APT_Without_ACK(addr,ST_UNLOCK);
	}
}

void DXMonitor_Camera_Unlock2(unsigned short addr)
{
	if ((addr >= DS1_ADDRESS) && (addr <= DS4_ADDRESS))
	{
		API_Stack_APT_Without_ACK(addr,ST_UNLOCK_SECOND);
	}
}


#if 0
void Display_Mon_Device(void)
{
	unsigned char disp_name[41];
	
    if ((Mon_Run.partner_IA >= DS1_ADDRESS)	&& (Mon_Run.partner_IA <= DS4_ADDRESS))	
    {
		Get_MonResName_ByAddr(Mon_Run.partner_IA,disp_name);
    }
    else
    {
    	snprintf(disp_name, 41, "CM%d", Mon_Run.partner_IA-MEM_CAMERA_ID5+1);
    }

	API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CLEAR_STATE_W, CLEAR_STATE_H);
	
	API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),CallMenuDisp_Name_Font,STR_UTF8, 0);
}
#endif
unsigned char DXMonitor_Next(void)
{
#if 0
#if 0
    OS_TASK_EVENT 	MyEvents;
    unsigned short nextAddr;
    unsigned char dsQty = 0;
    unsigned char camQty = 0;
    unsigned char index = 0;
	unsigned char disp_name[41];
	
    API_Event_IoServer_InnerRead_All(DS_QUANTITY, (uint8*)&dsQty);
    API_Event_IoServer_InnerRead_All(CAM_QUANTITY, (uint8*)&camQty);
    
    
    if ((Mon_Run.partner_IA >= DS1_ADDRESS)	&& (Mon_Run.partner_IA <= DS4_ADDRESS))	 // current address = door station
    {
        if( (Mon_Run.partner_IA - DS1_ADDRESS) < (dsQty-1) )
        {
            nextAddr = Mon_Run.partner_IA + 1;
        }
        else if( camQty )
        {
            nextAddr = MEM_CAMERA_ID5;
        }
        else if( dsQty > 1 )
        {
            nextAddr = DS1_ADDRESS;
        }
        else
        {
            BEEP_ERROR();
            return 1;
        }
    }
    else // current address = cammera
    {
        if( (Mon_Run.partner_IA - MEM_CAMERA_ID5) < (camQty-1) )
        {
            nextAddr = Mon_Run.partner_IA + 1;
        }
        else
        {
            nextAddr = DS1_ADDRESS;
        }            
    }

    if( (nextAddr >= DS2_ADDRESS) && (nextAddr <= DS4_ADDRESS) )
    {
    	if(Monitor_Camera_Link(nextAddr) == 0)
    	{
            if( camQty )
            {
                nextAddr = MEM_CAMERA_ID5;
            }
            else
            {
                BEEP_ERROR();
				printf("have no ds and camera\n");
                return 1;
            }
		}
    }
	
    if( VideoClient_Request(nextAddr, REASON_CODE_MON, Mon_Run.limit_time) )	//����ʧ��
    {
        if( (Mon_Run.partner_IA < DS1_ADDRESS) ) // camera
        {
            nextAddr = DS1_ADDRESS;
            VideoClient_Request(nextAddr, REASON_CODE_MON, Mon_Run.limit_time);
		    API_VideoTurnOff();
			printf("333333333333 --------------- nextAddr=%d, disp_name=%s, index=%d\n", nextAddr, disp_name, index);
            goto display;
        }
        else
        {
            BEEP_ERROR();
			printf("Monitor error\n");
            return 1;
        }
    }
    else
    {
		printf("111111111111 --------------- nextAddr=%d, disp_name=%s, index=%d\n", nextAddr, disp_name, index);
		//API_VideoTurnOff();
        if( (Mon_Run.partner_IA >= DS1_ADDRESS) && (Mon_Run.partner_IA <= DS4_ADDRESS) )
        {
            API_Stack_APT_Without_ACK(Mon_Run.partner_IA , MON_OFF_APPOINT);
            if (Mon_Run.talk == MON_TALK_ON)
            {	
                API_TalkOff();
                Mon_Run.talk = MON_TALK_OFF;
            }
        }
        display:
        Mon_Run.partner_IA = nextAddr;
        Mon_Run.timer = 0;
        //MON��ʱ��ʾ
        API_TimeLapseStart(COUNT_RUN_UP , 1);
		Display_Mon_Device();

		sleep(1);
        //API_VideoTurnOn();
        OS_RetriggerTimer(&timer_monitor_stop);
        return 0;
    }
#else	
    unsigned short nextAddr;
    unsigned char index = 0;
	unsigned char disp_name[41];
	unsigned char dsNotOnlineFlag;
	

	index = Get_MonResIndex_ByAddr(Mon_Run.partner_IA);
	if(index < 0)
	{
		BEEP_ERROR();
		return 1;
	}

	if(++index >= Get_MonRes_Num())
	{
		index = 0;
	}

	for(dsNotOnlineFlag = 0; index < Get_MonRes_Num(); index++)
	{
		Get_MonRes_Record(index, &nextAddr, disp_name);

		if(dsNotOnlineFlag && nextAddr >= DS1_ADDRESS && nextAddr <= DS4_ADDRESS)
		{
			continue;
		}
		
		if(Monitor_Camera_Link(nextAddr) == 0)
		{
			dsNotOnlineFlag = 1;
		}
		else
		{
			break;
		}
	}

	if(index >= Get_MonRes_Num())
	{
		BEEP_ERROR();
		printf("have no ds or camera\n");
		return 1;
	}

    if( VideoClient_Request(nextAddr, REASON_CODE_MON, Mon_Run.limit_time) )	//����ʧ��
    {
		nextAddr = DS1_ADDRESS;
		VideoClient_Request(nextAddr, REASON_CODE_MON, Mon_Run.limit_time);
		Get_MonResName_ByAddr(nextAddr,disp_name);
		goto display;
    }
    else
    {
		ClearFishEyeAndQswSprite();		
		
        if( (Mon_Run.partner_IA >= DS1_ADDRESS) && (Mon_Run.partner_IA <= DS4_ADDRESS) )
        {
            API_Stack_APT_Without_ACK(Mon_Run.partner_IA , MON_OFF_APPOINT);
            if (Mon_Run.talk == MON_TALK_ON)
            {	
                API_TalkOff();
                Mon_Run.talk = MON_TALK_OFF;
            }
        }

		display:
        Mon_Run.partner_IA = nextAddr;
        Mon_Run.timer = 0;
        //MON��ʱ��ʾ
        API_TimeLapseStart(COUNT_RUN_UP , 1);
		API_OsdStringClearExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CLEAR_STATE_W, CLEAR_STATE_H);
		API_OsdStringDisplayExt(CallMenuDisp_Name_x, CallMenuDisp_Name_y, CallMenuDisp_Name_Color, disp_name, strlen(disp_name),CallMenuDisp_Name_Font,STR_UTF8, 0);
        OS_RetriggerTimer(&timer_monitor_stop);
        return 0;
    }
	#endif
	#endif
}

/*------------------------------------------------------------------------
						task_process
------------------------------------------------------------------------*/
void vtk_TaskProcessEvent_DXMonitor(void *Msg,int len)
{
	MONITOR_STRUCT *msg_monitor = (MONITOR_STRUCT*)Msg;
	uint8 SR_apply;
	OS_TASK_EVENT MyEvents;
	char disp[41];
	MON_ERR_CODE errorCode;
	char tempData[42];
	int ds_addr;
	int wait_cnt;
	
	switch (msg_monitor->msg_type)
	{
		case MON_CONTROL_ON:
			
			
			if(Mon_Run.state !=  MON_STATE_OFF)
			{
				//cao_20181119 BEEP_ERROR();
				errorCode = MON_LINK_ERROR;
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));

				OS_RetriggerTimer(&timer_dxmonitor_stop);
				LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,1);	//֪ͨSurvey: ����������·ʧ��
		        	return;
			}
			
			Mon_Run.partner_IA = msg_monitor->address;
			
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStart);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStartting);

			
			
			SR_apply = API_SR_Request(CT11_ST_MONITOR);
			if (SR_apply == SR_DISABLE || SR_apply == SR_ENABLE_DEPRIVED)	//��·æ,���ɰ���
			{
				//cao_20181119 BEEP_ERROR();	//������ʾ��
				printf("1111111MON_LINK_BUSY:%d\n",SR_apply);
				errorCode = MON_LINK_BUSY;
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));
				LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);	//֪ͨSurvey: ����������·ʧ��
		        	return;
			}

			
			//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&Mon_Run.slaveMaster);
			Mon_Run.slaveMaster=0;
			if(Mon_Run.slaveMaster != 4)
			{
				#if 1
				if(DXMonitor_Camera_Link(Mon_Run.partner_IA) != 1)
				{
					//cao_20181119 BEEP_ERROR();
					errorCode = MON_LINK_ERROR2;
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));
					LocalMonitor_Business_Respones(MSG_ID_survey,MON_CONTROL_ON,1);
					return;
				}
				#endif
				if(DXMonitor_Camera_Open(Mon_Run.partner_IA) != 0)
				{
					//cao_20181119 BEEP_ERROR();
					errorCode = MON_LINK_ERROR2;
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));

					LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);
					return;
				}
				API_LedDisplay_LocalMon();
				printf("API_VideoTurnOn(333333)\n");
				//PrintCurrentTime(22222);
				//API_VideoTurnOn(msg_monitor->win_id);
				//PrintCurrentTime(33333);
				//usleep(1000000);
			}
			
			
			API_Event_IoServer_InnerRead_All(MONITOR_TIME_LIMIT, tempData);
			Mon_Run.limit_time=atoi(tempData)+5;
			//Mon_Run.limit_time=1200;
			if(Mon_Run.limit_time < 10)
			{
				Mon_Run.limit_time = 10;
			}
			#if 0
			else if(Mon_Run.limit_time > 600)
			{
				Mon_Run.limit_time = 600;
			}
			#endif
			
			Mon_Run.state = MON_STATE_ON;
			Mon_Run.talk = MON_TALK_OFF;
			Mon_Run.timer = 0;

			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOn);
			OS_RetriggerTimer(&timer_dxmonitor_stop);
			LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,0);
			API_VideoTurnOn(msg_monitor->win_id);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOn);
			break;
			
	
	
        case MON_NEXT:
			if(Mon_Run.state == MON_STATE_ON)
			{
				if (Mon_Run.talk == MON_TALK_ON)
				{
					BEEP_ERROR();
					LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);
					return;
				}
				
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorStartting);
				
				if(Mon_Run.slaveMaster != 4)
				{
					#if 1
					if(DXMonitor_Camera_Link(msg_monitor->address) != 1)
					{
						//cao_20181119 BEEP_ERROR();
						errorCode = MON_SWITCH_ERROR;
						API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));
						LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);
						return;
					}
					#endif
					if(DXMonitor_Camera_Open(msg_monitor->address) != 0)
					{
						//cao_20181119 BEEP_ERROR();
						errorCode = MON_SWITCH_ERROR;
						API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));
						LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);
						return;
					}
					
					usleep(1000000);
				}
				

				
				Mon_Run.partner_IA = msg_monitor->address;
				
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOn);
				
				Mon_Run.state = MON_STATE_ON;
				Mon_Run.talk = MON_TALK_OFF;
				Mon_Run.timer = 0;
				OS_RetriggerTimer(&timer_dxmonitor_stop);
				LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,0);
			}
			
            		break;
        
		case MON_CONTROL_TALK_ON:
			if (Mon_Run.state == MON_STATE_ON && Mon_Run.talk != MON_TALK_ON)
			{	
				if(Mon_Run.timer<2)
					usleep(1200000);
				if(Mon_Run.slaveMaster != 4)
				{
					DXMonitor_Camera_Talk(Mon_Run.partner_IA);
				}
				
				usleep(1000000);

				// lzh_20220415_s
				//API_TalkOn();//API_POWER_TALK_ON();
				API_POWER_TALK_ON();
				printf("===============================dx_audio_ds2dx_start monitor on=======================\n");
				//dx_audio_ds2dx_start();
				// lzh_20220415_e
				OpenDtTalk();
				//API_POWER_TALK_ON();
				//API_TalkOn();
				Mon_Run.talk = MON_TALK_ON;
				Mon_Run.timer = 0;
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
			}
			
			break;
        // zfz_mem
        case MON_CONTROL_TALK_OFF:
			#if 0
            if (Mon_Run.talk == MON_TALK_ON)
			{	
                Mon_Run.talk = MON_TALK_OFF;
				API_TalkOff();
				
				//zxj_add
				//......	�ر�Talkָʾ��

           		//API_LabelHide( LABEL_TALK );

			}
			#endif
            break;
		case MON_CONTROL_UNLOCK1:
			if (Mon_Run.state == MON_STATE_ON)
			{
				if(Mon_Run.slaveMaster != 4)
				{
					DXMonitor_Camera_Unlock(Mon_Run.partner_IA);
				}
				
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock1);
			}
			
			break;
		case MON_CONTROL_UNLOCK2:
			if (Mon_Run.state == MON_STATE_ON)
			{
				if(Mon_Run.slaveMaster != 4)
				{
					DXMonitor_Camera_Unlock2(Mon_Run.partner_IA);
				}
				
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorUnlock2);
			}
			
			break;
				
		case MON_CONTROL_CANCEL:
			if(Mon_Run.state == MON_STATE_ON)
			{
				API_LedDisplay_MonClose();
				API_POWER_DX_APP_TALK_OFF();
				API_POWER_TALK_OFF();
				API_TalkOff();//API_POWER_TALK_OFF();
				Mon_Run.state = MON_STATE_OFF;
				Mon_Run.talk = MON_TALK_OFF;
				OS_StopTimer(&timer_dxmonitor_stop);
				#if 0
				if(GetCurMenuCnt() == MENU_142_DX_MONITOR)
				{
					popDisplayLastMenu();
				}
				#endif
				if(Mon_Run.slaveMaster != 4)
				{
					//DXMonitor_Camera_Close(Mon_Run.partner_IA);
					API_VideoTurnOff();
				}
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
				local_mon_lasttime_update();

				// lzh_20220415_s
				printf("===============================monitor off=======================\n");
				API_POWER_TALK_OFF();
				dx_audio_ds2dx_stop();
				dx_audio_ds2app_stop();
				// lzh_20220415_e
			}
			
			LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,0);
			break;
		case MON_CONTROL_CANCEL_FOR_FUNCTION:
			
			break;				
				
				
		case MON_TIMER_STOP:	
		case MON_CONTROL_OFF:
			if(Mon_Run.state == MON_STATE_ON)
			{
				//API_Stack_APT_Without_ACK(Mon_Run.partner_IA, MON_OFF);
				Mon_Run.state = MON_STATE_OFF;
				Mon_Run.talk = MON_TALK_OFF;
				API_POWER_DX_APP_TALK_OFF();
				API_LedDisplay_MonClose();
				if(Mon_Run.slaveMaster != 4)
				{
					DXMonitor_Camera_Close(Mon_Run.partner_IA);
				}
				
				usleep(300000);
				API_POWER_TALK_OFF();
				API_TalkOff();
				
				OS_StopTimer(&timer_dxmonitor_stop);

				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
				usleep(300000);
				//API_VideoTurnOff();
				if(Mon_Run.slaveMaster != 4)
				{
					API_VideoTurnOff();
				}
				
				local_mon_lasttime_update();	

				// lzh_20220415_s
				printf("===============================monitor off=======================\n");
				API_POWER_TALK_OFF();
				dx_audio_ds2dx_stop();
				dx_audio_ds2app_stop();
				// lzh_20220415_e
			}	
			
			break;
			
		case MON_CONTROL_OFF_MENU:	//DT39_Monitor	�����ػ�,���ز˵�
		
			break;
		case MON_MAINCALL_CLOSE:
			if(Mon_Run.state == MON_STATE_ON)
			{
				//API_Stack_APT_Without_ACK(Mon_Run.partner_IA, MON_OFF);
				Mon_Run.state = MON_STATE_OFF;
				Mon_Run.talk = MON_TALK_OFF;
				API_POWER_DX_APP_TALK_OFF();
				API_LedDisplay_MonClose();
				if(Mon_Run.slaveMaster != 4)
				{
					//DXMonitor_Camera_Close(Mon_Run.partner_IA);
				}
				
				usleep(100000);
				API_POWER_TALK_OFF();
				API_TalkOff();
				
				OS_StopTimer(&timer_dxmonitor_stop);

				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOff);
				//usleep(300000);
				//API_VideoTurnOff();
				if(GetCurMenuCnt()==MENU_113_MonQuart)
				{
					API_VideoTurnOff();
				}
				
				local_mon_lasttime_update();	

				// lzh_20220415_s
				printf("===============================monitor off=======================\n");
				API_POWER_TALK_OFF();
				dx_audio_ds2dx_stop();
				dx_audio_ds2app_stop();
				// lzh_20220415_e
			}
			
			LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,0);
			break;
		case MON_VI_OPEN:
			API_LocalCaptureOn(msg_monitor->msg_sub_type);
			wait_cnt=0;
			while(wait_cnt++<10&&GetCurMenuCnt()!=MENU_027_CALLING2)
				usleep(100*1000);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorOn);
			break;
			
		case MON_VI_CLOSE:
			API_LocalCaptureOff();
			break;
		case MON_VI_CLOSE_KEEPPWR:
			API_LocalCaptureOffKeepPower();
			break;
		case MON_CONTROL_FISHEYE_ON:
			fisheye_addr=msg_monitor->address;
			fisheye_mode=0;
			API_FishEye_Mode(msg_monitor->address, 10);
			break;
		case MON_CONTROL_FISHEYE_SWITCH:
			if(fisheye_mode==0)
			{
				fisheye_mode=5;
				API_FishEye_Mode(fisheye_addr, 5);
			}
			else
			{
				fisheye_mode=0;
				API_FishEye_Mode(fisheye_addr, 0);
			}
			break;
		case MON_CONTROL_PHONE_TALK_ON:
			API_Event_IoServer_InnerRead_All(APP_MON_TALK_EN, tempData);
			if (atoi(tempData)==1&&Mon_Run.state == MON_STATE_ON && Mon_Run.talk != MON_TALK_ON)
			{	
				if(Mon_Run.timer<2)
					usleep(1200000);
				if(Mon_Run.slaveMaster != 4)
				{
					DXMonitor_Camera_Talk(Mon_Run.partner_IA);
				}
				
				usleep(1000000);
				#if 1
				API_POWER_TALK_ON();
				OpenAppTalk();
				printf("===============================dx_audio_ds2app_start turn on=======================\n");
				au_alsa_app_init();
				dx_audio_ds2app_start();
				usleep(50*1000);
				
				API_POWER_DX_APP_TALK_ON();
				#else
				// lzh_20220415_s
				//API_TalkOn();//API_POWER_TALK_ON();
				API_POWER_TALK_ON();
				printf("===============================dx_audio_ds2dx_start monitor on=======================\n");
				//dx_audio_ds2dx_start();
				// lzh_20220415_e
				OpenDtTalk();
				#endif
				//API_POWER_TALK_ON();
				//API_TalkOn();
				Mon_Run.talk = MON_TALK_ON;
				Mon_Run.timer = 0;
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorTalkOn);
			}
			
			break;
		case MON_CONTROL_PHONE_NEXT:
			if (Mon_Run.talk == MON_TALK_ON||Mon_Run.partner_IA==msg_monitor->address)
			{
				LocalMonitor_Business_Respones(MSG_ID_survey,MON_CONTROL_ON,1);
				return;
			}
				
					
					//usleep(100000);
				//Mon_Run.state = MON_STATE_PHONE_NEXT;
				if(DXMonitor_Camera_Link(msg_monitor->address) != 1)
					{
						//cao_20181119 BEEP_ERROR();
						//errorCode = MON_SWITCH_ERROR;
						//API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_MonitorError, &errorCode, sizeof(errorCode));
						//LocalMonitor_Business_Respones(msg_monitor->msg_source_id,MON_CONTROL_ON,1);
						return;
					}
				usleep(100000);
				Mon_Run.state = MON_STATE_PHONE_NEXT;
				DXMonitor_Camera_Close(Mon_Run.partner_IA);
				
				
				Mon_Run.partner_IA = msg_monitor->address;
				
				usleep(3000000);
				DXMonitor_Camera_Open(Mon_Run.partner_IA);
				//API_Stack_APT_Without_ACK(Mon_Run.partner_IA , MON_ON_APPOINT);

				Mon_Run.state = MON_STATE_ON;

				Mon_Run.timer = 0;
				OS_RetriggerTimer(&timer_dxmonitor_stop);
				
				//LocalMonitor_Business_Respones(msg_monitor->msg_source_id,msg_monitor->msg_type,0);
			break;
		
			
	}
}

/*------------------------------------------------------------------------
						BeCalled_TimerStop
------------------------------------------------------------------------*/
void DXMonitor_Timer_Stop(void)
{
	MONITOR_STRUCT	send_msg_to_monitor;	

	Mon_Run.timer++;	//��ʱ�ۼ�
	
	
	
	if((Mon_Run.timer >= Mon_Run.limit_time && Mon_Run.talk == MON_TALK_OFF) ||(Mon_Run.timer >= Mon_Run.limit_time && Mon_Run.talk == MON_TALK_ON))		
	{
		send_msg_to_monitor.msg_type = MON_TIMER_STOP;
		send_msg_to_monitor.address = NULL;			
		send_msg_to_monitor.command_type = NULL;
		if(push_vdp_common_queue(task_dxmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
		{
			Mon_Run.timer--;
			OS_RetriggerTimer(&timer_dxmonitor_stop);
		}
		else												//����ѹ��ɹ�
		{
			OS_StopTimer(&timer_dxmonitor_stop);	
		}
	}
	else
	{
		if(Mon_Run.timer > 2 && Mon_Run.state == MON_STATE_ON)
		{
		#if 0
			if(GetCurMenuCnt() != MENU_028_MONITOR2 && !GetMovementTestState())
			{
				send_msg_to_monitor.msg_type = MON_TIMER_STOP;
				send_msg_to_monitor.address = NULL;			
				send_msg_to_monitor.command_type = NULL;
				if(push_vdp_common_queue(task_localmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
				{
					Mon_Run.timer--;
					OS_RetriggerTimer(&timer_monitor_stop);
				}
				else												//����ѹ��ɹ�
				{
					OS_StopTimer(&timer_monitor_stop);	
				}
				return;
			}
		#endif
		}
		AutoPowerOffReset();
		OS_RetriggerTimer(&timer_dxmonitor_stop);
	}
	
}

/*==============================================================================
							Monitor_API
==============================================================================*/
/*------------------------------------------------------------------------
						API_MonitorTimeReset

����:	���Ӷ�ʱ��λ��ר����Ӱ����ʱʹ��

------------------------------------------------------------------------*/
unsigned char API_MonitorTimeReset(void)
{
    Mon_Run.timer = 0;
    return 0;
}
/*------------------------------------------------------------------------
						API_Monitor_Common
���:	��Ϣ����,���ӵ�ַ,ָ������

����:	��֯��Ϣ���͸�Monitor;  ������Ϣ���;����Ƿ�ȴ�Monitor��eventӦ��

����:
		0x00 = MonitorӦ��event = ���ܴ���Ϣ;
		0x01 = MonitorӦ��event = �����ܴ���Ϣ;
		0xff = Monitor����eventӦ��;
------------------------------------------------------------------------*/

uint8 API_DXMonitor_Common(uint8 msg_type_temp , uint16 address_temp , uint8 command_type_temp,int win_id)
{
	MONITOR_STRUCT	send_msg_to_monitor;	
	OS_TASK_EVENT MyEvents;	
	vdp_task_t* ptask = NULL;
	char rev[5];
	int   rev_len;
	#if !defined(PID_DX470)&&!defined(PID_DX482)
	return 0;
	#endif
	
	
	//��֯������Ϣ��Monitor
	send_msg_to_monitor.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg_to_monitor.msg_target_id	= MSG_ID_LocalMonitor;
	send_msg_to_monitor.msg_type = msg_type_temp;
	send_msg_to_monitor.msg_sub_type = 0;
	send_msg_to_monitor.address = address_temp; 
	send_msg_to_monitor.command_type = command_type_temp; 
	send_msg_to_monitor.win_id = win_id;
	
	//PrintCurrentTime(123123);
	if((msg_type_temp == MON_CONTROL_ON) || (msg_type_temp == MON_CONTROL_CANCEL) || (msg_type_temp == MON_CONTROL_CANCEL_FOR_FUNCTION) || (msg_type_temp == MON_CONTROL_OFF_MENU)||(msg_type_temp == MON_NEXT)||(msg_type_temp == MON_CONTROL_DXSLAVE_ON) )
	{
		ptask = GetTaskAccordingMsgID(send_msg_to_monitor.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				int wait_cnt=0;
				while(WaitForBusinessACK(ptask->p_syc_buf,msg_type_temp,rev,&rev_len,1) == 1&&wait_cnt++<100)
				{
					;
				}
			}
		}
	}

	//PrintCurrentTime(234234);
	
	if(push_vdp_common_queue(task_dxmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
	{
		if ((msg_type_temp == MON_CONTROL_ON)||(msg_type_temp == MON_CONTROL_CANCEL) ||(msg_type_temp == MON_CONTROL_DXSLAVE_ON)||(msg_type_temp == MON_MAINCALL_CLOSE))//|| (msg_type_temp == MON_CONTROL_CANCEL_FOR_FUNCTION) || (msg_type_temp == MON_CONTROL_OFF_MENU) )	// 20150506 ��eventӦ��
		{
			return (1);
		}
		else	//����eventӦ��
		{
			return (0xff);	
		}
	}

	//PrintCurrentTime(235235);
	
	//������Ϣ�����ж�,�Ƿ�ȴ�Monitor��eventӦ��
	if ((msg_type_temp == MON_CONTROL_ON)|| (msg_type_temp == MON_CONTROL_CANCEL)||(msg_type_temp == MON_NEXT)||(msg_type_temp == MON_CONTROL_DXSLAVE_ON)||(msg_type_temp == MON_MAINCALL_CLOSE) )//|| (msg_type_temp == MON_CONTROL_CANCEL_FOR_FUNCTION) || (msg_type_temp == MON_CONTROL_OFF_MENU) )	// 20150506 ��eventӦ��
	{
		if(ptask != NULL && ptask ->p_syc_buf != NULL)
		{
			
			//if(1/*WaitForBusinessACK(ptask->p_syc_buf,msg_type_temp,rev,&rev_len,4000) == 1*/)
			//if(send_msg_to_monitor.msg_source_id == MSG_ID_survey)
			{
				printf("task MSG_ID_survey!\n");
			
				if(WaitForBusinessACK(ptask->p_syc_buf,msg_type_temp,rev,&rev_len,4000) == 1)
				{
					//PrintCurrentTime(567567);
					if(rev_len >= 5)
					{
						if(rev[4] == 0)
						{
							return 0;
						}
					}
				}
			}
			#if 0
			else
			{
				//PrintCurrentTime(456456);
			
				printf(" NOT task MSG_ID_survey!\n");
				return 0;
			}
			#endif
		}

		//PrintCurrentTime(345345);

		return 1;
	}
	else	//����eventӦ��
	{
		return (0xff);	
	}
}

// lzh_20161103_s
int API_VideoTurnOn( int win )
{
	MONITOR_STRUCT	send_msg_to_monitor;	
	OS_TASK_EVENT MyEvents;	
	vdp_task_t* ptask = NULL;
	char rev[5];
	int   rev_len;

	
	
	//��֯������Ϣ��Monitor
	send_msg_to_monitor.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg_to_monitor.msg_target_id	= MSG_ID_LocalMonitor;
	send_msg_to_monitor.msg_type = MON_VI_OPEN;
	send_msg_to_monitor.msg_sub_type = win;
	if(send_msg_to_monitor.msg_source_id==MSG_ID_LocalMonitor)
	{
		API_LocalCaptureOn(win);
		return 0;	
	}
	if(push_vdp_common_queue(task_dxmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
	{
		return -1;
	}

	//API_LocalCaptureOn(win);
	return 0;	
}

int API_VideoTurnOff( void )
{
	MONITOR_STRUCT	send_msg_to_monitor;	
	OS_TASK_EVENT MyEvents;	
	vdp_task_t* ptask = NULL;
	char rev[5];
	int   rev_len;

	
	//��֯������Ϣ��Monitor
	send_msg_to_monitor.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg_to_monitor.msg_target_id	= MSG_ID_LocalMonitor;
	send_msg_to_monitor.msg_type = MON_VI_CLOSE;
	send_msg_to_monitor.msg_sub_type = 0;
	if(send_msg_to_monitor.msg_source_id==MSG_ID_LocalMonitor)
	{
		API_LocalCaptureOff();
		return 0;	
	}
	if(push_vdp_common_queue(task_dxmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
	{
		return -1;
	}

	//API_LocalCaptureOn(win);
	return 0;	
	//API_LocalCaptureOff();
	//return 0;	
}

int API_VideoTurnOffKeepPwr( void )
{
	MONITOR_STRUCT	send_msg_to_monitor;	
	OS_TASK_EVENT MyEvents;	
	vdp_task_t* ptask = NULL;
	char rev[5];
	int   rev_len;

	
	//��֯������Ϣ��Monitor
	send_msg_to_monitor.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg_to_monitor.msg_target_id	= MSG_ID_LocalMonitor;
	send_msg_to_monitor.msg_type = MON_VI_CLOSE_KEEPPWR;
	send_msg_to_monitor.msg_sub_type = 0;
	if(send_msg_to_monitor.msg_source_id==MSG_ID_LocalMonitor)
	{
		API_LocalCaptureOffKeepPower();
		return 0;	
	}
	if(push_vdp_common_queue(task_dxmon.p_msg_buf, (char *)&send_msg_to_monitor, sizeof(MONITOR_STRUCT)) != 0)	//����ѹ��ʧ��
	{
		return -1;
	}

	//API_LocalCaptureOn(win);
	return 0;	
	//API_LocalCaptureOff();
	//return 0;	
}
void API_FishEye_On(unsigned short addr )	//czn_20181117
{
   // if( dat[1] == partner_IA)
   //if(Mon_Run.state == MON_STATE_PHONE_ON)
    {
        //API_FishEyeSetMark(1);
   		//FishMode = 0;
 	//	API_FishEye_Mode(dat[1], 10);
    	//Display_FishEye_Sprite(FishMode);
    	API_DXMonitor_Common(MON_CONTROL_FISHEYE_ON, addr, NULL,NULL);
    }
    
}
void API_FishEye_Switch(void)	//czn_20181117
{
   // if( dat[1] == partner_IA)
   //if(Mon_Run.state == MON_STATE_PHONE_ON)
    {
        //API_FishEyeSetMark(1);
   		//FishMode = 0;
 	//	API_FishEye_Mode(dat[1], 10);
    	//Display_FishEye_Sprite(FishMode);
    	API_DXMonitor_Common(MON_CONTROL_FISHEYE_SWITCH, 0, NULL,NULL);
    }
    
}
void LocalMonitor_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	//if(respones_id != MSG_ID_survey)
	//	return;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL||ptask->p_syc_buf==NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}
int GetDXMonState(void)
{
	return Mon_Run.state;
}
int GetDXMonTalkState(void)
{
	return Mon_Run.talk;
}
time_t	linphone_mon_lasttime = 0;
time_t	local_mon_lasttime = 0;

void linphone_mon_lasttime_update(void)
{
	linphone_mon_lasttime = time(NULL);
}

void local_mon_lasttime_update(void)
{
	local_mon_lasttime = time(NULL);
}

int judge_local_mon_block(void)
{
	if(abs(local_mon_lasttime -time(NULL)) > 2)
	{
		return 0;
	}	
	return 1;
}
cJSON *dxMonReslist=NULL;
void load_DxMonRes_list(void)
{
	char *json_str;
	if(dxMonReslist==NULL)
	{
		json_str=GetJsonStringFromFile(CUSTOMERIAED_DXMonList);
		if(json_str!=NULL)
		{
			dxMonReslist=cJSON_Parse(json_str);
			free(json_str);
		}
	}
}
int Get_DxMonRes_Num_list(void)
{
	//load_DxMonRes_list();
	if(dxMonReslist==NULL)
		return -1;
	
	return cJSON_GetArraySize(dxMonReslist);
	
}
int Get_DxMonRes_Record_list(int index, char *pname, char *addr)
{
	cJSON *item;
	if(dxMonReslist==NULL)
		return -1;
	
	item=cJSON_GetArrayItem(dxMonReslist,index);
	if(item==NULL)
		return -1;
	if(pname)
	{
		strcpy(pname,cJSON_GetStringValue(cJSON_GetObjectItem(item,"Name")));
	}
	if(addr)
	{
		strcpy(addr,cJSON_GetStringValue(cJSON_GetObjectItem(item,"Addr")));
	}
	
	return 0;
}
int Get_DxMonRes_Num(void)
{
	#if (defined(PID_DX470)||defined(PID_DX482))
	int ret;
	if((ret=Get_DxMonRes_Num_list())>0)
	{
		return ret;
	}
	return 4;
	#else
	return 0;
	#endif
}
int Get_DxMonRes_Record(int index, char *pname, char *addr)
{
	if(Get_DxMonRes_Record_list(index,pname,addr)==0)
	{
		return 0;
	}
	if(pname!=NULL)
	{
		sprintf(pname,"DT-DS%d",index+1);
	}
	if(addr!=NULL)
	{
		sprintf(addr,"0x%x",DS1_ADDRESS+index);
	}
	return 0;
}

int Get_DxMonRes_Record2(int index, char *pname, char *addr)
{
#if 1
	
	if(Get_DxMonRes_Record_list(index,pname,addr)==0)
	{
		int len;
		int i;
		char name_temp2[41];
		if( GetMonFavByAddr(addr, NULL, name_temp2) == 1 )
		{
			if(strcmp(name_temp2, "-") !=0)
			strcpy(pname,name_temp2);
		}
		len = strlen(pname);
		for(i=0;i<len;i++)
		{
			if(pname[i]&0x80)
			{
				sprintf(pname,"DT-DS%d",strtoul(addr,NULL,0)-DS1_ADDRESS+1);
				break;
			}
		}
		return 0;
		
	}
#endif
	if(pname!=NULL)
	{
		sprintf(pname,"DT-DS%d",index+1);
	}
	if(addr!=NULL)
	{
		sprintf(addr,"0x%x",DS1_ADDRESS+index);
	}
	return 0;
}
int Get_DxMonRes_Name_ByAddr(uint16 dev_addr,char *pname)
{
	int i;
	char cmp_addr[10];
	char name[41];
	for(i=0;i<Get_DxMonRes_Num();i++)
	{
		Get_DxMonRes_Record(i,name,cmp_addr);
		if(dev_addr==strtoul(cmp_addr,NULL,0))
		//if(strcmp(addr,cmp_addr)==0)
		{
			if(pname!=NULL)
			{
				strcpy(pname,name);
			}
			return 0;
		}
	}
	return -1;
}

int Get_DxMonTalkState(void)
{
	return Mon_Run.talk;
}
cJSON *dxNamelist=NULL;
void load_DxNamelist_list(void)
{
	char *json_str;
	if(dxNamelist==NULL)
	{
		json_str=GetJsonStringFromFile(CUSTOMERIAED_DXNameList);
		if(json_str!=NULL)
		{
			dxNamelist=cJSON_Parse(json_str);
			free(json_str);
		}
	}
}
int Get_DxNamelist_Num_list(void)
{
	//load_DxMonRes_list();
	if(dxNamelist==NULL)
		return -1;
	
	return cJSON_GetArraySize(dxNamelist);
	
}
int Get_DxNamelist_Record_list(int index, char *pname, char *addr)
{
	cJSON *item;
	if(dxNamelist==NULL)
		return -1;
	
	item=cJSON_GetArrayItem(dxNamelist,index);
	if(item==NULL)
		return -1;
	if(pname)
	{
		strcpy(pname,cJSON_GetStringValue(cJSON_GetObjectItem(item,"Name")));
	}
	if(addr)
	{
		strcpy(addr,cJSON_GetStringValue(cJSON_GetObjectItem(item,"Addr")));
	}
	
	return 0;
}
int Get_DxNamelist_Num(void)
{
	//#ifdef PID_DX470
	#if (defined(PID_DX470)||defined(PID_DX482))
	int ret;
	if((ret=Get_DxNamelist_Num_list())>0)
	{
		return ret;
	}
	return 32;
	#else
	return 0;
	#endif
}
int Get_DxNamelist_Record(int index, char *pname, char *addr)
{
	if(Get_DxNamelist_Record_list(index,pname,addr)==0)
	{
		return 0;
	}
	if(pname!=NULL)
	{
		sprintf(pname,"DT-IM%d",index+1);
	}
	if(addr!=NULL)
	{
		sprintf(addr,"0x%x",index==31?0x80:(0x84+(index<<2)));
	}
	return 0;
}

int Get_DxNamelist_Name_ByAddr(uint16 dev_addr,char *pname)
{
	int i;
	char cmp_addr[10];
	char name[41];
	IM_CookieRecord_T cRecord;
	if(dev_addr==0x3c)
	{
		short unicode_buff[40];
		API_GetOSD_StringWithID2(MESG_TEXT_Icon_014_GuardStation, unicode_buff, &i);
		unicode2utf8(unicode_buff,i/2,pname);
		return 0;	
	}
	for(i=0;i<Get_DxNamelist_Num();i++)
	{
		Get_DxNamelist_Record(i,name,cmp_addr);
		if(dev_addr==strtoul(cmp_addr,NULL,0))
		//if(strcmp(addr,cmp_addr)==0)
		{
			if( !ImGetCookieItemByAddr(cmp_addr, &cRecord) )
			{
				if(strcmp(cRecord.NickName, "-"))
				{
					strcpy(name, cRecord.NickName);
				}
			}
			if(pname!=NULL)
			{
				strcpy(pname,name);
			}
			return 0;
		}
	}
	return -1;
}
int LinphoneDxMonitorDsStart(char *addr)
{
	int ds_addr = strtoul(addr,NULL,0);
	if(GetDXMonState()==0)
	{
		API_Event_DXMonitor_On(ds_addr,0);
		API_OpenDxRtpVideo();//api_ak_vi_ch_stream_encode(1,1);
		trigger_send_key_frame_delay(1,8);
	}
	else
	{
		if(GetDXMonTalkState())
			return 1;
		API_DXMonitor_PhoneNext(ds_addr);
	}
	return 0;
}
int LinphoneDxMonitorDsStartWithoutRtp(char *addr)
{
	int ds_addr = strtoul(addr,NULL,0);
	if(GetDXMonState()==0)
	{
		API_Event_DXMonitor_On(ds_addr,0);
		//API_OpenDxRtpVideo();//api_ak_vi_ch_stream_encode(1,1);
		trigger_send_key_frame_delay(1,8);
	}
}
int LinphoneDxMonitorDsStop(void)
{
	API_DXMonitor_Off();
	API_CloseDxRtpVideo();//api_ak_vi_ch_stream_encode(1,0);
	//API_Business_Close(Business_State_MonitorDs);
	return 0;
}
int LinphoneDxMonitorDsTalkStart(void)
{
	char tempData[5];
	if(API_Event_IoServer_InnerRead_All(APP_MON_TALK_EN, tempData)==0)
	{
		if(atoi(tempData))
		{
			API_DXMonitor_Phone_Talk_On();
			return 0;
		}
	}
	return -1;
}
#if 0
void video_block_for_linphone_mon(void)
{
	if(Mon_Run.state == MON_STATE_PHONE_ON || Mon_Run.state == MON_STATE_PHONE_NEXT)
	{
		API_LocalMonitor_Off();
	}
	if(abs(linphone_mon_lasttime - time(NULL)) < 3 )
	{
		usleep(3000000);
	}
	
	dxdev_start_video();		//czn_20170922
	
}	

void API_FishEye_PhoneMonitor_Process(unsigned char* dat)	//czn_20181117
{
   // if( dat[1] == partner_IA)
   //if(Mon_Run.state == MON_STATE_PHONE_ON)
    {
        //API_FishEyeSetMark(1);
   		//FishMode = 0;
 	//	API_FishEye_Mode(dat[1], 10);
    	//Display_FishEye_Sprite(FishMode);
    	API_LocalMonitor_Common(MON_CONTROL_FISHEYE_ON, dat[1], NULL,NULL);
    }
    
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
