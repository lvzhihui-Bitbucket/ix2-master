
#ifndef _ota_client_ifc_h_
#define _ota_client_ifc_h_

#include "unix_socket.h"
#include "task_survey.h"

#define FTP_INFO_STATE				"State"
#define FTP_INFO_FileName			"FileName"
#define FTP_INFO_FileDir			"FileDir"
#define FTP_INFO_FileLen			"FileLen"
#define FTP_INFO_DlLen				"DlLen"

#define		IFC_DOWNLOAD_STATE_NONE				0
#define		IFC_DOWNLOAD_STATE_CONNECTING		1
#define		IFC_DOWNLOAD_STATE_CONNECTED		2
#define		IFC_DOWNLOAD_STATE_TRANSLATING		3
#define		IFC_DOWNLOAD_STATE_TRANSLATED		4

#define 	OtaClientIfc_Msg_StartDownload		0
#define 	OtaClientIfc_Msg_CancelDownload		1

// lzh_20201201_s
#define 	OtaClientIfc_Msg_Startftp			2
#define 	OtaClientIfc_Msg_Cancelftp			3
// lzh_20201201_e

#define RES_DOWNLOAD_STATE_NO_SDCARD			"NO_SDCARD"
#define RES_DOWNLOAD_STATE_CHECK_OK				"CHECK_OK"
#define RES_DOWNLOAD_STATE_INSTALLING			"INSTALLING"
#define RES_DOWNLOAD_STATE_INSTALL_OK			"INSTALL_OK"
#define RES_DOWNLOAD_STATE_INSTALL_FAIL			"INSTALL_FAIL"
#define RES_DOWNLOAD_STATE_DOWNLOADING			"DOWNLOADING"
#define RES_DOWNLOAD_STATE_CONNECTING			"CONNECTING"
#define RES_DOWNLOAD_STATE_REBOOT				"REBOOT"
#define RES_DOWNLOAD_STATE_WAITING_REBOOT		"WAITING_REBOOT"
#define RES_DOWNLOAD_STATE_CANCEL				"CANCEL"

#define RES_DOWNLOAD_STATE_ERR_TYPE				"ERR_TYPE"
#define RES_DOWNLOAD_STATE_ERR_NO_FILE			"ERR_NO_FILE"
#define RES_DOWNLOAD_STATE_ERR_CODE				"ERR_CODE"
#define RES_DOWNLOAD_STATE_ERR_NO_SPASE			"ERR_NO_SPASE"
#define RES_DOWNLOAD_STATE_ERR_CONNECT			"ERR_CONNECT"
#define RES_DOWNLOAD_STATE_ERR_DOWNLOAD			"ERR_DOWNLOAD"

#define  OTA_TXT_INFO_STR_LEN	20

typedef struct
{	
	char			fw_info[OTA_TXT_INFO_STR_LEN];
	char			bref_info[OTA_TXT_INFO_STR_LEN];
	int 			file_length;
	char			app_code[OTA_TXT_INFO_STR_LEN];
	char			app_ver[OTA_TXT_INFO_STR_LEN];
	char			bsp_ver[OTA_TXT_INFO_STR_LEN];
	char			res_ver[OTA_TXT_INFO_STR_LEN];
	char			resCode[OTA_TXT_INFO_STR_LEN];
} OtaTxtInfo_t;


typedef enum
{
	OtaClientIfc_StartConnectting = 0,
	OtaClientIfc_NoSdcard,
	OtaClientIfc_NoSpace,
	OtaClientIfc_StartActOk,
	OtaClientIfc_StartActError,
	OtaClientIfc_CancelActOk,
	OtaClientIfc_CancelActError,
	OtaClientIfc_Translating,
	OtaClientIfc_Translated,
	OtaClientIfc_TranslateError,
}OtaClientReportType;

typedef enum
{
	OtaDlTypeTcp = 0,
	OtaDlTypeFtp,
}OtaDlType;



typedef struct
{	
	int				file_type;
	char			file_code[15];
	char			server_ip[16];
	char			file_dir[60];
	char			dev_type[10];
	char			dev_ver[60];
	char			dev_id[13];

	int 			space;
	OtaDlType		downloadType;
	int				reportIpCnt;
	int				reportIp[10];
} OtaClientPara;

typedef struct 
{
	OtaClientReportType		type;
	int						result;
	int 					translated_len;
	int 					file_len;
	char					file_name[64];
	char					resCode[9];
}OtaClientReport_T;


typedef struct
{	
	VDP_MSG_HEAD 	head;
	int				file_type;
	char				file_code[9];
	char				server_ip[16];
	char				file_dir[60];
	char				dev_type[10];
	char				dev_ver[60];
	char				dev_id[13];
} MSG_OtaClientIfc;

typedef struct
{	
	VDP_MSG_HEAD 	head;
	OtaClientPara	para;
} MSG_OtaClientIfc_2;


void comm_service_socket_send_data( char* pbuf, int len );

void ota_client_ifc_init(void);
int ota_ifc_file_download(char *server_ip,char *file_code,int file_type,char *dev_type,char *dev_ver,char *dev_id);


int API_OtaIfcFtpFileDownload2(OtaClientPara para);
int API_OtaIfcFtpCancel2(void);
static void OtaReportState(char* state, char* msg);

#endif




