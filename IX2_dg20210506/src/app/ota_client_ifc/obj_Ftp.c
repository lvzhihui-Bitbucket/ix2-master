/**
  ******************************************************************************
  * @file    obj_Ftp.c
  * @author  czb
  * @version V00.01.00
  * @date    2023.5.10
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include "obj_Ftp.h"   
#include "cJSON.h"   
#include "utility.h"

static size_t download_files(void *buffer, size_t size, size_t nmemb, void *stream)
{
	FTP_FILE *out = (FTP_FILE* )stream;
	if (out && !out->stream) 
	{
		if( out->store_filelen == 0 )
		{
			// open writable file, file len set 0 
			printf("store filename fopen with wb\n");
			out->stream = fopen(out->store_filename, "wb");
		}
		else
		{
			// open exist file, with update mode
			printf("store filename fopen with ab+\n");
			out->stream = fopen(out->store_filename, "ab+");
		}
		if (!out->stream)
		{
			printf("store filename fopen error\n");
			return -1; // failure, can't open file to write
		}
	}
	return fwrite(buffer, size, nmemb, out->stream);
}

static int Download_progress_func(char *progress_data, double dlTotal, /* dltotal */  double dlNow, /* dlnow */ double ulTotal, double ulNow)  
{ 
	int ret;
    FTP_Run_S *fptRun = (FTP_Run_S*)progress_data;

	if(fptRun->state)
	{
		ret = 0;

		static struct timeval time_old={0};
		int now,all;
		now = (int)dlNow;
		all = (int)dlTotal;
		double rate = now*100.0/all;
		static int now_old = 0;
		if( now_old != now )
		{
			now_old = now;
			struct timeval t1;
			gettimeofday(&t1,NULL);
			if( t1.tv_sec != time_old.tv_sec )
			{
				time_old = t1;
				if(fptRun->callback)
				{
					FTP_FILE_STATISTICS statistics;
					statistics.now = now;
					statistics.total = all;
					ret = (fptRun->callback)(FTP_STATE_DOWN_ING, statistics, fptRun->callbackData);
				}
				printf("FTP download %s: %u/%u(%u%%)\n", fptRun->storeFileName, now, all, (int)rate); 
				fflush(stdout); 
			}
		}
	}
	else
	{
		printf("FTP download stop %s\n", fptRun->storeFileName); 
		ret = 1;
	}

	return ret;
}

static int Upload_progress_func(char *progress_data, double dlTotal, /* dltotal */  double dlNow, /* dlnow */ double ulTotal, double ulNow)  
{ 
	int ret;
    FTP_Run_S *fptRun = (FTP_Run_S*)progress_data;

	if(fptRun->state)
	{
		ret = 0;

		static struct timeval time_old={0};
		int now,all;
		now = (int)ulNow;
		all = (int)ulTotal;
		double rate = now*100.0/all;
		static int now_old = 0;
		if( now_old != now )
		{
			now_old = now;
			struct timeval t1;
			gettimeofday(&t1,NULL);
			if( t1.tv_sec != time_old.tv_sec )
			{
				time_old = t1;
				if(fptRun->callback)
				{
					FTP_FILE_STATISTICS statistics;
					statistics.now = now;
					statistics.total = all;
					ret = (fptRun->callback)(FTP_STATE_UP_ING, statistics, fptRun->callbackData);
				}
				printf("FTP upload %s: %u/%u(%u%%)\n", fptRun->filename, now, all, (int)rate); 
				fflush(stdout); 
			}
		}
	}
	else
	{
		printf("FTP upload stop %s\n", fptRun->filename); 
		ret = 1;
	}

	return ret;
}


static int FTP_Download(char* server, char* filename, char* storeFileName, void* progressData)
{
	int ret;
	CURL *curl;
	CURLcode res;
	char usr[100];
	char len[20];
	FTP_FILE ftpfile;
	struct stat filestat;
    FTP_Run_S *fptRun = (FTP_Run_S*)progressData;
	FTP_FILE_STATISTICS statistics;
	int state;

	if( server == NULL || filename == NULL || storeFileName == NULL)
	{
		printf("FTP download parameter error.\n");
		if(fptRun->callback)
		{
			(fptRun->callback)(FTP_STATE_PARA_ERR, statistics, fptRun->callbackData);
		}
		return -1;
	}

	strcpy(ftpfile.ftp_server, server);
	strcpy(ftpfile.ftp_filename, filename);
	strcpy(ftpfile.store_filename, storeFileName);
	

	sprintf( ftpfile.ftp_url,"ftp://%s/%s", server, filename);
	
	if( stat(ftpfile.store_filename,&filestat) == -1 )
	{
		printf("file is new!\n");
		ftpfile.store_filelen = 0;
	}
	else
	{
		printf("file is exist,size=[%d]\n",filestat.st_size);
		ftpfile.store_filelen = filestat.st_size;
	}

	ftpfile.stream = NULL;

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	if (curl) 
	{
		snprintf(usr, 100, "%s:%s", FTP_DEFAULT_USERNAME, FTP_DEFAULT_PASSWORD);
		snprintf(len, 20, "%d-", ftpfile.store_filelen);
		
		curl_easy_setopt(curl, CURLOPT_URL,ftpfile.ftp_url);
		curl_easy_setopt(curl, CURLOPT_USERPWD, usr);
		
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT,15); 
		curl_easy_setopt(curl, CURLOPT_TIMEOUT,7200);	// download max time 
		//curl_easy_setopt(curl, CURLOPT_RESUME_FROM,len); 

		curl_easy_setopt(curl, CURLOPT_RANGE,len); 
		// curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)1000); 

		// Define our callback to get called when there's data to be written //
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, download_files);
		// Set a pointer to our struct to pass to the callback //
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);

		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
		curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, Download_progress_func);  
		curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, progressData);

		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 10);  	// limit 10bytes per second
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10);	// continue 10s with the limit status

		// Switch on full protocol/debug output //
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		res = curl_easy_perform(curl);

		// always cleanup 
		curl_easy_cleanup(curl);

		printf("FTP download CURLcode = %d.\n", res);
		switch (res)
		{
			case CURLE_ABORTED_BY_CALLBACK:
				ret = 3;
				break;

			case CURLE_OK:
				ret = 0;
				break;

			default:
				ret = 2;
				break;
		}
	}
	else
	{
		ret = 1;
	}

	if(ftpfile.stream) fclose(ftpfile.stream); // close the local file 
	ftpfile.stream = NULL;

	curl_global_cleanup();

	switch (ret)
	{
		case 0:
			printf("FTP download %s ok.\n", ftpfile.store_filename);
			state = FTP_STATE_DOWN_OK;
			break;
		case 1:
			printf("FTP download init error.\n");
			state = FTP_STATE_INIT_ERR;
			break;
		case 2:
			printf("FTP download %s error.\n", ftpfile.store_filename);
			state = FTP_STATE_DOWN_ERR;
			break;
		case 3:
			printf("FTP download callback stop.\n");
			state = FTP_STATE_DOWN_STOP;
			break;

		default:
			printf("FTP download %s error.\n", ftpfile.store_filename);
			state = FTP_STATE_DOWN_ERR;
			ret = 2;
			break;
	}
	
	if(fptRun->callback)
	{
		(fptRun->callback)(state, statistics, fptRun->callbackData);
	}

	return ret;
}

static cJSON* FTP_Data = NULL;

static void* FTP_DownProcess(void* arg)
{
    FTP_Run_S *fptRun = (FTP_Run_S*)arg;
	pthread_detach(pthread_self());

	FTP_Download(fptRun->server, fptRun->filename, fptRun->storeFileName, fptRun);

	cJSON_DeleteItemFromObjectCaseSensitive(FTP_Data, fptRun->storeFileName);
	free(fptRun);
}

//返回1：成功，0：失败
int FTP_StartDownload(char* server, char* filename, char* store_dir, void* cbData,  FTP_CB cb)
{
	int ret = 0;
	cJSON* elemnt;
	FTP_Run_S *fptRun;
	char storeFileName[200];
	char data[200];
	char* name, *pos1;
	int timeCnt = 0;

	if( server==NULL || filename==NULL)
	{
		dprintf("FTP download parameter orror.\n");
		return ret;
	}

	strcpy(data, filename);
	for(pos1 = strtok(data, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
	sprintf(storeFileName, "%s/%s", (store_dir ? store_dir : DEFAULT_FTP_DOWNLOAD_DIR), name);

	if(FTP_Data == NULL)
	{
		FTP_Data = cJSON_CreateObject();
	}

	while(elemnt = cJSON_GetObjectItemCaseSensitive(FTP_Data, storeFileName))
	{
		fptRun = elemnt->valueint;
		fptRun->state = 0;
		usleep(100*1000);
		//如果大于5秒还不能结束相同的下载进程，则开启新下载失败。
		if(++timeCnt > 5000/100)
		{
			dprintf("FTP download %s existed.\n", storeFileName);
			return ret;
		}
	}

	fptRun = malloc(sizeof(FTP_Run_S));

	if(fptRun)
	{
		fptRun->state = 1;
		strcpy(fptRun->server, server);
		strcpy(fptRun->filename, filename);
		strcpy(fptRun->storeFileName, storeFileName);

		fptRun->callbackData = cbData;
		fptRun->callback = cb;

		if(pthread_create(&fptRun->tid, NULL, FTP_DownProcess, (void*)fptRun))
		{
			//返回失败
			free(fptRun);
			dprintf("FTP download %s pthread create error.\n", storeFileName);
		}
		else
		{
			//返回成功
			cJSON_AddNumberToObject(FTP_Data, storeFileName, (int)fptRun);
			ret = 1;
		}
	}

	return ret;
}


//返回1：成功，0：失败
int FTP_StopDownload(char* filename, char* store_dir)
{
	int ret = 100;
	char storeFileName[200];
	FTP_Run_S *fptRun;
	cJSON* elemnt;
	char data[200];
	char* name, *pos1;

	strcpy(data, filename);
	for(pos1 = strtok(data, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
	sprintf(storeFileName, "%s/%s", (store_dir ? store_dir : DEFAULT_FTP_DOWNLOAD_DIR), name);

	while((elemnt = cJSON_GetObjectItemCaseSensitive(FTP_Data, storeFileName)) != NULL && ret > 0)
	{
		fptRun = elemnt->valueint;
		fptRun->state = 0;
		usleep(10*1000);
		ret --;
	}

	return ret;
}


static int FTP_Upload(char* server, char* filename, char* storeFileName, void* progressData)
{
	int ret=0;
	CURL *curl;
	CURLcode res;
	char usr[100];
	char len[20];
	FTP_FILE ftpfile;
    FTP_Run_S *fptRun = (FTP_Run_S*)progressData;
	FTP_FILE_STATISTICS statistics;
	int state;

	if( server == NULL || filename == NULL || storeFileName == NULL || (ftpfile.stream = fopen(filename, "r")) == NULL)
	{
		if(fptRun->callback)
		{
			(fptRun->callback)(FTP_STATE_PARA_ERR, statistics, fptRun->callbackData);
		}
		return -1;
	}

	strcpy(ftpfile.ftp_server, server);
	strcpy(ftpfile.ftp_filename, filename);
	strcpy(ftpfile.store_filename, storeFileName);
	sprintf( ftpfile.ftp_url,"ftp://%s/%s/%s", server, FTP_DEFAULT_UPLOAD_DIR, storeFileName);

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	if (curl) 
	{
		snprintf(usr, 100, "%s:%s", FTP_DEFAULT_USERNAME, FTP_DEFAULT_PASSWORD);
		
		curl_easy_setopt(curl, CURLOPT_URL,ftpfile.ftp_url);
		curl_easy_setopt(curl, CURLOPT_USERPWD, usr);
		
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT,15); 
		curl_easy_setopt(curl, CURLOPT_TIMEOUT,7200);	// download max time 
		//curl_easy_setopt(curl, CURLOPT_RESUME_FROM,len); 

		curl_easy_setopt(curl, CURLOPT_READDATA, ftpfile.stream);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);

		fseek(ftpfile.stream, 0L, SEEK_END);
		curl_easy_setopt(curl, CURLOPT_INFILESIZE, ftell(ftpfile.stream));
		fseek(ftpfile.stream, 0L, SEEK_SET);

		curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS, 1);

		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
		curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, Upload_progress_func);  
		curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, progressData);

		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 10);  	// limit 10bytes per second
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10);	// continue 10s with the limit status

		// Switch on full protocol/debug output //
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		res = curl_easy_perform(curl);

		// always cleanup 
		curl_easy_cleanup(curl);

		printf("FTP download CURLcode = %d.\n", res);
		switch (res)
		{
			case CURLE_ABORTED_BY_CALLBACK:
				ret = 3;
				break;

			case CURLE_OK:
				ret = 0;
				break;

			default:
				ret = 2;
				break;
		}
	}
	else
	{
		ret = 1;
	}

	if(ftpfile.stream) fclose(ftpfile.stream); // close the local file 

	curl_global_cleanup();

	switch (ret)
	{
		case 0:
			printf("FTP upload %s ok.\n", ftpfile.ftp_filename);
			state = FTP_STATE_UP_OK;
			break;
		case 1:
			printf("FTP upload init error.\n");
			state = FTP_STATE_INIT_ERR;
			break;
		case 2:
			printf("FTP upload %s error.\n", ftpfile.ftp_filename);
			state = FTP_STATE_UP_ERR;
			break;
		case 3:
			printf("FTP upload callback stop.\n");
			state = FTP_STATE_UP_STOP;
			break;

		default:
			printf("FTP upload %s error.\n", ftpfile.ftp_filename);
			state = FTP_STATE_UP_ERR;
			ret = 2;
			break;
	}
	
	if(fptRun->callback)
	{
		(fptRun->callback)(state, statistics, fptRun->callbackData);
	}

	return ret;
}

static void* FTP_UpProcess(void* arg)
{
    FTP_Run_S *fptRun = (FTP_Run_S*)arg;
	char uploadListKey[200];
	pthread_detach(pthread_self());

	FTP_Upload(fptRun->server, fptRun->filename, fptRun->storeFileName, fptRun);

	sprintf(uploadListKey, "%s:%s", fptRun->server, fptRun->storeFileName);

	cJSON_DeleteItemFromObjectCaseSensitive(FTP_Data, uploadListKey);
	free(fptRun);
}

//返回1：成功，0：失败
int FTP_StartUpload(char* server, char* filename, char* store_dir, void* cbData,  FTP_CB cb)
{
	int ret = 0;
	cJSON* elemnt;
	FTP_Run_S *fptRun;
	char uploadListKey[200];
	char data[200];
	char* name, *pos1;
	struct stat filestat;

	if( server==NULL || filename==NULL || stat(filename, &filestat) == -1 )
	{
		return ret;
	}

	strcpy(data, filename);
	for(pos1 = strtok(data, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
	sprintf(uploadListKey, "%s:%s/%s", server, (store_dir ? store_dir : ""), name);

	if(FTP_Data == NULL)
	{
		FTP_Data = cJSON_CreateObject();
	}

	while(elemnt = cJSON_GetObjectItemCaseSensitive(FTP_Data, uploadListKey))
	{
		fptRun = elemnt->valueint;
		fptRun->state = 0;
		usleep(100*1000);
	}

	fptRun = malloc(sizeof(FTP_Run_S));

	if(fptRun)
	{
		fptRun->state = 1;
		strcpy(fptRun->server, server);
		strcpy(fptRun->filename, filename);
		sprintf(fptRun->storeFileName, "%s/%s", (store_dir ? store_dir : ""), name);
		fptRun->callbackData = cbData;
		fptRun->callback = cb;

		if(pthread_create(&fptRun->tid, NULL, FTP_UpProcess, (void*)fptRun))
		{
			//返回失败
			free(fptRun);
			ret = 0;
		}
		else
		{
			//返回成功
			cJSON_AddNumberToObject(FTP_Data, uploadListKey, (int)fptRun);
			ret = 1;
		}
	}

	return ret;
}


//返回1：成功，0：失败
int FTP_StopUpload(char* server, char* filename, char* store_dir)
{
	int ret = 100;
	cJSON* elemnt;
	char data[200];
	char* name, *pos1;
	char uploadListKey[200];

	strcpy(data, filename);
	for(pos1 = strtok(data, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
	sprintf(uploadListKey, "%s:%s/%s", server, (store_dir ? store_dir : ""), name);

	while((elemnt = cJSON_GetObjectItemCaseSensitive(FTP_Data, uploadListKey)) != NULL && ret > 0)
	{
		FTP_Run_S *fptRun;
		fptRun = elemnt->valueint;
		fptRun->state = 0;
		usleep(10*1000);
		ret --;
	}

	return ret;
}

int FTP_Url(char* server, char*dir, char* cmd)
{
	int ret=0;
	CURL *curl;
	CURLcode res;
	char usr[100];
	char url[200];

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	if (curl) 
	{
		sprintf(url,"ftp://%s/%s/%s", server, FTP_DEFAULT_UPLOAD_DIR, dir ? dir : "");
		snprintf(usr, 100, "%s:%s", FTP_DEFAULT_USERNAME, FTP_DEFAULT_PASSWORD);
		struct curl_slist *plist = curl_slist_append(NULL, cmd); 
		
		printf("FTP url:%s cmd:%s\n", url, cmd);

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_USERPWD, usr);

		curl_easy_setopt(curl, CURLOPT_QUOTE, plist);
		
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT,15); 
		curl_easy_setopt(curl, CURLOPT_TIMEOUT,7200);	// download max time 
		//curl_easy_setopt(curl, CURLOPT_RESUME_FROM,len); 


		// Switch on full protocol/debug output //
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		res = curl_easy_perform(curl);

		curl_slist_free_all(plist);
		// always cleanup 
		curl_easy_cleanup(curl);

		if (CURLE_OK != res) 
		{
			ret = 2;
		}
	}
	else
	{
		ret = 1;
	}

	curl_global_cleanup();

	if(ret == 0)
	{
		printf("FTP url %s ok.\n", url);
	}

	return ret;
}
