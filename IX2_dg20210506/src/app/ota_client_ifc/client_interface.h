
#ifndef _CLIENT_INTERFACE_
#define _CLIENT_INTERFACE_

#define OTAC_IF_UNIX_SOCKET_FILE 	"/tmp/otac_if_socket"

#define OTAC_IF_SET_CONNECT_SERVER			0x0001
#define OTAC_IF_SET_DISCONNECT_SERVER		0x0002
#define OTAC_IF_GET_SERVER_DISCONNECTED		0x0003

#define OTAC_IF_SET_DOWNLOAD_START			0x0004
#define OTAC_IF_SET_DOWNLOAD_STOP			0x0005
#define OTAC_IF_GET_DOWNLOAD_RESULT			0x0006

#define OTAC_IF_NOTIFY_DOWNLOAD_STATUS		0x0007

#define	FS_DOWNLOAD_RESULT_OK							0
#define FS_FOWNLOAD_RESULT_CLIENT_CREATE_FILE_FAIL		1
#define FS_FOWNLOAD_RESULT_CLIENT_WRITE_FILE_FAIL		2
#define FS_FOWNLOAD_RESULT_CLIENT_WAIT_TIMEOUT			3
#define FS_FOWNLOAD_RESULT_CLIENT_ABORT					4
#define FS_FOWNLOAD_RESULT_CLIENT_CHECK_MD5_ERROR		5
#define FS_FOWNLOAD_RESULT_SERVER_NOTIFY_NO_FILE		6
#define FS_FOWNLOAD_RESULT_SERVER_NOTIFY_ABORT			7
#define FS_FOWNLOAD_RESULT_SERVER_NOTIFY_ERROR			8

typedef struct 
{
	char				server_str[16];
} req_server_connect;

typedef struct 
{
	unsigned char		file_code[6];
	unsigned int		file_type;	
	unsigned char		device_type[20];
	unsigned char		device_ver[20];	
	unsigned char		device_id[16];
	unsigned char		file_dir[64];	//czn_20181205
} req_file_download;


typedef struct 
{
	unsigned int		cmd;
	union
	{
		req_server_connect	server_connect;
		req_file_download	file_download;		
	}req;
} otac_if_request_t;

typedef struct 
{
	unsigned int		cmd;
	unsigned int		state;
	unsigned int		result;
	//czn_20181205_s
	union			
	{
		int			translated_len;
		int			download_result;
	};
	
	unsigned char		file_name[64];
	//czn_20181205_e
} otac_if_response_t;

int init_comm_service( void );
int api_ifc_set_connect_server( char* server_ip_str );
int api_ifc_set_disconnect_server( void );
int api_ifc_get_server_status( void );
int api_ifc_set_download_start(char *file_dir,char* file_code, int file_type, unsigned char* device_type, unsigned char* device_ver, unsigned char* device_id );
int api_ifc_set_download_stop( void );
int api_ifc_get_download_result( void );

#endif


