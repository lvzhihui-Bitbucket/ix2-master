

#include <dirent.h>
#include "utility.h"
#include "unix_socket.h"
#include "client_interface.h"
#include "ota_client_ifc.h"
#include "task_survey.h"
#include "task_VideoMenu.h"
#include "ftp_download.h"
#include "ota_FileProcess.h"
#include "cJSON.h"


Loop_Interactive_Buffer comm_cmd_queue;

 sem_t				ifc_sem;
 pthread_mutex_t	ifc_lock;
 int				ifc_state;
 
 otac_if_request_t	gl_ifc_req;
 otac_if_response_t	gl_ifc_rsp;

void otac_udp_data_process( char* pbuf, int len )
{
	otac_if_response_t*	ptr_rsp = (otac_if_response_t*)pbuf;
	pthread_mutex_lock(&ifc_lock);
	gl_ifc_rsp = *ptr_rsp;
	//printf("socket recv cmd=%d, result = %d...\n",gl_ifc_rsp.cmd, gl_ifc_rsp.result);			
	pthread_mutex_unlock(&ifc_lock);
	sem_post(&ifc_sem);
}
void ota_client_dir_init(void)
{
	
}
void ota_client_ifc_init(void)
{
	sem_init(&ifc_sem,0,0);
	pthread_mutex_init(&ifc_lock, 0);

	//ota_client_dir_init();
	
	// 创建otac进程服务socket通道
	init_comm_service();
}

int ota_ifc_file_download_step1(OtaClientPara para)
{
	DIR *dir;
	
	if(MakeDir(para.file_dir) != 0)
	{
		return -1;
	}
	
	if(ifc_state != IFC_DOWNLOAD_STATE_NONE)
	{
		sem_wait_ex2(&ifc_sem, 1);
		api_ifc_set_disconnect_server();			
		if( sem_wait_ex2(&ifc_sem, 2000) != 0 )
		{
			printf("have no response3, contiue download...\n");				
			ifc_state = IFC_DOWNLOAD_STATE_CONNECTED;							
		}
		else
		{
			printf("stop download ok!!!...\n");						
			ifc_state = IFC_DOWNLOAD_STATE_NONE;							
		}
	}
	
	if(ifc_state != IFC_DOWNLOAD_STATE_NONE)
		return -1;

	sem_wait_ex2(&ifc_sem, 1);
	api_ifc_set_connect_server(para.server_ip);
	ifc_state = IFC_DOWNLOAD_STATE_CONNECTING;

	if( sem_wait_ex2(&ifc_sem, 10000) != 0 )
	{
		printf("have no response1...\n");		
		ifc_state = IFC_DOWNLOAD_STATE_NONE;
	}
	else
	{				
		pthread_mutex_lock(&ifc_lock);				

		printf("cmd=%d, result = %d...\n",gl_ifc_rsp.cmd, gl_ifc_rsp.result);		
		
		if( gl_ifc_rsp.cmd == OTAC_IF_SET_CONNECT_SERVER && gl_ifc_rsp.result == 0 )
		{
			ifc_state = IFC_DOWNLOAD_STATE_CONNECTED;
		}
		else
		{
			ifc_state = IFC_DOWNLOAD_STATE_NONE;
		}
		pthread_mutex_unlock(&ifc_lock);					
	}
	
	if(ifc_state != IFC_DOWNLOAD_STATE_CONNECTED)
		return -2;
	sem_wait_ex2(&ifc_sem, 1);
	api_ifc_set_download_start(para.file_dir,para.file_code,para.file_type,para.dev_type,para.dev_ver,para.dev_id);

	if( sem_wait_ex2(&ifc_sem, 10000) != 0 )
	{
		printf("have no response2...\n");		
		ifc_state = IFC_DOWNLOAD_STATE_NONE;	
		return -2;
	}
	else if( gl_ifc_rsp.cmd == OTAC_IF_SET_DOWNLOAD_START  && gl_ifc_rsp.result == 0 )
	{
		ifc_state = IFC_DOWNLOAD_STATE_TRANSLATING;
		printf("download start... file_len = %d\n",gl_ifc_rsp.translated_len);		
	}
	else
	{
		ifc_state = IFC_DOWNLOAD_STATE_NONE;
		printf("connect error, exit...\n");							
	}

	if(ifc_state != IFC_DOWNLOAD_STATE_TRANSLATING)
		return -3;

	return 0;

	
}

int ota_ifc_file_download_step2(int *translated_len)	//-4:download error,0,no_tranfer,1 transfering 2 tranfer_ok
{
	//while(1)
	if(ifc_state != IFC_DOWNLOAD_STATE_TRANSLATING)
		return 0;
	sem_wait_ex2(&ifc_sem, 1);
	api_ifc_get_download_result();					
	if( sem_wait_ex2(&ifc_sem, 1000) != 0 )
	{
		printf("have no response2...\n");
	}
	else
	{				
		pthread_mutex_lock(&ifc_lock);				
		if( gl_ifc_rsp.cmd == OTAC_IF_GET_DOWNLOAD_RESULT )
		{
			if( gl_ifc_rsp.result == 1 )
			{
				//printf("download NOT compeleted!!....%d\n",gl_ifc_rsp.translated_len);
				if(translated_len) 
					*translated_len =gl_ifc_rsp.translated_len;
				pthread_mutex_unlock(&ifc_lock);
				return 1;
			}
			else if( gl_ifc_rsp.result == 0 )
			{
				if(gl_ifc_rsp.download_result == FS_DOWNLOAD_RESULT_OK)
				{
					ifc_state = IFC_DOWNLOAD_STATE_TRANSLATED;							
					dprintf("download compeleted!!....\n");	
				}
				else
				{
					dprintf("download error!!....gl_ifc_rsp.download_result = %d\n", gl_ifc_rsp.download_result);	
				}
			}
			else if( gl_ifc_rsp.result == 2 )
			{
				dprintf("download error --TCP disconnected!!....\n");	
			}
		}
		else
		{
			dprintf("ERROR RESPONSE....\n");	
		}
		pthread_mutex_unlock(&ifc_lock);					
	}
	
	if(ifc_state != IFC_DOWNLOAD_STATE_TRANSLATED)
	{
		sem_wait_ex2(&ifc_sem, 1);
		api_ifc_set_disconnect_server();			
		if( sem_wait_ex2(&ifc_sem, 2000) != 0 )
		{
			dprintf("have no response3, contiue download...\n");				
			ifc_state = IFC_DOWNLOAD_STATE_CONNECTED;							
		}
		else
		{
			dprintf("stop download ok!!!...\n");						
			ifc_state = IFC_DOWNLOAD_STATE_NONE;							
		}
		return -4;
	}
	sem_wait_ex2(&ifc_sem, 1);
	api_ifc_set_disconnect_server();	
	
	if( sem_wait_ex2(&ifc_sem, 2000) != 0 )
	{
		dprintf("have no response3, contiue download...\n");				
		ifc_state = IFC_DOWNLOAD_STATE_CONNECTED;							
	}
	else
	{
		dprintf("stop download ok!!!...\n");						
		ifc_state = IFC_DOWNLOAD_STATE_NONE;							
	}

	return 2;
}

int ota_ifc_file_download_cancel(void)
{
	if(ifc_state != IFC_DOWNLOAD_STATE_NONE)
	{
		sem_wait_ex2(&ifc_sem, 1);
		api_ifc_set_disconnect_server();			
		if( sem_wait_ex2(&ifc_sem, 2000) != 0 )
		{
			//printf("have no response3, contiue download...\n");				
			//ifc_state = IFC_DOWNLOAD_STATE_CONNECTED;							
		}
		else
		{
			//printf("stop download ok!!!...\n");						
			//ifc_state = IFC_DOWNLOAD_STATE_NONE;							
		}
		ifc_state = IFC_DOWNLOAD_STATE_NONE;
	}
	
	return 0;
}


static OtaClientReport_T	otaClientReport;
static OtaClientPara		otaClientPara;
static int 					bgDownload;

vdp_task_t			  		task_OtaClientIfc;
Loop_vdp_common_buffer  	vdp_OtaClientIfc_mesg_queue;
void* vdp_OtaClientIfc_task( void* arg );
void vdp_OtaClientIfc_mesg_data_process(char* msg_data,int len);

void vtk_TaskInit_Ota_client_ifc(int priority)
{
	init_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue, 1000, (msg_process)vdp_OtaClientIfc_mesg_data_process, &vdp_OtaClientIfc_mesg_queue);
	init_vdp_common_task(&task_OtaClientIfc, MSG_ID_OtaClientIfc, vdp_OtaClientIfc_task, &vdp_OtaClientIfc_mesg_queue, NULL);
	
}


void* vdp_OtaClientIfc_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;

	sleep(2);
	ota_client_ifc_init();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 2000);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		else
		{
		
			if( ftp_download_process() != 0 )
			{	
				otaClientReport.result = ota_ifc_file_download_step2(&otaClientReport.translated_len);

				if(bgDownload)
				{
					if(otaClientReport.result == 1)
					{
						otaClientReport.type = OtaClientIfc_Translating;
					}
					else if(otaClientReport.result == 2)
					{
						otaClientReport.type = OtaClientIfc_Translated;
					}
					else if(otaClientReport.result < 0)
					{
						otaClientReport.type = OtaClientIfc_TranslateError;
					}
					OtaClientReportProcess();
				}
				else
				{
					if(otaClientReport.result == 1)
					{
						API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_Translating, &otaClientReport.translated_len,4);
					}
					else if(otaClientReport.result == 2)
					{
						API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_Translated);
					}
					else if(otaClientReport.result < 0)
					{
						API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_Error, &otaClientReport.result,4);
					}
				}
			}
		}
	}
	
	return 0;
}

static char* CreateFtpDownloadInfoJsonData(int state, char* dir, char* fileName, int fileLen, int dlLen)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, FTP_INFO_STATE, state);
	if(fileName != NULL)
	{
		cJSON_AddStringToObject(root, FTP_INFO_FileName, fileName);
	}

	if(dir != NULL)
	{
		cJSON_AddStringToObject(root, FTP_INFO_FileDir, dir);
	}
	cJSON_AddNumberToObject(root, FTP_INFO_FileLen, fileLen);
	cJSON_AddNumberToObject(root, FTP_INFO_DlLen, dlLen);

	
	
	string = cJSON_Print(root);
	
	cJSON_Minify(string);
		
	cJSON_Delete(root);

	return string;
}

int ParseFtpDownloadInfoJsonData(const char* json, int* state, char* dir, char* fileName, int* fileLen, int* dlLen)
{
    int status = 0;
	int i;
	cJSON *pSub;
		
    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        dprintf("---------------------------------json Error.\n%s\n", json);
        status = 0;
        goto end;
    }

	if(fileName != NULL)
	{
		strcpy(fileName, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(root, FTP_INFO_FileName)));
	}

	if(dir != NULL)
	{
		strcpy(dir, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(root, FTP_INFO_FileDir)));
	}
	
	if(state != NULL)
	{
		ParseJsonNumber(root, FTP_INFO_STATE, state);
	}

	if(fileLen != NULL)
	{
		ParseJsonNumber(root, FTP_INFO_FileLen, fileLen);
	}
	
	if(dlLen != NULL)
	{
		ParseJsonNumber(root, FTP_INFO_DlLen, dlLen);
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}


// lzh_20201201_s
void ftp_download_info(int type, char* disp_str)
{
	int result;
	char temp[200];
	char *pStr;

	strncpy(temp, disp_str, 200);
	
	dprintf("(%d)%s\n",type, temp);

	switch( type )
	{
		case DOWNLOAD_INFO_PROCESS:
			otaClientReport.type = OtaClientIfc_Translating;
			otaClientReport.translated_len = 0;
			otaClientReport.file_len = 0;
			sscanf(temp,"%d/%d(.*)", &otaClientReport.translated_len, &otaClientReport.file_len);
			break;
		case DOWNLOAD_INFO_OK:
			ifc_state = IFC_DOWNLOAD_STATE_NONE;	
			otaClientReport.type = OtaClientIfc_Translated;
			break;	
		case DOWNLOAD_INFO_PERFORM_ERR: 		//ftp download error
			ifc_state = IFC_DOWNLOAD_STATE_NONE;		
			otaClientReport.type = OtaClientIfc_StartActError;
			break;			
		default:
			break;
	}

	if(bgDownload)
	{
		OtaClientReportProcess();
	}
	else
	{
		pStr = CreateFtpDownloadInfoJsonData(ifc_state, otaClientPara.file_dir, otaClientReport.file_name, otaClientReport.file_len, otaClientReport.translated_len);

		switch( type )
		{
			case DOWNLOAD_INFO_PROCESS:
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_run, pStr, strlen(pStr)+1);
				break;
			case DOWNLOAD_INFO_OK:
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_ok, pStr, strlen(pStr)+1);
				break;	
			case DOWNLOAD_INFO_PERFORM_ERR: 		//ftp download error
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_proc_er, pStr, strlen(pStr)+1);
				break;			

			default:
				break;
		}

		if(pStr != NULL)
		{
			free(pStr);
		}
	}
}
// lzh_20201201_e

void vdp_OtaClientIfc_mesg_data_process(char* msg_data,int len)
{
	int i;
	MSG_OtaClientIfc* msg;
	MSG_OtaClientIfc_2* msg2;
	
	msg = (MSG_OtaClientIfc*)msg_data;
	msg2 = (MSG_OtaClientIfc_2*)msg_data;

	int result;
	char buff[68];
	char* pString;
	
	bgDownload = msg->head.msg_sub_type;
	
	memcpy(&otaClientPara, &msg2->para, sizeof(otaClientPara));

	if(bgDownload)
	{
		switch(msg2->head.msg_type)
		{
			case OtaClientIfc_Msg_StartDownload:
				otaClientReport.result = get_upgrade_file_dir(otaClientPara.file_type, otaClientPara.space, otaClientPara.file_dir);
				if(otaClientReport.result == 0)
				{
					if(otaClientPara.file_type == 1)
					{
						otaClientReport.translated_len = 0;
						otaClientReport.type = OtaClientIfc_StartConnectting;
						OtaClientReportProcess();
					}
					
					otaClientReport.result = ota_ifc_file_download_step1(otaClientPara);
		
					if(otaClientReport.result == 0)
					{
						otaClientReport.type = OtaClientIfc_StartActOk;
						otaClientReport.file_len = gl_ifc_rsp.translated_len;
						strcpy(otaClientReport.file_name, gl_ifc_rsp.file_name);
					}
					else
					{
						otaClientReport.type = OtaClientIfc_StartActError;
					}
				}
				else if(otaClientReport.result == -1)
				{
					otaClientReport.type = OtaClientIfc_NoSdcard;
				}
				else
				{
					otaClientReport.type = OtaClientIfc_NoSpace;
				}
				OtaClientReportProcess();
				break;
			case OtaClientIfc_Msg_CancelDownload:
				otaClientReport.result = ota_ifc_file_download_cancel();
				otaClientReport.type = (otaClientReport.result == 0 ? OtaClientIfc_CancelActOk : OtaClientIfc_CancelActError);
				OtaClientReportProcess();
				break;
				
			// lzh_20201201_s
			case OtaClientIfc_Msg_Startftp:
				otaClientReport.result = get_upgrade_file_dir(otaClientPara.file_type, otaClientPara.space, otaClientPara.file_dir);
				if( otaClientReport.result == 0 )
				{
					sprintf(otaClientReport.file_name,"%s%s", otaClientPara.file_code, msg->file_type == 1 ? ".txt" : "");
					if(otaClientReport.file_name[0] != 0)
					{
						DeleteFileProcess(otaClientPara.file_dir, otaClientReport.file_name);			
					}
					result = ftp_download_start_default(otaClientPara.server_ip, otaClientReport.file_name,otaClientPara.file_dir,ftp_download_info);
					if( result == 0 )
					{
						ifc_state = IFC_DOWNLOAD_STATE_TRANSLATING;
						otaClientReport.type = OtaClientIfc_StartActOk;
					}
					else
					{
						ifc_state = IFC_DOWNLOAD_STATE_NONE;	
						otaClientReport.type = OtaClientIfc_StartActError;
					}
				}
				else
				{
					ifc_state = IFC_DOWNLOAD_STATE_NONE;	
					otaClientReport.type = OtaClientIfc_NoSpace;
				}
				OtaClientReportProcess();
				break;
				
			case OtaClientIfc_Msg_Cancelftp:
				ifc_state = IFC_DOWNLOAD_STATE_NONE;	
				otaClientReport.result = ftp_download_cancel();
				if(otaClientReport.result  == 0 )
				{
					otaClientReport.type = OtaClientIfc_CancelActOk;
				}
				else
				{
					otaClientReport.type = OtaClientIfc_CancelActError;
				}
				OtaClientReportProcess();
				break;
			// lzh_20201201_e
				
		}

	}
	else
	{
		switch(msg2->head.msg_type)
		{
			case OtaClientIfc_Msg_StartDownload:
				otaClientReport.result = get_upgrade_file_dir(otaClientPara.file_type, otaClientPara.space, otaClientPara.file_dir);
				if(otaClientReport.result == 0)
				{
					otaClientReport.result = ota_ifc_file_download_step1(otaClientPara);
			
					if(otaClientReport.result == 0)
					{
						otaClientReport.type = OtaClientIfc_StartActOk;
						otaClientReport.file_len = gl_ifc_rsp.translated_len;
						strcpy(otaClientReport.file_name, gl_ifc_rsp.file_name);
					}
					else
					{
						otaClientReport.type = OtaClientIfc_StartActError;
					}
				}
				else
				{
					otaClientReport.type = OtaClientIfc_NoSpace;
				}
				
				if(otaClientReport.type == OtaClientIfc_StartActOk)
				{
					memcpy(buff,&gl_ifc_rsp.translated_len,4);
					strcpy(buff+4,gl_ifc_rsp.file_name);
					
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ActOk,buff,5+strlen(gl_ifc_rsp.file_name));
				}
				else
				{
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_Error,&result,4);
				}
				break;
			case OtaClientIfc_Msg_CancelDownload:
				otaClientReport.result = ota_ifc_file_download_cancel();
				otaClientReport.type = (otaClientReport.result == 0 ? OtaClientIfc_CancelActOk : OtaClientIfc_CancelActError);
				if(otaClientReport.result == 0)
				{
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ActOk);
				}
				else
				{
					API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_Error,&result,4);
				}
				break;
								
			// lzh_20201201_s
			case OtaClientIfc_Msg_Startftp:
				otaClientReport.result = get_upgrade_file_dir(otaClientPara.file_type, otaClientPara.space, otaClientPara.file_dir);
				if(otaClientReport.result == 0)
				{
					sprintf(otaClientReport.file_name,"%s%s", otaClientPara.file_code, msg->file_type == 1 ? ".txt" : "");	
					if(otaClientReport.file_name[0] != 0)
					{
						DeleteFileProcess(otaClientPara.file_dir, otaClientReport.file_name);			
					}
					result = ftp_download_start_default(otaClientPara.server_ip, otaClientReport.file_name, otaClientPara.file_dir,ftp_download_info);
					if( result == 0 )
					{
						ifc_state = IFC_DOWNLOAD_STATE_TRANSLATING;
						API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_ok);
					}
					else
					{
						ifc_state = IFC_DOWNLOAD_STATE_NONE;	
						API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_er);
					}
				}
				else
				{
					otaClientReport.type = OtaClientIfc_NoSpace;
					ifc_state = IFC_DOWNLOAD_STATE_NONE;	
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_start_er);
				}
				break;
				
			case OtaClientIfc_Msg_Cancelftp:
				ifc_state = IFC_DOWNLOAD_STATE_NONE;			
				if( ftp_download_cancel() == 0 )
				{
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_cancel_ok);
				}
				else
				{
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaClientIfc_ftp_notify_cancel_er); 			
				}
				break;
			// lzh_20201201_e
		}
	}

	
	
}

int OtaClientReportProcess(void)
{		
	extern OtaDlTxt_t dlTxtInfo;
	char 	buff[200];
	int 	len;
	int 	rebootFlag = 0;

	switch(otaClientReport.type)
	{	
		case OtaClientIfc_StartConnectting:
			if(otaClientPara.file_type == 1)
			{
				OtaReportState(RES_DOWNLOAD_STATE_CONNECTING, NULL);
			}
			break;
			
		case OtaClientIfc_NoSdcard:
			OtaReportState(RES_DOWNLOAD_STATE_NO_SDCARD, NULL);
			break;
		
		case OtaClientIfc_NoSpace:
			OtaReportState(RES_DOWNLOAD_STATE_ERR_NO_SPASE, NULL);
			break;
			
		case OtaClientIfc_StartActOk:
			if(otaClientPara.file_type == 1)
			{

			}
			else if(otaClientPara.file_type == 0)
			{
				snprintf(buff, 68, "0%%");
				OtaReportState(RES_DOWNLOAD_STATE_DOWNLOADING, buff);
			}
			break;
			
		case OtaClientIfc_StartActError:
			break;

		case OtaClientIfc_CancelActOk:
			break;
			
		case OtaClientIfc_CancelActError:
			break;
			
		case OtaClientIfc_Translating:
			if(otaClientPara.file_type == 0)
			{
				snprintf(buff, 68, "%4.2f%%", otaClientReport.translated_len*100.0/otaClientReport.file_len);
			}
			else
			{
				snprintf(buff, 68, "%4.2f%%", 0);
			}
			OtaReportState(RES_DOWNLOAD_STATE_DOWNLOADING, buff);
			break;
			
			
		case OtaClientIfc_Translated:
			if(otaClientPara.file_type == 0)
			{
				snprintf(buff, 68, "%4.2f%%", otaClientReport.file_len*100.0/otaClientReport.file_len);
				OtaReportState(RES_DOWNLOAD_STATE_DOWNLOADING, buff);
				OtaReportState(RES_DOWNLOAD_STATE_INSTALLING, NULL);

				sprintf(buff,"%s/%s",otaClientPara.file_dir,otaClientReport.file_name);

				if(InstallDownloadZip(dlTxtInfo.dlType, buff, &rebootFlag) >= 0)
				{
					OtaReportState(RES_DOWNLOAD_STATE_INSTALL_OK, NULL);
					OtaReportState(RES_DOWNLOAD_STATE_WAITING_REBOOT, NULL);
					if(rebootFlag)
					{
						sleep(3);
						WriteRebootFlag(otaClientPara.reportIp, otaClientPara.reportIpCnt);
						SoftRestar();
					}
				}
				else
				{
					OtaReportState(RES_DOWNLOAD_STATE_INSTALL_FAIL, NULL);
				}
			}
			else if(otaClientPara.file_type == 1)
			{
				sprintf(buff,"%s/%s", otaClientPara.file_dir, otaClientReport.file_name);

				if(CheckDownloadIsAllowed(buff, &dlTxtInfo))
				{
					otaClientPara.space = dlTxtInfo.fileLength;
					otaClientPara.file_type = 0;
					dprintf("otaClientPara.downloadType=%d\n", otaClientPara.downloadType);
					API_OtaDownload(1, otaClientPara);
				}
				else
				{
					OtaReportState(RES_DOWNLOAD_STATE_ERR_TYPE, NULL);
				}
				//删除txt文件
				if(otaClientReport.file_name[0] != 0)
				{
					DeleteFileProcess(otaClientPara.file_dir, otaClientReport.file_name);
				}
			}
			break;
		
		case OtaClientIfc_TranslateError:
			break;
	}
}



int api_ota_ifc_file_download(char *server_ip,char *file_dir,char *file_code,int file_type,char *dev_type,char *dev_ver,char *dev_id)
{
	MSG_OtaClientIfc	msg;	

	memset(&msg,0,sizeof(msg));
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type 		= OtaClientIfc_Msg_StartDownload;
	msg.head.msg_sub_type 	= 0;
	
	msg.file_type = file_type;
	
	if(server_ip)
	{
		strcpy(msg.server_ip,server_ip);
	}

	if(file_dir)
	{
		strcpy(msg.file_dir,file_dir);
	}

	if(file_code)
	{
		strcpy(msg.file_code,file_code);
	}

	if(dev_type)
	{
		strcpy(msg.dev_type,dev_type);
	}

	if(dev_ver)
	{
		strcpy(msg.dev_ver,dev_ver);
	}

	if(dev_id)
	{
		strcpy(msg.dev_id,dev_id);
	}
	
	push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, sizeof(msg));
		
	return 0;
}

int api_ota_ifc_cancel(void)
{
	MSG_OtaClientIfc	msg;	

	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type 		= OtaClientIfc_Msg_CancelDownload;
	msg.head.msg_sub_type 	= 0;

	push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, 4);
		
	return 0;
}

// lzh_20201201_s
int api_ota_ifc_state_just_download(void)
{
	return (ifc_state==IFC_DOWNLOAD_STATE_TRANSLATING)?1:0;
}

int api_ota_ifc_ftp_start(char *server_ip,char *file_dir,char *file_code,int file_type,char *dev_type,char *dev_ver,char *dev_id)
{
	MSG_OtaClientIfc	msg;	

	memset(&msg,0,sizeof(MSG_OtaClientIfc));
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type 		= OtaClientIfc_Msg_Startftp;
	msg.head.msg_sub_type 	= 0;
	
	msg.file_type = file_type;
	
	if(server_ip)
	{
		strcpy(msg.server_ip,server_ip);
	}

	if(file_dir)
	{
		strcpy(msg.file_dir,file_dir);
	}

	if(file_code)
	{
		strcpy(msg.file_code,file_code);
	}

	if(dev_type)
	{
		strcpy(msg.dev_type,dev_type);
	}

	if(dev_ver)
	{
		strcpy(msg.dev_ver,dev_ver);
	}

	if(dev_id)
	{
		strcpy(msg.dev_id,dev_id);
	}
	
	push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, sizeof(MSG_OtaClientIfc));
		
	return 0;
}

int api_ota_ifc_ftp_cancel(void)
{
	MSG_OtaClientIfc	msg;	

	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type 		= OtaClientIfc_Msg_Cancelftp;
	msg.head.msg_sub_type 	= 0;

	push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, 4);
		
	return 0;
}
// lzh_20201201_e

int API_OtaDownload(int backstage, OtaClientPara para)
{
	MSG_OtaClientIfc_2	msg;	
	int i;

	memset(&msg, 0, sizeof(msg));
	
	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type		= (para.downloadType == OtaDlTypeFtp ? OtaClientIfc_Msg_Startftp : OtaClientIfc_Msg_StartDownload);
	msg.head.msg_sub_type 	= backstage;
	
	memcpy(&msg.para, &para, sizeof(para));

	return push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, sizeof(msg));
}

int API_OtaCancel()
{		
	MSG_OtaClientIfc_2	msg;	

	msg.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_OtaClientIfc;
	msg.head.msg_type		= (otaClientPara.downloadType == OtaDlTypeFtp ? OtaClientIfc_Msg_Cancelftp : OtaClientIfc_Msg_CancelDownload);
	msg.head.msg_sub_type 	= 0;

	return push_vdp_common_queue(&vdp_OtaClientIfc_mesg_queue,  &msg, 4);
}

static void OtaReportState(char* state, char* msg)
{
	IxReportResDownload(otaClientPara.reportIp , otaClientPara.reportIpCnt, "TCP", state, msg, NULL, otaClientPara.server_ip, otaClientPara.file_code);
}

