

#include "cJSON.h"
#include <dirent.h>
#include "utility.h"
#include "unix_socket.h"
#include "define_file.h"
#include "task_IoServer.h"
#include "ota_FileProcess.h"
#include "task_Led.h"

#define UPGRADE_JSON											"upgrade.json"
#define UPGRADE_SH												"upgrade.sh"
#define UPGRADE_DELETE											"UPGRADE_DELETE"

#define UPGRADE_JSON_KEY_Verify									"Verify"
#define UPGRADE_JSON_KEY_UpgradeType							"UpgradeType"
#define UPGRADE_JSON_KEY_FirmwareReset							"FirmwareReset"
#define UPGRADE_JSON_KEY_FirmwareParser							"FirmwareParser"
#define UPGRADE_JSON_KEY_MCU_Firmware							"MCU_Firmware"

#define DL_JSON_KEY_Verify									"Verify"
#define DL_JSON_KEY_PacketName								"PacketName"
#define DL_JSON_KEY_DownloadType							"DownloadType"
#define DL_JSON_KEY_Size									"Size"
#define DL_JSON_KEY_Info									"Info"


#define CMD_LINE_LONG_MAX							200

#define MCU_DOWNLOAD_STATE_READY						0
#define MCU_DOWNLOAD_STATE_GET_VER						1
#define MCU_DOWNLOAD_STATE_ERROR						2
#define MCU_DOWNLOAD_STATE_STOP							3
#define MCU_DOWNLOAD_STATE_INSTALLING					4
#define MCU_DOWNLOAD_STATE_INSTALL_OVER					5

static int mcuUpdateState;
static int mcuUpdateBlk_all;
static int mcuUpdateBlk_cur;
static int mcuUpdateMsgId;

static void fw_uart_upgrade_callback(void* pbuf,int size)
{
	unsigned char msg_id,blk_all,blk_cur;
	unsigned char* pdatbuf = (unsigned char*)pbuf;

	msg_id	= pdatbuf[0];
	blk_all = pdatbuf[1];
	blk_cur = pdatbuf[2];
	
	mcuUpdateBlk_all = blk_all;
	mcuUpdateBlk_cur = blk_cur;
	mcuUpdateMsgId = msg_id;

	switch(msg_id)
	{
		case 2:
		case 3:
		case 6:
		case 7:
		case 10:
		case 11:
			//出错
			mcuUpdateState = MCU_DOWNLOAD_STATE_ERROR;
			break;
		case 16:
			//安装完成
			mcuUpdateState = MCU_DOWNLOAD_STATE_INSTALL_OVER;
			break;
		case 0:
			//获取版本
			mcuUpdateState = MCU_DOWNLOAD_STATE_GET_VER;
			break;
		case 1:
		case 4:
		case 5:
		case 8:
		case 9:
			//安装中
			mcuUpdateState = MCU_DOWNLOAD_STATE_INSTALLING;
			break;
		case 12:
		case 13:
		case 14:
		case 15:
			//停止安装
			mcuUpdateState = MCU_DOWNLOAD_STATE_STOP;
			break;
	}
}

int get_upgrade_file_dir(int flie_type,int file_length,char *file_dir)	//-1,pls insert sd card,-2,no space
{
	int totalsize,freesize;
	DIR *dir;
	
	if(!Judge_SdCardLink())
	{
		MakeDir(NAND_DOWNLOAD_PATH);

		if(getDiscSize(NAND_DOWNLOAD_PATH,&freesize,&totalsize)!=0)
			return -1;
		dprintf("nand totalsize=%d,freesize=%d\n",totalsize,freesize);
		if(file_length > NAND_DOWNLOAD_LIMIT || (file_length/1000) > freesize)
			return -1;
	    	
		if(file_dir)
			strcpy(file_dir,NAND_DOWNLOAD_PATH);
	}
	else
	{
		MakeDir(SDCARD_DOWNLOAD_PATH);

		if(getDiscSize(SDCARD_DOWNLOAD_PATH,&freesize,&totalsize)!=0)
			return -2;
		dprintf("sdcard totalsize=%d,freesize=%d\n",totalsize,freesize);
		if( (file_length/1000) > (freesize/5))
			return -2;
		
		if(file_dir)
			strcpy(file_dir,SDCARD_DOWNLOAD_PATH);
	}

	return 0;
}

int PopenProcess(const char* cmd, char* readBuf[], int bufLen, int lineMax)
{
	#define Popen_LINE_LONG_MAX	1000
	
	FILE *pf;
	char buffer[Popen_LINE_LONG_MAX];
	int lineNum = 0;
	int i;

	pf = popen(cmd, "r");

	if(pf == NULL)
	{
		dprintf("Unknow Error!!!!!!!!\n");
		return lineNum;
	}
	
	for(lineNum = 0; fgets(buffer, Popen_LINE_LONG_MAX, pf) != NULL; lineNum++)
	{
		for(i = 0; buffer[i] != 0 && i < Popen_LINE_LONG_MAX; i++)
		{
			if(buffer[i] == '\r' || buffer[i] == '\n')
			{
				buffer[i] = 0;
				break;
			}
		}

		if(readBuf != NULL)
		{
			if(lineNum < lineMax && readBuf[lineNum] != NULL)
			{
				strncpy(readBuf[lineNum], buffer, bufLen-1);
				readBuf[lineNum][bufLen-1] = 0;
			}
		}
	}

	pclose(pf);

	return lineNum;
}

int IfZipHasFile(const char* zip, const char* fileName)
{
	char cmd[CMD_LINE_LONG_MAX];
	char linestr[CMD_LINE_LONG_MAX];
	int ret = 0;
	char* pReadBuf[1];

	pReadBuf[0] = linestr;

	snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -l \"%s\" | grep -w \"%s\" | awk -F ' ' '{print $NF}' | awk -F '/' '{print $NF}'", zip, fileName);

	ret = PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	
	return ret;
}

int UnZipFile(const char* zip, const char* fileName, const char* unZipPath, char* filePath)
{
	char cmd[CMD_LINE_LONG_MAX];
	char readBuf[CMD_LINE_LONG_MAX];
	char readBuf2[CMD_LINE_LONG_MAX];
	int fileNum = 0;
	char* pReadBuf[200];
	int i;
	
	char* pReadBuf2[1];
	
	pReadBuf2[0] = readBuf2;
	pReadBuf[0] = readBuf;

	
	fileNum = IfZipHasFile(zip, fileName);
	if(fileNum == 0)
	{
		dprintf("%s has not %s\n", zip, fileName);
		return fileNum;
	}
	else if(fileNum > 200)
	{
		fileNum = 200;
	}
	
	for(i = 1; i < fileNum; i++)
	{
		pReadBuf[i] = malloc(CMD_LINE_LONG_MAX);
	}

	snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -l %s | grep -w \"%s\" | awk -F ' ' '{print $NF}'", zip, fileName);

	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, fileNum);


	if(filePath != NULL)
	{
		
		snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | sed 's/\\//\\\\\\//g'", fileName);
		
		PopenProcess(cmd, pReadBuf2, CMD_LINE_LONG_MAX, 1);
		
		snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | sed \"s/%s", pReadBuf[0], pReadBuf2[0]);
		strcat(cmd, ".*$//g\"");
		
		PopenProcess(cmd, pReadBuf2, CMD_LINE_LONG_MAX, 1);
		
		strcpy(filePath, pReadBuf2[0]);
	}

	for(i = 0; i < fileNum; i++)
	{
		snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -oq %s %s -d %s", zip, pReadBuf[i], unZipPath);
		system(cmd);
		if(i >= 1)
		{
			free(pReadBuf[i]);
		}
	}
	
	return fileNum;
}


int FwUprade_Installing(char *file_path)
{
	int place;
	int ret;
	char tempFile[200];
	
	if(strstr(file_path, DISK_SDCARD) != NULL)
	{
		place = 0;
	}
	else
	{
		place = 1;
	}
	ret = start_updatefile_and_reboot_forsdcard(place, file_path);
	remove(file_path);
	snprintf(tempFile, 200, "%s.txt", file_path);
	remove(tempFile);
	
	return ret; 
}

void remove_upgrade_tempfile(void)
{
	char cmd[100];
	
	if(Judge_SdCardLink())
	{
		snprintf(cmd,100,"rm -rf %s/*",SDCARD_DOWNLOAD_PATH);
		system(cmd);
	}

	snprintf(cmd,100,"rm -rf %s/*",NAND_DOWNLOAD_PATH);
	system(cmd);
}

int FwUpradeIXDeviceInstalling(char *file_path)
{
	int ret;
	char tempFile[200];
	
	if(IfZipHasFile(file_path, UPGRADE_SH_FILE))
	{
		snprintf(tempFile, 200, "%s.txt", file_path);
		remove(tempFile);
		dprintf("remove=%s\n", tempFile);			
		ret = 0;
	}
	else
	{
		ret = FwUprade_Installing(file_path);
	}
	
	return ret; 
}

static int GetJsonNumber(const cJSON* json, const char* key)
{
	const cJSON* subJson = NULL;
	
	subJson = cJSON_GetObjectItemCaseSensitive( json, key);
	
	if (cJSON_IsNumber(subJson))
	{
		return subJson->valuedouble;
	}
	else
	{
		return 0;
	}
}

static int VarifyInstallPacket(cJSON* verifys)
{
	int 	verifyNum;
	int		ret = 1;
	int 	i;
	cJSON* 	temp;
	cJSON*	verify;
	char	iodata[20];

	if(verifys != NULL)
	{
		verifyNum = cJSON_GetArraySize(verifys);
		for(i = 0; i < verifyNum; i++)
		{
			verify = cJSON_GetArrayItem(verifys, i);
			temp = cJSON_GetArrayItem(verify, 0);
			switch(atoi(cJSON_GetStringValue(temp)))
			{
				case 0:
					API_Event_IoServer_InnerRead_All(DeviceModel, iodata);
					break;
				case 1:
					strcpy(iodata, GetSysVerInfo_type());
					//API_Event_IoServer_InnerRead_All(ParaDeviceType, iodata);
					break;
			}
			temp = cJSON_GetArrayItem(verify, 1);
			if(strcmp(iodata, cJSON_GetStringValue(temp)))
			{
				dprintf("verify error!!! iodata = %s.\n", iodata);
				ret = 0;
			}
		}
	}

	return ret;
}

int CheckDownloadIsAllowed(const char* txtFile, OtaDlTxt_t* txtInfo)
{
	char	iodata[20];
	cJSON*	json;
	cJSON*	verifys;
	int 	ret;

	dprintf("txtFile=%s\n", txtFile);
	
	json = GetJsonFromFile(txtFile);
	if(json != NULL)
	{
		verifys = cJSON_GetObjectItemCaseSensitive(json, DL_JSON_KEY_Verify);			
		ret = VarifyInstallPacket(verifys);
		
		if(ret && (txtInfo != NULL))
		{
			OtaDlTxt_t tempInfo;

			tempInfo.dlType = GetJsonNumber(json, DL_JSON_KEY_DownloadType);
			tempInfo.fileLength = GetJsonNumber(json, DL_JSON_KEY_Size);
			
			ParseJsonString(json, DL_JSON_KEY_Info, tempInfo.info, OTA_DL_STR_LEN);
			ParseJsonString(json, DL_JSON_KEY_PacketName, tempInfo.packetName, OTA_DL_STR_LEN);

			*txtInfo = tempInfo;
		}
		cJSON_Delete(json);
	}
	else
	{
		ret = 0;
	}

	return ret;
}

int InstallDownloadZip(int downloadType, char *file_path, int* rebootFlag)
{
	int ret = 1;
	
	switch(downloadType)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 8:
			API_LedDisplay_FwUpdate();
			ret = InstallFirmware(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		
		case 5:
			ret = InstallMcuCode(file_path);
			break;
		
		case 6:
			API_LedDisplay_FwUpdate();
			ret = InstallRes(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		case 7:
			API_LedDisplay_FwUpdate();
			ret = InstallCustomerized(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		case 9:
			break;
	}
	
	if(ret == 0)
	{
		ret = 1;
		*rebootFlag = 1;
	}
	else
	{
		*rebootFlag = 0;
	}
	
	
	return ret; 
}

int InstallRes(char *file_path)
{
	char cmd_buff[200];
	
	sprintf(cmd_buff,"unzip -o %s -d %s", file_path, PublicResFileDir);
	system(cmd_buff);
	
	PubResReloadToActFile();

	DeleteFileProcess(NULL, file_path);
	sprintf(cmd_buff,"%s.txt", file_path);
	DeleteFileProcess(NULL, cmd_buff);
	
	return 1; 
}

int InstallCustomerized(char *file_path)
{
	char cmd_buff[200];
	char dirName[200];
	char* pReadBuf[1];
	pReadBuf[0] = dirName;

	//删除订制目录
	DeleteFileProcess(CustomerFileDir, "*");
	
	//获取订制目录的上级目录
	snprintf(cmd_buff, 200, "dirname %s", CustomerFileDir);
	PopenProcess(cmd_buff, pReadBuf, 200, 1);

	//解压到订制目录的上级目录下
	snprintf(cmd_buff, 200, "unzip -o %s -d %s", file_path, dirName);
	system(cmd_buff);
	
	dprintf("%s\n", cmd_buff);

	ReloadCustomerFile();
	
	DeleteFileProcess(NULL, file_path);
	sprintf(cmd_buff,"%s.txt", file_path);
	DeleteFileProcess(NULL, cmd_buff);
	
	return 1; 
}

int InstallFirmware(char *file_path)
{
	char	cmd[CMD_LINE_LONG_MAX];
	char	srcDir[CMD_LINE_LONG_MAX];
	char	Path[CMD_LINE_LONG_MAX];
	char*	jsonString;
	int 	fileNum;
	int 	i;
	cJSON*	upgradeParse = NULL;
	cJSON*	verifys;
	cJSON*	subJson;
	cJSON*	srcFile;
	cJSON*	tagFile;
	int 	verifyNum;
	int		upgradeType;
	int		ret = 1;
	
	char* pReadBuf[1];

	if(!IfZipHasFile(file_path, UPGRADE_JSON))
	{
		dprintf("Have no %s.\n", UPGRADE_JSON);
		ret = -1;
		goto InstallFirmwareError;
	}

	//获取固件目录的上级目录
	snprintf(cmd, CMD_LINE_LONG_MAX, "dirname %s", file_path);
	pReadBuf[0] = Path;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);

	//解压UPGRADE_JSON文件并打开
	UnZipFile(file_path, UPGRADE_JSON, Path, srcDir);

	snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, UPGRADE_JSON);
	system(cmd);

	snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, UPGRADE_JSON);
	
	jsonString = GetJsonStringFromFile(cmd);
	//没有UPGRADE_JSON文件
	if(jsonString == NULL)
	{
		dprintf("unzip %s error.\n", UPGRADE_JSON);
		ret = -2;
		goto InstallFirmwareError;
	}

	//删除UPGRADE_JSON文件和临时文件夹
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);

	//解析UPGRADE_JSON文件
	upgradeParse = cJSON_Parse(jsonString);
	free(jsonString);
	
	verifys = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_Verify);
	if(!VarifyInstallPacket(verifys))
	{
		dprintf("--------------------------------------verify error!!!\n");
		ret = -3;
		goto InstallFirmwareError;
	}

	subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_UpgradeType);

	if(cJSON_IsNumber(subJson) )
	{
		upgradeType = subJson->valuedouble;
	}
	else
	{
		upgradeType = -1;
	}
	
	switch(upgradeType)
	{
		//普通更新方式
		case 0:
			subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareParser);
			fileNum = cJSON_GetArraySize(subJson);
			for(i = 0; i < fileNum; i++)
			{
				srcFile = cJSON_GetArrayItem(cJSON_GetArrayItem(subJson, i), 0);
				tagFile = cJSON_GetArrayItem(cJSON_GetArrayItem(subJson, i), 1);
				
				DeleteFileProcess(NULL, cJSON_GetStringValue(tagFile));
				if(UnZipFile(file_path, cJSON_GetStringValue(srcFile), Path, srcDir))
				{
					snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
					system(cmd);
					snprintf(cmd, CMD_LINE_LONG_MAX, "mv %s/%s/%s %s", Path, srcDir, cJSON_GetStringValue(srcFile), cJSON_GetStringValue(tagFile));
					system(cmd);
				}
			}
			
			//删除解压后的临时文件
			snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
			pReadBuf[0] = srcDir;
			PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
			DeleteFileProcess(Path, srcDir);

			if(strstr(file_path, SdUpgradePath) == NULL)
			{
				DeleteFileProcess(NULL, file_path);
				sprintf(cmd,"%s.txt", file_path);
				DeleteFileProcess(NULL, cmd);
			}
			if(ret == 1)
			{
				subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareReset);
				if(cJSON_IsNumber(subJson) && subJson->valuedouble)
				{
					ret = 0;
				}
			}
			break;
			
		//IAP更新方式
		case 1:
			if(!IfZipHasFile(file_path, UPGRADE_SH))
			{
				dprintf("Have no %s.\n", UPGRADE_SH);
				ret = -4;
				goto InstallFirmwareError;
			}
			else
			{
				if(strstr(file_path, DISK_SDCARD) != NULL)
				{
					MakeDir(SDCARD_DOWNLOAD_PATH);
					snprintf(cmd, CMD_LINE_LONG_MAX, "cp %s %s", file_path, SDCARD_DOWNLOAD_PATH);
				}
				else
				{
					MakeDir(NAND_DOWNLOAD_PATH);
					snprintf(cmd, CMD_LINE_LONG_MAX, "mv %s %s", file_path, NAND_DOWNLOAD_PATH);
				}
				system(cmd);
				ret = 0;
			}
			break;
			
		default:
			ret = -5;
			goto InstallFirmwareError;
			break;
	}
	
	InstallFirmwareError:
	cJSON_Delete(upgradeParse);

	return ret;
}


int InstallMcuCode(char *file_path)
{
	char	cmd[CMD_LINE_LONG_MAX];
	char	srcDir[CMD_LINE_LONG_MAX];
	char	Path[CMD_LINE_LONG_MAX];
	char*	jsonString;
	char*	mcuBin = NULL;
	cJSON*	upgradeParse = NULL;
	cJSON*	verifys;
	cJSON*	srcFile;
	cJSON*	subJson;
	int		ret = 1;
	
	char* pReadBuf[1];
	
	if(!IfZipHasFile(file_path, UPGRADE_JSON))
	{
		dprintf("Have no %s.\n", UPGRADE_JSON);
		ret = -1;
		goto InstallFirmwareError;
	}

	//获取固件目录的上级目录
	snprintf(cmd, CMD_LINE_LONG_MAX, "dirname %s", file_path);
	pReadBuf[0] = Path;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);

	//解压UPGRADE_JSON文件并打开
	UnZipFile(file_path, UPGRADE_JSON, Path, srcDir);

	snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, UPGRADE_JSON);
	system(cmd);

	snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, UPGRADE_JSON);
	
	jsonString = GetJsonStringFromFile(cmd);
	//没有UPGRADE_JSON文件
	if(jsonString == NULL)
	{
		dprintf("unzip %s error.\n", UPGRADE_JSON);
		ret = -2;
		goto InstallFirmwareError;
	}

	//删除UPGRADE_JSON文件和临时文件夹
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);

	//解析UPGRADE_JSON文件
	upgradeParse = cJSON_Parse(jsonString);
	free(jsonString);
	
	verifys = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_Verify);
	if(!VarifyInstallPacket(verifys))
	{
		dprintf("--------------------------------------verify error!!!\n");
		ret = -3;
		goto InstallFirmwareError;
	}

	srcFile = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_MCU_Firmware);
	if(srcFile != NULL)
	{
		int tempCnt = 0;
		
		if(UnZipFile(file_path, cJSON_GetStringValue(srcFile), Path, srcDir))
		{
			snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
			system(cmd);
			
			snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
	
			dprintf("%s\n", cmd);
			
			api_fw_uart_upgrade_stop();
			mcuUpdateState = MCU_DOWNLOAD_STATE_READY;
			if(api_fw_uart_upgrade_start_file(cmd, 0, fw_uart_upgrade_callback) == 0)
			{
				for(tempCnt = 0; tempCnt < 90; tempCnt++)
				{
					MCU_InstallDisplay(mcuUpdateMsgId, mcuUpdateBlk_all, mcuUpdateBlk_cur);
					usleep(1000*1000);

					if(mcuUpdateState == MCU_DOWNLOAD_STATE_INSTALL_OVER)
					{
						//安装完成
						break;
					}

					if(mcuUpdateState != MCU_DOWNLOAD_STATE_INSTALLING && tempCnt >= 10)
					{
						//10秒时间还没开始安装
						break;
					}

					if(mcuUpdateState == MCU_DOWNLOAD_STATE_ERROR || mcuUpdateState == MCU_DOWNLOAD_STATE_STOP)
					{
						//停止安装或者错误
						break;
					}
					
					dprintf("mcuUpdateMsgId=%d, mcuUpdateState=%d, tempCnt=%d\n", mcuUpdateMsgId, mcuUpdateState, tempCnt);
				}
			}
			api_fw_uart_upgrade_stop();
			if(mcuUpdateState != MCU_DOWNLOAD_STATE_INSTALL_OVER)
			{
				ret = -4;
				dprintf("--------------------------------------install error %d!!!\n", mcuUpdateState);
			}
		}
	}
	
	//删除解压后的临时文件
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);
	
	if(ret == 1)
	{
		subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareReset);
		if(cJSON_IsNumber(subJson) && subJson->valuedouble)
		{
			ret = 0;
		}
	}
	
	InstallFirmwareError:
	if(upgradeParse)
	{
		cJSON_Delete(upgradeParse);
	}

	if(strstr(file_path, SdUpgradePath) == NULL)
	{
		DeleteFileProcess(NULL, file_path);
		sprintf(cmd,"%s.txt", file_path);
		DeleteFileProcess(NULL, cmd);
	}
	
	return ret;
}


