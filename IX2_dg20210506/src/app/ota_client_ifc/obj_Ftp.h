/**
  ******************************************************************************
  * @file    obj_Ftp.h
  * @author  czb
  * @version V00.01.00
  * @date    2023.5.10
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Ftp_h_
#define _obj_Ftp_h_

#include <stdio.h>   
#include <stdlib.h>   
#include <string.h>   
#include <curl/curl.h>   
#include <sys/stat.h>
#include <pthread.h>

#define	DEFAULT_FTP_DOWNLOAD_DIR 	"/mnt/sdcard/vtkdownload"
#define FTP_DEFAULT_USERNAME		"userota"
#define FTP_DEFAULT_PASSWORD		"userota"
#define	FTP_DEFAULT_UPLOAD_DIR 		"/home/userota"

#define FTP_STATE_PARA_ERR      0
#define FTP_STATE_INIT_ERR			1
#define FTP_STATE_DOWN_OK				2
#define FTP_STATE_DOWN_ERR      3
#define FTP_STATE_DOWN_ING			4
#define FTP_STATE_DOWN_STOP			5
#define FTP_STATE_UP_OK					6
#define FTP_STATE_UP_ERR        7
#define FTP_STATE_UP_ING				8
#define FTP_STATE_UP_STOP				9

typedef struct
{
	char	ftp_server[200];
	char    ftp_filename[200];
	char    ftp_url[200];
	char    store_filename[200];
	int     store_filelen;
	FILE    *stream;
} FTP_FILE;

typedef struct
{
	int total;
	int now;
} FTP_FILE_STATISTICS;

//注意：返回1停止上传/下载, 返回0不影响。
typedef int (*FTP_CB)(int state, FTP_FILE_STATISTICS statistics, void* data);

typedef struct
{
    pthread_t	tid;
    int	      state;

	char 		server[100];
	char 		filename[200];
	char 		storeFileName[200];

  	FTP_CB	    callback;
  	void*		callbackData;
}FTP_Run_S;


//返回1：成功，0：失败
int FTP_StartDownload(char* server, char* filename, char* store_dir, void* cbData,  FTP_CB cb);

//返回1：成功，0：失败
int FTP_StopDownload(char* filename, char* store_dir);

//返回1：成功，0：失败
int FTP_StartUpload(char* server, char* filename, char* store_dir, void* cbData,  FTP_CB cb);

//返回1：成功，0：失败
int FTP_StopUpload(char* server, char* filename, char* store_dir);
#endif




