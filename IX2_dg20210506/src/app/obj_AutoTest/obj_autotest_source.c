/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "obj_TableProcess.h"
#include "task_CallServer.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_GetIpByInput.h"
#include "obj_autotest_console.h"
#include "obj_autotest_source.h"



enum 
{
	Autotest_Source_Idle = 0,
	Autotest_Source_Testing,
};

typedef struct
{
	int state;
	int test_type;
	int start_time;
	int console_self;
	int console_ip;
	AutotestDev_Stru source_dev;
	AutotestDev_Stru target_dev;
}AutoTestSource_Run_Stru;

AutoTestSource_Run_Stru AutoTestSource_Run={0};

pthread_mutex_t autotest_source_lock = PTHREAD_MUTEX_INITIALIZER;

int autotest_call_result = Call_Result_Succ;
int autotest_divert_result = Divert_Result_NoDivert;
int autotest_divert_video = Divert_Video_Succ;

void autotest_source_init(void)
{
	;
}

int Receive_AutoTest_Start(int console_ip,int rsp_id,int test_type,AutotestDev_Stru source_dev,AutotestDev_Stru target_dev)
{
	int rev = 0;
	time_t cur_time = time(NULL);
	IpCacheRecord_T record[8];
	int getNum;
	Call_Dev_Info call_dev;
	
	pthread_mutex_lock( &autotest_source_lock);
	if(AutoTestSource_Run.state != Autotest_Source_Idle && abs(cur_time - AutoTestSource_Run.start_time)<60)
	{
		rev = -1;
	}
	else
	{
		AutoTestSource_Run.start_time = cur_time;
		AutoTestSource_Run.test_type = test_type;
		AutoTestSource_Run.console_ip=console_ip;
		AutoTestSource_Run.source_dev = source_dev;
		AutoTestSource_Run.target_dev = target_dev;

		if(console_ip == 0)
			AutoTestSource_Run.console_self = 1;
		else
			AutoTestSource_Run.console_self = 0;
		
		AutoTestSource_Run.state = Autotest_Source_Testing;
	}
	pthread_mutex_unlock( &autotest_source_lock);
	
	if(target_dev.para_type == Autotest_ParaType_RmNum)
	{
		getNum = API_GetIpByInput(target_dev.dev_info, record);
		if(getNum == 0)
		{
			rev = -1;
		}
		else
		{
		
				memcpy(call_dev.bd_rm_ms,record[0].BD_RM_MS, 10);
				call_dev.ip_addr = record[0].ip;
				
				record[0].BD_RM_MS[8] = 0;
				//if(memcmp(record[i].BD_RM_MS,GetSysVerInfo_bd(),4) == 0)
				if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(record[0].BD_RM_MS, "0099", 4))
				{
					sprintf(call_dev.name,"IM%d",atol(record[0].BD_RM_MS+4));
					//strcpy(target_dev[i].name,record[i].BD_RM_MS+4);
				}
				else
				{
					//strcpy(target_dev[i].name,record[i].BD_RM_MS);
					strcpy(call_dev.name,"IM(");
					memcpy(call_dev.name+3,record[0].BD_RM_MS,4);
					strcat(call_dev.name,")");
					strcat(call_dev.name,record[0].BD_RM_MS+4);
				}
				#if 0
				if(strcmp(nameRecord.R_Name, "-"))
				{
					strcat(target_dev[i].name," ");
					//snprintf(display, 200, "[%s] %s", temp, record.R_Name);
					strcat(target_dev[i].name,nameRecord.R_Name);
				}
				else if(strlen(nameRecord.name1)>=1&&strcmp(nameRecord.name1, "-"))
				{
					strcat(target_dev[i].name," ");
					strcat(target_dev[i].name,nameRecord.name1);
				}
				#endif
				
			autotest_call_result = Call_Result_Succ;
			autotest_divert_result = Divert_Result_NoDivert;
			autotest_divert_video = Divert_Video_Succ;
			
			if(API_CallServer_Invite(IxCallScene1_Active, 1, &call_dev) < 0)
			{
				AutoTestSource_Run.state = Autotest_Source_Idle;
				rev = -1;
			}
			else
			{
				AutoTestSource_Run.state = Autotest_Source_Testing;
				rev = 0;
			}
		}
	}
	else if(target_dev.para_type == Autotest_ParaType_IP)
	{

	}
	
	if(!AutoTestSource_Run.console_self)
		Send_StartAutoTest_Rsp(console_ip,rsp_id,rev);

	return rev;
}

int API_AutoTest_LogWrite(char *log,int log_len)
{
	time_t cur_time = time(NULL);
	
	if(AutoTestSource_Run.state != Autotest_Source_Idle && abs(cur_time - AutoTestSource_Run.start_time)<180)
	{
		AutoTestSource_Run.start_time = cur_time;
		
		StartAutoTest_LogWrite_Cmd send_cmd;
		
		memset(&send_cmd,0,sizeof(StartAutoTest_LogWrite_Cmd));
		send_cmd.log_len = log_len;

		memcpy(send_cmd.log,log,log_len);
		
		if(AutoTestSource_Run.console_self)
			Receive_AutoTest_Log(0,log,log_len);
		else
			api_udp_c5_ipc_send_data(AutoTestSource_Run.console_ip,CMD_AUTOTEST_LOG_REQ,(char*)&send_cmd,sizeof(StartAutoTest_LogWrite_Cmd));
	}
	return 0;
}



int Send_AutoTestCall_Req(Call_Dev_Info target_info)
{
	time_t cur_time = time(NULL);
	int rev = 0;
	
	//pthread_mutex_lock( &autotest_source_lock);
	
	if(AutoTestSource_Run.state == Autotest_Source_Idle || abs(cur_time - AutoTestSource_Run.start_time)>10)
	{
		rev = 0;
	}
	else
	{
		AutoTestSource_Run.start_time = cur_time;
		
		AutoTestCallCmd_Stru AutoTestCallCmd_req;
		AutoTestCallCmd_Stru AutoTestCallCmd_rsp;
		int	response_len = sizeof(AutoTestCallCmd_Stru);

		memset( (char*)&AutoTestCallCmd_req, 0, sizeof(AutoTestCallCmd_Stru) );

		AutoTestCallCmd_req.test_type = AutoTestSource_Run.test_type;
		AutoTestCallCmd_req.talk_time = 10;
		AutoTestCallCmd_req.unlock_time = 15;
		AutoTestCallCmd_req.cancel_time = 30;
		
		AutoTestCallCmd_req.source_dev = CallServer_Run.source_dev;
		AutoTestCallCmd_req.target_dev 	= target_info;
		
		if( api_udp_c5_ipc_send_req(target_info.ip_addr, CMD_AUTOTEST_CALL_REQ, (char*)&AutoTestCallCmd_req,sizeof(AutoTestCallCmd_Stru), (char*)&AutoTestCallCmd_rsp, &response_len) == 0 )
		{		
			CallServer_Run.callee_divert_state 		= AutoTestCallCmd_rsp.rspstate;
			CallServer_Run.callee_master_sip_divert = AutoTestCallCmd_rsp.master_sip_divert;
			rev = 1;
		}
		else
		{
			rev = 2;
		}
	}
	
	//pthread_mutex_unlock( &autotest_source_lock);
	
	return rev;
	
}



int get_autotest_call_result(void)
{
	return autotest_call_result;
}

void set_autotest_call_result(int call_result)
{
	autotest_call_result = call_result;
}

int get_autotest_divert_result(void)
{
	return autotest_divert_result;
}

void set_autotest_divert_result(int divert_result)
{
	autotest_divert_result = divert_result;
}

int get_autotest_divert_video(void)
{
	return autotest_divert_video;
}

void set_autotest_divert_video(int divert_video)
{
	autotest_divert_video=divert_video;
}

int get_autotest_divert_retry(void)
{
	return 0;
}

int get_autotest_divert_video_retry(void)
{
	return 0;
}

int Send_AutoTestState_CallerClose(void)
{
	AutotestStateCmd_Stru send_cmd;

	send_cmd.notice_type = Autotest_State_CallerClose;
	send_cmd.source_dev = AutoTestSource_Run.source_dev;
	send_cmd.target_dev = AutoTestSource_Run.target_dev;
	send_cmd.autotest_result.call_result = get_autotest_call_result();
	send_cmd.autotest_result.divert_result = get_autotest_divert_result();
	send_cmd.autotest_result.divert_retry= get_autotest_divert_retry();
	send_cmd.autotest_result.divert_video = get_autotest_divert_video();
	send_cmd.autotest_result.divert_video_retry = get_autotest_divert_video_retry();

	api_udp_c5_ipc_send_data(AutoTestSource_Run.console_ip,CMD_AUTOTEST_STATE_REQ,(char*)&send_cmd,sizeof(AutotestStateCmd_Stru));
}

void API_AutoTest_CallerCancel(void)
{
	pthread_mutex_lock( &autotest_source_lock);
	if(AutoTestSource_Run.state!=Autotest_Source_Idle)
	{
		Send_AutoTestState_CallerClose();
		AutoTestSource_Run.state = Autotest_Source_Idle;
	}
	pthread_mutex_unlock( &autotest_source_lock);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

