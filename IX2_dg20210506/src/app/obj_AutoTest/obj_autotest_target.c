/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "obj_TableProcess.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_autotest_console.h"
#include "obj_autotest_target.h"

enum 
{
	Autotest_Target_Idle = 0,
	Autotest_Target_Testing,
};

typedef struct
{
	int state;
	int test_type;
	int start_time;
	int time;
	int talk_time;
	int unlock_time;
	int cancel_time;
	int rsp_id;
	//AutotestDev_Stru source_dev;
	//AutotestDev_Stru target_dev;
}AutoTestTarget_Run_Stru;

AutoTestTarget_Run_Stru AutoTestTarget_Run={0};

pthread_mutex_t autotest_target_lock = PTHREAD_MUTEX_INITIALIZER;
OS_TIMER timer_atuotest_target;

void timer_atuotest_target_callback(void);

void autotest_target_init(void)
{
	OS_CreateTimer(&timer_atuotest_target, timer_atuotest_target_callback, 1000/25);
}

int Receive_AutoTest_StartCall(int caller_ip,int rsp_id,AutoTestCallCmd_Stru *pcmd)
{
	int callscene;
	uint8 tranferset;
	Call_Dev_Info self_info;
	int rev = 0;
	time_t cur_time = time(NULL);
	
	//gaddr.gatewayid 	= node_id;	
	//gaddr.ip			= source_ip;
	//if(AutoTestTarget_Run.state != Autotest_Source_Idle && abs(cur_time - AutoTestTarget_Run.start_time)<60)
	AutoTestTarget_Run.start_time = cur_time;
	AutoTestTarget_Run.time = 0;
	AutoTestTarget_Run.test_type = pcmd->test_type;
	AutoTestTarget_Run.talk_time = pcmd->talk_time;
	AutoTestTarget_Run.unlock_time = pcmd->unlock_time;
	AutoTestTarget_Run.cancel_time = pcmd->cancel_time;
	
	if(pcmd->test_type == Autotest_Type_NormalCall)
	{
		if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
		{
			char temp[20];
			API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
			tranferset = atoi(temp);
		}
		
		if(tranferset== 1 || tranferset == 2)
		{
			return;
		}
	
		Get_SelfDevInfo(caller_ip, &self_info);
		if(strcmp(pcmd->target_dev.bd_rm_ms,self_info.bd_rm_ms) == 0)
		{
			if((callscene = Get_CallScene_BySourseInfo(pcmd->source_dev))> 0)
			{
				//czn_20190107_s
				if(callscene == IxCallScene1_Passive)	//czn_20190111
				{
					if(API_Business_Request(Business_State_BeMainCall) == 0 && (CallServer_Run.state != CallServer_Wait || CallServer_Run.source_dev.ip_addr != caller_ip))
						return;
				}
				else if(callscene == IxCallScene2_Passive)
				{
					if(API_Business_Request(Business_State_BeIntercomCall) == 0)
						return;
				}
				else if(callscene == IxCallScene3_Passive)
				{
					if(API_Business_Request(Business_State_BeInnerCall) == 0)
						return;
				}
				//czn_20190107_e
				CallServer_Run.callee_rsp_command_id = rsp_id;
				AutoTestTarget_Run.state = Autotest_Target_Testing;
				API_CallServer_BeInvite(callscene,&pcmd->source_dev);

				
				OS_RetriggerTimer(&timer_atuotest_target);
			}
		}
	}
	else
	{
		
	}
	return rev;
}

int Send_AutoTestCall_Rsp(Call_Dev_Info target_info)
{
	time_t cur_time = time(NULL);
	int rev = 0;
	if(AutoTestTarget_Run.state == Autotest_Target_Idle|| abs(cur_time - AutoTestTarget_Run.start_time)>10)
	{
		rev = 0;
	}
	else
	{
		AutoTestCallCmd_Stru AutoTestCallCmd_rsp;

		memset( (char*)&AutoTestCallCmd_rsp, 0, sizeof(AutoTestCallCmd_Stru) );

		AutoTestCallCmd_rsp.source_dev 		= CallServer_Run.source_dev;
		AutoTestCallCmd_rsp.target_dev 		= CallServer_Run.target_hook_dev;
		AutoTestCallCmd_rsp.rspstate			= CallServer_Run.caller_divert_state;
		AutoTestCallCmd_rsp.master_sip_divert = CallServer_Run.caller_master_sip_divert;

		// 必须要用同步应答的id
		//api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL_REP_1083,(char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru));
		api_udp_c5_ipc_send_rsp( target_info.ip_addr, CMD_AUTOTEST_CALL_RSP, CallServer_Run.callee_rsp_command_id, (char*)&AutoTestCallCmd_rsp,sizeof(AutoTestCallCmd_Stru) );

		rev = 1;
	}
	return rev;
}

void timer_atuotest_target_callback(void)
{
	if(AutoTestTarget_Run.state == Autotest_Target_Testing)
	{
		printf("11111timer_atuotest_target_callback:%d,tt=%d,ut=%d,ct=%d\n",AutoTestTarget_Run.time,AutoTestTarget_Run.talk_time,AutoTestTarget_Run.unlock_time,AutoTestTarget_Run.cancel_time);
		AutoTestTarget_Run.time++;
		
		if(AutoTestTarget_Run.time == AutoTestTarget_Run.talk_time)
		{
			if(CallServer_Run.call_rule == CallRule_Normal)
				API_CallServer_LocalAck(CallServer_Run.call_type);
		}
		else if(AutoTestTarget_Run.time == AutoTestTarget_Run.unlock_time)
		{
			if(CallServer_Run.call_rule == CallRule_Normal)
				API_CallServer_LocalUnlock1(CallServer_Run.call_type);
		}
		else if(AutoTestTarget_Run.time >= AutoTestTarget_Run.cancel_time)
		{
			if(CallServer_Run.call_rule == CallRule_Normal)
			{
				API_CallServer_LocalBye(CallServer_Run.call_type);
				AutoTestTarget_Run.state == Autotest_Target_Idle;
			}
			return;
		}

		OS_RetriggerTimer(&timer_atuotest_target);
	}
}

void API_AutoTest_BecalledCancel(void)
{
	pthread_mutex_lock( &autotest_target_lock);
	AutoTestTarget_Run.state = Autotest_Target_Idle;
	pthread_mutex_unlock( &autotest_target_lock);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

