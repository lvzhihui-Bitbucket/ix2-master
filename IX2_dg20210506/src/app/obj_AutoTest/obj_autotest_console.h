/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_autotest_console_H
#define _obj_autotest_console_H

#include "task_CallServer.h"
enum 
{
	Autotest_Console_Idle = 0,
	Autotest_Console_TestInterval,
	Autotest_Console_Testing,
};

enum 
{
	Autotest_Type_NormalCall = 0,
	Autotest_Type_MuteCall,
};

enum 
{
	Autotest_ParaType_Self = 0,
	Autotest_ParaType_IP,
	Autotest_ParaType_RmNum,
};

enum
{
	Autotest_Msg_StartTest = 0,
	Autotest_Msg_ConitnueTest,
};

enum
{
	Autotest_State_ConsoleClose = 0,
	Autotest_State_CallerClose,
};

typedef struct
{
	int para_type;
	int ip;
	char dev_info[21];
}AutotestDev_Stru;

#pragma pack(1)

typedef struct
{
	int test_type;
	AutotestDev_Stru source_dev;
	AutotestDev_Stru target_dev;
}StartAutoTest_Req_Cmd;

typedef struct
{
	int result;
}StartAutoTest_Rsp_Cmd;

typedef struct
{
	int log_len;
	char log[80];
}StartAutoTest_LogWrite_Cmd;

typedef struct
{
	unsigned short		test_type;			// 呼叫类型
	unsigned short		talk_time;
	unsigned short		unlock_time;
	unsigned short		cancel_time;
	unsigned short 		rspstate;			// 被叫方返回类型 0x00:  正常, 0x01：设备忙，禁止呼叫, 0x02：免扰，禁止呼叫, 0x80：立即转呼, 0x80-0xF0：延时转呼，时间为1-200秒, 0xF1-0xFF：保留
	Call_Dev_Info 		source_dev;			// 主叫方设备信息
	Call_Dev_Info 		target_dev;			// 被叫方设备信息
	sip_account_Info 	master_sip_divert;	// 被叫方转呼sip帐号
}AutoTestCallCmd_Stru;

enum
{
	Call_Result_Succ = 0,
	Call_Result_Talk,
	Call_Result_Divert,
	Call_Result_SearchErr,
	Call_Result_DND,
	Call_Result_Busy,
};

enum
{
	Divert_Result_NoDivert = 0,
	Divert_Result_Ring,	
	Divert_Result_Talk,
	Divert_Result_Fail,
};

enum
{
	Divert_Video_Succ = 0,
	Divert_Video_Fail,	
};

typedef struct
{
	int call_result;
	int divert_result;
	int divert_retry;
	int divert_video;
	int divert_video_retry;
}AutotestResult;

typedef struct
{
	int	notice_type;			// 呼叫类型
	AutotestDev_Stru source_dev;
	AutotestDev_Stru target_dev;
	union
	{
		AutotestResult autotest_result;
	};
}AutotestStateCmd_Stru;

#define AutotestStateCmd_Stru_Head_Length		(sizeof(int)+2*sizeof(AutotestDev_Stru))

#pragma pack()

int get_autotest_call_result(void);
void set_autotest_call_result(int call_result);
	#define set_autotest_call_result_succ()		set_autotest_call_result(Call_Result_Succ)
	#define set_autotest_call_result_talk()			set_autotest_call_result(Call_Result_Talk)
	#define set_autotest_call_result_searcherr()	set_autotest_call_result(Call_Result_SearchErr)
	#define set_autotest_call_result_dnd()			set_autotest_call_result(Call_Result_DND)
	#define set_autotest_call_result_busy()		set_autotest_call_result(Call_Result_Busy)
	#define set_autotest_call_result_divert()		set_autotest_call_result(Call_Result_Divert)
int get_autotest_divert_result(void);
void set_autotest_divert_result(int divert_result);
	#define set_autotest_divert_result_ring()		set_autotest_divert_result(Divert_Result_Ring)
	#define set_autotest_divert_result_talk()		set_autotest_divert_result(Divert_Result_Talk)
	#define set_autotest_divert_result_fail()		set_autotest_divert_result(Divert_Result_Fail)
int get_autotest_divert_video(void);
void set_autotest_divert_video(int divert_video);
	#define set_autotest_divert_video_succ()		set_autotest_divert_video(Divert_Video_Succ)
	#define set_autotest_divert_video_fail()			set_autotest_divert_video(Divert_Video_Fail)
	
int Get_Calltest_TotalNum(void);

int Get_Calltest_SuccNum(void);

float Get_Calltest_SuccPercent(void);

int Get_Divert_TotalNum(void);

int Get_Divert_SuccNum(void);

int Get_Divert_HookNum(void);

int Get_Divert_VideoConnectNum(void);

float Get_Divert_SuccPercent(void);

float Get_Divert_OnceSuccPercent(void);	
#endif


