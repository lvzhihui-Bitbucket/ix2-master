/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
//#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "task_IoServer.h"
#include "obj_TableProcess.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_autotest_console.h"
#include "obj_GetIpByNumber.h"
#include "task_VideoMenu.h"//20210428	#include "MENU_066_FwUpgrade.h"
#include "task_debug_sbu.h"
#include "obj_R8001_Table.h"


pthread_mutex_t autotest_console_lock = PTHREAD_MUTEX_INITIALIZER;
OS_TIMER timer_atuotest_console;

void AutotestByTable_GetOneTargetNum(char *target_num,int index);

typedef struct
{
	int state;
	int test_type;
	int test_count;
	int test_interval;
	int interval_timer;
	int update_time;
	AutotestDev_Stru source_dev;
	AutotestDev_Stru target_dev;
	int use_target_table;
	int target_table_num;
	int target_table_index;
}AutoTestConsole_Run_Stru;

AutoTestConsole_Run_Stru AutoTestConsole_Run ={0};

Loop_vdp_common_buffer	autotest_mesg_queue;
Loop_vdp_common_buffer	autotest_sync_queue;
vdp_task_t				task_autotest = {0};
void autotest_mesg_data_process(char* msg_data,int len);
void* autotest_mesg_task( void* arg );
void vtk_TaskInit_Autotest( unsigned char priority );

void timer_atuotest_console_callback(void);

int Send_StartAutoTest_Req(int test_type,AutotestDev_Stru source_dev,AutotestDev_Stru target_dev);

void AutotestLog_Stat_Init(void);

int autotest_stat_call_total = 0;
int autotest_stat_call_succ = 0;
int autotest_stat_call_fail = 0;
int autotest_stat_divert_total = 0;
int autotest_stat_divert_succ = 0;
int autotest_stat_divert_retry = 0;
int autotest_stat_divert_talk = 0;
int autotest_stat_divert_video_ok = 0;
int autotest_stat_divert_video_retry = 0;

void autotest_console_init(void)
{
	OS_CreateTimer(&timer_atuotest_console, timer_atuotest_console_callback, 1000/25);
}
void AutoTest_Init(void)
{
	autotest_console_init();
	autotest_source_init();
	autotest_target_init();
	vtk_TaskInit_Autotest(0);
	AutotestLog_Stat_Init();
}
int API_Start_AutoTest(int test_type,int test_count,int test_interval,AutotestDev_Stru source_dev,AutotestDev_Stru target_dev,int use_target_table,int table_num)
{
	int rev = 0;
	
	pthread_mutex_lock( &autotest_console_lock);
	OS_StopTimer(&timer_atuotest_console);
	AutoTestConsole_Run.interval_timer = 0;
	AutoTestConsole_Run.test_type = test_type;
	
	if(use_target_table)
		AutoTestConsole_Run.test_count = test_count*table_num;
	else
		AutoTestConsole_Run.test_count = test_count;
	
	AutoTestConsole_Run.test_interval = test_interval;
	AutoTestConsole_Run.source_dev = source_dev;
	AutoTestConsole_Run.target_dev = target_dev;
	AutoTestConsole_Run.use_target_table = use_target_table;
	AutoTestConsole_Run.target_table_num = table_num;
	AutoTestConsole_Run.target_table_index = 0;
	AutoTestConsole_Run.update_time = time(NULL);
	
	autotest_stat_call_total = 0;
	autotest_stat_call_succ = 0;
	autotest_stat_call_fail = 0;
	autotest_stat_divert_total = 0;
	autotest_stat_divert_succ = 0;
	autotest_stat_divert_talk = 0;
	autotest_stat_divert_video_ok = 0;
	autotest_stat_divert_retry = 0;
	autotest_stat_divert_video_retry= 0;
	API_ClearLogViewWin();
	if(AutoTestConsole_Run.use_target_table)
	{
		AutoTestConsole_Run.target_dev.para_type = Autotest_ParaType_RmNum;
		AutotestByTable_GetOneTargetNum(AutoTestConsole_Run.target_dev.dev_info,AutoTestConsole_Run.target_table_index);		
		rev = Send_StartAutoTest_Req(test_type,AutoTestConsole_Run.source_dev,AutoTestConsole_Run.target_dev);
		if(++AutoTestConsole_Run.target_table_index >= AutoTestConsole_Run.target_table_num)
			AutoTestConsole_Run.target_table_index = 0;
	}
	else
	{
		if(source_dev.para_type == Autotest_ParaType_Self)
		{

		}
		else
		{
			rev = Send_StartAutoTest_Req(test_type,source_dev,target_dev);
		}
	}
	if(rev == 0)
	{
		AutoTestConsole_Run.state = Autotest_Console_Testing;
	}
	else
	{
		if(AutoTestConsole_Run.test_count>1)
			AutoTestConsole_Run.state = Autotest_Console_TestInterval;
		else
			AutoTestConsole_Run.state = Autotest_Console_Idle;
	}
	
	AutoTestConsole_Run.test_count--;
	
	if(AutoTestConsole_Run.test_count > 0)
	{
		OS_RetriggerTimer(&timer_atuotest_console);
	}
	
	autotest_stat_call_total++;
	
	pthread_mutex_unlock( &autotest_console_lock);
	printf("!!!!API_Start_AutoTest%d,test_count=%d,interval=%d\n",AutoTestConsole_Run.state,AutoTestConsole_Run.test_count,AutoTestConsole_Run.test_interval);
	return rev;
}
void API_Continue_AutoTest(void)
{
	pthread_mutex_lock( &autotest_console_lock);
	if(AutoTestConsole_Run.state != Autotest_Console_Idle)
	{
		if(autotest_stat_call_total != (autotest_stat_call_succ+autotest_stat_call_fail))
			autotest_stat_call_fail+=(autotest_stat_call_total -autotest_stat_call_succ-autotest_stat_call_fail);
		
		if(AutoTestConsole_Run.test_count > 0)
		{
			AutoTestConsole_Run.update_time = time(NULL);
			API_ClearLogViewWin();
			AutoTestConsole_Run.interval_timer = 0;
			if(AutoTestConsole_Run.use_target_table)
			{
				AutoTestConsole_Run.target_dev.para_type = Autotest_ParaType_RmNum;
				AutotestByTable_GetOneTargetNum(AutoTestConsole_Run.target_dev.dev_info,AutoTestConsole_Run.target_table_index);		
				//rev = Send_StartAutoTest_Req(test_type,AutoTestConsole_Run.source_dev,AutoTestConsole_Run.target_dev);
				if(++AutoTestConsole_Run.target_table_index >= AutoTestConsole_Run.target_table_num)
					AutoTestConsole_Run.target_table_index = 0;
			}
			Send_StartAutoTest_Req(AutoTestConsole_Run.test_type,AutoTestConsole_Run.source_dev,AutoTestConsole_Run.target_dev);
			AutoTestConsole_Run.state = Autotest_Console_Testing;
		}
		
		AutoTestConsole_Run.test_count--;
	
		if(AutoTestConsole_Run.test_count > 0)
		{
			OS_RetriggerTimer(&timer_atuotest_console);
		}
		autotest_stat_call_total++;
	}
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_AutotestStateUpate);
	pthread_mutex_unlock( &autotest_console_lock);
}
void timer_atuotest_console_callback(void)
{
	VDP_MSG_HEAD send_msg;
	
	AutoPowerOffReset();
	
	AutoTestConsole_Run.interval_timer++;
	//printf("timer_atuotest_console_callback%d,test_count=%d,interval=%d\n",AutoTestConsole_Run.interval_timer,AutoTestConsole_Run.test_count,AutoTestConsole_Run.test_interval);
	if(AutoTestConsole_Run.interval_timer >= AutoTestConsole_Run.test_interval)
	{
		send_msg.msg_type 		= Autotest_Msg_ConitnueTest;
		send_msg.msg_sub_type	= 0;
		
		//if (OS_Q_Put(&q_phone, &send_msg_to_caller, sizeof(CALLER_STRUCT)))
		if(push_vdp_common_queue(&autotest_mesg_queue, (char *)&send_msg, sizeof(VDP_MSG_HEAD)) ==0 )
		{
			return;
		}
		AutoTestConsole_Run.interval_timer--;
	}
	OS_RetriggerTimer(&timer_atuotest_console);
	
}

int Send_StartAutoTest_Req(int test_type,AutotestDev_Stru source_dev,AutotestDev_Stru target_dev)
{
	StartAutoTest_Req_Cmd send_cmd;
	StartAutoTest_Rsp_Cmd rsp_cmd;
	int rsp_len = sizeof(StartAutoTest_Rsp_Cmd);

	send_cmd.test_type = test_type;
	send_cmd.source_dev = source_dev;
	send_cmd.target_dev = target_dev;

	if(api_udp_c5_ipc_send_req(source_dev.ip,CMD_AUTOTEST_START_REQ,(char*)&send_cmd,sizeof(StartAutoTest_Req_Cmd),(char*)&rsp_cmd,&rsp_len) != 0)
	{
		return -1;
	}

	if(rsp_cmd.result != 0)
	{
		return -1;
	}

	return 0;
}

int Send_StartAutoTest_Rsp(int console_ip,int rsp_id,int result)
{
	StartAutoTest_Rsp_Cmd send_cmd;

	send_cmd.result = result;

	api_udp_c5_ipc_send_rsp(console_ip,CMD_AUTOTEST_START_RSP,rsp_id,(char*)&send_cmd,sizeof(StartAutoTest_Rsp_Cmd));

	return 0;
}

void Receive_AutoTest_Log(int source_ip,char *log,int log_len)
{
#if 1
	char buff[100]={0};
	memcpy(buff,log,15);
	printf("11111recv autotest log:%d:%s\n",log_len,buff);
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN,"T%04d^%s",autotest_stat_call_total,log);	
	printf("22222recv autotest log:%d:%s\n",log_len,detail);
	API_add_log_item(LOG_CallTest_Level,Log_CallTest_Title,detail,NULL);
#endif
}

void Receive_AutoTest_StateNotice(int source_ip,int notice_type,AutotestDev_Stru source_dev,AutotestDev_Stru target_dev,void *ext_data)
{
	printf("222222222Receive_AutoTest_StateNotice%d,notice_type=\n",AutoTestConsole_Run.state,notice_type);
	if(notice_type == Autotest_State_CallerClose)
	{
		AutotestResult *presult = (AutotestResult*)ext_data;
		
		pthread_mutex_lock( &autotest_console_lock);
		if(AutoTestConsole_Run.state != Autotest_Console_Idle)
		{
			if(AutoTestConsole_Run.state == Autotest_Console_Testing&&autotest_stat_call_total!=(autotest_stat_call_succ+autotest_stat_call_fail))
			{
				if(presult->call_result == Call_Result_Succ||presult->call_result == Call_Result_Talk||presult->call_result ==Call_Result_Divert)
					autotest_stat_call_succ++;
				else
					autotest_stat_call_fail++;
				if(presult->divert_result != Divert_Result_NoDivert)
				{
					autotest_stat_divert_total++;
					if(presult->divert_result == Divert_Result_Ring || presult->divert_result == Divert_Result_Talk)
					{
						autotest_stat_divert_succ++;
						if(presult->divert_retry>0)
							autotest_stat_divert_retry++;
						if(presult->divert_result == Divert_Result_Talk)
						{
							autotest_stat_divert_talk++;
							
							if(presult->divert_video_retry>0)
								autotest_stat_divert_video_retry++;
							
							if(presult->divert_video == Divert_Video_Succ)
							{
								autotest_stat_divert_video_ok++;
							}
						}
					}
					else
					{
						;
					}
				}
			}
			
			if(AutoTestConsole_Run.test_count>0)
				AutoTestConsole_Run.state = Autotest_Console_TestInterval;
			else
				AutoTestConsole_Run.state = Autotest_Console_Idle;
			//will add
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_AutotestStateUpate);
		}
		pthread_mutex_unlock( &autotest_console_lock);
	}
}
int Send_AutoTestState_ConsoleClose(void)
{
	AutotestStateCmd_Stru send_cmd;

	send_cmd.notice_type = Autotest_State_ConsoleClose;
	send_cmd.source_dev = AutoTestConsole_Run.source_dev;
	send_cmd.target_dev = AutoTestConsole_Run.target_dev;

	api_udp_c5_ipc_send_data(AutoTestConsole_Run.source_dev.ip,CMD_AUTOTEST_STATE_REQ,(char*)&send_cmd,sizeof(AutotestStateCmd_Stru));
}
void API_AutoTest_Console_Close(void)
{
	pthread_mutex_lock(&autotest_console_lock);
	if(AutoTestConsole_Run.state != Autotest_Console_Idle)
	{
		Send_AutoTestState_ConsoleClose();
		AutoTestConsole_Run.state = Autotest_Console_Idle;
	}
	pthread_mutex_unlock(&autotest_console_lock);
}

void AutoTest_Cmd_Process(int source_ip,int cmd,int sn,uint8 *pkt , uint16 len)
{
	StartAutoTest_Req_Cmd *pstart_test;
	StartAutoTest_LogWrite_Cmd *plog_write;
	AutoTestCallCmd_Stru *pstart_call_test;
	AutotestStateCmd_Stru *pstate;
	switch(cmd)
	{
		case CMD_AUTOTEST_START_REQ:
			pstart_test = (StartAutoTest_Req_Cmd *)pkt;
			Receive_AutoTest_Start(source_ip,sn,pstart_test->test_type,pstart_test->source_dev,pstart_test->target_dev);
			break;

		case CMD_AUTOTEST_CALL_REQ:
			pstart_call_test = (AutoTestCallCmd_Stru *)pkt;
			Receive_AutoTest_StartCall(source_ip,sn,pstart_call_test);
			break;

		case CMD_AUTOTEST_LOG_REQ:
			plog_write = (StartAutoTest_LogWrite_Cmd*)pkt;
			Receive_AutoTest_Log(source_ip,plog_write->log,plog_write->log_len);
			break;

		case CMD_AUTOTEST_STATE_REQ:
			pstate = (AutotestStateCmd_Stru*)pkt;
			Receive_AutoTest_StateNotice(source_ip,pstate->notice_type,pstate->source_dev,pstate->target_dev,pkt+AutotestStateCmd_Stru_Head_Length);
			break;
	}
}
#if 0
void autotest_process_test(void)
{
	int ds_nums;
	AutotestDev_Stru t_dev,s_dev;
	
	
	AddMonRes_Temp_TableBySearching();
	ds_nums = Get_MonRes_Temp_Table_Num();
	
	printf("!!!!!!autotest_process_test_Process ds_nums = %d\n",ds_nums);
	

	if(ds_nums >= 1)
	{
		
		char bdRmMs[11];
		GetIpRspData data;
		//Call_Dev_Info target_dev;
		
		if(Get_MonRes_Temp_Table_Record(0,NULL,bdRmMs, NULL) != 0)
		{
			return;
		}	
		
		if(API_GetIpNumberFromNet(bdRmMs, NULL, NULL, 2, 1, &data) != 0)
		{
			
			return;
		}	
		
	
		s_dev.para_type = Autotest_ParaType_IP;
		s_dev.ip = data.Ip[0];
		t_dev.para_type = Autotest_ParaType_RmNum;
		strcpy(t_dev.dev_info,GetSysVerInfo_BdRmMs());
		
		if(API_Start_AutoTest(Autotest_Type_NormalCall,3,40,s_dev,t_dev)!=0)
		//if(Send_CmdCallbackReq(data.Ip[0],target_dev)!=0)
		{
			return;
		}

		
		//BEEP_CONFIRM();
		return;
	}

	
}
#endif
void API_Start_AutoTestByRmNum(int test_type,int test_count,int test_interval,char *source_num,char *target_num)
{
	GetIpRspData data;
	int ds_nums;
	AutotestDev_Stru t_dev,s_dev;
	
	if(API_GetIpNumberFromNet(source_num, NULL, NULL, 2, 1, &data) != 0)
		return;

	s_dev.para_type = Autotest_ParaType_IP;
	s_dev.ip = data.Ip[0];
	t_dev.para_type = Autotest_ParaType_RmNum;
	strcpy(t_dev.dev_info,target_num);
	
	if(API_Start_AutoTest(test_type,test_count,test_interval,s_dev,t_dev,0,0)!=0)
	//if(Send_CmdCallbackReq(data.Ip[0],target_dev)!=0)
	{
		return;
	}
}
void API_Start_AutoTestByTable(int test_type,int test_count,int test_interval,char *source_num,int target_tb_num)
{
	GetIpRspData data;
	int ds_nums;
	AutotestDev_Stru t_dev,s_dev;
	
	if(API_GetIpNumberFromNet(source_num, NULL, NULL, 2, 1, &data) != 0)
		return;

	s_dev.para_type = Autotest_ParaType_IP;
	s_dev.ip = data.Ip[0];
	memset(&t_dev,0,sizeof(AutotestDev_Stru));
	//strcpy(t_dev.dev_info,target_num);
	
	if(API_Start_AutoTest(test_type,test_count,test_interval,s_dev,t_dev,1,target_tb_num)!=0)
	//if(Send_CmdCallbackReq(data.Ip[0],target_dev)!=0)
	{
		return;
	}
}

void vtk_TaskInit_Autotest( unsigned char priority )
{
	init_vdp_common_queue(&autotest_mesg_queue, 500, (msg_process)autotest_mesg_data_process, &task_autotest);
	init_vdp_common_queue(&autotest_sync_queue, 100, NULL, 								  &task_autotest);
	init_vdp_common_task(&task_autotest, MSG_ID_Autotest, autotest_mesg_task, &autotest_mesg_queue, &autotest_sync_queue);

	dprintf("survey task starting............\n");
}

void vtk_TaskExit_Autotest(void)
{
	exit_vdp_common_queue(&autotest_mesg_queue);
	exit_vdp_common_queue(&autotest_sync_queue);
	exit_vdp_common_task(&task_autotest);
}

void autotest_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pVdpUdp = (VDP_MSG_HEAD*)msg_data;

	// 判断是否为系统服务
	switch(pVdpUdp->msg_type)
	{
		case Autotest_Msg_ConitnueTest:
			API_Continue_AutoTest();
			break;
	}
}

void* autotest_mesg_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int get_autotest_state(void)
{
	return AutoTestConsole_Run.state;
}

int judge_autotest_targetIsself(void)
{
	if(strcmp(AutoTestConsole_Run.target_dev.dev_info,GetSysVerInfo_BdRmMs())==0 && abs(AutoTestConsole_Run.update_time -time(NULL))<180)
	{
		return 1;
	}
	return 0;
}

int get_autotest_remain(void)
{
	return AutoTestConsole_Run.test_count;
}


int Get_Calltest_TotalNum(void)
{
	return (autotest_stat_call_succ+autotest_stat_call_fail);
}

int Get_Calltest_SuccNum(void)
{	
	return autotest_stat_call_succ;
}

float Get_Calltest_SuccPercent(void)
{
	float v1,v2,result;
	
	v1 = (float)autotest_stat_call_succ;
	v2 = (float)(autotest_stat_call_succ+autotest_stat_call_fail);
	
	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

int Get_Divert_TotalNum(void)
{
	return autotest_stat_divert_total;
}

int Get_Divert_SuccNum(void)
{	
	return autotest_stat_divert_succ;
}

int Get_Divert_HookNum(void)
{
	return autotest_stat_divert_talk;
}

int Get_Divert_VideoConnectNum(void)
{
	return autotest_stat_divert_video_ok;
}

float Get_Divert_SuccPercent(void)
{
	float v1,v2,result;
	
	v1 = (float)autotest_stat_divert_succ;
	v2 = (float)autotest_stat_divert_total;
	
	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}

float Get_Divert_OnceSuccPercent(void)
{
	float v1,v2,result;
	
	v1 = (float)autotest_stat_divert_succ-autotest_stat_divert_retry;
	v2 = (float)autotest_stat_divert_total;
	
	if(v2 == 0.0)
	{
		result = 0;
	}
	else
	{
		result = v1/v2*100.0;
	}
	
	return result;
}


///////////////////////////////////
int AutotestLog_start_line_save = 0;
extern char logfile_name[];
extern int	  logfile_line;

void AutotestLog_Stat_Init(void)	//czn_20181027
{
	FILE *pf = fopen(logfile_name,"r");
	char line_buff[LOG_DESC_LEN*2];
	int line_cnt = 0;
	
	
	AutotestLog_start_line_save = 0;
	
	
	if(pf!=NULL)
	{
		line_buff[0] = 0;
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			line_cnt ++;
			if(( strstr(line_buff,Log_CallTest_Title)) != NULL)
			{
				
				//if(tindex == 0||tindex == 1)
				{
					//if(divert_total >1)
					{
						
						//siplog_start_line_save = line_cnt;
						if(strstr(line_buff,"StatClear") != NULL)
						{
							AutotestLog_start_line_save = line_cnt;
						}
						
					}
				}
			}
			
			line_buff[0] = 0;	
		}
		fclose(pf);
	}
}

void AutotestLog_Stat_Clear(void)
{
	AutotestLog_start_line_save = logfile_line+1;
	
	char detail[LOG_DESC_LEN+1];
	snprintf(detail,LOG_DESC_LEN+1,"T0000^StatClear");	
	API_add_log_item(LOG_CallTest_Level,Log_CallTest_Title,detail,NULL);
	//sip_delay_stat_init();
}

int AutotestLog_Num;
int *AutotestLog_Index_Tb = NULL;

void Create_AutotestLog_Index_Tb(void)
{
	int Index_max;
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	
	if(AutotestLog_Index_Tb  != NULL)
	{
		free(AutotestLog_Index_Tb);
		AutotestLog_Index_Tb = NULL;
	}
	AutotestLog_Num = 0;
	
	FILE *pf = fopen(logfile_name,"r");

	
	if(pf != NULL)
	{
		Index_max = logfile_line - AutotestLog_start_line_save;
		//printf("!!!!!!!!!!!!!logfile_line =%d,siplog_start_line_save =%d\n",logfile_line,siplog_start_line_save);
		if(Index_max > 0)
		{
			AutotestLog_Index_Tb = malloc(sizeof(int)*Index_max);

			if(AutotestLog_Index_Tb != NULL)
			{
				while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
				{
					if(++line_cnt >= AutotestLog_start_line_save)
					{
						if(strstr(line_buff,Log_CallTest_Title)!= NULL)
						{
							if(strstr(line_buff,"StatClear")== NULL)
							{
								
								AutotestLog_Index_Tb[AutotestLog_Num++] = line_cnt;

								if(AutotestLog_Num >= Index_max)
									break;
							}
						}
					}
					//printf("!!!!!!!!!!!!!SipLog_Num=%d,line_cnt = %d,%s\n",SipLog_Num,line_cnt,line_buff);
				}
			}
		}
		fclose(pf);
	}

	//printf("!!!!!!!!!!!!!SipLog_Num=%d,line_cnt = %d\n",SipLog_Num,line_cnt);
}

void free_AutotestLog_Index_Tb(void)
{
	if(AutotestLog_Index_Tb  != NULL)
	{
		free(AutotestLog_Index_Tb);
		AutotestLog_Index_Tb = NULL;
	}
	AutotestLog_Num = 0;
}

int Get_AutotestLog_Num(void)
{
	return AutotestLog_Num;
}

int Get_One_AutotestLog(int index,char *log_str)
{
	int line_cnt = 0;
	char line_buff[LOG_DESC_LEN*2];
	char strbuff[LOG_DESC_LEN];
	//int strindex = 0;
	char *pch1,*pch2,*str;
	int result = -1;
	
	if(AutotestLog_Index_Tb == NULL || index >= AutotestLog_Num)
	{
		return -1;
	}
	str = strbuff;
	
	FILE *pf = fopen(logfile_name,"r");
	index = AutotestLog_Num-1-index; 
	if(pf != NULL)
	{
		while(fgets(line_buff,LOG_DESC_LEN*2,pf) != NULL)
		{
			if(++line_cnt == AutotestLog_Index_Tb[index])
			{
				if(strstr(line_buff,Log_CallTest_Title)!= NULL)
				{
					pch1 = strstr(line_buff," ");
					
					if(pch1 != NULL)
					{
						pch1 += 3;
						memcpy(str,pch1,2);
						str += 2;
						pch1+=2;
						*str++ = '/';
						pch2 = strstr(pch1," ");
						memcpy(str,pch1,pch2-pch1+1);
						str += (pch2-pch1+1);
						pch2++;
						pch1 = strstr(pch2," ");//T0000^
						pch1 += 7;
						pch2 = strstr(pch1," ");

						memcpy(str,pch1,pch2-pch1);
						str += (pch2-pch1);
						*str = 0;
						str = strbuff;
						while(*str)
						{
							if(*str == '_')
							{
								*str = ' ';
							}
							str++;
						}
						strcpy(log_str,strbuff);
					}
					result = 0;
				}
				break;
			}
		}
		fclose(pf);
	}

	return result;
}

void AutotestByTable_GetOneTargetNum(char *target_num,int index)
{
//	char test_tb[2][11]={"0099000101","0099000201"};
	CallNbr_T imCallNbr;
	if(index>=get_r8001_record_cnt())
		index = 0;
	get_r8001_record_items(index,&imCallNbr);
	strcpy(target_num,imCallNbr.BD_RM_MS);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

