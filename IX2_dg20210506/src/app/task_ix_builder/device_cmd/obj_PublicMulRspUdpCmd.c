/**
  ******************************************************************************
  * @file    obj_PublicUdpCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "task_VideoMenu.h"
#include "obj_PublicUdpCmd.h"
#include "obj_PublicUnicastCmd.h"
#include "vtk_udp_stack_device_update.h"

int FileManagementProcess(const char* reqData, char** rspData, int* rspLen);
int TftpWriteFileReqProcess(const char* reqData, char** rspData, int* rspDataLen);
int TftpReadFileReqProcess(const char* reqData, char** rspData, int* rspDataLen);
int FileVerifyReqProcess(const char* reqData, char** rspData, int* rspDataLen);

static PublicMulRspUdpCmdRun fileManagementRun = {.process = FileManagementProcess};
static PublicMulRspUdpCmdRun tftpWriteRun = {.process = TftpWriteFileReqProcess};
static PublicMulRspUdpCmdRun tftpReadRun = {.process = TftpReadFileReqProcess};
static PublicMulRspUdpCmdRun fileVarifyRun = {.process = FileVerifyReqProcess};

static PublicMulRspUdpCmdRun* GetPublicMulRspUdpCmdRun(unsigned short cmd)
{
	PublicMulRspUdpCmdRun *pReturn = NULL;
	switch(cmd)
	{
		case FileManagement_REQ:
		case FileManagement_RSP:
			pReturn = &fileManagementRun;
			break;

		case TFTP_WRITE_FILE_REQ:
		case TFTP_WRITE_FILE_RSP:
			pReturn = &tftpWriteRun;
			break;
		case TFTP_READ_FILE_REQ:
		case TFTP_READ_FILE_RSP:
			pReturn = &tftpReadRun;
			break;

		case TFTP_CHECK_FILE_REQ:
		case TFTP_CHECK_FILE_RSP:
			pReturn = &fileVarifyRun;
		default:
			break;
	}
	
	return pReturn;
}

int PublicMulRspUdpCmdReqProcess(int ip, int timeOut, unsigned short cmd, const char* data, char** retData, int* retLen)
{
	PublicMulRspUdpCmdRun* publicUdpCmdRun;
	PublicMulRspUdpCmdReq sendData;
	int i, j, timeCnt, ret, myIp, dataLen;
	char* tempData = NULL;
	int remoteFlag = 0;

	ret = -1;

	publicUdpCmdRun = GetPublicMulRspUdpCmdRun(cmd);
	if(publicUdpCmdRun == NULL)
	{
		return -1;
	}

	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	if(myIp == ip)
	{
		if(publicUdpCmdRun->process != NULL)
		{
			ret = publicUdpCmdRun->process(data, retData, retLen);
		}
	}
	else
	{		
		if(strlen(data) > PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN)
		{
			return -2;
		}
		if(publicUdpCmdRun->waitFlag)
		{
			return -3;
		}

		publicUdpCmdRun->waitFlag = 1;
		publicUdpCmdRun->result = -1;
		publicUdpCmdRun->rand = MyRand() + MyRand2(ip);
		publicUdpCmdRun->packetCnt = 0;
		publicUdpCmdRun->packet = NULL;

		sendData.rand = publicUdpCmdRun->rand;
		sendData.sourceIp = myIp;
		sendData.dataLen = strlen(data);
		memcpy(sendData.data, data, sendData.dataLen);

		api_udp_device_update_send_data(ip, htons(cmd), (char*)&sendData, sizeof(sendData) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + sendData.dataLen);
		remoteFlag = 1;
		
		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(1)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			if(publicUdpCmdRun->waitFlag == 0)
			{
				ret = publicUdpCmdRun->result;
				if(ret == 0)
				{
					for(i = 0, dataLen = 0; i < publicUdpCmdRun->packetCnt; i++)
					{
						dataLen += publicUdpCmdRun->packet[i].dataLen;
					}

					if(retData != NULL)
					{
						if(dataLen)
						{
							tempData = malloc(dataLen);
						}

						if(tempData != NULL)
						{
							for(i = 0, dataLen = 0; i < publicUdpCmdRun->packetCnt; i++)
							{
								for(j = 0; j < publicUdpCmdRun->packetCnt; j++)
								{
									if(i+1 == publicUdpCmdRun->packet[j].no)
									{
										memcpy(tempData + dataLen, publicUdpCmdRun->packet[j].data, publicUdpCmdRun->packet[j].dataLen);
										dataLen += publicUdpCmdRun->packet[j].dataLen;
										break;
									}
								}
							}
						}

						*retData = tempData;
					}

					if(retLen != NULL)
					{
						*retLen = dataLen;
					}
				}
				break;
			}

			if(--timeCnt == 0)
			{
				ret = -1;
				break;
			}
		}
		
		publicUdpCmdRun->waitFlag = 0;
	}
	
	if(remoteFlag)
	{
		for(j = 0; j < publicUdpCmdRun->packetCnt; j++)
		{
			if(publicUdpCmdRun->packet[j].data != NULL)
			{
				free(publicUdpCmdRun->packet[j].data);
				publicUdpCmdRun->packet[j].data = NULL;
			}
		}

		if(publicUdpCmdRun->packet != NULL)
		{
			free(publicUdpCmdRun->packet);
			publicUdpCmdRun->packet = NULL;
		}
	}
	
	return ret;
}

//接收到PublicMulRspUdp命令应答指令
void ReceivePublicMulRspUdpCmdRsp(unsigned short cmd, PublicMulRspUdpCmdRsp* rspData)
{
	PublicMulRspUdpCmdRun* publicUdpCmdRun;
	int i, j;

	publicUdpCmdRun = GetPublicMulRspUdpCmdRun(cmd);
	if(publicUdpCmdRun != NULL)
	{
		if(publicUdpCmdRun->waitFlag == 1)
		{
			if(rspData->rand == publicUdpCmdRun->rand)
			{
				publicUdpCmdRun->sourceIp = rspData->sourceIp;
				publicUdpCmdRun->result = rspData->result;

				if(publicUdpCmdRun->packet == NULL)
				{
					publicUdpCmdRun->packet = malloc(rspData->packetTotal * sizeof(PublicMulRspPacket));
				}

				if(publicUdpCmdRun->packet != NULL)
				{
					for(i = 0; i < publicUdpCmdRun->packetCnt; i++)
					{
						if(rspData->packetNo == publicUdpCmdRun->packet[i].no)
						{
							break;
						}
					}

					if(i == publicUdpCmdRun->packetCnt)
					{
						publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].no = rspData->packetNo;
						publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].dataLen = rspData->dataLen;
						if(rspData->dataLen)
						{
							publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].data = malloc(rspData->dataLen);
							if(publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].data != NULL)
							{
								memcpy(publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].data, rspData->data, rspData->dataLen);
							}
						}
						else
						{
							publicUdpCmdRun->packet[publicUdpCmdRun->packetCnt].data = NULL;
						}

						publicUdpCmdRun->packetCnt++;
						//dprintf("packetNo=%d, packetTotal=%d, packetCnt=%d, dataLen=%d\n", rspData->packetNo, rspData->packetTotal, publicUdpCmdRun->packetCnt, rspData->dataLen);
					}

					if(publicUdpCmdRun->packetCnt == rspData->packetTotal)
					{
						publicUdpCmdRun->waitFlag = 0;
					}
				}
			}
		}
	}
}

//接收到PublicMulRspUdp命令请求指令
int ReceivePublicMulRspUdpCmdReq(unsigned short cmd, PublicMulRspUdpCmdReq* reqData, int len)
{
	PublicMulRspUdpCmdRsp	sendData;
	PublicMulRspUdpCmdRun* publicUdpCmdRun;
	PublicUnicastCmdReq* reqData2 = (PublicUnicastCmdReq*)reqData;
	int reqPacketType;
	int targetIp;

	char *rspData = NULL;
	int rspDataLen;
	int i;
	publicUdpCmdRun = GetPublicMulRspUdpCmdRun(cmd);
	if(publicUdpCmdRun == NULL)
	{
		return -1;
	}

	if(publicUdpCmdRun->process == NULL)
	{
		return -2;
	}

	if(len == sizeof(PublicMulRspUdpCmdReq) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + reqData->dataLen)
	{
		reqPacketType = 0;
	}
	else if(len == sizeof(PublicUnicastCmdReq) - PUBLIC_UNICAST_DATA_LEN + reqData2->dataLen)
	{
		reqPacketType = 1;
	}
	else
	{
		dprintf("ReceivePublicMulRspUdpCmdReq packet error. len = %d\n", len);
		return -3;
	}




	sendData.result = publicUdpCmdRun->process((reqPacketType ? reqData2->data : reqData->data), &rspData, &rspDataLen);
	sendData.rand = (reqPacketType ? reqData2->rand : reqData->rand);
	targetIp = (reqPacketType ? reqData2->sourceIp : reqData->sourceIp);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(targetIp)));
	if(rspDataLen == 0 || rspData == NULL)
	{
		sendData.packetTotal = 1;
		api_udp_device_update_send_data(targetIp, htons(cmd+1), (char*)&sendData, sizeof(sendData) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN);
	}
	else
	{
		sendData.packetTotal = rspDataLen/PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + ((rspDataLen % PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN) ? 1 : 0);
		for(i = 0; i < sendData.packetTotal; i++)
		{
			sendData.dataLen = (rspDataLen - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN*i > PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN ? PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN : rspDataLen - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN*i);
			sendData.packetNo = i+1;
			memcpy(sendData.data, rspData + PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN*i, sendData.dataLen);
			LOCAL_CMD_RSP_DELAY();
			api_udp_device_update_send_data(targetIp, htons(cmd+1), (char*)&sendData, sizeof(sendData) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + sendData.dataLen);
			//dprintf("packetNo=%d, packetTotal=%d, dataLen=%d\n", sendData.packetNo, sendData.packetTotal, sendData.dataLen);
		}
	}
	if(rspData != NULL)
	{
		free(rspData);
		rspData = NULL;
	}

	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

