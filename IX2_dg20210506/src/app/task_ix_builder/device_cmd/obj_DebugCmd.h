/**
  ******************************************************************************
  * @file    obj_DebugCmd.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_DebugCmd_H
#define _obj_DebugCmd_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int			mic;
	int			spk;
	int			music;
	int			autoLock;
	int			autoTalk;
	int			testMode;
} TEST_MODE_T;


typedef struct
{
	int			waitFlag;			//等待回应标记
	int			rand;				//随机数
	int			debugState;			//安装模式
	TEST_MODE_T test;
} DebugStateRun_T;


typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	psw[8+1];			//密码
} DebugStateAskReq;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		state;
	char	jsonChar[500];		//json格式的数据
} DebugStateAskRsp;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
} DebugStateExitReq;


#pragma pack()



// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


