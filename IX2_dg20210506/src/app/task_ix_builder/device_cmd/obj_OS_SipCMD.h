/**
  ******************************************************************************
  * @file    obj_OS_SipCMD.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_OS_SipCMD_H
#define _obj_OS_SipCMD_H

// Define Object Property-------------------------------------------------------
#define SIP_CONFIG_MAX_DEVICE		30

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	BD_RM_MS[10+1];		//BD_RM_MS
	int		result;
} SipCfgDevice;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				sourceIp;			//源ip
	char			BD_RM_MS[10+1];		//BD_RM_MS
	int				result;
} SipCfgRun;


// GetIpBy Number 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	BD_RM_MS[10+1];		//BD_RM_MS
	int		cmd;
} SipCfgCmdReq;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	BD_RM_MS[10+1];		//BD_RM_MS
	int		result;
} SipCfgCmdRsp;

typedef struct
{
	int				cnt;			//等待回应标记
	SipCfgDevice	device[SIP_CONFIG_MAX_DEVICE];
} SipCfgResult;



#pragma pack()



// Define Object Function - Public----------------------------------------------
int API_SipConfigCmd(int cmd, char* BD_RM_MS, SipCfgResult* cfgData, int timeOut);
void ReceiveSipConfigCmdRsp(SipCfgCmdRsp* rspData);
void ReceiveSipConfigCmdReq(int target_ip, SipCfgCmdReq* pReadReq);
void API_SipConfigCmdRsp(int targetIp, int rand, int myIp, char* myBD_RM_MS, int result);


// Define Object Function - Private---------------------------------------------


#endif


