/**
  ******************************************************************************
  * @file    obj_PublicUnicastCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.3.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUnicastCmd.h"
#include "task_VideoMenu.h"
#include "define_string.h"

int RemoteTableProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
static int Ring2Process(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int EventCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int SetDivertStateProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
static int GetPbByIpProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int ShellCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int IXS_ProxyUpdateDevTbRsp(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);

static const PublicUnicastCmdtoProcess UnicastCmdTable[] = {
	{TABLE_PROCESS_REQ,					TABLE_PROCESS_RSP,			RemoteTableProcess},
	{EVENT_REQ, 						EVENT_RSP, 					EventCmdProcess},
	{SHELL_REQ, 						SHELL_RSP, 					ShellCmdProcess},
	{GET_PB_BY_IP_REQ, 					GET_PB_BY_IP_RSP, 			GetPbByIpProcess},
	{GET_RW_IO_BY_IP_REQ, 				GET_RW_IO_BY_IP_RSP, 		NULL},
	{IX_Ring2_REQ,						IX_Ring2_RSP,				Ring2Process},
	{SET_DIVERT_STATE_REQ,				SET_DIVERT_STATE_RSP,		SetDivertStateProcess},
	{GET_QR_CODE_REQ,					GET_QR_CODE_RSP,			NULL},
	{UPDATE_DEV_TABLE_REQ, 				UPDATE_DEV_TABLE_RSP, 		IXS_ProxyUpdateDevTbRsp},
	
	};

static const int UnicastCmdTableNum = sizeof(UnicastCmdTable)/sizeof(UnicastCmdTable[0]);
static MyPublicUnicastTable myPublicUnicastTable = {.lock = PTHREAD_MUTEX_INITIALIZER, .table = NULL};

static PublicUnicastCmdRun* CreatePublicUnicastCmdRun(int targetIp)
{
	int rand;
	cJSON* element = NULL;
	PublicUnicastCmdRun* publicUnicastCmdRun = NULL;

	pthread_mutex_lock(&myPublicUnicastTable.lock);

	if(myPublicUnicastTable.table == NULL)
	{
		myPublicUnicastTable.table = cJSON_CreateArray();
	}

	do
	{
		rand = MyRand() + MyRand2(targetIp);
		cJSON_ArrayForEach(element, myPublicUnicastTable.table)
		{
			publicUnicastCmdRun = (PublicUnicastCmdRun*)element->valueint;
			if(publicUnicastCmdRun->rand == rand)
			{
				break;
			}
		}
	} while(element);

	publicUnicastCmdRun = malloc(sizeof(PublicUnicastCmdRun));
	element = cJSON_CreateNumber((int)publicUnicastCmdRun);
	cJSON_AddItemToArray(myPublicUnicastTable.table, element);

	publicUnicastCmdRun->rand = rand;
	publicUnicastCmdRun->waitFlag = 1;
	publicUnicastCmdRun->result = -1;
	publicUnicastCmdRun->targetIp = targetIp;
	publicUnicastCmdRun->packetCnt = 0;
	publicUnicastCmdRun->packet = NULL;

	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return publicUnicastCmdRun;
}

static void FreePublicUnicastCmdRun(PublicUnicastCmdRun* publicUnicastCmdRun)
{
	cJSON* element = NULL;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	if(publicUnicastCmdRun)
	{
		free(publicUnicastCmdRun);
	}

	cJSON_ArrayForEach(element, myPublicUnicastTable.table)
	{
		if(((int)publicUnicastCmdRun) == element->valueint)
		{
			cJSON_DetachItemViaPointer(myPublicUnicastTable.table, element);
			cJSON_Delete(element);
			break;
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);
}

static PublicUnicastCmdProcess GetPublicUnicastCmdProcess(unsigned short cmd)
{
	int i;
	PublicUnicastCmdProcess			process;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	for(i = 0; i < UnicastCmdTableNum; i++)
	{
		if(cmd == UnicastCmdTable[i].reqCmd || cmd == UnicastCmdTable[i].rspCmd)
		{
			process = UnicastCmdTable[i].process;
			break;
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return process;
}

static unsigned short GetPublicUnicastRspCmd(unsigned short cmd)
{
	int i;
	unsigned short rspCmd = 0;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	for(i = 0; i < UnicastCmdTableNum; i++)
	{
		if(cmd == UnicastCmdTable[i].reqCmd)
		{
			rspCmd = UnicastCmdTable[i].rspCmd;
			break;
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return rspCmd;
}

static int VerifyMFG_SN(const char* mfg_sn, const char* mfg_sn2)
{
	if(mfg_sn == NULL || mfg_sn2 == NULL)
	{
		return 1;
	}
	else if(mfg_sn[0] == 0 || mfg_sn2[0] == 0)
	{
		return 1;
	}
	else if(!strcmp(mfg_sn, mfg_sn2))
	{
		return 1;
	}

	return 0;
}

int PublicUnicastCmdRequst(int ip, const char* mfg_sn, int timeOut, unsigned short cmd, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	PublicUnicastCmdRun* publicUnicastCmdRun;
	PublicUnicastCmdReq sendData;
	PublicUnicastCmdProcess process = GetPublicUnicastCmdProcess(cmd);
	cJSON * element;

	int rand, i, j, timeCnt, ret, myIp, dataLen;
	char* tempData = NULL;

	ret = -1;

	sendData.dataLen = dataIn.len;
	if(sendData.dataLen > PUBLIC_UNICAST_DATA_LEN)
	{
		return -2;
	}

	if(dataIn.data == NULL || sendData.dataLen == 0)
	{
		sendData.data[0] = 0;
	}
	else
	{
		memcpy(sendData.data, dataIn.data, sendData.dataLen);
	}

	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	if(myIp == ip)
	{
		if(VerifyMFG_SN(mfg_sn, GetSysVerInfo_Sn()))
		{
			if(process)
			{
				ret = process(ip, dataIn, dataOut);
			}
			else
			{
				ret = -4;
			}
		}
		else
		{
			ret = -3;
		}
	}
	else
	{		
		publicUnicastCmdRun = CreatePublicUnicastCmdRun(ip);

		sendData.rand = publicUnicastCmdRun->rand;
		sendData.sourceIp = myIp;
		strncpy(sendData.MFG_SN, (mfg_sn == NULL ? "" : mfg_sn), 13);

		api_udp_device_update_send_data(ip, htons(cmd), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN + sendData.dataLen);

		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(timeCnt)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			if(publicUnicastCmdRun->waitFlag == 0)
			{
				ret = publicUnicastCmdRun->result;
				if(ret == 0)
				{
					for(i = 0, dataLen = 0; i < publicUnicastCmdRun->packetCnt; i++)
					{
						dataLen += publicUnicastCmdRun->packet[i].dataLen;
					}

					if(dataOut != NULL)
					{
						if(dataLen)
						{
							tempData = malloc(dataLen);
						}

						if(tempData != NULL)
						{
							for(i = 0, dataLen = 0; i < publicUnicastCmdRun->packetCnt; i++)
							{
								for(j = 0; j < publicUnicastCmdRun->packetCnt; j++)
								{
									if(i+1 == publicUnicastCmdRun->packet[j].no)
									{
										memcpy(tempData + dataLen, publicUnicastCmdRun->packet[j].data, publicUnicastCmdRun->packet[j].dataLen);
										dataLen += publicUnicastCmdRun->packet[j].dataLen;
										break;
									}
								}
							}
						}

						(*dataOut).data = tempData;
						(*dataOut).len = dataLen;
					}
				}
				break;
			}

			if(--timeCnt == 0)
			{
				ret = -1;
				break;
			}
		}
		publicUnicastCmdRun->waitFlag = 0;

		for(j = 0; j < publicUnicastCmdRun->packetCnt; j++)
		{
			if(publicUnicastCmdRun->packet[j].data != NULL)
			{
				free(publicUnicastCmdRun->packet[j].data);
				publicUnicastCmdRun->packet[j].data = NULL;
			}
		}

		if(publicUnicastCmdRun->packet != NULL)
		{
			free(publicUnicastCmdRun->packet);
			publicUnicastCmdRun->packet = NULL;
		}

		FreePublicUnicastCmdRun(publicUnicastCmdRun);
	}
	//printf("%s33333333 ret=%d\n",__func__, ret);
	return ret;
}

//���յ�PublicUnicast����Ӧ��ָ��
void ReceivePublicUnicastCmdRsp(unsigned short cmd, PublicUnicastCmdRsp* rspData)
{
	PublicUnicastCmdRun* publicUnicastCmdRun;
	cJSON* element;
	int i;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	cJSON_ArrayForEach(element, myPublicUnicastTable.table)
	{
		publicUnicastCmdRun = (PublicUnicastCmdRun*)element->valueint;
		if(publicUnicastCmdRun->waitFlag && publicUnicastCmdRun->rand == rspData->rand && rspData->sourceIp == publicUnicastCmdRun->targetIp)
		{
			publicUnicastCmdRun->result = rspData->result;

			if(publicUnicastCmdRun->packet == NULL)
			{
				publicUnicastCmdRun->packet = malloc(rspData->packetTotal * sizeof(PublicUnicastPacket));
			}

			if(publicUnicastCmdRun->packet != NULL)
			{
				for(i = 0; i < publicUnicastCmdRun->packetCnt; i++)
				{
					if(rspData->packetNo == publicUnicastCmdRun->packet[i].no)
					{
						break;
					}
				}

				if(i == publicUnicastCmdRun->packetCnt)
				{
					publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].no = rspData->packetNo;
					publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].dataLen = rspData->dataLen;
					if(rspData->dataLen)
					{
						publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data = malloc(rspData->dataLen);
						if(publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data != NULL)
						{
							memcpy(publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data, rspData->data, rspData->dataLen);
						}
					}
					else
					{
						publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data = NULL;
					}

					publicUnicastCmdRun->packetCnt++;
				}

				if(publicUnicastCmdRun->packetCnt == rspData->packetTotal)
				{
					publicUnicastCmdRun->waitFlag = 0;
				}
			}

		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);
}

//���յ�PublicUnicast��������ָ��
int ReceivePublicUnicastCmdReq(char* netDeviceName, unsigned short cmd, PublicUnicastCmdReq* reqData, int len)
{
	PublicUnicastCmdRsp	sendData;
	PublicUnicastCmdProcess process = GetPublicUnicastCmdProcess(cmd);
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut = {.data = NULL, .len = 0};
	PublicMulRspUdpCmdReq* reqData2 = (PublicMulRspUdpCmdReq*)reqData;
	int i;

	if(len == sizeof(PublicUnicastCmdReq) - PUBLIC_UNICAST_DATA_LEN + reqData->dataLen)
	{
		if(!VerifyMFG_SN(reqData->MFG_SN, GetSysVerInfo_Sn()))
		{
			return -2;
		}

		dataIn.data = reqData->data;
		dataIn.len = reqData->dataLen;
	}
	else if(len == sizeof(PublicMulRspUdpCmdReq) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + reqData2->dataLen)
	{
		dataIn.data = reqData2->data;
		dataIn.len = reqData2->dataLen;
	}
	else
	{
		return -3;
	}

	if(process)
	{
		sendData.result = process(reqData->sourceIp, dataIn, &dataOut);
	}
	else
	{
		sendData.result = -4;
	}

	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(netDeviceName));
	if(dataOut.len == 0 || dataOut.data == NULL)
	{
		sendData.packetTotal = 1;
		sendData.packetNo=0;
		sendData.dataLen=0;
		api_udp_device_update_send_data_by_device(netDeviceName, reqData->sourceIp, htons(GetPublicUnicastRspCmd(cmd)), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN);
	}
	else
	{
		sendData.packetTotal = dataOut.len/PUBLIC_UNICAST_DATA_LEN + ((dataOut.len % PUBLIC_UNICAST_DATA_LEN) ? 1 : 0);
		for(i = 0; i < sendData.packetTotal; i++)
		{
			sendData.dataLen = (dataOut.len - PUBLIC_UNICAST_DATA_LEN*i > PUBLIC_UNICAST_DATA_LEN ? PUBLIC_UNICAST_DATA_LEN : dataOut.len - PUBLIC_UNICAST_DATA_LEN*i);
			sendData.packetNo = i+1;
			memcpy(sendData.data, dataOut.data + PUBLIC_UNICAST_DATA_LEN*i, sendData.dataLen);
			UNICAST_CMD_RSP_DELAY();
			api_udp_device_update_send_data_by_device(netDeviceName, reqData->sourceIp, htons(GetPublicUnicastRspCmd(cmd)), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN + sendData.dataLen);
		}
	}

	if(dataOut.data != NULL)
	{
		free(dataOut.data);
		dataOut.data = NULL;
	}

	return 0;
}

void FreeUnicastOutData(PublicUnicastCmdData dataOut)
{
	if(dataOut.data)
	{
		free(dataOut.data);
	}
}

static int GetPbByIpProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *view = cJSON_Parse(dataIn.data);;
	cJSON *retValue  = cJSON_CreateObject();
	cJSON *element;

	if(view)
	{
		cJSON_ArrayForEach(element, view)
		{
			if(element->string)
			{
				if(element->valuestring != NULL && !strcmp(element->valuestring, "IO"))
				{
					//cJSON_AddItemReferenceToObject(retValue, element->string, API_Para_Read_Public(element->string));
				}
				else
				{
					cJSON_AddItemReferenceToObject(retValue, element->string, API_PublicInfo_Read(element->string));
				}
			}
		}
	}

	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(retValue);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	cJSON_Delete(retValue);
	cJSON_Delete(view);

	return 0;
}

static int Ring2Process(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *ringData = cJSON_Parse(dataIn.data);
	cJSON * msg = cJSON_GetObjectItemCaseSensitive(ringData, "MSG");
	cJSON * time = cJSON_GetObjectItemCaseSensitive(ringData, "TIME");

	if(cJSON_IsString(msg) && cJSON_IsNumber(time))
	{
		API_VideoMenu_Ring2(msg->valuestring, time->valueint);
		#if	defined(PID_IX47)||defined(PID_IX482)||defined(PID_IXSE)		
		if(strstr(msg->valuestring, "Code Unlock")!= NULL)
		{
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_UnlcokEvent);
		}
		#endif
	}
	cJSON_Delete(ringData);

	return 0;
}

cJSON* API_GetPbIo(int ip, const char *sn, const cJSON *view)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;

	dataIn.data = cJSON_PrintUnformatted(view);
	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, sn, BusinessWaitUdpTime, GET_PB_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retJson;
}

cJSON* API_GetRemotePb(int ip, const char* pbName)
{
	cJSON* retJson = NULL;
	if(pbName)
	{
		cJSON *view = cJSON_CreateObject();
		cJSON_AddStringToObject(view, pbName, "PB");
		cJSON *getJson = API_GetPbIo(ip, NULL, view);
		retJson = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(getJson, pbName), 1);
		cJSON_Delete(view);
		cJSON_Delete(getJson);
	}
	return retJson;
}

//返回0--不在线，-1--不是字符串，1--正确。
int API_GetRemotePbString(int ip, const char* pbName, char* pbResult)
{
	int ret = 0;
	cJSON* retJson = NULL;
	cJSON* result = NULL;
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;

	cJSON *view = cJSON_CreateObject();
	cJSON_AddStringToObject(view, pbName, "PB");
	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_PB_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		result = cJSON_GetObjectItemCaseSensitive(retJson, pbName);
		if(cJSON_IsString(result))
		{
			ret = 1;
			if(pbResult)
			{
				strcpy(pbResult, result->valuestring);
			}
		}
		else
		{
			ret = -1;
		}
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}

//���أ�0 --ʧ�ܣ�1 --�ɹ�
int API_XD_EventJson(int ip, cJSON* event)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddItemToObject(sendJson, "Event", cJSON_Duplicate(event, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, EVENT_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}
int API_XD_EventJson2(int ip, cJSON* event)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddItemToObject(sendJson, "Event", cJSON_Duplicate(event, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, 0, EVENT_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}
/*
	value��ʽ��["IX_ADDR", "IP_ADDR", "IX_NAME"]

	����ֵ��ʽ��{
					"READ":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},

					"Result":"Succ"
	}
*/
cJSON* API_ReadRemoteIo(int ip, const cJSON *value)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	cJSON* element = NULL;

	cJSON* view = cJSON_CreateObject();
	cJSON* read = cJSON_AddObjectToObject(view, "READ");

	cJSON_ArrayForEach(element, value)
	{
		if(cJSON_IsString(element))
		{
			cJSON_AddFalseToObject(read, element->valuestring);
		}
	}

	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retJson;
}

/*
	value��ʽ��{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
	}

	����ֵ��ʽ��{
					"WRITE":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},
					
					"Result":"Succ"
	}
*/

cJSON* API_WriteRemoteIo(int ip, const cJSON *value)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	cJSON* element = NULL;

	cJSON* view = cJSON_CreateObject();
	cJSON_AddItemReferenceToObject(view, "WRITE", value);

	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retJson;
}
//���أ�0 --ʧ�ܣ�1 --�ɹ�
int API_Ring2(int ip, char* msg, int time)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddStringToObject(sendJson, "MSG", msg);
	cJSON_AddNumberToObject(sendJson, "TIME", time);
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, IX_Ring2_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}

void RemoteUpdate_Report(char* msg);

int EventCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *strIp, *strSn, *strDeviceNbr;
	char *eventStr = NULL;
	cJSON *event;
	cJSON *reqJson;
	int ret = 0;

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		return -1;
	}

	strIp = GetJsonItemString(reqJson, DM_KEY_IP_Addr);
	strSn = GetJsonItemString(reqJson, DM_KEY_MFG_SN);
	strDeviceNbr = GetJsonItemString(reqJson, DM_KEY_DeviceNbr);
	event = cJSON_GetObjectItem(reqJson, IX2V_Event);
	API_Event_Json(event);
	cJSON_Delete(reqJson);
	
	return ret;
}

int API_IXS_ProxyUpdateDevTbReq(int ip)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;

	dataIn.len = 0;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, UPDATE_DEV_TABLE_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	return ret;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

