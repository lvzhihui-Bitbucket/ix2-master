/**
  ******************************************************************************
  * @file    obj_OS_SipCMD.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <ctype.h>
#include "obj_BackupAndRestoreCmd.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"

static BackupAndRestoreCmdRun backupAndRestoreCmdRun;


int API_BackupAndRestoreCmd(int ip, int operation, int packet, int range, int timeOut)
{
	BackupAndRestoreCmdReq sendData;
	int i, timeCnt, ret;
	
	ret = -1;

	sendData.rand = MyRand();
	sendData.sourceIp = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip));
	sendData.operation = operation;
	sendData.range = range;
	sendData.backupPacket = packet;


	backupAndRestoreCmdRun.waitFlag = 1;
	backupAndRestoreCmdRun.result = -1;
	backupAndRestoreCmdRun.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(CMD_BACKUP_AND_RESTORE_REQ), (char*)&sendData, sizeof(BackupAndRestoreCmdReq) );
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(backupAndRestoreCmdRun.waitFlag == 0)
		{
			ret = backupAndRestoreCmdRun.result;
			break;
		}

		if(--timeCnt == 0)
		{
			ret = -1;
			break;
		}
	}

	return ret;
}

//接收到BackupAndRestore命令处理应答指令
void ReceiveBackupAndRestoreCmdRsp(BackupAndRestoreCmdRsp* rspData)
{
	if(backupAndRestoreCmdRun.waitFlag == 1)
	{
		if(rspData->rand == backupAndRestoreCmdRun.rand)
		{
			backupAndRestoreCmdRun.sourceIp = rspData->sourceIp;
			backupAndRestoreCmdRun.result = rspData->result;
			backupAndRestoreCmdRun.waitFlag = 0;
		}
	}
}


//接收到BackupAndRestore命令处理请求指令
void ReceiveBackupAndRestoreCmdReq(int target_ip, BackupAndRestoreCmdReq* pReadReq)
{
	BackupAndRestoreCmdRsp	sendData;
	
	sendData.rand = pReadReq->rand;
	sendData.sourceIp = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip));
	sendData.operation = pReadReq->operation;
	sendData.range = pReadReq->range;
	sendData.backupPacket = pReadReq->backupPacket;


	if(pReadReq->operation == OPERATION_RESTORE)
	{
		sendData.result = API_RestoreFromBackup(pReadReq->backupPacket, pReadReq->range, NULL);
	}
	else
	{
		sendData.result = API_Backup(pReadReq->backupPacket, pReadReq->range);
	}

	api_udp_device_update_send_data(target_ip, htons(CMD_BACKUP_AND_RESTORE_RSP), (char*)&sendData, sizeof(sendData) );
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

