/**
  ******************************************************************************
  * @file    obj_SettingFromR8001.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_SettingFromR8001_H
#define _obj_SettingFromR8001_H

// Define Object Property-------------------------------------------------------
#define SettingFromR8001DataLen		1

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
}SettingFromR8001Run;

// GetIpBy MFG_SN 指令包结构
typedef struct
{
	int		rand;							//随机数
	char	data[SettingFromR8001DataLen];
}SettingFromR8001Req;

typedef struct
{
	int		rand;							//随机数
	char	data[SettingFromR8001DataLen];
}SettingFromR8001Rsp;


#pragma pack()



// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


