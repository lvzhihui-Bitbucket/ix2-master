/**
  ******************************************************************************
  * @file    obj_ProgInfoByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_ProgInfoByIp.h"
#include "task_IoServer.h"

static ProgInfoRun progInfo[CLIENT_MAX_NUM];

//发送Prog信息指令
int API_ProgInfoByIp(int ip, int time, DeviceInfo* info)
{
	ProgInfoByIpReq sendData;
	int i, cnt;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	memcpy(&sendData.data, info, sizeof(DeviceInfo));
	
	for(i = 0; i < CLIENT_MAX_NUM && progInfo[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}
	
	progInfo[i].waitFlag = 1;

	progInfo[i].rand = sendData.rand;

	api_udp_device_update_send_data(ip, htons(PROG_INFO_BY_IP_REQ), (char*)&sendData, sizeof(sendData) );

	cnt = time*1000/UDP_WAIT_RSP_TIME;
	while(progInfo[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--cnt == 0)
		{
			progInfo[i].waitFlag = 0;
			return -3;
		}
	}
	memcpy(info, &progInfo[i].data, sizeof(DeviceInfo));

	progInfo[i].waitFlag = 0;

	return 0;
}

//接收到Prog信息应答指令
void ReceiveProgInfoRsp(ProgInfoByIpRsp* rspData)
{
	int i, ip;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(progInfo[i].waitFlag)
		{
			if(rspData->rand == progInfo[i].rand)
			{
				memcpy(&progInfo[i].data, &rspData->data, sizeof(DeviceInfo));
				
				ip = rspData->sourceIp;
				snprintf(progInfo[i].data.IP_ADDR, 16, "%03d.%03d.%03d.%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
				
				progInfo[i].waitFlag = 0;
				break;
			}
		}
	}
}


//接收到Prog info请求指令
void ReceiveProgInfoReq(ProgInfoByIpReq* pReadReq)
{
	ProgInfoByIpRsp rspData = {0};
	char temp[20];
	char dhcpState;
	
	if(!strcmp(pReadReq->data.MFG_SN, GetSysVerInfo_Sn()))
	{

		SetSysVerInfo_BdRmMs(pReadReq->data.BD_RM_MS);
		SetSysVerInfo_GlobalNum(pReadReq->data.Global);
		SetSysVerInfo_LocalNum(pReadReq->data.Local);
		SetSysVerInfo_name((strcmp(pReadReq->data.name2_utf8, "-")&&pReadReq->data.name2_utf8[0]) ? pReadReq->data.name2_utf8 : pReadReq->data.name1);
/*
		if(!strcmp(pReadReq->data.IP_STATIC, "STATIC"))
		{
			dhcpState = 0;
			sprintf(temp, "%d", dhcpState);
			API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
			API_DHCP_SaveEnable(0);
			SetSysVerInfo_IP(pReadReq->data.IP_ADDR);
			SetSysVerInfo_mask(pReadReq->data.IP_MASK);
			SetSysVerInfo_gateway(pReadReq->data.IP_GATEWAY);
			SetNetWork(NULL, GetSysVerInfo_IP(), GetSysVerInfo_mask(), GetSysVerInfo_gateway());
			ResetNetWork();
		}
		else if(!strcmp(pReadReq->data.IP_STATIC, "DHCP"))
		{
			API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
			if(atoi(temp) == 0)
			{
				dhcpState = 1;
				sprintf(temp, "%d", dhcpState);
				API_Event_IoServer_InnerWrite_All(DHCP_ENABLE, temp);
				API_DHCP_SaveEnable(1);
				sleep(3);
			}
		}
*/		
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(pReadReq->sourceIp)));
		
		strcpy(rspData.data.MFG_SN, GetSysVerInfo_Sn());
		strcpy(rspData.data.BD_RM_MS, GetSysVerInfo_BdRmMs());
		strcpy(rspData.data.Local, GetSysVerInfo_LocalNum());
		strcpy(rspData.data.Global, GetSysVerInfo_GlobalNum());
		//strcpy(rspData.data.name1, GetSysVerInfo_name());
		snprintf(rspData.data.name1, 21, "%s", GetSysVerInfo_name());
		
		api_udp_device_update_send_data(pReadReq->sourceIp, htons(PROG_INFO_BY_IP_RSP), (char*)&rspData, sizeof(rspData) );
		
		CmdProgCallNbrReq();
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

