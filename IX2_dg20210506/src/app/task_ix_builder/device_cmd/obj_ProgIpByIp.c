/**
  ******************************************************************************
  * @file    obj_ProgIpByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_ProgIpByIp.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_IX_Report.h"


IxProgIpAddrRun ixProgIpAddrRun;

int IxProgIpAddrProcess(int ip, PROG_IP_ADDR_REQ_DATA_T data, int timeOut)
{
	IxProgIpAddrReq sendData;
	int timeCnt, ret;
	char* dataJson;

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.data = data;

	if(sendData.sourceIp == ip)
	{
		return -1;
	}


	ixProgIpAddrRun.waitFlag = 1;
	ixProgIpAddrRun.result = -1;
	ixProgIpAddrRun.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(IX_PROG_IP_ADDR_REQ), (char*)&sendData, sizeof(sendData));
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(ixProgIpAddrRun.waitFlag == 0)
		{
			ret = ixProgIpAddrRun.result;
			break;
		}

		if(--timeCnt == 0)
		{
			ret = -1;
			break;
		}
	}

	ixProgIpAddrRun.waitFlag = 0;
	return ret;
}

//接收到IxProgIpAddr命令应答指令
void ReceiveIxProgIpAddrCmdRsp(IxProgIpAddrRsp* rspData)
{
	if(ixProgIpAddrRun.waitFlag == 1)
	{
		if(rspData->rand == ixProgIpAddrRun.rand)
		{
			ixProgIpAddrRun.sourceIp = rspData->sourceIp;
			ixProgIpAddrRun.result = rspData->result;
			ixProgIpAddrRun.waitFlag = 0;
		}
	}
}

static int reportIpAddrIp = 0;

//接收到IxProgIpAddr命令请求指令
int ReceiveIxProgIpAddrCmdReq(IxProgIpAddrReq* reqData)
{
	
	IxProgIpAddrRsp	sendData;
	
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));
	if(!strcmp(reqData->data.MFG_SN, GetSysVerInfo_Sn()))
	{
		sendData.result = 0;
		
		if(!strcmp(reqData->data.IP_ASSIGNED, "DHCP&AUTO"))
		{
			sendData.result = 0;
			api_udp_device_update_send_data(reqData->sourceIp, htons(IX_PROG_IP_ADDR_RSP), (char*)&sendData, sizeof(sendData) );
			reportIpAddrIp = reqData->sourceIp;
			ProgIpAddr(1, NULL, NULL, NULL);
		}
		else if(!strcmp(reqData->data.IP_ASSIGNED, "STATIC"))
		{
			sendData.result = 0;
			api_udp_device_update_send_data(reqData->sourceIp, htons(IX_PROG_IP_ADDR_RSP), (char*)&sendData, sizeof(sendData) );
			reportIpAddrIp = reqData->sourceIp;
			ProgIpAddr(0, reqData->data.IP_ADDR, reqData->data.IP_MASK, reqData->data.IP_GATEWAY);
			SetIpActionType(3);
			ReportIpAddr();
		}
		else
		{
			sendData.result = -1;
			api_udp_device_update_send_data(reqData->sourceIp, htons(IX_PROG_IP_ADDR_RSP), (char*)&sendData, sizeof(sendData) );
		}

	}
	else
	{
		sendData.result = -1;
		api_udp_device_update_send_data(reqData->sourceIp, htons(IX_PROG_IP_ADDR_RSP), (char*)&sendData, sizeof(sendData) );
	}
	
	return 0;
}

void ReportIpAddr(void)
{
	if(reportIpAddrIp != 0)
	{
		IxReport(reportIpAddrIp, REPORT_IP_ADDR_REQ);
		reportIpAddrIp = 0;
	}
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

