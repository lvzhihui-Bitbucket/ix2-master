/**
  ******************************************************************************
  * @file    obj_IX_Req_Ring.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_IX_Req_Ring.h"

#include "vtk_udp_stack_device_update.h"


IxReqRingRun ixReqRingRun;

int IxReqRingProcess(int ip, int timeOut, const char* message, IxReqRingRsp* pRsp)
{
	
	IxReqRingReq sendData = {0};
	int i, timeCnt, ret, len;
	
	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	len = sizeof(sendData) - RING_DATA_LEN;

	if(message != NULL)
	{
		strncpy(sendData.data, message, RING_DATA_LEN);
		len += strlen(message);
	}
	
	memset(pRsp, 0, sizeof(IxReqRingRsp));
	
	
	if(sendData.sourceIp == ip)
	{
		strcpy(pRsp->DeviceNbr, GetSysVerInfo_BdRmMs());
		strcpy(pRsp->IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
		strcpy(pRsp->MFG_SN, GetSysVerInfo_Sn());
		return CmdRingReq(message);
	}

	ixReqRingRun.waitFlag = 1;
	ixReqRingRun.rsp.result = -1;
	ixReqRingRun.rsp.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(IX_REQ_Ring_REQ), (char*)&sendData, len);
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(ixReqRingRun.waitFlag == 0)
		{
			ret = ixReqRingRun.rsp.result;
			*pRsp = ixReqRingRun.rsp; 
			break;
		}

		if(--timeCnt == 0)
		{
			ixReqRingRun.waitFlag = 0;
			ret = -1;
			break;
		}
	}

	return ret;
}

//接收到IxReqRing命令应答指令
void ReceiveIxReqRingCmdRsp(IxReqRingRsp* rspData)
{
	if(ixReqRingRun.waitFlag == 1)
	{
		if(rspData->rand == ixReqRingRun.rsp.rand)
		{
			ixReqRingRun.rsp = *rspData;
			ixReqRingRun.waitFlag = 0;
		}
	}
}

//接收到IxReqRing命令请求指令
int ReceiveIxReqRingCmdReq(IxReqRingReq* reqData)
{
	
	IxReqRingRsp	sendData;
	
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));
	sendData.result = 0;
	strcpy(sendData.DeviceNbr, GetSysVerInfo_BdRmMs());
	strcpy(sendData.IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));
	strcpy(sendData.MFG_SN, GetSysVerInfo_Sn());

	api_udp_device_update_send_data(reqData->sourceIp, htons(IX_REQ_Ring_RSP), (char*)&sendData, sizeof(sendData) );

	CmdRingReq(reqData->data);
	
	return 0;
}

int API_IxReqRing(int ip, const char* message)
{
	IxReqRingRsp rsp;
	
	return IxReqRingProcess(ip, 1, message, &rsp);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

