/**
  ******************************************************************************
  * @file    obj_GetAboutByIp.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_GetAboutByIp_H
#define _obj_GetAboutByIp_H

// Define Object Property-------------------------------------------------------
#define DATA_LEN 1000

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	data[DATA_LEN];		//请求数据
} GetAboutByIpReq;

typedef struct
{
	int		rand;				//随机数
	char	data[DATA_LEN];		//应答数据
} GetAboutByIpRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	char			data[DATA_LEN];		//源ip
} GetAboutRun;

#pragma pack()

#define ABOUT_ID_IP_Address				101
#define ABOUT_ID_BD_RM_MS_Nbr			102
#define ABOUT_ID_Name					103
#define ABOUT_ID_Global_Nbr				104
#define ABOUT_ID_Local_Nbr				105

#define ABOUT_ID_SW_Version				106
#define ABOUT_ID_HW_Version				107
#define ABOUT_ID_UpgradeTime			108
#define ABOUT_ID_UpgradeCode			109
#define ABOUT_ID_UpTime					110

#define ABOUT_ID_SerialNo				111
#define ABOUT_ID_DeviceType				112
#define ABOUT_ID_DeviceModel			113
#define ABOUT_ID_AreaCode				114
#define ABOUT_ID_TransferState			115

#define ABOUT_ID_HW_Address				116
#define ABOUT_ID_SubnetMask				117
#define ABOUT_ID_DefaultRoute			118


// Define Object Function - Public----------------------------------------------

//发送获取信息指令
int API_GetAboutByIp(int ip, int time, char* data);

//接收到Get About请求指令
void ReceiveGetAboutReq(int target_ip, GetAboutByIpReq* pReadReq);

//接收到获取信息应答指令
void ReceiveGetAboutRsp(GetAboutByIpRsp* rspData);


// Define Object Function - Private---------------------------------------------



#endif


