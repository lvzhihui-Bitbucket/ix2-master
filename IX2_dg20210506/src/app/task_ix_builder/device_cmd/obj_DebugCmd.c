/**
  ******************************************************************************
  * @file    obj_DebugCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_DebugCmd.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "task_IoServer.h"
#include "cJSON.h"


static DebugStateRun_T debugCmdRun[CLIENT_MAX_NUM];

static int ParseDebugReqJsonData(const char* json, TEST_MODE_T* pData)
{
    int status = 0;

    const cJSON *mic = NULL;
    const cJSON *spk = NULL;
    const cJSON *music = NULL;
    const cJSON *autoLock = NULL;
    const cJSON *autoTalk = NULL;
    const cJSON *testMode = NULL;

	memset(pData, 0, sizeof(TEST_MODE_T));


    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	mic = cJSON_GetObjectItemCaseSensitive( root, "MIC");
	if(mic != NULL)
	{
		if (cJSON_IsString(mic) && (mic->valuestring != NULL))
		{
			pData->mic = atoi(mic->valuestring);
		}
	}
	
	spk = cJSON_GetObjectItemCaseSensitive( root, "SPK");
	if(spk != NULL)
	{
		if (cJSON_IsString(spk) && (spk->valuestring != NULL))
		{
			pData->spk = atoi(spk->valuestring);
		}
	}

	music = cJSON_GetObjectItemCaseSensitive( root, "MUSIC");
	if(music != NULL)
	{
		if (cJSON_IsString(music) && (music->valuestring != NULL))
		{
			pData->music = atoi(music->valuestring);
		}
	}
	
	autoLock = cJSON_GetObjectItemCaseSensitive( root, "AUTO_LOCK");
	if(autoLock != NULL)
	{
		if (cJSON_IsString(autoLock) && (autoLock->valuestring != NULL))
		{
			pData->autoLock = atoi(autoLock->valuestring);
		}
	}
	
	autoTalk = cJSON_GetObjectItemCaseSensitive( root, "AUTO_TALK");
	if(autoTalk != NULL)
	{
		if (cJSON_IsString(autoTalk) && (autoTalk->valuestring != NULL))
		{
			pData->autoTalk = atoi(autoTalk->valuestring);
		}
	}

	
	
	testMode = cJSON_GetObjectItemCaseSensitive( root, "TEST_MODE");
	if(testMode != NULL)
	{
		if (cJSON_IsString(testMode) && (testMode->valuestring != NULL))
		{
			pData->testMode = atoi(testMode->valuestring);
		}
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

static char* CreateDebugRspJsonData(void)
{
	char temp[10] = {0};
	int i;
	
    cJSON *root = NULL;
	char *string = NULL;
	
	typedef struct
	{
		const char* ioid;
		const char* name;
	}JSON_TABLE_T;


	const JSON_TABLE_T jsonTable[] = 
	{
		{ManagePassword, 		"MIC"},	 
		{ManagePassword,		"SPK"},  
		{ManagePassword,		"MUSIC"},  
		{ManagePassword,		"AUTO_LOCK"},  
		{ManagePassword,		"AUTO_TALK"},  
		{ManagePassword,		"TEST_MODE"},  
	};
	
	const int jsonTableNum = sizeof(jsonTable)/sizeof(jsonTable[0]);


    root = cJSON_CreateObject();

	for(i = 0, temp[0] = 0; i < jsonTableNum; i++, temp[0] = 0)
	{
		temp[0] = 0;
		API_Event_IoServer_InnerRead_All(jsonTable[i].ioid, (uint8*)temp);
		cJSON_AddStringToObject(root, jsonTable[i].name, temp);
	}

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


int API_AskDebugState(int time_ms)
{
	DebugStateAskReq sendData;
	int i, timeCnt;
	char tempchar[5];
	API_Event_IoServer_InnerRead_All(InstallerRemoteDis, (uint8*)tempchar);
	if(atoi(tempchar) == 1)
	{
		return 0;
	}
	
	API_Event_IoServer_InnerRead_All(InstallerPwdAlways, (uint8*)tempchar);
	if(atoi(tempchar) == 1)
	{
		return 0;
	}
	
	for(i = 0; i < CLIENT_MAX_NUM && debugCmdRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return 0;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)sendData.psw);

	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_ASK_DEBUG_STATE_REQ), (char*)&sendData, sizeof(sendData) );
	
	debugCmdRun[i].waitFlag = 1;
	debugCmdRun[i].rand = sendData.rand;
		
	timeCnt = time_ms/5;
	while(debugCmdRun[i].waitFlag)
	{
		usleep(5*1000);
		if(--timeCnt <= 0)
		{
			debugCmdRun[i].waitFlag = 0;
			return 0;
		}
	}
		
	debugCmdRun[i].waitFlag = 0;
	
	return debugCmdRun[i].debugState;
}


int API_AskDebugAndTestState(int time_ms, TEST_MODE_T* pTestMode, int* pDebugState)
{
	DebugStateAskReq sendData;
	int i, timeCnt;
	char tempchar[5];
	API_Event_IoServer_InnerRead_All(InstallerRemoteDis, (uint8*)tempchar);
	if(atoi(tempchar) == 1)
	{
		return 0;
	}
	
	API_Event_IoServer_InnerRead_All(InstallerPwdAlways, (uint8*)tempchar);
	if(atoi(tempchar) == 1)
	{
		return 0;
	}
	
	for(i = 0; i < CLIENT_MAX_NUM && debugCmdRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return 0;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
 	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)sendData.psw);

	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_ASK_DEBUG_STATE_REQ), (char*)&sendData, sizeof(sendData) );
	
	debugCmdRun[i].waitFlag = 1;
	debugCmdRun[i].rand = sendData.rand;
		
	timeCnt = time_ms/5;
	while(debugCmdRun[i].waitFlag)
	{
		usleep(5*1000);
		if(--timeCnt <= 0)
		{
			debugCmdRun[i].waitFlag = 0;
			return 0;
		}
	}
		
	debugCmdRun[i].waitFlag = 0;
	*pDebugState = debugCmdRun[i].debugState;
	*pTestMode = debugCmdRun[i].test;
	
	printf("testMode=%d, DebugState=%d.\n", debugCmdRun[i].test.testMode, debugCmdRun[i].debugState);
	
	return 1;
}

//接收到搜索应答指令
void ReceiveAskDebugStateRsp(DebugStateAskRsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(debugCmdRun[i].waitFlag)
		{
			if(rspData->rand == debugCmdRun[i].rand)
			{
				debugCmdRun[i].debugState = rspData->state;
				ParseDebugReqJsonData(rspData->jsonChar, &debugCmdRun[i].test);
				
				debugCmdRun[i].waitFlag = 0;
			}
		}
	}
}

void ReceiveAskDebugStateReq(int target_ip, DebugStateAskReq* pReadReq)
{
	char pwd[8+1];
	int  testMode;
	char* json = NULL;
	
	//网络没初始化好，不应答远程指令
	if(!GetNetworkIsStarted())
	{
		return;
	}
	
	API_Event_IoServer_InnerRead_All(TEST_MODE, (uint8*)pwd);
	testMode = atoi(pwd);

	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)pwd);

	if((JudgeIsInstallerMode() || testMode) && !strcmp(pwd, pReadReq->psw))
	{
		DebugStateAskRsp rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		rspData.state = JudgeIsInstallerMode();
		
		json = CreateDebugRspJsonData();

		if(json != NULL)
		{
			strncpy(rspData.jsonChar, json, 500);
			free(json);
		}
		
		api_udp_device_update_send_data(target_ip, htons(CMD_ASK_DEBUG_STATE_RSP), (char*)&rspData, sizeof(rspData) );
	}
}

int API_ExitDebugState(void)
{
	DebugStateExitReq sendData;
	
	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();

	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_EXIT_DEBUG_STATE_REQ), (char*)&sendData, sizeof(sendData) );
 	return 1;
}

int ReceiveExitDebugStateReq(void)
{
	ExitInstallerMode();
	return 1;
}

int API_ChangeInstallPwd(int time)
{
	DebugStateAskReq sendData;
	int i, timeCnt;
	char temp[5];

	for(i = 0; i < CLIENT_MAX_NUM && debugCmdRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return 0;
	}
	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
 	API_Event_IoServer_InnerRead_All(ManagePassword, (uint8*)sendData.psw);

	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_CHANGE_INSTALL_PWD_REQ), (char*)&sendData, sizeof(sendData) );
	
 	debugCmdRun[i].waitFlag = 1;
	debugCmdRun[i].rand = sendData.rand;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(debugCmdRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			debugCmdRun[i].waitFlag = 0;
			return 0;
		}
	}
	
	debugCmdRun[i].waitFlag = 0;
	
	return debugCmdRun[i].debugState;
}
void ChangeInstallPwdRsp(DebugStateAskRsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(debugCmdRun[i].waitFlag)
		{
			if(rspData->rand == debugCmdRun[i].rand)
			{
				debugCmdRun[i].debugState = rspData->state;
				debugCmdRun[i].waitFlag = 0;
			}
		}
	}
}

void ChangeInstallPwdReq(int target_ip, DebugStateAskReq* pReadReq)
{
	//网络没初始化好，不应答远程指令
	if(!GetNetworkIsStarted())
	{
		return;
	}
	if(pReadReq->sourceIp == inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip))))
	{
		return;
	}
	API_Event_IoServer_InnerWrite_All(ManagePassword, (uint8*)pReadReq->psw);

	DebugStateAskRsp rspData;
	rspData.rand = pReadReq->rand;
	rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
	rspData.state = 1;
	api_udp_device_update_send_data(target_ip, htons(CMD_CHANGE_INSTALL_PWD_RSP), (char*)&rspData, sizeof(rspData) );
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

