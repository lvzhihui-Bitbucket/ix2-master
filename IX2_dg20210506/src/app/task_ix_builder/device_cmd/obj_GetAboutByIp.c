/**
  ******************************************************************************
  * @file    obj_GetAboutByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetAboutByIp.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"


static GetAboutRun getAbout[CLIENT_MAX_NUM];

//发送获取About信息指令
int API_GetAboutByIp(int ip, int time, char* data)
{
	GetAboutByIpReq sendData;
	char temp[16];
	int i, cnt;
	
	if(data == NULL)
	{
		return -2;
	}
	
	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcpy(sendData.data, data);
	
	for(i = 0; i < CLIENT_MAX_NUM && getAbout[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}
	
	memset(&getAbout, 0, sizeof(getAbout));
	
	getAbout[i].waitFlag = 1;
	getAbout[i].rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(GET_ABOUT_BY_IP_REQ), (char*)&sendData, 9 + strlen(sendData.data));

	cnt = time*1000/UDP_WAIT_RSP_TIME;
	while(getAbout[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--cnt == 0)
		{
			getAbout[i].waitFlag = 0;
			return -3;
		}
	}

	strcpy(data, getAbout[i].data);
	getAbout[i].waitFlag = 0;
	return 0;
}

//接收到获取信息应答指令
void ReceiveGetAboutRsp(GetAboutByIpRsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(getAbout[i].waitFlag)
		{
			if(rspData->rand == getAbout[i].rand)
			{
				strcpy(getAbout[i].data, &rspData->data);
				getAbout[i].waitFlag = 0;
				break;
			}
		}
	}
}

void GetMyAboutString(int ip, char* aboutData)
{
	char tempData[100];
	SYS_VER_INFO_T sysInfo;
	sysInfo = GetSysVerInfo();
	
	if(aboutData == NULL)
	{
		return;
	}

	aboutData[0] = 0;
		
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_IP_Address, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_BD_RM_MS_Nbr, GetSysVerInfo_BdRmMs());
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Name, GetSysVerInfo_name());
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Global_Nbr, GetSysVerInfo_GlobalNum());
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Local_Nbr, GetSysVerInfo_LocalNum());
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SW_Version, sysInfo.swVer);
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_HW_Version, "26710");
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpgradeTime, "2018-12-29");
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpgradeCode, "1243");
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpTime, TimeToString(GetSystemBootTime()));
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SerialNo, sysInfo.sn);
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DeviceType, sysInfo.type);
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DeviceModel, sysInfo.deviceModel);
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_AreaCode, "002");
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%d>", ABOUT_ID_TransferState, Get_SipAccount_State());
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_HW_Address, GetSysVerInfo_mac_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SubnetMask, GetSysVerInfo_mask_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcat(aboutData, tempData);
	
	snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DefaultRoute, GetSysVerInfo_gateway_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcat(aboutData, tempData);
}

//接收到Get About请求指令
void ReceiveGetAboutReq(int target_ip, GetAboutByIpReq* pReadReq)
{
	int dataIndex = 0;
	int pos1, pos2;
	int flag = 0;
	char tempId[20];
	char tempData[100];
	
	GetAboutByIpRsp rspData = {0};
	
	SYS_VER_INFO_T sysInfo;
	sysInfo = GetSysVerInfo();

	rspData.rand = pReadReq->rand;

	if(pReadReq->data[0] == 0)
	{
		GetMyAboutString(target_ip, rspData.data);
	}
	else
	{
		for(dataIndex = 0; pReadReq->data[dataIndex] != 0; dataIndex++)
		{
			if(pReadReq->data[dataIndex] == '<')
			{
				flag = 1;
				pos1 = dataIndex;
			}
			else if(pReadReq->data[dataIndex] == '>')
			{
				if(flag == 1)
				{
					pos2 = dataIndex;
					memset(tempId, 0, 20);
					memcpy(tempId, pReadReq->data+pos1+4, pos2 - pos1 - 4);
		
					switch(atoi(tempId))
					{
						case ABOUT_ID_IP_Address:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_IP_Address, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_BD_RM_MS_Nbr:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_BD_RM_MS_Nbr, GetSysVerInfo_BdRmMs());
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_Name:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Name, GetSysVerInfo_name());
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_Global_Nbr:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Global_Nbr, GetSysVerInfo_GlobalNum());
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_Local_Nbr:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_Local_Nbr, GetSysVerInfo_LocalNum());
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_SW_Version:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SW_Version, sysInfo.swVer);
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_HW_Version:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_HW_Version, "26710");
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_UpgradeTime:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpgradeTime, "2018-12-29");
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_UpgradeCode:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpgradeCode, "1243");
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_UpTime:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_UpTime, TimeToString(GetSystemBootTime()));
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_SerialNo:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SerialNo, sysInfo.sn);
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_DeviceType:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DeviceType, sysInfo.type);
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_DeviceModel:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DeviceModel, sysInfo.deviceModel);
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_AreaCode:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_AreaCode, "002");
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_TransferState:
							snprintf(tempData, 100, "<ID=%03d,Value=%d>", ABOUT_ID_TransferState, Get_SipAccount_State());
							strcat(rspData.data, tempData);
							break;
		
						case ABOUT_ID_HW_Address:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_HW_Address, GetSysVerInfo_mac_by_device(GetNetDeviceNameByTargetIp(target_ip)));
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_SubnetMask:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_SubnetMask, GetSysVerInfo_mask_by_device(GetNetDeviceNameByTargetIp(target_ip)));
							strcat(rspData.data, tempData);
							break;
						
						case ABOUT_ID_DefaultRoute:
							snprintf(tempData, 100, "<ID=%03d,Value=%s>", ABOUT_ID_DefaultRoute, GetSysVerInfo_gateway_by_device(GetNetDeviceNameByTargetIp(target_ip)));
							strcat(rspData.data, tempData);
							break;
		
					}
				}
				flag = 0;
			}
		
		}

	}

	api_udp_device_update_send_data(target_ip, htons(GET_ABOUT_BY_IP_RSP), (char*)&rspData, strlen(rspData.data) + 5);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

