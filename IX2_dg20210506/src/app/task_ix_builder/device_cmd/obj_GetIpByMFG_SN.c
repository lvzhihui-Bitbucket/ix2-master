/**
  ******************************************************************************
  * @file    obj_GetIpByMFG_SN.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetIpByMFG_SN.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_IpCacheTable.h"
#include "task_IoServer.h"
#include "obj_PublicInformation.h"


static GetIpRun getIpByMFG_SN[CLIENT_MAX_NUM];
static int networkIsStarted = 0;
static int appStartedFlag = 0;

void SetNetworkIsStarted(char* netDeviceName, int state)
{
	if(state && !appStartedFlag)
	{
		appStartedFlag = 1;
		API_PublicInfo_Write_String(PB_APP_UP_TIME, TimeToString(GetSystemBootTime()));
	}

	networkIsStarted = state;
	if(networkIsStarted)
	{
		API_Check_CERT();
		TLogInit();
		ReportIpAddr();
		RebootReport(netDeviceName);
		if(!IfHasSn())
		{
			char temp[6];
			API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
			if(Api_tcp_GetSn((atoi(temp) ? NET_WLAN0 : NET_ETH0), "47.106.104.38", 8862, temp) == 0)
			{
				//#if !defined(PID_IXSE)
				SetTiny1616Sn(temp);
				//#endif
			}
		}
	}
}

int GetNetworkIsStarted(void)
{
	return networkIsStarted;
}


//����By number����ָ��
// paras:
// MGF_SN: MFG_SN
// time: �ȴ�ʱ��
// ip : ���ҽ��
// return:
//  -1:ϵͳæ, -2:���ݽ�����������, -3:û�в��ҵ�, 0:�������
int API_GetIpByMFG_SN(const char* MFG_SN,  int time, int* ip, int* upTime)
{
	GetIpByMFG_SNReq sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && getIpByMFG_SN[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	if(strlen(MFG_SN) != 12)
	{
		return -2;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
 	strcpy(sendData.MFG_SN, MFG_SN);

	getIpByMFG_SN[i].waitFlag = 1;
	getIpByMFG_SN[i].rand = sendData.rand;

	api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_IP_BY_MFG_SN_REQ), (char*)&sendData, sizeof(sendData) );
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(getIpByMFG_SN[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			getIpByMFG_SN[i].waitFlag = 0;
			return -4;
		}
	}
	
		
	if(!strcmp(getIpByMFG_SN[i].data.MFG_SN, MFG_SN))
	{
		if(ip != NULL)
		{
			*ip = getIpByMFG_SN[i].data.Ip;
		}
		
		if(upTime != NULL)
		{
			*upTime = getIpByMFG_SN[i].data.upTime;
		}
		ret = 0;
	}
	else
	{
		ret = -3;
	}
	
	getIpByMFG_SN[i].waitFlag = 0;
	
	return ret;
}

//���յ�����Ӧ��ָ��
void ReceiveGetIpByMFG_SNRsp(int target_ip, GetIpByMFG_SNRsp* rspData)
{
	int i;
	
	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(getIpByMFG_SN[i].waitFlag)
		{
			if(rspData->rand == getIpByMFG_SN[i].rand)
			{
				getIpByMFG_SN[i].data.Ip = rspData->sourceIp;
				getIpByMFG_SN[i].data.upTime= rspData->upTime;
				strcpy(getIpByMFG_SN[i].data.MFG_SN, rspData->MFG_SN);
				getIpByMFG_SN[i].waitFlag = 0;
			}
		}
	}
}


//���յ�By MFG_SN����ָ��
void ReceiveGetIpByMFG_SNReq(int target_ip, GetIpByMFG_SNReq* pReadReq)
{
	//����û��ʼ���ã���Ӧ��Զ��ָ��
	if(!GetNetworkIsStarted())
	{
		return;
	}
	
	if(!strcmp(GetSysVerInfo_Sn(), pReadReq->MFG_SN))
	{
		GetIpByMFG_SNRsp rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
		strcpy(rspData.MFG_SN, GetSysVerInfo_Sn());
		rspData.upTime = GetSystemBootTime();
		
		api_udp_device_update_send_data(target_ip, htons(GET_IP_BY_MFG_SN_RSP), (char*)&rspData, sizeof(rspData) );
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

