/**
  ******************************************************************************
  * @file    obj_PublicUdpCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "task_VideoMenu.h"
#include "obj_PublicUdpCmd.h"
#include "vtk_udp_stack_device_update.h"

static PublicUdpCmdRun resDownloadRun;
static PublicUdpCmdRun restoreRun;
static PublicUdpCmdRun backupRun;

static void* GetPublicUdpCmdRun(unsigned short cmd)
{
	PublicUdpCmdRun *pReturn = NULL;
	switch(cmd)
	{
		case IX_DEVICE_RES_DOWNLOAD_REQ:
		case IX_DEVICE_RES_DOWNLOAD_RSP:
			pReturn = &resDownloadRun;
			break;
		
		case Restore_REQ:
		case Restore_RSP:
			pReturn = &restoreRun;
			break;
		case Backup_REQ:
		case Backup_RSP:
			pReturn = &backupRun;
			break;
		default:
			break;
	}
	
	return pReturn;
}

char* CreateUdpBackupReqJsonData(void* data)
{
    cJSON *root = NULL;
	char *string = NULL;
	CMD_BACKUP_ONE_REQ_T* pData = (CMD_BACKUP_ONE_REQ_T*)data;

    root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, DM_KEY_BAK_SELECT, pData->bakSelect);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static int ParseUdpBackupReqJsonData(const char* json, CMD_BACKUP_ONE_REQ_T* pData)
{
    int status = 0;
	int bakSeletc;
	
	memset(pData, 0, sizeof(CMD_BACKUP_ONE_REQ_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
	ParseJsonNumber(root, DM_KEY_BAK_SELECT, &bakSeletc);
	pData->bakSelect = bakSeletc;

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateUdpRestoreReqJsonData(void* data)
{
    cJSON *root = NULL;
	char *string = NULL;
	CMD_RESTORE_ONE_REQ_T* pData = (CMD_RESTORE_ONE_REQ_T*)data;

    root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, DM_KEY_BAK_SELECT, pData->bakSelect);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static int ParseUdpRestoreReqJsonData(const char* json, CMD_RESTORE_ONE_REQ_T* pData)
{
    int status = 0;
	
	memset(pData, 0, sizeof(CMD_RESTORE_ONE_REQ_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
	ParseJsonNumber(root, DM_KEY_BAK_SELECT, &pData->bakSelect);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

int PublicUdpCmdReqProcess(int ip, int timeOut, unsigned short cmd, LocalProcess loaclProcess, CreateJsonData createJson, void* data)
{
	char* dataJson = NULL;
	
	PublicUdpCmdRun* publicUdpCmdRun;
		
	PublicUdpCmdReq sendData;
	int i, timeCnt, ret, len, myIp;
	ret = -1;
	
	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	if(myIp == ip)
	{
		if(loaclProcess != NULL)
		{
			ret = loaclProcess(data);
		}
		else
		{
			ret = 0;
		}
	}
	else
	{		
		publicUdpCmdRun = GetPublicUdpCmdRun(cmd);
		if(publicUdpCmdRun == NULL)
		{
			return -1;
		}
		sendData.rand = MyRand() + MyRand2(ip);
		sendData.sourceIp = myIp;
		len = sizeof(sendData) - PUBLIC_UDP_CMD_REQ_DATA_LEN;

		if(createJson != NULL)
		{
			dataJson = createJson(data);
			strncpy(sendData.data, dataJson, PUBLIC_UDP_CMD_REQ_DATA_LEN);
			sendData.data[PUBLIC_UDP_CMD_REQ_DATA_LEN] = 0;
			len += (strlen(sendData.data)+1);
			free(dataJson);
		}

		publicUdpCmdRun->waitFlag = 1;
		publicUdpCmdRun->result = -1;
		publicUdpCmdRun->rand = sendData.rand;
		
		api_udp_device_update_send_data(ip, htons(cmd), (char*)&sendData, len);
		
		
		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(1)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			if(publicUdpCmdRun->waitFlag == 0)
			{
				ret = publicUdpCmdRun->result;
				break;
			}

			if(--timeCnt == 0)
			{
				ret = -1;
				break;
			}
		}

		publicUdpCmdRun->waitFlag = 0;
	}
	
	return ret;
}

int PublicUdpCmdReqProcess2(int ip, int timeOut, unsigned short cmd, LocalProcess loaclProcess, char* data)
{
	char* dataJson = NULL;
	
	PublicUdpCmdRun* publicUdpCmdRun;
		
	PublicUdpCmdReq sendData;
	int i, timeCnt, ret, len, myIp;
	ret = -1;
	
	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	if(myIp == ip)
	{
		if(loaclProcess != NULL)
		{
			ret = loaclProcess((void*)data);
		}
		else
		{
			ret = 0;
		}
	}
	else
	{		
		publicUdpCmdRun = GetPublicUdpCmdRun(cmd);
		if(publicUdpCmdRun == NULL)
		{
			return -1;
		}
		sendData.rand = MyRand() + MyRand2(ip);
		sendData.sourceIp = myIp;
		len = sizeof(sendData) - PUBLIC_UDP_CMD_REQ_DATA_LEN;

		if(data != NULL)
		{
			strncpy(sendData.data, data, PUBLIC_UDP_CMD_REQ_DATA_LEN);
			sendData.data[PUBLIC_UDP_CMD_REQ_DATA_LEN] = 0;
			len += (strlen(sendData.data)+1);
		}

		publicUdpCmdRun->waitFlag = 1;
		publicUdpCmdRun->result = -1;
		publicUdpCmdRun->rand = sendData.rand;
		
		api_udp_device_update_send_data(ip, htons(cmd), (char*)&sendData, len);
		
		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(1)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			if(publicUdpCmdRun->waitFlag == 0)
			{
				ret = publicUdpCmdRun->result;
				break;
			}

			if(--timeCnt == 0)
			{
				ret = -1;
				break;
			}
		}

		publicUdpCmdRun->waitFlag = 0;
	}

	dprintf("ip=%s, ret=%d, data=%s\n", my_inet_ntoa2(ip), ret, data);

	return ret;
}


//接收到PublicUdp命令应答指令
void ReceivePublicUdpCmdRsp(unsigned short cmd, PublicUdpCmdRsp* rspData)
{
	PublicUdpCmdRun* publicUdpCmdRun;

	publicUdpCmdRun = GetPublicUdpCmdRun(cmd);
	if(publicUdpCmdRun != NULL)
	{
		if(publicUdpCmdRun->waitFlag == 1)
		{
			if(rspData->rand == publicUdpCmdRun->rand)
			{
				publicUdpCmdRun->sourceIp = rspData->sourceIp;
				publicUdpCmdRun->result = rspData->result;
				publicUdpCmdRun->waitFlag = 0;
			}
		}
	}
}

//接收到PublicUdp命令请求指令
int ReceivePublicUdpCmdReq(unsigned short cmd, PublicUdpCmdReq* reqData)
{
	PublicUdpCmdRsp	sendData;
	unsigned short rspCmd;
	
	RES_DOWNLOAD_REQ_DATA_T downloadReq;
	CMD_BACKUP_ONE_REQ_T backupReq;
	CMD_RESTORE_ONE_REQ_T restoreReq;
	
	sendData.result = -1;
	
	switch(cmd)
	{
		case IX_DEVICE_RES_DOWNLOAD_REQ:
			sendData.result = CodeDownloadProcess(reqData->data);
			break;
			
		case Backup_REQ:
			if(ParseUdpBackupReqJsonData(reqData->data, &backupReq))
			{
				sendData.result = BackupInstallerBak(backupReq.bakSelect);
			}
			break;
			
		case Restore_REQ:
			if(ParseUdpRestoreReqJsonData(reqData->data, &restoreReq))
			{
				sendData.result = RestoreBak(restoreReq.bakSelect);
			}
			break;
			
		default:
			break;
	}

	rspCmd = cmd + 1;
	
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));

	api_udp_device_update_send_data(reqData->sourceIp, htons(rspCmd), (char*)&sendData, sizeof(sendData) );

	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

