/**
  ******************************************************************************
  * @file    obj_RemoteUpgrade.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_RemoteUpgrade.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "vtk_udp_stack_device_update.h"
#include "task_VideoMenu.h"//#include "MENU_066_FwUpgrade.h"
#include "task_IoServer.h"

static RemoteUpgradeRun remoteUpgradeRun[CLIENT_MAX_NUM];



extern int 	BackFwUpgrade_State;
extern int	BackFwUpgrade_Ser_Select;
extern char BackFwUpgrade_Ser_Disp[5][40];
extern char BackFwUpgrade_Code[7];
extern int BackFwUpgradeLine2StrId;
extern int BackFwUpgradeLine3StrId;
extern int BackFwUpgradeLine4StrId;
extern char BackFwUpgradeLine2ValueStr[40];
extern char BackFwUpgradeLine3ValueStr[40];
extern char BackFwUpgradeLine4ValueStr[40];
extern int  BackFwUpgrade_RebootTime;



//发送远程更新获取信息指令
int API_RemoteUpgradeGetState(int* ip, RemoteUpgrade_T* data, int time)
{
	RemoteUpgradeReq sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && remoteUpgradeRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.data.OP_CODE = UpgradeCMD_OP_CODE_GET_STATE;
	
	api_udp_device_update_send_data(ip, htons(CMD_REMOTE_UPGRADE_REQ), (char*)&sendData, sizeof(sendData) );
		
	remoteUpgradeRun[i].waitFlag = 1;
	remoteUpgradeRun[i].rand = sendData.rand;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(remoteUpgradeRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			remoteUpgradeRun[i].waitFlag = 0;
			return -2;
		}
	}
	
	if(data != NULL)
	{
		*data = remoteUpgradeRun[i].data;
	}
	
	remoteUpgradeRun[i].waitFlag = 0;
	
	return 0;
}

//发送远程更新获取信息指令
int API_RemoteUpgradeStart(int* ip, RemoteUpgrade_T* data, int time)
{
	RemoteUpgradeReq sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && remoteUpgradeRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.data.OP_CODE = UpgradeCMD_OP_CODE_START;
	
	api_udp_device_update_send_data(ip, htons(CMD_REMOTE_UPGRADE_REQ), (char*)&sendData, sizeof(sendData) );
		
	remoteUpgradeRun[i].waitFlag = 1;
	remoteUpgradeRun[i].rand = sendData.rand;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(remoteUpgradeRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			remoteUpgradeRun[i].waitFlag = 0;
			return -2;
		}
	}
	
	if(data != NULL)
	{
		*data = remoteUpgradeRun[i].data;
	}
	
	remoteUpgradeRun[i].waitFlag = 0;
	
	return 0;
}

void DecodeRemoteUpgradeData(RemoteUpgrade_T data)
{
	#define VALUE_LEN	200

	int i, *pos1;
	char temp[VALUE_LEN];
	
	BackFwUpgrade_State = data.state;

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_SERVER_DISP_ID, temp, data);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 5; i++, pos1 = strtok(NULL,","))
	{
		strcpy(BackFwUpgrade_Ser_Disp[i], pos1);
	}
	
	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_SERVER_SELECT_ID, temp, data);
	BackFwUpgrade_Ser_Select = atoi(temp);
	BackFwUpgrade_Ser_Select  =  (BackFwUpgrade_Ser_Select >= 5) ? 1 : BackFwUpgrade_Ser_Select;

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_UPGRADE_CODE_ID, temp, data);
	strcpy(BackFwUpgrade_Code, temp);
	
	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE2_DISP_ID, temp, data);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			BackFwUpgradeLine2StrId = atoi(pos1);
		}
		else
		{
			strcpy(BackFwUpgradeLine2ValueStr, pos1);
		}
	}

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE3_DISP_ID, temp, data);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			BackFwUpgradeLine3StrId = atoi(pos1);
		}
		else
		{
			strcpy(BackFwUpgradeLine3ValueStr, pos1);
		}
	}

	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_LINE4_DISP_ID, temp, data);
	for(i = 0,pos1 = strtok(temp,","); pos1 != NULL && i < 2; i++, pos1 = strtok(NULL,","))
	{
		if(i==0)
		{
			BackFwUpgradeLine4StrId = atoi(pos1);
		}
		else
		{
			strcpy(BackFwUpgradeLine4ValueStr, pos1);
		}
	}
	
	memset(temp, 0, VALUE_LEN);
	GetRemoteFwUpgradeInfo(FW_INFO_UPGRADE_REBOOT_TIME_ID, temp, data);
	BackFwUpgrade_RebootTime = atoi(temp);
	
}

void EncodeRemoteUpgradeData(RemoteUpgrade_T *data)
{
	#define VALUE_LEN	200
	char tempData[VALUE_LEN];
	
	data->state = BackFwUpgrade_State;
	
	memset(data->data, 0, RemoteUpgradeCMD_DATA_LEN);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%s,%s,%s,%s,%s>", FW_INFO_SERVER_DISP_ID, BackFwUpgrade_Ser_Disp[0],
	BackFwUpgrade_Ser_Disp[1], BackFwUpgrade_Ser_Disp[2], BackFwUpgrade_Ser_Disp[3], BackFwUpgrade_Ser_Disp[4]);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d>", FW_INFO_SERVER_SELECT_ID, BackFwUpgrade_Ser_Select);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%s>", FW_INFO_UPGRADE_CODE_ID, BackFwUpgrade_Code);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE2_DISP_ID, BackFwUpgradeLine2StrId, BackFwUpgradeLine2ValueStr);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE3_DISP_ID, BackFwUpgradeLine3StrId, BackFwUpgradeLine3ValueStr);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d,%s>", FW_INFO_LINE4_DISP_ID, BackFwUpgradeLine4StrId, BackFwUpgradeLine4ValueStr);
	strcat(data->data, tempData);

	snprintf(tempData, VALUE_LEN, "<ID=%02d,Value=%d>", FW_INFO_UPGRADE_REBOOT_TIME_ID, BackFwUpgrade_RebootTime);
	strcat(data->data, tempData);

}


//接收到远程更新请求指令
void ReceiveRemoteUpgradeReq(int target_ip, RemoteUpgradeReq* rspData)
{
	RemoteUpgradeRsp sendData;
	char tempData[100];
	
	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = rspData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
	extern int  BackFwUpgrade_ReomteIp;
	
	BackFwUpgrade_ReomteIp = target_ip;

	if(rspData->data.OP_CODE == UpgradeCMD_OP_CODE_START)
	{
		sendData.data.OP_CODE = UpgradeCMD_OP_CODE_START;
		#if defined(PID_IX850)
		if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
		{
			int i;
			StartInitOneMenu(MENU_045_BackFwUpgrade,0,1);
			for(i=0; i<3 && GetCurMenuCnt() != MENU_045_BackFwUpgrade; i++)
			{
				usleep(1000*1000);
			}
			
			if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
			{
				return;
			}
		}
		#else
		if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
		{
			int i;
			StartInitOneMenu(MENU_070_BackFwUpgrade,0,1);
			for(i=0; i<3 && GetCurMenuCnt() != MENU_070_BackFwUpgrade; i++)
			{
				usleep(1000*1000);
			}
			
			if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
			{
				return;
			}
		}
		#endif
		EncodeRemoteUpgradeData(&sendData.data);
		api_udp_device_update_send_data(target_ip, htons(CMD_REMOTE_UPGRADE_RSP), (char*)&sendData, 4*sizeof(int) + strlen(sendData.data.data) + 1 );
	}
	else if(rspData->data.OP_CODE == UpgradeCMD_OP_CODE_GET_STATE)
	{
		sendData.data.OP_CODE = UpgradeCMD_OP_CODE_GET_STATE;
		#if defined(PID_IX850)
		if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
		{
			return;
		}
		#else
		if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
		{
			return;
		}
		#endif
		EncodeRemoteUpgradeData(&sendData.data);
		api_udp_device_update_send_data(target_ip, htons(CMD_REMOTE_UPGRADE_RSP), (char*)&sendData, 4*sizeof(int) + strlen(sendData.data.data) + 1 );
	}
	else if(rspData->data.OP_CODE == UpgradeCMD_OP_CODE_SET_INFO)
	{
		DecodeRemoteUpgradeData(rspData->data);
		EncodeRemoteUpgradeData(&sendData.data);

		char tempString[20];
		#if defined(PID_IX850)
		if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
		{
			return;
		}
		#else
		if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
		{
			return;
		}
		#endif
		sprintf(tempString, "%d", BackFwUpgrade_Ser_Select);
		
		API_Event_IoServer_InnerWrite_All(FWUPGRADE_SER_SELECT, tempString);
		
		if(BackFwUpgrade_Ser_Disp[0][0] == 0)
		{
			strcpy(tempString, "-");
			API_Event_IoServer_InnerWrite_All(FWUPGRADE_OTHER_SER, tempString);
		}
		else
		{
			API_Event_IoServer_InnerWrite_All(FWUPGRADE_OTHER_SER, BackFwUpgrade_Ser_Disp[0]);
		}
		
		if(BackFwUpgrade_Code[0] == 0)
		{
			strcpy(tempString, "-");
			API_Event_IoServer_InnerWrite_All(FWUPGRADE_FW_CODE, tempString);
		}
		else
		{
			API_Event_IoServer_InnerWrite_All(FWUPGRADE_FW_CODE, BackFwUpgrade_Code);
		}
		
		sendData.data.OP_CODE = UpgradeCMD_OP_CODE_SET_INFO;
		api_udp_device_update_send_data(target_ip, htons(CMD_REMOTE_UPGRADE_RSP), (char*)&sendData, 4*sizeof(int) + strlen(sendData.data.data) + 1 );
	}
	else if(rspData->data.OP_CODE == UpgradeCMD_OP_CODE_OPERATION)
	{
		sendData.data.OP_CODE = UpgradeCMD_OP_CODE_OPERATION;
		#if defined(PID_IX850)
		if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
		{
			return;
		}
		#else
		if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
		{
			return;
		}
		#endif
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaOperation);
		
		EncodeRemoteUpgradeData(&sendData.data);
		api_udp_device_update_send_data(target_ip, htons(CMD_REMOTE_UPGRADE_RSP), (char*)&sendData, 4*sizeof(int) + strlen(sendData.data.data) + 1 );
	}
	else if(rspData->data.OP_CODE == UpgradeCMD_OP_CODE_CANCLE)
	{
		sendData.data.OP_CODE = UpgradeCMD_OP_CODE_CANCLE;
		#if defined(PID_IX850)
		if(GetCurMenuCnt() != MENU_045_BackFwUpgrade)
		{
			return;
		}
		#else
		if(GetCurMenuCnt() != MENU_070_BackFwUpgrade)
		{
			return;
		}
		#endif
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OtaReturn);
		
		EncodeRemoteUpgradeData(&sendData.data);
		api_udp_device_update_send_data(target_ip, htons(CMD_REMOTE_UPGRADE_RSP), (char*)&sendData, 4*sizeof(int) + strlen(sendData.data.data) + 1 );
	}
}

//接收远程更新获取信息应答指令
void ReceiveRemoteUpgradeRsp(RemoteUpgradeRsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(remoteUpgradeRun[i].waitFlag)
		{
			if(rspData->rand == remoteUpgradeRun[i].rand)
			{
				remoteUpgradeRun[i].data = rspData->data;
				remoteUpgradeRun[i].waitFlag = 0;
			}
		}
	}
}

//发送远程更新操作指令
int API_RemoteUpgradeOperation(int* ip, int process, RemoteUpgrade_T* data, int time)
{
	RemoteUpgradeReq sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && remoteUpgradeRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.data.OP_CODE = process;

	api_udp_device_update_send_data(ip, htons(CMD_REMOTE_UPGRADE_REQ), (char*)&sendData, 4*sizeof(int));
		
	remoteUpgradeRun[i].waitFlag = 1;
	remoteUpgradeRun[i].rand = sendData.rand;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(remoteUpgradeRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			remoteUpgradeRun[i].waitFlag = 0;
			return -2;
		}
	}
		
	if(data != NULL)
	{
		*data = remoteUpgradeRun[i].data;
	}
	
	remoteUpgradeRun[i].waitFlag = 0;
	
	return 0;
}

//发送远程更新设置参数指令
int API_RemoteUpgradeSetInfo(int* ip, RemoteUpgrade_T* data, int time)
{
	RemoteUpgradeReq sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && remoteUpgradeRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));

	sendData.data = *data;
	sendData.data.OP_CODE = UpgradeCMD_OP_CODE_SET_INFO;

	api_udp_device_update_send_data(ip, htons(CMD_REMOTE_UPGRADE_REQ), (char*)&sendData, 4*sizeof(int)+strlen(sendData.data.data)+1 );
		
	remoteUpgradeRun[i].waitFlag = 1;
	remoteUpgradeRun[i].rand = sendData.rand;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(remoteUpgradeRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			remoteUpgradeRun[i].waitFlag = 0;
			return -2;
		}
	}
	
	if(data != NULL)
	{
		*data = remoteUpgradeRun[i].data;
	}
	
	remoteUpgradeRun[i].waitFlag = 0;
	
	return 0;
}

//czn_20190506_s
#pragma pack(1) 
typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	char				server_ip[16];
}remoteSoftwareUpdate_t;
#pragma pack() 

int API_RemoteAllDevSoftwareUpdate(void)
{
	remoteSoftwareUpdate_t sendData;
	int i, timeCnt;
	int ret;
	
	memset(&sendData, 0, sizeof(remoteSoftwareUpdate_t));

	sendData.rand = MyRand();
	DisableRemoteUpdateServer();
	get_SwUpgradeSer(sendData.server_ip);
	//sendData.data.OP_CODE = UpgradeCMD_OP_CODE_START;
	
	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_REMOTE_SOFTWARE_UPDATE_REQ), (char*)&sendData, sizeof(sendData) );
	
	//api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CMD_REMOTE_SOFTWARE_UPDATE_REQ), (char*)&sendData, sizeof(sendData) );
}

int Recieve_RemoteAllDevSoftwareUpdateReq(char* puff)
{
	remoteSoftwareUpdate_t *pmsg = (remoteSoftwareUpdate_t*)puff;
	if(pmsg->server_ip[0]!=0)
	{
		SetRemoteUpdateServer(pmsg->server_ip);
	}
	else
	{
		DisableRemoteUpdateServer();
	}
	API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_RecvSoftwareUpdateReq);
}
//czn_20190506_e


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

