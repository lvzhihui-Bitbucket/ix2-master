/**
  ******************************************************************************
  * @file    obj_GetMergeInfoByIp.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件
#ifndef _obj_GetMergeInfoByIp_H
#define _obj_GetMergeInfoByIp_H

#include "obj_OtherParameterSetting.h"
#include "obj_GetInfoByIp.h"

// Define Object Property-------------------------------------------------------
#define ABOUT_DATA_DOMAIN_MAX_LEN		30

typedef struct
{
	char	Result[5+1];							//about结果
	char	IP_Addr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	Device_Addr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	Name[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	G_Nbr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	L_Nbr[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	SW_Ver[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	HW_Ver[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpgradeTime[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpgradeCode[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpTime[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	SerialNo[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DeviceType[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DeviceModel[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	AreaCode[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	TransferState[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	HW_Address[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	SubnetMask[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DefaultRoute[ABOUT_DATA_DOMAIN_MAX_LEN];
	
}GET_ABOUT_RSP_DATA_T;

typedef struct
{
	char					Result[5+1];	
	DeviceInfo				info;
	GET_ABOUT_RSP_DATA_T 	about;
	FW_VER_VERIFY_T 		version;
}MERGE_INFO_RSP_DATA;


typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
} GetMergeInfoByIpReq;

typedef struct
{
	int			rand;			//随机数
	char		data[1000];
} GetMergeInfoByIpRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	char			data[1000];
} GetMergeInfoRun;



// Define Object Function - Public----------------------------------------------

//发送获取信息指令
int API_GetMergeInfoByIp(int ip, int time, char* info);


//接收到Get info请求指令
void ReceiveGetMergeInfoReq(int target_ip, GetMergeInfoByIpReq* pReadReq);

//接收到获取信息应答指令
void ReceiveGetMergeInfoRsp(GetMergeInfoByIpRsp* rspData);


// Define Object Function - Private---------------------------------------------



#endif


