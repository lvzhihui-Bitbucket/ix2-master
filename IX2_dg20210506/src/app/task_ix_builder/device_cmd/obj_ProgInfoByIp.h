/**
  ******************************************************************************
  * @file    obj_ProgInfoByIp.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_ProgInfoByIp_H
#define _obj_ProgInfoByIp_H

#include "obj_GetInfoByIp.h"

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

// GetIpBy Number 指令包结构
typedef struct
{
	int			rand;				//随机数
	int			sourceIp;			//源ip
	DeviceInfo 	data;				//information
} ProgInfoByIpReq;

typedef struct
{
	int			rand;				//随机数
	int			sourceIp;			//源ip
	DeviceInfo	data;				//information
} ProgInfoByIpRsp;

typedef struct
{
	int				waitFlag;		//等待回应标记
	int				rand;			//随机数
	DeviceInfo 		data;			//information
} ProgInfoRun;

#pragma pack()



// Define Object Function - Public----------------------------------------------

//发送获取信息指令
int API_ProgInfoByIp(int ip, int time, DeviceInfo* info);

//接收到Prog info请求指令
void ReceiveProgInfoReq(ProgInfoByIpReq* pReadReq);

//接收到Prog信息应答指令
void ReceiveProgInfoRsp(ProgInfoByIpRsp* rspData);


// Define Object Function - Private---------------------------------------------



#endif


