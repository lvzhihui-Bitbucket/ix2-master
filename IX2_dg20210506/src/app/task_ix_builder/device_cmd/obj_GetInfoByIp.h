/**
  ******************************************************************************
  * @file    obj_GetInfoByIp.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_GetInfoByIp_H
#define _obj_GetInfoByIp_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐
typedef struct
{
	char	MFG_SN[12+1];		//MFG_SN
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	Local[6+1];			//Local
	char	Global[10+1];		//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
	int		VIRTUAL_DEVICE;
	char	REMARK[21];
	int		IP_NODE_ID;
	int		DT_ADDR;
	char	localSip[32+1];			//本机Sip帐号
	char	monCode[4+1];			//monitor code
	char	Ver_Devicetype[10+1];	//固件更新类型匹配
	char	monPwd[10+1];			//监视帐号的密码
} DeviceInfo;

// GetIpBy Number 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
} GetInfoByIpReq;

typedef struct
{
	int			rand;		//随机数
	DeviceInfo	data;		//BD_RM_MS
} GetInfoByIpRsp;

typedef struct
{
	pthread_mutex_t 	lock;
	int					waitFlag;			//等待回应标记
	int					rand;				//随机数
	DeviceInfo 			data;
} GetInfoRun;

#pragma pack()



// Define Object Function - Public----------------------------------------------

//发送获取信息指令
int API_GetInfoByIp(int ip, int time, DeviceInfo* info);

//接收到Get info请求指令
void ReceiveGetInfoReq(int target_ip, GetInfoByIpReq* pReadReq);

//接收到获取信息应答指令
void ReceiveGetInfoRsp(int target_ip, GetInfoByIpRsp* rspData, int len);


// Define Object Function - Private---------------------------------------------



#endif


