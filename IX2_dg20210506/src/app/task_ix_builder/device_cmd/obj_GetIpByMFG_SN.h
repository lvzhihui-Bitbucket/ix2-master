/**
  ******************************************************************************
  * @file    obj_GetIpByMFG_SN.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_GetIpByMFG_SN_H
#define _obj_GetIpByMFG_SN_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int		Ip;				//ip
	char	MFG_SN[12+1];		//MFG_SN
	int		upTime;				//上线时间
} GetIpData;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	GetIpData 		data;
} GetIpRun;

// GetIpBy MFG_SN 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	MFG_SN[12+1];		//MFG_SN
} GetIpByMFG_SNReq;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	MFG_SN[12+1];		//MFG_SN
	int		upTime;				//上线时间
} GetIpByMFG_SNRsp;


#pragma pack()



// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


