/**
  ******************************************************************************
  * @file    obj_CallBtnBinding.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CallBtnBinding_H
#define _obj_CallBtnBinding_H

// Define Object Property-------------------------------------------------------

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				state;
} GetCallBtnBindingStateRun;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		state;
} GetCallBtnBindingStateReq_t;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		state;
} GetCallBtnBindingStateRsp_t;

typedef struct
{
	int		waitFlag;			//等待回应标记
	int		rand;				//随机数
	char	BD_RM_MS[11];		//房号
	char	CallNbr[11];		//CallNbr
} CallBtnBindingRun;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	BD_RM_MS[11];		//房号
} CallBtnBindingReq_t;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	BD_RM_MS[11];		//房号
	char	CallNbr[11];		//CallNbr
} CallBtnBindingRsp_t;

#pragma pack()



// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


