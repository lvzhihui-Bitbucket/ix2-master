/**
  ******************************************************************************
  * @file    obj_GetIpByInput.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetIpByNumber.h"
#include "obj_GetIpByInput.h"

//通过输入得到IP
// paras：
// input_str: 输入字符串
// record: IP和房号
// 0/得不到IP, >0/得到多个房号IP 
int API_GetIpByInput(const char* input, IpCacheRecord_T record[])
{
	int i, j;
	GetIpRspData data = {0};
	DeviceTypeAndArea_T device;
	char* bd_rm_ms[11] = {0};
	
	GetBdRmMsByInput(input, bd_rm_ms);

	API_GetIpNumberFromNet(bd_rm_ms, input, GetSysVerInfo_bd(), 2, 0, &data);
	//printf("11111111111111111 data.cnt = %d\n", data.cnt);
	for(i = 0, j = 0; i < data.cnt; i++)
	{
		if(api_nm_if_judge_include_dev(data.BD_RM_MS[i], data.Ip[i])==0)
		{
			 continue;
		}
		//有呼叫控制表
		if(hasCallCtrlTable())
		{
			if(!Find_bd_rm_ms_in_call_control_table(data.BD_RM_MS[i]))
			{
				continue;
			}
		}
		//无呼叫控制表
		else
		{
			//printf("22222222222222 data.BD_RM_MS[i] = %s\n", data.BD_RM_MS[i]);
			device = GetDeviceTypeAndAreaByNumber(data.BD_RM_MS[i]);
			//printf("33333333333 device.type = %d, device.area = %d\n", device.type, device.area);
			
			//本机是小区管理机或者单栋管理机
			if(!memcmp(GetSysVerInfo_BdRmMs(), "00000000", 8) || !strcmp(GetSysVerInfo_rm(), "0000"))
			{
				if(device.type == TYPE_GL || device.type == TYPE_IM)
				{
					//printf("44444444444 device.type = %d, device.area = %d\n", device.type, device.area);					
				}
				else
				{
					//printf("5555555555555 device.type = %d, device.area = %d\n", device.type, device.area);					
					continue;
				}
			}
			//本机是普通分机
			else
			{
				if((device.area == CommonArea && device.type == TYPE_GL) ||
					(device.area == SameBuilding && device.type == TYPE_GL) ||
					(device.area == SameBuilding && device.type == TYPE_IM))
				{
					//printf("666666666666666 device.type = %d, device.area = %d\n", device.type, device.area);					
				}
				else
				{
					//printf("777777777777777 device.type = %d, device.area = %d\n", device.type, device.area);					
					continue;
				}
			}
		}

		record[j].ip = data.Ip[i];
		strcpy(record[j++].BD_RM_MS, data.BD_RM_MS[i]);
	}
	//printf("999999999999 j = %d\n", j); 				

	return j;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

