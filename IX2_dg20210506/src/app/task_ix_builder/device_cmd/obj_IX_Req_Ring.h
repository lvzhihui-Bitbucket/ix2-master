/**
  ******************************************************************************
  * @file    obj_IX_Req_Ring.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_IX_Req_Ring_H
#define _obj_IX_Req_Ring_H

// Define Object Property-------------------------------------------------------

#define RING_DATA_LEN		100

typedef struct
{
	int				rand;					//随机数
	int				sourceIp;				//源ip
	char			data[RING_DATA_LEN+1];
} IxReqRingReq;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	int				result;				//result
	char			DeviceNbr[10+1];		//房号
	char			IP_Addr[15+1];			//IP地址
	char			MFG_SN[12+1];			//MFG_SN
} IxReqRingRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	IxReqRingRsp	rsp;
}IxReqRingRun;


// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------



#endif


