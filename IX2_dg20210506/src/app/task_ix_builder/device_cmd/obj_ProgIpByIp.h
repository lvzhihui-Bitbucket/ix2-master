/**
  ******************************************************************************
  * @file    obj_ProgIpByIp.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_ProgIpByIp_H
#define _obj_ProgIpByIp_H


// Define Object Property-------------------------------------------------------
typedef struct
{
	char	MFG_SN[12+1];			
	char	IP_ASSIGNED[15+1];		//IP策略
	char	IP_ADDR[15+1];			
	char	IP_MASK[15+1];			
	char	IP_GATEWAY[15+1];
}PROG_IP_ADDR_REQ_DATA_T;

typedef struct
{
	int						rand;				//随机数
	int						sourceIp;			//源ip
	PROG_IP_ADDR_REQ_DATA_T	data;
} IxProgIpAddrReq;

typedef struct
{
	int						rand;				//随机数
	int						sourceIp;			//源ip
	int						result;				//result
} IxProgIpAddrRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				sourceIp;			//源ip
	int				result;
}IxProgIpAddrRun;


// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


