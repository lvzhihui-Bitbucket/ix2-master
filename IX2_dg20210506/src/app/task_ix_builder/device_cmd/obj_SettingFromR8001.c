/**
  ******************************************************************************
  * @file    obj_GetIpByMFG_SN.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetIpByMFG_SN.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_SettingFromR8001.h"
#include "obj_R8001_Table.h"


static SettingFromR8001Run settingFromR8001Run[CLIENT_MAX_NUM];

//发送SettingFromR8001指令
int API_SettingFromR8001(int ip)
{
	SettingFromR8001Req sendData;
	int len, i, timeCnt;

	for(i = 0; i < CLIENT_MAX_NUM && settingFromR8001Run[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}
	
	if(ip == inet_addr(GetSysVerInfo_IP()))
	{
		CallNbr_T imCallNbr;		
		
		if(get_r8001_record_by_brm(GetSysVerInfo_BdRmMs(),&imCallNbr))
		{
			UpdateCallNbrByRes(imCallNbr);
		}
		return 0;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	len = sizeof(sendData) - SettingFromR8001DataLen;

	rejoin_multicast_group();
	api_udp_device_update_send_data(ip, htons(SettingFromR8001_REQ), (char*)&sendData, len);
		
	settingFromR8001Run[i].waitFlag = 1;
	settingFromR8001Run[i].rand = sendData.rand;
	
	
	timeCnt = 1;//time*1000/UDP_WAIT_RSP_TIME;
	while(settingFromR8001Run[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(settingFromR8001Run[i].waitFlag)
		{
			timeCnt--;
		}

		if(timeCnt <= 0)
		{
			settingFromR8001Run[i].waitFlag = 0;
			return -1;
		}
	}

	return 0;	
}

//接收到应答指令
void ReceiveSettingFromR8001Rsp(SettingFromR8001Rsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(settingFromR8001Run[i].waitFlag)
		{
			if(rspData->rand == settingFromR8001Run[i].rand)
			{
				settingFromR8001Run[i].waitFlag = 0;
			}
		}
	}
}


//接收到请求指令
void ReceiveSettingFromR8001Req(int target_ip, SettingFromR8001Req* pReadReq)
{
	//网络没初始化好，不应答远程指令
	if(!GetNetworkIsStarted())
	{
		return;
	}
	
	SettingFromR8001Rsp rspData;
	int len;
	CallNbr_T imCallNbr;
	
	rspData.rand = pReadReq->rand;
	len = sizeof(rspData) - SettingFromR8001DataLen;
	
	api_udp_device_update_send_data(target_ip, htons(SettingFromR8001_RSP), (char*)&rspData, len);

	if(get_r8001_record_by_brm(GetSysVerInfo_BdRmMs(),&imCallNbr))
	{
		UpdateCallNbrByRes(imCallNbr);
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

