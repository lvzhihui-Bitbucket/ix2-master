/**
  ******************************************************************************
  * @file    obj_IX_Req_Tip.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_IX_Report.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_SearchIxProxy.h"
#include "obj_OtherParameterSetting.h"
#include "obj_GetInfoByIp.h"
#include "task_IxProxy.h"
#include "define_file.h"
#include "task_IoServer.h"




static IxReportRun ixReportRun;

static char* CreateReportIpAddrJsonData(REPORT_IP_ADDR_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
	
    cJSON_AddStringToObject(root, "MFG_SN", data->r8012Device.MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->r8012Device.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->r8012Device.Local);
    cJSON_AddStringToObject(root, "Global", data->r8012Device.Global);
    cJSON_AddStringToObject(root, "IP_ASSIGNED", data->r8012Device.IP_STATIC);	//历史问题//IP_ASSIGNED不一样

    cJSON_AddStringToObject(root, "IP_ADDR", data->r8012Device.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->r8012Device.IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->r8012Device.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->r8012Device.deviceType);
    cJSON_AddStringToObject(root, "name1", data->r8012Device.name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->r8012Device.name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->r8012Device.bootTime);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static char* CreateReportRebootJsonData(REPORT_REBOOT_DATA_T* data)
{
    cJSON *root = NULL;
    cJSON *ver = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "MFG_SN", data->r8012Device.MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->r8012Device.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->r8012Device.Local);
    cJSON_AddStringToObject(root, "Global", data->r8012Device.Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->r8012Device.IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->r8012Device.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->r8012Device.IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->r8012Device.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->r8012Device.deviceType);
    cJSON_AddStringToObject(root, "name1", data->r8012Device.name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->r8012Device.name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->r8012Device.bootTime);

    
    ver = cJSON_AddObjectToObject(root, "FwVersion");
	
    cJSON_AddStringToObject(ver, "Device type", data->FwVersion.deviceType);
    cJSON_AddStringToObject(ver, "Code info", data->FwVersion.codeInfo);
    cJSON_AddNumberToObject(ver, "Code size", data->FwVersion.codeSize);
    cJSON_AddStringToObject(ver, "App Code", data->FwVersion.appCode);
    cJSON_AddStringToObject(ver, "App Ver", data->FwVersion.appVer);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


static int ParseReportIpAddrJsonData(const char* json, REPORT_IP_ADDR_DATA_T* pData)
{
    int status = 0;

	memset(pData, 0, sizeof(REPORT_IP_ADDR_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
    ParseJsonString(root, "MFG_SN", pData->r8012Device.MFG_SN, 12);
    ParseJsonString(root, "BD_RM_MS", pData->r8012Device.BD_RM_MS, 10);
    ParseJsonString(root, "Local", pData->r8012Device.Local, 6);
    ParseJsonString(root, "Global", pData->r8012Device.Global, 10);
    ParseJsonString(root, "IP_ASSIGNED", pData->r8012Device.IP_STATIC, 6);

    ParseJsonString(root, "IP_ADDR", pData->r8012Device.IP_ADDR, 15);
    ParseJsonString(root, "IP_MASK", pData->r8012Device.IP_MASK, 15);
    ParseJsonString(root, "IP_GATEWAY", pData->r8012Device.IP_GATEWAY, 15);
	
    ParseJsonNumber(root, "deviceType", &(pData->r8012Device.deviceType));
	
    ParseJsonString(root, "name1", pData->r8012Device.name1, 20);
	
    ParseJsonString(root, "name2_utf8", pData->r8012Device.name2_utf8, 40);

    ParseJsonNumber(root, "bootTime", &(pData->r8012Device.bootTime));
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

static int ParseReportRebootJsonData(const char* json, REPORT_REBOOT_DATA_T* pData)
{
    int status = 0;
	
    const cJSON *dataTime = NULL;

	memset(pData, 0, sizeof(REPORT_REBOOT_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

    ParseJsonString(root, "MFG_SN", pData->r8012Device.MFG_SN, 12);
    ParseJsonString(root, "BD_RM_MS", pData->r8012Device.BD_RM_MS, 10);
    ParseJsonString(root, "Local", pData->r8012Device.Local, 6);
    ParseJsonString(root, "Global", pData->r8012Device.Global, 10);
    ParseJsonString(root, "IP_STATIC", pData->r8012Device.IP_STATIC, 6);

    ParseJsonString(root, "IP_ADDR", pData->r8012Device.IP_ADDR, 15);
    ParseJsonString(root, "IP_MASK", pData->r8012Device.IP_MASK, 15);
    ParseJsonString(root, "IP_GATEWAY", pData->r8012Device.IP_GATEWAY, 15);
	
    ParseJsonNumber(root, "deviceType", &(pData->r8012Device.deviceType));
	
    ParseJsonString(root, "name1", pData->r8012Device.name1, 20);
	
    ParseJsonString(root, "name2_utf8", pData->r8012Device.name2_utf8, 40);

    ParseJsonNumber(root, "bootTime", &(pData->r8012Device.bootTime));

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}


static char* CreateReportReqCallNbrJsonData(REPORT_REQ_CALL_NBR_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "MFG_SN", data->r8012Device.MFG_SN);
    cJSON_AddStringToObject(root, "DeviceNbr", data->r8012Device.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->r8012Device.Local);
    cJSON_AddStringToObject(root, "Global", data->r8012Device.Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->r8012Device.IP_STATIC);

    cJSON_AddStringToObject(root, "IP_Addr", data->r8012Device.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->r8012Device.IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->r8012Device.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->r8012Device.deviceType);
    cJSON_AddStringToObject(root, "name1", data->r8012Device.name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->r8012Device.name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->r8012Device.bootTime);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static char* CreateReqTipJsonData(REPORT_TIP_REQ_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "MFG_SN", data->r8012Device.MFG_SN);
    cJSON_AddStringToObject(root, "DeviceNbr", data->r8012Device.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->r8012Device.Local);
    cJSON_AddStringToObject(root, "Global", data->r8012Device.Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->r8012Device.IP_STATIC);

    cJSON_AddStringToObject(root, "IP_Addr", data->r8012Device.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->r8012Device.IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->r8012Device.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->r8012Device.deviceType);
    cJSON_AddStringToObject(root, "name1", data->r8012Device.name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->r8012Device.name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->r8012Device.bootTime);

	string = cJSON_Print(root);
	cJSON_Delete(root);

	return string;
}

static char* CreateDownloadReqJsonData(DOWNLOAD_REQ_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "res_nbr", data->res_nbr);

    cJSON_AddStringToObject(root, "MFG_SN", data->r8012Device.MFG_SN);
    cJSON_AddStringToObject(root, "DeviceNbr", data->r8012Device.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->r8012Device.Local);
    cJSON_AddStringToObject(root, "Global", data->r8012Device.Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->r8012Device.IP_STATIC);

    cJSON_AddStringToObject(root, "IP_Addr", data->r8012Device.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->r8012Device.IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->r8012Device.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->r8012Device.deviceType);
    cJSON_AddStringToObject(root, "name1", data->r8012Device.name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->r8012Device.name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->r8012Device.bootTime);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


int IxReportProcess(int ip, int timeOut, unsigned short reportType, char* data)
{
	IxReportReq sendData = {0};
	int i, timeCnt, ret, len;
	
	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.reportType = reportType;
	
	if(data != NULL)
	{
		strncpy(sendData.data, data, IX_REPORT_DATA_LEN);
	}

	len = sizeof(sendData)- IX_REPORT_DATA_LEN + strlen(sendData.data);

	if(sendData.sourceIp == ip)
	{
		return ReceiveIxReportCmdReq(&sendData);
	}

	ixReportRun.waitFlag = 1;
	ixReportRun.result = -1;
	ixReportRun.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(IX_Report_REQ), (char*)&sendData, len);
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(ixReportRun.waitFlag == 0)
		{
			ret = ixReportRun.result;
			break;
		}

		if(--timeCnt == 0)
		{
			ret = -1;
			break;
		}
	}

	ixReportRun.waitFlag = 0;
	return ret;
}

//接收到IxReport应答指令
void ReceiveIxReportCmdRsp(IxReportRsp* rspData)
{
	if(ixReportRun.waitFlag == 1)
	{
		if(rspData->rand == ixReportRun.rand)
		{
			ixReportRun.sourceIp = rspData->sourceIp;
			ixReportRun.result = rspData->result;
			ixReportRun.waitFlag = 0;
		}
	}
}

int TransmitCmdToPC(unsigned short type, char* data)
{
	int result;

	IxProxyData_T proxyData;
	proxyData = GetMyProxyData();
	
	
	if(IsMyProxyLinked())
	{
		result = 0;
		API_IxProxyClientTransmit(type, data);
	}
	else
	{
		result = -1;
	}
	
	return result;
}

//接收到IxReport请求指令
int ReceiveIxReportCmdReq(IxReportReq* reqData)
{
	IxReportRsp	sendData;
	
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));

	REPORT_IP_ADDR_DATA_T reportIpAddrData;
	REPORT_REBOOT_DATA_T reportRebootData;

	sendData.result = TransmitCmdToPC(reqData->reportType, reqData->data);
	
	switch(reqData->reportType)
	{
		case REPORT_IP_ADDR_REQ:
			ParseReportIpAddrJsonData(reqData->data, &reportIpAddrData);
			break;
			
		case REPORT_REBOOT_REQ:
			ParseReportRebootJsonData(reqData->data, &reportRebootData);
			break;
	}	

	if(reqData->sourceIp != sendData.sourceIp)
	{
		api_udp_device_update_send_data(reqData->sourceIp, htons(IX_Report_RSP), (char*)&sendData, sizeof(sendData) );
	}
	
	return 0;
}

void IxReport(int ip, unsigned short reportType)
{
	char ioData[501];
	char* data = NULL;
	
	DeviceInfo devideInfo;
	R8012_Device r8012DeviceInfo;
	
	REPORT_REBOOT_DATA_T reportRebootData;
	REPORT_IP_ADDR_DATA_T reportIpAddrData;
	REPORT_REQ_CALL_NBR_DATA_T reportReqCallNbrData;
	DOWNLOAD_REQ_T requestDownloadData;

	GetMyInfo(ip, &devideInfo);
	DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DeviceInfo, GetSystemBootTime());
	
	switch(reportType)
	{
		case REPORT_REBOOT_REQ:
			reportRebootData.r8012Device = r8012DeviceInfo;
			if(API_Event_IoServer_InnerRead_All(FWVersionAndVerify, ioData) == 0)
			{
				ParseFwVerVerifyObject(ioData, &reportRebootData.FwVersion);
			}
			data = CreateReportRebootJsonData(&reportRebootData);
			break;
		case REPORT_IP_ADDR_REQ:
			reportIpAddrData.r8012Device = r8012DeviceInfo;
			switch(GetIpActionType())
			{
				case 1:
					strcpy(reportIpAddrData.r8012Device.IP_STATIC, "DHCP");
					break;
				case 2:
					strcpy(reportIpAddrData.r8012Device.IP_STATIC, "AUTO");
					break;
				case 3:
					strcpy(reportIpAddrData.r8012Device.IP_STATIC, "STATIC");
					break;
				default:
					strcpy(reportIpAddrData.r8012Device.IP_STATIC, "UNKOWN");
					break;
			}
			data = CreateReportIpAddrJsonData(&reportIpAddrData);
			break;
		case REQUEST_PROG_CALL_NBR_REQ:
			reportReqCallNbrData.r8012Device = r8012DeviceInfo;
			data = CreateReportReqCallNbrJsonData(&reportReqCallNbrData);
			break;
			
		case REQUEST_TIP_REQ:
			reportReqCallNbrData.r8012Device = r8012DeviceInfo;
			data = CreateReportReqCallNbrJsonData(&reportReqCallNbrData);
			break;
	}	
	
	IxReportProcess(ip, 1, reportType, data);
	
	if(data != NULL)
	{
		free(data);
	}
}

static int ixReportResDownloadIp = 0;

void SetIxReportResDownloadIp(int ip)
{
	ixReportResDownloadIp = ip;
}

int GetIxReportResDownloadIp(void)
{
	return ixReportResDownloadIp;
}
		
void IxReportResDownload(int* reportIp, int reportCnt, char* TYPE, char* STATE, char* RATES, char* RES_NBR, char* IP, char* CODE)
{
	char* data = NULL;
	int i;
	
	DeviceInfo devideInfo;
	R8012_Device r8012DeviceInfo;
    cJSON *root = NULL;
	
	if(reportCnt == 0)
	{
		return;
	}
	
	GetMyInfo(reportIp, &devideInfo);
	DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DeviceInfo, GetSystemBootTime());
	
    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, DM_KEY_TYPE, GetNotNullString(TYPE));
    cJSON_AddStringToObject(root, DM_KEY_STATE, GetNotNullString(STATE));
    cJSON_AddStringToObject(root, DM_KEY_RATES, GetNotNullString(RATES));
	cJSON_AddStringToObject(root, DM_KEY_RES_NBR, GetNotNullString(RES_NBR));
    cJSON_AddStringToObject(root, DM_KEY_IP, GetNotNullString(IP));
    cJSON_AddStringToObject(root, DM_KEY_CODE, GetNotNullString(CODE));
	
	cJSON_AddStringToObject(root, DM_KEY_MFG_SN, r8012DeviceInfo.MFG_SN);
    cJSON_AddStringToObject(root, DM_KEY_DeviceNbr, r8012DeviceInfo.BD_RM_MS);
    cJSON_AddStringToObject(root, DM_KEY_Local, r8012DeviceInfo.Local);
    cJSON_AddStringToObject(root, DM_KEY_Global, r8012DeviceInfo.Global);
    cJSON_AddStringToObject(root, DM_KEY_IP_STATIC, r8012DeviceInfo.IP_STATIC);

    cJSON_AddStringToObject(root, DM_KEY_IP_Addr, r8012DeviceInfo.IP_ADDR);
    cJSON_AddStringToObject(root, DM_KEY_IP_MASK, r8012DeviceInfo.IP_MASK);
    cJSON_AddStringToObject(root, DM_KEY_IP_GATEWAY, r8012DeviceInfo.IP_GATEWAY);
    cJSON_AddNumberToObject(root, DM_KEY_deviceType, r8012DeviceInfo.deviceType);
    cJSON_AddStringToObject(root, DM_KEY_name1, r8012DeviceInfo.name1);
	
    cJSON_AddStringToObject(root, DM_KEY_name2_utf8, r8012DeviceInfo.name2_utf8);
    cJSON_AddNumberToObject(root, DM_KEY_bootTime, r8012DeviceInfo.bootTime);

	data = cJSON_Print(root);
	cJSON_Delete(root);

	for(i = 0; i < reportCnt; i++)
	{
		IxReportProcess(reportIp[i], IxProxyWaitUdpTime, REPORT_REQ_RES_DOWNLOAD, data);
	}
	
	if(data != NULL)
	{
		free(data);
	}
}

void IxRequestDownload(int ip, const char* res_nbr)
{
	char* data = NULL;
	
	DeviceInfo devideInfo;
	R8012_Device r8012DeviceInfo;
	DOWNLOAD_REQ_T requestDownloadData;

	if(res_nbr == NULL)
	{
		return;
	}
	
	if(strlen(res_nbr) != 4)
	{
		return;
	}

	GetMyInfo(ip, &devideInfo);
	DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DeviceInfo, GetSystemBootTime());

	requestDownloadData.r8012Device = r8012DeviceInfo;
	
	strncpy(requestDownloadData.res_nbr, res_nbr, 6);
	
	data = CreateDownloadReqJsonData(&requestDownloadData);

	IxReportProcess(ip, 1, REQUEST_RES_Download_REQ, data);
	
	if(data != NULL)
	{
		free(data);
	}
}

int IxReportShell(int ip, const char* reportData)
{
	#define EscapeCharacterLen		100		//预留400个转义字符吧
	cJSON* sendJson;
	char* sendString = NULL;
	int ret;
	int len, sendLen, oneLen, maxOneLen, stringLen;
	char sendData[IX_REPORT_DATA_LEN];	//因为生成json之后添加了转义字符，一次不能发太长，预留400个转义字符吧
	sendJson = cJSON_CreateObject();

	cJSON_AddStringToObject(sendJson, DM_KEY_DeviceNbr, GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(sendJson, DM_KEY_MFG_SN, GetSysVerInfo_Sn());
	cJSON_AddStringToObject(sendJson, DM_KEY_IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	cJSON_AddStringToObject(sendJson, "SHELL_REPORT", "");
	
	len = strlen(reportData) + 1;
	sendString = cJSON_PrintUnformatted(sendJson);
	maxOneLen = IX_REPORT_DATA_LEN - strlen(sendString) - 1 - EscapeCharacterLen;

	for(sendLen = 0; sendLen < len; sendLen += oneLen)
	{
		oneLen = maxOneLen;
		if(oneLen > (len - sendLen))
		{
			oneLen = len - sendLen;
		}

		do
		{
			memcpy(sendData, reportData+sendLen, oneLen);
			sendData[oneLen] = 0;

			if(sendString)
			{
				free(sendString);
				sendString = NULL;
			}

			cJSON_DeleteItemFromObjectCaseSensitive(sendJson, "SHELL_REPORT");
			cJSON_AddStringToObject(sendJson, "SHELL_REPORT", sendData);
			sendString = cJSON_PrintUnformatted(sendJson);
			stringLen = strlen(sendString);

			if(stringLen >= IX_REPORT_DATA_LEN)
			{
				oneLen -= EscapeCharacterLen;
			}

		} while (stringLen >= IX_REPORT_DATA_LEN);

		
		if(sendString)
		{
			ret = IxReportProcess(ip, 1, REPORT_SHELL_REQ, sendString);
			free(sendString);
			sendString = NULL;
		}
		//dprintf("len=%d, sendLen=%d, oneLen=%d, maxOneLen=%d, stringLen=%d\n", len, sendLen, oneLen, maxOneLen, stringLen);
		if(ret != 0)
		{
			dprintf("IxReportShell Error!!!!!!!!!!!!!!!!!!!!\n");
			break;
		}
	}

	cJSON_Delete(sendJson);

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

