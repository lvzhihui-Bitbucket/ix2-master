/**
  ******************************************************************************
  * @file    obj_RemoteTB_Process.c
  * @author  czb
  * @version V00.01.00
  * @date    2023.3.13
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <string.h>
#include "cJSON.h"
#include "obj_RemoteTB_Process.h"
#include "obj_PublicUnicastCmd.h"
#include "vtk_udp_stack_device_update.h"

const char* GetJsonItemString(cJSON* obj,  const char* itemName)
{
	char* item = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(obj, itemName));
	item = (item != NULL ? item : "");
	return item;
}

int GetJsonItemInt(cJSON* obj, const char* itemName)
{
	int retInt = 0;
	cJSON* result;

	result = cJSON_GetObjectItemCaseSensitive(obj, itemName);
	if(cJSON_IsNumber(result))
	{
		retInt = result->valueint;
	}

	return retInt;
}

int RemoteTableProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *tbProcess = cJSON_Parse(dataIn.data);
	char* tbName = GetJsonItemString(tbProcess, "TB_NAME");
	char* operation = GetJsonItemString(tbProcess, "OPERATION");
	cJSON* where;
	cJSON* view;
	cJSON* record;

	cJSON *retValue  = cJSON_CreateObject();
	cJSON_AddStringToObject(retValue, "TB_NAME", tbName);
	cJSON_AddStringToObject(retValue, "OPERATION", operation);

	if(!strcmp(operation, "COUNT"))
	{
		where = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_WHERE");
		cJSON_AddNumberToObject(retValue, "RESULT", API_TB_CountByName(tbName, where));
	}
	else if(!strcmp(operation, "ADD"))
	{
		record = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_RECORD");
		cJSON_AddNumberToObject(retValue, "RESULT", API_TB_AddByName(tbName, record));
	}
	else if(!strcmp(operation, "SELECT"))
	{
		view =	cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_VIEW"), 1);
		if(view == NULL)
		{
			view = cJSON_CreateArray();
		}

		cJSON_AddItemToObject(retValue, "TB_VIEW", view);
		where = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_WHERE");
		cJSON_AddNumberToObject(retValue, "RESULT", API_TB_SelectBySortByName(tbName, view, where, GetJsonItemInt(tbProcess, "TB_SORT")));
	}
	else if(!strcmp(operation, "DELETE"))
	{
		where = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_WHERE");
		cJSON_AddNumberToObject(retValue, "RESULT", API_TB_DeleteByName(tbName, where));
	}
	else if(!strcmp(operation, "UPDATE"))
	{
		where = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_WHERE");
		record = cJSON_GetObjectItemCaseSensitive(tbProcess, "TB_RECORD");
		cJSON_AddNumberToObject(retValue, "RESULT", API_TB_UpdateByName(tbName, where, record));
	}
	else if(!strcmp(operation, "KEY"))
	{
		cJSON_AddItemToObject(retValue, "RESULT", cJSON_Duplicate(API_TB_GetKeyByName(tbName), 1));
	}
	else if(!strcmp(operation, "NEW_RECORD"))
	{
		cJSON_AddItemToObject(retValue, "RESULT", API_TB_CreakNewRecordByName(tbName));
	}
	else
	{
		cJSON_AddStringToObject(retValue, "RESULT", "Operation code Error.");
	}

	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(retValue);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	cJSON_Delete(retValue);
	cJSON_Delete(tbProcess);

	return 0;
}

//远程读取表数量
int API_RemoteTableCount(int ip, char* TB_name, cJSON* where)
{
	cJSON* retJson = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "COUNT");
	cJSON_AddItemToObject(sendJson, "TB_WHERE", cJSON_Duplicate(where, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retInt = GetJsonItemInt(retJson, "RESULT");
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retInt;
}

//远程删除表
int API_RemoteTableDelete(int ip, char* TB_name, cJSON* where)
{
	cJSON* retJson = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "DELETE");
	cJSON_AddItemToObject(sendJson, "TB_WHERE", cJSON_Duplicate(where, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retInt = GetJsonItemInt(retJson, "RESULT");
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retInt;
}

//远程增加表记录
int API_RemoteTableAdd(int ip, char* TB_name, cJSON* record)
{
	cJSON* retJson = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "ADD");
	cJSON_AddItemToObject(sendJson, "TB_RECORD", cJSON_Duplicate(record, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retInt = GetJsonItemInt(retJson, "RESULT");
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}


	return retInt;
}

//远程更新表记录
int API_RemoteTableUpdate(int ip, char* TB_name, cJSON* where, cJSON* record)
{
	cJSON* retJson = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "UPDATE");
	cJSON_AddItemToObject(sendJson, "TB_WHERE", cJSON_Duplicate(where, 1));
	cJSON_AddItemToObject(sendJson, "TB_RECORD", cJSON_Duplicate(record, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retInt = GetJsonItemInt(retJson, "RESULT");
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retInt;
}

//远程读表记录
int API_RemoteTableSelect(int ip, char* TB_name, cJSON* view, cJSON* where, int sort)
{
	cJSON* retJson = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "SELECT");
	cJSON_AddItemToObject(sendJson, "TB_VIEW", cJSON_Duplicate(view, 1));
	cJSON_AddItemToObject(sendJson, "TB_WHERE", cJSON_Duplicate(where, 1));
	cJSON_AddNumberToObject(sendJson, "TB_SORT", sort);
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}
	//printf("%s1111111111 taget=%d\n",__func__, ip);
	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		//printf("%s2222222222\n",__func__);
		cJSON* element;
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		cJSON* retView = cJSON_GetObjectItemCaseSensitive(retJson, "TB_VIEW");
		retInt = GetJsonItemInt(retJson, "RESULT");
		while(cJSON_GetArraySize(view))
		{
			cJSON_DeleteItemFromArray(view, 0);
		}
		cJSON_ArrayForEach(element, retView)
		{
			cJSON_AddItemToArray(view, cJSON_Duplicate(element, 1));
		}
		cJSON_Delete(retJson);
		//printf("%s33333333 ret=%d\n",__func__, retInt);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retInt;
}

//远程读取表KEY值
cJSON* API_RemoteTableKey(int ip, char* TB_name)
{
	cJSON* retJson = NULL;
	cJSON* retKey = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "KEY");
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retKey = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(retJson, "RESULT"), 1);
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retKey;
}

//远程读取表新记录
cJSON* API_RemoteTableNewRecord(int ip, char* TB_name)
{
	cJSON* retJson = NULL;
	cJSON* retRecord = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = 0;

	cJSON_AddStringToObject(sendJson, "TB_NAME", TB_name);
	cJSON_AddStringToObject(sendJson, "OPERATION", "NEW_RECORD");
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, TABLE_PROCESS_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		FreeUnicastOutData(dataOut);
		retRecord = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(retJson, "RESULT"), 1);
		cJSON_Delete(retJson);
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return retRecord;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

