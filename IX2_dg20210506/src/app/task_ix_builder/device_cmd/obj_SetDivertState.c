/**
  ******************************************************************************
  * @file    obj_SetDivertState.c
  * @author  czb
  * @version V00.01.00
  * @date    2023.3.13
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <string.h>
#include "cJSON.h"
#include "obj_PublicUnicastCmd.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_GetIpByNumber.h"
#include "task_IoServer.h"	

int SetDivertStateProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	int ret = -1;
	cJSON *tbProcess = cJSON_Parse(dataIn.data);
	if(tbProcess)
	{
		if(dataOut != NULL)
		{
			(*dataOut).data = cJSON_PrintUnformatted(tbProcess);
			(*dataOut).len = strlen((*dataOut).data) + 1;
		}
		#ifdef PID_IXSE
		//DivertStateCallback(tbProcess);
		#endif
		//cJSON_AddStringToObject(tbProcess, "EventName", "EventSetDivertState");
		//API_Event_Json(tbProcess);
		cJSON_Delete(tbProcess);
		ret = 0;
	}

	return ret;
}

/*
	source: 自己的房号
	target：发送的目标房号
	setDev：设置的房号
	onOffCheck：设置状态OFF-关闭，ON-开启，CHECK-查询
	message:信息说明
*/
int API_SetDivertState(char* source, char* target, char* setDev, char* onOffCheck, char* message)
{
	GetIpRspData data;
	int retInt = API_GetIpNumberFromNet(target, NULL, NULL, 2, 1, &data);
	if(retInt == 0)
	{
		char *pos1, *pos2;
		char display[20];
		char code[10]={0};
		sprintf(display, "<ID=%s>", AreaCode);//??IX2V????
		API_io_server_UDP_to_read_remote(data.Ip[0], 0xFFFFFFFF, display);
		pos1 = strstr(display, "Value=");
		if(pos1 != NULL)
		{
			pos2 = strchr(pos1, '>');
			if(pos2 != NULL)
			{
				int len;
				len = ((int)(pos2-pos1))-strlen("Value=");
				memcpy(code, pos1+strlen("Value="), len);
				code[len] = 0;
				//printf("AreaCode: %s\n",code);			
			}
		}
		if(strcmp(code, "IX2V")!=0)
		{
			return -1;
		}
		cJSON* sendJson = cJSON_CreateObject();
		PublicUnicastCmdData dataIn;
		cJSON_AddStringToObject(sendJson, "SOURCE", source);
		cJSON_AddStringToObject(sendJson, "TARGET", setDev);
		cJSON_AddStringToObject(sendJson, "STATE", onOffCheck);
		cJSON_AddStringToObject(sendJson, "MSG", message);
		dataIn.data = cJSON_PrintUnformatted(sendJson);
		cJSON_Delete(sendJson);

		if(dataIn.data)
		{
			dataIn.len = strlen(dataIn.data) + 1;
		}
		else
		{
			dataIn.len = 0;
		}

		PublicUnicastCmdRequst(data.Ip[0], NULL, BusinessWaitUdpTime, SET_DIVERT_STATE_REQ, dataIn, NULL);

		if(dataIn.data)
		{
			free(dataIn.data);
		}
	}
	return retInt;
}


#if 0
int GetQR_CodeProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	int ret = -1;
	cJSON *tbProcess = cJSON_Parse(dataIn.data);
	if(tbProcess)
	{
		if(dataOut != NULL)
		{
			char sip_acc[100];
			char sip_pwd[100];
			char* im_addr = GetEventItemString(tbProcess, IX2V_TARGET);

			if(get_sip_ix_im_acc_from_tb2(im_addr, sip_acc, sip_pwd) == 0)
			{
				(*dataOut).data = create_sip_phone_qrcode(sip_acc,sip_pwd);
				(*dataOut).len = strlen((*dataOut).data) + 1;
			}
			else
			{
				(*dataOut).data = NULL;
				(*dataOut).len = 0;
			}
		}
		cJSON_Delete(tbProcess);
		ret = 0;
	}

	return ret;
}
#endif

/*
	source: 自己的房号
	target：发送的目标房号
	getDev：获取二维码的房号
	返回二维码字符串：如无返回NULL；如非NULL,使用完后需要调用free()释放。
*/
char* API_GetQR_Code(char* source, char* target, char* getDev)
{
	GetIpRspData data;
	char* ret = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	if(API_GetIpNumberFromNet(target, NULL, NULL, 2, 1, &data) == 0)
	{
		cJSON_AddStringToObject(sendJson, "SOURCE", source);
		cJSON_AddStringToObject(sendJson, "TARGET", getDev);
		dataIn.data = cJSON_PrintUnformatted(sendJson);
		cJSON_Delete(sendJson);

		if(dataIn.data)
		{
			dataIn.len = strlen(dataIn.data) + 1;
		}
		else
		{
			dataIn.len = 0;
		}

		if(PublicUnicastCmdRequst(data.Ip[0], NULL, BusinessWaitUdpTime, GET_QR_CODE_REQ, dataIn, &dataOut) == 0)
		{
			if(dataOut.len==0)
				ret=NULL;
			else
				ret = dataOut.data;
		}

		if(dataIn.data)
		{
			free(dataIn.data);
		}
	}

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

