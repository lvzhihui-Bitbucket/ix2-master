/**
  ******************************************************************************
  * @file    obj_GetInfoByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetInfoByIp.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "task_IoServer.h"


static GetInfoRun getInfo[CLIENT_MAX_NUM] = {{.lock = PTHREAD_MUTEX_INITIALIZER}};

//发送获取信息指令
int API_GetInfoByIp(int ip, int time, DeviceInfo* info)
{
	GetInfoByIpReq sendData;
	int i, cnt;
	int waitFlag;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	
	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		pthread_mutex_lock(&getInfo[i].lock);
		waitFlag = getInfo[i].waitFlag;
		pthread_mutex_unlock(&getInfo[i].lock);
		if(waitFlag == 0)
		{
			break;
		}
	}

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	pthread_mutex_lock(&getInfo[i].lock);
	getInfo[i].waitFlag = 1;
	getInfo[i].rand = sendData.rand;
	waitFlag = getInfo[i].waitFlag;
	pthread_mutex_unlock(&getInfo[i].lock);
	

	api_udp_device_update_send_data(ip, htons(GET_INFO_BY_IP_REQ), (char*)&sendData, sizeof(sendData) );

	cnt = time*1000/UDP_WAIT_RSP_TIME;
	while(waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		pthread_mutex_lock(&getInfo[i].lock);
		waitFlag = getInfo[i].waitFlag;
		pthread_mutex_unlock(&getInfo[i].lock);
		if(--cnt == 0)
		{
			pthread_mutex_lock(&getInfo[i].lock);
			getInfo[i].waitFlag = 0;
			pthread_mutex_unlock(&getInfo[i].lock);
			return -3;
		}
	}

	pthread_mutex_lock(&getInfo[i].lock);
	memcpy(info, &getInfo[i].data, sizeof(DeviceInfo));
	getInfo[i].waitFlag = 0;
	pthread_mutex_unlock(&getInfo[i].lock);

	return 0;
}

//接收到获取信息应答指令
void ReceiveGetInfoRsp(int target_ip, GetInfoByIpRsp* rspData, int len)
{
	int i;
	int waitFlag;
	int rand;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		pthread_mutex_lock(&getInfo[i].lock);
		waitFlag = getInfo[i].waitFlag;
		pthread_mutex_unlock(&getInfo[i].lock);
		
		if(waitFlag)
		{
			pthread_mutex_lock(&getInfo[i].lock);
			rand = getInfo[i].rand;
			pthread_mutex_unlock(&getInfo[i].lock);
			if(rspData->rand == rand)
			{
				pthread_mutex_lock(&getInfo[i].lock);
				memset(&getInfo[i].data, 0, sizeof(DeviceInfo));

				len = (len < sizeof(DeviceInfo) ? sizeof(DeviceInfo) - sizeof(getInfo[i].data.monPwd) : sizeof(DeviceInfo));
				
				memcpy(&getInfo[i].data, &rspData->data, len);
								
				getInfo[i].waitFlag = 0;
				pthread_mutex_unlock(&getInfo[i].lock);
				break;
			}
		}
	}
}

void GetMyInfo(int ip, DeviceInfo* info)
{
	strcpy(info->MFG_SN, GetSysVerInfo_Sn());
	strcpy(info->BD_RM_MS, GetSysVerInfo_BdRmMs());
	strcpy(info->Local, GetSysVerInfo_LocalNum());
	strcpy(info->Global, GetSysVerInfo_GlobalNum());
	snprintf(info->name1, 21, "%s", GetSysVerInfo_name());
	snprintf(info->name2_utf8, 41, "%s", GetSysVerInfo_name());
	info->deviceType = GetSysVerInfo_MyDeviceType();
	Get_SipConfig_Account(info->localSip);
	Get_SipConfig_MonCode(info->monCode);
	GetMyVerDevicetype(info->Ver_Devicetype);	

	char temp[20];
	if(!strcmp(GetNetDeviceNameByTargetIp(ip), NET_WLAN0))
	{
		strcpy(info->IP_STATIC, "DHCP");
		strcpy(info->IP_ADDR, GetSysVerInfo_IP_by_device(NET_WLAN0));
		strcpy(info->IP_MASK, GetSysVerInfo_mask_by_device(NET_WLAN0));
		strcpy(info->IP_GATEWAY, GetSysVerInfo_gateway_by_device(NET_WLAN0));
	}
	else
	{
		API_Event_IoServer_InnerRead_All(DHCP_ENABLE, temp);
		strcpy(info->IP_STATIC, atoi(temp)? "DHCP" : "STATIC");
		strcpy(info->IP_ADDR, GetSysVerInfo_IP_by_device(NET_ETH0));
		strcpy(info->IP_MASK, GetSysVerInfo_mask_by_device(NET_ETH0));
		strcpy(info->IP_GATEWAY, GetSysVerInfo_gateway_by_device(NET_ETH0));
	}
}

//接收到Get info请求指令
void ReceiveGetInfoReq(int target_ip, GetInfoByIpReq* pReadReq)
{

	GetInfoByIpRsp rspData = {0};
	
	rspData.rand = pReadReq->rand;

	GetMyInfo(target_ip, &rspData.data);

	api_udp_device_update_send_data(target_ip, htons(GET_INFO_BY_IP_RSP), (char*)&rspData, sizeof(rspData) );
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

