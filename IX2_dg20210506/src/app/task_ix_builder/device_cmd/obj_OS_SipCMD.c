/**
  ******************************************************************************
  * @file    obj_OS_SipCMD.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include<ctype.h>
#include "obj_OS_SipCMD.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_GetIpByNumber.h"

static SipCfgRun sipCfgCmd[SIP_CONFIG_MAX_DEVICE];


int API_SipConfigCmd(int cmd, char* BD_RM_MS, SipCfgResult* cfgData, int timeOut)
{
	SipCfgCmdReq sendData[SIP_CONFIG_MAX_DEVICE];
	char roomNumber[SIP_CONFIG_MAX_DEVICE][11];
	int ip[SIP_CONFIG_MAX_DEVICE];
	char temp[11];
	GetIpRspData data = {0};
	int i, timeCnt, ret;
	
	ret = -1;
	
	API_GetIpNumberFromNet(BD_RM_MS, NULL, NULL, 1, SIP_CONFIG_MAX_DEVICE, &data);
	
	for(i = 0, cfgData->cnt = 0; i < data.cnt; i++)
	{
		memcpy(temp, data.BD_RM_MS[i], 10);
		temp[10] = 0;
		
		if(atoi(temp+8) < 50)
		{
			continue;
		}
		
		memcpy(roomNumber[cfgData->cnt], data.BD_RM_MS[i], 10);
		roomNumber[cfgData->cnt][10] = 0;
		ip[cfgData->cnt] = data.Ip[i];
		cfgData->cnt++;
	}
			
	for(i = 0; i < cfgData->cnt; i++)
	{
		sendData[i].rand = MyRand();
		sendData[i].sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip[i])));
		sendData[i].cmd = cmd;
		strcpy(sendData[i].BD_RM_MS, roomNumber[i]);
		strcpy(cfgData->device[i].BD_RM_MS, sendData[i].BD_RM_MS);

		cfgData->device[i].rand = sendData[i].rand;
		sipCfgCmd[i].waitFlag = 1;
		sipCfgCmd[i].rand = sendData[i].rand;
		api_udp_device_update_send_data(ip[i], htons(CMD_SIP_CONFIG_REQ), (char*)&sendData[i], sizeof(SipCfgCmdReq) );
	};
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(--timeCnt == 0)
		{
			ret = -1;
			break;
		}
		
		for(i = 0; i < cfgData->cnt; i++)
		{
			if((sipCfgCmd[i].waitFlag == 2) && (sipCfgCmd[i].rand == cfgData->device[i].rand))
			{
				sipCfgCmd[i].waitFlag = 0;

				cfgData->device[i].result = sipCfgCmd[i].result;
				cfgData->device[i].sourceIp= sipCfgCmd[i].sourceIp;
				strcpy(cfgData->device[i].BD_RM_MS, sipCfgCmd[i].BD_RM_MS);
			}
		}
		
		for(i = 0; i < cfgData->cnt; i++)
		{
			if(sipCfgCmd[i].waitFlag != 0)
			{
				ret = -1;
				break;
			}
		}

		if(i == cfgData->cnt)
		{
			ret = 0;
			break;
		}
	}

	return ret;
}

//接收到SIP命令处理应答指令
void ReceiveSipConfigCmdRsp(SipCfgCmdRsp* rspData)
{
	int i;

	for(i = 0; i < SIP_CONFIG_MAX_DEVICE; i++)
	{
		if(sipCfgCmd[i].waitFlag == 1)
		{
			if(rspData->rand == sipCfgCmd[i].rand)
			{
				sipCfgCmd[i].waitFlag = 2;
				sipCfgCmd[i].sourceIp = rspData->sourceIp;
				sipCfgCmd[i].result = rspData->result;
				strcpy(sipCfgCmd[i].BD_RM_MS, rspData->BD_RM_MS);
			}
		}
	}
}


//接收到SIP命令处理请求指令
void ReceiveSipConfigCmdReq(int target_ip, SipCfgCmdReq* pReadReq)
{
	SYS_VER_INFO_T	sysVerInfoData;
	int myIp, check;
	int len;
	int result;

	check = 0;
	sysVerInfoData = GetSysVerInfo();
	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
	//自己发出的自己不回应
	if(pReadReq->sourceIp == myIp)
	{
		return;
	}

	len = strlen(pReadReq->BD_RM_MS);
	//八位房号
	if(len == 8)
	{
		if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 8))
		{
			check = 1;
		}
	}
	//十位房号
	else if(len == 10)
	{
		if(pReadReq->BD_RM_MS[8] == '0' && pReadReq->BD_RM_MS[9] == '0')
		{
			if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 8))
			{
				check = 1;
			}
		}
		else
		{
			if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 10))
			{
				check = 1;
			}
		}
	}

		
	if(check)
	{
		if(pReadReq->cmd == 1)
		{
			result = SipConfigUseDefault();
		}
		else
		{
			result = SipConfigManualRegistration();
		}
		API_SipConfigCmdRsp(target_ip, pReadReq->rand, myIp, sysVerInfoData.bd_rm_ms, result);
	}

}

//发送搜索应答指令
void API_SipConfigCmdRsp(int targetIp, int rand, int myIp, char* myBD_RM_MS, int result)
{
	SipCfgCmdRsp rspData;

	rspData.rand = rand;
	rspData.sourceIp = myIp;
	strcpy(rspData.BD_RM_MS, myBD_RM_MS);
	rspData.result = result;
	
	api_udp_device_update_send_data(targetIp, htons(CMD_SIP_CONFIG_RSP), (char*)&rspData, sizeof(SipCfgCmdRsp) );
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

