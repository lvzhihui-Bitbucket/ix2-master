/**
  ******************************************************************************
  * @file    obj_CallBtnBinding.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CallBtnBinding.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_IpCacheTable.h"
#include "task_VideoMenu.h"

static int callBtnBindingState;
static GetCallBtnBindingStateRun getCallBtnBindingStateRun[CLIENT_MAX_NUM];
static CallBtnBindingRun callBtnBindingRun[CLIENT_MAX_NUM];

int GetCallBtnBingdingState(void)
{
	return callBtnBindingState;
}

void SetCallBtnBingdingState(int state)
{
	callBtnBindingState = state;
}

//发送GetCallBtnBindingState指令
// paras:
// time: 等待时间
// return:
//  1:有设备在绑定状态, 0:没有设备在绑定状态，-1:忙
int API_GetCallBtnBindingState(int time)
{
	GetCallBtnBindingStateReq_t sendData;
	int i, timeCnt;
	int ret;

	for(i = 0; i < CLIENT_MAX_NUM && getCallBtnBindingStateRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();

	rejoin_multicast_group();

	int ixDeviceSelectNetwork;
	ixDeviceSelectNetwork = IX_DeviceSelectNetwork();
	if(ixDeviceSelectNetwork & 0x01)
	{
		sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		api_udp_device_update_send_data_by_device(NET_WLAN0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_GetCallBtnBindingState_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	if(ixDeviceSelectNetwork & 0x02)
	{
		sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_GetCallBtnBindingState_REQ), (char*)&sendData, sizeof(sendData) );
	}

	getCallBtnBindingStateRun[i].waitFlag = 1;
	getCallBtnBindingStateRun[i].rand = sendData.rand;
	getCallBtnBindingStateRun[i].state = callBtnBindingState;
	
	
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(getCallBtnBindingStateRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			getCallBtnBindingStateRun[i].waitFlag = 0;
			return 0;
		}
	}
		
	return getCallBtnBindingStateRun[i].state;
}

//接收到搜索应答指令
void GetCallBtnBindingStateRsp(GetCallBtnBindingStateRsp_t* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(getCallBtnBindingStateRun[i].waitFlag)
		{
			if(rspData->rand == getCallBtnBindingStateRun[i].rand)
			{
				getCallBtnBindingStateRun[i].state = rspData->state;
				getCallBtnBindingStateRun[i].waitFlag = 0;
			}
		}
	}
}


//接收到By MFG_SN搜索指令
void GetCallBtnBindingStateReq(int target_ip, GetCallBtnBindingStateReq_t* pReadReq)
{
	//网络没初始化好，不应答远程指令
	if(!GetNetworkIsStarted())
	{
		return;
	}
	
	if(callBtnBindingState)
	{
		GetCallBtnBindingStateRsp_t rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip));
		rspData.state = callBtnBindingState;
		
		api_udp_device_update_send_data(target_ip, htons(GET_GetCallBtnBindingState_RSP), (char*)&rspData, sizeof(rspData) );
	}
}

//发送CallBtnBinding指令
// paras:
// time: 等待时间
// return:
// 0:没有设备在绑定状态，-1:忙 ，-2:绑定失败
int API_CallBtnBinding(int time, char* BD_RM_MS, char* CallNbr)
{
	CallBtnBindingReq_t sendData;
	int i, timeCnt;
	int ret;
	
	for(i = 0; i < CLIENT_MAX_NUM && callBtnBindingRun[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();
	strcpy(sendData.BD_RM_MS, GetSysVerInfo_BdRmMs());
	
	rejoin_multicast_group();
	
	int ixDeviceSelectNetwork = IX_DeviceSelectNetwork();
	
	if(ixDeviceSelectNetwork & 0x02)
	{
		sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CallBtnBinding_REQ), (char*)&sendData, sizeof(sendData) );
	}
	
	if(ixDeviceSelectNetwork & 0x01)
	{
		sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		api_udp_device_update_send_data_by_device(NET_WLAN0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(CallBtnBinding_REQ), (char*)&sendData, sizeof(sendData) );
	}	
	callBtnBindingRun[i].waitFlag = 1;
	callBtnBindingRun[i].rand = sendData.rand;	
	callBtnBindingRun[i].BD_RM_MS[0] = 0;
	callBtnBindingRun[i].CallNbr[0] = 0;
	
	ret = 0;
	timeCnt = time*1000/UDP_WAIT_RSP_TIME;
	while(callBtnBindingRun[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--timeCnt <= 0)
		{
			callBtnBindingRun[i].waitFlag = 0;
			ret = -2;
			break;
		}
	}

	if(BD_RM_MS != NULL)
	{
		strcpy(BD_RM_MS, callBtnBindingRun[i].BD_RM_MS);
	}

	if(CallNbr != NULL)
	{
		strcpy(CallNbr, callBtnBindingRun[i].CallNbr);
	}
		
	return ret;
}

//接收到搜索应答指令
void CallBtnBindingRsp(CallBtnBindingRsp_t* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(callBtnBindingRun[i].waitFlag)
		{
			if(rspData->rand == callBtnBindingRun[i].rand)
			{
				strcpy(callBtnBindingRun[i].BD_RM_MS, rspData->BD_RM_MS);
				strcpy(callBtnBindingRun[i].CallNbr, rspData->CallNbr);
				callBtnBindingRun[i].waitFlag = 0;
			}
		}
	}
}


//接收到By MFG_SN搜索指令
void CallBtnBindingReq(int target_ip, CallBtnBindingReq_t* pReadReq)
{
	//网络没初始化好，不应答远程指令
	if(!GetNetworkIsStarted())
	{
		return;
	}
	
	if(callBtnBindingState)
	{
		CallBtnBindingRsp_t rspData;
		//printf("CallBtnBindingReq111111111111\n");
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip));
		strcpy(rspData.CallNbr, GetSysVerInfo_BdRmMs());
		strcpy(rspData.CallNbr+8, "00");
		
		strcpy(rspData.BD_RM_MS, GetSysVerInfo_BdRmMs());
		//printf("CallBtnBindingReq22222222222222\n");
		sprintf(rspData.BD_RM_MS+8, "%02d", GetFreeOSNumber());
		//printf("CallBtnBindingReq333333333333333\n");
		api_udp_device_update_send_data(target_ip, htons(CallBtnBinding_RSP), (char*)&rspData, sizeof(rspData) );
		//printf("CallBtnBindingReq44444444444444\n");
		API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_OSBinding);
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

