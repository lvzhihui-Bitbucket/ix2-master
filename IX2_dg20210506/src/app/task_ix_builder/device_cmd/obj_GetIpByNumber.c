/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetIpByNumber.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_SearchIpByFilter.h"
#include "obj_IoInterface.h"
#include "define_string.h"
#include "define_file.h"

static GetIpRsp getIpFromNet[CLIENT_MAX_NUM]={0};

int MyRand(void)
{	
	int temp;
	char temp1[6];
	static int init_flag=0;
	if(init_flag==0)
	{
		API_GetLocalSn(temp1);
		memcpy(&temp, &temp1[2], 4);
		srand(temp+time(NULL));
		init_flag=1;
	}
	return rand();
}

int MyRand2(int seed)
{
	srand(seed);
	return rand();
}

//发送By number搜索指令
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : 超时时间 单位S
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
int API_GetIpNumberFromNet(const char* bdRmMs, const char* input, const char* bd, int time, int getDeviceCnt, GetIpRspData* data)
{
	GetIpByNumberReq sendData;
 	int i;
	char temp[5];
	
	if(input == NULL && bd == NULL && bdRmMs == NULL)
	{
		return -2;
	}

	for(i = 0; i < CLIENT_MAX_NUM && getIpFromNet[i].waitFlag; i++);
	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}
	printf("11111111111API_GetIpNumberFromNet:%s:%s\n",bdRmMs,input);
	getIpFromNet[i].waitFlag = 1;
	getIpFromNet[i].rand = MyRand();
	getIpFromNet[i].data = data;
	getIpFromNet[i].data->cnt = 0;
	getIpFromNet[i].searchTimeCnt = 0;
	getIpFromNet[i].searchRspFlag = 0;	
	getIpFromNet[i].searchTimeout = GetSearch1Timeout();
	getIpFromNet[i].searchIntervalTime = GetSearch1IntervalTime();
	getIpFromNet[i].searchAllTimeCnt=0;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = getIpFromNet[i].rand;

	strcpy(sendData.BD_RM_MS, bdRmMs != NULL ? bdRmMs : "");
	strcpy(sendData.input, input != NULL ? input : "");
	strcpy(sendData.my_BD, bd != NULL ? bd : "");
	
	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_IP_BY_NUMBER_REQ), (char*)&sendData, sizeof(sendData) );
	//api_udp_device_update_send_data_by_device("eth0",inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_IP_BY_NUMBER_REQ), (char*)&sendData, sizeof(sendData) );
	while(getIpFromNet[i].waitFlag)
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		//printf("11111111111API_GetIpNumberFromNet:%s:%s\n",bdRmMs,input);
		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && getIpFromNet[i].data->cnt >= getDeviceCnt)
		{
			getIpFromNet[i].waitFlag = 0;
		}

		//搜索总时间超时
		getIpFromNet[i].searchAllTimeCnt++;
		if(getIpFromNet[i].searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
		{
			getIpFromNet[i].waitFlag = 0;
		}

		getIpFromNet[i].searchTimeCnt++;
		#if 1
		if(getIpFromNet[i].searchRspFlag)
		{
			//搜索有应答，但是规定时间内没有设备增加，搜索结束
			//if(getIpFromNet[i].searchTimeCnt >= getIpFromNet[i].searchIntervalTime/UDP_SEARCH_TIME_UNIT)
			if(getIpFromNet[i].searchTimeCnt >= 300/UDP_SEARCH_TIME_UNIT)
			{
				getIpFromNet[i].waitFlag = 0;
			}
		}
		else
		{
			//搜索没有应答，搜索超时结束
			//if(getIpFromNet[i].searchTimeCnt >= getIpFromNet[i].searchTimeout/UDP_SEARCH_TIME_UNIT)
			if(getIpFromNet[i].searchTimeCnt >= 1*1000/UDP_SEARCH_TIME_UNIT)	
			{
				getIpFromNet[i].waitFlag = 0;
			}

		}
		#endif
				
	}
	
	return (getIpFromNet[i].data->cnt != 0 ? 0 : -3);
}

//接收到搜索应答指令
void ReceiveGetIpByNumberFromNetRsp(int target_ip, GetIpByNumberRsp* rspData)
{
	int i, j;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(getIpFromNet[i].waitFlag)
		{
			if(rspData->rand == getIpFromNet[i].rand)
			{
				for(j = 0; j < getIpFromNet[i].data->cnt; j++)
				{
					if(getIpFromNet[i].data->Ip[j] == rspData->sourceIp)
					{
						dprintf("rspData->sourceIp = %s\n", my_inet_ntoa2(rspData->sourceIp));
						break;
					}
				}

				if(j == getIpFromNet[i].data->cnt)
				{
					getIpFromNet[i].searchRspFlag = 1;
					getIpFromNet[i].searchTimeCnt = 0;
					//搜素设备数量还没达到上限
					
					if(getIpFromNet[i].data->cnt < SEARCH_NUMBER_MAX_DEVICE)
					{
						getIpFromNet[i].data->Ip[getIpFromNet[i].data->cnt] = rspData->sourceIp;
						getIpFromNet[i].data->deviceType[getIpFromNet[i].data->cnt] = rspData->deviceType;
						memcpy(getIpFromNet[i].data->BD_RM_MS[getIpFromNet[i].data->cnt], rspData->BD_RM_MS, 10);
						getIpFromNet[i].data->BD_RM_MS[getIpFromNet[i].data->cnt][10] = 0;
						getIpFromNet[i].data->cnt++;
					}
				}
			}
		}
	}
}


//接收到By NUMBER搜索指令
void ReceiveGetIpByNumberFromNetReq(int target_ip, GetIpByNumberReq* pReadReq)
{
	int myIp, check;
	int len;

	check = 0;
	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));

	//自己发出的自己不回应
	if(pReadReq->sourceIp == myIp)
	{
		return;
	}

	len = strlen(pReadReq->BD_RM_MS);
	//八位房号
	if(len == 8)
	{
		if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 8))
		{
			check = 1;
		}
	}
	//十位房号
	else if(len == 10)
	{
		if(pReadReq->BD_RM_MS[8] == '0' && pReadReq->BD_RM_MS[9] == '0')
		{
			if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 8))
			{
				check = 1;
			}
		}
		else
		{
			if(!memcmp(GetSysVerInfo_BdRmMs(), pReadReq->BD_RM_MS, 10))
			{
				check = 1;
			}
		}
	}

	
	//匹配Global和Local number
	if(check == 0)
	{
		int i;
		char inputUpper[20];
		char globalUpper[20];
		char localUpper[20];
		char bdUpper[20];
		char reqBdUpper[20];
		char tempNum[20];

		
		for(i = 0, memset(inputUpper, 0, 20); pReadReq->input[i] != 0; i++)
		{
			inputUpper[i] = toupper(pReadReq->input[i]);
		}
		
		for(i = 0, memset(reqBdUpper, 0, 20); pReadReq->my_BD[i] != 0 && i < 4; i++)
		{
			reqBdUpper[i] = toupper(pReadReq->my_BD[i]);
		}

		strcpy(tempNum, GetSysVerInfo_GlobalNum());
		for(i = 0, memset(globalUpper, 0, 20); tempNum[i] != 0; i++)
		{
			globalUpper[i] = toupper(tempNum[i]);
		}
		
		strcpy(tempNum, GetSysVerInfo_LocalNum());
		for(i = 0, memset(localUpper, 0, 20); tempNum[i] != 0; i++)
		{
			localUpper[i] = toupper(tempNum[i]);
		}
		
		strcpy(tempNum, GetSysVerInfo_bd());
		for(i = 0, memset(bdUpper, 0, 20); tempNum[i] != 0; i++)
		{
			bdUpper[i] = toupper(tempNum[i]);
		}
		
		if(pReadReq->input[0] == 0)
		{
			check = 0;
		}	
		else if(!strcmp(globalUpper, inputUpper))
		{
			check = 1;
		}
		else if(!strcmp(localUpper, inputUpper) && !strcmp(bdUpper, reqBdUpper))
		{
			check = 1;
		}
	}
	
	if(check)
	{
		GetIpByNumberRsp rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.sourceIp = myIp;
		rspData.deviceType = GetSysVerInfo_MyDeviceType();
		memcpy(rspData.BD_RM_MS, GetSysVerInfo_BdRmMs(), 10);
		
		api_udp_device_update_send_data(target_ip, htons(GET_IP_BY_NUMBER_RSP), (char*)&rspData, sizeof(rspData) );
	}

}
static void SaveSearchByNumberDeviceTable(const cJSON* resultTable, GetIpRspData rspData)
{
	if(cJSON_IsArray(resultTable))
	{
		while (cJSON_GetArraySize(resultTable))
		{
			cJSON_DeleteItemFromArray(resultTable, 0);
		}
		
		for (size_t i = 0; i < rspData.cnt && i < SEARCH_NUMBER_MAX_DEVICE; i++)
		{
			cJSON* record = cJSON_CreateObject();
			cJSON_AddItemToArray(resultTable, record);
			cJSON_AddStringToObject(record, IX2V_IP_ADDR, my_inet_ntoa2(rspData.Ip[i]));
			cJSON_AddStringToObject(record, IX2V_IX_ADDR, rspData.BD_RM_MS[i]);
			cJSON_AddStringToObject(record, IX2V_IX_TYPE, DeviceTypeToString(rspData.deviceType[i]));
		}
	}
}

static void SaveSearchByNumberResult(const char* net, const char* deviceNbr, const char* input, GetIpRsp result)
{
	char* resultRecordEnable = API_Para_Read_String2(Search_Record);

	if(resultRecordEnable && strcmp(resultRecordEnable, "Ignore"))
	{
		if((!strcmp(resultRecordEnable, "New")) || (!strcmp(resultRecordEnable, "AddUp")))
		{
			cJSON* recordTable;
			cJSON* resultRecord = cJSON_CreateObject();

			cJSON_AddStringToObject(resultRecord, "Time", get_time_string());
			cJSON_AddStringToObject(resultRecord, "Net", ((net != NULL) && (!strcmp(net, "wlan0"))) ? "wlan0" : "eth0");
			cJSON_AddStringToObject(resultRecord, "DeviceNbr", (deviceNbr != NULL) ? deviceNbr : "");
			cJSON_AddStringToObject(resultRecord, "Input", (input != NULL) ? input : "");

			cJSON_AddNumberToObject(resultRecord, "SearchAllTime", result.searchAllTimeCnt*UDP_SEARCH_TIME_UNIT);
			cJSON_AddNumberToObject(resultRecord, "SearchAllCnt", result.data->cnt);

			if(!strcmp(resultRecordEnable, "New"))
			{
				recordTable = cJSON_CreateArray();
			}
			else
			{
				recordTable = GetJsonFromFile(SearchResult_FILE);
				if(cJSON_IsArray(recordTable))
				{
					while (cJSON_GetArraySize(recordTable) >= 100)
					{
						cJSON_DeleteItemFromArray(recordTable, 0);
					}
				}
				else
				{
					cJSON_Delete(recordTable);
					recordTable = cJSON_CreateArray();
				}
			}

			cJSON_AddItemToArray(recordTable, resultRecord);


			SetJsonToFile(SearchResult_FILE, recordTable);

			cJSON_Delete(recordTable);
		}
	}
}


//发送By number搜索指令
// paras:
// net:网卡名字
// bd_rm_ms: bd_rm_ms
// input : input
// resultTable : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, 0:没有查找到, >0:查找完成
int API_JsonGetIpByNumber(const char* net, cJSON* searchCtrl, const char* bdRmMs, const char* input, const cJSON* resultTable)
{
	#define SearchDeviceTime		3		//最长搜索时间3秒
	#define SearchDeviceMaxCnt		0		//最多搜索设备数量
	int getDeviceCnt = SearchDeviceMaxCnt;
	int timeCnt = SearchDeviceTime*1000/UDP_SEARCH_TIME_UNIT;

	GetIpByNumberReq sendData = {0};
	GetIpRspData rspData;
 	int i;
	char temp[5];
	
	for(i = 0; i < CLIENT_MAX_NUM && getIpFromNet[i].waitFlag; i++);
	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}

	getIpFromNet[i].waitFlag = 1;
	getIpFromNet[i].rand = MyRand();
	getIpFromNet[i].data = &rspData;
	getIpFromNet[i].data->cnt = 0;
	getIpFromNet[i].searchTimeCnt = 0;
	getIpFromNet[i].searchRspFlag = 0;	

	if(net && !strcmp(net, "wlan0"))
	{
		GetJsonDataOrIoPro(searchCtrl, Wlan_S1_Timeout, &getIpFromNet[i].searchTimeout);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S1_IntervalTime, &getIpFromNet[i].searchIntervalTime);
		sendData.sourceIp = GetLocalIpByDevice(NET_WLAN0);
	}
	else
	{
		GetJsonDataOrIoPro(searchCtrl, Lan_S1_Timeout, &getIpFromNet[i].searchTimeout);
		GetJsonDataOrIoPro(searchCtrl, Lan_S1_IntervalTime, &getIpFromNet[i].searchIntervalTime);
		sendData.sourceIp = GetLocalIpByDevice(NET_ETH0);
	}

	dprintf("getIpFromNet[i].searchTimeout=%d, getIpFromNet[i].searchIntervalTime=%d\n", getIpFromNet[i].searchTimeout, getIpFromNet[i].searchIntervalTime);
	dprintf("sendData.sourceIp=%s\n", my_inet_ntoa2(sendData.sourceIp));

	getIpFromNet[i].searchAllTimeCnt = 0;

	sendData.rand = getIpFromNet[i].rand;

	strcpy(sendData.BD_RM_MS, bdRmMs != NULL ? bdRmMs : "");
	strcpy(sendData.input, input != NULL ? input : "");
	strcpy(sendData.my_BD, GetSysVerInfo_bd());
	
	api_udp_device_update_send_data_by_device(net, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(GET_IP_BY_NUMBER_REQ), (char*)&sendData, sizeof(sendData) );
	while(getIpFromNet[i].waitFlag)
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && getIpFromNet[i].data->cnt >= getDeviceCnt)
		{
			getIpFromNet[i].waitFlag = 0;
		}

		//搜索总时间超时
		getIpFromNet[i].searchAllTimeCnt++;
		if(getIpFromNet[i].searchAllTimeCnt >= timeCnt)
		{
			getIpFromNet[i].waitFlag = 0;
		}

		getIpFromNet[i].searchTimeCnt++;
		if(getIpFromNet[i].searchRspFlag)
		{
			//搜索有应答，但是规定时间内没有设备增加，搜索结束
			if(getIpFromNet[i].searchTimeCnt >= getIpFromNet[i].searchIntervalTime/UDP_SEARCH_TIME_UNIT)
			{
				getIpFromNet[i].waitFlag = 0;
			}
		}
		else
		{
			//搜索没有应答，搜索超时结束
			if(getIpFromNet[i].searchTimeCnt >= getIpFromNet[i].searchTimeout/UDP_SEARCH_TIME_UNIT)
			{
				getIpFromNet[i].waitFlag = 0;
			}
		}
	}

	SaveSearchByNumberDeviceTable(resultTable, rspData);
	SaveSearchByNumberResult(net, bdRmMs, input, getIpFromNet[i]);

	return getIpFromNet[i].data->cnt;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

