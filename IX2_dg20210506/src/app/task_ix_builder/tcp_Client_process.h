
#ifndef _tcp_Client_process_H_
#define _tcp_Client_process_H_

#define IX_DEVICE_REMOTE_PROXY_ONLINE_REQ			0x1002
#define IX_DEVICE_REMOTE_PROXY_ONLINE_RSP			0x1003
#define IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_REQ		0x1006
#define IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_RSP		0x1007
#define IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT		0x1008

#define MFG_SN_CLIENT_REQ							0x0001
#define MFG_SN_SERVER_RSP							0x0002
#define MFG_SN_CLIENT_ACK							0x0003

#define IX_DEVICE_REMOTE_TCP_BUFF_MAX				1200
#define IX_DEVICE_REMOTE_TCP_PORT					8860

#define IX_DEVICE_REMOTE_CMD_HEAD					"VTEK"

#define IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS			100

#define IX_DEVICE_REMOTE_SEND_BEAT_HEART_TIME_S		10
#define IX_DEVICE_REMOTE_SEND_RECV_HEART_TIME_S		35

// client service
typedef struct
{	
	int						fd;
	char					ip[16];
	int						port;
	char					id[32];
	char					pwd[16];
	one_tiny_task_t 		task;
} client_tcp_ins, *p_client_tcp_ins;

typedef struct
{	
	p_client_tcp_ins 	client;
	short 				cmd_id;
	short 				beatHeartTimeing;
	int  				autoConnectEnable;
	pthread_mutex_t		lock;
} IxBuilderRemoteRun;


#pragma pack(2) 

typedef struct 
{	
	char	head[4];	
	short	cmd_type;	
	short	cmd_id;
	short	dev_type;	
	short	data_len;	
}ProxyRemoteHead;

typedef struct 
{	
	ProxyRemoteHead		head;	
	int					ID;	
	short				RESERVE;	
}ProxyRemoteBeatHeart;

typedef struct 
{	
	ProxyRemoteHead		head;
	short				online;
	char				proxyId[32];	
	char				proxyPwd[16];	
	short				reserve;
}ProxyRemoteOnlineReq;

typedef struct 
{	
	ProxyRemoteHead		head;
	short				online;
	char				proxyId[32];	
	char				proxyPwd[16];	
	short				reserve;
	short				result;
}ProxyRemoteOnlineRsp;

typedef struct 
{	
	ProxyRemoteHead		head;
	short				businessCode;
}ProxyRemoteTransmitRsp;

typedef struct 
{	
	ProxyRemoteHead		head;
	char				sn[6];
	short				result;
}MFG_SN_REQ;

#pragma pack()


static int tcp_client_connect_with_timeout(char* net_dev_name, char* ip_str, int port, int t_timeout, int* psockfd );
static int Api_tcp_client_connect_to_server(char* net_dev_name, char *ip_str, int port, char* id ,char* pwd);
static int Api_tcp_client_disconnect(int* fd);
static int Api_tcp_client_send(int fd, const char *dat, int dat_len);

static int ProxySendBeatHeart(int fd, int id);
static int ProxyOnlineReq(int fd, short online, char* id, char* pwd);

static void ProxyRemoteCmdProcess(ProxyRemoteHead packHead, char* data, int dat_len);

static int client_socket_task_init(void* arg);
static void client_socket_task_process( void* arg );
static void client_socket_task_exit( void* arg );

static p_client_tcp_ins ProxyConnectToServer(char* net_dev_name, char *ip_str, int port, char* id, char* pwd);
static int ProxyDisconnectToServer(p_client_tcp_ins ptr_ins);

int ProxyRemoteRspData(int fd, short cmd_id, short businessCode, char* data, int len);
int ProxyRemoteTransferData(int fd, short businessCode, char* data, int len);
void IxBuilderTcpClientConnect(void);
void IxBuilderTcpClientDisconnect(void);

#endif

