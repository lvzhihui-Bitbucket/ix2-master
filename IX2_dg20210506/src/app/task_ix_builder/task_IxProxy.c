/**
  ******************************************************************************
  * @file    task_IxProxy.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ix_proxy
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include "cJSON.h"
#include "onvif_tiny_task.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "obj_CmdGetAbout.h"
#include "obj_CmdReboot.h"
#include "obj_CmdParaRW.h"
#include "obj_CmdIpCheck.h"
#include "obj_CmdGetInfo.h"
#include "obj_CmdRing.h"
#include "obj_CmdProg.h"
#include "obj_CmdR8012.h"
#include "obj_CmdProgIpAddr.h"
#include "obj_CmdResDownload.h"
#include "obj_CmdGetVersion.h"
#include "obj_CmdGetMergeInfo.h"
#include "obj_CmdSettingFromR8001.h"
#include "obj_CmdBackup.h"
#include "obj_CmdRestore.h"


Loop_vdp_common_buffer	vdp_ix_proxy_mesg_queue;
Loop_vdp_common_buffer	vdp_ix_proxy_sync_queue;
vdp_task_t				task_ix_proxy;
static IxProxyData_T 	myProxyData;
static IxProxyRUN_T 	ixProxyRun = {PTHREAD_MUTEX_INITIALIZER, IX_PROXY_IDLE, 0, 0, 0, 0};

void SetIxProxyState(IX_PROXY_STATE state)
{
	pthread_mutex_lock(&ixProxyRun.lock);
	ixProxyRun.state = state;
	pthread_mutex_unlock(&ixProxyRun.lock);
}

void SetIxProxyRebootFlag(int flag)
{
	pthread_mutex_lock(&ixProxyRun.lock);
	ixProxyRun.rebootFlag = flag;
	pthread_mutex_unlock(&ixProxyRun.lock);
}
int GetIxProxyRebootFlag(void)
{
	return ixProxyRun.rebootFlag;
}

IX_PROXY_STATE GetIxProxyState(void)
{
	IX_PROXY_STATE state;
	pthread_mutex_lock(&ixProxyRun.lock);
	state = ixProxyRun.state;
	pthread_mutex_unlock(&ixProxyRun.lock);
	return state;
}

void SetIxProxyRSP_ID(int RSP_ID)
{
	ixProxyRun.RSP_ID = RSP_ID;
}

int IncreaseIxProxyRSP_ID(void)
{
	return ixProxyRun.RSP_ID++;
}

int GetIxProxyRSP_ID(void)
{
	return ixProxyRun.RSP_ID;
}
void vtk_TaskInit_IxProxy(int priority)
{
	init_vdp_common_queue(&vdp_ix_proxy_mesg_queue, 5000, vdp_ix_proxy_mesg_data_process, &task_ix_proxy);
	init_vdp_common_queue(&vdp_ix_proxy_sync_queue, 2000, NULL, &task_ix_proxy);
	init_vdp_common_task(&task_ix_proxy, MSG_ID_IX_PROXY, vdp_ix_proxy_task, &vdp_ix_proxy_mesg_queue, &vdp_ix_proxy_sync_queue);
	
	SetMyProxyData("1.0.0", "9", "0", NULL);
	SetIxProxyState(IX_PROXY_IDLE);
	MakeIxDeviceFileDir();
}

void exit_vdp_ix_proxy_task(void)
{
	exit_vdp_common_queue(&vdp_ix_proxy_mesg_queue);
	exit_vdp_common_queue(&vdp_ix_proxy_sync_queue);
	exit_vdp_common_task(&task_ix_proxy);	
}


IxProxyData_T GetMyProxyData(void)
{
	snprintf(myProxyData.CacheNbr, IX_BUILDER_NAME_LEN, "%d", GetR8012CacheTableNbr());
	strncpy(myProxyData.CacheTime, GetR8012CacheTableTime(), IX_BUILDER_NAME_LEN);

	return myProxyData;
}

int IsMyProxyLinked(void)
{
	return strcmp(myProxyData.linked, "0") ? 1 : 0;
}

const char*  GetMyProxyLinked(void)
{
	return myProxyData.linked;
}


const char* GetMyProxy_IP(void)
{
	return myProxyData.ip_addr;
}

void SetMyProxyData(char* version, char* priority, char* linked, char* netCard)
{
	SYS_VER_INFO_T info = GetSysVerInfo();
	char* deviceType;
	char* pIpChar;
	
	if(version)
	{
		strncpy(myProxyData.version, version, IX_BUILDER_NAME_LEN);
	}
	
	if(priority)
	{
		strncpy(myProxyData.priority, priority, IX_BUILDER_NAME_LEN);
	}
	
	if(linked)
	{
		strncpy(myProxyData.linked, linked, IX_BUILDER_NAME_LEN*2);
	}

	if((pIpChar = strchr(myProxyData.linked, '@')) != NULL)
	{
		strncpy(myProxyData.ip_addr, GetSysVerInfo_IP_by_device(netCard), IX_BUILDER_NAME_LEN);
	}
	else
	{
		myProxyData.ip_addr[0] = 0;
	}
	
	strncpy(myProxyData.device_addr, info.bd_rm_ms, IX_BUILDER_NAME_LEN);
	deviceType = DeviceTypeToString(info.myDeviceType);
	if(deviceType == NULL)
	{
		strncpy(myProxyData.deviceType, "", IX_BUILDER_NAME_LEN);
	}
	else
	{
		strncpy(myProxyData.deviceType, deviceType, IX_BUILDER_NAME_LEN);
	}
	strncpy(myProxyData.MFG_SN, info.sn, IX_BUILDER_NAME_LEN);
	strncpy(myProxyData.name, info.myName, IX_BUILDER_NAME_LEN);
	strncpy(myProxyData.name2, "", IX_BUILDER_NAME_LEN*2);

	if(linked != NULL && !strcmp(linked, "0") && GetIxProxyRebootFlag())
	{
		HardwareRestar();
	}
}

static void* vdp_ix_proxy_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

static void vdp_ix_proxy_mesg_data_process(char* msg_data,int len)
{
	IX_PROXY_MSG* pMsg = (IX_PROXY_MSG*)msg_data;
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pMsg->data;

	SetIxProxyState(IX_PROXY_BUSY);

	ixProxyRun.businessCode = pMsg->businessCode;
	ixProxyRun.cmd_id = pMsg->cmd_id;
	
	//dprintf("businessCode = %d, cmd_id = %d, phead->OP = 0x%04X\n", ixProxyRun.businessCode, ixProxyRun.cmd_id, ntohs(phead->OP));

	switch (ntohs(phead->OP))
	{
		case REQ_DM_REBOOT:
			IxProxyRebootProcess(pMsg->data, len);
			break;

		case REQ_DM_ABOUT:
			IxProxyGetAboutProcess(pMsg->data, len);
			break;
		case REQ_DM_INFO:
			IxProxyGetInfoProcess(pMsg->data, len);
			break;
		case REQ_DM_INFO2:
			IxProxyGetInfo2Process(pMsg->data, len);
			break;
		case REQ_DM_R8012:
			IxProxyGetR8012Process(pMsg->data, len);
			break;
		case REQ_DM_PARA_READ:
			IxProxyParameterReadProcess(pMsg->data, len);
			break;
		case REQ_DM_PARA_WRITE:
			IxProxyParameterWriteProcess(pMsg->data, len);
			break;
		case REQ_DM_PARA_DEFAULT:
			IxProxyParameterDefaultProcess(pMsg->data, len);
			break;
		case REQ_IP_CHECK:
			IxProxyIpCheckProcess(pMsg->data, len);
			break;

		case REQ_DM_RING:
			IxProxyRingProcess(pMsg->data, len);
			break;
			
		case REQ_DM_PROG_CALL_NBR:
			IxProxyProgProcess(pMsg->data, len);
			break;
			
		case REQ_DM_R8012_SCAN_NEW:
			IxProxyR8012ScanNewProcess(pMsg->data, len);
			break;
		case REQ_DM_R8012_GET_CACHE:
			IxProxyR8012GetCacheTableProcess(pMsg->data, len);
			break;
		case REQ_DM_R8012_CHECK:
			IxProxyR8012CheckProcess(pMsg->data, len);
			break;
		case REQ_DM_GET_INFO_BY_MFG_SN:
			IxProxyGetInfoBySnProcess(pMsg->data, len);
			break;
		case REQ_DM_PROG_IP_ADDR:
			IxProxyProgIpAddrProcess(pMsg->data, len);
			break;
		case REQ_DM_RES_DOWNLOAD:
			IxProxyPCDownloadResProcess(pMsg->data, len);
			break;
		case REQ_DM_GET_VERSION:
			IxProxyGetVersionProcess(pMsg->data, len);
			break;
		case REQ_DM_GET_MERGE_INFO:
			IxProxyGetMergeInfoProcess(pMsg->data, len);
			break;
		
		case REQ_DM_SETTING_FROM_R8001:
			IxProxySettingFromR8001Process(pMsg->data, len);
			break;
		
		case REQ_DM_BACKUP:
			IxProxyBackupProcess(pMsg->data, len);
			break;
		
		case REQ_DM_RESTORE:
			IxProxyRestoreProcess(pMsg->data, len);
			break;

		case REQ_DM_FILE:
			IxProxyFileManagementProcess(pMsg->data, len);
			break;

		case REQ_SHELL:
			IxProxyShellProcess(pMsg->data, pMsg->dataLen);
			break;
			
		default:
			break;
	}
	
	SetIxProxyState(IX_PROXY_IDLE);
}

void API_IxProxy(short businessCode, short cmd_id, char* pData, int len)
{
	IX_PROXY_MSG msg;
	int msgLen;
	
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	
	msg.cmd_id = cmd_id;
	msg.businessCode = businessCode;
	msg.dataLen = len;
	memcpy(msg.data, pData, len);

	msgLen = sizeof(msg) - CLIENT_CONNECT_BUF_MAX + msg.dataLen;
	
	if(ntohs(phead->Session_ID) == 0)
	{
		SetIxProxyState(IX_PROXY_IDLE);
		return;
	}

	// 压入本地队列
	push_vdp_common_queue(&vdp_ix_proxy_mesg_queue, (char*)&msg, msgLen);
}


void IxProxyRespone(unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, void* pCmdData)
{
	char* rspJson = NULL;
	IX_PROXY_TCP_HEAD head;
	
	head.OP = OP;

	if(!(head.OP & 0x8000))
	{
		head.OP |= 0x8000;
	}
	
	head.Session_ID = Session_ID;
	head.RSP_ID = RSP_ID;

	if(pCmdData != NULL)
	{
		switch(head.OP)
		{
			case RSP_DM_REBOOT:
				rspJson = CreateRebootRspJsonData((REBOOT_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_ABOUT:
				rspJson = CreateGetAboutRspJsonData((GET_ABOUT_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_INFO:
				rspJson = CreateGetInfoRspJsonData((DeviceInfo*)pCmdData);
				break;
				
			case RSP_DM_INFO2:
				rspJson = CreateGetInfo2RspJsonData((DeviceInfo2*)pCmdData);
				break;
				
			case RSP_DM_R8012:
			case RSP_DM_R8012_SCAN_NEW:
				rspJson = CreateGetR8012RspJsonData((R8012_Device*)pCmdData);
				break;
				
			case RSP_DM_PARA_READ:
			case RSP_DM_PARA_WRITE:
				rspJson = CreateParameterRspJsonData((PARAMETER_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_PARA_DEFAULT:
				rspJson = CreateParameterDefaultRspJsonData((PARAMETER_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_IP_CHECK:
				rspJson = CreateIpCheckRspJsonData((IP_CHECK_RSP_DATA_T*)pCmdData);
				break;
			case RSP_DM_RING:
				rspJson = CreateRingRspJsonData((RING_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_PROG_CALL_NBR:
				rspJson = CreateProgRspJsonData((PROG_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_R8012_GET_CACHE:
				rspJson = CreateR8012GetCacheTableRspJsonData((R8012_DeviceTable*)pCmdData);
				break;
				
			case RSP_DM_R8012_CHECK:
				rspJson = CreateR8012CheckRspJsonData((CHECK_R8012_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_GET_INFO_BY_MFG_SN:
				rspJson = CreateGetInfoBySnRspJsonData((DeviceInfo3*)pCmdData);
				break;
				
			case RSP_DM_PROG_IP_ADDR:
				rspJson = CreateProgIpAddrRspJsonData((PROG_IP_ADDR_RSP_DATA_T*)pCmdData);
				break;
			case RSP_DM_GET_VERSION:
				rspJson = CreateGetVersionRspJsonData((VERSION_RSP_T*)pCmdData);
				break;
			case RSP_DM_GET_MERGE_INFO:
				rspJson = CreateGetMergeInfoRspJsonData((MERGE_INFO_RSP_DATA*)pCmdData);
				break;	
			
			case RSP_DM_SETTING_FROM_R8001:
				rspJson = CreateSettingFromR8001RspJsonData((SETTING_FROM_R8001_RSP_DATA*)pCmdData);
				break;
			case RSP_DM_BACKUP:
				rspJson = CreateBackupRspJsonData((CMD_BACKUP_ONE_RSP_T*)pCmdData);
				break;
			case RSP_DM_RESTORE:
				rspJson = CreateRestoreRspJsonData((CMD_RESTORE_ONE_RSP_T*)pCmdData);
				break;
			case RSP_DM_RES_DOWNLOAD:
				rspJson = pCmdData;
				break;
			case RSP_SHELL:
				rspJson = cJSON_Print((cJSON*)pCmdData);
				break;
		}
	}
	
	if(rspJson != NULL)
	{
		//dprintf("op=0x%04x, data=%s\n", head.OP, rspJson);
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, rspJson, strlen(rspJson)+1);
		free(rspJson);
	}
	else
	{
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, NULL, 0);
	}
}

void IxProxyResponeNew(unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, char* jsonString, int jsonStringLen)
{
	IX_PROXY_TCP_HEAD head;
	int sendLen, packCnt, i;
	char sendData[IX_DEVICE_JSON_DATA_LEN_MAX+3];

	head.OP = OP;

	if(!(head.OP & 0x8000))
	{
		head.OP |= 0x8000;
	}
	
	head.Session_ID = Session_ID;
	head.RSP_ID = RSP_ID;
	if(jsonString != NULL)
	{
		packCnt = jsonStringLen/IX_DEVICE_JSON_DATA_LEN_MAX+1;
		for(i = 0; i < packCnt; i++)
		{
			sendLen = (jsonStringLen - IX_DEVICE_JSON_DATA_LEN_MAX*i > IX_DEVICE_JSON_DATA_LEN_MAX ? IX_DEVICE_JSON_DATA_LEN_MAX : jsonStringLen - IX_DEVICE_JSON_DATA_LEN_MAX*i);
			sendData[0] = i+1;
			sendData[1] = packCnt;
			memcpy(sendData+2, jsonString+IX_DEVICE_JSON_DATA_LEN_MAX*i, sendLen);
			sendData[sendLen+2] = 0;
			//dprintf("op=0x%04x, %d/%d, data=%s\n\n", head.OP, sendData[0], sendData[1], sendData+2);
			CMD_RSP_DELAY();
			ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, sendData, sendLen+2);
		}
	}
	else
	{
		CMD_RSP_DELAY();
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, NULL, 0);
	}
}


void MakeIxDeviceFileDir(void)
{
	MakeDir(IxDeviceFileDir);
	MakeDir(IxDeviceTempFileDir);
	DeleteFileProcess(IxDeviceTempFileDir, "*");
}

char* GetErrorCodeByResult(int result)
{
	char* retCode;

	switch(result)
	{
		case 0:
			retCode = RESULT_SUCC;
			break;

		case -1:
			retCode = RESULT_ERR01;
			break;
		case -2:
			retCode = RESULT_ERR02;
			break;
		case -3:
			retCode = RESULT_ERR03;
			break;
		case -4:
			retCode = RESULT_ERR04;
			break;
		case -5:
			retCode = RESULT_ERR05;
			break;
		case -6:
			retCode = RESULT_ERR06;
			break;
		case -7:
			retCode = RESULT_ERR07;
			break;
		case -10:
			retCode = RESULT_ERR10;
			break;

		default:
			retCode = RESULT_ERR07;
			break;
	}

	return retCode;
}


