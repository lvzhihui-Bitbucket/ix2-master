/**
  ******************************************************************************
  * @file    obj_CmdProg.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdProgIpAddr.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_IX_Req_Ring.h"

static int ParseProgIpAddrReqJsonData(const char* json, PROG_IP_ADDR_REQ_DATA_T* pData)
{
    int status = 0;

	memset(pData, 0, sizeof(PROG_IP_ADDR_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }
	
	ParseJsonString(root, "MFG_SN", pData->MFG_SN, 12);
	ParseJsonString(root, "IP_ASSIGNED", pData->IP_ASSIGNED, 15);
	ParseJsonString(root, "IP_ADDR", pData->IP_ADDR, 15);
	ParseJsonString(root, "IP_MASK", pData->IP_MASK, 15);
	ParseJsonString(root, "IP_GATEWAY", pData->IP_GATEWAY, 15);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateProgIpAddrRspJsonData(PROG_IP_ADDR_RSP_DATA_T* pData)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "Result", pData->Result);
	cJSON_AddStringToObject(root, "MFG_SN", pData->MFG_SN);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyProgIpAddrProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PROG_IP_ADDR_REQ_DATA_T reqObj;
	PROG_IP_ADDR_RSP_DATA_T rspObj = {0};
	
	DeviceInfo deviceInfo = {0};
	int ipAddr;
	
	SetIxProxyRSP_ID(1);
	
	ParseProgIpAddrReqJsonData(cmdData, &reqObj);
	
	strcpy(rspObj.MFG_SN, reqObj.MFG_SN);
	
	if(!strcmp(GetSysVerInfo_Sn(), reqObj.MFG_SN))
	{
		//本机不支持改IP
		strncpy(rspObj.Result, RESULT_ERR04, 6);
		IxProxyRespone(RSP_DM_PROG_IP_ADDR, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
	}
	else
	{
		if(API_GetIpByMFG_SN(reqObj.MFG_SN,	IxProxyWaitUdpTime, &ipAddr, NULL) == 0)
		{
			if(IxProgIpAddrProcess(ipAddr, reqObj, 1) == 0)
			{
				strncpy(rspObj.Result, RESULT_SUCC, 6);
			}
			else
			{
				strncpy(rspObj.Result, RESULT_ERR05, 6);
			}
		}
		else
		{
			strncpy(rspObj.Result, RESULT_ERR02, 6);
		}
		
		IxProxyRespone(RSP_DM_PROG_IP_ADDR, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
	}
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

