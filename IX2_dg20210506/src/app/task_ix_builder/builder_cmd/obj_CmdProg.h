/**
  ******************************************************************************
  * @file    obj_CmdProg.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdProg_H
#define _obj_CmdProg_H

// Define Object Property-------------------------------------------------------


typedef struct
{
	char	IP_Addr[15+1];			//IP地址
	char	MFG_SN[12+1];			//MFG_SN
	
	char	DeviceNbr[10+1];		//房号
	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	name1[21];
	char	name2_utf8[41];
}PROG_REQ_DATA_T;

typedef struct
{
	char	Result[5+1];			//修改结果
	char	IP_Addr[15+1];			//IP地址
	char	MFG_SN[12+1];			//MFG_SN

	char	DeviceNbr[10+1];		//房号
	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	name1[21];
	char	name2_utf8[41];	
}PROG_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------

char* CreateProgRspJsonData(PROG_RSP_DATA_T* pData);


// Define Object Function - Private---------------------------------------------



#endif


