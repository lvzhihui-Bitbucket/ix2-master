/**
  ******************************************************************************
  * @file    obj_CmdBackup.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdRestore_H
#define _obj_CmdRestore_H

#include "task_IxProxy.h"

// Define Object Property-------------------------------------------------------

typedef struct
{
	char	IP_Addr[15+1];			//IP地址
	char	DeviceNbr[10+1];		//房号
	char	MFG_SN[12+1];			//MFG_SN
	int		bakSelect;			//备份包选择
}CMD_RESTORE_ONE_REQ_T;

typedef struct
{
	int 					cnt;
	CMD_RESTORE_ONE_REQ_T	data[IxDeviceMaxNum];
}CMD_RESTORE_MUL_REQ_T;

typedef struct
{
	char	IP_Addr[15+1];			//IP地址
	char	DeviceNbr[10+1];		//房号
	char	MFG_SN[12+1];			//MFG_SN
	int		bakSelect;				//备份包选择
	char	Result[5+1];			//发送结果
}CMD_RESTORE_ONE_RSP_T;



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------



#endif


