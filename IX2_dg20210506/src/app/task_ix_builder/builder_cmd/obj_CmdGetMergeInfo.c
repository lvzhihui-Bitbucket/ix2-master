/**
  ******************************************************************************
  * @file    obj_CmdGetInfo.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <memory.h>
#include "obj_CmdGetMergeInfo.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
//#include "../task_io_server/task_IoServer.h"

/*
[
	{"DeviceNbr":"0099000101", "IP_Addr":"192.168.243.188", "MFG_SN":"04001a43b1cf"},
	{"DeviceNbr":"0099000101", "IP_Addr":"192.168.243.188", "MFG_SN":"04001ba7db6f"},
	{"DeviceNbr":"0099008801", "IP_Addr":"192.168.243.48", "MFG_SN":""}
]

*/

static int ParseGetMergeInfoReqJsonData(const char* json, GET_MERGE_INFO_REQ_DATA_T* pData)
{
	int status = 0;
	int iCnt;
	const cJSON *pSub = NULL;

	
	memset(pData, 0, sizeof(GET_MERGE_INFO_REQ_DATA_T));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	
	pData->cnt = cJSON_GetArraySize ( root );

	if(pData->cnt > DEVICE_MAX_NUM)
	{
		pData->cnt = DEVICE_MAX_NUM;
	}

	for( iCnt = 0 ; iCnt < pData->cnt; iCnt++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }

		
		ParseJsonString(pSub, "DeviceNbr", pData->dev[iCnt].DeviceNbr, 10);
		ParseJsonString(pSub, "IP_Addr", pData->dev[iCnt].IP_Addr, 15);
		ParseJsonString(pSub, "MFG_SN", pData->dev[iCnt].MFG_SN, 12);
	}

	status = 1;

end:
	
	cJSON_Delete(root);
	
	return status;
}


char* CreateGetMergeInfoRspJsonData(MERGE_INFO_RSP_DATA* data)
{
    cJSON *root = NULL;
	char *string = NULL;
	
    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "Result", data->Result);

    cJSON_AddStringToObject(root, "MFG_SN", data->info.MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->info.BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->info.Local);
    cJSON_AddStringToObject(root, "Global", data->info.Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->info.IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->info.IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->info.IP_MASK);
	
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->info.IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->info.deviceType);
    cJSON_AddStringToObject(root, "name1", data->info.name1);
    cJSON_AddStringToObject(root, "name2_utf8", data->info.name2_utf8);
	
	cJSON_AddStringToObject(root, "SW_Ver", data->about.SW_Ver);
	cJSON_AddStringToObject(root, "HW_Ver", data->about.HW_Ver);


	cJSON_AddStringToObject(root, "UpgradeTime", data->about.UpgradeTime);
	cJSON_AddStringToObject(root, "UpgradeCode", data->about.UpgradeCode);
	cJSON_AddStringToObject(root, "UpTime", data->about.UpTime);
	cJSON_AddStringToObject(root, "DeviceModel", data->about.DeviceModel);
	cJSON_AddStringToObject(root, "AreaCode", data->about.AreaCode);
	cJSON_AddStringToObject(root, "TransferState", data->about.TransferState);

	
	cJSON_AddStringToObject(root, "HW_Address", data->about.HW_Address);

	
    cJSON_AddStringToObject(root, "Ver_Devicetype", data->version.deviceType);
    cJSON_AddStringToObject(root, "Ver_Codeinfo", data->version.codeInfo);
    cJSON_AddNumberToObject(root, "Ver_Code size", data->version.codeSize);
    cJSON_AddStringToObject(root, "Ver_AppCode", data->version.appCode);
    cJSON_AddStringToObject(root, "Ver_AppVer", data->version.appVer);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

char* ParseGetMergeInfoRspJsonDataconst( char* json, MERGE_INFO_RSP_DATA* pData)
{
	int status = 0;
	int iCnt;
	const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(MERGE_INFO_RSP_DATA));

	int tempInt;

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	
	
	ParseJsonString(root, "Result", pData->Result, 6);

    ParseJsonString(root, "MFG_SN", pData->info.MFG_SN, 12);
    ParseJsonString(root, "BD_RM_MS", pData->info.BD_RM_MS, 10);
    ParseJsonString(root, "Local", pData->info.Local, 6);
    ParseJsonString(root, "Global", pData->info.Global, 10);
    ParseJsonString(root, "IP_STATIC", pData->info.IP_STATIC, 6);

    ParseJsonString(root, "IP_ADDR", pData->info.IP_ADDR, 15);
    ParseJsonString(root, "IP_MASK", pData->info.IP_MASK, 15);
    ParseJsonString(root, "IP_GATEWAY", pData->info.IP_GATEWAY, 15);
	
    ParseJsonNumber(root, "deviceType", &tempInt);
	pData->info.deviceType = tempInt;
		
    ParseJsonString(root, "name1", pData->info.name1, 20);
    ParseJsonString(root, "name2_utf8", pData->info.name2_utf8, 40);
	
	ParseJsonString(root, "SW_Ver", pData->about.SW_Ver, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "HW_Ver", pData->about.HW_Ver, ABOUT_DATA_DOMAIN_MAX_LEN-1);


	ParseJsonString(root, "UpgradeTime", pData->about.UpgradeTime, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "UpgradeCode", pData->about.UpgradeCode, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "UpTime", pData->about.UpTime, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "DeviceModel", pData->about.DeviceModel, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "AreaCode", pData->about.AreaCode, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	ParseJsonString(root, "TransferState", pData->about.TransferState, ABOUT_DATA_DOMAIN_MAX_LEN-1);

	
	ParseJsonString(root, "HW_Address", pData->about.HW_Address, ABOUT_DATA_DOMAIN_MAX_LEN-1);

	
    ParseJsonString(root, "Ver_Devicetype", pData->version.deviceType, ABOUT_DATA_DOMAIN_MAX_LEN-1);
    ParseJsonString(root, "Ver_Codeinfo", pData->version.codeInfo, ABOUT_DATA_DOMAIN_MAX_LEN-1);
	
    ParseJsonNumber(root, "Ver_Code size", &tempInt);
	pData->version.codeSize = tempInt;
	
    ParseJsonString(root, "Ver_AppCode", pData->version.appCode, ABOUT_DATA_DOMAIN_MAX_LEN-1);
    ParseJsonString(root, "Ver_AppVer", pData->version.appVer, ABOUT_DATA_DOMAIN_MAX_LEN-1);

	status = 1;

end:
	
	cJSON_Delete(root);
	
	return status;
}

void IxProxyGetMergeInfoProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	GET_MERGE_INFO_REQ_DATA_T cmdDataObj;
	MERGE_INFO_RSP_DATA rspData;
	int ipAddr, i, result;	
	char aboutData[DATA_LEN];

	SetIxProxyRSP_ID(1);
	
	ParseGetMergeInfoReqJsonData(cmdData, &cmdDataObj);
	
//	dprintf("cmdData=%s\n", cmdData);

	for(i = 0; i < cmdDataObj.cnt; i++)
	{
		memset(&rspData, 0, sizeof(rspData));
		ipAddr = inet_addr(cmdDataObj.dev[i].IP_Addr);
		result = 0;

		//通过IP地址和房号获取
		if(cmdDataObj.dev[i].MFG_SN[0] == 0)
		{
			//Get 本机
			if(ipAddr == inet_addr(GetMyProxy_IP()) )
			{
				//房号相等
				if(!strcmp(cmdDataObj.dev[i].DeviceNbr, GetSysVerInfo_BdRmMs()))
				{
					strcpy(rspData.Result, RESULT_SUCC);
					//读取本机info
					GetMyInfo(inet_addr(GetMyProxy_IP()), &rspData.info);
					
					//读取本机about
					GetMyAboutString(inet_addr(GetMyProxy_IP()), aboutData);
					GetALLAbout(aboutData, &rspData.about);
					
					//读取本机version
					GetFwVersionAndVerify(ipAddr, 0, &rspData.version);
				}
				//房号不匹配
				else
				{
					result = -1;
					strcpy(rspData.info.BD_RM_MS, GetSysVerInfo_BdRmMs());
				}
			}
			else
			{
				if(API_GetMergeInfoByIp(ipAddr, IxProxyWaitUdpTime, aboutData) == 0)
				{
					ParseGetMergeInfoRspJsonDataconst(aboutData, &rspData);
					strncpy(rspData.Result, RESULT_SUCC, 6);
				}
				else
				{
					if(API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &rspData.info) == 0)
					{
						//房号相等
						if(!strcmp(rspData.info.BD_RM_MS, cmdDataObj.dev[i].DeviceNbr))
						{
							API_GetAboutByIp(ipAddr, IxProxyWaitUdpTime, aboutData);
					
							strncpy(rspData.Result, RESULT_SUCC, 6);
							GetALLAbout(aboutData, &rspData.about);
							GetFwVersionAndVerify(ipAddr, rspData.info.deviceType, &rspData.version);
						}
						//房号不相等
						else
						{
							result = -1;
						}
					}
					else
					{
						result = -2;
					}
				}
			}

		}
		//通过序列号获取
		else
		{
			//读取本机
			if(!strcmp(GetSysVerInfo_Sn(), cmdDataObj.dev[i].MFG_SN))
			{
				strcpy(rspData.Result, RESULT_SUCC);
				//读取本机info
				GetMyInfo(inet_addr(GetMyProxy_IP()), &rspData.info);

				//读取本机about
				GetMyAboutString(inet_addr(GetMyProxy_IP()), aboutData);
				GetALLAbout(aboutData, &rspData.about);

				//读取本机version
				GetFwVersionAndVerify(inet_addr(GetMyProxy_IP()), 0, &rspData.version);
			}
			//读取其他机器
			else
			{
				//设备在线
				if(API_GetIpByMFG_SN(cmdDataObj.dev[i].MFG_SN,	IxProxyWaitUdpTime, &ipAddr, NULL) == 0)
				{
					if(API_GetMergeInfoByIp(ipAddr, IxProxyWaitUdpTime, aboutData) == 0)
					{
						ParseGetMergeInfoRspJsonDataconst(aboutData, &rspData);
					}
					else
					{
						int temp;
						
						temp = API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &rspData.info);
						temp = API_GetAboutByIp(ipAddr, IxProxyWaitUdpTime, aboutData);
						GetALLAbout(aboutData, &rspData.about);
						GetFwVersionAndVerify(ipAddr, rspData.info.deviceType, &rspData.version);
					}
					strcpy(rspData.Result, RESULT_SUCC);
				}
				//设备离线
				else
				{
					result = -2;
				}

			}
		}
		
		dprintf("result=%d\n", result);
		
		//房号不匹配
		if(result == -1)
		{
			strncpy(rspData.Result, RESULT_ERR01, 6);
			strcpy(rspData.info.IP_ADDR, cmdDataObj.dev[i].IP_Addr);
			strcpy(rspData.info.MFG_SN, cmdDataObj.dev[i].MFG_SN);
		}
		//设备离线
		else if(result == -2)
		{
			strcpy(rspData.Result, RESULT_ERR02);
			strcpy(rspData.info.MFG_SN, cmdDataObj.dev[i].MFG_SN);
			strcpy(rspData.info.IP_ADDR, cmdDataObj.dev[i].IP_Addr);
			strcpy(rspData.info.BD_RM_MS, cmdDataObj.dev[i].DeviceNbr);
		}
		
		IxProxyRespone(RSP_DM_GET_MERGE_INFO, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
	}
	IxProxyRespone(RSP_DM_GET_MERGE_INFO, ntohs(phead->Session_ID), 0, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

