/**
  ******************************************************************************
  * @file    obj_CmdRing.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdRing_H
#define _obj_CmdRing_H

// Define Object Property-------------------------------------------------------

#define RING_DATA_LEN	100

typedef struct
{
	char	IP_Addr[15+1];				//IP地址
	char	DeviceNbr[10+1];			//房号
	char	MFG_SN[12+1];				//MFG_SN
	char	Message[RING_DATA_LEN+1];
}RING_REQ_DATA_T;

typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
	char	MFG_SN[12+1];			//MFG_SN
	char	Result[5+1];			//重启结果
}RING_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------

char* CreateRingRspJsonData(RING_RSP_DATA_T* pData);


// Define Object Function - Private---------------------------------------------



#endif


