/**
  ******************************************************************************
  * @file    obj_CmdIpCheck.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdIpCheck_H
#define _obj_CmdIpCheck_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"
#include "obj_CmdGetAbout.h"

typedef struct
{
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	MFG_SN[12+1];			//MFG_SN
	char	name[20+1];			
	char	IP[15+1];			
	char	DeviceType[6+1];		//设备类型
	char	BootTime[12+1];			//开机时间

}IP_CHECK_DEVICE_INFO_T;


typedef struct
{
	char					Result[5+1];				//检测结果
	char					IP_Addr[15+1];				//IP
	char					DeviceNbr[10+1];			//设备号

	char					IP_Result[50+1];			//IP检测结果
	IP_CHECK_DEVICE_INFO_T	Addr_Result;				//地址检测结果

}IP_CHECK_RSP_DATA_T;



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------



#endif


