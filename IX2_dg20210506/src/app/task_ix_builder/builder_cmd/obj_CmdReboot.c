/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdReboot.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"

static int ParseRebootReqJsonData(const char* json, REBOOT_REQ_DATA_T* pData)
{
    int status = 0;
	int iCnt, deviceNum;
	
    const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(REBOOT_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	
	pData->cnt = cJSON_GetArraySize ( root );
 
	for( iCnt = 0 ; iCnt < pData->cnt; iCnt ++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }
		
		ParseJsonString(pSub, "MFG_SN", pData->device[iCnt].MFG_SN, 12);
		ParseJsonString(pSub, "DeviceNbr", pData->device[iCnt].DeviceNbr, 10);
		ParseJsonString(pSub, "IP_Addr", pData->device[iCnt].IP_Addr, 15);
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateRebootRspJsonData(REBOOT_RSP_DATA_T* pData)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "MFG_SN", pData->device.MFG_SN);
	cJSON_AddStringToObject(root, "DeviceNbr", pData->device.DeviceNbr);
	cJSON_AddStringToObject(root, "IP_Addr", pData->device.IP_Addr);
	cJSON_AddStringToObject(root, "Result", pData->Result);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyRebootProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	REBOOT_REQ_DATA_T rebootObj;
	REBOOT_RSP_DATA_T rebootRspObj;
	
	DEVICE_REBOOT_REQ_T req;
	DEVICE_REBOOT_RSP_T result;
	DeviceInfo deviceInfo;
	int i, ip;
	
	SetIxProxyRSP_ID(1);
	
	ParseRebootReqJsonData(cmdData, &rebootObj);

	for(i = 0; i < rebootObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{	
		memset(&deviceInfo, 0, sizeof(deviceInfo));
		memset(&rebootRspObj, 0, sizeof(rebootRspObj));

		if(rebootObj.device[i].MFG_SN[0] == 0)
		{
			//重启本机
			if(inet_addr(rebootObj.device[i].IP_Addr) == inet_addr(GetMyProxy_IP()) )
			{
				//本机不支持重启
				strcpy(rebootRspObj.device.MFG_SN, GetSysVerInfo_Sn());
				strcpy(rebootRspObj.device.DeviceNbr, GetSysVerInfo_BdRmMs());
				strcpy(rebootRspObj.device.IP_Addr, GetMyProxy_IP());
				strncpy(rebootRspObj.Result, RESULT_ERR04, 6);
				IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
			}
			else
			{
				strcpy(rebootRspObj.device.DeviceNbr, deviceInfo.BD_RM_MS);
				strcpy(rebootRspObj.device.IP_Addr, rebootObj.device[i].IP_Addr);

				if(API_GetInfoByIp(inet_addr(rebootObj.device[i].IP_Addr), IxProxyWaitUdpTime, &deviceInfo) == 0)
				{
					strcpy(rebootRspObj.device.MFG_SN, deviceInfo.MFG_SN);
				
					if(!strcmp(rebootObj.device[i].DeviceNbr, deviceInfo.BD_RM_MS))
					{
						req.IpAddr = inet_addr(rebootObj.device[i].IP_Addr);
						req.subAddr = 0;
						if(DeviceManageRebootRequest(req.IpAddr, req, &result) == 0)
						{
							//发送重启指令成功				
							strncpy(rebootRspObj.Result, RESULT_SUCC, 6);
						}
						else
						{
							dprintf("send reboot cmd error %s\n", rebootObj.device[i].IP_Addr);
							//发送重启指令失败
							strncpy(rebootRspObj.Result, RESULT_ERR02, 6);
						}
						
						IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
					}
					else
					{
						dprintf("device number error %s,%s\n", rebootObj.device[i].DeviceNbr, deviceInfo.BD_RM_MS);
						//房号不匹配不发送重启指令
						strncpy(rebootRspObj.Result, RESULT_ERR01, 6);
						IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
					}
				}
				else
				{
					dprintf("no device ip=%s\n", rebootObj.device[i].IP_Addr);
					//无目标设备
					strncpy(rebootRspObj.Result, RESULT_ERR02, 6);
					IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
				}		
			}
		
		}
		else
		{
			//重启本机
			if(!strcmp(rebootObj.device[i].MFG_SN, GetSysVerInfo_Sn()))
			{
				//本机不支持重启
				strcpy(rebootRspObj.device.MFG_SN, GetSysVerInfo_Sn());
				strcpy(rebootRspObj.device.DeviceNbr, GetSysVerInfo_BdRmMs());
				strcpy(rebootRspObj.device.IP_Addr, GetMyProxy_IP());
				strncpy(rebootRspObj.Result, RESULT_ERR04, 6);
				IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
			}
			else
			{
				strcpy(rebootRspObj.device.MFG_SN, rebootObj.device[i].MFG_SN);

				if(API_GetIpByMFG_SN(rebootObj.device[i].MFG_SN,  IxProxyWaitUdpTime, &ip, NULL) == 0)
				{
					req.IpAddr = ip;
					req.subAddr = 0;
					if(API_GetInfoByIp(ip, IxProxyWaitUdpTime, &deviceInfo) == 0)
					{
						strcpy(rebootRspObj.device.DeviceNbr, deviceInfo.BD_RM_MS);
						strcpy(rebootRspObj.device.IP_Addr, deviceInfo.IP_ADDR);
						if(DeviceManageRebootRequest(req.IpAddr, req, &result) == 0)
						{
							//发送重启指令成功				
							strncpy(rebootRspObj.Result, RESULT_SUCC, 6);
						}
						else
						{
							dprintf("send reboot cmd error %s\n", my_inet_ntoa2(req.IpAddr));
							//发送重启指令失败
							strncpy(rebootRspObj.Result, RESULT_ERR02, 6);
						}
						IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
					}
					else
					{
						dprintf("no device ip=%s\n", my_inet_ntoa2(req.IpAddr));
						//无目标设备
						strncpy(rebootRspObj.Result, RESULT_ERR02, 6);
						IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
					}		
					
				}
				else
				{
					dprintf("no device sn=%s\n", rebootObj.device[i].MFG_SN);
					//无目标设备
					strncpy(rebootRspObj.Result, RESULT_ERR02, 6);
					IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rebootRspObj);
				}
			}
		}
	}

	IxProxyRespone(RSP_DM_REBOOT, ntohs(phead->Session_ID), 0, NULL);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

