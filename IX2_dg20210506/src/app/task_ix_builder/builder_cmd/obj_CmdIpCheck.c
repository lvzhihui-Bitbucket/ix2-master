/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdIpCheck.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_SearchIpByFilter.h"

static SearchIpRspData unhealthyDevice;
static SearchIpRspData searchOnlineListData;

int ParseIpCheckReqJsonData(const char* json, DEVICE_T* pData)
{
    int status = 0;
	
	memset(pData, 0, sizeof(DEVICE_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "DeviceNbr", pData->DeviceNbr, 10);
	ParseJsonString(root, "IP_Addr", pData->IP_Addr, 15);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}


char* CreateIpCheckRspJsonData(IP_CHECK_RSP_DATA_T* data)
{
    cJSON *root = NULL;
	cJSON *Addr_Result = NULL;
	char *string = NULL;

	
    root = cJSON_CreateObject();


	cJSON_AddStringToObject(root, "Result", data->Result);
	cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
	cJSON_AddStringToObject(root, "DeviceNbr", data->DeviceNbr);

	cJSON_AddStringToObject(root, "IP_Result", data->IP_Result);
	cJSON_AddItemToObject(root, "Addr_Result", 	Addr_Result = cJSON_CreateObject());


	cJSON_AddStringToObject(Addr_Result, "BD_RM_MS", data->Addr_Result.BD_RM_MS);
	cJSON_AddStringToObject(Addr_Result, "MFG_SN", data->Addr_Result.MFG_SN);
	cJSON_AddStringToObject(Addr_Result, "name", data->Addr_Result.name);
	cJSON_AddStringToObject(Addr_Result, "IP", data->Addr_Result.IP);
	cJSON_AddStringToObject(Addr_Result, "DeviceType", data->Addr_Result.DeviceType);
	cJSON_AddStringToObject(Addr_Result, "BootTime", data->Addr_Result.BootTime);
	
	string = cJSON_Print(root);
	
	printf("%s\n", string);

	cJSON_Delete(root);

	return string;
}


void IxProxyIpCheckProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	DEVICE_T cmdDataObj;
	IP_CHECK_RSP_DATA_T rspData;
	int i;
	
	SetIxProxyRSP_ID(1);
	
	ParseIpCheckReqJsonData(cmdData, &cmdDataObj);

	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
		memset(&rspData, 0, sizeof(rspData));
		
		//本机
		if(inet_addr(cmdDataObj.IP_Addr) == inet_addr(GetMyProxy_IP()) )
		{
			//房号相等
			if(!strcmp(cmdDataObj.DeviceNbr, GetSysVerInfo_BdRmMs()))
			{
				strncpy(rspData.Result, RESULT_SUCC, 6);
				
				if(CheckIPExist())
				{
					strncpy(rspData.IP_Result, "Error:IP exist!", 51);
				}
				else
				{
					strncpy(rspData.IP_Result, "Self IP address checking passed.", 51);
				}
			}
			//房号不匹配
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
			}
			
			strcpy(rspData.IP_Addr, GetMyProxy_IP());
			strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
			
			IxProxyRespone(RSP_IP_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);



			API_SearchIpByFilter("9999", TYPE_ALL, SearchDeviceRecommendedWaitingTime, 0, &searchOnlineListData);
			SingleOutUnhealthyDevice();

			
			for(i = 0; i < unhealthyDevice.deviceCnt; i++)
			{
				memset(&rspData, 0, sizeof(rspData));

				strncpy(rspData.Result, RESULT_SUCC, 6);
				strcpy(rspData.IP_Addr, GetMyProxy_IP());
				strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
				
				strcpy(rspData.Addr_Result.BD_RM_MS, unhealthyDevice.data[i].BD_RM_MS);
				strcpy(rspData.Addr_Result.MFG_SN, unhealthyDevice.data[i].MFG_SN);
				strcpy(rspData.Addr_Result.name, unhealthyDevice.data[i].name);
				strcpy(rspData.Addr_Result.IP, unhealthyDevice.data[i].BD_RM_MS);
				snprintf(rspData.Addr_Result.IP, 16, "%03d.%03d.%03d.%03d", unhealthyDevice.data[i].Ip&0xFF, (unhealthyDevice.data[i].Ip>>8)&0xFF, (unhealthyDevice.data[i].Ip>>16)&0xFF, (unhealthyDevice.data[i].Ip>>24)&0xFF);
				strcpy(rspData.Addr_Result.DeviceType, DeviceTypeToString(unhealthyDevice.data[i].deviceType));
				snprintf(rspData.Addr_Result.BootTime, 13, "%02d:%02d", unhealthyDevice.data[i].systemBootTime/3600, (unhealthyDevice.data[i].systemBootTime%3600)/60);
			
				IxProxyRespone(RSP_IP_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);

			}

		}
		else
		{
			//其他机器
			strncpy(rspData.Result, RESULT_ERR04, 6);
			strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
			strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);
			IxProxyRespone(RSP_IP_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}
		

	}
	
	CMD_RSP_DELAY();
	IxProxyRespone(RSP_IP_CHECK, ntohs(phead->Session_ID), 0, NULL);

}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

