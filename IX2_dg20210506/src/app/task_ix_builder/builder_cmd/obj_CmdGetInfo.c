/**
  ******************************************************************************
  * @file    obj_CmdGetInfo.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdGetInfo.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"

static int ParseGetInfoReqJsonData(const char* json, GET_INFO_REQ_DATA_T* pData)
{
    int status = 0;
	int iCnt;
    const cJSON *device = NULL;
    const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(GET_INFO_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	
	/* 获取名为“Device”的值 */
	device = cJSON_GetObjectItemCaseSensitive( root, "Device");
	if( device != NULL )
	{
		pData->cnt = cJSON_GetArraySize ( device );
	 
		for( iCnt = 0 ; iCnt < pData->cnt; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(device, iCnt);
			
			if(NULL == pSub ){ continue ; }
			
			if (cJSON_IsString(pSub) && (pSub->valuestring != NULL))
			{
				strncpy(pData->deviceNbr[iCnt], pSub->valuestring, 10+1);
			}
		}
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateGetInfoRspJsonData(DeviceInfo* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->Local);
    cJSON_AddStringToObject(root, "Global", data->Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->deviceType);
    cJSON_AddStringToObject(root, "name1", data->name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

char* CreateGetInfo2RspJsonData(DeviceInfo2* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "Result", data->Result);
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->Local);
    cJSON_AddStringToObject(root, "Global", data->Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->deviceType);
    cJSON_AddStringToObject(root, "name1", data->name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

void IxProxyGetInfoProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	GET_INFO_REQ_DATA_T cmdDataObj;
	DeviceInfo rspData;
	int i, j;	

	SetIxProxyRSP_ID(1);
	
	ParseGetInfoReqJsonData(cmdData, &cmdDataObj);

	for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		GetIpRspData data = {0};
		
		//读取本机
		if(!strcmp(cmdDataObj.deviceNbr[i], GetSysVerInfo_BdRmMs()))
		{
			memset(&rspData, 0, sizeof(rspData));
			GetMyInfo(inet_addr(GetMyProxy_IP()), &rspData);
			
			IxProxyRespone(RSP_DM_INFO, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}

		//读取其他设备
		if(API_GetIpNumberFromNet(cmdDataObj.deviceNbr[i], NULL, NULL, 1, MAX_DEVICE, &data) == 0)
		{
			for(j = 0; j < data.cnt && GetIxProxyState() == IX_PROXY_BUSY; j++)
			{
				memset(&rspData, 0, sizeof(rspData));
				if(API_GetInfoByIp(data.Ip[j], IxProxyWaitUdpTime, &rspData) == 0)
				{
					;
				}
				else
				{
					strcpy(rspData.BD_RM_MS, cmdDataObj.deviceNbr[i]);
				}
				
				IxProxyRespone(RSP_DM_INFO, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			}
		}
		else
		{
			if(strcmp(cmdDataObj.deviceNbr[i], GetSysVerInfo_BdRmMs()))
			{
				memset(&rspData, 0, sizeof(rspData));
				strcpy(rspData.BD_RM_MS, cmdDataObj.deviceNbr[i]);
				
				IxProxyRespone(RSP_DM_INFO, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			}
		}
		
	}
	
	IxProxyRespone(RSP_DM_INFO, ntohs(phead->Session_ID), 0, NULL);

}

static int ParseGetInfoReq2JsonData(const char* json, GET_INFO_REQ2_DATA_T* pData)
{
    int status = 0;
	
	memset(pData, 0, sizeof(GET_INFO_REQ2_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "DeviceNbr", pData->DeviceNbr, 10);
	ParseJsonString(root, "IP_Addr", pData->IP_Addr, 15);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void GetMyInfo2(char* Result, DeviceInfo info1, DeviceInfo2* info)
{
	strcpy(info->Result, Result);
	strcpy(info->MFG_SN, info1.MFG_SN);
	strcpy(info->BD_RM_MS, info1.BD_RM_MS);
	strcpy(info->Local, info1.Local);
	strcpy(info->Global, info1.Global);
	strcpy(info->name1, info1.name1);
	info->deviceType = info1.deviceType;

	strcpy(info->IP_STATIC, info1.IP_STATIC);
	strcpy(info->IP_ADDR, info1.IP_ADDR);
	strcpy(info->IP_MASK, info1.IP_MASK);
	strcpy(info->IP_GATEWAY, info1.IP_GATEWAY);
}


void IxProxyGetInfo2Process(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	GET_INFO_REQ2_DATA_T cmdDataObj;
	DeviceInfo info1;
	DeviceInfo2 rspData;
	int ipAddr;	

	SetIxProxyRSP_ID(1);
	
	ParseGetInfoReq2JsonData(cmdData, &cmdDataObj);

	ipAddr = inet_addr(cmdDataObj.IP_Addr);
		
	memset(&rspData, 0, sizeof(rspData));
	memset(&info1, 0, sizeof(info1));

	//读取本机
	if(ipAddr == inet_addr(GetMyProxy_IP()))
	{
		GetMyInfo(inet_addr(GetMyProxy_IP()), &info1);
		if(!strcmp(cmdDataObj.DeviceNbr, info1.BD_RM_MS))
		{
			GetMyInfo2(RESULT_SUCC, info1, &rspData);
		}
		else
		{
			GetMyInfo2(RESULT_ERR01, info1, &rspData);
		}
	}
	//读取其他机器
	else
	{
		//读取设备信息成功
		if(API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &info1) == 0)
		{
			//房号匹配
			if(!strcmp(cmdDataObj.DeviceNbr, info1.BD_RM_MS))
			{
				GetMyInfo2(RESULT_SUCC, info1, &rspData);
			}
			//房号不匹配
			else
			{
				GetMyInfo2(RESULT_ERR01, info1, &rspData);
			}
		}
		//无此设备
		else
		{
			GetMyInfo2(RESULT_ERR02, info1, &rspData);
		}
		
	}
	
	IxProxyRespone(RSP_DM_INFO2, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
}

static int ParseGetInfoBySnReqJsonData(const char* json, GET_INFO_REQ3_DATA_T* pData)
{
	int status = 0;
	int iCnt;
	const cJSON *DeviceSn = NULL;
	const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(GET_INFO_REQ3_DATA_T));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}

	
	/* 获取名为“DeviceSn”的值 */
	DeviceSn = cJSON_GetObjectItemCaseSensitive( root, "DeviceSn");
	if( DeviceSn != NULL )
	{
		pData->deviceCnt = cJSON_GetArraySize ( DeviceSn );
	 
		for( iCnt = 0 ; iCnt < pData->deviceCnt; iCnt++ )
		{
			pSub = cJSON_GetArrayItem(DeviceSn, iCnt);
			
			if(NULL == pSub ){ continue ; }
			
			if (cJSON_IsString(pSub) && (pSub->valuestring != NULL))
			{
				strncpy(pData->MFG_SN[iCnt], pSub->valuestring, 12+1);
			}
		}
	}

	status = 1;

end:
	
	cJSON_Delete(root);
	
	return status;
}

void InfoToInfo3(char* Result, int bootTime, DeviceInfo info, DeviceInfo3* info3)
{
	strcpy(info3->Result, Result);
	strcpy(info3->MFG_SN, info.MFG_SN);
	strcpy(info3->BD_RM_MS, info.BD_RM_MS);
	strcpy(info3->Local, info.Local);
	strcpy(info3->Global, info.Global);
	strcpy(info3->name1, info.name1);
	info3->deviceType = info.deviceType;

	strcpy(info3->IP_STATIC, info.IP_STATIC);
	strcpy(info3->IP_ADDR, info.IP_ADDR);
	strcpy(info3->IP_MASK, info.IP_MASK);
	strcpy(info3->IP_GATEWAY, info.IP_GATEWAY);
	info3->bootTime = bootTime;
}

char* CreateGetInfoBySnRspJsonData(DeviceInfo3* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "Result", data->Result);
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->Local);
    cJSON_AddStringToObject(root, "Global", data->Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->deviceType);
    cJSON_AddStringToObject(root, "name1", data->name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->bootTime);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyGetInfoBySnProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	GET_INFO_REQ3_DATA_T cmdDataObj;
	DeviceInfo info1;
	DeviceInfo3 rspData;
	int ipAddr, i, bootTime;	

	SetIxProxyRSP_ID(1);
	
	ParseGetInfoBySnReqJsonData(cmdData, &cmdDataObj);

	for(i = 0; i < cmdDataObj.deviceCnt; i++)
	{
		memset(&rspData, 0, sizeof(rspData));
		memset(&info1, 0, sizeof(info1));
		bootTime = 0;

		//读取本机
		if(!strcmp(GetSysVerInfo_Sn(), cmdDataObj.MFG_SN[i]))
		{
			GetMyInfo(inet_addr(GetMyProxy_IP()), &info1);
			InfoToInfo3(RESULT_SUCC, GetSystemBootTime(), info1, &rspData);
		}
		//读取其他机器
		else
		{
			if(API_GetIpByMFG_SN(cmdDataObj.MFG_SN[i],  1, &ipAddr, &bootTime) == 0)
			{
				if(API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &info1) == 0)
				{
					InfoToInfo3(RESULT_SUCC, bootTime, info1, &rspData);
				}
				else
				{
					//设备离线
					InfoToInfo3(RESULT_ERR02, bootTime, info1, &rspData);
				}
			}
			else
			{
				//设备离线
				InfoToInfo3(RESULT_ERR02, bootTime, info1, &rspData);
			}

		}
		IxProxyRespone(RSP_DM_GET_INFO_BY_MFG_SN, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
	}
	IxProxyRespone(RSP_DM_GET_INFO_BY_MFG_SN, ntohs(phead->Session_ID), 0, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

