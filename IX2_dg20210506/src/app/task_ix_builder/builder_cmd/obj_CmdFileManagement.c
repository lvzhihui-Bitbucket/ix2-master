/**
  ******************************************************************************
  * @file    obj_CmdFileManagement.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "obj_CmdFileManagement.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_SearchIpByFilter.h"
#include "vtk_udp_stack_device_update.h"


int FileManagementProcess(const char* reqData, char** rspData, int* rspDataLen)
{
	char *strIp, *strSn, *strDeviceNbr, *strOperation, *strPath, *newPath, *fileMode;
	cJSON *reqJson;
	cJSON *rspJson;
	int ret = 0;

	if(rspDataLen != NULL)
	{
		*rspDataLen = 0;
	}

	reqJson = cJSON_Parse(reqData);
	rspJson = cJSON_Parse(reqData);
	if(reqJson == NULL)
	{
		return -1;
	}

	strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
	strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
	strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
	strOperation = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_Operation));
	strPath = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_Path));

	if(!strcmp(strOperation, DM_KEY_GetFileList))
	{
		ret = GetFileAndDirList(strPath, cJSON_AddArrayToObject(rspJson, DM_KEY_FileList), 2);
	}
	else if(!strcmp(strOperation, DM_KEY_DeleteFile))
	{
		ret = DeleteFileProcess(strPath, NULL);
	}
	else if(!strcmp(strOperation, DM_KEY_FileMove))
	{
		newPath = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_NewPath));
		ret = MoveFile(strPath, newPath);
	}
	else if(!strcmp(strOperation, DM_KEY_FileCopy))
	{
		newPath = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_NewPath));
		ret = CopyFile(strPath, newPath);
	}
	else if(!strcmp(strOperation, DM_KEY_MakeDir))
	{
		ret = MakeDir(strPath);
	}
	else if(!strcmp(strOperation, DM_KEY_GetFileInfo))
	{
		ret = GetFileAndDirInfo(strPath, cJSON_AddArrayToObject(rspJson, DM_KEY_FileInfo));
	}
	else if(!strcmp(strOperation, DM_KEY_FileChmod))
	{
		fileMode = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_FileMode));
		ret = FileChmod(strPath, fileMode);
	}

	if(ret == 0)
	{
		cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_SUCC);
	}
	else
	{
		cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
	}

	*rspData = cJSON_Print(rspJson);

	if(rspDataLen != NULL)
	{
		*rspDataLen = strlen(*rspData) + 1;
	}

	return ret;
}

int ProxyGetIpByPara(const char* strIp, const char* strDeviceNbr, const char* strSn, int *getIp)
{
	int result = 0;
	int ip = 0;

	if(strIp[0] != 0)
	{
		//����
		if(inet_addr(strIp) == inet_addr(GetMyProxy_IP()))
		{
			if(strDeviceNbr[0] != 0)
			{
				if(strcmp(strDeviceNbr, GetSysVerInfo_BdRmMs()))
				{
					result = -1;
				}
			}

			if(strSn[0] != 0)
			{
				if(strcmp(strSn, GetSysVerInfo_Sn()))
				{
					result = -1;
				}
			}
		}

		ip = inet_addr(strIp);
	}
	else if(strDeviceNbr[0] != 0)
	{
		GetIpRspData getIpRsp;
		API_GetIpNumberFromNet(strDeviceNbr, NULL, NULL, IxProxyWaitUdpTime, 0, &getIpRsp);
		if(getIpRsp.cnt == 0)
		{
			result = -2;
		}
		else if(getIpRsp.cnt == 1)
		{
			ip = getIpRsp.Ip[0];
			if(strSn[0] != 0)
			{
				DeviceInfo info;
				if(API_GetInfoByIp(ip, IxProxyWaitUdpTime, &info) == 0)
				{
					if(strcmp(strSn, info.MFG_SN))
					{
						result = -1;
					}
				}
				else
				{
					result = -3;
				}
			}
		}
		else
		{
			result = -3;
		}
	} 
	else if(strSn[0] != 0)
	{
		if(API_GetIpByMFG_SN(strSn,  IxProxyWaitUdpTime, &ip, NULL) != 0)
		{
			result = -2;
		}
	} 
	if(getIp != NULL)
	{
		*getIp = ip;
	}

	return result;
}

void IxProxyFileManagementProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	
	cJSON *reqJson;
	cJSON *rspJson;
	char* rspData = NULL;
	int rspDataLen;
	char* reqData = NULL;

	char *strIp, *strSn, *strDeviceNbr;
	int ip;
	int result;

	SetIxProxyRSP_ID(1);
	
	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
	    reqJson = cJSON_Parse(cmdData);
		rspJson = cJSON_Parse(cmdData);

		#if 0
			cJSON_DeleteItemFromObject(reqJson, DM_KEY_Operation);
			cJSON_AddStringToObject(reqJson, DM_KEY_Operation, DM_KEY_MakeDir);
		#endif

		if(reqJson != NULL)
		{
			strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
			strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
			strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
			result = ProxyGetIpByPara(strIp, strDeviceNbr, strSn, &ip);
		}
		else
		{
			result = -5;
		}
		
		if(result < 0)
		{
			cJSON_AddStringToObject(rspJson, DM_KEY_Result, GetErrorCodeByResult(result));
		}
		else
		{
			reqData = cJSON_Print(reqJson);
			if(PublicMulRspUdpCmdReqProcess(ip, IxProxyWaitUdpTime, FileManagement_REQ, reqData, &rspData, &rspDataLen) != 0)
			{
				cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
				rspData = cJSON_Print(rspJson);
			}
			free(reqData);
		}

		if(rspData != NULL)
		{
			IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspData, rspDataLen);
			free(rspData);
			rspData = NULL;
		}

		IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), 0, NULL, 0);

		if(reqJson != NULL)
		{
			cJSON_Delete(reqJson);
			reqJson = NULL;
		}
		if(rspJson != NULL)
		{
			cJSON_Delete(rspJson);
			rspJson = NULL;
		}
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

