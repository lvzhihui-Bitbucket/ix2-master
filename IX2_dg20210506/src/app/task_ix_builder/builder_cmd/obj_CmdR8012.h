/**
  ******************************************************************************
  * @file    obj_CmdR8012.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdR8012_H
#define _obj_CmdR8012_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"

typedef struct
{
	char	MFG_SN[12+1];			//MFG_SN
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
	int		bootTime;
	char	Ver_Devicetype[10+1];
} R8012_Device;

typedef struct
{
	int				deviceCnt;				//设备数量
	char			dataTime[20];			//2020-03-09 17:50:26 
	R8012_Device* 	device[MAX_DEVICE];
} R8012_DeviceTable;

typedef struct
{
	char			dataTime[20];			//2020-03-09 17:50:26 
}GET_R8012_REQ_DATA_T;

typedef struct
{
	char dataTime[20];						//2020-03-09 17:50:26 
	int	deviceCnt;							//设备数量
	char MFG_SN[MAX_DEVICE][12+1];			//MFG_SN
}CHECK_R8012_REQ_DATA_T;

typedef struct
{
	char	Result[5+1];			//检测结果
	char	MFG_SN[12+1];			//MFG_SN
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
	int		bootTime;
	char	Ver_Devicetype[10+1];
} CHECK_R8012_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------
char* CreateGetInfoRspJsonData(DeviceInfo* data);


// Define Object Function - Private---------------------------------------------



#endif


