/**
  ******************************************************************************
  * @file    obj_CmdGetInfo.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <memory.h>
#include "obj_CmdSettingFromR8001.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"

/*
[
	{"DeviceNbr":"0099000101", "IP_Addr":"192.168.243.188", "MFG_SN":"04001a43b1cf"},
	{"DeviceNbr":"0099000101", "IP_Addr":"192.168.243.188", "MFG_SN":"04001ba7db6f"},
	{"DeviceNbr":"0099008801", "IP_Addr":"192.168.243.48", "MFG_SN":""}
]

*/

static int ParseSettingFromR8001ReqJsonData(const char* json, SETTING_FROM_R8001_REQ_DATA_T* pData)
{
	int status = 0;
	int iCnt;
	const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(SETTING_FROM_R8001_REQ_DATA_T));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	
	pData->cnt = cJSON_GetArraySize ( root );

	if(pData->cnt > DEVICE_MAX_NUM)
	{
		pData->cnt = DEVICE_MAX_NUM;
	}

	for( iCnt = 0 ; iCnt < pData->cnt; iCnt++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }

		
		ParseJsonString(pSub, "DeviceNbr", pData->dev[iCnt].DeviceNbr, 10);
		ParseJsonString(pSub, "IP_Addr", pData->dev[iCnt].IP_Addr, 15);
		ParseJsonString(pSub, "MFG_SN", pData->dev[iCnt].MFG_SN, 12);
	}

	status = 1;

end:
	
	cJSON_Delete(root);
	
	return status;
}


char* CreateSettingFromR8001RspJsonData(SETTING_FROM_R8001_RSP_DATA* data)
{
    cJSON *root = NULL;
	char *string = NULL;
	
    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "Result", data->Result);

    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "DeviceNbr", data->DeviceNbr);
    cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

char* ParseSettingFromR8001RspJsonDataconst( char* json, SETTING_FROM_R8001_RSP_DATA* pData)
{
	int status = 0;
	int iCnt;
	const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(SETTING_FROM_R8001_RSP_DATA));

	int tempInt;

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}
	
	
	ParseJsonString(root, "Result", pData->Result, 6);

    ParseJsonString(root, "MFG_SN", pData->MFG_SN, 12);
    ParseJsonString(root, "DeviceNbr", pData->DeviceNbr, 10);
    ParseJsonString(root, "IP_Addr", pData->IP_Addr, 15);

	status = 1;

end:
	
	cJSON_Delete(root);
	
	return status;
}

void IxProxySettingFromR8001Process(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	SETTING_FROM_R8001_REQ_DATA_T 	cmdDataObj;
	SETTING_FROM_R8001_RSP_DATA 	rspData;
	int ipAddr, i, result;	
	DeviceInfo devInfo;
	char ip_a[16];

	SetIxProxyRSP_ID(1);
	
	ParseSettingFromR8001ReqJsonData(cmdData, &cmdDataObj);
	if(cmdDataObj.cnt == 0)
	{
		API_SettingFromR8001(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR));
		API_SettingFromR8001(inet_addr(GetSysVerInfo_IP()));

		strcpy(rspData.Result, RESULT_SUCC);
		strcpy(rspData.MFG_SN, cmdDataObj.dev[i].MFG_SN);
		strcpy(rspData.IP_Addr, cmdDataObj.dev[i].IP_Addr);
		strcpy(rspData.DeviceNbr, cmdDataObj.dev[i].DeviceNbr);

		IxProxyRespone(RSP_DM_SETTING_FROM_R8001, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		return;
	}
	else
	{
		for(i = 0; i < cmdDataObj.cnt; i++)
		{
			memset(&rspData, 0, sizeof(rspData));
			ipAddr = inet_addr(cmdDataObj.dev[i].IP_Addr);
			result = 0;
		
			//通过IP地址和房号获取
			if(cmdDataObj.dev[i].MFG_SN[0] == 0)
			{
				//Get 本机
				if(ipAddr == inet_addr(GetSysVerInfo_IP()) )
				{
					//房号相等
					if(!strcmp(cmdDataObj.dev[i].DeviceNbr, GetSysVerInfo_BdRmMs()))
					{
						API_SettingFromR8001(ipAddr);
					}
					//房号不匹配
					else
					{
						result = -1;
					}
					strcpy(cmdDataObj.dev[i].DeviceNbr, GetSysVerInfo_BdRmMs());
					strcpy(cmdDataObj.dev[i].MFG_SN, GetSysVerInfo_Sn());
				}
				else
				{
					if(API_GetInfoByIp(ipAddr, 1, &devInfo) == 0)
					{
						//房号相等
						if(!strcmp(devInfo.BD_RM_MS, cmdDataObj.dev[i].DeviceNbr))
						{
							API_SettingFromR8001(ipAddr);
						}
						//房号不相等
						else
						{
							result = -1;
						}
						strcpy(cmdDataObj.dev[i].DeviceNbr, devInfo.BD_RM_MS);
						strcpy(cmdDataObj.dev[i].MFG_SN, devInfo.MFG_SN);
					}
					else
					{
						result = -2;
					}
				}
		
			}
			//通过序列号获取
			else
			{
				//读取本机
				if(!strcmp(GetSysVerInfo_Sn(), cmdDataObj.dev[i].MFG_SN))
				{
					ipAddr = inet_addr(GetSysVerInfo_IP());
					API_SettingFromR8001(ipAddr);
					strcpy(cmdDataObj.dev[i].DeviceNbr, devInfo.BD_RM_MS);
					strcpy(cmdDataObj.dev[i].IP_Addr, my_inet_ntoa(ipAddr, ip_a));
				}
				//读取其他机器
				else
				{
					//设备在线
					if(API_GetIpByMFG_SN(cmdDataObj.dev[i].MFG_SN,	1, &ipAddr, NULL) == 0)
					{
						API_GetInfoByIp(ipAddr, 1, &devInfo);
						API_SettingFromR8001(ipAddr);
						
						strcpy(cmdDataObj.dev[i].DeviceNbr, devInfo.BD_RM_MS);
						strcpy(cmdDataObj.dev[i].IP_Addr, my_inet_ntoa(ipAddr, ip_a));
					}
					//设备离线
					else
					{
						result = -2;
					}
		
				}
			}
			
			if(result == 0)
			{
				strncpy(rspData.Result, RESULT_SUCC, 6);
			}
			//房号不匹配
			else if(result == -1)
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
			}
			//设备离线
			else if(result == -2)
			{
				strcpy(rspData.Result, RESULT_ERR02);
			}

			strcpy(rspData.MFG_SN, cmdDataObj.dev[i].MFG_SN);
			strcpy(rspData.IP_Addr, cmdDataObj.dev[i].IP_Addr);
			strcpy(rspData.DeviceNbr, cmdDataObj.dev[i].DeviceNbr);
			
			IxProxyRespone(RSP_DM_SETTING_FROM_R8001, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}

	}

	
	IxProxyRespone(RSP_DM_SETTING_FROM_R8001, ntohs(phead->Session_ID), 0, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

