/**
  ******************************************************************************
  * @file    obj_CmdGetAbout.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdParaRW_H
#define _obj_CmdParaRW_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"
#include "task_IoServer.h"
#include "obj_CmdReboot.h"
#include "obj_CmdGetAbout.h"


#define 				PARAMETER_MAX_NUM	10

typedef struct
{
	char	PARA_ID[4+1];						//房号
	char	VALUE[IO_DATA_LENGTH+1];			//IP地址
}PARAMETER_T;

typedef struct
{
	int				cnt;									//参数数量
	char			IP_Addr[15+1];							//IP
	char			DeviceNbr[10+1];						//设备号
	PARAMETER_T		Parameter[PARAMETER_MAX_NUM];			//参数值
}PARAMETER_REQ_DATA_T;

typedef struct
{
	char	Result[5+1];				//结果
	char	IP_Addr[15+1];				//IP
	char	DeviceNbr[10+1];			//设备号
	char	PARA_ID[4+1];				//参数ID
	char	VALUE[IO_DATA_LENGTH+1];	//参数值
}PARAMETER_RSP_DATA_T;

/*
参数请求数据
{
	"IP_Addr":		"192.168.243.100",
	"DeviceNbr":	"0099000001",
	"Parameter":
		[
			{"PARA_ID" : "1000", "VALUE" : "123"},
			{"PARA_ID" : "1001", "VALUE" : "123"},
			{"PARA_ID" : "1002", "VALUE" : "123"},
			{"PARA_ID" : "1003", "VALUE" : "123"},
		]
} 

参数应答数据
{
	"Result":		"Succ",
	"IP_Addr":		"192.168.243.100",
	"DeviceNbr":	"0099000001",
	"PARA_ID":		"1001",
	"VALUE":		"0099000001",
} 

*/

typedef struct
{
	int			cnt;						//设备数量
	DEVICE_T	device[DEVICE_MAX_NUM];		//设备属性
}PARAMETER_DEFAULT_REQ_DATA_T;

/*
回复默认参数请求数据
[
	{
		"DeviceNbr":	"0099000101",
		"IP_Addr":	"192.168.243.100"

	},
	{
		"DeviceNbr":	"0099000001",
		"IP_Addr":	"192.168.243.100"
	}
]

*/


// Define Object Function - Public----------------------------------------------

char* CreateParameterRspJsonData(PARAMETER_RSP_DATA_T* pData);



// Define Object Function - Private---------------------------------------------



#endif


