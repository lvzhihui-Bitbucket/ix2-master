/**
  ******************************************************************************
  * @file    obj_CmdR8012.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdR8012.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"

static R8012_DeviceTable r8012CacheTable = {0};

char* GetR8012CacheTableTime(void)
{
	return r8012CacheTable.dataTime;
}

int GetR8012CacheTableNbr(void)
{
	return r8012CacheTable.deviceCnt;
}


int ModifyR8012CacheTableTime(char* time)
{
	if(time != NULL)
	{
		strncpy(r8012CacheTable.dataTime, time, 20);
		return 0;
	}
	else
	{
		return -1;
	}
}

int AddR8012Record(R8012_Device record)
{
	int i;
	
	for(i = 0; i < MAX_DEVICE; i++)
	{
		if(r8012CacheTable.device[i] != NULL)
		{
			if(!strcmp(record.MFG_SN, r8012CacheTable.device[i]->MFG_SN))
			{
				memcpy(r8012CacheTable.device[i], &record, sizeof(record));
				return 1;	//已经存在，修改完成
			}
		}
	}

	//添加的记录如果不存在
	for(i = 0; i < MAX_DEVICE; i++)
	{
		if(r8012CacheTable.device[i] == NULL)
		{
			r8012CacheTable.device[i] = malloc(sizeof(record));
			if(r8012CacheTable.device[i] == NULL)
			{
				return -1;	//添加失败
			}
			else
			{
				memcpy(r8012CacheTable.device[i], &record, sizeof(record));
				r8012CacheTable.deviceCnt++;
				return 0;	//添加成功
			}
		}
	}

	return -1;	//表已经满了，添加失败
}

R8012_Device* GetR8012Record(char* MFG_SN)
{
	int i, cnt;
	
	for(i = 0, cnt = 0; i < MAX_DEVICE && cnt < r8012CacheTable.deviceCnt; i++)
	{
		if(r8012CacheTable.device[i] != NULL)
		{
			cnt++;
			if(!strcmp(MFG_SN, r8012CacheTable.device[i]->MFG_SN))
			{
				return r8012CacheTable.device[i];
			}
		}
	}

	return NULL;
}

int DelR8012Record(char* MFG_SN)
{
	int i;
	
	for(i = 0; i < MAX_DEVICE; i++)
	{
		if(r8012CacheTable.device[i] != NULL)
		{
			if(!strcmp(MFG_SN, r8012CacheTable.device[i]->MFG_SN))
			{
				free(r8012CacheTable.device[i]);
				r8012CacheTable.device[i] = NULL;
				if(r8012CacheTable.deviceCnt > 0)
				{
					r8012CacheTable.deviceCnt--;
				}
				return 0;	//删除成功
			}
		}
	}

	return -1;	//目标不存在
}

int DelR8012CacheTable(void)
{
	int i;

	r8012CacheTable.dataTime[0] = 0;
	r8012CacheTable.deviceCnt = 0;
	
	for(i = 0; i < MAX_DEVICE; i++)
	{
		if(r8012CacheTable.device[i] != NULL)
		{
			free(r8012CacheTable.device[i]);
			r8012CacheTable.device[i] = NULL;
		}
	}

	return 0;	//删除成功
}

static int ParseGetR8012ReqJsonData(const char* json, GET_R8012_REQ_DATA_T* pData)
{
    int status = 0;
	
    const cJSON *dataTime = NULL;

	memset(pData, 0, sizeof(GET_R8012_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "dataTime", pData->dataTime, 20);

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateGetR8012RspJsonData(R8012_Device* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->Local);
    cJSON_AddStringToObject(root, "Global", data->Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->deviceType);
    cJSON_AddStringToObject(root, "name1", data->name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->bootTime);
    cJSON_AddStringToObject(root, "Ver_Devicetype", data->Ver_Devicetype);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

void DeviceInfoToR8012DeviceInfo(DeviceInfo info, R8012_Device* r8012DeviceInfo, int bootTime)
{
	memset(r8012DeviceInfo, 0, sizeof(R8012_Device));
	
	strcpy(r8012DeviceInfo->MFG_SN, info.MFG_SN);
	strcpy(r8012DeviceInfo->BD_RM_MS, info.BD_RM_MS);
	strcpy(r8012DeviceInfo->Local, info.Local);
	strcpy(r8012DeviceInfo->Global, info.Global);
	strcpy(r8012DeviceInfo->name1, info.name1);
	r8012DeviceInfo->deviceType = info.deviceType;
	strcpy(r8012DeviceInfo->IP_STATIC, info.IP_STATIC);
	strcpy(r8012DeviceInfo->IP_ADDR, info.IP_ADDR);
	strcpy(r8012DeviceInfo->IP_MASK, info.IP_MASK);
	strcpy(r8012DeviceInfo->name2_utf8, info.name2_utf8);
	strcpy(r8012DeviceInfo->IP_GATEWAY, info.IP_GATEWAY);
	r8012DeviceInfo->bootTime = bootTime;
	strcpy(r8012DeviceInfo->Ver_Devicetype, info.Ver_Devicetype);
}


void IxProxyGetR8012Process(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	DeviceInfo devideInfo;
	R8012_Device rspData;
	GET_R8012_REQ_DATA_T reqData;
	int i, j;	
	char temp[20];

	SetIxProxyRSP_ID(1);
	
	SearchIpRspData onlineDevice;

	DelR8012CacheTable();
	ParseGetR8012ReqJsonData(cmdData, &reqData);
	ModifyR8012CacheTableTime(reqData.dataTime);

	//读取本机
	
	GetMyInfo(inet_addr(GetMyProxy_IP()), &devideInfo);
	DeviceInfoToR8012DeviceInfo(devideInfo, &rspData, GetSystemBootTime());
		
	IxProxyRespone(RSP_DM_R8012, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
	
	AddR8012Record(rspData);
	
	API_SearchIpByFilter("9999", TYPE_ALL, IxProxyWaitUdpTime*5, MAX_DEVICE, &onlineDevice);

	for(i = 0; i < onlineDevice.deviceCnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		memset(&devideInfo, 0, sizeof(devideInfo));

		if(API_GetInfoByIp(onlineDevice.data[i].Ip, IxProxyWaitUdpTime, &devideInfo) == 0)
		{
			;
		}
		else
		{
			strcpy(devideInfo.BD_RM_MS, onlineDevice.data[i].BD_RM_MS);
		}
		
		DeviceInfoToR8012DeviceInfo(devideInfo, &rspData, onlineDevice.data[i].systemBootTime);
		
		IxProxyRespone(RSP_DM_R8012, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		
		AddR8012Record(rspData);
	}
	
	IxProxyRespone(RSP_DM_R8012, ntohs(phead->Session_ID), 0, NULL);

}

void IxProxyR8012ScanNewProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	DeviceInfo devideInfo;
	R8012_Device rspData;
	GET_R8012_REQ_DATA_T reqData;
	int i;	
	char temp[20];

	SetIxProxyRSP_ID(1);
	
	SearchIpRspData onlineDevice;
	
	API_SearchNewDeviceIpByFilter(&r8012CacheTable, "9999", TYPE_ALL, IxProxyWaitUdpTime, MAX_DEVICE, &onlineDevice);
	SearchResultSorting(&onlineDevice);

	if(onlineDevice.deviceCnt)
	{
		ParseGetR8012ReqJsonData(cmdData, &reqData);
		ModifyR8012CacheTableTime(reqData.dataTime);
	}
	
	for(i = 0; i < onlineDevice.deviceCnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		memset(&devideInfo, 0, sizeof(devideInfo));
		memset(&rspData, 0, sizeof(rspData));

		if(API_GetInfoByIp(onlineDevice.data[i].Ip, IxProxyWaitUdpTime, &devideInfo) == 0)
		{
			;
		}
		else
		{
			strcpy(devideInfo.BD_RM_MS, onlineDevice.data[i].BD_RM_MS);
		}

		DeviceInfoToR8012DeviceInfo(devideInfo, &rspData, onlineDevice.data[i].systemBootTime);
		
		IxProxyRespone(RSP_DM_R8012_SCAN_NEW, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		
		AddR8012Record(rspData);
	}
	
	IxProxyRespone(RSP_DM_R8012_SCAN_NEW, ntohs(phead->Session_ID), 0, NULL);

}

void IxProxyR8012GetCacheTableProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	int i, sendCnt;
	R8012_DeviceTable rspData;
	
	SetIxProxyRSP_ID(1);

	memset(&rspData, 0, sizeof(rspData));
	strcpy(rspData.dataTime, r8012CacheTable.dataTime);

	for(i = 0, sendCnt = 0; i < MAX_DEVICE && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		if(r8012CacheTable.device[i] != NULL)
		{
			rspData.device[rspData.deviceCnt++] = r8012CacheTable.device[i];
			sendCnt++;
			if(sendCnt == r8012CacheTable.deviceCnt)
			{
				IxProxyRespone(RSP_DM_R8012_GET_CACHE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
				break;
			}
			else if(rspData.deviceCnt == 2)
			{
				IxProxyRespone(RSP_DM_R8012_GET_CACHE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
				memset(&rspData, 0, sizeof(rspData));
				strcpy(rspData.dataTime, r8012CacheTable.dataTime);
			}
		}
	}


	IxProxyRespone(RSP_DM_R8012_GET_CACHE, ntohs(phead->Session_ID), 0, NULL);

}

char* CreateR8012GetCacheTableRspJsonData(R8012_DeviceTable* pData)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	char *string = NULL;
	int i, cnt;

    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "dataTime", pData->dataTime);
	cJSON_AddItemToObject(root, "devices", devices = cJSON_CreateArray());	

    for (i = 0, cnt = 0; i < MAX_DEVICE && cnt < pData->deviceCnt; i++)
    {
    	if(pData->device[i] != NULL)
    	{
			device = cJSON_CreateObject();
			
			cJSON_AddStringToObject(device, "MFG_SN", pData->device[i]->MFG_SN);
			cJSON_AddStringToObject(device, "BD_RM_MS", pData->device[i]->BD_RM_MS);
			cJSON_AddStringToObject(device, "Local", pData->device[i]->Local);
			cJSON_AddStringToObject(device, "Global", pData->device[i]->Global);
			cJSON_AddStringToObject(device, "IP_STATIC", pData->device[i]->IP_STATIC);
			
			cJSON_AddStringToObject(device, "IP_ADDR", pData->device[i]->IP_ADDR);
			cJSON_AddStringToObject(device, "IP_MASK", pData->device[i]->IP_MASK);
			cJSON_AddStringToObject(device, "IP_GATEWAY", pData->device[i]->IP_GATEWAY);
			cJSON_AddNumberToObject(device, "deviceType", pData->device[i]->deviceType);
			cJSON_AddStringToObject(device, "name1", pData->device[i]->name1);
			
			cJSON_AddStringToObject(device, "name2_utf8", pData->device[i]->name2_utf8);
			cJSON_AddNumberToObject(device, "bootTime", pData->device[i]->bootTime);
			cJSON_AddStringToObject(device, "Ver_Devicetype", pData->device[i]->Ver_Devicetype);
			
			cJSON_AddItemToArray(devices, device);

			cnt++;
		}
    }

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static int ParseCheckR8012ReqJsonData(const char* json, CHECK_R8012_REQ_DATA_T* pData)
{
    int status = 0;
	
    const cJSON *devices = NULL;
    const cJSON *MFG_SN = NULL;

	memset(pData, 0, sizeof(CHECK_R8012_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	
	ParseJsonString(root, "dataTime", pData->dataTime, 20);

	devices = cJSON_GetObjectItem( root, "devices");
	
	if(cJSON_IsArray(devices))
	{
		int i;
		pData->deviceCnt = cJSON_GetArraySize(devices);
		for(i = 0; i < pData->deviceCnt; i++)
		{
			MFG_SN = cJSON_GetArrayItem(devices, i);
			
			if (cJSON_IsString(MFG_SN) && (MFG_SN->valuestring != NULL))
			{
				strncpy(pData->MFG_SN[i], MFG_SN->valuestring, 12+1);
			}
		}
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateR8012CheckRspJsonData(CHECK_R8012_RSP_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "Result", data->Result);
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "Local", data->Local);
    cJSON_AddStringToObject(root, "Global", data->Global);
    cJSON_AddStringToObject(root, "IP_STATIC", data->IP_STATIC);

    cJSON_AddStringToObject(root, "IP_ADDR", data->IP_ADDR);
    cJSON_AddStringToObject(root, "IP_MASK", data->IP_MASK);
    cJSON_AddStringToObject(root, "IP_GATEWAY", data->IP_GATEWAY);
    cJSON_AddNumberToObject(root, "deviceType", data->deviceType);
    cJSON_AddStringToObject(root, "name1", data->name1);
	
    cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);
    cJSON_AddNumberToObject(root, "bootTime", data->bootTime);
    cJSON_AddStringToObject(root, "Ver_Devicetype", data->Ver_Devicetype);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static void R8012DeviceInfoToR8012CheckRspData(R8012_Device info, CHECK_R8012_RSP_DATA_T* rspData, char* result)
{
	memset(rspData, 0, sizeof(CHECK_R8012_RSP_DATA_T));
	
	strcpy(rspData->Result, result);
	strcpy(rspData->MFG_SN, info.MFG_SN);
	strcpy(rspData->BD_RM_MS, info.BD_RM_MS);
	strcpy(rspData->Local, info.Local);
	strcpy(rspData->Global, info.Global);
	strcpy(rspData->name1, info.name1);
	rspData->deviceType = info.deviceType;
	strcpy(rspData->IP_STATIC, info.IP_STATIC);
	strcpy(rspData->IP_ADDR, info.IP_ADDR);
	strcpy(rspData->IP_MASK, info.IP_MASK);
	strcpy(rspData->name2_utf8, info.name2_utf8);
	strcpy(rspData->IP_GATEWAY, info.IP_GATEWAY);
	rspData->bootTime = info.bootTime;
	strcpy(rspData->Ver_Devicetype, info.Ver_Devicetype);
}


void IxProxyR8012CheckProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD)); 
	DeviceInfo devideInfo;
	R8012_Device r8012DevideInfo;
	CHECK_R8012_REQ_DATA_T reqData;
	CHECK_R8012_RSP_DATA_T rspData;
	int deviceIp;
	int bootTime;
	int flag;
	int i, j, deviceCnt;	
	
	SetIxProxyRSP_ID(1);
		
	ParseCheckR8012ReqJsonData(cmdData, &reqData);
	ModifyR8012CacheTableTime(reqData.dataTime);
	if(reqData.deviceCnt == 0)
	{
		for(i = 0, deviceCnt = 0; deviceCnt < r8012CacheTable.deviceCnt && i < MAX_DEVICE && GetIxProxyState() == IX_PROXY_BUSY; i++)
		{
			if(r8012CacheTable.device[i] != NULL)
			{
				deviceCnt++;

				//检测本机
				if(!strcmp(r8012CacheTable.device[i]->MFG_SN, GetSysVerInfo_Sn()))
				{
					GetMyInfo(inet_addr(GetMyProxy_IP()), &devideInfo);
					DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DevideInfo, GetSystemBootTime());
					r8012CacheTable.device[i]->bootTime = r8012DevideInfo.bootTime;

					
					if(memcmp(r8012CacheTable.device[i], &r8012DevideInfo, sizeof(r8012DevideInfo)))
					{
						//设备信息有变化
						AddR8012Record(r8012DevideInfo);
						R8012DeviceInfoToR8012CheckRspData(r8012DevideInfo, &rspData, RESULT_SUCC);
						IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
					}
					continue;
				}
				
				//检测其他设备
				if(API_GetIpByMFG_SN(r8012CacheTable.device[i]->MFG_SN,  1, &deviceIp, &bootTime) == 0)
				{
					if(API_GetInfoByIp(deviceIp, IxProxyWaitUdpTime, &devideInfo) == 0)
					{
						DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DevideInfo, bootTime);
						r8012CacheTable.device[i]->bootTime = bootTime;

						if(memcmp(r8012CacheTable.device[i], &r8012DevideInfo, sizeof(r8012DevideInfo)))
						{
							//设备信息有变化
							AddR8012Record(r8012DevideInfo);
							R8012DeviceInfoToR8012CheckRspData(r8012DevideInfo, &rspData, RESULT_SUCC);
							IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
						}
					}
					else
					{
						//设备离线
						R8012DeviceInfoToR8012CheckRspData(*(r8012CacheTable.device[i]), &rspData, RESULT_ERR02);
						IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
						DelR8012Record(r8012CacheTable.device[i]->MFG_SN);
					}
				}
				else
				{
					//设备离线
					R8012DeviceInfoToR8012CheckRspData(*(r8012CacheTable.device[i]), &rspData, RESULT_ERR02);
					IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
					DelR8012Record(r8012CacheTable.device[i]->MFG_SN);
				}
			}
		}
	}
	else
	{
		for(i = 0; i < reqData.deviceCnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
		{
			for(flag = 0, j = 0, deviceCnt = 0; deviceCnt < r8012CacheTable.deviceCnt && j < MAX_DEVICE; j++)
			{
				if(r8012CacheTable.device[j] != NULL)
				{
					deviceCnt++;
					if(!strcmp(r8012CacheTable.device[j]->MFG_SN, reqData.MFG_SN[i]))
					{
						flag = 1;	//表中找到设备

						
						//检测本机
						if(!strcmp(r8012CacheTable.device[j]->MFG_SN, GetSysVerInfo_Sn()))
						{
							GetMyInfo(inet_addr(GetMyProxy_IP()), &devideInfo);
							DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DevideInfo, GetSystemBootTime());
							r8012CacheTable.device[j]->bootTime = r8012DevideInfo.bootTime;
						
							if(memcmp(r8012CacheTable.device[j], &r8012DevideInfo, sizeof(r8012DevideInfo)))
							{
								//设备信息有变化
								AddR8012Record(r8012DevideInfo);
								R8012DeviceInfoToR8012CheckRspData(r8012DevideInfo, &rspData, RESULT_SUCC);
								IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
							}
							break;
						}

						//检测其他设备
						if(API_GetIpByMFG_SN(reqData.MFG_SN[i],  IxProxyWaitUdpTime, &deviceIp, &bootTime) == 0)
						{
							if(API_GetInfoByIp(deviceIp, IxProxyWaitUdpTime, &devideInfo) == 0)
							{
								DeviceInfoToR8012DeviceInfo(devideInfo, &r8012DevideInfo, bootTime);
								r8012CacheTable.device[j]->bootTime = bootTime;
						
								if(memcmp(r8012CacheTable.device[j], &r8012DevideInfo, sizeof(r8012DevideInfo)))
								{
									//设备信息有变化
									AddR8012Record(r8012DevideInfo);
									R8012DeviceInfoToR8012CheckRspData(r8012DevideInfo, &rspData, RESULT_SUCC);
									IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
								}
							}
							else
							{
								//设备离线
								R8012DeviceInfoToR8012CheckRspData(*(r8012CacheTable.device[j]), &rspData, RESULT_ERR02);
								IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
								DelR8012Record(r8012CacheTable.device[j]->MFG_SN);
							}
						}
						else
						{
							//设备离线
							R8012DeviceInfoToR8012CheckRspData(*(r8012CacheTable.device[j]), &rspData, RESULT_ERR02);
							IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
							DelR8012Record(r8012CacheTable.device[j]->MFG_SN);
						}
						break;
					}
				}
			}

			//表中没有此设备
			if(flag == 0)
			{
				memset(&rspData, 0, sizeof(rspData));
				strncpy(rspData.Result, RESULT_ERR02, 6);
				strcpy(rspData.MFG_SN, r8012CacheTable.device[j]->MFG_SN);
				IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			}
		}
	}
	
	IxProxyRespone(RSP_DM_R8012_CHECK, ntohs(phead->Session_ID), 0, NULL);

}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

