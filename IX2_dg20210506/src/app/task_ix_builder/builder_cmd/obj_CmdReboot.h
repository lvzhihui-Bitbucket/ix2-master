/**
  ******************************************************************************
  * @file    obj_CmdReboot.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdReboot_H
#define _obj_CmdReboot_H

// Define Object Property-------------------------------------------------------

#include "tcp_server_process.h"

/*
[
	{
		"DeviceNbr":	"0099000101",
		"IP_Addr":		"192.168.243.100"

	},
	{
		"DeviceNbr":	"0099000001",
		"IP_Addr":		"192.168.243.100"
	}
]

*/
typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
}DEVICE_T;

typedef struct
{
	char	MFG_SN[12+1];			//设备序列号
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
}REBOOT_DEVICE_T;


typedef struct
{
	int				cnt;						//设备数量
	REBOOT_DEVICE_T	device[DEVICE_MAX_NUM];	//设备属性
}REBOOT_REQ_DATA_T;

typedef struct
{
	REBOOT_DEVICE_T device;
	char			Result[5+1];			//重启结果
}REBOOT_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------

char* CreateRebootRspJsonData(REBOOT_RSP_DATA_T* pData);


// Define Object Function - Private---------------------------------------------



#endif


