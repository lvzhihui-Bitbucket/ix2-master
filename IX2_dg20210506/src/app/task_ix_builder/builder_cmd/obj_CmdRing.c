/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdRing.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_IX_Req_Ring.h"

static int ParseRingReqJsonData(const char* json, RING_REQ_DATA_T* pData)
{
    int status = 0;
	
	memset(pData, 0, sizeof(RING_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	ParseJsonString(root, "DeviceNbr", pData->DeviceNbr, 10);
	ParseJsonString(root, "IP_Addr", pData->IP_Addr, 15);
	ParseJsonString(root, "MFG_SN", pData->MFG_SN, 12);
	ParseJsonString(root, "Message", pData->Message, RING_DATA_LEN);

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateRingRspJsonData(RING_RSP_DATA_T* pData)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "DeviceNbr", pData->DeviceNbr);
	cJSON_AddStringToObject(root, "IP_Addr", pData->IP_Addr);
	cJSON_AddStringToObject(root, "MFG_SN", pData->MFG_SN);
	cJSON_AddStringToObject(root, "Result", pData->Result);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyRingProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	RING_REQ_DATA_T reqObj;
	RING_RSP_DATA_T rspObj;
	
	IxReqRingRsp ringRsp;
	int i, ipAddr;
	SetIxProxyRSP_ID(1);
	
	ParseRingReqJsonData(cmdData, &reqObj);

	//By IP_Addr
	if(reqObj.IP_Addr[0] != 0)
	{
		ipAddr = inet_addr(reqObj.IP_Addr);

		if(IxReqRingProcess(ipAddr, 1, reqObj.Message, &ringRsp) == 0)
		{
			strcpy(rspObj.DeviceNbr, ringRsp.DeviceNbr);
			strcpy(rspObj.MFG_SN, ringRsp.MFG_SN);
			strcpy(rspObj.IP_Addr, ringRsp.IP_Addr);
			strncpy(rspObj.Result, RESULT_SUCC, 6);
		}
		else
		{
			strncpy(rspObj.Result, RESULT_ERR02, 6);
		}
			
		IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
	}
	//By Device_Addr：可能有很多重复的
	else if(reqObj.DeviceNbr[0] != 0)
	{
		GetIpRspData data = {0};

		//ring本机
		if(!strcmp(reqObj.DeviceNbr, GetSysVerInfo_BdRmMs()))
		{
			if(IxReqRingProcess(inet_addr(GetMyProxy_IP()), 1, reqObj.Message, &ringRsp) == 0)
			{
				strcpy(rspObj.DeviceNbr, ringRsp.DeviceNbr);
				strcpy(rspObj.MFG_SN, ringRsp.MFG_SN);
				strcpy(rspObj.IP_Addr, ringRsp.IP_Addr);
				strncpy(rspObj.Result, RESULT_SUCC, 6);
				IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
			}
		}
		
		//ring其他机器
		if(API_GetIpNumberFromNet(reqObj.DeviceNbr, NULL, NULL, 2, MAX_DEVICE, &data) == 0)
		{
			for(i = 0; i < data.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
			{
				if(IxReqRingProcess(data.Ip[i], 1, reqObj.Message, &ringRsp) == 0)
				{
					strcpy(rspObj.DeviceNbr, ringRsp.DeviceNbr);
					strcpy(rspObj.MFG_SN, ringRsp.MFG_SN);
					strcpy(rspObj.IP_Addr, ringRsp.IP_Addr);
					strncpy(rspObj.Result, RESULT_SUCC, 6);
					IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
				}
			}
		}
		else
		{
			strncpy(rspObj.Result, RESULT_ERR02, 6);
			IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
		}
	}

	//By MFG_SN
	else if(reqObj.MFG_SN[0] != 0)
	{
		//ring本机
		if(!strcmp(reqObj.MFG_SN, GetSysVerInfo_Sn()))
		{
			if(IxReqRingProcess(inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(inet_addr(GetMyProxy_IP())))), 1, reqObj.Message, &ringRsp) == 0)
			{
				strcpy(rspObj.DeviceNbr, ringRsp.DeviceNbr);
				strcpy(rspObj.MFG_SN, ringRsp.MFG_SN);
				strcpy(rspObj.IP_Addr, ringRsp.IP_Addr);
				strncpy(rspObj.Result, RESULT_SUCC, 6);
				IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
			}
		}
		//ring其他机器
		else
		{
			if(API_GetIpByMFG_SN(reqObj.MFG_SN, 1, &ipAddr, NULL) == 0)
			{
				if(IxReqRingProcess(ipAddr, 1, reqObj.Message, &ringRsp) == 0)
				{
					strcpy(rspObj.DeviceNbr, ringRsp.DeviceNbr);
					strcpy(rspObj.MFG_SN, ringRsp.MFG_SN);
					strcpy(rspObj.IP_Addr, ringRsp.IP_Addr);
					strncpy(rspObj.Result, RESULT_SUCC, 6);
				}
				else
				{
					strncpy(rspObj.Result, RESULT_ERR02, 6);
				}
			}
			else
			{
				strncpy(rspObj.Result, RESULT_ERR02, 6);
			}
			IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
		}
	}

	IxProxyRespone(RSP_DM_RING, ntohs(phead->Session_ID), 0, NULL);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

