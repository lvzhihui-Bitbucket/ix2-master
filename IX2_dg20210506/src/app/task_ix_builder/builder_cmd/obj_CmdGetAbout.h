/**
  ******************************************************************************
  * @file    obj_CmdGetAbout.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdGetAbout_H
#define _obj_CmdGetAbout_H

// Define Object Property-------------------------------------------------------
#include "obj_CmdReboot.h"

#define ABOUT_DATA_DOMAIN_MAX_LEN		30

typedef struct
{
	int			cnt;						//设备数量
	DEVICE_T	device[DEVICE_MAX_NUM];	//设备属性
}GET_ABOUT_REQ_DATA_T;


typedef struct
{
	char	Result[5+1];							//about结果
	char	IP_Addr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	Device_Addr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	Name[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	G_Nbr[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	L_Nbr[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	SW_Ver[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	HW_Ver[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpgradeTime[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpgradeCode[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	UpTime[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	SerialNo[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DeviceType[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DeviceModel[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	AreaCode[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	TransferState[ABOUT_DATA_DOMAIN_MAX_LEN];

	char	HW_Address[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	SubnetMask[ABOUT_DATA_DOMAIN_MAX_LEN];
	char	DefaultRoute[ABOUT_DATA_DOMAIN_MAX_LEN];
	
}GET_ABOUT_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------
char* CreateGetAboutRspJsonData(GET_ABOUT_RSP_DATA_T* data);


// Define Object Function - Private---------------------------------------------



#endif


