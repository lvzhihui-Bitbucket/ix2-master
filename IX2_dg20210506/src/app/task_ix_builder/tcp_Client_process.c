
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>
#include "utility.h"

#include "vtk_udp_stack_class.h"
#include "onvif_tiny_task.h"
#include "tcp_Client_process.h"
#include "tcp_server_process.h"
#include "MenuSipConfig_Business.h"
#include "task_IoServer.h"

static IxBuilderRemoteRun ixDeviceRemoteRun;

static int tcp_client_connect_with_timeout(char* net_dev_name, char* ip_str, int port, int t_timeout, int* psockfd )
{
	int fd = 0;
	struct sockaddr_in	addr;
	struct sockaddr_in	sin;
	fd_set fdr, fdw;
	struct ifreq ifr;
	
	struct timeval timeout;
	int err = 0;
	int errlen = sizeof(err);
	
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_pton(AF_INET, ip_str, &addr.sin_addr);

	bzero(&sin, sizeof(sin));	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	

	fd = socket(AF_INET,SOCK_STREAM,0);
	
	if (fd < 0) 
	{
		eprintf("create socket failed\n");
		return -1;
	}

	int m_loop = 1;
	if( setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", fd);
		return -1;
	}

	if(net_dev_name != NULL)
	{
		memset(ifr.ifr_name, 0, IFNAMSIZ);
		sprintf(ifr.ifr_name, "%s", net_dev_name);
		if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
		{
			eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
			close(fd);
			return -1;
		}

		if(bind(fd, (struct sockaddr *) &sin, sizeof(sin)) == -1)
		{
			eprintf("fail to bind\n");
			close(fd);
			return -1;
		}
	}

	/*�����׽���Ϊ������*/
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) 
	{
		fprintf(stderr, "Get flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0) 
	{
		fprintf(stderr, "Set flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	
	/*���������linuxϵͳĬ�ϳ�ʱʱ��Ϊ75s*/
	int rc = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	if (rc != 0) 
	{
		if (errno == EINPROGRESS) 
		{
			printf("Doing connection.\n");
			/*���ڴ�������*/
			FD_ZERO(&fdr);
			FD_ZERO(&fdw);
			FD_SET(fd, &fdr);
			FD_SET(fd, &fdw);
			timeout.tv_sec = t_timeout;
			timeout.tv_usec = 0;
			rc = select(fd + 1, &fdr, &fdw, NULL, &timeout);
			printf("rc is: %d\n", rc);
			/*select����ʧ��*/
			if (rc < 0) 
			{
				fprintf(stderr, "connect error:%s\n", strerror(errno));
				close(fd);
				return -1;
			}
		
			/*���ӳ�ʱ*/
			if (rc == 0) 
			{
				fprintf(stderr, "Connect timeout.\n");
				close(fd);
				return -1;
			}
			/*[1] �����ӳɹ�����ʱ����������ɿ�д,rc=1*/
			if (rc == 1 && FD_ISSET(fd, &fdw)) 
			{
				goto connect_success;
			}
			/*[2] �����ӽ�����������ʱ����������Ϊ���ɶ���Ҳ��д��rc=2 ��������������ɵ���getsockopt����*/
			if (rc == 2) 
			{
				if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen) == -1) 
				{
					fprintf(stderr, "getsockopt(SO_ERROR): %s", strerror(errno));
					close(fd);
					return -1;
				}

				if (err) 
				{
					errno = err;
					fprintf(stderr, "connect error:%s\n", strerror(errno));
					close(fd);
					return -1;
				}
			}
		} 
		fprintf(stderr, "connect failed, error:%s.\n", strerror(errno));
		close(fd);
		return -1;
	} 

	connect_success:

	printf("Connect success\n");

	if(psockfd != NULL)
	{
		*psockfd = fd;
	}
	
	return 0;
}


static int Api_tcp_client_connect_to_server(char* net_dev_name, char *ip_str, int port, char* id ,char* pwd)
{
	int  err, num;
	int fd;
	ProxyRemoteOnlineRsp recv_pkt;
	int recv_len;

	srand(time(NULL));
	
	memset(&recv_pkt,0,sizeof(ProxyRemoteOnlineRsp));
	
		
	// connect
	int retry_cnt = 4;

	for(retry_cnt = 4, err = -1; retry_cnt > 0 && err != 0; retry_cnt--)
	{
		err = tcp_client_connect_with_timeout(net_dev_name, ip_str, port, 5, &fd);
	}
	
	if(err == 0)
	{
		int setflag = 1;
		int flags = fcntl(fd, F_GETFL, 0);
		if (flags < 0) 
		{
			dprintf("Get flags error:%s\n", strerror(errno));
			close(fd);
			return -1;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(fd, F_SETFL, flags) < 0) 
		{
			dprintf("Set flags error:%s\n", strerror(errno));
			close(fd);
			return -1;
		}
		
 		struct timeval tv_out;
    	tv_out.tv_sec = 3;
    	tv_out.tv_usec = 0;
    	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		
		dprintf("connect ok! [server:%s]\n",ip_str);
	
		// send
		struct timeval tv;
		fd_set fds;
		int ret;
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;	// 100ms
		if((ret = select( fd + 1, NULL, &fds, NULL, &tv ))>0)
		{
			if( FD_ISSET( fd, &fds ) )
				ProxyOnlineReq(fd, 1, id, pwd);
			else
				return 0;
		}
		else
			return 0;

		printf("1-------------------------------------------\n");

		
		// recv
		recv_len = sizeof(recv_pkt);

		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;
		if((ret = select( fd + 1, &fds,NULL, NULL, &tv ))>0)	
		{
			if( FD_ISSET( fd, &fds ) )
				num = recv(fd, (char*)&recv_pkt, recv_len, MSG_DONTWAIT);
			else
				return 0;
			
		}
		else
			return 0;

		printf("2-------------------------------------------\n");

		if (num > 0) // success
		{
			dprintf("recv_pkt.head.cmd_type=0x%04x, recv_pkt.online=%d, recv_pkt.result=%d\n", recv_pkt.head.cmd_type, recv_pkt.online, recv_pkt.result);	
			if(recv_pkt.head.cmd_type == IX_DEVICE_REMOTE_PROXY_ONLINE_RSP && recv_pkt.online == 1 && recv_pkt.result == 0)
			{
				return fd;
			}
		}
		else
		{
			printf("recv error\n");
		}
		
		usleep(10*1000);
		close( fd );	

	}
	else
	{
		fd = 0;
	}
	
	return 0;
}

static int Api_tcp_client_disconnect(int* fd)
{
	if(*fd != 0)
	{
		close(*fd);
		*fd=0;
	}
}

static int Api_tcp_client_send(int fd, const char *dat, int dat_len)
{		
	return send(fd, dat, dat_len, MSG_DONTWAIT);
}

static int ProxySendBeatHeart(int fd, int id)
{
	ProxyRemoteBeatHeart beatHeart;

	memcpy(beatHeart.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	beatHeart.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_REQ;
	beatHeart.head.cmd_id = ixDeviceRemoteRun.cmd_id++;
	beatHeart.head.dev_type = 0;
	beatHeart.head.data_len = sizeof(beatHeart) - sizeof(beatHeart.head);
	beatHeart.ID = id;
	beatHeart.RESERVE = 0x0000;
	
	dprintf("ProxySendBeatHeart, cmd_id =%d, len=%d\n", beatHeart.head.cmd_id, sizeof(beatHeart));

	return Api_tcp_client_send(fd, (char*)&beatHeart, sizeof(beatHeart));
}

static int ProxyOnlineReq(int fd, short online, char* id, char* pwd)
{
	ProxyRemoteOnlineReq send_pkt;

	memcpy(send_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	send_pkt.head.cmd_type = IX_DEVICE_REMOTE_PROXY_ONLINE_REQ;
	send_pkt.head.cmd_id = ixDeviceRemoteRun.cmd_id++;
	send_pkt.head.dev_type = 0;
	send_pkt.head.data_len = sizeof(send_pkt) - sizeof(ProxyRemoteHead);
	send_pkt.online = online;
	send_pkt.reserve = 0;
	strncpy(send_pkt.proxyId, id, 32);
	strncpy(send_pkt.proxyPwd, pwd, 16);
		
	//dprintf("ProxyOnlineReq, cmd_id =%d, len=%d\n", send_pkt.head.cmd_id, sizeof(send_pkt));
	
	return Api_tcp_client_send(fd, (char*)&send_pkt, sizeof(send_pkt));
}

int ProxyRemoteRspData(int fd, short cmd_id, short businessCode, char* data, int len)
{
	ProxyRemoteTransmitRsp sendPacket;
	int headLen;
	char sendData[IX_DEVICE_REMOTE_TCP_BUFF_MAX];

	memcpy(sendPacket.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	sendPacket.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT;
	sendPacket.head.cmd_id = cmd_id;
	sendPacket.head.dev_type = 0;
	sendPacket.head.data_len = sizeof(businessCode) + len;
	sendPacket.businessCode = businessCode;
	headLen = sizeof(sendPacket);

	
	memcpy(sendData, &sendPacket, headLen);
	memcpy(sendData + headLen, data, len);
	
	return Api_tcp_client_send(fd, sendData, headLen + len);
}

int ProxyRemoteTransferData(int fd, short businessCode, char* data, int len)
{
	ProxyRemoteTransmitRsp sendPacket;
	int headLen;
	char sendData[IX_DEVICE_REMOTE_TCP_BUFF_MAX];

	memcpy(sendPacket.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	sendPacket.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT;
	sendPacket.head.cmd_id = ixDeviceRemoteRun.cmd_id++;
	sendPacket.head.dev_type = 0;
	sendPacket.head.data_len = sizeof(businessCode) + len;
	sendPacket.businessCode = businessCode;
	headLen = sizeof(sendPacket);

	
	memcpy(sendData, &sendPacket, headLen);
	memcpy(sendData + headLen, data, len);
	
	//dprintf("ProxyRemoteTransferData, cmd_id =%d, len=%d\n", sendPacket.head.cmd_id, headLen + len);
	
	return Api_tcp_client_send(fd, sendData, headLen + len);
}


static int client_socket_task_init(void* arg)
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_client_tcp_ins ptr_ins = (p_client_tcp_ins)ptiny_task->pusr_dat;

	ixDeviceRemoteRun.cmd_id = 0;
	ixDeviceRemoteRun.beatHeartTimeing = 0;
	return 0;	
}

static void ProxyRemoteCmdProcess(ProxyRemoteHead packHead, char* data, int dat_len)
{
	short businessCode;
	
	businessCode = (data[1] << 8) + data[0];
	
	switch(packHead.cmd_type)
	{
		case IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT:
			IxProxyRecvDataProcess(businessCode, packHead.cmd_id, data+2, dat_len-2);
			break;
		case IX_DEVICE_REMOTE_PROXY_ONLINE_RSP:
			break;
		case IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_RSP:
			ixDeviceRemoteRun.beatHeartTimeing = 0;
			break;
	}
}

static void client_socket_task_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_client_tcp_ins ptr_ins = (p_client_tcp_ins)ptiny_task->pusr_dat;
	int recv_len, num, timeCnt, id;
	ProxyRemoteHead packHead;
	char recv_data[IX_DEVICE_REMOTE_TCP_BUFF_MAX];	
	timeCnt = 0;
	id = 0;

	dprintf("---------------------client_socket_task_process start...\n");

    while(one_tiny_task_is_running(&ptr_ins->task))
    {		
		recv_len = sizeof(ProxyRemoteHead);
		
		num = recv(ptr_ins->fd, (char*)&packHead, recv_len, MSG_DONTWAIT);
		if(num == recv_len && !memcmp(packHead.head, "VTEK", 4) && packHead.data_len > 0)
		{
			recv_len = packHead.data_len;
			num = recv(ptr_ins->fd, recv_data, recv_len, MSG_DONTWAIT);
			
//			dprintf("num=%d, cmd_type=0X%04x, cmd_id=%d, dev_type=%d, data_len=%d\n", num, packHead.cmd_type, packHead.cmd_id, packHead.dev_type, packHead.data_len);
			
			if(num != recv_len)
			{
				//���ݰ��д�����ս��ջ��������
				recv_len = IX_DEVICE_REMOTE_TCP_BUFF_MAX;
				while(recv(ptr_ins->fd, recv_data, recv_len, MSG_DONTWAIT) > 0);
			}
			else 
			{
				//���ݰ�����
				ProxyRemoteCmdProcess(packHead, recv_data, recv_len);
			}
		}
		//TCP�������Ѿ��Ͽ�
		else if(num == 0)
		{
			dprintf("TCP disconnect..........\n");
			break;
		}
		
		usleep(IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS*1000);


		if(++timeCnt >= IX_DEVICE_REMOTE_SEND_BEAT_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
		{
			timeCnt = 0;
			ProxySendBeatHeart(ptr_ins->fd, ++id);
		}
		
		if(++ixDeviceRemoteRun.beatHeartTimeing >= IX_DEVICE_REMOTE_SEND_RECV_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
		{
			dprintf("TCP server no respond\n");
			break;
		}
    }
	
	dprintf("------------------client_socket_task_process end...\n");
}

static void client_socket_task_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_client_tcp_ins ptr_ins = (p_client_tcp_ins)ptiny_task->pusr_dat;

	
	ProxyOnlineReq(ptr_ins->fd, 0, ptr_ins->id, ptr_ins->pwd);

	Api_tcp_client_disconnect(&ptr_ins->fd);

	free( ptr_ins );
	ptr_ins = NULL;
	
	ixDeviceRemoteRun.client = NULL;
	
	if(IsMyProxyLinked())
	{
		SetMyProxyData(NULL, NULL, "0", NULL);
	}

	dprintf("server_socket_task_exit!!\n");
}

static p_client_tcp_ins ProxyConnectToServer(char* net_dev_name, char *ip_str, int port, char* id, char* pwd)
{	
	p_client_tcp_ins	ptr_ins;
	ptr_ins = (p_client_tcp_ins)malloc(sizeof(client_tcp_ins));
	struct ifreq ifr;
	char tempData[IX_BUILDER_NAME_LEN*2];

	if( ptr_ins == NULL )
		return NULL;
	
	if(IsMyProxyLinked())
		return NULL;
		
	ptr_ins->port = port;	
	strncpy(ptr_ins->ip, ip_str, 16);
	strncpy(ptr_ins->id, id, 32);
	strncpy(ptr_ins->pwd, pwd, 16);

	ptr_ins->fd = Api_tcp_client_connect_to_server(net_dev_name, ptr_ins->ip, ptr_ins->port, ptr_ins->id, ptr_ins->pwd);

	if(ptr_ins->fd <= 0)
	{
		dprintf("client connect to server error!!! [ip=%s, port=%d]\n", ptr_ins->ip, ptr_ins->port);		

		goto error_exit;
	}

	// initial thread
	if( one_tiny_task_start( &ptr_ins->task, client_socket_task_init, client_socket_task_process, client_socket_task_exit, (void*)ptr_ins ) == -1 )
	{
		close(ptr_ins->fd);		
		goto error_exit;
	}

	snprintf(tempData, IX_BUILDER_NAME_LEN*2, "%s@%s", id, ip_str); 
	SetMyProxyData(NULL, NULL, tempData, net_dev_name);
	
	return ptr_ins;

	error_exit:
	if( ptr_ins != NULL )
	{
		free( ptr_ins );
		ptr_ins = NULL;
		dprintf("free ptr_ins ok!\n");		
	}
	return NULL;		
}



static int ProxyDisconnectToServer(p_client_tcp_ins ptr_ins)
{
	if( ptr_ins == NULL )
		return -1;

	ProxyOnlineReq(ptr_ins->fd, 0, ptr_ins->id, ptr_ins->pwd);
	
	Api_tcp_client_disconnect(&ptr_ins->fd);

	// �ȴ��߳̽���
	one_tiny_task_stop( &ptr_ins->task );
	
	ixDeviceRemoteRun.client = NULL;
	
	if(IsMyProxyLinked())
	{
		SetMyProxyData(NULL, NULL, "0", NULL);
	}

	return 0;
}

void IxDeviceRemoteInit(void)
{
	pthread_mutex_init( &ixDeviceRemoteRun.lock, 0);
	ixDeviceRemoteRun.beatHeartTimeing = 0;
	ixDeviceRemoteRun.client = NULL;
	ixDeviceRemoteRun.cmd_id = 0;
	ixDeviceRemoteRun.autoConnectEnable = 0;
}

p_client_tcp_ins GetIxProxyRemoteTcpFd(void)
{
	p_client_tcp_ins 	client;
	
	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	client = ixDeviceRemoteRun.client;
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
	
	return client;
}

void IxBuilderTcpClientConnect(void)
{
	SipCfg_T* pSip = GetSipConfig();
	char temp[10];

	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	if(ixDeviceRemoteRun.client == NULL)
	{
		API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, temp);
		ixDeviceRemoteRun.client = ProxyConnectToServer((atoi(temp) ? NET_WLAN0 : NET_ETH0), "47.106.104.38", IX_DEVICE_REMOTE_TCP_PORT, pSip->account, pSip->password);
	}
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
}

void IxBuilderTcpClientDisconnect(void)
{
	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	if(ixDeviceRemoteRun.client != NULL)
	{
		ProxyDisconnectToServer(ixDeviceRemoteRun.client);
		ixDeviceRemoteRun.client = NULL;
	}
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
}

void IxBuilderTcpClientSwitchLoad(void)
{
	char ioValue[5];
	int ret ;
	
	ret = API_Event_IoServer_InnerRead_All(IxDeviceRemoteSwitch, ioValue);
	
	ixDeviceRemoteRun.autoConnectEnable = atoi(ioValue);
	if(ixDeviceRemoteRun.autoConnectEnable)
	{
		IxBuilderTcpClientConnect();
	}
	else
	{
		IxBuilderTcpClientDisconnect();
	}
}

void IxBuilderTcpClientConnectPolling(void)
{
	if(ixDeviceRemoteRun.autoConnectEnable)
	{
		IxBuilderTcpClientConnect();
	}
}

int GetIxBuilderTcpClientAutoConnectEnable(void)
{
	return ixDeviceRemoteRun.autoConnectEnable;
}


int Api_tcp_GetSn(char* net_dev_name, char *ip_str, int port, char* sn)
{
	int  err, num;
	int fd;

	MFG_SN_REQ recv_pkt;
	int recv_len;

	srand(time(NULL));
	
	// connect
	int retry_cnt = 4;

	for(retry_cnt = 4, err = -1; retry_cnt > 0 && err != 0; retry_cnt--)
	{
		err = tcp_client_connect_with_timeout(net_dev_name, ip_str, port, 5, &fd);
	}
	
	if(err == 0)
	{
		int setflag = 1;
		int flags = fcntl(fd, F_GETFL, 0);
		if (flags < 0) 
		{
			dprintf("Get flags error:%s\n", strerror(errno));
			close(fd);
			return -1;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(fd, F_SETFL, flags) < 0) 
		{
			dprintf("Set flags error:%s\n", strerror(errno));
			close(fd);
			return -1;
		}
		
 		struct timeval tv_out;
    	tv_out.tv_sec = 3;
    	tv_out.tv_usec = 0;
    	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		
		dprintf("connect ok! [server:%s]\n",ip_str);
	
		// send
		struct timeval tv;
		fd_set fds;
		int ret;
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;	// 100ms
		if((ret = select( fd + 1, NULL, &fds, NULL, &tv ))>0)
		{
			if( FD_ISSET( fd, &fds ) )
			{
				memcpy(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
				recv_pkt.head.cmd_type = MFG_SN_CLIENT_REQ;
				recv_pkt.head.cmd_id = 1;
				recv_pkt.head.dev_type = 0;		//0�ֻ���1����
				recv_pkt.head.data_len = 0;
				Api_tcp_client_send(fd, &recv_pkt, sizeof(recv_pkt.head));
			}
			else
			{
				close(fd);
				return -2;
			}
		}
		else
		{
			close(fd);
			return -3;
		}

		dprintf("1-------------------------------------------\n");


		
		// recv
		recv_len = sizeof(recv_pkt);
		memset(&recv_pkt, 0, sizeof(recv_pkt));

		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 3;
		tv.tv_usec = 0;
		if((ret = select( fd + 1, &fds,NULL, NULL, &tv ))>0)	
		{
			if( FD_ISSET( fd, &fds ) )
			{
				num = recv(fd, (char*)&recv_pkt, recv_len, MSG_DONTWAIT);
			}
			else
			{
				close(fd);
				return -4;
			}
			
		}
		else
		{
			close(fd);
			return -5;
		}

		dprintf("2-------------------------------------------\n");

		if (num > 0) // success
		{
			dprintf("recv_pkt.head.cmd_type=0x%04x, recv_pkt.result=%d\n", recv_pkt.head.cmd_type, recv_pkt.result);	
			if(!memcmp(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4) && recv_pkt.head.cmd_type == MFG_SN_SERVER_RSP && recv_pkt.result == 0)
			{
				memcpy(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
				recv_pkt.head.cmd_type = MFG_SN_CLIENT_ACK;
				recv_pkt.head.cmd_id = 2;
				recv_pkt.head.dev_type = 0;		//0�ֻ���1����
				recv_pkt.head.data_len = 8;
				Api_tcp_client_send(fd, &recv_pkt, sizeof(recv_pkt));
				memcpy(sn, recv_pkt.sn, 6);
				
				close(fd);
				return 0;
			}
		}
		else
		{
			dprintf("recv error\n");
		}
	}

	close(fd);
	return -6;
}
