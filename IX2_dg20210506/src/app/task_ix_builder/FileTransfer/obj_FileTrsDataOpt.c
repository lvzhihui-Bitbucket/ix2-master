#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>

#include "obj_FileTrsCmd.h"
#include "obj_FileTrsControl.h"
#include "obj_ResSync.h"


FileDataOptObj_Stru* FileTrsDataOpt_CreateNewWriteObj(char* dir, char *file_name,int file_len,int pack_len)
{
	FileDataOptObj_Stru *pdata_opt;
	char file_path[MAX_FILE_TRS_FILENAME_LEN*2 + 1];
	FILE *pf;
	
	pdata_opt = malloc(sizeof(FileDataOptObj_Stru));
	
	if(pdata_opt == NULL)
	{
		return NULL;
	}
	snprintf(file_path,MAX_FILE_TRS_FILENAME_LEN*2,"%s%s", dir, file_name);
	pf = fopen(file_path,"wb");

	if(pf == NULL)
	{
		free(pdata_opt);
		return NULL;
	}

	pdata_opt->pfile = pf;
	pdata_opt->file_len = file_len;
	pdata_opt->pack_len = pack_len;
	pdata_opt->pack_no = file_len/pack_len + ((file_len%pack_len)? 1:0);
	pdata_opt->cur_pack_no = 0;

	return pdata_opt;
}

FileDataOptObj_Stru* FileTrsDataOpt_CreateNewReadObj(char* dir, char *file_name,int file_len,int pack_len)
{
	FileDataOptObj_Stru *pdata_opt;
	char file_path[MAX_FILE_TRS_FILENAME_LEN*2 + 1];
	FILE *pf;
	
	pdata_opt = malloc(sizeof(FileDataOptObj_Stru));
	
	if(pdata_opt == NULL)
	{
		return NULL;
	}
	snprintf(file_path, MAX_FILE_TRS_FILENAME_LEN*2, "%s%s", dir, file_name);
	pf = fopen(file_path,"rd");

	if(pf == NULL)
	{
		free(pdata_opt);
		return NULL;
	}

	pdata_opt->pfile 			= pf;
	pdata_opt->file_len 		= file_len;
	pdata_opt->pack_len 		= pack_len;
	pdata_opt->pack_no 		= file_len/pack_len + ((file_len%pack_len)? 1:0);
	pdata_opt->cur_pack_no 	= 0;

	return pdata_opt;
}

int FileTrsDataOpt_DestroyOptObj(FileDataOptObj_Stru *data_opt)
{
	if(data_opt == NULL)
	{
		return 0;
	}
	fclose(data_opt->pfile);
	free(data_opt);

	return 0;
}

int FileTrsDataOpt_WriteOnePack(int cmd_id, FileDataOptObj_Stru *data_opt,int pack_no,int pack_len,unsigned char *data, int* packetNum)
{
	unsigned char result = 0;
	int server_ip,id,rid;
	char tempDisplay[41];
	
	if(data_opt == NULL)
	{
		return -1;
	}
	if(pack_no < data_opt->cur_pack_no)
	{
		goto WriteOnePack_Rsp;
	}
	if(pack_no > data_opt->cur_pack_no || pack_len > data_opt->pack_len 
		|| pack_no >= data_opt->pack_no || (pack_no < (data_opt->pack_no-1) &&  pack_len < data_opt->pack_len))
	{
		result = 1;
		goto WriteOnePack_Rsp;
	}
	#if 1		//fortrstest
	if(pack_len != fwrite(data,1,pack_len,data_opt->pfile))
	{
		result = 1;
		goto WriteOnePack_Rsp;
	}
	#else
	int fd = fileno(data_opt->pfile);
	write(fd,data,pack_len);
	#endif
	data_opt->cur_pack_no ++;
	*packetNum = data_opt->pack_no;

WriteOnePack_Rsp:
	if(FileTrsCtrl_GetTrsMsg_ByDataOptObj(data_opt, &server_ip,&id,&rid) != 0)
	{
		return -1;
	}
	
	FileTrs_SendOnePackRsp(cmd_id, server_ip,id,rid,result);
	return result;
}

//czn_20160922
int FileTrsDataOpt_ReadOnePack(int cmd_id, FileDataOptObj_Stru *data_opt, int pack_no)
{
	int server_ip,id,rid;
	int pack_len = 0;
	unsigned char result = 0;
	unsigned char *pdata_buf = NULL;

	if(data_opt == NULL)
	{
		return -1;
	}
	
	if(pack_no >= data_opt->pack_no)
	{
		result = 1;
		goto ReadOnePack_Rsp;
	}

	pdata_buf = malloc(data_opt->pack_len);
	if(pdata_buf == NULL)
	{
		result = 1;
		goto ReadOnePack_Rsp;
	}
	
	data_opt->cur_pack_no = pack_no;
	fseek(data_opt->pfile,pack_no*(data_opt->pack_len),SEEK_SET);
	pack_len = fread(pdata_buf,1,data_opt->pack_len,data_opt->pfile);
	
ReadOnePack_Rsp:
	if(FileTrsCtrl_GetTrsMsg_ByDataOptObj(data_opt, &server_ip,&id,&rid) != 0)
	{
		if(pdata_buf != NULL)
		{
			free(pdata_buf);
		}
		return -1;
	}
	
	FileTrs_ReadOnePackRsp(cmd_id, server_ip,id,rid,result,pack_no,pack_len,pdata_buf);
	
	if(pdata_buf != NULL)
	{
		free(pdata_buf);
	}
	return 0;
}

