#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include "task_IxProxy.h"
#include "obj_FileTrsCmd.h"
#include "obj_FileTrsControl.h"
#include "tcp_server_process.h"
#include "vtk_udp_stack_class.h"
#include "obj_ResSync.h"
#include "cJSON.h"

static int PublicFileTrsCmd_Process(int cmd_id, int server_ip, int cmd, int id, int rid, char* dat, int len)
{
	FileTrsCmd_SendReq_Stru *sendreq_dpt = (FileTrsCmd_SendReq_Stru *)dat;
	FileTrsCmd_SendReq_Stru2 *sendreq2_dpt = (FileTrsCmd_SendReq_Stru2 *)dat;

	FileTrsCmd_ReadReq_Stru2 *readreq2_dpt = (FileTrsCmd_ReadReq_Stru2 *)dat;

	FileTrsCmd_ReadOnePackReq_Stru *readonepack_dpt = (FileTrsCmd_ReadOnePackReq_Stru *)dat;

	FileTrsCmd_SendOnePack_Stru *sendonepack_dpt = (FileTrsCmd_SendOnePack_Stru *)dat;

	char saveDir[MAX_FILE_TRS_DIR_LEN]={0};
	char file_name[MAX_FILE_TRS_FILENAME_LEN]={0};		
	int md5_temp;
	int nameLen;

	int file_len;
	int pack_len;
	int pack_no;
	int result;
	int allPacket;
	
	//dprintf("!!!!!!!rev ip = %08x, id = %d, cmd = %04x, rid = %d, cmd_id=%d\n", server_ip, id, cmd, rid, cmd_id);
	
	switch(cmd)
	{
		case RSTRS_CMD_SENDREQ:
			file_len = (sendreq_dpt->file_length[0]<<24)|(sendreq_dpt->file_length[1]<<16)|(sendreq_dpt->file_length[2]<<8)|(sendreq_dpt->file_length[3]);
			pack_len = (sendreq_dpt->pack_length[0]<<8)|sendreq_dpt->pack_length[1];
			if(rid != FILE_MANAGE_RID)
			{
				nameLen = len - (sizeof(FileTrsCmd_PKT_S) - IP8210_BUFFER_MAX) - (sizeof(FileTrsCmd_SendReq_Stru) - FileReadFileNameLenMax) - 1;
				if(nameLen > 0 && sendreq_dpt->file_name[0] != 0)
				{
					memcpy(file_name, sendreq_dpt->file_name, nameLen);
					file_name[nameLen] = 0;
				}
				else
				{
					snprintf(file_name, MAX_FILE_TRS_FILENAME_LEN, "%d_%s.zip", rid, my_inet_ntoa(server_ip, saveDir));
				}
				snprintf(saveDir, MAX_FILE_TRS_DIR_LEN, "%s", IxDeviceTempFileDir);
			}
			else
			{
				cJSON *reqJson;
				//dprintf("jsonData=%s\n", sendreq2_dpt->jsonData);
				reqJson = cJSON_Parse(sendreq2_dpt->jsonData);
				if(reqJson != NULL)
				{
					snprintf(file_name, MAX_FILE_TRS_FILENAME_LEN, "%s", cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, "name")));
					snprintf(saveDir, MAX_FILE_TRS_DIR_LEN, "%s", cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, "dir")));
					cJSON_Delete(reqJson);
				}
			}

			DeleteFileProcess(IxDeviceTempFileDir, file_name);
			result = FileTrsCtrl_SendReq(cmd_id, server_ip,id,rid, saveDir, IxDeviceTempFileDir, file_name,sendreq_dpt->file_md5,file_len,pack_len);
			if(rid != FILE_MANAGE_RID && result == 0)
			{
				IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_DOWNLOADING, "0%%", rid);
			}

			//dprintf("RSTRS_CMD_SENDREQ saveDir=%s, file_name = %s\n",saveDir, file_name);
			break;

		case RSTRS_CMD_SENDONEPACK:
			pack_len = (sendonepack_dpt->pack_length[0]<<8)|sendonepack_dpt->pack_length[1];
			pack_no =(sendonepack_dpt->pack_no[0]<<8)|sendonepack_dpt->pack_no[1];
			result = FileTrsCtrl_SendOnePack(cmd_id, server_ip,id,rid,pack_no,pack_len,sendonepack_dpt->data, &allPacket);
			#if 0
			if(rid != FILE_MANAGE_RID && result == 0)
			{
				pack_no++;
				if(pack_no%10 == 0 || pack_no == allPacket)
				{
					snprintf(file_name, MAX_FILE_TRS_FILENAME_LEN, "%4.2f%%", (pack_no*100.0)/allPacket);
					IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_DOWNLOADING, file_name, rid);
				}
			}
			#endif
			break;

		case RSTRS_CMD_SENDEND:
			result = FileTrsCtrl_SendEnd(cmd_id, server_ip,id,rid, saveDir, file_name);	//czn_20170216
			dprintf("-------------------------------------------------------------- rid = %d\n", rid);
			if(rid != FILE_MANAGE_RID)
			{
				IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_DOWNLOADING, "100%%", rid);
				if(result == 2)
				{
					IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_ERR_DOWNLOAD, NULL, rid);
				}
				else if(result == 0)
				{
					if(server_ip == inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(server_ip))) || cmd_id == -1)
					{
						int rebootFlag = 0;
						
						IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_INSTALLING, NULL, rid);
						if(ResUpdateProcess(rid, saveDir, file_name, &rebootFlag) >= 0)
						{
							IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_INSTALL_OK, NULL, rid);
							if(rebootFlag)
							{
								WriteRebootFlag(server_ip, 1);
								SoftRestar();
							}
						}
						else
						{
							IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_INSTALL_FAIL, NULL, rid);
						}
					}
				}
			}
			break;

		case RSTRS_CMD_SENDCANCEL:
			result = FileTrsCtrl_SendCancel(cmd_id, server_ip,id,rid);
			if(result == 0 && rid != FILE_MANAGE_RID)
			{						
				IxDeviceFileDownloadReport(inet_addr(GetMyProxy_IP()), RES_DOWNLOAD_CANCEL, NULL, rid);
			}
			break;

		case RSTRS_CMD_READREQ:
			if(rid != FILE_MANAGE_RID)
			{
				FileTrsResFileSave(rid);
				GetLocalResById(rid, GetSysVerInfo_BdRmMs(), file_name);
				snprintf(saveDir, MAX_FILE_TRS_DIR_LEN, "%s", ResFileDir);
			}
			else
			{
				cJSON *reqJson;
				//dprintf("jsonData=%s\n", readreq2_dpt->jsonData);
				reqJson = cJSON_Parse(readreq2_dpt->jsonData);
				if(reqJson != NULL)
				{
					snprintf(file_name, MAX_FILE_TRS_FILENAME_LEN, "%s", cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, "name")));
					snprintf(saveDir, MAX_FILE_TRS_DIR_LEN, "%s", cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, "dir")));
					cJSON_Delete(reqJson);
				}
			}

			FileTrsCtrl_ReadReq(cmd_id, server_ip, id, rid, saveDir, IxDeviceTempFileDir, file_name);
			//dprintf("RSTRS_CMD_READREQ saveDir=%s, file_name = %s\n",saveDir, file_name);
			break;

		case RSTRS_CMD_READONEPACK:
			pack_no = (readonepack_dpt->pack_no[0]<<8)|readonepack_dpt->pack_no[1];
			FileTrsCtrl_ReadOnePack(cmd_id, server_ip,id,rid, pack_no);
			break;

		case RSTRS_CMD_READEND:
			FileTrsCtrl_ReadEnd(cmd_id, server_ip,id,rid);
			break;

		case RSTRS_CMD_READCANCEL:
			FileTrsCtrl_ReadCancel(cmd_id, server_ip,id,rid);
			break;
	}
	
	return 0;
}

int RemoteRsTrsCmd_Process(int cmd_id, char* pbuf, int len)
{
	FileTrsCmd_PKT_S* pPkt = (FileTrsCmd_PKT_S*)pbuf;
	
	int server_ip = pPkt->target.ip;
	int cmd = ntohs(pPkt->target.cmd);
	int id = ntohs(pPkt->target.id);
	int rid = (pPkt->dat[0] << 8) + pPkt->dat[1];
	
	PublicFileTrsCmd_Process(cmd_id, server_ip, cmd, id, rid, (char*)&(pPkt->dat), len - (sizeof(FileTrsCmd_PKT_S) - IP8210_BUFFER_MAX));
	return 0;
}

int LocalRsTrsCmd_Process(int server_ip,int cmd, int id,char *pkt , int len)
{
	RsTrsCmd_Head_Stru *cmd_head = (RsTrsCmd_Head_Stru*)pkt;
	int rid = (cmd_head->resid[0]<<8)|cmd_head->resid[1];
	int cmd_id = -1;

	PublicFileTrsCmd_Process(cmd_id, server_ip, cmd, id, rid, pkt, len);
	return 0;
}


int FileTrs_SendRsp_Remote_Udp_Common(int cmd_id, int server_ip, int rspcmd, int id, char* data, int len)
{
	int result;
	char temp[16];

	if(len > IP8210_BUFFER_MAX - 1)
	{
		return -1;
	}

	if(cmd_id == -1)
	{
		result = api_udp_io_server_send_rsp(server_ip, rspcmd, id, data, len);
	}
	else
	{
		FileTrsCmd_PKT_S sendPkt;
		int pktLen;
	
		sendPkt.head.start = 0xA1;
		sendPkt.head.type = 0xF1;
		
		sendPkt.target.ip = server_ip;
		sendPkt.target.id = htons(id);
		sendPkt.target.cmd = htons(rspcmd);

		memcpy(sendPkt.dat, data, len);
		
		sendPkt.dat[len++] = get_pack_checksum(&sendPkt.target.cmd, len+4);

		pktLen = sizeof(sendPkt) - IP8210_BUFFER_MAX + len;

		if(pktLen < 256)
		{
			sendPkt.head.len = pktLen & 0xFF;
			sendPkt.head.flag = 0x5A;
		}
		else
		{
			sendPkt.head.len = pktLen & 0xFF;
			sendPkt.head.flag = pktLen >> 8;
		}

		result = IxDeviceResponseRemoteUDP(cmd_id, &sendPkt, pktLen);
	}

	//dprintf("cmd_id=%d, result = %d, ip = %s, rspcmd = 0x%02x, len = %d\n", cmd_id, result, my_inet_ntoa(server_ip, temp), rspcmd, len);

	return result;
}

int FileTrs_SendRsp_Common(int cmd_id, int server_ip, int id,int rspcmd,int rid,unsigned char result)
{
	FileTrsCmd_SendRsp_Stru send_rsp;
	send_rsp.cmd_head.resid[0] = rid>>8;
	send_rsp.cmd_head.resid[1] = rid&0x0ff;
	//send_rsp.cmd_head.cmd = rspcmd;
	send_rsp.result = result;
	
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip, rspcmd, id, (char*)&send_rsp, sizeof(FileTrsCmd_SendRsp_Stru));		
	return 0;
}

//czn_20160919_s
int FileTrs_SendReqRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result,int pack_len)
{
	FileTrsCmd_SendReqRsp_Stru  send_req_rsp;
	send_req_rsp.cmd_head.resid[0] = rid>>8;
	send_req_rsp.cmd_head.resid[1] = rid&0x0ff;
	//send_rsp.cmd_head.cmd = rspcmd;
	send_req_rsp.result = result;
	send_req_rsp.pack_len[0] = pack_len>>8;
	send_req_rsp.pack_len[1] = pack_len;
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_SENDREQ|0x80,id,(char*)&send_req_rsp,sizeof(FileTrsCmd_SendReqRsp_Stru));
	return 0;
}
//czn_20160919_e

//czn_20160922_s
int FileTrs_ReadRsp(int cmd_id, int server_ip,int id,int rid,unsigned char agree,int file_length,int pack_length,unsigned char *md5_chk,char *file_name)
{
	int i;
	
	FileTrsCmd_ReadRsp_Stru rsp_cmd;
	int sendLen;
	
	sendLen = sizeof(FileTrsCmd_ReadRsp_Stru) - FileReadFileNameLenMax;

	rsp_cmd.cmd_head.resid[0] = rid>>8;
	rsp_cmd.cmd_head.resid[1] = rid&0xff;
	rsp_cmd.agree = agree;
	if(agree == 0)
	{
		//dprintf("!!!!!!!!!!RsTrs_ReadRsp 0:length=%d pack_length=%d %s\n",file_length,pack_length,file_name);
		rsp_cmd.file_length[0] = file_length>>24;
		rsp_cmd.file_length[1] = file_length>>16;
		rsp_cmd.file_length[2] = file_length>>8;
		rsp_cmd.file_length[3] = file_length&0xff;
		rsp_cmd.pack_length[0] = pack_length>>8;
		rsp_cmd.pack_length[1] = pack_length&0xff;
		
		for(i = 0;i < 16;i++)
		{
			rsp_cmd.file_md5[i] = md5_chk[i];
		}

		if(file_name != NULL)
		{
			snprintf(rsp_cmd.file_name, FileReadFileNameLenMax, "%s", file_name);
			sendLen += (strlen(rsp_cmd.file_name) + 1);
		}
	}
	else
	{
		dprintf("!!!!!!!!!!RsTrs_ReadRsp 1:fail\n");
		i = 0;
	}
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_READREQ|0x80,id,(char*)&rsp_cmd, sendLen);
	return 0;
}

int FileTrs_ReadOnePackRsp(int cmd_id, int server_ip,int id,int rid,unsigned char result,int pack_no,int pack_length,unsigned char *data)
{
	FileTrsCmd_ReadOnePackRsp_Stru *prsp_cmd;
	int sendLen = sizeof(FileTrsCmd_ReadOnePackRsp_Stru) - 1 + pack_length;

	prsp_cmd = malloc(sendLen);
	if(prsp_cmd == NULL)
	{
		return -1;
	}
	prsp_cmd->cmd_head.resid[0] = rid>>8;
	prsp_cmd->cmd_head.resid[1] = rid&0xff;
	prsp_cmd->result = result;
	prsp_cmd->pack_no[0] = pack_no>>8;
	prsp_cmd->pack_no[1] = pack_no&0xff;
	prsp_cmd->pack_length[0] = pack_length>>8;
	prsp_cmd->pack_length[1] = pack_length&0xff;
	
	memcpy(prsp_cmd->data,data,pack_length);

	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_READONEPACK|0x80,id,(char*)prsp_cmd, sendLen);

	free(prsp_cmd);
	
	return 0;
}

int FileTrs_ReadEndRsp(int cmd_id, int server_ip,int id,int rid)
{
	FileTrsCmd_ReadEndRsp_Stru rsp_cmd;
	rsp_cmd.cmd_head.resid[0] = rid>>8;
	rsp_cmd.cmd_head.resid[1] = rid&0xff;
	
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_READEND|0x80,id,(char*)&rsp_cmd,sizeof(FileTrsCmd_ReadEndRsp_Stru));
	return 0;
}

int FileTrs_ReadCancelRsp(int cmd_id, int server_ip,int id,int rid,unsigned  char result)
{
	FileTrsCmd_ReadCancelRsp_Stru rsp_cmd;
	rsp_cmd.cmd_head.resid[0] = rid>>8;
	rsp_cmd.cmd_head.resid[1] = rid&0xff;
	rsp_cmd.result = result;
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_READCANCEL|0x80,id,(char*)&rsp_cmd,sizeof(FileTrsCmd_ReadCancelRsp_Stru));
	return 0;
}
//czn_20160922_e

int FileTrs_FwUpdateRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result)
{
	FileTrsCmd_FwUpdateRsp rsp_cmd;
	rsp_cmd.cmd_head.resid[0] = rid>>8;
	rsp_cmd.cmd_head.resid[1] = rid&0xff;
	rsp_cmd.result = result;
	
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_FWUPDATEREQ|0x80,id,(char*)&rsp_cmd,sizeof(FileTrsCmd_FwUpdateRsp));
	
	return 0;
}

int FileTrs_GetFwVerRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result,int file_length,char *fw_ver,unsigned char *file_md5)
{
	FileTrsCmd_GetFwVerRsp_Stru rsp_cmd;
	rsp_cmd.cmd_head.resid[0] = rid>>8;
	rsp_cmd.cmd_head.resid[1] = rid&0xff;
	rsp_cmd.result = result;
	
	if(result == 0)
	{
		rsp_cmd.file_length[0] = file_length>>24;
		rsp_cmd.file_length[1] = file_length>>16;
		rsp_cmd.file_length[2] = file_length>>8;
		rsp_cmd.file_length[3] = file_length;
		memset(rsp_cmd.ver,0,18);
		strncpy(rsp_cmd.ver,fw_ver,18);
		memcpy(rsp_cmd.file_md5,file_md5,16);
	}
	
	FileTrs_SendRsp_Remote_Udp_Common(cmd_id, server_ip,RSTRS_CMD_GETFWVER|0x80,id,(char*)&rsp_cmd,sizeof(FileTrsCmd_GetFwVerRsp_Stru));
	
	return 0;
}


