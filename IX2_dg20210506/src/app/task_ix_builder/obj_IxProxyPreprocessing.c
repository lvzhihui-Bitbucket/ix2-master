
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>

#include "onvif_tiny_task.h"
#include "tcp_server_process.h"
#include "tcp_Client_process.h"
#include "cJSON.h"
#include "task_IoServer.h"
#include "task_IxProxy.h"
#include "obj_IxProxyPreprocessing.h"

int FillSendPackage(char* sendData, IX_PROXY_TCP_HEAD head, char* data, int dataLen)
{
	int len = 0;
	int i;
	
	head.CheckSUM = 0;

	if(data && dataLen)
	{
		if(len+dataLen > CLIENT_CONNECT_BUF_MAX)
		{
			dataLen = CLIENT_CONNECT_BUF_MAX - len;
		}
		
		for(i = 0, head.CheckSUM = 0; i < dataLen; i++)
		{
				head.CheckSUM += data[i];
		}
	}
	
	head.OP = htons(head.OP);
	head.Session_ID = htons(head.Session_ID);
	head.RSP_ID = htons(head.RSP_ID);
	head.Length = htons(sizeof(IX_PROXY_TCP_HEAD) + dataLen);
	head.CheckSUM = htons(head.CheckSUM);


	memcpy(sendData+len, &head.OP, sizeof(head.OP));
	len += sizeof(head.OP);
	
	memcpy(sendData+len, &head.Session_ID, sizeof(head.Session_ID));
	len += sizeof(head.Session_ID);
	
	memcpy(sendData+len, &head.RSP_ID, sizeof(head.RSP_ID));
	len += sizeof(head.RSP_ID);
	
	memcpy(sendData+len, &head.Length, sizeof(head.Length));
	len += sizeof(head.Length);
	
	memcpy(sendData+len, &head.CheckSUM, sizeof(head.CheckSUM));
	len += sizeof(head.CheckSUM);
	
	memcpy(sendData+len, data, dataLen);
	len += dataLen;

	return len;
}

int FillTransmitPackage(char* sendData, IX_PROXY_TRANSMIT_HEAD head, char* data, int dataLen)
{
	int len = 0;
	int i;
	
	head.CheckSUM = (head.SUB_OP >> 8) + (head.SUB_OP & 0x00FF);

	if(data && dataLen)
	{
		if(len+dataLen > CLIENT_CONNECT_BUF_MAX)
		{
			dataLen = CLIENT_CONNECT_BUF_MAX - len;
		}
		
		for(i = 0; i < dataLen; i++)
		{
				head.CheckSUM += data[i];
		}
	}
	
	head.OP = htons(head.OP);
	head.Session_ID = htons(head.Session_ID);
	head.RSP_ID = htons(head.RSP_ID);
	head.Length = htons(sizeof(IX_PROXY_TRANSMIT_HEAD) + dataLen);
	head.CheckSUM = htons(head.CheckSUM);
	head.SUB_OP = htons(head.SUB_OP);


	memcpy(sendData+len, &head.OP, sizeof(head.OP));
	len += sizeof(head.OP);
	
	memcpy(sendData+len, &head.Session_ID, sizeof(head.Session_ID));
	len += sizeof(head.Session_ID);
	
	memcpy(sendData+len, &head.RSP_ID, sizeof(head.RSP_ID));
	len += sizeof(head.RSP_ID);
	
	memcpy(sendData+len, &head.Length, sizeof(head.Length));
	len += sizeof(head.Length);
	
	memcpy(sendData+len, &head.CheckSUM, sizeof(head.CheckSUM));
	len += sizeof(head.CheckSUM);
	
	memcpy(sendData+len, &head.SUB_OP, sizeof(head.SUB_OP));
	len += sizeof(head.SUB_OP);

	memcpy(sendData+len, data, dataLen);
	len += dataLen;

//	dprintf("data=%s\n", data);

	return len;
}


int GetCheckSumResult(unsigned short sourceCheckSum, char* data, int dataLen)
{
	int len = 0;
	int i;
	int CheckSUM = 0;

	if(data && dataLen)
	{
		if(len+dataLen > CLIENT_CONNECT_BUF_MAX)
		{
			dataLen = CLIENT_CONNECT_BUF_MAX - len;
		}
		
		for(i = 0, CheckSUM = 0; i < dataLen; i++)
		{
				CheckSUM += data[i];
		}
	}
	else
	{
		sourceCheckSum = 0;
	}

	if(sourceCheckSum != CheckSUM)
	{
		printf("sourceCheckSum=0x%04X, CheckSUM=0x%04X\n", sourceCheckSum, CheckSUM);
	}

	return sourceCheckSum == CheckSUM ? 1 : 0;
}


void IxProxyRecvDataProcess(short businessCode, short cmd_id, char* data, int dataLen)
{
	char* businessData;
	int businessDataLen;

	businessData = data;
	businessDataLen = dataLen;

	#if 0
		int i;
		dprintf( "businessCode =%d, businessDataLen=%d\n", businessCode, businessDataLen);
		printf("businessData = ");
		for(i = 0; i < businessDataLen; i++)
			printf("%02x ", businessData[i]);
		printf("\n");
	#endif
	
	//原来的TCP指令
	if(businessCode == 0)
	{
		IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)businessData;
		char* cmdData = (char*)(businessData + sizeof(IX_PROXY_TCP_HEAD));
		int len = ntohs(phead->Length) - sizeof(IX_PROXY_TCP_HEAD);
		unsigned short OP = ntohs(phead->OP);
				
		if(GetCheckSumResult(ntohs(phead->CheckSUM), cmdData, len) == 0)
		{
			return;
		}

		switch(OP)
		{
			//心跳指令处理
			case REQ_HEARTBEAT:
			case REQ_LOGIN:
				break;
				
			default:
				if((OP & 0x0F00) == 0x0200)
				{
					API_IxProxy(businessCode, cmd_id, businessData, businessDataLen);
				}
				else if((OP & 0x0F00) == 0x0300)
				{
					API_IxProxyClient(businessData, businessDataLen);
				}
				break;
		}
	}
	
	//原来的UDP指令
	else if(businessCode == 1)
	{
#if 0
			int i;
			dprintf( "businessCode =%d, businessDataLen=%d\n", businessCode, businessDataLen);
			printf("businessData = ");
			for(i = 0; i < businessDataLen; i++)
				printf("%02x ", businessData[i]);
			printf("\n");
#endif
	
		RemoteRsTrsCmd_Process(cmd_id, businessData, businessDataLen);
	}
}


int ResponseClient(IX_PROXY_TCP_HEAD head, short cmd_id, short businessCode, char* pbuf, int len)
{
	int ret = -1;
	int i;
	char sendData[CLIENT_CONNECT_BUF_MAX];
	char* cmdData = (char*)pbuf;
	struct list_head *plist_client_connect_list;
	
	p_client_tcp_ins tcpClient;
	p_server_tcp_ins tcpServer;
	
	if(head.RSP_ID == 0)
	{
		len = FillSendPackage(sendData, head, NULL, 0);
	}
	else
	{
		len = FillSendPackage(sendData, head, cmdData, len);
	}

	tcpClient = GetIxProxyRemoteTcpFd();
		
	//发送到远程电脑
	if(tcpClient != NULL && tcpClient->fd > 0)
	{
		ProxyRemoteRspData(tcpClient->fd, cmd_id, businessCode, sendData, len);
	}
	//发送到本地电脑
	else
	{
		for(i = 0; i < 2; i++)
		{
			tcpServer = (i == 0 ? GetIxProxyLanTcpFd(NET_ETH0) : GetIxProxyLanTcpFd(NET_WLAN0));

			if(tcpServer != NULL && tcpServer->client_connected_cnt > 0)
			{
				list_for_each(plist_client_connect_list, &tcpServer->client_connect_list)
				{
					client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
					ret = client_connect_send_data(pnode, sendData, len);
				}
			}
		}
	}

	return ret;
}

int TransmitClient(IX_PROXY_TRANSMIT_HEAD head, char* pbuf, int len)
{
	int ret = -1;
	int i;
	char sendData[CLIENT_CONNECT_BUF_MAX];
	char* cmdData = (char*)pbuf;
	struct list_head *plist_client_connect_list;
	
	p_client_tcp_ins tcpClient;
	p_server_tcp_ins tcpServer;

	
	if(cmdData == NULL)
	{
		len = FillTransmitPackage(sendData, head, NULL, 0);
	}
	else
	{
		len = FillTransmitPackage(sendData, head, cmdData, len);
	}

	tcpClient = GetIxProxyRemoteTcpFd();

	//发送到远程电脑
	if(tcpClient != NULL && tcpClient->fd > 0)
	{
		ProxyRemoteTransferData(tcpClient->fd, 0, sendData, len);
	}
	//发送到本地电脑
	else
	{
		for(i = 0; i < 2; i++)
		{
			tcpServer = (i == 0 ? GetIxProxyLanTcpFd(NET_ETH0) : GetIxProxyLanTcpFd(NET_WLAN0));
			
			if(tcpServer != NULL && tcpServer->client_connected_cnt > 0)
			{
				list_for_each(plist_client_connect_list,&tcpServer->client_connect_list)
				{
					client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
					ret = client_connect_send_data(pnode, sendData, len);
				}
			}
		}
	}
	
	return ret;
}

int IxDeviceResponseRemoteUDP(short cmd_id, char* pbuf, int len)
{
	p_client_tcp_ins tcpClient;

	tcpClient = GetIxProxyRemoteTcpFd();

	
	//发送到远程电脑
	if(tcpClient != NULL && tcpClient->fd > 0)
	{
#if 0
				int i;
				dprintf( "cmd_id =%d, businessCode =%d, len=%d\n", cmd_id, 1, len);
				printf("pbuf = ");
				for(i = 0; i < len; i++)
					printf("%02x ", pbuf[i]);
				printf("\n");
#endif
		ProxyRemoteRspData(tcpClient->fd, cmd_id, 1, pbuf, len);
	}
	
	return 0;
}

