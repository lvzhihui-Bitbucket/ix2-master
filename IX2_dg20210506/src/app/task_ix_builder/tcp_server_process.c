
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>

#include "onvif_tiny_task.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "task_IoServer.h"
#include "task_IxProxy.h"

int client_connect_send_data( void* arg, char* pbuf, int len )
{
	client_tcp_connect_node* ptr_node = (client_tcp_connect_node*)arg;

	int ret;
	fd_set wfds;
	struct timeval cwtv;

	FD_ZERO(&wfds);
	cwtv.tv_sec = 3;
	cwtv.tv_usec = 0;
	
	FD_SET(ptr_node->data.client_fd, &wfds);
	if( select_ex(ptr_node->data.client_fd + 1, 0, &wfds, 0, &cwtv) == -1 )
	{
		printf("select call failure!msg:%s\n", strerror(errno));
		return -1;
	}
	if( FD_ISSET(ptr_node->data.client_fd, &wfds) )
	{			
		//CMD_RSP_DELAY();
		ret = send(ptr_node->data.client_fd, pbuf, len, 0);
		if( ret < 0 )
		{
			printf("send failure!msg:%s\n", strerror(errno));
			return -1;
		}
	}
	return 0;		
}

int ParseSignInProxyObject(const char* json, PROXY_SIGN_IN_JSON* pObj)
{
    int status = 0;

	memset(pObj, 0, sizeof(PROXY_SIGN_IN_JSON));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *proxyObject = cJSON_Parse(json);
    if (proxyObject == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	/* 获取名为“NAME”的值 */
	ParseJsonString(proxyObject, "NAME", pObj->name, IX_BUILDER_NAME_LEN-1);

	/* 获取名为“ACCOUNT”的值 */
	ParseJsonString(proxyObject, "ACCOUNT", pObj->account, IX_BUILDER_NAME_LEN-1);

	/* 获取名为“PASSWORD”的值 */
	ParseJsonString(proxyObject, "PASSWORD", pObj->password, IX_BUILDER_NAME_LEN-1);

    status = 1;

end:
	
    cJSON_Delete(proxyObject);
	
    return status;
}

void client_connect_recv_data_process( void* arg)
{
	client_tcp_connect_node* ptr_node = (client_tcp_connect_node*)arg;
	
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)ptr_node->data.r_buf_ptr;
	char* cmdData = (char*)(ptr_node->data.r_buf_ptr + sizeof(IX_PROXY_TCP_HEAD));
	int len = ntohs(phead->Length) - sizeof(IX_PROXY_TCP_HEAD);
	IX_PROXY_TCP_HEAD head;
	unsigned short OP = ntohs(phead->OP);
	
	char addr_p[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &ptr_node->data.client_ip, addr_p, sizeof(addr_p));
	char sendData[CLIENT_CONNECT_BUF_MAX];

	dprintf("OP = 0x%04X, Session_ID = 0x%04X, RSP_ID = 0x%04X, Length = %d, CheckSUM = 0x%04X\n", OP, ntohs(phead->Session_ID), ntohs(phead->RSP_ID), ntohs(phead->Length), ntohs(phead->CheckSUM));
	
	switch(OP)
	{
		//心跳指令处理
		case REQ_HEARTBEAT:
			if(GetCheckSumResult(ntohs(phead->CheckSUM), cmdData, len) == 0)
			{
				break;
			}

			head.OP = RSP_HEARTBEAT;
			head.Session_ID = ntohs(phead->Session_ID);
			head.RSP_ID = ntohs(phead->RSP_ID);
			
			len = FillSendPackage(sendData, head, NULL, 0);

			client_connect_send_data( arg, sendData, len);
			ptr_node->data.client_cmd_to = 0;					
			break;
		case REQ_LOGIN:
			if(ptr_node->data.state == WAITING_ROR_SIGN_IN && !IsMyProxyLinked())
			{
				PROXY_SIGN_IN_JSON signInData;
				char signInResult;
				char account1[IX_BUILDER_NAME_LEN];
				char account2[IX_BUILDER_NAME_LEN];
				char password1[IX_BUILDER_NAME_LEN];
				char password2[IX_BUILDER_NAME_LEN];
				
				if(GetCheckSumResult(ntohs(phead->CheckSUM), cmdData, len) == 0)
				{
					break;
				}

				ptr_node->data.client_cmd_to = 0;					

				
				head.OP = RSP_LOGIN;
				head.Session_ID = ntohs(phead->Session_ID);
				head.RSP_ID = ntohs(phead->RSP_ID);

				ParseSignInProxyObject(cmdData, &signInData);
				
				API_Event_IoServer_InnerRead_All(IX_BUILDER_ACCOUNT1, account1);
				API_Event_IoServer_InnerRead_All(IX_BUILDER_PWD1, password1);
				API_Event_IoServer_InnerRead_All(IX_BUILDER_ACCOUNT2, account2);
				API_Event_IoServer_InnerRead_All(IX_BUILDER_PWD2, password2);

				//dprintf("signInData.account=%s, signInData.password=%s\naccount1=%s, password1=%s\naccount2=%s, password2=%s\n", signInData.account, signInData.password, account1, password1, account2, password2);
				if(!strcmp(signInData.account, account1) && !strcmp(signInData.password, password1))
				{
					ptr_node->data.state = TCP_SERVER_IDLE;
					
					sprintf(sendData, "%s@%s", signInData.name, addr_p); 
					SetMyProxyData(NULL, NULL, sendData, GetNetDeviceNameByTargetIp(inet_addr(addr_p)));

					signInResult = 0;
					
					len = FillSendPackage(sendData, head, (char*)&signInResult, sizeof(signInResult));
					
					client_connect_send_data( arg, sendData, len);
				}
				else if(!strcmp(signInData.account, account2) && !strcmp(signInData.password, password2))
				{
					ptr_node->data.state = TCP_SERVER_IDLE;

					sprintf(sendData, "%s@%s", signInData.name, addr_p); 
					SetMyProxyData(NULL, NULL, sendData, GetNetDeviceNameByTargetIp(inet_addr(addr_p)));
					
					signInResult = 0;
					
					len = FillSendPackage(sendData, head, (char*)&signInResult, sizeof(signInResult));

					client_connect_send_data( arg, sendData, len);
				}
				else
				{
					SetMyProxyData(NULL, NULL, "0", NULL);

					signInResult = 1;
					
					len = FillSendPackage(sendData, head, (char*)&signInResult, sizeof(signInResult));

					client_connect_send_data( arg, sendData, len);

					one_tiny_task_stop(&ptr_node->data.socket_task);
				}
			}
			break;
			
		default:	
			if(ptr_node->data.state == TCP_SERVER_IDLE)
			{
				ptr_node->data.client_cmd_to = 0;
				IxProxyRecvDataProcess(0, 0, ptr_node->data.r_buf_ptr, ptr_node->data.r_dat_len);
			}
			break;
	}
	// processing data...

	
}


//--------------------client connecting process--------------------------------------------------------------------------------------------
static int client_connect_socket_task_init(void* arg)
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	client_tcp_connect_node* ptr_node = (client_tcp_connect_node*)ptiny_task->pusr_dat;

	ptr_node->data.ptr_user_data 		= NULL;
	ptr_node->data.ptr_user_callback 	= NULL;
	ptr_node->data.client_cmd_to		= 0;
	ptr_node->data.state				= WAITING_ROR_SIGN_IN;
	
	SetMyProxyData(NULL, NULL, "0", NULL);
	
	return 0;	
}

static void client_connect_socket_task_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	client_tcp_connect_node* ptr_node = (client_tcp_connect_node*)ptiny_task->pusr_dat;

	int ret;	
	fd_set fds;
	struct timeval tv;		
    while( one_tiny_task_is_running(&ptr_node->data.socket_task) )
	{
		FD_ZERO(&fds);
		FD_SET(ptr_node->data.client_fd, &fds); 
		tv.tv_sec  = 1;
		tv.tv_usec = 0;	

		ret = select_ex(ptr_node->data.client_fd + 1, &fds, 0, 0, &tv);
		if( ret == -1)
		{
			printf("client_connect_socket_task_process!msg:%s\n", strerror(errno));
			continue;
		}
		else if( ret != 0 )
		{
			if( FD_ISSET(ptr_node->data.client_fd, &fds) )
			{		
				bzero(ptr_node->data.r_buf_ptr, ptr_node->data.r_buf_max );
				ptr_node->data.r_dat_len = recv(ptr_node->data.client_fd, ptr_node->data.r_buf_ptr, ptr_node->data.r_buf_max, 0);

				if( ptr_node->data.r_dat_len < 0)
				{
					if( (ptr_node->data.r_dat_len == -1) && (errno == EAGAIN) )
					{
						//printf("timeout\n");
						//(*ptr_node->data.callback)(ptr_node,1);
					}
					else
					{
						one_tiny_task_stop(&ptr_node->data.socket_task);
					}
				}							
				else if( ptr_node->data.r_dat_len == 0 )
				{
					printf("client is active disconnected!\n");
					one_tiny_task_stop(&ptr_node->data.socket_task);
				}
				//data process
				else
				{
					(*ptr_node->data.callback)(ptr_node);
				}									
			}
		}
		else if( ret == 0 )
		{
			char addr_p[INET_ADDRSTRLEN];

			ptr_node->data.client_cmd_to++;

			inet_ntop(AF_INET, &ptr_node->data.client_ip, addr_p, sizeof(addr_p));

			//printf("client IP is %s(fd=%d), timer running...%ds\n", addr_p,ptr_node->data.client_fd, ptr_node->data.client_cmd_to);
			 
			if(ptr_node->data.state == WAITING_ROR_SIGN_IN)
			{
				// 10秒内还没有数据包通信则关闭客户端的连接
				if(ptr_node->data.client_cmd_to >= 10)
				{
					ptr_node->data.client_cmd_to = 0;
					one_tiny_task_stop(&ptr_node->data.socket_task);	
				}
			}
			else if(ptr_node->data.state == TCP_SERVER_IDLE)
			{
				// 60秒内还没有数据包通信则关闭客户端的连接
				if(ptr_node->data.client_cmd_to >= 60)
				{
					ptr_node->data.client_cmd_to = 0;
					one_tiny_task_stop(&ptr_node->data.socket_task);	
				}
			}

		}
    }	
}

static void client_connect_socket_task_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	client_tcp_connect_node* ptr_node = (client_tcp_connect_node*)ptiny_task->pusr_dat;

	p_server_tcp_ins ptr_ins = (p_server_tcp_ins)ptr_node->data.server_ins;

	ptr_ins->client_connected_cnt--;
	printf("client(fd=%d) is discnnected from server, remain clients(%d)!!\n",ptr_node->data.client_fd,ptr_ins->client_connected_cnt);
	
	free(ptr_node->data.r_buf_ptr);
	ptr_node->data.r_buf_ptr = NULL;

	if( ptr_node->data.ptr_user_data )
	{
		if( ptr_node->data.ptr_user_callback )
		{
			(*ptr_node->data.ptr_user_callback)(ptr_node->data.ptr_user_data);
		}
		free(ptr_node->data.ptr_user_data);
		ptr_node->data.ptr_user_data = NULL;
	}
	
	list_del_init(&ptr_node->list);

	close( ptr_node->data.client_fd );

	if(IsMyProxyLinked())
	{
		SetMyProxyData(NULL, NULL, "0", NULL);
	}
	
	free( ptr_node );
	ptr_node = NULL;
}


int new_one_client_connect_node( p_server_tcp_ins ptr_ins, int client_ip, int client_pt, int client_fd )
{
	// create node data
	client_tcp_connect_node* pnew_node = malloc(sizeof(client_tcp_connect_node));

	if( pnew_node == NULL )
	{
		printf("new_one_client_connect_node fail 1...\n");
		return -1;
	}
	pnew_node->data.server_ins 	= ptr_ins;
	pnew_node->data.client_fd 	= client_fd;
	pnew_node->data.client_ip 	= client_ip;
	pnew_node->data.client_pt 	= client_pt;	
	pnew_node->data.r_buf_max 	= CLIENT_CONNECT_BUF_MAX;
	pnew_node->data.r_buf_ptr 	= (char*)malloc(CLIENT_CONNECT_BUF_MAX);
	pnew_node->data.callback	= client_connect_recv_data_process;

	if( one_tiny_task_start( &pnew_node->data.socket_task, client_connect_socket_task_init, client_connect_socket_task_process, client_connect_socket_task_exit, (void*)pnew_node ) == -1 )
	{		
		printf("new_one_client_connect_node fail 2...\n");
		if( pnew_node->data.r_buf_ptr )
		{
			free(pnew_node->data.r_buf_ptr);
			pnew_node->data.r_buf_ptr = NULL;
		}
		free(pnew_node);
		pnew_node = NULL;
		return -1;
	}		
	list_add_tail(&pnew_node->list,&ptr_ins->client_connect_list);	
	return 0;
}

int del_one_client_connect_node( p_server_tcp_ins ptr_ins, int client_fd )
{
	struct list_head *plist_client_connect_list;

	list_for_each(plist_client_connect_list,&ptr_ins->client_connect_list)
	{
		client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
		// free node data
		if( pnode->data.client_fd == client_fd )
		{
			// free node data
			printf("del_one_client_connect_node[%d]...\n",client_fd);	
			one_tiny_task_stop(&pnode->data.socket_task);	
			// 等待内存释放
			usleep(100*1000);
			if( pnode != NULL )
				usleep(100*1000);
			return 1;
		}
	}
	return 0;	
}	

int del_all_client_connect_node( p_server_tcp_ins ptr_ins )
{
	struct list_head *plist_client_connect_list,*plist_temp;

	list_for_each_safe(plist_client_connect_list,plist_temp,&ptr_ins->client_connect_list)
	{
		client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
		if( pnode == NULL ) continue;
		// free node data
		printf("del_all_client_connect_node...\n");
		one_tiny_task_stop(&pnode->data.socket_task);	
		// 等待内存释放
		usleep(100*1000);
		if( pnode != NULL )
			usleep(100*1000);
	}
	return 0;
}

//--------------------server listenning process-------------------------------------------------------------------------------------------------
static int server_socket_task_init(void* arg)
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_server_tcp_ins ptr_ins = (p_server_tcp_ins)ptiny_task->pusr_dat;

	INIT_LIST_HEAD(&ptr_ins->client_connect_list); 

	ptr_ins->client_connected_cnt = 0;
	return 0;	
}

static void server_socket_task_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_server_tcp_ins ptr_ins = (p_server_tcp_ins)ptiny_task->pusr_dat;

	printf("server_socket_task_process...\n");

	int client_counter = 0;

	int ret;
	fd_set fds;
	struct timeval tv;	
    while(one_tiny_task_is_running(&ptr_ins->socket_task))
    {
		FD_ZERO(&fds);
		FD_SET(ptr_ins->server_fd, &fds); 
		tv.tv_sec  = 10;
		tv.tv_usec = 0;			
		ret = select_ex(ptr_ins->server_fd + 1, &fds, 0, 0, &tv);
		if( ret == -1)
		{
			printf("server_socket_task_process failure!msg:%s\n", strerror(errno));
			continue;
		}
		else if( ret != 0 )
		{
			if( FD_ISSET(ptr_ins->server_fd, &fds) )
			{
				int client_fd = 0;
				
				struct sockaddr_in cin;
				socklen_t len;
				char addr_p[INET_ADDRSTRLEN];
				
				if((client_fd = accept(ptr_ins->server_fd, (struct sockaddr *) &cin, &len)) == -1)
				{
					perror("server waiting for client timeout,continue wait...\n");
					continue;
				}
				else
				{
					if(ptr_ins->client_connected_cnt >= 1)
					{
						printf("Connected cnt = %d, please disconnect the client and try again!\n", ptr_ins->client_connected_cnt);
						close(client_fd);
						continue;
					}

					ptr_ins->client_connected_cnt++;
					inet_ntop(AF_INET, &cin.sin_addr, addr_p, sizeof(addr_p));
					printf("accept one client: ip is %s, port is %d, fd=%d, total clients(%d)\n", addr_p, ntohs(cin.sin_port),client_fd,ptr_ins->client_connected_cnt );			
					if( new_one_client_connect_node(ptr_ins,cin.sin_addr.s_addr,ntohs(cin.sin_port),client_fd) == 0 )
						printf("add one client[%s] to server list ok!!\n",addr_p);
					else			
						printf("add one client[%s] to server list er!!\n",addr_p);
				}		
			}
		}
		else if( ret == 0 )
		{
			IxBuilderTcpClientConnectPolling();
			//printf("waiting for client connecting,10s timeout...\n");			
		}
    }
}

static void server_socket_task_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	p_server_tcp_ins ptr_ins = (p_server_tcp_ins)ptiny_task->pusr_dat;

	printf("server_socket_task_exit!!\n");

	close( ptr_ins->server_fd );

	del_all_client_connect_node(ptr_ins);

	free( ptr_ins );
	ptr_ins = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static p_server_tcp_ins pIxBuilderTcpServer = NULL;
static p_server_tcp_ins pIxBuilderTcpServer2 = NULL;


p_server_tcp_ins GetIxProxyLanTcpFd(char* net_dev_name)
{
	if(!strcmp(NET_WLAN0, net_dev_name))
	{
		return pIxBuilderTcpServer;
	}
	else if(!strcmp(NET_ETH0, net_dev_name))
	{
		return pIxBuilderTcpServer2;
	}
	else
	{
		return NULL;
	}
}


static p_server_tcp_ins api_start_one_tcp_server_listen(const char* net_dev_name, int server_port )
{	
	p_server_tcp_ins	ptr_ins;
	ptr_ins = (p_server_tcp_ins)malloc(sizeof(server_tcp_ins));
	struct ifreq ifr;

	if( ptr_ins == NULL )
		return NULL;
		
	ptr_ins->server_port	= server_port;	

	struct sockaddr_in sin;
	bzero(&sin, sizeof(sin));	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(server_port);


	if( (ptr_ins->server_fd = socket(AF_INET,SOCK_STREAM,0)) == -1 )
	{
		printf("socket open failure,msg:%s\n",strerror(errno));
		goto error_exit;
	}
#if 1
	int m_loop = 1;

	if( setsockopt(ptr_ins->server_fd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		printf("Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n");
		close(ptr_ins->server_fd);
		goto error_exit;
	}


	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(ptr_ins->server_fd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		close(ptr_ins->server_fd);
		goto error_exit;
	}

	/*设置套接字为阻塞*/
	int flags = fcntl(ptr_ins->server_fd, F_GETFL, 0);
	if (flags < 0) 
	{
		printf("Get flags error:%s\n", strerror(errno));
		close(ptr_ins->server_fd);		
		goto error_exit;
	}
	flags &= ~O_NONBLOCK;
	if (fcntl(ptr_ins->server_fd, F_SETFL, flags) < 0) 
	{
		printf("Set flags error:%s\n", strerror(errno));
		close(ptr_ins->server_fd);		
		goto error_exit;
	}	
#endif
	if(bind(ptr_ins->server_fd, (struct sockaddr *) &sin, sizeof(sin)) == -1)
	{
		perror("fail to bind\n");
		close(ptr_ins->server_fd);		
		goto error_exit;
	}
	
	if( listen(ptr_ins->server_fd, 100) == -1 )
	{
		perror("fail to listen\n");
		close(ptr_ins->server_fd);		
		goto error_exit;
	}

	printf( "%s(%d)-<%s>,ptr_ins=%d,&ptr_ins->socket_task=%d\n", __FILE__, __LINE__, __FUNCTION__,ptr_ins,&ptr_ins->socket_task );		

	// initial thread
	if( one_tiny_task_start( &ptr_ins->socket_task, server_socket_task_init, server_socket_task_process, server_socket_task_exit, (void*)ptr_ins ) == -1 )
	{
		close(ptr_ins->server_fd);		
		goto error_exit;
	}
	
	return ptr_ins;

	error_exit:
	if( ptr_ins != NULL )
	{
		free( ptr_ins );
		ptr_ins = NULL;
		printf("free ptr_ins ok!\n");		
	}
	return NULL;		
}


static int api_stop_one_tcp_server_listen( p_server_tcp_ins ptr_ins )
{
	if( ptr_ins == NULL )
		return -1;

	// 等待线程结束
	one_tiny_task_stop( &ptr_ins->socket_task );

	// 等待内存释放
	while( ptr_ins != NULL )
	{
		usleep(100*1000);
	}
	return 0;
}

void IxBuilderTcpServerInit(void)
{
	IxDeviceRemoteInit();
	pIxBuilderTcpServer = api_start_one_tcp_server_listen(NET_WLAN0, IX_BUILDER_TCP_PORT);
	pIxBuilderTcpServer2 = api_start_one_tcp_server_listen(NET_ETH0, IX_BUILDER_TCP_PORT);
	IxBuilderTcpClientSwitchLoad();
}


int tcp_client_disconnect(void)
{
	int ret = -1;

	struct list_head *plist_client_connect_list;

	list_for_each(plist_client_connect_list,&pIxBuilderTcpServer->client_connect_list)
	{
		client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
		
		pnode->data.client_cmd_to = 0;
		ret = one_tiny_task_stop(&pnode->data.socket_task);					
	}
	
	list_for_each(plist_client_connect_list,&pIxBuilderTcpServer2->client_connect_list)
	{
		client_tcp_connect_node* pnode = list_entry(plist_client_connect_list,client_tcp_connect_node,list);
		
		pnode->data.client_cmd_to = 0;
		ret = one_tiny_task_stop(&pnode->data.socket_task);					
	}
	
	return ret;
}



