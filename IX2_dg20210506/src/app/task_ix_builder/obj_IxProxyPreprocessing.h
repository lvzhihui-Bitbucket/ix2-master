

#ifndef _obj_IxProxyPreprocessing_H_
#define _obj_IxProxyPreprocessing_H_


int FillSendPackage(char* sendData, IX_PROXY_TCP_HEAD head, char* data, int dataLen);
int FillTransmitPackage(char* sendData, IX_PROXY_TRANSMIT_HEAD head, char* data, int dataLen);
int GetCheckSumResult(unsigned short sourceCheckSum, char* data, int dataLen);


void IxProxyRecvDataProcess(short businessCode, short cmd_id, char* data, int dataLen);
int ResponseClient(IX_PROXY_TCP_HEAD head, short cmd_id, short businessCode, char* pbuf, int len);
int TransmitClient(IX_PROXY_TRANSMIT_HEAD head, char* pbuf, int len);

#endif


