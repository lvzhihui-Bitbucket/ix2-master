

#ifndef _TCP_SERVER_PROCESS_H_
#define _TCP_SERVER_PROCESS_H_

#include "list.h"
#include "onvif_tiny_task.h"


#define CLIENT_CONNECT_BUF_MAX					(1024+128)
#define IX_BUILDER_NAME_LEN						33
#define IX_DEVICE_JSON_DATA_LEN_MAX				(1024)

#define IX_BUILDER_TCP_PORT						8900			// 组播播搜索所有设备
#define DEVICE_MAX_NUM							50
#define CMD_RSP_DELAY()							usleep(100*1000)


typedef enum
{
	 WAITING_ROR_SIGN_IN = 0,
	 TCP_SERVER_IDLE,
}CLIENT_TCP_CONNECT_STATE_E;

// server service
typedef struct _server_tcp_ins_
{	
	int						server_fd;
	int						server_port;
	one_tiny_task_t 		socket_task;
	int						client_connected_cnt;
	struct list_head 		client_connect_list;		
} server_tcp_ins, *p_server_tcp_ins;

typedef void (*client_dat_process)( void* arg);
typedef void (*client_user_process)( void* arg );

typedef struct _client_tcp_connect_
{
	p_server_tcp_ins				server_ins;
	int								client_ip;
	int								client_fd;	
	int								client_pt;	
	int								client_cmd_to;
	int								r_buf_max;
	char* 							r_buf_ptr;
	int 							r_dat_len;	
	client_dat_process				callback;
	one_tiny_task_t 				socket_task;
	char*							ptr_user_data;			// 用户数据指针
	client_user_process 			ptr_user_callback;		// 用户数据回调函数
	CLIENT_TCP_CONNECT_STATE_E 		state;	
} client_tcp_connect, *p_client_tcp_connect;

typedef struct
{ 
	client_tcp_connect		data;		
 	struct list_head 		list;
} client_tcp_connect_node;

typedef unsigned short tcp_op;


/*
{
    "NAME":"caozhenbin",
    "ACCOUNT":"admin",
    "PASSWORD":"admin"
};
*/


typedef struct 
{
	char name[IX_BUILDER_NAME_LEN];
	char account[IX_BUILDER_NAME_LEN];
	char password[IX_BUILDER_NAME_LEN];
} PROXY_SIGN_IN_JSON;


static p_server_tcp_ins api_start_one_tcp_server_listen(const char* net_dev_name, int server_port );
static int api_stop_one_tcp_server_listen( p_server_tcp_ins ptr_ins );

void IxBuilderTcpServerInit(void);
int tcp_client_disconnect(void);
int client_connect_send_data( void* arg, char* pbuf, int len );



#endif


