/**
  ******************************************************************************
  * @file    hal_gpio_def.h
  * @author  jia
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the define of GPIO use.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _hal_gpio_def_H_
#define _hal_gpio_def_H_

#if 0
#define MIC_MUTE_SET					_IOW('G', 10, unsigned int)
#define SPK_MUTE_SET					_IOW('G', 11, unsigned int)
#define BACK_LIGHT_1_SET				_IOW('G', 12, unsigned int)
#define BACK_LIGHT_2_SET				_IOW('G', 13, unsigned int)
#define LED_1_SET						_IOW('G', 14, unsigned int)
#define LED_2_SET						_IOW('G', 15, unsigned int)
#define LED_3_SET						_IOW('G', 16, unsigned int)
#define RL_CON_SET						_IOW('G', 17, unsigned int)
#define DET_CH_SET						_IOW('G', 18, unsigned int)

#define PWN_OUT_SET					_IOW('G', 19, unsigned int)

#else

#define TOUCH_KEY_RESET				_IO('G', 50)
#define TOUCH_CHECH_ENABLE			_IOW('G', 51, unsigned int)

#define BACK_LIGHT_ENABLE			_IOW('G', 10, unsigned int)

#define PWN_OUT_SET				_IOW('G', 11, unsigned int)

#define VDD12_ENABLE				_IOW('G', 12, unsigned int)

#define AMP_MUTE_ENABLE			_IOW('G', 13, unsigned int)

#define AP324_ENABLE				_IOW('G', 14, unsigned int)
#define MIC_MUTE_ENABLE			_IOW('G', 15, unsigned int)

#define POW324_ENABLE				_IOW('G', 16, unsigned int)
#define NT329_MUTE_ENABLE			_IOW('G', 17, unsigned int)

#define LED_TALK_SET				_IOW('G', 18, unsigned int)
#define LED_UNLOCK_SET				_IOW('G', 19, unsigned int)

#define RL_CON_ENABLE				_IOW('G', 20, unsigned int)

//#define DET_CH_SET					_IOW('G', 22, unsigned int)          //lyx 20170502
#define LED_POWER_SET				_IOW('G', 21, unsigned int)     
#define LED_MENU_SET			        _IOW('G', 22, unsigned int)  
#define LED_UP_SET					_IOW('G', 23, unsigned int)	
#define LED_DOWN_SET				_IOW('G', 24, unsigned int)	
#define LED_BACK_SET					_IOW('G', 25, unsigned int)
#define LED_NO_DISTURB_SET			_IOW('G', 26, unsigned int)	

#define NJW1124_DIR_SET                        _IOW('G', 27, unsigned int)      //lyx 20170502


typedef struct                            //lyx 20170504
{
	unsigned char family_code;             //8bit
	unsigned char serial_number[6];      //48bit
	unsigned char crc_code;                  //8bit
}DS2411_SN;


#define DS2411_SN_GET				_IOR('G', 28,  DS2411_SN)	

#define PT2259_SET					_IOW('G', 29, unsigned int)      //lyx 20170720


//lyx_20170921_s
#define LED_RED_CTL					_IOW('G', 30, unsigned int)     
#define LED_GREEN_CTL				_IOW('G', 31, unsigned int) 
#define LED_BLUE_CTL					_IOW('G', 32, unsigned int) 

#define LCD_VCOM_CTL				_IOW('G', 33, unsigned int) 
#define EXT_RING_CTL					_IOW('G', 34, unsigned int) 
//lyx_20170921_e



#endif


#endif

