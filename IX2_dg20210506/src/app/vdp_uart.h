
#ifndef VDP_UART_H
#define  VDP_UART_H

// 1 = STM8接收到总线指令,向N329发送; - 总线指令包
#define UART_TYPE_S2N_REC_DT_CMD				1

// 2 = N329向STM8申请:发送指令 - macType+uNew(新旧指令)+总线指令包
#define UART_TYPE_N2S_SND_DT_CMD				2

// 3 = STM8执行发送指令,  返回发送结果给N329 - * 发送结果
#define UART_TYPE_S2N_SND_RESULT				3

// 4 = N329向STM8申请: 控制PT2259 - 通道选择(0=MIC; 1=SPK)+衰减DB数
#define UART_TYPE_N2S_PT2259_CTRL				4

// 5 = N329向STM8申请: ExtRinger - 0=OFF; 1=ON
#define UART_TYPE_N2S_EXTRINGER_CTRL			5

// 6 = STM8向N329报告:   DIP状态(改变) - 5位DIP开关的值
#define UART_TYPE_S2N_DIP_STATUS				6

// 7 = STM8向N329报告:   DoorBell状态(改变) - 0=释放; 1=按下 
#define UART_TYPE_S2N_DOORBELL_STATUS			7

// 8 = N329向STM8索取DIP状态
#define UART_TYPE_N2S_APPLY_DIP_STATUS			8

// 9 = STM8向N329应答:   DIP状态  - 5位DIP开关的值
#define UART_TYPE_S2N_REPLY_DIP_STATUS			9

// 10 = N329向STM8索取DoorBell状态
#define UART_TYPE_N2S_APPLY_DOORBELL_STATUS		10

// 11 = STM8向N329应答:   DoorBell状态  - 0=释放; 1=按下 
#define UART_TYPE_S2N_REPLY_DOORBELL_STATUS		11

// 12 = N329向STM8发送LINKING
#define UART_TYPE_N2S_LINKING					12



// lzh_20161207_s
#define UART_TYPE_N2S_BUS_ACK_SND   			13
#define UART_TYPE_S2N_BUS_ACK_RCV   			14
// lzh_20161207_e


//15 = STM8向N329报告:  按键值
#define UART_TYPE_S2N_TOUCHKEY_STATUS     		15      //lyx 20170721

//16 = N329向STM8控制: LED指示
#define UART_TYPE_N2S_LED_CTRL					16

//17 = N329向STM8报告Application状态
#define UART_TYPE_N2S_REPOTR_APP_STATUS		17

//18 =  N329向STM8发送NOTIFY_STATE                                   
#define UART_TYPE_N2S_NOTIFY_STATE				18

//19 =  STM8向N329发送REQUEST_STATE
#define UART_TYPE_S2N_REQUEST_STATE                       19

//20 = STM8向N329发送NOTIFY_RESET
#define UART_TYPE_S2N_NOTIFY_RESET                          20

//21 = N329向STM8发送RESET指令
#define UART_TYPE_N2S_RESET                          21


//22 = N329向STM8申请: 控制AMP_MUTE
#define UART_TYPE_N2S_MUTE_CTRL				22	 //lyx_20170921

//23 = N329向STM8申请: 控制WIFI_POWER
#define UART_TYPE_N2S_WIFI_CTRL				23

//24 = N329向STM8发送TALK_STATE
#define UART_TYPE_N2S_TALK_STATE			24    //lyx_20170930	

#define UART_TYPE_STM8_VERSION_GET      25   //cao_20180604 
#define UART_TYPE_STM8_VERSION_ASK      26   //cao_20180604 

#define UART_TYPE_LOCAL_TIME_GET        27   //cao_20181116
#define UART_TYPE_LOCAL_TIME_UPDATE     28   //cao_20181116 

#define UART_TYPE_BEEP_C				29	//N329向STM8控制BEEP

#define UART_TYPE_GET_STM8_CONFIG		29	// n329->stm8 读取Z1~Z4工作模式
#define UART_TYPE_GET_STM8_CONFIG_RSP	30	// 
#define UART_TYPE_SET_STM8_CONFIG		31	// n329->stm8 配置Z1~Z4工作模式
#define UART_TYPE_READ_LINE				32	// n329->stm8 读取Z1-Z4口和事件位
#define UART_TYPE_READ_LINE_RSP			33	// 
#define UART_TYPE_WRITE_LINE			       34	// n329->stm8 写Z1-Z4口和有效位
#define UART_TYPE_LINE_IN_REPORT			35	// stm8->n329 汇报Z1-Z4输入翻转事件
#define UART_TYPE_READ_LINE_EXT			36	// n329->stm8 扩展防区指令
#define UART_TYPE_READ_LINE_EXT_RSP		37	// 
#define UART_TYPE_LINE_IN_REPORT_EXT		38	// stm8->n329 扩展防区指令
#define UART_TYPE_LINEIN_POWER_SET_REQ	39	// n329->stm8	申请12v控制
#define UART_TYPE_AUIN_SWITCH_REQ		40	// n329->stm8	申请MIC切换控制

//dh_20200303
#define UART_TYPE_SEND_REBOOT_LOG		40 //  n329->stm8发送 REBOOT记录
#define UART_TYPE_GET_REBOOT_LOG		41 //  
#define UART_TYPE_GET_REBOOT_LOG_RSP	42 //  

#define UART_TYPE_FIRMWARE_M2S        	44   //lzh_20210727	 	master->tiny1616
#define UART_TYPE_FIRMWARE_S2M     		45   //lzh_20210727		tiny1616->master

#define UART_TYPE_GetSn        			46   //master->tiny1616	获取序列号
#define UART_TYPE_GetSn_RSP     		47   //tiny1616->master	获取序列号应答

#define UART_TYPE_SetSn        			48   //master->tiny1616	设置序列号
#define UART_TYPE_SetSn_RSP     		49   //tiny1616->master	设置序列号应答

#define UART_TYPE_AU_1124_PWR_CTRL      50   //AK->tiny1616 控制1124电源
#define UART_TYPE_AUOUT_SWITCH 			51   //AK->tiny1616 控制音频开关

#define UART_TYPE_AMP_MUTE    				52 

#define UART_TYPE_ReportAddr    				60 

#define POOL_LENGTH			(200)		// 100

//dh_20211014
#define MODE_ALARM4		0xf0	//4防区
#define MODE_ALARM8		0x1e	//8防区
#define MODE_HAND_PLUG	0x34	//手柄
#define MODE_DJ4A		0x70	//DJ4A Z1 Z2 Z3 输入

typedef struct _BUFFER_POOL_{
	volatile unsigned short ptr_i;
	volatile unsigned short ptr_o;
	unsigned char Buffer[POOL_LENGTH];
} BUFFER_POOL;

#define PACKET_LENGTH			(140)	// 40

typedef struct {
	unsigned char len;
	unsigned char data[PACKET_LENGTH];
} OnePacket;

/*******************************************************************************************
 * @fn:		Init_vdp_uart
 *
 * @brief:	初始化uart端口
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int Init_vdp_uart(void);

/*******************************************************************************************
 * @fn:		vdp_uart_send_data
 *
 * @brief:	串口发送数据包处理
 *
 * @param:  	*data	- 数据区指针
 * @param:  	len		- 数据包长度
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int vdp_uart_send_data(char *data, int len);

/*******************************************************************************************
 * @fn:		vdp_uart_recv_data
 *
 * @brief:	串口接收数据包处理
 *
 * @param:  	buf - 数据指针
 * @param:  	len - 数据长度
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int vdp_uart_recv_data( char* buf, int len);

/*******************************************************************************************
 * @fn:		close_vdp_uart
 *
 * @brief:	关闭串口即相关资源
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int close_vdp_uart(void);

int api_uart_recv_callback( char* pbuf, unsigned int len );

//////////////////////////////////////////////////////////////////////////////////////////////////
// 物理层接口定义
//////////////////////////////////////////////////////////////////////////////////////////////////

//总线接收结果（与链路层通信标志）
typedef enum
{
	COM_RCV_NONE = 0,	//无收发数据
	COM_RCV_DAT,		//总线接收到数据(..\IND)
	COM_RCV_ACK,		//总线接收到ACK信号(..\ACON)
	COM_RCV_ERR,		//总线接收错误(..\IND)
} COM_RCV_FLAG;

//总线发送结果（与链路层通信标志）
typedef enum
{
	COM_TRS_NONE = 0,	//无收发数据	
	COM_TRS_DAT,		//总线发送完数据(..\LCON)
	COM_TRS_ACK,		//总线发送完ACK信号(..\LCON)
	COM_TRS_ERR,		//总线发送错误(..\LCON)
	// lzh_20181116_s
	COM_TRS_DAT_HAVE_ACK,		//总线发送完数据(..\LCON)
	// lzh_20181116_e
} COM_TRS_FLAG;

//总线错误定义
typedef enum
{	BUS_ERR_NONE = 0,		//数据帧正常
	BUS_ERR_RCV_LEAD_L_MIN,	//数据帧头长度小于最小值
	BUS_ERR_RCV_LEAD_G_MAX,	//数据帧头长度大于最大值
	BUS_ERR_RCV_PACK_L_MIN,	//数据帧长度为0
	BUS_ERR_RCV_BIT_ERR,	//数据帧位长度小于最小值
	BUS_ERR_TRS_LEAD_ERR,	//数据帧头发送错误
	BUS_ERR_TRS_BIT_ERR,	//数据帧位发送错误
} BUS_ERR;

#define COMM_RCV_FLAG_ENABLE(a,b)	{ phComRcvFlag = a; stateError = b; }
#define COMM_TRS_FLAG_ENABLE(a,b)	{ phComTrsFlag = a; stateError = b; }

unsigned char Ph_AckWrite(void);
unsigned char Ph_DataWrite(unsigned char *pdat,unsigned char len,unsigned char uNew);
unsigned char Ph_PollingReadResult(unsigned char *pdat,COM_RCV_FLAG *flag);
unsigned char Ph_PollingWriteResult(COM_TRS_FLAG *flag);

int api_stm8_mode_match(unsigned char stm8_mode);
int api_set_stm8_mode(void);
int api_get_stm8_mode(void);

#endif


