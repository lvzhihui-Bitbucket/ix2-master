/**
  ******************************************************************************
  * @file    obj_TalkState.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of obj_TalkState
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "../../../os/RTOS.h"
#include "../../../os/OSQ.h"
#include "../../../os/OSTIME.h"
#include "../task_Sundry.h"
#include "../../task_survey/task_survey.h"
#include "task_Talk.h"
#include "obj_TalkState.h"
#include "obj_PT2259.h"
#include "hd_Audio.h"
#include "../task_Power/task_Power.h"
#include "../../task_io_server/task_IoServer.h"
#include "../../vdp_uart.h"


TALK_STATE talkState;


/***********************************************************************
 * @fn      EnterTalkingState
 *
 * @brief   enter talking state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void EnterTalkingState(void)
{    
    	//API_WavePlayer_Disable();

    	API_POWER_TALK_ON();

    	talkState.talkingState = TALK_ON;

		api_uart_send_pack(UART_TYPE_N2S_TALK_STATE, &talkState.talkingState, 1);   //lyx_20170930

    	NJW1124Direct(NJW1124_TALK);      //lyx 20170502

		char temp[20];
    	API_Event_IoServer_InnerRead_All( TALK_VOLUME, temp);
		talkState.SPK_Volume = atoi(temp);

		PT2259_Set(CHANNEL_SPK,ConvertSpkVolumnData(talkState.SPK_Volume));

    	MIC_MuteOff();     		                       //lyx 20170502  
    	SPK_MuteOff();
    	//printf("*******************************EnterTalkingState!!!!!!!!!!!!!!!!!!");
}

/***********************************************************************
 * @fn      ExitTalkingState
 *
 * @brief   exit talking state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void ExitTalkingState(void)
{
    	API_POWER_TALK_OFF();

    	talkState.talkingState = TALK_OFF;

		api_uart_send_pack(UART_TYPE_N2S_TALK_STATE, &talkState.talkingState, 1);   //lyx_20170930

    	NJW1124Direct(NJW1124_IDLE);     //lyx 20170502

    	MIC_MuteOn();             		    	   //lyx 20170502
    	SPK_MuteOn();
    
    	//API_WavePlayer_Enable();
}


/***********************************************************************
 * @fn      GetTalkingState
 *
 * @brief   get the talkingState flag
 *
 * @param   none
 *
 * @return  talkingState
 ************************************************************************/
unsigned char GetTalkingState(void)
{
    	return talkState.talkingState;
}

/***********************************************************************
 * @fn      SPK_MuteOn
 *
 * @brief   set the speaker channel to mute state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void SPK_MuteOn(void)
{
	if( talkState.talkingState == TALK_ON)
	{
    		SPKMuteCtrl(MUTE_ON);
		//usleep(100000);
	}
	#if 0
	else if(get_audio_state() != 0)
	{
		API_POWER_TALK_OFF();
	}
	#endif
}

/***********************************************************************
 * @fn      SPK_MuteOn
 *
 * @brief   set the speaker channel to mute state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void SPK_MuteOff(void)
{
    	if( GetPowerTalkState() || GetPowerRingState() || GetPowerBeepState() )
    	{
       	 	SPKMuteCtrl(MUTE_OFF);
    	}
		#if 0
	else if(get_audio_state() != 0)
	{
		API_POWER_UDP_TALK_ON();
	}
	#endif
}


/***********************************************************************
 * @fn      MIC_MuteOn
 *
 * @brief   set the microphone channel to mute state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void MIC_MuteOn(void)
{
    	MICMuteCtrl(MUTE_ON);
}

/***********************************************************************
 * @fn      MIC_MuteOn
 *
 * @brief   set the microphone channel to mute state
 *
 * @param   none
 *
 * @return  none
 ************************************************************************/
void MIC_MuteOff(void)
{
    	if( talkState.talkingState == TALK_ON)
    	{
       	 	MICMuteCtrl(MUTE_OFF);
    	}
}

// lzh_20170802_s
// para: 		inc - 0/decrease, 1/increase
// return: 	talk_volume
int adjust_speaker_volume( int inc )
{
	char temp[20];

	if( inc )
	{
		if( talkState.SPK_Volume < 9 )
			talkState.SPK_Volume++;
		else			
			talkState.SPK_Volume = 0;
	}
	else
	{
		if( talkState.SPK_Volume != 0 )
			talkState.SPK_Volume--;
		else
			talkState.SPK_Volume = 9;
	}
	
	
	sprintf(temp, "%d", talkState.SPK_Volume);
	API_Event_IoServer_InnerWrite_All( TALK_VOLUME, temp);
	
	PT2259_Set(CHANNEL_SPK,ConvertSpkVolumnData(talkState.SPK_Volume));
	
	return talkState.SPK_Volume;
}

int get_speaker_volume( void )		//czn_20170803
{
	char temp[20];
	
	API_Event_IoServer_InnerRead_All( TALK_VOLUME, temp);
	return atoi(temp);
	//return talkState.SPK_Volume;
}

// lzh_20170802_e

