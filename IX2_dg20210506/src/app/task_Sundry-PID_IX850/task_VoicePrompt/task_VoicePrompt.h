/**
  ******************************************************************************
  * @file    task_VoicePrompt.h
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of task_VoicePrompt
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef TASK_VOICEPROMPT_H
#define TASK_VOICEPROMPT_H

#include "RTOS.h"
#include "task_survey.h"

// Define Task Vars and Structures----------------------------------------------
	//消息子类型
#define VOICEPROMPT_STOP           0
#define VOICEPROMPT_PLAY           1
	//场景枚举
typedef enum
{
    SCENE_CALL_FAIL_INEXISTENT,	//呼叫失败_分机不存在
    SCENE_CALL_FAIL_DND,		//呼叫失败_分机进入免扰模式
    SCENE_CALL_SUCC,			//呼叫成功
    SCENE_CALL_TALK,			//呼叫通话
    SCENE_CALL_CLOSE,			//呼叫结束
    SCENE_SYS_BUSY,				//系统忙
    SCENE_UNLOCK_COMMAND,		//开锁(指令开锁)
    SCENE_UNLOCK_BUTTON,		//开锁(出门按钮开锁)
    SCENE_VOLUME_ADJUST,		//提示音量调节
}Voice_Scene_Type;

	//消息结构
#pragma pack(1)
typedef struct
{
	VDP_MSG_HEAD 	head;
    uint8 msgMainType;
	uint8 cmd;
	uint8 subMsg;
} VOICE_STRUCT ;
#pragma pack()

// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_Voice( void );
void vtk_TaskProcessEvent_Voice(VOICE_STRUCT *msgVoice);

// Define API-------------------------------------------------------------------
void API_VoicePlay(uint8 voice_ctrl, Voice_Scene_Type prompt_scene);

#define API_Voice_CallFailInexistent()	API_VoicePlay(VOICEPROMPT_PLAY, SCENE_CALL_FAIL_INEXISTENT)
#define API_Voice_CallFailDND()			API_VoicePlay(VOICEPROMPT_PLAY, SCENE_CALL_FAIL_DND)
#define API_Voice_CallSucc()			API_VoicePlay(VOICEPROMPT_PLAY, SCENE_CALL_SUCC)
#define API_Voice_CallTalk()			API_VoicePlay(VOICEPROMPT_PLAY, SCENE_CALL_TALK)
#define API_Voice_CallClose()			API_VoicePlay(VOICEPROMPT_PLAY, SCENE_CALL_CLOSE)
#define API_Voice_SysBusy()				API_VoicePlay(VOICEPROMPT_PLAY, SCENE_SYS_BUSY)
#define API_Voice_UnlockCommand()		API_VoicePlay(VOICEPROMPT_PLAY, SCENE_UNLOCK_COMMAND)
#define API_Voice_UnlockButton()		API_VoicePlay(VOICEPROMPT_PLAY, SCENE_UNLOCK_BUTTON)
#define API_Voice_VolumeAdjust()		API_VoicePlay(VOICEPROMPT_PLAY, SCENE_VOLUME_ADJUST)
#define API_Voice_Unlock()				API_VoicePlay(VOICEPROMPT_PLAY, SCENE_UNLOCK_COMMAND)

#define API_Voice_Stop()				API_VoicePlay(VOICEPROMPT_STOP, 0)


#endif
