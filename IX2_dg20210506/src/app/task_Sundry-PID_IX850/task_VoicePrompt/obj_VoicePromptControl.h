/**
  ******************************************************************************
  * @file    obj_VoicePromptControl.h
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of obj_VoicePromptControl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef __OBJ_RINGCONTROL_H
#define __OBJ_RINGCONTROL_H


// Define Object Property-------------------------------------------------------
#define VOICE_STATE_STOP   0
#define VOICE_STATE_PLAY   1

#define VOICE_PLAY_DISABLE  0
#define VOICE_PLAY_ENABLE   1


#define CALL_FAIL_INEXISTENT_IOID	"1082"					//场景语音文件选择
#define CALL_FAIL_DND_IOID			"1083"					//场景语音文件选择	
#define CALL_SUCC_IOID				"1084"					//场景语音文件选择	
#define CALL_TALK_IOID				"1085"					//场景语音文件选择	
#define CALL_CLOSE_IOID				"1086"					//场景语音文件选择	
#define SYS_BUSY_IOID				"1087"					//场景语音文件选择	
#define UNLOCK_COMMAND_IOID			"1088"					//场景语音文件选择	
#define UNLOCK_BUTTON_IOID			"1089"					//场景语音文件选择	
#define VOLUME_ADJUST_IOID			"1090"					//场景语音文件选择	

// Define Object Function - Public----------------------------------------------
void VoicePromptCtrl_Init(void);
void VoiceCtrlPlay(Voice_Scene_Type prompt_scene);
void VoiceCtrlStop(uint8 keep_power);
uint8 GetVoiceState(void);

// Define Object Function - Private---------------------------------------------
static void Timer_VoiceTimeLimit_Callback(void);

#endif
