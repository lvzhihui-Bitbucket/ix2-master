/**
  ******************************************************************************
  * @file    obj_VoicePromptData.h
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of obj_VoicePromptData
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef OBJ_RING_CONFIG_H
#define OBJ_RING_CONFIG_H

#include "../../../os/RTOS.h"
#include "task_VoicePrompt.h"

#define VOICE_LANGUAGE_MAX_CNT					20

// Define Object Property-------------------------------------------------------
#define CALL_FAIL_INEXISTENT_FILE_NAME		"Cfail.wav"					//场景语音文件名
#define CALL_FAIL_DND_FILE_NAME				"Cfail.wav"					//场景语音文件名
#define CALL_SUCC_FILE_NAME					"Csucc.wav"					//场景语音文件名
#define CALL_TALK_FILE_NAME					"Ctalk.wav"					//场景语音文件名
#define CALL_CLOSE_FILE_NAME				"Cclose.wav"				//场景语音文件名
#define SYS_BUSY_FILE_NAME					"Sbusy.wav"					//场景语音文件名
#define UNLOCK_COMMAND_FILE_NAME			"Unlock.wav"				//场景语音文件名
#define UNLOCK_BUTTON_FILE_NAME				"Unlock.wav"				//场景语音文件名
#define VOLUME_ADJUST_FILE_NAME				"0_ATest.wav"				//场景语音文件名

typedef struct
{
    Voice_Scene_Type    scene;				// 场景
	const char*			io_id;				// 参数编码
	const char*			file;				// 文件名
}SCENE_TO_IOID_FILE_NAME;

typedef struct
{
    Voice_Scene_Type    scene;				// 场景
}SCENE_TO_FILE_NAME;

typedef struct
{
	int		cnt;	
	char	name[VOICE_LANGUAGE_MAX_CNT][100];
	char	displayName[VOICE_LANGUAGE_MAX_CNT][30];
} VOICE_LANGUAGE_LIST_T;

typedef struct
{
    Voice_Scene_Type  scene;				// 场景
    char              fileName[100];		// 语音文件名
    int               wave_volume;	        // 语音数据幅度(音量)
    int32             play_timeout;	        // 播放时长 ms为单位
    char              play_language[100];   // 语言类型
    int               play_enable;	        // 播放是否允许
}VoiceCfg_TypeDef;
extern VoiceCfg_TypeDef voice_prompt_config;

// Define Object Function - Public----------------------------------------------
void VoiceData_Init(Voice_Scene_Type prompt_scene);

// Define Object Function - Private---------------------------------------------

#endif
