/**
  ******************************************************************************
  * @file    task_VoicePrompt.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of task_VoicePrompt
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "../task_Sundry.h"
#include "task_VoicePrompt.h"
#include "obj_VoicePromptControl.h"
#include "obj_VoicePromptData.h"



/*******************************************************************************
 * @fn：	        vtk_TaskInit_Voice
 *
 * @brief:	初始化任务
 *
 * @param:  none
 *
 * @return: none
 ******************************************************************************/
void vtk_TaskInit_Voice(void)
{
    VoicePromptCtrl_Init();
	VoiceLanguageListInit();
}

/*******************************************************************************
 * @fn：	        vtk_TaskProcessEvent_Voice
 *
 * @brief:	任务对消息处理
 *
 * @param:  msgVoice --消息数据
 *
 * @return: none
 ******************************************************************************/
void vtk_TaskProcessEvent_Voice(VOICE_STRUCT *msgVoice)
{
	switch (msgVoice->cmd)
    {
		//播放
    	case VOICEPROMPT_PLAY:
        	VoiceCtrlPlay((Voice_Scene_Type)msgVoice->subMsg);
        	break;
		
		//停止	
    	case VOICEPROMPT_STOP:
        	VoiceCtrlStop(0);
        	break;
    	default:
        	break;
    }
}


/*******************************************************************************
  * @fn：	        API_VoicePlay
  *
  * @brief:
  *
  * @param
  *
  * @return:
 ******************************************************************************/
void API_VoicePlay(uint8 voice_ctrl, Voice_Scene_Type prompt_scene)
{
	VOICE_STRUCT msgVoice;

	msgVoice.head.msg_source_id 	= 0;
	msgVoice.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgVoice.head.msg_type 		= MSG_TYPE_VOICEPROMPT;
	msgVoice.head.msg_sub_type 	= 0;

    msgVoice.msgMainType = MSG_TYPE_VOICEPROMPT;			//消息总类型
	msgVoice.cmd = voice_ctrl;								//消息子类型
	msgVoice.subMsg = (uint8)prompt_scene;					//场景
	
	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgVoice, sizeof(VOICE_STRUCT));
}

