/**
  ******************************************************************************
  * @file    obj_VoicePromptControl.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of obj_VoicePromptControl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
//20141127

#include "task_VoicePrompt.h"
#include "obj_VoicePromptData.h"
#include "obj_VoicePromptControl.h"
#include "../../task_io_server/task_IoServer.h"
#include "../task_Power/task_Power.h"
#include "../task_Beeper/task_Beeper.h"


static uint8 VoiceState;
static OS_TIMER timer_TimeLimit;

/*******************************************************************************
 * @fn：	                VoicePromptCtrl_Init
 *
 * @brief:	初始化语音提示
 *
 * @param:  none
 *
 * @return: none
 ******************************************************************************/
void VoicePromptCtrl_Init(void)
{
    //创建软定时(未启动)
    OS_CreateTimer(&timer_TimeLimit, Timer_VoiceTimeLimit_Callback, 1);
	//工作状态初始化
    VoiceState = VOICE_STATE_STOP;
}

/*******************************************************************************
 * @fn：	                VoiceCtrlPlay
 *
 * @brief:	
 *
 * @param:  
 *                          
 * @return: 
 ******************************************************************************/
void VoiceCtrlPlay(Voice_Scene_Type prompt_scene)
{
	uint8 temp;
	OS_StopTimer(&timer_TimeLimit);
	//场景对应的配置装载
    VoiceData_Init(prompt_scene);
	
	//禁止播放,直接返回
    if(voice_prompt_config.play_enable == VOICE_PLAY_DISABLE)
    {
    	//BEEP_CONFIRM();		//czn_20191115
    	if(prompt_scene!=SCENE_UNLOCK_COMMAND)
    		Beep_On(BEEP_TYPE_DL1);
	bprintf("voice_prompt_config.play_enable == VOICE_PLAY_DISABLE\n");
        return;
    }
	
	//电源申请
    if(VoiceState == VOICE_STATE_STOP)  //本来在停止状态才需要打开电源
    {
		//申请电源
		//API_POWER_RING_ON();
		PowerOption( POWER_RING, POWER_ON );	//czn_20190617
    }
    else
    {
        VoiceCtrlStop(1);       //仅停止播放, 其电源仍开启中
    }
	
	//启动播放
    VoiceState = VOICE_STATE_PLAY;
	if (prompt_scene == SCENE_UNLOCK_COMMAND)	//如为开锁,500mS后提醒(解决分机开锁时, 语言前部分被禁音的问题)
	{
		usleep(500*1000);
	}
    API_WavePlayer_Play_DS(voice_prompt_config.fileName);

	OS_SetTimerPeriod(&timer_TimeLimit, (voice_prompt_config.play_timeout+300)/25);
	OS_RetriggerTimer(&timer_TimeLimit);
	
}

/*******************************************************************************
 * @fn：	                VoiceCtrlStop
 *
 * @brief:	语音播放停止
 *
 * @param:  keep_power -- 1 播放停止不关闭电源，0 播放停止并关闭电源
 *
 * @return: none
 ******************************************************************************/
void VoiceCtrlStop(uint8 keep_power)
{
    if(VoiceState == VOICE_STATE_STOP)
    {
        return;
    }

    API_WavePlayer_Stop();
    
    if(!keep_power)
    {
		//API_POWER_RING_OFF();
		PowerOption( POWER_RING, POWER_OFF);	//czn_20190617
    }
    
    VoiceState = VOICE_STATE_STOP;
}

/*******************************************************************************
 * @fn：	                Timer_VoiceTimeLimit_Callback
 *
 * @brief:	播放定时停止
 *
 * @param:  none
 *
 * @return: none
 ******************************************************************************/
static void Timer_VoiceTimeLimit_Callback(void)
{
	OS_StopTimer(&timer_TimeLimit);
    API_VoicePlay(VOICEPROMPT_STOP, (Voice_Scene_Type)0);
}

/*******************************************************************************
 * @fn：	                GetVoiceState
 *
 * @brief:	取语音提示状态
 *
 * @param:  none
 *
 * @return: VoiceState
 ******************************************************************************/
uint8 GetVoiceState(void)
{
    return VoiceState;
}

