/**
  ******************************************************************************
  * @file    obj_VoicePromptData.c
  * @author  cao
  * @version V1.0.0
  * @date    2014.09.15
  * @brief   This file contains the functions of obj_VoicePromptData
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
//20141127

#include "../../task_io_server/task_IoServer.h"
#include "obj_VoicePromptData.h"
#include "task_VoicePrompt.h"
#include "obj_VoicePromptControl.h"

void VoiceLangugePathMatch(char *laguage_path);


//场景对应参数编码(是否允许播放)
const SCENE_TO_IOID_FILE_NAME TAB_SCENE_TO_IOID_FILE_NAME[] = 
{
	//场景							//参数编码							//文件名
	{SCENE_CALL_FAIL_INEXISTENT,	CALL_FAIL_INEXISTENT_IOID,			CALL_FAIL_INEXISTENT_FILE_NAME  },	//呼叫失败_分机不存在
    {SCENE_CALL_FAIL_DND,			CALL_FAIL_DND_IOID,					CALL_FAIL_DND_FILE_NAME			},	//呼叫失败_分机进入免扰模式
    {SCENE_CALL_SUCC,				CALL_SUCC_IOID,						CALL_SUCC_FILE_NAME				},	//呼叫成功
    {SCENE_CALL_TALK,				CALL_TALK_IOID,						CALL_TALK_FILE_NAME				},	//呼叫通话
    {SCENE_CALL_CLOSE,				CALL_CLOSE_IOID,					CALL_CLOSE_FILE_NAME			},	//呼叫结束
    {SCENE_SYS_BUSY,				SYS_BUSY_IOID,						SYS_BUSY_FILE_NAME				},	//系统忙
    {SCENE_UNLOCK_COMMAND,			UNLOCK_COMMAND_IOID,				UNLOCK_COMMAND_FILE_NAME		},	//开锁(指令开锁)
    {SCENE_UNLOCK_BUTTON,			UNLOCK_BUTTON_IOID,					UNLOCK_BUTTON_FILE_NAME			},	//开锁(出门按钮开锁)
    {SCENE_VOLUME_ADJUST,			VOLUME_ADJUST_IOID,					VOLUME_ADJUST_FILE_NAME			},	//提示音量调节
};
const uint8 SCENE_TO_IOID_FILE_NAME_NUM = sizeof(TAB_SCENE_TO_IOID_FILE_NAME)/sizeof(TAB_SCENE_TO_IOID_FILE_NAME[0]);


VoiceCfg_TypeDef voice_prompt_config;


/*******************************************************************************
 * @fn：	                VoiceData_Init
 *
 * @brief:	根据[场景], 装载相应的运行参数
 *
 * @param:  
 *          
 * @return: 
 *
 ******************************************************************************/
void VoiceData_Init(Voice_Scene_Type prompt_scene)
{
    uint8 i;
	uint8 flag_default;
    char temp[100];
	
	flag_default = 0;	//装载所设置的语言对应的数据
	
	
	//装载场景
    voice_prompt_config.scene = prompt_scene;

	//装载语言
    API_Event_IoServer_InnerRead_All(VOICE_LANGUAGE_SELECT, voice_prompt_config.play_language);
	bprintf("%s\n",voice_prompt_config.play_language);
    VoiceLangugePathMatch(voice_prompt_config.play_language);		//czn_20191221
	Load_Default:	
	//该场景下, 允许播放与否
	voice_prompt_config.play_enable = VOICE_PLAY_DISABLE;
	for(i=0; i<SCENE_TO_IOID_FILE_NAME_NUM; i++)
    {
        if (TAB_SCENE_TO_IOID_FILE_NAME[i].scene == voice_prompt_config.scene)
        {
            API_Event_IoServer_InnerRead_All(TAB_SCENE_TO_IOID_FILE_NAME[i].io_id, temp);
			voice_prompt_config.play_enable = atoi(temp);
            break;
        }
    }
    if(voice_prompt_config.play_enable == VOICE_PLAY_DISABLE || i >= SCENE_TO_IOID_FILE_NAME_NUM)	
    {
    	bprintf("%d:scene=%d:%d\n",voice_prompt_config.play_enable,voice_prompt_config.scene,i);
        return;	//禁止播放, 直接返回
    }

	//WAVE音量装载及设置
	API_Event_IoServer_InnerRead_All(VOICE_VOLUME, temp);
	voice_prompt_config.wave_volume = atoi(temp);
	API_WavePlayer_SetVolume(voice_prompt_config.wave_volume);

	//文件名提取	
	snprintf(voice_prompt_config.fileName, 100, "%s/%s", voice_prompt_config.play_language, TAB_SCENE_TO_IOID_FILE_NAME[i].file);
	bprintf("%s\n",voice_prompt_config.fileName);
	//读语音播放时间长度
	//API_PlayList_GetUsingLength_ms(voice_prompt_config.fileName, &voice_prompt_config.play_timeout); 
	//if (voice_prompt_config.play_timeout < 0)	//时间小于0,则任务无该文件
	if(API_PlayList_GetUsingLength_ms_DS(voice_prompt_config.fileName, &voice_prompt_config.play_timeout)<0)
	{
		if (flag_default == 0)
		{
			API_Event_IoServer_InnerRead_All(VOICE_PLAY_MODE, temp);
			if (atoi(temp))
			{
				flag_default = 1;
				strcpy(voice_prompt_config.play_language, "/mnt/nand1-2/rings/1_EN");
				goto Load_Default;
			}
			else
			{
				voice_prompt_config.play_enable = VOICE_PLAY_DISABLE;	//该场景无对应的文件ID, 禁止播放
			}
		}
		else
		{
			bprintf("API_PlayList_GetUsingLength_ms\n");
			voice_prompt_config.play_enable = VOICE_PLAY_DISABLE;	//该场景无对应的文件ID, 禁止播放
		}	
	}
}



VOICE_LANGUAGE_LIST_T voiceDir;
char* voiceDisplayBuffer[VOICE_LANGUAGE_MAX_CNT]={NULL};


void VoiceLanguageListInit(void)
{
	voiceDir.cnt = 0;
	
	GetVoiceDirName("/mnt/sdcard/Customerized/prompt/", &voiceDir);
	if(voiceDir.cnt == 0)
	{
		GetVoiceDirName("/mnt/nand1-2/Customerized/prompt/", &voiceDir);

		if(voiceDir.cnt == 0)
		{
			GetVoiceDirName(RING_Folder, &voiceDir);
		}
	}

}


int GetVoiceDirName(const char * dir, VOICE_LANGUAGE_LIST_T *pVoiceDir)
{
	char cmd[100];
	char linestr[100];
	int ret = 0;
	char *backupDir;
	char *p;
	int i;
	
	snprintf(cmd, 100, "ls -l %s | grep ^d | awk '{print $9}'", dir);
	
	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}
		
		if(strlen(linestr) == 0)
		{
			continue;
		}
		
		snprintf(pVoiceDir->name[pVoiceDir->cnt++], 100, "%s%s", dir, linestr);
		
		snprintf(pVoiceDir->displayName[pVoiceDir->cnt-1], 30, "%s", linestr);
		
		if(pVoiceDir->cnt > 0)
		{
			//printf("name[%d] = %s\n", pVoiceDir->cnt-1, pVoiceDir->name[pVoiceDir->cnt-1]);
		}

		if(pVoiceDir->cnt >= VOICE_LANGUAGE_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}

int GetVoiceLanguageListCnt(void)
{
	return voiceDir.cnt;
}

int GetVoiceLanguageListRecord(int index, char* record)
{
	if(index >= voiceDir.cnt)
	{
		record[0] = 0;
		return -1;
	}

	strcpy(record, voiceDir.name[index]);
	return 0;
}

int GetVoiceLanguageDisplayName(char* name)
{
	char record[100] = {0};
	int i;
	
    API_Event_IoServer_InnerRead_All(VOICE_LANGUAGE_SELECT, record);

	for(i = 0; i < voiceDir.cnt; i++)
	{
		if(!strcmp(record, voiceDir.name[i]))
		{
			break;
		}
	}

	if(name != NULL)
	{
		strcpy(name, voiceDir.displayName[i]);
	}
	
	return 0;
}

char** GetVoiceLanguageListBuffer(void)
{
	int i;

	for(i = 0; i < voiceDir.cnt; i++)
	{
		voiceDisplayBuffer[i] = voiceDir.displayName[i];
	}
	
	return voiceDisplayBuffer;
}

int GetVoiceLanguageIndex(void)
{
	char record[100] = {0};
	int i;
	
    API_Event_IoServer_InnerRead_All(VOICE_LANGUAGE_SELECT, record);

	for(i = 0; i < voiceDir.cnt; i++)
	{
		if(!strcmp(record, voiceDir.name[i]))
		{
			break;
		}
	}
	
	return i;
}


int SetVoiceLanguage(int index)
{
	if(index >= voiceDir.cnt)
	{
		return -1;
	}

    API_Event_IoServer_InnerWrite_All(VOICE_LANGUAGE_SELECT, voiceDir.name[index]);
	return 0;
}

void VoiceLangugePathMatch(char *laguage_path)	//czn_20191221
{
	int i;
	if(strstr(laguage_path,"/")!=NULL)
		return;
	for(i = 0; i < voiceDir.cnt; i++)
	{
		if(strcmp(laguage_path,voiceDir.displayName[i])==0)
		{
			strcpy(laguage_path,voiceDir.name[i]);
		}
	}
}

