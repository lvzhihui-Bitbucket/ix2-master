/**
  ******************************************************************************
  * @file    obj_Unlock_State.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
//KP: V000103	//ID: V000101
#if 0
#include "obj_Unlock_State.h"
#include "obj_Unlock_Data.h"
#include "task_Unlock.h"
#include "task_Power.h"
//#include "task_Beeper.h"
#include "task_Led.h"
#include "task_IoServer.h"
#include "hal_eeprom.h"
#include "hd_power.h"
#include "obj_L2Layer.h"		
#include "define_Command.h"
//#include "task_Survey.h"
//#include "obj_Survey_Setup.h"

UNLOCK_RUN Lock_Run;
uint8 unlock_second_ok;

/*------------------------------------------------------------------------
								Lock_Open_Timer	(开锁)
入口:  
	无

处理:
	开锁启动

返回: 
	无
------------------------------------------------------------------------*/
void Lock_Open_Timer(uint8 lock_ID, uint8 unlock_timer, uint8 source)	//20140625
{
	//装载unlock_config
	Unlock_Data_Init();

	if (lock_ID == LOCK1)
	{
		if (unlock_timer == 1)	
		{
			Lock_Config.lock1_timer = unlock_timer * 6;	//单位为100ms, 设置1S时, 实际开锁延时为 1 * 6 * 100 =600ms
		}
		else
		{
			Lock_Config.lock1_timer = unlock_timer * 10;	//单位为100ms, 设置nS时, 实际开锁延时为 n * 10 * 100 =n(Kms)
		}
	}
	else
	{
		if (unlock_timer == 1)	
		{
			Lock_Config.lock2_timer = unlock_timer * 6;	//单位为100ms, 设置1S时, 实际开锁延时为 1 * 6 * 100 =600ms
		}
		else
		{
			Lock_Config.lock2_timer = unlock_timer * 10;	//单位为100ms, 设置nS时, 实际开锁延时为 n * 10 * 100 =n(Kms)
		}
	}
	
	Lock_Open_Process(lock_ID, source);	//20140625
}

/*------------------------------------------------------------------------
								Lock_Open	(开锁)
入口:  
	无

处理:
	开锁启动

返回: 
	无
------------------------------------------------------------------------*/
void Lock_Open(uint8 lock_ID, uint8 source)	//20140625
{
	//装载unlock_config
	Unlock_Data_Init();
	
	Lock_Open_Process(lock_ID, source);	//20140625
}

/*------------------------------------------------------------------------
								Lock_Open_Process	(开锁)
入口:  
	无

处理:
	开锁启动

返回: 
	无
------------------------------------------------------------------------*/
void Lock_Open_Process(uint8 lock_ID, uint8 source)	//20140625
{
	if (lock_ID == LOCK1)
	{

		
		//20140625
		//判断开锁来源
		if (source == MSG_UNLOCK_SOURCE_CMD)
		{
			if (Lock_Run.cmd_unlock_disable_flag)
			{
				return;
			}
			else
			{
				Lock_Run.cmd_unlock_disable_flag = 1;
				//API_Event_IoServer_InnerRead_All(UNLOCK_GAP_TIMING, (uint8*)&Lock_Config.cmd_unlock_disable_timer);
			}
		}
		else
		{
			Lock_Run.cmd_unlock_disable_flag = 0;
			OS_StopTimer(&unlock_disable_timing);
		}

		
		//开锁
		if (Lock_Config.lock1_mode == NORMAL_OPEN)	//常开模式
		{
			API_POWER_UNLOCK1_ON();	
		}
		else	//常闭模式
		{
			API_POWER_UNLOCK1_OFF();	
		}

//20140625		if (Lock_Run.lock1_state) return;	//正在开锁中(上次开锁尚未关闭), 则不再进行开锁的相关提示, 且开锁不重新计时

		//开锁1指示灯_ON
//DT607		API_UNLOCK1_LED_ON();
		//DT607
		if(Lock_Config.lock1_timer < 50)	//小于5秒
		{
			API_LedUnlock_OnTime(50);
		}
		else
		{
			API_LedUnlock_OnTime(Lock_Config.lock1_timer);
		}
		
		//开锁标志	
		Lock_Run.lock1_state = 1;
		
		//启动开锁定时
		Lock_Run.lock1_timer_count = 0;
		OS_RetriggerTimer(&unlock1_timing);

		//20140625
		//API_Survey_MsgInform(INFORM_UNLOCK1_GA_SEND);	//通知Survey: 发送开锁组播(UNLOCK1/UNLOCK3/UNLOCK5/UNLOCK7)

		
	}
	else	
	{
		//开锁
		//20131108_UnlockSecond
		/*		
		if (Lock_Config.lock2_mode == NORMAL_OPEN)	//常开模式
		{
			API_POWER_UNLOCK2_ON();	
		}
		else	//常闭模式
		{
			API_POWER_UNLOCK2_OFF();	
		}
		*/

		//开锁2指示灯_ON
//DT607		API_UNLOCK2_LED_ON();
		//DT607
		if(Lock_Config.lock2_timer < 50)	//小于5秒
		{
			API_LedUnlock_OnTime(50);
		}
		else
		{
			API_LedUnlock_OnTime(Lock_Config.lock2_timer);
		}
		
		//开锁标志	
		Lock_Run.lock2_state = 1;
		
		//启动开锁定时
		Lock_Run.lock2_timer_count = 0;
		OS_RetriggerTimer(&unlock2_timing);

		//20140625
		//API_Survey_MsgInform(INFORM_UNLOCK2_GA_SEND);	//通知Survey: 发送开锁组播(UNLOCK2/UNLOCK4/UNLOCK6/UNLOCK8)
		
		
//		OS_RetriggerTimer(&response_timing);	//20131108_UnlockSecond	//启动开锁等待证实定时	//20140625	//张工指示: 取消第二把锁证实提醒
	}	
	
	//开锁提示音
//	OS_Delay(500);	//等待通信完毕	, 以免通信将MUTE打开, 从而无BEEP声
	//BEEP_UNLOCK();	
}

/*------------------------------------------------------------------------
								Lock_Close	(关锁)
入口:  
	无

处理:
	关锁

返回: 
	无
------------------------------------------------------------------------*/
void Lock_Close(uint8 lock_ID)
{
	if (lock_ID == LOCK1)
	{
		if (Lock_Config.lock1_mode == NORMAL_OPEN)	//常开模式
		{
			API_POWER_UNLOCK1_OFF();	
		}
		else	//常闭模式
		{
			API_POWER_UNLOCK1_ON();	
		}
	
		//开锁1指示灯_OFF
//DT607		API_UNLOCK1_LED_OFF();
		
		//关锁标志	
		Lock_Run.lock1_state = 0;
		
		//关闭开锁定时
		OS_StopTimer(&unlock1_timing);	

		
		//20140625
		if (Lock_Run.cmd_unlock_disable_flag)    
		{
			//启动指令开锁禁止定时
			Lock_Run.unlock_disable_timer = 0;
			OS_RetriggerTimer(&unlock_disable_timing);
		}

		
	}
	else
	{
		//20131108_UnlockSecond
		/*		
		if (Lock_Config.lock2_mode == NORMAL_OPEN)	//常开模式
		{
			API_POWER_UNLOCK2_OFF();	
		}
		else	//常闭模式
		{
			API_POWER_UNLOCK2_ON();	
		}
		*/

		//开锁2指示灯_OFF
//DT607		API_UNLOCK2_LED_OFF();
		
		//关锁标志	
		Lock_Run.lock2_state = 0;
		
		//关闭开锁定时
		OS_StopTimer(&unlock2_timing);	
	}
}

/*------------------------------------------------------------------------
						读取锁状态
入口:  
	无

处理:
	读取锁状态

返回: 
	0 = 关锁
	1 = 开锁
------------------------------------------------------------------------*/
uint8 Get_Unlock_State(uint8 lock_ID)
{
	if (lock_ID == LOCK1)
	{
		return (Lock_Run.lock1_state);
	}
	else
	{
		return (Lock_Run.lock2_state);
	}
}

/*------------------------------------------------------------------------
						根据开锁模式置继电器状态
入口:  
	flag_init = 0 = 上电初始化
	flag_init = 1 = 运行初始化

处理:
	置继电器平时状态

返回: 
	无
------------------------------------------------------------------------*/
void Set_Relay_Unlock(uint8 flag_init)
{
	//根据unlock_mode, 置继电器的初始状态
		//UNLOCK1
	if (flag_init)
	{
		//API_Event_IoServer_InnerRead_All(UNLOCK1_MODE, (uint8*)&Lock_Config.lock1_mode);		//Unlock1开锁模式
	}
	else
	{
		//NVMEM_Read_P(INTER_EEPROM, EEPROM_ADDR[UNLOCK1_MODE].address, (uint8*)&Lock_Config.lock1_mode, 1);		//Unlock1开锁模式
	}
	if (Lock_Config.lock1_mode == NORMAL_OPEN)
	{
		Unlock1_PowerCtrl(0);	//常开模式
	}
	else
	{
		Unlock1_PowerCtrl(1);	//常闭模式
	}

	//UNLOCK2
	if (flag_init)
	{
		//API_Event_IoServer_InnerRead_All(UNLOCK2_MODE, (uint8*)&Lock_Config.lock2_mode);		//Unlock2开锁模式
	}
	else
	{
		//NVMEM_Read_P(INTER_EEPROM, [UNLOCK2_MODE].address, (uint8*)&Lock_Config.lock2_mode, 1);		//Unlock1开锁模式
	}
	if (Lock_Config.lock2_mode == NORMAL_OPEN)
	{
		Unlock2_PowerCtrl(0);	//常开模式
	}
	else
	{
		Unlock2_PowerCtrl(1);	//常闭模式
	}
}

/*------------------------------------------------------------------------
						清除开第二把锁等待证实标志
入口:  
	无

处理:
	清除开第二把锁等待证实标志

返回: 
	无
------------------------------------------------------------------------*/
//20131108_UnlockSecond
void Set_Unlock_Second_OK(void)
{
	unlock_second_ok = 0;
}

#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
