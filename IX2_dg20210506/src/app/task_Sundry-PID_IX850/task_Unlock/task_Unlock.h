/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Unlock_H
#define _task_Unlock_H
//KP: V000103	//ID: V000101
#if 0
#include "RTOS.h"
#include "BSP.h"

#include "obj_Unlock_State.h"


// Define Task Vars and Structures----------------------------------------------
	//第几把锁
#define LOCK1						1	//锁1
#define LOCK2						2	//锁2

#define MSG_UNLOCK_SOURCE_KEY       0	//20140625	
#define MSG_UNLOCK_SOURCE_CMD       1	//20140625


// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
typedef struct MSG_UNLOCK_STRU	
{	
    uint8   msgMainType;
	uint8	msg_type;						//消息类型
	uint8	lock_ID;						//哪吧锁:  1=第一把锁,2=第二把锁
    uint8   msg_unlock_source;		//20140625		
}	MSG_UNLOCK;
typedef struct MSG_UNLOCK_TIMER_STRU	
{	
    uint8   msgMainType;
	uint8	msg_type;						//消息类型
	uint8	lock_ID;						//哪吧锁:  1=第一把锁,2=第二把锁
	uint8	unlock_timer;					//开锁延时
    uint8   msg_unlock_source;		//20140625		
}	MSG_UNLOCK_TIMER;
	//msg_type
#define MSG_TYPE_LOCK_OPEN			0
#define MSG_TYPE_LOCK_CLOSE			1
#define MSG_TYPE_LOCK_TIMER_CLOSE	2
#define MSG_TYPE_LOCK_ERROR			3	//20131108_UnlockSecond
#define MSG_TYPE_LOCK_OPEN_TIMER	4

	//Queues-------------------------------------

	//Task events--------------------------------

	//Event objects------------------------------

// Define Task 2 items----------------------------------------------------------
	//软定时
extern OS_TIMER unlock1_timing;		//开锁1定时
extern OS_TIMER unlock2_timing;		//开锁2定时
extern OS_TIMER response_timing;	//开锁2的证实定时	//20131108_UnlockSecond
extern OS_TIMER unlock_disable_timing;		//开锁使能定时	//20140625
//extern OS_TASK tcb_unlock;                     //Task-control-blocks
void vtk_TaskInit_Unlock(void);


// Define Task others-----------------------------------------------------------
void vtk_TaskProcessEvent_Unlock(MSG_UNLOCK *msgUnlock);
void Unlock1_Timer_Off(void);
void Unlock2_Timer_Off(void);
void Response_Timer_Off(void);	//20131108_UnlockSecond
void Unlock_Disable_Off(void);	//20140625



// Define API-------------------------------------------------------------------
void API_Unlock(uint8 msg_type_temp, uint8 msg_lock_ID_temp, uint8 source);	//20140625
void API_Unlock_Timer(uint8 msg_type_temp , uint8 msg_lock_ID_temp, uint8 unlock_timer, uint8 source);	//20140625

	/*void API_Unlock1(void)
	入口:	无

	处理:	发送消息给Unlock

	出口:	无
	*/
	#define API_Unlock1(source)	API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK1, source)	//20140625
			
	/*void API_Unlock1_Timer(uint8 unlock_timer, uint8 source)
	入口:	无

	处理:	发送消息给Unlock

	出口:	无
	*/
	#define API_Unlock1_Timer(unlock_timer, source)	API_Unlock_Timer(MSG_TYPE_LOCK_OPEN_TIMER , LOCK1, unlock_timer, source)	//20140625

	/*void API_Lock1(void)
	入口:	无

	处理:	发送消息给Unlock

	出口:	无
	*/
	#define API_Lock1()		API_Unlock(MSG_TYPE_LOCK_CLOSE , LOCK1, NULL)	//20140625

	/*void API_Unlock2(void)
	入口:	无

	处理:	发送消息给Unlock

	出口:	无
	*/
	#define API_Unlock2(source)	API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK2, source)	//20140625
			

	/*void API_Set_Relay_Unlock(void)
	入口:	无

	处理:	根据开锁模式置开锁继电器初始状态

	出口:	无
	*/
	#define API_Set_Relay_Unlock()	Set_Relay_Unlock(1)

	//20131108_UnlockSecond
	#define API_Unlock2_Error()	API_Unlock(MSG_TYPE_LOCK_ERROR , LOCK2, NULL)	//20140625

#else
#include "RTOS.h"
#include "task_Relay.h"

#define LOCK1						1	//锁1
#define LOCK2						2	//锁2

#define MSG_TYPE_LOCK_OPEN			0
#define MSG_TYPE_LOCK_CLOSE			1
#define MSG_TYPE_LOCK_TIMER_CLOSE	2
#define MSG_TYPE_LOCK_ERROR			3	//20131108_UnlockSecond
#define MSG_TYPE_LOCK_OPEN_TIMER	4

#define MSG_UNLOCK_SOURCE_KEY       0	//20140625	
#define MSG_UNLOCK_SOURCE_CMD       1	//20140625
void API_Unlock(uint8 msg_type_temp, uint8 msg_lock_ID_temp, uint8 source);

#define API_Unlock1(source)	{extern int STM8_CODE_VERSION; if(STM8_CODE_VERSION < 3) {API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK1, source); BEEP_CONFIRM();} else API_Relay(MSG_TYPE_RELAY_OPEN);}
#define API_Unlock2(source)	{extern int STM8_CODE_VERSION; if(STM8_CODE_VERSION < 3) {API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK2, source); BEEP_CONFIRM();} else API_Relay(MSG_TYPE_RELAY_OPEN);}
#endif

#endif
