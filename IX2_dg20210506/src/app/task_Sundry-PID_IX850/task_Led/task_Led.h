/**
  ******************************************************************************
  * @file    task_Led.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_Led.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef TASK_LED_H
#define TASK_LED_H

#include "hd_Led.h"
#include "task_survey.h"	

typedef enum
{
	Led_DisplayMsg_KernelBoot = 0,
	Led_DisplayMsg_AppBoot,
	Led_DisplayMsg_BootFinish,
	Led_DisplayMsg_BootError,
	Led_DisplayMsg_CallRing,
	Led_DisplayMsg_CallTalk,
	Led_DisplayMsg_CallDivert,
	Led_DisplayMsg_CallClose,
	Led_DisplayMsg_LocalMonitor,
	Led_DisplayMsg_RemoteMonitor,
	Led_DisplayMsg_MonitorClose,
	Led_DisplayMsg_MissEvent,
	Led_DisplayMsg_CheckEvent,
	Led_DisplayMsg_NotDisturb,
	Led_DisplayMsg_CloudBeDisturbed,
	Led_DisplayMsg_FwUpdate,
	Led_DisplayMsg_FwUpdateFinish,
	
}Led_DisplayMsg_e;

/*******************************************************************************
                              Define Msg Type  
*******************************************************************************/
#define     LED_CTRL_TYPE_OFF				0
#define     LED_CTRL_TYPE_ON				1
#define     LED_CTRL_TYPE_FAST_FLASH		2
#define     LED_CTRL_TYPE_DOUBLE_FLASH	3

#define 	 LED_CTRL_FAST_FLASH			4       //lyx 20170727
#define     LED_CTRL_SLOW_FLASH			5
#define     LED_CTRL_IRREGULAR_FLASH           6    

/*******************************************************************************
                               Define led_select
*******************************************************************************/
#define LED_SELECT_TAKL       		0x01
#define LED_SELECT_UNLOCK      	0x02
#define LED_SELECT_POWER		0x04
#define LED_SELECT_MENU			0x08
#define LED_SELECT_UP			0x10
#define LED_SELECT_DOWN		0x20
#define LED_SELECT_BACK			0x40
#define LED_SELECT_DISTURB		0x80


//#define LED_SELECT_ALL         		0x03

/*******************************************************************************
                        Define message structure
*******************************************************************************/
typedef struct
{
	VDP_MSG_HEAD 	head;
	unsigned char 	msgLedType;
	unsigned char		ledSelect;
	unsigned short 	time;
} MsgLed;

/*******************************************************************************
                            Declare task 2 items
*******************************************************************************/
//extern OS_TASK TCB_Power;
void vtk_TaskInit_Led( void );
void vtk_TaskProcessEvent_Led(MsgLed *msgLed);

/*******************************************************************************
                      Task application interface
*******************************************************************************/
unsigned char API_LedControl(uint8 ledCtrlType, uint8 led_select, uint16 iTime);	//iTime��λ100ms

#define API_LedDisplay_KernelBoot()                  	API_LedControl(Led_DisplayMsg_KernelBoot, 0, 0)
#define API_LedDisplay_AppBoot()                      	API_LedControl(Led_DisplayMsg_AppBoot, 0, 0)
#define API_LedDisplay_BootFinish()                      	API_LedControl(Led_DisplayMsg_BootFinish, 0, 0)
#define API_LedDisplay_CallRing()                      	API_LedControl(Led_DisplayMsg_CallRing, 0, 0)
#define API_LedDisplay_CallTalk()                     		API_LedControl(Led_DisplayMsg_CallTalk, 0, 0)
#define API_LedDisplay_CallDivert()                      	API_LedControl(Led_DisplayMsg_CallDivert, 0, 0)
#define API_LedDisplay_CallClose()                      	API_LedControl(Led_DisplayMsg_CallClose, 0, 0)
#define API_LedDisplay_LocalMon()                      	API_LedControl(Led_DisplayMsg_LocalMonitor, 0, 0)
#define API_LedDisplay_RemoteMon()                      	API_LedControl(Led_DisplayMsg_RemoteMonitor, 0, 0)
#define API_LedDisplay_MonClose()                      	API_LedControl(Led_DisplayMsg_MonitorClose, 0, 0)
#define API_LedDisplay_MissEvent()                      	API_LedControl(Led_DisplayMsg_MissEvent, 0, 0)
#define API_LedDisplay_CheckEvent()                      	API_LedControl(Led_DisplayMsg_CheckEvent, 0, 0)
#define API_LedDisplay_NotDisturb()                      	API_LedControl(Led_DisplayMsg_NotDisturb, 0, 0)
#define API_LedDisplay_CloudBeDisturbed()             API_LedControl(Led_DisplayMsg_CloudBeDisturbed, 0, 0)
#define API_LedDisplay_FwUpdate()                      	API_LedControl(Led_DisplayMsg_FwUpdate, 0, 0)
#define API_LedDisplay_FwUpdateFinish()                API_LedControl(Led_DisplayMsg_FwUpdateFinish, 0, 0)


#if 0
#define API_LedOn(led_select)                      \
                  API_LedControl(LED_CTRL_TYPE_ON, led_select, 0)
#define API_LedOff(led_select)                     \
                  API_LedControl(LED_CTRL_TYPE_OFF, led_select, 0)
#define API_LedFastFlash(led_select)               \
                  API_LedControl(LED_CTRL_TYPE_FAST_FLASH, led_select, 0)		 
#define API_LedDoubleFlash(led_select)               \
                  API_LedControl(LED_CTRL_TYPE_DOUBLE_FLASH, led_select, 0)

#define API_LedIrregularFlash(led_select)               \
                  API_LedControl(LED_CTRL_IRREGULAR_FLASH, led_select, 0)



#define API_LedOnTime(led_select,iTime)            \
                  API_LedControl(LED_CTRL_TYPE_ON, led_select, iTime)
#define API_LedOffTime(led_select,iTime)           \
                  API_LedControl(LED_CTRL_TYPE_OFF, led_select, iTime)
#define API_LedFastFlashTime(led_select,iTime)     \
                  API_LedControl(LED_CTRL_TYPE_FAST_FLASH, led_select, iTime)
#define API_LedDoubleFlashTime(led_select,iTime)     \
                  API_LedControl(LED_CTRL_TYPE_DOUBLE_FLASH, led_select, iTime)



#define API_LED_TALK_FLASH_FAST()		API_LedFastFlash(LED_SELECT_TAKL) 
#define API_LED_TALK_FLASH_SLOW()		API_LedDoubleFlash(LED_SELECT_TAKL)
#define API_LED_TALK_OFF()				API_LedOff(LED_SELECT_TAKL)
#define API_LED_TALK_ON()				API_LedOn(LED_SELECT_TAKL)


#define API_LED_UNLOCK_FLASH_FAST()		API_LedFastFlash(LED_SELECT_UNLOCK)         
#define API_LED_UNLOCK_FLASH_SLOW()	API_LedDoubleFlash(LED_SELECT_UNLOCK)
#define API_LED_UNLOCK_ON()				API_LedOn(LED_SELECT_UNLOCK)
#define API_LED_UNLOCK_OFF()			API_LedOff(LED_SELECT_UNLOCK)


#define API_LED_POWER_FLASH_FAST()		API_LedFastFlash(LED_SELECT_POWER)      
#define API_LED_POWER_FLASH_SLOW()		API_LedDoubleFlash(LED_SELECT_POWER)
#define API_LED_POWER_FLASH_IRREGULAR()  API_LedIrregularFlash(LED_SELECT_POWER)
#define API_LED_POWER_ON()				API_LedOn(LED_SELECT_POWER)
#define API_LED_POWER_OFF()				API_LedOff(LED_SELECT_POWER)


#define API_LED_MENU_FLASH_FAST()		API_LedFastFlash(LED_SELECT_MENU)      
#define API_LED_MENU_FLASH_SLOW()		API_LedDoubleFlash(LED_SELECT_MENU)
#define API_LED_MENU_ON()				API_LedOn(LED_SELECT_MENU)
#define API_LED_MENU_OFF()				API_LedOff(LED_SELECT_MENU)


#define API_LED_UP_FLASH_FAST()			API_LedFastFlash(LED_SELECT_UP)      
#define API_LED_UP_FLASH_SLOW()			API_LedDoubleFlash(LED_SELECT_UP)
#define API_LED_UP_ON()					API_LedOn(LED_SELECT_UP)
#define API_LED_UP_OFF()					API_LedOff(LED_SELECT_UP)


#define API_LED_DOWN_FLASH_FAST()		API_LedFastFlash(LED_SELECT_DOWN)      
#define API_LED_DOWN_FLASH_SLOW()		API_LedDoubleFlash(LED_SELECT_DOWN)
#define API_LED_DOWN_ON()				API_LedOn(LED_SELECT_DOWN)
#define API_LED_DOWN_OFF()				API_LedOff(LED_SELECT_DOWN)


#define API_LED_BACK_FLASH_FAST()		API_LedFastFlash(LED_SELECT_BACK)      
#define API_LED_BACK_FLASH_SLOW()		API_LedDoubleFlash(LED_SELECT_BACK)
#define API_LED_BACK_ON()				API_LedOn(LED_SELECT_BACK)
#define API_LED_BACK_OFF()				API_LedOff(LED_SELECT_BACK)


#define API_LED_DISTURB_FLASH_FAST()	API_LedFastFlash(LED_SELECT_DISTURB)      
#define API_LED_DISTURB_FLASH_SLOW()	API_LedDoubleFlash(LED_SELECT_DISTURB)
#define API_LED_DISTURB_ON()				API_LedOn(LED_SELECT_DISTURB)
#define API_LED_DISTURB_OFF()			API_LedOff(LED_SELECT_DISTURB)

#endif

/*******************************************************************************
                      Declare task other functions
*******************************************************************************/


#endif

