/**
  ******************************************************************************
  * @file    hd_Led.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the hd_Led.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _HD_LED_H
#define _HD_LED_H

// Define Object Property-------------------------------------------------------
#define LED_OFF 1
#define LED_ON  0

#define LED_FLASH_SLOW 		2    //lyx 20170727
#define LED_FLASH_FAST   		3
#define LED_FLASH_IRREGULAR 4    


typedef enum       //lyx 20170721
{
    MSG_LED_ON,
    MSG_LED_OFF,
    MSG_LED_FLASH_SLOW,
    MSG_LED_FLASH_FAST,
    MSG_LED_FLASH_IRREGULAR,
}MsgLedType;


typedef enum
{
    LED_UP = 0x01,
    LED_MENU = 0x02,
    LED_POWER = 0x04,
    LED_TALK = 0x08,
    LED_UNLOCK = 0x10,
    LED_DOWN = 0x20,
    LED_BACK = 0x40,
    LED_DISTURB = 0x80,
}LED_ID;


// Define Object Function - Public----------------------------------------------
void hdLedRedCtrl(unsigned char iOnOff);
void hdLedGreenCtrl(unsigned char iOnOff);
void hdLedBlueCtrl(unsigned char iOnOff);
#if 0
void hd_LedInit(void);
void hdLedTalkCtrl(unsigned char iOnOff);
void hdLedUnlockCtrl(unsigned char iOnOff);
void hdLedPowerCtrl(unsigned char iOnOff);
void hdLedMenuCtrl(unsigned char iOnOff);
void hdLedUpCtrl(unsigned char iOnOff);
void hdLedDownCtrl(unsigned char iOnOff);
void hdLedBackCtrl(unsigned char iOnOff);
void hdLedNoDisturbCtrl(unsigned char iOnOff);

void hdLedTalkFlashCtrl(unsigned char ledCtrlType);            //lyx 20170727
void hdLedUnlockFlashCtrl(unsigned char ledCtrlType);
void hdLedPowerFlashCtrl(unsigned char ledCtrlType);
void hdLedMenuFlashCtrl(unsigned char ledCtrlType);
void hdLedUpFlashCtrl(unsigned char ledCtrlType);
void hdLedDownFlashCtrl(unsigned char ledCtrlType);
void hdLedBackFlashCtrl(unsigned char ledCtrlType);
void hdLedNoDisturbFlashCtrl(unsigned char ledCtrlType);
#endif

// Define Object Function - Private---------------------------------------------

#endif

