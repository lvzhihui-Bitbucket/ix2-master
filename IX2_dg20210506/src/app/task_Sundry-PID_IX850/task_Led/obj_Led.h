/**
  ******************************************************************************
  * @file    obj_Led.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power obj_Led.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
 */
#ifndef OBJ_LED_H
#define OBJ_LED_H

#include "task_Led.h"
#include "hd_Led.h"
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"

/*******************************************************************************
                         Define object property
*******************************************************************************/
typedef enum
{
	// lzh_20171021_s
	Led_DisplayScene_Undefine = -1,
	// lzh_20171021_e
	Led_DisplayScene_Normal = 0,
	Led_DisplayScene_KernelBoot,
	Led_DisplayScene_AppBoot,
	Led_DisplayScene_BootError,
	Led_DisplayScene_Ring,
	Led_DisplayScene_MonitorOrTalk,
	Led_DisplayScene_Diverting,
	Led_DisplayScene_RemoteMonitor,
	Led_DisplayScene_MissEvent,
	Led_DisplayScene_NotDisturb,
	Led_DisplayScene_FwUpdate,
	
}Led_DisplayScene_e;



typedef enum
{ 
	LED_STATUS_LightOFF					= 0x00,
	LED_STATUS_LightON					= 0x01,
	LED_STATUS_FastFlashing				= 0x02,
	LED_STATUS_NormalFlashing			= 0x03,
	LED_STATUS_ShortIrregularFlashing	= 0x04,
	LED_STATUS_LongIrregularFlashing	= 0x05,
}LedStatus_TypeDef;

typedef void (*LedCrtl)(unsigned char);
typedef void(*LedFlashCtrl)(unsigned char);     //lyx 20170727
#if 0
typedef struct
{
	unsigned char 	ledCtrlType;
	unsigned char 	ledState;
	unsigned char 	ledFlashCnt;
	unsigned char 	ledFlashTabIndex;
	unsigned char* 	ledFlashTab;
	unsigned short 	ledTiming;
	LedCrtl			ledCtrl;
	LedFlashCtrl            ledFlashCtrl;    	 //lyx 20170727
}LED_PROPERTY;

extern LED_PROPERTY ledTalk;
extern LED_PROPERTY ledUnlock;
extern LED_PROPERTY ledPower;   //lyx
extern LED_PROPERTY ledMenu;
extern LED_PROPERTY ledUp;
extern LED_PROPERTY ledDown;
extern LED_PROPERTY ledBack;
extern LED_PROPERTY ledDisturb; 
#endif
/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
void LedInit(void);
void LedMessageProcessing( uint8 ledCtrlType, uint8 led_select, uint16 iTime );
//uint8 GetLedState(LED_PROPERTY* led);		

/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/
void TimerLedFlashCallBack(void);
//void LedTimerProcess(LED_PROPERTY* led);
//void LedProcess(LED_PROPERTY* led, uint8 ledCtrlType, uint16 iTime);


#endif

