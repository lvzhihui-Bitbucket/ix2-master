/**
  ******************************************************************************
  * @file    task_Relay.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "task_Relay.h"
#include "obj_Relay_State.h"
#include "task_Sundry.h"
#include "task_survey.h"


//软定时
OS_TIMER relayTiming;	

/*------------------------------------------------------------------------
						task_init
------------------------------------------------------------------------*/
void vtk_TaskInit_Relay(void)
{
	//run_state init
	Relay_Run.relay_mode = NORMAL_OPEN;
	Relay_Run.relay_state = 0;
	
	//创建软定时(未启动)
	OS_CreateTimer(&relayTiming, Relay_Timer_Off, 100/25);	//100ms
	dprintf("Relay >>>vtk_TaskInit_Relay. \n");
}

/*------------------------------------------------------------------------
						task_process
------------------------------------------------------------------------*/
void vtk_TaskProcessEvent_Relay(MSG_RELAY *msgRelay)
{	    
    switch (msgRelay->msg_type)
    {
        case MSG_TYPE_RELAY_OPEN:
            Relay_Open_Timer(msgRelay->relay_timer);
            break;
			
        case MSG_TYPE_RELAY_CLOSE:
            Relay_Close();
            break;					 

        default:	
            break;
    }
}

/*------------------------------------------------------------------------
				软定时: 锁1开锁定时到,发送消息,使关锁
------------------------------------------------------------------------*/
void Relay_Timer_Off(void)
{
	Relay_Run.relay_timer_count++;
	if (Relay_Run.relay_timer_count >= Relay_Run.relay_timer)	//定时到
	{	
		MSG_RELAY buf;
		
		// push in buf
		buf.head.msg_source_id 	= 0;
		buf.head.msg_target_id 	= MSG_ID_SUNDRY;
		buf.head.msg_type 		= MSG_TYPE_RELAY;
		buf.head.msg_sub_type 	= 0;
		buf.msg_type 				= MSG_TYPE_RELAY_CLOSE;

		// 压入本地队列
		if(push_vdp_common_queue(&vdp_sundry_mesg_queue,  &buf, sizeof(MSG_RELAY)))
		{
			OS_RetriggerTimer(&relayTiming);
		}
		else	//消息发送成功
		{
			OS_StopTimer(&relayTiming);	
		}
	}	
	else	//定时未到
	{	
		OS_RetriggerTimer(&relayTiming);
	}		
}

/*------------------------------------------------------------------------
						发送消息=>Unlock(队列)
------------------------------------------------------------------------*/
void API_Relay_Timer(unsigned char msg_type_temp,unsigned char relay_timer)
{
	MSG_RELAY buf;

	// push in buf
	buf.head.msg_source_id 	= 0;
	buf.head.msg_target_id 	= MSG_ID_SUNDRY;
	buf.head.msg_type 		= MSG_TYPE_RELAY;
	buf.head.msg_sub_type 	= 0;
	buf.msg_type 				= msg_type_temp;
	buf.relay_timer 			= relay_timer;

	// 压入本地队列
	push_vdp_common_queue(&vdp_sundry_mesg_queue,  &buf, sizeof(MSG_RELAY));
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
