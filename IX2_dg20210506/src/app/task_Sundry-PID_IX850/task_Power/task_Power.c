/**
  ******************************************************************************
  * @file    task_Power.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the task_Power
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "../../../os/RTOS.h"
#include "../../../os/OSQ.h"
#include "../../../os/OSTIME.h"
#include "../task_Sundry.h"
#include "hd_Power.h"
#include "task_Power.h"
#include "obj_Power.h"
#include "../../task_survey/task_survey.h"

// timer
OS_TIMER timerBeepPower;


/******************************************************************************
 * @fn      vtk_TaskInit_Power()
 *
 * @brief   Task power initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Power(void)
{
	power.tftState   	= 0;
	power.talkState	= 0;
	power.ringState	= 0;
	power.beepState	= 0;
	power.videoState	= 0;
	hd_PowerInit();
    	// create timer, not start
//   	OS_CreateTimer(&timerBeepPower,TimerBeepPowerCallBack,5000/25);	
    	dprintf("Power >>>vtk_TaskInit_Power.\n");
}

/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Power()
 *
 * @brief   Task power loop routine
 *
 * @param   msgPower - power msg
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Power(MsgPower *msgPower)
{
	PowerOption( msgPower->msgType, msgPower->subMsg );
}

/******************************************************************************
 * @fn      TimerBeepPowerCallBack()
 *
 * @brief   Beep hold timeout, close beep power
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
/*void TimerBeepPowerCallBack(void)
{
    	MsgPower buf;
	
    	power.beepHold = 0;

 	buf.head.msg_source_id 	= 0;
	buf.head.msg_target_id 	= MSG_ID_SUNDRY;
	buf.head.msg_type 		= MSG_TYPE_POWER;
	buf.head.msg_sub_type 	= 0;
    	buf.msgMainType = MSG_TYPE_POWER;
    	buf.msgType = POWER_BEEP;
    	buf.subMsg = POWER_OFF;
    	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&buf, sizeof(MsgPower));
}
*/
/******************************************************************************
 * @fn      API_PowerRequst()
 *
 * @brief   Provide an application interface for other task to requst power
 *
 * @param   iMsgType -  power msg type, should be:
 *          0) POWER_TFT
 *          1) POWER_TALK
 *          2) POWER_RING
 *          3) POWER_BEEP
 *          
 * @param   iSubMsg -   power subMsg type, should be:
 *          1) POWER_ON
 *          2) POWER_OFF
 *
 * @return  0 - false, 1 - ok
 *****************************************************************************/
//unsigned char API_PowerRequst(unsigned char msg_source_id, unsigned char iMsgType, unsigned char iSubMsg)
unsigned char API_PowerRequst(unsigned char iMsgType, unsigned char iSubMsg)
{
    	MsgPower buf;    
    	// push in buf

 	buf.head.msg_source_id 	= 0;
	buf.head.msg_target_id 	= MSG_ID_SUNDRY;
	buf.head.msg_type 		= MSG_TYPE_POWER;
	buf.head.msg_sub_type 	= 0;
    	buf.msgMainType = MSG_TYPE_POWER;
    	buf.msgType = iMsgType;
    	buf.subMsg = iSubMsg;

	//vdp_task_t* pTask = GetTaskAccordingMsgID(msg_source_id);

	//if( pTask != NULL && pTask != task_sundry)
	//{
		push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&buf, sizeof(MsgPower));
		// �ȴ�ackӦ��
		//ret = WaitForBusinessACK( pTask->p_syc_buf, PROPERTY_VALUE_InnerRead, (char*)&data, &len, 5000 );
		//if( ret > 0 )
		//{
			return 1;
		//}
		//return 0;
	//}
	//else
	//{
		//vtk_TaskProcessEvent_Power( (MsgPower*)&buf );
		//return 1;
	//}
}


