#ifndef POWER_H
#define POWER_H

void hd_PowerInit(void);
unsigned char PowerOption(unsigned char iPowerType, unsigned char iOptionType);

void TFT_PowerCtrl( unsigned char iEnable );
void VDD12_PowerCtrl(uint8 iEnable);
void AMP_MUTE_PowerCtrl(uint8 iEnable);
void AP324_PowerCtrl(uint8 iEnable);
void MIC_MUTE_PowerCtrl(uint8 iEnable);
void POW324_PowerCtrl(uint8 iEnable);
void NT329_MUTE_PowerCtrl(uint8 iEnable);
void AU_RLCON_SWITCH( uint8 iEnable );
void ExtRing_PowerCtrl( unsigned char iEnable );


#endif
