
/**
  ******************************************************************************
  * @file    task_Beeper.h
  * @author  WGC
  * @version V1.0.0
  * @date    2012.10.25
  * @brief   This file contains the headers of task_Beeper.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef TASK_BEEPER_H
#define TASK_BEEPER_H

#include <RTOS.h>
#include "OSQ.h"
#include "OSTIME.h"
#include "task_survey.h"

// Define Task Vars and Structures----------------------------------------------
#define BEEP_TYPE_DI1     	0	
#define BEEP_TYPE_DI2     	1
#define BEEP_TYPE_DI3  	  	2
#define BEEP_TYPE_DI4  	  	3
#define BEEP_TYPE_DI5  	  	4
#define BEEP_TYPE_DI8  	  	5
#define BEEP_TYPE_DL1  	  	6
#define BEEP_TYPE_DL2  	  	7
#define BEEP_TYPE_DL1DI1  	8
#define BEEP_TYPE_DI1DL1  	9
#define BEEP_TYPE_PHONECALL  	10

#define TIMER_BEEP_STOP    	0xFF

// Define task interface-------------------------------------------------------------
#pragma pack(1)	//stm32_pack
typedef struct 
{
    VDP_MSG_HEAD head;	//cao_20151118
    uint8 beeptype;
} BEEP_STRUCT;
#pragma pack()	//stm32_pack

extern OS_TIMER timer_beep_control;

// Define Task 2 items----------------------------------------------------------

void vtk_TaskInit_Beep( void );
void vtk_TaskProcessEvent_Beep( BEEP_STRUCT *msgBeep );

// Define API-------------------------------------------------------------------
void API_Beep( uint8 beep_type ) ;

#define BEEP_KEY()					API_Beep(BEEP_TYPE_DI1)		
#define BEEP_INFORM()				API_Beep(BEEP_TYPE_DI2)		
#define BEEP_ERROR()				API_Beep(BEEP_TYPE_DI3)	
#define BEEP_CONFIRM()				API_Beep(BEEP_TYPE_DL1)	

#define BEEP_LINK_ERROR()			API_Beep(BEEP_TYPE_DI3)	
#define BEEP_DIAL_ERROR()			API_Beep(BEEP_TYPE_DI3)	
#define BEEP_CALL_CLOSE()			API_Beep(BEEP_TYPE_DI1)	
#define BEEP_CALL_CANCEL()			API_Beep(BEEP_TYPE_DI1)	


#define BEEP_UNLOCK()				API_Beep(BEEP_TYPE_DI1)		
#define BEEP_SHOW_CARD()			API_Beep(BEEP_TYPE_DI1)		
#define BEEP_SET_ENTRY()			API_Beep(BEEP_TYPE_DL1DI1)		
#define BEEP_SET_EXIT()				API_Beep(BEEP_TYPE_DI1DL1)		
#define BEEP_CARD_FULL()			API_Beep(BEEP_TYPE_DI5)		
#define BEEP_DISABLE_SHOW_CARD()	API_Beep(BEEP_TYPE_DI8)		
#define BEEP_ACCESS_ADD_ERROR()	API_Beep(BEEP_TYPE_DI8)		
#define BEEP_SR_BUSY()				API_Beep(BEEP_TYPE_DL2)		
#define BEEP_ACCESS_STATE_ERROR()	API_Beep(BEEP_TYPE_DI4)		
#define BEEP_ACCESS_UNLOCK_ERROR()	API_Beep(BEEP_TYPE_DI3)		
#define BEEP_ACCESS_ALREADY()		API_Beep(BEEP_TYPE_DL2)		



#define BEEP_T1()					API_Beep(BEEP_TYPE_DI1)		
#define BEEP_T2()					API_Beep(BEEP_TYPE_DI2)		
#define BEEP_T3()					API_Beep(BEEP_TYPE_DI3)		
#define BEEP_T4()					API_Beep(BEEP_TYPE_DI4)		
#define BEEP_T5()					API_Beep(BEEP_TYPE_DI5)		
#define BEEP_T6()					API_Beep(BEEP_TYPE_DI8)		
#define BEEP_T7()					API_Beep(BEEP_TYPE_DL1)
#define BEEP_T8()					API_Beep(BEEP_TYPE_DL2)		
#define BEEP_T9()					API_Beep(BEEP_TYPE_DL1DI1)
#define BEEP_T10()					API_Beep(BEEP_TYPE_DI1DL1)

#define BEEP_PhoneCall()				API_Beep(BEEP_TYPE_PHONECALL)		//czn_20180228
#define BEEP_PhoneCall_Stop()		API_Beep(TIMER_BEEP_STOP)

#endif
