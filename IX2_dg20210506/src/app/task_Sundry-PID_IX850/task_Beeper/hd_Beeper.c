/**
  ******************************************************************************
  * @file    hd_beeper.c
  * @author  WGC
  * @version V1.0.0
  * @date    2012.10.25
  * @brief   This file contains the functions of hd_beeper
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "task_Beeper.h"
#include "hd_Beeper.h"
#include "task_Hal.h"
#include "task_survey.h"
#include "vdp_uart.h"
#include "task_Power.h"

//#define BPT	5
#define BPT		2	//BEEP timer base just 25ms

//BEEP?????
const uint8 DI1[2]		= {1,	BPT};					//??50ms
const uint8 DI2[4]		= {3,	BPT,BPT*2,BPT};			//??50ms,??100ms,??50ms
const uint8 DI3[6]		= {5,	BPT,BPT*2,BPT,BPT*2,BPT};		//??50ms,??100ms,??50ms,??100ms,??50ms
const uint8 DI4[8]		= {7,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};	//??50ms,??100ms,??50ms,??100ms,??50ms,??100ms,??50ms
const uint8 DI5[10]		= {9,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};					//??50ms,??100ms......??50ms,??100ms,??50ms
const uint8 DI8[16]		= {15,	BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT,BPT*2,BPT};	//??50ms,??100ms......??50ms,??100ms,??50ms
const uint8 DL1[2]		= {1,	BPT*10};				//??500ms
const uint8 DL2[4]		= {3,	BPT*10,	BPT*2,	BPT*10};		//??500ms,??100ms,??500ms
const uint8 DL1DI1[4]	= {3,	BPT*10,	BPT*2,	BPT};			//??500ms,??100ms,??50ms
const uint8 DI1DL1[4]	= {3,	BPT*2,	BPT*2,	BPT*10};		//??50ms,??100ms,??500ms
const uint8 PHONECALL[4]	= {3,	BPT*10,	BPT*2,	BPT*10};

const uint8* const ptrBeepRomTab[] =
{
	(const uint8*)DI1,
	(const uint8*)DI2,
	(const uint8*)DI3,
	(const uint8*)DI4,
	(const uint8*)DI5,
	(const uint8*)DI8,
	(const uint8*)DL1,
	(const uint8*)DL2,
	(const uint8*)DL1DI1,
	(const uint8*)DI1DL1,
	(const uint8*)PHONECALL,
};

BEEP_RUN beep_run;
extern int hal_fd;
/*------------------------------------------------------------------------
						Beep_init
------------------------------------------------------------------------*/
void Beep_Init( void )
{
	char cmd[100];
	strcpy(cmd,"echo set 300 150 > /dev/buzzer ");
	system(cmd);
	sync();
	//Sound_Disable();
}

/*------------------------------------------------------------------------
						Sound_Enable
------------------------------------------------------------------------*/
void Sound_Enable( void )
{
	//printf("Beep  >>>beep_run.state = %d.\n", pwn_on );
	//if( get_pane_type() == 2 )
	{
		char cmd[100];
		strcpy(cmd,"echo start > /dev/buzzer ");
		system(cmd);
		//sync();
	}
	#if 0
	else
	{
		int pwn_on = 1;
		//IX2_TEST	api_uart_send_pack(UART_TYPE_BEEP_C, &pwn_on, 1); 
	}
	#endif
	//ioctl( hal_fd, PWN_OUT_SET, &pwn_on );
}

/*------------------------------------------------------------------------
						Sound_Off
------------------------------------------------------------------------*/
void Sound_Disable( void )
{
	//printf("Beep  >>>beep_run.state = %d.\n", pwn_off );

	//if( get_pane_type() == 2 )
	{
		char cmd[100];
		strcpy(cmd,"echo stop > /dev/buzzer ");
		system(cmd);
		//sync();
	}
	#if 0
	else
	{
		int pwn_off = 0;
		//IX2_TEST	api_uart_send_pack(UART_TYPE_BEEP_C, &pwn_off, 1); 
	}
	#endif
	//ioctl( hal_fd, PWN_OUT_SET, &pwn_off );
}


/*------------------------------------------------------------------------
						??Beep
------------------------------------------------------------------------*/
void Beep_On( uint8 beep_type )
{
#if 1	
//	API_POWER_BEEP_ON();	//zxj_add_20151124
	PowerOption( POWER_BEEP, POWER_ON );
#if 0
	if( get_pane_type() == 2 )
		AMPMUTE_SET2();
	else
		AMPMUTE_SET1();
#endif
	usleep(10000);
	Sound_Enable();
	beep_run.state = BEEP_ON;
	beep_run.type = beep_type;
	beep_run.count = 1;
	beep_run.stage = BEEP_SOUND;		
	OS_SetTimerPeriod( &timer_beep_control, (*(ptrBeepRomTab[beep_run.type] + beep_run.count)) );
	OS_RetriggerTimer( &timer_beep_control );
			
	//dprintf("Beep  >>>beep_run.state = %d.\n", beep_run.state );
#else
	//API_POWER_BEEP_ON();	//zxj_add_20151124
	PowerOption( POWER_BEEP, POWER_ON );
	Sound_Enable();
	usleep(50000);
	//API_POWER_BEEP_OFF();	//zxj_add_20151124
	PowerOption( POWER_BEEP, POWER_OFF );
	Sound_Disable();	
#endif

}

/*------------------------------------------------------------------------
						Beep_Off
------------------------------------------------------------------------*/
void Beep_Off( void )
{	
	beep_run.state = BEEP_OFF;	
    OS_StopTimer( &timer_beep_control );
	//API_POWER_BEEP_OFF();	//zxj_add_20151124
	//AMPMUTE_RESET();
	Sound_Disable();	
	PowerOption( POWER_BEEP, POWER_OFF );
		
	//dprintf("Beep  >>>beep_run.state = %d.\n", beep_run.state );


}

/*------------------------------------------------------------------------
						Get_BeepState
------------------------------------------------------------------------*/
uint8 Get_BeepState( void )
{
	return (beep_run.state);
}


