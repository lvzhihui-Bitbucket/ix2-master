/**
  ******************************************************************************
  * @file    obj_RingControl.c
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of obj_RingControl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
//#include "RTOS.h"
#include "../task_Sundry.h"
//#include "task_Power.h"
#include "task_Ring.h"
#include "obj_RingControl.h"
#include "obj_RingConfig.h"
//#include "../../task_RingPlayer/task_player.h"
#include "../../task_survey/task_survey.h"
#include "../task_Power/task_Power.h"	//zxj_add_20151124


#define CYCLE_STATE_STOP   0
#define CYCLE_STATE_PLAY   1

#define SRESCENDO_TIME_INTERVAL    1000


static RingCtrl_TypeDef ringCtrl;
static OS_TIMER timer_CountLimit;  //用于按次数播放铃声
static OS_TIMER timer_TimeLimit;  //用于按时长播放铃声
static OS_TIMER timer_Crescendo;//用于铃声渐强


static void Timer_CountLimit_Callback(void)
{
	if (ringCtrl.RingCount >= ringCfg.RingCountLimit)
	{
		//不要直接调用RingStop(),以免在关power时,在timer中调用power的API,从而因为使用OS_PutMail()而产生错误;
		//cao_20181020_s
		OS_StopTimer(&timer_CountLimit);
		//cao_20181020_e
		API_RingStop();
		return;
	}

	if (ringCtrl.RingCycleState == CYCLE_STATE_PLAY)//进入Timer ISR之前处于"CYCLE_STATE_PLAY"状态
	{
		ringCtrl.RingCycleState = CYCLE_STATE_STOP;
		API_WavePlayer_Stop();
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleGap/25);		//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
	else //进入Timer ISR之前处于"CYCLE_STATE_STOP"状态
	{
		ringCtrl.RingCycleState = CYCLE_STATE_PLAY;
		API_WavePlayer_Play(ringCfg.RingTune);
		ringCtrl.RingCount++;
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleTimeLimit/25);	//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
}

static void Timer_TimeLimit_Callback(void)
{
	if (ringCtrl.RingTime >= (1000 * ringCfg.RingTimeLimit))	//czn_20170113		//if (ringCtrl.RingTime >= ((1000/25) * ringCfg.RingTimeLimit))
	{
		//不要直接调用RingStop(),以免在关power时,在timer中调用power的API,从而因为使用OS_PutMail()而产生错误;
		//cao_20181020_s
		OS_StopTimer(&timer_TimeLimit);
		//cao_20181020_e
		API_RingStop();
		return;
	}

	if (ringCtrl.RingCycleState == CYCLE_STATE_PLAY)//进入Timer ISR之前处于"CYCLE_STATE_PLAY"状态
	{
		ringCtrl.RingCycleState = CYCLE_STATE_STOP;
		API_WavePlayer_Stop();
		ringCtrl.RingTime += ringCfg.RingCycleGap;
		OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleGap/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}
	else //进入Timer ISR之前处于"CYCLE_STATE_STOP"状态
	{
		ringCtrl.RingCycleState = CYCLE_STATE_PLAY;
		API_WavePlayer_Play(ringCfg.RingTune);
		ringCtrl.RingTime += ringCfg.RingCycleTimeLimit;
		OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}
}


static void Timer_Crescendo_Callback(void)
{
	if( RingGetState() != RING_STATE_PLAY )
	{
		return;
	}
	ringCtrl.RingCrescendoCnt++;
	API_WavePlayer_SetVolume(ringCtrl.RingCrescendoCnt);
	if(ringCtrl.RingCrescendoCnt < ringCfg.RingVolume)
	{
		OS_RetriggerTimer(&timer_Crescendo);//启动软定时
	}
}


void RingCtrl_Init(void)
{
	//创建软定时(未启动)
	OS_CreateTimer(&timer_CountLimit, Timer_CountLimit_Callback, 1);
	OS_CreateTimer(&timer_TimeLimit, Timer_TimeLimit_Callback, 1);
	OS_CreateTimer(&timer_Crescendo, Timer_Crescendo_Callback, SRESCENDO_TIME_INTERVAL/25);
}

/**
 * @brief   Start ring
 * @param   None
 * @return  None
 */
void RingCtrl_Start(void)
{
	//申请电源
	//API_POWER_RING_ON();
	#if 0
	if((ringCfg.RingCrescendo == 1) && (ringCfg.RingVolume > 1))
	{
		ringCtrl.RingCrescendoCnt = 1;//铃声渐强是从逻辑音量1起始
		API_WavePlayer_SetVolume(ringCtrl.RingCrescendoCnt);
		OS_RetriggerTimer(&timer_Crescendo);//启动软定时
	}
	else // in case of NOT crescendo effect or ringCfg.RingVolume is 0 or 1
	{
	
		API_WavePlayer_SetVolume(ringCfg.RingVolume);
	}
	#endif
	API_WavePlayer_SetVolume(ringCfg.RingVolume);
	// start ring
	RingSetState(RING_STATE_PLAY);	//播放标志
	ringCtrl.RingCycleState = CYCLE_STATE_PLAY;

	//ringCfg.RingTune = 1;
	API_WavePlayer_Play(ringCfg.RingTune);     //启动播放

	if (ringCfg.RingCtrlPriority == PRIORITY_COUNT_LIMIT)
	{
		ringCtrl.RingCount = 1;
		OS_SetTimerPeriod(&timer_CountLimit, ringCfg.RingCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_CountLimit);//启动软定时
	}
	else if (ringCfg.RingCtrlPriority == PRIORITY_TIME_LIMIT)
	{
		ringCtrl.RingTime = ringCfg.RingCycleTimeLimit;
		OS_SetTimerPeriod(&timer_TimeLimit, ringCfg.RingCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_TimeLimit);//启动软定?
	}
}

/**
 * @brief   Stop ring
 * @param   None
 * @return  None
 */
void RingCtrl_Stop(unsigned char powerOff)
{
	OS_StopTimer(&timer_CountLimit);
	OS_StopTimer(&timer_TimeLimit);
	API_WavePlayer_Stop();
	if( powerOff )
	{
		//API_POWER_RING_OFF();
	}

	RingSetState(RING_STATE_STOP);
	ringCtrl.RingCycleState   = CYCLE_STATE_STOP;
	ringCtrl.RingCount        = 0;
	ringCtrl.RingTime         = 0;
	ringCtrl.RingCrescendoCnt = 0;
}

/**
 * @brief   Get the running ring state
 * @param   None
 * @return  ringCtrl.RingState
 */
uint8 RingGetState(void)
{
	return ringCtrl.RingState;
}



/**
 * @brief   change the the ring running state
 * @param   state RING_STATE_PLAY or RING_STATE_STOP
 * @return  None
 */
void RingSetState(uint8 state)
{
	ringCtrl.RingState = state;
}

