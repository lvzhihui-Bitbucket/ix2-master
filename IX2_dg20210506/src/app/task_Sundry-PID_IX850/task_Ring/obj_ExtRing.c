
#include "task_Ring.h"
#include "../task_Power/task_Power.h"


OS_TIMER timer_ExtRing;
unsigned char extRingState = 0;
unsigned char extRingTimer = 0;
unsigned char extRingCycle = 0;
unsigned char extRingRun = 0;

unsigned char extRingToltalCycle;
unsigned char extRingOnTime;
unsigned char extRingOffTime;

void Timer_CallbackExitRing(void)
{
    API_ExtRingCtrl(EXT_RING_TIMER_EVENT);
}

void ExtRingInit(void)
{
    OS_CreateTimer(&timer_ExtRing, Timer_CallbackExitRing, 100/25);
}



void ExitRingCtrl(unsigned char onOff)
{
    if( onOff )
    {
        //API_Event_IoServer_InnerRead_All(EXT_RING_CYCLES, (unsigned char*)&extRingToltalCycle );
       // API_Event_IoServer_InnerRead_All(EXT_RING_ON_TIME, (unsigned char*)&extRingOnTime );

	extRingToltalCycle = 3;
	extRingOnTime = 8; 
	
        if( (!extRingToltalCycle) || (!extRingOnTime) )
        {
            return;
        }
		
        //API_Event_IoServer_InnerRead_All(EXT_RING_OFF_TIME, (unsigned char*)&extRingOffTime );    
        extRingOffTime = 15;
        
        //API_POWER_EXT_RING_ON();
		API_PowerRequst( POWER_EXT_RING, POWER_ON );
        extRingState = 1;
        extRingRun = 1;
        extRingTimer = 0;
        extRingCycle = 0;
        OS_RetriggerTimer(&timer_ExtRing);
    }
    else
    {
        if( extRingRun )
        {
            extRingRun = 0;
            extRingCycle = 0;
            //API_POWER_EXT_RING_OFF();
            API_PowerRequst( POWER_EXT_RING, POWER_OFF ); //lyx_20171026
            extRingState = 0;
            OS_StopTimer(&timer_ExtRing);
        }
    }
}


void ExtRingTimerEventHandler(void)
{
    if( !extRingRun )
    {
        return;
    }
    if( extRingState )
    {
        if( ++extRingTimer >= extRingOnTime )
        {
            extRingTimer = 0;
            if( extRingOffTime )
            {
                //API_POWER_EXT_RING_OFF();
				API_PowerRequst( POWER_EXT_RING, POWER_OFF ); //lyx_20171026
            }
            extRingState = 0;
        }
    }
    else
    {
        if( ++extRingTimer >= extRingOffTime )
        {
            extRingTimer = 0;
            if( ++extRingCycle >= extRingToltalCycle )
            {
                ExitRingCtrl(0);
                return;
            }
            if( extRingOffTime )
            {
                //API_POWER_EXT_RING_ON();
				API_PowerRequst( POWER_EXT_RING, POWER_ON ); //lyx_20171026
            }
            extRingState = 1;
        }
    }
    OS_RetriggerTimer(&timer_ExtRing);
}
