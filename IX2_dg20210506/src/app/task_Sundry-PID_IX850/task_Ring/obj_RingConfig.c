/**
  ******************************************************************************
  * @file    obj_RingConfig.c
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of obj_RingConfig
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "task_Ring.h"
#include "task_IoServer.h"
#include "obj_RingConfig.h"
#include "obj_RingControl.h"
//#include "../../task_PlayList/task_PlayList.h"


RingCfg_TypeDef ringCfg;



/* alarm scene tune tab*/
const uint8 RingAlamSceneTune[] =
{
	1, 2, 3, 4
};

/* alarm tune cycle time, 1ms/uint */
const uint16 RingAlamSceneCycleTimeLimit[] =
{
	100, 100, 100, 100
};

/* all kinds of scene ring gap, 1ms/uint */
const uint16 RingCycleGap[] =
{
	2000, 2000, 2000, 50, 50, 50, 50
};

const unsigned char RingPrompt[] = 
{
	100,101
};
// zfz_20190601
const RingCfg_Table RingCfg[] = 
{
	{RING_DS, 			DS_TUNE_SELECT, 		CALL_TUNE_TIME_LIMIT},
	{RING_CDS, 			CDS_TUNE_SELECT, 		CALL_TUNE_TIME_LIMIT},
	{RING_OS, 			OS_TUNE_SELECT, 		CALL_TUNE_TIME_LIMIT},
	{RING_MSG, 			MSG_TUNE_SELECT, 		CALL_TUNE_TIME_LIMIT},
	{RING_Doorbell, 	DOORBELL_TUNE_SELECT, 	CALL_TUNE_TIME_LIMIT},
	{RING_Intercom, 	INTERCOM_TUNE_SELECT, 	CALL_TUNE_TIME_LIMIT},
	{RING_InnerCall, 	INNERCALL_TUNE_SELECT, 	CALL_TUNE_TIME_LIMIT},
	{RING_ALARM,		ALARM_TUNE_SELECT, 		CALL_TUNE_TIME_LIMIT},
};

void Ring_LoadScene(uint8 scene)
{
	int i;
	char temp[20];
	for(i = 0; i < RING_TYPE_MAX; i++)
	{
		if(RingCfg[i].RingType == scene)
		{
			break;
		}
	}
	if(i == RING_TYPE_MAX)
	{
		Ring_ConfigTryVolumeScene(1);
	}
	else
	{
		ringCfg.RingCycleGap = 2000;
		ringCfg.RingCrescendo = 0;
		
		API_Event_IoServer_InnerRead_All(RingCfg[i].RingTuneIoId, temp);
		ringCfg.RingTune = atoi(temp);
		//API_Event_IoServer_InnerRead_All(RingCfg[i].RingCountLimitIoId, temp);
		//ringCfg.RingCountLimit = atoi(temp);
		
		API_Event_IoServer_InnerRead_All(RingCfg[i].RingCycleTimeLimitIoId, temp);
		ringCfg.RingTimeLimit = atoi(temp);
		
		//API_Event_IoServer_InnerRead_All(RingCfg[i].RingCtrlPriorityIoId, temp);
		//ringCfg.RingCtrlPriority = atoi(temp);

		API_PlayList_GetUsingLength_ms(ringCfg.RingTune, &ringCfg.RingCycleTimeLimit);	//czn_20170724
		
		if(RingCfg[i].RingType == RING_Doorbell)
		{
			ringCfg.RingCountLimit = 1;
			ringCfg.RingCtrlPriority = PRIORITY_COUNT_LIMIT;
		}
		else
		{
			uint32 ringTime;

			ringCfg.RingCtrlPriority = PRIORITY_TIME_LIMIT;

			ringTime = ringCfg.RingTimeLimit*1000;
			
			if(ringTime <= ringCfg.RingCycleTimeLimit)
			{
				ringTime = ringCfg.RingCycleTimeLimit;
			}
			else
			{
				uint32 remainder;
				remainder = (ringTime - ringCfg.RingCycleTimeLimit)%(ringCfg.RingCycleTimeLimit + ringCfg.RingCycleGap);
				if(remainder <= ringCfg.RingCycleGap)
				{
					ringTime = ringTime - remainder;
				}
				else
				{
					ringTime = ringTime + (ringCfg.RingCycleTimeLimit + ringCfg.RingCycleGap - remainder);
				}
			}
		
			ringCfg.RingTimeLimit = ringTime/1000 + (ringTime%1000 ? 1 : 0);
			//printf("----RingCycleTime = %d---RingTimeLimit = %d----\n", ringCfg.RingCycleTimeLimit, ringCfg.RingTimeLimit);
		}

	}

	//API_Event_IoServer_InnerRead_All(GetRtcState(MSG_ID_SUNDRY) == 1 ? NIGHT_CALL_VOLUME : DAY_CALL_VOLUME, temp);
	API_Event_IoServer_InnerRead_All(DAY_CALL_VOLUME, temp);
	ringCfg.RingVolume = atoi(temp);
}


void Ring_ConfigTryTuneScene(uint8 tune)
{
	char temp[20];
	
    ringCfg.RingTune = tune;
	// read call scene volume from eeprom according to day or night
	//API_Event_IoServer_InnerRead_All(GetRtcState(MSG_ID_SUNDRY) == 1 ? NIGHT_CALL_VOLUME : DAY_CALL_VOLUME, temp);
	API_Event_IoServer_InnerRead_All(DAY_CALL_VOLUME, temp);
	ringCfg.RingVolume = atoi(temp);

	ringCfg.RingCrescendo = 0;
	API_PlayList_GetUsingLength_ms(ringCfg.RingTune, &ringCfg.RingCycleTimeLimit);
	ringCfg.RingCountLimit = 1;
	ringCfg.RingTimeLimit = 10;
	ringCfg.RingCtrlPriority = PRIORITY_COUNT_LIMIT;//PRIORITY_TIME_LIMIT
	ringCfg.RingCycleGap = 1000;
}

void Ring_ConfigTryVolumeScene(uint8 volume)
{
	ringCfg.RingTune = 1;
	ringCfg.RingVolume = volume;
	ringCfg.RingCrescendo = 0;
	API_PlayList_GetUsingLength_ms(ringCfg.RingTune, &ringCfg.RingCycleTimeLimit);
	ringCfg.RingCountLimit = 1;
	ringCfg.RingCtrlPriority = PRIORITY_COUNT_LIMIT;
	ringCfg.RingCycleGap = 1000;
}


/**
 * @brief   Get the ring config tune
 * @param   none
 * @return  ringCfg.RingCallSceneData.RingTune
 */
uint8 Ring_GetTune(void)
{
	return ringCfg.RingTune;
}

/**
 * @brief   Set the ring config tune
 * @return  none
 */
void Ring_SetTune(uint8 logic_tune)
{
	ringCfg.RingTune = logic_tune;
}

/**
 * @brief   Get the ring config volume scale
 * @param   none
 * @return  ringCfg.RingVolume
 */
uint8 Ring_GetVolume(void)
{
	return ringCfg.RingVolume;
}

/**
 * @brief   Set the ring config volume scale
 * @param   vol - the volume scale to be set
 * @return  none
 */
void Ring_SetVolume(uint8 logic_volume)
{
	ringCfg.RingVolume = logic_volume;
}

