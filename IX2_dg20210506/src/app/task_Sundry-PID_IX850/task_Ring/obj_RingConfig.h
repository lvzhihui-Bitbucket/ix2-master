/**
  ******************************************************************************
  * @file    obj_RingConfig.h
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of obj_RingConfig
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef OBJ_RING_CONFIG_H
#define OBJ_RING_CONFIG_H

#define PRIORITY_COUNT_LIMIT     0
#define PRIORITY_TIME_LIMIT      1

// Define Object Property-------------------------------------------------------
typedef struct
{
    uint8  RingScene;         // mark current scene index
    uint8  RingTune;          // scene logic tune
    uint8  RingVolume;        // current logical ring volume
    uint8  RingCrescendo;     // 铃声渐强, 0 or 1
    uint8  RingCtrlPriority;  // PRIORITY_COUNT_LIMIT or PRIORITY_TIME_LIMIT
    uint8  RingCountLimit;    // the whole ring times limit of the scene tune
    uint8  RingTimeLimit;     // the whole ring time limit of the scene tune, 1s/unit
    uint32 RingCycleTimeLimit;// 曲子每次播放的时长。one cycle time limit, 1ms/unit
    uint32 RingCycleGap;      // 间隔intermission of one cycle and the next cycle, 1ms/uint
} RingCfg_TypeDef;

typedef struct
{
    int  RingType;    
    int  RingTuneIoId;     
    //int  RingCountLimitIoId;       
    int  RingCycleTimeLimitIoId;
	//int  RingCtrlPriorityIoId;
} RingCfg_Table;

extern RingCfg_TypeDef ringCfg;

// Define Object Function - Public----------------------------------------------
void Ring_LoadScene(uint8 scene);
void Ring_LoadDsSeperatedScene(uint8 callSource);

void Ring_ConfigTryTuneScene(uint8 tune);
void Ring_ConfigTryVolumeScene(uint8 volume);

uint8 Ring_GetTune(void);
void Ring_SetTune(uint8 logic_tune);

uint8 Ring_GetVolume(void);
void Ring_SetVolume(uint8 logic_volume);

// Define Object Function - Private---------------------------------------------
//#define API_PlayList_GetUsingLength_ms(ttime)		((ttime+1-ttime)*1000)
#endif
