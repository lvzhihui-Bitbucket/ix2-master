/**
  ******************************************************************************
  * @file    obj_RingControl.h
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of obj_RingControl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef __OBJ_RINGCONTROL_H
#define __OBJ_RINGCONTROL_H

#define RING_STATE_STOP   0
#define RING_STATE_PLAY   1

// Define Object Property-------------------------------------------------------
typedef struct
{
    uint8 RingState;                // the whole ring state,ring or stop
    uint8 RingCycleState;           // current cycle state, play or stop
    uint8 RingCount;                // how many cycles ring counter
    uint32 RingTime;                // the whole ring running time ,1ms/unit
    uint8 RingCrescendoCnt;         // ��ǿ
} RingCtrl_TypeDef;

// Define Object Function - Public----------------------------------------------
void RingCtrl_Init(void);

void RingCtrl_Start(void);
void RingCtrl_Stop(unsigned char powerOff);

uint8 RingGetState(void);
void  RingSetState(uint8 state);

void Ring_LoadDsSeperatedScene(uint8 callSource);
// Define Object Function - Private---------------------------------------------
//zxj_add_20151124 #define API_POWER_RING_ON()	
//zxj_add_20151124 #define API_POWER_RING_OFF()	
#endif

