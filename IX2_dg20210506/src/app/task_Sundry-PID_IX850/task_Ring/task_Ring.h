/**
  ******************************************************************************
  * @file    task_Ring.h
  * @author  L
  * @version V1.0.0
  * @date    2014.01.08
  * @brief   This file contains the functions of task_Ring
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#ifndef TASK_RING_H
#define TASK_RING_H

#include "task_survey.h"



// Define Task Vars and Structures----------------------------------------------
// zfz_20190601
typedef enum
{
    RING_DS,
    RING_CDS,
    RING_OS,
    RING_MSG,
    RING_Doorbell,
    RING_Intercom,
    RING_InnerCall,
    RING_ALARM,
    
    RING_TYPE_MAX,
} Ring_Scene_Type;

typedef enum
{
    EXT_RING_OFF,
    EXT_RING_ON,
    EXT_RING_TIMER_EVENT,
} ExtRing_Type;


// Define task interface--------------------------------------------------------

// Define Message Structure-----------------------------------------------------
#pragma pack(1)
typedef struct
{
    	VDP_MSG_HEAD head;
	uint8 cmd;
	uint8 subMsg;
} RING_STRUCT ;
#pragma pack()

// Define Sub_Type--------------------------------------------------------------
#define RING_STOP           0
#define RING_PLAY           1
#define RING_SET_TUNE       2
#define RING_SET_VOLUME     3
#define RING_PLAY_DS_SEP    4
#define RING_STOP_KEEP_PWR  5
#define EXT_RING            6

// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_Ring( void );
void vtk_TaskProcessEvent_RingCtrl(RING_STRUCT *msgRing);

// Define Task others-----------------------------------------------------------

// Define API-------------------------------------------------------------------
void API_RingPlay(Ring_Scene_Type scene_temp);
void API_RingTuneSelect(unsigned char iTune);
void API_RingVolSelect(unsigned char iVol);
void API_RingStop(void);
void API_RingStopKeepPwr(void);

void API_ExtRingCtrl(ExtRing_Type msg);   //lyx_20170921



#endif

