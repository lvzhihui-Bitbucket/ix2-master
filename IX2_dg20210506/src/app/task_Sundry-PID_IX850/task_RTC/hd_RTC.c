/**
  ******************************************************************************
  * @file    hd_RTC.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the function of hd_RTC.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "hd_RTC.h"
#include <stdio.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <signal.h>

#define RTC_RD_TIME _IOR('p', 0x09, struct rtc_time) /* Read RTC time   */ 
#define RTC_SET_TIME    _IOW('p', 0x0a, struct rtc_time) /* Set RTC time    */

/*******************************************************************************
 * @fn      hd_RtcInit()
 *
 * @brief   init rtc hardware block
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void hd_RtcInit(void)
{
/*
#if ((defined PCF8563) && (PCF8563 == 1) )
#else
    Stm32_RtcConfig(RTC_HourFormat_24);
#endif
*/
}


/*******************************************************************************
 * @fn      hd_RtcSetDate()
 *
 * @brief   set rtc date
 *
 * @param   iDate - date init data, in RTC_DateTypeDef type
 *
 * @return  none
 ******************************************************************************/
void hd_RtcSetDate(RTC_DateTypeDef iDate)
{
	RTC_TIME time;

	time = hd_N329GetTime();
	time.date_t= iDate;
	hd_N329SetTime(time);
}

/*******************************************************************************
 * @fn      hd_RtcGetDate()
 *
 * @brief   get date from rtc 
 *
 * @param   none
 *
 * @return  date in RTC_DateTypeDef struct
 ******************************************************************************/
RTC_DateTypeDef hd_RtcGetDate(void)
{
	RTC_DateTypeDef date;
	int year,month,day;

	date = hd_N329GetTime().date_t;

	year 	= ((date.RTC_Year>>4)&0x0F)*10 + (date.RTC_Year & 0x0F) + 2000;
	month 	= ((date.RTC_Month>>4)&0x0F)*10 + (date.RTC_Month & 0x0F);
	day 		= ((date.RTC_Date>>4)&0x0F)*10 + (date.RTC_Date & 0x0F);
	
	if(month==1||month==2)//�ж�month�Ƿ�Ϊ1��2��
	{
		year--;
		month+=12;
	}
	int c = year/100;
	int y = year-c*100;
	int week = (c/4)-2*c+(y+y/4) + (13*(month+1)/5) + day - 1;
	week = (week%7+7)%7;

	date.RTC_WeekDay = ((week/10)<<4) + (week % 10);
	
	return date;
}
void hd_RtcSetTime(RTC_TimeTypeDef iTime)
{
	RTC_TIME time;

	time = hd_N329GetTime();
	time.time_t = iTime;
	hd_N329SetTime(time);
}
RTC_TimeTypeDef hd_RtcGetTime(void)
{
	return hd_N329GetTime().time_t;
}
/*******************************************************************************
 * @fn      hd_N329SetTime()
 *
 * @brief   set time to rtc
 *
 * @param   iDate - SetTime init data, in BCD,24 hours format
 *
 * @return  none
 ******************************************************************************/
void hd_N329SetTime(RTC_TIME iTime)
{
	struct rtc_time rtc_tm;
	int retval;
	int fd = open ("/dev/rtc0", O_RDONLY);

	if( fd < 0)
	{
		printf("\topen /dev/rtc faild!!!\n");
		return ;
	}
	rtc_tm.tm_year 	= (((iTime.date_t.RTC_Year>>4)&0x0F)*10 + (iTime.date_t.RTC_Year & 0x0F) +2000) - 1900;
	rtc_tm.tm_mon 	= ((iTime.date_t.RTC_Month>>4)&0x0F)*10 + (iTime.date_t.RTC_Month & 0x0F) - 1;
	rtc_tm.tm_mday 	= ((iTime.date_t.RTC_Date>>4)&0x0F)*10 + (iTime.date_t.RTC_Date & 0x0F);
	rtc_tm.tm_hour	=  ((iTime.time_t.RTC_Hours>>4)&0x0F)*10 + (iTime.time_t.RTC_Hours & 0x0F);
	rtc_tm.tm_min 	= ((iTime.time_t.RTC_Minutes>>4)&0x0F)*10 + (iTime.time_t.RTC_Minutes & 0x0F);
	rtc_tm.tm_sec 	= ((iTime.time_t.RTC_Seconds>>4)&0x0F)*10 + (iTime.time_t.RTC_Seconds & 0x0F);
  
	retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
	if( retval < 0 ) 
	{
		printf("ioctl RTC_SET_TIME  faild!!!\n");
		close(fd);			
		return ;
	}

	/*print current time*/
	printf("Adjust current RTC time as: %04d-%02d-%02d %02d:%02d:%02d\n\n",
	rtc_tm.tm_year + 1900,
	rtc_tm.tm_mon + 1,
	rtc_tm.tm_mday,
	rtc_tm.tm_hour,
	rtc_tm.tm_min,
	rtc_tm.tm_sec);

	close(fd);

}

/*******************************************************************************
 * @fn      hd_N329GetTime()
 *
 * @brief   get time from rtc
 *
 *
 * @return  time, in BCD,24 hours format
 ******************************************************************************/
RTC_TIME hd_N329GetTime(void)
{
	struct rtc_time rtc_tm;
	RTC_TIME iTime;
	int retval;
	int fd = open ("/dev/rtc0", O_RDONLY);

	if( fd < 0)
	{
		printf("\topen /dev/rtc faild!!!\n");
		return iTime;
	}
	
	/*read current time*/

	retval=ioctl(fd,RTC_RD_TIME,&rtc_tm);
	if( retval < 0 ) 
	{
		printf("ioctl RTC_RD_TIME  faild!!!\n");
		close(fd);
		return iTime;
	}

	
	iTime.date_t.RTC_Year			= (((rtc_tm.tm_year+1900 - 2000)/10)<<4) + ((rtc_tm.tm_year+1900 - 2000)%10);
	iTime.date_t.RTC_Month		= (((rtc_tm.tm_mon + 1)/10)<<4) + ((rtc_tm.tm_mon + 1)%10);
	iTime.date_t.RTC_Date			= ((rtc_tm.tm_mday/10)<<4) + (rtc_tm.tm_mday % 10);
	iTime.time_t.RTC_Hours		= ((rtc_tm.tm_hour/10)<<4) + (rtc_tm.tm_hour % 10);
	iTime.time_t.RTC_Minutes		= ((rtc_tm.tm_min/10)<<4) + (rtc_tm.tm_min % 10);
	iTime.time_t.RTC_Seconds		= ((rtc_tm.tm_sec/10)<<4) + (rtc_tm.tm_sec % 10);

	//printf("date = %02x : %02x : %02x\ntime = %02x : %02x : %02x\n", iTime.date_t.RTC_Year, iTime.date_t.RTC_Month, iTime.date_t.RTC_Date, iTime.time_t.RTC_Hours, iTime.time_t.RTC_Minutes, iTime.time_t.RTC_Seconds);
	/*print current time*/
	/*
	printf("Read current RTC time is: %04d-%02d-%02d %02d:%02d:%02d\n\n",
	rtc_tm.tm_year + 1900,
	rtc_tm.tm_mon + 1,
	rtc_tm.tm_mday,
	rtc_tm.tm_hour,
	rtc_tm.tm_min,
	rtc_tm.tm_sec);
	*/
	close(fd);
	return iTime;
}

