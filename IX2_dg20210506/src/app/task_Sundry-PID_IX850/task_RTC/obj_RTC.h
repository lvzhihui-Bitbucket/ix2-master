/**
  ******************************************************************************
  * @file    obj_RTC.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the function of obj_RTC.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef OBJ_RTC_H
#define OBJ_RTC_H

#include "hd_RTC.h"
//#include "obj_RTC.h"
//#include "task_RTC.h"


// sync cycle, in minute
#define RTC_SYNC_CYC    1

/*******************************************************************************
                         Define object property
*******************************************************************************/
//#pragma pack(1)
typedef struct
{
    RTC_DateTypeDef date;
    RTC_TimeTypeDef time;
}RtcTypeDef;
//#pragma pack()
/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
void RtcInit(void);
void RtcSetDate(RTC_DateTypeDef iDate);
RTC_DateTypeDef RtcGetDate(void);
void RtcSetTime(RTC_TimeTypeDef iTime);
RTC_TimeTypeDef RtcGetTime(void);
void RtcSyncSend(void);
void RtcSyncReceive(RtcTypeDef iRecDate);
void RtcSet(RtcTypeDef iRecDate);
void RtcTimerEventProcess(void);
unsigned char GetRtcState(unsigned char msg_id);
/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/
void TimerRtcCallBack(void);

#endif
