#!/bin/bash

function recursive_dir()
{
	local curdir subdir
	curdir=$(pwd)
	for subdir in $(ls ${curdir})
	do
		if test -d ${subdir}
		then
			if [ ${subdir} != "onvif_service" ];then
				rm ${curdir}/${subdir}/Makefile
				cp ${SUBMF} ${curdir}/${subdir}/Makefile
			fi
			cd ${subdir}
			recursive_dir
			cd ..
		else
			file=${curdir}/${subdir}
			filename=`basename ${file}`
			name=${filename%%.*}
			ext=${filename##*.}
			if [ ${ext} == "h" ];then				
				#ln -s ${file} ${INCPATH}/${filename}
				echo ${INCPATH}/${filename}
			fi
		fi
	done
}
SUBMF=$(pwd)/Makefile_SUB
INCPATH=$(pwd)/include
APPPATH=$(pwd)/app
OSPATH=$(pwd)/os

#cd ${INCPATH}
#rm *.h
#cd ..
cp ${SUBMF} ${APPPATH}/Makefile
cp ${SUBMF} ${OSPATH}/Makefile
cd ${APPPATH}
recursive_dir
cd ..
cd ${OSPATH}
recursive_dir
cd ..

